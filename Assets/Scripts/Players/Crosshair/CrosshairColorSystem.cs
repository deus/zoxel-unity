using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;             // for target
using Zoxel.AI;             // for target

namespace Zoxel.Players.UI
{
    //! Recolors the Crosshair based on target.
    /**
    *   \todo Remove use of attackAngle constant and use data that skill uses.
    *   \todo Make lerp over time when changing animation state.
    */
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class CrosshairColorSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery charactersQuery;
        const float attackDistance = 1.8f;
        const float attackAngle = 0.22f;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            charactersQuery = GetEntityQuery(
                ComponentType.ReadOnly<PlayerCharacter>(),
                ComponentType.ReadOnly<Target>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(charactersQuery);
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var crosshairDefaultColor = CrosshairSpawnSystem.crosshairDefaultColor;
            var crosshairTargetColor = CrosshairSpawnSystem.crosshairTargetColor;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var characterEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var targets = GetComponentLookup<Target>(true);
            characterEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity>()
                .WithAll<Crosshair>()
                .ForEach((Entity e, int entityInQueryIndex, ref CrosshairState crosshairState, in CharacterLink characterLink) =>
            {
                var isFacingTarget = false;
                if (targets.HasComponent(characterLink.character))
                {
                    var target = targets[characterLink.character];
                    isFacingTarget = target.target.Index > 0 && target.targetDistance < attackDistance && math.abs(target.targetAngle) <= attackAngle;
                }
                if (crosshairState.state == 0)
                {
                    if (isFacingTarget)
                    {
                        crosshairState.state = 1;
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e, new MaterialBaseColor(crosshairTargetColor));
                        //PostUpdateCommands.SetComponent(entityInQueryIndex, e, new MaterialFrameColor { Value = new float4(0, 0, 0, 1) });
                    }
                }
                else
                {
                    if (!isFacingTarget)
                    {
                        crosshairState.state = 0;
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e, new MaterialBaseColor(crosshairDefaultColor));
                        //PostUpdateCommands.SetComponent(entityInQueryIndex, e, new MaterialFrameColor { Value = new float4(0, 0, 0, 1) });
                    }
                }
            })  .WithReadOnly(targets)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}