﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Cameras;
using Zoxel.UI;
using Zoxel.Input;
using Zoxel.Cameras.TopDown;
using Zoxel.UI.MouseFollowing;

namespace Zoxel.Players.Spawning
{
    //! Spawns player entities with controllers.
    /**
    *   When first player connects to StartScreen.
    */
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class PlayerSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery devicesQuery;
        private EntityQuery mousesQuery;
        private EntityQuery keyboardsQuery;
        private Entity playerPrefab;
        private NativeArray<Text> texts;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            devicesQuery = GetEntityQuery(
                ComponentType.Exclude<DeviceConnected>(),
                ComponentType.ReadOnly<DeviceConnecting>(),
                ComponentType.ReadOnly<Device>());
            mousesQuery = GetEntityQuery(
                ComponentType.Exclude<DeviceConnected>(),
                ComponentType.ReadOnly<Mouse>(),
                ComponentType.ReadOnly<Device>());
            keyboardsQuery = GetEntityQuery(
                ComponentType.Exclude<DeviceConnected>(),
                ComponentType.ReadOnly<Keyboard>(),
                ComponentType.ReadOnly<Device>());
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnTooltips),
                typeof(SpawnMouseUI),
                typeof(SpawnScreenFader),
                typeof(ZoxID),
                typeof(Player),
                typeof(ColorData),
                typeof(Controller),     // Input
                typeof(DeviceTypeData),
                typeof(ConnectedDevices),
                typeof(CameraLink),     // Attached to Camera
                typeof(CharacterLink),  // Attached to Character
                typeof(PlayerHomeLink),
                typeof(RealmLink),      // do I need this
                typeof(VoxLink),        // do I need this
                typeof(PlayerTooltipLinks),
                typeof(PlayerPanelTooltipLink),
                typeof(MouseUILink),
                typeof(ScreenFaderLink),
                typeof(Navigator),
                typeof(NavigatorSelected),
                typeof(UILink)
            );
            playerPrefab = EntityManager.CreateEntity(archetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(playerPrefab); // , new EditorName("[top-down-camera]"));
            #endif
            texts = new NativeArray<Text>(1, Allocator.Persistent);
            texts[0] = new Text("[player]");
            RequireForUpdate(processQuery);
            RequireForUpdate(devicesQuery);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // - [ ] press start to spawn a player connected to an input
            //  if press start again, after connected, destroy entity.
            var newPlayerID = IDUtil.GenerateUniqueID();
            var mainMenuCameraEntity = new Entity();
            if (CameraReferences.cameras.Count > 0)
            {
                mainMenuCameraEntity = CameraReferences.cameras[0];
            }
            var spawnTopDownCameraEventPrefab = TopDownCameraSpawnSystem.spawnTopDownCameraEventPrefab;
            var playerPrefab = this.playerPrefab;
            var spawnCameraPosition = float3.zero; // CameraManager.instance.cameraSettings.spawnCameraPosition;
            var isTopDownCamera = CameraManager.instance.cameraSettings.isTopDownCamera;
            var texts = this.texts;
            var playerHomeEntity = PlayerSystemGroup.playerHome;
            var deviceEntities = devicesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var deviceTypeDatas = GetComponentLookup<DeviceTypeData>(true);
            var mouseEntities = mousesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var keyboardEntities = keyboardsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            Entities
                .WithNone<DestroyEntity>()
                .WithAll<DestroyOnConnectedPlayer>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                if (deviceEntities.Length != 0)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e);
                    if (playerHomeEntity.Index != 0 && !isTopDownCamera)
                    {
                        PostUpdateCommands.AddComponent<OnTitlescreenProgressed>(entityInQueryIndex, playerHomeEntity);
                    }
                }
            })  .WithReadOnly(deviceEntities)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<DeviceConnector>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                if (deviceEntities.Length == 0)
                {
                    return;
                }
                // UnityEngine.Debug.LogError("Player Spawning: " + e.Index);
                var deviceEntity = deviceEntities[0];
                var deviceType = deviceTypeDatas[deviceEntity].type;
                var connectedDevices = new ConnectedDevices();
                connectedDevices.Add(deviceEntity);
                // if keyboard, find first mouse
                if (HasComponent<Keyboard>(deviceEntity))
                {
                    if (mouseEntities.Length > 0)
                    {
                        // UnityEngine.Debug.LogError("Mouse Device Found: " + mouseEntities[0].Index);
                        connectedDevices.Add(mouseEntities[0]);
                    }
                }
                if (HasComponent<Mouse>(deviceEntity))
                {
                    deviceType = DeviceType.Keyboard;
                    if (keyboardEntities.Length > 0)
                    {
                        // UnityEngine.Debug.LogError("Mouse Device Found: " + mouseEntities[0].Index);
                        connectedDevices.Add(keyboardEntities[0]);
                    }
                }
                var thisID = newPlayerID + entityInQueryIndex;
                var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, playerPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ZoxID(thisID));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Controller(ControllerMapping.Menu));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new DeviceTypeData(deviceType));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, connectedDevices);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new CameraLink(mainMenuCameraEntity));
                var random = new Random();
                random.InitState((uint) thisID);
                var color = Color.FromHSV(new float3(random.NextInt(0, 360), random.NextInt(50, 80), random.NextInt(50, 80)));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ColorData(color));
                if (playerHomeEntity.Index != 0)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, playerHomeEntity, new OnPlayerSpawned(e2));
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new PlayerHomeLink(playerHomeEntity));
                }
                for (int i = 0; i < connectedDevices.devices.Length; i++)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, connectedDevices.devices[i], new DeviceConnected(e2));
                }
                if (isTopDownCamera)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex,
                        PostUpdateCommands.Instantiate(entityInQueryIndex, spawnTopDownCameraEventPrefab),
                        new SpawnTopDownCamera(e2, new float3(0, 96, 0)));
                }
                #if UNITY_EDITOR
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new EditorName(texts[0].Clone()));
                #endif
            })  .WithReadOnly(deviceEntities)
                .WithReadOnly(mouseEntities)
                .WithReadOnly(keyboardEntities)
                .WithReadOnly(deviceTypeDatas)
                .WithDisposeOnCompletion(deviceEntities)
                .WithDisposeOnCompletion(mouseEntities)
                .WithDisposeOnCompletion(keyboardEntities)
                .WithDisposeOnCompletion(deviceTypeDatas)
                #if UNITY_EDITOR
                .WithReadOnly(texts)
                #endif
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                /*var connectedDeviceIDs = new NativeList<int>();
                var pad = InputUtil.GetConnectingGamepad(in connectedDeviceIDs);
                var keyboard = InputUtil.GetConnectingKeyboard(in connectedDeviceIDs);
                var touchpad = InputUtil.GetConnectingTouchpad(in connectedDeviceIDs);
                var isPad = pad != null;
                var isKeyboard = keyboard != null;
                var isTouch = touchpad != null;
                if (isPad || isKeyboard || isTouch)*/
                    /*UnityEngine.InputSystem.InputDevice inputDevice = null;
                    if (isPad)
                    {
                        deviceType = DeviceType.Gamepad;
                        inputDevice = pad;
                    }
                    else if (isKeyboard)
                    {
                        deviceType = DeviceType.Keyboard;
                        inputDevice = keyboard;
                    }
                    else if (isTouch)
                    {
                        deviceType = DeviceType.Touch;
                        inputDevice = touchpad;
                    }*/

// Move this to GamePlayerSystem - move this system to game
// Removes Transform from player, it doesn't need one
// keep all player only stuff on this
// foreach (var cameraEntity in CameraReferences.cameras)
// PostUpdateCommands.AddComponent(cameraEntity, new DestroyEntity());
// if main menu, check if another controller connects
// Still using EntityManager to spawn controller for now
//      need to create a set parent component
//      Pass in all these entities into game, and set accordingly, remove components after
// for a certain player handle connecting and reconnecting and switching controllers
// DeviceConnectedSystem
// Probaly I can control this event from realms - or core - spawn player - main menu spawns after.. ?


                // connectedDeviceIDs.Dispose();
                // Spawn camera, link to controller
                /*var cameraSpawnPosition = spawnCameraPosition + new float3((CameraReferences.cameras.Count - 1) * 32, 0, 0);
                var spawnCameraEntity = PostUpdateCommands.CreateEntity();
                if (isTopDownCamera)
                {
                    PostUpdateCommands.AddComponent(spawnCameraEntity, new SpawnTopDownCamera(e2, cameraSpawnPosition + new float3(0, 96, 0)));
                }
                else
                {
                    PostUpdateCommands.AddComponent(spawnCameraEntity, new SpawnFirstPersonCamera(e2, cameraSpawnPosition));
                }
                // Spawns main menu for all players - todo: do this only, spawn controller when one connects to the application
                // Removes StartScreen camera.
                for (int i = CameraReferences.cameras.Count - 1; i >= 0; i--)
                {
                    var cameraEntity = CameraReferences.cameras[i];
                    PostUpdateCommands.AddComponent(cameraEntity, new DestroyEntityInFrames(1));
                    CameraReferences.cameras.RemoveAt(i);
                }*/