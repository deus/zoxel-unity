using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Cameras;
using Zoxel.UI;
using Zoxel.Input;
using Zoxel.Cameras.TopDown;

namespace Zoxel.Players.Spawning
{
    //! Spawns player entities with controllers.
    /**
    *   When first player connects to StartScreen.
    */
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class PlayerRemoveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<DeviceDisconnecting>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref PlayerLinks playerLinks, in OnPlayerDestroyed onPlayerDestroyed) =>
            {
                PostUpdateCommands.RemoveComponent<OnPlayerDestroyed>(entityInQueryIndex, e);
                // UnityEngine.Debug.LogError("Removing player: " + onPlayerDestroyed.player.Index);
                if (playerLinks.players.Length > 1 && onPlayerDestroyed.player == playerLinks.players[0])
                {
                    // spawn new main menu for player 2 when player 1 is removed
                    PostUpdateCommands.AddComponent<PlayerOne>(entityInQueryIndex, playerLinks.players[1]);
                }
                playerLinks.Remove(onPlayerDestroyed.player);
                PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, onPlayerDestroyed.player);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            if (processQuery.IsEmpty)
            {
                return;
            }
            var deviceEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var deviceDisconnectings = GetComponentLookup<DeviceDisconnecting>(true);
            deviceEntities.Dispose();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, in ConnectedDevices connectedDevices, in PlayerHomeLink playerHomeLink) =>
            {
                // UnityEngine.Debug.LogError("Checking player connections: " + connectedDevices.devices.Length);
                var isDisconnecting = false;
                for (byte i = 0; i < connectedDevices.devices.Length; i++)
                {
                    var deviceEntity = connectedDevices.devices[i];
                    if (deviceDisconnectings.HasComponent(deviceEntity))
                    {
                        isDisconnecting = true;
                        // UnityEngine.Debug.LogError("Disconnecting player: " + e.Index);
                        break;
                    }
                }
                if (isDisconnecting)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, playerHomeLink.playerHome, new OnPlayerDestroyed(e));
                    for (byte i = 0; i < connectedDevices.devices.Length; i++)
                    {
                        var deviceEntity = connectedDevices.devices[i];
                        PostUpdateCommands.RemoveComponent<DeviceConnected>(entityInQueryIndex, deviceEntity);
                    }
                }
            })  .WithReadOnly(deviceDisconnectings)
                .WithDisposeOnCompletion(deviceDisconnectings)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

/*if (!HasComponent<RemovePlayer>(playerHomeLink.playerHome))
{
    PostUpdateCommands.AddComponent(entityInQueryIndex, playerHomeLink.playerHome, new RemovePlayer(e));
}*/

/*Dependency = Entities
    .WithNone<DestroyEntity>()
    .ForEach((Entity e, int entityInQueryIndex, ref PlayerLinks playerLinks, in RemovePlayer removePlayer) =>
{
    PostUpdateCommands.RemoveComponent<RemovePlayer>(entityInQueryIndex, e);
    // UnityEngine.Debug.LogError("Removing player: " + removePlayer.player.Index);
    playerLinks.Remove(removePlayer.player);
    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnPlayerDestroyed(removePlayer.player));
}) .ScheduleParallel(Dependency);
// commandBufferSystem.AddJobHandleForProducer(Dependency);*/