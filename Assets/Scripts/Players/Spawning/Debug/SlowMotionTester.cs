using UnityEngine;
using UnityEngine.InputSystem;

namespace Zoxel
{
    //! Uses f5, f6, f7 to alter time scales for play testing.
    /**
    *   \todo Move this to a debug namespace?  Use a system and Controller to effect time scales.
    */
    public partial class SlowMotionTester : MonoBehaviour
    {
        // Update is called once per frame
        void Update()
        {
            var amount = 0.1f;
            if (Time.timeScale <= 0.04f)
            {
                amount = 0.003f;
            }
            else if (Time.timeScale <= 0.1f)
            {
                amount = 0.02f;
            }
            else if (Time.timeScale <= 0.3f)
            {
                amount = 0.04f;
            }
            else if (Time.timeScale >= 3f)
            {
                amount = 0.5f;
            }
            else if (Time.timeScale >= 2f)
            {
                amount = 0.25f;
            }
            if (Keyboard.current.f5Key.wasPressedThisFrame)
            {
                Time.timeScale -= amount;
                UnityEngine.Debug.LogError("Decreasing Time Scale to: " + Time.timeScale + " by " + amount);
            }
            else if (Keyboard.current.f6Key.wasPressedThisFrame)
            {
                Time.timeScale += amount;
                UnityEngine.Debug.LogError("Increasing Time Scale to: " + Time.timeScale + " by " + amount);
            }
            else if (Keyboard.current.f7Key.wasPressedThisFrame)
            {
                Time.timeScale = 1f;
                UnityEngine.Debug.LogError("Restored Time Scale to: " + Time.timeScale);
            }
        }
    }
}