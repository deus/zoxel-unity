using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Players.UI
{
    //! Delays game ui changes too fast.
    /**
    *   - End System -
    *   Removes NavigateDelay after 4 frames.
    */
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class NavigateDelayEndSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities
                .WithNone<DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref NavigateDelay navigateDelay) =>
            {
                navigateDelay.count++;
                if (navigateDelay.count == 4)
                {
                    PostUpdateCommands.RemoveComponent<NavigateDelay>(entityInQueryIndex, e);
                }
			}).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}