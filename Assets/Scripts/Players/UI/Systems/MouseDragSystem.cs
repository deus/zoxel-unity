using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Input;
using Zoxel.UI;

namespace Zoxel.Players.UI
{
    //! Updates mouse drag event with Mouse device data.
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class MouseDragSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery devicesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            devicesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Device>());
            RequireForUpdate(processQuery);
            RequireForUpdate(devicesQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var deviceEntities = devicesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var mouses = GetComponentLookup<Mouse>(true);
            var touchpads = GetComponentLookup<Touchpad>(true);
            deviceEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref MouseDragEvent mouseDragEvent) =>
            {
                var targetDeviceEntity = mouseDragEvent.mouse;
                if (mouses.HasComponent(targetDeviceEntity))
                {
                    var mouse = mouses[targetDeviceEntity];
                    if (mouse.leftButton.isPressed == 0)
                    {
                        mouseDragEvent.delta = float2.zero;
                        PostUpdateCommands.RemoveComponent<MouseDragEvent>(entityInQueryIndex, e);
                        return;
                    }
                    mouseDragEvent.delta = mouse.position - mouseDragEvent.position;
                    mouseDragEvent.position = mouse.position;
                }
                else if (touchpads.HasComponent(targetDeviceEntity))
                {
                    var touchpad = touchpads[targetDeviceEntity];
                    bool didFind;
                    var finger = touchpad.GetFinger(mouseDragEvent.fingerID, out didFind);
                    if (!didFind || finger.state == FingerState.End)
                    {
                        mouseDragEvent.delta = float2.zero;
                        PostUpdateCommands.RemoveComponent<MouseDragEvent>(entityInQueryIndex, e);
                        return;
                    }
                    mouseDragEvent.delta = finger.position - mouseDragEvent.position;
                    mouseDragEvent.position = finger.position;
                }
                else // if (!isMouse && !touchpads.HasComponent(targetDeviceEntity))
                {
                    mouseDragEvent.delta = float2.zero;
                    PostUpdateCommands.RemoveComponent<MouseDragEvent>(entityInQueryIndex, e);
                }
                // UnityEngine.Debug.LogError("Mouse Drag Delta: " + mouseDragEvent.delta);
			})  .WithReadOnly(mouses)
                .WithReadOnly(touchpads)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}