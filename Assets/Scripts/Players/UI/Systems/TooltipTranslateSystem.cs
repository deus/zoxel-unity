using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Zoxel.UI;
using Zoxel.Input;

namespace Zoxel.Players.UI
{
    //! This system converts [tags] into useful information based on Input Type, using SetTooltipText.
    /**
    *   \todo Convert to Parallel.
    *   \todo Converting to glyphs or symbols? (like Ben10)
    */
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class TooltipTranslateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities
                .WithNone<SetRenderText, RenderTextDirty, OnRenderTextSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref SetTooltipText setTooltipText, in CharacterLink characterLink) =>
            {
                var characterEntity = characterLink.character;
                Entity controllerEntity;
                if (!HasComponent<Controller>(characterEntity))
                {
                    if (!HasComponent<Character>(characterEntity))
                    {
                        return;
                    }
                    controllerEntity = EntityManager.GetComponentData<ControllerLink>(characterEntity).controller;
                    // UnityEngine.Debug.LogError("Not player, is character? " + HasComponent<Character>(characterLink.character));
                }
                else
                {
                    controllerEntity = characterEntity;
                }
                // var controller = EntityManager.GetComponentData<Controller>(controllerEntity);
                var deviceTypeData = EntityManager.GetComponentData<DeviceTypeData>(controllerEntity);
                var tooltipText = setTooltipText.text.ToString();
                if (deviceTypeData.type == DeviceType.Gamepad)
                {
                    tooltipText = tooltipText.Replace("[trigger1]", "Right Trigger");
                    tooltipText = tooltipText.Replace("[action1]", "Button A");
                    tooltipText = tooltipText.Replace("[action2]", "Button RT");
                    tooltipText = tooltipText.Replace("[action3]", "Button X");
                    tooltipText = tooltipText.Replace("[action4]", "Button Y");
                    tooltipText = tooltipText.Replace("[cancelAction]", "Button B");
                }
                else if (deviceTypeData.type == DeviceType.Keyboard)
                {
                    tooltipText = tooltipText.Replace("[trigger1]", "Left Click");
                    tooltipText = tooltipText.Replace("[action1]", "Left Click");
                    tooltipText = tooltipText.Replace("[action2]", "Right Click");
                    tooltipText = tooltipText.Replace("[action3]", "Control Right Click");
                    // tooltipText = tooltipText.Replace("[action4]", "F Key");
                    tooltipText = tooltipText.Replace("[cancelAction]", "Escape Key");
                }
                setTooltipText.text.SetText(tooltipText);
            }).WithoutBurst().Run();
            Dependency = Entities
                .WithNone<SetRenderText, RenderTextDirty, OnRenderTextSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, in SetTooltipText setTooltipText) =>
            {
                PostUpdateCommands.RemoveComponent<SetTooltipText>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetRenderText(setTooltipText.text));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}