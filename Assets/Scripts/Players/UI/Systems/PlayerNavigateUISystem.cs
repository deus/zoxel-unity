﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Input;

namespace Zoxel.Players.UI
{
    //! Player can navigate the UI through input.
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class PlayerNavigateUISystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var navigationThreshold = 0.6;
            var clickRate = 0.3;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities
                .WithNone<ControllerDisabled, SetControllerMapping, NavigateDelay>()
                .ForEach((Entity e, int entityInQueryIndex, ref Navigator navigator, in Controller controller, in DeviceTypeData deviceTypeData) =>
            {
                if (navigator.navigateData.Length == 0)
                {
                    return;
                }
                var isNavigateRight = false;
                var isNavigateLeft = false;
                var isNavigateUp = false;
                var isNavigateDown = false;
                // actionbar in game
                if (controller.mapping == ControllerMapping.InGame)    // in game
                {
                    isNavigateUp = false;
                    isNavigateDown = false;
                    if (deviceTypeData.type == DeviceType.Keyboard)
                    {
                        // for mouse
                        if (controller.mouse.scroll.y > 0)
                        {
                            isNavigateRight = true;
                        }
                        else if (controller.mouse.scroll.y < 0)
                        {
                            isNavigateLeft = true;
                            //UnityEngine.Debug.LogError("Scrolling Left.");
                        }
                        // UnityEngine.Debug.LogError("Scrolling [" + controller.mouse.scroll.y + "].");
                    }
                    // for gamepad
                    else
                    {
                        isNavigateRight = controller.gamepad.buttonRB.wasPressedThisFrame == 1;
                        isNavigateLeft = controller.gamepad.buttonLB.wasPressedThisFrame == 1;
                    }
                }
                else if (controller.mapping == ControllerMapping.Menu || controller.mapping == ControllerMapping.RealmUI || controller.mapping == ControllerMapping.Dialogue)
                {
                    if (deviceTypeData.type == DeviceType.Gamepad)
                    {
                        isNavigateRight = controller.gamepad.leftStick.x > navigationThreshold;
                        isNavigateLeft = controller.gamepad.leftStick.x < -navigationThreshold;
                        isNavigateUp = controller.gamepad.leftStick.y > navigationThreshold;
                        isNavigateDown = controller.gamepad.leftStick.y < -navigationThreshold;
                    }
                }
                else
                {
                    // if mapping is input
                    return;
                }
                if (!isNavigateRight && !isNavigateLeft && !isNavigateUp && !isNavigateDown)
                {
                    return;
                }
                // timer
                var timePassed = elapsedTime - navigator.lastInputTime;
                if (controller.wasInputLastFrame == 0 || timePassed >= clickRate)
                {
                    navigator.lastInputTime = elapsedTime;
                    var selectedIndex = 0;
                    for (int i = 0; i < navigator.navigateData.Length; i++)
                    {
                        if (navigator.navigateData[i].button == navigator.selected)
                        {
                            selectedIndex = i;
                            break;
                        }
                    }
                    var thisPosition = navigator.navigateData[selectedIndex].targetPosition;
                    // UnityEngine.Debug.LogError("Navigating at: " + selectedIndex + ": " + navigator.navigateData.Length);
                    for (int i = 0; i < navigator.navigateData.Length; i++)
                    {
                        var navigateUIElement = navigator.navigateData[i];
                        var checkPosition = navigateUIElement.previousPosition;
                        if (thisPosition.x == checkPosition.x && thisPosition.y == checkPosition.y)
                        {
                            if ((isNavigateRight && navigateUIElement.direction == NavigationUIDirection.Right)
                                || (isNavigateLeft && navigateUIElement.direction == NavigationUIDirection.Left)
                                || (isNavigateUp && navigateUIElement.direction == NavigationUIDirection.Up)
                                || (isNavigateDown && navigateUIElement.direction == NavigationUIDirection.Down))
                            {
                                // UnityEngine.Debug.LogError("Navigating: " + navigateUIElement.direction + " :: " + navigateUIElement.button.Index);
                                PostUpdateCommands.AddComponent<NavigateDelay>(entityInQueryIndex, e);
                                PostUpdateCommands.AddComponent(entityInQueryIndex, e,
                                    new SelectionEvent(SelectionEventType.Input, e, navigateUIElement.button, navigateUIElement.targetIndex));
                                break;
                            }
                        }
                    }
                }
			}).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}