using Unity.Entities;

namespace Zoxel.Players
{
    //! Delays game ui changes too fast
    public struct NavigateDelay : IComponentData
    {
        public byte count;
    }
}