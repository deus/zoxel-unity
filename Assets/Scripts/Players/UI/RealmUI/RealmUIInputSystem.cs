﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Actions;
using Zoxel.Actions.UI;
using Zoxel.Input;
using Zoxel.Realms.UI;

namespace Zoxel.Players.UI
{
    //! Switches between character game uis.
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class RealmUIInputSystem : SystemBase
    {
        private const float gameUISwitchDelay = 0.4f;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllerQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllerQuery = GetEntityQuery(
                ComponentType.ReadOnly<Controller>(),
                ComponentType.Exclude<DestroyEntity>());
            controllerQuery.SetChangedVersionFilter(typeof(Controller));
			RequireForUpdate(processQuery);
			RequireForUpdate(controllerQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var maxUIs = (byte) (PanelType.MaxRealmUI - 1); // 7;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = controllerQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var controllers = GetComponentLookup<Controller>(true);
            var deviceTypeDatas = GetComponentLookup<DeviceTypeData>(true);
            controllerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<RealmUIDelay>()
                .WithNone<DeadEntity, DestroyEntity, SwitchRealmUI>()
                .ForEach((Entity e, int entityInQueryIndex, in UILink uiLink, in RealmUILink realmUILink, in ControllerLink controllerLink) =>
            {
                if (!controllers.HasComponent(controllerLink.controller)
                    || HasComponent<ControllerDisabled>(controllerLink.controller)
                    || HasComponent<SetControllerMapping>(controllerLink.controller))
                {
                    return;
                }
                var controller = controllers[controllerLink.controller];
                var deviceTypeData = deviceTypeDatas[controllerLink.controller];
                if (controller.mapping == ControllerMapping.RealmUI)
                {
                    if (deviceTypeData.type == DeviceType.Gamepad)
                    {
                        if (controller.gamepad.buttonRB.wasPressedThisFrame == 1 || controller.gamepad.buttonLB.wasPressedThisFrame == 1)
                        {
                            var newUIIndex = realmUILink.index;
                            if (controller.gamepad.buttonRB.wasPressedThisFrame == 1)
                            {
                                if (newUIIndex == maxUIs) // max
                                {
                                    newUIIndex = 0;
                                }
                                else
                                {
                                    newUIIndex += 1;
                                }
                            }
                            else if (controller.gamepad.buttonLB.wasPressedThisFrame == 1)
                            {
                                if (newUIIndex == 0)
                                {
                                    newUIIndex = maxUIs; // max
                                }
                                else
                                {
                                    newUIIndex -= 1;
                                }
                            }
                            // PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SwitchRealmUI(newUIIndex));
                            for (int i = 0; i < uiLink.uis.Length; i++)
                            {
                                var uiEntity = uiLink.uis[i];
                                if (HasComponent<Taskbar>(uiEntity))
                                {
                                    PostUpdateCommands.AddComponent(entityInQueryIndex, uiEntity,
                                        new ClickPanelChild(controllerLink.controller, newUIIndex, deviceTypeData.type, ButtonActionType.Confirm));
                                    break;
                                }
                            }
                        }
                    }
                }
                else if (controller.mapping == ControllerMapping.InGame)
                {
                    if (deviceTypeData.type == DeviceType.Keyboard)
                    {
                        var selectAction = (byte) 255;
                        if (controller.keyboard.digit1Key.wasPressedThisFrame == 1)
                        {
                            selectAction = 0;
                        }
                        else if (controller.keyboard.digit2Key.wasPressedThisFrame == 1)
                        {
                            selectAction = 1;
                        }
                        else if (controller.keyboard.digit3Key.wasPressedThisFrame == 1)
                        {
                            selectAction = 2;
                        }
                        else if (controller.keyboard.digit4Key.wasPressedThisFrame == 1)
                        {
                            selectAction = 3;
                        }
                        else if (controller.keyboard.digit5Key.wasPressedThisFrame == 1)
                        {
                            selectAction = 4;
                        }
                        else if (controller.keyboard.digit6Key.wasPressedThisFrame == 1)
                        {
                            selectAction = 5;
                        }
                        else if (controller.keyboard.digit7Key.wasPressedThisFrame == 1)
                        {
                            selectAction = 6;
                        }
                        else if (controller.keyboard.digit8Key.wasPressedThisFrame == 1)
                        {
                            selectAction = 7;
                        }
                        else if (controller.keyboard.digit9Key.wasPressedThisFrame == 1)
                        {
                            selectAction = 8;
                        }
                        else if (controller.keyboard.digit0Key.wasPressedThisFrame == 1)
                        {
                            selectAction = 9;
                        }
                        if (selectAction != 255)
                        {
                            PostUpdateCommands.AddComponent<RealmUIDelay>(entityInQueryIndex, e);
                            for (int i = 0; i < uiLink.uis.Length; i++)
                            {
                                var uiEntity = uiLink.uis[i];
                                if (HasComponent<Actionbar>(uiEntity))
                                {
                                    PostUpdateCommands.AddComponent(entityInQueryIndex, uiEntity, new SelectPanelChild(controllerLink.controller, selectAction));
                                    break;
                                }
                            }
                        }
                    }
                }
            })  .WithReadOnly(controllers)
                .WithReadOnly(deviceTypeDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}