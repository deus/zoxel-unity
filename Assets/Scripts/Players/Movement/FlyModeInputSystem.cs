using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Input;
using Zoxel.Movement.Authoring;

namespace Zoxel.Players
{
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class FlyModeInputSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery controllerQuery;

        protected override void OnCreate()
        {
            controllerQuery = GetEntityQuery(
                ComponentType.ReadOnly<Controller>(),
                ComponentType.Exclude<DestroyEntity>());
            controllerQuery.SetChangedVersionFilter(typeof(Controller));
            RequireForUpdate(processQuery);
            RequireForUpdate<PhysicsSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var physicsSettings = GetSingleton<PhysicsSettings>();
            var forcePower = physicsSettings.flyForce; // 24;
            var flyForce = new float3(0, 1, 0) * forcePower;
            var moveForceX = new float3(1, 0, 0) * forcePower;
            var moveForceZ = new float3(0, 0, 1) * forcePower;
            var controllerEntities = controllerQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var controllers = GetComponentLookup<Controller>(true);
            controllerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<FlyMode>()
                .ForEach((ref BodyForce bodyForce, in ControllerLink controllerLink, in Rotation rotation) =>
            {
                if (HasComponent<ControllerDisabled>(controllerLink.controller))
                {
                    return;
                }
                var controller = controllers[controllerLink.controller];
                if (controller.mapping == (ControllerMapping.InGame))
                {
                    var playerAccelaration = new float3();
                    if (controller.keyboard.wKey.isPressed == 1)
                    {
                        playerAccelaration += moveForceZ;
                    }
                    else if (controller.keyboard.sKey.isPressed == 1)
                    {
                        playerAccelaration -= moveForceZ;
                    }
                    /*else
                    {
                        bodyForce.velocity.z *= 0.8f;
                    }*/
                    if (controller.keyboard.dKey.isPressed == 1)
                    {
                        playerAccelaration += moveForceX;
                    }
                    else if (controller.keyboard.aKey.isPressed == 1)
                    {
                        playerAccelaration -= moveForceX;
                    }
                    /*else
                    {
                        bodyForce.velocity.x *= 0.8f;
                    }*/

                    if (controller.keyboard.eKey.isPressed == 1)
                    {
                        playerAccelaration += flyForce;
                    }
                    else if (controller.keyboard.qKey.isPressed == 1)
                    {
                        playerAccelaration -= flyForce;
                    }
                    var currentVelocity = math.mul(math.inverse(rotation.Value), bodyForce.velocity);
                    if (playerAccelaration.x == 0 && playerAccelaration.z == 0)
                    {
                        //bodyForce.velocity.x *= 0.8f;
                        //bodyForce.velocity.z *= 0.8f;
                        bodyForce.velocity -= math.mul(rotation.Value, new float3(currentVelocity.x, 0, currentVelocity.z) * 0.8f); // ;
                    }
                    if (playerAccelaration.y == 0)
                    {
                        bodyForce.velocity -= math.mul(rotation.Value, new float3(0, currentVelocity.y, 0) * 0.8f); // ;
                    }
                    bodyForce.acceleration += math.mul(rotation.Value, playerAccelaration);
                }
            })  .WithReadOnly(controllers)
                .ScheduleParallel(Dependency);
		}
    }
}