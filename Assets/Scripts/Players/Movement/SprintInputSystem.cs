using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Movement;
using Zoxel.Input;

namespace Zoxel.Players
{
    //! A sprinting player buff to movement speed.
    /**
    *   \todo Add post processing effects and camera changes to sprinting.
    *   \todo Integrate sprinting into a skill - with keybinding option in the skill book ui
    */
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class SprintSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<ControllerDisabled>()
                .ForEach((int entityInQueryIndex, in  Controller controller, in CharacterLink characterLink) =>
            {
                //! \todo Add ControllerDisconnected to input when it disconnects
                if (controller.mapping != (ControllerMapping.InGame) || HasComponent<OverSprinted>(characterLink.character))
                {
                    return;
                }
                if (controller.keyboard.leftShiftKey.wasPressedThisFrame == 1)
                {
                    if (!HasComponent<Sprinting>(characterLink.character))
                    {
                        PostUpdateCommands.AddComponent<Sprinting>(entityInQueryIndex, characterLink.character);
                    }
                }
                else if (controller.keyboard.leftShiftKey.wasReleasedThisFrame == 1)
                {
                    if (HasComponent<Sprinting>(characterLink.character))
                    {
                        PostUpdateCommands.RemoveComponent<Sprinting>(entityInQueryIndex, characterLink.character);
                        // UnityEngine.Debug.LogError("Removing Sprinting2!");
                    }
                    // UnityEngine.Debug.LogError("Removing Sprinting!");
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}