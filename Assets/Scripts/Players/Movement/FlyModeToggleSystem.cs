using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Zoxel.Movement;
using Zoxel.Input;

namespace Zoxel.Players
{
    //! Begins or ends fly mode.
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class FlyModeToggleSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // fly mode system
            Dependency = Entities
                .WithNone<ControllerDisabled>()
                .WithAll<CanFly>()
                .ForEach((int entityInQueryIndex, in  Controller controller, in CharacterLink characterLink) =>
            {
                if (controller.mapping != (ControllerMapping.InGame))
                {
                    return;
                }
                if (controller.keyboard.fKey.wasPressedThisFrame == 1)
                {
                    if (!HasComponent<FlyMode>(characterLink.character))
                    {
                        PostUpdateCommands.AddComponent<FlyMode>(entityInQueryIndex, characterLink.character);
                    }
                    else
                    {
                        PostUpdateCommands.RemoveComponent<FlyMode>(entityInQueryIndex, characterLink.character);
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}