﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Input;
using Zoxel.Movement.Authoring;

namespace Zoxel.Players
{
    //! Moves the player's character Body around using the Controller component
    /**
    *   Move player inputs to controlling the body
    *   XY controller goes to XZ body movement
    */
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class PlayerMoveInputSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllerQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllerQuery = GetEntityQuery(
                ComponentType.ReadOnly<Controller>(),
                ComponentType.Exclude<DestroyEntity>());
            controllerQuery.SetChangedVersionFilter(typeof(Controller));
            RequireForUpdate(processQuery);
            RequireForUpdate(controllerQuery);
            RequireForUpdate<PhysicsSettings>();
        }

		[BurstCompile]
        protected override void OnUpdate()
        {
            //! \todo Base movement speed off speed stat.
            var physicsSettings = GetSingleton<PhysicsSettings>();
            var sprintSpeedMultiplier = physicsSettings.sprintSpeedMultiplier; // 1.4f;
            var airSlowMultiplier = physicsSettings.airSlowMultiplier; // 6f
            var isAutoWalk = physicsSettings.isAutoWalk;
            var controllerEntities = controllerQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var controllers = GetComponentLookup<Controller>(true);
            controllerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<FlyMode>()
                .ForEach((Entity e, ref BodyForce bodyForce, in IsOnGround isOnGround, in BodyInnerForce innerBody, in Rotation rotation, in ControllerLink controllerLink) =>
            {
                if (HasComponent<ControllerDisabled>(controllerLink.controller))
                {
                    return;
                }
                var controller = controllers[controllerLink.controller];
                if (controller.mapping == ControllerMapping.InGame)    // in game
                {
                    var isSprinting = HasComponent<Sprinting>(e);
                    var movementForce = innerBody.movementForce;
                    var maxVelocity = innerBody.maxVelocity;
                    var accelaration = new float3();
                    var currentVelocity = math.mul(math.inverse(rotation.Value), bodyForce.velocity);
                    if (math.abs(controller.gamepad.leftStick.x) >= 0.001f)
                    {
                        if (math.abs(currentVelocity.x) < maxVelocity * 0.9f)
                        {
                            accelaration.x += controller.gamepad.leftStick.x * movementForce;
                        }
                    }
                    var isMoveForward = math.abs(controller.gamepad.leftStick.y) >= 0.001f || isAutoWalk;
                    if (isMoveForward && math.abs(currentVelocity.z) < maxVelocity)
                    {
                        if (isAutoWalk)
                        {
                            accelaration.z += 1f * movementForce;
                        }
                        else
                        {
                            accelaration.z += controller.gamepad.leftStick.y * movementForce;
                        }
                    }
                    // isn't this applied elsewhere.. friction system?
                    if (!isOnGround.isOnGround)
                    {
                        accelaration *= airSlowMultiplier;
                    }
                    if (isSprinting)
                    {
                        accelaration *= sprintSpeedMultiplier;
                    }
                    bodyForce.acceleration += math.mul(rotation.Value, accelaration);
                }
            })  .WithReadOnly(controllers)
                .ScheduleParallel(Dependency);
		}
    }
}