using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Movement;
using Zoxel.Input;
using Zoxel.Movement.Authoring;

namespace Zoxel.Players
{
    //! This handles player jump input.
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class PlayerJumpInputSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllerQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllerQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<ControllerDisabled>(),
                ComponentType.ReadOnly<Controller>());
            controllerQuery.SetChangedVersionFilter(typeof(Controller));
            RequireForUpdate(processQuery);
            RequireForUpdate(controllerQuery);
            RequireForUpdate<PhysicsSettings>();
        }
        
		[BurstCompile]
        protected override void OnUpdate()
        {
            var physicsSettings = GetSingleton<PhysicsSettings>();
            var isAutoWalk = physicsSettings.isAutoWalk;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = controllerQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var controllers = GetComponentLookup<Controller>(true);
            var controllerDisableds = GetComponentLookup<ControllerDisabled>(true);
            controllerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<Jumping>()
                .ForEach((Entity e, int entityInQueryIndex, in IsOnGround isOnGround, in ControllerLink controllerLink) =>
            {
                if (!controllers.HasComponent(controllerLink.controller) || controllerDisableds.HasComponent(controllerLink.controller))
                {
                    return;
                }
                var controller = controllers[controllerLink.controller];
                if (controller.mapping == (ControllerMapping.InGame))    // in game
                {
                    if (controller.gamepad.buttonA.wasPressedThisFrame == 1 || controller.gamepad.buttonA.isPressed == 1 || isAutoWalk)
                    {
                        if (isOnGround.isOnGround)
                        {
                            PostUpdateCommands.AddComponent<Jumping>(entityInQueryIndex, e);
                        }
                    }
                }
            })  .WithReadOnly(controllers)
                .WithReadOnly(controllerDisableds)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}