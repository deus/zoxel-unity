using Unity.Entities;
using Unity.Burst;
using Zoxel.UI;
using Zoxel.Cameras;
using Zoxel.Actions;
using Zoxel.Input;
using Zoxel.Realms.UI;
using Zoxel.Cameras.PostProcessing;

namespace Zoxel.Players.UI
{
    //! This system removes control away from the player while they use game uis & changes post processing
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class UnPauseSystem : SystemBase
    {
        const float removeDelay = 0.0f;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var elapsedTime = World.Time.ElapsedTime;
            var disableMinimap = UIManager.instance.uiSettings.disableMinimap;
            var spawnCharacterUIPrefab = UICoreSystem.spawnCharacterUIPrefab;
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<Speaking, SpawnRealmUI>()
                .WithAll<UnPauseGame>()
                .ForEach((Entity e, int entityInQueryIndex, in UILink uiLink, in CameraLink cameraLink, in UserActionLinks userActionLinks, in RealmUILink gameUILink, in ControllerLink controllerLink) =>
            {
                PostUpdateCommands.RemoveComponent<UnPauseGame>(entityInQueryIndex, e);
                // Update to In Realm Profile
                PostUpdateCommands.AddComponent(entityInQueryIndex, cameraLink.camera, new UpdatePostProcessing(PostProcessingProfileType.Game));
                // Remove paused state for character
                PostUpdateCommands.AddComponent(entityInQueryIndex, controllerLink.controller, new SetControllerMapping(ControllerMapping.InGame));
                PostUpdateCommands.RemoveComponent<DisableRaycaster>(entityInQueryIndex, e);
                PostUpdateCommands.RemoveComponent<DisableFirstPersonCamera>(entityInQueryIndex, cameraLink.camera);
                // Remove game UIs
                UICoreSystem.RemoveUI(PostUpdateCommands, entityInQueryIndex, removeCharacterUIPrefab, e, gameUILink.previousUI, removeDelay, elapsedTime);
                if (gameUILink.previousUI2.Index > 0)
                {
                    UICoreSystem.RemoveUI(PostUpdateCommands, entityInQueryIndex, removeCharacterUIPrefab, e, gameUILink.previousUI2, removeDelay, elapsedTime);
                }
                // Remove taskbar
                for (int i = 0; i < uiLink.uis.Length; i++)
                {
                    var ui = uiLink.uis[i];
                    if (HasComponent<Taskbar>(ui))
                    {
                        UICoreSystem.RemoveUI(PostUpdateCommands, entityInQueryIndex, removeCharacterUIPrefab, e, ui, removeDelay, elapsedTime);
                        break;
                    }
                }
                // Spawn in game UI back!
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SpawnRealmUI(SpawnRealmUIType.DisableSpawnActionbar));
                //! Disable Raycasting on Actionbar and set navigation back to it!
                for (int i = 0; i < uiLink.uis.Length; i++)
                {
                    var ui = uiLink.uis[i];
                    if (HasComponent<GameAndPauseUI>(ui))
                    {
                        PostUpdateCommands.AddComponent<DisablePanelRaycast>(entityInQueryIndex, ui);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, ui, new NavigationDirty(userActionLinks.lastSelectedAction));
                        break;
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}