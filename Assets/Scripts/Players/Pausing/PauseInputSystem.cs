using Unity.Entities;
using Unity.Burst;
using Zoxel.Input;
using Zoxel.Realms.UI;

namespace Zoxel.Players
{
    //! Input will add a PauseGame or UnPauseGame event to the character.
    /**
    *   This system removes control away from the player while they use game uis. Also changes post processing.
    */
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class PauseInputSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<ControllerDisabled, SetControllerMapping>()
                .WithNone<PauseGame, UnPauseGame>()
                .ForEach((int entityInQueryIndex, in Controller controller, in DeviceTypeData deviceTypeData, in CharacterLink characterLink) =>
            {
                if (HasComponent<RealmUIDelay>(characterLink.character))
                {
                    return;
                }
                byte isPauseGame = 0;
                if (controller.mapping == ControllerMapping.InGame)
                {
                    // UnityEngine.Debug.LogError("InGame.");
                    if (deviceTypeData.type == DeviceType.Gamepad)
                    {
                        if (controller.gamepad.startButton.wasPressedThisFrame == 1 || controller.gamepad.selectButton.wasPressedThisFrame == 1)
                        {
                            isPauseGame = 1;
                        }
                    }
                    else if (deviceTypeData.type == DeviceType.Keyboard)
                    {
                        if (controller.keyboard.leftAltKey.isPressed == 0 && controller.keyboard.rightAltKey.isPressed == 0 &&
                            (controller.keyboard.escapeKey.wasPressedThisFrame == 1 || controller.keyboard.enterKey.wasPressedThisFrame == 1))
                        {
                            isPauseGame = 1;
                        }
                    }
                    else // if (deviceTypeData.type == DeviceType.Gamepad)
                    {
                        if (controller.gamepad.startButton.wasPressedThisFrame == 1 || controller.gamepad.selectButton.wasPressedThisFrame == 1)
                        {
                            isPauseGame = 1;
                        }
                    }
                }
                else if (controller.mapping == ControllerMapping.RealmUI)
                {
                    if (deviceTypeData.type == DeviceType.Gamepad)
                    {
                        if (controller.gamepad.startButton.wasPressedThisFrame == 1
                            || controller.gamepad.selectButton.wasPressedThisFrame == 1
                            || controller.gamepad.buttonB.wasPressedThisFrame == 1)
                        {
                            isPauseGame = 2;
                        }
                    }
                    else if (deviceTypeData.type == DeviceType.Keyboard)
                    {
                        if (controller.keyboard.leftAltKey.isPressed == 0 && controller.keyboard.rightAltKey.isPressed == 0 &&
                            (controller.keyboard.escapeKey.wasPressedThisFrame == 1 || controller.keyboard.enterKey.wasPressedThisFrame == 1))
                        {
                            isPauseGame = 2;
                        }
                    }
                    else if (deviceTypeData.type == DeviceType.Gamepad)
                    {
                        if (controller.gamepad.startButton.wasPressedThisFrame == 1
                            || controller.gamepad.selectButton.wasPressedThisFrame == 1)
                        {
                            isPauseGame = 2;
                        }
                    }
                }
                if (isPauseGame == 1)
                {
                    // UnityEngine.Debug.LogError("Pausing.");
                    PostUpdateCommands.AddComponent<PauseGame>(entityInQueryIndex, characterLink.character);
                    PostUpdateCommands.AddComponent<RealmUIDelay>(entityInQueryIndex, characterLink.character);
                }
                else if (isPauseGame == 2)
                {
                    // UnityEngine.Debug.LogError("UnPausing.");
                    PostUpdateCommands.AddComponent<UnPauseGame>(entityInQueryIndex, characterLink.character);
                    PostUpdateCommands.AddComponent<RealmUIDelay>(entityInQueryIndex, characterLink.character);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}