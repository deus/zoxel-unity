using Unity.Entities;
using Unity.Burst;
using Zoxel.UI;
using Zoxel.Actions;
using Zoxel.Cameras;
using Zoxel.Maps;
using Zoxel.Input;
using Zoxel.Realms.UI;
using Zoxel.Cameras.PostProcessing;

namespace Zoxel.Players.UI
{
    //! Removes control away from the player while they use game uis, also changes post processing.
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class PauseSystem : SystemBase
    {
        const float removeDelay = 0f;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var disableTaskbar = UIManager.instance.uiSettings.disableTaskbar;
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities
                .WithNone<UnPauseGame, DeadEntity, DestroyEntity>()
                .WithNone<Speaking, SelectedActionUpdated, SpawnRealmUI>()
                .WithAll<PauseGame>()
                .ForEach((Entity e, int entityInQueryIndex, in UILink uiLink, in CameraLink cameraLink, in RealmUILink gameUILink, in ControllerLink controllerLink) =>
            {
                PostUpdateCommands.RemoveComponent<PauseGame>(entityInQueryIndex, e);
                // complete event
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SelectedActionUpdated(255));
                // Update to Paused Profile
                PostUpdateCommands.AddComponent(entityInQueryIndex, cameraLink.camera, new UpdatePostProcessing(PostProcessingProfileType.Paused));
                // Add paused state for player character
                PostUpdateCommands.AddComponent(entityInQueryIndex, controllerLink.controller, new SetControllerMapping(ControllerMapping.RealmUI));
                PostUpdateCommands.AddComponent<DisableRaycaster>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<DisableFirstPersonCamera>(entityInQueryIndex, cameraLink.camera);
                // Removes Crosshair
                for (int i = 0; i < uiLink.uis.Length; i++)
                {
                    var ui = uiLink.uis[i];
                    // replace with tag - game only UI?
                    if (HasComponent<GameOnlyUI>(ui))
                    {
                        UICoreSystem.RemoveUI(PostUpdateCommands, entityInQueryIndex, removeCharacterUIPrefab, e, ui, removeDelay, elapsedTime);
                    }
                    else if (HasComponent<GameAndPauseUI>(ui))
                    {
                        PostUpdateCommands.AddComponent<EnablePanelRaycast>(entityInQueryIndex, ui);
                    }
                }
                // Spawn Game UI
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OpenRealmUI(gameUILink.index));
                // spawn Realm UIs
                if (!disableTaskbar)
                {
                    var spawnTaskbarEntity = PostUpdateCommands.CreateEntity(entityInQueryIndex);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, spawnTaskbarEntity, new CharacterLink(e));
                    PostUpdateCommands.AddComponent<SpawnTaskbar>(entityInQueryIndex, spawnTaskbarEntity);
                    // PostUpdateCommands.AddComponent(entityInQueryIndex, spawnTaskbarEntity, new DelayEvent(elapsedTime, spawnDelay));
                }
			}).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}