using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Cameras;
using Zoxel.Transforms;
using Zoxel.Input;

namespace Zoxel.Players
{
    //! This system toggles third person mode.
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class CameraToggleSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var thirdPersonOffset = new float3(0, 0.4f, -1.2f);
            var elapsedTime = (float) World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<ControllerDisabled>()
                .ForEach((Entity e, int entityInQueryIndex, in Controller controller, in CameraLink cameraLink) =>
            {
                if (controller.keyboard.f1Key.wasPressedThisFrame == 1)
                {
                    // UnityEngine.Debug.LogError("Toggling Camera.");
                    if (HasComponent<LocalPosition>(cameraLink.camera))
                    {
                        PostUpdateCommands.AddComponent<ToggleThirdPersonCamera>(entityInQueryIndex, cameraLink.camera);
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<ToggleThirdPersonCamera>()
                .ForEach((Entity e, int entityInQueryIndex, ref LocalPosition localPosition, in FirstPersonCameraOffset firstPersonCameraOffset) =>
            {
                PostUpdateCommands.RemoveComponent<ToggleThirdPersonCamera>(entityInQueryIndex, e);
                if (localPosition.position.x == firstPersonCameraOffset.localPosition.x
                    && localPosition.position.y == firstPersonCameraOffset.localPosition.y
                    && localPosition.position.z == firstPersonCameraOffset.localPosition.z)
                {
                    localPosition.position = thirdPersonOffset;
                }
                else
                {
                    localPosition.position = firstPersonCameraOffset.localPosition;
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}