using Unity.Entities;

namespace Zoxel.Players
{
    //! Attached to start screen to handle new player connecting.
    public struct DestroyOnConnectedPlayer : IComponentData { }
}