using Unity.Entities;

namespace Zoxel.Players
{
    //! A tag for player links entity.
    public struct PlayerHome : IComponentData { }
}