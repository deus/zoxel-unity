using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Players
{
    //! Has links to all the player entities.
    public struct PlayerLinks : IComponentData
    {
        public BlitableArray<Entity> players;

        public void DisposeFinal()
        {
            players.DisposeFinal();
        }

        public void Dispose()
        {
            players.Dispose();
        }

        //! Adds a player from the list.
        public void Add(Entity player)
        {
            var players2 = new BlitableArray<Entity>(players.Length + 1, Allocator.Persistent, players);
            players2[players.Length] = player;
            Dispose();
            players = players2;
        }

        //! Removes a player from the list.
        public void Remove(Entity player)
        {
            var newPlayers = new BlitableArray<Entity>(players.Length - 1, Allocator.Persistent);
            var isPassRemovingPlayer = false;
            for (byte i = 0; i < players.Length; i++)
            {
                if (player == players[i])
                {
                    isPassRemovingPlayer = true;
                    // UnityEngine.Debug.LogError("Removing player: " + i);
                }
                else
                {
                    if (isPassRemovingPlayer)
                    {
                        newPlayers[i - 1] = players[i];
                    }
                    else
                    {
                        newPlayers[i] = players[i];
                    }
                }
            }
            Dispose();
            players = newPlayers;
        }
    }
}