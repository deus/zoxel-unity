using Unity.Entities;

namespace Zoxel.Players
{
    //! Unpauses the game from a character.
    public struct UnPauseGame : IComponentData { }
}