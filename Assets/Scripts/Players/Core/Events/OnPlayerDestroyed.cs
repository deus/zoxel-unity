
using Unity.Entities;

namespace Zoxel.Players
{
    public struct OnPlayerDestroyed : IComponentData
    {
        public Entity player;

        public OnPlayerDestroyed(Entity player)
        {
            this.player = player;
        }
    }
}