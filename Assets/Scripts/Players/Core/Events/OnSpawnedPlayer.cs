using Unity.Entities;

namespace Zoxel.Players
{
    public struct OnPlayerSpawned : IComponentData
    {
        public Entity player;

        public OnPlayerSpawned(Entity player)
        {
            this.player = player;
        }
    }
}