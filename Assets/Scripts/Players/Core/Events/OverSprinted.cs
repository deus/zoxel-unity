using Unity.Entities;

namespace Zoxel
{
    //! When a player sprints too much.
    public struct OverSprinted : IComponentData
    {
        public double elapsedTime;

        public OverSprinted(double elapsedTime)
        {
            this.elapsedTime = elapsedTime;
        }
    }
}