using Unity.Entities;

namespace Zoxel.Players
{
    //! Pauses the game from a character.
    public struct PauseGame : IComponentData { }
}