
using Unity.Entities;

namespace Zoxel.Players
{
    public struct RemovePlayer : IComponentData
    {
        public Entity player;

        public RemovePlayer(Entity player)
        {
            this.player = player;
        }
    }
}