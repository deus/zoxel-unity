using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Players
{
    //! Disposes of PlayerLinks
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class PlayerLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithAll<DestroyEntity, PlayerLinks>()
                .ForEach((int entityInQueryIndex, in PlayerLinks playerLinks) =>
			{
                for (int i = 0; i < playerLinks.players.Length; i++)
                {
                    var playerEntity = playerLinks.players[i];
                    if (HasComponent<Player>(playerEntity))
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, playerEntity);
                    }
                }
                playerLinks.DisposeFinal();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}