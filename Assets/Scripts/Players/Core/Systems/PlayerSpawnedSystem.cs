using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Players
{
    //! Adds player to player home after spawned.
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class PlayerSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities.ForEach((ref PlayerLinks playerLinks, in OnPlayerSpawned onPlayerSpawned) =>
            {
                playerLinks.Add(onPlayerSpawned.player);
            }).ScheduleParallel();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<OnPlayerSpawned>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<OnPlayerSpawned>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<OnPlayerSpawned>()
                .ForEach((int entityInQueryIndex, in PlayerLinks playerLinks, in OnPlayerSpawned onPlayerSpawned) =>
            {
                if (playerLinks.players.Length == 1)
                {
                    PostUpdateCommands.AddComponent<PlayerOne>(entityInQueryIndex, onPlayerSpawned.player);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}