﻿using Unity.Entities;
using Zoxel.Input;

namespace Zoxel.Players
{
    //! Handles player thinggs.
    /**
    *   Runs before movement systems so movement is slightly more responsive.
    */
    [UpdateAfter(typeof(InputSystemGroup))]
    public partial class PlayerSystemGroup : ComponentSystemGroup
    { 
        public static Entity playerHome;
        
        public static void SpawnPlayers(EntityManager EntityManager)
        {
            var playersArchetype = EntityManager.CreateArchetype(
                typeof(PlayerHome),
                typeof(PlayerLinks),
                typeof(RealmLink));
            PlayerSystemGroup.playerHome = EntityManager.CreateEntity(playersArchetype);
            #if UNITY_EDITOR
            EntityManager.AddComponentData(PlayerSystemGroup.playerHome, new EditorName("Player Home"));
            #endif
        }
    }
}