using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Movement;
using Zoxel.Stats;
using Zoxel.Stats.Authoring;

namespace Zoxel.Players
{
    //! Sprinting takes energy!
    /**
    *   \todo Move into Skill SyStemS, like SkillTriggerSystem.
    */
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class SprintResourceUseSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userStatsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.ReadOnly<StateStat>(),
                ComponentType.ReadWrite<StatValue>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate<StatSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var statSettings = GetSingleton<StatSettings>();
            if (statSettings.isInfiniteRun)
            {
                return;
            }
            var sprintResourceUse = 0.5f;
            var deltaTime = (float) World.Time.DeltaTime;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var statValues = GetComponentLookup<StatValue>(false);
            userStatEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .WithAll<Sprinting>()
                .ForEach((Entity e, int entityInQueryIndex, in UserStatLinks userStatLinks) =>
            {
                if (deltaTime == 0)
                {
                    return;
                }
                if (!HasComponent<Regening>(e))
                {
                    PostUpdateCommands.AddComponent<StartRegening>(entityInQueryIndex, e);
                }
                var energyUserStatEntity = new Entity();
                var statStateCount = 0;
                for (int i = 0; i < userStatLinks.stats.Length; i++)
                {
                    var userStatEntity = userStatLinks.stats[i];
                    if (HasComponent<StateStat>(userStatEntity))
                    {
                        if (statStateCount == 1)
                        {
                            energyUserStatEntity = userStatEntity;
                            break;
                        }
                        statStateCount++;
                    }
                }
                if (energyUserStatEntity.Index == 0)
                {
                    PostUpdateCommands.RemoveComponent<Sprinting>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OverSprinted(elapsedTime));
                    return;
                }
                var energyStat = statValues[energyUserStatEntity];
                energyStat.value -= sprintResourceUse * deltaTime;
                if (energyStat.value < 0)
                {
                    energyStat.value = 0;
                    //! \todo Visual and Audio feedback for over sprinting exhaustion effect.
                    PostUpdateCommands.RemoveComponent<Sprinting>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OverSprinted(elapsedTime));
                }
                statValues[energyUserStatEntity] = energyStat;
            })  .WithNativeDisableContainerSafetyRestriction(statValues)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}