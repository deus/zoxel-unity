﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Actions;
using Zoxel.Input;

namespace Zoxel.Players
{
    //! Adds action events onto character when input is pressed.
    /**
    *   Adds action events onto character when input is pressed.
    *   Player can activate or switch UserSkillLinks using the Controller component.
    */
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class PlayerActivateActionSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllerQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllerQuery = GetEntityQuery(
                ComponentType.ReadOnly<Controller>(),
                ComponentType.Exclude<DestroyEntity>());
            controllerQuery.SetChangedVersionFilter(typeof(Controller));
			RequireForUpdate(processQuery);
			RequireForUpdate(controllerQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = controllerQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var controllers = GetComponentLookup<Controller>(true);
            controllerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<ActivatedAction, ActivateWorldAction>()
                .WithAll<UserActionLinks>()
                .ForEach((Entity e, int entityInQueryIndex, in ControllerLink controllerLink) =>
            {
                if (HasComponent<ControllerDisabled>(controllerLink.controller) || !controllers.HasComponent(controllerLink.controller))
                {
                    return;
                }
                var controller = controllers[controllerLink.controller];
                if (controller.mapping == ControllerMapping.InGame)
                {
                    if (controller.gamepad.buttonRT.wasPressedThisFrame == 1)
                    {
                        if (!HasComponent<ActivateAction>(e))
                        {
                            PostUpdateCommands.AddComponent<ActivateAction>(entityInQueryIndex, e);
                        }
                    }
                    else if (controller.gamepad.buttonRT.isPressed == 1)
                    {
                        if (!HasComponent<ActivateAction>(e))
                        {
                            PostUpdateCommands.AddComponent<ActivateAction>(entityInQueryIndex, e);
                        }
                    }
                    else if (controller.gamepad.buttonLT.wasPressedThisFrame == 1)
                    {
                        PostUpdateCommands.AddComponent<ActivateWorldAction>(entityInQueryIndex, e);
                    }
                }
                if (controller.mapping == ControllerMapping.Dialogue)
                {
                    if (controller.gamepad.buttonB.wasPressedThisFrame == 1 || controller.keyboard.escapeKey.isPressed == 1)
                    {
                        PostUpdateCommands.AddComponent<CancelWorldAction>(entityInQueryIndex, e);
                    }
                }
            })  .WithReadOnly(controllers)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}