using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Players
{
    //! Sprinting takes energy!
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class OverSprintEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var overSprintedCooldown = 1.5;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, in OverSprinted overSprinted) =>
            {
                if (elapsedTime - overSprinted.elapsedTime >= overSprintedCooldown)
                {
                    PostUpdateCommands.RemoveComponent<OverSprinted>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}