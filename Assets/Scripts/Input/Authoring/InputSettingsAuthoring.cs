using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Zoxel.Input.Authoring
{
    //! Settings data for our ai namespace.
    // [GenerateAuthoringComponent]
    public struct InputSettings : IComponentData
    {
        public bool isTestVirtualJoysticks;
        public bool isDisableEditorMouseLock;
    }

    public class InputSettingsAuthoring : MonoBehaviour
    {
        public bool isTestVirtualJoysticks;
        public bool isDisableEditorMouseLock;
    }

    public class InputSettingsAuthoringBaker : Baker<InputSettingsAuthoring>
    {
        public override void Bake(InputSettingsAuthoring authoring)
        {
            AddComponent(new InputSettings
            {
                isTestVirtualJoysticks = authoring.isTestVirtualJoysticks,
                isDisableEditorMouseLock = authoring.isDisableEditorMouseLock
            });       
        }
    }
}

// [Header("Settings")]

/*public void DrawUI()
{
    GUILayout.Label(" [Input] ");
}

public void DrawDebug()
{
    GUILayout.Label("Input");
    // disableNighttime = GUILayout.Toggle(disableNighttime, "Disable Nighttime");
}*/