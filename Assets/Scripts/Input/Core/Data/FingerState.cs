namespace Zoxel.Input
{
    //! A mapping to UnityEngine.InputSystem.TouchPhase.
    /**
    *   Can be found at: https://docs.unity3d.com/Packages/com.unity.inputsystem@1.0/api/UnityEngine.InputSystem.TouchPhase.html
    */
    public class FingerState
    {
        public const byte None = 0;
        public const byte Begin = 1;
        public const byte Moved = 2;
        public const byte End = 3;
        public const byte Canceled = 4;
        public const byte Stationary = 5;
    }
}