namespace Zoxel.Input
{
    //! Type of input being used.
    public static class ControllerMapping
    {
		public const byte None = 0;
		public const byte Menu = 1;
		public const byte InGame = 2;
		public const byte RealmUI = 3;
		public const byte Dialogue = 4;
		public const byte Input = 5;
		public const byte ChestUI = 6;
    }
}