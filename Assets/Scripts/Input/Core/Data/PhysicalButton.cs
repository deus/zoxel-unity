using UnityEngine.InputSystem.Controls;

namespace Zoxel.Input
{
    //! A button data contains state information.
    public struct PhysicalButton
    {
        public byte wasPressedThisFrame;
        public byte isPressed;
        public byte wasReleasedThisFrame;

        public void Reset()
        {
            isPressed = 0;
            wasPressedThisFrame = 0;
        }

        public void Update(KeyControl keyControl)
        {
            if (keyControl.isPressed)
            {
                if (this.isPressed == 0)
                {
                    this.isPressed = 1;
                    this.wasPressedThisFrame = 1;
                }
                else
                {
                    this.wasPressedThisFrame = 0;
                }
            }
            else
            {
                if (this.isPressed == 1)
                {
                    this.isPressed = 0;
                    this.wasPressedThisFrame = 0;
                }
            }
            if (keyControl.wasReleasedThisFrame)
            {
                this.wasReleasedThisFrame = 1;
            }
            else
            {
                this.wasReleasedThisFrame = 0;
            }
        }

        public void Update(ButtonControl keyControl)
        {
            if (keyControl.isPressed)
            {
                if (this.isPressed == 0)
                {
                    this.isPressed = 1;
                    this.wasPressedThisFrame = 1;
                }
                else
                {
                    this.wasPressedThisFrame = 0;
                }
            }
            else
            {
                if (this.isPressed == 1)
                {
                    this.isPressed = 0;
                    this.wasPressedThisFrame = 0;
                }
            }
            if (keyControl.wasReleasedThisFrame)
            {
                this.wasReleasedThisFrame = 1;
            }
            else
            {
                this.wasReleasedThisFrame = 0;
            }
        }
    }
}