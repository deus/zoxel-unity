using Unity.Entities;

namespace Zoxel.Input
{
    //! Links to a PlayerHome Entity.
    public struct DeviceHomeLink : IComponentData
    {
        public Entity deviceHome;

        public DeviceHomeLink(Entity deviceHome)
        {
            this.deviceHome = deviceHome;
        }
    }
}