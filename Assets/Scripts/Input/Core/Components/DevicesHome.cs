using Unity.Entities;

namespace Zoxel.Input
{
    //! A tag for Input links entity.
    public struct DevicesHome : IComponentData { }
}