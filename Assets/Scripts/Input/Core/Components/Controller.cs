﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Input
{
    //! Contains input gamepad for a player.
    /**
    *   \todo Replace with ControllerMapping and set based on key bindings per action.
    */
	public struct Controller : IComponentData
    {
        //! The mapping for the player input
        public byte mapping;
        //! The Cursor Lock State
        public byte lockState;
        // why so many of these?
        public byte wasInputLastFrame;
        public double lastPausedTime; 
        //! \todo Remove Device copies and pass in
        //! Merged Device Input Data.
            public Gamepad gamepad;
            public Keyboard keyboard;
            public Mouse mouse;

        public Controller(byte controllerMapping)
        {
            this.mapping = controllerMapping;
            this.lockState = 0;
            this.lastPausedTime = 0;
            this.wasInputLastFrame = 0;
            this.gamepad = new Gamepad();
            this.keyboard = new Keyboard();
            this.mouse = new Mouse();
        }

        public void Reset()
        {
            this.gamepad.Reset();
        }

        public bool NoInput()
        {
            return keyboard.NoInput() && gamepad.NoInput();
        }

        public void SetMapping(byte newMapping)
        {
            if (mapping != newMapping)
            {
                mapping = newMapping;
                gamepad.Reset();
                keyboard.Reset();
                mouse.Reset();
            }
        }
    }
}