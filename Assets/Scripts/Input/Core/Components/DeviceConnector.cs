using Unity.Entities;

namespace Zoxel.Input
{
    //! Added to home screen, allows devices to connect/disconnect.
    public struct DeviceConnector : IComponentData { }
}