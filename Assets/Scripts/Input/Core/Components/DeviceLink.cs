using Unity.Entities;

namespace Zoxel.Input
{
    //! Links to a Device Entity.
    public struct DeviceLink : IComponentData
    {
        public Entity device;

        public DeviceLink(Entity device)
        {
            this.device = device;
        }
    }
}