using Unity.Entities;
using Unity.Collections;
// remember to dispose of these

namespace Zoxel.Input
{
    //! A player can have multiple connected devices.
    public struct ConnectedDevices : IComponentData
    {
        public BlitableArray<Entity> devices;

        public void DisposeFinal()
        {
            devices.DisposeFinal();
        }

        public void Dispose()
        {
            devices.Dispose();
        }

        public bool Has(Entity e)
        {
            for (int i = 0; i < devices.Length; i++)
            {
                if (devices[i] == e)
                {
                    return true;
                }
            }
            return false;
        }

        public bool Remove(in ZoxID disconnectedID, in ComponentLookup<ZoxID> zoxIDs)
        {
            for (int i = 0; i < devices.Length; i++)
            {
                if (!zoxIDs.HasComponent(devices[i]))
                {
                    continue;
                }
                var zoxID = zoxIDs[devices[i]];
                if (zoxID.id == disconnectedID.id)
                {
                    Remove(devices[i]);
                    return true;
                }
            }
            return false;
        }

        //! Adds a device from the list.
        public void Add(Entity device)
        {
            var devices2 = new BlitableArray<Entity>(devices.Length + 1, Allocator.Persistent, devices);
            devices2[devices.Length] = device;
            Dispose();
            devices = devices2;
        }

        //! Removes a device from the list.
        public void Remove(Entity device)
        {
            var newPlayers = new BlitableArray<Entity>(devices.Length - 1, Allocator.Persistent);
            var isPassRemovingPlayer = false;
            for (int i = 0; i < devices.Length; i++)
            {
                if (device == devices[i])
                {
                    isPassRemovingPlayer = true;
                }
                else
                {
                    if (isPassRemovingPlayer)
                    {
                        newPlayers[i - 1] = devices[i];
                    }
                    else
                    {
                        newPlayers[i] = devices[i];
                    }
                }
            }
            Dispose();
            devices = newPlayers;
        }
    }
}