using Unity.Entities;

namespace Zoxel.Input
{
	//! An input device.
	public struct DeviceTypeData : IComponentData
    {
        public byte type;

        public DeviceTypeData(byte type)
        {
            this.type = type;
        }
    }
}