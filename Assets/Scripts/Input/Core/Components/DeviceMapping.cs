using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Input
{
    //! Contains game mapping for player data.
	public struct DeviceMapping : IComponentData
    {
        //! The mapping for the player input.
        public byte mapping;

        public DeviceMapping(byte mapping)
        {
            this.mapping = mapping;
        }
    }
}