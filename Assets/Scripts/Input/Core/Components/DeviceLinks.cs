using Unity.Entities;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
// remember to dispose of these

namespace Zoxel.Input
{
    //! Has links to all the deviceEntity entities.
    public struct DeviceLinks : IComponentData
    {
        public UnsafeParallelHashMap<int, Entity> devices;
		public int removedCount;

        public int Length
        {
            get
            { 
				if (!devices.IsCreated)
				{
					return 0;
				}
                return devices.Count();
            }
        }

		public void Dispose()
		{
			if (devices.IsCreated)
			{
            	devices.Dispose();
				removedCount = 0;
			}
		}

        //! Adds a deviceEntity from the list.
		public void Add(int id)
		{
			if (!devices.IsCreated)
			{
            	this.devices = new UnsafeParallelHashMap<int, Entity>(1, Allocator.Persistent);
				devices[id] = new Entity();
				return;
			}
			Add2(id);
		}

		public void Add2(int id)
		{
			if (Has(id))
			{
				// UnityEngine.Debug.LogError("Key already existed: " + id);
				devices[id] = new Entity();
				return;
			}
			removedCount -= 1;
			if (removedCount < 0)
			{
				this.devices.Capacity++;
				removedCount++;
			}
			devices[id] = new Entity();
		}

		public bool Remove(int id)
		{
			//UnityEngine.Debug.LogError("Before removed: " + this.devices.Capacity);
			var success = devices.Remove(id);
			if (success)
			{
				removedCount++;
			}
			//UnityEngine.Debug.LogError("Decreased Minivoxes: " + this.devices.Capacity);
			return success;
		}

		public bool Has(int id)
		{
			if (!devices.IsCreated)
			{
				return false;
			}
			return devices.ContainsKey(id);
		}

		public Entity Get(int id)
		{
			Entity e;
			if (devices.IsCreated && devices.TryGetValue(id, out e))
			{
				return e;
			}
			return new Entity();
		}
    }
}