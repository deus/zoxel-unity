using Unity.Entities;

namespace Zoxel.Input
{
    //! Holds input data for a Keyboard device.
    /**
    *   \todo Implement Key Bindings.
    *   \todo Finish adding all keys on the keyboard.
    */
    public struct Keyboard : IComponentData
    {
        // Special Keys
        public PhysicalButton spaceKey;
        public PhysicalButton escapeKey;
        public PhysicalButton backspaceKey;
        public PhysicalButton f1Key;
        public PhysicalButton f2Key;
        public PhysicalButton f3Key;
        public PhysicalButton f4Key;
        public PhysicalButton minusKey;
        public PhysicalButton enterKey;
        public PhysicalButton leftControlKey;
        public PhysicalButton leftShiftKey;
        public PhysicalButton leftAltKey;
        public PhysicalButton rightAltKey;
        // Normal Keys
        public PhysicalButton aKey;
        public PhysicalButton bKey;
        public PhysicalButton cKey;
        public PhysicalButton dKey;
        public PhysicalButton eKey;
        public PhysicalButton fKey;
        public PhysicalButton gKey;
        public PhysicalButton hKey;
        public PhysicalButton iKey;
        public PhysicalButton jKey;
        public PhysicalButton kKey;
        public PhysicalButton lKey;
        public PhysicalButton mKey;
        public PhysicalButton nKey;
        public PhysicalButton oKey;
        public PhysicalButton pKey;
        public PhysicalButton qKey;
        public PhysicalButton rKey;
        public PhysicalButton sKey;
        public PhysicalButton tKey;
        public PhysicalButton uKey;
        public PhysicalButton vKey;
        public PhysicalButton wKey;
        public PhysicalButton xKey;
        public PhysicalButton yKey;
        public PhysicalButton zKey;
        // Digit Keys
        public PhysicalButton digit0Key;
        public PhysicalButton digit1Key;
        public PhysicalButton digit2Key;
        public PhysicalButton digit3Key;
        public PhysicalButton digit4Key;
        public PhysicalButton digit5Key;
        public PhysicalButton digit6Key;
        public PhysicalButton digit7Key;
        public PhysicalButton digit8Key;
        public PhysicalButton digit9Key;

        public bool IsConnecting()
        {
            return enterKey.wasPressedThisFrame == 1 || spaceKey.wasPressedThisFrame == 1;
            // return !NoInput();
        }

        public bool IsDisconnecting()
        {
            return escapeKey.wasPressedThisFrame == 1;
        }

        public bool NoInput()
        {
            return digit0Key.wasPressedThisFrame == 0 && digit1Key.wasPressedThisFrame == 0 && digit2Key.wasPressedThisFrame == 0  && digit3Key.wasPressedThisFrame == 0 && digit4Key.wasPressedThisFrame == 0 &&
                digit5Key.wasPressedThisFrame == 0 && digit6Key.wasPressedThisFrame == 0 && digit7Key.wasPressedThisFrame == 0 && digit8Key.wasPressedThisFrame == 0 && digit9Key.wasPressedThisFrame == 0 &&
                aKey.wasPressedThisFrame == 0 && bKey.wasPressedThisFrame == 0 && cKey.wasPressedThisFrame == 0 && dKey.wasPressedThisFrame == 0 && eKey.wasPressedThisFrame == 0 && fKey.wasPressedThisFrame == 0
                && gKey.wasPressedThisFrame == 0 && hKey.wasPressedThisFrame == 0 && 
                iKey.wasPressedThisFrame == 0 && jKey.wasPressedThisFrame == 0 && kKey.wasPressedThisFrame == 0 && lKey.wasPressedThisFrame == 0 && mKey.wasPressedThisFrame == 0 && nKey.wasPressedThisFrame == 0
                && oKey.wasPressedThisFrame == 0 && pKey.wasPressedThisFrame == 0 && qKey.wasPressedThisFrame == 0 && 
                rKey.wasPressedThisFrame == 0 && sKey.wasPressedThisFrame == 0 && tKey.wasPressedThisFrame == 0 && uKey.wasPressedThisFrame == 0 && vKey.wasPressedThisFrame == 0 && wKey.wasPressedThisFrame == 0
                && xKey.wasPressedThisFrame == 0 && yKey.wasPressedThisFrame == 0 && zKey.wasPressedThisFrame == 0 && 
                spaceKey.wasPressedThisFrame == 0 && backspaceKey.wasPressedThisFrame == 0 && enterKey.wasPressedThisFrame == 0;
        }

        public void Update(in UnityEngine.InputSystem.Keyboard keyboard)
        {
            //UnityEngine.Debug.LogError("Extracting Keyboard " + keyboard.deviceId);
            // special keys
            backspaceKey.Update(keyboard.backspaceKey);
            enterKey.Update(keyboard.enterKey);
            leftShiftKey.Update(keyboard.leftShiftKey);
            leftControlKey.Update(keyboard.leftCtrlKey);
            leftAltKey.Update(keyboard.leftAltKey);
            rightAltKey.Update(keyboard.rightAltKey);
            spaceKey.Update(keyboard.spaceKey);
            minusKey.Update(keyboard.minusKey);
            escapeKey.Update(keyboard.escapeKey);
            f1Key.Update(keyboard.f1Key);
            f2Key.Update(keyboard.f2Key);
            f3Key.Update(keyboard.f3Key);
            f4Key.Update(keyboard.f4Key);
            // normal keys
            aKey.Update(keyboard.aKey);
            bKey.Update(keyboard.bKey);
            cKey.Update(keyboard.cKey);
            dKey.Update(keyboard.dKey);
            eKey.Update(keyboard.eKey);
            fKey.Update(keyboard.fKey);
            gKey.Update(keyboard.gKey);
            hKey.Update(keyboard.hKey);
            iKey.Update(keyboard.iKey);
            jKey.Update(keyboard.jKey);
            kKey.Update(keyboard.kKey);
            lKey.Update(keyboard.lKey);
            mKey.Update(keyboard.mKey);
            nKey.Update(keyboard.nKey);
            oKey.Update(keyboard.oKey);
            pKey.Update(keyboard.pKey);
            qKey.Update(keyboard.qKey);
            rKey.Update(keyboard.rKey);
            sKey.Update(keyboard.sKey);
            tKey.Update(keyboard.tKey);
            uKey.Update(keyboard.uKey);
            vKey.Update(keyboard.vKey);
            wKey.Update(keyboard.wKey);
            xKey.Update(keyboard.xKey);
            yKey.Update(keyboard.yKey);
            zKey.Update(keyboard.zKey);
            // Digital keys
            digit0Key.Update(keyboard.digit0Key);
            digit1Key.Update(keyboard.digit1Key);
            digit2Key.Update(keyboard.digit2Key);
            digit3Key.Update(keyboard.digit3Key);
            digit4Key.Update(keyboard.digit4Key);
            digit5Key.Update(keyboard.digit5Key);
            digit6Key.Update(keyboard.digit6Key);
            digit7Key.Update(keyboard.digit7Key);
            digit8Key.Update(keyboard.digit8Key);
            digit9Key.Update(keyboard.digit9Key);
        }

        public void Reset()
        {
            escapeKey.Reset();
            f1Key.Reset();
            f2Key.Reset();
            f3Key.Reset();
            f4Key.Reset();
            spaceKey.Reset();
            backspaceKey.Reset();
            minusKey.Reset();
            enterKey.Reset();
            leftControlKey.Reset();
            leftAltKey.Reset();
            rightAltKey.Reset();
            leftShiftKey.Reset();
            aKey.Reset();
            bKey.Reset();
            cKey.Reset();
            dKey.Reset();
            eKey.Reset();
            fKey.Reset();
            gKey.Reset();
            hKey.Reset();
            iKey.Reset();
            jKey.Reset();
            kKey.Reset();
            lKey.Reset();
            mKey.Reset();
            nKey.Reset();
            oKey.Reset();
            pKey.Reset();
            qKey.Reset();
            rKey.Reset();
            sKey.Reset();
            tKey.Reset();
            uKey.Reset();
            vKey.Reset();
            wKey.Reset();
            xKey.Reset();
            yKey.Reset();
            zKey.Reset();
            digit0Key.Reset();
            digit1Key.Reset();
            digit2Key.Reset();
            digit3Key.Reset();
            digit4Key.Reset();
            digit5Key.Reset();
            digit6Key.Reset();
            digit7Key.Reset();
            digit8Key.Reset();
            digit9Key.Reset();
            /*eKeyHeld.Reset();
            qKeyHeld.Reset();
            lKeyDown.Reset();
            sKeyDown.Reset();
            eKey.Reset();
            qKeyDown.Reset();
            wKeyDown.Reset();*/
        }
    }

}