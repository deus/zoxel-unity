using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Input
{
    //! A input device for a controller.
    public struct Gamepad : IComponentData
    {
        public float2 leftStick;
        public float2 rightStick;
        public PhysicalButton startButton;
        public PhysicalButton selectButton;
        public PhysicalButton buttonA;
        public PhysicalButton buttonB;
        public PhysicalButton buttonX;
        public PhysicalButton buttonY;
        public PhysicalButton buttonLB;
        public PhysicalButton buttonRB;
        public PhysicalButton buttonLT;
        public PhysicalButton buttonRT;

        public void Reset()
        {
            leftStick.x = 0;
            leftStick.y = 0;
            rightStick.x = 0;
            rightStick.y = 0;
            startButton.Reset();
            selectButton.Reset();
            buttonA.Reset();
            buttonB.Reset();
            buttonX.Reset();
            buttonY.Reset();
            buttonLB.Reset();
            buttonRB.Reset();
            buttonLT.Reset();
            buttonRT.Reset();
        }

        public void Update(in UnityEngine.InputSystem.Gamepad gamepad)
        {
            leftStick = gamepad.leftStick.ReadValue();
            rightStick = gamepad.rightStick.ReadValue();
            startButton.Update(gamepad.startButton);
            selectButton.Update(gamepad.selectButton);
            buttonA.Update(gamepad.aButton);
            buttonB.Update(gamepad.bButton);
            buttonX.Update(gamepad.xButton);
            buttonY.Update(gamepad.yButton);
            buttonRB.Update(gamepad.rightShoulder);
            buttonLB.Update(gamepad.leftShoulder);
            buttonRT.Update(gamepad.rightTrigger);
            buttonLT.Update(gamepad.leftTrigger);
        }

        public bool NoInput()
        {
            return leftStick.x == 0 && leftStick.y == 0 && rightStick.x == 0 && rightStick.y == 0 &&
                buttonA.wasPressedThisFrame == 0 && buttonB.wasPressedThisFrame == 0 && buttonX.wasPressedThisFrame == 0 && buttonY.wasPressedThisFrame == 0 &&
                buttonLT.wasPressedThisFrame == 0 && buttonRT.wasPressedThisFrame == 0 && buttonLB.wasPressedThisFrame == 0 && buttonRB.wasPressedThisFrame == 0;
        }

        public bool IsConnecting()
        {
            // return !NoInput();
            return startButton.wasPressedThisFrame == 1 || selectButton.wasPressedThisFrame == 1 || buttonA.wasPressedThisFrame == 1;
        }

        public bool IsDisconnecting()
        {
            return buttonB.wasPressedThisFrame == 1;
        }
    }
}