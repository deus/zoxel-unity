using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Input
{
    //! Holds input data for a Touchpad device.
    /**
    *   \todo When tap, use radius to select closest ui.
    *   \todo Fix: Sometimes gets stuck while turning and moving (turning gets stuck when finger released)
    */
    public struct Touchpad : IComponentData
    {
        public byte touchesCount;
        public BlitableArray<Finger> fingers;
        public float2 leftStick;
        public float2 rightStick;

        public void Initialize(byte fingersCount = 10)
        {
            fingers = new BlitableArray<Finger>(fingersCount, Allocator.Persistent);
        }

        public void Dispose()
        {
            fingers.Dispose();
        }

        public void Reset()
        {
            touchesCount = 0;
            for (byte i = 0; i < fingers.Length; i++)
            {
                var finger = fingers[i];
                finger.Reset();
                fingers[i] = finger;
            }
        }

        public bool IsConnecting()
        {
            return !NoInput();
        }

        public bool NoInput()
        {
            return !IsInput();
        }

        public bool IsInput()
        {
            return touchesCount > 0 && fingers[0].state == FingerState.Moved;
        }

        public bool IsDisconnecting()
        {
            //\todo Detect swipe down for exiting.
            return false;
        }

        public void Update(in UnityEngine.InputSystem.Touchscreen touchscreen, double elapsedTime)
        {
            if (touchscreen == null)
            {
                return;
            }
            // touchesCount = (byte) touchscreen.touches.Count;
            touchesCount = (byte) UnityEngine.InputSystem.EnhancedTouch.Touch.activeTouches.Count;
            if (touchesCount == 0)
            {
                leftStick = new float2();
                rightStick = new float2();
            }
            else
            {
                for (byte i = 0; i < fingers.Length; i++)
                {
                    var unityTouch = touchscreen.touches[i];
                    var finger = fingers[i];
                    finger.Update(in unityTouch, elapsedTime);
                    fingers[i] = finger;
                }
            }
        }

        public Finger GetFinger(int targetID, out bool success)
        {
            for (byte i = 0; i < fingers.Length; i++)
            {
                var finger = fingers[i];
                if (finger.id == targetID)
                {
                    success = true;
                    return finger;
                }
            }
            success = false;
            return new Finger();
        }
    }
}
            // if (touchesCount >= 2)
            // {
            // }
            /*if (touchesCount == 0)
            {
                var newState = (byte) touchscreen.touches[0].phase.ReadValue();
                if (newState == FingerState.Begin && newState == FingerState.Begin)
                {
                    touchesCount++;
                    UnityEngine.Debug.LogError("Touches count failed with began.");
                }
            }*/