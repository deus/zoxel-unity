using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Input
{
    //! Data input for one finger.
    /**
    *   Mapped from UnityEngine.InputSystem.Controls.TouchControl.
    *
    *   https://docs.unity3d.com/Packages/com.unity.inputsystem@1.0/api/UnityEngine.InputSystem.Controls.TouchControl.html
    */
    public struct Finger : IComponentData
    {
        public int id;
        public byte state;
        public byte previousState;
        public float2 startPosition;
        public float2 position;
        public float2 delta;
        public double lastTapped;
        public double lastLastTapped;
        public bool active;

        public void Reset()
        {
            active = false;
            id = 0;
            state = FingerState.None;
            startPosition.x = 0;
            startPosition.y = 0;
            position.x = 0;
            position.y = 0;
            delta.x = 0;
            delta.y = 0;
            // lastTapped = 0;
            // lastLastTapped = 0;
        }

        public void Update(in UnityEngine.InputSystem.Controls.TouchControl unityTouch, double elapsedTime)
        {
            var beforeState = state;
            id = unityTouch.touchId.ReadValue();
            state = (byte) unityTouch.phase.ReadValue();
            startPosition = unityTouch.startPosition.ReadValue();
            position = unityTouch.position.ReadValue();
            delta = unityTouch.delta.ReadValue();
            if (beforeState != state)
            {
                if (beforeState != FingerState.Begin && state == FingerState.Moved)
                {
                    state = FingerState.Begin;
                }
                if (beforeState == FingerState.Moved && state != FingerState.End)
                {
                    state = FingerState.End;
                }
            }
            if (state == FingerState.Begin)
            {
                lastLastTapped = lastTapped;
                lastTapped = elapsedTime;
            }
            active = UnityEngine.InputSystem.InputExtensions.IsActive(unityTouch.phase.ReadValue());
            previousState = beforeState;
        }
    }
}