using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Input
{
    //! Contains Mouse mapped from Unity's new Input System.
    public struct Mouse : IComponentData
    {
        public float2 position;
        public float2 delta;
        public float2 scroll;
        public float3 rayOrigin;
        public float3 rayDirection;
        public PhysicalButton leftButton;
        public PhysicalButton rightButton;
        public byte isInScreen;

        public bool IsConnecting()
        {
            return isInScreen == 1 && leftButton.wasPressedThisFrame == 1; //  || rightButton.wasPressedThisFrame == 1;
        }

        public bool IsDisconnecting()
        {
            return isInScreen == 1 && rightButton.wasPressedThisFrame == 1;
        }

        public void Reset()
        {
            position.x = 0;
            position.y = 0;
            scroll.x = 0;
            scroll.y = 0;
            rayOrigin.x = 0;
            rayOrigin.y = 0;
            rayOrigin.z = 0;
            rayDirection.x = 0;
            rayDirection.y = 0;
            rayDirection.z = 0;
            isInScreen = 0;
            leftButton.Reset();
            rightButton.Reset();
        }

        public void SetIsInScreen(int2 screenDimensions)
        {
            if (position.x >= 0 && position.x <= screenDimensions.x && position.y >= 0 && position.y <= screenDimensions.y)
            {
                isInScreen = 1;
            }
            else
            {
                isInScreen = 0;
            }
        }

        public void Update(in UnityEngine.InputSystem.Mouse mouse, int2 screenDimensions)
        {
            SetIsInScreen(screenDimensions);
            position = mouse.position.ReadValue();
            scroll = mouse.scroll.ReadValue();
            delta = mouse.delta.ReadValue();
            #if UNITY_EDITOR
            scroll.y *= -1;
            #endif
            leftButton.Update(mouse.leftButton);
            rightButton.Update(mouse.rightButton);
            #if UNITY_EDITOR
            if (!UnityEditor.EditorApplication.isPlaying)
            {
                var sceneView = UnityEditor.SceneView.lastActiveSceneView;
                // UnityEngine.Debug.LogError("Scene View Position: " + sceneView.position);
                // UnityEngine.Debug.LogError("titleContent height: " + sceneView.titleContent.image.height);
                var sceneViewCamera = sceneView.camera;
                position.x -= sceneView.position.x;
                position.y = sceneViewCamera.pixelHeight - position.y;
                position.y += sceneView.position.y;
                // comes to about 136
                position.y += sceneView.titleContent.image.height;
                position.y += 30;   // this represents the bar on top of sceneview
            }
            #endif
        }

        public float2 GetPointer(float2 screenDimensions)
        {
            return new float2(position.x / screenDimensions.x, position.y / screenDimensions.y);
        }

        public float2 GetPointer(float2 screenDimensions, Rect cameraScreen) // float2 cameraScreenRectPosition, float2 cameraScreenRectSize)
        {
            return MouseUtil.GetPointer(position, screenDimensions, cameraScreen);
        }
    }
}
            //cameraScreenRectPosition = new float2(0.5f, 0.5f);
            //cameraScreenRectSize = new float2(0.25f, 0.25f);

            //if (scaledPointer.x < 0 || scaledPointer.x > 1f || scaledPointer.y < 0 || scaledPointer.y > 1f)
                //UnityEngine.Debug.LogError("Scaled position out of bounds: " + scaledPointer);
                //return new float2(-1f, -1f);
            //scaledPointer.y %= cameraScreenRectSize.y;

            //scaledPointer.x /= screenDimensions.x;
            //scaledPointer.y /= screenDimensions.y;
            //var newScaledPointer = new float2(scaledPointer.x - cameraScreenRectPosition.x, scaledPointer.y - cameraScreenRectPosition.y);
            /*if (scaledPointer.y < 0)
            {
                // UnityEngine.Debug.LogError("Scaled position out of bounds: " + scaledPointer + " - " + cameraScreenRectPosition);
                scaledPointer.y += 1;
            }
            if (scaledPointer.x < 0)
            {
                scaledPointer.x += 1;
            }
            if (scaledPointer.x > 1)
            {
                scaledPointer.x -= 1;
            }*/
            // UnityEngine.Debug.LogError("Scaled position out of bounds: " + scaledPointer);