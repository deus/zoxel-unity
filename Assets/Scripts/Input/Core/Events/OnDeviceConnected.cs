using Unity.Entities;

namespace Zoxel.Input
{
    //! An Event tag for when a device connects.
    public struct OnDeviceConnected : IComponentData { }
}