using Unity.Entities;

namespace Zoxel.Input
{
    //! An event that signals a ui spawned for an entity.
    public struct OnDeviceSpawned : IComponentData
    {
        public Entity deviceHome;
        public int id;

        public OnDeviceSpawned(Entity deviceHome, int id)
        {
            this.deviceHome = deviceHome;
            this.id = id;
        }
    }
}