using Unity.Entities;

namespace Zoxel.Input
{
    public struct OnDeviceDisconnected : IComponentData { }
}