using Unity.Entities;

namespace Zoxel.Input
{
    //! Sets the controller mapping.
    public struct SetControllerMapping : IComponentData
    {
        public byte newMapping;
        public double timeStarted;
        public double timeDelay;

        public SetControllerMapping(byte newMapping, double timeStarted = 0, double timeDelay = 0)
        {
            this.newMapping = newMapping;
            this.timeStarted = timeStarted;
            this.timeDelay = timeDelay;
        }
    }
}