using Unity.Entities;

namespace Zoxel.Input
{
    //! A tag for when a device is being used by a Player.
    public struct DeviceConnected : IComponentData
    {
        public Entity player;

        public DeviceConnected(Entity player)
        {
            this.player = player;
        }
    }
}