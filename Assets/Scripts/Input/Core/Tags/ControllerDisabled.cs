using Unity.Entities;

namespace Zoxel.Input
{
	//! When a controller is disabled.
	public struct ControllerDisabled : IComponentData { }
}