using Unity.Entities;

namespace Zoxel.Input
{
	//! A new input device.
	public struct NewDevice : IComponentData { }
}