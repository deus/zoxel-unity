using Unity.Entities;

namespace Zoxel.Input
{
	//! An input device.
	public struct DeviceDisconnecting : IComponentData { }
}