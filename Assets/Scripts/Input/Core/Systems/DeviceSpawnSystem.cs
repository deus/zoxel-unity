using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Input
{
    //! Spawns Input entities with controllers.
    /**
    *   - Spawn System -
    *   When input device connects or disconnects, create/destroy entities for them.
    *   Create connect/disconnect Events as well.
    */
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class DeviceSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity deviceSpawnedPrefab;
        private Entity keyboardPrefab;
        private Entity mousePrefab;
        private Entity gamepadPrefab;
        private Entity touchpadPrefab;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var deviceSpawnedArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(OnDeviceSpawned),
                typeof(GenericEvent)
            );
            deviceSpawnedPrefab = EntityManager.CreateEntity(deviceSpawnedArchetype);
            var deviceArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(NewDevice),
                typeof(InitializeEntity),
                typeof(Device),
                typeof(DeviceTypeData),
                typeof(ZoxID),
                typeof(DeviceHomeLink)
            );
            keyboardPrefab = EntityManager.CreateEntity(deviceArchetype);
            EntityManager.AddComponent<Keyboard>(keyboardPrefab);
            gamepadPrefab = EntityManager.CreateEntity(deviceArchetype);
            EntityManager.AddComponent<Gamepad>(gamepadPrefab);
            touchpadPrefab = EntityManager.CreateEntity(deviceArchetype);
            EntityManager.AddComponent<Touchpad>(touchpadPrefab);
            mousePrefab = EntityManager.CreateEntity(deviceArchetype);
            EntityManager.AddComponent<Mouse>(mousePrefab);
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DelayEvent>(),
                ComponentType.ReadOnly<OnDeviceConnected>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.ReadOnly<DeviceTypeData>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var mousePrefab = this.mousePrefab;
            var keyboardPrefab = this.keyboardPrefab;
            var gamepadPrefab = this.gamepadPrefab;
            var touchpadPrefab = this.touchpadPrefab;
            var deviceSpawnedPrefab = this.deviceSpawnedPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var connectedDeviceEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var connectedDeviceIDs = GetComponentLookup<ZoxID>(true);
            var deviceTypeDatas = GetComponentLookup<DeviceTypeData>(true);
            Dependency = Entities
                .WithAll<DevicesHome, DeviceLinks>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                for (int i = 0; i < connectedDeviceEntities.Length; i++)
                {
                    var e2 = connectedDeviceEntities[i]; 
                    var deviceID = connectedDeviceIDs[e2];
                    var deviceTypeData = deviceTypeDatas[e2];
                    var prefab = keyboardPrefab;
                    if (deviceTypeData.type == DeviceType.Gamepad)
                    {
                        prefab = gamepadPrefab;
                    }
                    else if (deviceTypeData.type == DeviceType.Mouse)
                    {
                        prefab = mousePrefab;
                    }
                    else if (deviceTypeData.type == DeviceType.Touch)
                    {
                        prefab = touchpadPrefab;
                    }
                    var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, deviceID);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, deviceTypeData);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new DeviceHomeLink(e));
                    var spawnedEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, deviceSpawnedPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnedEntity, new OnDeviceSpawned(e, deviceID.id));
                    if (deviceTypeData.type == DeviceType.Touch)
                    {
                        var touchpad = new Touchpad();
                        touchpad.Initialize();
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e3, touchpad);
                    }
                }
            })  .WithReadOnly(connectedDeviceEntities)
                .WithReadOnly(connectedDeviceIDs)
                .WithReadOnly(deviceTypeDatas)
                .WithDisposeOnCompletion(connectedDeviceEntities)
                .WithDisposeOnCompletion(connectedDeviceIDs)
                .WithDisposeOnCompletion(deviceTypeDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}