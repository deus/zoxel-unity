using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Input
{
    //! Spawns Input entities with controllers.
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class DeviceRemoveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DelayEvent>(),
                ComponentType.ReadOnly<OnDeviceDisconnected>(),
                ComponentType.ReadOnly<ZoxID>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var disconnectedDeviceEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var deviceIDs = GetComponentLookup<ZoxID>(true);
            Dependency = Entities
                .WithAll<DevicesHome>()
                .ForEach((int entityInQueryIndex, ref DeviceLinks deviceLinks) =>
            {
                for (int i = 0; i < disconnectedDeviceEntities.Length; i++)
                {
                    var e2 = disconnectedDeviceEntities[i]; 
                    var deviceID = deviceIDs[e2];
                    var deviceEntity = deviceLinks.Get(deviceID.id);
                    if (deviceEntity.Index > 0)
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, deviceEntity);
                        deviceLinks.Remove(deviceID.id);
                    }
                }
            })  .WithReadOnly(disconnectedDeviceEntities)
                .WithReadOnly(deviceIDs)
                .WithDisposeOnCompletion(disconnectedDeviceEntities)
				.WithDisposeOnCompletion(deviceIDs)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}