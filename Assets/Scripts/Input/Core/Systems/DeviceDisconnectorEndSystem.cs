using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Input
{
    //! Removes DeviceDisconnecting events from entities.
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class DeviceDisconnectorEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<DeviceDisconnecting>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<DeviceDisconnecting>(processQuery);
        }
    }
}