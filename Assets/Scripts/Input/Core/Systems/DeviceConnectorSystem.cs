using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Input
{
    //! Spawns Input entities with controllers.
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class DeviceConnectorSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery devicesConnectingQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            devicesConnectingQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<DeviceConnecting>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            if (!devicesConnectingQuery.IsEmpty)
            {
                PostUpdateCommands2.RemoveComponent<DeviceConnecting>(devicesConnectingQuery);
            }
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            // Keyboard
            Dependency = Entities
                .WithNone<InitializeEntity, DeviceConnecting, DeviceConnected>()
                .WithAll<Device>()
                .ForEach((Entity e, int entityInQueryIndex, in Keyboard keyboard) =>
            {
                if (keyboard.IsConnecting())
                {
                    PostUpdateCommands.AddComponent<DeviceConnecting>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // Mouse
            Dependency = Entities
                .WithNone<InitializeEntity, DeviceConnecting, DeviceConnected>()
                .WithAll<Device>()
                .ForEach((Entity e, int entityInQueryIndex, in Mouse mouse) =>
            {
                if (mouse.IsConnecting())
                {
                    PostUpdateCommands.AddComponent<DeviceConnecting>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // Gamepad
            Dependency = Entities
                .WithNone<InitializeEntity, DeviceConnecting, DeviceConnected>()
                .WithAll<Device>()
                .ForEach((Entity e, int entityInQueryIndex, in Gamepad gamepad) =>
            {
                if (gamepad.IsConnecting()) // startButton.wasPressedThisFrame == 1 || gamepad.selectButton.wasPressedThisFrame == 1)
                {
                    PostUpdateCommands.AddComponent<DeviceConnecting>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // Touchpad
            Dependency = Entities
                .WithNone<InitializeEntity, DeviceConnecting, DeviceConnected>()
                .WithAll<Device>()
                .ForEach((Entity e, int entityInQueryIndex, in Touchpad touchpad) =>
            {
                if (touchpad.IsConnecting()) // startButton.wasPressedThisFrame == 1 || gamepad.selectButton.wasPressedThisFrame == 1)
                {
                    PostUpdateCommands.AddComponent<DeviceConnecting>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}