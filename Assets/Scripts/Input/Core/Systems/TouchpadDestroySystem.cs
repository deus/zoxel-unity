using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Input
{
    //! Disposes of Touchpads.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class TouchpadDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, Touchpad>()
                .ForEach((in Touchpad touchpad) =>
			{
                touchpad.Dispose();
			}).ScheduleParallel();
        }
    }
}