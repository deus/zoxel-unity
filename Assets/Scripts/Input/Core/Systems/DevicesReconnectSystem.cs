using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Input
{
    //! Spawns player entities with controllers.
    [UpdateAfter(typeof(ConnectedDevicesDisconnectSystem))]
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class DevicesReconnectSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery devicesQuery;
        private EntityQuery mouseDevicesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            devicesQuery = GetEntityQuery(
                ComponentType.Exclude<DeviceConnected>(),
                ComponentType.ReadOnly<DeviceConnecting>(),
                ComponentType.ReadOnly<Device>());
            mouseDevicesQuery = GetEntityQuery(
                ComponentType.Exclude<DeviceConnected>(),
                ComponentType.ReadOnly<Mouse>(),
                ComponentType.ReadOnly<Device>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (devicesQuery.IsEmpty)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var deviceEntities = devicesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var deviceTypeDatas = GetComponentLookup<DeviceTypeData>(true);
            var mouseEntities = mouseDevicesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            Dependency = Entities
                .WithAll<ConnectedDevicesDisconnected>()
                .ForEach((Entity e, int entityInQueryIndex, ref ConnectedDevices connectedDevices, ref DeviceTypeData deviceTypeData) =>
            {
                if (deviceEntities.Length == 0)
                {
                    return;
                }
                var deviceEntity = deviceEntities[0];
                var deviceTypeData2 = deviceTypeDatas[deviceEntity];
                deviceTypeData.type = deviceTypeData2.type;
                connectedDevices.Add(deviceEntity);
                PostUpdateCommands.AddComponent<DeviceConnected>(entityInQueryIndex, deviceEntity);
                // if keyboard, find first mouse
                if (HasComponent<Keyboard>(deviceEntity))
                {
                    if (mouseEntities.Length > 0)
                    {
                        var mouseDeviceEntity = mouseEntities[0];
                        connectedDevices.Add(mouseDeviceEntity);
                        PostUpdateCommands.AddComponent<DeviceConnected>(entityInQueryIndex, mouseDeviceEntity);
                    }
                }
            })  .WithReadOnly(deviceEntities)
                .WithReadOnly(mouseEntities)
                .WithReadOnly(deviceTypeDatas)
                .WithNativeDisableContainerSafetyRestriction(deviceTypeDatas)
                .WithDisposeOnCompletion(deviceEntities)
                .WithDisposeOnCompletion(mouseEntities)
				.WithDisposeOnCompletion(deviceTypeDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}