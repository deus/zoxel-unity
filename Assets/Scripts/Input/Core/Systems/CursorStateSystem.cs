﻿using Unity.Entities;
using Unity.Burst;
using Zoxel.Input.Authoring;

namespace Zoxel.Input
{
    //! Sets unity Cursor Visible.
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class CursorStateSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            var alwaysUnlocked = false;
            #if UNITY_EDITOR
            if (HasSingleton<InputSettings>())
            {
                alwaysUnlocked = GetSingleton<InputSettings>().isDisableEditorMouseLock;
            }
            #endif
            // this seems bad logic, make it just switch not locked once, and then never switch after
            Entities
                .WithAll<ControllerDisabled>()
                .ForEach((ref Controller controller) =>
            {
                controller.lockState = (int) UnityEngine.CursorLockMode.None;
            }).ScheduleParallel();
            Entities
                .WithNone<ControllerDisabled>()
                .ForEach((ref Controller controller, in DeviceTypeData deviceTypeData) =>
            {
                if (controller.mapping == ControllerMapping.Menu 
                    || controller.mapping == ControllerMapping.Input
                    || controller.mapping == ControllerMapping.RealmUI 
                    || controller.mapping == ControllerMapping.Dialogue
                    || controller.mapping == ControllerMapping.ChestUI 
                    || controller.mapping == ControllerMapping.None)
                {
                    if (controller.mapping == ControllerMapping.None)
                    {
                        if (deviceTypeData.type == DeviceType.Keyboard)
                        {
                            controller.lockState = (byte) UnityEngine.CursorLockMode.None;
                        }
                    }
                    else
                    {
                        controller.lockState = (byte) UnityEngine.CursorLockMode.None;
                    }
                }
                else if (deviceTypeData.type == DeviceType.Keyboard)
                {
                    controller.lockState = (byte) UnityEngine.CursorLockMode.Locked;
                }
                else if (deviceTypeData.type == DeviceType.Gamepad)
                {
                    controller.lockState = (byte) UnityEngine.CursorLockMode.Locked;
                }
                else
                {
                    controller.lockState = (byte) UnityEngine.CursorLockMode.None;
                }
                if (alwaysUnlocked)
                {
                    controller.lockState = (byte) UnityEngine.CursorLockMode.None;
                }
            }).ScheduleParallel();
            Entities.ForEach((in Controller controller, in DeviceTypeData deviceTypeData) =>
            {
                if (deviceTypeData.type != DeviceType.Keyboard) //  && deviceTypeData.type != DeviceType.Mouse)
                {
                    return;
                }
                // hide normally if gamepad
                if (controller.lockState != (byte) UnityEngine.Cursor.lockState)
                {
                    UnityEngine.Cursor.lockState = (UnityEngine.CursorLockMode) controller.lockState;
                    UnityEngine.Cursor.visible = UnityEngine.Cursor.lockState != UnityEngine.CursorLockMode.Locked;
                }
            }).WithoutBurst().Run();
        }
    }
}

// UnityEngine.Cursor.visible = false;
/*if (UnityEngine.Cursor.lockState == UnityEngine.CursorLockMode.Locked)
{
    UnityEngine.Cursor.visible = false;
}
else
{
    UnityEngine.Cursor.visible = true;
}*/
/*#if UNITY_EDITOR
public const bool isAlwaysUnlockedCursor = true;    // true;
#else
public const bool isAlwaysUnlockedCursor = false;
#endif
*/
//if (isAlwaysUnlockedCursor)
//{
//    controller.lockState = (byte) UnityEngine.CursorLockMode.None;
//}
// lock cursor if mouse?
/*#if UNITY_EDITOR
controller.lockState = (int)UnityEngine.CursorLockMode.None;
#else
controller.lockState = (int)UnityEngine.CursorLockMode.Locked;
#endif*/
        /*#if UNITY_EDITOR
        private const bool alwaysUnlocked = true;
        // private const bool alwaysUnlocked = false;
        #else
        private const bool alwaysUnlocked = false;
        #endif*/
