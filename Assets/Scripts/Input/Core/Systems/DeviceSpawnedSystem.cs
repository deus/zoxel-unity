using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Input
{
    //! Adds player to player home after spawned.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class DeviceSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery devicesHomeQuery;
        private EntityQuery newDevicesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DelayEvent>(),
                ComponentType.ReadOnly<OnDeviceSpawned>());
            devicesHomeQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadWrite<DeviceLinks>());
            newDevicesQuery = GetEntityQuery(
                ComponentType.ReadOnly<NewDevice>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.ReadOnly<DeviceHomeLink>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<NewDevice>(newDevicesQuery);
			var deviceSpawnedEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
			Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var onDeviceSpawned = GetComponentLookup<OnDeviceSpawned>(true);
			Dependency = Entities
                .WithNone<DestroyEntity>()
                .ForEach((Entity e, ref DeviceLinks deviceLinks) =>
			{
				for (int i = 0; i < deviceSpawnedEntities.Length; i++)
				{
					var e2 = deviceSpawnedEntities[i];
					var onDeviceSpawned2 = onDeviceSpawned[e2];
                    if (onDeviceSpawned2.deviceHome == e)
                    {
						deviceLinks.Add(onDeviceSpawned2.id);
                    }
				}
				// UnityEngine.Debug.LogError("Added " + onMinivoxesSpawned.spawned + " to chunk.");
			})  .WithReadOnly(deviceSpawnedEntities)
				.WithReadOnly(onDeviceSpawned)
				.WithDisposeOnCompletion(deviceSpawnedEntities)
				.WithDisposeOnCompletion(onDeviceSpawned)
				.ScheduleParallel(Dependency);
			var deviceHomeEntities = devicesHomeQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
			Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var deviceLinks = GetComponentLookup<DeviceLinks>(false);
			// deviceHomeEntities.Dispose();
			Dependency = Entities
				.WithAll<NewDevice>()
				.ForEach((Entity e, in ZoxID zoxID, in DeviceHomeLink deviceHomeLink) =>
			{
				var deviceLinks2 = deviceLinks[deviceHomeLink.deviceHome];
				deviceLinks2.devices[zoxID.id] = e;
                if (deviceHomeEntities.Length > 0) { var e2 = deviceHomeEntities[0]; }
			})  .WithNativeDisableContainerSafetyRestriction(deviceLinks)
				.WithDisposeOnCompletion(deviceLinks)
                .WithReadOnly(deviceHomeEntities)
                .WithDisposeOnCompletion(deviceHomeEntities)
				.ScheduleParallel(Dependency);
        }
    }
}