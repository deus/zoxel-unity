/*using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
// todo: handle new connections externally and assign to a controller entity that is empty

namespace Zoxel.Input
{
    //! Handles an input device disconnecting.
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class ControllerDisconnectedSystem : SystemBase
    {
        private EntityQuery playerQuery;

        protected override void OnCreate()
        {
            playerQuery = GetEntityQuery(ComponentType.ReadOnly<Controller>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (playerQuery.IsEmpty)
            {
                return;
            }
            // Get connected devices - by getting all controllers
            // for all players, add device id
            var connectedDeviceIDs = new NativeList<int>(Allocator.TempJob);
            Entities.ForEach((in Controller controller) =>
            {
                connectedDeviceIDs.Add(controller.deviceID);
            }).WithoutBurst().Run();
            var connectingDeviceType = (byte) 0;
            var connectingDeviceID = -1;
            var pad = InputUtil.GetConnectingGamepad(in connectedDeviceIDs);
            if (pad != null)
            {
                connectingDeviceID = pad.deviceId;
                connectingDeviceType = DeviceType.Gamepad;
            }
            else
            {
                var keyboard = InputUtil.GetConnectingKeyboard(in connectedDeviceIDs);
                if (keyboard != null)
                {
                    connectingDeviceID = keyboard.deviceId;
                    connectingDeviceType = DeviceType.Keyboard;
                }
                // var touchscreen = PlayerSpawnSystem.GetConnectingTouchpad(in connectedDeviceIDs);
            }
            // pass in any new connecting devices
            // pass in any previous players
            // do it in parallel, just cause
            Entities.ForEach((ref Controller controller) =>
            {
                // if disconnected
                if (controller.deviceID == 0)
                {
                    if (connectingDeviceID != -1)
                    {
                        deviceTypeData.type = connectingDeviceType;
                        controller.deviceID = connectingDeviceID;
                    }
                }
                // if connected!
                else if (connectedDeviceIDs.Length == 1)
                {
                    HandleSwitchingInput(ref controller, in connectedDeviceIDs, connectingDeviceID, connectingDeviceType);
                }
            })  .WithReadOnly(connectedDeviceIDs)
                .WithDisposeOnCompletion(connectedDeviceIDs)
                .ScheduleParallel();
        }

        private static bool HandleSwitchingInput(ref Controller controller, in NativeList<int> connectedDeviceIDs,
            int connectingDeviceID, byte connectingDeviceType)
        {
            if (connectingDeviceID != -1)
            {
                if (deviceTypeData.type == DeviceType.Gamepad && connectingDeviceType != DeviceType.Gamepad)
                {
                    deviceTypeData.type = connectingDeviceType;
                    controller.deviceID = connectingDeviceID;
                    return true;
                }
                else if (deviceTypeData.type == DeviceType.Keyboard && connectingDeviceType != DeviceType.Keyboard)
                {
                    deviceTypeData.type = connectingDeviceType;
                    controller.deviceID = connectingDeviceID;
                    return true;
                }
            }
            return false;
        }
    }
}*/