using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Input
{
    //! Spawns Input entities with controllers.
    [UpdateAfter(typeof(DeviceSpawnedSystem))]
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class ConnectedDevicesDisconnectSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery devicesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DelayEvent>(),
                ComponentType.ReadOnly<OnDeviceDisconnected>(),
                ComponentType.ReadOnly<ZoxID>());
            devicesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Device>(),
                ComponentType.ReadOnly<ZoxID>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var disconnectedDeviceEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var diconnectedIDs = GetComponentLookup<ZoxID>(true);
            var deviceEntities = devicesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var deviceIDs = GetComponentLookup<ZoxID>(true);
            deviceEntities.Dispose();
            Dependency = Entities
                .WithNone<ConnectedDevicesDisconnected>()
                .WithAll<DevicesHome>()
                .ForEach((Entity e, int entityInQueryIndex, ref ConnectedDevices connectedDevices) =>
            {
                var disconnectedDevices = false;
                for (int i = 0; i < disconnectedDeviceEntities.Length; i++)
                {
                    var e2 = disconnectedDeviceEntities[i];
                    var disconnectedID = diconnectedIDs[e2];
                    if (connectedDevices.Remove(in disconnectedID, in deviceIDs))
                    {
                        disconnectedDevices = true;
                    }
                }
                if (disconnectedDevices)
                {
                    // UnityEngine.Debug.LogError("Removed connected devices");
                    PostUpdateCommands.AddComponent<ConnectedDevicesDisconnected>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(disconnectedDeviceEntities)
                .WithReadOnly(diconnectedIDs)
                .WithReadOnly(deviceIDs)
                .WithDisposeOnCompletion(disconnectedDeviceEntities)
				.WithDisposeOnCompletion(diconnectedIDs)
				.WithDisposeOnCompletion(deviceIDs)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}