using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Input
{
    //! Disposes of ConnectedDevices & DeviceLinks.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ConnectedDevicesDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithAll<DestroyEntity, ConnectedDevices>()
                .ForEach((int entityInQueryIndex, in ConnectedDevices connectedDevices) =>
			{
                for (byte i = 0; i < connectedDevices.devices.Length; i++)
                {
                    var deviceEntity = connectedDevices.devices[i];
                    PostUpdateCommands.RemoveComponent<DeviceConnected>(entityInQueryIndex, deviceEntity);
                }
                connectedDevices.DisposeFinal();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
			Dependency = Entities
                .WithAll<DestroyEntity, DeviceLinks>()
                .ForEach((int entityInQueryIndex, in DeviceLinks deviceLinks) =>
			{
                if (deviceLinks.devices.IsCreated)
                {
                    foreach (var KVP in deviceLinks.devices)
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, KVP.Value);
                    }
                }
                deviceLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}