using Unity.Entities;
using Unity.Burst;
// lock a button on gamepad when switched mapping

namespace Zoxel.Input
{
    //! Sets controller mapping.
    /**
    *   \tod: Fix - when unpausing, or finishing dialogue, it jumps
    */
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class InputMappingSetSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref Controller controller, in SetControllerMapping setControllerMapping) =>
            {
                if (setControllerMapping.timeDelay == 0 || (elapsedTime - setControllerMapping.timeStarted) >= setControllerMapping.timeDelay)
                {
                    PostUpdateCommands.RemoveComponent<SetControllerMapping>(entityInQueryIndex, e);
                    controller.SetMapping(setControllerMapping.newMapping);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}