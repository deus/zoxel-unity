using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Input
{
    //! Adds DeviceDisconnecting events to devices that want to disconnect (only on home screen).
    /**
    *   \todo Make DeviceConnector based on actual DeviceHomeLink of each device
    */
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class DeviceDisconnectorSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<DeviceConnector>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities
                .WithNone<InitializeEntity, DeviceDisconnecting>()
                .WithAll<Device, DeviceConnected>()
                .ForEach((Entity e, int entityInQueryIndex, in Gamepad gamepad) =>
            {
                if (gamepad.IsDisconnecting())
                {
                    PostUpdateCommands.AddComponent<DeviceDisconnecting>(entityInQueryIndex, e);
                }
            }).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Entities
                .WithNone<InitializeEntity, DeviceDisconnecting>()
                .WithAll<Device, DeviceConnected>()
                .ForEach((Entity e, int entityInQueryIndex, in Keyboard keyboard) =>
            {
                if (keyboard.IsDisconnecting())
                {
                    PostUpdateCommands.AddComponent<DeviceDisconnecting>(entityInQueryIndex, e);
                }
            }).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Entities
                .WithNone<InitializeEntity, DeviceDisconnecting>()
                .WithAll<Device, DeviceConnected>()
                .ForEach((Entity e, int entityInQueryIndex, in Mouse mouse) =>
            {
                if (mouse.IsDisconnecting())
                {
                    PostUpdateCommands.AddComponent<DeviceDisconnecting>(entityInQueryIndex, e);
                }
            }).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Entities
                .WithNone<InitializeEntity, DeviceDisconnecting>()
                .WithAll<Device, DeviceConnected>()
                .ForEach((Entity e, int entityInQueryIndex, in Touchpad touchpad) =>
            {
                if (touchpad.IsDisconnecting())
                {
                    PostUpdateCommands.AddComponent<DeviceDisconnecting>(entityInQueryIndex, e);
                }
            }).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}