using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Input;
using Zoxel.UI;
using Zoxel.Cameras;

namespace Zoxel.Input.UI
{
    //! Spawns the generic Zoxel game UI entities.
    /**
    *   \todo Move functionality to SliderXY UI thingo.
    */
    [BurstCompile, UpdateInGroup(typeof(InputUISystemGroup))]
    public partial class JoystickUIMoveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery camerasQuery;
        private EntityQuery devicesQuery;
        private EntityQuery uisQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            camerasQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Camera>(),
                ComponentType.ReadOnly<LocalToWorld>());
            devicesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Device>());
            uisQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<JoystickUI>());
            RequireForUpdate(processQuery);
            RequireForUpdate(camerasQuery);
            RequireForUpdate(devicesQuery);
            RequireForUpdate(uisQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AddComponent<SetUIElementPosition>(processQuery);
            var cameraEntities = camerasQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var cameras = GetComponentLookup<Camera>(true);
            var cameraScreenRects = GetComponentLookup<CameraScreenRect>(true);
            var localToWorlds = GetComponentLookup<LocalToWorld>(true);
            var cameraProjectionMatrixs = GetComponentLookup<CameraProjectionMatrix>(true);
            cameraEntities.Dispose();
            var deviceEntities = devicesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var mouses = GetComponentLookup<Mouse>(true);
            var touchpads = GetComponentLookup<Touchpad>(true);
            deviceEntities.Dispose();
            var uiEntities = uisQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var joystickUIPositions = GetComponentLookup<Translation>(true);
            var joystickUIRotations = GetComponentLookup<Rotation>(true);
            var joystickUIUIPositions = GetComponentLookup<UIPosition>(true);
            var joystickUISize2Ds = GetComponentLookup<Size2D>(true);
            uiEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<JoystickButton>()
                .ForEach((ref LocalPosition localPosition, ref JoystickUIBounds joystickUIBounds, in Size2D size2D, in ParentLink parentLink,
                    in MouseDragEvent mouseDragEvent) =>
            {
                var isMouse = mouses.HasComponent(mouseDragEvent.mouse);
                if (!isMouse && !touchpads.HasComponent(mouseDragEvent.mouse))
                {
                    return;
                }
                var cameraEntity = mouseDragEvent.camera;
                if (!cameras.HasComponent(cameraEntity))
                {
                    return;
                }
                var isDragging = false;
                var mousePosition = float2.zero;
                if (isMouse)
                {
                    var mouse = mouses[mouseDragEvent.mouse];
                    isDragging = mouse.leftButton.isPressed != 0;
                    mousePosition = mouse.position;
                }
                else
                {
                    var touchpad = touchpads[mouseDragEvent.mouse];
                    bool didFind;
                    var finger = touchpad.GetFinger(mouseDragEvent.fingerID, out didFind);
                    isDragging = !(!didFind || (finger.state != FingerState.Begin && finger.state != FingerState.Moved));
                    mousePosition = finger.position;
                }
                //! When end dragging.
                if (!isDragging)
                {
                    localPosition.position = float3.zero;
                    return;
                }
                var uiDepth = joystickUIUIPositions[parentLink.parent].position.z;
                var camera = cameras[cameraEntity];
                var cameraScreenRect = cameraScreenRects[cameraEntity];
                var screenDimensions = camera.screenDimensions.ToFloat2();
                var pointer = MouseUtil.GetPointer(mousePosition, screenDimensions, cameraScreenRect.rect);
                var screenPosition = new float3(pointer.x, pointer.y, uiDepth);
                var cameraLocalToWorld = localToWorlds[cameraEntity];
                float4x4 cameraLocalToWorld2 = cameraLocalToWorld.Value;
                var projectionMatrix = cameraProjectionMatrixs[cameraEntity].projectionMatrix;
                // Flip z for camera matrix
                var globalPosition = CameraUtil.ScreenToWorldPoint(screenPosition, cameraLocalToWorld2, projectionMatrix);
                // Get new position at depth, like mouse follow
                var parentPosition = joystickUIPositions[parentLink.parent].Value;
                var parentRotation = joystickUIRotations[parentLink.parent].Value;
                localPosition.position = math.rotate(math.inverse(parentRotation), globalPosition - parentPosition);
                var bounds = (joystickUISize2Ds[parentLink.parent].size / 2f) - (size2D.size / 2f);
                localPosition.position.x = math.clamp(localPosition.position.x, -bounds.x, bounds.x);
                localPosition.position.y = math.clamp(localPosition.position.y, -bounds.y, bounds.y);
                joystickUIBounds.bounds = bounds;
                // min/max the button based on parent bounds and this ones size
			})  .WithReadOnly(mouses)
                .WithReadOnly(touchpads)
                .WithReadOnly(cameras)
                .WithReadOnly(cameraScreenRects)
                .WithReadOnly(localToWorlds)
                .WithReadOnly(cameraProjectionMatrixs)
                .WithReadOnly(joystickUIPositions)
                .WithReadOnly(joystickUIRotations)
                .WithReadOnly(joystickUIUIPositions)
                .WithReadOnly(joystickUISize2Ds)
                .ScheduleParallel();
        }
    }
}