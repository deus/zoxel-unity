using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.UI;

namespace Zoxel.Input.UI
{
    //! Spawns a JoystickUI ui!
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(InputUISystemGroup))]
    public partial class JoystickUISpawnSystem : SystemBase
    {
        public const int maxLogs = 6;
        public const float fadeOutDelay = 4f;
        private const float fadeOutTime = 4f;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity spawnPrefab;
        private Entity panelPrefab;
        private Entity buttonPrefab;
        private Entity buttonPrefab2;
        private float3 panelPosition;
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnJoystickUI),
                typeof(CharacterLink),
                typeof(UIAnchor),
                typeof(GenericEvent),
                typeof(DelayEvent));
            spawnPrefab = EntityManager.CreateEntity(spawnArchetype);
            uiHoldersQuery = GetEntityQuery(
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<UILink>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var panelPrefab = this.panelPrefab;
            var buttonPrefab = this.buttonPrefab;
            var buttonPrefab2 = this.buttonPrefab2;
            var panelPosition = this.panelPosition;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var characterEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
			var controllerLinks = GetComponentLookup<ControllerLink>(true);
            characterEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<SpawnJoystickUI>()
                .ForEach((int entityInQueryIndex, in CharacterLink characterLink, in UIAnchor uiAnchor) =>
            {
                var uiEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, panelPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, uiEntity, uiAnchor);
                if (uiAnchor.anchor == AnchorUIType.BottomLeft)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, uiEntity, new PanelPosition(panelPosition));
                }
                else if (uiAnchor.anchor == AnchorUIType.BottomRight)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, uiEntity, new PanelPosition(new float3(-panelPosition.x, panelPosition.y, 0f)));
                }
                // var spawnedCount = (byte) 1;
                var spawnedChildren = new NativeList<Entity>();
                // spawn button child
                var seed = (int) (128 + elapsedTime + 0 * 2048 + 2048 * entityInQueryIndex);
                var joystickButtonEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, buttonPrefab, uiEntity);
                //UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, joystickButtonEntity, 0);
                PostUpdateCommands.SetComponent(entityInQueryIndex, joystickButtonEntity, new Seed(seed));
                spawnedChildren.Add(joystickButtonEntity);
                // if rigght side, add extra buttons
                if (uiAnchor.anchor == AnchorUIType.BottomRight)
                {
                    var extraButtonEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, buttonPrefab2, uiEntity);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, extraButtonEntity, 1);
                    var seed2 = (int) (128 + elapsedTime + 1 * 2048 + 2048 * entityInQueryIndex);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, extraButtonEntity, new Seed(seed2));
                    spawnedChildren.Add(extraButtonEntity);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, extraButtonEntity, new VirtualInputButton(0));
                }
                else // if (uiAnchor.anchor == AnchorUIType.BottomRight)
                {
                    var extraButtonEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, buttonPrefab2, uiEntity);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, extraButtonEntity, 1);
                    var seed2 = (int) (128 + elapsedTime + 1 * 2048 + 2048 * entityInQueryIndex);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, extraButtonEntity, new Seed(seed2));
                    spawnedChildren.Add(extraButtonEntity);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, extraButtonEntity, new VirtualInputButton(1));
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, uiEntity, new OnChildrenSpawned((byte) spawnedChildren.Length));
                if (characterLink.character.Index == 0)
                {
                    spawnedChildren.Dispose();
                    return;
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, uiEntity, characterLink);
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab),
                    new OnUISpawned(characterLink.character, 1));
                if (cameraLinks.HasComponent(characterLink.character))
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, uiEntity, cameraLinks[characterLink.character]);
                }
                if (controllerLinks.HasComponent(characterLink.character))
                {
                    var controllerLink = controllerLinks[characterLink.character];
                    for (byte i = 0; i < spawnedChildren.Length; i++)
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, spawnedChildren[i], controllerLink);
                    }
                }
                if (uiAnchor.anchor == AnchorUIType.BottomLeft)
                {
                    // PostUpdateCommands.SetComponent(entityInQueryIndex, joystickButtonEntity, new JoystickUIType(0));
                }
                else if (uiAnchor.anchor == AnchorUIType.BottomRight)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, joystickButtonEntity, new JoystickUIType(1));
                }
                spawnedChildren.Dispose();
            })  .WithReadOnly(cameraLinks)
                .WithReadOnly(controllerLinks)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private void InitializePrefabs()
        {
            if (panelPrefab.Index != 0)
            {
                return;
            }
            var joystickScale = 4.4f;   // 3.6f;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var iconSize = uiDatam.GetIconSize(uiScale);
            panelPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(panelPrefab);
            EntityManager.RemoveComponent<SpawnHeaderUI>(panelPrefab);
            EntityManager.RemoveComponent<NavigationDirty>(panelPrefab);
            EntityManager.RemoveComponent<GridUISize>(panelPrefab);
            EntityManager.RemoveComponent<GridUI>(panelPrefab);
            EntityManager.RemoveComponent<GridUIDirty>(panelPrefab);
            EntityManager.AddComponent<JoystickUI>(panelPrefab);
            EntityManager.AddComponent<GameOnlyUI>(panelPrefab);
            EntityManager.SetComponentData(panelPrefab, new PanelUI(PanelType.JoystickUI));
            EntityManager.SetComponentData(panelPrefab, new Size2D(new float2(iconSize.x, iconSize.y) * joystickScale));
            EntityManager.AddComponent<PanelPosition>(panelPrefab);
            panelPosition = new float3(iconSize.x, iconSize.y, 0f) * 1f;
            buttonPrefab = EntityManager.Instantiate(UICoreSystem.textlessButtonPrefab); // iconOverlayPrefab);
            EntityManager.AddComponent<Prefab>(buttonPrefab);
            EntityManager.AddComponent<JoystickButton>(buttonPrefab);
            EntityManager.AddComponent<SetUIElementPosition>(buttonPrefab);
            EntityManager.SetComponentData(buttonPrefab, new Size2D(iconSize * 1.6f));
            EntityManager.AddComponentData(buttonPrefab, new RaycastUISize(iconSize * joystickScale));
            EntityManager.SetComponentData(buttonPrefab, new MaterialBaseColor(new Color(105, 25, 35)));
            EntityManager.AddComponent<JoystickUIType>(buttonPrefab);
            EntityManager.AddComponent<JoystickUIBounds>(buttonPrefab);
            EntityManager.AddComponent<ControllerLink>(buttonPrefab);
            // extra!
            buttonPrefab2 = EntityManager.Instantiate(UICoreSystem.textlessButtonPrefab); // iconOverlayPrefab);
            EntityManager.AddComponent<Prefab>(buttonPrefab2);
            EntityManager.AddComponent<VirtualInputButton>(buttonPrefab2);
            EntityManager.AddComponent<ControllerLink>(buttonPrefab2);
            EntityManager.AddComponent<SetUIElementPosition>(buttonPrefab2);
            EntityManager.SetComponentData(buttonPrefab2, new Size2D(iconSize * 1.4f));
            EntityManager.AddComponentData(buttonPrefab2, new RaycastUISize(iconSize * 1.8f));
            EntityManager.SetComponentData(buttonPrefab2, new MaterialBaseColor(new Color(45, 8, 20)));
            EntityManager.AddComponentData(buttonPrefab2, new LocalPosition(new float3(0, iconSize.y * 3.6f, 0)));
        }
    }
}