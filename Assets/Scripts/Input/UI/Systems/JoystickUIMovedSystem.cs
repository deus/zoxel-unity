using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Input;
using Zoxel.UI;

namespace Zoxel.Input.UI
{
    //! Spawns the generic Zoxel game UI entities.
    /**
    *   \todo Move functionality to SliderXY UI refactor.
    */
    [UpdateAfter(typeof(JoystickUIMoveSystem))]
    [BurstCompile, UpdateInGroup(typeof(InputUISystemGroup))]
    public partial class JoystickUIMovedSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery controllersQuery;
        private EntityQuery devicesQuery;

        protected override void OnCreate()
        {
            controllersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Controller>(),
                ComponentType.ReadOnly<ConnectedDevices>());
            devicesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Device>());
            RequireForUpdate(processQuery);
            RequireForUpdate(controllersQuery);
            RequireForUpdate(devicesQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var controllersEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var connectedDevices = GetComponentLookup<ConnectedDevices>(true);
            controllersEntities.Dispose();
            var deviceEntities = devicesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var touchpads = GetComponentLookup<Touchpad>(false);
            var gamepads = GetComponentLookup<Gamepad>(false);
            deviceEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery) 
                .WithChangeFilter<LocalPosition>()
                .WithAll<JoystickButton>()
                .ForEach((in LocalPosition localPosition, in ControllerLink controllerLink, in JoystickUIType joystickUIType,
                    in JoystickUIBounds joystickUIBounds) =>
            {
                if (!connectedDevices.HasComponent(controllerLink.controller))
                {
                    return;
                }
                // set virtual joystick values
                var connectedDevices2 = connectedDevices[controllerLink.controller];
                var targetDeviceEntity = new Entity();
                for (byte i = 0; i < connectedDevices2.devices.Length; i++)
                {
                    var deviceEntity = connectedDevices2.devices[i];
                    if (touchpads.HasComponent(deviceEntity))
                    {
                        targetDeviceEntity = deviceEntity;
                        break;
                    }
                    else if (gamepads.HasComponent(deviceEntity))
                    {
                        targetDeviceEntity = deviceEntity;
                        break;
                    }
                }
                if (targetDeviceEntity.Index == 0)
                {
                    return;
                }
                var joystickValue = new float2(
                    localPosition.position.x / joystickUIBounds.bounds.x,
                    localPosition.position.y / joystickUIBounds.bounds.y);
                joystickValue.x = math.clamp(joystickValue.x, -1f, 1f);
                joystickValue.y = math.clamp(joystickValue.y, -1f, 1f);
                if (gamepads.HasComponent(targetDeviceEntity))
                {
                    var gamepad = gamepads[targetDeviceEntity];
                    if (joystickUIType.type == 0)
                    {
                        gamepad.leftStick = joystickValue;
                    }
                    else
                    {
                        gamepad.rightStick = joystickValue;
                    }
                    gamepads[targetDeviceEntity] = gamepad;
                }
                else if (touchpads.HasComponent(targetDeviceEntity))
                {
                    var touchpad = touchpads[targetDeviceEntity];
                    if (joystickUIType.type == 0)
                    {
                        touchpad.leftStick = joystickValue;
                    }
                    else
                    {
                        touchpad.rightStick = joystickValue;
                    }
                    touchpads[targetDeviceEntity] = touchpad;
                }
			})  .WithReadOnly(connectedDevices)
                .WithNativeDisableContainerSafetyRestriction(touchpads)
                .WithNativeDisableContainerSafetyRestriction(gamepads)
                .ScheduleParallel();
        }
    }
}