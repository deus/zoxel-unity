using Unity.Entities;
using Zoxel.Rendering;
using Zoxel.UI;

namespace Zoxel.Input.UI
{
    [UpdateInGroup(typeof(InputUISystemGroup))]
    public partial class JoystickButtonInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var cooldownOverlayMaterial = UIManager.instance.materials.cooldownOverlayMaterial;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, JoystickButton>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in UIMeshData uiMeshData, in Size2D size2D) =>
            {
                var size = size2D.size;
                zoxMesh.material = new UnityEngine.Material(cooldownOverlayMaterial);
                zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size, uiMeshData.horizontalAlignment, uiMeshData.verticalAlignment, uiMeshData.offset);
                zoxMesh.layer = worldUILayer;
                PostUpdateCommands2.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
            Entities
                .WithAll<InitializeEntity, VirtualInputButton>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in UIMeshData uiMeshData, in Size2D size2D) =>
            {
                var size = size2D.size;
                zoxMesh.material = new UnityEngine.Material(cooldownOverlayMaterial);
                zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size, uiMeshData.horizontalAlignment, uiMeshData.verticalAlignment, uiMeshData.offset);
                zoxMesh.layer = worldUILayer;
                PostUpdateCommands2.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
        }
    }
}