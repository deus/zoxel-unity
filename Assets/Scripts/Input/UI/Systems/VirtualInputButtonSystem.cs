using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Input;
using Zoxel.UI;

namespace Zoxel.Input.UI
{
    //! Triggers device buttons.
    [BurstCompile, UpdateInGroup(typeof(InputUISystemGroup))]
    public partial class VirtualInputButtonSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllersQuery;
        private EntityQuery devicesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Controller>());
            /*devicesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Device>());*/
            RequireForUpdate(processQuery);
            RequireForUpdate(controllersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var controllers = GetComponentLookup<Controller>(false);
            controllerEntities.Dispose();
            /*var deviceEntities = devicesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var mouses = GetComponentLookup<Mouse>(true);
            var gamepads = GetComponentLookup<Gamepad>(true);
            deviceEntities.Dispose();*/
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<VirtualInputButton>()
                .ForEach((in VirtualInputButton virtualInputButton, in UIClickEvent uiClickEvent) =>
            {
                var controllerEntity = uiClickEvent.controller;
                var controller = controllers[controllerEntity];
                if (virtualInputButton.type == 0)
                {
                    controller.gamepad.buttonA.wasPressedThisFrame = 1;
                    controller.gamepad.buttonA.isPressed = 1;
                }
                else if (virtualInputButton.type == 1)
                {
                    controller.gamepad.startButton.wasPressedThisFrame = 1;
                    controller.gamepad.startButton.isPressed = 1;
                    controller.keyboard.escapeKey.wasPressedThisFrame = 1;
                    controller.keyboard.escapeKey.isPressed = 1;
                }
                controllers[controllerEntity] = controller;
                // UnityEngine.Debug.LogError("Button Pressed: " + virtualInputButton.type);
                // var deviceEntity = uiClickEvent.device;
                // var deviceEntity = uiClickEvent.device;
			})  //.WithReadOnly(mouses)
                //.WithReadOnly(touchpads)
                .WithNativeDisableContainerSafetyRestriction(controllers)
                .ScheduleParallel();
        }
    }
}