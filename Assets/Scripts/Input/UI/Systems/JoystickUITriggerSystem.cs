using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Input;
using Zoxel.Input.Authoring;
using Zoxel.UI;

namespace Zoxel.Input.UI
{
    //! Spawns the generic Zoxel game UI entities.
    [BurstCompile, UpdateInGroup(typeof(InputUISystemGroup))]
    public partial class JoystickUITriggerSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery playersQuery;
        private EntityQuery devicesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            playersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Controller>());
            devicesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Touchpad>());
            RequireForUpdate(processQuery);
            RequireForUpdate(playersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var isTestVirtualJoysticks = false;
            if (HasSingleton<InputSettings>())
            {
                isTestVirtualJoysticks = GetSingleton<InputSettings>().isTestVirtualJoysticks;
            }
            const float spawnDelay = 0.0f;
            var elapsedTime = World.Time.ElapsedTime;
            var spawnJoystickUIPrefab = JoystickUISpawnSystem.spawnPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = playersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var connectedDevices = GetComponentLookup<ConnectedDevices>(true);
            controllerEntities.Dispose();
            var deviceEntities = devicesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var touchpads = GetComponentLookup<Touchpad>(true);
            deviceEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity, InitializeEntity>()
                .WithAll<SpawnRealmUI>()
                .ForEach((Entity e, int entityInQueryIndex, in ControllerLink controllerLink, in SpawnRealmUI spawnRealmUI) =>
            {
                // does player have a touch devvice connected
                if (!isTestVirtualJoysticks)
                {
                    if (!connectedDevices.HasComponent(controllerLink.controller))
                    {
                        // UnityEngine.Debug.LogError("Controller Link not set: " + controllerLink.controller.Index);
                        return;
                    }
                    var connectedDevices2 = connectedDevices[controllerLink.controller];
                    var hasTouchpad = false;
                    for (byte i = 0; i < connectedDevices2.devices.Length; i++)
                    {
                        var deviceEntity = connectedDevices2.devices[i];
                        if (touchpads.HasComponent(deviceEntity))
                        {
                            hasTouchpad = true;
                            break;
                        }
                    }
                    if (!hasTouchpad)
                    {
                        return;
                    }
                }
                // DeviceLink, check if player has device of Touch connected
                // spawn joy stick ui
                {
                    var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnJoystickUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new CharacterLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new DelayEvent(elapsedTime, spawnDelay));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new UIAnchor(AnchorUIType.BottomLeft));
                }
                {
                    var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnJoystickUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new CharacterLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new DelayEvent(elapsedTime, spawnDelay));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new UIAnchor(AnchorUIType.BottomRight));
                }
			})  .WithReadOnly(connectedDevices)
                .WithReadOnly(touchpads)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}