﻿using Unity.Entities;

namespace Zoxel.Input.UI
{
    //! The grouping of all input systems.
    [ UpdateInGroup(typeof(InputSystemGroup))]
    public partial class InputUISystemGroup : ComponentSystemGroup { }
}