using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Input.UI
{
    public struct JoystickUIBounds : IComponentData
    {
        public float2 bounds;
    }
}