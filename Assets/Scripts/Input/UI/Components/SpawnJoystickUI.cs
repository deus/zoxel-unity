using Unity.Entities;

namespace Zoxel.Input.UI
{
	//! A spawn event for a JoystickUI.
	public struct SpawnJoystickUI : IComponentData { }
}