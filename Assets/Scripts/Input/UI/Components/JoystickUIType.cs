using Unity.Entities;

namespace Zoxel.Input.UI
{
    public struct JoystickUIType : IComponentData
    {
        public byte type;

        public JoystickUIType(byte type)
        {
            this.type = type;
        }
    }
}