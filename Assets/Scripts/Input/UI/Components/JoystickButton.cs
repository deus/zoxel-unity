using Unity.Entities;

namespace Zoxel.Input.UI
{
	//! A tag for a joystick Button.
	public struct JoystickButton : IComponentData { }
}