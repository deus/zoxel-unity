using Unity.Entities;

namespace Zoxel.Input.UI
{
    public struct VirtualInputButton : IComponentData
    {
        public byte type;

        public VirtualInputButton(byte type)
        {
            this.type = type;
        }
    }
}