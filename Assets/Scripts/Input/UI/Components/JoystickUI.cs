using Unity.Entities;

namespace Zoxel.Input.UI
{
	//! A tag for a joystick ui.
	public struct JoystickUI : IComponentData { }
}