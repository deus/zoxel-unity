using UnityEngine.InputSystem;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Input
{
    public static class InputUtil
    {
        public static byte GetDeviceType(in UnityEngine.InputSystem.InputDevice inputDevice)
        {
            if (inputDevice == null)
            {
                return DeviceType.None;
            }
            var keyboard = inputDevice as UnityEngine.InputSystem.Keyboard;
            if (keyboard != null)
            {
                return DeviceType.Keyboard;
            }
            var mouse = inputDevice as UnityEngine.InputSystem.Mouse;
            if (mouse != null)
            {
                return DeviceType.Mouse;
            }
            var gamepad = inputDevice as UnityEngine.InputSystem.Gamepad;
            if (gamepad != null)
            {
                return DeviceType.Gamepad;
            }
            var touchscreen = inputDevice as UnityEngine.InputSystem.Touchscreen;
            if (touchscreen != null)
            {
                return DeviceType.Touch;
            }
            return DeviceType.None;
        }
    }
}

        // (InputDevice) Touchscreen
        /*public static UnityEngine.InputSystem.InputDevice GetConnectingTouchpad(in NativeList<int> deviceIDs) // EntityManager EntityManager, in PlayerLinks playerLinks)
        {
            foreach (var inputDevice in UnityEngine.InputSystem.InputSystem.devices)
            {
                var touchscreen = inputDevice as UnityEngine.InputSystem.Touchscreen;
                if (touchscreen != null && !deviceIDs.Contains(touchscreen.deviceId)) // !playerLinks.HasPlayer(EntityManager, touchscreen.deviceId))
                {
                    if (touchscreen.primaryTouch.phase.ReadValue() == UnityEngine.InputSystem.TouchPhase.Began)
                    {
                        return touchscreen as UnityEngine.InputSystem.InputDevice;
                    }
                }
            }
            return null;
        }

        public static UnityEngine.InputSystem.Keyboard GetConnectingKeyboard(in NativeList<int> deviceIDs) // EntityManager EntityManager, in PlayerLinks playerLinks)
        {
            foreach (var inputDevice in UnityEngine.InputSystem.InputSystem.devices)
            {
                var keyboard = inputDevice as UnityEngine.InputSystem.Keyboard;
                if (keyboard != null && !deviceIDs.Contains(keyboard.deviceId)) // !playerLinks.HasPlayer(EntityManager, keyboard.deviceId))
                {
                    var didMouseInput = false;
                    if (UnityEngine.InputSystem.Mouse.current != null)
                    {
                        var mousePosition = UnityEngine.InputSystem.Mouse.current.position.ReadValue();
                        didMouseInput = UnityEngine.InputSystem.Mouse.current.leftButton.wasPressedThisFrame
                            && mousePosition.x >= 0 && mousePosition.x <= UnityEngine.Screen.width
                            && mousePosition.y >= 0 && mousePosition.y <= UnityEngine.Screen.height;
                    }
                    if (didMouseInput || 
                            keyboard.enterKey.wasPressedThisFrame ||
                            keyboard.wKey.wasPressedThisFrame ||
                            keyboard.aKey.wasPressedThisFrame ||
                            keyboard.sKey.wasPressedThisFrame ||
                            keyboard.dKey.wasPressedThisFrame ||
                            keyboard.qKey.wasPressedThisFrame ||
                            keyboard.eKey.wasPressedThisFrame ||
                            keyboard.spaceKey.wasPressedThisFrame ||
                            keyboard.backspaceKey.wasPressedThisFrame)
                    {
                        return keyboard;
                    }
                }
            }
            return null;
        }

        public static UnityEngine.InputSystem.Gamepad GetConnectingGamepad(in NativeList<int> deviceIDs) // EntityManager EntityManager, in PlayerLinks playerLinks)
        {
            foreach (var pad in UnityEngine.InputSystem.Gamepad.all)
            {
                if (!deviceIDs.Contains(pad.deviceId)) // playerLinks.HasPlayer(EntityManager, pad.deviceId))
                {
                    if (pad.startButton.wasPressedThisFrame ||
                            pad.selectButton.wasPressedThisFrame ||
                            pad.aButton.wasPressedThisFrame ||
                            pad.bButton.wasPressedThisFrame ||
                            pad.yButton.wasPressedThisFrame ||
                            pad.xButton.wasPressedThisFrame ||
                            pad.leftTrigger.wasPressedThisFrame ||
                            pad.rightTrigger.wasPressedThisFrame ||
                            pad.leftShoulder.wasPressedThisFrame ||
                            pad.rightShoulder.wasPressedThisFrame ||
                            pad.leftStickButton.wasPressedThisFrame ||
                            pad.rightStickButton.wasPressedThisFrame ||

                            pad.buttonEast.wasPressedThisFrame ||
                            pad.buttonNorth.wasPressedThisFrame ||
                            pad.buttonSouth.wasPressedThisFrame ||
                            pad.buttonWest.wasPressedThisFrame ||

                            pad.crossButton.wasPressedThisFrame ||
                            pad.circleButton.wasPressedThisFrame ||
                            pad.triangleButton.wasPressedThisFrame ||
                            pad.squareButton.wasPressedThisFrame)
                    {
                        return pad;
                    }
                }
            }
            return null;
        }*/