using UnityEngine.InputSystem;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Input
{
    public static class MouseUtil
    {
        public static UnityEngine.InputSystem.Mouse GetMouse(int deviceID)
        {
            foreach (var device in UnityEngine.InputSystem.InputSystem.devices)
            {
                var device2 = device as UnityEngine.InputSystem.Mouse;
                if (device2 != null && device2.deviceId == deviceID)
                {
                    return device2;
                }
            }
            //Debug.LogError("Could not find keyboard: " + deviceID);
            return null;
        }

        public static void ExtractMouse(ref Mouse mouse, in UnityEngine.InputSystem.Mouse unityMouse, int2 screenDimensions)
        {
            if (unityMouse == null)
            {
                mouse.Reset();
            }
            else
            {
                mouse.Update(in unityMouse, screenDimensions);
            }
        }

        public static void ExtractMouse(ref Controller controller, in Mouse mouse)
        {
            // Mouse to gamepad data
            controller.mouse = mouse;
            controller.gamepad.buttonLT = mouse.rightButton;
            controller.gamepad.buttonRT = mouse.leftButton;
            controller.gamepad.buttonLT = mouse.rightButton;
            controller.gamepad.rightStick = mouse.delta;
            //UnityEngine.Debug.LogError("pointer: " + controller.mouse.pointer + ", pointer: " + controller.mouse.pointer);
            // UnityEngine.Debug.LogError("mouse.delta - controller.gamepad.rightStick: " + controller.gamepad.rightStick);
        }

        public static float2 GetPointer(float2 position, float2 screenDimensions, Rect cameraScreen)
        {
            var originalScreenDimensions = new float2(screenDimensions.x / cameraScreen.size.x, screenDimensions.y / cameraScreen.size.y);
            var scaledPointer = new float2(position.x / originalScreenDimensions.x, position.y / originalScreenDimensions.y);
            if (scaledPointer.x < cameraScreen.position.x || scaledPointer.x > cameraScreen.position.x + cameraScreen.size.x
                || scaledPointer.y < cameraScreen.position.y || scaledPointer.y > cameraScreen.position.y + cameraScreen.size.y)
            {
                // UnityEngine.Debug.LogError("Scaled position out of bounds: " + scaledPointer);
                return new float2(-1f, -1f);
            }
            // this should be 0 to 1 for x and y
            // if screen position y = 0.5, then ill need to
            //  minus position
            //  rescale position
            scaledPointer.x -= cameraScreen.position.x;
            scaledPointer.y -= cameraScreen.position.y;
            scaledPointer.x /= cameraScreen.size.x;
            scaledPointer.y /= cameraScreen.size.y;
            return scaledPointer;
        }
    }
} 

//.ReadValue();
/*if (mouse.rightButton.wasPressedThisFrame == 1)
{
    controller.gamepad.buttonLT = 1;
}
if (mouse.leftButton.wasPressedThisFrame == 1)
{
    controller.gamepad.buttonRT = 1;
    controller.gamepad.buttonRTHeld = 1;
}
else if (mouse.leftButton.wasReleasedThisFrame == 1)
{
    controller.gamepad.buttonRTHeld = 0;
}*/