using UnityEngine.InputSystem;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Input
{
    public static class KeyboardUtil
    {
        public static UnityEngine.InputSystem.Keyboard GetKeyboard(int deviceID)
        {
            foreach (var device in UnityEngine.InputSystem.InputSystem.devices)
            {
                var device2 = device as UnityEngine.InputSystem.Keyboard;
                if (device2 != null && device2.deviceId == deviceID)
                {
                    return device2;
                }
            }
            //Debug.LogError("Could not find keyboard: " + deviceID);
            return null;
        }

        public static void ExtractKeyboard(ref Keyboard keyboard, in UnityEngine.InputSystem.Keyboard unityKeyboard)
        {
            if (unityKeyboard == null)
            {
                keyboard.Reset();
            }
            else
            {
                keyboard.Update(in unityKeyboard);
            }
        }

        public static void ExtractKeyboard(ref Controller controller, in Keyboard keyboard)
        {
            controller.keyboard = keyboard;
            // controller.gamepad.leftStick = float2.zero;
            // controller.gamepad.rightStick = float2.zero;
            if (keyboard.wKey.isPressed != 0)
            {
                controller.gamepad.leftStick.y = 1;
            }
            if (keyboard.aKey.isPressed != 0)
            {
                controller.gamepad.leftStick.x = -1;
            } 
            if (keyboard.sKey.isPressed != 0)
            {
                controller.gamepad.leftStick.y = -1;
            }
            if (keyboard.dKey.isPressed != 0)
            {
                controller.gamepad.leftStick.x = 1;
            }
            controller.gamepad.startButton = keyboard.enterKey;
            controller.gamepad.selectButton = keyboard.backspaceKey;
            controller.gamepad.buttonRB = keyboard.eKey;
            controller.gamepad.buttonLB = keyboard.qKey;
            // controller.gamepad.buttonB = keyboard.tKey;
            controller.gamepad.buttonX = keyboard.rKey;
            controller.gamepad.buttonY = keyboard.fKey;
            controller.gamepad.buttonA = keyboard.spaceKey;
            controller.gamepad.buttonB = keyboard.escapeKey;
        }
    }
}
            /*if (keyboard.enterKey.wasPressedThisFrame == 1)
            {
                controller.gamepad.startButton = 1;
            }
            // Buttons
            if (keyboard.eKey.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonRB = 1;
            }
            if (keyboard.qKey.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonLB = 1;
            }
            if (keyboard.tKey.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonB = 1;
            }
            if (keyboard.rKey.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonX = 1;
            }
            if (keyboard.fKey.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonY = 1;
            }
            if (keyboard.backspaceKey.wasPressedThisFrame == 1)
            {
                controller.gamepad.selectButton = 1;
            }
            if (keyboard.spaceKey.isPressed == 1)
            {
                controller.gamepad.buttonA.isPressed = 1;
            }
            if (keyboard.spaceKey.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonA.wasPressedThisFrame = 1;
            }
            if (keyboard.escapeKey.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonB = 1;
            }*/