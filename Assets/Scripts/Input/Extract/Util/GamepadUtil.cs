using UnityEngine.InputSystem;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Input
{
    public static class GamepadUtil
    {
        public static UnityEngine.InputSystem.Gamepad GetGamepad(int deviceID)
        {
            foreach (var device in UnityEngine.InputSystem.Gamepad.all)
            {
                if (device != null && device.deviceId == deviceID)
                {
                    return device;
                }
            }
            //Debug.LogError("Could not find gamepad: " + deviceID);
            return null;
        }

        public static void ExtractGamepad(ref Gamepad gamepad, in UnityEngine.InputSystem.Gamepad unityGamepad)
        {
            if (unityGamepad == null)
            {
                gamepad.Reset();
            }
            else
            {
                gamepad.Update(in unityGamepad);
            }
        }

        public static void ExtractGamepad(ref Controller controller, in Gamepad gamepad)
        {
            controller.gamepad = gamepad;
        }
    }
}
            /*controller.gamepad.leftStick = gamepad.leftStick; // .ReadValue();
            controller.gamepad.rightStick = gamepad.rightStick; // .ReadValue();
            if (gamepad.buttonA.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonA.wasPressedThisFrame = 1;
            }
            if (gamepad.buttonA.isPressed == 1)
            {
                controller.gamepad.buttonA.isPressed = 1;
            }
            if (gamepad.buttonB.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonB = 1;
            }
            if (gamepad.buttonX.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonX = 1;
            }
            if (gamepad.buttonY.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonY = 1;
            }
            if (gamepad.startButton.wasPressedThisFrame == 1)
            {
                controller.gamepad.startButton = 1;
            }
            if (gamepad.selectButton.wasPressedThisFrame == 1)
            {
                controller.gamepad.selectButton = 1;
            }
            if (gamepad.buttonRB.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonRB = 1;
            }
            if (gamepad.buttonLB.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonLB = 1;
            }
            if (gamepad.buttonRT.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonRT = 1;
            }
            if (gamepad.buttonLT.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonLT = 1;
            }
            // ButtonRT Held
            if (gamepad.buttonRT.wasPressedThisFrame == 1)
            {
                controller.gamepad.buttonRTHeld = 1;
            }
            else if (gamepad.buttonRT.wasReleasedThisFrame == 1)
            {
                controller.gamepad.buttonRTHeld = 0;
            }*/