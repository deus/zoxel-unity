using UnityEngine.InputSystem;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Input
{
    public static class TouchUtil
    {
        const float movementSpeed = 3.4f;
        const float rotationSpeedX = 8f;    // 6
        const float rotationSpeedY = 6f;
        
        public static UnityEngine.InputSystem.Touchscreen GetTouchpad(int deviceID)
        {
            foreach (var device in InputSystem.devices)
            {
                var device2 = device as UnityEngine.InputSystem.Touchscreen;
                if (device2 != null && device2.deviceId == deviceID)
                {
                    return device2;
                }
            }
            //Debug.LogError("Could not find gamepad: " + deviceID);
            return null;
        }

        public static void ExtractTouchpad(ref Controller controller, in Touchpad touchpad, int2 screenDimensions, double elapsedTime)
        {
            controller.mouse.Reset();
            controller.gamepad.leftStick = new float2();
            controller.gamepad.rightStick = new float2();
            if (touchpad.touchesCount == 0)
            {
                return;
            }
            controller.gamepad.leftStick = touchpad.leftStick;
            controller.gamepad.rightStick = touchpad.rightStick;
            controller.gamepad.rightStick.x *= rotationSpeedX;
            controller.gamepad.rightStick.y *= rotationSpeedY;
            var finger = touchpad.fingers[0];
            controller.mouse.position = finger.position;
            controller.mouse.delta = finger.delta;
            ExtractFinger(ref controller, in finger, screenDimensions, elapsedTime);
            /*if (touchpad.touchesCount >= 2)
            {
                var secondaryTouch = touchpad.secondaryTouch;
                ExtractFinger(ref controller, in secondaryTouch, screenDimensions, elapsedTime);
            }*/
        }

        private static void ExtractFinger(ref Controller controller, in Finger finger, int2 screenDimensions, double elapsedTime)
        {
            if (finger.state == FingerState.Begin)
            {
                controller.mouse.leftButton.wasPressedThisFrame = 1;
                controller.mouse.leftButton.isPressed = 1;
            }
            else if (finger.state == FingerState.Moved)
            {
                controller.mouse.leftButton.isPressed = 1;
            }
            else if (finger.state == FingerState.End)
            {
                controller.mouse.leftButton.isPressed = 1;
            }
        }
    }
}
            // clamp
            /*controller.gamepad.leftStick.x = math.clamp(controller.gamepad.leftStick.x, -1, 1);
            controller.gamepad.leftStick.y = math.clamp(controller.gamepad.leftStick.y, -1, 1);
            controller.gamepad.rightStick.x = math.clamp(controller.gamepad.rightStick.x, -12f, 12f);
            controller.gamepad.rightStick.y = math.clamp(controller.gamepad.rightStick.y, -12f, 12f);*/
            
            /*var fingerStartPosition = new float2(
                finger.startPosition.x / ((float) screenDimensions.x),
                finger.startPosition.y / ((float) screenDimensions.y));
            var fingerPosition = new float2(
                finger.position.x / ((float) screenDimensions.x),
                finger.position.y / ((float) screenDimensions.y));*/

                // var touchStartDelta = fingerPosition - fingerStartPosition;
                // controller.touchpad.touchDelta = direction;
                /*if (fingerStartPosition.x <= 0.4f)
                {
                    controller.gamepad.leftStick = touchStartDelta * movementSpeed;
                    controller.gamepad.leftStick.x *= 3f;
                    controller.gamepad.leftStick.y *= 1.5f;
                    controller.gamepad.leftStick.x = math.clamp(controller.gamepad.leftStick.x, -1, 1);
                    controller.gamepad.leftStick.y = math.clamp(controller.gamepad.leftStick.y, -1, 1);
                }
                else if (fingerStartPosition.x >= 0.6f)
                {
                    controller.gamepad.rightStick.x = touchStartDelta.x * 2f * rotationSpeed;
                    controller.gamepad.rightStick.y = touchStartDelta.y * rotationSpeed;
                    controller.gamepad.rightStick.x = math.clamp(controller.gamepad.rightStick.x, -12f, 12f);
                    controller.gamepad.rightStick.y = math.clamp(controller.gamepad.rightStick.y, -12f, 12f);
                }*/
                /* var inGame = controller.mapping == ControllerMapping.InGame;
                if (!inGame)
                {
                    controller.gamepad.buttonRT.wasPressedThisFrame = 1;
                }
                if (inGame)
                {
                    // if time between taps is low
                    // if time released on taps is low
                    //! If Double Taps
                    if (finger.lastTapped - finger.lastLastTapped <= 0.5 && elapsedTime - finger.lastTapped <= 0.5)
                    {
                        if (fingerStartPosition.x >= 0.6f)
                        {
                            if (fingerStartPosition.y >= 0.5f)
                            {
                                controller.gamepad.buttonRT.wasPressedThisFrame = 1;   // attack
                            }
                            else
                            {
                                controller.gamepad.buttonA.wasPressedThisFrame = 1;    // Jumping
                            }
                        }
                        else if (fingerStartPosition.x <= 0.4f)
                        {
                            controller.gamepad.buttonLT.wasPressedThisFrame = 1;   // talking
                        }
                        else
                        {
                            if (fingerStartPosition.y >= 0.7f)
                            {
                                controller.gamepad.buttonRB.wasPressedThisFrame = 1;   // switch action
                            }
                            else if (fingerStartPosition.y <= 0.3f)
                            {
                                controller.gamepad.buttonLB.wasPressedThisFrame = 1;   // switch action
                            }
                            else
                            {
                                controller.gamepad.startButton.wasPressedThisFrame = 1;   // pause
                            }
                        }
                    }
                }*/