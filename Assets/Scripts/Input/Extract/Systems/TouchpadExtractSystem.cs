using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using UnityEngine.InputSystem;

namespace Zoxel.Input
{
    //! Extracts touch data from unity and puts it into ecs.
    [UpdateAfter(typeof(ControllerResetSystem))]
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class TouchpadExtractSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var screenDimensions = new int2(UnityEngine.Screen.width, UnityEngine.Screen.height);
            Entities
                .WithNone<DestroyEntity>()
                .ForEach((ref Touchpad touchpad, in ZoxID zoxID) =>
            {
                var unityDevice = TouchUtil.GetTouchpad(zoxID.id);
                if (unityDevice == null)
                {
                    touchpad.Reset();
                }
                else
                {
                    touchpad.Update(in unityDevice, elapsedTime);
                }
            }).WithoutBurst().Run();
            Entities
                .WithNone<DestroyEntity>()
                .ForEach((ref Controller controller, in ConnectedDevices connectedDevices) =>
            {
                for (int i = 0; i < connectedDevices.devices.Length; i++)
                {
                    var deviceEntity = connectedDevices.devices[i];
                    if (HasComponent<Touchpad>(deviceEntity))
                    {
                        var touchpad = EntityManager.GetComponentData<Touchpad>(deviceEntity);
                        TouchUtil.ExtractTouchpad(ref controller, in touchpad, screenDimensions, elapsedTime);
                    }
                }
            }).WithoutBurst().Run();
        }
    }
}