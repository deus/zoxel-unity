using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using UnityEngine.InputSystem;

namespace Zoxel.Input
{
    //! Reset controller input before mapping to devices.
    [AlwaysUpdateSystem]
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class ControllerResetSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<DestroyEntity>()
                .WithAll<ConnectedDevices>()
                .ForEach((ref Controller controller) =>
            {
                if (controller.NoInput())
                {
                    controller.wasInputLastFrame = 0;
                }
                else
                {
                    controller.wasInputLastFrame = 1;
                }
                controller.Reset();
            }).ScheduleParallel();
        }
    }
}