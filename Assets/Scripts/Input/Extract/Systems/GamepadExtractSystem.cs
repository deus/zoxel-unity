using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using UnityEngine.InputSystem;

namespace Zoxel.Input
{
    //! Maps gamepad data from unity to ecs.
    [UpdateAfter(typeof(ControllerResetSystem))]
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class GamepadExtractSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithNone<DestroyEntity>()
                .ForEach((ref Gamepad gamepad, in ZoxID zoxID) =>
            {
                var gamepad2 = GamepadUtil.GetGamepad(zoxID.id);
                GamepadUtil.ExtractGamepad(ref gamepad, in gamepad2);
            }).WithoutBurst().Run();
            Entities
                .WithNone<DestroyEntity>()
                .ForEach((ref Controller controller, in ConnectedDevices connectedDevices) =>
            {
                for (int i = 0; i < connectedDevices.devices.Length; i++)
                {
                    var deviceEntity = connectedDevices.devices[i];
                    if (HasComponent<Gamepad>(deviceEntity))
                    {
                        var gamepad = EntityManager.GetComponentData<Gamepad>(deviceEntity);
                        GamepadUtil.ExtractGamepad(ref controller, in gamepad);
                    }
                }
            }).WithoutBurst().Run();
        }
    }
}