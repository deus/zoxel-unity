using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using UnityEngine.InputSystem;

namespace Zoxel.Input
{
    //! Maps mouse data from unity to ecs.
    [UpdateAfter(typeof(ControllerResetSystem))]
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class MouseExtractSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            //! \todo Make use player screenDimensions (with camera link)
            var screenDimensions = new int2(UnityEngine.Screen.width, UnityEngine.Screen.height);
            Entities
                .WithNone<DestroyEntity>()
                .ForEach((ref Mouse mouse, in ZoxID zoxID) =>
            {
                var mouse2 = MouseUtil.GetMouse(zoxID.id);
                MouseUtil.ExtractMouse(ref mouse, in mouse2, screenDimensions);
            }).WithoutBurst().Run();
            // can move this to a new parallel system
            Entities
                .WithNone<DestroyEntity>()
                .ForEach((ref Controller controller, in ConnectedDevices connectedDevices) =>
            {
                for (int i = 0; i < connectedDevices.devices.Length; i++)
                {
                    var deviceEntity = connectedDevices.devices[i];
                    if (HasComponent<Mouse>(deviceEntity))
                    {
                        var mouse = EntityManager.GetComponentData<Mouse>(deviceEntity);
                        MouseUtil.ExtractMouse(ref controller, in mouse);
                    }
                }
            }).WithoutBurst().Run();
        }
    }
}