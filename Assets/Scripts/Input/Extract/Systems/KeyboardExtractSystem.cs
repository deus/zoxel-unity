﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using UnityEngine.InputSystem;

namespace Zoxel.Input
{
    //! Maps keyboard data from unity to ecs.
    /**
    *   \todo Link Device directly with shared component link.
    *   \todo Rewrite Controller to not have copy of device datas. Possibly use a key binder layer in betwee.
    */
    [UpdateAfter(typeof(ControllerResetSystem))]
    [BurstCompile, UpdateInGroup(typeof(InputSystemGroup))]
    public partial class KeyboardExtractSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithNone<DestroyEntity>()
                .ForEach((ref Keyboard keyboard, in ZoxID zoxID) =>
            {
                var keyboard2 = KeyboardUtil.GetKeyboard(zoxID.id);
                KeyboardUtil.ExtractKeyboard(ref keyboard, in keyboard2);
            }).WithoutBurst().Run();
            // can move this to a new parallel system
            Entities
                .WithNone<DestroyEntity>()
                .ForEach((ref Controller controller, in ConnectedDevices connectedDevices) =>
            {
                for (int i = 0; i < connectedDevices.devices.Length; i++)
                {
                    var deviceEntity = connectedDevices.devices[i];
                    if (HasComponent<Keyboard>(deviceEntity))
                    {
                        var keyboard = EntityManager.GetComponentData<Keyboard>(deviceEntity);
                        KeyboardUtil.ExtractKeyboard(ref controller, in keyboard);
                    }
                }
            }).WithoutBurst().Run();
        }
    }
}

            /*Entities.ForEach((Entity e, ref Controller controller) =>
            {
                // using previous frames data, set if input was active
                if (controller.NoInput())
                {
                    controller.wasInputLastFrame = 0;
                }
                else
                {
                    controller.wasInputLastFrame = 1;
                }
                controller.Reset();
                // set new input
                if (deviceTypeData.type == DeviceType.Gamepad)
                {
                    var gamepad = InputUtil.GetGamepad(controller.deviceID);
                    if (gamepad != null)
                    {
                        InputUtil.ExtractGamepad(in gamepad, ref controller);
                    }
                    else
                    {
                        controller.OnDeviceDisconnected();
                    }
                }
                else if (deviceTypeData.type == DeviceType.Keyboard)
                {
                    var keyboard = InputUtil.GetKeyboard(controller.deviceID);
                    if (keyboard != null)
                    {
                        InputUtil.ExtractKeyboard(ref controller.keyboard, in keyboard);
                        InputUtil.ExtractKeyboard(ref controller, in controller.keyboard);
                    }
                    else
                    {
                        controller.OnDeviceDisconnected();
                    }
                    var mouse = UnityEngine.InputSystem.Mouse.current;
                    InputUtil.ExtractMouse(in mouse, ref controller);
                }
                else if (deviceTypeData.type == DeviceType.Touch)
                {
                    var found = false;
                    foreach (var touchscreen in InputSystem.devices)
                    {
                        if (controller.deviceID == touchscreen.deviceId)
                        {
                            InputUtil.ExtractTouchscreen(touchscreen as Touchscreen, ref controller, elapsedTime);
                            found = true;
                            break;
                        }
                    }
                    if (!found)
                    {
                        controller.OnDeviceDisconnected();
                    }
                }
            }).WithoutBurst().Run();*/