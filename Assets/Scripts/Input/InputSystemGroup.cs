﻿using Unity.Entities;
using UnityEngine.InputSystem.EnhancedTouch;
// [UpdateBefore(typeof(AISystemGroup))]

namespace Zoxel.Input
{
    //! The grouping of all input systems.
    public partial class InputSystemGroup : ComponentSystemGroup
    {
        public static Entity devicesHome;
        private static Entity deviceConnectedPrefab;
        private static Entity deviceDisconnectedPrefab;

        protected override void OnCreate()
        {
            base.OnCreate();
            var deviceConnectedArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(OnDeviceConnected),
                typeof(DeviceTypeData),
                typeof(ZoxID),
                typeof(DelayEvent),
                typeof(GenericEvent));
            deviceConnectedPrefab = EntityManager.CreateEntity(deviceConnectedArchetype);
            var deviceDisconnectedArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(OnDeviceDisconnected),
                typeof(ZoxID),
                typeof(DelayEvent),
                typeof(GenericEvent));
            deviceDisconnectedPrefab = EntityManager.CreateEntity(deviceDisconnectedArchetype);
            // UnityEngine.Debug.LogError("Created Devices: " + UnityEngine.InputSystem.InputSystem.devices.Count);
            EnhancedTouchSupport.Enable();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            EnhancedTouchSupport.Disable();
        }

        protected override void OnUpdate()
        {
            UnityEngine.InputSystem.InputSystem.Update();
            base.OnUpdate();
        }
        
        public static void SpawnDevices(EntityManager EntityManager)
        {
            var archetype = EntityManager.CreateArchetype(
                typeof(DevicesHome),
                typeof(DeviceLinks));
            InputSystemGroup.devicesHome = EntityManager.CreateEntity(archetype);
            #if UNITY_EDITOR
            EntityManager.AddComponentData(InputSystemGroup.devicesHome, new EditorName("Devices Home"));
            #endif
            if (deviceConnectedPrefab.Index == 0)
            {
                return;
            }
            // handle unity input system
            UnityEngine.InputSystem.InputSystem.Update();
            foreach (var inputDevice in UnityEngine.InputSystem.InputSystem.devices)
            {
                var device = inputDevice;
                var type = InputUtil.GetDeviceType(in device);
                UnityEngine.Debug.Log("Device added: " + device + " : " + type);
                var deviceConnectedEvent = EntityManager.Instantiate(deviceConnectedPrefab);
                EntityManager.SetComponentData(deviceConnectedEvent, new ZoxID(device.deviceId));
                EntityManager.SetComponentData(deviceConnectedEvent, new DeviceTypeData(type));
                EntityManager.SetComponentData(deviceConnectedEvent, new DelayEvent(0, 0.5));
            }
            UnityEngine.InputSystem.InputSystem.onDeviceChange += (device, change) =>
            {
                if (World.DefaultGameObjectInjectionWorld == null)
                {
                    UnityEngine.Debug.LogError("DefaultGameObjectInjectionWorld is null.");
                    return;
                }
                var EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
                switch (change)
                {
                    case UnityEngine.InputSystem.InputDeviceChange.Added:
                        var type = InputUtil.GetDeviceType(in device);
                        UnityEngine.Debug.Log("Device added: " + device + " : " + device.deviceId);
                        var deviceConnectedEvent = EntityManager.Instantiate(deviceConnectedPrefab);
                        EntityManager.SetComponentData(deviceConnectedEvent, new ZoxID(device.deviceId));
                        EntityManager.SetComponentData(deviceConnectedEvent, new DeviceTypeData(type));
                        break;
                    case UnityEngine.InputSystem.InputDeviceChange.Removed:
                        UnityEngine.Debug.Log("Device removed: " + device + " : " + device.deviceId);
                        var deviceDisconnectedEvent = EntityManager.Instantiate(deviceDisconnectedPrefab);
                        EntityManager.SetComponentData(deviceDisconnectedEvent, new ZoxID(device.deviceId));
                        break;
                    case UnityEngine.InputSystem.InputDeviceChange.ConfigurationChanged:
                        UnityEngine.Debug.Log("Device configuration changed: " + device);
                        break;
                }
            };
        }
    }
}