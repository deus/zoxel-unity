using Unity.Entities;
using Unity.Mathematics;
using System.Collections;
using UnityEngine;
using Zoxel.UI;

namespace Zoxel.Input.Debug
{
    //! Debug ui for touch input on mobile.
    public class TouchDebugUI : MonoBehaviour
    {
        public bool isSecondLine;
        public int fontSize = 22;
        public UnityEngine.Color fontColor = UnityEngine.Color.green;
        public UnityEngine.Font font;
        public UnityEngine.TextAnchor textAnchor;

        public EntityManager EntityManager
        {
            get
            {
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        void OnGUI()
        {
            if (font)
            {
                GUI.skin.font = font;
            }
            GUI.color = fontColor;
            GUI.skin.label.fontSize = fontSize;
            if (isSecondLine)
            {
                UnityEngine.GUILayout.Label("");
            }
            var devicesHomeEntity = InputSystemGroup.devicesHome;
            if (!EntityManager.HasComponent<DeviceLinks>(devicesHomeEntity))
            {
                return;
            }
            var deviceLinks = EntityManager.GetComponentData<DeviceLinks>(devicesHomeEntity);
            if (!deviceLinks.devices.IsCreated)
            {
                UnityEngine.GUILayout.Label("Devices: 0");
                return;
            }
            UnityEngine.GUILayout.Label("Devices: " + deviceLinks.devices.Count());
            if (deviceLinks.devices.IsCreated)
            {
                foreach (var KVP in deviceLinks.devices)
                {
                    var deviceEntity = KVP.Value;
                    if (!EntityManager.Exists(deviceEntity))
                    {
                        GUILayout.Label("   Input Device Invalid [" + deviceEntity.Index + "]");
                        continue;
                    }
                    if (!EntityManager.HasComponent<DeviceConnected>(deviceEntity))
                    {
                        continue;
                    }
                    if (!EntityManager.HasComponent<Touchpad>(deviceEntity))
                    {
                        continue;
                    }
                    var playerEntity = EntityManager.GetComponentData<DeviceConnected>(deviceEntity).player;
                    var controller = EntityManager.GetComponentData<Controller>(playerEntity);
                    GUILayout.Label("   controller mapping [" + controller.mapping + "]");
                    GUILayout.Label("       buttonRT [" + controller.gamepad.buttonRT + "]");
                    var touchpad = EntityManager.GetComponentData<Touchpad>(deviceEntity);
                    var navigator = EntityManager.GetComponentData<Navigator>(playerEntity);
                    var navigatorSelected = EntityManager.GetComponentData<NavigatorSelected>(playerEntity);
                    var mouseOverEntity = navigatorSelected.mouseOverEntity;
                    if (mouseOverEntity.Index > 0)
                    {
                        GUILayout.Label("       Mouse Over Entity [" + mouseOverEntity.Index + "]");
                        if (EntityManager.HasComponent<MouseDragEvent>(mouseOverEntity))
                        {
                            var mouseDragEvent = EntityManager.GetComponentData<MouseDragEvent>(mouseOverEntity);
                            GUILayout.Label("       Mouse Dragging: position: " + mouseDragEvent.position + ". delta: " + mouseDragEvent.delta);
                        }
                    }

                    GUILayout.Label("   touchpad touches [" + touchpad.touchesCount + "]");
                    if (touchpad.touchesCount > 0)
                    {
                        for (int i = 0; i < touchpad.fingers.Length; i++)
                        {
                            var primaryTouch = touchpad.fingers[i];
                            GUILayout.Label("       finger [" + i + "] id [" + primaryTouch.id + "]");
                            GUILayout.Label("       finger [" + i + "] state [" + primaryTouch.state + "]");
                            GUILayout.Label("       finger [" + i + "] startPosition [" + primaryTouch.startPosition + "]");
                            GUILayout.Label("       finger [" + i + "] position [" + primaryTouch.position + "]");
                            GUILayout.Label("       finger [" + i + "] delta [" + primaryTouch.delta + "]");
                        }
                    }
                }
            }
        }
    }
}