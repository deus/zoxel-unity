using UnityEngine;
using Unity.Entities;

namespace Zoxel.Input
{
    //! Debugs the raycasting with the UI
    public partial class DeviceDebugger : MonoBehaviour
    {
        public static DeviceDebugger instance;
        private int fontSize = 26;
        private UnityEngine.Color fontColor = UnityEngine.Color.green;
        public bool isDebugUI;

        void Awake()
        {
            instance = this;
        }

        public void OnGUI()
        {
            if (isDebugUI)
            {
                GUI.skin.label.fontSize = fontSize;
                GUI.color = fontColor;
                DebugUI();
            }
        }

        public EntityManager EntityManager
        {
            get
            { 
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        public void DebugUI()
        {
            var deviceLinks = EntityManager.GetComponentData<DeviceLinks>(InputSystemGroup.devicesHome);
            GUILayout.Label("Devices [" + deviceLinks.Length + "]");
            /*for (int i = 0; i < deviceLinks.devices.Length; i++)
            {
                var deviceEntity = deviceLinks.devices[i];*/
            if (deviceLinks.devices.IsCreated)
            {
                foreach (var KVP in deviceLinks.devices)
                {
                    var deviceEntity = KVP.Value;
                    if (!EntityManager.Exists(deviceEntity))
                    {
                        GUILayout.Label("   Input Device Invalid [" + deviceEntity.Index + "]");
                        continue;
                    }
                    DebugInputUI(deviceEntity);
                }
            }
        }

        private void DebugInputUI(Entity deviceEntity)
        {
            var inputID = EntityManager.GetComponentData<ZoxID>(deviceEntity).id;
            var type = EntityManager.GetComponentData<DeviceTypeData>(deviceEntity).type;
            var deviceConnected = EntityManager.HasComponent<DeviceConnected>(deviceEntity);
            var connectedLabel = "free";
            if (deviceConnected)
            {
                connectedLabel = "connected";
            }
            if (!deviceConnected)
            {
                var deviceConnecting = EntityManager.HasComponent<DeviceConnecting>(deviceEntity);
                if (deviceConnected)
                {
                    connectedLabel = "connecting";
                }
            }
            GUILayout.Label("   Input Device [" + inputID + "] of type [" + ((DeviceTypeEditor) type).ToString() + "] - [" + connectedLabel + "]");
        }
    }
}