using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.Lines
{
    //! Initializes RenderLine ZoxMesh.
    // [UpdateBefore(typeof(RenderLineTransformSystem))]
    [UpdateInGroup(typeof(RenderLineGroup))]
    public partial class RenderLineInitializationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null) return;
            var layermask = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var lineMesh = RenderLineGroup.lineMesh;
            var lineMaterial = MaterialsManager.instance.materials.lineMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, LineRender>()
                .ForEach((Entity e, in LineMaterialType lineMaterialType) =>
            {
                var lineMaterial = MaterialsManager.instance.materials.lineMaterial;
                if (lineMaterialType.type == 1)
                {
                    lineMaterial = MaterialsManager.instance.materials.lineMaterial2;
                }
                var zoxMesh = new ZoxMesh
                {
                    mesh = lineMesh,
                    material = new UnityEngine.Material(lineMaterial),
                };
                zoxMesh.layer = layermask;
                // Need a better way to know if UI
                /*if (lineStyle.material != zoxMesh.material)
                {
                    zoxMesh.layer = layermask;
                }*/
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
                #if UNITY_EDITOR
                if (!UnityEditor.EditorApplication.isPlaying && ZoxelEditorSettings.isEditorMeshes)
                {
                    PostUpdateCommands.AddComponent<InitializeEditorMesh>(e);
                    PostUpdateCommands.AddComponent<EditorMesh>(e);
                }
                #endif
            }).WithoutBurst().Run();
        }
    }
}
