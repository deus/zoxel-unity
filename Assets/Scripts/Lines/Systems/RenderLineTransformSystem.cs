using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Cameras;
using Unity.Burst.Intrinsics;

namespace Zoxel.Lines
{
    //! Makes lines face camera.
    /**
    *   \todo Make use camera rotation as well. (http://www.opengl-tutorial.org/intermediate-tutorials/billboards-particles/billboards/)
    *   \todo Better support for multiple camerasQuery. It would be via `alignWithCamera` on the LineStyle?
    *   \todo Position/Scale only need to update when placed, but not after, only rotation changes to match camera
    */
    [BurstCompile, UpdateInGroup(typeof(RenderLineGroup))]
    public partial class RenderLineTransformSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery renderLinesQuery;
        private EntityQuery camerasQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            renderLinesQuery = GetEntityQuery(
                ComponentType.ReadOnly<LineRender>(),
                ComponentType.ReadWrite<Translation>(), 
                ComponentType.ReadWrite<Rotation>(),
                ComponentType.ReadWrite<NonUniformScale>());
            camerasQuery = GetEntityQuery(
                ComponentType.ReadOnly<Camera>(),
                ComponentType.ReadOnly<LocalToWorld>(),
                ComponentType.ReadOnly<CameraEnabledState>());
            camerasQuery.SetChangedVersionFilter(typeof(LocalToWorld));
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (renderLinesQuery.IsEmpty || camerasQuery.IsEmpty)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var cameraEntities = camerasQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var cameraLocalToWorlds = GetComponentLookup<LocalToWorld>(true);
            var cameraEnabledStates = GetComponentLookup<CameraEnabledState>(true);
            var job = new LinePositioningJob
            {
                lastSystemVersion = LastSystemVersion,
                translationType = GetComponentTypeHandle<Translation>(false),
                rotationType = GetComponentTypeHandle<Rotation>(false),
                scaleType = GetComponentTypeHandle<NonUniformScale>(false),
                lineRenderHandle = GetComponentTypeHandle<LineRender>(true),
                // camerasChunk = camerasQuery.CreateArchetypeChunkArray(Allocator.TempJob),
                cameraEntities = cameraEntities,
                cameraLocalToWorlds = cameraLocalToWorlds,
                cameraEnabledStates = cameraEnabledStates
            };
            Dependency = job.ScheduleParallel(renderLinesQuery, Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        [BurstCompile]
        struct LinePositioningJob : IJobChunk
        {
            public uint lastSystemVersion;
            public ComponentTypeHandle<Translation> translationType;
            public ComponentTypeHandle<Rotation> rotationType;
            public ComponentTypeHandle<NonUniformScale> scaleType;
            [ReadOnly] public ComponentTypeHandle<LineRender> lineRenderHandle;
            // [ReadOnly, DeallocateOnJobCompletion] public NativeArray<ArchetypeChunk> camerasChunk;
            // [ReadOnly] public EntityTypeHandle cameraEntityHandle;
            [ReadOnly, DeallocateOnJobCompletion] public NativeArray<Entity> cameraEntities;
            [ReadOnly] public ComponentLookup<LocalToWorld> cameraLocalToWorlds;
            [ReadOnly] public ComponentLookup<CameraEnabledState> cameraEnabledStates;

            public void Execute(in ArchetypeChunk chunk, int unfilteredChunkIndex, bool useEnabledMask, in v128 chunkEnabledMask)
            // public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
            {
                // Do not commit to change if possible
                bool lineChunkChanged = chunk.DidChange(lineRenderHandle, lastSystemVersion);
                if (!lineChunkChanged)
                {
                    return;
                }
                // These gets will commit a version bump
                var lineRenders = chunk.GetNativeArray(lineRenderHandle);
                var transforms = chunk.GetNativeArray(translationType);
                var rotations = chunk.GetNativeArray(rotationType);
                var scales = chunk.GetNativeArray(scaleType);
                // var cameraEntities = camerasQuery[0].GetNativeArray();
                // var cameraEntities = chunk.GetNativeArray(cameraEntityHandle);
                // var cameraEnabledStates = chunk.GetNativeArray(cameraEnabledStatesHandle);
                var enabledCameraEntity = new Entity();
                for (int i = 0; i < cameraEntities.Length; i++)
                {
                    var cameraEntity = cameraEntities[i];
                    var cameraEnabledState = cameraEnabledStates[cameraEntity];
                    if (cameraEnabledState.state == 1)
                    {
                        enabledCameraEntity = cameraEntity;
                        break;
                    }
                }
                if (enabledCameraEntity.Index == 0)
                {
                    lineRenders.Dispose();
                    transforms.Dispose();
                    rotations.Dispose();
                    scales.Dispose();
                    return;
                }
                // var cameraLocalToWorlds = chunk.GetNativeArray(cameraLocalToWorldsHandle);
                var cameraLocalToWorld = cameraLocalToWorlds[enabledCameraEntity]; // camerasQuery[0].GetNativeArray(cameraLocalToWorlds);
                var cameraRigid = math.RigidTransform(cameraLocalToWorld.Value);
                var cameraTranslation = cameraRigid.pos;
                var cameraRotation = cameraRigid.rot;
                for (int i = 0; i < lineRenders.Length; i++)
                {
                    var lineRender = lineRenders[i];
                    if (lineRender.from.Equals(lineRender.to))
                    {
                        continue;
                    }
                    var forward = lineRender.to - lineRender.from;
                    // We will use length too so not using normalize here
                    var lineLength = math.length(forward);
                    var forwardUnit = forward / lineLength;
                    // Billboard rotation
                    var rotation = quaternion.identity;
                    var toCamera = math.normalize(cameraTranslation - lineRender.from);
                    // If forward and toCamera is collinear the cross product is 0 and it will gives quaternion with tons of NaN
                    // So we rather do nothing if that is the case
                    if (!(lineRender.from.Equals(cameraTranslation) || math.cross(forwardUnit, toCamera).Equals(new float3())))
                    {
                        //This is wrong because it only taken account of the camera's position, not also its rotation.
                        rotation = quaternion.LookRotation(forwardUnit, toCamera);
                    }
                    // var tran = transforms[i];
                    // var rot = rotations[i];
                    // var scale = scales[i];
                    transforms[i] = new Translation { Value = lineRender.from };
                    rotations[i] = new Rotation { Value = rotation };
                    scales[i] = new NonUniformScale {Value = math.float3(lineRender.lineWidth, 1, lineLength) };
                }
                lineRenders.Dispose();
                transforms.Dispose();
                rotations.Dispose();
                scales.Dispose();
                // cameraEntities.Dispose();
                // cameraEnabledStates.Dispose();
                // cameraLocalToWorlds.Dispose();
            }
        }
    }
}
                /*bool cameraMovedOrRotated = camerasChunk.Length != 0 && camerasChunk[0].DidChange(cameraLocalToWorlds, lastSystemVersion);
                if (!lineChunkChanged && !cameraMovedOrRotated)
                {
                    return;
                }*/