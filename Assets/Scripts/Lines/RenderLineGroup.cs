using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Transforms;
// [UpdateBefore(typeof(Zoxel.Transforms.TransformSystemGroup))]

namespace Zoxel.Lines
{
    //! The system group for RenderLine's.
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class RenderLineGroup : ComponentSystemGroup
    {
        public static Entity linePrefab;
        public static UnityEngine.Mesh lineMesh;
        const float lineDefaultWidth = 1f;
        const float lineDefaultWidthHalf = lineDefaultWidth / 2f;
        const string lineMeshName = "ECSLineMesh";
        private static Entity eternalLinePrefab;

        protected override void OnCreate()
        {
            base.OnCreate();
            CreateMesh();
            var lineArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(BasicRender),
                typeof(LineRender),
                typeof(LineMaterialType),
                typeof(MaterialBaseColor),
                // typeof(LineStyle),
                typeof(ZoxMesh),
                typeof(DontDestroyMesh),
                typeof(DontDestroyTexture),
                // typeof(DontDestroyMaterial),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld));
            linePrefab = EntityManager.CreateEntity(lineArchetype);
            EntityManager.AddComponent<DestroyEntityInTime>(linePrefab);
            eternalLinePrefab = EntityManager.CreateEntity(lineArchetype);
        }

        public static void SpawnLine(EntityCommandBuffer PostUpdateCommands, Entity prefab, double elapsedTime,
            float3 beginPosition, float3 endPosition, Color color, float lineWidth = 0.025f, float lineLife = 0.3f)
        {
            var lineEntity = PostUpdateCommands.Instantiate(prefab);
            PostUpdateCommands.SetComponent(lineEntity, new LineRender(beginPosition, endPosition, lineWidth));
            PostUpdateCommands.SetComponent(lineEntity, new MaterialBaseColor(color.ToFloat4()));
            PostUpdateCommands.SetComponent(lineEntity, new DestroyEntityInTime(elapsedTime, lineLife));
        }

        public static void SpawnLine(EntityManager EntityManager, double elapsedTime,
            float3 beginPosition, float3 endPosition,
            byte materialType = 0, float lineWidth = 0.025f, float lineLife = 0.3f)
        {
            /*var lineMaterial = MaterialsManager.instance.materials.lineMaterial;
            if (materialType == 1)
            {
                lineMaterial = MaterialsManager.instance.materials.lineMaterial2;
            }*/
            var prefab = linePrefab;
            if (lineLife == 0)
            {
                prefab = eternalLinePrefab;
            }
            var lineEntity = EntityManager.Instantiate(prefab);
            EntityManager.SetComponentData(lineEntity, new LineRender(beginPosition, endPosition, lineWidth));
            // EntityManager.SetSharedComponentData(lineEntity, new LineStyle { material = lineMaterial });
            EntityManager.SetComponentData(lineEntity, new LineMaterialType(materialType));
            if (lineLife != 0)
            {
                EntityManager.SetComponentData(lineEntity, new DestroyEntityInTime(elapsedTime, lineLife));
            }
        }

        public static void SpawnLine(EntityCommandBuffer PostUpdateCommands, Entity prefab, double elapsedTime,
            float3 beginPosition, float3 endPosition, byte materialType = 0,
            float lineWidth = 0.025f, float lineLife = 0.3f)
        {
            /*var lineMaterial = MaterialsManager.instance.materials.lineMaterial;
            if (materialType == 1)
            {
                lineMaterial = MaterialsManager.instance.materials.lineMaterial2;
            }*/
            var lineEntity = PostUpdateCommands.Instantiate(prefab); // linePrefab);
            PostUpdateCommands.SetComponent(lineEntity, new DestroyEntityInTime(elapsedTime, lineLife));
            PostUpdateCommands.SetComponent(lineEntity, new LineRender(beginPosition, endPosition, lineWidth));
            PostUpdateCommands.SetComponent(lineEntity, new LineMaterialType(materialType));
            // PostUpdateCommands.SetSharedComponentManaged(lineEntity, new LineStyle { material = lineMaterial });
        }

        public static void CreateCubeLines(EntityCommandBuffer PostUpdateCommands, Entity prefab, double elapsedTime,
            float3 previewMeshPosition, float previewMeshSize, byte materialType = 0,
            float lineWidth = 0.025f, float lineLife = 0.3f)
        {
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, previewMeshPosition + new float3(-previewMeshSize, previewMeshSize, previewMeshSize), 
                previewMeshPosition + new float3(previewMeshSize, previewMeshSize, previewMeshSize), materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, previewMeshPosition + new float3(-previewMeshSize, previewMeshSize, -previewMeshSize),
                previewMeshPosition + new float3(previewMeshSize, previewMeshSize, -previewMeshSize), materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, previewMeshPosition + new float3(-previewMeshSize, previewMeshSize, -previewMeshSize), 
                previewMeshPosition + new float3(-previewMeshSize, previewMeshSize, previewMeshSize), materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, previewMeshPosition + new float3(previewMeshSize, previewMeshSize, -previewMeshSize), 
                previewMeshPosition + new float3(previewMeshSize, previewMeshSize, previewMeshSize), materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, previewMeshPosition + new float3(-previewMeshSize, -previewMeshSize, previewMeshSize), 
                previewMeshPosition + new float3(previewMeshSize, -previewMeshSize, previewMeshSize), materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, previewMeshPosition + new float3(-previewMeshSize, -previewMeshSize, -previewMeshSize),
                previewMeshPosition + new float3(previewMeshSize, -previewMeshSize, -previewMeshSize), materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, previewMeshPosition + new float3(-previewMeshSize, -previewMeshSize, -previewMeshSize), 
                previewMeshPosition + new float3(-previewMeshSize, -previewMeshSize, previewMeshSize), materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, previewMeshPosition + new float3(previewMeshSize, -previewMeshSize, -previewMeshSize), 
                previewMeshPosition + new float3(previewMeshSize, -previewMeshSize, previewMeshSize), materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, previewMeshPosition + new float3(-previewMeshSize, -previewMeshSize, previewMeshSize), 
                previewMeshPosition + new float3(-previewMeshSize, previewMeshSize, previewMeshSize), materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, previewMeshPosition + new float3(-previewMeshSize, -previewMeshSize, -previewMeshSize),
                previewMeshPosition + new float3(-previewMeshSize, previewMeshSize, -previewMeshSize), materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, previewMeshPosition + new float3(previewMeshSize, -previewMeshSize, previewMeshSize), 
                previewMeshPosition + new float3(previewMeshSize, previewMeshSize, previewMeshSize), materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, previewMeshPosition + new float3(previewMeshSize, -previewMeshSize, -previewMeshSize), 
                previewMeshPosition + new float3(previewMeshSize, previewMeshSize, -previewMeshSize), materialType, lineWidth, lineLife);
        }

        public static void CreateCubeLines(EntityCommandBuffer PostUpdateCommands, Entity prefab, double elapsedTime,
            float3 position, quaternion rotation, float3 size, byte materialType = 0,
            float lineWidth = 0.025f, float lineLife = 0.3f)
        {
            var pointA = new float3(-size.x, size.y, size.z);
            var pointB = new float3(size.x, size.y, size.z);
            var pointC = new float3(-size.x, size.y, -size.z);
            var pointD = new float3(size.x, -size.y, size.z);
            var pointE = new float3(size.x, size.y, -size.z);
            var pointF = new float3(-size.x, -size.y, size.z);
            var pointG = new float3(-size.x, -size.y, -size.z);
            var pointH = new float3(size.x, -size.y, -size.z);
            // Rotate cube points
            pointA = math.mul(rotation, pointA);
            pointB = math.mul(rotation, pointB);
            pointC = math.mul(rotation, pointC);
            pointD = math.mul(rotation, pointD);
            pointE = math.mul(rotation, pointE);
            pointF = math.mul(rotation, pointF);
            pointG = math.mul(rotation, pointG);
            pointH = math.mul(rotation, pointH);
            pointA += position;
            pointB += position;
            pointC += position;
            pointD += position;
            pointE += position;
            pointF += position;
            pointG += position;
            pointH += position;
            // Create Cube Lines
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, pointA, pointB, materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, pointC, pointE, materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, pointC, pointA, materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, pointE, pointB, materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, pointF, pointD, materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, pointG, pointH, materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, pointG, pointF, materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, pointH, pointD, materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, pointF, pointA, materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, pointG, pointC, materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, pointD, pointB, materialType, lineWidth, lineLife);
            RenderLineGroup.SpawnLine(PostUpdateCommands, prefab, elapsedTime, pointH, pointE, materialType, lineWidth, lineLife);
        }

        private void CreateMesh()
        {
            if (lineMesh == null)
            {
                var mesh = new UnityEngine.Mesh();
                mesh.name = lineMeshName;
                var vertices = new UnityEngine.Vector3[4];
                vertices[0] = new UnityEngine.Vector3(-lineDefaultWidthHalf, 0, 0);
                vertices[1] = new UnityEngine.Vector3(lineDefaultWidthHalf, 0, 0);
                vertices[2] = new UnityEngine.Vector3(-lineDefaultWidthHalf, 0, 1);
                vertices[3] = new UnityEngine.Vector3(lineDefaultWidthHalf, 0, 1);
                mesh.vertices = vertices;
                var tri = new int[6];
                tri[0] = 0;
                tri[1] = 2;
                tri[2] = 1;
                tri[3] = 2;
                tri[4] = 3;
                tri[5] = 1;
                mesh.triangles = tri;
                var normals = new UnityEngine.Vector3[4];
                normals[0] = -UnityEngine.Vector3.forward;
                normals[1] = -UnityEngine.Vector3.forward;
                normals[2] = -UnityEngine.Vector3.forward;
                normals[3] = -UnityEngine.Vector3.forward;
                mesh.normals = normals;
                var uv = new UnityEngine.Vector2[4];
                uv[0] = new UnityEngine.Vector2(0, 0);
                uv[1] = new UnityEngine.Vector2(1, 0);
                uv[2] = new UnityEngine.Vector2(0, 1);
                uv[3] = new UnityEngine.Vector2(1, 1);
                mesh.uv = uv;
                lineMesh = mesh;
            }
        }
    }
}