﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Lines
{
    public struct LineRender : IComponentData
    {
        public float3 from;
        public float3 to;
        public float lineWidth;

        public LineRender(float3 from, float3 to, float lineWidth = 0.03f)
        {
            this.from = from;
            this.to = to;
            this.lineWidth = lineWidth;
        }
    }
}
