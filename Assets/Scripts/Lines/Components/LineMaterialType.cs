using Unity.Entities;

namespace Zoxel.Lines
{
    public struct LineMaterialType : IComponentData
    {
        public byte type;

        public LineMaterialType(byte type)
        {
            this.type = type;
        }
    }
}
