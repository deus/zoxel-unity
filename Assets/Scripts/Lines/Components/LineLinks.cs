using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Lines
{
    //! Meta data for lines now generated per game.
    public struct LinkLinks : IComponentData
    {
        public BlitableArray<Entity> lines;

        public void Dispose()
        {
            lines.Dispose();
        }

        public void Initialize(int count)
        {
            Dispose();
            this.lines = new BlitableArray<Entity>(count, Allocator.Persistent);
        }
    }
}