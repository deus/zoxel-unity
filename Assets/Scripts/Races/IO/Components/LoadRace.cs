using Unity.Entities;

namespace Zoxel.Races
{
    public struct LoadRace : IComponentData
    {
        public Text loadPath;
        public AsyncFileReader reader;

        public void Dispose()
        {
            loadPath.Dispose();
            reader.Dispose();
        }
    }
}