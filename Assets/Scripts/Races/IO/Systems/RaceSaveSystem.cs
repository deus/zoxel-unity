using Unity.Entities;
using Unity.Collections;
using System;
using System.IO;

namespace Zoxel.Races
{
    //! Saves RaceID of entity RaceLink.
    /**
    *   - Save System -
    */
    [UpdateInGroup(typeof(RaceSystemGroup))]
    public partial class RaceSaveSystem : SystemBase
    {
        private const string filename = "Race.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            Entities
                .WithNone<DeadEntity, DestroyEntity, InitializeSaver>()
                .WithAll<SaveRace>()
                .ForEach((Entity e, in EntitySaver entitySaver, in ZoxID zoxID, in RaceLink raceLink) =>
            {
                if (raceLink.race.Index == 0)
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<SaveRace>(e);
                var directoryPath = entitySaver.GetPath();
                if (!Directory.Exists(directoryPath))
                {
                    UnityEngine.Debug.LogError("Save Directory did not exist: " + directoryPath);
                    return;
                }
                var filepath = directoryPath + filename;
                try
                {
                    var raceID = EntityManager.GetComponentData<ZoxID>(raceLink.race).id;
                    var bytes = new NativeList<byte>();
                    ByteUtil.AddInt(ref bytes, raceID);
                    var bytes2 = bytes.AsArray().ToArray();
                    #if WRITE_ASYNC
                    File.WriteAllBytesAsync(filepath, bytes2);
                    #else
                    File.WriteAllBytes(filepath, bytes2);
                    #endif
                    bytes.Dispose();
                }
                catch (Exception exception)
                {
                    UnityEngine.Debug.LogError("Exceptiong saving Race: " + exception);
                }
            }).WithoutBurst().Run();
        }
    }
}