using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Races
{
    //! Loads race data.
    /**
    *   - Load System -
    */
    [BurstCompile, UpdateInGroup(typeof(RaceSystemGroup))]
    public partial class RaceLoadSystem : SystemBase
    {
        private const string filename = "Race.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<RaceLinks>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithAll<RealmLink>()
                .ForEach((ref LoadRace loadRace, ref RaceLink raceLink, in EntitySaver entitySaver) =>
            {
                if (loadRace.loadPath.Length == 0)
                {
                    loadRace.loadPath = new Text(entitySaver.GetPath() + filename);
                }
                loadRace.reader.UpdateOnMainThread(in loadRace.loadPath);
            }).WithoutBurst().Run();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var raceLinks = GetComponentLookup<RaceLinks>(true);
            realmEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity, InitializeSaver>()
                .WithAll<EntitySaver>()
                .ForEach((Entity e, int entityInQueryIndex, ref LoadRace loadRace, ref RaceLink raceLink, in RealmLink realmLink) =>
            {
                if (loadRace.reader.IsReadFileInfoComplete())
                {
                    if (loadRace.reader.DoesFileExist())
                    {
                        loadRace.reader.state = AsyncReadState.StartOpenFile;
                    }
                    else
                    {
                        // UnityEngine.Debug.LogError("File Did not exist.");
                        loadRace.Dispose();
                        PostUpdateCommands.RemoveComponent<LoadRace>(entityInQueryIndex, e);
                    }
                }
                else if (loadRace.reader.IsReadFileComplete())
                {
                    if (loadRace.reader.DidFileReadSuccessfully())
                    {
                        var bytes = loadRace.reader.GetBytes();
                        var raceLinks2 = raceLinks[realmLink.realm];
                        var raceID = ByteUtil.GetInt(bytes, 0);
                        raceLink.race = raceLinks2.GetRace(raceID);
                    }
                    // Dispose
                    loadRace.Dispose();
                    PostUpdateCommands.RemoveComponent<LoadRace>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(raceLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}