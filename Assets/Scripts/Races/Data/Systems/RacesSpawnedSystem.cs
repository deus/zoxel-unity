using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Races
{
    //! Links user races to raceLinks user after they spawn.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(RaceSystemGroup))]
    public partial class RacesSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery racesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            racesQuery = GetEntityQuery(ComponentType.ReadOnly<Race>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var raceEntities = racesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var creatorLinks = GetComponentLookup<CreatorLink>(true);
            var dataIndexes = GetComponentLookup<UserDataIndex>(true);
            var zoxIDs = GetComponentLookup<ZoxID>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref RaceLinks raceLinks, ref OnRacesSpawned onRacesSpawned) =>
            {
                if (onRacesSpawned.spawned != 255)
                {
                    raceLinks.Initialize(onRacesSpawned.spawned);
                    onRacesSpawned.spawned = 255;
                }
                byte count = 0;
                for (int i = 0; i < raceEntities.Length; i++)
                {
                    var e2 = raceEntities[i];
                    var creator = creatorLinks[e2].creator;
                    if (creator == e)
                    {
                        var index = dataIndexes[e2].index;
                        raceLinks.races[index] = e2;
                        var raceID = zoxIDs[e2].id;
                        raceLinks.racesHash[raceID] = e2;
                        count++;
                        if (count == raceLinks.races.Length)
                        {
                            PostUpdateCommands.RemoveComponent<OnRacesSpawned>(entityInQueryIndex, e);
                            break;
                        }
                    }
                }
            })  .WithReadOnly(raceEntities)
                .WithDisposeOnCompletion(raceEntities)
                .WithReadOnly(creatorLinks)
                .WithReadOnly(dataIndexes)
                .WithReadOnly(zoxIDs)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}