using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Races
{
    //! Generates the races of the realm.
    /**
    *   - Data Generation System -
    */
    [BurstCompile, UpdateInGroup(typeof(RaceSystemGroup))]
    public partial class RacesGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity racePrefab;
        private NativeArray<Text> texts;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var raceArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(Race),
                typeof(ZoxID),
                typeof(ZoxName),
                typeof(ZoxDescription),
                typeof(UserDataIndex),
                typeof(CreatorLink));
            racePrefab = EntityManager.CreateEntity(raceArchetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(racePrefab); // EntityManager.AddComponentData(racePrefab, new EditorName("[race]"));
            #endif
            texts = new NativeArray<Text>(15, Allocator.Persistent);
            texts[0] = new Text("Glub");        // humanoid
            texts[1] = new Text("Slem");        // mini slime
            texts[2] = new Text("Zork");        // medium slime
            texts[3] = new Text("Borknub");    // Large Slime
            texts[4] = new Text("Human");
            texts[5] = new Text("The most boring of humanoid races.");
            texts[6] = new Text("Elf");
            texts[7] = new Text("The most long lived of races.");
            texts[8] = new Text("Orc");
            texts[9] = new Text("The most war hungry of races.");
            texts[10] = new Text("Undead");
            texts[11] = new Text("The most flesh hungry of races.");
            texts[12] = new Text("Goblin");
            texts[13] = new Text("The scum of the earth.");
            texts[14] = new Text("[race] ");
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var texts = this.texts;
            var racePrefab = this.racePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<OnRacesSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref GenerateRealm generateRealm, ref RaceLinks raceLinks, in ZoxID zoxID) =>
            {
                if (generateRealm.state == (byte) GenerateRealmState.GenerateRaces2)
                {
                    generateRealm.state = (byte) GenerateRealmState.GenerateQuests;
                    return;
                }
                if (generateRealm.state != (byte) GenerateRealmState.GenerateRaces)
                {
                    return;
                }
                var realmSeed = zoxID.id;
                if (realmSeed == 0)
                {
                    return;
                }
                generateRealm.state = (byte) GenerateRealmState.GenerateRaces2;
                // Color.TestHSVConversion();
                // Destroy Races
                for (int i = 0; i < raceLinks.races.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, raceLinks.races[i]);
                }
                raceLinks.Dispose();
                // Spawn Skill Entities
                var racesCount = (byte) 9;
                var creatorLink = new CreatorLink(e);
                // raceLinks.Initialize(racesCount);
                for (byte i = 0; i < racesCount; i++)
                {
                    var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, racePrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, creatorLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new UserDataIndex(i));
                    var raceID = realmSeed + 1000 + (i + 1);
                    var random = new Random();
                    random.InitState((uint) raceID);
                    // BoneLinks: Humanoid, Slime, Golem (without legs), Dog(4 legs)
                    // Rough Sizes - limits
                    // Colour limits and base
                    // Variants?
                    // Male Female Differences
                    var raceName = new Text();
                    var description = new Text();
                    var race = new Race();
                    if (i == 0)
                    {
                        raceName = texts[0]; // new Text("Glub");    // humanoid
                    }
                    else if (i == 1)
                    {
                        raceName = texts[1]; // new Text("Slem");    // mini slime
                    }
                    else if (i == 2)
                    {
                        raceName = texts[2]; // new Text("Zork");    // medium slime
                    }
                    else if (i == 3)
                    {
                        raceName = texts[3]; // new Text("Borknub");    // Large Slime
                    }
                    else
                    {
                        PostUpdateCommands.AddComponent<HumanoidRace>(entityInQueryIndex, e2);
                        // Generate constraints per race!
                        // if height is between 1 and 2, make a player race

                        // 4 + random.NextInt(8);
                        if (i == 4)
                        {
                            raceName = texts[4];
                            description = texts[5];
                            // texts[1] = new Text("Human");
                            // texts[1] = new Text("The most boring of humanoid races.");
                            race.hsvBase = new float3(random.NextInt(30, 60), 32, 46); // 46
                            race.hsvVariance = new float3(20, 5, 4);
                            race.heightBase = 1.44f;
                            race.heightVariance = 0.1f;
                            race.chestDimensions = new int2(8, 16);  // 14-20
                            race.shoulderDimensions = new int2(3, 7);
                            race.shoulderLength = new int2(6, 8);
                            race.bicepDimensions = new int2(2, 4);
                            race.bicepLength = new int2(5, 10);
                            race.forearmLength = new int2(5, 10);
                            race.hipsLength = new int2(7, 9);
                            PostUpdateCommands.AddComponent<PlayerChoice>(entityInQueryIndex, e2);
                        }
                        else if (i == 5)
                        {
                            //texts[1] = new Text("Elf");
                            //texts[1] = new Text("The most long lived of races.");
                            raceName = texts[6];
                            description = texts[7];
                            race.hsvBase = new float3(random.NextInt(160, 200), 22, 54); // 184
                            race.hsvVariance = new float3(12, 5, 6);
                            race.heightBase = 1.8f;
                            race.heightVariance = 0.08f;
                            race.chestDimensions = new int2(10, 11);
                            race.shoulderDimensions = new int2(2, 4);
                            race.shoulderLength = new int2(6, 6);
                            race.bicepDimensions = new int2(1, 3);
                            race.bicepLength = new int2(8, 14);
                            race.forearmLength = new int2(8, 14);
                            race.hipsLength = new int2(7, 9);
                            PostUpdateCommands.AddComponent<PlayerChoice>(entityInQueryIndex, e2);
                        }
                        else if (i == 6)
                        {
                            //texts[1] = new Text("Orc");
                            //texts[1] = new Text("The most war hungry of races.");
                            raceName = texts[8];
                            description = texts[9];
                            race.hsvBase = new float3(random.NextInt(90, 130), 44, 38); // 112
                            race.hsvVariance = new float3(42, 12, 4);
                            race.heightBase = 1.66f;
                            race.heightVariance = 0.12f;
                            race.chestDimensions = new int2(16, 20);  // 14-20
                            race.shoulderDimensions = new int2(6, 10);
                            race.shoulderLength = new int2(6, 10);
                            race.bicepDimensions = new int2(4, 8);
                            race.bicepLength = new int2(6, 12);
                            race.forearmLength = new int2(6, 12);
                            race.hipsLength = new int2(8, 10);
                            PostUpdateCommands.AddComponent<PlayerChoice>(entityInQueryIndex, e2);
                        }
                        else if (i == 7)
                        {
                            //texts[1] = new Text("Undead");
                            //texts[1] = new Text("The most flesh hungry of races.");
                            raceName = texts[10];
                            description = texts[11];
                            race.hsvBase = new float3(random.NextInt(10, 70), 0, 32);   // 44
                            race.hsvVariance = new float3(24, 4, 8);
                            race.heightBase = 1.24f;
                            race.heightVariance = 0.16f;
                            race.chestDimensions = new int2(10, 14);  // 14-20
                            race.shoulderDimensions = new int2(4, 7);
                            race.shoulderLength = new int2(6, 9);
                            race.bicepDimensions = new int2(2, 4);
                            race.bicepLength = new int2(4, 8);
                            race.forearmLength = new int2(4, 8);
                            race.hipsLength = new int2(7, 9);
                            PostUpdateCommands.AddComponent<PlayerChoice>(entityInQueryIndex, e2);
                        }
                        else // if (i == 8)
                        {
                            //texts[1] = new Text("Goblin");
                            //texts[1] = new Text("The scum of the earth.");
                            raceName = texts[12];
                            description = texts[13];
                            race.hsvBase = new float3(random.NextInt(90, 130), 32, 32); // 112
                            race.hsvVariance = new float3(42, 12, 4);
                            race.heightBase = 0.7f;
                            race.heightVariance = 0.1f;
                            race.chestDimensions = new int2(8, 10);     // 14-20
                            race.shoulderDimensions = new int2(3, 6);
                            race.shoulderLength = new int2(4, 6);
                            race.bicepDimensions = new int2(1, 4);
                            race.bicepLength = new int2(2, 4);
                            race.forearmLength = new int2(2, 4);
                            race.hipsLength = new int2(5, 7);       // 14
                            PostUpdateCommands.AddComponent<PlayerChoice>(entityInQueryIndex, e2);
                        }
                        var legLength = 14;
                        race.chestLength = new int2(10, 14);
                        race.thighLength = new int2(legLength - 2, legLength + 2);
                        race.thighDimensions = new int2(1, 3);
                        race.calfLength = new int2(legLength - 2, legLength + 2);
                        race.calfDimensions = new int2(1, 3);
                        race.hipsDimensions = new int2(4, 6);
                        if (i == 4)
                        {
                            race.thighLength += new int2(3, 3);
                            race.calfLength += new int2(2, 2);
                        }
                        else if (i == 5)
                        {
                            race.thighLength += new int2(4, 4);
                            race.calfLength += new int2(4, 4);
                        }
                        else if (i == 6)
                        {
                            race.chestLength += new int2(2, 2);
                            race.thighLength += new int2(6, 6);
                            race.calfLength += new int2(4, 4);
                            race.thighDimensions += new int2(1, 1);
                        }
                        else if (i == 7)
                        {
                            race.thighLength -= new int2(4, 4);
                            race.calfLength -= new int2(4, 4);
                        }
                        else if (i == 8)
                        {
                            race.chestLength = new int2(6, 10);
                            race.thighLength = new int2(3, 6);
                            race.calfLength = new int2(3, 6);
                            race.hipsDimensions = new int2(1, 3);
                            race.thighDimensions = new int2(1, 3);
                            race.calfDimensions = new int2(2, 4);
                        }
                    }
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ZoxID(raceID));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ZoxName(raceName.Clone()));
                    #if UNITY_EDITOR
                    var editorName = texts[14].Clone();
                    editorName.AddText(raceName);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new EditorName(editorName));
                    #endif
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ZoxDescription(description.Clone()));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, race);
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnRacesSpawned(racesCount));
            })  .WithReadOnly(texts)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}