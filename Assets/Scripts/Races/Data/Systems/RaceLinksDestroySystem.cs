using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Races
{
    //! Cleans up RaceLinks on DestroyEntity.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class RaceLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithAll<DestroyEntity, RaceLinks>()
				.ForEach((int entityInQueryIndex, in RaceLinks raceLinks) =>
			{
                for (int i = 0; i < raceLinks.races.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, raceLinks.races[i]);
                }
                raceLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}