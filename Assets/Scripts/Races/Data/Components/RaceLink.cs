using Unity.Entities;
// todo: when saving, save ID, link to games data

namespace Zoxel.Races
{
    public struct RaceLink : IComponentData
    {
        public Entity race;

        public RaceLink(Entity race)
        {
            this.race = race;
        }
    }
}

// Save the raceID, and then also link it to Realm's Race Data
// make sure to save race ID as well when creating character