using Unity.Entities;

namespace Zoxel.Races
{
    //! An event for linking up Races after spawning.
    public struct OnRacesSpawned : IComponentData
    {
        public byte spawned;

        public OnRacesSpawned(byte spawned)
        {
            this.spawned = spawned;
        }
    }
}