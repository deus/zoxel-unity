using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Races
{
    //! Some basic race data.
    /**
    *   \todo Support for different races instead of just bibedal races.
    */
    public struct Race : IComponentData
    {
        public float3 hsvBase;
        public float3 hsvVariance;
        public float heightBase;
        public float heightVariance;
        public int2 chestDimensions;
        public int2 chestLength;
        public int2 shoulderDimensions;
        public int2 shoulderLength;
        public int2 bicepDimensions;
        public int2 bicepLength;
        public int2 forearmLength;
        public int2 hipsDimensions;
        public int2 hipsLength;
        public int2 thighDimensions;
        public int2 thighLength;
        public int2 calfDimensions;
        public int2 calfLength;
    }
}