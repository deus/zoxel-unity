using Unity.Entities;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Zoxel.Races
{
    //! Links to Race entities.
    public struct RaceLinks : IComponentData
    {
        public BlitableArray<Entity> races;
        public UnsafeParallelHashMap<int, Entity> racesHash;

        public void Dispose()
        {
            if (races.Length > 0)
            {
                races.Dispose();
            }
			if (racesHash.IsCreated)
			{
				racesHash.Dispose();
			}
        }

        public void Initialize(int count)
        {
            this.Dispose();
            this.races = new BlitableArray<Entity>(count, Allocator.Persistent);
            this.racesHash = new UnsafeParallelHashMap<int, Entity>(count, Allocator.Persistent);
        }

        public Entity GetRace(int targetID)
        {
            if (racesHash.IsCreated)
            {
                Entity outputEntity;
                if (racesHash.TryGetValue(targetID, out outputEntity))
                {
                    return outputEntity;
                }
            }
            return new Entity();
        }
    }
}