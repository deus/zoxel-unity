using Unity.Entities;

namespace Zoxel.Races
{
    //! A tag for humanoid race types.
    public struct HumanoidRace : IComponentData { }
}