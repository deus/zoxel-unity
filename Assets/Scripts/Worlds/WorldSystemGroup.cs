﻿using Unity.Entities;

namespace Zoxel.Worlds
{
    //! Holds world generation systems.
    /**
    *   Worlds is the generation for the games content
    *   Solar System -> Planet -> MegaChunk -> Chunk & ChunkBiome & Towns
    *   \todo Fix MegaChunks
    *   \todo Solar Systems
    */
    [AlwaysUpdateSystem]
    public partial class WorldSystemGroup : ComponentSystemGroup
    {
        public static Entity spawnPlanetPrefab;

        protected override void OnCreate()
        {
            base.OnCreate();
            var spawnPlanetArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnPlanet));
            spawnPlanetPrefab = EntityManager.CreateEntity(spawnPlanetArchetype);
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();
        }
    }
}


// === Character Reworks ===
// todo: spawn directly in here instead
// todo: give mintman a different behaviour - he will use path finding - and water around - then wait for 20 seconds and repeat
// todo: npcs save/loading from chunks
// todo: slimes to stick around their spawn point
// todo: reset chunkCharacterLinks when they move into a new chunk

// === Enhance ===
// todo: outline shader instead of weird box thing
// todo: name labels above npc
// todo: emoticon ! above npc with quest
// todo: emoticon $ above npc with shop
// todo: npc to attack blocks - and builld a house
// todo: slimes to have baby slimes
// todo: spawn characters over entire world - and just change their simulation level when they 'load'
//          lvl 0: abstract position - in town of Xelgore
//          lvl 1: abstract position - abstract userStatLinks (good health, minor wound)
//          lvl 2: abstract position - abstract userStatLinks - abstract inventory (low food, medium coins, etc)
//          lvl 3: quadtree position (lowest division) - userStatLinks - inventory
//          lvl 4: exact position - userStatLinks - inventory - quests