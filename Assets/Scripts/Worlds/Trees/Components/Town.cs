using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Worlds
{
    public struct Town
    {
        public int3 position;
        public int3 dimensions;
        public int wallThickness;
        public byte gateSide;
        public int gateThickness;
        public BlitableArray<Building> buildings;
        public BlitableArray<Road> roads;
        public BlitableArray<Road> gates;
        public BlitableArray<byte> planet;
        [ReadOnly] public Biome biome;

        public void Dispose()
        {
            if (buildings.Length > 0)
            {
                buildings.Dispose();
            }
            if (roads.Length > 0)
            {
                roads.Dispose();
            }
            if (planet.Length > 0)
            {
                planet.Dispose();
            }
        }
    }
}
