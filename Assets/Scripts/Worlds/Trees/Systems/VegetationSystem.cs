using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! Spawns a tree or weed on top of grass blocks.
    /**
    *   Uses 2-3ms now on threads per frame. If using SetOrAdd voxelRotations -> multiplies time by 4 (11ms)
    *   \todo transform planeted voxel into dirt in other chunks - make it part of tree growing - changes voxel under first wood to dirt
    */
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class VegetationSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery worldsQuery;
        private EntityQuery biomesQuery;
        private Entity treePrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            worldsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<BiomeLinks>());
            biomesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Biome>(),
                ComponentType.Exclude<DestroyEntity>());
            var treeArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
				typeof(Tree),
				typeof(Structure),
				typeof(ChunkLink),
				typeof(VoxLink),
				typeof(Translation)
            );
            treePrefab = EntityManager.CreateEntity(treeArchetype);
            RequireForUpdate(processQuery);
		}
		
		[BurstCompile]
        protected override void OnUpdate()
        {
            var halfOneOffset = new float3(0.5f, 0.5f, 0.5f);
			var weedNoiseScale = 0.0966f;
			// const int treeSpawnChance = 6;
			var disableVegetation = WorldsManager.instance.worldsSettings.disableVegetation;
			var disableWeeds = WorldsManager.instance.worldsSettings.disableWeeds;
			var planetHeightRadius = WorldsManager.instance.worldsSettings.planetHeightRadius;
			var treeSpawnChance = WorldsManager.instance.worldsSettings.treeSpawnChance;
			var voxGrassChance = WorldsManager.instance.worldsSettings.voxGrassChance;
			var edgeBlocksCount = planetHeightRadius;
			var planetOffset = new int3(0, 0, 0);
			var planetStretch = new float3(1, 1, 1);
			var treePrefab = this.treePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var worldEntities = worldsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var seeds = GetComponentLookup<Seed>(true);
            var biomeLinks = GetComponentLookup<BiomeLinks>(true);
            var simplexNoises = GetComponentLookup<SimplexNoise>(true);
            worldEntities.Dispose();
            var chunkEntities = worldsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunks = GetComponentLookup<Chunk>(true);
            chunkEntities.Dispose();
            var biomeEntities = biomesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var biomes = GetComponentLookup<Biome>(true);
            biomeEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<UpdateChunkNeighbors>()
				.WithAll<GenerateVegetation>()
				.ForEach((Entity e, int entityInQueryIndex, ref Chunk chunk, ref ChunkVoxelRotations chunkVoxelRotations, in ChunkPosition chunkPosition,
                    in VoxLink voxLink, in ChunkHeights chunkHeights, in ChunkNeighbors chunkNeighbors) =>
            {
                if (HasComponent<StreamVox>(voxLink.vox) || HasComponent<OnSpawnedPlanetChunks>(voxLink.vox))
                {
                    return;
                }
                if (HasComponent<EditedChunk>(e))
                {
                    //  wait if edited - it doesn't spawn trees?
                    //! \todo Save Structures when editing chunks - the first time when saving chunk. Load them here.
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnSpawnedStructures(0));
                    PostUpdateCommands.RemoveComponent<GenerateVegetation>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<GenerateTrees>(entityInQueryIndex, e);
                    return;
                }
                var simplexNoise = simplexNoises[voxLink.vox];
                var chunkNeighbors2 = chunkNeighbors.GetNeighborsAdjacent();
                for (int i = 0; i < chunkNeighbors2.Length; i++)
                {
                    if (HasComponent<GenerateTerrain>(chunkNeighbors2[i]))
                    {
			            chunkNeighbors2.Dispose();
                        return;
                    }
                }
                PostUpdateCommands.RemoveComponent<GenerateVegetation>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<GenerateTrees>(entityInQueryIndex, e);
                if (disableVegetation)
                {
                    chunkNeighbors2.Dispose();
                    return;
                }
				var chunkAdjacentNeighborsData = new ChunkAdjacentNeighborsData(in chunkNeighbors, in chunks);
                var seed = seeds[voxLink.vox];
                var planetBiomes = biomeLinks[voxLink.vox].biomes;
                var biomeEntity = planetBiomes[0];
                var biome = biomes[biomeEntity];
                var voxelDimensions = chunk.voxelDimensions;
                var spawnedStructures = 0;
                var random = new Random();
                random.InitState(seed.GetChunkSeed(chunkPosition.position));
                var perlinOffset = new float2(chunkPosition.position.x * voxelDimensions.x, chunkPosition.position.z * voxelDimensions.z);
                int heightOffset = (int)(chunkPosition.position.y * voxelDimensions.y);
                var chunkVoxelPosition = chunkPosition.GetVoxelPosition(voxelDimensions);
                var voxelIndex = 0;
                var blockDirection = PlanetSide.None;
                var localPosition = int3.zero;
                var globalPosition = int3.zero;
                byte voxelType;
                var chunkLink = new ChunkLink(e);
                var growFromPosition = int3.zero;
                var growFromIndex = 0;
                Entity treeEntity;
                // UnityEngine.Profiling.Profiler.BeginSample("Vegetation: Gather Voxel Neighbors");
                for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                {
                    for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                    {
                        for (localPosition.z =  0; localPosition.z < voxelDimensions.z; localPosition.z++)
                        {
                            var calculateSolidData = chunkHeights.heights[voxelIndex];
                            // if bedrock
                            if (calculateSolidData.distanceToGround == ChunkHeightsGenerationSystem.bedrockType || calculateSolidData.distanceToGround == ChunkHeightsGenerationSystem.disabledType)
                            {
                                voxelIndex++;
                                continue;
                            }
                            // find a grass facing up
                            voxelType = chunk.voxels[voxelIndex];
                            if (voxelType != 0)
                            {
                                voxelIndex++;
                                continue;
                            }
                            if (chunkAdjacentNeighborsData.GetVoxelValue(in chunk, localPosition.Left(), voxelDimensions) == biome.grassID)
                            {
                                growFromPosition = localPosition.Left();
                                blockDirection = BlockRotation.Right;
                            }
                            else if (chunkAdjacentNeighborsData.GetVoxelValue(in chunk, localPosition.Right(), voxelDimensions) == biome.grassID)
                            {
                                growFromPosition = localPosition.Right();
                                blockDirection = BlockRotation.Left;
                            }
                            else if (chunkAdjacentNeighborsData.GetVoxelValue(in chunk, localPosition.Up(), voxelDimensions) == biome.grassID)
                            {
                                growFromPosition = localPosition.Up();
                                blockDirection = BlockRotation.Down;
                            }
                            else if (chunkAdjacentNeighborsData.GetVoxelValue(in chunk, localPosition.Down(), voxelDimensions) == biome.grassID)
                            {
                                growFromPosition = localPosition.Down();
                                blockDirection = BlockRotation.Up;
                            }
                            else if (chunkAdjacentNeighborsData.GetVoxelValue(in chunk, localPosition.Backward(), voxelDimensions) == biome.grassID)
                            {
                                growFromPosition = localPosition.Backward();
                                blockDirection = BlockRotation.Forward;
                            }
                            else if (chunkAdjacentNeighborsData.GetVoxelValue(in chunk, localPosition.Forward(), voxelDimensions) == biome.grassID)
                            {
                                growFromPosition = localPosition.Forward();
                                blockDirection = BlockRotation.Backward;
                            }
                            // If cannot find grass to grow tree/weeds from, skip
                            else
                            {
                                voxelIndex++;
                                continue;
                            }
                            globalPosition = chunkVoxelPosition + localPosition;
                            var isWeeds = !disableWeeds && (100 * 0.5f * ( 1 + simplexNoise.Generate(
                                globalPosition.x * weedNoiseScale,
                                globalPosition.y * weedNoiseScale,
                                globalPosition.z * weedNoiseScale))) < voxGrassChance;
                            // var isWeeds = !disableWeeds && random.NextInt(100) < voxGrassChance;
                            // grow weeds
                            if (isWeeds)
                            {
                                chunk.voxels[voxelIndex] = biome.grassVoxID;
                                // should be all new rotations as placing weeds in air!
                                chunkVoxelRotations.AddRotation(localPosition, blockDirection);
                            }
                            // grow tree
                            else if (random.NextInt(100) < treeSpawnChance)
                            {
                                //! \todo Use SetVoxel - for surrounding chunk setting. Or set when spawning tree structure.
                                if (VoxelUtilities.InBounds(growFromPosition, voxelDimensions))
                                {
                                    growFromIndex = VoxelUtilities.GetVoxelArrayIndex(growFromPosition, voxelDimensions);
                                    chunk.voxels[growFromIndex] = biome.dirtID;
                                }
                                treeEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, treePrefab);
                                PostUpdateCommands.SetComponent(entityInQueryIndex, treeEntity, chunkLink);
                                PostUpdateCommands.SetComponent(entityInQueryIndex, treeEntity, voxLink);
                                PostUpdateCommands.SetComponent(entityInQueryIndex, treeEntity, new Translation { Value = globalPosition.ToFloat3() + halfOneOffset });
                                PostUpdateCommands.SetComponent(entityInQueryIndex, treeEntity, new Structure(blockDirection));
                                spawnedStructures++;
                            }
                            voxelIndex++;
                        }
                    }
                }
                // UnityEngine.Profiling.Profiler.EndSample();
                // UnityEngine.Debug.LogError("voxelRotations: " + voxelRotations.Length);
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnSpawnedStructures(spawnedStructures));
                chunkNeighbors2.Dispose();
            })  .WithReadOnly(seeds)
                .WithReadOnly(simplexNoises)
                .WithReadOnly(biomeLinks)
                .WithReadOnly(chunks)
                .WithNativeDisableContainerSafetyRestriction(chunks)
                .WithReadOnly(biomes)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}