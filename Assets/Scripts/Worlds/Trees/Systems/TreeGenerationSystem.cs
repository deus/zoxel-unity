using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
	//! Spawns tree structure in chunks
	/**
	*	\todo Fix issue where chunks next to the ones that becomevisible don't update, but the voxels do
	*		this will be an issue though with lighting, so need to improve that process of Chunk Updates
	*	\todo ... Tree won't build the same when chunk edited...  perhaps need to any side chunk trees when edited chunk
	*/
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class TreeGenerationSystem : SystemBase
	{
		const int stumpChance = 90;
		const int branchChance = 12;
		const int leafSpawnChance = 80;
		// const int adjacentWoodGrowthChance = 61;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery worldsQuery;
        private EntityQuery chunksQuery;
        private EntityQuery biomesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            worldsQuery = GetEntityQuery(ComponentType.ReadOnly<Planet>());
            chunksQuery = GetEntityQuery(
				ComponentType.ReadWrite<Chunk>(),
				ComponentType.ReadWrite<GenerateTrees>(),
				ComponentType.ReadOnly<StructureLinks>(),
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.Exclude<OnSpawnedStructures>(),
				ComponentType.Exclude<UpdateChunkNeighbors>());
            biomesQuery = GetEntityQuery(
				ComponentType.ReadOnly<Biome>(),
				ComponentType.Exclude<DestroyEntity>());
			RequireForUpdate(processQuery);
			RequireForUpdate(chunksQuery);
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
			var disableTrees = WorldsManager.instance.worldsSettings.disableTrees;
			var treeStump = WorldsManager.instance.worldsSettings.treeStump;
			var leafLength = WorldsManager.instance.worldsSettings.leafLength;
			var worldEntities = worldsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
			Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var biomeLinks = GetComponentLookup<BiomeLinks>(true);
			worldEntities.Dispose();
			var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
			Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var chunks = GetComponentLookup<Chunk>(false);
			var chunkNeighbors2 = GetComponentLookup<ChunkNeighbors>(true);
			var seeds = GetComponentLookup<Seed>(true);
			var chunkPositions = GetComponentLookup<ChunkPosition>(true);
			chunkEntities.Dispose();
			var biomeEntities = biomesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
			Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var biomes = GetComponentLookup<Biome>(true);
			biomeEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<DestroyEntity>()
				.WithAll<Tree>()
				.ForEach((Entity e, in Translation translation, in Structure structure, in VoxLink voxLink, in ChunkLink chunkLink) =>
			{
				if (chunkLink.chunk.Index == 0 || HasComponent<UpdateChunkNeighbors>(chunkLink.chunk) || !seeds.HasComponent(chunkLink.chunk))
				{
					// UnityEngine.Debug.LogError("Skipping Building Tree.");
					return;
				}
				var chunkNeighbors = chunkNeighbors2[chunkLink.chunk].GetNeighbors();
				for (int i = 0; i < chunkNeighbors.Length; i++)
				{
					var chunkEntity = chunkNeighbors[i];
					if (HasComponent<OnSpawnedStructures>(chunkEntity) || HasComponent<UpdateChunkNeighbors>(chunkEntity))
					{
						// UnityEngine.Debug.LogError("Skipping Building Tree.");
						chunkNeighbors.Dispose();
						return;
					}
				}
				if (!biomeLinks.HasComponent(voxLink.vox))
				{
					return;
				}
				// UnityEngine.Debug.LogError("Spawning Tree.");
				var seed = seeds[chunkLink.chunk];
				var chunkPosition = chunkPositions[chunkLink.chunk];
				var voxelDimensions = chunks[chunkLink.chunk].voxelDimensions;
				var planetBiomes = biomeLinks[voxLink.vox].biomes;
				var biomeEntity = planetBiomes[0];
				var biome = biomes[biomeEntity];
				var treeTranslation = translation.Value;
				var globalPosition = new int3((int) math.floor(treeTranslation.x), (int) math.floor(treeTranslation.y), (int) math.floor(treeTranslation.z));
				var seed2 = seed.GetChunkSeed(chunkPosition.position) + globalPosition.x + globalPosition.y * 255 + globalPosition.z * 255 * 255;
				var random = new Random();
				random.InitState((uint) seed2);
				// get overlapping chunks
				var overlappingChunks = new NativeList<Entity>();
				// for all neighbors, check if bounds of tree overlaps them
				overlappingChunks.Add(chunkLink.chunk);
				// tree bounding box
				var treeRadius = 4;
				var treeHeight = 8;
				var treeLowerBounds = new int3(globalPosition.x, globalPosition.y, globalPosition.z);
				var treeUpperBounds = new int3(globalPosition.x, globalPosition.y, globalPosition.z);
				if (structure.direction == BlockRotation.Down)
				{
					treeLowerBounds -= (new int3(treeRadius, 0, treeRadius));
					treeUpperBounds += (new int3(treeRadius, -treeHeight, treeRadius));
				}
				else if (structure.direction == BlockRotation.Up)
				{
					treeLowerBounds -= (new int3(treeRadius, 0, treeRadius));
					treeUpperBounds += (new int3(treeRadius, treeHeight, treeRadius));
				}
				else if (structure.direction == BlockRotation.Left)
				{
					treeLowerBounds -= (new int3(0, treeRadius, treeRadius));
					treeUpperBounds += (new int3(-treeHeight, treeRadius, treeRadius));
				}
				else if (structure.direction == BlockRotation.Right)
				{
					treeLowerBounds -= (new int3(0, treeRadius, treeRadius));
					treeUpperBounds += (new int3(treeHeight, treeRadius, treeRadius));
				}
				else if (structure.direction == BlockRotation.Backward)
				{
					treeLowerBounds -= (new int3(treeRadius, treeRadius, 0));
					treeUpperBounds += (new int3(treeRadius, treeRadius, -treeHeight));
				}
				else if (structure.direction == BlockRotation.Forward)
				{
					treeLowerBounds -= (new int3(treeRadius, treeRadius, 0));
					treeUpperBounds += (new int3(treeRadius, treeRadius, treeHeight));
				}
				for (int i = 0; i < chunkNeighbors.Length; i++)
				{
					var chunkEntity = chunkNeighbors[i];
					if (chunkEntity.Index == 0 || !chunkPositions.HasComponent(chunkEntity))
					{
						continue;
					}
					// get chunk bounds
					var chunkPosition2 = chunkPositions[chunkEntity];
					int3 chunkLowerBounds;
					int3 chunkUpperBounds;
					chunkPosition2.GetChunkBounds(voxelDimensions, out chunkLowerBounds, out chunkUpperBounds);
					// does overlap
					// todo: make BoundingBox struct with this function!
					var doesNotOverlap = treeUpperBounds.x < chunkLowerBounds.x || treeLowerBounds.x > chunkUpperBounds.x
						|| treeUpperBounds.y < chunkLowerBounds.y || treeLowerBounds.y > chunkUpperBounds.y
						|| treeUpperBounds.z < chunkLowerBounds.z || treeLowerBounds.z > chunkUpperBounds.z;
					if (!doesNotOverlap)
					{
						overlappingChunks.Add(chunkEntity);
					}
				}
				chunkNeighbors.Dispose();
				var areAllEdited = true;
				for (int i = 0; i < overlappingChunks.Length; i++)
				{
					var chunkEntity = overlappingChunks[i];
					if (areAllEdited && !HasComponent<EditedChunk>(chunkEntity))
					{
						areAllEdited = false;
					}
				}
				overlappingChunks.Dispose();
				if (areAllEdited)
				{
					return;
				}
				var spawnBlocks = new NativeParallelHashMap<int3, VoxelPlaceData>(16, Allocator.Temp);
				// are any actually generating trees
				var seedChunkEntity = chunkLink.chunk;
				var chunkVoxelPosition = chunkPositions[seedChunkEntity].GetVoxelPosition(voxelDimensions);
				var localPosition = globalPosition - chunkVoxelPosition;
				if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
				{
					var treeStump2 = (byte) (random.NextInt(treeStump.x, treeStump.y));
					var leafLength2 = (byte) (random.NextInt(leafLength.x, leafLength.y));
					GrowTree(structure.direction, treeStump2, leafLength2, ref spawnBlocks, ref random, in biome, seedChunkEntity, in chunks, in chunkNeighbors2, in chunkPositions,
						localPosition, voxelDimensions, 0, 0, 1);
					// UnityEngine.Debug.LogError("Growing Tree: " + spawnBlocks.Count());
					// now set in tree
					// for next time!
					foreach (var KVP in spawnBlocks)
					{
						var chunkEntity = KVP.Value.chunkEntity;
						if (!HasComponent<EditedChunk>(chunkEntity))
						{
							var chunk = chunks[chunkEntity];
							chunk.voxels[KVP.Value.voxelIndex] = KVP.Value.voxelID;
							chunks[chunkEntity] = chunk;
						}
					}
				}
				spawnBlocks.Dispose();
			})  .WithReadOnly(biomeLinks)
				.WithReadOnly(chunkNeighbors2)
				.WithReadOnly(seeds)
				.WithReadOnly(chunkPositions)
				.WithNativeDisableContainerSafetyRestriction(chunks)
				.WithReadOnly(biomes)
				.ScheduleParallel(Dependency);
        }

		public struct VoxelPlaceData
		{
			public Entity chunkEntity;
			public int voxelIndex;
			public byte voxelID;
		}

		private static void GrowTree(byte direction, byte treeStump, byte leafLength, ref NativeParallelHashMap<int3, VoxelPlaceData> spawnBlocks, ref Random random, in Biome biome, Entity chunkEntity, 
			in ComponentLookup<Chunk> chunks, in ComponentLookup<ChunkNeighbors> chunkNeighbors, in ComponentLookup<ChunkPosition> chunkPositions,
			int3 localPosition, int3 voxelDimensions, byte isLeaf = 0, int length = 0, byte isInitial = 0)
		{
			if (isInitial == 0)
			{
				if (direction == BlockRotation.Down)
				{
					localPosition = localPosition.Down();
				}
				else if (direction == BlockRotation.Up)
				{
					localPosition = localPosition.Up();
				}
				else if (direction == BlockRotation.Left)
				{
					localPosition = localPosition.Left();
				}
				else if (direction == BlockRotation.Right)
				{
					localPosition = localPosition.Right();
				}
				else if (direction == BlockRotation.Backward)
				{
					localPosition = localPosition.Backward();
				}
				else if (direction == BlockRotation.Forward)
				{
					localPosition = localPosition.Forward();
				}
			}
			// move between local chunks
			if (!VoxelUtilities.InBounds(localPosition, voxelDimensions))
			{
				int3 otherVoxelPosition;
				var chunkNeighbors2 = chunkNeighbors[chunkEntity];
				chunkEntity = chunkNeighbors2.GetChunkAdjacent(localPosition, voxelDimensions, out otherVoxelPosition);
				if (chunkEntity.Index == 0)
				{
					// UnityEngine.Debug.LogError("Tree Growing Ended Early 1: " + spawnBlocks.Count());
					return;
				}
				if (!chunks.HasComponent(chunkEntity))
				{
					// UnityEngine.Debug.LogError("Tree Growing Ended Early 2: " + spawnBlocks.Count());
					return;
				}
				localPosition = otherVoxelPosition;
			}
			// place wood block
			if (!VoxelUtilities.InBounds(localPosition, voxelDimensions))
			{
				// UnityEngine.Debug.LogError("Tree Growing Ended Early 3: " + spawnBlocks.Count());
				return;
			}
			var chunk = chunks[chunkEntity];
			if (chunk.voxels.Length == 0)
			{
				// UnityEngine.Debug.LogError("Tree Growing Ended Early 4: " + spawnBlocks.Count());
				return;
			}
			var chunkPosition = chunkPositions[chunkEntity];
			var globalPosition = chunkPosition.GetVoxelPosition(voxelDimensions) + localPosition;
			var voxelIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
			var placeOnVoxelID = chunk.voxels[voxelIndex];
			if ((placeOnVoxelID != 0 && placeOnVoxelID != biome.woodID && placeOnVoxelID != biome.leafID) || spawnBlocks.ContainsKey(globalPosition))
			{
				// UnityEngine.Debug.LogError("Tree Growing Ended Early: " + placeOnVoxelID + " :: " + direction); //  spawnBlocks.Count());
				return;
			}
			var placeType = biome.woodID;
			if (isLeaf == 1)
			{
				placeType = biome.leafID;
				// UnityEngine.Debug.LogError("Placing Leaf: " + length);
			}
			spawnBlocks.Add(globalPosition, new VoxelPlaceData
				{
					chunkEntity = chunkEntity,
					voxelIndex = voxelIndex,
					voxelID = placeType
				});
			// UnityEngine.Debug.LogError("Placed Tree at: " + localPosition + ", global: " + globalPosition);
			// next keep growing
			length++;
			var isGrowLeaves = isLeaf == 1;
			// base of tree
			if (!isGrowLeaves)
			{
				if (length <= treeStump && (stumpChance == 100 || random.NextInt(100) <= stumpChance))
				{
					// UnityEngine.Debug.LogError("Increasing TreeStump Length: " + length);
					GrowTree(direction, treeStump, leafLength, ref spawnBlocks, ref random, in biome, chunkEntity,
						in chunks, in chunkNeighbors, in chunkPositions, localPosition, voxelDimensions, isLeaf, length);
				}
				//! Grow Wood Branches out from tree.
				else
				{
					var didBranchOff = false;
					if (random.NextInt(100) < branchChance)
					{
						didBranchOff = true;
						GrowTree(BlockRotation.Backward, treeStump, leafLength, ref spawnBlocks, ref random, in biome, chunkEntity,
							in chunks, in chunkNeighbors, in chunkPositions, localPosition, voxelDimensions, isLeaf, length);
					}
					if (random.NextInt(100) < branchChance)
					{
						didBranchOff = true;
						GrowTree(BlockRotation.Forward, treeStump, leafLength, ref spawnBlocks, ref random, in biome, chunkEntity,
							in chunks, in chunkNeighbors, in chunkPositions, localPosition, voxelDimensions, isLeaf, length);
					}
					if (random.NextInt(100) < branchChance)
					{
						didBranchOff = true;
						GrowTree(BlockRotation.Left, treeStump, leafLength, ref spawnBlocks, ref random, in biome, chunkEntity,
							in chunks, in chunkNeighbors, in chunkPositions, localPosition, voxelDimensions, isLeaf, length);
					}
					if (random.NextInt(100) < branchChance)
					{
						didBranchOff = true;
						GrowTree(BlockRotation.Right, treeStump, leafLength, ref spawnBlocks, ref random, in biome, chunkEntity,
							in chunks, in chunkNeighbors, in chunkPositions, localPosition, voxelDimensions, isLeaf, length);
					}
					if (random.NextInt(100) < branchChance)
					{
						didBranchOff = true;
						GrowTree(BlockRotation.Down, treeStump, leafLength, ref spawnBlocks, ref random, in biome, chunkEntity,
							in chunks, in chunkNeighbors, in chunkPositions, localPosition, voxelDimensions, isLeaf, length);
					}
					if (random.NextInt(100) < branchChance)
					{
						didBranchOff = true;
						GrowTree(BlockRotation.Up, treeStump, leafLength, ref spawnBlocks, ref random, in biome, chunkEntity,
							in chunks, in chunkNeighbors, in chunkPositions, localPosition, voxelDimensions, isLeaf, length);
					}
					if (!didBranchOff)
					{
						isLeaf = 1;
						isGrowLeaves = true;
					}
				}
			}
			// branches and leaves
			if (isGrowLeaves)
			{
				if (length > treeStump + leafLength)
				{
					//UnityEngine.Debug.LogError("Leaf Ending: " + length + " :: " + leafLength);
					return;
				}
				//UnityEngine.Debug.LogError("Growing Leaf: " + length);
				/*if (random.NextInt(1000) <= 3 * adjacentWoodGrowthChance)
				{
					if (isLeaf == 0 && random.NextInt(100) <= 90)
					{
						isLeaf = 1;
					}
					isGrow = true;
				}
				else if (isLeaf == 0)
				{
					isLeaf = 1;
					isGrow = true;
				}*/
				if (random.NextInt(100) < leafSpawnChance)
				{
					GrowTree(BlockRotation.Backward, treeStump, leafLength, ref spawnBlocks, ref random, in biome, chunkEntity,
						in chunks, in chunkNeighbors, in chunkPositions, localPosition, voxelDimensions, isLeaf, length);
				}
				if (random.NextInt(100) < leafSpawnChance)
				{
					GrowTree(BlockRotation.Forward, treeStump, leafLength, ref spawnBlocks, ref random, in biome, chunkEntity,
						in chunks, in chunkNeighbors, in chunkPositions, localPosition, voxelDimensions, isLeaf, length);
				}
				if (random.NextInt(100) < leafSpawnChance)
				{
					GrowTree(BlockRotation.Left, treeStump, leafLength, ref spawnBlocks, ref random, in biome, chunkEntity,
						in chunks, in chunkNeighbors, in chunkPositions, localPosition, voxelDimensions, isLeaf, length);
				}
				if (random.NextInt(100) < leafSpawnChance)
				{
					GrowTree(BlockRotation.Right, treeStump, leafLength, ref spawnBlocks, ref random, in biome, chunkEntity,
						in chunks, in chunkNeighbors, in chunkPositions, localPosition, voxelDimensions, isLeaf, length);
				}
				if (random.NextInt(100) < leafSpawnChance)
				{
					GrowTree(BlockRotation.Down, treeStump, leafLength, ref spawnBlocks, ref random, in biome, chunkEntity,
						in chunks, in chunkNeighbors, in chunkPositions, localPosition, voxelDimensions, isLeaf, length);
				}
				if (random.NextInt(100) < leafSpawnChance)
				{
					GrowTree(BlockRotation.Up, treeStump, leafLength, ref spawnBlocks, ref random, in biome, chunkEntity,
						in chunks, in chunkNeighbors, in chunkPositions, localPosition, voxelDimensions, isLeaf, length);
				}
			}
		}
		/*if (random.NextInt(1000) <= adjacentWoodGrowthChance)
		{
			GrowWood(in biome, chunkEntity, ref chunk, ref chunks, in chunkNeighbors, ref random, localPosition, voxelDimensions, BlockRotation.Backward, isLeaf, length);
		}
		if (random.NextInt(1000) <= adjacentWoodGrowthChance)
		{
			GrowWood(in biome, chunkEntity, ref chunk, ref chunks, in chunkNeighbors, ref random, localPosition, voxelDimensions, BlockRotation.Forward, isLeaf, length);
		}
		if (random.NextInt(1000) <= adjacentWoodGrowthChance)
		{
			GrowWood(in biome, chunkEntity, ref chunk, ref chunks, in chunkNeighbors, ref random, localPosition, voxelDimensions, BlockRotation.Left, isLeaf, length);
		}
		if (random.NextInt(1000) <= adjacentWoodGrowthChance)
		{
			GrowWood(in biome, chunkEntity, ref chunk, ref chunks, in chunkNeighbors, ref random, localPosition, voxelDimensions, BlockRotation.Right, isLeaf, length);
		}
		if (random.NextInt(1000) <= adjacentWoodGrowthChance)
		{
			GrowWood(in biome, chunkEntity, ref chunk, ref chunks, in chunkNeighbors, ref random, localPosition, voxelDimensions, BlockRotation.Down, isLeaf, length);
		}
		if (random.NextInt(1000) <= adjacentWoodGrowthChance)
		{
			GrowWood(in biome, chunkEntity, ref chunk, ref chunks, in chunkNeighbors, ref random, localPosition, voxelDimensions, BlockRotation.Up, isLeaf, length);
		}*/
    }
}


		// Grow a tree one block at a time
		// chance to grow upward is 80%
		// chance to grow outward is 30%
		// only grow in air
		/*private static void GrowTree(ref Chunk chunk, ref Random treeRandom, ref NativeList<int3> spawnWoodPositions, int treeStump, leafLength,
			int3 localPosition, byte woodID, int length = 0)
		{
			var isGrowTree = false;
			if (VoxelUtilities.InBounds(localPosition, chunk.voxelDimensions)) // voxelIndex >= 0 && voxelIndex < chunk.voxels.Length && )
			{
				var voxelIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, chunk.voxelDimensions);
				if (chunk.voxels[voxelIndex] == 0)
				{
					chunk.voxels[voxelIndex] = woodID;
					isGrowTree = true;
				}
			}
			else
			{
				// check neighbor chunks
			}

			if (isGrowTree)
			{
				length++;
				if (length >= treeStump && !spawnWoodPositions.Contains(localPosition))
				{
					spawnWoodPositions.Add(localPosition);
				}
				if (length <= maxWoodLength)
				{
					if (length >= treeStump)
					{
						if (treeRandom.NextInt(1000) <= adjacentWoodGrowthChance)
						{
							GrowTree(ref chunk, ref treeRandom, ref spawnWoodPositions, treeStump, leafLength, localPosition.Up(), woodID, length);
						}
						if (treeRandom.NextInt(1000) <= adjacentWoodGrowthChance)
						{
							GrowTree(ref chunk, ref treeRandom, ref spawnWoodPositions, treeStump, leafLength, localPosition.Down(), woodID, length);
						}
						if (treeRandom.NextInt(1000) <= adjacentWoodGrowthChance)
						{
							GrowTree(ref chunk, ref treeRandom, ref spawnWoodPositions, treeStump, leafLength, localPosition.Left(), woodID, length);
						}
						if (treeRandom.NextInt(1000) <= adjacentWoodGrowthChance)
						{
							GrowTree(ref chunk, ref treeRandom, ref spawnWoodPositions, treeStump, leafLength, localPosition.Right(), woodID, length);
						}
						if (treeRandom.NextInt(1000) <= adjacentWoodGrowthChance)
						{
							GrowTree(ref chunk, ref treeRandom, ref spawnWoodPositions, treeStump, leafLength, localPosition.Backward(), woodID, length);
						}
						if (treeRandom.NextInt(1000) <= adjacentWoodGrowthChance)
						{
							GrowTree(ref chunk, ref treeRandom, ref spawnWoodPositions, treeStump, leafLength, localPosition.Forward(), woodID, length);
						}
					}
					else
					{
						GrowTree(ref chunk, ref treeRandom, ref spawnWoodPositions, treeStump, leafLength, localPosition.Up(), woodID, length);
					}
				}
			}
		}*/
/*private static void BeginGrowLeaves(ref Chunk chunk, ref Random treeRandom, int3 localPosition, byte leafID, int length = 0)
{
	if (treeRandom.NextInt(1000) < growLeafChance * 2)
	{
		GrowLeaves(ref chunk, ref treeRandom, localPosition.Up(), leafID, length);
	}
	if (treeRandom.NextInt(1000) < growLeafChance * 2)
	{
		GrowLeaves(ref chunk, ref treeRandom, localPosition.Down(), leafID, length);
	}
	if (treeRandom.NextInt(1000) < growLeafChance * 2)
	{
		GrowLeaves(ref chunk, ref treeRandom, localPosition.Left(), leafID, length);
	}
	if (treeRandom.NextInt(1000) < growLeafChance * 2)
	{
		GrowLeaves(ref chunk, ref treeRandom, localPosition.Right(), leafID, length);
	}
	if (treeRandom.NextInt(1000) < growLeafChance * 2)
	{
		GrowLeaves(ref chunk, ref treeRandom, localPosition.Backward(), leafID, length);
	}
	if (treeRandom.NextInt(1000) < growLeafChance * 2)
	{
		GrowLeaves(ref chunk, ref treeRandom, localPosition.Forward(), leafID, length);
	}
}

private static void GrowLeaves(ref Chunk chunk, ref Random treeRandom, int3 localPosition, byte leafID, int length = 0)
{
	if (VoxelUtilities.InBounds(localPosition, chunk.voxelDimensions))
	{
		var voxelIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, chunk.voxelDimensions);
		if (chunk.voxels[voxelIndex] == 0)
		{
			chunk.voxels[voxelIndex] = leafID;
			length++;
			if (length < maxLeavesLength)
			{
				if (treeRandom.NextInt(1000) < growLeafChance)
				{
					GrowLeaves(ref chunk, ref treeRandom, localPosition.Up(), leafID, length);
				}
				if (treeRandom.NextInt(1000) < growLeafChance)
				{
					GrowLeaves(ref chunk, ref treeRandom, localPosition.Down(), leafID, length);
				}
				if (treeRandom.NextInt(1000) < growLeafChance)
				{
					GrowLeaves(ref chunk, ref treeRandom, localPosition.Left(), leafID, length);
				}
				if (treeRandom.NextInt(1000) < growLeafChance)
				{
					GrowLeaves(ref chunk, ref treeRandom, localPosition.Right(), leafID, length);
				}
				if (treeRandom.NextInt(1000) < growLeafChance)
				{
					GrowLeaves(ref chunk, ref treeRandom, localPosition.Backward(), leafID, length);
				}
				if (treeRandom.NextInt(1000) < growLeafChance)
				{
					GrowLeaves(ref chunk, ref treeRandom, localPosition.Forward(), leafID, length);
				}
			}
		}
	}
}*/

/*for (position.x = border; position.x < voxelDimensions.x - border; position.x++)
{
	for (position.z = border; position.z < voxelDimensions.z - border; position.z++)
	{
		var positionXZ = VoxelUtilities.GetVoxelArrayIndexXZ(position, voxelDimensions);
		//var positionXZ = (int)(position.x * voxelDimensions.z + position.z);
		position.y = (int) (terrainHeightMap.heights[positionXZ] - heightOffset);
		// if grass
		var biomeIndex = (int)chunkBiome.biomes[positionXZ];
		var biomeIndex2 = math.min(biomes.Length - 1, biomeIndex);
		if (biomeIndex2 >= 0 && biomeIndex2 < biomes.Length)
		{
			var biome = biomes[biomeIndex2];
			var grassIndex = VoxelUtilities.GetVoxelArrayIndex(position, voxelDimensions);
			if (grassIndex >= 0 && grassIndex < chunk.voxels.Length && 
				chunk.voxels[grassIndex] == biome.grassID)
			{
				var isTree = treeRandom.NextInt(100) < treeSpawnChance;
				if (isTree)
				{
					chunk.voxels[grassIndex] = biome.dirtID;
					// spawn tree
					var treeStump = treeStumpMinimum + treeRandom.NextInt(treeStumpVariance);
					var treePosition = position.Up();
					GrowTree(ref chunk, ref treeRandom, ref spawnWoodPositions, treeStump, leafLength, treePosition, biome.woodID);
				}
			}
		}
	}
}*/


/*for (position.x = 0; position.x < voxelDimensions.x; position.x++)
{
	for (position.y = 0; position.y < voxelDimensions.y; position.y++)
	{
		for (position.z = 0; position.z < voxelDimensions.z; position.z++)
		{
			var voxelIndex = VoxelUtilities.GetVoxelArrayIndex(position, voxelDimensions);
			var voxelID = chunk.voxels[voxelIndex];
			var positionXZ = VoxelUtilities.GetVoxelArrayIndexXZ(position, voxelDimensions);
			var biomeIndex = (int)chunkBiome.biomes[positionXZ];
			var biome = terrainHeightMap.biomes[math.min(terrainHeightMap.biomes.Length - 1, biomeIndex)];
			if (voxelID == biome.woodID)
			{
				// GrowLeaves from here
				BeginGrowLeaves(ref chunk, ref treeRandom, position, biome.leafID);
			}
		}
	}
}*/
/*treeHeight = 1 + treeRandom.NextInt(7);
if (treeHeight <= 2)
{
	treeLeafsRadius = 0;
}
else if (treeHeight <= 4)
{
	treeLeafsRadius = 1;
}
else if (treeHeight <= 6)
{
	treeLeafsRadius = 1 + treeRandom.NextInt(1);
}
else
{
	treeLeafsRadius = 1 + treeRandom.NextInt(2);
}*/

									/*var treeLeafCenter = treePosition + new int3(0, treeHeight, 0);
									for (int i = -treeLeafsRadius; i <= treeLeafsRadius; i++)
									{
										for (int j = -treeLeafsRadius; j <= treeLeafsRadius; j++)
										{
											for (int k = -treeLeafsRadius; k <= treeLeafsRadius; k++)
											{
												var treePosition2 = treeLeafCenter + new int3(i, j, k);
												if (treePosition2.x >= 0 && treePosition2.x < voxelDimensions.x
													&& treePosition2.y >= 0 && treePosition2.y < voxelDimensions.y
													&& treePosition2.z >= 0 && treePosition2.z < voxelDimensions.z)
												{
													var voxelIndex2 = VoxelUtilities.GetVoxelArrayIndex(treePosition2, voxelDimensions);
													if (voxelIndex2 >= 0 && voxelIndex2 < chunk.voxels.Length)
													{
														chunk.voxels[voxelIndex2] = (byte)(biome.leafID);	// biome.dirtID
													}
												}
											}
										}
									}
									for (var a = 0; a < treeHeight + treeLeafsRadius; a++)
									{
										var voxelIndex2 = VoxelUtilities.GetVoxelArrayIndex(treePosition, voxelDimensions);
										if (voxelIndex2 >= 0 && voxelIndex2 < chunk.voxels.Length)
										{
											chunk.voxels[voxelIndex2] = (byte)(biome.woodID);	// 
										}
										treePosition = treePosition.Up();
									}*/






					/*for (int i = 0; i < spawnWoodPositions.Length; i++)
					{
						var position2 = spawnWoodPositions[i];
						var positionXZ = VoxelUtilities.GetVoxelArrayIndexXZ(position2, voxelDimensions);
						//var biomeIndex = (int)chunkBiome.biomes[positionXZ];
						//var biomeIndex2 = math.min(biomeIndex, biomes.Length - 1);
						//if (biomeIndex2 >= 0 && biomeIndex2 < biomes.Length)
						{
							//var biome = biomes[biomeIndex2];
							BeginGrowLeaves(ref chunk, ref treeRandom, position2, biome.leafID);
						}
					}*/
                //}

					/*var treeSpawnChance = math.max(0, treeSpawnMin + (int)(treeSpawnChance2 *
						0.5f * ( 1 + simplexNoise.Generate(chunkPosition.position.x * noiseScale,
							chunkPosition.position.y * noiseScale, chunkPosition.position.z * noiseScale))));*/

					// UnityEngine.Debug.LogError("Tree Spawn Chance: " + chunkPosition.position + " ::: " + treeSpawnChance);
					// put this in another system?
					// Or simply removetrees when inside a town
					//for (int i = 0; i < structureLinks.structures.Length; i++)
						//var tree = structureLinks.structures[i];
						//if (treeTranslations.HasComponent(tree))
							/*var treeTranslation = translation.Value; // treeTranslations[tree].Value;
							var globalPosition = new int3((int)treeTranslation.x, (int)treeTranslation.y, (int)treeTranslation.z);
							var chunkVoxelPosition = chunkPosition.GetVoxelPosition(chunk.voxelDimensions);
							var localPosition = globalPosition - chunkVoxelPosition;
							var grassIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
							if (grassIndex >= 0 && grassIndex < chunk.voxels.Length && 
								chunk.voxels[grassIndex] == biome.grassID)
							{
								chunk.voxels[grassIndex] = biome.dirtID;
								var treeStump = treeStumpMinimum + treeRandom.NextInt(treeStumpVariance);
								var treePosition = position.Up();
								// GrowTree(ref chunk, ref treeRandom, ref spawnWoodPositions, treeStump, leafLength, treePosition, biome.woodID);
							}*/



				// .WithNone<OnSpawnedStructures>()
				/*.ForEach((Entity e, ref GenerateChunk generateChunk, ref Chunk chunk, // in ChunkBiome chunkBiome,
					in ChunkPosition chunkPosition, in SimplexNoise simplexNoise, in VoxLink voxLink, in Seed seed, in StructureLinks structureLinks,
					in ChunkNeighbors chunkNeighbors) =>*/