using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Worlds
{
    //! End the tree generation events and add a complete event onto the chunk.
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class TreeGenerateEndSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<OnSpawnedStructures>(),
                ComponentType.ReadOnly<GenerateTrees>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<GenerateTrees>(processQuery);
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AddComponent<GenerateTowns>(processQuery);
        }
    }
}