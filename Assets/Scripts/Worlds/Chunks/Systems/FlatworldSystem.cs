using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Worlds;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
	public partial class FlatworldSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref Chunk chunk, ref GenerateFlatworld generateFlatworld) =>
            {
                /*if (generateFlatworld.initialized == 1)
                {
                    return;
                }*/
                //generateFlatworld.initialized = 1;
                var height = chunk.voxelDimensions.y / 2 + generateFlatworld.random.NextInt(3);
                var voxelIndex = 0;
                for (int i = 0; i < chunk.voxelDimensions.x; i++)
                {
                    for (int j= 0; j < chunk.voxelDimensions.y; j++)
                    {
                        for (int k = 0; k < chunk.voxelDimensions.z; k++)
                        {
                            if (j <= height)
                            {
                                chunk.voxels[voxelIndex] = 1;
                            }
                            else
                            {
                                chunk.voxels[voxelIndex] = 0;
                            }
                            voxelIndex++;
                        }
                    }
                }
                PostUpdateCommands.RemoveComponent<GenerateFlatworld>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}