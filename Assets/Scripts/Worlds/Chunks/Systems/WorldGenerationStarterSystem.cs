﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! Starts the Chunk world generation process.
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class WorldGenerationBeginSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity>()
                .ForEach((Entity e, int entityInQueryIndex, in GenerateChunk generateChunk) =>
            {
                if (generateChunk.state != GenerateChunkState.GenerationStart)
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<GenerateChunk>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<GenerateHeights>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}