﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! Links a chunk to a bunch of PlanetChunk2Ds.
    /**
    *   \todo Store PlanetChunk2Ds by using a hashmap.
    */
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class PlanetChunk2DLinkSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery planetsQuery;
        private EntityQuery planetChunk2DsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            planetsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<Vox>());
            planetChunk2DsQuery = GetEntityQuery(
                ComponentType.ReadOnly<PlanetChunk2D>(),
                ComponentType.ReadOnly<QuadrantPosition2D>(),
                ComponentType.ReadOnly<TerrainHeightMap>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
			var planetStretch = new float3(1, 1, 1);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var planetEntities = planetsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var planets = GetComponentLookup<Planet>(true);
            var planetChunk2DLinks = GetComponentLookup<PlanetChunk2DLinks>(true);
            var chunkDimensions = GetComponentLookup<ChunkDimensions>(true);
            var planetChunk2DEntities = planetChunk2DsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var quadrantPosition2Ds = GetComponentLookup<QuadrantPosition2D>(true);
            //planetEntities.Dispose();
            //planetChunk2DEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref GenerateChunk generateChunk, ref ChunkPlanetChunk2DLinks chunkPlanetChunk2DLinks, ref ChunkGravityQuadrant chunkGravityQuadrant,
                    in ChunkPosition chunkPosition, in VoxLink voxLink) =>
            {
                if (generateChunk.state != GenerateChunkState.LinkPlanetChunk2Ds || !planets.HasComponent(voxLink.vox))
                {
                    return;
                }
			    // UnityEngine.Profiling.Profiler.BeginSample("PlanetChunk2DLinkSystem");
                generateChunk.state = GenerateChunkState.GenerationStart;
                var quadrant = new bool3(false, false, false);
                var quadrantA = new bool3(false, false, false);
                var quadrantB = new bool3(false, false, false);
                var planet = planets[voxLink.vox];
                var planetChunk2DLinks2 = planetChunk2DLinks[voxLink.vox];
                var voxelDimensions = chunkDimensions[voxLink.vox].voxelDimensions;
                var planetRadius = 16 * (math.floor(planet.radius / 16) - 1);
                var planetLowerBounds = - (new int3((int) (planetRadius * planetStretch.x), (int) (planetRadius * planetStretch.y), (int) (planetRadius * planetStretch.z)));
                var planetUpperBounds = new int3((int) (planetRadius * planetStretch.x), (int) (planetRadius * planetStretch.y), (int) (planetRadius * planetStretch.z));
                int3 chunkLowerBounds;
                int3 chunkUpperBounds;
                chunkPosition.GetChunkBounds(voxelDimensions, out chunkLowerBounds, out chunkUpperBounds);
                //! Link a Chunk to PlanetChunk2D's.
                var intersectingChunks = new NativeList<Entity>();
                for (int i = 0; i < planetChunk2DLinks2.chunks.Length; i++)
                {
                    var planetChunk2DEntity = planetChunk2DLinks2.chunks[i];
                    if (!quadrantPosition2Ds.HasComponent(planetChunk2DEntity))
                    {
                        continue;
                    }
                    var quadrantPosition2D = quadrantPosition2Ds[planetChunk2DEntity];
                    if (quadrantPosition2D.IsChunkPosition(chunkPosition.position))
                    {
                        intersectingChunks.Add(planetChunk2DEntity);
                        var intersectingSide = quadrantPosition2D.quadrant;
                        if (intersectingSide == PlanetSide.Down || intersectingSide == PlanetSide.Up)
                        {
                            quadrant.SetBoolA(true);
                            if (chunkUpperBounds.y < planetLowerBounds.y)
                            {
                                quadrantA.SetBoolA(intersectingSide == PlanetSide.Down);
                            }
                            else if (chunkLowerBounds.y > planetUpperBounds.y)
                            {
                                quadrantB.SetBoolA(intersectingSide == PlanetSide.Up);
                            }
                        }
                        else if (intersectingSide == PlanetSide.Left || intersectingSide == PlanetSide.Right)
                        {
                            quadrant.SetBoolB(true);
                            if (chunkUpperBounds.x < planetLowerBounds.x)
                            {
                                quadrantA.SetBoolB(intersectingSide == PlanetSide.Left);
                            }
                            else if (chunkLowerBounds.x > planetUpperBounds.x)
                            {
                                quadrantB.SetBoolB(intersectingSide == PlanetSide.Right);
                            }
                        }
                        else if (intersectingSide == PlanetSide.Backward || intersectingSide == PlanetSide.Forward)
                        {
                            quadrant.SetBoolC(true);
                            if (chunkUpperBounds.z < planetLowerBounds.z)
                            {
                                quadrantA.SetBoolC(intersectingSide == PlanetSide.Backward);
                            }
                            else if (chunkLowerBounds.z > planetUpperBounds.z)
                            {
                                quadrantB.SetBoolC(intersectingSide == PlanetSide.Forward);
                            }
                        }
                    }
                }
                // UnityEngine.Debug.LogError("Heights: " + hasHeights.GetBoolA() + ":" + hasHeights.GetBoolB() + "::" + hasHeights.GetBoolC());
                chunkPlanetChunk2DLinks.SetChunks(intersectingChunks, quadrant);
                chunkGravityQuadrant.quadrantA = quadrantA;
                chunkGravityQuadrant.quadrantB = quadrantB;
                intersectingChunks.Dispose();
			    // UnityEngine.Profiling.Profiler.EndSample();
                if (planetEntities.Length > 0) { var e2 = planetEntities[0]; }
                if (planetChunk2DEntities.Length > 0) { var e2 = planetChunk2DEntities[0]; }
            })  .WithReadOnly(planets)
                .WithReadOnly(chunkDimensions)
                .WithReadOnly(planetChunk2DLinks)
                .WithReadOnly(quadrantPosition2Ds)
                .WithReadOnly(planetEntities)
                .WithDisposeOnCompletion(planetEntities)
                .WithReadOnly(planetChunk2DEntities)
                .WithDisposeOnCompletion(planetChunk2DEntities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                    /*var planetChunkVoxLink = planetChunkVoxLinks[planetChunk2DEntity];
                    if (planetChunkVoxLink.vox != voxLink.vox)
                    {
                        continue;
                    }*/
                // .WithReadOnly(planetChunkVoxLinks)