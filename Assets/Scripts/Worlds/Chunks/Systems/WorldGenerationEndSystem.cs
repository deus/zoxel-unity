﻿using Unity.Entities;
using Unity.Burst;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! Adds build chunk events after generation.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class WorldGenerationEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();

            //! Removes GenerateChunkComplete event component after use.
            Dependency = Entities
                .WithAll<GenerateChunkComplete>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<GenerateChunkComplete>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

            //! Removes EntityGenerating event component after use.
            Dependency = Entities
                .WithAll<GenerateChunkComplete, EntityGenerating>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<EntityGenerating>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

            //! Triggers Non EdgeChunk to spawn ChunkRenders after generation has completed.
            Dependency = Entities
                .WithNone<MapUIOnly, EdgeChunk, ChunkSpawnRenders>()
                .WithAll<GenerateChunkComplete>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.AddComponent<ChunkSpawnRenders>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

            //! Triggers Chunk to Build after generation has completed.
            Dependency = Entities
                .WithNone<MapUIOnly, ChunkBuilder>()
                // .WithAll<EdgeChunk>()
                .WithAll<GenerateChunkComplete>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

            // No EdgeChunk Component
// With EdgeChunk Component