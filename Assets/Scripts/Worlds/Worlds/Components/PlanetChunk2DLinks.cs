using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Worlds
{
    //! Links a Planet to to PlanetChunk2D's.
    public struct PlanetChunk2DLinks : IComponentData
    {
        public BlitableArray<Entity> chunks;
        
        public void DisposeFinal()
        {
            chunks.DisposeFinal();
        }
        
        public void Dispose()
        {
            chunks.Dispose();
        }

        public void AddNew(int addCount)
        {
            var newChunks = new BlitableArray<Entity>(chunks.Length + addCount, Allocator.Persistent);
            for (int i = 0; i < chunks.Length; i++)
            {
                newChunks[i] = chunks[i];
            }
            for (int i = chunks.Length; i < newChunks.Length; i++)
            {
                newChunks[i] = new Entity();
            }
            if (chunks.Length > 0)
            {
                chunks.Dispose();
            }
            chunks = newChunks;
        }
    }
}