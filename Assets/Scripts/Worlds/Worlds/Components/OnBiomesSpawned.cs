using Unity.Entities;

namespace Zoxel.Worlds
{
    //! Added to planet after biomes spawn to collect them.
    public struct OnBiomesSpawned : IComponentData { }
}