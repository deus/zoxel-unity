using Unity.Entities;

namespace Zoxel.Worlds
{
    // contains block data used per biome
    // biome determined from height, humidity, temperature..?
    public struct Biome : IComponentData
    {
        // terrain
        public byte dirtID;
        public byte grassID;
        public byte grassVoxID;
        public byte stoneID;
        public byte bedrockID;

        // oceans
        public byte sandID;
        public byte waterID;

        // trees
        public byte leafID;
        public byte woodID;

        // houses
        public byte bricksID;
        public byte tilesID;
        public byte doorID;
        public byte torchID;
        /*public byte floorID;
        public byte wallID;
        public byte roofID;*/

        public byte grassChance;
    }
}