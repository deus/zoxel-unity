using Unity.Entities;

namespace Zoxel.Worlds
{
    public struct RefreshWorld : IComponentData
    {
        public int seed;
        
        public RefreshWorld(int seed)
        {
            this.seed = seed;
        }
    }
}