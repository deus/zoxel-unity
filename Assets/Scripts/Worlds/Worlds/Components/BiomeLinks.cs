using Unity.Entities;
using Unity.Collections;
// TODO: Set first town in center
//      new town must be 100 voxels away minimum
//      Generate path between town gates
//      Generate Lines for the town wall and vary the shape
//      For new buildings, check doesn't overlap over buildings, check it doesn't overlap side of town

namespace Zoxel.Worlds
{
    public struct BiomeLinks : IComponentData
    {
        public BlitableArray<Entity> biomes;

        public void Dispose()
        {
            if (biomes.Length > 0)
            {
                biomes.Dispose();
            }
        }

        public void InitializeBiomeLinks(int newCount)
        {
            if (biomes.Length != newCount)
            {
                Dispose();
                biomes = new BlitableArray<Entity>(newCount, Allocator.Persistent);
                for (int i = 0; i < biomes.Length; i++)
                {
                    biomes[i] = new Entity();
                }
            }
        }
    }
}