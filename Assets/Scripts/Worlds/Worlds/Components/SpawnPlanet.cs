using Unity.Entities;

namespace Zoxel.Worlds
{
    //! Spawns planet from Realm.
    public struct SpawnPlanet : IComponentData
    {
        public Entity realm;
        public Entity player;
        public int worldID;

        public SpawnPlanet(Entity realm, Entity player, int worldID)
        {
            this.realm = realm;
            this.player = player;
            this.worldID = worldID;
        }
    }
}