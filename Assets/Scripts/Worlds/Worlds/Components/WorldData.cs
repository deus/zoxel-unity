using Unity.Entities;

namespace Zoxel.Worlds
{
    public struct WorldData : IComponentData
    {
        // Land
        public float landBase;          // = 6;
        public float landAmplitude;     // = 1;
        public float landScale;         // = 0.015f;
    }
}