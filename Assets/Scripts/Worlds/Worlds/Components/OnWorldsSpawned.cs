using Unity.Entities;

namespace Zoxel.Worlds
{
    //! An event for linking new spawned worlds to the Realm.
    public struct OnWorldsSpawned : IComponentData { }
}