using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Worlds
{
	//! Destroys PlanetChunk2D entities.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class PlanetChunkLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithAll<DestroyEntity, PlanetChunk2DLinks>()
				.ForEach((int entityInQueryIndex, in PlanetChunk2DLinks planetChunk2DLinks) =>
			{
				for (int i = 0; i < planetChunk2DLinks.chunks.Length; i++)
				{
					PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, planetChunk2DLinks.chunks[i]);
				}
                planetChunk2DLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}