using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! Sets a planets voxels based on the Realm. (Just uses the same voxels atm)
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class PlanetSetVoxelsSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery realmsQuery;

        protected override void OnCreate()
        {
            realmsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<VoxelLinks>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var realmEntities = realmsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var realmVoxelLinks = GetComponentLookup<VoxelLinks>(true);
            realmEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<StreamVox, DestroyEntity>()
                .ForEach((ref GeneratePlanet generatePlanet, ref VoxelLinks voxelLinks, in ZoxID zoxID, in RealmLink realmLink) =>
            {
                if (!HasComponent<Realm>(realmLink.realm) || HasComponent<EntityBusy>(realmLink.realm) || generatePlanet.state != 0)
                {
                    return;
                }
                generatePlanet.state = 1;
                var realmVoxels = realmVoxelLinks[realmLink.realm].voxels;
                voxelLinks.InitializeVoxelsGroup(realmVoxels.Length);
                for (int i = 0; i < realmVoxels.Length; i++)
                {
                    voxelLinks.voxels[i] = realmVoxels[i];
                }
            })  .WithReadOnly(realmVoxelLinks)
                .WithNativeDisableContainerSafetyRestriction(realmVoxelLinks)
                .ScheduleParallel(Dependency);
        }
    }
}