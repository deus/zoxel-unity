using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! While generating Chunk's, it links a chunk to a MegaChunk.
    /**
    *   \todo this system seems stupid but i dare not remove it.
    */
    [UpdateBefore(typeof(ChunkGenerationBeginSystem))]
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class ChunkGenerationLoadingSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery chunksLoadingQuery;

        protected override void OnCreate()
        {
            chunksLoadingQuery = GetEntityQuery(ComponentType.ReadOnly<LoadChunk>());
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (!chunksLoadingQuery.IsEmpty) // CalculateEntityCount() > 0)
            {
                return;
            }
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, LoadChunk, UpdateChunkNeighbors>()
                .ForEach((Entity e, int entityInQueryIndex, ref GenerateChunk generateChunk, in ChunkNeighbors chunkNeighbors) =>
            {
                if (generateChunk.state != GenerateChunkState.Loading)
                {
                    return;
                }
                // Waits for any neighbors still loading.
                var adjacentNeighbors = chunkNeighbors.GetNeighborsAdjacent();
                for (int i = 0; i < adjacentNeighbors.Length; i++)
                {
                    if (HasComponent<LoadChunk>(adjacentNeighbors[i]))
                    {
                        adjacentNeighbors.Dispose();
                        return;
                    }
                }
                generateChunk.state = GenerateChunkState.LinkMegaChunk;
                adjacentNeighbors.Dispose();
            }).ScheduleParallel(Dependency);
        }
    }
}