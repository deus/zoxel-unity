using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Worlds
{
    //! Cleans up WorldLinks' worlds.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class WorldLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithAll<DestroyEntity, WorldLinks>()
				.ForEach((int entityInQueryIndex, in WorldLinks worldLinks) =>
			{
				// UnityEngine.Debug.LogError("Worlds Destroyed: " + worldLinks.worlds.Length);
				for (int i = 0; i < worldLinks.worlds.Length; i++)
				{
					var worldEntity = worldLinks.worlds[i];
					// UnityEngine.Debug.LogError("	worldEntity: " + worldEntity.Index);
					if (HasComponent<Planet>(worldEntity))
					{
						PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, worldEntity);
					}
				}
				worldLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}