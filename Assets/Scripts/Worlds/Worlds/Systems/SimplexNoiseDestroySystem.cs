using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Worlds
{
    //! Cleans up SimplexNoise's data.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class SimplexNoiseDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, Planet>()
                .ForEach((in SimplexNoise simplexNoise) =>
			{
                simplexNoise.Dispose();
            }).ScheduleParallel();
		}
	}
}