using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! Handles finding worlds after they spawned.
    /**
    *   - Linking System -
    *   \todo add subdivision to models of chunks
    */
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class OnWorldsSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery worldsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            worldsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<RealmLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var worldEntities = worldsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var realmLinks = GetComponentLookup<RealmLink>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<OnWorldsSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref WorldLinks worldLinks) =>
            {
                if (worldLinks.worlds.Length == 0)
                {
                    worldLinks.worlds = new BlitableArray<Entity>(1, Allocator.Persistent);
                }
                for (int i = 0; i < worldEntities.Length; i++)
                {
                    var e2 = worldEntities[i];
                    var realmEntity = realmLinks[e2].realm;
                    if (realmEntity == e)
                    {
                        worldLinks.worlds[0] = e2;
                        PostUpdateCommands.RemoveComponent<OnWorldsSpawned>(entityInQueryIndex, e);
                        // UnityEngine.Debug.LogError("WorldLinks: Done.");
                        break;
                    }
                }
            })  .WithReadOnly(worldEntities)
                .WithDisposeOnCompletion(worldEntities)
                .WithReadOnly(realmLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}