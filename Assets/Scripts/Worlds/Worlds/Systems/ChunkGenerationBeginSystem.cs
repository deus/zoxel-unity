using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! Starts the Chunk world generation process.
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class ChunkGenerationBeginSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, LoadChunk, EditedChunk>()
                .ForEach((ref Chunk chunk, ref GenerateChunk generateChunk) =>
            {
                if (generateChunk.state == GenerateChunkState.Begin)
                {
                    chunk.SetAllToAir();
                }
            }).ScheduleParallel(Dependency);
            Dependency = Entities
                .WithNone<InitializeEntity, LoadChunk>()
                .ForEach((Entity e, int entityInQueryIndex, ref Chunk chunk, ref GenerateChunk generateChunk) =>
            {
                if (generateChunk.state != GenerateChunkState.Begin)
                {
                    return;
                }
                generateChunk.state = GenerateChunkState.Loading;
                PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, e); 
                PostUpdateCommands.AddComponent<UpdateChunkNeighbors>(entityInQueryIndex, e);
                // PostUpdateCommands.AddComponent<GeneratingEntity>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}