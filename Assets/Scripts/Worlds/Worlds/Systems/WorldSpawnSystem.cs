﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Voxels.Lighting;
using Zoxel.Weather;
using Zoxel.Rendering;
using Zoxel.Rendering.Authoring;
using Zoxel.Textures;
using Zoxel.Textures.Tilemaps;
using Zoxel.Voxels.Lighting.Authoring;

namespace Zoxel.Worlds
{
    //! Spawns a World vox. It is where characters live.
    /**
    *   - Spawn System -
    *  Edge chunks are non visible so have no chunk renders. They are used for information for their surrounding chunks.
    *   Todo: Add subdivision - lesser resolution of chunks. Chunks on the outside of render view will be less resolution (bigger size).
    */
	[BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class WorldSpawnSystem : SystemBase
    {
        private bool hasSetSettings;
        private NativeArray<Text> texts;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity worldPrefab;
        private EntityQuery processQuery;
        private EntityQuery controllersQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Player>(),
                ComponentType.ReadOnly<CameraLink>());
            var worldArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(EntityBusy),
                typeof(GeneratePlanet),
                typeof(InitializeEntity),
                typeof(ZoxID),
                typeof(Seed),
                typeof(SimplexNoise),
                typeof(RealmLink),
                typeof(Planet),
                typeof(BiomeLinks),
                typeof(WorldData),
                typeof(MegaChunkLinks),
                typeof(MegaChunk2DLinks),
                typeof(PlanetChunk2DLinks),
                typeof(Skylight),
                typeof(WorldTime),
                typeof(WorldTimeSaver),
                typeof(Sky),
                typeof(SkyMaterial),
                typeof(VoxItemMaterial),
                typeof(VoxItemMaterialTilemap),
                typeof(RainCloudLink),
                // typeof(DayTime),
                // typeof(CloudSpawner),
                typeof(StreamableVox),
                typeof(Vox),
                typeof(ChunkLinks),
                typeof(ChunkDimensions),
                typeof(VoxScale),
                typeof(VoxelLinks),
                typeof(EntityMaterials),
                typeof(Translation),
                typeof(NonUniformScale),
                typeof(Rotation),
                typeof(LocalToWorld)
            );
            worldPrefab = EntityManager.CreateEntity(worldArchetype);
            EntityManager.SetComponentData(worldPrefab, new Rotation { Value = quaternion.identity });
            EntityManager.SetComponentData(worldPrefab, new NonUniformScale { Value = new float3(1, 1, 1) });
            EntityManager.SetComponentData(worldPrefab, new VoxScale(new float3(1, 1, 1) * 0.5f));
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(worldPrefab); // , new EditorName("[top-down-camera]"));
            #endif
            texts = new NativeArray<Text>(1, Allocator.Persistent);
            texts[0] = new Text("[world]");
            RequireForUpdate<LightsSettings>();
            RequireForUpdate<RenderSettings>();
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (!hasSetSettings)
            {
                hasSetSettings = true;
                var planetVoxelScale = VoxelManager.instance.voxelSettings.planetVoxelScale;
                EntityManager.SetComponentData(this.worldPrefab, new VoxScale(planetVoxelScale));
            }
            var lightsSettings = GetSingleton<LightsSettings>();
            var renderSettings = GetSingleton<RenderSettings>();
            var renderDistance = (byte) renderSettings.renderDistance;
            var totalChunks = (renderDistance + renderDistance + 1) * (renderDistance + renderDistance + 1) * (renderDistance + renderDistance + 1);
            // UnityEngine.Debug.LogError("Total Loading Chunks: " + totalChunks);
            var disableClouds = WeatherManager.instance.weatherSettings.disableClouds;
            var dayTimeTransitionTime = WeatherManager.instance.weatherSettings.dayTimeTransitionTime;
            var timeSpeed = WeatherManager.instance.weatherSettings.timeSpeed;
            var timePerDay = WeatherManager.instance.weatherSettings.timePerDay;
            var timePerNight = WeatherManager.instance.weatherSettings.timePerNight; 
            var planetRadius = WorldsManager.instance.worldsSettings.planetRadius;
            var voxelDimensions = VoxelManager.instance.voxelSettings.voxelDimensions;
            var daySettings = WeatherManager.instance.weatherSettings.daySettings;
            var nightSettings = WeatherManager.instance.weatherSettings.nightSettings;
			var sunlight = lightsSettings.sunlight;
            var rainPrefab = RainSystem.rainPrefab;
            var texts = this.texts;
            var worldPrefab = this.worldPrefab;
            var disablePlanetIO = VoxelManager.instance.voxelSettings.disablePlanetIO;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            if (disablePlanetIO)
            {
                Entities.ForEach((int entityInQueryIndex, ref SpawnPlanet spawnPlanet) =>
                {
                    var realmEntity = spawnPlanet.realm;
                    if (HasComponent<InitializeEntity>(realmEntity) || HasComponent<GenerateRealm>(realmEntity) || !HasComponent<Realm>(realmEntity))
                    {
                        return;
                    }
                    spawnPlanet.worldID = IDUtil.GenerateUniqueID();
                }).WithoutBurst().Run();
            }
            else
            {
                Entities
                    .WithNone<DisableSaving>()
                    .WithAll<SpawnNewPlanet>()
                    .ForEach((ref SpawnPlanet spawnPlanet) =>
                {
                    var realmEntity = spawnPlanet.realm;
                    if (HasComponent<InitializeEntity>(realmEntity) || HasComponent<GenerateRealm>(realmEntity) || !HasComponent<Realm>(realmEntity))
                    {
                        return;
                    }
                    var realmID = EntityManager.GetComponentData<ZoxID>(spawnPlanet.realm).id;
                    var worldID = 0; // SaveUtilities.GetFirstWorldID(realmID);
                    if (!disablePlanetIO)
                    {
                        worldID = SaveUtilities.GetFirstWorldID(realmID);
                    }
                    if (worldID == 0)
                    {
                        worldID = IDUtil.GenerateUniqueID();
                        if (!disablePlanetIO)
                        {
                            SaveUtilities.CreateNewWorldFolder(realmID, worldID);
                        }
                        UnityEngine.Debug.Log("Creating First Realm World: " + worldID);
                    }
                    spawnPlanet.worldID = worldID;
                }).WithoutBurst().Run();
                Entities
                    .WithAll<SpawnNewPlanet, DisableSaving>()
                    .ForEach((ref SpawnPlanet spawnPlanet) =>
                {
                    var realmEntity = spawnPlanet.realm;
                    if (HasComponent<InitializeEntity>(realmEntity) || HasComponent<GenerateRealm>(realmEntity) || !HasComponent<Realm>(realmEntity))
                    {
                        return;
                    }
                    // var realmID = EntityManager.GetComponentData<ZoxID>(spawnPlanet.realm).id;
                    var worldID = IDUtil.GenerateUniqueID();
                    spawnPlanet.worldID = worldID;
                }).WithoutBurst().Run();
            }
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var cameraLinks = GetComponentLookup<CameraLink>(true);
            controllerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, in SpawnPlanet spawnPlanet) =>
            {
                var realmEntity = spawnPlanet.realm;
                if (HasComponent<InitializeEntity>(realmEntity) || HasComponent<GenerateRealm>(realmEntity) || !HasComponent<Realm>(realmEntity))
                {
                    return;
                }
                var worldID = spawnPlanet.worldID;
                if (worldID == 0)
                {
                    #if UNITY_EDITOR
                    //UnityEngine.Debug.LogError("New ID Using: " + newWorldSeed);
                    UnityEngine.Debug.LogError("worldID is 0.");
                    #endif
                    return;
                }
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                var planetEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, worldPrefab);
                // PostUpdateCommands.SetComponent(entityInQueryIndex, planetEntity, new Seed(worldID));
                PostUpdateCommands.SetComponent(entityInQueryIndex, planetEntity, new ZoxID(worldID));
                PostUpdateCommands.SetComponent(entityInQueryIndex, planetEntity, new Planet(planetRadius, voxelDimensions.x));
                PostUpdateCommands.SetComponent(entityInQueryIndex, planetEntity, new ChunkDimensions(voxelDimensions));
                PostUpdateCommands.SetComponent(entityInQueryIndex, planetEntity, new RealmLink(realmEntity));
                PostUpdateCommands.AddComponent<OnWorldsSpawned>(entityInQueryIndex, realmEntity);
                var random = new Random();
                random.InitState((uint) worldID);
                // todo: Make these Values adjustable through WeatherManager
                var skyColorHSV = new float3(
                    random.NextInt(0, 360 * 100) % 360,
                    32 + random.NextInt(0, 40 * 100) % 40,
                    36 + random.NextInt(0, 20 * 100) % 20);
                var skyColor = Color.GetColorFromHSV(skyColorHSV);
                // UnityEngine.Debug.LogError("SkyColor: " + skyColor + " ::: " + skyColorHSV + " from worldID: " + worldID);
                var sky = new Sky
                { 
                    fadeTime = dayTimeTransitionTime
                };
                sky.InitializeSkySettings(skyColor, in daySettings, in nightSettings);
                PostUpdateCommands.SetComponent(entityInQueryIndex, planetEntity, sky);
                PostUpdateCommands.SetComponent(entityInQueryIndex, planetEntity, new Skylight(sunlight));
                PostUpdateCommands.SetComponent(entityInQueryIndex, planetEntity, new WorldTime(timeSpeed, timePerDay, timePerNight));
                // Spawns Rain Cloud
                if (!disableClouds)
                {
                    var rainCloudEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, rainPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, rainCloudEntity, new VoxLink(planetEntity));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, planetEntity, new RainCloudLink(rainCloudEntity));
                }
                if (spawnPlanet.player.Index > 0 && HasComponent<VoxLink>(spawnPlanet.player))
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnPlanet.player, new VoxLink(planetEntity));
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, planetEntity, new ChunkLinks(totalChunks));
                #if UNITY_EDITOR
                PostUpdateCommands.SetComponent(entityInQueryIndex, planetEntity, new EditorName(texts[0].Clone()));
                #endif
                if (HasComponent<DisableSaving>(e))
                {
                    PostUpdateCommands.AddComponent<DisableSaving>(entityInQueryIndex, planetEntity);
                }
                if (cameraLinks.HasComponent(spawnPlanet.player))
                {
                    var cameraEntity = cameraLinks[spawnPlanet.player].camera;
                    PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new VoxLink(planetEntity));
                }
            })  .WithReadOnly(cameraLinks)
                #if UNITY_EDITOR
                .WithReadOnly(texts)
                #endif
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}