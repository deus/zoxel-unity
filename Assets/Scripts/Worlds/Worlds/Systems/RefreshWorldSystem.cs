using Unity.Entities;
using Unity.Burst;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! Sets the seed of a Vox, and sets it for all linked chunks.
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class RefreshWorldSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<OnChunksSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref SimplexNoise simplexNoise, ref Seed seed, in Vox vox, in RefreshWorld refreshWorld) =>
            {
                for (int i = 0; i < vox.chunks.Length; i++)
                {
                    var chunkEntity = vox.chunks[i];
                    if (chunkEntity.Index <= 0 || HasComponent<InitializeEntity>(chunkEntity))
                    {
                        return;
                    }
                }
                var worldSeed = refreshWorld.seed;
                if (seed.SetSeed(worldSeed))
                {
                    simplexNoise.SetValues(worldSeed);
                    for (int i = 0; i < vox.chunks.Length; i++)
                    {
                        var chunkEntity = vox.chunks[i];
                        PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, seed);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, simplexNoise);
                        PostUpdateCommands.AddComponent<GenerateChunk>(entityInQueryIndex, chunkEntity);
                    }
                }
                PostUpdateCommands.RemoveComponent<RefreshWorld>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}