using Unity.Entities;
using Unity.Burst;
using Zoxel.Voxels;
using Zoxel.Weather;
using Zoxel.Rendering;
using Zoxel.Textures;
using Zoxel.Textures.Tilemaps;

namespace Zoxel.Worlds
{
    //! Initializs materials for world.
    /**
    *   - Initialize System -
    */
	[BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class WorldMaterialInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        const string textureName = "_BaseMap";
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null) return;
            var voxelItemMaterial = MaterialsManager.instance.materials.voxelItemMaterial;
            var skyMaterial2 = WeatherManager.instance.WeatherSettings.skyMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, Planet>()
                .ForEach((Entity e, EntityMaterials entityMaterials, SkyMaterial skyMaterial, in RealmLink realmLink) =>
            {
                var realmEntity = realmLink.realm;
                var realmMaterialLinks = EntityManager.GetComponentData<MaterialLinks>(realmEntity);
                var materials = new UnityEngine.Material[realmMaterialLinks.materials.Length];
                // set tilemaps here using tilemapLinks
                for (int i = 0; i < materials.Length; i++)
                {
                    var materialEntity = realmMaterialLinks.materials[i];
                    var tilemapLinks = EntityManager.GetComponentData<TilemapLinks>(materialEntity);
                    var j = 0;
                    if (tilemapLinks.tilemaps.Length == 0)
                    {
                        return;
                    }
                    var tilemapEntity = tilemapLinks.tilemaps[j];
                    var material = EntityManager.GetSharedComponentManaged<UnityMaterial>(tilemapEntity).material;
                    materials[i] = material;
                    // Set item tilemap texture
                    if (i == 0 && voxelItemMaterial)
                    {
                        var tilemap = EntityManager.GetComponentData<Tilemap>(tilemapEntity);
                        PostUpdateCommands.SetComponent(e, new VoxItemMaterialTilemap(tilemap.gridSize));
                    }
                }
                PostUpdateCommands.SetSharedComponentManaged(e, new EntityMaterials(materials));
                if (skyMaterial.skyMaterial == null)
                {
                    UnityEngine.RenderSettings.skybox = skyMaterial2;
                    PostUpdateCommands.SetSharedComponentManaged(e, new SkyMaterial(skyMaterial2));
                }
                PostUpdateCommands.SetSharedComponentManaged(e, new VoxItemMaterial(voxelItemMaterial));
            }).WithoutBurst().Run();
        }
    }
}