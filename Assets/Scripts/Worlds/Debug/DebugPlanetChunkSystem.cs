﻿using Unity.Entities;
using Unity.Burst;

using Unity.Mathematics;
using Zoxel.Voxels;
using Zoxel.Lines;
// Todo: Draw Grid lines
// Todo: Make work in game as well, efficient line drawing system?

namespace Zoxel.Worlds
{
	[BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class DebugPlanetChunkSystem : SystemBase
    {
        // float timing = 0.01f;
        UnityEngine.Color lineColor = UnityEngine.Color.green;
        UnityEngine.Color lineColorEdge = new UnityEngine.Color(0.66f, 0.1f, 0.1f);
        UnityEngine.Color lineColorCorner = new UnityEngine.Color(0f, 0f, 0f);
        UnityEngine.Color lineColorMiddle = new UnityEngine.Color(0.8f, 0.8f, 0.8f);
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (WorldsManager.instance == null) return;
            if (!WorldsManager.instance.worldsSettings.isDebugPlanetChunks)
            {
                return;
            }
            var materialType = (byte) 0;
            var lineWidth = 0.025f;
            var lineLife = 0.3f;
            var planetRadius = WorldsManager.instance.worldsSettings.planetRadius;
            var lineLength = 32;
            var elapsedTime = World.Time.ElapsedTime;
            var linePrefab = RenderLineGroup.linePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<PlanetChunk2D>()
                .ForEach((Entity e, in QuadrantPosition2D quadrantPosition2D) =>
            {
                var position = new float3();
                var size = new float3(8, 8, 8);
                var position2 = new float3();
                if (quadrantPosition2D.quadrant == PlanetSide.Up)
                {
                    position.x = quadrantPosition2D.position.x * 16;
                    position.z = quadrantPosition2D.position.y * 16;
                    position.y = planetRadius;
                    position += new float3(8f, 0, 8f);
                    size.y = lineLength;
                    position2 = position + new float3(0, size.y, 0);
                }
                else if (quadrantPosition2D.quadrant == PlanetSide.Down)
                {
                    position.x = quadrantPosition2D.position.x * 16;
                    position.z = quadrantPosition2D.position.y * 16;
                    position.y = -planetRadius;
                    position += new float3(8f, 0, 8f);
                    size.y = lineLength;
                    position2 = position - new float3(0, size.y, 0);
                }
                else if (quadrantPosition2D.quadrant == PlanetSide.Right)
                {
                    position.y = quadrantPosition2D.position.x * 16;
                    position.z = quadrantPosition2D.position.y * 16;
                    position.x = planetRadius;
                    position += new float3(0, 8f, 8f);
                    size.x = lineLength;
                    position2 = position + new float3(size.x, 0, 0);
                }
                else if (quadrantPosition2D.quadrant == PlanetSide.Left)
                {
                    position.y = quadrantPosition2D.position.x * 16;
                    position.z = quadrantPosition2D.position.y * 16;
                    position.x = -planetRadius;
                    position += new float3(0, 8f, 8f);
                    size.x = lineLength;
                    position2 = position - new float3(size.x, 0, 0);
                }
                else if (quadrantPosition2D.quadrant == PlanetSide.Forward)
                {
                    position.x = quadrantPosition2D.position.x * 16;
                    position.y = quadrantPosition2D.position.y * 16;
                    position.z = planetRadius;
                    position += new float3(8f, 8f, 0);
                    size.z = lineLength;
                    position2 = position + new float3(0, 0, size.z);
                }
                else if (quadrantPosition2D.quadrant == PlanetSide.Backward)
                {
                    position.x = quadrantPosition2D.position.x * 16;
                    position.y = quadrantPosition2D.position.y * 16;
                    position.z = -planetRadius;
                    position += new float3(8f, 8f, 0);
                    size.z = lineLength;
                    position2 = position - new float3(0, 0, size.z);
                }
                // RenderLineGroup.CreateCubeLines(PostUpdateCommands, elapsedTime, position, quaternion.identity, size);
                RenderLineGroup.SpawnLine(PostUpdateCommands, linePrefab, elapsedTime, position, position2, materialType, lineWidth, lineLife);
            }).Run();
        }
    }
}