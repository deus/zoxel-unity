using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Lines;
// Todo: Draw Grid lines
// Todo: Make work in game as well, efficient line drawing system?

namespace Zoxel.Worlds
{
	[BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class DebugStructuresSystem : SystemBase
    {
        // float timing = 0.01f;
        UnityEngine.Color lineColor = UnityEngine.Color.green;
        UnityEngine.Color lineColorEdge = new UnityEngine.Color(0.66f, 0.1f, 0.1f);
        UnityEngine.Color lineColorCorner = new UnityEngine.Color(0f, 0f, 0f);
        UnityEngine.Color lineColorMiddle = new UnityEngine.Color(0.8f, 0.8f, 0.8f);
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (VoxelManager.instance == null) return;
            if (!VoxelManager.instance.voxelSettings.isDebugStructures)
            {
                return;
            }
            var size = new float3(0.5f, 4, 0.5f);
            var materialType = (byte) 0;
            var lineWidth = 0.025f;
            var lineLife = 0.3f;
            // var lineLength = 32;
            var elapsedTime = World.Time.ElapsedTime;
            var linePrefab = RenderLineGroup.linePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((Entity e, in Translation translation, in Structure structure) =>
            {
                // RenderLineGroup.CreateCubeLines(PostUpdateCommands, elapsedTime, translation.Value, quaternion.identity, size);
                var offsetA = new float3(-size.x, 0, -size.z);
                var offsetB = new float3(size.x, 0, -size.z);
                var offsetC = new float3(-size.x, 0, size.z);
                var offsetD = new float3(size.x, 0, size.z);
                var pointA = translation.Value;
                var pointB = pointA;
                if (structure.direction == PlanetSide.Up)
                {
                    pointB += new float3(0, size.y, 0);
                }
                else if (structure.direction == PlanetSide.Down)
                {
                    pointB += new float3(0, -size.y, 0);
                }
                else if (structure.direction == PlanetSide.Left)
                {
                    pointB += new float3(-size.y, 0, 0);
                    offsetA = new float3(0, -size.x, -size.z);
                    offsetB = new float3(0, size.x, -size.z);
                    offsetC = new float3(0, -size.x, size.z);
                    offsetD = new float3(0, size.x, size.z);
                }
                else if (structure.direction == PlanetSide.Right)
                {
                    pointB += new float3(size.y, 0, 0);
                    offsetA = new float3(0, -size.x, -size.z);
                    offsetB = new float3(0, size.x, -size.z);
                    offsetC = new float3(0, -size.x, size.z);
                    offsetD = new float3(0, size.x, size.z);
                }
                else if (structure.direction == PlanetSide.Backward)
                {
                    pointB += new float3(0, 0, -size.y);
                    offsetA = new float3(-size.x, -size.z, 0);
                    offsetB = new float3(size.x, -size.z, 0);
                    offsetC = new float3(-size.x, size.z, 0);
                    offsetD = new float3(size.x, size.z, 0);
                }
                else if (structure.direction == PlanetSide.Forward)
                {
                    pointB += new float3(0, 0, size.y);
                    offsetA = new float3(-size.x, -size.z, 0);
                    offsetB = new float3(size.x, -size.z, 0);
                    offsetC = new float3(-size.x, size.z, 0);
                    offsetD = new float3(size.x, size.z, 0);
                }
                // RenderLineGroup.SpawnLine(PostUpdateCommands, elapsedTime, translation.Value, translation.Value + new float3(0, 8, 0), materialType, lineWidth, lineLife);
                /*RenderLineGroup.SpawnLine(PostUpdateCommands, elapsedTime, pointA + offsetA, pointB + offsetA,
                    materialType, lineWidth, lineLife);
                RenderLineGroup.SpawnLine(PostUpdateCommands, elapsedTime, pointA + offsetB, pointB + offsetB,
                    materialType, lineWidth, lineLife);
                RenderLineGroup.SpawnLine(PostUpdateCommands, elapsedTime, pointA + offsetC, pointB + offsetC,
                    materialType, lineWidth, lineLife);
                RenderLineGroup.SpawnLine(PostUpdateCommands, elapsedTime, pointA + offsetD, pointB + offsetD,
                    materialType, lineWidth, lineLife);*/
                RenderLineGroup.SpawnLine(PostUpdateCommands, linePrefab, elapsedTime, pointA, pointB, materialType, lineWidth, lineLife);
            }).Run();
        }
    }
}