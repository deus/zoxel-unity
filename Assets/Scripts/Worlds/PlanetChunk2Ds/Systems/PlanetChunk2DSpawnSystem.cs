using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! Spawns PlanetChunk2D entities.
    /**
    *   - Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class PlanetChunk2DSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery planetChunkQuery;
        private EntityQuery busyChunksQuery;
        private Entity planetChunkPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            planetChunkQuery = GetEntityQuery(
                ComponentType.ReadOnly<PlanetChunk2D>(),
                ComponentType.ReadOnly<QuadrantPosition2D>());
            var planetChunkArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(PlanetChunk2D),
                typeof(QuadrantPosition2D),
                typeof(VoxLink),
                typeof(Seed),
                typeof(SimplexNoise),
                typeof(TerrainHeightMap),
                typeof(ChunkBiome)
            );
            planetChunkPrefab = EntityManager.CreateEntity(planetChunkArchetype);
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var outerPlanetChunkEdge = WorldsManager.instance.worldsSettings.planetHeightRadius / 16; // 1;
            var planetChunkPrefab = this.planetChunkPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var planetChunkEntities = planetChunkQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var planetChunkPositions = GetComponentLookup<QuadrantPosition2D>(true);
            planetChunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<OnSpawnedPlanetChunks>()
                .WithAll<StreamVox>()
                .ForEach((Entity e, int entityInQueryIndex, in PlanetChunk2DLinks planetChunk2DLinks, in StreamableVox streamableVox,
                    in Planet planet, in SimplexNoise simplexNoise, in Seed seed, in ZoxID zoxID) =>
            {
                // find new chunks to spawn
                var spawnPositions = new NativeList<int3>();
                var centralPosition = streamableVox.position;
                var renderDistance = streamableVox.renderDistance; // 6;
                var quadrant = streamableVox.quadrant;
                var planetChunkRadius = planet.GetChunkRadius();
                var expand = 5;
                var lowerBounds = centralPosition + new int3(-(renderDistance + expand), -(renderDistance + expand), -(renderDistance + expand));
                var upperBounds = centralPosition + new int3(renderDistance + expand, renderDistance + expand, renderDistance + expand);
                var planetLowerBounds = new int3(- planetChunkRadius, - planetChunkRadius, - planetChunkRadius);
                var planetUpperBounds = new int3(planetChunkRadius, planetChunkRadius, planetChunkRadius);
                var keepChunksCount = 0;
                int3 surfacePosition;
                int3 position;
                var planetLowerBounds2 = planetLowerBounds - new int3(outerPlanetChunkEdge, outerPlanetChunkEdge, outerPlanetChunkEdge);
                var planetUpperBounds2 = planetUpperBounds + new int3(outerPlanetChunkEdge, outerPlanetChunkEdge, outerPlanetChunkEdge);
                var planetSideNegative = new int3((int) PlanetSide.Left, (int) PlanetSide.Down, (int) PlanetSide.Backward);
                var planetSidePositive = new int3((int) PlanetSide.Right, (int) PlanetSide.Up, (int) PlanetSide.Forward);
                // UnityEngine.Profiling.Profiler.BeginSample("PlanetChunk2DStreamingGatherPositions");
                for (position.x = lowerBounds.x; position.x <= upperBounds.x; position.x++)
                {
                    for (position.z = lowerBounds.z; position.z <= upperBounds.z; position.z++)
                    {
                        // Surface Y has X Z
                        if (position.x >= planetLowerBounds2.x && position.x <= planetUpperBounds2.x && position.z >= planetLowerBounds2.z && position.z <= planetUpperBounds2.z)
                        {
                            surfacePosition.x = position.x;
                            surfacePosition.y = position.z;
                            if (upperBounds.y >= planetUpperBounds.y)
                            {
                                surfacePosition.z = planetSidePositive.y;
                                spawnPositions.Add(surfacePosition);
                            }
                            if (lowerBounds.y <= planetLowerBounds.y)
                            {
                                surfacePosition.z = planetSideNegative.y;
                                spawnPositions.Add(surfacePosition);
                            }
                        }
                    }
                }
                for (position.y = lowerBounds.y; position.y <= upperBounds.y; position.y++)
                {
                    for (position.z = lowerBounds.z; position.z <= upperBounds.z; position.z++)
                    {
                        // Surface X has Y Z
                        if (position.y >= planetLowerBounds2.y && position.y <= planetUpperBounds2.y && position.z >= planetLowerBounds2.z && position.z <= planetUpperBounds2.z)
                        {
                            surfacePosition.x = position.y;
                            surfacePosition.y = position.z;
                            if (upperBounds.x >= planetUpperBounds.x)
                            {
                                surfacePosition.z = planetSidePositive.x;
                                spawnPositions.Add(surfacePosition);
                            }
                            if (lowerBounds.x <= planetLowerBounds.x)
                            {
                                surfacePosition.z = planetSideNegative.x;
                                spawnPositions.Add(surfacePosition);
                            }
                        }
                    }
                }
                for (position.x = lowerBounds.x; position.x <= upperBounds.x; position.x++)
                {
                    for (position.y = lowerBounds.y; position.y <= upperBounds.y; position.y++)
                    {
                        // Surface Z has X Y
                        if (position.x >= planetLowerBounds2.x && position.x <= planetUpperBounds2.x && position.y >= planetLowerBounds2.y && position.y <= planetUpperBounds2.y)
                        {
                            surfacePosition.x = position.x;
                            surfacePosition.y = position.y;
                            if (upperBounds.z >= planetUpperBounds.z)
                            {
                                surfacePosition.z = planetSidePositive.z;
                                spawnPositions.Add(surfacePosition);
                            }
                            if (lowerBounds.z <= planetLowerBounds.z)
                            {
                                surfacePosition.z = planetSideNegative.z;
                                spawnPositions.Add(surfacePosition);
                            }
                        }
                    }
                }
                // UnityEngine.Profiling.Profiler.EndSample();
               // UnityEngine.Profiling.Profiler.BeginSample("PlanetChunk2DStreamingGatherPositions2");
                if (planetChunk2DLinks.chunks.Length > 0)
                {
                    // add previous chunks to hashmap
                    for (int i = 0; i < planetChunk2DLinks.chunks.Length; i++)
                    {
                        var chunkEntity = planetChunk2DLinks.chunks[i];
                        var chunkPosition = planetChunkPositions[chunkEntity].GetDictionaryPosition();
                        var spawnPositionsIndex = spawnPositions.IndexOf(chunkPosition);
                        // If contains position within render bounds
                        if (spawnPositionsIndex > 0)
                        {
                            spawnPositions.RemoveAt(spawnPositionsIndex);
                            keepChunksCount++;
                        }
                        else
                        {
                            PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, chunkEntity);
                        }
                    }
                }
                // Spawn New Chunks here
                // maybe base off different prefabs
                var voxLink = new VoxLink(e);
                for (int i = 0; i < spawnPositions.Length; i++)
                {
                    var chunkEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, planetChunkPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, voxLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, simplexNoise);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, seed);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, new QuadrantPosition2D(spawnPositions[i]));
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnSpawnedPlanetChunks(spawnPositions.Length + keepChunksCount));  // incase removed
                // UnityEngine.Profiling.Profiler.EndSample();
                spawnPositions.Dispose();
            })  .WithReadOnly(planetChunkPositions)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

// Debug
/*UnityEngine.Debug.LogError("Planet Chunks Spawned: " + chunkSpawnPositions.Length
    + "\nAt position: " + centralPosition + " and render distance: " + renderDistance
    + "\nWith planetChunkRadius: " + planetChunkRadius + " - " + ((PlanetSide) quadrant)
    + "\nWith Render Lower Bounds: " + lowerBounds.x + ":" + lowerBounds.y
    + "\nWith Render Upper Bounds: " + upperBounds.x + ":" + upperBounds.y
    + "\nWith Planet Lower Bounds: " + planetLowerBounds.x + ":" + planetLowerBounds.y
    + "\nWith Planet Upper Bounds: " + planetUpperBounds.x + ":" + planetUpperBounds.y);*/
/*for (int i = 0; i < chunkSpawnPositions.Length; i++)
{
    UnityEngine.Debug.LogError("New Planet Chunks at [" + i + "]: " + chunkSpawnPositions[i]);
}*/

// destroy any ones that wern't added
/*foreach (var KVP in previousChunks)
{
    var previousChunkPosition = KVP.Key;
    if (!spawnPositions.Contains(previousChunkPosition))
    {
        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, KVP.Value);
    }
    else
    {
        keepChunksCount++;
    }
}
// for all bounds positions - if not in previous chunks - spawn
for (int i = spawnPositions.Length - 1; i >= 0; i--)
{
    var spawnPosition = spawnPositions[i];
    if (previousChunks.ContainsKey(spawnPosition))
    {
        spawnPositions.RemoveAt(i);
    }
}*/

/*for (position.x = lowerBounds.x; position.x <= upperBounds.x; position.x++)
{
    for (position.y = lowerBounds.y; position.y <= upperBounds.y; position.y++)
    {
        for (position.z = lowerBounds.z; position.z <= upperBounds.z; position.z++)
        {
            // for each chunk position
            // get position on surface
            // Surface Y has X Z
            if (position.x >= planetLowerBounds2.x && position.x <= planetUpperBounds2.x && position.z >= planetLowerBounds2.z && position.z <= planetUpperBounds2.z)
            {
                surfacePosition.x = position.x;
                surfacePosition.y = position.z;
                if (position.y >= planetUpperBounds.y)
                {
                    surfacePosition.z = planetSidePositive.y;
                    if (!spawnPositions.Contains(surfacePosition))
                    {
                        spawnPositions.Add(surfacePosition);
                    }
                }
                else if (position.y <= planetLowerBounds.y)
                {
                    surfacePosition.z = planetSideNegative.y;
                    if (!spawnPositions.Contains(surfacePosition))
                    {
                        spawnPositions.Add(surfacePosition);
                    }
                }
            }
            // Surface X has Y Z
            if (position.y >= planetLowerBounds2.y && position.y <= planetUpperBounds2.y && position.z >= planetLowerBounds2.z && position.z <= planetUpperBounds2.z)
            {
                surfacePosition.x = position.y;
                surfacePosition.y = position.z;
                if (position.x >= planetUpperBounds.x)
                {
                    surfacePosition.z = planetSidePositive.x;
                    if (!spawnPositions.Contains(surfacePosition))
                    {
                        spawnPositions.Add(surfacePosition);
                    }
                }
                else if (position.x <= planetLowerBounds.x)
                {
                    surfacePosition.z = planetSideNegative.x;
                    if (!spawnPositions.Contains(surfacePosition))
                    {
                        spawnPositions.Add(surfacePosition);
                    }
                }
            }
            // Surface Z has X Y
            if (position.x >= planetLowerBounds2.x && position.x <= planetUpperBounds2.x && position.y >= planetLowerBounds2.y && position.y <= planetUpperBounds2.y)
            {
                surfacePosition.x = position.x;
                surfacePosition.y = position.y;
                if (position.z >= planetUpperBounds.z)
                {
                    surfacePosition.z = planetSidePositive.z;
                    if (!spawnPositions.Contains(surfacePosition))
                    {
                        spawnPositions.Add(surfacePosition);
                    }
                }
                else if (position.z <= planetLowerBounds.z)
                {
                    surfacePosition.z = planetSideNegative.z;
                    if (!spawnPositions.Contains(surfacePosition))
                    {
                        spawnPositions.Add(surfacePosition);
                    }
                }
            }
        }
    }
}*/