using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Worlds
{
    //! Linking PlanetChunk2Ds after they have spawned.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class PlanetChunk2DsSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery planetChunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            planetChunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<PlanetChunk2D>(),
                ComponentType.ReadOnly<VoxLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var planetChunk2DEntities = planetChunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref PlanetChunk2DLinks planetChunk2DLinks, ref OnSpawnedPlanetChunks onPlanetChunksSpawned) =>
            {
                if (onPlanetChunksSpawned.spawned != 0)
                {
                    planetChunk2DLinks.Dispose();
                    planetChunk2DLinks.AddNew(onPlanetChunksSpawned.spawned);
                    onPlanetChunksSpawned.spawned = 0;
                }
                var count = 0;
                for (int i = 0; i < planetChunk2DEntities.Length; i++)
                {
                    var e2 = planetChunk2DEntities[i];
                    var voxLink = voxLinks[e2];
                    if (voxLink.vox == e)
                    {
                        planetChunk2DLinks.chunks[count] = e2;
                        count++;
                        if (count >= planetChunk2DLinks.chunks.Length)
                        {
                            break;
                        }
                    }
                }
                if (count == planetChunk2DLinks.chunks.Length)
                {
                    // UnityEngine.Debug.LogError("    - On Planet Chunks Spawned: " + count);
                    PostUpdateCommands.RemoveComponent<OnSpawnedPlanetChunks>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(planetChunk2DEntities)
                .WithDisposeOnCompletion(planetChunk2DEntities)
                .WithReadOnly(voxLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
} 
