using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Worlds
{
	//! Cleans up ChunkPlanetChunk2DLinks on DestroyEntity event.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class PlanetChunk2DLinksDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, ChunkPlanetChunk2DLinks>()
                .ForEach((in ChunkPlanetChunk2DLinks chunkPlanetChunk2DLinks) =>
			{
                chunkPlanetChunk2DLinks.DisposeFinal();
			}).ScheduleParallel();
		}
	}
}