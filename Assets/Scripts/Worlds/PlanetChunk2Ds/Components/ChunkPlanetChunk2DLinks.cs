using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Worlds
{
    //! Links from a Chunk to PlanetChunk2D entities.
    public struct ChunkPlanetChunk2DLinks : IComponentData
    {
        public bool3 quadrant;
        public BlitableArray<Entity> chunks;
        
        public void DisposeFinal()
        {
            chunks.DisposeFinal();
        }
        
        public void Dispose()
        {
            chunks.Dispose();
        }

        public void SetChunks(in NativeList<Entity> chunks, bool3 quadrant)
        {
            this.Dispose();
            this.quadrant = quadrant;
            // this.chunks = new BlitableArray<Entity>(chunks, Allocator.Persistent);
            this.chunks = new BlitableArray<Entity>(chunks.Length, Allocator.Persistent);
            for (int i = 0; i < chunks.Length; i++)
            {
                this.chunks[i] = chunks[i];
            }
        }
    }
}