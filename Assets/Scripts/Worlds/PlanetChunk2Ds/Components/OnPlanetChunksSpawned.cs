using Unity.Entities;

namespace Zoxel.Worlds
{
    public struct OnSpawnedPlanetChunks : IComponentData
    {
        public int spawned;

        public OnSpawnedPlanetChunks(int spawned)
        {
            this.spawned = spawned;
        }
    }
}