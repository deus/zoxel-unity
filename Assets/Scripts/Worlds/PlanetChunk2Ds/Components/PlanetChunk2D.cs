using Unity.Entities;

namespace Zoxel.Worlds
{
    //! A 2D chunk that stores maps used for 3D chunk generation.
    public struct PlanetChunk2D : IComponentData { }
}