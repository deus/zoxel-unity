using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels.Lighting;
using Zoxel.Worlds;

namespace Zoxel.Voxels
{
	//! Generates ChunkGravity's by using Planet radius data.
	[BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ChunkGravityGenerateSystem : SystemBase
	{
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;

		protected override void OnCreate()
		{
            voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<ChunkDimensions>());
            RequireForUpdate(processQuery);
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
            var maxDepth = VoxelManager.instance.voxelSettings.maxGravityDepth;
			var planetStretch = new float3(1, 1, 1);
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var planets = GetComponentLookup<Planet>(true);
            var chunkDimensions = GetComponentLookup<ChunkDimensions>(true);
            voxEntities.Dispose();
            //! This needs to be set based on planet generation!
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<InitializeEntity, PlanetChunk>()
                .ForEach((ref ChunkGravity chunkGravity, in ChunkPosition chunkPosition, in VoxLink voxLink) =>
			{
                if (!chunkDimensions.HasComponent(voxLink.vox))
                {
                    return;
                }
                var planet = planets[voxLink.vox];
                var voxelDimensions = chunkDimensions[voxLink.vox].voxelDimensions;
                var planetRadius = voxelDimensions.y * (math.floor(planet.radius / voxelDimensions.y)); //  + 1  - 1
                var planetLowerBounds = - (new int3((int) (planetRadius * planetStretch.x), (int) (planetRadius * planetStretch.y), (int) (planetRadius * planetStretch.z)));
                var planetUpperBounds = new int3((int) (planetRadius * planetStretch.x), (int) (planetRadius * planetStretch.y), (int) (planetRadius * planetStretch.z));
                int3 chunkLowerBounds;
                int3 chunkUpperBounds;
                chunkPosition.GetChunkBounds(voxelDimensions, out chunkLowerBounds, out chunkUpperBounds);
                // UnityEngine.Debug.LogError("chunkLowerBounds: " + chunkLowerBounds + " planetLowerBounds: " + planetLowerBounds + ", planetUpperBounds: " + planetUpperBounds);
                var planetSide = PlanetSide.None;
                var depth = (byte) 0;
                var xzBuffer = math.abs(chunkUpperBounds.y - planetUpperBounds.y);
                if (chunkUpperBounds.y <= planetLowerBounds.y)
                {
                    xzBuffer = math.abs(chunkLowerBounds.y - planetUpperBounds.y);
                }
                var isInXZBounds = chunkLowerBounds.x >= planetLowerBounds.x - xzBuffer && chunkUpperBounds.x <= planetUpperBounds.x + xzBuffer &&
                    chunkLowerBounds.z >= planetLowerBounds.z - xzBuffer && chunkUpperBounds.z <= planetUpperBounds.z + xzBuffer;
                var yzBuffer = math.abs(chunkUpperBounds.x - planetUpperBounds.x);
                if (chunkUpperBounds.x <= planetLowerBounds.x)
                {
                    yzBuffer = math.abs(chunkLowerBounds.x - planetUpperBounds.x);
                }
                var isInYZBounds = chunkLowerBounds.y >= planetLowerBounds.y - yzBuffer && chunkUpperBounds.y <= planetUpperBounds.y + yzBuffer  &&
                    chunkLowerBounds.z >= planetLowerBounds.z - yzBuffer  && chunkUpperBounds.z <= planetUpperBounds.z + yzBuffer;
                var xyBuffer = math.abs(chunkUpperBounds.z - planetUpperBounds.z);
                if (chunkUpperBounds.z <= planetLowerBounds.z)
                {
                    xyBuffer = math.abs(chunkLowerBounds.z - planetUpperBounds.z);
                }
                var isInXYBounds = chunkLowerBounds.y >= planetLowerBounds.y - xyBuffer && chunkUpperBounds.y <= planetUpperBounds.y + xyBuffer &&
                    chunkLowerBounds.x >= planetLowerBounds.x - xyBuffer && chunkUpperBounds.x <= planetUpperBounds.x + xyBuffer;
                // now generate gravity
                if ((chunkLowerBounds.y >= planetUpperBounds.y || chunkUpperBounds.y <= planetLowerBounds.y) &&
                    (chunkUpperBounds.x <= planetLowerBounds.x || chunkLowerBounds.x >= planetUpperBounds.x || 
                    chunkUpperBounds.z <= planetLowerBounds.z || chunkLowerBounds.z >= planetUpperBounds.z))
                {
                    if (chunkLowerBounds.y >= planetUpperBounds.y)
                    {
                        planetSide = PlanetSide.Up;
                    }
                    else
                    {
                        planetSide = PlanetSide.Down;
                    }
                    depth = maxDepth;
                }
                else if ((chunkUpperBounds.z <= planetLowerBounds.z || chunkLowerBounds.z >= planetUpperBounds.z) &&
                    (chunkUpperBounds.x <= planetLowerBounds.x || chunkLowerBounds.x >= planetUpperBounds.x || 
                    chunkLowerBounds.y >= planetUpperBounds.y || chunkUpperBounds.y <= planetLowerBounds.y))
                {
                    if (chunkLowerBounds.z >= planetUpperBounds.z)
                    {
                        planetSide = PlanetSide.Forward;
                    }
                    else
                    {
                        planetSide = PlanetSide.Backward;
                    }
                    depth = maxDepth;
                }
                else if ((chunkUpperBounds.x <= planetLowerBounds.x || chunkLowerBounds.x >= planetUpperBounds.x) &&
                    (chunkLowerBounds.y >= planetUpperBounds.y || chunkUpperBounds.y <= planetLowerBounds.y || 
                    chunkUpperBounds.z <= planetLowerBounds.z || chunkLowerBounds.z >= planetUpperBounds.z))
                {
                    if (chunkLowerBounds.x >= planetUpperBounds.x)
                    {
                        planetSide = PlanetSide.Right;
                    }
                    else
                    {
                        planetSide = PlanetSide.Left;
                    }
                    depth = maxDepth;
                }
                // normal places
                else if (isInYZBounds && chunkUpperBounds.x <= planetLowerBounds.x)
                {
                    planetSide = PlanetSide.Left;
                }
                else if (isInYZBounds && chunkLowerBounds.x >= planetUpperBounds.x)
                {
                    planetSide = PlanetSide.Right;
                }
                else if (isInXZBounds && chunkUpperBounds.y <= planetLowerBounds.y)
                {
                    planetSide = PlanetSide.Down;
                }
                else if (isInXZBounds && chunkLowerBounds.y >= planetUpperBounds.y)
                {
                    planetSide = PlanetSide.Up;
                }
                else if (isInXYBounds && chunkUpperBounds.z <= planetLowerBounds.z)
                {
                    planetSide = PlanetSide.Backward;
                }
                else if (isInXYBounds && chunkLowerBounds.z >= planetUpperBounds.z)
                {
                    planetSide = PlanetSide.Forward;
                }
                chunkGravity.Initialize(planetSide, depth);
            })  .WithReadOnly(planets)
                .WithReadOnly(chunkDimensions)
                .ScheduleParallel(Dependency);
		}
	}
}
                    //chunkGravity.Initialize(planetSide, depth);
                    /*gravity.SetValue(new int3(0, 0, 0), (byte) 1, PlanetSide.Forward);
                    gravity.SetValue(new int3(1, 0, 0), (byte) 1, PlanetSide.Forward);
                    gravity.SetValue(new int3(0, 0, 1), (byte) 1, PlanetSide.Forward);
                    gravity.SetValue(new int3(1, 0, 1), (byte) 1, PlanetSide.Forward);*/
                    // gravity.SetValue(new byte3(2, 0, 2), depth, dimensions, PlanetSide.Forward);
                    /*var gravity = chunkGravity.gravity;
                    var dimensions = OctNodeHelper.GetDimensions(depth);
                    for (byte i = 0; i < dimensions; i++)
                    {
                        for (byte j = 0; j < 1; j++)
                        {
                            for (byte k = (byte) (dimensions / 2); k < dimensions; k++)
                            {
                                gravity.SetValue(new byte3(i, j, k), depth, dimensions, PlanetSide.Forward);
                            }
                        }
                    }
                    chunkGravity.gravity = gravity;*/
                /*if (isInXZBounds && isInXYBounds && chunkLowerBounds.y >= planetUpperBounds.y)  //  && !isInYZBounds
                {
                    planetSide = PlanetSide.Up;
                    depth = 4;
                }
                else if (isInXZBounds && isInXYBounds && chunkLowerBounds.z >= planetUpperBounds.z)
                {
                    planetSide = PlanetSide.Forward;
                    depth = 4;
                    //chunkGravity.Initialize(planetSide, depth);
                    // chunkGravity.gravity.SetValue(new int3(1, 0, 0), (byte) 1, PlanetSide.Down);
                    // chunkGravity.gravity.SetValue(new int3(1, 0, 1), (byte) 1, PlanetSide.Down);
                }*/