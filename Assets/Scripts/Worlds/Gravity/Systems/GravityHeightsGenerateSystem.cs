using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
	//! Generates Gravity per ChunkHeights height map.
    /**
    *   This basically mirror height data for gravity around world edges.
    */
    [UpdateAfter(typeof(ChunkHeightsGenerationSystem))]
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class GravityHeightsGenerateSystem : SystemBase
	{
		[BurstCompile]
        protected override void OnUpdate()
        {
            if (VoxelManager.instance == null) return;
            var maxDepth = VoxelManager.instance.voxelSettings.maxGravityDepth;
            var iteration = (byte) 2;
            Dependency = Entities
				.WithNone<UpdateChunkNeighbors>()
				.WithAll<GenerateHeights>()
				.ForEach((ref ChunkGravity chunkGravity, in ChunkHeights chunkHeights, in Chunk chunk) =>
            {
                if (chunkGravity.depth != maxDepth || chunkHeights.heights.Length == 0)
                {
                    return;
                }
                var voxelDimensions = chunk.voxelDimensions;
                var gravity = chunkGravity.gravity;
                var dimensions = OctNodeHelper.GetDimensions(chunkGravity.depth);
                var localPosition = byte3.zero;
                for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x += iteration)
                {
                    for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y += iteration)
                    {
                        for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z += iteration)
                        {
                            var voxelIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
                            var blockDirection = chunkHeights.heights[voxelIndex].blockDirection;
                            if (blockDirection != PlanetSide.None)
                            {
                                gravity.SetValue(localPosition / iteration, chunkGravity.depth, dimensions, blockDirection);
                            }
                        }
                    }
                }
                chunkGravity.gravity = gravity;
            }).ScheduleParallel(Dependency);
		}
    }
}