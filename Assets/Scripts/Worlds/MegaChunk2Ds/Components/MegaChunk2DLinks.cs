
using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Worlds
{
    //! Links to MegaChunk entities from a Planet.
    public struct MegaChunk2DLinks : IComponentData
    {
        public byte init;
        public byte centralQuadrant;
        public int2 centralPosition;
        public BlitableArray<Entity> megaChunks;

        public void DisposeFinal()
        {
            megaChunks.DisposeFinal();
        }

        public void Initialize(int size)
        {
                megaChunks.Dispose();
            megaChunks = new BlitableArray<Entity>(size, Allocator.Persistent);
            for (int i = 0; i < megaChunks.Length; i++)
            {
                megaChunks[i] = new Entity();
            }
        }

        public void SetAs(int newCount)
        {
			if (megaChunks.Length != newCount)
			{
                megaChunks.Dispose();
				megaChunks = new BlitableArray<Entity>(newCount, Allocator.Persistent);
				for (int i = 0; i < newCount; i++)
				{
					megaChunks[i] = new Entity();
				}
			}
        }
    }
}