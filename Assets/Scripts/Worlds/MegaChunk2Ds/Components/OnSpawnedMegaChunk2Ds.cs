using Unity.Entities;

namespace Zoxel.Worlds
{
    //! An event for linking spawned MegaChunk2D's to the Planet.
    public struct OnSpawnedMegaChunk2Ds : IComponentData
    {
        public int spawned;

        public OnSpawnedMegaChunk2Ds(int spawned)
        {
            this.spawned = spawned;
        }
    }
}