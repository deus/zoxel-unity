
using Unity.Entities;

namespace Zoxel.Worlds
{
    //! A Chunk's way to link to a MegaChunk2D.
    public struct MegaChunk2DLink : IComponentData
    {
        public Entity megaChunk2D;
    }
}