using Unity.Entities;

namespace Zoxel.Worlds
{
    //! A tag for a bigger chunk on the Planet's surface.
    /**
    *   Each MegaChunk2D will have Towns, Highways, Dungeons, Monster Spawn Zones and Biomes.
    */
    public struct MegaChunk2D : IComponentData { }
}