using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! Spawns MegaChunk2D entities using the StreamVox event.
    /**
    *   - Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class MegaChunk2DsSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery megaChunk2DQuery;
        private Entity megaChunk2DPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            megaChunk2DQuery = GetEntityQuery(
                ComponentType.ReadOnly<MegaChunk2D>(),
                ComponentType.Exclude<DestroyEntity>());
            var megaChunk2DArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(MegaChunk2D),
                typeof(QuadrantPosition2D),
                typeof(GenerateTownMap2D),
                typeof(VoxLink),
                typeof(Seed),
                typeof(SimplexNoise),
                typeof(TownMap2D)
            );
            megaChunk2DPrefab = EntityManager.CreateEntity(megaChunk2DArchetype);
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var megaChunk2DRenderDistance = 1;
            var megaChunkDivision = WorldsManager.instance.worldsSettings.megaChunkDivision;
            var megaChunkPrefab = this.megaChunk2DPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var megaChunk2DEntities = megaChunk2DQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var planetChunk2DPositions = GetComponentLookup<QuadrantPosition2D>(true);
            megaChunk2DEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, OnSpawnedMegaChunks>()
                .WithAll<StreamVox>()
                .ForEach((Entity e, int entityInQueryIndex, ref MegaChunk2DLinks megaChunk2DLinks, in StreamableVox streamableVox, in Seed seed, in SimplexNoise simplexNoise) =>
            {
                // First find positions
                var centralPosition2 = VoxelUtilities.GetMegaChunkPosition(streamableVox.position, megaChunkDivision);
                var centralPosition = new int2(centralPosition2.x, centralPosition2.z);
                if (!(megaChunk2DLinks.init == 0 || centralPosition != megaChunk2DLinks.centralPosition))
                {
                    return;
                }
                megaChunk2DLinks.init = 1;
                megaChunk2DLinks.centralPosition = centralPosition;
                // should check if central megachunk position is updated
                var lowerBounds = centralPosition + new int2(-megaChunk2DRenderDistance, -megaChunk2DRenderDistance);
                var upperBounds = centralPosition + new int2(megaChunk2DRenderDistance, megaChunk2DRenderDistance);
                // Get new MegaChunkPositions
                var spawnPositions = new NativeList<int2>();
                var spawnQuadrants = new NativeList<byte>();
                int2 position;
                var quadrant = PlanetSide.Up;
                for (position.x = lowerBounds.x; position.x <= upperBounds.x; position.x++)
                {
                    for (position.y = lowerBounds.y; position.y <= upperBounds.y; position.y++)
                    {
                        // if doesn't exist, add to list
                        var doesExist = false;
                        // does exist
                        for (int i = 0; i < megaChunk2DLinks.megaChunks.Length; i++)
                        {
                            var megaChunk = megaChunk2DLinks.megaChunks[i];
                            if (!planetChunk2DPositions.HasComponent(megaChunk))
                            {
                                return;
                            }
                            var quadrantPosition2D = planetChunk2DPositions[megaChunk];
                            if (quadrantPosition2D.position == position && quadrantPosition2D.quadrant == quadrant)
                            {
                                doesExist = true;
                                break;
                            }
                        }
                        if (!doesExist)
                        {
                            spawnPositions.Add(position);
                            spawnQuadrants.Add(quadrant);
                        }
                    }
                }
                
                // remove old Megachunks
                var megaChunksCount = 0;
                for (int i = 0; i < megaChunk2DLinks.megaChunks.Length; i++)
                {
                    var megaChunk = megaChunk2DLinks.megaChunks[i];
                    var position2 = planetChunk2DPositions[megaChunk].position;
                    if (position2.x < lowerBounds.x || position2.x > upperBounds.x || position2.y < lowerBounds.y || position2.y > upperBounds.y)
                    {
                        //removeMegaChunks.Add(megaChunk);
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, megaChunk);
                    }
                    else
                    {
                        // megaChunks.Add(megaChunk);
                        megaChunksCount++;
                    }
                }

                // Spawn New Chunks here
                for (int i = 0; i < spawnPositions.Length; i++)
                {
                    var spawnPosition = spawnPositions[i];
                    var spawnQuadrant = spawnQuadrants[i];
                    var megaChunkEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, megaChunkPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, megaChunkEntity, new VoxLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, megaChunkEntity, new QuadrantPosition2D(spawnPosition, spawnQuadrant));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, megaChunkEntity, seed);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, megaChunkEntity, simplexNoise);
                    //UnityEngine.Debug.LogError("Mega Chunk Spawned [" + i + "] " + spawnPosition);
                }
                megaChunksCount += spawnPositions.Length;
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnSpawnedMegaChunk2Ds(megaChunksCount));
                //UnityEngine.Debug.LogError("Mega Chunk Total: " + megaChunks.Length + " :: " + centralPosition + " --- " + streamMegaChunk.position
                //    + ", upperBounds: " + upperBounds + ", lowerBounds: " + lowerBounds + ", streamMegaChunk.position: " + streamMegaChunk.position);
                spawnPositions.Dispose();
                spawnQuadrants.Dispose();
            })  .WithReadOnly(planetChunk2DPositions)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}