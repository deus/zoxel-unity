using Unity.Entities;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;

namespace Zoxel.Worlds
{
    //! After a Vox spawns MegaChunk2D's, links MegaChunk2D's to Vox's.
    /**
    *   Links Vox to MegaChunk2Ds.
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class MegaChunk2DsSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery megaChunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            megaChunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<MegaChunk2D>(),
                ComponentType.ReadOnly<VoxLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var megaChunkEntities = megaChunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref MegaChunk2DLinks megaChunk2DLinks, ref OnSpawnedMegaChunk2Ds onSpawnedMegaChunk2Ds) =>
            {
                if (onSpawnedMegaChunk2Ds.spawned != 0)
                {
                    megaChunk2DLinks.SetAs(onSpawnedMegaChunk2Ds.spawned);
                    onSpawnedMegaChunk2Ds.spawned = 0;
                }
                var count = 0;
                for (int i = 0; i < megaChunkEntities.Length; i++)
                {
                    var e2 = megaChunkEntities[i];
                    var voxLink = voxLinks[e2];
                    if (voxLink.vox == e)
                    {
                        megaChunk2DLinks.megaChunks[count] = e2;
                        count++;
                        if (count >= megaChunk2DLinks.megaChunks.Length)
                        {
                            break;
                        }
                    }
                }
                if (count == megaChunk2DLinks.megaChunks.Length)
                {
                    // link megachunks to vox
                    PostUpdateCommands.RemoveComponent<OnSpawnedMegaChunk2Ds>(entityInQueryIndex, e);
                }
                /*else
                {
                    UnityEngine.Debug.LogError("Could not link MegaChunk2Ds.");
                }*/
            })  .WithReadOnly(megaChunkEntities)
                .WithDisposeOnCompletion(megaChunkEntities)
                .WithReadOnly(voxLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
} 
