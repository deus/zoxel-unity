using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Worlds
{
    //! Destroys MegaChunk2Ds.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class MegaChunkLinks2DDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithAll<DestroyEntity, MegaChunk2DLinks>()
				.ForEach((int entityInQueryIndex, in MegaChunk2DLinks megaChunk2DLinks) =>
			{
				for (int i = 0; i < megaChunk2DLinks.megaChunks.Length; i++)
				{
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex,  megaChunk2DLinks.megaChunks[i]);
				}
                megaChunk2DLinks.DisposeFinal();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}