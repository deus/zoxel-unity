using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! While generating Chunk's, it links a chunk to a MegaChunk.
    /**
    *   - Linking System -
    *   Links a Chunk to a MegaChunk2D.
    */
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class MegaChunk2DLinkSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery megaChunk2DsQuery;

        protected override void OnCreate()
        {
        	megaChunk2DsQuery = GetEntityQuery(
                ComponentType.ReadOnly<MegaChunk2D>(),
                ComponentType.ReadOnly<VoxLink>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var megaChunkDivision = WorldsManager.instance.worldsSettings.megaChunkDivision;
            var megaChunk2DEntities = megaChunk2DsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var quadrantPosition2Ds = GetComponentLookup<QuadrantPosition2D>(true);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref GenerateChunk generateChunk, ref MegaChunk2DLink megaChunk2DLink, in VoxLink voxLink, in ChunkPosition chunkPosition) =>
            {
                if (generateChunk.state != GenerateChunkState.LinkMegaChunk2D)
                {
                    return;
                }
                generateChunk.state = GenerateChunkState.LinkPlanetChunk2Ds;
                if (HasComponent<MegaChunk2D>(megaChunk2DLink.megaChunk2D))
                {
                    return;
                }
                // Link Them
                var megaChunkPosition2 = VoxelUtilities.GetMegaChunkPosition(chunkPosition.position, megaChunkDivision);
                var megaChunkPosition = new int2(megaChunkPosition2.x, megaChunkPosition2.z);
                for (int i = 0; i < megaChunk2DEntities.Length; i++)
                {
                    var e2 = megaChunk2DEntities[i];
                    var megaChunkVoxLink = voxLinks[e2];
                    if (megaChunkVoxLink.vox == voxLink.vox)
                    {
                        // check megachunk position too
                        var quadrantPosition2D = quadrantPosition2Ds[e2];
                        if (megaChunkPosition == quadrantPosition2D.position)
                        {
                            megaChunk2DLink.megaChunk2D = e2;
                            break;
                        }
                    }
                }
            })  .WithReadOnly(megaChunk2DEntities)
                .WithDisposeOnCompletion(megaChunk2DEntities)
                .WithReadOnly(quadrantPosition2Ds)
                .WithReadOnly(voxLinks)
                .ScheduleParallel(Dependency);
        }
    }
}