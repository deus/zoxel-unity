using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Worlds
{
    //! Links a chunk to various vox structures.
    public struct StructureLinks : IComponentData
    {
        public BlitableArray<Entity> structures;
        
        public void Dispose()
        {
            structures.Dispose();
        }
        
        public void DisposeFinal()
        {
            structures.DisposeFinal();
        }

        //! Adds N new structures to the entity array.
        public void AddNewStructures(int addCount)
        {
            var newStructures = new BlitableArray<Entity>(structures.Length + addCount, Allocator.Persistent);
            for (int i = 0; i < structures.Length; i++)
            {
                newStructures[i] = structures[i];
            }
            for (int i = structures.Length; i < newStructures.Length; i++)
            {
                newStructures[i] = new Entity();
            }
            if (structures.Length > 0)
            {
                structures.Dispose();
            }
            structures = newStructures;
        }
    }
}