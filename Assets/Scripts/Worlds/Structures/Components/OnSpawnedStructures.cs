using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
// using Zoxel.Worlds;
// After a vox spawns chunks, add one of these to it

namespace Zoxel.Worlds
{
    public struct OnSpawnedStructures : IComponentData
    {
        public int spawnedChunks;

        public OnSpawnedStructures(int spawnedChunks)
        {
            this.spawnedChunks = spawnedChunks;
        }
    }
}