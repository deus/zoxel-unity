using Unity.Entities;

namespace Zoxel.Worlds
{
	//! The bounds of a structure.
	public struct StructureBounds : IComponentData
	{
		public int3 lowerBounds;
		public int3 upperBounds;
		
        //! For initializing a tree.
		public StructureBounds(int3 globalPosition, byte direction, int treeRadius, int treeHeight)
		{
            lowerBounds = new int3(globalPosition.x, globalPosition.y, globalPosition.z);
            upperBounds = new int3(globalPosition.x, globalPosition.y, globalPosition.z);
            if (direction == PlanetSide.Down)
            {
                lowerBounds -= (new int3(treeRadius, 0, treeRadius));
                upperBounds += (new int3(treeRadius, -treeHeight, treeRadius));
            }
            else if (direction == PlanetSide.Up)
            {
                lowerBounds -= (new int3(treeRadius, 0, treeRadius));
                upperBounds += (new int3(treeRadius, treeHeight, treeRadius));
            }
            else if (direction == PlanetSide.Left)
            {
                lowerBounds -= (new int3(0, treeRadius, treeRadius));
                upperBounds += (new int3(-treeHeight, treeRadius, treeRadius));
            }
            else if (direction == PlanetSide.Right)
            {
                lowerBounds -= (new int3(0, treeRadius, treeRadius));
                upperBounds += (new int3(treeHeight, treeRadius, treeRadius));
            }
            else if (direction == PlanetSide.Backward)
            {
                lowerBounds -= (new int3(treeRadius, treeRadius, 0));
                upperBounds += (new int3(treeRadius, treeRadius, -treeHeight));
            }
            else if (direction == PlanetSide.Forward)
            {
                lowerBounds -= (new int3(treeRadius, treeRadius, 0));
                upperBounds += (new int3(treeRadius, treeRadius, treeHeight));
            }
		}
	 }
}