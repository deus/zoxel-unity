using Unity.Entities;

namespace Zoxel.Worlds
{
	// used for any vox structure placed into map
	public struct Structure : IComponentData
	{
		public byte direction;
		
		public Structure(byte direction)
		{
			this.direction = direction;
		}
	 }
}