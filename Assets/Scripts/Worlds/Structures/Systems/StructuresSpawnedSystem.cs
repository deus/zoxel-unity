using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels; // For ChunkLink

namespace Zoxel.Worlds
{
    //! Links spawned Structure entities to StructureLinks.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class StructuresSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery structuresQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            structuresQuery = GetEntityQuery(
                ComponentType.ReadOnly<Structure>(),
                ComponentType.ReadOnly<ChunkLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var structureEntities = structuresQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var chunkLinks = GetComponentLookup<ChunkLink>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref StructureLinks structureLinks, ref OnSpawnedStructures onSpawnedStructures) =>
            {
                if (onSpawnedStructures.spawnedChunks != 0)
                {
                    structureLinks.AddNewStructures(onSpawnedStructures.spawnedChunks);
                    onSpawnedStructures.spawnedChunks = 0;
                }
                var count = 0;
                for (int i = 0; i < structureEntities.Length; i++)
                {
                    var e2 = structureEntities[i];
                    var chunkLink = chunkLinks[e2];
                    if (chunkLink.chunk == e)
                    {
                        structureLinks.structures[count] = e2;
                        count++;
                        if (count >= structureLinks.structures.Length)
                        {
                            break;
                        }
                    }
                }
                if (count == structureLinks.structures.Length)
                {
                    PostUpdateCommands.RemoveComponent<OnSpawnedStructures>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(structureEntities)
                .WithDisposeOnCompletion(structureEntities)
                .WithReadOnly(chunkLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
} 
