using Unity.Entities;

namespace Zoxel.Worlds
{
    //! A position for a MegaChunk in the world.
    public struct MegaChunkPosition : IComponentData
    {
        public byte quadrant;
        public int2 position;
    }
}