using Unity.Entities;

namespace Zoxel.Worlds
{
    //! Bigger than a regular chunk, contains bigger structures, landmarks and more.
    public struct MegaChunk : IComponentData
    {
        public int3 position;
        public BlitableArray<Town> towns;

        public MegaChunk(int3 position)
        {
            this.position = position;
            this.towns = new BlitableArray<Town>();
        }

        public void Dispose()
        {
            if (towns.Length > 0)
            {
                towns.Dispose();
            }
        }
    }
}