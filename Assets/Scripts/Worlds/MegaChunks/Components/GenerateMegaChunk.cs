using Unity.Entities;

namespace Zoxel.Worlds
{
    public struct GenerateMegaChunk : IComponentData
    {
        public byte state;
        public int worldID;
    }
}