using Unity.Entities;

namespace Zoxel.Worlds
{
    //! An event for linking spawned MegaChunk's to the Planet.
    public struct OnSpawnedMegaChunks : IComponentData
    {
        public int spawned;

        public OnSpawnedMegaChunks(int spawned)
        {
            this.spawned = spawned;
        }
    }
}