using Unity.Entities;

namespace Zoxel.Worlds
{
    //! Links to Town entities from a MegaChunk.
    public struct TownLinks : IComponentData
    {
        public BlitableArray<Entity> towns;
    }
}