
using Unity.Entities;

namespace Zoxel.Worlds
{
    //! A Chunk's way to link to a MegaChunk.
    /**
    *   Each planet chunk will be linked to a mega chunk.
    */
    public struct MegaChunkLink : IComponentData
    {
        public Entity megaChunk;
    }
}