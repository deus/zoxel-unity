﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! While generating Chunk's, it links a chunk to a MegaChunk.
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class MegaChunkLinkSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery megaChunkQuery;

        protected override void OnCreate()
        {
        	megaChunkQuery = GetEntityQuery(
                ComponentType.ReadOnly<MegaChunk>(),
                ComponentType.ReadOnly<VoxLink>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var megaChunkDivision = WorldsManager.instance.worldsSettings.megaChunkDivision;
            var megaChunkEntities = megaChunkQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var megaChunks = GetComponentLookup<MegaChunk>(true);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref GenerateChunk generateChunk, ref MegaChunkLink megaChunkLink, in VoxLink voxLink, in ChunkPosition chunkPosition, in ChunkNeighbors chunkNeighbors) =>
            {
                if (generateChunk.state != GenerateChunkState.LinkMegaChunk)
                {
                    return;
                }
                if (HasComponent<MegaChunk>(megaChunkLink.megaChunk))
                {
                    generateChunk.state = GenerateChunkState.LinkMegaChunk2D;
                    return;
                }
                generateChunk.state = GenerateChunkState.LinkMegaChunk2D;
                // Link Them
                var megaChunkPosition = VoxelUtilities.GetMegaChunkPosition(chunkPosition.position, megaChunkDivision);
                // for now
                // UnityEngine.Debug.LogError("megaChunkPosition: " + megaChunkPosition);
                for (int i = 0; i < megaChunkEntities.Length; i++)
                {
                    var e2 = megaChunkEntities[i];
                    var megaChunkVoxLink = voxLinks[e2];
                    if (megaChunkVoxLink.vox == voxLink.vox)
                    {
                        // check megachunk position too
                        var megaChunk = megaChunks[e2];
                        if (megaChunkPosition == megaChunk.position)
                        {
                            megaChunkLink.megaChunk = e2;
                            break;
                        }
                    }
                }
            })  .WithReadOnly(megaChunkEntities)
                .WithDisposeOnCompletion(megaChunkEntities)
                .WithReadOnly(megaChunks)
                .WithReadOnly(voxLinks)
                .ScheduleParallel(Dependency);
        }
    }
}