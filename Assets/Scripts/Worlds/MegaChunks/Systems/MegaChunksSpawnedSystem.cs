using Unity.Entities;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;

namespace Zoxel.Worlds
{
    //! After a vox spawns MegaChunk's, links MegaChunk's to Vox's.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class MegaChunksSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery megaChunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            megaChunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<MegaChunk>(),
                ComponentType.ReadOnly<VoxLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var megaChunkEntities = megaChunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref MegaChunkLinks megaChunkLinks, ref OnSpawnedMegaChunks onSpawnedMegaChunks) =>
            {
                if (onSpawnedMegaChunks.spawned != 0)
                {
                    megaChunkLinks.SetAs(onSpawnedMegaChunks.spawned);
                    onSpawnedMegaChunks.spawned = 0;
                }
                var count = 0;
                for (int i = 0; i < megaChunkEntities.Length; i++)
                {
                    var e2 = megaChunkEntities[i];
                    var voxLink = voxLinks[e2];
                    if (voxLink.vox == e)
                    {
                        megaChunkLinks.megaChunks[count] = e2;
                        count++;
                        if (count >= megaChunkLinks.megaChunks.Length)
                        {
                            break;
                        }
                    }
                }
                if (count == megaChunkLinks.megaChunks.Length)
                {
                    // link megachunks to vox
                    PostUpdateCommands.RemoveComponent<OnSpawnedMegaChunks>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(megaChunkEntities)
                .WithDisposeOnCompletion(megaChunkEntities)
                .WithReadOnly(voxLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
} 
