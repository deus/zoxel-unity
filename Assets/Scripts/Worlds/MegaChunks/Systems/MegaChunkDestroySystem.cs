using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Worlds
{
	//! Cleans up MegaChunk's on DestroyEntity events.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class MegaChunkDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Dependency = Entities
				.WithAll<DestroyEntity, MegaChunk>()
				.ForEach((Entity e, int entityInQueryIndex, in MegaChunk megaChunk) =>
			{
                megaChunk.Dispose();
			}).ScheduleParallel(Dependency);
		}
	}
}