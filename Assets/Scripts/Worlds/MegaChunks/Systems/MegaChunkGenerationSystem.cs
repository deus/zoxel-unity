/*using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Jobs;
using Zoxel.Voxels;
// todo: add dungeon zones here

namespace Zoxel.Worlds
{
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class MegaChunkGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery worldQuery;
        private EntityQuery biomesQuery;
        const int buildingPadding = 4;
        const int townPadding = 25;
        const int maxChecks = 250;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			processQuery = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>(), 
                ComponentType.ReadWrite<GenerateMegaChunk>(),
                ComponentType.ReadWrite<MegaChunk>(),
                ComponentType.ReadOnly<VoxLink>());
            worldQuery = GetEntityQuery(
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<BiomeLinks>());
            biomesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Biome>(),
                ComponentType.Exclude<DestroyEntity>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
			if (processQuery.IsEmpty)
			{
				return;
			}
            var megaChunkDivision = WorldsManager.instance.worldsSettings.megaChunkDivision;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var worldEntities = worldQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var worldIDs = GetComponentLookup<ZoxID>(true);
            var biomeLinks = GetComponentLookup<BiomeLinks>(true);
            var biomeEntities = biomesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var biomes = GetComponentLookup<Biome>(true);
            worldEntities.Dispose();
            biomeEntities.Dispose();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref GenerateMegaChunk generateMegaChunk, ref MegaChunk megaChunk, in VoxLink voxLink) =>
            {
                if (generateMegaChunk.state != 0)
                {
                    return;
                }
                generateMegaChunk.state = 1;
                var planetBiomes = biomeLinks[voxLink.vox].biomes;
                var biomeEntity = planetBiomes[0];
                var biome = biomes[biomeEntity];
                var worldSeed = worldIDs[voxLink.vox].id;
                generateMegaChunk.worldID = worldSeed;
                var random = new Random();
                random.InitState((uint) (worldSeed + megaChunk.position.x + megaChunk.position.z * 10000));
                var townsCount = 1 + random.NextInt(4);
                var roadsCount = 4; //2 + random.NextInt(6);
                // Spawn Towns
                // using seed generate some biomes
                var distanceFromOrigin = math.distance(megaChunk.position.ToFloat3(), new float3());
                // --- Spawn Towns ---
                var towns = new NativeList<Town>();
                for (int i = 0; i < townsCount; i++)
                {
                    var townSize = 12 + random.NextInt(14) + (int) math.max(12, distanceFromOrigin * 2);
                    var townHeight = 1 + random.NextInt(4) + (int) math.max(8, distanceFromOrigin * 2);
                    var townDimensions = new int3(townSize, townHeight, townSize);
                    bool hasFoundNewPosition;
                    var townPosition = FindNewTownPosition(towns, ref random, in megaChunk, townDimensions, out hasFoundNewPosition, megaChunkDivision);
                    if (hasFoundNewPosition)
                    {
                        var town = new Town();
                        town.position = townPosition;
                        town.dimensions = townDimensions;
                        town.roads = new BlitableArray<Road>(roadsCount, Allocator.Persistent);
                        town.biome = biome;
                        towns.Add(town);
                    }
                }
                megaChunk.towns = new BlitableArray<Town>(towns.Length, Allocator.Persistent);
                for (int i = 0; i < megaChunk.towns.Length; i++)
                {
                    megaChunk.towns[i] = towns[i];
                }
                towns.Dispose();
            })  .WithReadOnly(worldIDs)
                .WithReadOnly(biomeLinks)
                .WithReadOnly(biomes)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static int3 FindNewTownPosition(NativeList<Town> towns, ref Random random, in MegaChunk megaChunk, int3 townDimensions, out bool hasFoundNewPosition,
            int megaChunkDivision)
        {
            var townPositionVariance = (megaChunkDivision * 16) / 2;
            var megaChunkVoxelPosition = VoxelUtilities.MegaChunkToChunkPosition(megaChunk.position, megaChunkDivision);
            megaChunkVoxelPosition.x *= 16;
            megaChunkVoxelPosition.z *= 16;
            townPositionVariance -= 36;
            hasFoundNewPosition = false;
            int checks = 0;
            var newTownPosition = int3.zero;
            while (!hasFoundNewPosition)
            {
                var newPosition = megaChunkVoxelPosition + new int3(-townPositionVariance + random.NextInt(townPositionVariance * 2), 
                    0, -townPositionVariance + random.NextInt(townPositionVariance * 2));
                var isTooCloseToOthers = false;
                for (int i = 0; i < towns.Length; i++)
                {
                    var previousTown = towns[i];
                    // check if towns intersect
                    //var distanceTo = Unity.Mathematics.math.distance(previousTown.position.ToFloat3(), newPosition.ToFloat3());
                    //if (distanceTo <= previousTown.dimensions.x + townDimensions.x + townPadding)
                    if (!(previousTown.position.x + previousTown.dimensions.x < newPosition.x - townDimensions.x - townPadding    // Left Side
                    || previousTown.position.x - previousTown.dimensions.x > newPosition.x + townDimensions.x + townPadding     // Right Side
                    || previousTown.position.z + previousTown.dimensions.z < newPosition.z - townDimensions.z - townPadding
                    || previousTown.position.z - previousTown.dimensions.z > newPosition.z + townDimensions.z + townPadding))
                    {
                        isTooCloseToOthers = true;
                        break;
                    }
                }
                if (!isTooCloseToOthers)
                {
                    newTownPosition = newPosition;
                    hasFoundNewPosition = true;
                }
                checks++;
                if (checks >= maxChecks)
                {
                    break;
                }
            }
            if (hasFoundNewPosition)
            {
                return newTownPosition;
            }
            return new int3();
        }
    }
}*/

/*UnityEngine.Debug.LogError("In MegaChunk: " + megaChunk.position
    + ", megaChunkVoxelPosition: " + megaChunkVoxelPosition
    + ", Position Found: " + newTownPosition
    + ", townPositionVariance: " + townPositionVariance
    + ", hasFoundNewPosition: " + hasFoundNewPosition);*/