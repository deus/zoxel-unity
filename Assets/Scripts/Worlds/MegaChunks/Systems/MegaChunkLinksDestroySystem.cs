using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Worlds
{
    //! Destroys MegaChunks.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class MegaChunkLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithAll<DestroyEntity, MegaChunkLinks>()
				.ForEach((int entityInQueryIndex, in MegaChunkLinks megaChunkLinks) =>
			{
				for (int i = 0; i < megaChunkLinks.megaChunks.Length; i++)
				{
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, megaChunkLinks.megaChunks[i]);
				}
                megaChunkLinks.DisposeFinal();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}