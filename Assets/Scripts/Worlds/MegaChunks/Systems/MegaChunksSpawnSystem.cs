using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! Spawns MegaChunk entities using the StreamVox event.
    /**
    *   - Spawn System -
    *   Spawns a MegaChunk that spans over 16x16x16 regular chunks. Used for cave systems? or something in future.
    */
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class MegaChunksSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery megaChunkQuery;
        private Entity megaChunkPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var megaChunkArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                // Worlds
                typeof(MegaChunk),
                typeof(ChunkPosition),
                // typeof(GenerateMegaChunk),
                // typeof(GenerateTowns),
                // Voxels
                typeof(VoxLink)
            );
            megaChunkPrefab = EntityManager.CreateEntity(megaChunkArchetype);
            megaChunkQuery = GetEntityQuery(ComponentType.ReadOnly<MegaChunk>(), ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var megaChunkDivision = WorldsManager.instance.worldsSettings.megaChunkDivision;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var megaChunkPrefab2 = megaChunkPrefab;
            var megaChunkEntities = megaChunkQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var megaChunks2 = GetComponentLookup<MegaChunk>(true);
            megaChunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<OnSpawnedMegaChunks>()
                .WithAll<StreamVox>()
                .ForEach((Entity e, int entityInQueryIndex, ref MegaChunkLinks megaChunkLinks, in StreamableVox streamableVox, in SimplexNoise simplexNoise) =>
            {
                // First find positions
                var centralPosition = VoxelUtilities.GetMegaChunkPosition(streamableVox.position, megaChunkDivision);
                if (!(megaChunkLinks.init == 0 || centralPosition != megaChunkLinks.centralPosition))
                {
                    return;
                }
                megaChunkLinks.init = 1;
                megaChunkLinks.centralPosition = centralPosition;
                // should check if central megachunk position is updated
                var lowerBounds = centralPosition + new int3(-1,  -1, -1);
                var upperBounds = centralPosition + new int3(1,  1, 1);
                int3 position;
                // Get new MegaChunkPositions
                var spawnPositions = new NativeList<int3>();
                for (position.x = lowerBounds.x; position.x <= upperBounds.x; position.x++)
                {
                    for (position.y = lowerBounds.y; position.y <= upperBounds.y; position.y++)
                    {
                        for (position.z = lowerBounds.z; position.z <= upperBounds.z; position.z++)
                        {
                            // if doesn't exist, add to list
                            var doesExist = false;
                            // does exist
                            for (int i = 0; i < megaChunkLinks.megaChunks.Length; i++)
                            {
                                var megaChunk = megaChunkLinks.megaChunks[i];
                                if (!megaChunks2.HasComponent(megaChunk))
                                {
                                    return;
                                }
                                var position2 = megaChunks2[megaChunk].position;
                                if (position2 == position)
                                {
                                    doesExist = true;
                                    break;
                                }
                            }
                            if (!doesExist)
                            {
                                spawnPositions.Add(position);
                            }
                        }
                    }
                }
                
                // remove old Megachunks
                var megaChunksCount = 0;
                for (int i = 0; i < megaChunkLinks.megaChunks.Length; i++)
                {
                    var megaChunk = megaChunkLinks.megaChunks[i];
                    var position2 = megaChunks2[megaChunk].position;
                    if (position2.x < lowerBounds.x || position2.x > upperBounds.x || position2.z < lowerBounds.z || position2.z > upperBounds.z)
                    {
                        //removeMegaChunks.Add(megaChunk);
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, megaChunk);
                    }
                    else
                    {
                        // megaChunks.Add(megaChunk);
                        megaChunksCount++;
                    }
                }

                // Spawn New Chunks here
                for (int i = 0; i < spawnPositions.Length; i++)
                {
                    var spawnPosition = spawnPositions[i];
                    var megaChunkEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, megaChunkPrefab2);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, megaChunkEntity, new VoxLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, megaChunkEntity, new MegaChunk(spawnPosition));
                    //UnityEngine.Debug.LogError("Mega Chunk Spawned [" + i + "] " + spawnPosition);
                }
                
                megaChunksCount += spawnPositions.Length;
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnSpawnedMegaChunks(megaChunksCount));
                //UnityEngine.Debug.LogError("Mega Chunk Total: " + megaChunks.Length + " :: " + centralPosition + " --- " + streamMegaChunk.position
                //    + ", upperBounds: " + upperBounds + ", lowerBounds: " + lowerBounds + ", streamMegaChunk.position: " + streamMegaChunk.position);
                spawnPositions.Dispose();
            })  .WithReadOnly(megaChunks2)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}