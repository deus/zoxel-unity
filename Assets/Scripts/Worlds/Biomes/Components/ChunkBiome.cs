﻿using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Worlds
{
	//! A biome map determines what kind of terrain spawns on the planet side.
	public struct ChunkBiome : IComponentData
	{
		//! An index that represents a biome from the planet.
		public BlitableArray<byte> biomes;
		//! A number that blends between two different biomes.
		public BlitableArray<float> blends; 

		public void InitializeBiome(int count)
		{
			biomes = new BlitableArray<byte>(count, Allocator.Persistent);
			blends = new BlitableArray<float>(count, Allocator.Persistent);
		}

		public void Dispose()
		{
			if (biomes.Length > 0)   
			{
				biomes.Dispose();
			}
			if (blends.Length > 0)
			{
				blends.Dispose();
			}
		}
	}
}