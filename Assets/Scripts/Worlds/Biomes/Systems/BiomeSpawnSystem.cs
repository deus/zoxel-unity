using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! Spawns biomes for planet.
    /**
    *   - Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class BiomeSpawnSystem : SystemBase
    {
        //! \todo Use property data for biomes count.
        const int biomesCount = 12;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxelsQuery;
        private Entity biomePrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			voxelsQuery = GetEntityQuery(ComponentType.ReadOnly<Voxel>());
            var biomeArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(ZoxID),
                typeof(Biome),
                typeof(VoxLink));
            biomePrefab = EntityManager.CreateEntity(biomeArchetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(biomePrefab); // EntityManager.AddComponentData(biomePrefab, new EditorName("[biome]"));
            #endif
            RequireForUpdate(processQuery);
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
            var biomePrefab = this.biomePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxelEntities = voxelsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var voxelsList = GetComponentLookup<Voxel>(true);
            voxelEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity, OnBiomesSpawned>()
                .WithNone<StreamVox, OnVoxelsSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref BiomeLinks biomeLinks, ref WorldData worldData, in VoxelLinks voxelLinks, in ZoxID zoxID, in GeneratePlanet generatePlanet,
                    in MegaChunkLinks megaChunkLinks) =>
            {
                if (generatePlanet.state != 1)
                {
                    return;
                }
                for (int i = 0; i < voxelLinks.voxels.Length; i++)
                {
                    var voxelEntity = voxelLinks.voxels[i];
                    if (!voxelsList.HasComponent(voxelEntity))
                    {
                        // UnityEngine.Debug.LogError("Voxel at: " + voxelEntity.Index + " :: " + i + " does not exist in MapGen.");
                        return;
                    }
                }
                PostUpdateCommands.RemoveComponent<GeneratePlanet>(entityInQueryIndex, e);
                PostUpdateCommands.RemoveComponent<EntityBusy>(entityInQueryIndex, e);
                var worldSeed = (uint) zoxID.id;
                var random = new Random();
                random.InitState(worldSeed);
                var landBase = 32 + (int) random.NextFloat(12);
                var landAmplitude = 28 + (int) random.NextFloat(12);
                var landScale = 0.00025f + random.NextFloat(0.0025f);
                worldData.landBase = landBase;
                worldData.landAmplitude = landAmplitude;
                worldData.landScale = landScale;
                var voxels = new NativeList<Voxel>();
                for (int i = 0; i < voxelLinks.voxels.Length; i++)
                {
                    var voxelEntity = voxelLinks.voxels[i];
                    voxels.Add(voxelsList[voxelEntity]);
                }
                // Destroy old
                for (int i = 0; i < biomeLinks.biomes.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, biomeLinks.biomes[i]);
                }
                biomeLinks.InitializeBiomeLinks(biomesCount);
                var voxLink = new VoxLink(e);
                for (byte i = 0; i < biomeLinks.biomes.Length; i++)
                {
                    var biomeEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, biomePrefab);
                    var biome = new Biome();
                    biome.grassChance = 70;
                    biome.dirtID = GetVoxelID(VoxelType.Soil, in voxels, i);
                    biome.grassID = GetVoxelID(VoxelType.Grass, in voxels, i);
                    biome.sandID = GetVoxelID(VoxelType.Sand, in voxels, i);
                    biome.stoneID = GetVoxelID(VoxelType.Stone, in voxels, i);
                    biome.leafID = GetVoxelID(VoxelType.Leaf, in voxels, i);
                    biome.woodID = GetVoxelID(VoxelType.Wood, in voxels, i);
                    biome.bedrockID = GetVoxelID(VoxelType.Bedrock, in voxels, i);
                    biome.tilesID = GetVoxelID(VoxelType.Tiles, in voxels, i);
                    biome.bricksID = GetVoxelID(VoxelType.Bricks, in voxels, i);
                    var biomeIndex2 = 0;
                    for (int j = 0; j < voxelLinks.voxels.Length; j++)
                    {
                        if (HasComponent<GrassVoxel>(voxelLinks.voxels[j]))
                        {
                            if (biomeIndex2 == i)
                            {
                                biome.grassVoxID = (byte)(j + 1);
                                break;
                            }
                            biomeIndex2++;
                        }
                    }
                    for (int j = 0; j < voxelLinks.voxels.Length; j++)
                    {
                        if (HasComponent<DoorVoxel>(voxelLinks.voxels[j]))
                        {
                            biome.doorID = (byte)(j + 1);
                            break;
                        }
                    }
                    for (int j = 0; j < voxelLinks.voxels.Length; j++)
                    {
                        if (HasComponent<VoxelEmitLight>(voxelLinks.voxels[j]))
                        {
                            biome.torchID = (byte)(j + 1);
                            break;
                        }
                    }
                    for (int j = 0; j < voxelLinks.voxels.Length; j++)
                    {
                        if (HasComponent<WaterVoxel>(voxelLinks.voxels[j]))
                        {
                            biome.waterID = (byte)(j + 1);
                            break;
                        }
                    }
                    PostUpdateCommands.SetComponent(entityInQueryIndex, biomeEntity, biome);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, biomeEntity, voxLink);
                }
                /*for (int i = 0; i < megaChunkLinks.megaChunks.Length; i++)
                {
                    PostUpdateCommands.AddComponent<GenerateMegaChunk>(entityInQueryIndex, megaChunkLinks.megaChunks[i]);
                }*/
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new RefreshWorld(zoxID.id));
                PostUpdateCommands.AddComponent<OnBiomesSpawned>(entityInQueryIndex, e);
                voxels.Dispose();
            })  .WithReadOnly(voxelsList)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static byte GetVoxelID(VoxelType voxelType, in NativeList<Voxel> voxels, byte biomeIndex)
        {
            for (byte j = 0; j < voxels.Length; j++)
            {
                if (biomeIndex == voxels[j].biomeIndex)
                {
                    var voxelType2 = (VoxelType) voxels[j].type;
                    if (voxelType == voxelType2)
                    {
                        return (byte) (j + 1);
                    }
                }
            }
            // UnityEngine.Debug.LogError("Could not find VoxelID: " + voxelType + ":" + biomeIndex);
            return 0;
        }
    }
}