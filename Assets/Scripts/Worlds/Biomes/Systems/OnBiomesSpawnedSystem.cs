using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Worlds
{
    //! Handles finding biomes after they spawned.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class OnBiomesSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery biomesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            biomesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Biome>(),
                ComponentType.ReadOnly<VoxLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var biomeEntities = biomesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<OnBiomesSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref BiomeLinks biomeLinks) =>
            {
                var count = 0;
                for (int i = 0; i < biomeEntities.Length; i++)
                {
                    var e2 = biomeEntities[i];
                    var voxLink = voxLinks[e2];
                    if (voxLink.vox == e)
                    {
                        biomeLinks.biomes[count] = e2;
                        count++;
                        if (count == biomeLinks.biomes.Length)
                        {
                            PostUpdateCommands.RemoveComponent<OnBiomesSpawned>(entityInQueryIndex, e);
                            break;
                        }
                    }
                }
            })  .WithReadOnly(biomeEntities)
                .WithDisposeOnCompletion(biomeEntities)
                .WithReadOnly(voxLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}