using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Worlds
{
	//! Cleans up ChunkBiome on DestroyEntity event.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ChunkBiomeDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, ChunkBiome>()
                .ForEach((in ChunkBiome chunkBiome) =>
			{
                chunkBiome.Dispose();
			}).ScheduleParallel();
		}
	}
}