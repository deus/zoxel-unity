/*using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Zoxel.Voxels;
// todo: Generate biome based off height, humidity, light, temperaure, air maps

namespace Zoxel.Worlds
{
    [UpdateAfter(typeof(TownStartSystem))]
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
	public partial class BiomeGenerationSystem : SystemBase
	{
        private EntityQuery processQuery;
        private EntityQuery worldsQuery;

        protected override void OnCreate()
        {
			processQuery = GetEntityQuery(ComponentType.ReadWrite<GenerateChunk>(), ComponentType.ReadWrite<ChunkBiome>(),
				ComponentType.ReadWrite<ChunkPosition>(), ComponentType.ReadOnly<SimplexNoise>(), ComponentType.ReadOnly<Chunk>(),
				ComponentType.ReadOnly<TerrainHeightMap>(), ComponentType.ReadOnly<VoxLink>());
            worldsQuery = GetEntityQuery(ComponentType.ReadOnly<BiomeLinks>());
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
			if (processQuery.IsEmpty)
			{
				return;
			}
            var megaChunkDivision = VoxelManager.instance.voxelSettings.megaChunkDivision;
			var disableBiomes = VoxelManager.instance.voxelSettings.disableBiomes;
            var worldEntities = worldsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var biomeLinks = GetComponentLookup<BiomeLinks>(true);
            Dependency = Entities.ForEach((ref GenerateChunk generateChunk, ref ChunkBiome chunkBiome, in ChunkPosition chunkPosition,
				in SimplexNoise simplexNoise, in Chunk chunk, in TerrainHeightMap terrainHeightMap, in VoxLink voxLink) =>
            {
                if (generateChunk.state == GenerateChunkState.ChunkBiome && worldEntities.Length > 0)
				{
                    generateChunk.state = GenerateChunkState.Height;
					var biomes = biomeLinks[voxLink.vox].biomes;
					var voxelDimensions = chunk.voxelDimensions;
					var localPosition = new int2();
					var biomeTypesLength = (byte) biomes.Length;
					chunkBiome.InitializeBiome((int)(voxelDimensions.x * voxelDimensions.z));
					//var position = new float3(0, 0, 0);
					var perlinOffset = new int2(
							(512 + chunkPosition.position.x) * voxelDimensions.x, 
							(512 + chunkPosition.position.z) * voxelDimensions.z);
					var chunkVoxelPosition = new int2(
							(chunkPosition.position.x) * voxelDimensions.x, 
							(chunkPosition.position.z) * voxelDimensions.z);
					float2 noisePosition;
					// need to generate a blend of all chunkBiome types ranging from 0 to 1, like a combination of the chunkBiome maps will equal 1
					// then in terrain generation - for heightmaps
					//		Add heights as a combination of blends
					//		Base types as a percentage chance between all the voxels based on weights
					//			if 0.6 value of grass lands, give like 3 types of grass 0.2 chance each out of 1
					//int voxelIndex = 0;
					var megaChunkPosition = VoxelUtilities.GetMegaChunkPosition(
						chunkPosition.position, // new int3(globalVoxelPosition.x, 0, globalVoxelPosition.y), 
						megaChunkDivision);
					for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
					{
						for (localPosition.y = 0; localPosition.y < voxelDimensions.z; localPosition.y++)
						{
							var voxelIndex = VoxelUtilities.GetVoxelArrayIndexXZ(new int3((int)localPosition.x, 0, (int)localPosition.y), voxelDimensions);
							var globalVoxelPosition = chunkVoxelPosition + localPosition;
							// var biomeIndex = new int2(4096 + (globalVoxelPosition.x / megaChunkDivision), 4096 + (globalVoxelPosition.y / megaChunkDivision));
							var random = new Random();
							var number = 4096 * 512 + megaChunkPosition.x * 4096 + megaChunkPosition.z * 4096 * 512;
							if (number == 0)
							{
								number = 6666666;
							}
							random.InitState((uint)(number));
							chunkBiome.biomes[voxelIndex] = (byte) (random.NextInt(biomeTypesLength - 1));	// 
						}
					}
				}
            })  .WithReadOnly(worldEntities)
                .WithDisposeOnCompletion(worldEntities)
                .WithReadOnly(biomeLinks)
                .ScheduleParallel(Dependency);
		}
	}
}*/
							/*noisePosition = (perlinOffset + localPosition);
							var noiseScale = 0.0016f; // 0.002f;
							var noiseValue = 0.625f * 0.5f * ( 1 + 
								simplexNoise.Generate(
									noisePosition.x * noiseScale,
									noisePosition.y * noiseScale) ); 	//(1 + noise.snoise(biomeNoisePosition)) / 2f;
							noiseValue += 0.25f * 0.5f * ( 1 + 
								simplexNoise.Generate(
									noisePosition.x * noiseScale * 2f,
									noisePosition.y * noiseScale * 2f) ); 
							noiseValue += 0.125f * 0.5f * ( 1 + 
								simplexNoise.Generate(
									noisePosition.x * noiseScale * 4f,
									noisePosition.y * noiseScale * 4f) );
							noiseValue = math.clamp(noiseValue, 0, 1);*/

//int voxelIndex = (int)(heightPosition.y + heightPosition.x * chunkBiome.voxelDimensions.z);
//float noiseValue = (noise.snoise(0.001f * noisePosition));
	//chunkBiome.blends[voxelIndex] = noiseValue;
//math.clamp((0.99f * (terrainHeightMap.biomes.Length) * noiseValue), 0, terrainHeightMap.biomes.Length);
//noisePosition.x *= terrainHeightMap.chunkBiome.landScale;
//noisePosition.y *= terrainHeightMap.chunkBiome.landScale;
//int biomeInt = (int)math.floor(0.99f * (terrainHeightMap.biomes.Length - 1) *  (1 + noise.snoise(0.01f * noisePosition)) / 2);
//chunkBiome.biomes[voxelIndex] = (byte)(biomeInt);
//Debug.LogError("ChunkBiome spawned of: " + chunkBiome.biomes[voxelIndex] + "::" + biomeInt);
// one or two?
//float noiseScale = 0.008f;
//float noiseValue = (1 + noise.snoise(noiseScale * noisePosition)) / 2f;// (1 + noise.snoise(0.003f * noisePosition)) / 2;
//noiseValue += noise.snoise(noisePosition * noiseScale * 4) / 8;
//noiseValue += noise.snoise(noisePosition * noiseScale * 10) / 6;
//noiseValue = (1 + math.clamp(noiseValue, 0, 1)) / 2f;