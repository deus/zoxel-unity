using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Worlds
{
    //! Disposes of BiomeLinks made up of Biome datas.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class BiomeLinksDestroySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithAll<DestroyEntity, BiomeLinks>()
                .ForEach((int entityInQueryIndex, in BiomeLinks biomeLinks) =>
			{
                for (int i = 0; i < biomeLinks.biomes.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, biomeLinks.biomes[i]);
                }
                biomeLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}