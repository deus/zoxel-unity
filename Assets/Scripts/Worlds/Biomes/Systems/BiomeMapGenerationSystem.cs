using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;
// todo: Seperate 2D maps from chunk generation - used on each side of the planet
// todo: rename this to heightmaps?

namespace Zoxel.Worlds
{
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
	public partial class BiomeMapGenerationSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery planetsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            planetsQuery = GetEntityQuery(ComponentType.ReadOnly<Planet>());
            RequireForUpdate(processQuery);
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
            // var noiseA = 4f * 0.0007f;
            var noiseA = 0.01f;
            var noiseB = 2f * 0.004f;
            var noiseC = 1f * 0.012f;
            var maxBiomes = 16; // WorldsManager.instance.worldsSettings.planetHeightRadius;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var planetEntities = planetsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var planets = GetComponentLookup<Planet>(true);
            var worldDatas = GetComponentLookup<WorldData>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<InitializeEntity, PlanetChunk2D>()
                .ForEach((Entity e, int entityInQueryIndex, ref ChunkBiome chunkBiome, in QuadrantPosition2D quadrantPosition2D,
                    in SimplexNoise simplexNoise, in VoxLink voxLink) =>
            {
                if (planetEntities.Length == 0)
                {
                    return;
                }
                var planet = planets[voxLink.vox];
                var worldData = worldDatas[voxLink.vox];
                var chunkPosition = quadrantPosition2D.GetDictionaryPosition();
                var voxelDimensions = new int3(planet.voxelDimensions, planet.voxelDimensions, planet.voxelDimensions);
                chunkBiome.InitializeBiome(planet.voxelDimensions * planet.voxelDimensions);
                var perlinOffset = new float2(chunkPosition.x * voxelDimensions.x, chunkPosition.y * voxelDimensions.z);
                // UnityEngine.Debug.LogError("perlinOffset: " + perlinOffset);
                perlinOffset += new float2(8192, 8192);
                var voxelIndex = 0;
                float2 mapPosition;
                for (mapPosition.x = 0; mapPosition.x < voxelDimensions.x; mapPosition.x++)
                // for (mapPosition.y = 0; mapPosition.y < voxelDimensions.z; mapPosition.y++)
                {
                    for (mapPosition.y = 0; mapPosition.y < voxelDimensions.z; mapPosition.y++)
                    //for (mapPosition.x = 0; mapPosition.x < voxelDimensions.x; mapPosition.x++)
                    {
                        var biomeNoisePosition = perlinOffset + mapPosition;
                        // 3 octaves of perlin noise to give different 
                        var value = 0.6f * 0.5f * ( 1 + simplexNoise.Generate(
                            biomeNoisePosition.x * noiseA,
                            biomeNoisePosition.y * noiseA) );
                        value += 0.3f * 0.5f * ( 1 + simplexNoise.Generate(
                            biomeNoisePosition.x * noiseB,
                            biomeNoisePosition.y * noiseB) ); 
                        value += 0.15f * 0.5f * ( 1 + simplexNoise.Generate(
                            biomeNoisePosition.x * noiseC,
                            biomeNoisePosition.y * noiseC) );
                        value *= maxBiomes; // (0 + worldData.landAmplitude);
                        // voxelIndex = VoxelUtilities.GetVoxelArrayIndexXZ(mapPosition, voxelDimensions);
						voxelIndex = (int) (mapPosition.x + mapPosition.y * voxelDimensions.z);
                        chunkBiome.biomes[voxelIndex] = (byte)(math.floor(value));
                        // voxelIndex++;
                    }
                }
            })  .WithReadOnly(planetEntities)
                .WithDisposeOnCompletion(planetEntities)
                .WithReadOnly(planets)
                .WithReadOnly(worldDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}