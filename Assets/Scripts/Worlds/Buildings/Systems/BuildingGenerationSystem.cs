/*using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Burst;
using Zoxel.Voxels;
using Unity.Burst.Intrinsics;

// disabled for now

// todo:
//      Spawn building as a entity object
//      Pass in buildings - if they have been set
//          If not set yet, wait until they are finished 'generating' - GenerateBuilding
//      For buildings, when setting, if chunks have generated heightmaps - position them
//      DebugBuildingsSystem - where it shows their boxes

//      DebugLines - option to show them over top of map


// 60ms lag when spawning many - maybe need initialize system again

// todo: Spawn door at different positions in house
// todo: Multiple floors for house!
// todo: staircases in house! One Voxel steepness! (or more if you're feeling saucy)
// todo: Different roof types in houses

namespace Zoxel.Worlds
{
    [UpdateAfter(typeof(ChunkTownGenerationSystem))]
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class BuildingGenerationSystem : SystemBase
    {
		private EntityQuery processQuery;
        private EntityQuery chunkTerrainsQuery;

        protected override void OnCreate()
        {
        	processQuery = GetEntityQuery(ComponentType.ReadWrite<Chunk>(), ComponentType.ReadWrite<GenerateChunk>(),
                ComponentType.ReadOnly<ChunkPosition>(), ComponentType.ReadOnly<ChunkTown>(), ComponentType.ReadOnly<TerrainHeightMap>());
            chunkTerrainsQuery = GetEntityQuery(ComponentType.ReadOnly<PlanetChunk>(), ComponentType.ReadOnly<TerrainHeightMap>());
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
			if (processQuery.IsEmpty)
			{
				return;
			}
            var chunkEntities = chunkTerrainsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var chunkTerrains = GetComponentLookup<TerrainHeightMap>(true);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            var job = new BuildingGenerationJob
            {
                disableTowns = VoxelManager.instance.voxelSettings.disableTowns,
                chunkBuilderHandle = GetComponentTypeHandle<GenerateChunk>(false),
                chunkHandle = GetComponentTypeHandle<Chunk>(false),
                chunkTownHandle = GetComponentTypeHandle<ChunkTown>(false),
                chunkVoxelRotationsHandle = GetComponentTypeHandle<ChunkVoxelRotations>(false),
                chunkPositionHandle = GetComponentTypeHandle<ChunkPosition>(true),
                chunkTerrainHandle = GetComponentTypeHandle<TerrainHeightMap>(true),
                seedHandle = GetComponentTypeHandle<Seed>(true),
                voxLinkHandle = GetComponentTypeHandle<VoxLink>(true),
                chunkEntities = chunkEntities,
                chunkTerrains = chunkTerrains,
                chunkPositions = chunkPositions
            };
            Dependency = job.ScheduleParallel(processQuery, Dependency);
        }

        [BurstCompile]
        struct BuildingGenerationJob : IJobChunk
        {
            [ReadOnly] public bool disableTowns;
            public ComponentTypeHandle<GenerateChunk> chunkBuilderHandle;
            public ComponentTypeHandle<Chunk> chunkHandle;
            public ComponentTypeHandle<ChunkTown> chunkTownHandle;
            public ComponentTypeHandle<ChunkVoxelRotations> chunkVoxelRotationsHandle;
            [ReadOnly] public ComponentTypeHandle<ChunkPosition> chunkPositionHandle;
            [ReadOnly] public ComponentTypeHandle<TerrainHeightMap> chunkTerrainHandle;
            [ReadOnly] public ComponentTypeHandle<Seed> seedHandle;
            [ReadOnly] public ComponentTypeHandle<VoxLink> voxLinkHandle;
            [ReadOnly, DeallocateOnJobCompletion] public NativeArray<Entity> chunkEntities;
            [ReadOnly, NativeDisableParallelForRestriction] public ComponentLookup<TerrainHeightMap> chunkTerrains;
            [ReadOnly, NativeDisableParallelForRestriction] public ComponentLookup<ChunkPosition> chunkPositions;
            //[ReadOnly] public ComponentTypeHandle<ChunkBiome> chunkBiomeHandle;
            //[ReadOnly, DeallocateOnJobCompletion] public NativeArray<Entity> worldEntities;
            //[ReadOnly] public ComponentLookup<Planet> maps;

            public void Execute(in ArchetypeChunk chunk, int unfilteredChunkIndex, bool useEnabledMask, in v128 chunkEnabledMask)
            // public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
            {
                if (chunkEntities.Length == 0) // || worldEntities.Length == 0)
                { 
                    return;
                }
                //var entities = chunk.GetNativeArray(entityHandle);
                var generatingChunks = chunk.GetNativeArray(chunkBuilderHandle);
                var chunks = chunk.GetNativeArray(chunkHandle);
                var chunkTowns = chunk.GetNativeArray(chunkTownHandle);
                var chunkVoxelRotations = chunk.GetNativeArray(chunkVoxelRotationsHandle);
                var chunkPositions = chunk.GetNativeArray(chunkPositionHandle);
                var chunkTerrains = chunk.GetNativeArray(chunkTerrainHandle);
                var seeds = chunk.GetNativeArray(seedHandle);
               // var chunkBiomes = chunk.GetNativeArray(chunkBiomeHandle);
                var voxLinks = chunk.GetNativeArray(voxLinkHandle);
                for (var i = 0; i < chunk.Count; i++)
                {
                    var chunk2 = chunks[i];
                    var generateChunk = generatingChunks[i];
                    var chunkTown = chunkTowns[i];
                    var chunkVoxelRotation = chunkVoxelRotations[i];
                    var chunkPosition = chunkPositions[i];
                    var terrainHeightMap = chunkTerrains[i];
                    var seed = seeds[i];
                    var voxLink = voxLinks[i];
                    Process(ref chunk2, ref generateChunk, ref chunkTown, ref chunkVoxelRotation,
                        in chunkPosition, in terrainHeightMap, in seed, in voxLink);
                    generatingChunks[i] = generateChunk;
                    chunks[i] = chunk2;
                    chunkTowns[i] = chunkTown;
                    chunkVoxelRotations[i] = chunkVoxelRotation;
                }
            }

            public void Process(ref Chunk chunk, ref GenerateChunk generateChunk, ref ChunkTown chunkTown, ref ChunkVoxelRotations chunkVoxelRotations,
                in ChunkPosition chunkPosition, in TerrainHeightMap terrainHeightMap, in Seed seed, in VoxLink voxLink) // in ChunkBiome chunkBiome, 
            {
                if (generateChunk.state == GenerateChunkState.Buildings)
				{
                    // if (chunkTown.buildings.Length == 0)
                    {
                        generateChunk.state = GenerateChunkState.Trees;
                        return;
                    }
                    var voxelDimensions = chunk.voxelDimensions;
                    int voxelIndex = 0;
                    int positionXZ = 0;
                    int height = 0;
                    var position = int3.zero;
                    var localPosition = int3.zero;
                    var voxelWorldPosition = chunkPosition.GetVoxelPosition(voxelDimensions);
                    int3 townPosition = int3.zero;
                    for (int i = 0; i < chunkTown.towns.Length; i++)
                    {
                        var town = chunkTown.towns[i];
                        townPosition = town.position;
                    }
                    var random = new Random();
                    random.InitState(seed.GetChunkSeed(chunkPosition.position)); // terrainHeightMap.treeRandom.state);
                    // var buildings = new NativeArray<Building>(chunkTown.buildings.Length, Allocator.Temp);
                    for (var i = 0; i < chunkTown.buildings.Length; i++)
                    {
                        var building = chunkTown.buildings[i];
                        var buildingPosition = townPosition + building.position;
                        // For each building, using heightmap, find the average height of it
                        //      -= This works across chunks =-
                        var maxFloorHeight = 0;
                        var lowestFloorHeight = 128;
                        voxelIndex = 0;
                        for (position.x = buildingPosition.x - building.dimensions.x; position.x < buildingPosition.x + building.dimensions.x; position.x++)
                        {
                            for (position.z = buildingPosition.z -building.dimensions.z; position.z < buildingPosition.z + building.dimensions.z; position.z++)
                            {
                                // from position get chunk position
                                var chunkPosition2 = VoxelUtilities.GetChunkPosition(position, voxelDimensions);
                                var chunkEntity = new Entity();
                                for (int a = 0; a < chunkEntities.Length; a++)
                                {
                                    var e2 = chunkEntities[a];
                                    if (chunkPosition2 == chunkPositions[e2].position)
                                    {
                                        chunkEntity = e2;
                                        break;
                                    }
                                }
                                var localChunkPosition = VoxelUtilities.GetLocalPosition(position, chunkPosition2, voxelDimensions);
                                if (!chunkTerrains.HasComponent(chunkEntity))
                                {
                                    continue;   // this shouldn't happen? unless of course on the border and the building goes over the edge...
                                }
                                var thatChunkTerrain = chunkTerrains[chunkEntity];
                                if (thatChunkTerrain.heights.Length == 0)
                                {
                                    return;   // if other chunk still generating heightmap
                                }
                                //localPosition = new int3(position.x - voxelWorldPosition.x, position.y - voxelWorldPosition.y, position.z - voxelWorldPosition.z);
                                positionXZ = VoxelUtilities.GetVoxelArrayIndexXZ(localChunkPosition, voxelDimensions); //(int)(localChunkPosition.x * chunk.voxelDimensions.z + localChunkPosition.z);
                                height = thatChunkTerrain.heights[positionXZ];
                                if (height == 0)
                                {
                                    return;   // if other chunk still generating heightmap? this shouldnt occur
                                }
                                if (height > maxFloorHeight)
                                {
                                    maxFloorHeight = height;
                                }
                                if (height < lowestFloorHeight)
                                {
                                    lowestFloorHeight = height;
                                }
                            }
                        }
                        building.position = new int3(chunkTown.buildings[i].position.x, maxFloorHeight, chunkTown.buildings[i].position.z); //  + 1
                        building.lowestFloorHeight = lowestFloorHeight;
                        //buildings[i] = building;
                        chunkTown.buildings[i] = building;
                        // if (chunkTown.buildings.Length >= 2)
                        // {
                        //     UnityEngine.Debug.LogError(i + ": " + building.position + " - size: " + building.dimensions + ", In " + chunkPosition.position +
                        //         ", Buildings Count: " + chunkTown.buildings.Length);
                        // }
                    }
                    // if made it through the terrain checks
                    generateChunk.state = GenerateChunkState.Trees;
					if (generateChunk.loaded == 1 || disableTowns)
                    {
                        return;
                    }
                    // spawn door seperately?
                    for (var i = 0; i < chunkTown.buildings.Length; i++)
                    {
                        var building = chunkTown.buildings[i];
                        var houseHeight = building.dimensions.y;
                        var houseWidth = building.dimensions.x;
                        var houseDepth = building.dimensions.z;
                        // get position of door
                        // if door is within this chunk, place
                        // add rotation to door positions
                        var buildingPosition = townPosition + building.position;
                        // spawn foundation
                        for (position.x = buildingPosition.x - houseWidth; position.x <= buildingPosition.x + houseWidth; position.x++)
                        {
                            for (position.z = buildingPosition.z - houseDepth; position.z <= buildingPosition.z + houseDepth; position.z++)
                            {
                                position.y = 0;
                                localPosition = position - voxelWorldPosition;
                                if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                                {
                                    positionXZ = VoxelUtilities.GetVoxelArrayIndexXZ(localPosition, voxelDimensions);
                                    height = terrainHeightMap.heights[positionXZ];
                                    for (position.y = height; position.y < buildingPosition.y; position.y++)
                                    {
                                        localPosition = position - voxelWorldPosition;
                                        if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                                        {
                                            chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] = building.foundationID;
                                        }
                                    }
                                }
                            }
                        }
                        // spawn roof
                        // todo: triangular roof
                                // position.y = buildingPosition.y + houseHeight;
                        for (position.y = buildingPosition.y + houseHeight; position.y <= buildingPosition.y + houseHeight + 3; position.y++)
                        {
                            var difference = position.y - buildingPosition.y - houseHeight;
                            for (position.x = buildingPosition.x - houseWidth + difference; position.x <= buildingPosition.x + houseWidth - difference; position.x++)
                            {
                                for (position.z = buildingPosition.z - houseDepth + difference; position.z <= buildingPosition.z + houseDepth - difference; position.z++)
                                {
                                    localPosition = position - voxelWorldPosition;
                                    if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                                    {
                                        chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] = building.roofID;
                                    }
                                }
                            }
                        }
                        // spawn floor
                        for (position.x = buildingPosition.x + 1 - houseWidth; position.x < buildingPosition.x + houseWidth; position.x++)
                        {
                            for (position.z = buildingPosition.z + 1 - houseDepth; position.z < buildingPosition.z + houseDepth; position.z++)
                            {
                                position.y = buildingPosition.y;
                                localPosition = position - voxelWorldPosition;
                                if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                                {
                                    chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] = building.floorID;
                                }
                            }
                        }
                        // spawn walls
                        var roofOverhang = 1;
                        var wallsEdgeMinusZ = buildingPosition.z - houseDepth + roofOverhang;
                        var wallsEdgePlusZ = buildingPosition.z + houseDepth - roofOverhang;
                        var wallsEdgeMinusX = buildingPosition.x - houseWidth + roofOverhang;
                        var wallsEdgePlusX = buildingPosition.x + houseWidth - roofOverhang;
                        position.z = wallsEdgeMinusZ;
                        for (position.x = wallsEdgeMinusX; position.x <= wallsEdgePlusX; position.x++)
                        {
                            for (position.y = buildingPosition.y; position.y < buildingPosition.y + houseHeight; position.y++)
                            {
                                localPosition = position - voxelWorldPosition;
                                if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                                {
                                    chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] = building.wallID;
                                }
                            }
                        }
                        position.z = wallsEdgePlusZ;
                        for (position.x = wallsEdgeMinusX; position.x <= wallsEdgePlusX; position.x++)
                        {
                            for (position.y = buildingPosition.y; position.y < buildingPosition.y + houseHeight; position.y++)
                            {
                                localPosition = position - voxelWorldPosition;
                                if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                                {
                                    chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] = building.wallID;
                                }
                            }
                        }
                        position.x = wallsEdgeMinusX;
                        for (position.z = wallsEdgeMinusZ; position.z <= wallsEdgePlusZ; position.z++)
                        {
                            for (position.y = buildingPosition.y; position.y < buildingPosition.y + houseHeight; position.y++)
                            {
                                localPosition = position - voxelWorldPosition;
                                if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                                {
                                    chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] = building.wallID;
                                }
                            }
                        }
                        position.x = wallsEdgePlusX;
                        for (position.z = wallsEdgeMinusZ; position.z <= wallsEdgePlusZ; position.z++)
                        {
                            for (position.y = buildingPosition.y; position.y < buildingPosition.y + houseHeight; position.y++)
                            {
                                localPosition = position - voxelWorldPosition;
                                if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                                {
                                    chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] = building.wallID;
                                }
                            }
                        }
                        // spawn air
                        for (position.x = wallsEdgeMinusX + 1; position.x <= wallsEdgePlusX - 1; position.x++)
                        {
                            for (position.z = wallsEdgeMinusZ + 1; position.z <= wallsEdgePlusZ - 1; position.z++)
                            {
                                for (position.y = buildingPosition.y + 1; position.y < buildingPosition.y + houseHeight; position.y++)
                                {
                                    localPosition = position - voxelWorldPosition;
                                    if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                                    {
                                        chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] = 0;
                                    }
                                }
                            }
                        }

                        var torchPosition = new int3(wallsEdgeMinusX + 1, buildingPosition.y + 1, wallsEdgeMinusZ + 1);
                        localPosition = torchPosition - voxelWorldPosition;
                        if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                        {
                            chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] = building.torchID;
                        }
                        torchPosition = new int3(wallsEdgePlusX - 1, buildingPosition.y + 1, wallsEdgeMinusZ + 1);
                        localPosition = torchPosition - voxelWorldPosition;
                        if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                        {
                            chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] = building.torchID;
                        }
                        torchPosition = new int3(wallsEdgeMinusX + 1, buildingPosition.y + 1, wallsEdgePlusZ - 1);
                        localPosition = torchPosition - voxelWorldPosition;
                        if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                        {
                            chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] = building.torchID;
                        }
                        torchPosition = new int3(wallsEdgePlusX - 1, buildingPosition.y + 1, wallsEdgePlusZ - 1);
                        localPosition = torchPosition - voxelWorldPosition;
                        if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                        {
                            chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] = building.torchID;
                        }

                        // calculate door position
                        var doorPosition = new int3(buildingPosition.x, buildingPosition.y + 1, buildingPosition.z);
                        if (building.doorSideType == 0)
                        {
                            doorPosition.z += houseDepth - roofOverhang;
                        }
                        else if (building.doorSideType == 1)
                        {
                            doorPosition.z -= houseDepth - roofOverhang;
                        }
                        else if (building.doorSideType == 3)
                        {
                            doorPosition.x += houseWidth - roofOverhang;
                        }
                        else if (building.doorSideType == 2)
                        {
                            doorPosition.x -= houseWidth - roofOverhang;
                        }

                        // spawn door
                        localPosition = doorPosition - voxelWorldPosition;
                        if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                        {
                            var voxelIndex2 = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
                            chunk.voxels[voxelIndex2] = building.doorID; // door
                            var voxelIndex3 = VoxelUtilities.GetVoxelArrayIndex(localPosition.Up(), voxelDimensions);
                            chunk.voxels[voxelIndex3] = building.doorID; // door
                            // Add rotation as well
                            if (building.doorSideType == 0)
                            {
                                chunkVoxelRotations.AddRotation(localPosition, 0);
                            }
                            else if (building.doorSideType == 1)
                            {
                                chunkVoxelRotations.AddRotation(localPosition, 1);
                            }
                            else if (building.doorSideType == 2)
                            {
                                chunkVoxelRotations.AddRotation(localPosition, 3);
                            }
                            else if (building.doorSideType == 3)
                            {
                                chunkVoxelRotations.AddRotation(localPosition, 2);
                            }
                        }
                    }

                    for (var i = 0; i < chunkTown.roads.Length; i++)
                    {
                        var road = chunkTown.roads[i];
                        var beginPosition = townPosition + road.beginPosition;
                        var endPosition = townPosition + road.endPosition;
                        if (beginPosition.z != endPosition.z)
                        {
                            localPosition.y = 0;
                            for (position.x = beginPosition.x - road.width; position.x <= beginPosition.x + road.width; position.x++)
                            {
                                for (position.z = beginPosition.z; position.z <= endPosition.z; position.z++)
                                {
                                    localPosition = position - voxelWorldPosition;
                                    if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                                    {
                                        localPosition.y = terrainHeightMap.heights[VoxelUtilities.GetVoxelArrayIndexXZ(localPosition, voxelDimensions)];
                                        chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] = road.roadID;
                                    }
                                }
                            }
                        }
                        else if (beginPosition.x != endPosition.x)
                        {
                            for (position.x = beginPosition.x; position.x <= endPosition.x; position.x++)
                            {
                                for (position.z = beginPosition.z - road.width; position.z <= beginPosition.z + road.width; position.z++)
                                {
                                    localPosition = position - voxelWorldPosition;
                                    if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                                    {
                                        localPosition.y = terrainHeightMap.heights[VoxelUtilities.GetVoxelArrayIndexXZ(localPosition, voxelDimensions)];
                                        chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] = road.roadID;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}*/





            /*private void BuildRoad(int3 townPosition, ref Chunk chunk, in Road road, float height, int3 position, int voxelIndex) // , Biome biome)
            {
                // road should be a line, with a width
                var beginPosition = townPosition + road.beginPosition;
                var endPosition = townPosition + road.endPosition;
                if (beginPosition.z != endPosition.z
                    && position.z >= beginPosition.z && position.z <= endPosition.z
                    && position.x >= beginPosition.x - road.width
                    && position.x <= beginPosition.x + road.width)
                {
                    chunk.voxels[voxelIndex] = road.roadID;
                }
                else if (beginPosition.x != endPosition.x
                    && position.x >= beginPosition.x && position.x <= endPosition.x
                    && position.z >= beginPosition.z - road.width
                    && position.z <= beginPosition.z + road.width)
                {
                    chunk.voxels[voxelIndex] = road.roadID;
                }
            }*/
                                    /*var chunkPosition2 = VoxelUtilities.GetChunkPosition(position, voxelDimensions);
                                    localPosition = VoxelUtilities.GetLocalPosition(position, chunkPosition2, voxelDimensions);
                                    if (localPosition.x >= 0 && localPosition.x < voxelDimensions.x
                                        && localPosition.z >= 0 && localPosition.z < voxelDimensions.z)
                                    {
                                        positionXZ = VoxelUtilities.GetVoxelArrayIndexXZ(localPosition, voxelDimensions);
                                        height = terrainHeightMap.heights[positionXZ];
                                        localPosition.y = height;
                                        position.y = height;
                                        //var biomeIndex = chunkBiome.biomes[positionXZ];
                                        //var biome = biomes[biomeIndex];
                                        var voxelIndex2 = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
                                        if (voxelIndex2 >= 0 && voxelIndex2 < chunk.voxels.Length)
                                        {
                                            BuildRoad(townPosition, ref chunk, in road, height, position, voxelIndex2); // , biome);
                                        }
                                    }*/

// door
/*if (position.y < buildingPosition.y + 3)
{
    // z +
    if (building.doorSideType == 0 &&
        position.z == buildingPosition.z + houseDepth &&
        position.x >= buildingPosition.x - doorSizeLower && 
        position.x <= buildingPosition.x + doorSizeUpper)
    {
        chunk.voxels[voxelIndex] = biome.doorID; // door
    }
    // z -
    else if (building.doorSideType == 1 &&
        position.z == buildingPosition.z - houseDepth &&
        position.x >= buildingPosition.x - doorSizeLower && 
        position.x <= buildingPosition.x + doorSizeUpper)
    {
        chunk.voxels[voxelIndex] = biome.doorID; // door
    }
    // x -
    else if (building.doorSideType == 2 &&
        position.x == buildingPosition.x - houseWidth &&
        position.z >= buildingPosition.z - doorSizeLower && 
        position.z <= buildingPosition.z + doorSizeUpper)
    {
        chunk.voxels[voxelIndex] = biome.doorID; // door
    }
    // x +
    else if (building.doorSideType == 3 &&
        position.x == buildingPosition.x + houseWidth &&
        position.z >= buildingPosition.z - doorSizeLower && 
        position.z <= buildingPosition.z + doorSizeUpper)
    {
        chunk.voxels[voxelIndex] = biome.doorID; // door
    }
}*/

            // , Biome biome
            /*private void BuildBuilding(ref Chunk chunk, int3 townPosition, in Building building, int3 position, int voxelIndex, ref Random random)
            {
                var doorSizeLower = 0;
                var doorSizeUpper = 0;
                int houseHeight = (int)building.dimensions.y;
                int houseWidth = (int)building.dimensions.x;
                int houseDepth = (int)building.dimensions.z;
                var buildingPosition = townPosition + building.position;
                // walls
                var isLeftSide = (position.x == buildingPosition.x - houseWidth && 
                    position.z >= buildingPosition.z - houseDepth && position.z <= buildingPosition.z + houseDepth);
                var isRightSide = (position.x == buildingPosition.x + houseWidth && 
                    position.z >= buildingPosition.z - houseDepth && position.z <= buildingPosition.z + houseDepth);
                var isFrontSide = (position.z == buildingPosition.z - houseDepth && 
                        position.x >= buildingPosition.x - houseWidth && position.x <= buildingPosition.x + houseWidth);
                var isBackSide = (position.z == buildingPosition.z + houseDepth && 
                        position.x >= buildingPosition.x - houseWidth && position.x <= buildingPosition.x + houseWidth);
                if (position.y > buildingPosition.y && position.y < buildingPosition.y + houseHeight && (isLeftSide || isRightSide || isFrontSide || isBackSide))
                {
                    // window
                    if (position.y == buildingPosition.y + 2 && (
                        ((isLeftSide || isRightSide) && position.z % 2 == 0) 
                        || ((isFrontSide || isBackSide) && position.x % 2 == 0) ) && random.NextInt(100) >= 60)
                    {
                        chunk.voxels[voxelIndex] = building.windowID;
                    }
                    else
                    {
                        chunk.voxels[voxelIndex] = building.wallID;
                    }
                }
                // above roof
                else if ((position.y == buildingPosition.y + houseHeight + 1)
                    && (position.x >= buildingPosition.x - houseWidth + 1 && position.x <= buildingPosition.x + houseWidth - 1
                    && position.z >= buildingPosition.z - houseDepth + 1 && position.z <= buildingPosition.z + houseDepth - 1))
                {
                    chunk.voxels[voxelIndex] = building.roofID;
                }
                // roof
                else if ((position.y == buildingPosition.y + houseHeight)
                    && (position.x >= buildingPosition.x - houseWidth && position.x <= buildingPosition.x + houseWidth
                    && position.z >= buildingPosition.z - houseDepth && position.z <= buildingPosition.z + houseDepth))
                {
                    chunk.voxels[voxelIndex] = building.roofID;
                }
                // floor
                else if ((position.y == buildingPosition.y)
                    && (position.x >= buildingPosition.x - houseWidth && position.x <= buildingPosition.x + houseWidth
                    && position.z >= buildingPosition.z - houseDepth && position.z <= buildingPosition.z + houseDepth))
                {
                    chunk.voxels[voxelIndex] = building.floorID;
                }
                // inside house
                else if ((position.y > buildingPosition.y && position.y < buildingPosition.y + houseHeight)
                    && (position.x > buildingPosition.x - houseWidth && position.x < buildingPosition.x + houseWidth
                    && position.z > buildingPosition.z - houseDepth && position.z < buildingPosition.z + houseDepth))
                {
                    chunk.voxels[voxelIndex] = 0;
                }
                // below house
                else if ((position.y >= building.lowestFloorHeight && position.y < buildingPosition.y)
                    && (position.x >= buildingPosition.x - houseWidth && position.x <= buildingPosition.x + houseWidth
                    && position.z >= buildingPosition.z - houseDepth && position.z <= buildingPosition.z + houseDepth))
                {
                    chunk.voxels[voxelIndex] = building.foundationID;
                }
            }*/




                    /*for (var i = 0; i < chunkTown.buildings.Length; i++)
                    {
                        var building = chunkTown.buildings[i];
                        var doorSizeLower = 0;
                        var doorSizeUpper = 0;
                        int houseHeight = (int)building.dimensions.y;
                        int houseWidth = (int)building.dimensions.x;
                        int houseDepth = (int)building.dimensions.z;
                        var buildingPosition = townPosition + building.position;

                    }*/
                        // walls
                        /*var isLeftSide = (position.x == buildingPosition.x - houseWidth && 
                            position.z >= buildingPosition.z - houseDepth && position.z <= buildingPosition.z + houseDepth);
                        var isRightSide = (position.x == buildingPosition.x + houseWidth && 
                            position.z >= buildingPosition.z - houseDepth && position.z <= buildingPosition.z + houseDepth);
                        var isFrontSide = (position.z == buildingPosition.z - houseDepth && 
                                position.x >= buildingPosition.x - houseWidth && position.x <= buildingPosition.x + houseWidth);
                        var isBackSide = (position.z == buildingPosition.z + houseDepth && 
                                position.x >= buildingPosition.x - houseWidth && position.x <= buildingPosition.x + houseWidth);*/

                        // spawn foundation and base

                        /*if (position.y > buildingPosition.y && position.y < buildingPosition.y + houseHeight && (isLeftSide || isRightSide || isFrontSide || isBackSide))
                        {
                            // window
                            if (position.y == buildingPosition.y + 2 && (
                                ((isLeftSide || isRightSide) && position.z % 2 == 0) 
                                || ((isFrontSide || isBackSide) && position.x % 2 == 0) ) && random.NextInt(100) >= 60)
                            {
                                chunk.voxels[voxelIndex] = building.windowID;
                            }
                            else
                            {
                                chunk.voxels[voxelIndex] = building.wallID;
                            }
                        }
                        // above roof
                        else if ((position.y == buildingPosition.y + houseHeight + 1)
                            && (position.x >= buildingPosition.x - houseWidth + 1 && position.x <= buildingPosition.x + houseWidth - 1
                            && position.z >= buildingPosition.z - houseDepth + 1 && position.z <= buildingPosition.z + houseDepth - 1))
                        {
                            chunk.voxels[voxelIndex] = building.roofID;
                        }
                        // roof
                        else if ((position.y == buildingPosition.y + houseHeight)
                            && (position.x >= buildingPosition.x - houseWidth && position.x <= buildingPosition.x + houseWidth
                            && position.z >= buildingPosition.z - houseDepth && position.z <= buildingPosition.z + houseDepth))
                        {
                            chunk.voxels[voxelIndex] = building.roofID;
                        }
                        // floor
                        else if ((position.y == buildingPosition.y)
                            && (position.x >= buildingPosition.x - houseWidth && position.x <= buildingPosition.x + houseWidth
                            && position.z >= buildingPosition.z - houseDepth && position.z <= buildingPosition.z + houseDepth))
                        {
                            chunk.voxels[voxelIndex] = building.floorID;
                        }
                        // inside house
                        else if ((position.y > buildingPosition.y && position.y < buildingPosition.y + houseHeight)
                            && (position.x > buildingPosition.x - houseWidth && position.x < buildingPosition.x + houseWidth
                            && position.z > buildingPosition.z - houseDepth && position.z < buildingPosition.z + houseDepth))
                        {
                            chunk.voxels[voxelIndex] = 0;
                        }
                        // below house
                        else if ((position.y >= building.lowestFloorHeight && position.y < buildingPosition.y)
                            && (position.x >= buildingPosition.x - houseWidth && position.x <= buildingPosition.x + houseWidth
                            && position.z >= buildingPosition.z - houseDepth && position.z <= buildingPosition.z + houseDepth))
                        {
                            chunk.voxels[voxelIndex] = building.foundationID;
                        }*/
                        
                        
                        // should also build underneath building
                        //chunkTown.buildings[i] = building;
                        /*voxelIndex = 0;
                        for (position.x = voxelWorldPosition.x; position.x < voxelWorldPosition.x + voxelDimensions.x; position.x++)
                        {
                            for (position.y = voxelWorldPosition.y; position.y < voxelWorldPosition.y + voxelDimensions.y; position.y++)
                            {
                                for (position.z = voxelWorldPosition.z; position.z < voxelWorldPosition.z + voxelDimensions.z; position.z++)
                                {
                                    localPosition = position - voxelWorldPosition;
                                    // positionXZ = VoxelUtilities.GetVoxelArrayIndexXZ(localPosition, voxelDimensions);
                                    // var biome = biomes[chunkBiome.biomes[positionXZ]]; , biome
                                    BuildBuilding(ref chunk, townPosition, in building, position, voxelIndex, ref random);
                                    voxelIndex++;
                                }
                            }
                        }*/
