using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Jobs;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! Spawns a house in the world.
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class HouseSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery processQuery;
        private EntityQuery chunksQuery;
        private EntityQuery worldsQuery;
        private EntityQuery biomesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            chunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<PlanetChunk>(),
                ComponentType.ReadWrite<Chunk>(),
                ComponentType.ReadWrite<ChunkVoxelRotations>(),
                ComponentType.ReadOnly<ChunkNeighbors>(),
                ComponentType.ReadOnly<VoxLink>());
            worldsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<BiomeLinks>());
            biomesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Biome>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
		}

        public static void SpawnHouse(EntityManager EntityManager, Entity world, Entity chunk, int3 position)
        {
            var e = EntityManager.CreateEntity();
            EntityManager.AddComponentData(e, new SpawnHouse(world, chunk, position));
        }

		[BurstCompile]
		protected override void OnUpdate()
		{
            var houseHeight = 6;
            var houseWidth = 20;
            var houseDepth = 14;
            byte isHouseCentredX = 1;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var worldEntities = worldsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var biomeLinks = GetComponentLookup<BiomeLinks>(true);
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var chunks = GetComponentLookup<Chunk>(false);
            var chunkVoxelRotations = GetComponentLookup<ChunkVoxelRotations>(false);
            var chunkNeighbors2 = GetComponentLookup<ChunkNeighbors>(true);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            var biomeEntities = biomesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var biomes = GetComponentLookup<Biome>(true);
            worldEntities.Dispose();
            chunkEntities.Dispose();
            biomeEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, in SpawnHouse spawnHouse) =>
            {
                if (!voxLinks.HasComponent(spawnHouse.chunk))
                {
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                    return;
                }
                // for house, calculate how many rooms I can add based on width / depth / height / resources used
                // Can add Hallway and lounge room, then spin off bedrooms
                // Add fence around house too - just dirt for now at one block
                // for house dimensions, place house voxels into chunk
                var voxLink = voxLinks[spawnHouse.chunk];
                var world = voxLink.vox;
                var biomeEntity = biomeLinks[world].biomes[0];
                var biome = biomes[biomeEntity];
                var localChunk = chunks[spawnHouse.chunk];
                var localChunkVoxelRotations = chunkVoxelRotations[spawnHouse.chunk];
                var voxelDimensions = localChunk.voxelDimensions;
                var chunkNeighbors = chunkNeighbors2[spawnHouse.chunk];
                var voxelWorldPosition = chunkPositions[spawnHouse.chunk].GetVoxelPosition(voxelDimensions);
                var localPosition = int3.zero;
                var voxelPosition = spawnHouse.position;
                var spawnHousePosition = spawnHouse.position;
                spawnHousePosition.y--;
                var doorPosition = spawnHousePosition + new int3(houseWidth / 2, 1, 0);
                var doorPosition2 = doorPosition.Up();
                if (isHouseCentredX == 1)
                {
                    spawnHousePosition.x -= houseWidth / 2;
                    doorPosition.x -= houseWidth / 2;
                    doorPosition2.x -= houseWidth / 2;
                }
                var spawnHouseMax = spawnHousePosition + new int3(houseWidth - 1, houseHeight - 1, houseDepth - 1);
                var isEvenX = spawnHousePosition.x % 2 == 0;
                var isEvenZ = spawnHousePosition.z % 2 == 0;
                // UnityEngine.Debug.LogError("Even: " + isEvenX + " : " + isEvenZ);
                var updatedChunks = new NativeList<Entity>();
                updatedChunks.Add(spawnHouse.chunk);
                // UnityEngine.Debug.LogError("chunkNeighbors: " + chunkNeighbors.chunkBack.Index + " :: " + chunkNeighbors.chunkForward.Index);
                // first test
                for (voxelPosition.x = spawnHousePosition.x; voxelPosition.x <= spawnHouseMax.x; voxelPosition.x++)
                {
                    for (voxelPosition.y = spawnHousePosition.y; voxelPosition.y <= spawnHouseMax.y; voxelPosition.y++)
                    {
                        for (voxelPosition.z = spawnHousePosition.z; voxelPosition.z <= spawnHouseMax.z; voxelPosition.z++)
                        {
                            var rotationType = BlockRotation.Up;
                            var placeType = biome.bricksID;
                            if (voxelPosition == doorPosition || voxelPosition == doorPosition2)
                            {
                                placeType = biome.doorID;
                                rotationType = BlockRotation.Up90;
                            }
                            else if (voxelPosition.y == spawnHousePosition.y || voxelPosition.y == spawnHouseMax.y)
                            {
                                placeType = biome.tilesID;
                            }
                            else if (!(voxelPosition.x == spawnHousePosition.x || voxelPosition.x == spawnHouseMax.x || 
                                voxelPosition.y == spawnHousePosition.y || voxelPosition.y == spawnHouseMax.y || 
                                voxelPosition.z == spawnHousePosition.z || voxelPosition.z == spawnHouseMax.z))
                            {
                                placeType = 0;
                            }
                            // Windows
                            if (voxelPosition.y == spawnHousePosition.y + 2)
                            {
                                if (((voxelPosition.z == spawnHousePosition.z || voxelPosition.z == spawnHouseMax.z))
                                    && ((isEvenX && voxelPosition.x % 2 == 0) || (!isEvenX && voxelPosition.x % 2 != 0)) )
                                {
                                    if (placeType != biome.doorID)
                                    {
                                        placeType = 0;
                                    }
                                }
                                if (((voxelPosition.x == spawnHousePosition.x || voxelPosition.x == spawnHouseMax.x))
                                    && ((isEvenZ && voxelPosition.z % 2 == 0) || (!isEvenZ && voxelPosition.z % 2 != 0)) )
                                {
                                    if (placeType != biome.doorID)
                                    {
                                        placeType = 0; // biome.dirtID;
                                    }
                                }
                            }
                            // place voxels
                            localPosition = voxelPosition - voxelWorldPosition;
                            if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
                            {
                                var voxelIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
                                localChunk.voxels[voxelIndex] = placeType;
                                if (rotationType != BlockRotation.Up)
                                {
                                    localChunkVoxelRotations.AddRotation(localPosition, rotationType);
                                }
                            }
                            else 
                            {
                                // var preLocalPosition = new int3(localPosition.x, localPosition.y, localPosition.z);
                                var chunkEntity = chunkNeighbors.GetChunk(ref localPosition, voxelDimensions); // , out otherVoxelPosition);
                                var voxelIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
                                var neighborChunk = chunks[chunkEntity];
                                neighborChunk.voxels[voxelIndex] = placeType;
                                if (rotationType != BlockRotation.Up)
                                {
                                    var neighborChunkVoxelRotations = chunkVoxelRotations[chunkEntity];
                                    neighborChunkVoxelRotations.AddRotation(localPosition, rotationType);
                                    chunkVoxelRotations[chunkEntity] = neighborChunkVoxelRotations;
                                }
                                if (!updatedChunks.Contains(chunkEntity))
                                {
                                    updatedChunks.Add(chunkEntity);
                                }
                                /*else
                                {
                                    UnityEngine.Debug.LogError("Voxel has no chunk at: " + preLocalPosition + " :: " + localPosition
                                        + " : " + chunkEntity.Index);
                                }*/
                            }
                        }
                    }
                }
                chunkVoxelRotations[spawnHouse.chunk] = localChunkVoxelRotations;
                // should also update edge of chunks
                for (int i = 0; i < updatedChunks.Length; i++)
                {
                    var chunkEntity = updatedChunks[i];
                    if (!HasComponent<ChunkBuilder>(chunkEntity))
                    {
                        PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, chunkEntity);
                        PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, chunkEntity);
                        PostUpdateCommands.AddComponent<SaveChunk>(entityInQueryIndex, chunkEntity);
                        // save to chunks too?
                    }
                }
                // for any chunk neighbors in bounds, add updates to them
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                updatedChunks.Dispose();
            })  .WithReadOnly(biomeLinks)
                .WithReadOnly(chunkNeighbors2)
                .WithReadOnly(chunkPositions)
                .WithNativeDisableContainerSafetyRestriction(chunks)
                .WithNativeDisableContainerSafetyRestriction(chunkVoxelRotations)
                .WithReadOnly(voxLinks)
                .WithReadOnly(biomes)
                .ScheduleParallel(Dependency);
            //! \todo (chunkVoxelRotations) This is going to break with more than one house omg, since it involves updating more than one chunks!
        }
    }
}