using Unity.Entities;

namespace Zoxel.Worlds
{
    public struct SpawnHouse : IComponentData
    {
        public Entity world;
        public Entity chunk;
        public int3 position;
        public byte rotation;

        public SpawnHouse(Entity world, Entity chunk, int3 position)
        {
            this.world = world;
            this.chunk = chunk;
            this.position = position;
            this.rotation = 0;
        }
    }
}