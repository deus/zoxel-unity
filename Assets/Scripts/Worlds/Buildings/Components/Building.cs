using Unity.Entities;

namespace Zoxel.Worlds
{
    public struct Building
    {
        public int3 position;           // where to place house (planet pos or local?)
        public int3 dimensions;         // X Y Z - bounding box of house
        public byte doorSideType;       // door direction on building
        public int characterID;         // spawn character in building
        public int lowestFloorHeight;   // what level to start building foundation at

        // types for building
        public byte foundationID;   // below floor - stone or dirt
        public byte floorID;
        public byte wallID;
        public byte roofID;
        public byte windowID;
        public byte doorID;
        public byte torchID;

        public void SetWithBiome(in Biome biome)
        {
            this.foundationID = biome.dirtID;
            this.floorID = biome.tilesID;
            this.wallID = biome.bricksID;
            this.roofID = biome.stoneID;
            this.windowID = 0;
            this.doorID = biome.doorID;
            this.torchID = biome.torchID;
        }
    }
}

//using Unity.Mathematics;
//[Serializable]