namespace Zoxel.Worlds
{
    public struct Road
    {
        public byte roadID;
        public int3 beginPosition;
        public int3 endPosition;
        public int width;
    }
}