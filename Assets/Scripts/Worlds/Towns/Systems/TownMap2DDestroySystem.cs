using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Worlds
{
    //! Disposes of TownMap2D on DestroyEntity event.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class TownMap2DDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, TownMap2D>()
                .ForEach((in TownMap2D townMap2D) =>
			{
                townMap2D.Dispose();
			}).ScheduleParallel();
		}
	}
}