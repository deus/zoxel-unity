using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
	//! Generates Town's from MegaChunk2D TownMap2D's.
	/**
	*	Doesn't generate town when Chunk has LoadedChunk.
	*/
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class ChunkTownGenerationSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery worldsQuery;
        private EntityQuery chunksQuery;
        private EntityQuery biomesQuery;
        private EntityQuery megaChunk2DsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			processQuery = GetEntityQuery(
				ComponentType.ReadOnly<GenerateTowns>(),
				ComponentType.ReadWrite<Chunk>(),
				ComponentType.ReadWrite<ChunkVoxelRotations>(),
                ComponentType.ReadOnly<ChunkHeights>(),
                ComponentType.ReadOnly<ChunkPosition>(),
				ComponentType.ReadOnly<VoxLink>(),
				ComponentType.ReadOnly<MegaChunk2DLink>());
            worldsQuery = GetEntityQuery(
                ComponentType.Exclude<StreamVox>(),
                ComponentType.Exclude<OnSpawnedPlanetChunks>(),
				ComponentType.ReadOnly<Planet>(),
				ComponentType.ReadOnly<BiomeLinks>());
            megaChunk2DsQuery = GetEntityQuery(
				ComponentType.ReadOnly<QuadrantPosition2D>(),
				ComponentType.ReadOnly<TownMap2D>());
            biomesQuery = GetEntityQuery(
				ComponentType.ReadOnly<Biome>(),
				ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
		}
		
		[BurstCompile]
        protected override void OnUpdate()
        {
            var disableTowns = WorldsManager.instance.worldsSettings.disableTowns;
            var megaChunkDivision = WorldsManager.instance.worldsSettings.megaChunkDivision;
			var planetOffset = new int3(0, 0, 0);
			var planetStretch = new float3(1, 1, 1);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var worldEntities = worldsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var planets = GetComponentLookup<Planet>(true);
            var biomeLinks = GetComponentLookup<BiomeLinks>(true);
            worldEntities.Dispose();
            var chunkEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            chunkEntities.Dispose();
            var megaChunk2DEntities = megaChunk2DsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var quadrantPosition2Ds = GetComponentLookup<QuadrantPosition2D>(true);
            var townMap2Ds = GetComponentLookup<TownMap2D>(true);
            megaChunk2DEntities.Dispose();
            var biomeEntities = biomesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var biomes = GetComponentLookup<Biome>(true);
            biomeEntities.Dispose();
            Dependency = Entities
				.WithNone<UpdateChunkNeighbors>()
				.WithAll<GenerateTowns, EditedChunk>()
				.ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<GenerateTowns>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<GenerateChunkComplete>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
				.WithNone<UpdateChunkNeighbors, EditedChunk>()
				.WithAll<GenerateTowns>()
				.ForEach((Entity e, int entityInQueryIndex, ref Chunk chunk, ref ChunkVoxelRotations chunkVoxelRotations, in ChunkHeights chunkHeights,
					in ChunkPosition chunkPosition, in MegaChunk2DLink megaChunk2DLink) =>
            {
                if (disableTowns)   // HasComponent<EditedChunk>(e) || 
                {
                    PostUpdateCommands.RemoveComponent<GenerateTowns>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<GenerateChunkComplete>(entityInQueryIndex, e);
                    return;
                }
                var voxLink = voxLinks[e];
                if (!planets.HasComponent(voxLink.vox))
                {
                    return;
                }
                var planetRadius = planets[voxLink.vox].radius;
                var planetBiomes = biomeLinks[voxLink.vox].biomes;
                var biomeEntity = planetBiomes[0];
                if (!biomes.HasComponent(biomeEntity))
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<GenerateTowns>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<GenerateChunkComplete>(entityInQueryIndex, e);
                if (!townMap2Ds.HasComponent(megaChunk2DLink.megaChunk2D))
                {
                    return;
                }
                // UnityEngine.Debug.LogError("Generating Town in Chunk: " + megaChunk2DLink.megaChunk2D.Index);
                var voxelRotations = new NativeList<VoxelRotation>();
                var biome = biomes[biomeEntity];
                var localPosition = int3.zero;
                var voxelDimensions = chunk.voxelDimensions;
                var planetLowerBounds = - (new int3((int) (planetRadius * planetStretch.x), (int) (planetRadius * planetStretch.y), (int) (planetRadius * planetStretch.z)));
                var planetUpperBounds = new int3((int) (planetRadius * planetStretch.x), (int) (planetRadius * planetStretch.y), (int) (planetRadius * planetStretch.z));
                var voxelWorldPosition = chunkPosition.GetVoxelPosition(voxelDimensions);
                var townMap2D = townMap2Ds[megaChunk2DLink.megaChunk2D];
                var quadrantPosition2D = quadrantPosition2Ds[megaChunk2DLink.megaChunk2D];
                var chunkVoxelPosition2 = chunkPosition.GetVoxelPosition(voxelDimensions);
                var chunkVoxelPosition = new int2(chunkVoxelPosition2.x, chunkVoxelPosition2.z);
                var townPositionOffset = quadrantPosition2D.GetVoxelPosition2D(voxelDimensions, megaChunkDivision) + townMap2D.position;
                // UnityEngine.Debug.LogError("Town townPositionOffset: " + townPositionOffset + " - " + quadrantPosition2D.localPosition);
                int3 globalPosition;
                int2 townPosition;
                var voxelIndex2 = 0;
                var roadTile = (byte) 1;
                var buildingTile = (byte) 2;
                var doorTile = (byte) 3;
                var townWallTile = (byte) 4;
                var stoneBlockID = (byte) 4;
                var woodBlockID = (byte) 7;
                // I feel like I need a height map per voxel
                // As well as a direction map
                // get height as well per voxel
                //  then place roads at height localPosition
                //  and build building at building positions
                for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                {
                    for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                    {
                        for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
                        {
                            var voxelByte = chunk.voxels[voxelIndex2];
                            globalPosition = voxelWorldPosition + localPosition + planetOffset;
                            townPosition = new int2(globalPosition.x, globalPosition.z) - townPositionOffset;
                            if (townPosition.x < 0 || townPosition.x >= townMap2D.size.x || townPosition.y < 0 || townPosition.y >= townMap2D.size.y)
                            {
                                voxelIndex2++;
                                continue;
                            }
                            var townMapArrayIndex = townPosition.x + townPosition.y * townMap2D.size.x;
                            if (townMapArrayIndex < 0 || townMapArrayIndex >= townMap2D.data.Length)
                            {
                                // UnityEngine.Debug.LogError("townMapArrayIndex out of Index: " + townMapArrayIndex);
                                voxelIndex2++;
                                continue;
                            }
                            var height = chunkHeights.heights[voxelIndex2];
                            if (!(height.blockDirection == PlanetSide.Up)) //  && height.distanceToGround >= 0))
                            {
                                voxelIndex2++;
                                continue;
                            }
                            /*if (height.distanceToGround < 0)
                            {
                                UnityEngine.Debug.LogError("height.distanceToGround: " + height.distanceToGround);
                            }*/
                            // UnityEngine.Debug.LogError("townMapArrayIndex out of Index: " + townMapArrayIndex);
                            // Set map ui based on townMap2D
                            var townByte = townMap2D.data[townMapArrayIndex];
                            // Road
                            if (townByte == roadTile)
                            {
                                if (height.distanceToGround == 0)
                                {
                                    chunk.voxels[voxelIndex2] = stoneBlockID;  // stone for now
                                }
                                else if (height.distanceToGround >= -5 && height.distanceToGround < 0)
                                {
                                    chunk.voxels[voxelIndex2] = 0;
                                }
                            }
                            // house
                            else if (townByte == buildingTile || townByte == doorTile)
                            {
                                if (height.distanceToGround == 0 || height.distanceToGround == -4)
                                {
                                    chunk.voxels[voxelIndex2] = woodBlockID;
                                }
                                else 
                                {
                                    if (height.distanceToGround >= -3 && height.distanceToGround <= 0)
                                    {
                                        var isWall = !IsNeighbors(in townMap2D, townPosition, buildingTile, doorTile);
                                        if (isWall)
                                        {
                                            // chunk.voxels[voxelIndex2] = 6;
                                            if (townByte == doorTile && (height.distanceToGround == -1 || height.distanceToGround == -2 || height.distanceToGround == -3))
                                            {
                                                chunk.voxels[voxelIndex2] = 0;  // door
                                            }
                                            else
                                            {
                                                chunk.voxels[voxelIndex2] = stoneBlockID;
                                            }
                                        }
                                        else
                                        {
                                            chunk.voxels[voxelIndex2] = 0;  // inside house
                                        }
                                    }
                                }
                            }
                            // Wall
                            else if (townByte == townWallTile)
                            {
                                if (height.distanceToGround >= -5 && height.distanceToGround <= 0)
                                {
                                    chunk.voxels[voxelIndex2] = stoneBlockID;
                                }
                            }
                            voxelIndex2++;
                            // get localPosition in town map
                            // spawn voxel based on town map
                            // note - use pixel generation to base this off
                        }
                    }
                }
                chunkVoxelRotations.AddRotations(in voxelRotations);
                voxelRotations.Dispose();
            })  .WithReadOnly(planets)
                .WithReadOnly(biomeLinks)
                .WithReadOnly(voxLinks)
                .WithReadOnly(biomes)
                .WithReadOnly(quadrantPosition2Ds)
                .WithReadOnly(townMap2Ds)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}

        private static bool IsNeighbors(in TownMap2D townMap2D, int2 localPosition, byte targetType, byte targetType2)
        {
            var townSize = townMap2D.size;
            var positionDown = localPosition.Down();
            var typeDown = (byte) 0;
            if (positionDown.y >= 0)
            {
                typeDown = townMap2D.data[positionDown.x + positionDown.y * townSize.x];
            }
            var isTypeDown = positionDown.y < 0 || typeDown == targetType || typeDown == targetType2;

            var positionUp = localPosition.Up();
            var typeUp = (byte) 0;
            if (positionUp.y < townSize.y)
            {
                typeUp = townMap2D.data[positionUp.x + positionUp.y * townSize.x];
            }
            var isTypeUp = positionUp.y >= townMap2D.size.y || typeUp == targetType || typeUp == targetType2;

            var positionLeft = localPosition.Left();
            var typeLeft = (byte) 0;
            if (positionLeft.x >= 0)
            {
                typeLeft = townMap2D.data[positionLeft.x + positionLeft.y * townSize.x];
            }
            var isTypeLeft = positionLeft.x < 0 || typeLeft == targetType || typeLeft == targetType2;

            var positionRight = localPosition.Right();
            var typeRight = (byte) 0;
            if (positionRight.x < townSize.x)
            {
                typeRight = townMap2D.data[positionRight.x + positionRight.y * townSize.x];
            }
            var isTypeRight = positionRight.x >= townMap2D.size.x || typeRight == targetType || typeRight == targetType2;
            return isTypeUp && isTypeDown && isTypeLeft && isTypeRight;
        }
	}
}