using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Worlds
{
    //! Generates the TownMap2D on a MegaChunk2D.
    /**
    *   Optimized System; Works by iteration.
    */
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
	public partial class TownMap2DGenerateSystem : SystemBase
	{
        private const byte roadTile = (byte) 1;
        private const byte buildingTile = (byte) 2;
        private const byte doorTile = (byte) 3;
        private const byte townWallTile = (byte) 4;
        private const byte townWallRadius = 3;
        private const byte buildingSpawnChance = 97;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
            if (WorldsManager.instance == null) return;
            var megaChunkDivision = WorldsManager.instance.worldsSettings.megaChunkDivision;
            var seed = (uint) IDUtil.GenerateUniqueID();
            var roadTilesPlaceCount = 300;
            var townSizeMin = new int2(32, 32);
            var townSizeMax = new int2(80, 80);
            var townPositionOffsetMin = new int2(12, 12);
            var townPositionOffsetMax = new int2(42, 42);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<GenerateTownMap2D>()
                .ForEach((Entity e, int entityInQueryIndex, ref TownMap2D townMap2D, ref GenerateTownMap2D generateTownMap2D, in Seed seed, in QuadrantPosition2D quadrantPosition2D) =>
            {
                if (generateTownMap2D.state == 0)
                {
                    //! Initialize data based off unique seed.
                    var chunkPosition = quadrantPosition2D.GetDictionaryPosition();
                    var random = new Random();
                    random.InitState(seed.GetChunkSeed(chunkPosition));
                    var size = new int2(random.NextInt(36, 64), random.NextInt(36, 64));
                    townMap2D.Initialize(size);
                    var megaPlanetChunkSize = 16 * megaChunkDivision;
                    townMap2D.position = new int2(random.NextInt(0, megaPlanetChunkSize - size.x), random.NextInt(0, megaPlanetChunkSize - size.y));
                    // first clear map
                    for (int i = 0; i < townMap2D.data.Length; i++)
                    {
                        townMap2D.data[i] = 0;
                    }
                    // step function
                    // start at a position
                    generateTownMap2D.random = random;
                    generateTownMap2D.mapPosition = new int2(townMap2D.size.x / 2, 2);
                }
                else if (generateTownMap2D.state == 1)
                {
                    //! Create towm wall.
                    var size = townMap2D.size;
                    for (int i = 0; i < size.x; i++)
                    {
                        for (int j = 0; j < size.y; j++)
                        {
                            if (i >= townWallRadius && i <= size.x - 1 - townWallRadius && j >= townWallRadius && j <= size.y - 1 - townWallRadius)
                            {
                                continue;
                            }
                            var tilePosition = new int2(i, j);
                            townMap2D.data[tilePosition.GetArrayIndex(size)] = townWallTile;
                        }
                    }
                    for (int i = size.x / 2 - 2; i < size.x / 2 + 1; i++)
                    {
                        for (int j = 0; j < townWallRadius; j++)
                        {
                            var tilePosition = new int2(i, j);
                            townMap2D.data[tilePosition.GetArrayIndex(size)] = roadTile;
                        }
                    }
                }
                else
                {
                    //! Create Roads.
                    // n x n algorithm as it weights all new positions
                    // this will be slow for bigger towns
                    for (int i = 0; i < roadTilesPlaceCount / 10; i++)
                    {
                        generateTownMap2D.mapPosition = SpawnRoadTile(generateTownMap2D.mapPosition, ref townMap2D, ref generateTownMap2D.random);
                    }
                }
                generateTownMap2D.state++;
                if (generateTownMap2D.state >= 12)
                {
                    PostUpdateCommands.RemoveComponent<GenerateTownMap2D>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}

        //! Spawns road tiles of a length at a position, with a direction.
        private static int2 SpawnRoadTile(int2 mapPosition, ref TownMap2D townMap2D, ref Random random)
        {
            var roadPositionsAdded = new NativeList<int2>();
            // if possible, place building here
            // first do a chance
            var size = townMap2D.size;
            var placedBuilding = false;
            if (random.NextInt(0, 100) >= buildingSpawnChance)
            {
                var buildingSize = new int2(random.NextInt(4, 8), random.NextInt(4, 8));
                var buildingOffset = new int2(buildingSize.x / 2, 0);
                var canPlaceBuilding = true;
                for (int j = 0; j < buildingSize.x; j++)
                {
                    for (int k = 0; k < buildingSize.y; k++)
                    {
                        var tilePosition = mapPosition + new int2(j, k) + buildingOffset;
                        var mapByte = townMap2D.data[tilePosition.GetArrayIndex(size)];
                        if (mapByte != 0)
                        {
                            canPlaceBuilding = false;
                            k = buildingSize.y;
                            j = buildingSize.x;
                        }
                    }
                }
                if (canPlaceBuilding)
                {
                    placedBuilding = true;
                    for (int j = 0; j < buildingSize.x; j++)
                    {
                        for (int k = 0; k < buildingSize.y; k++)
                        {
                            var tilePosition = mapPosition + new int2(j, k) + buildingOffset;
                            if (j == buildingOffset.x && k == 0)
                            {
                                townMap2D.data[tilePosition.GetArrayIndex(size)] = doorTile;
                            }
                            else
                            {
                                townMap2D.data[tilePosition.GetArrayIndex(size)] = buildingTile;
                            }
                            roadPositionsAdded.Add(tilePosition);
                        }
                    }
                }
            }
            if (!placedBuilding)
            {
                townMap2D.data[mapPosition.GetArrayIndex(size)] = roadTile;
                roadPositionsAdded.Add(mapPosition);
            }

            // place road
            //UnityEngine.Debug.LogError(i + " - Placing Road at: " + mapPosition);
            // count neighbors
            // and for any of those neighbors, add one to their count
            // find next position
            // var checksCount = 0;
            var possibleNewRoads = new NativeList<int2>();
            for (int j = 0; j < roadPositionsAdded.Length; j++)
            {
                var previousRoadPosition = roadPositionsAdded[j];
                // check position in front to see if empty
                var positionUp = previousRoadPosition.Up();
                if (positionUp.y < size.y && townMap2D.data[positionUp.GetArrayIndex(size)] == 0
                    && !roadPositionsAdded.Contains(positionUp) && !possibleNewRoads.Contains(positionUp))
                {
                    possibleNewRoads.Add(positionUp);
                }
                var positionDown = previousRoadPosition.Down();
                if (positionDown.y >= 0 && townMap2D.data[positionDown.GetArrayIndex(size)] == 0
                    && !roadPositionsAdded.Contains(positionDown) && !possibleNewRoads.Contains(positionDown))
                {
                    possibleNewRoads.Add(positionDown);
                }
                var positionLeft = previousRoadPosition.Left();
                if (positionLeft.x >= 0 && townMap2D.data[positionLeft.GetArrayIndex(size)] == 0
                    && !roadPositionsAdded.Contains(positionLeft) && !possibleNewRoads.Contains(positionLeft))
                {
                    possibleNewRoads.Add(positionLeft);
                }
                var positionRight = previousRoadPosition.Right();
                if (positionRight.x < size.x && townMap2D.data[positionRight.GetArrayIndex(size)] == 0
                    && !roadPositionsAdded.Contains(positionRight) && !possibleNewRoads.Contains(positionRight))
                {
                    possibleNewRoads.Add(positionRight);
                }
            }
            if (possibleNewRoads.Length == 0)
            {
                possibleNewRoads.Dispose();
                roadPositionsAdded.Dispose();
                return mapPosition;
            }
            // remove any new places with 2 or more surrounding road tiles
            for (int j = possibleNewRoads.Length - 1; j >= 0; j--)
            {
                var roadPosition = possibleNewRoads[j];
                var roadTileNeighborsCount = 0;
                var positionUp = roadPosition.Up();
                if (positionUp.y < size.y && townMap2D.data[positionUp.GetArrayIndex(size)] == roadTile)
                {
                    roadTileNeighborsCount++;
                }
                var positionDown = roadPosition.Down();
                if (positionDown.y >= 0 && townMap2D.data[positionDown.GetArrayIndex(size)] == roadTile)
                {
                    roadTileNeighborsCount++;
                }
                var positionLeft = roadPosition.Left();
                if (positionLeft.x >= 0 && townMap2D.data[positionLeft.GetArrayIndex(size)] == roadTile)
                {
                    roadTileNeighborsCount++;
                }
                var positionRight = roadPosition.Right();
                if (positionRight.x < size.x && townMap2D.data[positionRight.GetArrayIndex(size)] == roadTile)
                {
                    roadTileNeighborsCount++;
                }
                if (roadTileNeighborsCount >= 2)
                {
                    possibleNewRoads.RemoveAt(j);
                }
            }
            //! Idea - Count total roads already in the opposite direction of the new tile
            //      - will be better if I also add directions when adding new tile positions
            //      This way I can make roads more or less straight
            if (possibleNewRoads.Length == 0)
            {
                possibleNewRoads.Dispose();
                roadPositionsAdded.Dispose();
                return mapPosition;
            }
            // pick new one based on list
            mapPosition = possibleNewRoads[random.NextInt(0, possibleNewRoads.Length)];
            // get list of possible new roads with 2 neighbors tops, 1 neighbor for new direction and 2 for a road joining another road
            possibleNewRoads.Dispose();
            roadPositionsAdded.Dispose();
            return mapPosition;
        }

        /*private static void SpawnRoad(int2 position, byte direction, int length)
        {

        }*/
	}
}
                //var chunkPosition = quadrantPosition2D.GetDictionaryPosition();
                //var voxelDimensions = new int3(planet.voxelDimensions, planet.voxelDimensions, planet.voxelDimensions);
                //var perlinOffset = new float2(chunkPosition.x * voxelDimensions.x, chunkPosition.y * voxelDimensions.z);
                // UnityEngine.Debug.LogError("perlinOffset: " + perlinOffset);
                // perlinOffset += new float2(8192, 8192);

                /*int2 mapPosition;
                for (mapPosition.x = 0; mapPosition.x < size.x; mapPosition.x++)
                {
                    for (mapPosition.y = 0; mapPosition.y < size.y; mapPosition.y++)
                    {
						// arrayIndex = (int) (mapPosition.x + mapPosition.y * size.y);
						arrayIndex = (int) (mapPosition.x + mapPosition.y * size.x);
                        if (mapPosition.x == 0 || mapPosition.x == size.x - 1 || mapPosition.y == 0 || mapPosition.y == size.y - 1)
                        {
                            townMap2D.data[arrayIndex] = 2;
                        }
                        else
                        {
                            townMap2D.data[arrayIndex] = 1;
                        }
                    }
                }*/