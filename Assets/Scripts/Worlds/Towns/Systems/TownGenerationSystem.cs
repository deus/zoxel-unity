/*using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;
using Zoxel.Voxels;
// todo: use 2D Town Planet to place things
// todo: place doors next to roads
// todo: place gate and initial road randomly - so it can vary from centre - and vary on side of town it spawns
// todo: support vertical horizontal and diagonal roads -  but place mostly next to straight ones?
// todo: different house types for Merchant, Villager, Mayor, Priest (in a church), Farmer, etc
// todo: Merchant house to contain a chest - but it says it doesn't belong to you if you try and open it (chest owner IDs!)
// todo: If you attack anything in house - the owner will warn you - if he warned you already he will hit you until you leave the house
// todo: Spawn towers sometimes where a gate might be - goes up - including staircase - until it reaches top floor
//      - actually give it various levels with gaurds spawning per level

namespace Zoxel.Worlds
{
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class TownGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        const int buildingPadding = 4;
        const int maxChecks = 4000;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref GenerateMegaChunk generateMegaChunk, ref MegaChunk megaChunk, in VoxLink voxLink) =>
            {
                if (generateMegaChunk.state != 1)
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<GenerateMegaChunk>(entityInQueryIndex, e);
                var worldSeed = generateMegaChunk.worldID;
                var random = new Random();
                random.InitState((uint) (worldSeed + megaChunk.position.x + megaChunk.position.z * 10000));
                int buildSizeMin = 3;
                int buildSizeMax = 8;
                int buildHeightVariance = 2;
                var buildingsCount = 2 + random.NextInt(6);
                for (int i = 0; i < megaChunk.towns.Length; i++)
                {
                    var town = megaChunk.towns[i];
                    GenerateTown(ref town, ref random, in megaChunk, i, buildingsCount, buildSizeMin, buildSizeMax, buildHeightVariance);
                    megaChunk.towns[i] = town;
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        // Later put towns in their own entities and generate it that way
        //      Do this for buildings too, to give more unique variation, they need their own system for increased details
        private static void GenerateTown(ref Town town, ref Random random, in MegaChunk megaChunk, int i, int buildingsCount, int buildSizeMin, int buildSizeMax, int buildHeightVariance)
        {
            town.gateSide = (byte) random.NextInt(0, 4);
            town.wallThickness = random.NextInt(6);
            town.gateThickness = random.NextInt(3, 6);
            // search new position for town
            // for all previously added towns, make sure doesn't conflict with other town positions
            // --- Town Positioning and Size ---
            //UnityEngine.Debug.Log("- New Town [" + i + "] Added at: " +  data.position + " : " + data.dimensions + " with buildings [" + buildingsCount + "]");
            // --- Spawn Roads ---
            // spawn roads randomly using cellular automata or something
            int roadOffset = 3 + town.wallThickness;
            for (int j = 0; j < town.roads.Length; j++)
            {
                var road = new Road();
                road.roadID = town.biome.stoneID;
                var spawnPosition = new int3();
                road.beginPosition = spawnPosition;
                if (town.gateSide == 0)
                {
                    road.endPosition = spawnPosition + new int3(0, 0, town.dimensions.z + town.wallThickness);
                }
                else if (town.gateSide == 1)
                {
                    road.beginPosition = spawnPosition + new int3(0, 0, -town.dimensions.z - town.wallThickness);
                    road.endPosition = spawnPosition;
                }
                else if (town.gateSide == 2)
                {
                    road.endPosition = spawnPosition + new int3(town.dimensions.x + town.wallThickness, 0, 0);
                }
                else if (town.gateSide == 3)
                {
                    //road.endPosition = spawnPosition + new int3(-town.dimensions.x, 0, 4);
                    road.beginPosition = spawnPosition + new int3(-town.dimensions.x - town.wallThickness, 0, 0);
                    road.endPosition = spawnPosition;
                }
                road.width = 3;
                town.roads[j] = road;
            }

            // --- Spawn Buildings ---
            var buildings = new NativeList<Building>();
            for (int j = 0; j < buildingsCount; j++)    // town.buildings.Length
            {
                var building = new Building();
                building.SetWithBiome(in town.biome);
                building.doorSideType = (byte) random.NextInt(3);
                var buildingSize = buildSizeMin + random.NextInt(buildSizeMax - buildSizeMin);
                building.dimensions = new int3(
                    buildingSize,
                    4 + random.NextInt(buildHeightVariance),
                    buildingSize);
                // search new position for building
                // for all previously added buildings, make sure doesn't conflict with other building positions
                var buildingPlaceOffset = (int)town.wallThickness * 2 + buildingSize + 1; // + 4;
                bool hasFoundNewPosition = false;
                int checks = 0;
                var newBuildingPosition = int3.zero;
                var townDimensionsLower = new int3(-town.dimensions.x + buildingPlaceOffset, 0, -town.dimensions.z + buildingPlaceOffset);
                var townDimensionsDouble = new int3((town.dimensions.x - buildingPlaceOffset) * 2, 0, (town.dimensions.z - buildingPlaceOffset) * 2);
                while (!hasFoundNewPosition)
                {
                    // new spawn position should be outside of bounds of town walls
                    //      position local to town position
                    var spawnPosition = new int3(townDimensionsLower.x + random.NextInt(townDimensionsDouble.x), 0,
                        townDimensionsLower.z + random.NextInt(townDimensionsDouble.z));
                    var rightSide = spawnPosition.x + building.dimensions.x;
                    var leftSide = spawnPosition.x - building.dimensions.x;
                    var upSide = spawnPosition.z + building.dimensions.z;
                    var downSide = spawnPosition.z - building.dimensions.z;
                    bool isTooCloseToOthers = false;
                    //  check if new building is too close to other buildings
                    for (int k = buildings.Length - 1; k >= 0; k--)
                    {
                        var previousBuilding = buildings[k];
                        var rightSide2 = previousBuilding.position.x + previousBuilding.dimensions.x + buildingPadding;
                        var leftSide2 = previousBuilding.position.x - previousBuilding.dimensions.x - buildingPadding;
                        var upSide2 = previousBuilding.position.z + previousBuilding.dimensions.z + buildingPadding;
                        var downSide2 = previousBuilding.position.z - previousBuilding.dimensions.z - buildingPadding;
                        if (!(rightSide < leftSide2 || leftSide > rightSide2 || upSide < downSide2 || downSide > upSide2))
                        {
                            isTooCloseToOthers = true;
                            break;
                        }
                    }
                    // todo: Check if it intercepts any roads
                    var isNextToRoad = false;
                    for (int k = 0; k < town.roads.Length; k++)
                    {
                        var road = town.roads[k];
                        var leftSide2 = road.beginPosition.x - road.width;
                        var rightSide2 = road.endPosition.x + road.width;
                        var downSide2 = road.beginPosition.z; // - road.width;
                        var upSide2 = road.endPosition.z; // + road.width;
                        if (road.beginPosition.x != road.endPosition.x)
                        {
                            leftSide2 = road.beginPosition.x;
                            rightSide2 = road.endPosition.x;
                            downSide2 = road.beginPosition.z - road.width; // - road.width;
                            upSide2 = road.endPosition.z + road.width; // + road.width;
                        }
                        if (!(rightSide < leftSide2 || leftSide > rightSide2 || upSide < downSide2 || downSide > upSide2))
                        {
                            isTooCloseToOthers = true;
                            break;
                        }
                        // if not next to road, then it sucks
                        if (leftSide - 1 == rightSide2 && downSide >= downSide2 && upSide < upSide2)
                        {
                            isNextToRoad = true;
                            building.doorSideType = 2;
                        }
                        else if (rightSide + 1 == leftSide2 && downSide >= downSide2 && upSide < upSide2)   //  + 1
                        {
                            isNextToRoad = true;
                            building.doorSideType = 3;
                        }
                        //  && building.position.x > leftSide2 && building.position.x < rightSide2
                        else if (downSide - 1 == upSide2 && leftSide > leftSide2 - 2 && rightSide < rightSide2 + 2) //  + 1
                        {
                            isNextToRoad = true;
                            building.doorSideType = 1;
                        }
                        //  && building.position.x > leftSide2 && building.position.x < rightSide2
                        else if (upSide + 1 == downSide2 && leftSide > leftSide2 - 2 && rightSide < rightSide2 + 2) //  - 1
                        {
                            isNextToRoad = true;
                            building.doorSideType = 0;
                        }
                    }
                    if (!isTooCloseToOthers && isNextToRoad)
                    {
                        newBuildingPosition = spawnPosition;
                        hasFoundNewPosition = true;
                    }
                    checks++;
                    if (checks >= maxChecks)
                    {
                        // UnityEngine.Debug.LogError("Could not find building position.");
                        break;
                    }
                }
                if (hasFoundNewPosition)
                {
                    building.position = newBuildingPosition;
                    // town.buildings[j] = building;
                    buildings.Add(building);
                    //UnityEngine.Debug.Log("    - New Building [" + j + "] Added at: " +  building.position + " : " +  building.dimensions);
                }
            }
            town.buildings = new BlitableArray<Building>(buildings.Length, Allocator.Persistent);
            for (int j = 0; j < buildings.Length; j++)
            {
                town.buildings[j] = buildings[j];
            }
            buildings.Dispose();
        }
    }
}*/
                /*var testPosition = new int3(
                    -data.dimensions.x + roadOffset + random.NextInt(data.dimensions.x - roadOffset),
                    0,
                    -data.dimensions.z + roadOffset + random.NextInt(data.dimensions.z - roadOffset)
                );*/
                        /*rightSide2 = road.beginPosition.x + road.width;
                        leftSide2 = road.beginPosition.x - road.width;
                        upSide2 = road.endPosition.z + road.width; // + road.width;
                        downSide2 = road.beginPosition.z - road.width; // - road.width;*/