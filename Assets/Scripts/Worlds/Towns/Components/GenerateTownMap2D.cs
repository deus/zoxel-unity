using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Worlds
{
    //! Generates data for the TownMap2D.
	public struct GenerateTownMap2D : IComponentData
    {
        public byte state;
        public Random random;
        public int2 mapPosition;
    }
}