using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Worlds
{
    //! A 2D map of town data with a position and size.
	public struct TownMap2D : IComponentData
	{
        //! Size of TownMap
        public int2 size;
        //! Offset position from MegaChunk2D
        public int2 position;
        //! A 2D map of town placement data.
		public BlitableArray<byte> data;

		public void Initialize(int2 size)
		{
            if (size != this.size)
            {
                this.size = size;
                Dispose();
                this.data = new BlitableArray<byte>((int)(size.x * size.y), Allocator.Persistent);
            }
		}

		public void Dispose()
		{
			if (data.Length > 0)
			{
				data.Dispose();
			}
		}
	}
}