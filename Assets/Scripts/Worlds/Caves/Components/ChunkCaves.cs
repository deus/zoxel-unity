using Unity.Entities;
using Unity.Collections;
// using start points, and end points, generate tunnels through chunks
// for all tunnels, make air, under the ground

namespace Zoxel.Worlds
{
    public struct Tunnel
    {
        public int3 positionStart;
        public int3 positionEnd;
        public int thickness;

		public Tunnel(int3 positionStart, int3 positionEnd, int thickness)
		{
			this.positionStart = positionStart;
			this.positionEnd = positionEnd;
			this.thickness = thickness;
		}
    }
	public struct ChunkCaves : IComponentData
	{
		public BlitableArray<Tunnel> tunnels;

		public void Dispose()
		{
			if (tunnels.Length > 0)
			{
				tunnels.Dispose();
			}
		}
	}
}