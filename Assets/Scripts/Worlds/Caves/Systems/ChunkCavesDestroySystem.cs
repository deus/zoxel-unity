using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Worlds
{
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ChunkCavesDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, ChunkCaves>()
                .ForEach((in ChunkCaves chunkCaves) =>
			{
                chunkCaves.Dispose();
			}).ScheduleParallel();
		}
	}
}