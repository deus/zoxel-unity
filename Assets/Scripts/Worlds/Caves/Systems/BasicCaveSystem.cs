/*using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Zoxel.Voxels;
// todo: generate points on edge of chunk within each chunk, but based on the chunk positions of it and the neighbor chunk
// todo: a function that randomizes the tunnel steering towards the other points - closest point in the tunnel
// todo: know when its underground and above, and if it reaches above ground - change some of its blocks? or something

namespace Zoxel.Worlds
{
    [UpdateAfter(typeof(GrassSpawnSystem))]
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class BasicCaveSystem : SystemBase
	{
		[BurstCompile]
        protected override void OnUpdate()
        {
			var disableCaves = VoxelManager.instance.voxelSettings.disableCaves;
			var tunnelHeightVariation = 52;
			var tunnelSpawnChance = 100;
			var noiseScale = 0.15f;
			var tunnelThickness = 2; // 3
			var tunnelThicknessVariation = 1; // 3
			var holeNoise = 2;
            Entities.ForEach((ref GenerateChunk generateChunk, ref Chunk chunk, ref ChunkCaves chunkCaves, in ChunkPosition chunkPosition, in SimplexNoise simplexNoise,
				in Seed seed) =>
            {
                if (generateChunk.state == GenerateChunkState.Caves)
				{
                    generateChunk.state = GenerateChunkState.Town;
					if (generateChunk.loaded == 1 || disableCaves)
					{
						return;
					}
					// using perlin, make cave
					// point between this and -1 chunk
					var points = new NativeList<int3>();
					// this is meant to open up a tunnel, randomly, at the 4 sides of a chunk
					// possibly can add lower levels
					// their height will vary randomly
					// later make intersections in between them and control how much it changes?

					// atm doesn't work

					var randomC = new Random();
					var chunkPositionC = chunkPosition.position.ToFloat3() - new float3(0.5f, 0, 0);
					randomC.InitState(seed.GetChunkSeed(chunkPositionC));
					if (randomC.NextInt(100) <= tunnelSpawnChance)
					{
						var pointC = new int3(0, 8, 8);
						pointC.y += (int)(tunnelHeightVariation * ( 1 + simplexNoise.Generate(
							chunkPositionC.x * noiseScale,
							chunkPositionC.y * noiseScale,
							chunkPositionC.z * noiseScale)));
						points.Add(pointC);
					}

					var randomD = new Random();
					var chunkPositionD = chunkPosition.position.ToFloat3() + new float3(0.5f, 0, 0);
					randomD.InitState(seed.GetChunkSeed(chunkPositionD));
					if (randomD.NextInt(100) <= tunnelSpawnChance)
					{
						var pointD = new int3(15, 8, 8);
						pointD.y += (int)(tunnelHeightVariation * ( 1 + simplexNoise.Generate(
							chunkPositionD.x * noiseScale,
							chunkPositionD.y * noiseScale,
							chunkPositionD.z * noiseScale)));
						points.Add(pointD);
					}

					// chunks random, used for points in between tunnels
					var random = new Random();
					random.InitState(seed.GetChunkSeed(chunkPosition.position));

					// first spawn tunnels
					var tunnels = new NativeList<Tunnel>();
					// how can i generate tunnels without them colliding?
					if (points.Length == 0)
					{
						return;
					}
					var tunnelThickness2 = tunnelThickness + random.NextInt(tunnelThicknessVariation);
					if (points.Length == 1)
					{
						tunnels.Add(new Tunnel(points[0], points[0] + new int3(0, 0, 4 + random.NextInt(10)), tunnelThickness2));
					}
					else if (points.Length == 2)
					{
						tunnels.Add(new Tunnel(points[0], points[1], tunnelThickness2));
					}
					else if (points.Length == 3)
					{
						tunnels.Add(new Tunnel(points[0], points[1], tunnelThickness2));
						tunnels.Add(new Tunnel(points[1], points[2], tunnelThickness2));
					}
					else if (points.Length == 3)
					{
						tunnels.Add(new Tunnel(points[0], points[1], tunnelThickness2));
						tunnels.Add(new Tunnel(points[2], points[3], tunnelThickness2));
					}
					// tunnels.Add(new Tunnel(pointA, new int3(8, 8 + random.NextInt(16), 16), 3));
					chunkCaves.Dispose();
					chunkCaves.tunnels = new BlitableArray<Tunnel>(tunnels.Length, Allocator.Persistent);

					var voxelDimensions = chunk.voxelDimensions;
					for (int i = 0; i < tunnels.Length; i++)
					{
						var tunnel = tunnels[i];
						var direction = math.normalize((tunnel.positionEnd - tunnel.positionStart).ToFloat3());
						var distance = math.distance(tunnel.positionEnd.ToFloat3(), tunnel.positionStart.ToFloat3());
						for (int j = 0; j <= distance; j++)
						{
							// get position based on distance to
							var position = tunnel.positionStart.ToFloat3() + j * direction;
							var minDifference = new int3(random.NextInt(holeNoise), random.NextInt(holeNoise), random.NextInt(holeNoise));
							var maxDifference = new int3(random.NextInt(holeNoise), random.NextInt(holeNoise), random.NextInt(holeNoise));
							for (int k = -tunnel.thickness + minDifference.x; k <= tunnel.thickness - maxDifference.x; k++)
							{
								for (int l = -tunnel.thickness + minDifference.y; l <= tunnel.thickness - maxDifference.y; l++)
								{
									for (int m = -tunnel.thickness + minDifference.z; m <= tunnel.thickness - maxDifference.z; m++)
									{
										// get perpendicular from direction
										var voxelPosition = new int3((int)position.x + k, (int)position.y + l, (int)position.z + m);
										if (voxelPosition.x >= 0 && voxelPosition.x < voxelDimensions.x && 
											voxelPosition.y >= 0 && voxelPosition.y < voxelDimensions.y && 
											voxelPosition.z >= 0 && voxelPosition.z < voxelDimensions.z)
										{
											// increase chance when further from center of tunnel
											var distanceToCenter = math.min(math.min(math.abs(k), math.abs(l)), math.abs(m));
											if (random.NextInt(100) <= 80 - 20 * distanceToCenter)
											{
												var voxelIndex = VoxelUtilities.GetVoxelArrayIndex(voxelPosition, voxelDimensions);
												chunk.voxels[voxelIndex] = 0;
											}
										}
									}
								}
							}
						}
					}
				}
			}).ScheduleParallel();
		}
	}
}*/

					/*var randomA = new Random();
					randomA.InitState(simplexNoise.GetChunkSeed(chunkPosition.position.ToFloat3() - new float3(0, 0, 0.5f)));
					if (randomA.NextInt(100) <= tunnelSpawnChance)
					{
						var pointA = new int3(8, 8 + randomA.NextInt(tunnelHeightVariation), 0);
						points.Add(pointA);
					}

					var randomB = new Random();
					randomB.InitState(simplexNoise.GetChunkSeed(chunkPosition.position.ToFloat3() + new float3(0, 0, 0.5f)));
					if (randomA.NextInt(100) <= tunnelSpawnChance)
					{
						var pointB = new int3(8, 8 + randomB.NextInt(tunnelHeightVariation), 15);
						points.Add(pointB);
					}*/

					/*var voxelDimensions = chunk.voxelDimensions;
					var position = new int3(0, 0, 0);
                    var voxelIndex = 0;
					for (position.x = 0; position.x < voxelDimensions.x; position.x++)
					{
						for (position.y = 0; position.y < voxelDimensions.y; position.y++)
						{
							for (position.z = 0; position.z < voxelDimensions.z; position.z++)
							{
								if (position.y != 0)
								{
									// add some 3 dimensional noise here for caves and cliffs
									var noisePosition = position.ToFloat3() + new float3(
										chunkPosition.position.x * voxelDimensions.x,
										chunkPosition.position.y * voxelDimensions.y,
										chunkPosition.position.z * voxelDimensions.z);
									var noiseScale = 0.01f;
									var noiseValue = 0.5f * 0.5f * ( 1 + simplexNoise.Generate(
										noisePosition.x * noiseScale,
										noisePosition.y * noiseScale,
										noisePosition.z * noiseScale) ); 
									noiseValue += 0.25f * 0.5f * ( 1 + simplexNoise.Generate(
										noisePosition.x * noiseScale * 2f,
										noisePosition.y * noiseScale * 2f,
										noisePosition.z * noiseScale * 2f) ); 
									noiseValue += 0.125f * 0.5f * ( 1 + simplexNoise.Generate(
										noisePosition.x * noiseScale * 4f,
										noisePosition.y * noiseScale * 4f,
										noisePosition.z * noiseScale * 4f) ); 
									noiseValue += 0.066f * 0.5f * ( 1 + simplexNoise.Generate(
										noisePosition.x * noiseScale * 8f,
										noisePosition.y * noiseScale * 8f,
										noisePosition.z * noiseScale * 8f) );
									// caves
									if (noiseValue >= 0.72f)
									{
										chunk.voxels[voxelIndex] = 0;
									}
								}
                                voxelIndex++;
							}
						}
					}*/


/*UnityEngine.Debug.DrawLine(chunk.GetVoxelPosition(),
	chunk.GetVoxelPosition() + new float3(8.5f, 66, 8.5f),
	UnityEngine.Color.blue, 4);*/
	/*for (heightPosition.x = 0; heightPosition.x < chunk.voxelDimensions.x; heightPosition.x++)
	{
		for (heightPosition.y = 0; heightPosition.y < chunk.voxelDimensions.z; heightPosition.y++)
		{
			noisePosition = (perlinOffset + heightPosition);
			var biome = terrainHeightMap.biomes[math.min(terrainHeightMap.biomes.Length - 1, (int) chunkBiome.biomes[voxelIndex])];
			noisePosition.x *= biome.landScale;
			noisePosition.y *= biome.landScale;
			terrainHeightMap.heights[voxelIndex] = (int)(biome.landBase
				+ biome.landAmplitude * noise.snoise(noisePosition)
				+ biome.landAmplitude * noise.snoise(noisePosition * 10) / 4);
			voxelIndex++;
		}
	}*/

/*if (noiseValue < 0.333f)
{
	chunk.voxels[voxelIndex] = (byte)(biome.stoneID);
}*/