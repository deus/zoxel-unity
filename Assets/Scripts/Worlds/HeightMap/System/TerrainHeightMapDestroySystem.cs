using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Worlds
{
    //! Disposes of TerrainHeightMap on DestroyEntity event.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class TerrainHeightMapDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, TerrainHeightMap>()
                .ForEach((in TerrainHeightMap terrainHeightMap) =>
			{
                terrainHeightMap.DisposeFinal();
			}).ScheduleParallel();
		}
	}
}