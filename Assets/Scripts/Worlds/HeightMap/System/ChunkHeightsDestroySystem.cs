using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Worlds
{
    //! Disposes of TerrainHeightMap on DestroyEntity event.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ChunkHeightsDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, ChunkHeights>()
                .ForEach((in ChunkHeights chunkHeights) =>
			{
                chunkHeights.DisposeFinal();
			}).ScheduleParallel();
		}
	}
}