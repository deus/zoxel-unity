using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! Generates a simple 2D height map for PlanetChunk2D Entities.
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
	public partial class HeightGenerationSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery planetsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            planetsQuery = GetEntityQuery(ComponentType.ReadOnly<Planet>());
            RequireForUpdate(processQuery);
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
            var noiseA = 4f * 0.0007f;
            var noiseB = 2f * 0.004f;
            var noiseC = 1f * 0.012f;
            var planetHeightRadius = WorldsManager.instance.worldsSettings.planetHeightRadius;
			var disableHeights = WorldsManager.instance.worldsSettings.disableHeights;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var planetEntities = planetsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var planets = GetComponentLookup<Planet>(true);
            var worldDatas = GetComponentLookup<WorldData>(true);
            planetEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<InitializeEntity, PlanetChunk2D>()
                .ForEach((Entity e, int entityInQueryIndex, ref TerrainHeightMap terrainHeightMap, in QuadrantPosition2D quadrantPosition2D,
                    in SimplexNoise simplexNoise, in VoxLink voxLink) =>
            {
                var planet = planets[voxLink.vox];
                var worldData = worldDatas[voxLink.vox];
                float2 heightPosition;
                var chunkPosition = quadrantPosition2D.GetDictionaryPosition(); // new int3();
                var voxelDimensions = new int3(planet.voxelDimensions, planet.voxelDimensions, planet.voxelDimensions);
                terrainHeightMap.Initialize(voxelDimensions);
                if (disableHeights)
                {
                    for (int i = 0; i < terrainHeightMap.heights.Length; i++)
                    {
                        terrainHeightMap.heights[i] = planetHeightRadius;
                    }
                    return;
                }
                var perlinOffset = new float2(chunkPosition.x * voxelDimensions.x, chunkPosition.y * voxelDimensions.z) - new float2(8192, 8192);
                var voxelIndex = 0;
                for (heightPosition.x = 0; heightPosition.x < voxelDimensions.x; heightPosition.x++)
                {
                    for (heightPosition.y = 0; heightPosition.y < voxelDimensions.z; heightPosition.y++)
                    {
                        var noisePosition = perlinOffset + heightPosition;
                        //var biomeDataA = terrainHeightMap.biomes[0];
                        var biomeNoisePosition = noisePosition;	//biomeDataA.landScale * 1f;
                        // 3 octaves of perlin noise to give different 
                        var noiseValueA = 0.6f * 0.5f * ( 1 + simplexNoise.Generate(biomeNoisePosition.x * noiseA,
                            biomeNoisePosition.y * noiseA) ); 	//(1 + noise.snoise(biomeNoisePosition)) / 2f;
                        noiseValueA += 0.3f * 0.5f * ( 1 + simplexNoise.Generate(biomeNoisePosition.x * noiseB,
                            biomeNoisePosition.y * noiseB) ); 
                        noiseValueA += 0.15f * 0.5f * ( 1 + simplexNoise.Generate(biomeNoisePosition.x * noiseC,
                            biomeNoisePosition.y * noiseC) );
                        //noiseValueA += 2.5f * SimplexNoise.Generate(biomeNoisePosition.x, biomeNoisePosition.y); 				//noise.snoise(biomeNoisePositionA * 10) / 4;
                        //noiseValueA *= 2f;
                        //noiseValueA *= 32; // (0 + worldData.landAmplitude);
                        noiseValueA *= planetHeightRadius; // (0 + worldData.landAmplitude);
                        //(int)(biomeDataA.landBase + noiseValueA);
                        terrainHeightMap.heights[voxelIndex] = (int)(noiseValueA);  // worldData.landBase + 
                        //GenerateBiomeHeight(ref terrainHeightMap, ref chunkBiome, noisePosition);
                        voxelIndex++;
                    }
                }
            })  .WithReadOnly(planets)
                .WithReadOnly(worldDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}