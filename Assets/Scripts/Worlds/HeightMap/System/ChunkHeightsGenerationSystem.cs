using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
	//! Generates  a 3D height data for our voxel chunk using PlanetChunk2D's.
	/**
	*	Doesn't generate terrain when Chunk is LoadedChunk.
	*	\todo Optimize System; There's alot of conditional branching atm.
	*/
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class ChunkHeightsGenerationSystem : SystemBase
	{
		public const int disabledType = 10000;
		public const int bedrockType = 10001;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery planetsQuery;
        private EntityQuery planetChunk2DsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            planetsQuery = GetEntityQuery(
				ComponentType.Exclude<StreamVox>(),
				ComponentType.Exclude<OnSpawnedPlanetChunks>(),
				ComponentType.ReadOnly<Planet>(),
				ComponentType.ReadOnly<BiomeLinks>());
            planetChunk2DsQuery = GetEntityQuery(
				ComponentType.ReadOnly<QuadrantPosition2D>(),
				ComponentType.ReadOnly<TerrainHeightMap>());
            RequireForUpdate(processQuery);
            RequireForUpdate(planetsQuery);
            RequireForUpdate(planetChunk2DsQuery);
		}
		
		[BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
			PostUpdateCommands2.RemoveComponent<GenerateHeights>(processQuery);
			PostUpdateCommands2.AddComponent<GenerateTerrain>(processQuery);
			var disableChunkHeights = WorldsManager.instance.worldsSettings.disableChunkHeights;
			var planetOffset = new int3(0, 0, 0);
			var planetStretch = new float3(1, 1, 1);
			var planetHeightRadius = WorldsManager.instance.worldsSettings.planetHeightRadius;
			var edgeBlocksCount = planetHeightRadius;
			var waterHeight = WorldsManager.instance.worldsSettings.waterHeight;
            // var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var worldEntities = planetsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var planets = GetComponentLookup<Planet>(true);
			worldEntities.Dispose();
            var planetChunk2DEntities = planetChunk2DsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var planetChunkPositions = GetComponentLookup<QuadrantPosition2D>(true);
            var planetChunkTerrains = GetComponentLookup<TerrainHeightMap>(true);
			planetChunk2DEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<UpdateChunkNeighbors>() // , EditedChunk>()
				.WithAll<GenerateHeights>()
				.ForEach((Entity e, int entityInQueryIndex, ref ChunkHeights chunkHeights, in Chunk chunk, in ChunkPosition chunkPosition, in ChunkPlanetChunk2DLinks chunkPlanetChunk2DLinks, in VoxLink voxLink) =>
            {
                // UnityEngine.Profiling.Profiler.BeginSample("Heights Generation System");
                if (!planets.HasComponent(voxLink.vox))
                {
					// UnityEngine.Debug.LogError("Planet busy.");
                    return;
                }
                var voxelDimensions = chunk.voxelDimensions;
                chunkHeights.Initialize(voxelDimensions);
				if (disableChunkHeights)
				{
					for (int i = 0; i < chunkHeights.heights.Length; i++)
					{
						chunkHeights.heights[i] = new CalculateSolidData { distanceToGround = disabledType };
					}
                    return;
				}
                var planetRadius = planets[voxLink.vox].radius;
                var planetLowerBounds = - (new int3((int) (planetRadius * planetStretch.x), (int) (planetRadius * planetStretch.y), (int) (planetRadius * planetStretch.z)));
                var planetUpperBounds = new int3((int) (planetRadius * planetStretch.x), (int) (planetRadius * planetStretch.y), (int) (planetRadius * planetStretch.z));
                var voxelWorldPosition = chunkPosition.GetVoxelPosition(voxelDimensions);
                //  get heightmap
                //  todo: other sides
                //	todo: a lists of terrains for edges
                var intersectingMaps = new NativeList<TerrainHeightMap>();
                var intersectingSides = new NativeList<byte>();
                var hasHeights = chunkPlanetChunk2DLinks.quadrant;
                var hasHeightsY = hasHeights.GetBoolA();
                var hasHeightsX = hasHeights.GetBoolB();
                var hasHeightsZ = hasHeights.GetBoolC();
                for (int i = 0; i < chunkPlanetChunk2DLinks.chunks.Length; i++)
                {
                    var planetChunkEntity = chunkPlanetChunk2DLinks.chunks[i];
					var intersectingMap = planetChunkTerrains[planetChunkEntity];
					if (intersectingMap.heights.Length == 0)
					{
						// UnityEngine.Debug.LogError("intersectingMap.heights.Length == 0");
						intersectingMaps.Dispose();
						intersectingSides.Dispose();
						return;
					}
					intersectingMaps.Add(intersectingMap);
					intersectingSides.Add(planetChunkPositions[planetChunkEntity].quadrant);
                }
                if (intersectingMaps.Length == 0)
				{
					// UnityEngine.Debug.LogError("intersectingMaps.Length == 0: " + chunkPlanetChunk2DLinks.chunks.Length);
					for (int i = 0; i < chunkHeights.heights.Length; i++)
					{
						chunkHeights.heights[i] = new CalculateSolidData { distanceToGround = disabledType };
					}
					intersectingMaps.Dispose();
					intersectingSides.Dispose();
					return;
				}
				// UnityEngine.Debug.LogError("Generating 3D Height Map.");
                int3 globalPosition;
                bool isOutOfBoundsX;
                bool isOutOfBoundsY;
                bool isOutOfBoundsZ;
                var voxelIndex2 = 0;
                var localPosition = int3.zero;
                for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                {
                    for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                    {
                        for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
                        {
                            var calculateSolidData = new CalculateSolidData { distanceToGround = disabledType };
                            globalPosition = voxelWorldPosition + localPosition + planetOffset;
                            if (globalPosition.x >= planetLowerBounds.x && globalPosition.x <= planetUpperBounds.x
                                && globalPosition.y >= planetLowerBounds.y && globalPosition.y <= planetUpperBounds.y
                                && globalPosition.z >= planetLowerBounds.z && globalPosition.z <= planetUpperBounds.z)
                            {
                                chunkHeights.heights[voxelIndex2] = new CalculateSolidData { distanceToGround = bedrockType };
                                voxelIndex2++;
                                continue;
                            }
                            isOutOfBoundsX = globalPosition.x >= planetUpperBounds.x || globalPosition.x <= planetLowerBounds.x;
                            isOutOfBoundsY = globalPosition.y >= planetUpperBounds.y || globalPosition.y <= planetLowerBounds.y;
                            isOutOfBoundsZ = globalPosition.z >= planetUpperBounds.z || globalPosition.z <= planetLowerBounds.z;
							CalculateSolid(ref calculateSolidData, in intersectingMaps, in intersectingSides,
								localPosition, globalPosition, voxelDimensions, planetLowerBounds, planetUpperBounds,
								PlanetAxis.Horizontal, PlanetAxis.Depth, PlanetAxis.Depth, PlanetAxis.Horizontal,
								isOutOfBoundsX, isOutOfBoundsY, isOutOfBoundsZ, hasHeightsX, hasHeightsY, hasHeightsZ, 
								edgeBlocksCount);
                            chunkHeights.heights[voxelIndex2] = calculateSolidData;
                            voxelIndex2++;
                        }
                    }
                }
                intersectingMaps.Dispose();
                intersectingSides.Dispose();
                // UnityEngine.Profiling.Profiler.EndSample();
            })  .WithReadOnly(planets)
                .WithReadOnly(planetChunkPositions)
                .WithReadOnly(planetChunkTerrains)
                .ScheduleParallel(Dependency);
            // // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}

		private static void CalculateSolid(ref CalculateSolidData calculateSolidData, in NativeList<TerrainHeightMap> intersectingMaps, in NativeList<byte> intersectingSides,
			int3 localPosition, int3 globalPosition, int3 voxelDimensions, int3 planetLowerBounds, int3 planetUpperBounds,
			byte planetAxis, byte planetAxis2, byte planetAxisInput, byte planetAxisInput2,
			bool isOutOfBoundsX, bool isOutOfBoundsY, bool isOutOfBoundsZ, bool hasHeightsX, bool hasHeightsY, bool hasHeightsZ, int edgeBlocksCount)
		{
			//  && !isWithinHeight
			for (int i = 0; i < intersectingMaps.Length; i++)
			{
				var intersectingMap = intersectingMaps[i];
				var intersectingSide = intersectingSides[i];

				// === 1st - Y - Up ===
				if (intersectingSide == PlanetSide.Up) //  PlanetSide.Up)
				{
					if (globalPosition.x >= planetLowerBounds.x - edgeBlocksCount && globalPosition.x <= planetUpperBounds.x + edgeBlocksCount
						&& globalPosition.z >= planetLowerBounds.z - edgeBlocksCount && globalPosition.z <= planetUpperBounds.z + edgeBlocksCount
						&& globalPosition.y >= planetUpperBounds.y)
					{
						var planeHeight = intersectingMap.heights[VoxelUtilities.GetVoxelArrayIndexXZ(localPosition, voxelDimensions)];
						var distanceFromCore = globalPosition.y - planetUpperBounds.y;
						if (distanceFromCore >= 0)
						{
							if ((!hasHeightsX && !hasHeightsZ) || (!isOutOfBoundsX && !isOutOfBoundsZ)) // intersectingMaps.Length == 1 || (!isOutOfBoundsX && !isOutOfBoundsZ))
							{
								var distanceToGround = planeHeight - distanceFromCore;
								if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
								{
									calculateSolidData.distanceToGround = distanceToGround;
									calculateSolidData.blockDirection = intersectingSide;
								}
							}
							else if (distanceFromCore <= planeHeight)
							{
								CalculateSolidSemiFinal(ref calculateSolidData, in intersectingMaps, in intersectingSides,
									localPosition, globalPosition, voxelDimensions, planetLowerBounds, planetUpperBounds,
									i, PlanetAxis.Horizontal, PlanetAxis.Depth, PlanetAxis.Depth, PlanetAxis.Horizontal,
									isOutOfBoundsX, isOutOfBoundsY, isOutOfBoundsZ, hasHeightsX, hasHeightsY, hasHeightsZ);
							}
						}
					}
				}

				// === 1st - Y - Down ===
				else if (intersectingSide == PlanetSide.Down)
				{
					if (globalPosition.x >= planetLowerBounds.x - edgeBlocksCount && globalPosition.x <= planetUpperBounds.x + edgeBlocksCount
						&& globalPosition.z >= planetLowerBounds.z - edgeBlocksCount && globalPosition.z <= planetUpperBounds.z + edgeBlocksCount
						&& globalPosition.y <= planetLowerBounds.y)
					{
						var planeHeight = intersectingMap.heights[VoxelUtilities.GetVoxelArrayIndexXZ(localPosition, voxelDimensions)];
						var distanceFromCore = planetLowerBounds.y - globalPosition.y;
						if (distanceFromCore >= 0)
						{
							if (intersectingMaps.Length == 1 || (!isOutOfBoundsX && !isOutOfBoundsZ))
							{
								var distanceToGround = planeHeight - distanceFromCore;
								if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
								{
									calculateSolidData.distanceToGround = distanceToGround;
									calculateSolidData.blockDirection = intersectingSide;
								}
							}
							else if (distanceFromCore <= planeHeight)
							{
								CalculateSolidSemiFinal(ref calculateSolidData, in intersectingMaps, in intersectingSides,
									localPosition, globalPosition, voxelDimensions, planetLowerBounds, planetUpperBounds,
									i, PlanetAxis.Horizontal, PlanetAxis.Depth, PlanetAxis.Depth, PlanetAxis.Horizontal,
									isOutOfBoundsX, isOutOfBoundsY, isOutOfBoundsZ, hasHeightsX, hasHeightsY, hasHeightsZ);
							}
						}
					}
				}

				// === 1st - X - Left ===
				else if (intersectingSide == PlanetSide.Left)
				{
					if (globalPosition.y >= planetLowerBounds.y - edgeBlocksCount && globalPosition.y <= planetUpperBounds.y + edgeBlocksCount
						&& globalPosition.z >= planetLowerBounds.z - edgeBlocksCount && globalPosition.z <= planetUpperBounds.z + edgeBlocksCount
						&& globalPosition.x <= planetLowerBounds.x)
					{
						var planeHeight = intersectingMap.heights[VoxelUtilities.GetVoxelArrayIndexYZ(localPosition, voxelDimensions)];
						var distanceFromCore = planetLowerBounds.x - globalPosition.x;
						if (distanceFromCore >= 0)
						{
							if ((!hasHeightsY && !hasHeightsZ) || (!isOutOfBoundsY && !isOutOfBoundsZ)) // !isOutOfBoundsY && !isOutOfBoundsZ) // intersectingMaps.Length == 1 || ())
							{
								var distanceToGround = planeHeight - distanceFromCore;
								if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
								{
									calculateSolidData.distanceToGround = distanceToGround;
									calculateSolidData.blockDirection = intersectingSide;
								}
							}
							else if (distanceFromCore <= planeHeight)
							{
								CalculateSolidSemiFinal(ref calculateSolidData, in intersectingMaps, in intersectingSides,
									localPosition, globalPosition, voxelDimensions, planetLowerBounds, planetUpperBounds,
									i, PlanetAxis.Vertical, PlanetAxis.Depth, PlanetAxis.Depth, PlanetAxis.Vertical,
									isOutOfBoundsX, isOutOfBoundsY, isOutOfBoundsZ, hasHeightsX, hasHeightsY, hasHeightsZ);
							}
						}
					}
				}
				
				// === 1st - X - Right ===
				else if (intersectingSide == PlanetSide.Right)
				{
					if (globalPosition.y >= planetLowerBounds.y - edgeBlocksCount && globalPosition.y <= planetUpperBounds.y + edgeBlocksCount
						&& globalPosition.z >= planetLowerBounds.z - edgeBlocksCount && globalPosition.z <= planetUpperBounds.z + edgeBlocksCount
						&& globalPosition.x >= planetUpperBounds.x)
					{
						var planeHeight = intersectingMap.heights[VoxelUtilities.GetVoxelArrayIndexYZ(localPosition, voxelDimensions)];
						var distanceFromCore = globalPosition.x - planetUpperBounds.x;
						if (distanceFromCore >= 0)
						{
							if (intersectingMaps.Length == 1 || (!isOutOfBoundsY && !isOutOfBoundsZ))
							{
								var distanceToGround = planeHeight - distanceFromCore;
								if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
								{
									calculateSolidData.distanceToGround = distanceToGround;
									calculateSolidData.blockDirection = intersectingSide;
								}
							}
							else if (distanceFromCore <= planeHeight)
							{
								CalculateSolidSemiFinal(ref calculateSolidData, in intersectingMaps, in intersectingSides,
									localPosition, globalPosition, voxelDimensions, planetLowerBounds, planetUpperBounds,
									i, PlanetAxis.Vertical, PlanetAxis.Depth, PlanetAxis.Depth, PlanetAxis.Vertical,
									isOutOfBoundsX, isOutOfBoundsY, isOutOfBoundsZ, hasHeightsX, hasHeightsY, hasHeightsZ);
							}
						}
					}
				}
				
				// 1st - Backward
				else if (intersectingSide == PlanetSide.Backward)
				{
					if (globalPosition.y >= planetLowerBounds.y - edgeBlocksCount && globalPosition.y <= planetUpperBounds.y + edgeBlocksCount
						&& globalPosition.x >= planetLowerBounds.x - edgeBlocksCount && globalPosition.x <= planetUpperBounds.x + edgeBlocksCount
						&& globalPosition.z <= planetLowerBounds.z)
					{
						var planeHeight = intersectingMap.heights[VoxelUtilities.GetVoxelArrayIndexXY(localPosition, voxelDimensions)];
						var distanceFromCore = planetLowerBounds.z - globalPosition.z;
						if (distanceFromCore >= 0)
						{
							if (intersectingMaps.Length == 1 || (!isOutOfBoundsY && !isOutOfBoundsX))
							{
								var distanceToGround = planeHeight - distanceFromCore;
								if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
								{
									calculateSolidData.distanceToGround = distanceToGround;
									calculateSolidData.blockDirection = intersectingSide;
								}
							}
							else if (distanceFromCore <= planeHeight)
							{
								CalculateSolidSemiFinal(ref calculateSolidData, in intersectingMaps, in intersectingSides,
									localPosition, globalPosition, voxelDimensions, planetLowerBounds, planetUpperBounds,
									i, PlanetAxis.Vertical, PlanetAxis.Horizontal, PlanetAxis.Horizontal, PlanetAxis.Vertical,
									isOutOfBoundsX, isOutOfBoundsY, isOutOfBoundsZ, hasHeightsX, hasHeightsY, hasHeightsZ);
							}
						}
					}
				}
				
				// 1st - Forward
				else if (intersectingSide == PlanetSide.Forward)
				{
					if (globalPosition.y >= planetLowerBounds.y - edgeBlocksCount && globalPosition.y <= planetUpperBounds.y + edgeBlocksCount
						&& globalPosition.x >= planetLowerBounds.x  - edgeBlocksCount && globalPosition.x <= planetUpperBounds.x + edgeBlocksCount
						&& globalPosition.z >= planetUpperBounds.z)
					{
						var planeHeight = intersectingMap.heights[VoxelUtilities.GetVoxelArrayIndexXY(localPosition, voxelDimensions)];
						var distanceFromCore = globalPosition.z - planetUpperBounds.z;
						if (distanceFromCore >= 0)
						{
							if (intersectingMaps.Length == 1 || (!isOutOfBoundsY && !isOutOfBoundsX))
							{
								var distanceToGround = planeHeight - distanceFromCore;
								if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
								{
									calculateSolidData.distanceToGround = distanceToGround;
									calculateSolidData.blockDirection = intersectingSide;
								}
							}
							else if (distanceFromCore <= planeHeight)
							{
								CalculateSolidSemiFinal(ref calculateSolidData, in intersectingMaps, in intersectingSides,
									localPosition, globalPosition, voxelDimensions, planetLowerBounds, planetUpperBounds,
									i, PlanetAxis.Vertical, PlanetAxis.Horizontal, PlanetAxis.Horizontal, PlanetAxis.Vertical,
									isOutOfBoundsX, isOutOfBoundsY, isOutOfBoundsZ, hasHeightsX, hasHeightsY, hasHeightsZ);
							}
						}
					}
				}
			}
		}

		// -== 2nd - X - Z ==-
		private static void CalculateSolidSemiFinal(ref CalculateSolidData calculateSolidData, in NativeList<TerrainHeightMap> intersectingMaps, in NativeList<byte> intersectingSides,
			int3 localPosition, int3 globalPosition, int3 voxelDimensions, int3 planetLowerBounds, int3 planetUpperBounds,
			int i, byte planetAxis, byte planetAxis2, byte planetAxisInput, byte planetAxisInput2,
			bool isOutOfBoundsX, bool isOutOfBoundsY, bool isOutOfBoundsZ, bool hasHeightsX, bool hasHeightsY, bool hasHeightsZ)
		{
			//  && !isWithinHeight
			for (int j = 0; j < intersectingMaps.Length; j++)
			{
				if (i != j)
				{
					var intersectingMap2 = intersectingMaps[j];
					var intersectingSide2 = intersectingSides[j];
					// -== 2nd - X ==-
					if ((planetAxis == PlanetAxis.Horizontal || planetAxis2 == PlanetAxis.Horizontal) && isOutOfBoundsX)
					{
						var planetAxis3 = planetAxisInput;
						if (planetAxis2 == PlanetAxis.Horizontal)
						{
							planetAxis3 = planetAxisInput2;
						}
						if (intersectingSide2 == PlanetSide.Left || intersectingSide2 == PlanetSide.Right)
						{
							var planeHeight = intersectingMap2.heights[VoxelUtilities.GetVoxelArrayIndexYZ(localPosition, voxelDimensions)];
							// -== 2nd - X - Left ==-
							if (intersectingSide2 == PlanetSide.Left)
							{
								var distanceFromCore = planetLowerBounds.x - globalPosition.x;
								if (distanceFromCore >= 0)
								{
									if ((planetAxis3 == PlanetAxis.Vertical && (!hasHeightsY || !isOutOfBoundsY))
										|| (planetAxis3 == PlanetAxis.Depth && (!hasHeightsZ || !isOutOfBoundsZ)))
									{
										var distanceToGround = planeHeight - distanceFromCore;
										if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
										{
											calculateSolidData.distanceToGround = distanceToGround;
											calculateSolidData.blockDirection = PlanetSide.Left;
										}
									}
									else if (distanceFromCore <= planeHeight)
									{
										CalculateSolidFinal(ref calculateSolidData, in intersectingMaps, in intersectingSides,
											localPosition, globalPosition, voxelDimensions, planetLowerBounds, planetUpperBounds, i, j, planetAxis3);
									}
								}
							}
							// -== 2nd - X - Right ==-
							else if (intersectingSide2 == PlanetSide.Right)
							{
								var distanceFromCore = globalPosition.x - planetUpperBounds.x;
								if (distanceFromCore >= 0)
								{
									if ((planetAxis3 == PlanetAxis.Vertical && (!hasHeightsY || !isOutOfBoundsY))
										|| (planetAxis3 == PlanetAxis.Depth && (!hasHeightsZ || !isOutOfBoundsZ)))
									{
										var distanceToGround = planeHeight - distanceFromCore;
										if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
										{
											calculateSolidData.distanceToGround = distanceToGround;
											calculateSolidData.blockDirection = PlanetSide.Right;
										}
									}
									else if (distanceFromCore <= planeHeight)
									{
										CalculateSolidFinal(ref calculateSolidData, in intersectingMaps, in intersectingSides,
											localPosition, globalPosition, voxelDimensions, planetLowerBounds, planetUpperBounds, i, j, planetAxis3);
									}
								}
							}
						}
					}
					
					// -== 2nd - Z ==-
					if ((planetAxis == PlanetAxis.Depth || planetAxis2 == PlanetAxis.Depth) && isOutOfBoundsZ)
					{
						if (isOutOfBoundsZ && (intersectingSide2 == PlanetSide.Forward || intersectingSide2 == PlanetSide.Backward))
						{
							var planeHeight = intersectingMap2.heights[VoxelUtilities.GetVoxelArrayIndexXY(localPosition, voxelDimensions)];
							var planetAxis3 = planetAxisInput;
							if (planetAxis2 == PlanetAxis.Depth)
							{
								planetAxis3 = planetAxisInput2;
							}
							// 2nd - Backward
							if (intersectingSide2 == PlanetSide.Backward)
							{
								var distanceFromCore = planetLowerBounds.z - globalPosition.z;
								if (distanceFromCore >= 0)
								{
									if ((planetAxis3 == PlanetAxis.Horizontal && (!hasHeightsX || !isOutOfBoundsX))
										|| (planetAxis3 == PlanetAxis.Vertical && (!hasHeightsY || !isOutOfBoundsY)))
									{
										var distanceToGround = planeHeight - distanceFromCore;
										if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
										{
											calculateSolidData.distanceToGround = distanceToGround;
											calculateSolidData.blockDirection = PlanetSide.Backward;
										}
									}
									else if (distanceFromCore <= planeHeight)
									{
										CalculateSolidFinal(ref calculateSolidData, in intersectingMaps, in  intersectingSides,
											localPosition, globalPosition, voxelDimensions, planetLowerBounds, planetUpperBounds, i, j, planetAxis3);
									}
								}
							} 
							// 2nd - Forward
							else if (intersectingSide2 == PlanetSide.Forward)
							{
								var distanceFromCore = globalPosition.z - planetUpperBounds.z;
								if (distanceFromCore >= 0)
								{
									if ((planetAxis3 == PlanetAxis.Horizontal && (!hasHeightsX || !isOutOfBoundsX))
										|| (planetAxis3 == PlanetAxis.Vertical && (!hasHeightsY || !isOutOfBoundsY)))
									{
										var distanceToGround = planeHeight - distanceFromCore;
										if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
										{
											calculateSolidData.distanceToGround = distanceToGround;
											calculateSolidData.blockDirection = PlanetSide.Forward;
										}
									}
									else if (distanceFromCore <= planeHeight)
									{
										CalculateSolidFinal(ref calculateSolidData, in intersectingMaps, in  intersectingSides,
											localPosition, globalPosition, voxelDimensions, planetLowerBounds, planetUpperBounds, i, j, planetAxis3);
									}
								}
							}
						}
					}
					// -== 2nd - Y ==-
					if ((planetAxis == PlanetAxis.Vertical || planetAxis2 == PlanetAxis.Vertical) && isOutOfBoundsY)
					{
						if (intersectingSide2 == PlanetSide.Down || intersectingSide2 == PlanetSide.Up)
						{
							var planeHeight = intersectingMap2.heights[VoxelUtilities.GetVoxelArrayIndexXZ(localPosition, voxelDimensions)];
							var planetAxis3 = planetAxisInput;
							if (planetAxis2 == PlanetAxis.Vertical)
							{
								planetAxis3 = planetAxisInput2;
							}
							// 2nd - Down
							if (intersectingSide2 == PlanetSide.Down)
							{
								var distanceFromCore = planetLowerBounds.y - globalPosition.y;
								if (distanceFromCore >= 0)
								{
									if ((planetAxis3 == PlanetAxis.Horizontal && (!hasHeightsX || !isOutOfBoundsX))
										|| (planetAxis3 == PlanetAxis.Depth && (!hasHeightsZ || !isOutOfBoundsZ)))
									{
										var distanceToGround = planeHeight - distanceFromCore;
										if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
										{
											calculateSolidData.distanceToGround = distanceToGround;
											calculateSolidData.blockDirection = PlanetSide.Down;
										}
									}
									else if (distanceFromCore <= planeHeight)
									{
										CalculateSolidFinal(ref calculateSolidData, in intersectingMaps, in  intersectingSides,
											localPosition, globalPosition, voxelDimensions, planetLowerBounds, planetUpperBounds, i, j, planetAxis3);
									}
								}
							}
							// 2nd - Up
							else if (intersectingSide2 == PlanetSide.Up)
							{
								var distanceFromCore = globalPosition.y - planetUpperBounds.y;
								if (distanceFromCore >= 0)
								{
									if ((planetAxis3 == PlanetAxis.Horizontal && (!hasHeightsX || !isOutOfBoundsX))
										|| (planetAxis3 == PlanetAxis.Depth && (!hasHeightsZ || !isOutOfBoundsZ)))
									{
										var distanceToGround = planeHeight - distanceFromCore;
										if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
										{
											calculateSolidData.distanceToGround = distanceToGround;
											calculateSolidData.blockDirection = PlanetSide.Up;
										}
									}
									else if (distanceFromCore <= planeHeight)
									{
										CalculateSolidFinal(ref calculateSolidData, in intersectingMaps, in  intersectingSides,
											localPosition, globalPosition, voxelDimensions, planetLowerBounds, planetUpperBounds, i, j, planetAxis3);
									}
								}
							}
						}
					}
				}
			}
		}

		// -== 3rd - Z ==-
		private static void CalculateSolidFinal(ref CalculateSolidData calculateSolidData, in NativeList<TerrainHeightMap> intersectingMaps, in NativeList<byte> intersectingSides,
			int3 localPosition, int3 globalPosition, int3 voxelDimensions, int3 planetLowerBounds, int3 planetUpperBounds, int i, int j, byte planetAxis)
		{
			for (int k = 0; k < intersectingMaps.Length; k++)
			{
				if (i != k && j != k)
				{
					var intersectingMap3 = intersectingMaps[k];
					var intersectingSide3 = intersectingSides[k];
					if (planetAxis == PlanetAxis.Depth)
					{
						if (intersectingSide3 == PlanetSide.Backward || intersectingSide3 == PlanetSide.Forward)
						{
							var planeHeight = intersectingMap3.heights[VoxelUtilities.GetVoxelArrayIndexXY(localPosition, voxelDimensions)];
							if (intersectingSide3 == PlanetSide.Backward)
							{
								var distanceFromCore = planetLowerBounds.z - globalPosition.z;
								if (distanceFromCore >= 0)
								{
									var distanceToGround = planeHeight - distanceFromCore;
									if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
									{
										calculateSolidData.distanceToGround = distanceToGround;
										calculateSolidData.blockDirection = PlanetSide.Backward;
									}
								}
							}
							else if (intersectingSide3 == PlanetSide.Forward)
							{
								var distanceFromCore = globalPosition.z - planetUpperBounds.z;
								if (distanceFromCore >= 0)
								{
									var distanceToGround = planeHeight - distanceFromCore;
									if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
									{
										calculateSolidData.distanceToGround = distanceToGround;
										calculateSolidData.blockDirection = PlanetSide.Forward;
									}
								}
							}
						}
					}
					else if (planetAxis == PlanetAxis.Horizontal)
					{
						if (intersectingSide3 == PlanetSide.Left || intersectingSide3 == PlanetSide.Right)
						{
							var planeHeight = intersectingMap3.heights[VoxelUtilities.GetVoxelArrayIndexYZ(localPosition, voxelDimensions)];
							if (intersectingSide3 == PlanetSide.Left)
							{
								var distanceFromCore = planetLowerBounds.x - globalPosition.x;
								if (distanceFromCore >= 0)
								{
									var distanceToGround = planeHeight - distanceFromCore;
									if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
									{
										calculateSolidData.distanceToGround = distanceToGround;
										calculateSolidData.blockDirection = PlanetSide.Left;
									}
								}
							}
							else if (intersectingSide3 == PlanetSide.Right)
							{
								var distanceFromCore = globalPosition.x - planetUpperBounds.x;
								if (distanceFromCore >= 0)
								{
									var distanceToGround = planeHeight - distanceFromCore;
									if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
									{
										calculateSolidData.distanceToGround = distanceToGround;
										calculateSolidData.blockDirection = PlanetSide.Right;
									}
								}
							}
						}
					}
					else if (planetAxis == PlanetAxis.Vertical)
					{
						if (intersectingSide3 == PlanetSide.Down || intersectingSide3 == PlanetSide.Up)
						{
							var planeHeight = intersectingMap3.heights[VoxelUtilities.GetVoxelArrayIndexXZ(localPosition, voxelDimensions)];
							if (intersectingSide3 == PlanetSide.Down)
							{
								var distanceFromCore = planetLowerBounds.y - globalPosition.y;
								if (distanceFromCore >= 0)
								{
									var distanceToGround = planeHeight - distanceFromCore;
									if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
									{
										calculateSolidData.distanceToGround = distanceToGround;
										calculateSolidData.blockDirection = PlanetSide.Down;
									}
								}
							}
							else if (intersectingSide3 == PlanetSide.Up)
							{
								var distanceFromCore = globalPosition.y - planetUpperBounds.y;
								if (distanceFromCore >= 0)
								{
									var distanceToGround = planeHeight - distanceFromCore;
									if (math.abs(distanceToGround) < math.abs(calculateSolidData.distanceToGround))
									{
										calculateSolidData.distanceToGround = distanceToGround;
										calculateSolidData.blockDirection = PlanetSide.Up;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}