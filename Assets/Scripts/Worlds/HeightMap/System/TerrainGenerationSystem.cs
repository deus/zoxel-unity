using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;

namespace Zoxel.Worlds
{
    //! No need to generate.
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class EditedTerrainGenerationSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}
		
		[BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
				.WithNone<UpdateChunkNeighbors>()
                .WithAll<GenerateTerrain, EditedChunk>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<GenerateTerrain>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<GenerateVegetation>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }

	//! Generates terrain from planet chunk sides
	/**
	*	\todo Rewrite this better! There's alot of conditional branching atm.
	*	\todo Optimize? Used 10ms before.
	*	Doesn't generate terrain when Chunk is LoadedChunk.
	*/
    [BurstCompile, UpdateInGroup(typeof(WorldSystemGroup))]
    public partial class TerrainGenerationSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery worldsQuery;
        private EntityQuery chunk2DQuery;
        private EntityQuery biomesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            worldsQuery = GetEntityQuery(
				ComponentType.Exclude<StreamVox>(),
				ComponentType.Exclude<OnSpawnedPlanetChunks>(),
				ComponentType.ReadOnly<Planet>(),
				ComponentType.ReadOnly<BiomeLinks>());
            biomesQuery = GetEntityQuery(
				ComponentType.ReadOnly<Biome>(),
				ComponentType.Exclude<DestroyEntity>());
            chunk2DQuery = GetEntityQuery(
				ComponentType.ReadOnly<QuadrantPosition2D>(),
				ComponentType.ReadOnly<TerrainHeightMap>());
            RequireForUpdate(processQuery);
            RequireForUpdate(worldsQuery);
            RequireForUpdate(biomesQuery);
            RequireForUpdate(chunk2DQuery);
		}
		
		[BurstCompile]
        protected override void OnUpdate()
        {
			var disableTerrains = WorldsManager.instance.worldsSettings.disableTerrains;
			var disableGrass = WorldsManager.instance.worldsSettings.disableGrass;
			var planetHeightRadius = WorldsManager.instance.worldsSettings.planetHeightRadius;
			var stoneSpawnChance = WorldsManager.instance.worldsSettings.stoneSpawnChance;
			var dirtSurfaceHeight = WorldsManager.instance.worldsSettings.dirtSurfaceHeight;
			var edgeBlocksCount = planetHeightRadius;
			var planetOffset = new int3(0, 0, 0);
			var planetStretch = new float3(1, 1, 1);
			var waterHeight = WorldsManager.instance.worldsSettings.waterHeight;
			var groundNoiseScale = WorldsManager.instance.worldsSettings.groundNoiseScale;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var worldEntities = worldsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var planets = GetComponentLookup<Planet>(true);
            var biomeLinks = GetComponentLookup<BiomeLinks>(true);
			worldEntities.Dispose();
            var chunkEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var voxLinks = GetComponentLookup<VoxLink>(true);
			chunkEntities.Dispose();
            var biomeEntities = biomesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var biomes = GetComponentLookup<Biome>(true);
			biomeEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<UpdateChunkNeighbors, EditedChunk>()
				.WithAll<GenerateTerrain>()
				.ForEach((Entity e, int entityInQueryIndex, ref Chunk chunk, ref ChunkVoxelRotations chunkVoxelRotations, in ChunkHeights chunkHeights,
                    in ChunkPosition chunkPosition, in SimplexNoise simplexNoise, in ChunkPlanetChunk2DLinks chunkPlanetChunk2DLinks) =>
            {
                var voxLink = voxLinks[e];
                if (!planets.HasComponent(voxLink.vox))
                {
                    return;
                }
                // var spawnedStructures = 0;
                var position = int3.zero;
                var voxelDimensions = chunk.voxelDimensions;
                var planetRadius = planets[voxLink.vox].radius;
                var planetBiomes = biomeLinks[voxLink.vox].biomes;
                var biomeEntity = planetBiomes[0];
                if (!biomes.HasComponent(biomeEntity))
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<GenerateTerrain>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<GenerateVegetation>(entityInQueryIndex, e);
                var biome = biomes[biomeEntity];
                var planetLowerBounds = - (new int3((int)(planetRadius * planetStretch.x), (int) (planetRadius * planetStretch.y), (int) (planetRadius * planetStretch.z)));
                var planetUpperBounds = new int3((int) (planetRadius * planetStretch.x), (int) (planetRadius * planetStretch.y), (int) (planetRadius * planetStretch.z));
                var voxelWorldPosition = chunkPosition.GetVoxelPosition(voxelDimensions);
                int3 globalPosition;
                var voxelIndex2 = 0;
                // === First Loop
                if (chunkPlanetChunk2DLinks.chunks.Length == 0)
                {
                    return;
                }
                for (position.x = 0; position.x < voxelDimensions.x; position.x++)
                {
                    for (position.y = 0; position.y < voxelDimensions.y; position.y++)
                    {
                        for (position.z = 0; position.z < voxelDimensions.z; position.z++)
                        {
                            var calculateSolidData = chunkHeights.heights[voxelIndex2];
                            if (calculateSolidData.distanceToGround == ChunkHeightsGenerationSystem.bedrockType)
                            {
                                chunk.voxels[voxelIndex2] = biome.bedrockID;
                                voxelIndex2++;
                                continue;
                            }
                            if (calculateSolidData.blockDirection == PlanetSide.None)
                            {
                                voxelIndex2++;
                                continue;
                            }
                            globalPosition = voxelWorldPosition + position + planetOffset;
                            // now set blocks
                            if (calculateSolidData.distanceToGround >= 0 && calculateSolidData.distanceToGround != ChunkHeightsGenerationSystem.disabledType)
                            {
                                // get distance from center of planet
                                // if <= sand level, set to sand
                                // center of planet at 0,0,0 for now
                                var distanceToRadius = 0;
                                if (calculateSolidData.blockDirection == PlanetSide.Down)
                                {
                                    distanceToRadius = (- globalPosition.y) - planetRadius;
                                }
                                else if (calculateSolidData.blockDirection == PlanetSide.Up)
                                {
                                    distanceToRadius = globalPosition.y - planetRadius;
                                }
                                else if (calculateSolidData.blockDirection == PlanetSide.Left)
                                {
                                    distanceToRadius = (- globalPosition.x) - planetRadius;
                                }
                                else if (calculateSolidData.blockDirection == PlanetSide.Right)
                                {
                                    distanceToRadius = globalPosition.x - planetRadius;
                                }
                                else if (calculateSolidData.blockDirection == PlanetSide.Backward)
                                {
                                    distanceToRadius = (- globalPosition.z) - planetRadius;
                                }
                                else if (calculateSolidData.blockDirection == PlanetSide.Forward)
                                {
                                    distanceToRadius = globalPosition.z - planetRadius;
                                }
                                var sandRadius = planetRadius + waterHeight + 1;
                                sandRadius += (int)(2 * 0.5f * ( 1 + simplexNoise.Generate(globalPosition.x * groundNoiseScale,
                                    globalPosition.y * groundNoiseScale, globalPosition.z * groundNoiseScale)));;
                                var inSand = globalPosition.x >= -sandRadius && globalPosition.x <= sandRadius
                                    && globalPosition.y >= -sandRadius && globalPosition.y <= sandRadius
                                    && globalPosition.z >= -sandRadius && globalPosition.z <= sandRadius;
                                if (inSand && distanceToRadius <= waterHeight && calculateSolidData.distanceToGround == 0)
                                {
                                    //! \todo Grow sand around it in a few voxel direction
                                    chunk.voxels[voxelIndex2] = biome.sandID;
                                }
                                else if (calculateSolidData.distanceToGround == 0)
                                {
                                    if (!disableGrass)
                                    {
                                        var grassChance = (int)(100 * 0.5f * (1 + simplexNoise.Generate(globalPosition.x * groundNoiseScale,
                                            globalPosition.y * groundNoiseScale, globalPosition.z * groundNoiseScale)));
                                        if (grassChance <= stoneSpawnChance)
                                        {
                                            chunk.voxels[voxelIndex2] = biome.stoneID;
                                        }
                                        else
                                        {
                                            chunk.voxels[voxelIndex2] = biome.grassID;
                                        }
                                    }
                                    else
                                    {
                                        chunk.voxels[voxelIndex2] = biome.dirtID;
                                    }
                                }
                                // soil
                                else if (calculateSolidData.distanceToGround > 0 && calculateSolidData.distanceToGround <= dirtSurfaceHeight)
                                {
                                    chunk.voxels[voxelIndex2] = biome.dirtID;
                                }
                                // stone
                                else if (calculateSolidData.distanceToGround > dirtSurfaceHeight)
                                {
                                    chunk.voxels[voxelIndex2] = biome.stoneID;
                                }
                            }
                            else
                            {
                                var waterRadius = planetRadius + waterHeight;
                                var inWater = globalPosition.x >= -waterRadius && globalPosition.x <= waterRadius
                                    && globalPosition.y >= -waterRadius && globalPosition.y <= waterRadius
                                    && globalPosition.z >= -waterRadius && globalPosition.z <= waterRadius;
                                if (inWater)
                                {
                                    chunk.voxels[voxelIndex2] = biome.waterID;
                                }
                            }
                            voxelIndex2++;
                        }
                    }
                }
            })  .WithReadOnly(planets)
                .WithReadOnly(biomeLinks)
                .WithReadOnly(voxLinks)
                .WithReadOnly(biomes)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}