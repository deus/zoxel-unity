﻿using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Worlds
{
	//! Our 2D heightmaps used to generate the world.
	public struct TerrainHeightMap : IComponentData
	{
		public BlitableArray<int> heights;

		public void Initialize(int3 voxelDimensions)
		{
			heights = new BlitableArray<int>((int)(voxelDimensions.x * voxelDimensions.z), Allocator.Persistent);
		}

		public void DisposeFinal()
		{
			heights.DisposeFinal();
		}

		public void Dispose()
		{
			heights.Dispose();
		}
	}
}