using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Worlds
{
    //! Holds Voxel Height in a single array per chunk.
	public struct ChunkHeights : IComponentData
	{
		public BlitableArray<CalculateSolidData> heights;		// relating to voxel in Vox VoxelLinks

		public void Initialize(int3 voxelDimensions)
		{
			this.Dispose();
			this.heights = new BlitableArray<CalculateSolidData>(voxelDimensions.x * voxelDimensions.y * voxelDimensions.z, Allocator.Persistent);
		}

		public void Dispose()
		{
			this.heights.Dispose();
		}

		public void DisposeFinal()
		{
			this.heights.DisposeFinal();
		}
	}
}