using Unity.Entities;

namespace Zoxel.Worlds
{
    //! This can be used for different block texture maps.
    /*public enum BlockDirection // : byte
    {
        None,
        Down,
        Up,
        Left,
        Right,
        Backward,
        Forward,
        // 12 Cube Lines!
        // Vertical YX YZ Permutations
        DownLeft,
        DownRight,
        DownForward,
        DownBackward,
        UpLeft,
        UpRight,
        UpForward,
        UpBackward,
        // Horizontal XZ Permutations
        LeftForward,
        LeftBackward,
        RightForward,
        RightBackward,
        // Vertical-Horizontal YX-Z Permutations - 8 Corners
        DownLeftForward,
        DownLeftBackward,
        DownRightForward,
        DownRightBackward,
        UpLeftForward,
        UpLeftBackward,
        UpRightForward,
        UpRightBackward
    }*/
    //! Per Voxel data for the height from the planet's core.
    public struct CalculateSolidData
    {
        public byte blockDirection;     // using PlanetSide
        public int distanceToGround;
    }
}