using UnityEngine;

namespace Zoxel.Worlds
{
    [CreateAssetMenu(fileName = "WorldsSettings", menuName = "ZoxelSettings/WorldsSettings")]
    public partial class WorldsSettingsDatam : ScriptableObject
    {
        public WorldsSettings worldsSettings;
    }
}