using Unity.Mathematics;
using UnityEngine;
using System.Collections.Generic;

namespace Zoxel.Worlds
{
    public partial class WorldsManager : MonoBehaviour
    {
        public static WorldsManager instance;
        [UnityEngine.SerializeField] public WorldsSettingsDatam WorldsSettings;

        [UnityEngine.HideInInspector] public WorldsSettings realWorldsSettings;

        public WorldsSettings worldsSettings
        {
            get
            { 
                return realWorldsSettings;
            }
        }

        public void Awake()
        {
            instance = this;
            realWorldsSettings = WorldsSettings.worldsSettings;
        }
    }
}