using UnityEngine;
using Unity.Mathematics;
using System;

namespace Zoxel.Worlds
{
    //! Generation Settings for the world namespace.
    [Serializable]
    public struct WorldsSettings
    {
        [Header("Core")]
        public int megaChunkDivision;       // 32

        [Header("Planets")]
        public int planetRadius;            // 66
        public int planetHeightRadius;      // 32
        public float groundNoiseScale;      //  0.017f
        public int dirtSurfaceHeight;       // 3
        public int stoneSpawnChance;        // 26
        public int waterHeight;             // 14

        [Header("Veggies")]
        public int2 treeStump;              // 3, 8
        public int2 leafLength;             // 1, 3
        public int treeSpawnChance;         // 6
        public int voxGrassChance;         // 48

        [Header("Visual Debug")]
        public bool isDebugPlanetChunks;

        [Header("Worlds Disables")]
        public bool disableChunkHeights;
        public bool disableHeights;
        public bool disableTowns;
        public bool disableBiomes;
        public bool disableCaves;
        public bool disableTerrains;

        [Header("Veggies Disables")]
        public bool disableVegetation;
        public bool disableTrees;
        public bool disableGrass;
        public bool disableWeeds;

        public void DrawUI()
        {
            GUILayout.Label(" [worlds] ");
            //GUILayout.Label("   Minivox Render Distance");
            // = int.Parse(GUILayout.TextField(.ToString()));
        }

        public void DrawDisableUI()
        {
            GUILayout.Label(" [debug worlds] ");
            isDebugPlanetChunks = GUILayout.Toggle(isDebugPlanetChunks, "Draw Planet Chunks");

            GUILayout.Label(" [debug worlds] ");
            // disableWorlds = GUILayout.Toggle(disableWorlds, "Disable Worlds");
            disableHeights = GUILayout.Toggle(disableHeights, "Disable Heights");
            disableGrass = GUILayout.Toggle(disableGrass, "Disable Grass");
            disableWeeds = GUILayout.Toggle(disableWeeds, "Disable Weeds");
            disableTrees = GUILayout.Toggle(disableTrees, "Disable Trees");
            disableTowns = GUILayout.Toggle(disableTowns, "Disable Towns");
            disableBiomes = GUILayout.Toggle(disableBiomes, "Disable Biomes");
            disableCaves = GUILayout.Toggle(disableCaves, "Disable Caves");
            disableTerrains = GUILayout.Toggle(disableTerrains, "Disable Terrains");
        }
    }
}