using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Lines;
// Todo: Draw Grid lines
// Todo: Make work in game as well, efficient line drawing system?

namespace Zoxel.Neural
{
    //! Debugs a neural network.
    /**
    *   \todo Convert to Parallel.
    */
	[UpdateInGroup(typeof(NeuralSystemGroup))]
    public partial class NeuralNetworkDebugSystem : SystemBase
    {
        // float timing = 0.01f;
        UnityEngine.Color lineColor = UnityEngine.Color.green;
        UnityEngine.Color lineColorEdge = new UnityEngine.Color(0.66f, 0.1f, 0.1f);
        UnityEngine.Color lineColorCorner = new UnityEngine.Color(0f, 0f, 0f);
        UnityEngine.Color lineColorMiddle = new UnityEngine.Color(0.8f, 0.8f, 0.8f);
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            // Add NeuralManager
            /*if (!Bootstrap.instance.debugSettings.isDebugNeuralNet)
            {
                return;
            }*/
            var isShowThickness = false;
            var inputOutputLength = 0.16f;
            // var materialType = 0;
            // var lineWidth = 0.025f;
            // var lineLife = 0.3f;
            // var lineLength = 16;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<OnChildrenSpawned>()
                .WithAll<NeuralNetwork>()
                .ForEach((Entity e, in Childrens childrens) =>
            {
                // hmm
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    var neuronA = childrens.children[i];
                    if (!HasComponent<Translation>(neuronA) || HasComponent<OnChildrenSpawned>(neuronA))
                    {
                        // UnityEngine.Debug.LogError("Issue with neuron at " + i);
                        continue;
                    }
                    var positionA = EntityManager.GetComponentData<Translation>(neuronA).Value;
                    var neuronValue = EntityManager.GetComponentData<Neuron>(neuronA).value;
                    
                    DebugLines.DrawCubeLines(positionA, quaternion.identity, 0.02f + neuronValue * 0.06f, UnityEngine.Color.black);
                    // feed neuron A to neuron B
                    // neuralNetwork.connections[i].feedForward();
                    // get any connections that neuron is related to
                    if (HasComponent<InputNeuron>(neuronA))
                    {
                        UnityEngine.Debug.DrawLine(positionA, positionA + new float3(0, inputOutputLength, 0), UnityEngine.Color.cyan);
                    }
                    var childrens2 = EntityManager.GetComponentData<Childrens>(neuronA);
                    if (childrens2.children.Length > 0)
                    {
                        for (int j = 0; j < childrens2.children.Length; j++)
                        {
                            var connectionEntity = childrens2.children[j];
                            var connection = EntityManager.GetComponentData<Connection>(connectionEntity);
                            var neuronB = connection.GetConnectedEntity(neuronA);
                            var positionB = EntityManager.GetComponentData<Translation>(neuronB).Value;
                            if (!isShowThickness)
                            {
                                UnityEngine.Debug.DrawLine(positionA, positionB, lineColor);
                            }
                            else
                            {
                                var weightThickness = (0.1f + 0.9f * connection.weight) * 0.03f;
                                var weightAddition = weightThickness / 4f;
                                for (var a = -weightThickness; a <= weightThickness; a += weightAddition)
                                {
                                    for (var b = -weightThickness; b <= weightThickness; b += weightAddition)
                                    {
                                        for (var c = -weightThickness; c <= weightThickness; c += weightAddition)
                                        {
                                            var add = new float3(a,b,c);
                                            UnityEngine.Debug.DrawLine(positionA + add, positionB + add, lineColor);  
                                        }
                                    }
                                }
                            }
                            // UnityEditor.Handles.DrawLine(positionA, positionB, connection.weight);
                            // RenderLineGroup.SpawnLine(PostUpdateCommands, elapsedTime, positionA, positionB, materialType, lineWidth, lineLife);
                        }
                    }
                    else
                    {
                        UnityEngine.Debug.DrawLine(positionA, positionA + new float3(0, inputOutputLength, 0), UnityEngine.Color.red);
                        //RenderLineGroup.SpawnLine(PostUpdateCommands, elapsedTime, positionA, positionA + new float3(0, 0.5f, 0), materialType, lineWidth, lineLife);
                    }
                }
            }).WithoutBurst().Run();
        }
    }
}