using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Lines;

namespace Zoxel.Neural
{
    //! Debugs a  Signal in a neural network.
    /**
    *   \todo Convert to Parallel.
    */
	[UpdateInGroup(typeof(NeuralSystemGroup))]
    public partial class SignalDebugSystem : SystemBase
    {
        // float timing = 0.01f;
        UnityEngine.Color lineColor = UnityEngine.Color.green;
        UnityEngine.Color lineColorEdge = new UnityEngine.Color(0.66f, 0.1f, 0.1f);
        UnityEngine.Color lineColorCorner = new UnityEngine.Color(0f, 0f, 0f);
        UnityEngine.Color lineColorMiddle = new UnityEngine.Color(0.8f, 0.8f, 0.8f);
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            // Add NeuralManager
            /*if (!Bootstrap.instance.debugSettings.isDebugNeuralNet)
            {
                return;
            }*/
            // var isShowThickness = false;
            // var inputOutputLength = 0.16f;
            var signalSpeed = 1f;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<Signal>()
                .ForEach((Entity e, in Signal signal) =>
            {
                var signalTargetPosition = EntityManager.GetComponentData<Translation>(signal.target).Value;
                var position = math.lerp(signal.positionStart, signalTargetPosition, (float) (elapsedTime - signal.timeBorn) / signalSpeed);
                DebugLines.DrawCubeLines(position, quaternion.identity, 0.02f + signal.value * 0.06f, UnityEngine.Color.white);
            }).WithoutBurst().Run();
        }
    }
}