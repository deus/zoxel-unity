using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;
using Zoxel.Transforms;
// pass in Creator (characters) ClanLink, VoxLink
//      pass in planet ZoxID (used for bullets?)
// todo: Make bullets use skill colours rather then random, just add a bit of variation

namespace Zoxel.Neural
{
    // Each Neuron creates signals to send to the connected neurons, via the connections
    [BurstCompile, UpdateInGroup(typeof(NeuralSystemGroup))]
    public partial class SignalArriveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery neuronQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            neuronQuery = GetEntityQuery(ComponentType.ReadWrite<Neuron>(), ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var signalSpeed = 1f;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var neuronEntities = neuronQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var neurons = GetComponentLookup<Neuron>(false);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, in Signal signal) =>
            {
                if (neuronEntities.Length == 0)
                {
                    return;
                }
                if (elapsedTime - signal.timeBorn >= signalSpeed)
                {
                    // i need to add value to the target neuron too
                    var neuron = neurons[signal.target];
                    neuron.value += signal.value;
                    neurons[signal.target] = neuron;
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                }
            })  .WithReadOnly(neuronEntities)
                .WithDisposeOnCompletion(neuronEntities)
                .WithNativeDisableContainerSafetyRestriction(neurons)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}