using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;
using Zoxel.Transforms;
// change later to unordered system
// render later with particle renderer per neuron
// [BurstCompile, UpdateInGroup(typeof(NeuralSystemGroup))]

namespace Zoxel.Neural
{
    //! Spawns neural connections.
    /**
    *   - Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(NeuralSystemGroup))]
    public partial class ConnectionSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery neuralNetworkQuery;
        private EntityQuery neuronQuery;
        private Entity connectionPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            neuralNetworkQuery = GetEntityQuery(
                ComponentType.ReadOnly<NeuralNetwork>(),
                ComponentType.ReadOnly<Childrens>(),
                ComponentType.Exclude<DestroyEntity>());
            neuronQuery = GetEntityQuery(
                ComponentType.ReadOnly<Neuron>(),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.Exclude<DestroyEntity>());
            var connectionArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(Connection),
                typeof(UnorderedChild),
                typeof(ParentLink),
                typeof(InitializeEntity)
            );
            connectionPrefab = EntityManager.CreateEntity(connectionArchetype);
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var seed = (uint) IDUtil.GenerateUniqueID();
            var connectionPrefab = this.connectionPrefab;
            // int layers = 2;
            // int neuronsPerLayer = 4;
            // pass in parent - list of neurons
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var neuralNetworkEntities = neuralNetworkQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var neuralNetworkChildrens = GetComponentLookup<Childrens>(true);
            var neuronEntities = neuronQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var neuronPositions = GetComponentLookup<Translation>(true);
            neuralNetworkEntities.Dispose();
            neuronEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity>()
                .WithAll<SpawnNeuronConnections>()
                .ForEach((Entity e, int entityInQueryIndex, in Translation translation, in ParentLink parentLink) =>
            {
                var random = new Random();
                random.InitState(seed);
                var parentLink2 = new ParentLink(e);
                var spawned = 0;
                // get list of closest neurons in next layer
                var nextNeurons = new NativeList<Entity>();
                var neurons = neuralNetworkChildrens[parentLink.parent].children;
                for (int i = 0; i < neurons.Length; i++)
                {
                    var otherNeuron = neurons[i];
                    var neuronPosition = neuronPositions[otherNeuron].Value;
                    if (neuronPosition.x == translation.Value.x + 1)
                    {
                        nextNeurons.Add(otherNeuron);
                    }
                }
                // now create connections
                for (int i = 0; i < nextNeurons.Length; i++)
                {
                    var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, connectionPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, parentLink2);
                    var connection = new Connection();
                    connection.weight = random.NextFloat(0f, 1f);   // randomize this
                    connection.neuronA = e;
                    connection.neuronB = nextNeurons[i];
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, connection);
                    spawned++;
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChildrenSpawnedInt(spawned));
                PostUpdateCommands.RemoveComponent<SpawnNeuronConnections>(entityInQueryIndex, e);
                nextNeurons.Dispose();
            })  .WithReadOnly(neuralNetworkChildrens)
                .WithReadOnly(neuronPositions)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}