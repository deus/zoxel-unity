using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;

namespace Zoxel.Neural
{
    /*[BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class NeuralNetworkDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			processQuery = GetEntityQuery(ComponentType.ReadOnly<DestroyEntity>(), ComponentType.ReadOnly<NeuralNetwork>(), ComponentType.ReadOnly<Childrens>(),
                ComponentType.Exclude<OnChildrenSpawned>());
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            if (!processQuery.IsEmpty)
            {
                var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
                Dependency = Entities
                    .WithNone<OnChildrenSpawned>() // OnChildrenSpawnedUnordered
                    .WithAll<DestroyEntity, NeuralNetwork>()
                    .ForEach((Entity e, int entityInQueryIndex, in Childrens childrens) =>
                {
                    for (int i = 0; i < childrens.children.Length; i++)
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, childrens.children[i]);
                    }
                    childrens.Dispose();
                    // PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                }).ScheduleParallel(Dependency);
                // commandBufferSystem.AddJobHandleForProducer(Dependency);
                SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).DestroyEntity(processQuery);
            }
        }
    }

	[UpdateAfter(typeof(NeuralNetworkDestroySystem))]
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class NeuronDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			processQuery = GetEntityQuery(ComponentType.ReadOnly<DestroyEntity>(), ComponentType.ReadOnly<Neuron>(), ComponentType.ReadOnly<Childrens>());
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            if (!processQuery.IsEmpty)
            {
                var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
                Dependency = Entities
                    .WithNone<OnChildrenSpawned>() // OnChildrenSpawnedUnordered
                    .WithAll<DestroyEntity, Neuron>()
                    .ForEach((Entity e, int entityInQueryIndex, in Childrens childrens) =>
                {
                    for (int i = 0; i < childrens.children.Length; i++)
                    {
                        var e2 = childrens.children[i];
                        // PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, connections.connections[i]);
                        PostUpdateCommands.DestroyEntity(entityInQueryIndex, e2);
                    }
                    childrens.Dispose();
                    // PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                }).ScheduleParallel(Dependency);
                // commandBufferSystem.AddJobHandleForProducer(Dependency);
                // commandBufferSystem.CreateCommandBuffer(World.Unmanaged).DestroyEntity(processQuery);
            }
        }
    }*/
}