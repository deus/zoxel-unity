using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Transforms;
// change later to unordered system
// render later with particle renderer per neuron

namespace Zoxel.Neural
{
    //! Spawns neural network entities.
    /**
    *   - Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(NeuralSystemGroup))]
    public partial class NeuralNetworkSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity neuralNetPrefab;
        private Entity neuronPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var neuralNetArchetype = EntityManager.CreateArchetype(
                // Events
                typeof(Prefab),
                typeof(OnChildrenSpawnedInt),
                typeof(OnChildrenSpawned),
                // Add Event to Spawn Connections
                // Data
                typeof(NeuralNetwork),
                typeof(Childrens)
            );
            neuralNetPrefab = EntityManager.CreateEntity(neuralNetArchetype);
            var neuronArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(NewChild),
                typeof(SpawnNeuronConnections),
                typeof(Neuron),
                typeof(Translation),
                typeof(ParentLink),
                typeof(Childrens)
            );
            neuronPrefab = EntityManager.CreateEntity(neuronArchetype);
            // spawn one for testing
            //var spawnNeuralNetworkArchetype = EntityManager.CreateArchetype(typeof(SpawnNeuralNetwork));
            //var e = EntityManager.CreateEntity(spawnNeuralNetworkArchetype);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var seed = (uint) IDUtil.GenerateUniqueID();
            var neuralNetPrefab = this.neuralNetPrefab;
            var neuronPrefab = this.neuronPrefab;
            int layers = 6;
            int hiddenNeurons = 8;    // for hidden layerss
            int inputNeurons = 2;
            int outputNeurons = 3;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<SpawnNeuralNetwork>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                var random = new Random();
                random.InitState(seed);
                var neuralNetEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, neuralNetPrefab);
                // set data for neural net
                //var neuralNet = new NeuralNetwork();
                //neuralNet.neurons = new BlitableArray<Neuron>(neuronsPerLayer * layers, Allocator.Persistent);
                var parentLink = new ParentLink(neuralNetEntity);
                var spawned = 0;
                for (int i = 0; i < layers; i++)
                {
                    var neuronsPerLayer = hiddenNeurons;
                    if (i == 0)
                    {
                        neuronsPerLayer = inputNeurons;
                    }
                    else if (i == layers - 1)
                    {
                        neuronsPerLayer = outputNeurons;
                    }
                    for (int j = 0; j < neuronsPerLayer; j++)
                    {
                        var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, neuronPrefab);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, parentLink);
                        // randomize starting value of neuron
                        spawned++;
                        var spawnPosition = new float3(i, j, 0);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Translation { Value = spawnPosition });
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Neuron(random.NextFloat(0f, 1f)));
                        if (i == 0)
                        {
                            PostUpdateCommands.AddComponent<InputNeuron>(entityInQueryIndex, e2);
                        }
                    }
                }
                // randomize starting neurons
                // PostUpdateCommands.SetComponent(entityInQueryIndex, neuralNetEntity, neuralNet);
                PostUpdateCommands.SetComponent(entityInQueryIndex, neuralNetEntity, new OnChildrenSpawnedInt(spawned));
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}