using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;
using Zoxel.Transforms;
// pass in Creator (characters) ClanLink, VoxLink
//      pass in planet ZoxID (used for bullets?)
// todo: Make bullets use skill colours rather then random, just add a bit of variation

namespace Zoxel.Neural
{
    // Each Neuron creates signals to send to the connected neurons, via the connections
    [BurstCompile, UpdateInGroup(typeof(NeuralSystemGroup))]
    public partial class FeedForwardSystemSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery connectionQuery;
        private Entity signalPrefab;
        double lastPulsed = 0;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            connectionQuery = GetEntityQuery(
                ComponentType.ReadOnly<Connection>(),
                ComponentType.Exclude<DestroyEntity>());
            var signalAchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(Signal)
            );
            signalPrefab = EntityManager.CreateEntity(signalAchetype);
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var signalSpeed = 1f;
            var elapsedTime = World.Time.ElapsedTime;
            if (!(elapsedTime - lastPulsed >= signalSpeed))
            {
                return;
            }
            lastPulsed = elapsedTime;
            var signalPrefab = this.signalPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var connectionEntities = connectionQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var connections = GetComponentLookup<Connection>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref Neuron neuron, in Childrens childrens, in Translation translation) =>
            {
                if (connectionEntities.Length == 0)
                {
                    return;
                }
                var isInputNeuron = HasComponent<InputNeuron>(e);
                var isSend = isInputNeuron || neuron.value >= 1;
                if (!isSend)
                {
                    return;
                }
                if (!isInputNeuron)
                {
                    neuron.value = 0;
                }
                // i should time the sending of neurons
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    var connectionEntity = childrens.children[i];
                    var connection = connections[connectionEntity];
                    // feed neuron A to neuron B
                    var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, signalPrefab);
                    var signal = new Signal();
                    signal.timeBorn = elapsedTime;
                    signal.target = connection.neuronB;
                    signal.positionStart = translation.Value;
                    signal.value = 0.5f * connection.weight; // 0.1f; // neuron.value;
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, signal);
                }
                // neuron.value = 0;    // as it gets sent out
            })  .WithReadOnly(connectionEntities)
                .WithDisposeOnCompletion(connectionEntities)
                .WithReadOnly(connections)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}