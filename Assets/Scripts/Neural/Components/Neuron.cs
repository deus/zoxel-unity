using Unity.Entities;
// value is just the sum - every time it exceeds limit, it will fire to connected ones

namespace Zoxel.Neural
{
    public struct Neuron : IComponentData
    {
        public float value;

        public Neuron(float value)
        {
            this.value = value;
        }
    }
}