using Unity.Entities;
// Each connection can transmit signals accross based on time
using Unity.Mathematics;

namespace Zoxel.Neural
{
    public struct Signal : IComponentData
    {
        public float value; // add to neuron when it reaches
        public double timeBorn;
        public Entity target;
        // debug
        public float3 positionStart;
        // public float3 positionEnd;
    }
}