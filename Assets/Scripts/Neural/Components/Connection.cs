using Unity.Entities;

namespace Zoxel.Neural
{
    public struct Connection : IComponentData
    {
        public float weight;
        public Entity neuronA;
        public Entity neuronB;

        public Entity GetConnectedEntity(Entity neuronC)
        {
            if (neuronA == neuronC)
            {
                return neuronB;
            }
            else
            {
                return neuronA;
            }
        }
    }
}