using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Zoxel.Dialogue.Authoring
{
    //! Settings for Dialogue, including cheats.
    // [GenerateAuthoringComponent]
    public struct DialogueSettings : IComponentData
    {
        public float2 timePerLetter;        // 0.09 to 0.16
    }

    public class DialogueSettingsAuthoring : MonoBehaviour
    {
        public float2 timePerLetter;
    }

    public class DialogueSettingsAuthoringBaker : Baker<DialogueSettingsAuthoring>
    {
        public override void Bake(DialogueSettingsAuthoring authoring)
        {
            AddComponent(new DialogueSettings
            {
                timePerLetter = authoring.timePerLetter
            });       
        }
    }
}