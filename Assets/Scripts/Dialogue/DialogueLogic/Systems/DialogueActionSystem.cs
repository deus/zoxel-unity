using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Quests;
using Zoxel.Items;
using Zoxel.Stats;
using Zoxel.UI;
using Zoxel.Items.UI;

namespace Zoxel.Dialogue
{
    //! Performs an action in the DialogueTree.
    /**
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class DialogueActionSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var spawnDelay = 0.1f;
            var spawnTradeUIPrefab = ItemUISpawnSystem.spawnTradeUIPrefab;
            var userQuestPrefab = QuestSystemGroup.userQuestPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var pickupItemPrefab = ItemHitSystem.pickupItemPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((Entity e, in DialogueController dialogueController, in ApplyDialogueAction applyDialogueAction) =>
            {
                PostUpdateCommands.RemoveComponent<ApplyDialogueAction>(e);
                var controllerEntity = EntityManager.GetComponentData<ControllerLink>(dialogueController.characterA).controller;
                // var logUILink = EntityManager.GetComponentData<LogUILink>(controllerEntity);
                if (applyDialogueAction.action.type == DialogueActionType.GiveQuest)
                {
                    var questlogB = EntityManager.GetComponentData<Questlog>(dialogueController.characterB);
                    var questID = applyDialogueAction.action.inputID;
                    var userQuestEntity = questlogB.GetQuest(EntityManager, questID);
                    if (userQuestEntity.Index != 0)
                    {
                        // get character A
                        //      Add quest for them
                        //      using the component of give quest, add it to their LogUI as well
                        // this quest should already be on character - questlog.requests - should save whether they are taken or not - or if limited
                        // add quest blocks here for slaying slime race entities
                        // later on used new system to give characters new quests
                        var questlogA = EntityManager.GetComponentData<Questlog>(dialogueController.characterA);
                        var givingUserQuest = EntityManager.GetComponentData<UserQuest>(userQuestEntity);
                        var givingUserQuestEntity = EntityManager.GetComponentData<MetaData>(userQuestEntity).data;
                        var characterBID = EntityManager.GetComponentData<ZoxID>(dialogueController.characterB).id;
                        var addUserQuest = new UserQuest(givingUserQuest, characterBID);
                        var addUserQuestEntity = PostUpdateCommands.Instantiate(userQuestPrefab);
                        PostUpdateCommands.SetComponent(addUserQuestEntity, new MetaData(givingUserQuestEntity));
                        PostUpdateCommands.SetComponent(addUserQuestEntity, new QuestHolderLink(dialogueController.characterA));
                        PostUpdateCommands.SetComponent(addUserQuestEntity, new UserDataIndex(questlogA.quests.Length));
                        PostUpdateCommands.SetComponent(addUserQuestEntity, addUserQuest);
                        PostUpdateCommands.AddComponent(dialogueController.characterA, new OnUserQuestSpawned((byte) (questlogA.quests.Length + 1)));
                        PostUpdateCommands.AddComponent<SaveQuestlog>(dialogueController.characterA);
                        var questName = EntityManager.GetComponentData<ZoxName>(givingUserQuestEntity);
                        // logUILink.AddToLogUI(PostUpdateCommands, "You Started Quest " + questName.name, elapsedTime);
                    }
                    /*else
                    {
                        UnityEngine.Debug.LogError("Failed to find quest: " + questID + " on characterB.");
                    }*/
                    PostUpdateCommands.AddComponent<IncrementDialogue>(e);
                }
                else if (applyDialogueAction.action.type == DialogueActionType.HandInQuest)
                {
                    var questID = applyDialogueAction.action.inputID;
                    var questlogA = EntityManager.GetComponentData<Questlog>(dialogueController.characterA);
                    if (questlogA.HasCompletedQuest(EntityManager, questID))
                    {
                        questlogA.OnQuestHandedIn(EntityManager, PostUpdateCommands, questID);
                        PostUpdateCommands.SetComponent(dialogueController.characterA, questlogA);
                        PostUpdateCommands.AddComponent<SaveQuestlog>(dialogueController.characterA);
                        var userQuestEntity = questlogA.GetQuest(EntityManager, questID);
                        var userQuest = EntityManager.GetComponentData<UserQuest>(userQuestEntity);
                        var questEntity = EntityManager.GetComponentData<MetaData>(userQuestEntity).data;
                        var quest = EntityManager.GetComponentData<Quest>(questEntity);
                        var questName = EntityManager.GetComponentData<ZoxName>(questEntity);

                        var realmEntity = EntityManager.GetComponentData<RealmLink>(dialogueController.characterA).realm;
                        var itemLinks = EntityManager.GetComponentData<ItemLinks>(realmEntity);
                        for (int i = 0; i < quest.rewards.Length; i++)
                        {
                            var reward = quest.rewards[i];
                            if (reward.type == QuestRewardType.Experience)
                            {
                                // Add GainExperience event here.
                                // reward.quantity
                                //! \todo GainExperience to be an event
                                PostUpdateCommands.AddComponent(dialogueController.characterA, new GainExperience(reward.quantity));
                                /*for (int j = 0; j < userStatLinks.levels.Length; j++)
                                {
                                    var levelStat = userStatLinks.levels[j];
                                    if (levelStat.stat == reward.targetEntity) 
                                    {
                                        // Add XP here
                                        levelStat.experienceGained += reward.quantity;
                                        userStatLinks.levels[j] = levelStat;
                                        if (levelStat.experienceGained >= levelStat.experienceRequired)
                                        {
                                            didLevelUp = true;
                                        }
                                        break;
                                    }
                                }*/
                            }
                            else if (reward.type == QuestRewardType.Item)
                            {
                                for (int j = 0; j < itemLinks.items.Length; j++)
                                {
                                    var itemEntity = itemLinks.items[j];
                                    // var itemID = EntityManager.GetComponentData<ZoxID>(itemEntity).id;
                                    if (itemEntity == reward.targetEntity)
                                    {
                                        var worldItem = new WorldItem(itemEntity, reward.quantity, elapsedTime);
                                        var pickupEntity = PostUpdateCommands.Instantiate(pickupItemPrefab);
                                        PostUpdateCommands.SetComponent(pickupEntity, new PickupItem(dialogueController.characterA, worldItem));
                                        PostUpdateCommands.SetComponent(pickupEntity, new DelayEvent(elapsedTime, 0.01));
                                        /*PostUpdateCommands.AddComponent(dialogueController.characterA, new PickupItem
                                        {
                                            item = new WorldItem
                                            {
                                                data = item,
                                                quantity = reward.quantity
                                            }
                                        });*/
                                        // break;
                                    }
                                }
                                // break;  // todo: support multiple item pickups at once
                            }
                        }
                        /*if (didLevelUp)
                        {
                            PostUpdateCommands.AddComponent<LevelUp>(dialogueController.characterA);
                        }
                        PostUpdateCommands.SetComponent(dialogueController.characterA, userStatLinks);*/
                        // UnityEngine.Debug.LogError("Giving Character Rewards: " + foundIndex);
                        // todo: remove items from NPC as well

                        // todo: Play hand in sound
                        // todo: Add Particles when handing in quest

                        // logUILink.AddToLogUI(PostUpdateCommands, "You Handed in Quest " + questName.name, elapsedTime);
                    }
                    PostUpdateCommands.AddComponent<IncrementDialogue>(e);
                }
                else if (applyDialogueAction.action.type == DialogueActionType.OpenTrade)
                {
                    var spawnEntity = PostUpdateCommands.Instantiate(spawnTradeUIPrefab);
                    PostUpdateCommands.SetComponent(spawnEntity, new CharacterLink(dialogueController.characterA));
                    PostUpdateCommands.SetComponent(spawnEntity, new DelayEvent(elapsedTime, spawnDelay));
                    PostUpdateCommands.SetComponent(spawnEntity, new SpawnTradeUI(dialogueController.characterB));
                }
            }).WithoutBurst().Run();
        }
    }
}