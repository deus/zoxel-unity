using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Zoxel.UI;
using Zoxel.Quests;
using Zoxel.Races;
// todo: Visualize for dialogue - debugger
//      Press a secret key - opens up target character dialogue branches as a UI
//      Call it DialogueMakerUI
// todo:
//      If has finished quest - do different dialogue (condition block for that)
//      Make a Quest Accept UI or decline - like previous prototype
//      Input that into more dialogue responses - ' oh you didnt want to help me.. I hate you -- mintmen reputation'
//      LogUI - you have accepted the 'hunt all traitors' quest
// NPC Reputation
//          - Action Block for less reputation - If you chose no he likes you less
//          - Log UI entry - Mintman likes you less
//          - Condition Block for reputation being less then 0 - 'I hate you, leave me'
//          - (1) You have handed in Slime Slayer Quest (2) Mintman likes you more - when you hand in the quest

namespace Zoxel.Dialogue
{
    //! Generates dialogue for a character.
    /**
    *   \todo Break this into a node system.
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class GenerateDialogueSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            //var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            //Dependency = Entities
            Entities
                .WithNone<SummonedEntity>()
                .WithNone<GiveRealmQuest, GenerateQuest, LoadQuestlog>()
                .ForEach((Entity e, int entityInQueryIndex, ref DialogueTree dialogueTree, in GenerateDialogue generateDialogue, in Questlog questlog, in ZoxID zoxID, in RealmLink realmLink) =>
            {
                if (zoxID.id == 0)
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<GenerateDialogue>(e);
                var random = new Random();
                random.InitState((uint)zoxID.id);
                var branches = new NativeList<DialogueBranch>();
                var actions = new NativeList<DialogueAction>();
                var conditions = new NativeList<DialogueCondition>();
                // generate a basic dialogue tree
                var beginningDialogue = new DialogueBranch(IDUtil.GenerateUniqueID());
                // dialogueA.speakerType = 1; - not used atm
                var chanceA = random.NextInt(100);
                if (chanceA >= 70)
                {
                    beginningDialogue.speech = new Text("Good evening sir.");
                }
                else if (chanceA >= 40)
                {
                    beginningDialogue.speech = new Text("Good day sir.");
                    // beginningDialogue.speech = new Text("Thank you paypercutts!");
                }
                else
                {
                    beginningDialogue.speech = new Text("Hello there.");
                }
                branches.Add(beginningDialogue);
                if (questlog.quests.Length > 0)
                {
                    var userQuestEntity = questlog.quests[0];
                    var userQuest = EntityManager.GetComponentData<UserQuest>(userQuestEntity);
                    var questEntity = EntityManager.GetComponentData<MetaData>(userQuestEntity).data;
                    var quest = EntityManager.GetComponentData<Quest>(questEntity);
                    var questID = EntityManager.GetComponentData<ZoxID>(questEntity).id;
                    var raceLinks = EntityManager.GetComponentData<RaceLinks>(realmLink.realm);
                    var raceName = "Uknown";
                    if (quest.blocks.Length != 0)
                    {
                        var raceEntity = quest.blocks[0].target;
                        if (raceEntity.Index != 0)
                        {
                            raceName = EntityManager.GetComponentData<ZoxName>(raceEntity).name.ToString();
                        }
                    }
                    // UnityEngine.Debug.LogError("QuestID: " + questID);
                    // here there should be 3 text choices
                    // if user has already done quest, they will recieve the third (you have already done my quest boy)
                    //  If user has yet to accept, the first one (doyouwishtoquest)
                    //  If user is doing it, You have slayed [questHuntCount] [monsterType]'s. + Come back to me when you have hunted [totalHuntCount]

                    // conditions
                    var hasGotQuestCondition = new DialogueCondition(IDUtil.GenerateUniqueID(), questID, DialogueConditionType.HasQuest);
                    var hasCompletedQuestCondition = new DialogueCondition(IDUtil.GenerateUniqueID(), questID, DialogueConditionType.HasCompletedQuest);
                    var hasHandedInQuestCondition = new DialogueCondition(IDUtil.GenerateUniqueID(), questID, DialogueConditionType.HasHandedInQuest);

                    var alreadyGotQuestDialogue = new DialogueBranch(IDUtil.GenerateUniqueID());
                    alreadyGotQuestDialogue.speech = new Text("Finish your task, Peasant.");
                    var completedQuestDialogue = new DialogueBranch(IDUtil.GenerateUniqueID());
                    completedQuestDialogue.speech = new Text("Oh, nicely done. You are a violent one.");
                    var completedQuestDialogue2 = new DialogueBranch(IDUtil.GenerateUniqueID());
                    completedQuestDialogue2.speech = new Text("Here is my spare change boy.");
                    var handedInQuestDialogue = new DialogueBranch(IDUtil.GenerateUniqueID());
                    handedInQuestDialogue.speech = new Text("Thank you again for your violence.");
                    // condition for quersst
                    // insert what task it is here?
                    var offerQuestDialogue = new DialogueBranch(IDUtil.GenerateUniqueID());
                    offerQuestDialogue.speech = new Text("Slay them nasty " + raceName + "'s for me.");  // slimes*
                    // options
                    var dialogueC1 = new DialogueBranch(IDUtil.GenerateUniqueID());
                    dialogueC1.speech = new Text("Sure thing captain");
                    var giveQuestAction = new DialogueAction(IDUtil.GenerateUniqueID(), DialogueActionType.GiveQuest, questID);    
                    // if user accepts this quest, give them quest with ID X (use questlog, generate quest to give in another)
                    // for now just give them a quest
                    // Next Node should be an action block
                    // with type of GiveQuest (DialogueActionType)
                    var dialogueC2 = new DialogueBranch(IDUtil.GenerateUniqueID());
                    if (random.NextInt(100) >= 40)
                    {
                        dialogueC2.speech = new Text("I am busy with skipping");
                    }
                    else
                    {
                        dialogueC2.speech = new Text("I wish to be a florist");
                    }
                    var endDialogue = new DialogueBranch(IDUtil.GenerateUniqueID());
                    if (random.NextInt(100) >= 40)
                    {
                        endDialogue.speech = new Text("Goodbye..");
                    }
                    else
                    {
                        endDialogue.speech = new Text("Fare thee well traveler");
                    }
                    // Set node links
                    // dialogueA -> offerQuestDialogue
                    beginningDialogue.AddNextLink(hasGotQuestCondition.id);        // go to condition after first speech

                    // for condition, only offer quest if doesn't have quest

                    // conditions
                    // check if has quest already
                    hasGotQuestCondition.nextNodeA = hasCompletedQuestCondition.id;
                    hasGotQuestCondition.nextNodeB = offerQuestDialogue.id;

                    // completed Quest
                    hasCompletedQuestCondition.nextNodeA = hasHandedInQuestCondition.id;    // finished it?
                    hasCompletedQuestCondition.nextNodeB = alreadyGotQuestDialogue.id;      // still doing quest
                    alreadyGotQuestDialogue.AddNextLink(endDialogue.id);

                    // handed in quest
                    hasHandedInQuestCondition.nextNodeA = handedInQuestDialogue.id;     // already handed in!
                    // if no, it should hand it in here, next should be action for opening hand in quest UI
                    hasHandedInQuestCondition.nextNodeB = completedQuestDialogue.id;    // has no handed in yet!
                    handedInQuestDialogue.AddNextLink(endDialogue.id);

                    // hand in quest
                    var handInQuestaction = new DialogueAction(IDUtil.GenerateUniqueID(), DialogueActionType.HandInQuest, questID);   
                    completedQuestDialogue.AddNextLink(completedQuestDialogue2.id);
                    completedQuestDialogue2.AddNextLink(handInQuestaction.id);
                    handInQuestaction.nextNode = endDialogue.id;

                    // hasCompletedQuestCondition.AddNextLink(endDialogue.id);          // go to end speech

                    offerQuestDialogue.AddNextLinks(dialogueC1.id, dialogueC2.id);
                    dialogueC1.AddNextLink(giveQuestAction.id);
                    dialogueC2.AddNextLink(endDialogue.id);
                    giveQuestAction.nextNode = endDialogue.id;          // go to end speech after giving quest
                    branches.Add(offerQuestDialogue);
                    branches.Add(dialogueC1);
                    branches.Add(dialogueC2);
                    branches.Add(alreadyGotQuestDialogue);
                    branches.Add(completedQuestDialogue);
                    branches.Add(completedQuestDialogue2);
                    branches.Add(handedInQuestDialogue);
                    branches.Add(endDialogue);
                    actions.Add(giveQuestAction);
                    actions.Add(handInQuestaction);
                    conditions.Add(hasGotQuestCondition);
                    conditions.Add(hasCompletedQuestCondition);
                    conditions.Add(hasHandedInQuestCondition);
                }
                else
                {
                    if (generateDialogue.type == 4)
                    {
                        // UnityEngine.Debug.LogError("Open Trade UI Action.");
                        var endDialogue = new DialogueBranch(IDUtil.GenerateUniqueID());
                        if (random.NextInt(100) >= 40)
                        {
                            beginningDialogue.speech = new Text("Please browse my Wares");
                        }
                        else
                        {
                            beginningDialogue.speech = new Text("Welcome to my Shoppy Shop");
                        }
                        endDialogue.speech = new Text("Thank you, Come again");
                        // action
                        var tradeAction = new DialogueAction(IDUtil.GenerateUniqueID(), DialogueActionType.OpenTrade);  
                        beginningDialogue.AddNextLink(tradeAction.id);
                        // end dialogue
                        tradeAction.nextNode = (endDialogue.id);
                        branches.Add(endDialogue);
                        actions.Add(tradeAction);
                    }
                    else //if (random.NextInt(100) >= 50)
                    {
                        var endDialogue = new DialogueBranch(IDUtil.GenerateUniqueID());
                        if (random.NextInt(100) >= 40)
                        {
                            endDialogue.speech = new Text("I have nothing to offer thee.");
                        }
                        else
                        {
                            endDialogue.speech = new Text("You should be more careful..");
                        }
                        branches.Add(endDialogue);
                        beginningDialogue.AddNextLink(endDialogue.id);
                    }
                }
                // updates its next node
                branches[0] = (beginningDialogue);
                // set branches to tree
                dialogueTree.branches = new BlitableArray<DialogueBranch>(branches.Length, Allocator.Persistent);
                for (int i = 0; i < branches.Length; i++)
                {
                    dialogueTree.branches[i] = branches[i];
                }
                dialogueTree.actions = new BlitableArray<DialogueAction>(actions.Length, Allocator.Persistent);
                for (int i = 0; i < actions.Length; i++)
                {
                    dialogueTree.actions[i] = actions[i];
                }
                dialogueTree.conditions = new BlitableArray<DialogueCondition>(conditions.Length, Allocator.Persistent);
                for (int i = 0; i < conditions.Length; i++)
                {
                    dialogueTree.conditions[i] = conditions[i];
                }
                branches.Dispose();
                actions.Dispose();
                conditions.Dispose();
            }).WithoutBurst().Run();
            //}).ScheduleParallel(Dependency);
            //// commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}