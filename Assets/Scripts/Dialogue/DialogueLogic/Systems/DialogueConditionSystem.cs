using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Quests;

namespace Zoxel.Dialogue
{
    //! Completes a condition check during the dialogue section.
    /**
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class DialogueConditionSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((Entity e, in DialogueController dialogueController, in ApplyDialogueCondition applyDialogueCondition) =>
            {
                // get character A
                //      Add quest for them
                //      using the component of give quest, add it to their LogUI as well
                var dialogueCondition = applyDialogueCondition.condition;
                byte dialogueChoice;
                // Handle Quest Conditions
                if (    dialogueCondition.type == (byte) DialogueConditionType.HasQuest
                        || dialogueCondition.type == (byte) DialogueConditionType.HasCompletedQuest
                        || dialogueCondition.type == (byte) DialogueConditionType.HasHandedInQuest)
                {
                    var questlog = EntityManager.GetComponentData<Questlog>(dialogueController.characterA);
                    if (questlog.quests.Length > 0)
                    {
                        if (questlog.HasQuest(EntityManager, applyDialogueCondition.condition.inputID))
                        {
                            if (dialogueCondition.type == (byte) DialogueConditionType.HasHandedInQuest)
                            {
                                //UnityEngine.Debug.LogError("Checking if Handed in Quest!");
                                if (questlog.HasHandedInQuest(EntityManager, applyDialogueCondition.condition.inputID))
                                {
                                    //UnityEngine.Debug.LogError("Has Handed In Quest!");
                                    dialogueChoice = 0;
                                }
                                else
                                {
                                    //UnityEngine.Debug.LogError("Has Not Handed In Quest..");
                                    dialogueChoice = 1;
                                }
                            }
                            else if (dialogueCondition.type == (byte) DialogueConditionType.HasCompletedQuest)
                            {
                                //UnityEngine.Debug.LogError("Checking if Completed Quest!");
                                if (questlog.HasCompletedQuest(EntityManager, applyDialogueCondition.condition.inputID))
                                {
                                    // UnityEngine.Debug.LogError("Completed the Quest!");
                                    dialogueChoice = 0;
                                }
                                else
                                {
                                    dialogueChoice = 1;
                                }
                            }
                            else // if (dialogueCondition.type == (byte) DialogueConditionType.HasQuest)
                            {
                                dialogueChoice = 0;
                            }
                        }
                        else
                        {
                            dialogueChoice = 1;
                            // UnityEngine.Debug.LogError("Character does not have quest: " + applyDialogueCondition.condition.inputID);
                        }
                    }
                    else
                    {
                        dialogueChoice = 1;
                    }
                }
                else
                {
                    // havnt implemented yet
                    dialogueChoice = 0;
                }
                PostUpdateCommands.RemoveComponent<ApplyDialogueCondition>(e);
                PostUpdateCommands.AddComponent(e, new IncrementDialogue { dialogueChoice = dialogueChoice });
            }).WithoutBurst().Run();
        }
    }
}