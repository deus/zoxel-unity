using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Zoxel.UI;
using Zoxel.Quests;
using Zoxel.Races;

namespace Zoxel.Dialogue
{
    //! Generates dialogue for a Minion.
    /**
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class GenerateMinionDialogueSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            //var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<GenerateQuest>()
                .WithAll<GenerateDialogue, SummonedEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref DialogueTree dialogueTree, in Questlog questlog, in ZoxID zoxID, in RealmLink realmLink) =>
            {
                if (zoxID.id == 0)
                {
                    return;
                }
                // generate a basic dialogue tree
                var beginningDialogue = new DialogueBranch(IDUtil.GenerateUniqueID());
                // dialogueA.speakerType = 1; - not used atm
                var random = new Random();
                random.InitState((uint)zoxID.id);
                var chance = random.NextInt(100);
                if (chance >= 70)
                {
                    beginningDialogue.speech = new Text("Yes me lord.");
                }
                else if (chance >= 40)
                {
                    beginningDialogue.speech = new Text("Work work!");
                }
                else
                {
                    beginningDialogue.speech = new Text("Something need doing?");
                }
                dialogueTree.branches = new BlitableArray<DialogueBranch>(1, Allocator.Persistent);
                dialogueTree.branches[0] = beginningDialogue;
                PostUpdateCommands.RemoveComponent<GenerateDialogue>(e);
            }).WithoutBurst().Run();
        }
    }
}