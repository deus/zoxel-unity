using Unity.Entities;
using Unity.Burst;
using Zoxel.AI;

namespace Zoxel.Dialogue
{
    //! Gives npc's a new brain state after they finish speaking.
    /**
    *   Gives npc's a new brain state after they finish speaking.
    */
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class StopSpeakingNPCSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DeadEntity>()
                .WithAll<StopSpeaking, Brain>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetBrainState(AIStateType.Idle));
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}