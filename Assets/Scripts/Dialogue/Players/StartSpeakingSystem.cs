using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;             // PlayerTooltips
using Zoxel.Players;        // ControllerLink
using Zoxel.AI;             // RotateTowards, AIRotateTowards
using Zoxel.Input;

namespace Zoxel.Dialogue
{
    //! Begins the speaking process.
    /**
    *   \todo Use new target component instead of raycaster.
    *   \todo Lock Target while speaking.
    */
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class StartSpeakingSystesm : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllerQuery;
        private EntityQuery charactersQuery;
        private NativeArray<Text> texts;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllerQuery = GetEntityQuery(
                ComponentType.ReadOnly<Controller>(),
                ComponentType.ReadOnly<CharacterLink>());
            charactersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Character>(),
                ComponentType.ReadOnly<ZoxName>());
            texts = new NativeArray<Text>(2, Allocator.Persistent);
            texts[0] = new Text("Talking to ");
            texts[1] =  new Text("\n[cancelAction] to Stop Talking");
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            const int speakCountDelay = 18;
            const double startSpeakingDelay = 1.2;
            var texts = this.texts;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = controllerQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var playerTooltipLinks = GetComponentLookup<PlayerTooltipLinks>(true);
            var characterEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var characterNames = GetComponentLookup<ZoxName>(true);
            controllerEntities.Dispose();
            characterEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref StartSpeaking startSpeaking, in ControllerLink controllerLink) =>
            {
                var playerTooltipLinks2 = playerTooltipLinks[controllerLink.controller];
                if (startSpeaking.state == 0)
                {
                    startSpeaking.timeStartedSpeaking = elapsedTime;
                    // command Player
                    // PostUpdateCommands.AddComponent(entityInQueryIndex, e, new RotateTowards(startSpeaking.partner));
                    PostUpdateCommands.AddComponent<RotateTowards>(entityInQueryIndex, e);
                    // command AI
                    PostUpdateCommands.AddComponent(entityInQueryIndex, startSpeaking.partner, new SetBrainState(AIStateType.Follow));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, startSpeaking.partner, new Target(e));
                    // Remove Game UI - Component
                    PostUpdateCommands.AddComponent<CloseRealmUI>(entityInQueryIndex, e);
                    // playerTooltipLinks2.SetPlayerSelectionTooltip(PostUpdateCommands, entityInQueryIndex, new Text());
                    PostUpdateCommands.AddComponent<ClearRenderText>(entityInQueryIndex, playerTooltipLinks2.actionsTooltip);
                }
                else if (startSpeaking.state == 1)
                {
                    if (elapsedTime - startSpeaking.timeStartedSpeaking >= startSpeakingDelay)
                    {
                        startSpeaking.state = 2;
                        if (HasComponent<ZoxName>(startSpeaking.partner))
                        {
                            var hitCharacterName = characterNames[startSpeaking.partner].name;
                            // var label = "Talking to " + hitCharacterName.ToString() + "\n[cancelAction] to Stop Talking";
                            var label = texts[0].Clone(); // new Text();
                            label.AddText(in hitCharacterName);
                            var label2 = texts[1];
                            label.AddText(in label2);
                            playerTooltipLinks2.SetPlayerSelectionTooltip(PostUpdateCommands, entityInQueryIndex, label);
                            // DialogueUISpawnSystem.SpawnDialogueUI(PostUpdateCommands, entityInQueryIndex, e, startSpeaking.partner);
                            PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SpawnDialogueUI(startSpeaking.partner));
                        }
                    }
                }
                else if (startSpeaking.state == speakCountDelay)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, controllerLink.controller, new SetControllerMapping(ControllerMapping.Dialogue));
                    PostUpdateCommands.RemoveComponent<StartSpeaking>(entityInQueryIndex, e);
                }
                if (startSpeaking.state != 1)
                {
                    startSpeaking.state++;
                }
			})  .WithReadOnly(playerTooltipLinks)
                .WithReadOnly(characterNames)
                .WithReadOnly(texts)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}