using Unity.Entities;
using Unity.Burst;
using Zoxel.UI;
using Zoxel.Items.UI;

namespace Zoxel.Dialogue
{
    //! Closes TradeUI after dialogue ends.
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class StopSpeakingTradeUISystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<StopSpeaking>()
                .ForEach((Entity e, int entityInQueryIndex, in UILink uiLink) =>
            {
                for (int i = 0; i < uiLink.uis.Length; i++)
                {
                    var uiEntity = uiLink.uis[i];
                    if (HasComponent<TradeUI>(uiEntity))
                    {
                        // UICoreSystem.RemoveUI(PostUpdateCommands, entityInQueryIndex, e, removeCharacterUIPrefab, uiEntity);
                        var removeEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, removeCharacterUIPrefab);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, removeEntity, new RemoveUI(e, uiEntity));
                        // PostUpdateCommands.SetComponent(entityInQueryIndex, removeEntity, new DelayEvent(timeStarted, timeDelay));
                        break;
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}