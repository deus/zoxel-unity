using Unity.Entities;
using Unity.Mathematics;
using Zoxel.UI;             // gets dialogue ui
using Zoxel.AI;             // for Attack AI
using Zoxel.Actions;        // for Cancel trigger

namespace Zoxel.Dialogue
{
    //! Ends dialogue if characters in combat.
    /**
    *   Checks if any of the characters are in combat, and ends dialogue if they are.
    *       Todo: Use InCombat tag for characters instead of ai.
    */
    [UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class DialogueCombatEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            // End dialogue of npc attacking
            // should be npc sending event OnSwitchedBrainState - then sending message to the controller
            Entities.ForEach((Entity e, in DialogueController dialogueController) =>
            {
                var isEndDialogue = false;
                if (HasComponent<Brain>(dialogueController.characterA))
                {
                    var brain = EntityManager.GetComponentData<Brain>(dialogueController.characterA);
                    if (brain.state == AIStateType.Attack)
                    {
                        isEndDialogue = true;
                    }
                }
                if (HasComponent<Brain>(dialogueController.characterB))
                {
                    var brain = EntityManager.GetComponentData<Brain>(dialogueController.characterB);
                    if (brain.state == AIStateType.Attack)
                    {
                        isEndDialogue = true;
                    }
                }
                if (HasComponent<CancelWorldAction>(dialogueController.characterA))
                {
                    isEndDialogue = true;
                }
                if (HasComponent<CancelWorldAction>(dialogueController.characterB))
                {
                    isEndDialogue = true;
                }
                if (isEndDialogue)
                {
                    PostUpdateCommands.AddComponent<EndDialogue>(e);
                }
            }).WithoutBurst().Run();
        }
    }
}