using Unity.Entities;
using Unity.Burst;
using Zoxel.Actions;        // for trigger
using Zoxel.AI;             // To check combat state
using Zoxel.UI;             // gets dialogue ui
using Zoxel.Cameras;        // disables camera
using Zoxel.Players;        // for input
using Zoxel.Input;
// todo: use new target instead
//          lock target while speaking
// todo: Bug, gets stuck if character dies during animating text, if target gains a new target
// change material of character to selected
// change last selected entity to deselected!
// TODO: Make it so you can talk to voxels but they don't talk back ;)
//      ^ involves a trade button and a talk button seperate

namespace Zoxel.Dialogue
{
    //! Begins the speaking flow from a player character.
    /**
    *   Begins talking to the npc from a player character. Checks if npc is in combat.
    *       Todo: Rewrite this to allow for npcs to initiate the talking as well. Add InCombat tag for characters.
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class PlayerDialogueBeginSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            Entities
                .WithNone<DestroyEntity, DeadEntity>()
                .WithNone<Speaking, StartSpeaking, StopSpeaking>()
                .WithAll<ActivateWorldAction>()
                .ForEach((Entity e, in RaycastCharacter raycastCharacter, in ControllerLink controllerLink, in CameraLink cameraLink) =>
            {
                // seperate into another system later
                if (!HasComponent<Speaking>(e))
                {
                    var hitCharacter = raycastCharacter.character;
                    if (hitCharacter.Index != 0 && HasComponent<DialogueTree>(hitCharacter))
                    {
                        var brain = EntityManager.GetComponentData<Brain>(hitCharacter);
                        if (brain.state != AIStateType.Attack)
                        {
                            // start speaking
                            PostUpdateCommands.AddComponent(controllerLink.controller, new SetControllerMapping(ControllerMapping.None));
                            PostUpdateCommands.AddComponent<Speaking>(e);
                            PostUpdateCommands.AddComponent(e, new StartSpeaking(hitCharacter));
                            PostUpdateCommands.AddComponent<DisableRaycaster>(e);
                            PostUpdateCommands.AddComponent<DisableFirstPersonCamera>(cameraLink.camera);
                        }
                    }
                }
            }).WithoutBurst().Run();
        }
    }
}