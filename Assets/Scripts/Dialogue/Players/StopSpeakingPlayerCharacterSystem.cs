using Unity.Entities;
using Unity.Burst;
using Zoxel.Players;
using Zoxel.Cameras;
using Zoxel.AI;
using Zoxel.Input;

namespace Zoxel.Dialogue
{
    //! PlayerCharacter things for when dialogue ends.
    /**
    *   Includes removing disables for raycasting and camera, as well as removing auto rotation.
    *   It also sets the Controller Mapping back to InGame.
    *   And it clears the selection tooltip.
    *   Finally, it spawns the Game UIs.
    */
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class StopSpeakingPlayerCharacterSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<StopSpeaking>()
                .ForEach((Entity e, int entityInQueryIndex, in CameraLink cameraLink, in ControllerLink controllerLink) =>
            {
                PostUpdateCommands.RemoveComponent<RotateTowards>(entityInQueryIndex, e);
                PostUpdateCommands.RemoveComponent<DisableRaycaster>(entityInQueryIndex, e);
                PostUpdateCommands.RemoveComponent<DisableFirstPersonCamera>(entityInQueryIndex, cameraLink.camera);
                PostUpdateCommands.AddComponent(entityInQueryIndex, controllerLink.controller, new SetControllerMapping(ControllerMapping.InGame));
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SpawnRealmUI(SpawnRealmUIType.Default));
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}