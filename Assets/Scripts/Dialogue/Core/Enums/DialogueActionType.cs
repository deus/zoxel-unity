namespace Zoxel.Dialogue
{
    // this is used for things like events - giving quests! etc
    // each action can be inserted into the dialogue tree
    public static class DialogueActionType
    {
        public const byte None = 0;
        public const byte GiveQuest = 1;
        public const byte HandInQuest = 2;
        public const byte GiveItem = 3;
        public const byte OpenTrade = 4;
    }
}