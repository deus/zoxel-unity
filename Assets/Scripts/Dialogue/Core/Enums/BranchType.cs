namespace Zoxel.Dialogue
{
    public enum BranchType : byte
    {
        End,
        Dialogue,       // with a speech type
        Action,         // givequest, item, do a dance, anything
        Condition,      // does an if check on something
        Choices         // options for player to speak as
    }
}