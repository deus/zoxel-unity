namespace Zoxel.Dialogue
{
    public enum DialogueConditionType : byte
    {
        None,
        HasQuest,
        HasCompletedQuest,
        HasHandedInQuest,
        HasSpokenToBefore   // Needs new data
    }
}

    // has quest
    // has started quest
    // has finished quest
    // has talked before