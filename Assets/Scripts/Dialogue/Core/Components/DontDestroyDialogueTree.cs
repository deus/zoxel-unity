using Unity.Entities;

namespace Zoxel.Dialogue
{
    //! Stops disposal of DialogueTree on DestroyEntity event.
    public struct DontDestroyDialogueTree : IComponentData { }
}