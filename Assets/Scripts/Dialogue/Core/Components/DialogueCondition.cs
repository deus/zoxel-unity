using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Dialogue
{
    //! Holds an condition check performed during dialogue.
    /**
    *   Holds an condition check performed during dialogue.
    *   Todo: Re structure this as a component.
    */
    public struct DialogueCondition : IComponentData
    {
        public int id;
        public byte type;
        public int nextNodeA;       // next node to go to after action
        public int nextNodeB;       // next node to go to after action
        public int inputID;         // what quest I am checking for
    
        public DialogueCondition(int id, int inputID, DialogueConditionType type)
        {
            this.id = id;
            this.type = (byte) type;
            this.nextNodeA = 0;
            this.nextNodeB = 0;
            this.inputID = inputID;
        }
    }
}