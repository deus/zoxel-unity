using Unity.Entities;

namespace Zoxel.Dialogue
{
    public struct SetDialogueBranch : IComponentData
    {
        public DialogueBranch branch;
        public byte isClear;
    }

}