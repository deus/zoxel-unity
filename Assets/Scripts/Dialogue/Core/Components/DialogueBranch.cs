using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Dialogue
{
    //! Holds speech and link to next branches.
    /**
    *   Holds speech and link to next branches.
    *   Todo: Rewrite this as entities
    */
    public struct DialogueBranch : IComponentData
    {
        public int id;              // node identifier
        public byte speakerType;    // whos speaking?
        public Text speech;         // convert this to blitablearray of bytes!
        public BlitableArray<int> links;

        public DialogueBranch(int id)
        {
            this.id = id;
            this.speakerType = 0;
            this.speech = new Text();
            this.links = new BlitableArray<int>(0, Allocator.Persistent);
        }

        public void Dispose()
        {
            if (links.Length > 0)
            {
                links.Dispose();
            }
        }

        public void AddNextLink(int nextNodeID)
        {
            Dispose();
            links = new BlitableArray<int>(1, Allocator.Persistent);
            links[0] = nextNodeID;
        }

        public void AddNextLinks(int nextNodeA, int nextNodeB)
        {
            Dispose();
            links = new BlitableArray<int>(2, Allocator.Persistent);
            links[0] = nextNodeA;
            links[1] = nextNodeB;
        }

        public static int GetBranchChoice(BlitableArray<int> links, int choiceIndex)
        {
            var branchID = -1;
            if (links.Length > 0 && choiceIndex < links.Length)
            {
                branchID = links[choiceIndex];
            }
            return branchID;
        }

        public int GetBranchChoice(int choiceIndex)
        {
            var branchID = -1;
            if (links.Length > 0 && choiceIndex < links.Length)
            {
                branchID = links[choiceIndex];
            }
            return branchID;
        }

        public void InitializeLinks(int newCount)
        {
            links = new BlitableArray<int>(newCount, Allocator.Persistent);
            for (int i = 0; i < links.Length; i++)
            {
                links[i] = 0;
            }
        }

        public void AddBranchLink()
        {
            var oldLinks = links.ToArray();
            links = new BlitableArray<int>(links.Length + 1, Allocator.Persistent);
            for (int i = 0; i < links.Length - 1; i++)
            {
                links[i] = oldLinks[i];
            }
            links[links.Length - 1] = 0;
        }

        public void SetSpeech(string newSpeech)
        {
            speech = new Text(newSpeech);
        }

        public void SetSpeakerType(bool newSpeakerType)
        {
            if (newSpeakerType)
            {
                speakerType = 1;
            }
            else
            {
                speakerType = 0;
            }
        }
    }
}
    /*public enum DialogueBlockType
    {
        Speech,
        Action,
        Options
    }*/
