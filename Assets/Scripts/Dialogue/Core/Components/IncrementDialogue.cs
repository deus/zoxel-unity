using Unity.Entities;

namespace Zoxel.Dialogue
{
    public struct IncrementDialogue : IComponentData
    {
        public byte dialogueChoice;
        public DialogueBranch branch;

        public IncrementDialogue(byte dialogueChoice)
        {
            this.dialogueChoice = dialogueChoice;
            this.branch = new DialogueBranch();
        }
    }    
}