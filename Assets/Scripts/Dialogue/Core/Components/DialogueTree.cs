using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Dialogue
{
    //! Data that creates interactive dialogue.
    /**
    *   \todo Refactor data to use entities for branch nodes.
    *   Holds a list of branches, actions and conditions for the dialogue system.
    *       todo: Rewrite this to keep branches, actions and conditions as entities
    */
    public struct DialogueTree : IComponentData
    {
        public BlitableArray<DialogueBranch> branches;
        public BlitableArray<DialogueAction> actions;
        public BlitableArray<DialogueCondition> conditions;

        public void DisposeFinal()
        {
            branches.DisposeFinal();
            actions.DisposeFinal();
            conditions.DisposeFinal();
        }

        public void Dispose()
        {
            branches.Dispose();
            actions.Dispose();
            conditions.Dispose();
        }

        public byte GetBranchType(int branchID)
        {
            for (int i = 0; i < branches.Length; i++) 
            {
                if (branches[i].id == branchID) 
                {
                    return (byte)BranchType.Dialogue;
                }
            }
            for (int i = 0; i < actions.Length; i++) 
            {
                if (actions[i].id == branchID) 
                {
                    return (byte)BranchType.Action;
                }
            }
            for (int i = 0; i < conditions.Length; i++) 
            {
                if (conditions[i].id == branchID) 
                {
                    return (byte)BranchType.Condition;
                }
            }
            return (byte)BranchType.End;
        }

        public DialogueBranch GetBranch(int branchID)
        {
            for (int i = 0; i < branches.Length; i++) 
            {
                if (branches[i].id == branchID) 
                {
                    return branches[i];
                }
            }
            return new DialogueBranch();
        }

        public DialogueAction GetAction(int branchID)
        {
            for (int i = 0; i < actions.Length; i++) 
            {
                if(actions[i].id == branchID) 
                {
                    return actions[i];
                }
            }
            return new DialogueAction();
        }

        public DialogueCondition GetCondition(int branchID)
        {
            for (int i = 0; i < conditions.Length; i++) 
            {
                if (conditions[i].id == branchID) 
                {
                    return conditions[i];
                }
            }
            return new DialogueCondition();
        }

        public int GetBranchIndex(int branchID)
        {
            for (int i = 0; i < branches.Length; i++) 
            {
                if (branches[i].id == branchID) 
                {
                    return i;
                }
            }
            return -1;
        }

        public int GetActionIndex(int branchID)
        {
            for (int i = 0; i < actions.Length; i++) 
            {
                if(actions[i].id == branchID) 
                {
                    return i;
                }
            }
            return -1;
        }
    }
}