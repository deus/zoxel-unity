using Unity.Entities;

namespace Zoxel.Dialogue
{
    //! An event for the character to stop speaking.
    /**
    *   An event for the character to stop speaking.
    */
    public struct StopSpeaking : IComponentData { }
}