using Unity.Entities;

namespace Zoxel.Dialogue
{
    public struct SendIncrementDialogue : IComponentData
    {
        public byte dialogueChoice;

        public SendIncrementDialogue(byte dialogueChoice)
        {
            this.dialogueChoice = dialogueChoice;
        }
    }
    
}