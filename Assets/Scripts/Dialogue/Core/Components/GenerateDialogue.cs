using Unity.Entities;

namespace Zoxel.Dialogue
{
    public struct GenerateDialogue : IComponentData
    {
        public byte type;

        public GenerateDialogue(byte type)
        {
            this.type = type;
        }
    }
}