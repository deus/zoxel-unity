using Unity.Entities;

namespace Zoxel.Dialogue
{
    //! Holds an action performed during dialogue.
    /**
    *   Holds an action performed during dialogue.
    *   Todo: Re structure this as a component.
    */
    public struct DialogueAction : IComponentData
    {
        public int id;
        public int nextNode;    // next node to go to after action
        public byte type;
        public int inputID;  // id for action, if giving quest its the ID of the quest
    
        public DialogueAction(int id, byte actionType, int inputID = 0)
        {
            this.id = id;
            this.nextNode = 0;
            this.type = actionType;
            this.inputID = inputID;
        }
    }
}