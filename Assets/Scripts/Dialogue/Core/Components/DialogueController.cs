﻿using Unity.Entities;

namespace Zoxel.Dialogue
{
    //! A Dialogue Controller Entity.
    /**
    *   Holds the participants and the uis of the conversation in one place.
    */
    public struct DialogueController : IComponentData
    {
        //! Has the controller initialized state.
        public byte init;
        //! Which branch the conversation is on.
        public int branchID;
        //! The type of branch the conversation is on.
        public byte branchType;
        //! Remembers last choice state.
        public byte lastWasChoice;
        public Entity characterA;
        public Entity dialogueUIA;
        public Entity characterB;
        public Entity dialogueUIB;

        public DialogueController(Entity characterA, Entity dialogueUIA, Entity characterB, Entity dialogueUIB)
        {
            this.branchID = 0;
            this.branchType = 0;
            this.init = 0;
            this.dialogueUIA = dialogueUIA;
            this.dialogueUIB = dialogueUIB;
            this.characterA = characterA;
            this.characterB = characterB;
            this.lastWasChoice = 0;
        }
    }
}