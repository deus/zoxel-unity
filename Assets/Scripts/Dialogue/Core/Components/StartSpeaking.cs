using Unity.Entities;

namespace Zoxel.Dialogue
{
    //! An event for the character to start speaking.
    /**
    *   An event for the character to start speaking.
    */
    public struct StartSpeaking : IComponentData
    { 
        public byte state;
        public Entity partner;
        public double timeStartedSpeaking;

        public StartSpeaking(Entity partner)
        {
            this.partner = partner;
            this.state = 0;
            this.timeStartedSpeaking = 0;
        }
    }
}