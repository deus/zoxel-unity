using Unity.Entities;

namespace Zoxel.Dialogue
{
    //! Attached to the buttons within the dialogueUI.
    /**
    *   Clicking these buttons will lead to the next branch in the dialogue tree.
    */
    public struct DialogueButton : IComponentData { }
}