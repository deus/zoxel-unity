using Unity.Entities;

namespace Zoxel.Dialogue
{
    public struct ApplyDialogueCondition : IComponentData
    {
        public DialogueCondition condition;
    }
}