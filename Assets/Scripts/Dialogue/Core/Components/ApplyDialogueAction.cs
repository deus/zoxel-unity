using Unity.Entities;

namespace Zoxel.Dialogue
{
    public struct ApplyDialogueAction : IComponentData
    {
        public DialogueAction action;
    }
}