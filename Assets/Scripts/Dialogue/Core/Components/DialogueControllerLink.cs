using Unity.Entities;

namespace Zoxel.Dialogue
{
    //! Links to the Dialogue Controller.
    /**
    *   Added to DialogueUI's and Character's.
    */
    public struct DialogueControllerLink : IComponentData
    {
        public Entity dialogueController;

        public DialogueControllerLink(Entity dialogueController)
        {
            this.dialogueController = dialogueController;
        }
    }
}