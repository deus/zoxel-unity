using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Dialogue
{
    //! A Dialogue Panel Component.
    /**
    *   A UI Panel that displays dialogue for one character.
    */
    public struct DialogueUI : IComponentData
    {
        public BlitableArray<int> links;
    }
}