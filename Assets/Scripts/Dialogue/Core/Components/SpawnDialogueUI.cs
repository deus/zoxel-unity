using Unity.Entities;

namespace Zoxel.Dialogue
{
    //! Spawns a dialogue ui with a target character.
    public struct SpawnDialogueUI : IComponentData
    {
        public Entity targetCharacter;

        public SpawnDialogueUI(Entity targetCharacter)
        {
            this.targetCharacter = targetCharacter;
        }
    }
}