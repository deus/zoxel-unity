using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Dialogue
{
    //! Passses IncrementDialogue Event along through DialogueControllerLink.
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class DialogueIncrementSendSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in SendIncrementDialogue incrementDialogue, in DialogueControllerLink dialogueControllerLink) =>
            {
                PostUpdateCommands.AddComponent(entityInQueryIndex, dialogueControllerLink.dialogueController, new IncrementDialogue(incrementDialogue.dialogueChoice));
                PostUpdateCommands.RemoveComponent<SendIncrementDialogue>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}