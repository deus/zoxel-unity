using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.UI;

namespace Zoxel.Dialogue
{
    //! Handles UIClickEvent for DialogueButton's.
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class DialogueUIClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<UIClickEvent, DialogueButton>()
                .ForEach((int entityInQueryIndex, in Child child, in ParentLink parentLink) =>
            {
                PostUpdateCommands.AddComponent<DestroyChildren>(entityInQueryIndex, parentLink.parent);
                PostUpdateCommands.AddComponent<NavigationDirty>(entityInQueryIndex, parentLink.parent);
                PostUpdateCommands.AddComponent(entityInQueryIndex, parentLink.parent, new SendIncrementDialogue((byte)(child.index)));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}