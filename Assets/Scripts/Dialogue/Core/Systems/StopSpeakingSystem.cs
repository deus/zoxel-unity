using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Dialogue
{
    //! The most basic event that removes Speaking and StopSpeaking components.
    /**
    *   The most basic event that removes Speaking and StopSpeaking components.
    */
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class StopSpeakingSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<StopSpeaking>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<Speaking>(entityInQueryIndex, e);
                PostUpdateCommands.RemoveComponent<StopSpeaking>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}