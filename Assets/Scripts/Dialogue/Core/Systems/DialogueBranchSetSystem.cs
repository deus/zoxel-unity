using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Collections;
using Zoxel.UI;

namespace Zoxel.Dialogue
{
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class DialogueBranchSetSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<RenderTextDirty>()
                .WithAll<ClearRenderText, DialogueUI>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<SetDialogueBranch>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

            Dependency = Entities
                .WithNone<RenderTextDirty>()
                .ForEach((Entity e, int entityInQueryIndex, ref DialogueUI dialogueUI, ref RenderText renderText, ref Size2D size2D, in SetDialogueBranch setDialogueBranch) =>
            {
                PostUpdateCommands.RemoveComponent<SetDialogueBranch>(entityInQueryIndex, e);
                dialogueUI.links = setDialogueBranch.branch.links;
                if (setDialogueBranch.isClear == 1)
                {
                    if (renderText.ClearText())
                    {
                        size2D.size = new float2();
                        PostUpdateCommands.AddComponent<RenderTextDirty>(entityInQueryIndex, e);
                        PostUpdateCommands.AddComponent<MeshUIDirty>(entityInQueryIndex, e);
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

//, ref RenderText renderText, ref Size2D size2D) =>
/*if (renderText.ClearText())
{
    size2D.size = new float2();
    PostUpdateCommands.AddComponent<RenderTextDirty>(entityInQueryIndex, e);
    PostUpdateCommands.AddComponent<MeshUIDirty>(entityInQueryIndex, e);
}
PostUpdateCommands.RemoveComponent<ClearRenderText>(entityInQueryIndex, e);*/