﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;

namespace Zoxel.Dialogue
{
    //! Spawns dialogue buttons!
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class DialogueButtonSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery dialogueTreeQuery;
        private Entity buttonPrefab;
        private NativeArray<Text> texts;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            dialogueTreeQuery = GetEntityQuery(
                ComponentType.ReadOnly<DialogueTree>(),
                ComponentType.Exclude<DestroyEntity>());
            texts = new NativeArray<Text>(2, Allocator.Persistent);
            texts[0] = new Text("Next");
            texts[1] = new Text("Leave");
            RequireForUpdate(processQuery);
            RequireForUpdate<SoundSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var soundSettings = GetSingleton<SoundSettings>();
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var buttonPrefab = this.buttonPrefab;
            var soundVolume = soundSettings.uiSoundVolume;
            var uiDatam = UIManager.instance.uiDatam;
            var dialogueAnswerStyle = uiDatam.dialogueAnswerStyle.GetStyle(uiScale);
            var iconSize = uiDatam.GetIconSize(uiScale);
            var elapsedTime = World.Time.ElapsedTime;
            // When dialogueUI is finished animating, spawn buttons
            var texts = this.texts;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var dialogueTreeEntities = dialogueTreeQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var dialogueTrees = GetComponentLookup<DialogueTree>(true);
            dialogueTreeEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, InitializeEntity, NavigationDirty>()
                .WithNone<OnChildrenSpawned, DestroyChildren>()
                .WithAll<SpawnChoiceButtons>()
                .ForEach((Entity e, int entityInQueryIndex, in RenderTextData renderTextData, in DialogueUI dialogueUI, in DialogueControllerLink dialogueControllerLink) =>
            {
                var links = dialogueUI.links;
                if (!HasComponent<DialogueTree>(dialogueControllerLink.dialogueController))
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<SpawnChoiceButtons>(entityInQueryIndex, e);
                var seed = entityInQueryIndex + (int) (elapsedTime * 100);
                var dialogueTree = dialogueTrees[dialogueControllerLink.dialogueController];
                var fontSize = renderTextData.fontSize;
                var cellPadding = fontSize * 0.2f;
                var position = new float3();
                var spawnedAmount = (byte) 0;
                if (links.Length <= 1)
                {
                    // this is with player ui
                    position = new float3(0, -renderTextData.fontSize - renderTextData.margins.y * 2 - cellPadding, 0);
                    // spawn next button
                    spawnedAmount = 1;
                    var isNext = false;
                    if (links.Length == 1)
                    {
                        var nextNodeID = links[0];
                        var nextType = dialogueTree.GetBranchType(nextNodeID);
                        if (nextType != 0)
                        {
                            isNext = true;
                        }
                    }
                    if (isNext)
                    {
                        var text = texts[0].Clone();
                        UICoreSystem.SpawnButton(PostUpdateCommands, entityInQueryIndex, buttonPrefab,
                            e, 0, position, text, fontSize, renderTextData.margins, 
                            dialogueAnswerStyle.color, dialogueAnswerStyle.frameGenerationData.color, dialogueAnswerStyle.textColor, dialogueAnswerStyle.textOutlineColor,
                            seed, soundVolume);
                    }
                    else
                    {
                        var text = texts[1].Clone();
                        UICoreSystem.SpawnButton(PostUpdateCommands, entityInQueryIndex, buttonPrefab,
                            e, 0, position, text, fontSize, renderTextData.margins, 
                            dialogueAnswerStyle.color, dialogueAnswerStyle.frameGenerationData.color, dialogueAnswerStyle.textColor, dialogueAnswerStyle.textOutlineColor,
                            seed, soundVolume);
                    }
                }
                else
                {
                    // for each link, get the dialogue speech of the next node (from the choices)
                    for (int i = 0; i < links.Length; i++)
                    {
                        //position = new float3(0, -renderTextData.fontSize - renderTextData.margins.y * 2 - cellPadding, 0);
                        position.y = - ((fontSize + renderTextData.margins.y * 2) + cellPadding) * i;   //  + renderTextData.margins.y * 2
                        var nextNodeID = links[i];
                        var nextType = dialogueTree.GetBranchType(nextNodeID);
                        var dialogueBranch = dialogueTree.GetBranch(nextNodeID);
                        var text = dialogueBranch.speech.Clone();
                        UICoreSystem.SpawnButton(PostUpdateCommands, entityInQueryIndex, buttonPrefab,
                            e, i, position, text, fontSize, renderTextData.margins, 
                            dialogueAnswerStyle.color, dialogueAnswerStyle.frameGenerationData.color, dialogueAnswerStyle.textColor, dialogueAnswerStyle.textOutlineColor,
                            seed + i * 100, soundVolume);
                    }
                    spawnedAmount = (byte) links.Length;
                }
                // UnityEngine.Debug.LogError("Spawned " + childrens.children.Length + " dialogue buttons.");
                PostUpdateCommands.AddComponent<NavigationDirty>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChildrenSpawned(spawnedAmount));
            })  .WithReadOnly(dialogueTrees)
                .WithReadOnly(texts)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        private void InitializePrefabs()
        {
            if (this.buttonPrefab.Index == 0)
            {
                this.buttonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
                EntityManager.AddComponent<Prefab>(this.buttonPrefab);
                EntityManager.AddComponent<DialogueButton>(this.buttonPrefab);
                EntityManager.AddComponent<SetUIElementPosition>(this.buttonPrefab);
            }
        }
    }
}