using Unity.Entities;
using Unity.Burst;
using Zoxel.UI;

namespace Zoxel.Dialogue
{
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class DialogueAnimatedTextFinishedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // When dialogueUI is finished animating, spawn buttons
            Dependency = Entities
                .WithAll<AnimatedTextFinished>()
                .ForEach((Entity e, int entityInQueryIndex, in DialogueControllerLink dialogueControllerLink) =>
            {
                if (HasComponent<DialogueController>(dialogueControllerLink.dialogueController))
                {
                    PostUpdateCommands.AddComponent<AnimatedTextFinished>(entityInQueryIndex, dialogueControllerLink.dialogueController);
                }
                PostUpdateCommands.RemoveComponent<AnimatedTextFinished>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<AnimatedTextFinished>()
                .ForEach((Entity e, int entityInQueryIndex,  in DialogueController dialogueController) =>
            {
                if (HasComponent<DialogueControllerLink>(dialogueController.dialogueUIA))
                {
                    PostUpdateCommands.AddComponent<SpawnChoiceButtons>(entityInQueryIndex, dialogueController.dialogueUIA);
                }
                PostUpdateCommands.RemoveComponent<AnimatedTextFinished>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}