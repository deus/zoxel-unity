using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Textures;

namespace Zoxel.Dialogue
{
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class DialogueUIIncrementSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<RenderTextDirty>()
                .ForEach((Entity e, int entityInQueryIndex, ref RenderText renderText, ref RenderTextData renderTextData, ref Size2D size2D, ref DialogueUI dialogueUI, ref AnimatedText animatedText, in IncrementDialogue incrementDialogue) =>
            {
                PostUpdateCommands.RemoveComponent<IncrementDialogue>(entityInQueryIndex, e);
                dialogueUI.links = incrementDialogue.branch.links;
                // Clear Render Text
                if (renderText.ClearText())
                {
                    PostUpdateCommands.AddComponent<RenderTextDirty>(entityInQueryIndex, e);
                }
                // Set Animated Text
                var speech = incrementDialogue.branch.speech;
                animatedText.SetText(speech.Clone());
                renderTextData.offsetX = ((-speech.Length - 1f) / 2f) * renderTextData.fontSize;
                var textLength = speech.Length;
                float2 newSize;
                if (HasComponent<TrailerUI>(e))
                {
                    newSize = new float2(textLength * renderTextData.fontSize + renderTextData.margins.x * 2f * 10f, renderTextData.fontSize + renderTextData.margins.y * 2f * 10f);
                }
                else
                {
                    newSize = new float2(textLength * renderTextData.fontSize + renderTextData.margins.x * 2f, renderTextData.fontSize + renderTextData.margins.y * 2f);
                }
                size2D.size = newSize;
                PostUpdateCommands.AddComponent<MeshUIDirty>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<GenerateTextureFrame>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}