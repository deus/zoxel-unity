﻿using Unity.Entities;
using Unity.Mathematics;

using Unity.Collections;
using Zoxel.UI;

namespace Zoxel.Dialogue
{
    //! When player has given a choice, increment dialogue in tree.
    /**
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class DialogueTreeIncrementSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<GenerateDialogue, EndDialogue>()
                .ForEach((Entity e, ref IncrementDialogue incrementDialogue, ref DialogueController dialogueController, in DialogueTree dialogueTree) =>
            {
                if (dialogueController.init == 0)
                {
                    dialogueController.init = 1;
                    if (dialogueTree.branches.Length != 0)
                    {
                        dialogueController.branchID = dialogueTree.branches[0].id;
                        dialogueController.branchType = 1;
                    }
                    else
                    {
                        dialogueController.branchID = 0;
                        dialogueController.branchType = 0;
                    }
                }
                else
                {
                    // for alot of nodes, choice will be 0, going to the next node
                    if (dialogueController.branchType == (byte)BranchType.Dialogue)
                    {
                        var previousBranch = dialogueTree.GetBranch(dialogueController.branchID);
                        // get next branch ID from our choice
                        // set new branch node and type
                        dialogueController.branchID = previousBranch.GetBranchChoice(incrementDialogue.dialogueChoice);
                    }
                    else if (dialogueController.branchType == (byte)BranchType.Action)
                    {
                        var previousAction = dialogueTree.GetAction(dialogueController.branchID);
                        // get next branch ID from our choice
                        // set new branch node and type
                        dialogueController.branchID = previousAction.nextNode; // previousBranch.GetBranchChoice(incrementDialogue.dialogueChoice);
                    }
                    else if (dialogueController.branchType == (byte)BranchType.Condition)
                    {
                        var previousCondition = dialogueTree.GetCondition(dialogueController.branchID);
                        // get next branch ID from our choice
                        // set new branch node and type
                        if (incrementDialogue.dialogueChoice == 0)
                        {
                            dialogueController.branchID = previousCondition.nextNodeA;
                        }
                        else
                        {
                            dialogueController.branchID = previousCondition.nextNodeB;
                        }
                    }
                    // set new branch node's type
                    dialogueController.branchType = dialogueTree.GetBranchType(dialogueController.branchID);
                }
                //if (dialogueController.branchType == 1)
                if (dialogueController.branchType == (byte)BranchType.Dialogue)
                {        
                    if (!HasComponent<UILink>(dialogueController.characterB))
                    {
                        PostUpdateCommands.AddComponent<EndDialogue>(e);
                        return;
                    }
                    var branch = dialogueTree.GetBranch(dialogueController.branchID);
                    var speech = branch.speech;
                    incrementDialogue.branch = branch;
                    // depending on speaker, play dialogue there
                    if (dialogueController.lastWasChoice == 0)
                    {
                        PostUpdateCommands.AddComponent(dialogueController.dialogueUIB, incrementDialogue);
                        PostUpdateCommands.AddComponent(dialogueController.dialogueUIA, new SetDialogueBranch { branch = branch, isClear = 1 });
                    }
                    else
                    {
                        PostUpdateCommands.AddComponent(dialogueController.dialogueUIA, incrementDialogue);
                        PostUpdateCommands.AddComponent(dialogueController.dialogueUIB, new SetDialogueBranch { branch = branch });
                    }
                    if (branch.links.Length > 1)
                    {
                        dialogueController.lastWasChoice = 1;
                    }
                    else
                    {
                        dialogueController.lastWasChoice = 0;
                    }
                }
                // apply action next
                else if (dialogueController.branchType == (byte)BranchType.Action)
                {
                    //UnityEngine.Debug.LogError("Giving Player Quest.");
                    // get action
                    var dialogueAction = dialogueTree.GetAction(dialogueController.branchID);
                    // apply action
                    PostUpdateCommands.AddComponent(e, new ApplyDialogueAction { action = dialogueAction });
                    // Clear dialogue
                    // what was last dialogue on? can we check? doesnt matter actualy
                    PostUpdateCommands.AddComponent<ClearRenderText>(dialogueController.dialogueUIA);
                    PostUpdateCommands.AddComponent<ClearRenderText>(dialogueController.dialogueUIB);
                }
                // apply condition next
                else if (dialogueController.branchType == (byte)BranchType.Condition)
                {
                    var condition = dialogueTree.GetCondition(dialogueController.branchID);
                    PostUpdateCommands.AddComponent(e, new ApplyDialogueCondition { condition = condition });
                }
                else if (dialogueController.branchType == (byte)BranchType.End)
                {
                    // add completedTree component
                    // flag for now
                    PostUpdateCommands.AddComponent<EndDialogue>(e);
                }
                PostUpdateCommands.RemoveComponent<IncrementDialogue>(e);
            }).WithoutBurst().Run();
        }
    }
}