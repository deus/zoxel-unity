﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Dialogue.Authoring;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Cameras;
using Zoxel.Textures;
using Zoxel.UI;

namespace Zoxel.Dialogue
{
    //! Spawns a Dialogue UI
    /**
    *   - UI Spawn System -
    *   \todo Make use UICoreSystem prefabs and add to them, instead of brand new ones.
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class DialogueUISpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity dialogueControllerPrefab;
        private Entity playerPanelPrefab;
        private Entity npcPanelPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var dialogueControllerArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(DialogueController),
                typeof(DialogueTree),
                typeof(DontDestroyDialogueTree),
                typeof(ZoxID));
            dialogueControllerPrefab = EntityManager.CreateEntity(dialogueControllerArchetype);
            var playerPanelArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(GenerateTextureFrame),
                typeof(TextureFrame),
                typeof(DialogueUI),
                typeof(DialogueControllerLink),
                typeof(NewCharacterUI),
                typeof(CharacterUI),
                typeof(CharacterLink),
                typeof(PanelUI),
                typeof(UIPosition),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(CameraLink),
                typeof(AnimatedText),
                typeof(RenderText),
                typeof(RenderTextData),
                typeof(OrbitTransform),
                typeof(OrbitPosition),
                typeof(Childrens),
                typeof(ZoxMesh),
                typeof(Texture),
                typeof(MaterialBaseColor),
                typeof(MaterialFrameColor),
                // typeof(FrameColorBuffer),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld));
            playerPanelPrefab = EntityManager.CreateEntity(playerPanelArchetype);
            var npcPanelArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(GenerateTextureFrame),
                typeof(TextureFrame),
                typeof(DialogueUI),
                typeof(DialogueControllerLink),
                typeof(NewCharacterUI),
                typeof(CharacterUI),
                typeof(CharacterLink),
                typeof(PanelUI),
                typeof(UIPosition),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(AnimatedText),
                typeof(RenderText),
                typeof(RenderTextData),
                typeof(CameraLink),
                typeof(FaceCamera),
                typeof(TrailerUI),
                typeof(Childrens),
                typeof(ZoxMesh),
                typeof(Texture),
                typeof(MaterialBaseColor),
                typeof(MaterialFrameColor),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld));
            npcPanelPrefab = EntityManager.CreateEntity(npcPanelArchetype);
            RequireForUpdate<DialogueSettings>();
        }

        protected override void OnUpdate()
        {
            var dialogueSettings = GetSingleton<DialogueSettings>();
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var gamePanelMaterial = UIManager.instance.materials.panelMaterial;
            var elapsedTime = (float) World.Time.ElapsedTime;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var npcDialogueScale = 160f / uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var uiDepth = CameraManager.instance.cameraSettings.uiDepth;
            var timePerLetter = dialogueSettings.timePerLetter;
            var dialogueSpeechStyle = uiDatam.dialogueSpeechStyle.GetStyle(uiScale);
            var dialogueAnswerStyle = uiDatam.dialogueAnswerStyle.GetStyle(uiScale);
            var playerPanelPrefab2 = playerPanelPrefab;
            var npcPanelPrefab2 = npcPanelPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            Entities
                .WithNone<DestroyEntity, InitializeEntity>()
                .ForEach((Entity e, in SpawnDialogueUI spawnDialogueUI, in CameraLink cameraLink, in UILink uiLink) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnDialogueUI>(e);
                var targetCharacter = spawnDialogueUI.targetCharacter;
                var dialogueControllerEntity = PostUpdateCommands.Instantiate(dialogueControllerPrefab);
                PostUpdateCommands.SetComponent(dialogueControllerEntity, new ZoxID(IDUtil.GenerateUniqueID()));
                var dialogueTree = EntityManager.GetComponentData<DialogueTree>(targetCharacter);
                PostUpdateCommands.SetComponent(dialogueControllerEntity, dialogueTree);
                // Create two panels one for each character
                var camera = cameraLink.camera;
                var playerPanelUI = PostUpdateCommands.Instantiate(playerPanelPrefab2);
                PostUpdateCommands.SetComponent(playerPanelUI, new MaterialBaseColor(dialogueAnswerStyle.color));
                PostUpdateCommands.SetComponent(playerPanelUI, new CameraLink(camera));
                PostUpdateCommands.SetComponent(playerPanelUI, new NonUniformScale { Value = new float3(1, 1, 1) });
                PostUpdateCommands.SetComponent(PostUpdateCommands.Instantiate(uiSpawnedEventPrefab), new OnUISpawned(e, 1));
                SetPanelThings(PostUpdateCommands, e, playerPanelUI, dialogueControllerEntity, dialogueAnswerStyle,
                    elapsedTime, false, uiDepth, timePerLetter, 1f);
                var uiLink2 = EntityManager.GetComponentData<UILink>(targetCharacter);
                var npcPanelUI = PostUpdateCommands.Instantiate(npcPanelPrefab2);
                PostUpdateCommands.SetComponent(npcPanelUI, new MaterialBaseColor(dialogueSpeechStyle.color));
                PostUpdateCommands.SetComponent(npcPanelUI, new NonUniformScale { Value = new float3(1, 1, 1) });
                PostUpdateCommands.SetComponent(targetCharacter, uiLink2);
                SetPanelThings(PostUpdateCommands, targetCharacter, npcPanelUI, dialogueControllerEntity, dialogueSpeechStyle,
                    elapsedTime, true, uiDepth, timePerLetter, npcDialogueScale);
                PostUpdateCommands.SetComponent(PostUpdateCommands.Instantiate(uiSpawnedEventPrefab), new OnUISpawned(targetCharacter, 1));
                // finish
                PostUpdateCommands.SetComponent(dialogueControllerEntity, new DialogueController(e, playerPanelUI, targetCharacter, npcPanelUI));
                PostUpdateCommands.AddComponent<IncrementDialogue>(dialogueControllerEntity);
            }).WithoutBurst().Run();
        }

        private void SetPanelThings(EntityCommandBuffer PostUpdateCommands, Entity character, Entity panelUIEntity, Entity dialogueController, in UIElementStyle dialogueSpeechStyle,
            float elapsedTime, bool isNPC, float uiDepth, float2 timePerLetter, float sizeMultiplier)
        {
            UICoreSystem.SetTextureFrame(PostUpdateCommands, panelUIEntity, in dialogueSpeechStyle.frameGenerationData);
            var seed = EntityManager.GetComponentData<ZoxID>(character).id;
            PostUpdateCommands.SetComponent(panelUIEntity, new CharacterLink(character));
            PostUpdateCommands.SetComponent(panelUIEntity, new PanelUI(PanelType.DialogueUI));
            PostUpdateCommands.SetComponent(panelUIEntity, new UIPosition(uiDepth));
            PostUpdateCommands.SetComponent(panelUIEntity, new DialogueControllerLink(dialogueController));
            UICoreSystem.SetRenderText(PostUpdateCommands,  panelUIEntity, dialogueSpeechStyle.textColor, dialogueSpeechStyle.textOutlineColor, dialogueSpeechStyle.textGenerationData,
                new Text(), dialogueSpeechStyle.fontSize * sizeMultiplier, dialogueSpeechStyle.textPadding * sizeMultiplier);
            PostUpdateCommands.SetComponent(panelUIEntity, new AnimatedText(timePerLetter, elapsedTime, seed));
            if (isNPC)
            {
                var body = EntityManager.GetComponentData<Body>(character);
                PostUpdateCommands.SetComponent(panelUIEntity, new TrailerUI(body.size.y));
            }
        }
    }
}

// Todo:
//      Remove dialogue UI from characterUI system
//      Spawn the panel here
//      Spawn a panel above NPC you are talking to, and in front of the player
//      When npc talks - text will appear above them
//      when player talks or has options - it will appear in front of them
// remove centrelinking from animated text as it looks weird (makes eyes move too much)
// fade in individual text
// randomize font location a little
// lerp in font positions from the character talking
// make sounds when they spawn too
// change words to be different colours
// Randomly generate textures for letters - use AI to generate them
// load in meta for dialogue tree
// once finished a line - can chose some lines depending on next information
// spawn a Dialogue Entity that has a list of dialogueUIs/Characters
// Every time a new speech happens, or button pressed, goes through here (events on here)