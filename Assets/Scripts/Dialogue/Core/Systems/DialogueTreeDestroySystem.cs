using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Dialogue
{
    //! Cleans up DialogueTree data in memory.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class DialogueTreeDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithNone<DontDestroyDialogueTree>()
                .WithAll<DestroyEntity, DialogueTree>()
                .ForEach((in DialogueTree dialogueTree) =>
			{
                dialogueTree.DisposeFinal();
			}).ScheduleParallel();
        }
    }
}