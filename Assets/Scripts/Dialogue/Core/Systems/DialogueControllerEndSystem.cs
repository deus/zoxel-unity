using Unity.Entities;
using Unity.Burst;
using Zoxel.UI;             // gets dialogue ui

namespace Zoxel.Dialogue
{
    //! A DialogueController ends a conversation.
    /**
    *   Sends commands to characters that dialogue has ended.
    *   Destroys the UIs.
    *   Destroys the controller itself.
    *   \todo Bug, gets stuck if character dies during animating text, if target gains a new target
    */
    [BurstCompile, UpdateInGroup(typeof(DialogueSystemGroup))]
    public partial class DialogueControllerEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<EndDialogue>()
                .ForEach((Entity e, int entityInQueryIndex, in DialogueController dialogueController) =>
            {
                PostUpdateCommands.AddComponent<StopSpeaking>(entityInQueryIndex, dialogueController.characterA);
                PostUpdateCommands.AddComponent<StopSpeaking>(entityInQueryIndex, dialogueController.characterB);
                UICoreSystem.RemoveUI(PostUpdateCommands, entityInQueryIndex, removeCharacterUIPrefab, dialogueController.characterA, dialogueController.dialogueUIA);
                UICoreSystem.RemoveUI(PostUpdateCommands, entityInQueryIndex, removeCharacterUIPrefab, dialogueController.characterB, dialogueController.dialogueUIB);
                PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}