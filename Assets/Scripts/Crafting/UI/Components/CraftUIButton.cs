using Unity.Entities;

namespace Zoxel.Crafting.UI
{
    //! A tag for the Craft's UI.
    public struct CraftUIButton : IComponentData { }
}