using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Cameras;
using Zoxel.Audio;
using Zoxel.Textures;
using Zoxel.Items;
using Zoxel.Items.UI;

namespace Zoxel.Crafting.UI
{
    //! Spawns a ItemUI connected to a user.
    /**
    *   - UI Spawn System -
    *   \todo Generate Recipe Data.
    *   \todo Check recipes when placing items.
    */
    [BurstCompile, UpdateInGroup(typeof(CraftingSystemGroup))]
    public partial class CraftUISpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        public static Entity craftUIPrefab;
        public static Entity spawnCraftUIPrefab;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.ReadOnly<SpawnCraftUI>(),
                ComponentType.ReadOnly<CharacterLink>(),
                ComponentType.Exclude<DelayEvent>());
            var spawnCraftUIArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnCraftUI),
                typeof(CharacterLink),
                typeof(DelayEvent));
            spawnCraftUIPrefab = EntityManager.CreateEntity(spawnCraftUIArchetype);
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var uiDatam = UIManager.instance.uiDatam;
            var iconStyle = uiDatam.iconStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var elapsedTime = World.Time.ElapsedTime;
            var craftUIPrefab = CraftUISpawnSystem.craftUIPrefab;
            var iconPrefabs = ItemUISpawnSystem.iconPrefabs;
            var userItemPrefab = ItemSystemGroup.userItemPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var spawnInventoryUIEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var characterLinks = GetComponentLookup<CharacterLink>(true);
            var uiAnchors = GetComponentLookup<UIAnchor>(true);
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in CameraLink cameraLink) =>
            {
                // Does spawn a invenntoryy ui?
                var spawnInventoryEntity = new Entity();
                for (int i = 0; i < spawnInventoryUIEntities.Length; i++)
                {
                    var e2 = spawnInventoryUIEntities[i];
                    var characterLink = characterLinks[e2];
                    if (characterLink.character == e)
                    {
                        spawnInventoryEntity = e2;
                        break;  // only one UI per character atm
                    }
                }
                if (spawnInventoryEntity.Index == 0)
                {
                    return;
                }
                // destroy event entity
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, spawnInventoryEntity);
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, craftUIPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CharacterLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLink);
                var itemUICount = (byte) 10;
                // var maxUIs = 0;
                if (uiAnchors.HasComponent(spawnInventoryEntity))
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, uiAnchors[spawnInventoryEntity]);
                }
                for (byte i = 0; i < 10; i++)
                {
                    //! Spawns a new UserItem Entity.
                    var userItemEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, userItemPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, new InventoryLink(e3));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, new UserDataIndex(i));
                    // now spawn UI for it
                    var frameEntity = ItemUISpawnSystem.SpawnItemUI(PostUpdateCommands, entityInQueryIndex, elapsedTime, i, iconPrefabs, e3, userItemEntity, in iconStyle);
                    var gridPosition = new int2((int)(i) % 3, (int)(i) / 3);
                    if (i == 9)
                    {
                        gridPosition = new int2(4, 1);
                        // Add special component to output button, cannot place there
                        PostUpdateCommands.AddComponent<DisableItemUIPlacement>(entityInQueryIndex, frameEntity);
                    }
                    PostUpdateCommands.AddComponent(entityInQueryIndex, frameEntity, new SetGridPosition(gridPosition));
                    PostUpdateCommands.AddComponent<CraftUIButton>(entityInQueryIndex, frameEntity);
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawned(itemUICount));
                PostUpdateCommands.AddComponent(entityInQueryIndex, e3, new OnUserItemsSpawned(itemUICount));
                // spawnedUI event
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));
            })  .WithReadOnly(spawnInventoryUIEntities)
                .WithDisposeOnCompletion(spawnInventoryUIEntities)
                .WithReadOnly(characterLinks)
                .WithReadOnly(uiAnchors)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public void InitializePrefabs()
        {
            if (craftUIPrefab.Index != 0) // && ItemUISpawnSystem.inventoryUIPrefab.Index != 0)
            {
                return;
            }
            ItemUISpawnSystem.InitializePrefabs(EntityManager);
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var panelStyle = uiDatam.panelStyle.GetStyle();
            var iconSize = uiDatam.GetIconSize(uiScale);
            var padding = uiDatam.GetIconPadding(uiScale); // iconSize / 8f;
            var margins = uiDatam.GetIconMargins(uiScale); // iconSize / 6f;
            craftUIPrefab = EntityManager.Instantiate(ItemUISpawnSystem.inventoryUIPrefab);
            EntityManager.AddComponent<Prefab>(craftUIPrefab);
            EntityManager.RemoveComponent<InventoryUI>(craftUIPrefab);
            EntityManager.AddComponent<CraftUI>(craftUIPrefab);
            EntityManager.AddComponent<Inventory>(craftUIPrefab);
            var gridUI = new GridUI(new int2(5, 3), padding, margins);
            EntityManager.SetComponentData(craftUIPrefab, gridUI);
            EntityManager.SetComponentData(craftUIPrefab, new Size2D(gridUI.GetSize(iconSize)));
            EntityManager.SetComponentData(craftUIPrefab, new PanelUI(PanelType.CraftUI));
            EntityManager.SetComponentData(craftUIPrefab, new UIAnchor(AnchorUIType.LeftMiddle));
            // configure non griddy grid
            EntityManager.RemoveComponent<GridUIDirty>(craftUIPrefab);
            EntityManager.AddComponent<MeshUIDirty>(craftUIPrefab);
        }
    }
}