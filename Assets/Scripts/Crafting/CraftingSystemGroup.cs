using Unity.Entities;
using Zoxel.Items;
// using Zoxel.Items.UI;

namespace Zoxel.Crafting
{
    //! Hosts achievement systems.
    [UpdateInGroup(typeof(ItemSystemGroup))]    // UI
    public partial class CraftingSystemGroup : ComponentSystemGroup { }
}