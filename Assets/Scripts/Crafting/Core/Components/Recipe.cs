using Unity.Entities;

namespace Zoxel.Crafting
{
    //! A tag for a recipe entity.
    public struct Recipe : IComponentData { }
}