using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Actions.UI;
using Zoxel.Cameras;

namespace Zoxel.Stats.UI
{
    //! Just used to spawn experience bars on characters.
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class PlayerExperiencebarSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery actionbarQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            actionbarQuery = GetEntityQuery(
                ComponentType.ReadOnly<Actionbar>(),
                ComponentType.ReadOnly<PanelUI>(),
                ComponentType.ReadOnly<UIElement>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            const float experienceBarTextMargins = 0.008f;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDepth = CameraManager.instance.cameraSettings.uiDepth;
            var uiDatam = UIManager.instance.uiDatam;
            var iconSize = uiDatam.GetIconSize(uiScale);
            var statbarSize = new float2(0, iconSize.y / uiDatam.playerStatbarDivision);
            var statbarPadding = uiDatam.GetPlayerStatbarPadding(uiScale);
            var margins = uiDatam.GetIconMargins(uiScale);
            var paddings = uiDatam.GetIconPadding(uiScale);
            var experienceBarSize = statbarSize;
            var healthbarPosition = new float3(0, 0, uiDepth);    // actionbarOffsetY - offset2
            var textPadding = uiDatam.statbarTextPadding;
            var statbarFadeTime = UIManager.instance.uiDatam.statbarFadeTime;
            var experienceColor = uiDatam.experienceColor;
            var experiencePanelColor = uiDatam.experiencePanelColor;
            var experienceTextColor = new Color(uiDatam.experienceTextColor);
            var experienceTextOutlineColor = new Color(uiDatam.experienceTextOutlineColor);
            var experienceGenerationData = uiDatam.experienceGenerationData;
            // prefabs
            var experiencebarPrefab = StatsUISystemGroup.experiencebarPanelPrefab;
            var experiencebarFrontPrefab = StatsUISystemGroup.experiencebarFrontPrefab;
            var experiencebarTextbarPrefab = StatsUISystemGroup.experiencebarTextbarPrefab;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            // PostUpdateCommands2.RemoveComponentForEntityQuery<SpawnExperienceBar>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var actionbarEntities = actionbarQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var actionbarPanels = GetComponentLookup<PanelPosition>(true);
            var uiAnchors = GetComponentLookup<UIAnchor>(true);
            var actionbarSize2Ds = GetComponentLookup<Size2D>(true);
            actionbarEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity, SpawnStatbar>()
                .WithAll<SpawnExperienceBar>()
                .ForEach((Entity e, int entityInQueryIndex, in UILink uiLink, in UserStatLinks userStatLinks, in CameraLink cameraLink) =>
            {
                if (userStatLinks.stats.Length == 0)
                {
                    return;
                }
                var actionbarUI = new Entity();
                for (int i = 0; i < uiLink.uis.Length; i++)
                {
                    if (HasComponent<Actionbar>(uiLink.uis[i]))
                    {
                        actionbarUI = uiLink.uis[i];
                        break;
                    }
                }
                if (actionbarUI.Index == 0
                    || HasComponent<GridUIDirty>(actionbarUI)
                    || HasComponent<SpawnActionsUI>(actionbarUI)
                    || HasComponent<OnChildrenSpawned>(actionbarUI))
                {
                    return;
                }
                if (!actionbarPanels.HasComponent(actionbarUI) || !actionbarSize2Ds.HasComponent(actionbarUI))
                {
                    // UnityEngine.Debug.LogError("Actionbar lacking Panel or Size2D, has size: " + actionbarSize2Ds.HasComponent(actionbarUI));
                    return;
                }
                PostUpdateCommands.RemoveComponent<SpawnExperienceBar>(entityInQueryIndex, e);
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));

                var soulUserStatEntity = new Entity();
                for (int i = 0; i < userStatLinks.stats.Length; i++)
                {
                    var userStatEntity = userStatLinks.stats[i];
                    if (HasComponent<SoulStat>(userStatEntity))
                    {
                        soulUserStatEntity = userStatEntity;
                    }
                }
                var actionbarPanel = actionbarPanels[actionbarUI];
                var actionbarSize = actionbarSize2Ds[actionbarUI].size;
                experienceBarSize.x = actionbarSize.x;
                statbarSize.x = experienceBarSize.x / 3f;
                //! \todo Gete actionbar UIAnchor
                var anchor = uiAnchors[actionbarUI].anchor;
                var actionsPanelPosition = actionbarPanel.GetPositionOffset(actionbarSize, anchor);
                var experiencebarPosition = new float3(0,
                    actionsPanelPosition.y + actionbarSize.y / 2f + statbarPadding,
                    uiDepth);
                var experiencebar = PlayerStatbarSpawnSystem.CreateStatbar(PostUpdateCommands, entityInQueryIndex, e, false, soulUserStatEntity,
                    experiencebarPosition, experiencePanelColor, experienceColor, experienceTextColor, experienceTextOutlineColor, experienceGenerationData,
                    experienceBarSize, true, experiencebarPrefab, experiencebarFrontPrefab,
                    experiencebarTextbarPrefab, uiDepth, experienceBarTextMargins, cameraLink.camera,
                    textPadding, statbarFadeTime);
            })  .WithReadOnly(actionbarPanels)
                .WithReadOnly(actionbarSize2Ds)
                .WithReadOnly(uiAnchors)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}