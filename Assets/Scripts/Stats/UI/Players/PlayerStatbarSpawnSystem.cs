using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Actions.UI;
using Zoxel.Cameras;

namespace Zoxel.Stats.UI
{
    //! Spawns player statbars.
    /**
    *   - UI Spawn System -
    *   \todo Remove reliance on actionbar for positioning. Reposition statbars to top left of UI.
    */
    [BurstCompile, UpdateInGroup(typeof(StatsUISystemGroup))]
    public partial class PlayerStatbarSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery actionbarQuery;
        private EntityQuery userStatsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.ReadOnly<SpawnStatbar>(),
                ComponentType.ReadOnly<PlayerCharacter>());
            actionbarQuery = GetEntityQuery(
                ComponentType.ReadOnly<Actionbar>(),
                ComponentType.ReadOnly<PanelUI>(),
                ComponentType.ReadOnly<UIElement>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.ReadOnly<StateStat>(),
                ComponentType.Exclude<DestroyEntity>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            if (UIManager.instance.uiSettings.disableStatbars || processQuery.IsEmpty)
            {
                return;
            }
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            const float experienceBarTextMargins = 0.008f;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDepth = CameraManager.instance.cameraSettings.uiDepth;
            var uiDatam = UIManager.instance.uiDatam;
            var textOutlineColor = new Color(uiDatam.overlayTextOutlineColor);
            var iconSize = uiDatam.GetIconSize(uiScale);
            var statbarSize = new float2(0, iconSize.y / uiDatam.playerStatbarDivision);
            var experienceColor = uiDatam.experienceColor;
            var experiencePanelColor = uiDatam.experiencePanelColor;
            var experienceTextColor = new Color(uiDatam.experienceTextColor);
            var experienceTextOutlineColor = new Color(uiDatam.experienceTextOutlineColor);
            var healthColor = uiDatam.healthColor;
            var energyColor = uiDatam.energyColor;
            var manaColor = uiDatam.manaColor;
            var healthPanelColor = uiDatam.healthPanelColor;
            var energyPanelColor = uiDatam.energyPanelColor;
            var manaPanelColor = uiDatam.manaPanelColor;
            var healthTextColor = new Color(uiDatam.healthTextColor);
            var energyTextColor = new Color(uiDatam.energyTextColor);
            var manaTextColor = new Color(uiDatam.manaTextColor);
            var statbarPadding = uiDatam.GetPlayerStatbarPadding(uiScale);
            // var margins = uiDatam.GetIconMargins(uiScale);
            var experienceBarSize = statbarSize; //new float2(0, 0.0032f);
            var healthbarPosition = new float3(0, 0, uiDepth);
            var manaPosition = new float3(0, 0, uiDepth);
            var energyPosition = new float3(0, 0, uiDepth);
            var textPadding = uiDatam.statbarTextPadding;
            var statbarFadeTime = UIManager.instance.uiDatam.statbarFadeTime;
            var statbarMargins = 0.024f;
            var textGenerationData = uiDatam.statbarGenerationData;
            var experienceGenerationData = uiDatam.experienceGenerationData;
            // prefabs
            var frontbarPrefab = StatsUISystemGroup.frontbarPrefab;
            var statbarLabelPrefab = StatsUISystemGroup.statbarLabelPrefab;
            var panelPrefab = StatsUISystemGroup.panelPrefab;
            var experiencebarPrefab = StatsUISystemGroup.experiencebarPanelPrefab;
            var experiencebarFrontPrefab = StatsUISystemGroup.experiencebarFrontPrefab;
            var experiencebarTextbarPrefab = StatsUISystemGroup.experiencebarTextbarPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var actionbarEntities = actionbarQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var actionbarPanels = GetComponentLookup<PanelPosition>(true);
            var uiAnchors = GetComponentLookup<UIAnchor>(true);
            var actionbarSize2Ds = GetComponentLookup<Size2D>(true);
            var userStatEntities = actionbarQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var statValues = GetComponentLookup<StatValue>(true);
            var statValueMaxs = GetComponentLookup<StatValueMax>(true);
            actionbarEntities.Dispose();
            userStatEntities.Dispose();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithAll<SpawnStatbar>()
                .ForEach((Entity e, int entityInQueryIndex, in UserStatLinks userStatLinks, in UILink uiLink, in CameraLink cameraLink) =>
            {
                if (userStatLinks.stats.Length == 0)
                {
                    return;
                }
                var actionbarUI = new Entity();
                for (int i = 0; i < uiLink.uis.Length; i++)
                {
                    if (HasComponent<Actionbar>(uiLink.uis[i]))
                    {
                        actionbarUI = uiLink.uis[i];
                        break;
                    }
                }
                if (actionbarUI.Index == 0
                    || HasComponent<GridUIDirty>(actionbarUI)
                    || HasComponent<SpawnActionsUI>(actionbarUI)
                    || HasComponent<OnChildrenSpawned>(actionbarUI))
                {
                    // UnityEngine.Debug.LogError("Actionbar not found.");
                    return;
                }
                if (!actionbarPanels.HasComponent(actionbarUI) || !actionbarSize2Ds.HasComponent(actionbarUI))
                {
                    // UnityEngine.Debug.LogError("Actionbar lacking Panel or Size2D, has size: " + actionbarSize2Ds.HasComponent(actionbarUI));
                    return;
                }
                // UnityEngine.Debug.LogError("Spawning Player Statbars.");
                PostUpdateCommands.RemoveComponent<SpawnStatbar>(entityInQueryIndex, e);
                var actionbarPanel = actionbarPanels[actionbarUI];
                var anchor = uiAnchors[actionbarUI].anchor;
                var actionbarSize = actionbarSize2Ds[actionbarUI].size;
                experienceBarSize.x = actionbarSize.x;
                statbarSize.x = experienceBarSize.x / 3f;
                var actionsPanelPosition = actionbarPanel.GetPositionOffset(actionbarSize, anchor);
                var experiencebarPosition = new float3(0, actionsPanelPosition.y + actionbarSize.y / 2f + statbarPadding,  uiDepth);
                healthbarPosition.y = experiencebarPosition.y + experienceBarSize.y / 2f + statbarSize.y / 2f;
                energyPosition.y = healthbarPosition.y;
                manaPosition.y = healthbarPosition.y;
                manaPosition.x = statbarSize.x;
                energyPosition.x = -statbarSize.x;
                // spawn 4 bars for states
                var stateStatsCount = 0;
                var healthUserStatEntity = new Entity();
                var energyUserStatEntity = new Entity();
                var manaUserStatEntity = new Entity();
                var soulUserStatEntity = new Entity();
                for (int i = 0; i < userStatLinks.stats.Length; i++)
                {
                    var userStatEntity = userStatLinks.stats[i];
                    if (HasComponent<StateStat>(userStatEntity))
                    {
                        if (stateStatsCount == 0)
                        {
                            healthUserStatEntity = userStatEntity;
                        }
                        else if (stateStatsCount == 1)
                        {
                            energyUserStatEntity = userStatEntity;
                        }
                        else if (stateStatsCount == 2)
                        {
                            manaUserStatEntity = userStatEntity;
                        }
                        stateStatsCount++;
                    }
                    else if (HasComponent<SoulStat>(userStatEntity))
                    {
                        soulUserStatEntity = userStatEntity;
                    }
                }
                var healthStat = statValues[healthUserStatEntity];
                var healthMax = healthStat.value == statValueMaxs[healthUserStatEntity].value;
                var energyStat = statValues[energyUserStatEntity];
                var energyMax = energyStat.value == statValueMaxs[energyUserStatEntity].value;
                var manaStat = statValues[manaUserStatEntity];
                var manaMax = manaStat.value == statValueMaxs[manaUserStatEntity].value;

                var healthbar = CreateStatbar(PostUpdateCommands, entityInQueryIndex, e, true, healthUserStatEntity,
                    healthbarPosition, healthPanelColor, healthColor, healthTextColor, textOutlineColor, textGenerationData,
                    statbarSize, !healthMax, panelPrefab, frontbarPrefab, statbarLabelPrefab, uiDepth, statbarMargins, cameraLink.camera,
                    textPadding, statbarFadeTime);
                var energybar = CreateStatbar(PostUpdateCommands, entityInQueryIndex, e, true, energyUserStatEntity,
                    energyPosition, energyPanelColor, energyColor, energyTextColor, textOutlineColor, textGenerationData,
                    statbarSize, !energyMax, panelPrefab, frontbarPrefab, statbarLabelPrefab, uiDepth, statbarMargins, cameraLink.camera,
                    textPadding, statbarFadeTime);
                var manabar = CreateStatbar(PostUpdateCommands, entityInQueryIndex, e, true, manaUserStatEntity,
                    manaPosition, manaPanelColor, manaColor, manaTextColor, textOutlineColor, textGenerationData,
                    statbarSize, !manaMax, panelPrefab, frontbarPrefab, statbarLabelPrefab, uiDepth, statbarMargins, cameraLink.camera,
                    textPadding, statbarFadeTime);
                var experiencebar = CreateStatbar(PostUpdateCommands, entityInQueryIndex, e, false, soulUserStatEntity,
                    experiencebarPosition, experiencePanelColor, experienceColor, experienceTextColor, experienceTextOutlineColor, experienceGenerationData,
                    experienceBarSize, true, experiencebarPrefab, experiencebarFrontPrefab, experiencebarTextbarPrefab,
                    uiDepth, experienceBarTextMargins, cameraLink.camera,
                    textPadding, statbarFadeTime);
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 4));
            })  .WithReadOnly(actionbarPanels)
                .WithReadOnly(uiAnchors)
                .WithReadOnly(actionbarSize2Ds)
                .WithReadOnly(statValues)
                .WithReadOnly(statValueMaxs)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static Entity CreateStatbar(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity character,
            bool hasFader, Entity userStat,
            float3 position, UnityEngine.Color panelColor, UnityEngine.Color frontColor,
            Color textColor, Color textOutlineColor, TextGenerationData textGenerationData,
            float2 panelSize, bool visible, Entity panelPrefab, Entity frontbarPrefab, Entity textbarPrefab,
            float uiDepth, float edgeSpace, Entity camera, float2 textPadding, float statbarFadeTime)
        {
            var backbar = PostUpdateCommands.Instantiate(entityInQueryIndex, panelPrefab);
            CreateStatbar(PostUpdateCommands, entityInQueryIndex, character, userStat, backbar,
                hasFader, panelSize, panelColor, frontColor, textColor, textOutlineColor, textGenerationData,
                frontbarPrefab, textbarPrefab, edgeSpace, textPadding, statbarFadeTime);
            if (visible)
            {
                PostUpdateCommands.AddComponent<FadeIn>(entityInQueryIndex, backbar);
            }
            PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, new PanelPosition(new float3(position.x, position.y, 0)));
            PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, new UIAnchor(AnchorUIType.BottomMiddle));
            PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, new UIPosition(uiDepth));
            PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, new CameraLink(camera));
            return backbar;
        }

        public static void CreateStatbar(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity character,
            Entity userStat, Entity backbar, bool hasFader, float2 panelSize, UnityEngine.Color panelColor,
            UnityEngine.Color frontbarColor, Color textColor, Color textOutlineColor, TextGenerationData textGenerationData,
            Entity frontbarPrefab, Entity statbarLabelPrefab,
            float edgeSpace, float2 textPadding, float statbarFadeTime)
        {
            //UnityEngine.Debug.LogError("Setting front Colour: " + backbar.Index + " : " + frontbarColor);
            var statLink = new StatLink(userStat); // (byte) statType, statIndex); // all
            var uiElement = new Size2D(panelSize);
            // Setters
            PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, new CharacterLink(character));
            PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, statLink);
            PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, uiElement);
            if (hasFader)
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, new Fader(0, panelColor.a, statbarFadeTime));
                PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, new MaterialBaseColor { Value = new float4(panelColor.r, panelColor.g, panelColor.b, 0) });
            }
            else
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, new MaterialBaseColor(panelColor));
            }
            
            var parentLink = new ParentLink(backbar);   // for 2 children

            var frontbar = PostUpdateCommands.Instantiate(entityInQueryIndex, frontbarPrefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, new Zoxel.Transforms.Child(0));
            PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, parentLink);
            var percentageUI = new PercentageUI
            {
                percentage = 1, 
                size = panelSize,
                edgeSpace = edgeSpace // 0.008f
            };
            percentageUI.SetTargetPercentage(0);
            PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, percentageUI);
            PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, statLink);
            PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, uiElement);
            if (hasFader)
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, new Fader(0, frontbarColor.a, statbarFadeTime));
                PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, new MaterialBaseColor { Value = new float4(frontbarColor.r, frontbarColor.g, frontbarColor.b, 0) });
            }
            else
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, new MaterialBaseColor(frontbarColor));
            }

            var textbar = PostUpdateCommands.Instantiate(entityInQueryIndex, statbarLabelPrefab);
            var renderText = new RenderTextData
            {
                textGenerationData = textGenerationData,
                color = textColor,
                outlineColor = textOutlineColor,
                fontSize = panelSize.y * 0.76f,
                margins = textPadding
            };
            PostUpdateCommands.SetComponent(entityInQueryIndex, textbar, renderText);
            PostUpdateCommands.SetComponent(entityInQueryIndex, textbar, parentLink);
            PostUpdateCommands.SetComponent(entityInQueryIndex, textbar, statLink);
            var textColor2 = textColor.GetColor();
            if (hasFader)
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, textbar, new Fader(0, textColor2.a, statbarFadeTime));
                PostUpdateCommands.SetComponent(entityInQueryIndex, textbar, new MaterialBaseColor { Value = new float4(textColor2.r, textColor2.g, textColor2.b, 0) });
            }
            else
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, textbar, new MaterialBaseColor(textColor2));
            }
            PostUpdateCommands.SetComponent(entityInQueryIndex, textbar, new Zoxel.Transforms.Child(1));

            PostUpdateCommands.AddComponent(entityInQueryIndex, backbar, new OnChildrenSpawned(2));
        }
    }
}