using Unity.Entities;

namespace Zoxel.Stats.UI
{
    //! A component for a stat button entity.
    public struct StatButton : IComponentData { }
}