﻿using Unity.Entities;

namespace Zoxel.Stats.UI
{
    //! Links to an entity user stat.
    public struct StatLink : IComponentData
    {
        public Entity userStat;

        public StatLink(Entity userStat)
        {
            this.userStat = userStat;
        }
    }
}