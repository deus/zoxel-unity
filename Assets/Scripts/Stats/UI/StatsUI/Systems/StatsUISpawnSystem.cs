﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Textures;
using Zoxel.Audio;

namespace Zoxel.Stats.UI
{
    //! Spawns stat icons for a character.
    /**
    *   - UI Spawn System -
    *   \todo Move CharacterUISpawnSystem functions out of there and place here. SpawnStatsUI event.
    *   \todo GenerateStatTexture ~ base it off AI Generation - has to be symbolic. There was that algorithm that took input and made something similar in pattern.
    *   \todo Different frame UI per type of stat.
    */
    [BurstCompile, UpdateInGroup(typeof(StatsUISystemGroup))]
    public partial class StatsUISpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity statsUIPrefab;
        public static IconPrefabs iconPrefabs;
        public static Entity spawnStatsUIPrefab;
        private EntityQuery processQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnStatsUI),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnStatsUIPrefab = EntityManager.CreateEntity(spawnArchetype);
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DelayEvent>(),
                ComponentType.ReadOnly<SpawnStatsUI>(),
                ComponentType.ReadOnly<CharacterLink>());
            RequireForUpdate(processQuery);
        }

        private void InitializePrefabs()
        {
            if (StatsUISpawnSystem.statsUIPrefab.Index != 0)
            {
                return;
            }
            var statsUIPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            StatsUISpawnSystem.statsUIPrefab = statsUIPrefab;
            EntityManager.AddComponent<Prefab>(statsUIPrefab);
            EntityManager.AddComponent<StatsUI>(statsUIPrefab);
            EntityManager.SetComponentData(statsUIPrefab, new PanelUI(PanelType.StatsUI));
            iconPrefabs.frame = EntityManager.Instantiate(UICoreSystem.iconPrefabs.frame);
            EntityManager.AddComponent<Prefab>(iconPrefabs.frame);
            EntityManager.AddComponent<StatButton>(iconPrefabs.frame);
            iconPrefabs.icon = EntityManager.Instantiate(UICoreSystem.iconPrefabs.icon);
            EntityManager.AddComponent<Prefab>(iconPrefabs.icon);
            EntityManager.AddComponent<StatIcon>(iconPrefabs.icon);
            iconPrefabs.label = UICoreSystem.iconPrefabs.label;
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            InitializePrefabs();
            var uiDatam = UIManager.instance.uiDatam;
            var iconStyle = uiDatam.iconStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var statsUIPrefab = StatsUISpawnSystem.statsUIPrefab;
            var iconPrefabs = StatsUISpawnSystem.iconPrefabs;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var spawnEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var characterLinks = GetComponentLookup<CharacterLink>(true);
            var prefabLinks = GetComponentLookup<PrefabLink>(true);
            var uiAnchors = GetComponentLookup<UIAnchor>(true);
            Dependency = Entities
                .WithAll<UILink>()
                .ForEach((Entity e, int entityInQueryIndex, in UserStatLinks userStatLinks, in CameraLink cameraLink) =>
            {
                var spawnEntity = new Entity();
                for (int i = 0; i < spawnEntities.Length; i++)
                {
                    var e2 = spawnEntities[i];
                    var characterLink = characterLinks[e2];
                    if (characterLink.character == e)
                    {
                        spawnEntity = e2;
                        break;  // only one UI per character atm
                    }
                }
                if (spawnEntity.Index == 0)
                {
                    return;
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));
                var prefab = statsUIPrefab;
                if (prefabLinks.HasComponent(spawnEntity))
                {
                    prefab = prefabLinks[spawnEntity].prefab;
                }
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CharacterLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLink);
                if (uiAnchors.HasComponent(spawnEntity))
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, uiAnchors[spawnEntity]);
                }
                // spawnedUI event
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawned((byte) userStatLinks.stats.Length));
                for (byte i = 0; i < userStatLinks.stats.Length; i++)
                {
                    var seed = (int) (128 + elapsedTime + i * 2048 + 4196 * entityInQueryIndex);
                    var frameEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.frame, e3);
                    UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, frameEntity, e3);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, frameEntity, i);
                    UICoreSystem.SetSound(PostUpdateCommands, entityInQueryIndex, frameEntity, seed, iconStyle.soundVolume);
                    UICoreSystem.SetSeed(PostUpdateCommands, entityInQueryIndex, frameEntity, seed);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, frameEntity, new Seed(seed));
                    UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.icon, frameEntity);
                    UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.label, frameEntity); // , labelPosition);
                    var dataEntity = userStatLinks.stats[i];
                    if (dataEntity.Index != 0)
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, frameEntity, new SetIconData(dataEntity));
                    }
                }
            })  .WithReadOnly(spawnEntities)
                .WithDisposeOnCompletion(spawnEntities)
                .WithReadOnly(characterLinks)
                .WithReadOnly(prefabLinks)
                .WithReadOnly(uiAnchors)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}