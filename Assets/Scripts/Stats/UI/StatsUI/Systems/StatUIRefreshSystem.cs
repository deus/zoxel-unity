using Unity.Entities;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using Zoxel.UI;
using Zoxel.Transforms;
// Todo: Redo this, it's a bit bad
// another system should check if stat has updated, then update the UI
// or create a command for stat updated, so the UI will be updated
// the idea is to create unlinked systems - decoupled
// When stat updates:
//  > use character UI link to update any connected stat uis

namespace Zoxel.Stats.UI
{
    [BurstCompile, UpdateInGroup(typeof(StatsUISystemGroup))]
    public partial class StatUIRefreshSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery characterQuery;
        private EntityQuery userStatsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			characterQuery = GetEntityQuery(ComponentType.ReadOnly<UserStatLinks>());
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var characterEntities = characterQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
			var characterStats = GetComponentLookup<UserStatLinks>(true);
            characterEntities.Dispose();
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var statValues = GetComponentLookup<StatValue>(true);
            var levelStats = GetComponentLookup<LevelStat>(true);
            userStatEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<RefreshStatsUI>()
                .ForEach((Entity e, int entityInQueryIndex, in Childrens childrens, in CharacterLink characterLink) =>
            {
                if (HasComponent<ApplyAttributes>(characterLink.character))
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<RefreshStatsUI>(entityInQueryIndex, e);
                // UnityEngine.Debug.LogError("Refreshing Stats UI");
                var userStatLinks = characterStats[characterLink.character];
                var values = new NativeList<int>();
                for (int i = 0; i < userStatLinks.stats.Length; i++)
                {
                    var userStatEntity = userStatLinks.stats[i];
                    var value = 0;
                    if (HasComponent<BaseStat>(userStatEntity))
                    {
                        value = (int) statValues[userStatEntity].value;
                    }
                    else if (HasComponent<AttributeStat>(userStatEntity))
                    {
                        value = (int) statValues[userStatEntity].value;
                    }
                    else if (HasComponent<RegenStat>(userStatEntity))
                    {
                        value = (int) statValues[userStatEntity].value;
                    }
                    else if (HasComponent<StateStat>(userStatEntity))
                    {
                        value = (int) statValues[userStatEntity].value;
                    }
                    else if (HasComponent<LevelStat>(userStatEntity))
                    {
                        value = (int) levelStats[userStatEntity].value;
                    }
                    values.Add(value);
                }
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    var child = childrens.children[i];
                    var value = values[i];
                    var numberText = new Text();
                    if (value != 0)
                    {
                        numberText = Text.IntegerToText(value);    // assumes positive?
                    }
                    PostUpdateCommands.AddComponent(entityInQueryIndex, child, new SetIconLabel(numberText));
                }
                values.Dispose();
            })  .WithReadOnly(characterStats)
                .WithReadOnly(levelStats)
                .WithReadOnly(statValues)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}