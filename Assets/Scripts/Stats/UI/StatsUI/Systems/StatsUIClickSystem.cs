using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Stats.Authoring;

namespace Zoxel.Stats.UI
{
    //! Increases a stat when you click it, using stat points.
    /**
    *   - UIClickEvent System -
    */
    [BurstCompile, UpdateInGroup(typeof(StatsUISystemGroup))]
    public partial class StatsUIClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery characterQuery;
        private EntityQuery controllersQuery;
        private EntityQuery userStatsQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            characterQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStatLinks>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<DeadEntity>());
            controllersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Player>(),
                ComponentType.ReadOnly<CharacterLink>(),
                ComponentType.Exclude<DestroyEntity>());
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<BaseStat>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate<StatSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var statSettings = GetSingleton<StatSettings>();
            var isUnlimitedStatPoints = statSettings.isUnlimitedStatPoints;
            var updateStatPrefab = StatsSystemGroup.updateStatPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var characterEntities = characterQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var characterStats = GetComponentLookup<UserStatLinks>(true);
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var characterLinks = GetComponentLookup<CharacterLink>(true);
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var statValues = GetComponentLookup<StatValue>(true);
            characterEntities.Dispose();
            controllerEntities.Dispose();
            userStatEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<StatButton>()
                .ForEach((Entity e, int entityInQueryIndex, in IconData iconData, in UIClickEvent uiClickEvent) =>
            {
                if (uiClickEvent.buttonType != ButtonActionType.Confirm)
                {
                    return;
                }
                if (!HasComponent<AttributeStat>(iconData.data))
                {
                    return;
                }
                var controllerEntity = uiClickEvent.controller;
                var characterEntity = characterLinks[controllerEntity].character;
                var userStatLinks = characterStats[characterEntity];
                // stat points ~ statpoints is a base
                var statPointUserStatEntity = new Entity();
                for (int i = 0; i < userStatLinks.stats.Length; i++)
                {
                    var userStatEntity = userStatLinks.stats[i];
                    if (HasComponent<StatPoint>(userStatEntity))
                    {
                        statPointUserStatEntity = userStatEntity;
                        break;
                    }
                }
                if (statPointUserStatEntity.Index == 0)
                {
                    return;
                }
                var attributeUserStatEntity = iconData.data;
                var statPoint = statValues[statPointUserStatEntity];
                if (statPoint.value > 0 || isUnlimitedStatPoints)
                {
                    if (!isUnlimitedStatPoints)
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, updateStatPrefab),
                            new UpdateEntityStat(characterEntity, statPointUserStatEntity, -1));
                    }
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, updateStatPrefab),
                        new UpdateEntityStat(characterEntity, attributeUserStatEntity, 1));
                }
            })  .WithReadOnly(characterStats)
                .WithReadOnly(characterLinks)
                .WithReadOnly(statValues)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}