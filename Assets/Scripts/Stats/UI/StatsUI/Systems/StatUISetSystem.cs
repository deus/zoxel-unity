using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.UI;
using Zoxel.Textures;

namespace Zoxel.Stats.UI
{
    //! Sets the ItemUI's icon and texture.
    /**
    *   - UI Update System -Skills
    */
    [BurstCompile, UpdateInGroup(typeof(StatsUISystemGroup))]
    public partial class StatUISetSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery statsQuery;
        private EntityQuery userStatsQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            statsQuery = GetEntityQuery(
                ComponentType.ReadOnly<RealmStat>(),
                ComponentType.ReadOnly<Texture>(),
                ComponentType.Exclude<DestroyEntity>());
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // Set icon to item
            var statEntities = statsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var textures = GetComponentLookup<Texture>(true);
            statEntities.Dispose();
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var userStatMetaDatas = GetComponentLookup<MetaData>(true);
            var statValues = GetComponentLookup<StatValue>(true);
            var levelStats = GetComponentLookup<LevelStat>(true);
            userStatEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEventFrames, InitializeEntity>()
                .WithAll<SetIconData>()
                .ForEach((Entity e, int entityInQueryIndex, ref IconData iconData, in SetIconData setIconData, in Childrens childrens) =>
            {
                if (!HasComponent<UserStat>(setIconData.data))
                {
                    return;
                }
                // UnityEngine.Debug.LogError("    > SetIconData");
                var userStatEntity = setIconData.data;
                if (!userStatMetaDatas.HasComponent(userStatEntity))
                {
                    return;
                }
                iconData.data = setIconData.data;
                if (childrens.children.Length >= 1)
                {
                    //var metaEntity = EntityManager.GetComponentData<MetaData>(iconData.data).data;
                    var iconEntity = childrens.children[0];
                    // ItemIconSetSystem.SetIconTextureEvent(EntityManager, PostUpdateCommands, iconEntity, metaEntity);
                    var statEntity = userStatMetaDatas[userStatEntity].data;
                    if (textures.HasComponent(statEntity))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, iconEntity, textures[statEntity].Clone());
                    }
                    PostUpdateCommands.AddComponent<TextureDirty>(entityInQueryIndex, iconEntity);
                }
                if (childrens.children.Length >= 2)
                {
                    var value = 0;
                    if (statValues.HasComponent(userStatEntity))
                    {
                        value = (int) statValues[userStatEntity].value;
                    }
                    else if (levelStats.HasComponent(userStatEntity))
                    {
                        value = (int) levelStats[userStatEntity].value;
                    }
                    var numberText = new Text();
                    if (value != 0)
                    {
                        numberText = Text.IntegerToText(value);
                    }
                    var textEntity = childrens.children[1];
                    PostUpdateCommands.AddComponent(entityInQueryIndex, textEntity, new SetRenderText(numberText));
                }
                // UnityEngine.Debug.LogError("Set to new Item: " + e.Index + " :: " + setIconData.data.Index + " with item meta: " + metaEntity.Index);
            })  .WithReadOnly(userStatMetaDatas)
                .WithReadOnly(levelStats)
                .WithReadOnly(statValues)
                .WithReadOnly(textures)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}