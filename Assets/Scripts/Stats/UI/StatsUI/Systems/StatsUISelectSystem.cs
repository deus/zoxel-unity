using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.UI;

namespace Zoxel.Stats.UI
{
    //! Handles UISelectEvent events for StatButton's.
    /**
    *   \todo Fix Attribute frames - for can spend point on them.
    */
    [BurstCompile, UpdateInGroup(typeof(StatsUISystemGroup))]
    public partial class StatsUISelectSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery statsQuery;
        private EntityQuery userStatsQuery;
        private NativeArray<Text> texts;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            statsQuery = GetEntityQuery(
                ComponentType.ReadOnly<RealmStat>(),
                ComponentType.ReadOnly<ZoxName>(),
                ComponentType.Exclude<DestroyEntity>());
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.Exclude<DestroyEntity>());
            texts = new NativeArray<Text>(7, Allocator.Persistent);
            texts[0] = new Text("\nBase Stat");
            texts[1] = new Text("\nState Stat");
            texts[2] = new Text("\nRegen Stat");
            texts[3] = new Text("\nAttribute Stat");
            texts[4] = new Text("\nLevel Stat");
            texts[5] = new Text("\nTargets: ");
            texts[6] = new Text("Nothing");
            RequireForUpdate(processQuery);
            
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var texts = this.texts;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var statEntities = statsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var statNames = GetComponentLookup<ZoxName>(true);
            var statDescriptions = GetComponentLookup<ZoxDescription>(true);
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var statValues = GetComponentLookup<StatValue>(true);
            var statValueMaxs = GetComponentLookup<StatValueMax>(true);
            var statEffectTargets = GetComponentLookup<StatEffectTarget>(true);
            var levelStats = GetComponentLookup<LevelStat>(true);
            var userStatMetaDatas = GetComponentLookup<MetaData>(true);
            statEntities.Dispose();
            userStatEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<UISelectEvent, StatButton>()
                .ForEach((Entity e, int entityInQueryIndex, in IconData iconData, in PanelLink panelLink, in UISelectEvent uiSelectEvent) =>
            {
                var controllerEntity = uiSelectEvent.character;
                PostUpdateCommands.AddComponent(entityInQueryIndex, controllerEntity, new SetPlayerTooltip(new Text()));
                var userStatEntity = iconData.data;
                if (!userStatMetaDatas.HasComponent(userStatEntity))
                {
                    return;
                }
                var statEntity = userStatMetaDatas[userStatEntity].data;
                if (!HasComponent<RealmStat>(statEntity))
                {
                    // UnityEngine.Debug.LogError("State stat has no entity: " + statIndex + " - " + arrayIndex);
                    return;
                }
                var tooltipText = statNames[statEntity].name.Clone();
                if (HasComponent<BaseStat>(userStatEntity)) // statType == StatTypeEditor.Base)
                {
                    var baseStat = statValues[userStatEntity];
                    if (baseStat.value != 0)
                    {
                        tooltipText.AddChar(' ');
                        tooltipText.AddInteger((int) baseStat.value);
                    }
                    tooltipText.AddText(texts[0]);
                }
                else if (HasComponent<StateStat>(userStatEntity)) // statType == StatTypeEditor.State)
                {
                    var stateStat = statValues[userStatEntity];
                    var stateStatMax = statValueMaxs[userStatEntity];
                    /*if (statSelected.value != 0)
                    {
                        tooltipText += " " + ((int)statSelected.value) + "/" + ((int)statSelected.maxValue);
                    }*/
                    if (stateStat.value != 0)
                    {
                        tooltipText.AddChar(' ');
                        tooltipText.AddInteger((int) stateStat.value);
                        tooltipText.AddChar('/');
                        tooltipText.AddInteger((int) stateStatMax.value);
                    }
                    tooltipText.AddText(texts[1]);
                }
                else if (HasComponent<RegenStat>(userStatEntity)) // statType == StatTypeEditor.Regen)
                {
                    var regenStat = statValues[userStatEntity];
                    if (regenStat.value != 0)
                    {
                        tooltipText.AddChar(' ');
                        tooltipText.AddInteger((int) regenStat.value);
                    }
                    tooltipText.AddText(texts[2]);
                }
                else if (HasComponent<AttributeStat>(userStatEntity)) // statType == StatTypeEditor.Attribute)
                {
                    var attribubteStat = statValues[userStatEntity];
                    if (attribubteStat.value != 0)
                    {
                        tooltipText.AddChar(' ');
                        tooltipText.AddInteger((int) attribubteStat.value);
                    }
                    tooltipText.AddText(texts[3]);
                }
                else if (HasComponent<LevelStat>(userStatEntity)) // statType == StatTypeEditor.Level)
                {
                    var levelStat = levelStats[userStatEntity];
                    if (levelStat.value != 0)
                    {
                        tooltipText.AddChar(' ');
                        tooltipText.AddChar('l');
                        tooltipText.AddChar('v');
                        tooltipText.AddChar('l');
                        tooltipText.AddChar(' ');
                        tooltipText.AddInteger((int) levelStat.value);
                    }
                    tooltipText.AddChar(' ');
                    tooltipText.AddChar('x');
                    tooltipText.AddChar('p');
                    tooltipText.AddChar(' ');
                    var experienceRequired = (int) (100 * (levelStat.experienceGained / levelStat.experienceRequired));
                    tooltipText.AddInteger((int) experienceRequired);
                    tooltipText.AddChar('%');
                    tooltipText.AddText(texts[4]);
                }
                if (statEffectTargets.HasComponent(userStatEntity))
                {
                    tooltipText.AddText(texts[5]);
                    var userStatTarget = statEffectTargets[userStatEntity].target;
                    if (userStatMetaDatas.HasComponent(userStatTarget))
                    {
                        var statTarget = userStatMetaDatas[userStatTarget].data;
                        tooltipText.AddText(statNames[statTarget].name);
                    }
                    else
                    {
                        tooltipText.AddText(texts[6]);
                    }
                }
                if (statDescriptions.HasComponent(statEntity))
                {
                    tooltipText.AddChar('\n');
                    tooltipText.AddText(statDescriptions[statEntity].description);
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, panelLink.panel, new SetPanelTooltip(tooltipText));
            })  .WithReadOnly(texts)
                .WithReadOnly(statNames)
                .WithReadOnly(statDescriptions)
                .WithReadOnly(statValues)
                .WithReadOnly(statValueMaxs)
                .WithReadOnly(statEffectTargets)
                .WithReadOnly(levelStats)
                .WithReadOnly(userStatMetaDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                    /*if (statSelected.value != 0)
                    {
                        tooltipText += " lvl " + statSelected.value; //statSelected.experienceGained + " x " + statSelected.experienceRequired;
                    }
                    else
                    {
                        tooltipText += " lvl 0"; //statSelected.experienceGained + " x " + statSelected.experienceRequired;
                    }
                    if (statSelected.experienceRequired != 0)
                    {
                        var experienceRequired = (int)(100*(statSelected.experienceGained / statSelected.experienceRequired));
                        tooltipText += " xp " + experienceRequired + "%"; //statSelected.experienceGained + " x " + statSelected.experienceRequired;
                    }*/
                /*var playerTooltipLinks = EntityManager.GetComponentData<PlayerTooltipLinks>(controllerEntity);
                if (statType == StatTypeEditor.Attribute)
                {
                    if (userStatLinks.stats[0].value > 0)
                    {
                        playerTooltipLinks.SetTooltipText(PostUpdateCommands, controllerEntity, "[action1] to Increase Attribute");
                    }
                    else
                    {
                        playerTooltipLinks.SetTooltipText(PostUpdateCommands, controllerEntity, "You have no Attribute Points");
                    }
                }
                else
                {
                    playerTooltipLinks.SetTooltipText(PostUpdateCommands, controllerEntity, "");
                }*/