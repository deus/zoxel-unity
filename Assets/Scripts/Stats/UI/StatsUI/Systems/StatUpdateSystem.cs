﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Transforms;

namespace Zoxel.Stats.UI
{
    //! Updates a Stat on a Stats entity.
    [BurstCompile, UpdateInGroup(typeof(StatsUISystemGroup))]
    public partial class StatUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery updateStatUpdateQuery;
        private EntityQuery userStatsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			updateStatUpdateQuery = GetEntityQuery(
                ComponentType.ReadOnly<UpdateEntityStat>(),
                ComponentType.Exclude<DelayEvent>());
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(updateStatUpdateQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var eventEntities = updateStatUpdateQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
			var updateCharacterStats = GetComponentLookup<UpdateEntityStat>(true);
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var statValues = GetComponentLookup<StatValue>(false);
            userStatEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<UserStatLinks>()
                .ForEach((Entity e, int entityInQueryIndex, in UILink uiLink) =>
            {
                var didUpdateAttributes = false;
                for (int i = 0; i < eventEntities.Length; i++)
                {
                    var e2 = eventEntities[i];
                    var updateEntityStat = updateCharacterStats[e2];
                    if (updateEntityStat.character == e)
                    {
                        if (UpdateStat(updateEntityStat.stat, updateEntityStat.amount, ref statValues))
                        {
                            if (HasComponent<AttributeStat>(updateEntityStat.stat))
                            {
                                didUpdateAttributes = true;
                            }
                        }
                    }
                }
                if (didUpdateAttributes)
                {
                    for (int i = 0; i < uiLink.uis.Length; i++)
                    {
                        if (HasComponent<StatsUI>(uiLink.uis[i]))
                        {
                            PostUpdateCommands.AddComponent<RefreshStatsUI>(entityInQueryIndex, uiLink.uis[i]);
                            break;
                        }
                    }
                    PostUpdateCommands.AddComponent<ApplyAttributes>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(eventEntities)
                .WithDisposeOnCompletion(eventEntities)
                .WithReadOnly(updateCharacterStats)
                .WithNativeDisableContainerSafetyRestriction(statValues)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static bool UpdateStat(Entity targetUserStat, int amount, ref ComponentLookup<StatValue> statValues)
    {
            if (statValues.HasComponent(targetUserStat))
            {
                var baseStat = statValues[targetUserStat];
                baseStat.value = (byte) (((int) baseStat.value) + amount);
                statValues[targetUserStat] = baseStat;
                return true;
            }
            return false;
        }
    }
}