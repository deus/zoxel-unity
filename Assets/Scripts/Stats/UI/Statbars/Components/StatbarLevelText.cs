using Unity.Entities;

namespace Zoxel.Stats.UI
{
    //! UIElement for level userStatLinks text.
    /*
    *   \todo Remove delay. Test that it still works.
    */
    public struct StatbarLevelText : IComponentData
    {
        public LevelStat stat;
        public byte delay;
    }
}