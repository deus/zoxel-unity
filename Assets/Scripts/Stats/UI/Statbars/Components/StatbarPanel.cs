using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Stats.UI
{
    public struct StatbarPanel : IComponentData
    {
        public BlitableArray<Entity> statbars;

        public void Dispose()
        {
            if (statbars.Length > 0)
            {
                statbars.Dispose();
            }
        }
    }
}