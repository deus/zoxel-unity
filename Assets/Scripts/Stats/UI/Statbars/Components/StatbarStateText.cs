using Unity.Entities;

namespace Zoxel.Stats.UI
{
    //! UIElement for state userStatLinks text.
    struct StatbarStateText : IComponentData
    {
        public float value;
        public float maxValue;
    }
}