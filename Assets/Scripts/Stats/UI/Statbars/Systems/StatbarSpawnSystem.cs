﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Stats.UI
{
    //! Spawns generic statbars over the top of characters.
    /**
    *   - UI Spawn System -
    *   \todo Make statbar flash white after taking damage. (.3 seconds)
    */
    [BurstCompile, UpdateInGroup(typeof(StatsUISystemGroup))]
    public partial class StatbarSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        public float2 panelSizeNPC = new float2(0.3f, 0.08f);

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var edgeSpace = 0.03f;
            var uiDatam = UIManager.instance.uiDatam;
            var npcbarPrefab = StatsUISystemGroup.npcbarPrefab;
            var frontbarPrefab = StatsUISystemGroup.frontbarPrefab;
            var statbarLabelPrefab = StatsUISystemGroup.statbarLabelPrefab;
            var healthColor = uiDatam.healthColor;
            var healthTextColor = new Color(uiDatam.healthTextColor);
            var textPadding = uiDatam.statbarTextPadding;
            var healthPanelColor = uiDatam.healthPanelColor;
            var statbarFadeTime = UIManager.instance.uiDatam.statbarFadeTime;
            var panelSizeNPC2 = panelSizeNPC;
            var textOutlineColor = new Color(uiDatam.overlayTextOutlineColor);
            var textGenerationData = uiDatam.statbarGenerationData;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<SpawnStatbar>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<OnUserStatsSpawned>()
                .WithAll<SpawnStatbar, NPCCharacter>()
                .ForEach((Entity e, int entityInQueryIndex, in UILink uiLink, in Body body, in Translation translation, in UserStatLinks userStatLinks) =>
            {
                var positionOffset = body.size.y * 1f;
                var spawnPosition = translation.Value + new float3(0, positionOffset, 0);
                var backbar = PostUpdateCommands.Instantiate(entityInQueryIndex, npcbarPrefab);
                // var userStatEntity = userStatLinks.stats[0];
                var healthUserStat = new Entity();
                for (int i = 0; i < userStatLinks.stats.Length; i++)
                {
                    if (HasComponent<HealthStat>(userStatLinks.stats[i]))
                    {
                        healthUserStat = userStatLinks.stats[i];
                        break;
                    }
                }
                if (healthUserStat.Index > 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));
                    CreateStatbar(PostUpdateCommands, entityInQueryIndex, e, healthUserStat, backbar, true, panelSizeNPC2, healthPanelColor,
                        healthColor, healthTextColor, textOutlineColor, textGenerationData, frontbarPrefab, statbarLabelPrefab, edgeSpace, textPadding, statbarFadeTime);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, new TrailerUI(positionOffset));
                    // var spawnedEventEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static void CreateStatbar(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity character,
            Entity userStatEntity, Entity backbar, bool hasFader, float2 panelSize, UnityEngine.Color panelColor,
            UnityEngine.Color frontbarColor, Color textColor, Color textOutlineColor, TextGenerationData textGenerationData,
            Entity frontbarPrefab, Entity statbarLabelPrefab,
            float edgeSpace, float2 textPadding, float statbarFadeTime)
        {
            //UnityEngine.Debug.LogError("Setting front Colour: " + backbar.Index + " : " + frontbarColor);
            float fontSize = panelSize.y * 0.76f;
            var statLink = new StatLink(userStatEntity); // (byte) statType, statIndex); // all
            var uiElement = new Size2D(panelSize);
            // Setters
            PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, new CharacterLink(character));
            PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, statLink);
            PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, uiElement);
            if (hasFader)
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, new Fader(0, panelColor.a, statbarFadeTime));
                PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, new MaterialBaseColor {
                    Value = new float4(panelColor.r, panelColor.g, panelColor.b, 0) });
            }
            else
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, backbar, new MaterialBaseColor(panelColor));
            }
            PostUpdateCommands.AddComponent(entityInQueryIndex, backbar, new OnChildrenSpawned(2));
            
            var parentLink = new ParentLink(backbar);   // for 2 children

            var frontbar = PostUpdateCommands.Instantiate(entityInQueryIndex, frontbarPrefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, new Zoxel.Transforms.Child(0));
            PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, parentLink);
            var percentageUI = new PercentageUI
            {
                percentage = 1, 
                size = panelSize,
                edgeSpace = edgeSpace // 0.008f
            };
            percentageUI.SetTargetPercentage(0);
            PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, percentageUI);
            PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, statLink);
            PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, uiElement);
            if (hasFader)
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, new Fader(0, frontbarColor.a, statbarFadeTime));
                PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, new MaterialBaseColor {
                    Value = new float4(frontbarColor.r, frontbarColor.g, frontbarColor.b, 0) });
            }
            else
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, frontbar, new MaterialBaseColor(frontbarColor));
            }
            var textbar = PostUpdateCommands.Instantiate(entityInQueryIndex, statbarLabelPrefab);
            var renderText = new RenderTextData
            {
                textGenerationData = textGenerationData,
                color = textColor,
                outlineColor = textOutlineColor,
                fontSize = fontSize,
                margins = textPadding
            };
            PostUpdateCommands.SetComponent(entityInQueryIndex, textbar, renderText);
            PostUpdateCommands.SetComponent(entityInQueryIndex, textbar, parentLink);
            PostUpdateCommands.SetComponent(entityInQueryIndex, textbar, statLink);
            var textColor2 = textColor.GetColor();
            if (hasFader)
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, textbar, new Fader(0, textColor2.a, statbarFadeTime));
                PostUpdateCommands.SetComponent(entityInQueryIndex, textbar, new MaterialBaseColor {
                    Value = new float4(textColor2.r, textColor2.g, textColor2.b, 0) });
            }
            else
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, textbar, new MaterialBaseColor(textColor2));
            }
            PostUpdateCommands.SetComponent(entityInQueryIndex, textbar, new Zoxel.Transforms.Child(1));
        }
    }
}