using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;

namespace Zoxel.Stats.UI
{
    //! Updates the text to match the LevelStat stat of a character.
    [BurstCompile, UpdateInGroup(typeof(StatsUISystemGroup))]
    public partial class StatbarLevelTextSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery userStatsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<LevelStat>(),
                ComponentType.Exclude<DestroyEntity>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            //! \todo Add helper functions in Text Struct for integer conversion and addition of Texts
            var spaceByte = Text.ConvertCharToByte(' ');
            var LByte = Text.ConvertCharToByte('L');
            var vByte = Text.ConvertCharToByte('v');
            var lByte = Text.ConvertCharToByte('l');
            var slashByte = Text.ConvertCharToByte('/');
            var doubleDots = Text.ConvertCharToByte(':');
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var levelStats = GetComponentLookup<LevelStat>(true);
            // userStatEntities.Dispose();
            Dependency = Entities
                .WithNone<InitializeEntity, RenderTextDirty, OnRenderTextSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref RenderText renderText, ref StatbarLevelText statbarLevelText, in StatLink statLink) =>
            {
                var userStatEntity = statLink.userStat;
                if (!levelStats.HasComponent(userStatEntity))
                {
                    return;
                }
                var levelStat = levelStats[userStatEntity];
                /*if (statLink.statType != (byte) StatTypeEditor.Level || statLink.statIndex < 0 || statLink.statIndex >= characterStats.levels.Length)
                {
                    return;
                }
                var levelStat = characterStats.levels[statLink.statIndex];*/
                var levelValue = (int)(math.floor(levelStat.value));
                var experienceGainedValue = (int)(math.floor(levelStat.experienceGained));
                var experienceRequiredValue = (int)(math.floor(levelStat.experienceRequired));
                if ((int)(math.floor(statbarLevelText.stat.value)) == levelValue && 
                    (int)(math.floor(statbarLevelText.stat.experienceGained)) == experienceGainedValue &&
                    (int)(math.floor(statbarLevelText.stat.experienceRequired)) == experienceRequiredValue)
                {
                    return;
                }
                statbarLevelText.stat = levelStat;
                // string lengths
                var levelValueLength = ((levelValue == 0) ? 1 : ((int) math.floor(math.log10(math.abs(levelValue))) + 1));
                var experienceGainedValueLength = ((experienceGainedValue == 0) ? 1 : ((int) math.floor(math.log10(math.abs(experienceGainedValue))) + 1));
                var experienceRequiredValueLength = ((experienceRequiredValue == 0) ? 1 : ((int) math.floor(math.log10(math.abs(experienceRequiredValue))) + 1));
                
                var levelWordLength = 4;
                var text = new Text();
                text.Initialize(levelWordLength + levelValueLength + 2 + experienceGainedValueLength + 1 + experienceRequiredValueLength);

                // LevelStat Value Gained
                text.textData[0] = LByte; // 21 + 26;   // L
                text.textData[1] = vByte; // 31;        // v
                text.textData[2] = lByte; // 21;        // l
                text.textData[3] = spaceByte; // 255;
                // Note:
                //      This uses division and modulus to get the bytes/numbers at an index of an integer
                var digitMultiplier = 1;
                var levelValuStartIndex = levelWordLength;
                var levelValuMaxIndex = levelWordLength + levelValueLength;
                for (int i = levelValuStartIndex; i < levelValuMaxIndex; i++)
                {
                    var reverseIndex = levelValuMaxIndex - 1 - i + levelValuStartIndex;    // max index - current one + startIndex again
                    text.textData[reverseIndex] = (byte) ((levelValue / digitMultiplier) % 10);
                    digitMultiplier *= 10;
                }
                text.textData[levelValuMaxIndex] = doubleDots; // 77;        // :
                text.textData[levelValuMaxIndex + 1] = 255;   // space
                // Experience Gained
                digitMultiplier = 1;
                var experienceGainedStartIndex = levelValuMaxIndex + 2;
                var experienceGainedMaxIndex = experienceGainedStartIndex + experienceGainedValueLength;
                for (int i = experienceGainedStartIndex; i < experienceGainedMaxIndex; i++)
                {
                    var reverseIndex = experienceGainedMaxIndex - 1 - i + experienceGainedStartIndex;    // max index - current one + startIndex again
                    text.textData[reverseIndex] = (byte) ((experienceGainedValue / digitMultiplier) % 10);
                    digitMultiplier *= 10;
                }
                text.textData[experienceGainedStartIndex + experienceGainedValueLength] = slashByte; // 75;  // / - slash
                // Experience Required
                digitMultiplier = 1;
                var experienceRequiredStartIndex = experienceGainedStartIndex + experienceGainedValueLength + 1;
                var experienceRequiredMaxIndex = text.textData.Length;
                for (int i = experienceRequiredStartIndex; i < experienceRequiredMaxIndex; i++)
                {
                    var reverseIndex = experienceRequiredMaxIndex - 1 - i + experienceRequiredStartIndex;    // max index - current one + startIndex again
                    text.textData[reverseIndex] = (byte) ((experienceRequiredValue / digitMultiplier) % 10);
                    digitMultiplier *= 10;
                }
                // Now set text
                renderText.text.Dispose();
                renderText.text = text;
                PostUpdateCommands.AddComponent<RenderTextDirty>(entityInQueryIndex, e);
                if (userStatEntities.Length > 0) { var e2 = userStatEntities[0]; }
            })  .WithReadOnly(levelStats)
                .WithReadOnly(userStatEntities)
                .WithDisposeOnCompletion(userStatEntities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}