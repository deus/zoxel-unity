using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;

namespace Zoxel.Stats.UI
{
    //! Sets percentage for the BarUI based on stat value.
    [BurstCompile, UpdateInGroup(typeof(StatsUISystemGroup))]
    public partial class StatbarPercentageSystem : SystemBase
    {
        private EntityQuery userStatsQuery;

        protected override void OnCreate()
        {
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.Exclude<DestroyEntity>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var statValues = GetComponentLookup<StatValue>(true);
            var statValueMaxs = GetComponentLookup<StatValueMax>(true);
            var levelStats = GetComponentLookup<LevelStat>(true);
            // userStatEntities.Dispose();
            Dependency = Entities.ForEach((Entity e, ref PercentageUI percentageUI, in StatLink statLink) =>
            {
                var userStatEntity = statLink.userStat;
                if (statValueMaxs.HasComponent(userStatEntity)) //statLink.statType == (byte)StatTypeEditor.State && statLink.statIndex >= 0 && statLink.statIndex < characterStats.states.Length)
                {
                    var stateStat = statValues[userStatEntity];
                    var statValueMax = statValueMaxs[userStatEntity];
                    var percentage = stateStat.value / statValueMax.value;
                    percentageUI.SetTargetPercentage(percentage);
                }
                else if (levelStats.HasComponent(userStatEntity)) // statLink.statType == (byte)StatTypeEditor.Level && statLink.statIndex >= 0 && statLink.statIndex < characterStats.levels.Length)
                {
                    var levelStat = levelStats[userStatEntity];
                    var percentage = levelStat.experienceGained / levelStat.experienceRequired;
                    percentageUI.SetTargetPercentage(percentage);
                }
                else
                {
                    percentageUI.SetTargetPercentage(0);
                }
                if (userStatEntities.Length > 0) { var e2 = userStatEntities[0]; }
            })  .WithReadOnly(statValues)
                .WithReadOnly(statValueMaxs)
                .WithReadOnly(levelStats)
                .WithReadOnly(userStatEntities)
                .WithDisposeOnCompletion(userStatEntities)
                .ScheduleParallel(Dependency);
        }
    }
}