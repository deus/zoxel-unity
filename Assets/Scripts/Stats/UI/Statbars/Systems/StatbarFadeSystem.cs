using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Rendering;

namespace Zoxel.Stats.UI
{
    //! Closes and opens statbars based on max/nonmax value states
    [BurstCompile, UpdateInGroup(typeof(StatsUISystemGroup))]
    public partial class StatbarFadeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery userStatsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userStatsQuery = GetEntityQuery(ComponentType.ReadOnly<StateStat>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var statValues = GetComponentLookup<StatValue>(true);
            var statValueMaxs = GetComponentLookup<StatValueMax>(true);
            // userStatEntities.Dispose();
            Dependency = Entities
                .WithNone<Regening, FadeIn, FadeOut>()
                .WithAll<Fader, Statbar>()
                .ForEach((Entity e, int entityInQueryIndex, in StatLink statLink) =>
            {
                if (!HasComponent<StateStat>(statLink.userStat))
                {
                    return;
                }
                var userStatEntity = statLink.userStat;
                if (statValues.HasComponent(userStatEntity))
                {
                    var stateStat = statValues[userStatEntity];
                    var stateStatMax = statValueMaxs[userStatEntity];
                    if (stateStat.value == stateStatMax.value)
                    {
                        if (HasComponent<FaderVisible>(e))
                        {
                            PostUpdateCommands.AddComponent<FadeOut>(entityInQueryIndex, e);
                        }
                    }
                    else
                    {
                        if (HasComponent<FaderInvisible>(e))
                        {
                            PostUpdateCommands.AddComponent<FadeIn>(entityInQueryIndex, e);
                        }
                    }
                }
                if (userStatEntities.Length > 0) { var e2 = userStatEntities[0]; }
            })  .WithReadOnly(statValues)
                .WithReadOnly(statValueMaxs)
                .WithReadOnly(userStatEntities)
                .WithDisposeOnCompletion(userStatEntities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}