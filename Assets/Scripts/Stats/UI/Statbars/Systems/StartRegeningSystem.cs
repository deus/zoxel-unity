using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Rendering;
using Zoxel.Stats.Authoring;

namespace Zoxel.Stats.UI
{
    //! System adds a regen component if they dont have one
    /**
    *   Opens up a statbar UI if they dont have one
    *   \todo Fix for multiple userStatLinks regening at once.
    */
    [BurstCompile, UpdateInGroup(typeof(StatsUISystemGroup))]
    public partial class StartRegeningSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userStatsQuery;
        private EntityQuery statbarsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.ReadOnly<StateStat>(),
                ComponentType.Exclude<DestroyEntity>());
            statbarsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Statbar>(),
                ComponentType.ReadOnly<StatLink>(),
                ComponentType.ReadOnly<Fader>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(userStatsQuery);
            RequireForUpdate<StatSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var statSettings = GetSingleton<StatSettings>();
            var disableStatbars = statSettings.disableStatbars;
            if (disableStatbars)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var statValues = GetComponentLookup<StatValue>(true);
            var statValueMaxs = GetComponentLookup<StatValueMax>(true);
            userStatEntities.Dispose();
            var statbarEntities = statbarsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var statLinks = GetComponentLookup<StatLink>(true);
            var hasStatbars = statbarEntities.Length > 0;
            statbarEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, in StartRegening startRegening, in UserStatLinks userStatLinks, in UILink uiLink) =>
            {
                var userStatEntity = startRegening.userStat;
                // Adding Regen component to start regening
                if (userStatLinks.stats.Length == 0 || !statValues.HasComponent(userStatEntity))
                {
                    return;
                }
                var stateStat = statValues[userStatEntity];
                var stateStatMax = statValueMaxs[userStatEntity];
                if (stateStat.value == stateStatMax.value)
                {
                    // no need for regen
                    return;
                }
                var wasHealthbarFound = false;
                if (hasStatbars)
                {
                    //! For any statbars that are linked to the triggering user stat
                    for (int i = 0; i < uiLink.uis.Length; i++)
                    {
                        var uiEntity = uiLink.uis[i];
                        if (uiEntity.Index <= 0)
                        {
                            continue;
                        }
                        if (statLinks.HasComponent(uiEntity))
                        {
                            var statLink = statLinks[uiEntity];
                            if (statLink.userStat == userStatEntity)
                            {
                                wasHealthbarFound = true;
                                if (HasComponent<FaderInvisible>(e))
                                {
                                    PostUpdateCommands.AddComponent<FadeIn>(entityInQueryIndex, uiEntity);
                                }
                            }
                        }
                    }
                }
                // NPC doesn't have healthbar, so spawn them when they enter combat
                if (!wasHealthbarFound && HasComponent<HealthStat>(userStatEntity))
                {
                    PostUpdateCommands.AddComponent<SpawnStatbar>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(statLinks)
                .WithReadOnly(statValues)
                .WithReadOnly(statValueMaxs)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

/*if (!stateStat.IsMaxValue())
{
    if (HasComponent<FaderInvisible>(e))
    {
        PostUpdateCommands.AddComponent<FadeIn>(entityInQueryIndex, uiEntity);
    }
}
else
{
    if (HasComponent<FaderVisible>(e))
    {
        PostUpdateCommands.AddComponent<FadeOut>(entityInQueryIndex, uiEntity);
    }
}*/