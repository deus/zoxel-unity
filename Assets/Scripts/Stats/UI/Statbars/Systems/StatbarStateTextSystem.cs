using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;

namespace Zoxel.Stats.UI
{
    //! Updates the text to match the state stat of a character.
    [BurstCompile, UpdateInGroup(typeof(StatsUISystemGroup))]
    public partial class StatbarStateTextSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery userStatsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<StateStat>(),
                ComponentType.Exclude<DestroyEntity>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var slashByte = Text.ConvertCharToByte('/');
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var statValues = GetComponentLookup<StatValue>(true);
            var statValueMaxs = GetComponentLookup<StatValueMax>(true);
            // userStatEntities.Dispose();
            Dependency = Entities
                .WithNone<InitializeEntity, RenderTextDirty, OnRenderTextSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref RenderText renderText, ref StatbarStateText statbarText, in StatLink statLink) =>
            {
                var userStatEntity = statLink.userStat;
                if (!statValueMaxs.HasComponent(userStatEntity))
                {
                    return;
                }
                var stateStat = statValues[userStatEntity];
                var stateStatMax = statValueMaxs[userStatEntity];
                // var stateStat = characterStats.states[statLink.statIndex];
                var statValue = (int)(stateStat.value);
                // var statValue = (int)(math.ceil(stateStat.value));
                //! Doesn't say 1 Health.
                if (HasComponent<HealthStat>(userStatEntity) && stateStat.value > 0 && stateStat.value < 1)
                {
                    statValue = 1;
                }
                var statMaxValue = (int)(stateStatMax.value);
                // If stat in text is the same as the stat, return.
                if ((int) statbarText.value == statValue && (int) statbarText.maxValue == statMaxValue)
                {
                    return;
                }
                // UnityEngine.Debug.LogError(character.Index + ": Updated statbar text: " + statValue);
                statbarText.value = statValue;
                statbarText.maxValue = statMaxValue;
                var statValueLength = ((statValue == 0) ? 1 : ((int)math.floor(math.log10(math.abs(statValue))) + 1));
                var statMaxValueLength = ((statMaxValue == 0) ? 1 : ((int)math.floor(math.log10(math.abs(statMaxValue))) + 1));
                var text = new Text();
                text.Initialize(statValueLength + statMaxValueLength + 1);
                // reverse after
                int digitMultiplier = 1;
                for (int i = 0; i < statValueLength; i++)
                {
                    var reverseIndex = statValueLength - 1 - i + 0;    // max index - current one + startIndex again
                    text.textData[reverseIndex] = (byte) ((statValue / digitMultiplier) % 10);
                    digitMultiplier *= 10;
                }
                text.textData[statValueLength] = slashByte; // 75;  // Slash
                // Stat Value
                digitMultiplier = 1;
                var statValueStartIndex = statValueLength + 1;
                var statValueMaxIndex = text.textData.Length;
                for (int i = statValueStartIndex; i < statValueMaxIndex; i++)
                {
                    var reverseIndex = statValueMaxIndex - 1 - i + statValueStartIndex;    // max index - current one + startIndex again
                    text.textData[reverseIndex] = (byte) ((statMaxValue / digitMultiplier) % 10);
                    digitMultiplier *= 10;
                }
                renderText.text.Dispose();
                renderText.text = text;
                PostUpdateCommands.AddComponent<RenderTextDirty>(entityInQueryIndex, e);
                if (userStatEntities.Length > 0) { var e2 = userStatEntities[0]; }
            })  .WithReadOnly(statValues)
                .WithReadOnly(statValueMaxs)
                .WithReadOnly(userStatEntities)
                .WithDisposeOnCompletion(userStatEntities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

/*var stateStat = characterStats.levels[statLink.statIndex];
var text = "Lvl " + stateStat.value + ": " + (int)(math.floor(stateStat.experienceGained)) +
    "/" + (int)(math.floor(stateStat.experienceRequired));
if (renderText.SetText(text))
{
    //UnityEngine.Debug.LogError("Stat RenderText updated to: " + text);
}*/
/*else if (statLink.statType == (byte)StatTypeEditor.Level && statLink.statIndex >= 0 && statLink.statIndex < characterStats.levels.Length)
{
    var stateStat = characterStats.levels[statLink.statIndex];
    var text = "Lvl " + stateStat.value + ": " + (int)(math.floor(stateStat.experienceGained)) +
        "/" + (int)(math.floor(stateStat.experienceRequired));
    if (renderText.SetText(text))
    {
        //UnityEngine.Debug.LogError("Stat RenderText updated to: " + text);
    }
}*/