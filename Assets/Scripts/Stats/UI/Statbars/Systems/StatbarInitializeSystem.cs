using Unity.Entities;
using Zoxel.UI;
using Zoxel.Rendering;

namespace Zoxel.Stats.UI
{
    //! Initializes the statbar UI mesh and material
    [ UpdateInGroup(typeof(StatsUISystemGroup))]
    public partial class StatbarInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var materials = UIManager.instance.materials;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var backbarMaterial = materials.backBarMaterial;
            var frontMaterial = materials.frontBarMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<DestroyEntity>()
                .WithAll<InitializeEntity, Statbar>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in Size2D size2D, in UIMeshData uiMeshData) =>
            {
                zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size2D.size, uiMeshData.horizontalAlignment, uiMeshData.verticalAlignment, uiMeshData.offset);
                zoxMesh.material = new UnityEngine.Material(backbarMaterial);
                zoxMesh.layer = worldUILayer;
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
            Entities
                .WithNone<DestroyEntity>()
                .WithAll<InitializeEntity, StatbarFront>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in Size2D size2D, in UIMeshData uiMeshData) =>
            {
                zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size2D.size, uiMeshData.horizontalAlignment, uiMeshData.verticalAlignment, uiMeshData.offset);
                zoxMesh.material = new UnityEngine.Material(frontMaterial);
                zoxMesh.layer = worldUILayer;
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
        }   
    }
}