﻿using Unity.Entities;
using Unity.Mathematics;

using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.UI;

namespace Zoxel.Stats.UI
{
    [UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class StatsUISystemGroup : ComponentSystemGroup
    {
        public static Entity npcbarPrefab;
        public static Entity frontbarPrefab;
        public static Entity statbarLabelPrefab;
        public static Entity panelPrefab;
        public static Entity experiencebarPanelPrefab;
        public static Entity experiencebarFrontPrefab;
        public static Entity experiencebarTextbarPrefab;
        
        protected override void OnCreate()
        {
            base.OnCreate();
            var npcArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(FadeIn),
                typeof(Statbar),
                typeof(StatLink),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(FaceCamera),
                typeof(TrailerUI),
                typeof(NewCharacterUI),
                typeof(CharacterUI),
                typeof(CharacterLink),
                typeof(Childrens),
                typeof(MaterialBaseColor),
                typeof(ZoxMesh),
                typeof(Fader),
                typeof(FaderInvisible),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            npcbarPrefab = EntityManager.CreateEntity(npcArchetype);
            EntityManager.SetComponentData(npcbarPrefab, new NonUniformScale { Value = new float3(1f, 1f, 1f) });
            var frontbarArchtype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(StatbarFront),
                typeof(StatLink),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(PercentageUI),
                typeof(ZoxMesh),
                typeof(MaterialBaseColor),
                typeof(Fader),
                typeof(FaderInvisible),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(LocalScale),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale)
            );
            frontbarPrefab = EntityManager.CreateEntity(frontbarArchtype);
            EntityManager.SetComponentData(frontbarPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(frontbarPrefab, LocalScale.One);
            var statbarLabelArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(StatLink),
                typeof(StatbarStateText),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(RenderText),
                typeof(RenderTextData),
                typeof(MaterialBaseColor),
                typeof(InvisibleUI),
                typeof(Fader),
                typeof(FaderInvisible),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale)
            );
            statbarLabelPrefab = EntityManager.CreateEntity(statbarLabelArchetype);
            EntityManager.SetComponentData(statbarLabelPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(statbarLabelPrefab, new NonUniformScale { Value = new float3(1f, 1f, 1f) });
            // player ui
            var panelArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(Statbar),
                typeof(StatLink),
                typeof(GameOnlyUI),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(PanelUI),
                typeof(PanelPosition),
                typeof(UIAnchor),
                typeof(UIPosition),
                typeof(PanelTooltipLink),
                typeof(NewCharacterUI),
                typeof(CharacterUI),
                typeof(CharacterLink),
                typeof(CameraLink),
                typeof(OrbitTransform),
                typeof(OrbitPosition),
                typeof(Childrens),
                typeof(ZoxMesh),
                typeof(MaterialBaseColor),
                typeof(Fader),
                typeof(FaderInvisible),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld));
            panelPrefab = EntityManager.CreateEntity(panelArchetype);
            EntityManager.SetComponentData(panelPrefab, new NonUniformScale { Value = new float3(1f, 1f, 1f) });
            experiencebarPanelPrefab = EntityManager.Instantiate(panelPrefab);
            EntityManager.AddComponent<Prefab>(experiencebarPanelPrefab);
            EntityManager.AddComponent<ExperienceBar>(experiencebarPanelPrefab);
            EntityManager.RemoveComponent<Fader>(experiencebarPanelPrefab);
            EntityManager.RemoveComponent<FaderInvisible>(experiencebarPanelPrefab);
            experiencebarTextbarPrefab = EntityManager.Instantiate(statbarLabelPrefab);
            EntityManager.AddComponent<Prefab>(experiencebarTextbarPrefab);
            EntityManager.AddComponent<StatbarLevelText>(experiencebarTextbarPrefab);
            EntityManager.RemoveComponent<Fader>(experiencebarTextbarPrefab);
            EntityManager.RemoveComponent<FaderInvisible>(experiencebarTextbarPrefab);
            experiencebarFrontPrefab = EntityManager.Instantiate(frontbarPrefab);
            EntityManager.AddComponent<Prefab>(experiencebarFrontPrefab);
            EntityManager.RemoveComponent<Fader>(experiencebarFrontPrefab);
            EntityManager.RemoveComponent<FaderInvisible>(experiencebarFrontPrefab);
        }
    }
}