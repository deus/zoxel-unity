using Unity.Entities;

namespace Zoxel.Stats
{
    //! Timed saves when  userStatLinks are regening.
    public struct StatsSaverTimer : IComponentData
    {
        public double lastSavedStatsTime;
    }
}