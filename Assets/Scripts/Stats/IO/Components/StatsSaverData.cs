using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Stats
{
    //! Stores a byte array used to save userStatLinks.
    public struct StatsSaverData : IComponentData
    {
        public BlitableArray<byte> data;
        const int sizeOfInt = 4;
        const int sizeOfFloat = 4;

        public void Dispose()
        {
            data.Dispose();
        }

        public void Initialize(int count)
        {
            if (data.Length != count)
            {
                Dispose();
                data = new BlitableArray<byte>(count, Allocator.Persistent);
            }
        }

        /*public static int GetFileSize(in UserStatLinks userStatLinks)
        {
            return 0;
        }*/
            /*return 5 // header size
                + userStatLinks.stats.Length * (sizeOfInt + sizeOfFloat)   // int + float;
                + userStatLinks.states.Length * (sizeOfInt + 2 * sizeOfFloat)   // int + 2 float;
                + userStatLinks.regens.Length * (2 * sizeOfInt + sizeOfFloat)   // 2 int + float;
                + userStatLinks.attributes.Length * (2 * sizeOfInt + 2 * sizeOfFloat)   // 2 int + float;
                + userStatLinks.levels.Length * (2 * sizeOfInt + 2 * sizeOfFloat)   // 2 int + float;
                ;*/

        public static readonly byte[] statTypeByteSizes = new byte[] { 9, 13, 9, 13, 17 };
        /*    1 + sizeOfInt + sizeOfFloat,
            1 + sizeOfInt + sizeOfFloat * 2,
            1 + sizeOfInt + sizeOfFloat,
            1 + sizeOfInt + sizeOfFloat * 2,
            1 + sizeOfInt + sizeOfFloat * 3
        };*/

        public void SetStats(in UserStatLinks userStatLinks,
            in ComponentLookup<ZoxID> statIDs,
            in ComponentLookup<StatMetaType> statMetaTypes,
            in ComponentLookup<MetaData> userStatMetaDatas,
            in ComponentLookup<StatValue> statValues,
            in ComponentLookup<StatValueMax> statValueMaxs,
            in ComponentLookup<AttributeStat> attributeStats,
            in ComponentLookup<LevelStat> levelStats)
        {
            if (userStatLinks.stats.Length == 0)
            {
                // UnityEngine.Debug.LogError("statsLength is 0");
                Dispose();
                return;
            }
            var length = 0;
            for (int i = 0; i < userStatLinks.stats.Length; i++)
            {
                var userStatEntity = userStatLinks.stats[i];
                if (!userStatMetaDatas.HasComponent(userStatEntity))
                {
                    Dispose();
                    return;
                }
                var statEntity = userStatMetaDatas[userStatEntity].data;
                if (!statIDs.HasComponent(statEntity))
                {
                    Dispose();
                    return;
                }
                var statType = statMetaTypes[statEntity].type;
                length += StatsSaverData.statTypeByteSizes[statType];
            }
            Initialize(length);

            int index = 0;
            var bytes = new NativeList<byte>();
            for (int i = 0; i < userStatLinks.stats.Length; i++)
            {
                var userStatEntity = userStatLinks.stats[i];
                var statEntity = userStatMetaDatas[userStatEntity].data;
                var statType = statMetaTypes[statEntity].type;
                data[index] = statType;
                index += 1;
                var statID = statIDs[statEntity].id;
                ByteUtil.SetInt(ref data, index, statID);
                index += sizeOfInt;
                if (statType == StatType.Level)
                {
                    var levelStat = levelStats[userStatEntity];
                    ByteUtil.SetInt(ref data, index, levelStat.value);
                    index += sizeOfFloat;
                    ByteUtil.SetFloat(ref data, index, levelStat.experienceGained);
                    index += sizeOfFloat;
                    ByteUtil.SetFloat(ref data, index, levelStat.experienceRequired);
                    index += sizeOfFloat;
                }
                else
                {
                    var statValue = statValues[userStatEntity].value;
                    ByteUtil.SetFloat(ref data, index, statValue);
                    index += sizeOfFloat;
                    if (statType == StatType.State)
                    {
                        var statValueMax = statValueMaxs[userStatEntity].value;
                        ByteUtil.SetFloat(ref data, index, statValueMax);
                        index += sizeOfFloat;
                    }
                    else if (statType == StatType.Attribute)
                    {
                        var attributeStat = attributeStats[userStatEntity];
                        ByteUtil.SetFloat(ref data, index, attributeStat.previousAdded);
                        index += sizeOfFloat;
                    }
                }
            }
        }
    }
}
                /*if (statType == StatType.Level)
                {
                    length += 1 + sizeOfInt + sizeOfFloat * 3;
                }
                else if (statType == StatType.Base)
                {
                    length += 1 + sizeOfInt + sizeOfFloat;
                }
                else if (statType == StatType.State)
                {
                    length += 1 + sizeOfInt + sizeOfFloat * 2;
                }
                else if (statType == StatType.Regen)
                {
                    length += 1 + sizeOfInt + sizeOfFloat;
                }
                else if (statType == StatType.Attribute)
                {
                    length += 1 + sizeOfInt + sizeOfFloat * 2;
                }*/
                
            // var statsLength = GetFileSize(in userStatLinks);
            // Size of the files
            /*data[0] = (byte) userStatLinks.stats.Length;
            data[1] = (byte) userStatLinks.states.Length;
            data[2] = (byte) userStatLinks.regens.Length;
            data[3] = (byte) userStatLinks.attributes.Length;
            data[4] = (byte) userStatLinks.levels.Length;*/
            /*for (int i = 0; i < userStatLinks.states.Length; i++)
            {
                var statEntity = userStatLinks.states[i].stat;
                if (!statIDs.HasComponent(statEntity))
                {
                    Dispose();
                    return;
                }
                ByteUtil.SetInt(ref data, index, statIDs[statEntity].id);
                index += sizeOfInt;
                ByteUtil.SetFloat(ref data, index, userStatLinks.states[i].value);
                index += sizeOfFloat;
                ByteUtil.SetFloat(ref data, index, userStatLinks.states[i].maxValue);
                index += sizeOfFloat;
            }
            for (int i = 0; i < userStatLinks.regens.Length; i++)
            {
                ByteUtil.SetInt(ref data, index, statIDs[userStatLinks.regens[i].stat].id);
                index += sizeOfInt;
                ByteUtil.SetInt(ref data, index, statIDs[userStatLinks.regens[i].targetStat].id);
                index += sizeOfFloat;
                ByteUtil.SetFloat(ref data, index, userStatLinks.regens[i].value);
                index += sizeOfFloat;
            }
            for (int i = 0; i < userStatLinks.attributes.Length; i++)
            {
                ByteUtil.SetInt(ref data, index, statIDs[userStatLinks.attributes[i].stat].id);
                index += sizeOfInt;
                ByteUtil.SetInt(ref data, index, statIDs[userStatLinks.attributes[i].targetStat].id);
                index += sizeOfFloat;
                ByteUtil.SetFloat(ref data, index, userStatLinks.attributes[i].value);
                index += sizeOfFloat;
                ByteUtil.SetFloat(ref data, index, userStatLinks.attributes[i].previousAdded);
                index += sizeOfFloat;
            }
            for (int i = 0; i < userStatLinks.levels.Length; i++)
            {
                var statEntity = userStatLinks.levels[i].stat;
                if (!statIDs.HasComponent(statEntity))
                {
                    // UnityEngine.Debug.LogError("LevelStat Stat Broken: " + i);
                    ByteUtil.SetInt(ref data, index, 0);
                }
                else
                {
                    ByteUtil.SetInt(ref data, index, statIDs[statEntity].id);
                }
                index += sizeOfInt;
                ByteUtil.SetInt(ref data, index, userStatLinks.levels[i].value);
                index += sizeOfFloat;
                ByteUtil.SetFloat(ref data, index, userStatLinks.levels[i].experienceGained);
                index += sizeOfFloat;
                ByteUtil.SetFloat(ref data, index, userStatLinks.levels[i].experienceRequired);
                index += sizeOfFloat;
            }*/


            // test
            // UnityEngine.Debug.LogError("Testing Integer: " + statIDs[userStatLinks.stats[i].stat].id + " to " + GetInt(in data, index));
            // test
            //UnityEngine.Debug.LogError("Testing Integer: " + userStatLinks.stats[i].value + " to " + GetFloat(in data, index));

                /*if (userStatLinks.states[i].maxValue == 6 && userStatLinks.states[i].value != userStatLinks.states[i].maxValue)
                {
                    UnityEngine.Debug.LogError("Saving Slime Health: " + userStatLinks.states[i].value);
                }*/
                /*if (i == 3)
                {
                    UnityEngine.Debug.LogError("Saved Stat: " + userStatLinks.stats[i].id);
                    UnityEngine.Debug.LogError("Saved Stat Value: " + userStatLinks.stats[i].value);
                }*/