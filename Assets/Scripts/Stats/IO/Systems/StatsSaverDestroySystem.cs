using Unity.Entities;

namespace Zoxel.Stats
{
    //! Handles disposing StatsSaverData after character is destroyed.
    [UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class StatsSaverDataDestroySystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, StatsSaverData>()
                .ForEach((in StatsSaverData statsSaverData) =>
            {
                statsSaverData.Dispose();
            }).WithoutBurst().Run();
        }
    }
}