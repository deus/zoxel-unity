using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Stats
{
    //! Caches userStatLinks into a format to save to file.
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class StatsSaverCacheSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery statsQuery;
        private EntityQuery userStatsQuery;
        private EntityQuery processQuery2;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<GenerateStats>(),
                ComponentType.ReadWrite<StatsSaverData>(),
                ComponentType.ReadOnly<UserStatLinks>());
            // processQuery.SetChangedVersionFilter(typeof(Stats));
            statsQuery = GetEntityQuery(
                ComponentType.ReadOnly<RealmStat>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<DestroyEntity>());
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.Exclude<DestroyEntity>());
            processQuery2 = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.ReadOnly<DisableSaving>(),
                ComponentType.ReadOnly<SaveStats>(),
                ComponentType.ReadOnly<UserStatLinks>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (!processQuery2.IsEmpty)
            {
                SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<SaveStats>(processQuery2);
            }
            if (processQuery.IsEmpty)
            {
                return;
            }
            var elapsedTime = World.Time.ElapsedTime;
            var statEntities = statsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var statIDs = GetComponentLookup<ZoxID>(true);
            var statMetaTypes = GetComponentLookup<StatMetaType>(true);
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var userStatMetaDatas = GetComponentLookup<MetaData>(true);
            var statValues = GetComponentLookup<StatValue>(true);
            var statValueMaxs = GetComponentLookup<StatValueMax>(true);
            var attributeStats = GetComponentLookup<AttributeStat>(true);
            var levelStats = GetComponentLookup<LevelStat>(true);
            statEntities.Dispose();
            userStatEntities.Dispose();
            Dependency = Entities
                .WithNone<ApplyAttributes>()
                .WithNone<InitializeEntity, GenerateStats>()
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<LoadStats>()
                .WithNone<InitializeSaver, DisableSaving>()
                .WithAll<Regening>()
                .ForEach((ref StatsSaverData statsSaverData, in StatsSaverTimer statsSaverTimer, in UserStatLinks userStatLinks) =>
            {
                if (elapsedTime - statsSaverTimer.lastSavedStatsTime >= 1)
                {
                    statsSaverData.SetStats(in userStatLinks, in statIDs, in statMetaTypes, in userStatMetaDatas, in statValues, in statValueMaxs, in attributeStats, in levelStats);
                }
            })  .WithReadOnly(statIDs)
                .WithReadOnly(statMetaTypes)
                .WithReadOnly(userStatMetaDatas)
                .WithReadOnly(statValues)
                .WithReadOnly(statValueMaxs)
                .WithReadOnly(attributeStats)
                .WithReadOnly(levelStats)
                .ScheduleParallel(Dependency);
            Dependency = Entities
                .WithNone<Regening, ApplyAttributes>()
                .WithNone<InitializeEntity, GenerateStats>()
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<LoadStats>()
                .WithNone<InitializeSaver, DisableSaving>()
                .WithAll<SaveStats>()
                .ForEach((ref StatsSaverData statsSaverData, in UserStatLinks userStatLinks) =>
            {
                statsSaverData.SetStats(in userStatLinks, in statIDs, in statMetaTypes, in userStatMetaDatas, in statValues, in statValueMaxs, in attributeStats, in levelStats);
            })  .WithReadOnly(statIDs)
                .WithReadOnly(statMetaTypes)
                .WithReadOnly(userStatMetaDatas)
                .WithReadOnly(statValues)
                .WithReadOnly(statValueMaxs)
                .WithReadOnly(attributeStats)
                .WithReadOnly(levelStats)
                .ScheduleParallel(Dependency);
        }
    }
}