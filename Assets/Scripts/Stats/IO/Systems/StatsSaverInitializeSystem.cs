/*using Unity.Entities;

namespace Zoxel.Stats
{
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class StatsSaverInitializeSystem : SystemBase
    {
        private const string filename = "Stats.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<DestroyEntity, LoadStats>()
                .WithNone<InitializeSaver, CharacterMakerCharacter, EntityBusy>()
                .WithAll<InitializeStatsSaver, StatsSaver>()
                .ForEach((Entity e, in EntitySaver entitySaver, in UserStatLinks userStatLinks) =>
            {
                PostUpdateCommands.RemoveComponent<InitializeStatsSaver>(e);
                var filepath = entitySaver.GetPath() + filename;
                var statsSaver = new StatsSaver();
                if (statsSaver.CreateStream(filepath))
                {
                    PostUpdateCommands.SetSharedComponentManaged(e, statsSaver);
                }
                else
                {
                    PostUpdateCommands.RemoveComponent<StatsSaver>(e);
                    // UnityEngine.Debug.LogError("    - Exception Opening StatsSaver Stream [" + filepath + "] " + e.Index);
                }
            }).WithoutBurst().Run();
        }
    }
}*/