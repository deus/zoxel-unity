using Unity.Entities;
using System.IO;

namespace Zoxel.Stats
{
    //! Saves character userStatLinks to a file.
    [UpdateAfter(typeof(StatsSaverCacheSystem))]
    [UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class StatsSaveSystem : SystemBase
    {
        const string fileName = "Stats.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            // save character when regening
            Entities
                .WithNone<ApplyAttributes>()
                .WithNone<InitializeEntity, GenerateStats>()
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<LoadStats>()
                .WithNone<InitializeSaver, DisableSaving>()
                .WithAll<Regening>()
                .ForEach((Entity e, ref StatsSaverTimer statsSaverTimer, in StatsSaverData statsSaverData, in EntitySaver entitySaver) =>
            {
                if (elapsedTime - statsSaverTimer.lastSavedStatsTime >= 1)
                {
                    statsSaverTimer.lastSavedStatsTime = elapsedTime;
                    if (statsSaverData.data.Length > 0)
                    {
                        // statsSaver.WriteToFile(in statsSaverData);
                        var filepath = entitySaver.GetPath() + fileName;
                        var bytesArray = statsSaverData.data.ToArray();
                        #if WRITE_ASYNC
                        File.WriteAllBytesAsync(filepath, bytesArray);
                        #else
                        File.WriteAllBytes(filepath, bytesArray);
                        #endif
                    }
                    /*else
                    {
                        UnityEngine.Debug.LogError("Stats data 0");
                    }*/
                }
            }).WithoutBurst().Run();
            // either save userStatLinks is added or regening
            Entities
                .WithNone<Regening, ApplyAttributes>()
                .WithNone<InitializeEntity, GenerateStats>()
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<LoadStats>()
                .WithNone<InitializeSaver, DisableSaving>()
                .WithAll<SaveStats>()
                .ForEach((Entity e, in StatsSaverData statsSaverData, in EntitySaver entitySaver) =>
            {
                PostUpdateCommands.RemoveComponent<SaveStats>(e);
                if (statsSaverData.data.Length > 0)
                {
                    // statsSaver.WriteToFile(in statsSaverData);
                    var filepath = entitySaver.GetPath() + fileName;
                    var bytesArray = statsSaverData.data.ToArray();
                    #if WRITE_ASYNC
                    File.WriteAllBytesAsync(filepath, bytesArray);
                    #else
                    File.WriteAllBytes(filepath, bytesArray);
                    #endif
                }
                /*else
                {
                    UnityEngine.Debug.LogError("Stats data 0");
                }*/
            }).WithoutBurst().Run();
        }
    }
}