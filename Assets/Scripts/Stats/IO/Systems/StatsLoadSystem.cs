using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using System;
using System.IO;

namespace Zoxel.Stats
{
    //! Load character userStatLinks from a file.
    /**
    *   Loads a byte before every stat to know what type it is.
    *   - Load System -
    */
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class StatsLoadSystem : SystemBase
    {
        private const string filename = "Stats.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmsQuery;
        private EntityQuery statsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<StatLinks>(),
                ComponentType.Exclude<DestroyEntity>());
            statsQuery = GetEntityQuery(
                ComponentType.ReadOnly<RealmStat>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(realmsQuery);
            RequireForUpdate(statsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithAll<UserStatLinks, RealmLink>()
                .ForEach((ref LoadStats loadStats, in EntitySaver entitySaver) =>
            {
                if (loadStats.loadPath.Length == 0)
                {
                    loadStats.loadPath = new Text(entitySaver.GetPath() + filename);
                }
                loadStats.reader.UpdateOnMainThread(in loadStats.loadPath);
            }).WithoutBurst().Run();
            var userStatPrefabs = StatsSystemGroup.userStatPrefabs;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var realmEntities = realmsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var realmStatLinks = GetComponentLookup<StatLinks>(true);
            realmEntities.Dispose();
            var statEntities = statsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var statEffectTargets = GetComponentLookup<StatEffectTarget>(true);
            var soulStats = GetComponentLookup<SoulStat>(true);
            var healthStats = GetComponentLookup<HealthStat>(true);
            var skillPoints = GetComponentLookup<SkillPoint>(true);
            var statPoints = GetComponentLookup<StatPoint>(true);
            statEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithAll<UserStatLinks, EntitySaver>()
                .ForEach((Entity e, int entityInQueryIndex, ref LoadStats loadStats, in RealmLink realmLink) =>
            {
                if (loadStats.reader.IsReadFileInfoComplete())
                {
                    if (loadStats.reader.DoesFileExist())
                    {
                        loadStats.reader.state = AsyncReadState.StartOpenFile;
                    }
                    else
                    {
                        // UnityEngine.Debug.LogError("File Did not exist.");
                        loadStats.Dispose();
                        PostUpdateCommands.RemoveComponent<LoadStats>(entityInQueryIndex, e);
                    }
                }
                else if (loadStats.reader.IsReadFileComplete())
                {
                    if (loadStats.reader.DidFileReadSuccessfully())
                    {
                        var realmStatLinks2 = realmStatLinks[realmLink.realm];
                        bool anyStateStatsNonMax;
                        NativeList<Entity> userStatEntities;
                        NativeList<Entity> statEntities2;
                        SpawnUserStats(in loadStats, PostUpdateCommands, entityInQueryIndex, e, userStatPrefabs, in realmStatLinks2, in statEffectTargets, out anyStateStatsNonMax,
                            out userStatEntities, out statEntities2);
                        for (int i = 0; i < statEntities2.Length; i++)
                        {
                            var userStatEntity = userStatEntities[i];
                            var statEntity = statEntities2[i];
                            if (soulStats.HasComponent(statEntity))
                            {
                                PostUpdateCommands.AddComponent<SoulStat>(entityInQueryIndex, userStatEntity);
                            }
                            else if (healthStats.HasComponent(statEntity))
                            {
                                PostUpdateCommands.AddComponent<HealthStat>(entityInQueryIndex, userStatEntity);
                            }
                            else if (skillPoints.HasComponent(statEntity))
                            {
                                PostUpdateCommands.AddComponent<SkillPoint>(entityInQueryIndex, userStatEntity);
                            }
                            else if (statPoints.HasComponent(statEntity))
                            {
                                PostUpdateCommands.AddComponent<StatPoint>(entityInQueryIndex, userStatEntity);
                            }
                        }
                        // loadStats.Dispose();
                        if (anyStateStatsNonMax)
                        {
                            // UnityEngine.Debug.LogError("Entity Loaded with non Max State Stat: " + HasComponent<PlayerCharacter>(e));
                            PostUpdateCommands.AddComponent<Regening>(entityInQueryIndex, e);
                            if (HasComponent<NPCCharacter>(e))
                            {
                                PostUpdateCommands.AddComponent<SpawnStatbar>(entityInQueryIndex, e);
                            }
                        }
                        userStatEntities.Dispose();
                        statEntities2.Dispose();
                    }
                    // Dispose
                    loadStats.Dispose();
                    PostUpdateCommands.RemoveComponent<LoadStats>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(realmStatLinks)
                .WithReadOnly(statEffectTargets)
                .WithReadOnly(soulStats)
                .WithReadOnly(healthStats)
                .WithReadOnly(skillPoints)
                .WithReadOnly(statPoints)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static void SpawnUserStats(in LoadStats loadStats, EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e, UserStatPrefabGroup userStatPrefabs,
            in StatLinks realmStatLinks, in ComponentLookup<StatEffectTarget> statEffectTargets, out bool anyStateStatsNonMax,
            out NativeList<Entity> userStatEntities2, out NativeList<Entity> statEntities2)
        {
            anyStateStatsNonMax = false;
            var userStatEntities = new NativeList<Entity>();
            var statEntities = new NativeList<Entity>();
            var targetStatEntities = new NativeList<Entity>();
            var userStatsCount = (byte) 0;
            var byteIndex = (int) 0;
            var bytes = loadStats.reader.GetBytes(); // bytes;
            while (byteIndex < bytes.Length)
            {
                var statType = bytes[byteIndex];
                var length = StatsSaverData.statTypeByteSizes[statType];
                var statID = ByteUtil.GetInt(in bytes, byteIndex + 1);
                var statEntity = realmStatLinks.GetStat(statID);
                var userStatEntity = new Entity();
                var targetStatEntity = new Entity();
                if (statType == StatType.Level)
                {
                    var statValue = ByteUtil.GetInt(in bytes, byteIndex + 5);
                    var experienceGained = ByteUtil.GetFloat(in bytes, byteIndex + 9);
                    var experienceRequired = ByteUtil.GetFloat(in bytes, byteIndex + 13);
                    userStatEntity = StatsSystemGroup.AddUserStatLevel(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.levelUserStatPrefab, userStatsCount, e, statEntity, statValue, experienceGained, experienceRequired);
                }
                else
                {
                    var statValue = ByteUtil.GetFloat(in bytes, byteIndex + 5);
                    if (statType == StatType.Base)
                    {
                        userStatEntity = StatsSystemGroup.AddUserStatBase(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.baseUserStatPrefab, userStatsCount, e, statEntity, statValue);
                    }
                    else if (statType == StatType.State)
                    {
                        var statMaxValue = ByteUtil.GetFloat(in bytes, byteIndex + 9);
                        if (!anyStateStatsNonMax)
                        {
                            anyStateStatsNonMax = statValue != statMaxValue;
                        }
                        userStatEntity = StatsSystemGroup.AddUserStatState(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.stateUserStatPrefab, userStatsCount, e, statEntity, statValue, statMaxValue);
                    }
                    else if (statType == StatType.Regen)
                    {
                        if (statEffectTargets.HasComponent(statEntity))
                        {
                            targetStatEntity = statEffectTargets[statEntity].target;
                        }
                        userStatEntity = StatsSystemGroup.AddUserStatRegen(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.regenUserStatPrefab, userStatsCount, e, statEntity, targetStatEntity, statValue, false);
                    }
                    else if (statType == StatType.Attribute)
                    {
                        if (statEffectTargets.HasComponent(statEntity))
                        {
                            targetStatEntity = statEffectTargets[statEntity].target;
                        }
                        var previousAdded = ByteUtil.GetFloat(in bytes, byteIndex + 9);
                        userStatEntity = StatsSystemGroup.AddUserStatAttribute(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.attributeUserStatPrefab, userStatsCount, e, statEntity, targetStatEntity, statValue,
                            previousAdded, false);
                    }
                }
                userStatsCount++;
                byteIndex += length;
                // Used to fix stat targets
                userStatEntities.Add(userStatEntity);
                statEntities.Add(statEntity);
                targetStatEntities.Add(targetStatEntity);
            }
            //! Set all proper effect targets after they spawn in list
            for (int i = 0; i < targetStatEntities.Length; i++)
            {
                var targetStatEntity = targetStatEntities[i];
                if (targetStatEntity.Index != 0)
                {
                    // find statEntity
                    //var didFind = false;
                    var userStatEntity = userStatEntities[i];
                    for (byte j = 0; j < statEntities.Length; j++)
                    {
                        var statEntity = statEntities[j];
                        if (targetStatEntity == statEntity)
                        {
                            var targetUserStatEntity = userStatEntities[j];
                            PostUpdateCommands.SetComponent(entityInQueryIndex, userStatEntity, new StatEffectTarget(targetUserStatEntity));
                            //didFind = true;
                            break;
                        }
                    }
                    /*if (!didFind)
                    {
                        // UnityEngine.Debug.LogError("Could not find stat target for: " + i);
                        PostUpdateCommands.AddComponent<StatEffectTarget>(entityInQueryIndex, userStatEntity);
                    }*/
                }
            }
            var userStatEntitiesArray = userStatEntities.AsArray();
            PostUpdateCommands.AddComponent(entityInQueryIndex, userStatEntitiesArray, new CreatorLink(e));
            userStatEntitiesArray.Dispose();
            targetStatEntities.Dispose();
            userStatEntities2 = userStatEntities;
            statEntities2 = statEntities;
            PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUserStatsSpawned(userStatsCount));
            // UnityEngine.Debug.LogError("Spawned User Stats of size: " + userStatsCount);
        }
    }
}

/*public static bool ReadFromFile(string filePath, ref LoadStats loadStats)
{
    if (!File.Exists(filePath))
    {
        UnityEngine.Debug.LogWarning("Exception reading userStatLinks: File does not exist: " + filePath);
        return false;
    }
    BinaryReader reader;
    try
    {
        reader = new BinaryReader(File.OpenRead(filePath));
        // UnityEngine.Debug.LogError("Read userStatLinks from: " + filePath + " of size: " + reader.BaseStream.Length);
    }
    catch (Exception e)
    {
        UnityEngine.Debug.LogWarning("Exception reading userStatLinks: " + e + " : " + filePath);
        return false;
    }
    reader.BaseStream.Position = 0;
    var bytes = reader.ReadBytes((int) reader.BaseStream.Length);
    reader.Close();
    reader.Dispose();
    loadStats.bytes = new BlitableArray<byte>(bytes, Allocator.Persistent);
    return true;
}*/