﻿using Unity.Entities;
using Zoxel.Textures;

namespace Zoxel.Stats
{
    public struct UserStatPrefabGroup
    {
        public Entity baseUserStatPrefab;
        public Entity stateUserStatPrefab;
        public Entity regenUserStatPrefab;
        public Entity attributeUserStatPrefab;
        public Entity levelUserStatPrefab;
    }

    //! Manages all stat system groups.
    [AlwaysUpdateSystem]
    public partial class StatsSystemGroup : ComponentSystemGroup
    {
        public static Entity damagePrefab;
        public static Entity updateStatPrefab;
        public static int statsCount;
		private EntityQuery statsQuery;
        public static int userStatsCount;
		private EntityQuery userStatsQuery;
        public static Entity statPrefab;
        public static UserStatPrefabGroup userStatPrefabs;
        
        protected override void OnCreate()
        {
            base.OnCreate();
            var damageArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(GenericEvent),
                typeof(ApplyDamage)
            );
            damagePrefab = EntityManager.CreateEntity(damageArchetype);
            var updateArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(GenericEvent),
                typeof(UpdateEntityStat)
            );
            updateStatPrefab = EntityManager.CreateEntity(updateArchetype);
            var statArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(RealmStat),
                typeof(StatMetaType),
                typeof(ZoxID),
                typeof(ZoxName),
                typeof(ZoxDescription),
                // typeof(Stat),
                typeof(Texture)
            );
            statPrefab = EntityManager.CreateEntity(statArchetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(statPrefab); // EntityManager.AddComponentData(statPrefab, new EditorName("[stat]"));
            #endif
            var userStatArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(NewUserStat),
                typeof(UserStat),
                typeof(MetaData),
                typeof(CreatorLink),
                typeof(UserDataIndex)
            );
            // userStatPrefab = EntityManager.CreateEntity(userStatArchetype);
            var userStatPrefabs = StatsSystemGroup.userStatPrefabs;
            userStatPrefabs.baseUserStatPrefab = EntityManager.CreateEntity(userStatArchetype);
            EntityManager.AddComponent<BaseStat>(userStatPrefabs.baseUserStatPrefab);
            EntityManager.AddComponent<StatValue>(userStatPrefabs.baseUserStatPrefab);
            userStatPrefabs.stateUserStatPrefab = EntityManager.CreateEntity(userStatArchetype);
            EntityManager.AddComponent<StateStat>(userStatPrefabs.stateUserStatPrefab);
            EntityManager.AddComponent<StatValue>(userStatPrefabs.stateUserStatPrefab);
            EntityManager.AddComponent<StatValueMax>(userStatPrefabs.stateUserStatPrefab);
            userStatPrefabs.regenUserStatPrefab = EntityManager.CreateEntity(userStatArchetype);
            EntityManager.AddComponent<RegenStat>(userStatPrefabs.regenUserStatPrefab);
            EntityManager.AddComponent<StatValue>(userStatPrefabs.regenUserStatPrefab);
            EntityManager.AddComponent<StatEffectTarget>(userStatPrefabs.regenUserStatPrefab);
            userStatPrefabs.attributeUserStatPrefab = EntityManager.CreateEntity(userStatArchetype);
            EntityManager.AddComponent<AttributeStat>(userStatPrefabs.attributeUserStatPrefab);
            EntityManager.AddComponent<StatValue>(userStatPrefabs.attributeUserStatPrefab);
            EntityManager.AddComponent<StatEffectTarget>(userStatPrefabs.attributeUserStatPrefab);
            userStatPrefabs.levelUserStatPrefab = EntityManager.CreateEntity(userStatArchetype);
            EntityManager.AddComponent<LevelStat>(userStatPrefabs.levelUserStatPrefab);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(userStatPrefabs.baseUserStatPrefab);
            EntityManager.AddComponent<EditorName>(userStatPrefabs.stateUserStatPrefab);
            EntityManager.AddComponent<EditorName>(userStatPrefabs.regenUserStatPrefab);
            EntityManager.AddComponent<EditorName>(userStatPrefabs.attributeUserStatPrefab);
            EntityManager.AddComponent<EditorName>(userStatPrefabs.levelUserStatPrefab);
            #endif
            StatsSystemGroup.userStatPrefabs = userStatPrefabs;
			statsQuery = GetEntityQuery(ComponentType.ReadOnly<RealmStat>());
			userStatsQuery = GetEntityQuery(ComponentType.ReadOnly<UserStat>());
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();
            statsCount = statsQuery.CalculateEntityCount();
            userStatsCount = userStatsQuery.CalculateEntityCount();
        }

        public static Entity AddUserStatBase(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab, byte statIndex, Entity e, Entity metaData, float value)
        {
            var e2 = AddUserStat(PostUpdateCommands, entityInQueryIndex, prefab, statIndex, e, metaData);
            //PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new UserStatType(StatType.Base));
            // PostUpdateCommands.AddComponent<BaseStat>(entityInQueryIndex, e2);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new StatValue(value));
            return e2;
        }

        public static Entity AddUserStatState(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab, byte statIndex, Entity e, Entity metaData, float value)
        {
            var e2 = AddUserStat(PostUpdateCommands, entityInQueryIndex, prefab, statIndex, e, metaData);
            //PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new UserStatType(StatType.State));
            // PostUpdateCommands.AddComponent<StateStat>(entityInQueryIndex, e2);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new StatValue(value));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new StatValueMax(value));
            return e2;
        }

        public static Entity AddUserStatState(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab, byte statIndex, Entity e, Entity metaData, float value, float maxValue)
        {
            var e2 = AddUserStat(PostUpdateCommands, entityInQueryIndex, prefab, statIndex, e, metaData);
            //PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new UserStatType(StatType.State));
            // PostUpdateCommands.SetComponent<StateStat>(entityInQueryIndex, e2);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new StatValue(value));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new StatValueMax(maxValue));
            return e2;
        }

        public static Entity AddUserStatRegen(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab, byte statIndex, Entity e, Entity metaData,
            Entity target, float value, bool isAddTarget = true)
        {
            var e2 = AddUserStat(PostUpdateCommands, entityInQueryIndex, prefab, statIndex, e, metaData);
            //PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new UserStatType(StatType.Regen));
            // PostUpdateCommands.AddComponent<RegenStat>(entityInQueryIndex, e2);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new StatValue(value));
            if (isAddTarget)
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new StatEffectTarget(target));
            }
            return e2;
        }

        public static Entity AddUserStatAttribute(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab, byte statIndex, Entity e, Entity metaData,
            Entity target, float value)
        {
            var e2 = AddUserStat(PostUpdateCommands, entityInQueryIndex, prefab, statIndex, e, metaData);
            //PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new UserStatType(StatType.Attribute));
            // PostUpdateCommands.AddComponent<AttributeStat>(entityInQueryIndex, e2);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new StatValue(value));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new StatEffectTarget(target));
            return e2;
        }

        public static Entity AddUserStatAttribute(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab, byte statIndex, Entity e, Entity metaData,
            Entity target, float value, float previousAdded, bool isAddTarget = true)
        {
            var e2 = AddUserStat(PostUpdateCommands, entityInQueryIndex, prefab, statIndex, e, metaData);
            //PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new UserStatType(StatType.Attribute));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new AttributeStat(previousAdded));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new StatValue(value));
            if (isAddTarget)
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new StatEffectTarget(target));
            }
            return e2;
        }

        public static Entity AddUserStatLevel(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab, byte statIndex, Entity e, Entity metaData,
            int value, float experienceGained, float experienceRequired)
        {
            var e2 = AddUserStat(PostUpdateCommands, entityInQueryIndex, prefab, statIndex, e, metaData);
            //PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new UserStatType(StatType.Level));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new LevelStat(value, experienceGained, experienceRequired));
            return e2;
        }

        private static Entity AddUserStat(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab, byte statIndex, Entity e, Entity metaData)
        {
            var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
            // Will set creator link in thingo
            // PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new CreatorLink(e));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new UserDataIndex(statIndex));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new MetaData(metaData));
            return e2;
        }
    }
}