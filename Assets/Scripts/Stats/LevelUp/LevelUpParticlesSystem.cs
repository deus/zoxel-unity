using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;    // particle spawning
using Zoxel.Transforms;  
using Zoxel.Particles;

namespace Zoxel.Stats
{
    //! Adds particles to the character when they levels up.
    /**
    *   - Particles System -
    */
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class LevelUpParticlesSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .WithAll<LevelUp>()
                .ForEach((Entity e, int entityInQueryIndex, in ZoxID zoxID, in Body body, in Translation translation, in Rotation rotation) =>
            {
                var seed = zoxID.id + (int)(256 * elapsedTime);
                var random = new Random();
                random.InitState((uint) seed);
                var maxBodyDimension = math.max(body.size.x, body.size.z);
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ParticleSystem
                {
                    random = random,
                    lifeTime = 4,
                    timeSpawn = 0.02,
                    timeBegun = elapsedTime,
                    baseColor = new Color(200, 200, 25, 120),
                    colorVariance = new float3(0.15f, 0.15f, 0.1f), 
                    spawnRate = 14,
                    particleLife = 3,
                    spawnSize = new float3(maxBodyDimension, body.size.y, maxBodyDimension), // math.mul(rotation.Value, new float3(0.35f, 1.2f, 0.35f)),
                    positionAdd = math.mul(rotation.Value, new float3(0, 1.2f, 0))
                });
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}