using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;    // particle spawning
using Zoxel.Transforms;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;

namespace Zoxel.Stats
{
    //! Plays a sound upon leveling up.
    /**
    *   - Sound System -
    */
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class LevelUpSoundSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<SoundSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var soundSettings = GetSingleton<SoundSettings>();
            var sampleRate = soundSettings.sampleRate;
            var levelUpSoundVolume = soundSettings.levelUpSoundVolume;    // 0.7f
            var soundPrefab = SoundPlaySystem.soundPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .WithAll<LevelUp>()
                .ForEach((int entityInQueryIndex, in Translation translation, in ZoxID zoxID) =>
            {
                var seed = zoxID.id + (int)(256 * elapsedTime);
                var random = new Random();
                random.InitState((uint) seed);
                var soundEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, soundPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Seed(seed));
                var generateSound = new GenerateSound();
                generateSound.CreateMusicSound((byte)(40 + random.NextInt(6)), (byte) InstrumentType.EDM, sampleRate);
                generateSound.timeLength = 4;
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new SoundData(ref random, generateSound.timeLength, generateSound.sampleRate, 1, levelUpSoundVolume));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new DelayEvent(elapsedTime, 0.05));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Translation { Value = translation.Value });
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}