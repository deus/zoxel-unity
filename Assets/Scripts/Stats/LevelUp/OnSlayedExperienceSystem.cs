using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.UI.Animations;

namespace Zoxel.Stats
{
    //! Adds experience to an entity after slaying another.
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class OnSlayedExperienceSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery aliveCharactersQuery;
        private EntityQuery deadCharactersQuery;
        private EntityQuery userStatsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            aliveCharactersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Character>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DyingEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            deadCharactersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Character>(),
                ComponentType.ReadOnly<DyingEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.ReadOnly<LevelStat>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(aliveCharactersQuery);
            RequireForUpdate(deadCharactersQuery);
            RequireForUpdate(userStatsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var spawnPopupPrefab = TextPopupSystem.spawnPopupPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var elapsedTime = World.Time.ElapsedTime;
            var aliveEntities = aliveCharactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var aliveStats = GetComponentLookup<UserStatLinks>(true);
            var aliveIDs = GetComponentLookup<ZoxID>(true);
            var aliveGravityQuadrants = GetComponentLookup<GravityQuadrant>(true);
            aliveEntities.Dispose();
            var deadEntities = deadCharactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var deadStats = GetComponentLookup<UserStatLinks>(true);
            var positions = GetComponentLookup<Translation>(true);
            deadEntities.Dispose();
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var levelStats = GetComponentLookup<LevelStat>(true);
            userStatEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((int entityInQueryIndex, in OnKilledEntity onKilledEntity) =>
            {
                var killerEntity = onKilledEntity.killer;
                var enemyEntity = onKilledEntity.enemy;
                // if killer dies in same frame
                if (!aliveStats.HasComponent(killerEntity) || !deadStats.HasComponent(enemyEntity))
                {
                    return;
                }
                var zoxID = aliveIDs[killerEntity];
                var gravityQuadrant = aliveGravityQuadrants[killerEntity];

                var defenderLevel = -1; // deadStats[enemyEntity].levels[0].value;
                var defenderStats = deadStats[enemyEntity];
                for (int i = 0; i < defenderStats.stats.Length; i++)
                {
                    var userStatEntity = defenderStats.stats[i];
                    if (HasComponent<SoulStat>(userStatEntity))
                    {
                        defenderLevel = levelStats[userStatEntity].value;
                    }
                }
                if (defenderLevel == -1)
                {
                    return;
                }

                var attackerLevel = -1; // deadStats[enemyEntity].levels[0].value;
                var attackerStats = aliveStats[killerEntity];
                for (int i = 0; i < attackerStats.stats.Length; i++)
                {
                    var userStatEntity = attackerStats.stats[i];
                    if (HasComponent<SoulStat>(userStatEntity))
                    {
                        attackerLevel = levelStats[userStatEntity].value;
                    }
                }
                if (attackerLevel == -1)
                {
                    return;
                }
                var levelDifference = attackerLevel - defenderLevel;

                var enemyPosition = positions[enemyEntity].Value;
                var random = new Random();
                random.InitState((uint)(zoxID.id + elapsedTime * 256));
                // var attackerLevel = userStatLinks.levels[0].value;
                float experienceGained = (defenderLevel + 1) * random.NextFloat(0.5f, 1.5f);
                if (levelDifference > 0)
                {
                    experienceGained -= levelDifference * 0.25f;
                }
                if (experienceGained > 0)
                {
                    // Saving now happens in GainExperienceSystem
                    //! \todo GainExperience to be an event
                    PostUpdateCommands.AddComponent(entityInQueryIndex, killerEntity, new GainExperience(experienceGained));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, spawnPopupPrefab), 
                        new SpawnTextPopup(math.ceil(experienceGained), enemyPosition, gravityQuadrant.quadrant, TextPopupType.Experience));
                }
            })  .WithReadOnly(aliveStats)
                .WithReadOnly(aliveIDs)
                .WithReadOnly(aliveGravityQuadrants)
                .WithReadOnly(deadStats)
                .WithReadOnly(positions)
                .WithReadOnly(levelStats)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}