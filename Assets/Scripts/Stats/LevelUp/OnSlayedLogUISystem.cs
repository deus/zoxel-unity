using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Races;
using Zoxel.UI;
using Zoxel.Logs;

namespace Zoxel.Stats.UI
{
    //! Adds a kill event to a player log.
    /**
    *   Added extra checks incase something doesn't load properly (races).
    */
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class OnSlayedLogUISystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private NativeArray<Text> texts;
        private EntityQuery processQuery;
        private EntityQuery aliveCharactersQuery;
        private EntityQuery deadCharactersQuery;
        private EntityQuery raceQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            texts = new NativeArray<Text>(1, Allocator.Persistent);
            texts[0] = new Text("You Slayed a ");
            aliveCharactersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DyingEntity>(),
                ComponentType.ReadOnly<Character>());
            deadCharactersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Character>(),
                ComponentType.ReadOnly<DyingEntity>());
            raceQuery = GetEntityQuery(
                ComponentType.ReadOnly<Race>(),
                ComponentType.ReadOnly<ZoxID>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var texts = this.texts;
            var logPrefab = LogSystemGroup.logPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var aliveEntities = aliveCharactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var logLinks = GetComponentLookup<LogLinks>(true);
            aliveEntities.Dispose();
            var deadEntities = deadCharactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var raceLinks = GetComponentLookup<RaceLink>(true);
            deadEntities.Dispose();
            var raceEntities = raceQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var raceNames = GetComponentLookup<ZoxName>(true);
            raceEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .ForEach((int entityInQueryIndex, in OnKilledEntity onKilledEntity) =>
            {
                var killerEntity = onKilledEntity.killer;
                if (!logLinks.HasComponent(killerEntity))
                {
                    return;
                }
                var enemyEntity = onKilledEntity.enemy;
                if (!raceLinks.HasComponent(enemyEntity))
                {
                    return;
                }
                var enemyRace = raceLinks[enemyEntity].race;
                if (!raceNames.HasComponent(enemyRace))
                {
                    return;
                }
                var raceName = raceNames[enemyRace].name;
                var logText = texts[0].Clone();
                logText.AddText(in raceName);
                PostUpdateCommands.AddComponent<OnLogsSpawned>(entityInQueryIndex, killerEntity);
                var logEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, logPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, logEntity, new LogText(logText));
                PostUpdateCommands.SetComponent(entityInQueryIndex, logEntity, new LogHolderLink(killerEntity));
            })  .WithReadOnly(raceLinks)
                .WithReadOnly(raceNames)
                .WithReadOnly(texts)
                .WithReadOnly(logLinks)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }
    }
}

// var logUILink = logLinks[controllerLink.controller];
// logUILink.AddToLogUI(PostUpdateCommands, entityInQueryIndex, ref log, elapsedTime);

// var controllerLink = aliveControllerLinks[killerEntity];
// if killer dies in same frame
/*if (!aliveControllerLinks.HasComponent(killerEntity))
{
    return;
}*/

/*var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
var logLinks = GetComponentLookup<LogUILink>(true);
controllerEntities.Dispose();*/