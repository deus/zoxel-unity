﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Stats
{
    //! Handles when a character levels up.
    /**
    *   Adds attribute and skill points.
    *   Adds SaveStats event.
    *   \todo Work with multiple types of levels, not just soul stat.
    */
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class LevelUpSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmsQuery;
        private EntityQuery userStatsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<StatLinks>());
            userStatsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserStat>());
            RequireForUpdate(processQuery);
            RequireForUpdate(realmsQuery);
            RequireForUpdate(userStatsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var skillPointsPerLevels = 1;
            // leveling up also gives a skill sometimes!
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<LevelUp>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var realmEntities = realmsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var statLinks = GetComponentLookup<StatLinks>(true);
            realmEntities.Dispose();
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var userStatMetaDatas = GetComponentLookup<MetaData>(true);
            var levelStats = GetComponentLookup<LevelStat>(false);
            var statValues = GetComponentLookup<StatValue>(false);
            var statValueMaxs = GetComponentLookup<StatValueMax>(true);
            userStatEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .WithAll<LevelUp>()
                .ForEach((Entity e, int entityInQueryIndex, in UserStatLinks userStatLinks, in RealmLink realmLink) =>
            {
                var realmStatLinks = statLinks[realmLink.realm];
                var soulEntity = new Entity();
                var skillPointEntity = new Entity();
                var statPointEntity = new Entity();
                var baseStatsCount = 0;
                for (int i = 0; i < realmStatLinks.userStatLinks.Length; i++)
                {
                    var statEntity = realmStatLinks.userStatLinks[i];
                    if (HasComponent<SoulStat>(statEntity))
                    {
                        soulEntity = statEntity;
                    }
                    else if (HasComponent<RealmBaseStat>(statEntity))
                    {
                        if (baseStatsCount == 0)
                        {
                            skillPointEntity = statEntity;
                        }
                        else if (baseStatsCount == 1)
                        {
                            statPointEntity = statEntity;
                        }
                        baseStatsCount++;
                    }
                }
                var count = 0;
                var statPointsGained = 0;
                var skillPointsGained = 0;
                //! Add levelStats
                var levelUserStatEntity = new Entity();
                for (int i = 0; i < userStatLinks.stats.Length; i++)
                {
                    var userStatEntity = userStatLinks.stats[i];
                    if (HasComponent<SoulStat>(userStatEntity))
                    {
                        levelUserStatEntity = userStatEntity;
                        break;
                    }
                }
                if (levelUserStatEntity.Index == 0)
                {
                    return;
                }
                if (!HasComponent<DisableSaving>(e))
                {
                    PostUpdateCommands.AddComponent<SaveStats>(entityInQueryIndex, e);
                }
                var level = levelStats[levelUserStatEntity];
                while (level.experienceGained >= level.experienceRequired)
                {
                    level.experienceGained -= level.experienceRequired;
                    level.experienceRequired *= 1.2f;
                    // increase SkillPoints (base stat)
                    level.value++;
                    // if level above 10, add 2 stat points, etc
                    statPointsGained += 1 + ((int)level.value / 10);
                    if (level.value % skillPointsPerLevels == 0)
                    {
                        skillPointsGained++;
                    }
                    count++;
                    if (count >= 64)
                    {
                        break;
                    }
                }
                levelStats[levelUserStatEntity] = level;

                //! Restore StateStats
                RestoreStateStatsSystem.RestoreStateStats(in userStatLinks, ref statValues, in statValueMaxs);

                //! Add stat points
                var didFind = false;
                for (int i = 0; i < userStatLinks.stats.Length; i++)
                {
                    var userStatEntity = userStatLinks.stats[i];
                    if (!HasComponent<BaseStat>(userStatEntity))
                    {
                        continue;
                    }
                    var statEntity = userStatMetaDatas[userStatEntity].data;
                    if (statEntity == statPointEntity)
                    {
                        didFind = true;
                        var statPoint = statValues[userStatEntity];
                        statPoint.value += statPointsGained;
                        statValues[userStatEntity] = statPoint;
                        break;
                    }
                }
                if (!didFind)
                {
                    return;
                }
                //! Add skill Points
                if (skillPointsGained > 0)
                {
                    for (int i = 0; i < userStatLinks.stats.Length; i++)
                    {
                        var userStatEntity = userStatLinks.stats[i];
                        if (!HasComponent<BaseStat>(userStatEntity))
                        {
                            continue;
                        }
                        var statEntity = userStatMetaDatas[userStatEntity].data;
                        if (statEntity == skillPointEntity)
                        {
                            didFind = true;
                            var statPoint = statValues[userStatEntity];
                            statPoint.value += skillPointsGained;
                            statValues[userStatEntity] = statPoint;
                            break;
                        }
                    }
                }
            })  .WithReadOnly(statLinks)
                .WithReadOnly(userStatMetaDatas)
                .WithReadOnly(statValueMaxs)
                .WithNativeDisableContainerSafetyRestriction(levelStats)
                .WithNativeDisableContainerSafetyRestriction(statValues)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

// new achievements can gain userSkillLinks (todo: that idea)
/*if (level.value == 1)
{
    // give fireball skill
    // Later on give a skill point for skill tree!
    PostUpdateCommands.AddComponent<GiveSkill>(entityInQueryIndex, e);
}*/
                /*for (int j = 0; j < userStatLinks.stats.Length; j++)
                {
                    var userStatEntity = userStatLinks.stats[j];
                    if (!HasComponent<StateStat>(userStatEntity))
                    {
                        continue;
                    }
                    var stateStat = stateStats[userStatEntity];
                    if (stateStat.value != stateStat.maxValue)
                    {
                        stateStat.value = stateStat.maxValue * 0.99f;
                        stateStats[userStatEntity] = stateStat;
                    }
                }*/