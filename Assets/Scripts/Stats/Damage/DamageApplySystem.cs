﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;          // DamageFlashEffect
using Zoxel.Movement;           // Knock Back
using Zoxel.AI;                 // OnAttacked - Threat Response
using Zoxel.UI.Animations;      // TextPopup
using Zoxel.Skills;
using Zoxel.Stats.Authoring;

namespace Zoxel.Stats
{
    //! Applies damage to Stats applyDamageEntities.
    /**
    *   \todo Optimize for battlefield. Remove event injection and just inject characters. Since going through thousands of damage events per frame would chugg.
    */
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class DamageApplySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery userStatsQuery;
        private EntityQuery damageQuery;
        private Entity onKilledEntityPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(GenericEvent),
                typeof(OnKilledEntity));
            onKilledEntityPrefab = EntityManager.CreateEntity(archetype);
			damageQuery = GetEntityQuery(ComponentType.ReadOnly<ApplyDamage>());
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.ReadOnly<StateStat>(),
                ComponentType.ReadOnly<StatValue>());
            RequireForUpdate(damageQuery);
            RequireForUpdate(userStatsQuery);
            RequireForUpdate<StatSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var statSettings = GetSingleton<StatSettings>();
            const int clearBodiesTime = 6;
            var knockBackForce = 2f;
            var elapsedTime = World.Time.ElapsedTime;
            var deltaTime = World.Time.DeltaTime;
            var spawnPopupPrefab = TextPopupSystem.spawnPopupPrefab;
            var instantKO = statSettings.instantKO;
            var onKilledEntityPrefab = this.onKilledEntityPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var applyDamageEntities = damageQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var applyDamages = GetComponentLookup<ApplyDamage>(true);
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var statValues = GetComponentLookup<StatValue>(false);
			var statValueMaxs = GetComponentLookup<StatValueMax>(true);
            userStatEntities.Dispose();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref BodyForce bodyForce, in UserStatLinks userStatLinks, in Translation position, in GravityQuadrant gravityQuadrant) =>
            {
                if (userStatLinks.stats.Length == 0)
                {
                    return; // cannot apply damage to gods
                }
                var healthUserStatEntity = new Entity();
                for (int i = 0; i < userStatLinks.stats.Length; i++)
                {
                    var userStatEntity = userStatLinks.stats[i];
                    if (HasComponent<HealthStat>(userStatEntity))
                    {
                        healthUserStatEntity = userStatEntity;
                        break;
                    }
                }
                if (healthUserStatEntity.Index == 0)
                {
                    return;
                }
                var healthUserStat = statValues[healthUserStatEntity];
                var healthUserStatMax = statValueMaxs[healthUserStatEntity];
                var wasMaxHealth = healthUserStat.value == healthUserStatMax.value;
                var killerCharacter = new Entity();
                var attacker = new Entity();
                var wasKilled = false;
                var hitForce = new float3();
                //! Calculates damage on entity by summing up all damage events.
                var totalDamage = 0f;
                for (int i = 0; i < applyDamageEntities.Length; i++)
                {
                    var e2 = applyDamageEntities[i];
                    var applyDamage = applyDamages[e2];
                    if (e == applyDamage.defender)
                    {
                        var damage = applyDamage.damage;
                        if (instantKO)
                        {
                            damage += 999999;
                        }
                        totalDamage += damage;
                        healthUserStat.value -= damage;
                        attacker = applyDamage.attacker;
                        hitForce += math.normalize(position.Value - applyDamage.attackPosition) * applyDamage.forceApplied * knockBackForce * (1 / deltaTime); // normalize and multiply
                        if (healthUserStat.value <= 0)
                        {
                            if (healthUserStat.value < 0)
                            {
                                healthUserStat.value = 0;
                            }
                            killerCharacter = applyDamage.attacker;
                            wasKilled = true;
                            break;
                        }
                    }
                }
                if (HasComponent<Shielding>(e))
                {
                    var shieldAmount = 1;
                    var spawnPopupEntity2 = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnPopupPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnPopupEntity2,
                        new SpawnTextPopup(shieldAmount, position.Value, gravityQuadrant.quadrant, TextPopupType.Shield));
                    totalDamage = math.max(0, totalDamage - shieldAmount);
                }
                if (totalDamage == 0)
                {
                    return;
                }
                // Knock Back! 
                bodyForce.acceleration += hitForce;
                // Update Health Stat!
                statValues[healthUserStatEntity] = healthUserStat;
                //  Boom!
                if (wasKilled)
                {
                    PostUpdateCommands.AddComponent<DyingEntity>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new DestroyEntityInTime(elapsedTime, clearBodiesTime));
                    // Killed Evvent
                    var onKilledEntityEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, onKilledEntityPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, onKilledEntityEntity, new OnKilledEntity(killerCharacter, e));
                }
                else
                {
                    if (wasMaxHealth)
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new StartRegening(healthUserStatEntity));
                    }
                    // todo: increase threat if an AI
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnAttacked
                    {
                        attacker = attacker,
                        attackerPosition = position.Value
                    });
                }
                // material flash effect
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new DamageFlashEffect(elapsedTime));
                // Damage Popup
                var spawnPopupEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnPopupPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnPopupEntity, 
                    new SpawnTextPopup(totalDamage, position.Value, gravityQuadrant.quadrant, TextPopupType.Damage));
                //Debug.Log("Damage done: " + damageDone + " to " + defender.Index + " - " + healthUserStat.value + " out of " + healthUserStat.maxValue);
            })  .WithReadOnly(applyDamageEntities)
                .WithReadOnly(applyDamages)
                .WithReadOnly(statValueMaxs)
                .WithDisposeOnCompletion(applyDamageEntities)
                .WithNativeDisableContainerSafetyRestriction(statValues)
			    .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}