using UnityEngine;
using System.Collections.Generic;

namespace Zoxel.Stats
{
    //! Holds authored data for userStatLinks.
    public partial class StatsManager : MonoBehaviour
    {
        public static StatsManager instance;

        [UnityEngine.SerializeField] public StatSettingsDatam StatSettings;
        Dictionary<int, StatDatam> statsMeta = null;    // obsolete

        public void Awake()
        {
            instance = this;
        }

        public StatDatam GetStat(string name)
        {
            foreach (var stat in StatSettings.stats)
            {
                if (stat.name == name)
                {
                    return stat;
                }
            }
            return null;
        }

        public Dictionary<int, StatDatam> GetStats()
        {
            if (statsMeta == null)
            {
                statsMeta = new Dictionary<int, StatDatam>();
                foreach (var stat in StatSettings.stats)
                {
                    if (stat.Value.id == 0)
                    {
                        Debug.LogError(stat.name + " has no ID.");
                        continue;
                    }
                    statsMeta.Add(stat.Value.id, stat);
                }
            }
            return statsMeta;
        }
    }
}