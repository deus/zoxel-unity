using UnityEngine;

namespace Zoxel.Stats
{
    [CreateAssetMenu(fileName = "StatSettings", menuName = "ZoxelSettings/StatSettings")]
    public partial class StatSettingsDatam : ScriptableObject
    {
        [Header("Core Stats")]
        public StatDatam soul;
        public StatDatam statPoint;
        public StatDatam skillPoint;
        public StatDatam healthDatam;
        public StatDatam energyDatam;
        public StatDatam manaDatam;
        public StatDatam[] stats;
    }
}