﻿using Zoxel.Textures;
using UnityEngine;

namespace Zoxel.Stats
{
    [CreateAssetMenu(fileName = "Stat", menuName = "Zoxel/Stat")]
    public partial class StatDatam : ScriptableObject
    {
        public StatTypeEditor type;
        public StatData Value;
        public bool isPlayerChoice;
        [MultilineAttribute(5)]
        public string description;
        public StatDatam targetStat;
        public TextureDatam texture;

        [ContextMenu("Generate ID")]
        public void GenerateID()
        {
            Value.GenerateID();
        }

        public int GetTargetStatID()
        {
            if (targetStat)
            {
                Value.targetStatID = targetStat.Value.id;
            }
            return Value.targetStatID;
        }
    }
}