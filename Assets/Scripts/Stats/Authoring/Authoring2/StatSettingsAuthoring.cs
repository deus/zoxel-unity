using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Zoxel.Stats.Authoring
{
    //! Settings for stats, including cheats.
    // [GenerateAuthoringComponent]
    public struct StatSettings : IComponentData
    {
        public float regenMultiplier;       // 0.04
        public int defaultVoxelHealth;      //  4
        public float defaultAttackSpeed;    // 0.8
        public float defaultResourceCost;   // 0.6
        public float defaultPlayerHealth;   // 10
        public float defaultPlayerEnergy;   // 10
        public float defaultPlayerMana;     // 10
        public float defaultPlayerHealthRegen;   // 4
        public float defaultPlayerEnergyRegen;   // 4
        public float defaultPlayerManaRegen;     // 4
        public bool disableStatbars;
        public bool disableRegen;
        public bool isUnlimitedStatPoints;
        public bool instantKO;
        public bool timesTenExperience;
        public bool isInfiniteRun;
    }

    public class StatSettingsAuthoring : MonoBehaviour
    {
        public float regenMultiplier;       // 0.04
        public int defaultVoxelHealth;      //  4
        public float defaultAttackSpeed;    // 0.8
        public float defaultResourceCost;   // 0.6
        public float defaultPlayerHealth;   // 10
        public float defaultPlayerEnergy;   // 10
        public float defaultPlayerMana;     // 10
        public float defaultPlayerHealthRegen;   // 4
        public float defaultPlayerEnergyRegen;   // 4
        public float defaultPlayerManaRegen;     // 4
        public bool disableStatbars;
        public bool disableRegen;
        public bool isUnlimitedStatPoints;
        public bool instantKO;
        public bool timesTenExperience;
        public bool isInfiniteRun;
    }

    public class StatSettingsAuthoringBaker : Baker<StatSettingsAuthoring>
    {
        public override void Bake(StatSettingsAuthoring authoring)
        {
            AddComponent(new StatSettings
            {
                regenMultiplier = authoring.regenMultiplier,
                defaultVoxelHealth = authoring.defaultVoxelHealth,
                defaultAttackSpeed = authoring.defaultAttackSpeed,
                defaultResourceCost = authoring.defaultResourceCost,
                defaultPlayerHealth = authoring.defaultPlayerHealth,
                defaultPlayerEnergy = authoring.defaultPlayerEnergy,
                defaultPlayerMana = authoring.defaultPlayerMana,
                defaultPlayerHealthRegen = authoring.defaultPlayerHealthRegen,
                defaultPlayerEnergyRegen = authoring.defaultPlayerEnergyRegen,
                defaultPlayerManaRegen = authoring.defaultPlayerManaRegen,
                disableStatbars = authoring.disableStatbars,
                disableRegen = authoring.disableRegen,
                isUnlimitedStatPoints = authoring.isUnlimitedStatPoints,
                instantKO = authoring.instantKO,
                timesTenExperience = authoring.timesTenExperience,
                isInfiniteRun = authoring.isInfiniteRun
            });       
        }
    }

        /*public void DrawStatsUI()
        {
            GUILayout.Label("=== Stats ===");
            GUILayout.Label("Player Starting Health:");
            defaultPlayerHealth = float.Parse(GUILayout.TextField(defaultPlayerHealth.ToString()));
            GUILayout.Label("Player Starting Energy:");
            defaultPlayerEnergy = float.Parse(GUILayout.TextField(defaultPlayerEnergy.ToString()));
            GUILayout.Label("Player Starting Mana:");
            defaultPlayerMana = float.Parse(GUILayout.TextField(defaultPlayerMana.ToString()));
            GUILayout.Label("Default Voxel Health:");
            defaultVoxelHealth = int.Parse(GUILayout.TextField(defaultVoxelHealth.ToString()));
            GUILayout.Label("Regen Multiplier:");
            regenMultiplier = float.Parse(GUILayout.TextField(regenMultiplier.ToString()));
            GUILayout.Label("=== Disables ===");
            disableStatbars = GUILayout.Toggle(disableStatbars, "Disable Statbars");
            disableRegen = GUILayout.Toggle(disableRegen, "Disable Regen");
        }

        public void DrawStatsCheatsUI()
        {
            GUILayout.Label("Stats");
            isUnlimitedStatPoints = GUILayout.Toggle(isUnlimitedStatPoints, "Unlimited Stat Points");
            timesTenExperience = GUILayout.Toggle(timesTenExperience, "Ten Times Experience");
            instantKO = GUILayout.Toggle(instantKO, "KO Mode");
            isInfiniteRun = GUILayout.Toggle(isInfiniteRun, "Infinite Run");
        }*/
}