﻿using Unity.Entities;

namespace Zoxel.Stats
{
    //! Acquire experience for different userActionLinks. 
    /**
    *   You can also recieve benefits from the levels, in the form of userStatLinks or new userSkillLinks.
    *
    *   You can get different levels for different things. 
    *     - Combat
    *     - Planting Flowers
    *     - Digging Dirt
    *
    *   \todo Use StatValue component for value instead.
    */
    public struct LevelStat : IComponentData
    {
        public int value;
        public float experienceGained;
        public float experienceRequired;
        
        public LevelStat(int value, float experienceGained, float experienceRequired)
        {
            this.value = value;
            this.experienceGained = experienceGained;
            this.experienceRequired = experienceRequired;
        }

        public static float GetExperience(int level)
        {
            var experienceRequired = 10f;
            while (level > 0)
            {
                experienceRequired *= 1.2f;
                level--;
            }
            return experienceRequired;
        }
    }
}