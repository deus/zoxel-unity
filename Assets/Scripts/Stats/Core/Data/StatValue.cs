﻿using Unity.Entities;

namespace Zoxel.Stats
{
    //! A basic stat data with a float value.
    public struct StatValue : IComponentData
    {
        public float value;

        public StatValue(float value)
        {
            this.value = value;
        }
    }
}