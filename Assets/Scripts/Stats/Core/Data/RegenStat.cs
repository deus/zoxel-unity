﻿using Unity.Entities;

namespace Zoxel.Stats
{
    //! Regens a state value over time.
    public struct RegenStat : IComponentData { }
}