﻿using Unity.Entities;

namespace Zoxel.Stats
{
    //! Attributes can increase any other type of stat, by multiplying and adding the value
    /**
    *   UserSkillLinks - passive buffs - can increase the multipliers
    */
    public struct AttributeStat : IComponentData
    {
        public float previousAdded; // previous stat value of effected stat
        //public float multiplier;      // the multiplier that it increases it by
        
        public AttributeStat(float previousAdded)
        {
            this.previousAdded = previousAdded;
        }
    }
}

//! Target stat that it is improving
// public Entity targetStat;

/*public void SetData(Entity targetStat, float newValue)
{
    this.targetStat = targetStat;
    value = newValue;
    previousAdded = 0;
}*/