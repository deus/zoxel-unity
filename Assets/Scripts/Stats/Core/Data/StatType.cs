using System;

namespace Zoxel.Stats
{
    [Serializable]
    public enum StatTypeEditor : byte
    {
        Base,
        State,
        Regen,
        Attribute,
        Level,
        Buff
    }
    
    public static class StatType
    {
        public const byte Base = 0;
        public const byte State = 1;
        public const byte Regen = 2;
        public const byte Attribute = 3;
        public const byte Level = 4;
        public const byte Buff = 5;
    }
}