﻿using Unity.Entities;

namespace Zoxel.Stats
{
    //! A basic stat data with a float value.
    public struct StatValueMax : IComponentData
    {
        public float value;

        public StatValueMax(float value)
        {
            this.value = value;
        }
    }
}