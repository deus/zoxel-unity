using Unity.Entities;

namespace Zoxel.Stats
{
    //! Corresponds to StatType. Used for saving data.
    public struct StatMetaType : IComponentData
    {
        public byte type;

        public StatMetaType(byte type)
        {
            this.type = type;
        }
    }
}