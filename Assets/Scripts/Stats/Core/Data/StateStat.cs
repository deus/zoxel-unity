﻿using Unity.Entities;

namespace Zoxel.Stats
{
    //! A state can be less than it's maxiumum value.
    public struct StateStat : IComponentData { }
}