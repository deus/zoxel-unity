﻿using Unity.Entities;

namespace Zoxel.Stats
{
    //! A basic stat data with a float value.
    public struct BaseStat : IComponentData { }
}