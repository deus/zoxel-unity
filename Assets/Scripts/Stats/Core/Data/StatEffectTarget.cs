using Unity.Entities;

namespace Zoxel.Stats
{
    //! A Stat can effect another stat.
    public struct StatEffectTarget : IComponentData
    {
        //! Target stat that it is effecting or user stat, depending on what it's attached to.
        public Entity target;
        
        public StatEffectTarget(Entity target)
        {
            this.target = target;
        }
    }
}