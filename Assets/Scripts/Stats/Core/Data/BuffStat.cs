﻿using Unity.Entities;

namespace Zoxel.Stats
{
    //! A buff is a temporary value addition to another stat.
    public struct BuffStat : IComponentData
    {
        //! Value that it increases for.
        public float value;
        //! Target stat that it is improving
        public Entity targetStat;

        public BuffStat(float value, Entity targetStat)
        {
            this.value = value;
            this.targetStat = targetStat;
        }
    }
}