using System;
// Is this even used anymore?

namespace Zoxel.Stats
{
    [Serializable]
    public struct StatData
    {
        public int id;
        public int targetStatID;

        public void GenerateID()
        {
            id = Guid.NewGuid().GetHashCode();
        }
    }
}