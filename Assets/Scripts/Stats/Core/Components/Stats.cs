﻿using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Stats
{
    //! Contains an array of Stat entities.
	public struct UserStatLinks : IComponentData
    {
        //! Holds a generic stat entity
        public BlitableArray<Entity> stats;

        public void DisposeFinal()
        {
            stats.DisposeFinal();
        }

        public void Dispose()
        {
            stats.Dispose();
        }
        
        public void Initialize(int count)
        {
            Dispose();
            stats = new BlitableArray<Entity>(count, Allocator.Persistent);
        }
    }
}