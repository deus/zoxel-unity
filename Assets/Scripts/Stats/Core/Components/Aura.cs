﻿using Unity.Entities;

namespace Zoxel.Stats
{
    public struct Aura : IComponentData
    {
        public float range;
        // buff that it gives

        public Aura(float range)
        {
            this.range = range;
        }
    }
}