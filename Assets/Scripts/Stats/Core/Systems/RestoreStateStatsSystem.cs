using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
// this will break in web gl
// do this when leveling up and gained alot of userStatLinks
// or did a big quest chain and got free userStatLinks as quest rewards
// first set all userStatLinks back to original
// now apply new userStatLinks
// Add onto character when loaded game, or whenever attributes are applied

namespace Zoxel.Stats
{
    //! Restores state stat values.
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class RestoreStateStatsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userStatsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userStatsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.ReadOnly<StateStat>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<RestoreStateStats>(processQuery);
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var statValues = GetComponentLookup<StatValue>(false);
            var statValueMaxs = GetComponentLookup<StatValueMax>(true);
            userStatEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<RestoreStateStats>()
                .ForEach((in UserStatLinks userStatLinks) =>
            {
                RestoreStateStatsSystem.RestoreStateStats(in userStatLinks, ref statValues, in statValueMaxs);
            })  .WithNativeDisableContainerSafetyRestriction(statValues)
                .WithReadOnly(statValueMaxs)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        //! Set all state values to max.
        public static void RestoreStateStats(in UserStatLinks userStatLinks, ref ComponentLookup<StatValue> statValues, in ComponentLookup<StatValueMax> statValueMaxs)
        {
            for (int i = 0; i < userStatLinks.stats.Length; i++)
            {
                var userStatEntity2 = userStatLinks.stats[i];
                if (statValueMaxs.HasComponent(userStatEntity2))
                {
                    var stateStat = statValues[userStatEntity2];
                    // stateStat.value = statValueMaxs[userStatEntity2].value;
                    var maxValue = statValueMaxs[userStatEntity2].value;
                    stateStat.value = maxValue * 0.99f;
                    statValues[userStatEntity2] = stateStat;
                }
            }
        }
    }
}