﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Stats.Authoring;

namespace Zoxel.Stats
{
    //! Increases entity experience for a soulLevel.
    /**
    *   \todo Use Event Entity for gaining experience. Instead of adding Component onto Characters.
    *   \todo Support gaining experience per unique LevelStat.
    */
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class GainExperienceSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmsQuery;
        private EntityQuery userStatsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userStatsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserStat>());
            RequireForUpdate(processQuery);
            RequireForUpdate<StatSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var statSettings = GetSingleton<StatSettings>();
            var timesTenExperience = statSettings.timesTenExperience;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<GainExperience>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var levelStats = GetComponentLookup<LevelStat>(false);
            var soulStats = GetComponentLookup<SoulStat>(true);
            userStatEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity, LevelUp>()
                .ForEach((Entity e, int entityInQueryIndex, in UserStatLinks userStatLinks, in GainExperience gainExperience) =>
            {
                var soulUserStatEntity = new Entity();
                for (int i = 0; i < userStatLinks.stats.Length; i++)
                {
                    var userStatEntity = userStatLinks.stats[i];
                    if (soulStats.HasComponent(userStatEntity))
                    {
                        soulUserStatEntity = userStatEntity;
                    }
                }
                if (soulUserStatEntity.Index == 0)
                {
                    return;
                }
                var soulLevel = levelStats[soulUserStatEntity];
                soulLevel.experienceGained += gainExperience.experience;
                if (timesTenExperience)
                {
                    soulLevel.experienceGained += gainExperience.experience * 9f;
                    soulLevel.experienceGained = soulLevel.experienceRequired;
                }
                if (soulLevel.experienceGained >= soulLevel.experienceRequired)
                {
                    PostUpdateCommands.AddComponent<LevelUp>(entityInQueryIndex, e);
                }
                else if (HasComponent<EntitySaver>(e))
                {
                    PostUpdateCommands.AddComponent<SaveStats>(entityInQueryIndex, e);
                }
                levelStats[soulUserStatEntity] = soulLevel;
			})  .WithNativeDisableContainerSafetyRestriction(levelStats)
                .WithReadOnly(soulStats)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}