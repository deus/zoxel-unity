using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Stats
{
    //! Links UserStat's to userStatLinks user after they spawn.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class UserStatsSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userStatsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.ReadWrite<UserStatLinks>(),
                ComponentType.ReadOnly<OnUserStatsSpawned>());
            userStatsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<NewUserStat>(),
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.ReadOnly<CreatorLink>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<OnUserStatsSpawned>(processQuery);
            PostUpdateCommands2.RemoveComponent<NewUserStat>(userStatsQuery);
            //! First initializes children.
            Dependency = Entities
                .WithNone<InitializeEntity>()
                .ForEach((ref UserStatLinks userStatLinks, in OnUserStatsSpawned onUserStatsSpawned) =>
            {
                if (onUserStatsSpawned.spawned != 0)
                {
                    userStatLinks.Initialize(onUserStatsSpawned.spawned);
                }
            }).ScheduleParallel(Dependency);
            //! For each child, using the index, sets into parents children that is passed in.
            var parentEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var userStatLinks = GetComponentLookup<UserStatLinks>(false);
            parentEntities.Dispose();
            Dependency = Entities
                .WithAll<NewUserStat>()
                .ForEach((Entity e, in UserDataIndex userDataIndex, in CreatorLink creatorLink) =>
            {
                var userStatLinks2 = userStatLinks[creatorLink.creator];
                userStatLinks2.stats[userDataIndex.index] = e;
            })  .WithNativeDisableContainerSafetyRestriction(userStatLinks)
                .ScheduleParallel(Dependency);

        }
    }
}