﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Stats.Authoring;

namespace Zoxel.Stats
{
    //! Regens userStatLinks! Based on the regen value adding to state userStatLinks.
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class RegenSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userStatsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate<StatSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var statSettings = GetSingleton<StatSettings>();
            if (statSettings.disableRegen)
            {
                return;
            }
            var regenMultiplier = statSettings.regenMultiplier;
            var deltaTime = (float) World.Time.DeltaTime;
            if (deltaTime == 0)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var statValues = GetComponentLookup<StatValue>(false);
            var statEffectTargets = GetComponentLookup<StatEffectTarget>(true);
            var statValueMaxs = GetComponentLookup<StatValueMax>(true);
            userStatEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity, ApplyAttributes>()
                .WithNone<OnUserStatsSpawned>()
                .WithAll<Regening>()
                .ForEach((Entity e, int entityInQueryIndex, in UserStatLinks userStatLinks) =>
            {
                var didUpdate = false;
                for (int i = 0; i < userStatLinks.stats.Length; i++)
                {
                    var regenUserStatEntity = userStatLinks.stats[i];
                    if (!HasComponent<RegenStat>(regenUserStatEntity))
                    {
                        continue;
                    }
                    var regenStat = statValues[regenUserStatEntity];
                    var targetUserStatEntity = statEffectTargets[regenUserStatEntity].target;
                    if (targetUserStatEntity.Index == 0 || !HasComponent<StateStat>(targetUserStatEntity))
                    {
                        // UnityEngine.Debug.LogError("RegenStat at " + i + " has no target");
                        continue;
                    }
                    var statState = statValues[targetUserStatEntity];
                    var statValueMax = statValueMaxs[targetUserStatEntity];
                    if (statState.value == statValueMax.value)
                    {
                        continue;
                    }
                    didUpdate = true;
                    var addition = regenStat.value * regenMultiplier;
                    addition *= deltaTime;
                    statState.value += addition;   // based on time
                    if (statState.value > statValueMax.value)
                    {
                        statState.value = statValueMax.value;
                    }
                    statValues[targetUserStatEntity] = statState;
                }
                if (!didUpdate)
                {
                    PostUpdateCommands.RemoveComponent<Regening>(entityInQueryIndex, e);
                }
            })  .WithNativeDisableContainerSafetyRestriction(statValues)
                .WithReadOnly(statEffectTargets)
                .WithReadOnly(statValueMaxs)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}