using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Stats.Authoring;

namespace Zoxel.Stats
{
    //! Shares entity experience with minion Creator.
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class ExperienceShareSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<StatSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var statSettings = GetSingleton<StatSettings>();
            var timesTenExperience = statSettings.timesTenExperience;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<LevelUp>()
                .WithAll<SummonedEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref GainExperience gainExperience, in CreatorLink creatorLink) =>
            {
                if (HasComponent<UserStatLinks>(creatorLink.creator)) // !HasComponent<GainExperience>(creatorLink.creator))
                {
                    var experienceShared = gainExperience.experience / 2f;
                    // UnityEngine.Debug.LogError("Creator Gaining Experience: " + experienceShared + " : creatorLink.creator : " + creatorLink.creator.Index);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, creatorLink.creator, new GainExperience(experienceShared));
                    gainExperience.experience -= experienceShared;
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}