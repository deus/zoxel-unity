using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Stats
{
    //! Removes StartRegening event from character.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class StartRegeningEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<StartRegening>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<StartRegening>(entityInQueryIndex, e);
                if (!HasComponent<Regening>(e))
                {
                    PostUpdateCommands.AddComponent<Regening>(entityInQueryIndex, e);
                }
                if (HasComponent<StatsSaverTimer>(e))
                {
                    PostUpdateCommands.AddComponent<SaveStats>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}