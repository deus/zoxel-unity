﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
// this will break in web gl
// do this when leveling up and gained alot of userStatLinks
// or did a big quest chain and got free userStatLinks as quest rewards
// first set all userStatLinks back to original
// now apply new userStatLinks
// Add onto character when loaded game, or whenever attributes are applied

namespace Zoxel.Stats
{
    //! Applies attributes by adding to other user userStatLinks.
    /**
    *   \todo Give each attributeStat unique multipliers.
    */
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class AttributesSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userStatsQuery;
        private EntityQuery statsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userStatsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserStat>());
            statsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<RealmStat>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            const float multiplier = 3f;
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<ApplyAttributes>(processQuery);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var attributeStats = GetComponentLookup<AttributeStat>(false);
            var statValues = GetComponentLookup<StatValue>(false);
            var statValueMaxs = GetComponentLookup<StatValueMax>(false);
            var statEntities = statsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var statEffectTargets = GetComponentLookup<StatEffectTarget>(true);
            userStatEntities.Dispose();
            statEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DeadEntity, DestroyEntity>()
                .WithNone<GenerateStats, LoadStats>()
                .ForEach((Entity e, int entityInQueryIndex, in UserStatLinks userStatLinks, in ApplyAttributes applyAttributes) =>
            {
                for (int j = 0; j < userStatLinks.stats.Length; j++)
                {
                    var userStatEntity = userStatLinks.stats[j];
                    if (!HasComponent<AttributeStat>(userStatEntity))
                    {
                        continue;
                    }
                    var statValue = statValues[userStatEntity];
                    var attributeStat = attributeStats[userStatEntity];
                    if (statValue.value == 0)
                    {
                        continue;
                    }
                    var targetUserStatEntity = statEffectTargets[userStatEntity].target;
                    if (targetUserStatEntity.Index == 0)
                    {
                        //UnityEngine.Debug.LogError("targetUserStatEntity.Index == 0");
                        continue;
                    }
                    float bonusValue = statValue.value * multiplier;
                    if (HasComponent<StateStat>(targetUserStatEntity)) // statValues.HasComponent(targetUserStatEntity))
                    {
                        // var stateStat = statValues[targetUserStatEntity];
                        var stateStatMax = statValueMaxs[targetUserStatEntity];
                        if (attributeStat.previousAdded != 0)
                        {
                            stateStatMax.value = stateStatMax.value - attributeStat.previousAdded;
                        }
                        stateStatMax.value = stateStatMax.value + bonusValue;
                        statValueMaxs[targetUserStatEntity] = stateStatMax;
                        // UnityEngine.Debug.LogError("Adding to stateStat: " + stateStat.value);
                    }
                    else if (HasComponent<RegenStat>(targetUserStatEntity)) // regenStats.HasComponent(targetUserStatEntity))
                    {
                        var regenStat = statValues[targetUserStatEntity];
                        if (attributeStat.previousAdded != 0)
                        {
                            regenStat.value = regenStat.value - attributeStat.previousAdded;
                        }
                        var newValue = regenStat.value + bonusValue;
                        regenStat.value = newValue;
                        statValues[targetUserStatEntity] = regenStat;
                        //UnityEngine.Debug.LogError("Adding to regenStat: " + regenStat.value);
                    }
                    /*else
                    {
                        UnityEngine.Debug.LogError("Could not find stat to update with attribute power!");
                    }*/
                    attributeStat.previousAdded = bonusValue;
                    attributeStats[userStatEntity] = attributeStat;
                    /*Debug.LogError("Adding bonus value to State Stat of: " + bonusValue + 
                        ", attributeStat.multiplier: " + attributeStat.multiplier + ", attributeStat.value: " + attributeStat.value
                        + ", statID was: " + attributeStat.targetID + ", index: " + i);*/
                }
                if (applyAttributes.isNewCharacter == 1)
                {
                    RestoreStateStatsSystem.RestoreStateStats(in userStatLinks, ref statValues, in statValueMaxs);
                    PostUpdateCommands.AddComponent<SaveStats>(entityInQueryIndex, e);
                }
                else // if (applyAttributes.isNewCharacter == 0)
                {
                    if (!HasComponent<Regening>(e))
                    {
                        //! \todo Fix this for multiple userStatLinks regening at once.
                        PostUpdateCommands.AddComponent<StartRegening>(entityInQueryIndex, e);
                    }
                }
			})  .WithReadOnly(statEffectTargets)
                .WithNativeDisableContainerSafetyRestriction(statValues)        // attribubte, regen, base
                .WithNativeDisableContainerSafetyRestriction(statValueMaxs)
                .WithNativeDisableContainerSafetyRestriction(attributeStats)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}