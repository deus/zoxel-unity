using Unity.Entities;

namespace Zoxel.Stats
{
    //! An event entity to update an Entitys user stat.
    public struct UpdateEntityStat : IComponentData
    {
        public Entity character;
        public Entity stat;
        public int amount;

        public UpdateEntityStat(Entity character, Entity stat, int amount)
        {
            this.character = character;
            this.stat = stat;
            this.amount = amount;
        }
    }
}