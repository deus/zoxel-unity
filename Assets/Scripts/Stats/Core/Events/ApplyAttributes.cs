using Unity.Entities;

namespace Zoxel.Stats
{
    //! Applies attribubutes onto other userStatLinks.
    public struct ApplyAttributes : IComponentData
    {
        public byte isNewCharacter;

        public ApplyAttributes(byte isNewCharacter)
        {
            this.isNewCharacter = isNewCharacter;
        }
    }
}