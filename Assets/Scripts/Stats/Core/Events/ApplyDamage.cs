using Unity.Entities;
using Unity.Mathematics;
// Multiple types of damage can spawn during combat step one a target

namespace Zoxel.Stats
{
    public struct ApplyDamage : IComponentData
    {
        public byte state;
        public Entity defender;
        public float damage;
        public byte hitType;
        public Entity attacker;
        public float3 attackPosition;
        public float forceApplied;

        public ApplyDamage(Entity defender, Entity attacker, float damage, byte hitType, float3 attackPosition, float forceApplied)
        {
            this.state = 0;
            this.defender = defender;
            this.attacker = attacker;
            this.damage = damage;
            this.hitType = hitType;
            this.attackPosition = attackPosition;
            this.forceApplied = forceApplied;
        }
    }
}