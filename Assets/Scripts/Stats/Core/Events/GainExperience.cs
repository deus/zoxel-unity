using Unity.Entities;

namespace Zoxel.Stats
{
    //! Adds experience onto a UserStat.
    public struct GainExperience : IComponentData
    { 
        //public Entity userStat;
        public float experience;

        public GainExperience(float experience)
        {
            this.experience = experience;
        }
    }
}