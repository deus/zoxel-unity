using Unity.Entities;

namespace Zoxel.Stats
{
    //! When an entity kills another.
    public struct OnKilledEntity : IComponentData
    {
        public Entity killer;
        public Entity enemy;

        public OnKilledEntity(Entity killer, Entity enemy)
        {
            this.killer = killer;
            this.enemy = enemy;
        }
    }
}