using Unity.Entities;

namespace Zoxel.Stats
{
    //! An event for starting an entitys regen mechanics.
    public struct StartRegening : IComponentData
    {
        //! The UserStat entity that triggered regening.
        public Entity userStat;

        public StartRegening(Entity userStat)
        {
            this.userStat = userStat;
        }
    }
}