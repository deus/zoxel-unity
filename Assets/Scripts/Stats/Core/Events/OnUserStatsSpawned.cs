using Unity.Entities;

namespace Zoxel.Stats
{
    //! An event for linking up user items after spawning.
    public struct OnUserStatsSpawned : IComponentData
    {
        public byte spawned;

        public OnUserStatsSpawned(byte spawned)
        {
            this.spawned = spawned;
        }
    }
}