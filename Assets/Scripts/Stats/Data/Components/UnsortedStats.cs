using Unity.Entities;

namespace Zoxel.Stats
{
    public struct StatPoint : IComponentData { }
    public struct SkillPoint : IComponentData { }
    public struct RealmBaseStat : IComponentData { }
    public struct RealmStateStat : IComponentData { }
    public struct RealmRegenStat : IComponentData { }
    public struct RealmAttributeStat : IComponentData { }
    public struct RealmLevelStat : IComponentData { }
}