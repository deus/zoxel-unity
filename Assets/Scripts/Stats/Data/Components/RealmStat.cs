using Unity.Entities;

namespace Zoxel.Stats
{
    //! Added to a Stat Meta data Entity.
    public struct RealmStat : IComponentData { }
}