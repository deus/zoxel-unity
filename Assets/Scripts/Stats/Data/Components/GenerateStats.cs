using Unity.Entities;

namespace Zoxel.Stats
{
    //! Generates a bunch of userStatLinks on an entity.
    public struct GenerateStats : IComponentData
    {
        public byte spawnType;

        public GenerateStats(byte spawnType)
        {
            this.spawnType = spawnType;
        }
    }
}