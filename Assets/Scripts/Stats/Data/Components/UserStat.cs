using Unity.Entities;

namespace Zoxel.Stats
{
    //! The users stat tag.
    public struct UserStat : IComponentData { }
}