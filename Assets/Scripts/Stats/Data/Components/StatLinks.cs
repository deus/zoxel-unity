using Unity.Entities;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Zoxel.Stats
{
    //! Meta data for userStatLinks now generated per game.
    public struct StatLinks : IComponentData
    {
        public BlitableArray<Entity> userStatLinks;
        public UnsafeParallelHashMap<int, Entity> statsHash;

        public void Dispose()
        {
            userStatLinks.Dispose();
			if (statsHash.IsCreated)
			{
				statsHash.Dispose();
			}
        }

        public void Initialize(int count)
        {
            Dispose();
            this.userStatLinks = new BlitableArray<Entity>(count, Allocator.Persistent);
            this.statsHash = new UnsafeParallelHashMap<int, Entity>(count, Allocator.Persistent);
        }

        public Entity GetStat(int statID)
        {
            if (statsHash.IsCreated)
            {
                Entity outputEntity;
                if (statsHash.TryGetValue(statID, out outputEntity))
                {
                    return outputEntity;
                }
            }
            return new Entity();
        }
    }
}