using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Textures;

namespace Zoxel.Stats
{
    //! Generates Stats for the Realm.
    /**
    *   - Data Generation System -
    *   \todo Convert to Parallel.
    *   \todo Load texture datam into texture component.
    *   \todo Generate stat textures from base shapes and add colours and noise per stat type.
    */
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class StatsGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity statPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (StatsManager.instance == null) return;
            var statPrefab = StatsSystemGroup.statPrefab;
            var soulID = StatsManager.instance.StatSettings.soul.Value.id;
            var statDatams = StatsManager.instance.StatSettings.stats;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); //.AsParallelWriter();
            Entities
                .ForEach((Entity e, ref StatLinks statLinks, ref GenerateRealm generateRealm, in ZoxID zoxID) =>
            {
                if (generateRealm.state == (byte) GenerateRealmState.GenerateStatsLinking)
                {
                    generateRealm.state = (byte) GenerateRealmState.GenerateSkills;
                    return;
                }
                if (generateRealm.state != (byte) GenerateRealmState.GenerateStats)
                {
                    return;
                }
                var realmSeed = zoxID.id;
                if (realmSeed == 0)
                {
                    // UnityEngine.Debug.LogError("realmSeed iS 0.");
                    return;
                }
                generateRealm.state = (byte) GenerateRealmState.GenerateStatsLinking;
                // Destroy UserSkillLinks and SkillTrees
                for (int i = 0; i < statLinks.userStatLinks.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(statLinks.userStatLinks[i]);
                }
                statLinks.Dispose();
                // since some of this data comes from authored datams, we should not dispose of that data
                // later on, generate some of the itemDatams too
                // UnityEngine.Debug.LogError("Initializing Realm Items: " + itemDatams.Count);
                var stateStatsCount = 0;
                var newIDs = new NativeList<int>();
                var newOnes = new NativeList<Entity>();
                // Exclude Regen Stats
                for (int i = 0; i < statDatams.Length; i++)
                {
                    //var itemID = realmSeed + i * 500;
                    var statDatam = statDatams[i];
                    if (statDatam == null)
                    {
                        // UnityEngine.Debug.LogError("Problem with Stat at: " + i);
                        continue;
                    }
                    var name = statDatam.name;
                    var description = statDatam.description;
                    var id = statDatam.Value.id;
                    var e2 = EntityManager.Instantiate(statPrefab);
                    if (id == soulID)
                    {
                        PostUpdateCommands.AddComponent<SoulStat>(e2);
                    }
                    PostUpdateCommands.SetComponent(e2, new StatMetaType((byte) statDatam.type));
                    PostUpdateCommands.SetComponent(e2, new ZoxID(id));
                    PostUpdateCommands.SetComponent(e2, new ZoxName(name));
                    #if UNITY_EDITOR
                    PostUpdateCommands.SetComponent(e2, new EditorName("[stat] " + name));
                    #endif
                    PostUpdateCommands.SetComponent(e2, new ZoxDescription(description));
                    var texture = new Texture(in statDatam.texture.texture);
                    PostUpdateCommands.SetComponent(e2, texture);
                    if ((byte) statDatam.type == StatType.Base)
                    {
                        PostUpdateCommands.AddComponent<RealmBaseStat>(e2);
                        if (statDatam == StatsManager.instance.StatSettings.statPoint)
                        {
                            PostUpdateCommands.AddComponent<StatPoint>(e2);
                        }
                        else if (statDatam == StatsManager.instance.StatSettings.skillPoint)
                        {
                            PostUpdateCommands.AddComponent<SkillPoint>(e2);
                        }
                    }
                    else if ((byte) statDatam.type == StatType.State)
                    {
                        PostUpdateCommands.AddComponent<RealmStateStat>(e2);
                        if (stateStatsCount == 0)
                        {
                            PostUpdateCommands.AddComponent<HealthStat>(e2);
                        }
                        stateStatsCount++;
                    }
                    else if ((byte) statDatam.type == StatType.Regen)
                    {
                        PostUpdateCommands.AddComponent<RealmRegenStat>(e2);
                    }
                    else if ((byte) statDatam.type == StatType.Regen)
                    {
                        PostUpdateCommands.AddComponent<RealmRegenStat>(e2);
                    }
                    else if ((byte) statDatam.type == StatType.Attribute)
                    {
                        PostUpdateCommands.AddComponent<RealmAttributeStat>(e2);
                    }
                    else if ((byte) statDatam.type == StatType.Level)
                    {
                        PostUpdateCommands.AddComponent<RealmLevelStat>(e2);
                    }
                    if (statDatam.isPlayerChoice)
                    {
                        PostUpdateCommands.AddComponent<PlayerStartingStat>(e2);
                    }
                    newOnes.Add(e2);
                    newIDs.Add(id);
                    // UnityEngine.Debug.LogError(i + " New Realm Stat: " + name + " :: " + statID);
                }
                // Set Targets
                for (int i = 0; i < statDatams.Length; i++)
                {
                    var statDatam = statDatams[i];
                    if (statDatam == null)
                    {
                        continue;
                    }
                    if (statDatam.targetStat == null)
                    {
                        continue;
                    }
                    var statEntity = newOnes[i];
                    // Link Regen to State Stat here.
                    // get index
                    var targetStatIndex = -1; 
                    for (int j = 0; j < statDatams.Length; j++)
                    {
                        if (statDatams[j] == statDatam.targetStat)
                        {
                            targetStatIndex = j;
                            break;
                        }
                    }
                    var targetStatEntity = newOnes[targetStatIndex];
                    PostUpdateCommands.AddComponent(statEntity, new StatEffectTarget(targetStatEntity));
                }
                statLinks.Initialize(newOnes.Length);
                for (int i = 0; i < newOnes.Length; i++)
                {
                    statLinks.userStatLinks[i] = newOnes[i];
                    statLinks.statsHash[newIDs[i]] = newOnes[i];
                }
            }).WithStructuralChanges().Run();   // spawns skill tree and skill data
        }
    }
}