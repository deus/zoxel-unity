using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Stats.Authoring;

namespace Zoxel.Stats
{
    //! Generates character Stats.
    /**
    *   \todo Generate userStatLinks based off LevelStat input and Class of character. (Fighter, Tank, etc)
    */
    [BurstCompile, UpdateInGroup(typeof(StatsSystemGroup))]
    public partial class CharacterStatsGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmsQuery;
        private EntityQuery statsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<StatLinks>(),
                ComponentType.Exclude<DestroyEntity>());
            statsQuery = GetEntityQuery(
                ComponentType.ReadOnly<RealmStat>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(realmsQuery);
            RequireForUpdate(statsQuery);
            RequireForUpdate<StatSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var statSettings = GetSingleton<StatSettings>();
            var userStatPrefabs = StatsSystemGroup.userStatPrefabs;
            var defaultPlayerHealth = statSettings.defaultPlayerHealth;
            var defaultPlayerEnergy = statSettings.defaultPlayerEnergy;
            var defaultPlayerMana = statSettings.defaultPlayerMana;
            var defaultPlayerHealthRegen = statSettings.defaultPlayerHealthRegen;
            var defaultPlayerEnergyRegen = statSettings.defaultPlayerEnergyRegen;
            var defaultPlayerManaRegen = statSettings.defaultPlayerManaRegen;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<GenerateStats>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var realmEntities = realmsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var statLinks = GetComponentLookup<StatLinks>(true);
            realmEntities.Dispose();
            var statEntities = statsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var statEffectTargets = GetComponentLookup<StatEffectTarget>(true);
            statEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity, DeadEntity>()
                .WithAll<GenerateStats, UserStatLinks>()
                .ForEach((Entity e, int entityInQueryIndex, in GenerateStats generateStats, in RealmLink realmLink, in ZoxID zoxID) =>
            {
                var realmStatLinks = statLinks[realmLink.realm];
                var soulEntity = new Entity();
                // states
                var healthEntity = new Entity();
                var energyEntity = new Entity();
                var manaEntity = new Entity();
                // regens
                var healthRegenEntity = new Entity();
                var energyRegenEntity = new Entity();
                var manaRegenEntity = new Entity();
                // attributes
                var strengthEntity = new Entity();
                var agilityEntity = new Entity();
                var intelligenceEntity = new Entity();
                var vitalityEntity = new Entity();
                var dexterityEntity = new Entity();
                var wisdomEntity = new Entity();
                // base
                var skillPointEntity = new Entity();
                var statPointEntity = new Entity();
                var physicalDamageEntity = new Entity();
                var physicalDefenceEntity = new Entity();
                var magicalDamageEntity = new Entity();
                var magicalDefenceEntity = new Entity();
                var movementSpeedEntity = new Entity();
                var attackSpeedEntity = new Entity();
                var stateStatsCount = 0;
                var regenStatsCount = 0;
                var attributeStatsCount = 0;
                var baseStatsCount = 0;
                for (int i = 0; i < realmStatLinks.userStatLinks.Length; i++)
                {
                    var statEntity = realmStatLinks.userStatLinks[i];
                    if (HasComponent<RealmLevelStat>(statEntity))
                    {
                        if (HasComponent<SoulStat>(statEntity))
                        {
                            soulEntity = statEntity;
                        }
                    }
                    else if (HasComponent<RealmStateStat>(statEntity))
                    {
                        if (stateStatsCount == 0)
                        {
                            healthEntity = statEntity;
                        }
                        else if (stateStatsCount == 1)
                        {
                            energyEntity = statEntity;
                        }
                        else if (stateStatsCount == 2)
                        {
                            manaEntity = statEntity;
                        }
                        stateStatsCount++;
                    }
                    else if (HasComponent<RealmRegenStat>(statEntity))
                    {
                        if (regenStatsCount == 0)
                        {
                            healthRegenEntity = statEntity;
                        }
                        else if (regenStatsCount == 1)
                        {
                            energyRegenEntity = statEntity;
                        }
                        else if (regenStatsCount == 2)
                        {
                            manaRegenEntity = statEntity;
                        }
                        regenStatsCount++;
                    }
                    else if (HasComponent<RealmAttributeStat>(statEntity))
                    {
                        if (attributeStatsCount == 0)
                        {
                            strengthEntity = statEntity;
                        }
                        else if (attributeStatsCount == 1)
                        {
                            vitalityEntity = statEntity;
                        }
                        else if (attributeStatsCount == 2)
                        {
                            agilityEntity = statEntity;
                        }
                        else if (attributeStatsCount == 3)
                        {
                            dexterityEntity = statEntity;
                        }
                        else if (attributeStatsCount == 4)
                        {
                            intelligenceEntity = statEntity;
                        }
                        else if (attributeStatsCount == 5)
                        {
                            wisdomEntity = statEntity;
                        }
                        attributeStatsCount++;
                    }
                    else if (HasComponent<RealmBaseStat>(statEntity))
                    {
                        if (baseStatsCount == 0)
                        {
                            skillPointEntity = statEntity;
                        }
                        else if (baseStatsCount == 1)
                        {
                            statPointEntity = statEntity;
                        }
                        else if (baseStatsCount == 2)
                        {
                            physicalDamageEntity = statEntity;
                        }
                        else if (baseStatsCount == 3)
                        {
                            physicalDefenceEntity = statEntity;
                        }
                        else if (baseStatsCount == 4)
                        {
                            magicalDamageEntity = statEntity;
                        }
                        else if (baseStatsCount == 5)
                        {
                            magicalDefenceEntity = statEntity;
                        }
                        else if (baseStatsCount == 6)
                        {
                            movementSpeedEntity = statEntity;
                        }
                        else if (baseStatsCount == 7)
                        {
                            attackSpeedEntity = statEntity;
                        }
                        baseStatsCount++;
                    }
                }
                // UnityEngine.Debug.LogError("Adding States healthEntity: " + healthEntity.Index);
                // base this on position of character spawning
                //      Or of what type of character it is
                // add basic userStatLinks for npc
                var random = new Random();
                random.InitState((uint) zoxID.id);
                var soulLevel = 0;
                var regenValue = 1;
                var healthValue = 6f;
                var energyValue = 6f;
                var manaValue = 6f;
                if (generateStats.spawnType == NPCType.slem)
                {
                    soulLevel = random.NextInt(0, 1);
                    healthValue = 4 + soulLevel * 2;
                    energyValue = 4 + soulLevel * 2;
                    energyValue = 4 + soulLevel * 2;
                }
                else if (generateStats.spawnType == NPCType.glem)
                {
                    soulLevel = random.NextInt(1, 3);
                    healthValue = 4 + soulLevel * 3;
                    energyValue = 0;
                    manaValue = 10;
                }
                else if (generateStats.spawnType == NPCType.chen)
                {
                    soulLevel = random.NextInt(2, 4);
                    healthValue = 6 + soulLevel * 6;
                    energyValue = 6;
                    manaValue = 6 + soulLevel * 4;
                    regenValue = 2;
                }
                else if (generateStats.spawnType == NPCType.player)
                {
                    soulLevel = 0;
                    healthValue = defaultPlayerHealth;
                    energyValue = defaultPlayerEnergy;
                    manaValue = defaultPlayerMana;
                    regenValue = 1;
                }
                else // if (generateStats.spawnType == 3 || generateStats.spawnType == 4)
                {
                    soulLevel = random.NextInt(5, 8);
                    healthValue = 6 + soulLevel * 6;
                    healthValue = 6 + soulLevel * 4;
                    manaValue = 20;
                    regenValue = 4;
                }
                var userStatEntities = new NativeList<Entity>();
                byte statIndex = 0;
                // Add Soul Stat
                // UnityEngine.Debug.LogError("Soul Level is: " + soulLevel + " and spawn type: " + generateStats.spawnType);
                var experienceRequired = LevelStat.GetExperience(soulLevel);
                var soulUserStatEntity = StatsSystemGroup.AddUserStatLevel(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.levelUserStatPrefab, statIndex, e, soulEntity, soulLevel, 0, experienceRequired);
                PostUpdateCommands.AddComponent<SoulStat>(entityInQueryIndex, soulUserStatEntity);
                userStatEntities.Add(soulUserStatEntity);
                statIndex++;
                // Add 8 Base Stats
                userStatEntities.Add(StatsSystemGroup.AddUserStatBase(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.baseUserStatPrefab, statIndex, e, physicalDamageEntity, 0));
                statIndex++;
                userStatEntities.Add(StatsSystemGroup.AddUserStatBase(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.baseUserStatPrefab, statIndex, e, physicalDefenceEntity, 0));
                statIndex++;
                userStatEntities.Add(StatsSystemGroup.AddUserStatBase(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.baseUserStatPrefab, statIndex, e, magicalDamageEntity, 0));
                statIndex++;
                userStatEntities.Add(StatsSystemGroup.AddUserStatBase(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.baseUserStatPrefab, statIndex, e, magicalDefenceEntity, 0));
                statIndex++;
                userStatEntities.Add(StatsSystemGroup.AddUserStatBase(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.baseUserStatPrefab, statIndex, e, attackSpeedEntity, 0));
                statIndex++;
                userStatEntities.Add(StatsSystemGroup.AddUserStatBase(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.baseUserStatPrefab, statIndex, e, movementSpeedEntity, 0));
                statIndex++;
                var statPointUserStatEntity = StatsSystemGroup.AddUserStatBase(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.baseUserStatPrefab, statIndex, e, statPointEntity, 0);
                PostUpdateCommands.AddComponent<StatPoint>(entityInQueryIndex, statPointUserStatEntity);
                userStatEntities.Add(statPointUserStatEntity);
                statIndex++;
                var skillPointUserStatEntity = StatsSystemGroup.AddUserStatBase(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.baseUserStatPrefab, statIndex, e, skillPointEntity, 0);
                PostUpdateCommands.AddComponent<SkillPoint>(entityInQueryIndex, skillPointUserStatEntity);
                userStatEntities.Add(skillPointUserStatEntity);
                statIndex++;
                // 3 state userStatLinks
                var healthUserStatEntity = StatsSystemGroup.AddUserStatState(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.stateUserStatPrefab, statIndex, e, healthEntity, healthValue);
                PostUpdateCommands.AddComponent<HealthStat>(entityInQueryIndex, healthUserStatEntity);
                userStatEntities.Add(healthUserStatEntity);
                statIndex++;
                var energyUserStatEntity = StatsSystemGroup.AddUserStatState(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.stateUserStatPrefab, statIndex, e, energyEntity, energyValue);
                userStatEntities.Add(energyUserStatEntity);
                statIndex++;
                var manaUserStatEntity = StatsSystemGroup.AddUserStatState(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.stateUserStatPrefab, statIndex, e, manaEntity, manaValue);
                userStatEntities.Add(manaUserStatEntity);
                statIndex++;
                // 3 regens
                var healthEntity2 = statEffectTargets[healthRegenEntity].target;
                var energyEntity2 = statEffectTargets[energyRegenEntity].target;
                var manaEntity2 = statEffectTargets[manaRegenEntity].target;
                var healthRegenUserEntity = StatsSystemGroup.AddUserStatRegen(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.regenUserStatPrefab, statIndex, e, healthRegenEntity, healthUserStatEntity, regenValue);
                statIndex++;
                var energyRegenUserEntity = StatsSystemGroup.AddUserStatRegen(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.regenUserStatPrefab, statIndex, e, energyRegenEntity, energyUserStatEntity, regenValue);
                statIndex++;
                var manaRegenUserEntity = StatsSystemGroup.AddUserStatRegen(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.regenUserStatPrefab, statIndex, e, manaRegenEntity, manaUserStatEntity, regenValue);
                statIndex++;
                userStatEntities.Add(healthRegenUserEntity);
                userStatEntities.Add(energyRegenUserEntity);
                userStatEntities.Add(manaRegenUserEntity);
                // Player - add attributes
                if (generateStats.spawnType == NPCType.player)
                {
                    userStatEntities.Add(StatsSystemGroup.AddUserStatAttribute(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.attributeUserStatPrefab, statIndex, e, strengthEntity, healthUserStatEntity, 0));
                    statIndex++;
                    userStatEntities.Add(StatsSystemGroup.AddUserStatAttribute(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.attributeUserStatPrefab, statIndex, e, vitalityEntity, healthRegenUserEntity, 0));
                    statIndex++;
                    userStatEntities.Add(StatsSystemGroup.AddUserStatAttribute(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.attributeUserStatPrefab, statIndex, e, agilityEntity, energyUserStatEntity, 0));
                    statIndex++;
                    userStatEntities.Add(StatsSystemGroup.AddUserStatAttribute(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.attributeUserStatPrefab, statIndex, e, dexterityEntity, energyRegenUserEntity, 0));
                    statIndex++;
                    userStatEntities.Add(StatsSystemGroup.AddUserStatAttribute(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.attributeUserStatPrefab, statIndex, e, intelligenceEntity, manaUserStatEntity, 0));
                    statIndex++;
                    userStatEntities.Add(StatsSystemGroup.AddUserStatAttribute(PostUpdateCommands, entityInQueryIndex, userStatPrefabs.attributeUserStatPrefab, statIndex, e, wisdomEntity, manaRegenUserEntity, 0));
                    statIndex++;
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ApplyAttributes(1));
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUserStatsSpawned(statIndex));
                var userStatEntitiesArray = userStatEntities.AsArray(); // .ToArray();
                // PostUpdateCommands.SetComponent(entityInQueryIndex, userStatEntitiesArray, new CreatorLink(e));
                PostUpdateCommands.AddComponent(entityInQueryIndex, userStatEntitiesArray, new CreatorLink(e));
                userStatEntities.Dispose();
                userStatEntitiesArray.Dispose();
            })  .WithReadOnly(statLinks)
                .WithReadOnly(statEffectTargets)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

/*userStatLinks.AddBaseStat(movementSpeedEntity, 5);
userStatLinks.AddBaseStat(attackSpeedEntity, 5);
userStatLinks.AddBaseStat(physicalDamageEntity, 1);
userStatLinks.AddBaseStat(physicalDefenceEntity, 1);
userStatLinks.AddBaseStat(magicalDamageEntity, 1);
userStatLinks.AddBaseStat(magicalDefenceEntity, 1);
userStatLinks.AddBaseStat(statPointEntity, 0);
userStatLinks.AddBaseStat(skillPointEntity, 0);*/

//if (generateStats.spawnType != 5)
//{
//    userStatLinks.GenerateRegenStats(healthEntity2, energyEntity2, manaEntity2, healthRegenEntity, energyRegenEntity, manaRegenEntity, regenValue);
//}
//else
//{
    // userStatLinks.AddStates(healthEntity, energyEntity, manaEntity, defaultPlayerHealth, defaultPlayerEnergy, defaultPlayerMana);   // 6 6 6
    //userStatLinks.GenerateRegenStats(healthEntity2, energyEntity2, manaEntity2, healthRegenEntity, energyRegenEntity, manaRegenEntity,
    //    defaultPlayerHealthRegen, defaultPlayerEnergyRegen, defaultPlayerManaRegen);
//}
// userStatLinks.GenerateLevel(soulEntity, soulLevel);
/*if (generateStats.spawnType == 1)
{
    // ranged
    //userStatLinks.GenerateAttributes(healthEntity, energyEntity, manaEntity, healthRegenEntity, energyRegenEntity, manaRegenEntity,
    //    strengthEntity, agilityEntity, intelligenceEntity, vitalityEntity, dexterityEntity, wisdomEntity, 2);
}*/
/*userStatLinks.GenerateAttributes(healthEntity, energyEntity, manaEntity, healthRegenEntity, energyRegenEntity, manaRegenEntity,
    strengthEntity, agilityEntity, intelligenceEntity, vitalityEntity, dexterityEntity, wisdomEntity, 0);*/

/*var soulEntity = StatsManager.instance.GetStat("Soul").Value.id;
var healthEntity = StatsManager.instance.GetStat("Health").Value.id;
var energyEntity = StatsManager.instance.GetStat("Energy").Value.id;
var manaEntity = StatsManager.instance.GetStat("Mana").Value.id;
var healthRegenEntity = StatsManager.instance.GetStat("HealthRegen").Value.id;
var energyRegenEntity = StatsManager.instance.GetStat("EnergyRegen").Value.id;
var manaRegenEntity = StatsManager.instance.GetStat("ManaRegen").Value.id;
var strengthEntity = StatsManager.instance.GetStat("Strength").Value.id;
var agilityEntity = StatsManager.instance.GetStat("Agility").Value.id;
var intelligenceEntity = StatsManager.instance.GetStat("Intelligence").Value.id;
var vitalityEntity = StatsManager.instance.GetStat("Vitality").Value.id;
var dexterityEntity = StatsManager.instance.GetStat("Dexterity").Value.id;
var wisdomEntity = StatsManager.instance.GetStat("Wisdom").Value.id;
var skillPointEntity = StatsManager.instance.GetStat("SkillPoint").Value.id;
var statPointEntity = StatsManager.instance.GetStat("StatPoint").Value.id;
var physicalDamageEntity = StatsManager.instance.GetStat("Physical Damage").Value.id;
var physicalDefenceEntity = StatsManager.instance.GetStat("Physical Defence").Value.id;
var magicalDamageEntity = StatsManager.instance.GetStat("Magical Damage").Value.id;
var magicalDefenceEntity = StatsManager.instance.GetStat("Magical Defence").Value.id;
var movementSpeedEntity = StatsManager.instance.GetStat("Movement Speed").Value.id;
var attackSpeedEntity = StatsManager.instance.GetStat("Attack Speed").Value.id;*/


// userStatLinks.AddStates(healthEntity, energyEntity, manaEntity, 6, 12, 0);
// userStatLinks.AddStates(healthEntity, energyEntity, manaEntity, 4, 0, 12);
// userStatLinks.AddStates(healthEntity, energyEntity, manaEntity, 14, 14, 4);    // give boss bro a mana sheild later
//userStatLinks.GenerateAttributes(healthEntity, energyEntity, manaEntity, healthRegenEntity, energyRegenEntity, manaRegenEntity,
//    strengthEntity, agilityEntity, intelligenceEntity, vitalityEntity, dexterityEntity, wisdomEntity, 2);
// userStatLinks.AddStates(healthEntity, energyEntity, manaEntity, 32, 32, 32);    // give boss bro a mana sheild later
//userStatLinks.GenerateAttributes(healthEntity, energyEntity, manaEntity, healthRegenEntity, energyRegenEntity, manaRegenEntity,
//    strengthEntity, agilityEntity, intelligenceEntity, vitalityEntity, dexterityEntity, wisdomEntity, 6);
// userStatLinks.AddStates(healthEntity, energyEntity, manaEntity, defaultPlayerHealth, defaultPlayerEnergy, defaultPlayerMana);   // 6 6 6