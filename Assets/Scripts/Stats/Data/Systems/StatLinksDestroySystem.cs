using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Stats
{
    //! Disposes of StatLinks made up of stat datas.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class StatLinksDestroySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // Unique Items
			Dependency = Entities
                .WithAll<DestroyEntity, StatLinks>()
                .ForEach((int entityInQueryIndex, in StatLinks statLinks) =>
			{
                for (int i = 0; i < statLinks.userStatLinks.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, statLinks.userStatLinks[i]);
                }
                statLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}