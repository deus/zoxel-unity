﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Stats;  // damage system
// todo: use chunk link on bullets to determine which characters are in chunks - to reduce amount of checks

// bullet hit system relies on cached bullet data to be passed to each character
// it outputs the hits that happen
// a component system should take care of actual hits separately
//  todo: make bullets not hit grass
//  todo: When hit, spawn entity as event?

namespace Zoxel.Bullets
{
    [BurstCompile, UpdateInGroup(typeof(BulletSystemGroup))]
	public partial class BulletHitSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery bulletsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            bulletsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<Bullet>(),
                ComponentType.ReadOnly<CreatorLink>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(bulletsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime =  World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var damagePrefab = StatsSystemGroup.damagePrefab;
            var bulletEntities = bulletsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var translations = GetComponentLookup<Translation>(true);
            var creatorLinks = GetComponentLookup<CreatorLink>(true);
            var bullets = GetComponentLookup<Bullet>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithNone<EntityBusy, DeadEntity>()
                .WithAll<BulletHitTaker>()
                .ForEach((Entity e, int entityInQueryIndex, in Body body, in Translation position) =>
            {
                var bulletHit = new Entity();
                var bulletPosition = float3.zero;
                for (var i = 0; i < bulletEntities.Length; i++)
                {
                    var e2 = bulletEntities[i];
                    var creatorLink = creatorLinks[e2];
                    if (creatorLink.creator == e) 
                    {
                        continue;
                    }
                    var translation = translations[e2].Value;
                    var difference = translation - position.Value;
                    if (math.abs(difference.x) <= body.size.x && math.abs(difference.y) <= body.size.y && math.abs(difference.z) <= body.size.z)
                    {
                        bulletHit = e2;
                        bulletPosition = translation;
                        // UnityEngine.Debug.LogError("Bullet Hit Character!");
                        break;  // can't hit multiple things? - can always make an array for that with 16 targets
                    }
                }
                if (bulletHit.Index > 0)
                {
                    var bullet = bullets[bulletHit];
                    var attackingCharacter = creatorLinks[bulletHit].creator;
                    PostUpdateCommands.AddComponent<DeadEntity>(entityInQueryIndex, bulletHit);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, bulletHit, new DestroyEntityInTime(elapsedTime, 1));
                    var applyDamage = new ApplyDamage(e, attackingCharacter, bullet.damage, 1, bulletPosition, 1.7f);
                    var applyDamageEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, damagePrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, applyDamageEntity, applyDamage);
                }
            })  .WithReadOnly(bulletEntities)
                .WithDisposeOnCompletion(bulletEntities)
                .WithReadOnly(translations)
                .WithNativeDisableContainerSafetyRestriction(translations)
                .WithReadOnly(creatorLinks)
                .WithReadOnly(bullets)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}