﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Voxels;
using Zoxel.Particles;
using Zoxel.Clans;
using Zoxel.Portals;
using Zoxel.Rendering;
using Zoxel.Voxels.Lighting;
using Zoxel.Movement.Authoring;

namespace Zoxel.Bullets
{
    //! Spawns bullets!
    /**
    *   - Spawn System -
    *   \todo Use Vox Model prefab and just add bullet things to it.
    *   \todo Make bullets use skill colours rather then random, just add a bit of variation.
    *   \todo Make bullets different shapes - bullet - cube - sphere - ninja star.
    */
    [BurstCompile, UpdateInGroup(typeof(BulletSystemGroup))]
    public partial class BulletSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity bulletPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var bulletArchtype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(UpdateEntityLighting),
                typeof(SpawnVoxChunk),
                typeof(Seed),
                typeof(GenerateVox),
                typeof(GenerateVoxData),
                typeof(DestroyEntityInTime),
                typeof(Bullet), 
                typeof(ZoxID),
                typeof(CreatorLink),
                typeof(ClanLink),
                typeof(Body),
                typeof(BodyForce),
                typeof(EntityVoxelPosition),
                typeof(GravityQuadrant),
                typeof(GravityForce),
                // typeof(Traveler),
                typeof(BasicVoxelCollider),
                typeof(Model),
                typeof(Vox),
                typeof(ChunkDimensions),
                typeof(VoxScale),
                typeof(VoxColors),
                typeof(ParticleSystem),
                typeof(VoxLink),
                typeof(EntityMaterials),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld)
            );
            bulletPrefab = EntityManager.CreateEntity(bulletArchtype);
            EntityManager.SetComponentData(bulletPrefab, new NonUniformScale { Value = new float3(1,1,1) });
            RequireForUpdate<PhysicsSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var physicsSettings = GetSingleton<PhysicsSettings>();
            var gravity = new float3(0, -physicsSettings.fallSpeed / 5f, 0);
            // var size = VoxelManager.instance.GetVoxelScale();   // 0.5f * 
            var voxScale = new float3(1f, 1f, 1f) / 32f;
            var voxelDimensions = new int3(8, 8, 8); //  new int3(16, 16, 16);    // this..? should depend on vox size
            var size = voxScale * voxelDimensions.ToFloat3();
            // var lifetime = 0.5f; // data.lifetime; // increase by character userStatLinks
            var speed = 4f; // 0.4f;   // data.speed; // increase by character userStatLinks
            var bulletPrefab = this.bulletPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DelayEvent>()
                .ForEach((Entity e, int entityInQueryIndex, in SpawnBullet spawnBullet) =>
            {
                var spawnID = entityInQueryIndex + ((int) elapsedTime * 1000) % 1000000; //IDUtil.GenerateUniqueID();
                var attackDamage = spawnBullet.attackDamage;
                var position = spawnBullet.spawnPosition;
                var rotation = spawnBullet.spawnRotation;
                //var velocity = math.rotate(rotation, new float3(0, 0, speed));
                var velocity = math.mul(rotation, new float3(0, 0, speed));
                var bulletEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, bulletPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new UpdateEntityLighting(16));
                // initialize bullet
                // increase by character userStatLinks and skill level!
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new ZoxID(spawnID));
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new CreatorLink(spawnBullet.creator));
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new ClanLink(spawnBullet.clan));
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new Bullet(spawnBullet.attackDamage));
                // Physics
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new BodyForce(velocity));
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new GravityForce(gravity));
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new GravityQuadrant(spawnBullet.gravityQuadrant));
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new Body(size));
                // Voxel
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new ChunkDimensions(voxelDimensions));
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new VoxScale(voxScale));
                // Transforms
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new Translation { Value = position + velocity * 0.004f });
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new Rotation { Value = rotation });
                var random = new Random();
                random.InitState((uint)(spawnID));
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new ParticleSystem
                {
                    timeSpawn = 0.03,
                    random = random,
                    spawnSize = new float3(0.1f, 0.1f, 0.1f),
                    baseColor = new Color(200, 25, 25),
                    colorVariance = new float3(0.15f, 0.1f, 0.1f),
                    particleLife = 2,
                    positionAdd = new float3(0, 0.6f, 0)
                });
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new VoxLink(spawnBullet.planet));
                var otherSeed = (uint) spawnBullet.creatorID;
                var voxelColors = new VoxColors();
                voxelColors.SetAsColor(new Color(random.NextInt(180, 255), 0, 0));
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, voxelColors);
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new DestroyEntityInTime(elapsedTime, 3));
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new GenerateVoxData(GenerateVoxType.Bullet));
                PostUpdateCommands.SetComponent(entityInQueryIndex, bulletEntity, new Seed(spawnID));
                // UnityEngine.Debug.DrawLine(position, position + math.rotate(rotation, new float3(0, 0, 3)), UnityEngine.Color.red);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}