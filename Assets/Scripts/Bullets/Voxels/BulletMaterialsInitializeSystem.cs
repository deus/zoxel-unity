using Unity.Entities;
using Zoxel.Voxels;
using Zoxel.Rendering;

namespace Zoxel.Bullets
{
    //! Initializes bullet materials!
    [UpdateInGroup(typeof(BulletSystemGroup))]
    public partial class BulletMaterialsInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null) return;
            var bulletMaterial = MaterialsManager.instance.materials.bulletMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, Bullet>()
                .ForEach((Entity e) =>
            {
                PostUpdateCommands.SetSharedComponentManaged(e, new EntityMaterials(bulletMaterial));
            }).WithoutBurst().Run();
        }
    }
}