using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Bullets
{
    public struct SpawnBullet : IComponentData
    {
        public int id;
        public Entity creator;
        public Entity planet;
        public float3 spawnPosition;
        public quaternion spawnRotation;
        public float attackDamage; // min/max
        // pass in entity for creator instead
        public Entity clan;          // use entity for clan later
        public int creatorID;
        public byte gravityQuadrant;

        public SpawnBullet(Entity creator, Entity planet, float3 spawnPosition, quaternion spawnRotation, float attackDamage,
            Entity clan, int creatorID, byte gravityQuadrant)
        {
            this.id = 0;
            this.creator = creator;
            this.planet = planet;
            this.spawnPosition = spawnPosition;
            this.spawnRotation = spawnRotation;
            this.attackDamage = attackDamage;
            this.clan = clan;
            this.creatorID = creatorID;
            this.gravityQuadrant = gravityQuadrant;
        }
    }
}