using Unity.Entities;

namespace Zoxel.Bullets
{
    public struct DestroyAllBullets : IComponentData { }
}