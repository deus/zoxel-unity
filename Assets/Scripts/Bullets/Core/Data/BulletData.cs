﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Bullets
{
    public struct BulletData
    {
        public float lifetime;
        public float speed;
        public float2 damage;
        public float betweenSpread;
        public float2 spread;
        public float scale;
    }
}