using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;

namespace Zoxel.Bullets
{
    //! Destroys bullets when game ends.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class DestroyAllBulletsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery bullets;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            bullets = GetEntityQuery(
                ComponentType.ReadOnly<Bullet>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var entities = bullets.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<DestroyAllBullets>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                for (int i = 0; i < entities.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, entities[i]);
                }
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
            })  .WithReadOnly(entities)
                .WithDisposeOnCompletion(entities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}