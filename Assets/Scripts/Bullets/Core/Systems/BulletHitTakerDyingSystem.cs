using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Bullets
{
    //! Handles when a character dies using the DyingEntity event.
    [BurstCompile, UpdateInGroup(typeof(BulletSystemGroup))]
	public partial class BulletHitTakerDyingSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DyingEntity, BulletHitTaker>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            { 
                PostUpdateCommands.RemoveComponent<BulletHitTaker>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}