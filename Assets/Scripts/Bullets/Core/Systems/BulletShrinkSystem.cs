﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Bullets
{
    //! Animated bullets shrinking. \todo Replace with animation systems.
    [BurstCompile, UpdateInGroup(typeof(BulletSystemGroup))]
    public partial class BulletShrinkSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
		{
            var deltaTime = World.Time.DeltaTime * 1f;
            var targetValue = float3.zero;
            Entities
                .WithAll<Bullet, DestroyEntityInTime>()
                .ForEach((ref NonUniformScale scale) =>
            {
                scale.Value = math.lerp(scale.Value, targetValue, deltaTime);
            }).ScheduleParallel();
        }
    }
}