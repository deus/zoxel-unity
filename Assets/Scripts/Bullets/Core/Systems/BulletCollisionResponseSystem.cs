using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Movement;

namespace Zoxel.Bullets
{
    //! Handles collision event for bullet colliding with solid voxels.
    [BurstCompile, UpdateInGroup(typeof(BulletSystemGroup))]
    public partial class BulletCollisionResponseSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var deltaTime = World.Time.DeltaTime;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DeadEntity>()
                .WithAll<BasicVoxelCollided, Bullet>()
                .ForEach((Entity e, int entityInQueryIndex, ref Translation translation, in BodyForce bodyForce) => // in BasicVoxelCollider collider, 
            {
                // UnityEngine.Debug.LogError("Bullet Hit Voxel: " + collider.hit.voxelType);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e, new DestroyEntityInTime(elapsedTime, 1));
                PostUpdateCommands.AddComponent<DisableForce>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<DeadEntity>(entityInQueryIndex, e);
                // displaces it outside where it hit? so on the edge of a voxel
                translation.Value -= 2 * bodyForce.velocity * deltaTime;
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}