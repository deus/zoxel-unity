namespace Zoxel
{
    public enum DeviceTypeEditor : byte
    {
		None = 0,
		Keyboard = 1,
		Gamepad = 2,
		Touch = 3,
		Mouse = 4
    }
}