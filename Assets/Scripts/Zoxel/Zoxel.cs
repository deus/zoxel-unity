/**
 * \defgroup Zoxel The Zoxelest Zox
 * @brief Zoxel is much more than it ever could be.
 *
 * A Long doc of Zoxel.
 */

/**
* @brief Zoxel is more than reality ever could be.
* 
*   \todo Move all tag components back to their namespaces.
*   \todo Make all WithoutBurst systems ScheduleParallel. There are still 270 without burst in the project. Only maybe 30 need to be without burst.
*   \todo Finish moving unsorted asmdef's into their proper namespaces.
*   \todo Clean up project by removing warnings in console.
*
* \ingroup Zoxel
*/
namespace Zoxel { }