using System;
using System.IO;
using System.Collections;
using System.Diagnostics;
using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;
using UnityEngine.Rendering;
// using Zoxel.UI;

namespace Zoxel.Capture
{
    public struct ScreenCaptureData
    {
        public NativeArray<byte> buffer;
        public int2 screenDimensions;

        public ScreenCaptureData(int2 screenDimensions)
        {
            this.screenDimensions = screenDimensions;
            var bytesPerPixel = CaptureScreen.instance.bytesPerPixel;
            this.buffer = new NativeArray<byte>(screenDimensions.x * screenDimensions.y * bytesPerPixel, Allocator.Persistent);
        }

        public void Dispose()
        {
            if (buffer.Length > 0)
            {
                buffer.Dispose();
            }
        }

        public byte[] GetPNGArray()
        {
            var experimentalTextureFormat = CaptureScreen.instance.experimentalTextureFormat;
            var encodedBytes = ImageConversion.EncodeNativeArrayToPNG(buffer, experimentalTextureFormat, (uint) screenDimensions.x, (uint) screenDimensions.y);
            var bytesArray = encodedBytes.ToArray();
            encodedBytes.Dispose();
            return bytesArray;
        }

        public void SaveAsPNG(string savepath)
        {
            var bytesArray = GetPNGArray();
            #if WRITE_ASYNC
            File.WriteAllBytesAsync(savepath, bytesArray);
            #else
            File.WriteAllBytes(savepath, bytesArray);
            #endif
        }
    }
}