using System;
using System.IO;
using System.Collections;
using System.Diagnostics;
using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using Zoxel.Audio.Recording;

namespace Zoxel.Capture
{
    //! First prototype at screen capture.
    /**
    *   How To:
    *       f4Key is Gif Recording
    *       f5Key is Video Recording
    *       f6Key is Toggle Voice
    *
    *   Uses PACKAGE_IMAGECONVERSION definition to see if package is installed.
    *
    *   Critical:
    *   \todo Fix voice recording issues
    *   \todo Add Shell Scripts to builds, and run using them instead. Don't rely on any personal folder locations for process.
    *
    *   Marketing:
    *   \todo Automate video uploads to socials
    *   \todo Script that compiles 20 30 second shorts into a 10 min video, with transitions in between.
    *
    *   Hard:
    *   \todo Move this into ECS structures. Textures.
    *   \todo Write png file myself to remove lag and speed up saving.
    *
    *   For End Users:
    *   \todo Check if user has the right libraries installed for processing them.
    *   \todo IL2CPP builds fail to execute terminal for some reason
    *   \todo Add Hotkey in a hotkey manager for users to chose in option a screenshot/recording button
    *
    *   Done:
    *       - Capture image 30 times a second and put it in a folder
    *       - UI Toast to say 'Screenshot Captured' or 'Gif Captured'.
    *       - Add voice recording to the video, make daily 30-60 second tiktok/youtube shorts
    */
    public partial class GifCapturer : MonoBehaviour
    {
        public static GifCapturer instance;
        private const string shellDirectory = "/home/deus/projects/zoxel/automation/gifs/";
        private const string compileGifsFilename = "gif-compile.sh";
        private const string compileVideoFilename = "video-compile.sh";
        private string screenshotsDirectory;
        private string outputDirectory;

        [Header("Settings")]
        public int gifRecordTime = 5;
        public int videoRecordTime = 30;
        public int frameRate = 24;
        public bool isDeleteAfter = true;
        private int recordTime;

        [Header("Lesser")]
        public int startCountDownTime = 3;
        public bool isWatermark = true;
        public bool isDebugShell = false;
        // UI 
        private bool isCountingDown;
        private int countDownTime;
        private string countdownText = "";
        private string toastText = "";
        private bool isSavingPNGs;
        private int pngCount;
        private int gifCount;
        // Data
        private ScreenCaptureData[] screenCaptureDatas;
        private int totalFrames;
        private bool isCapturing = false;
        private float recordStartTime;
        
        [Header("Font")]
        public int fontSize = 34;
        public UnityEngine.Color fontColor = UnityEngine.Color.green;
        public UnityEngine.Font font;

        // [Header("Video")]
        // public bool isRecordVoice = false;

        public EntityManager EntityManager
        {
            get
            { 
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        void Awake()
        {
            instance = this;
            if (NetworkUtil.IsHeadless())
            {
                enabled = false;
            }
        }
        
        private void Update()
        {
            var keyboard = UnityEngine.InputSystem.Keyboard.current;
            if (keyboard == null || isCapturing || toastText != "")
            {
                // UnityEngine.Debug.LogError("GifCapture Failed.");
                return;
            }
            if (keyboard.f4Key.wasPressedThisFrame)
            {
                recordTime = gifRecordTime;
                GifCapturer.instance.CaptureGif();
            }
            else if (keyboard.f5Key.wasPressedThisFrame)
            {
                recordTime = videoRecordTime;
                GifCapturer.instance.CaptureVideo();
            }
            else if (keyboard.f6Key.wasPressedThisFrame)
            {
                if (VoiceRecorder.instance == null)
                {
                    return;
                }
                StartCoroutine(ToggleVoice());
            }
        }

        IEnumerator ToggleVoice()
        {
            VoiceRecorder.instance.enabled = !VoiceRecorder.instance.enabled;
            toastText = "Voice Recorder Set [" + VoiceRecorder.instance.enabled + "]";
            UnityEngine.Debug.Log(toastText);
            yield return new WaitForSeconds(3f);
            toastText = "";
        }

        public void OnGUI()
        {
            SetGUISettings();
            if (isSavingPNGs)
            {
                GUILayout.Label("Saving PNG [" + pngCount + " / " + totalFrames + "]");
            }
            else if (isCountingDown)
            {
                // GUILayout.Label("Capturing Gif in [" + countDownTime + "]");
                GUILayout.Label(countdownText + " in [" + countDownTime + "]");
            }
            else if (toastText != "")
            {
                GUILayout.Label(toastText);
            }
            else if (isCapturing && isWatermark)
            {
                GUILayout.Label("Zoxel [" + ((int)(Time.time - recordStartTime)) + "/" + recordTime + "]");
            }
        }

        void StartRecording()
        {
            isCapturing = true;
            totalFrames = frameRate * recordTime;
            // UnityEngine.Debug.LogError("totalFrames: " + totalFrames + " frameRate: " + frameRate + " recordTime: " + recordTime);
        }

        IEnumerator CountDown()
        {
            isCountingDown = true;
            countDownTime = startCountDownTime;
            for (int i = 3; i >= 0; i--)
            {
                countDownTime = i;
                yield return new WaitForSeconds(0.7f);
            }
            isCountingDown = false;
            // yield return new WaitForSeconds(1);
        }

        private string GetScreenshotPath(int gifCount)
        {
            var fileLabel = BuildOptions.gameName + "-screenshot-";
            var savePath = "" + gifCount;
            if (totalFrames >= 100)
            {
                if (gifCount < 10)
                {
                    savePath = "00" + gifCount;
                }
                else if (gifCount < 100)
                {
                    savePath = "0" + gifCount;
                }
            }
            else if (totalFrames >= 10)
            {
                if (gifCount < 10)
                {
                    savePath = "0" + gifCount;
                }
            }
            savePath = screenshotsDirectory + fileLabel + savePath + ".png";
            // UnityEngine.Debug.LogError(gifCount + " - savePath: " + savePath);
            return savePath;
        }

        IEnumerator StartAsyncCaptureFrames()
        {
            yield return CountDown();
            UnityEngine.Application.targetFrameRate = frameRate;
            InitializeFrameData();
            yield return new WaitForEndOfFrame();
        }

        private void InitializeFrameData()
        {
            var screenDimensions = CaptureScreen.instance.screenDimensions;
            screenCaptureDatas = new ScreenCaptureData[totalFrames];
            for (var i = 0; i < totalFrames; i++)
            {
                var screenCaptureData = new ScreenCaptureData(screenDimensions);
                screenCaptureDatas[i] = screenCaptureData;
            }
        }

        IEnumerator AsyncCaptureFrames()
        {
            // if video, start recording audio
            for (var i = 0; i < totalFrames; i++)
            {
                var screenCaptureData = screenCaptureDatas[i];
                CaptureScreen.instance.JustCaptureFrame(ref screenCaptureData);
                //CaptureScreen.instance.SetData(ref screenCaptureData);
                //yield return StartCoroutine(CaptureScreen.instance.AsyncCaptureFrame(false));   // i == totalFrames - 1
                /*var framesWaited = CaptureScreen.instance.framesWaited;
                if (framesWaited > 1)
                {
                    UnityEngine.Debug.LogError("framesWaited[" + i + "]: " + framesWaited);
                }*/
                yield return new WaitForEndOfFrame();
            }
            yield return new WaitForEndOfFrame();
            UnityEngine.Application.targetFrameRate = 0;
        }

        IEnumerator SaveFrames()
        {
            toastText = "Finished Capture";
            yield return new WaitForEndOfFrame();
            toastText = "";
            isSavingPNGs = true;
            // Save them as pngs
            for (var i = 0; i < totalFrames; i++)
            {
                pngCount = i;
                var filepath = GetScreenshotPath(i);
                var screenCaptureData = screenCaptureDatas[i];
                screenCaptureData.SaveAsPNG(filepath);
                screenCaptureData.Dispose();
                yield return new WaitForEndOfFrame();
            }
            isSavingPNGs = false;
        }

        void CreateScreenshotsDirectory()
        {
            Directory.CreateDirectory(screenshotsDirectory);
        }

        IEnumerator DeleteScreenshotsDirectory()
        {
            // yield return new WaitForSeconds(1f);
            if (isDeleteAfter)
            {
                toastText = "Deleting Temporary Files [" + screenshotsDirectory + "]";
                yield return new WaitForSeconds(1f);
                UnityEngine.Debug.Log(" -> Deleting Process Images Directory [" + screenshotsDirectory + "]");
                System.IO.Directory.Delete(screenshotsDirectory, true);
                // yield return new WaitForSeconds(1f);
            }
            toastText = "";
            isCapturing = false;
        }

        #region gif

        public void CaptureGif()
        {
            StartCoroutine(AsyncCaptureGif());
        }

        void SetGifDirectory()
        {
            outputDirectory = CaptureScreen.GetFolderPathGifs();
            screenshotsDirectory = outputDirectory + BuildOptions.gameName + "-" + CaptureScreen.GetDateString() + "/";
        }

        IEnumerator AsyncCaptureGif()
        {
            StartRecording();
            SetGifDirectory();
            CreateScreenshotsDirectory();
            countdownText = "Capturing Gif";
            yield return StartCoroutine(StartAsyncCaptureFrames());
            yield return StartCoroutine(AsyncCaptureFrames());
            toastText = "Finished Capturing";
            yield return new WaitForSeconds(0.5f);
            yield return StartCoroutine(SaveFrames());
            // yield return StartCoroutine(EndAsyncCaptureFrames());
            yield return StartCoroutine(CompileGif());
            yield return StartCoroutine(DeleteScreenshotsDirectory());
        }

        IEnumerator CompileGif()
        {
            toastText = "Executing Gif Creation";
            if (Directory.Exists(outputDirectory))
            {
                yield return StartCoroutine(ExecuteProcessTerminal(screenshotsDirectory, compileGifsFilename));
            }
        }

        #endregion

        #region video

        public void CaptureVideo()
        {
            StartCoroutine(AsyncCaptureVideo());
        }

        void SetVideoDirectory()
        {
            outputDirectory = CaptureScreen.GetFolderPathVideos();
            screenshotsDirectory = outputDirectory + BuildOptions.gameName + "-" + CaptureScreen.GetDateString() + "/";
        }

        private string voiceFileName;
        IEnumerator AsyncCaptureVideo()
        {
            voiceFileName = "";
            StartRecording();
            SetVideoDirectory();
            CreateScreenshotsDirectory();
            // start recording sound
            countdownText = "Capturing Video";
            yield return StartCoroutine(StartAsyncCaptureFrames());
            recordStartTime = Time.time;
            AudioRecorder.instance.StartRecording(screenshotsDirectory + "audio.wav");
            if (VoiceRecorder.instance && VoiceRecorder.instance.enabled) // isRecordVoice)
            {
                // StartCoroutine(VoiceRecorder.instance.StartRecording());
                VoiceRecorder.instance.StartRecording(recordTime);
            }
            yield return StartCoroutine(AsyncCaptureFrames());
            // finished
            toastText = "Finished Capturing";
            AudioRecorder.instance.StopRecording();
            if (VoiceRecorder.instance && VoiceRecorder.instance.enabled) // isRecordVoice)
            {
                voiceFileName = screenshotsDirectory + "voice.wav";
                while (VoiceRecorder.instance.isRecording)
                {
                    UnityEngine.Debug.LogError("Voice is still recording.");
                    yield return new WaitForEndOfFrame();
                }
                VoiceRecorder.instance.SaveRecording(voiceFileName);
                // yield return new WaitForSeconds(0.5f);
            }
            yield return new WaitForEndOfFrame();
            // yield return new WaitForSeconds(0.5f);
            yield return StartCoroutine(SaveFrames());
            // yield return StartCoroutine(EndAsyncCaptureFrames());
            yield return StartCoroutine(CompileVideo());
            yield return StartCoroutine(DeleteScreenshotsDirectory());
        }

        IEnumerator CompileVideo()
        {
            toastText = "Executing Video Creation";
            if (Directory.Exists(outputDirectory))
            {
                yield return StartCoroutine(ExecuteProcessTerminal(screenshotsDirectory, compileVideoFilename));
            }
        }
        #endregion

        private bool isProcessing;
        // idea - show latest line of terminal as toast ui
        public IEnumerator ExecuteProcessTerminal(string screenshotsDirectory, string shellFilename)
        {
            var arguments = BuildOptions.gameName + " " + CaptureScreen.GetDateString() + " " + screenshotsDirectory;
            if (shellFilename == compileVideoFilename)
            {
                arguments += " " + frameRate;
                if (voiceFileName != "")
                {
                    arguments +=  " " + voiceFileName;
                }
            }
            var startInfo = new ProcessStartInfo()
            {
                UseShellExecute = false,
                CreateNoWindow = true,
                FileName = shellDirectory + shellFilename,
                Arguments = arguments
            };
            if (isDebugShell)
            {
                startInfo.RedirectStandardError = true;
                startInfo.RedirectStandardInput = true;
                startInfo.RedirectStandardOutput = true;
            }
            var process = new Process
            {
                StartInfo = startInfo
            };
            UnityEngine.Debug.Log(" -> Started Processing [" + startInfo.FileName + "]");
            // isProcessing = true;
            // process.Exited += new EventHandler(OnExitedProcess);
            process.Start();
            while (!process.HasExited)
            {
                yield return new WaitForEndOfFrame();
            }
            if (isDebugShell)
            {
                while (!process.StandardOutput.EndOfStream)
                {
                    UnityEngine.Debug.Log(process.StandardOutput.ReadLine());
                }
            }
            UnityEngine.Debug.Log(" -> Finished Processing!");
        }

        private void SetGUISettings()
        {
            if (font)
            {
                GUI.skin.font = font;
            }
            GUI.skin.label.fontSize = fontSize;
            GUI.color = fontColor;
        }
    }
}
        /*

        void OnExitedProcess(object sender, EventArgs e)
        {
            if (p.ExitCode != 0)
            {
                // failed
                StringBuilder args = new StringBuilder();
                args.Append("-s k2smtpout.secureserver.net ");
                args.Append("-f build@example.com ");
                args.Append("-t josh@example.com ");
                args.Append("-a \"Build failed.\" ");
                args.AppendFormat("-m {0} -h", stdout);

                // send email
                StartProcess(BMAIL, args.ToString(), false);
            }
        }*/
            // now calculate totalFrames count
            // var savePaths = new string[totalFrames];
            // Add bytes to list! Encode after! Maybe make into entity.

            // var arguments = "x-terminal-emulator -e convert -delay 10 -loop 0 " + screenshotsDirectory + "*.png " + screenshotsDirectory + "zoxelgif-output.gif";
            /*var arguments = "x-terminal-emulator -e ";
            // var arguments = "nohup -e ";
            arguments += outputDirectory;
            arguments += "compile-gif.sh";
            arguments += " " + screenshotsDirectory;
            UnityEngine.Debug.Log("============== Start Executing Terminal ===============");
            UnityEngine.Debug.Log(arguments);*/

        /*[Header("Texture Data")]
        public RenderTexture initialRenderTexture;
        public RenderTexture renderTexture;
        private NativeArray<byte> processedData;
        private NativeArray<byte> rawData;*/
        // private Vector2 captureScale = new Vector2(1, -1);
        // private Vector2 captureOffset = new Vector2(0, 1);
        // private string savePath;
        //public BinaryWriter stream;
        // private byte[] pngBytes = new byte[0];

    /*for (gifCount = 0; gifCount < totalFrames; gifCount++)
    {
        var gifTexture = textures[gifCount];
        File.WriteAllBytes(savePaths[gifCount], ImageConversion.EncodeToPNG(gifTexture));
        UnityEngine.GameObject.Destroy(gifTexture);
        yield return new WaitForEndOfFrame();
    }*/

    /*void OnCompleteReadback(AsyncGPUReadbackRequest asyncGPUReadbackRequest)
    {
        // get screenshot data as nativearray or handle error
        if (asyncGPUReadbackRequest.hasError)
        {
            UnityEngine.Debug.LogError("Error Capturing Screenshot: With AsyncGPUReadbackRequest.");
            return;
        }
        rawData = asyncGPUReadbackRequest.GetData<byte>();
        isCapturingFrame = false;
        // todo: hook this up with my twitter.py script
    }*/

    /*[BurstCompile]
    public struct CaptureScreenJob2 : IJob
    {
        // [NativeDisableParallelForRestriction] 
        public NativeArray<byte> encodedBytes;
        [ReadOnly] public NativeArray<byte> captureBuffer;
        [ReadOnly] public byte graphicsFormat;
        [ReadOnly] public int width;
        [ReadOnly] public int height;

        public void Execute()
        {
            encodedBytes = ImageConversion.EncodeNativeArrayToPNG(captureBuffer,
                (UnityEngine.Experimental.Rendering.GraphicsFormat) graphicsFormat,
                (uint) width, (uint) height);
        }
    }*/

        /*void OnDestroy()
        {
            AsyncGPUReadback.WaitAllRequests();
            if (stream != null)
            {
                stream.Close();
                stream.Dispose();
            }
            if (initialRenderTexture != null)
            {
                initialRenderTexture.Release();
                renderTexture.Release();
            }
            if (initialRenderTexture)
            {
                UnityEngine.GameObject.Destroy(initialRenderTexture);
            }
            if (renderTexture)
            {
                UnityEngine.GameObject.Destroy(renderTexture);
            }
            if (captureBuffer.Length > 0)
            {
                captureBuffer.Dispose();
            }
        }*/

        /*void OnScreenshot(AsyncGPUReadbackRequest asyncGPUReadbackRequest)
        {
            if (asyncGPUReadbackRequest.hasError)
            {
                UnityEngine.Debug.LogError("Error Capturing Screenshot: With AsyncGPUReadbackRequest.");
                return;
            }
            var dateTime = DateTime.Now;
            var savePath = screenshotsDirectory + "zoxelscreen_" 
                + dateTime.ToString("-yyyy-MM-dd_") + dateTime.Hour + "h-" + dateTime.Minute + "m" + dateTime.Second + "s"
                + ".png";
            var stream = new BinaryWriter(File.OpenWrite(savePath));
            var encodedBytes = ImageConversion.EncodeNativeArrayToPNG(captureBuffer, initialRenderTexture.graphicsFormat,
                (uint) width, (uint) height);
            for (int i = 0; i < encodedBytes.Length; i++)
            {
                stream.Write(encodedBytes[i]);
            }
            stream.Close();
            stream.Dispose();
            isCapturingFrame = false;
        }*/
            /*var textures = new Texture2D[totalFrames];
            for (gifCount = 0; gifCount < totalFrames; gifCount++)
            {
                var texture = new Texture2D(width, height, TextureFormat.RGBA32, false);
                textures[gifCount] = texture;
            }*/
                /*isCapturingFrame = true;
                var rt = RenderTexture.GetTemporary(Screen.width, Screen.height, 0, RenderTextureFormat.ARGB32);
                #if PACKAGE_IMAGECONVERSION
                ScreenCapture.CaptureScreenshotIntoRenderTexture(rt);
                #endif
                AsyncGPUReadback.Request(rt, 0, TextureFormat.RGBA32, OnCompleteReadback);
                RenderTexture.ReleaseTemporary(rt);
                while (isCapturingFrame)
                {
                    yield return new WaitForEndOfFrame();
                }
                processedData = textures[gifCount].GetRawTextureData<byte>();
                var job = new CaptureScreenJob
                {
                    processedData = processedData,
                    rawData = rawData,
                    width = width,
                    height = height
                };
                // var jobHandle = job.ScheduleParallel(null); //  rawData.Length, 64);   // 1
                var jobHandle = job.Schedule(rawData.Length / 4, 1024);   // 1
                // jobHandle.Complete();
                while (!jobHandle.IsCompleted)
                {
                    yield return new WaitForEndOfFrame();
                }
                // create texture and save as png using datetime
                // textures[gifCount] = texture;
                savePaths[gifCount] = savePath;
                // Replace with other function? Stream Writer?
                // File.WriteAllBytes(savePath, ImageConversion.EncodeToPNG(texture));
                */

/*
    var encoded = ImageConversion.EncodeNativeArrayToPNG
    (_buffer, _rt.flip.graphicsFormat,
    (uint)_rt.flip.width, (uint)_rt.flip.height);
*/

//var rt = RenderTexture.GetTemporary(Screen.width, Screen.height, 0, RenderTextureFormat.ARGB32);
//ScreenCapture.CaptureScreenshotIntoRenderTexture(rt);
//AsyncGPUReadback.Request(rt, 0, TextureFormat.RGBA32, OnCompleteReadback);
/*var addition = index % 4;
var arrayIndex = index / 4;
var flippedIndex = (arrayIndex % width) + (height - 1 - (arrayIndex / width)) * width;
processedData[index + addition] = rawData[flippedIndex * 4 + addition];*/
// processedData[index] = rawData[index];
/*
var pixelArrayIndex = index; //  / 4;
var x = pixelArrayIndex % width;
var y = pixelArrayIndex / width;
var flippedY = (height - 1 - y);
var flippedIndex = x + flippedY * width;*/

// var rawData = asyncGPUReadbackRequest.GetData<uint>();
//var processedData = new NativeArray<byte>(rawData.Length, Allocator.Temp);
// UnityEngine.Debug.LogError("Dimension: " + width + "x" + height + ": " + rawData.Length + " - " + (width * height));
// texture.LoadRawTextureData(processedData);
// texture.LoadRawTextureData(rawData);
// var processedData = new NativeArray<uint>(rawData.Length, Allocator.Temp);
// ARGB32
/*for (int i = 0; i < )
for (int i = 0; i < width; i++)
{
    for (int j = 0; j < height; j++)
    {
        var flippedJ = (height - 1 - j);
        var flippedIndex = flippedJ + i * height;
        processedData[normalIndex] = rawData[flippedIndex];
        normalIndex++;
    }
}*/

//  = "/home/deus/Pictures/Screenshots/screenshot";


            // Grab screen dimensions
            // now flip vertical pixels
            /*for (int i = 0; i < rawData.Length; i += 4)
            {
                var arrayIndex = i / 4;
                var x = arrayIndex % width;
                var y = arrayIndex / width;
                var flippedY = (height - 1 - y);
                var flippedIndex = x + flippedY * width;
                // flip the data
                processedData[i] = rawData[flippedIndex * 4];
                processedData[i + 1] = rawData[flippedIndex * 4 + 1];
                processedData[i + 2] = rawData[flippedIndex * 4 + 2];
                processedData[i + 3] = rawData[flippedIndex * 4 + 3];
            }*/
            /*for (int i = 0; i < rawData.Length; i++)
            {
                processedData[i] = rawData[i];
            }*/
        
            /*if (captureBuffer2.Length != arraySize)
            {
                captureBuffer2 = new NativeArray<byte>(arraySize, Allocator.Persistent); //, NativeArrayOptions.UninitializedMemory);
            }*/
            // if (initialRenderTexture != null)
                //initialRenderTexture = RenderTexture.GetTemporary(width, height, 0, RenderTextureFormat.ARGB32);
                //renderTexture = RenderTexture.GetTemporary(width, height, 0, RenderTextureFormat.ARGB32);
            //initialRenderTexture = RenderTexture.GetTemporary(width, height, 0, RenderTextureFormat.ARGB32);
            //renderTexture = RenderTexture.GetTemporary(width, height, 0, RenderTextureFormat.ARGB32);
            //RenderTexture.ReleaseTemporary(initialRenderTexture);
            //RenderTexture.ReleaseTemporary(renderTexture);
            /*ScreenCapture.CaptureScreenshotIntoRenderTexture(initialRenderTexture);
            // this flips it, how efficient is it? should be done on shader?
            
            Graphics.Blit(initialRenderTexture, renderTexture, captureScale, captureOffset);
            //AsyncGPUReadback.RequestIntoNativeArray(ref captureBuffer, renderTexture, 0, TextureFormat.RGBA32, OnScreenshot);
            AsyncGPUReadback.Request(renderTexture, 0, TextureFormat.RGBA32, OnScreenshot);*/

            // AsyncGPUReadback.RequestIntoNativeArray(ref captureBuffer, initialRenderTexture, 0, TextureFormat.RGBA32, OnScreenshot);

            // AsyncGPUReadback.RequestIntoNativeArray(ref captureBuffer, initialRenderTexture, 0, TextureFormat.RGBA32);
            // AsyncGPUReadback.Request(rt, 0, TextureFormat.RGBA32, OnCompleteReadback);
            //RenderTexture.ReleaseTemporary(initialRenderTexture);
            //RenderTexture.ReleaseTemporary(renderTexture);
            /*var texture = new Texture2D(width, height, TextureFormat.RGBA32, false);
            processedData = texture.GetRawTextureData<byte>();
            var job = new CaptureScreenJob
            {
                processedData = processedData,
                rawData = rawData,
                width = width,
                height = height
            };
            // var jobHandle = job.ScheduleParallel(null); //  rawData.Length, 64);   // 1
            var jobHandle = job.Schedule(rawData.Length / 4, 1024);   // 1
            while (!jobHandle.IsCompleted)
            {
                yield return new WaitForEndOfFrame();
            }
            // jobHandle.Complete();
            // create texture and save as png using datetime
            var dateTime = DateTime.Now;
            var savePath = screenshotsDirectory + "zoxelscreen_" 
                + dateTime.ToString("-yyyy-MM-dd_") + dateTime.Hour + "h-" + dateTime.Minute + "m" + dateTime.Second + "s"
                + ".png";
            File.WriteAllBytes(savePath, ImageConversion.EncodeToPNG(texture));
            Destroy(texture, 1f);*/
            
            /*var job = new CaptureScreenJob2
            {
                encodedBytes = new NativeArray<byte>(1184393, Allocator.TempJob), // width * height, Allocator.TempJob),
                captureBuffer = captureBuffer2,
                graphicsFormat = (byte) initialRenderTexture.graphicsFormat,
                width = width,
                height = height
            };
            var jobHandle = job.Schedule(); // Schedule(1, 32);   // 1
            jobHandle.Complete();
            var encodedBytes = job.encodedBytes;*/

            // stream.Write(encodedBytes.AsArray().ToArray());
            // job.captureBuffer.Dispose();


            // rawData = asyncGPUReadbackRequest.GetData<byte>();

            // captureBuffer.Dispose();

            /*captureBuffer.Dispose();
            captureBuffer = asyncGPUReadbackRequest.GetData<byte>();
            var texture = new Texture2D(width, height, TextureFormat.RGBA32, false);
            var processedData = texture.GetRawTextureData<byte>();
            for (int i = 0; i < processedData.Length; i++)
            {
                processedData[i] = captureBuffer[i];
            }
            var pngBytes = ImageConversion.EncodeToPNG(texture);*/

			//UnsafeUtility.MemCpy(pngBytes,
            //    NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(encodedBytes), encodedBytes.Length);

			/*if (pngBytes.Length != encodedBytes.Length)
			{
				pngBytes = new byte[encodedBytes.Length];
			}
            encodedBytes.CopyTo(pngBytes);
            stream.Write(pngBytes, 0, pngBytes.Length);*/

            // UnsafeUtility.CopyPtrToStructure(NativeArrayUnsafeUtility.GetUnsafeBufferPointerWithoutChecks(encodedBytes), out pngBytes);
            /*for (int i = 0; i < encodedBytes.Length; i++)
            {
				pngBytes[i] = encodedBytes[i];
            }*/
            // encodedBytes.ReadIntoArray(ref pngBytes);

            //System.IO.File.WriteAllBytes(savePath, pngBytes);