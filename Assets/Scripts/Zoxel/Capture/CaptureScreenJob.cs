/*using UnityEngine;
using UnityEngine.Rendering;
using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using System;
using System.IO;
using System.Collections;

namespace Zoxel.Capture
{
    [BurstCompile]
    public struct CaptureScreenJob : IJobParallelFor
    {
        [NativeDisableParallelForRestriction] public NativeArray<byte> processedData;
        [ReadOnly] public NativeArray<byte> rawData;
        [ReadOnly] public int width;
        [ReadOnly] public int height;

        public void Execute(int index)
        {
            var byteArrayIndex = index * 4;
            // flip the data
            var flippedIndex = (index % width) +  (height - 1 - (index / width)) * width;
            processedData[byteArrayIndex] = rawData[flippedIndex * 4];
            processedData[byteArrayIndex + 1] = rawData[flippedIndex * 4 + 1];
            processedData[byteArrayIndex + 2] = rawData[flippedIndex * 4 + 2];
            processedData[byteArrayIndex + 3] = rawData[flippedIndex * 4 + 3];
        }
    }
}*/