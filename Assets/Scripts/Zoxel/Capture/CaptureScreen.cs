using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering;
using System;
using System.IO;
using System.Collections;
using System.Reflection;

namespace Zoxel.Capture
{
    //! First prototype at screen capture.
    /**
    *   How To:
    *       f3Key is Screenshot
    *
    *   Uses PACKAGE_IMAGECONVERSION definition to see if package is installed.
    *   \todo Move this into ECS structures.
    *   \todo UI Toast to say 'Screenshot Captured' or 'Gif Captured'.
    *   \todo Capture image 30 times a second and put it in a folder
    */
    public partial class CaptureScreen : MonoBehaviour
    {
        public static CaptureScreen instance;

        [Header("File")]
        private string saveDirectory;
        private string screenshotDirectory;

        [Header("Format")]
        public int bytesPerPixel = 3;
        public RenderTextureFormat renderTextureFormat = RenderTextureFormat.ARGB32;
        public GraphicsFormat experimentalTextureFormat = GraphicsFormat.R8G8B8A8_SRGB;
        public TextureFormat textureFormat = TextureFormat.ARGB32;
        public int2 screenDimensions;
        public Vector2 captureScale = new Vector2(1, -1);
        public Vector2 captureOffset = new Vector2(0, 1);
        private ScreenCaptureData screenCaptureData;
        // Coroutine cache
        private WaitForEndOfFrame delay = new WaitForEndOfFrame();
        private WaitForSeconds delay2 = new WaitForSeconds(1f);
        private WaitForSeconds delay3 = new WaitForSeconds(4f);

        [Header("Debug")]
        public RenderTexture initialRenderTexture;
        public RenderTexture renderTexture;
        
        [Header("Font")]
        public int fontSize = 34;
        public UnityEngine.Color fontColor = UnityEngine.Color.green;
        public UnityEngine.Font font;

        // UI 
        private bool isCapturing = false;
        private bool isRequestGPUReadback = false;
        private bool isToastUI;
        private string toastText;
        private bool isCountingDown;
        private int countDownTime;

        public static string GetDateString()
        {
            var dateTime = DateTime.Now;
            return dateTime.ToString("yyyy-MM-dd_") + dateTime.Hour + "h-" + dateTime.Minute + "m" + dateTime.Second + "s";
        }

        public static string GetFolderPathGifs()
        {
            var gameName = BuildOptions.gameName;
            return CaptureScreen.GetPicturesFolder() + "/" + gameName + "/gifs/";
        }

        public static string GetFolderPathVideos()
        {
            var gameName = BuildOptions.gameName;
            return CaptureScreen.GetPicturesFolder() + "/" + gameName + "/videos/";
        }
        

        public static string GetGameFolderPath()
        {
            var gameName = BuildOptions.gameName;
            return CaptureScreen.GetPicturesFolder() + "/" + gameName;
        }

        public static string GetPicturesFolder()
        {
            return System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/zoxel-capture";
        }

        public EntityManager EntityManager
        {
            get
            { 
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        public static string ScreenshotDirectory
        {
            get
            {
                return instance.screenshotDirectory;
            }
        }

        void Awake()
        {
            instance = this;
            if (NetworkUtil.IsHeadless())
            {
                enabled = false;
                return;
            }
            screenshotDirectory = GetGameFolderPath(); //  GetPicturesFolder() + "/" + gameName;
            UnityEngine.Debug.Log("Screenshot Directory Path: " + screenshotDirectory);
            var directoryExists = Directory.Exists(screenshotDirectory);
            if (!directoryExists)
            {
                UnityEngine.Debug.Log("Screenshot Folder 1 did not exist: " + screenshotDirectory);
                Directory.CreateDirectory(screenshotDirectory);
            }
            var screenshotDirectory2 = screenshotDirectory + "/screenshots";
            var directory2Exists = Directory.Exists(screenshotDirectory2);
            if (!directory2Exists)
            {
                UnityEngine.Debug.Log("Screenshot Folder 2 did not exist: " + screenshotDirectory2);
                Directory.CreateDirectory(screenshotDirectory2);
            }
            saveDirectory = screenshotDirectory2 + "/";
            var gifsDirectory = screenshotDirectory + "/gifs";
            var gifsDirectoryExists = Directory.Exists(gifsDirectory);
            if (!gifsDirectoryExists)
            {
                UnityEngine.Debug.Log("Gifs Directory did not exist: " + gifsDirectory);
                Directory.CreateDirectory(gifsDirectory);
            }
            var videosDirectory = screenshotDirectory + "/videos";
            var videosDirectoryExists = Directory.Exists(videosDirectory);
            if (!videosDirectoryExists)
            {
                UnityEngine.Debug.Log("Videos Directory did not exist: " + videosDirectory);
                Directory.CreateDirectory(videosDirectory);
            }
        }

        public void OnGUI()
        {
            SetGUISettings();
            if (isCountingDown)
            {
                GUILayout.Label("Capturing Screenshot in [" + countDownTime + "]");
            }
            else if (isToastUI)
            {
                GUILayout.Label(toastText);
            }
        }
        
        private void Update()
        {
            if (isCapturing)
            {
                return;
            }
            UpdateRenderTextures();
            if (UnityEngine.InputSystem.Keyboard.current != null)
            {
                if (UnityEngine.InputSystem.Keyboard.current.f3Key.wasPressedThisFrame)
                {
                    CaptureScreen.instance.Capture();
                }
            }
        }

        void UpdateRenderTextures()
        {
            var newDimensions = new int2(Screen.width, Screen.height);
            if (newDimensions != screenDimensions)
            {
                screenDimensions = newDimensions;
                UnityEngine.Debug.Log("Resized Render Textures: " + screenDimensions);
                //#if UNITY_EDITOR
                //    Debug.Log("    GameView Scale: " + scale);
                //#endif
                DestroyRenderTextures();
                InitializeRenderTextures();
            }
        }

        void InitializeRenderTextures()
        {
            initialRenderTexture = new RenderTexture(screenDimensions.x, screenDimensions.y, 0, renderTextureFormat);
            initialRenderTexture.Create();
            renderTexture = new RenderTexture(screenDimensions.x, screenDimensions.y, 0, renderTextureFormat);
            renderTexture.Create();
            screenCaptureData = new ScreenCaptureData(screenDimensions);
            // buffer = new NativeArray<byte>(screenDimensions.x * screenDimensions.y * bytesPerPixel, Allocator.Persistent);  // , NativeArrayOptions.UninitializedMemory);
        }

        private void OnDestroy()
        {
            AsyncGPUReadback.WaitAllRequests();
            DestroyRenderTextures();
        }

        private void DestroyRenderTextures()
        {
            if (initialRenderTexture != null)
            {
                initialRenderTexture.Release();
                renderTexture.Release();
            }
            if (initialRenderTexture)
            {
                Destroy(initialRenderTexture);
            }
            if (renderTexture)
            {
                Destroy(renderTexture);
            }
            screenCaptureData.Dispose();
        }

        private void Capture()
        {
            StartCoroutine(AsyncCapture());
        }

        private IEnumerator AsyncCapture()
        {
            if (initialRenderTexture == null)
            {
                UnityEngine.Debug.LogError("initialRenderTexture is null.");
                yield break;
            }
            isCapturing = true;
            isCountingDown = true;
            for (int i = 0; i < 3; i++)
            {
                countDownTime = 3 - i;
                yield return delay2;
            }
            isCountingDown = false;
            yield return delay;
            SetData(ref screenCaptureData);
            yield return StartCoroutine(AsyncCaptureFrame());
            var savepath = saveDirectory + "zoxelscreen_" + GetDateString() + ".png";
            screenCaptureData.SaveAsPNG(savepath);
            isToastUI = true;
            toastText = "Screenshot Saved to [" + savepath + "] Size [" + screenDimensions.x + "x" + screenDimensions.y + "]";
            // UnityEngine.Debug.Log("Screenshot took " + framesWaiting + " frames.");
            yield return delay3;
            isToastUI = false;
            isCapturing = false;
        }

        public ScreenCaptureData tempScreenCaptureData;

        public void SetData(ref ScreenCaptureData tempScreenCaptureData)
        {
            this.tempScreenCaptureData = tempScreenCaptureData;
        }

        public void JustCaptureFrame(ref ScreenCaptureData screenCaptureData)  // ref ScreenCaptureData screenCaptureData
        {
            ScreenCapture.CaptureScreenshotIntoRenderTexture(initialRenderTexture); // Grabs pixels into our initial render texture
            Graphics.Blit(initialRenderTexture, renderTexture, captureScale, captureOffset);    // Flips and scales render texture
            var buffer = screenCaptureData.buffer;
            AsyncGPUReadback.RequestIntoNativeArray(ref buffer, renderTexture, 0, textureFormat, OnScreenshot);
        }

        public int framesWaited;
        public IEnumerator AsyncCaptureFrame(bool isWait = true)  // ref ScreenCaptureData screenCaptureData
        {
            ScreenCapture.CaptureScreenshotIntoRenderTexture(initialRenderTexture); // Grabs pixels into our initial render texture
            Graphics.Blit(initialRenderTexture, renderTexture, captureScale, captureOffset);    // Flips and scales render texture
            isRequestGPUReadback = true;
            var buffer = tempScreenCaptureData.buffer;
            AsyncGPUReadback.RequestIntoNativeArray(ref buffer, renderTexture, 0, textureFormat, OnScreenshot);
            framesWaited = 0;
            if (!isWait)
            {
                // yield return delay;
                yield break;
            }
            while (isRequestGPUReadback)
            {
                framesWaited++;
                yield return delay;
            }
        }

        void OnScreenshot(AsyncGPUReadbackRequest asyncGPUReadbackRequest)
        {
            isRequestGPUReadback = false;
            if (asyncGPUReadbackRequest.hasError)
            {
                Debug.LogError("Error Capturing Screenshot: With AsyncGPUReadbackRequest");
            }
        }

        private void SetGUISettings()
        {
            if (font)
            {
                GUI.skin.font = font;
            }
            GUI.skin.label.fontSize = fontSize;
            GUI.color = fontColor;
        }
    }
}
            /*if (UnityEngine.InputSystem.Keyboard.current.f2Key.wasPressedThisFrame)
            {
                var renderUISystem = EntityManager.World.GetOrCreateSystem<UIRenderSystem>();
                renderUISystem.Enabled = !renderUISystem.Enabled;
            }*/
        
        //! Saves our texture buffer
        /*private void SaveBufferAsPNG(string savepath, in ScreenCaptureData screenCaptureData)
        {
            UnityEngine.Profiling.Profiler.BeginSample("ImageConversion.EncodeNativeArrayToPNG.X");
            var encodedBytes = ImageConversion.EncodeNativeArrayToPNG(buffer, experimentalTextureFormat, (uint) screenDimensions.x, (uint) screenDimensions.y);
            var bytesArray = encodedBytes.AsArray().ToArray();
            encodedBytes.Dispose();
            UnityEngine.Profiling.Profiler.EndSample();
            #if WRITE_ASYNC
            File.WriteAllBytesAsync(savepath, bytesArray);
            #else
            File.WriteAllBytes(savepath, bytesArray);
            #endif
        }*/

// var buffer = new NativeArray<byte>(screenDimensions.x * screenDimensions.y * bytesPerPixel, Allocator.Persistent, NativeArrayOptions.UninitializedMemory);
// buffer.Dispose();

// Tested this way, it is slower!
/*var stream = new BinaryWriter(File.OpenWrite(savepath));
for (int i = 0; i < encodedBytes.Length; i++)
{
    stream.Write(encodedBytes[i]);
}
stream.Close();
stream.Dispose();*/


// var encodedBytes = ImageConversion.EncodeToPNG(buffer.AsArray().ToArray());
// var texture = new Texture2D(width, height, textureFormat, false);
// var texture = ScreenCapture.CaptureScreenshotAsTexture();
/*// Graphics.CopyTexture(renderTexture, texture);
// Graphics.CopyTexture(initialRenderTexture, texture);
// var savepath = saveDirectory + "zoxelscreen_" + GetDateString() + ".png";
// var encodedBytes = ImageConversion.EncodeToPNG(texture);
// System.IO.File.WriteAllBytes(savepath, encodedBytes);
if (debugTexture != null)
    Destroy(debugTexture);
debugTexture = texture;*/

//#if PACKAGE_IMAGECONVERSION
/*#else
UnityEngine.Debug.LogError("Add PACKAGE_IMAGECONVERSION to save.");
#endif*/
/*#if UNITY_EDITOR
var size = GetGameViewSize();
width = size.x;
height = size.y;
UnityEngine.Debug.LogError("Game View Size: " + size);
#endif*/

/*for (int i = 0; i < buffer.Length; i += 4)
{
    if (buffer[i] != 255)
    {
        UnityEngine.Debug.LogError("Alpha issues: " + i);
    }
    buffer[i] = 255;
}*/

//#if UNITY_EDITOR
// var scale = GetGameViewScale();
// newDimensions.x = (int) (newDimensions.x * scale.x);
// newDimensions.y = (int) (newDimensions.y * scale.y);
//#endif