using Unity.Mathematics;

namespace Zoxel // .Voxels
{
    public static class VoxelUtilities
    {
        public static int GetVoxelArrayIndex(byte3 position, int3 size)
        {
            return (position.z + size.z * (position.y + size.y * position.x));
        }
        
        public static int GetVoxelArrayIndex(int3 position, int3 size)
        {
            return (position.z + size.z * (position.y + size.y * position.x));
        }

        public static int GetVoxelArrayIndexXY(int3 position, int3 size)
        {
            return position.y + size.y * position.x;
        }

        public static int GetVoxelArrayIndexYZ(int3 position, int3 size)
        {
            return (position.z + size.z * position.y);
        }

        public static int GetVoxelArrayIndexXZ(int3 position, int3 size)
        {
            return position.z + size.z * position.x;
        }
       
        public static int GetTextureArrayIndexXY(int2 position, int2 size)
        {
            return position.x + size.x * position.y;
        }
       
        public static int GetTextureArrayIndexXY(int3 position, int3 size)
        {
            return position.x + size.x * position.y;
        }
		

		public static void SetAsLocalPosition(ref int3 localPosition, int3 voxelDimensions)
		{
			if (localPosition.x < 0)
			{
				localPosition.x += voxelDimensions.x;
			}
			else if (localPosition.x >= voxelDimensions.x)
			{
				localPosition.x -= voxelDimensions.x; // = 0;
			}
			if (localPosition.z < 0)
			{
				localPosition.z += voxelDimensions.z;
			}
			else if (localPosition.z >= voxelDimensions.z)
			{
				localPosition.z -= voxelDimensions.z;
			}
			if (localPosition.y < 0)
			{
				localPosition.y += voxelDimensions.y;
			}
			else if (localPosition.y >= voxelDimensions.y)
			{
				localPosition.y -= voxelDimensions.y;
			}
		}


        public static int3 GetLocalPosition(int3 voxelWorldPosition, int3 voxelDimensions)
        {
            if (voxelDimensions.x == 0 || voxelDimensions.y == 0 || voxelDimensions.z == 0)
            {
                return new int3();
            }
            var chunkPosition = GetChunkPosition(voxelWorldPosition, voxelDimensions);
            var localPosition = voxelWorldPosition - GetChunkVoxelPosition(chunkPosition, voxelDimensions);
            return localPosition;
        }

        public static int3 GetLocalPosition(int3 voxelWorldPosition, int3 chunkPosition, int3 voxelDimensions)
        {
            if (voxelDimensions.x == 0 || voxelDimensions.y == 0 || voxelDimensions.z == 0)
            {
                return new int3();
            }
            /*var localPosition = voxelWorldPosition;
            if (localPosition.x >= 0)
            {
                localPosition.x %= voxelDimensions.x;
            }
            else
            {
                localPosition.x = (-chunkPosition.x * voxelDimensions.x + localPosition.x) % voxelDimensions.x;
            }
            if (localPosition.z >= 0)
            {
                localPosition.z %= voxelDimensions.z;
            }
            else
            {
                localPosition.z = (-chunkPosition.z * voxelDimensions.z + localPosition.z) % voxelDimensions.z;
            }
            if (localPosition.y >= 0)
            {
                // localPosition.y %= voxelDimensions.y;
                localPosition.y = (-chunkPosition.y * voxelDimensions.y + localPosition.y); //  % voxelDimensions.y;
            }
            else
            {
                localPosition.y = (-chunkPosition.y * voxelDimensions.y + localPosition.y) % voxelDimensions.y;
            }*/
            var localPosition = voxelWorldPosition - GetChunkVoxelPosition(chunkPosition, voxelDimensions);
            return localPosition;
        }

        public static int3 GetChunkVoxelPosition(int3 chunkPosition, int3 voxelDimensions)
        {
            return new int3(
                chunkPosition.x * voxelDimensions.x,
                chunkPosition.y * voxelDimensions.y, 
                chunkPosition.z * voxelDimensions.z);
        }
        
        public static int3 GetChunkPosition(int3 voxelWorldPosition, int3 voxelDimensions)
        {
            if (voxelDimensions.x == 0 || voxelDimensions.y == 0 || voxelDimensions.z == 0)
            {
                voxelDimensions = new int3(16, 16, 16);
            }
            var chunkPosition = voxelWorldPosition;
            chunkPosition.x = GetChunkPositionInt(voxelWorldPosition.x, voxelDimensions.x);
            chunkPosition.y = GetChunkPositionInt(voxelWorldPosition.y, voxelDimensions.y);
            chunkPosition.z = GetChunkPositionInt(voxelWorldPosition.z, voxelDimensions.z);
            /*if (voxelWorldPosition.x < 0)
            {
                chunkPosition.x++;
                chunkPosition.x /= voxelDimensions.x;
                chunkPosition.x--;
            }
            else
            {
                chunkPosition.x /= voxelDimensions.x;
            }
            if (voxelWorldPosition.z < 0)
            {
                chunkPosition.z++;
                chunkPosition.z /= voxelDimensions.z;
                chunkPosition.z--;
            }
            else
            {
                chunkPosition.z /= voxelDimensions.z;
            }
            if (voxelWorldPosition.y < 0)
            {
                chunkPosition.y++;
                chunkPosition.y /= voxelDimensions.y;
                chunkPosition.y--;
            }
            else
            {
                chunkPosition.y /= voxelDimensions.y;
            }*/
            return chunkPosition;
        }

        public static int GetChunkPositionInt(int position, int voxelDimensions)
        {
            if (position < 0)
            {
                position++;
                position /= voxelDimensions;
                position--;
            }
            else
            {
                position /= voxelDimensions;
            }
            return position;
        }
        
        public static int3 GetVoxelPosition(float3 worldPosition, float3 voxScale)
        {
            worldPosition.x /= voxScale.x;
            worldPosition.y /= voxScale.y;
            worldPosition.z /= voxScale.z;
            return new int3((int)math.floor(worldPosition.x), (int)math.floor(worldPosition.y), (int)math.floor(worldPosition.z));
        }

        public static float3 GetRealPosition(int3 voxelPosition, float3 voxScale)
        {
            var realPosition = voxelPosition.ToFloat3() + new float3(0.5f, 0.5f, 0.5f);
            realPosition.x *= voxScale.x;
            realPosition.y *= voxScale.y;
            realPosition.z *= voxScale.z;
            return realPosition;
        }
        
        /*public static int3 GetVoxelPosition(float3 worldPosition)
        {
            return new int3((int)math.floor(worldPosition.x), (int)math.floor(worldPosition.y), (int)math.floor(worldPosition.z));
        }
        
        public static int3 WorldPositionToVoxelPosition2(float3 worldPosition)
        {
            return new int3((int)(worldPosition.x), (int)(worldPosition.y), (int)(worldPosition.z));
        }*/

        public static int3 GetMegaChunkPosition(int3 chunkPosition2, int megaChunkDivision)
        {
            var chunkPosition = chunkPosition2;
            // offset by half
            // chunkPosition += new int3(megaChunkDivision / 2, 0, megaChunkDivision / 2);
            if (chunkPosition.x < 0)
            {
                chunkPosition.x++;
                chunkPosition.x /= megaChunkDivision;
                chunkPosition.x--;
            }
            else
            {
                chunkPosition.x /= megaChunkDivision;
            }
            if (chunkPosition.y < 0)
            {
                chunkPosition.y++;
                chunkPosition.y /= megaChunkDivision;
                chunkPosition.y--;
            }
            else
            {
                chunkPosition.y /= megaChunkDivision;
            }
            if (chunkPosition.z < 0)
            {
                chunkPosition.z++;
                chunkPosition.z /= megaChunkDivision;
                chunkPosition.z--;
            }
            else
            {
                chunkPosition.z /= megaChunkDivision;
            }
            return chunkPosition;
        }

        public static int3 MegaChunkToChunkPosition(int3 megaChunkPosition, int megaChunkDivision)
        {
            return new int3(
                (int) (megaChunkPosition.x * megaChunkDivision + 0.5f * megaChunkDivision), 
                (int) (megaChunkPosition.y * megaChunkDivision), 
                (int) (megaChunkPosition.z * megaChunkDivision + 0.5f * megaChunkDivision));
        }

        public static bool InBounds(int3 voxelPosition, int3 voxelDimension)
        {
            return voxelPosition.x >= 0 && voxelPosition.x < voxelDimension.x
                && voxelPosition.y >= 0 && voxelPosition.y < voxelDimension.y
                && voxelPosition.z >= 0 && voxelPosition.z < voxelDimension.z;
        }
    }
}