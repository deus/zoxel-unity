using Unity.IO.LowLevel.Unsafe;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using System;
using System.IO;

namespace Zoxel
{
    public static class AsyncReadState
    {
        public const byte StartReadFileInfo = 0;
        public const byte WaitReadFileInfo = 1;
        public const byte StartOpenFile = 2;
        public const byte WaitOpenFile = 3;
        public const byte Complete = 4;
    }
    #if DISABLE_ASYNC_FILE_LOADING
    //! Loads files asynchronously.
    public unsafe struct AsyncFileReader
    {
        public byte state;
        public BlitableArray<byte> data;
        private bool didFileExist;

		public int Length
        {
            get
            { 
                return data.Length;
            }
        }

		unsafe public byte this[int index]
		{
			get
			{
                return data[index];
			}
		}

        public BlitableArray<byte> GetBytes()
        {
            return data;
        }

        public void UpdateOnMainThread(in Text filepath)
        {
            if (state == AsyncReadState.StartReadFileInfo)
            {
                state = AsyncReadState.WaitReadFileInfo;
                var filepath2 = filepath.ToString();
                if (File.Exists(filepath2))
                {
                    var array = File.ReadAllBytes(filepath2);
                    data = new BlitableArray<byte>(in array, Allocator.Persistent);
                    this.didFileExist = true;
                    // UnityEngine.Debug.LogError("File Read at: " + filepath2 + " of size: " + data.Length);
                }
                else
                {
                    this.didFileExist = false;
                    data = new BlitableArray<byte>(0, Allocator.Persistent);
                }
            }
            else if (state == AsyncReadState.StartOpenFile)
            {
                state = AsyncReadState.WaitOpenFile;
            }
        }

        public bool IsReadFileInfoComplete()
        {
            return state == AsyncReadState.WaitReadFileInfo;
        }

        public bool IsReadFileComplete()
        {
            if (state == AsyncReadState.WaitOpenFile)
            {
                state = AsyncReadState.Complete;
            }
            return state == AsyncReadState.Complete; // true;
        }

        public bool DoesFileExist()
        {
            return this.didFileExist; // UnsafeUtility.ReadArrayElement<FileInfoResult>(fileInfoResult, 0).FileState == FileState.Exists;
        }

        public bool DidFileReadSuccessfully()
        {
            return true; // status == AsyncReadState.WaitOpenFile; //  true; // readHandle.Status == ReadStatus.Complete;
        }

        public void Dispose()
        {
            data.Dispose();
        }
    }
    #else
    //! Loads files asynchronously.
    public unsafe struct AsyncFileReader
    {
        public byte state;
        public FileInfoResult* fileInfoResult;
        public byte openFileStatus;
        // public FileHandle fileHandle;
        public ReadHandle readHandle;
        public ReadCommand readCommand;
		private void* pointer;
        // used for read only
        public BlitableArray<ReadCommand> readCommands;

		public int Length
        {
            get
            { 
                return (int) readCommand.Size;
            }
        }

		unsafe public byte this[int index]
		{
			get
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				if (index >= readCommand.Size)
					throw new IndexOutOfRangeException();
#endif
				return UnsafeUtility.ReadArrayElement<byte>(readCommand.Buffer, index);
			}
		}

        public BlitableArray<byte> GetBytes()
        {
            //return readCommand.Buffer;
            var array = new BlitableArray<byte>();
            array.Set(pointer, Length, Allocator.Persistent);
            return array;
        }

        public void UpdateOnMainThread(in Text filepath)
        {
            if (state == AsyncReadState.StartReadFileInfo)
            {
                ReadFileInfoAsync(filepath.ToString());
            }
            else if (state == AsyncReadState.StartOpenFile)
            {
                ReadFileAsync(filepath.ToString());
            }
        }

        public bool IsReadFileInfoComplete()
        {
            return state == AsyncReadState.WaitReadFileInfo && readHandle.Status != ReadStatus.InProgress;
        }

        public bool IsReadFileComplete()
        {
            var readComplete = state == AsyncReadState.WaitOpenFile && readHandle.Status != ReadStatus.InProgress;
            if (readComplete)
            {
                state = AsyncReadState.Complete;
            }
            return readComplete;
        }

        public bool DoesFileExist()
        {
            return UnsafeUtility.ReadArrayElement<FileInfoResult>(fileInfoResult, 0).FileState == FileState.Exists;
        }

        public bool DidFileReadSuccessfully()
        {
            return readHandle.Status == ReadStatus.Complete;
        }

        public void ReadFileInfoAsync(string filepath)
        {
            state = AsyncReadState.WaitReadFileInfo;
            fileInfoResult = (FileInfoResult*) UnsafeUtility.Malloc(UnsafeUtility.SizeOf<FileInfoResult>(), 0, Allocator.Persistent);
            readHandle = AsyncReadManager.GetFileInfo(filepath, fileInfoResult);
        }
        
        public void ReadFileAsync(string filepath)
        {
            state = AsyncReadState.WaitOpenFile;
            readHandle.Dispose();
            var fileInfoResult2 = UnsafeUtility.ReadArrayElement<FileInfoResult>(fileInfoResult, 0);
            readCommand.Offset = 0;
            readCommand.Size = fileInfoResult2.FileSize;
            this.pointer = (byte*) UnsafeUtility.Malloc(readCommand.Size, 0, Allocator.Persistent);
            readCommand.Buffer = (byte*) this.pointer;
            // readCommand.Buffer = (byte*) UnsafeUtility.Malloc(readCommand.Size, 0, Allocator.Persistent);
            readCommands = new BlitableArray<ReadCommand>(1, Allocator.Persistent);
            readCommands[0] = readCommand;
            readHandle = AsyncReadManager.Read(filepath, (ReadCommand*) readCommands.GetUnsafePtr(), 1);
        }

        public void Dispose()
        {
            // file doesn't exist
            if (state == AsyncReadState.WaitReadFileInfo)
            {
                UnsafeUtility.Free(fileInfoResult, Allocator.Persistent);
                readHandle.Dispose();
            }
            // disposes handles
            else if (state == AsyncReadState.Complete)
            {
                UnsafeUtility.Free(fileInfoResult, Allocator.Persistent);
                UnsafeUtility.Free(readCommand.Buffer, Allocator.Persistent);
                readHandle.Dispose();
                // fileHandle.Close();
                readCommands.Dispose();
            }
        }
    }
    #endif
}

        /*public byte ReadArrayElement(int index)
        {
            return UnsafeUtility.ReadArrayElement<byte>(readCommand.Buffer, index);
        }*/
        /*public unsafe static WriteHandle CreateWriteHandle(string filepath, in BlitableArray<WriteCommand> blitableArray)
        {
            return AsyncWriteManager.Write(filepath, (WriteCommand*) blitableArray.GetUnsafePtr(), 1); //  cmds.GetUnsafePtr()
        }*/

        /*public unsafe static byte* GetBuffer(in BlitableArray<ReadCommand> blitableArray)
        {
            return (byte*) blitableArray[0].Buffer;
        }*/
            //readCommands = new BlitableArray<ReadCommand>(1, Allocator.Persistent);
            //readCommands[0] = readCommand;
            /*ReadCommandArray readCmdArray;
            readCmdArray.ReadCommands = &readCommand;
            readCmdArray.CommandCount = 1;
            // readHandle = ReadHandleUtility.CreateReadHandle(fileHandle, in readCommands);
            readHandle = AsyncReadManager.Read(fileHandle, readCmdArray); // (ReadCommand*) readCommands.GetUnsafePtr()); // , 1);*/
        
        /*public void OpenFileAsync(string filepath)
        {
            openFileStatus = 0; // FileStatus.Pending;
            // fileHandle = AsyncReadManager.OpenFileAsync(filepath);
        }

        public void ReadOpenFileStatus()
        {
            if (openFileStatus == 1)
            {
                return;
            }
            // if (fileHandle.JobHandle.IsCompleted) // .Status != FileStatus.Pending)
            {
                openFileStatus = 1;
            }
        }

        public bool IsOpenFileComplete()
        {
            return openFileStatus == 1; // openFileStatus != FileStatus.Pending; // JobHandle.IsCompleted;
        }*/
            /*else
            {
                openFileStatus = 0;
            }*/
        

            /* ReadCommandArray readCmdArray;
            var readCommand2 = readCommand;
            readCmdArray.ReadCommands = &readCommand2;
            readCmdArray.CommandCount = 1;
            readHandle = AsyncReadManager.Read(filepath, readCmdArray);*/
        /*public void ReadFileAsync2()
        {
            ReadCommandArray readCmdArray;
            var readCommand2 = readCommand;
            readCmdArray.ReadCommands = &readCommand2;
            readCmdArray.CommandCount = 1;
            readHandle = AsyncReadManager.Read(fileHandle, readCmdArray);
            // fileHandle.Close();
        }*/

    //! AWEAWEew
    /*public unsafe static class ReadHandleUtility
    {

        public static ReadCommand CreateReadCommand(long length)
        {
            ReadCommand readCommand;
            readCommand.Offset = 0;
            readCommand.Size = length;
            readCommand.Buffer = (byte*) UnsafeUtility.Malloc(readCommand.Size, 0, Allocator.Persistent);
            return readCommand;
        }

        //public unsafe static ReadHandle CreateReadHandle(string filepath, in BlitableArray<ReadCommand> blitableArray)
    }*/