using Unity.Collections;
using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Zoxel
{
    //! Data used for Saved Realm and Character datas.
    public struct SaveDirectoryData : IComparable<SaveDirectoryData>
    {
        public int id;
        public string name;
        public DateTime writeTime;

        public int CompareTo(SaveDirectoryData other)
        {
            if (writeTime.Ticks > other.writeTime.Ticks)
            {
                return 1;
            }
            else if (writeTime.Ticks < other.writeTime.Ticks)
            {
                return -1;
            }
            else
            {
                return 0;
            }
            // return writeTime.Ticks.Compare<long>(other.writeTime.Ticks);
            // return writeTime => other.writeTime;
            /*if (other.x == x && other.y == y && other.z == z)
            {
                return 0;
            }
            if (other.x > x)
            {
                return 1;
            }*/
        }
    }

    //! Utility functions for IO things
    public static class SaveUtilities
    {
        public static string persistentDataPath = Application.persistentDataPath;

        public static string GetRealmsPath()
        {
            return persistentDataPath + "/realms/";
        }

        public static SaveDirectoryData[] GetServers()
        {
            var source = persistentDataPath + "/";
            if (!Directory.Exists(source))
            {
                UnityEngine.Debug.LogError("Directory Does not Exist for Zoxel: " + source);
                Directory.CreateDirectory(source);
            }
            // UnityEngine.Debug.LogError("Save Path: " + source);
            source += "servers/";
            if (!Directory.Exists(source))
            {
                UnityEngine.Debug.Log("Creating Servers Directory: " + source);
                Directory.CreateDirectory(source);
            }
            return new SaveDirectoryData[] { };
        }

        public static SaveDirectoryData[] GetSavedRealms()
        {
            var source = persistentDataPath + "/";
            if (!Directory.Exists(source))
            {
                UnityEngine.Debug.LogError("Directory Does not Exist for Zoxel: " + source);
                Directory.CreateDirectory(source);
            }
            source += "realms/";
            if (!Directory.Exists(source))
            {
                UnityEngine.Debug.Log("Creating Realms Directory: " + source);
                Directory.CreateDirectory(source);
            }
            // var folders = directoryInfo.GetDirectories().OrderBy(x<=x.CreationTime).ToList();
            var folders = new DirectoryInfo(source).GetDirectories().ToList(); // .OrderByDescending(x => x.LastWriteTime)
            var realmDatas = new List<SaveDirectoryData>();
            for (int i = 0; i < folders.Count; i++)
            {
                var directoryInfo = folders[i];
                var folderName = directoryInfo.ToString();
                var saveDirectoryData = new SaveDirectoryData();
                var folderName2 = Path.GetFileName(folderName).ToLower();
                if (folderName2 == "unity")
                {
                    continue;
                }
                try
                {
                   saveDirectoryData.id = int.Parse(folderName2);
                }
                catch
                {
                    continue;
                }
                // check for file
                var fileName = folderName + "/RealmName.zox";
                if (!File.Exists(fileName))
                {
                    continue;
                }
                saveDirectoryData.name = File.ReadAllText(fileName);
                saveDirectoryData.writeTime = directoryInfo.LastWriteTime;
                var fileName2 = folderName + "/LastLoadTime.zox";
                if (File.Exists(fileName2))
                {
                    var fileInfo = new FileInfo(fileName2);
                    saveDirectoryData.writeTime = fileInfo.LastWriteTime;
                }
                realmDatas.Add(saveDirectoryData);
            }
            realmDatas = realmDatas.OrderByDescending(x => x.writeTime).ToList();
            return realmDatas.ToArray();
        }

        public static string[] GetCharacters(int realmID, bool isPlayers)
        {
            string folderName;
            if (isPlayers)
            {
                folderName = "Players";
            }
            else
            {
                folderName = "Characters";
            }
            var source = GetRealmsPath() + realmID.ToString() + "/" + folderName + "/";
            var directoryInfo = new DirectoryInfo(source);
            // var folders = directoryInfo.GetDirectories().OrderBy(x < x.CreationTime).ToList();
            var folders = directoryInfo.GetDirectories().OrderByDescending(x => x.LastWriteTime).ToList();
            var folderNames = new string[folders.Count];
            for (int i = 0; i < folders.Count; i++)
            {
                folderNames[i] = folders[i].ToString();
            }
            return folderNames; // Directory.GetDirectories();
        }

        public static Text[] GetCharacterNames(int realmID, bool isPlayers)
        {
            var folders = GetCharacters(realmID, isPlayers);
            var names = new List<Text>();
            for (int i = 0; i < folders.Length; i++)
            {
                //var newName = Path.GetFileName(folders[i]);
                // check for file
                var fileName = folders[i] + "/CharacterName.zox";
                if (File.Exists(fileName))
                {
                    var bytes = File.ReadAllBytes(fileName);
                    var name = new Text();
                    name.textData = new BlitableArray<byte>(bytes, Allocator.Persistent);
                    names.Add(name);
                }
                else
                {
                    UnityEngine.Debug.LogError("Character did not exist at " + i + " ::: " + fileName);
                    names.Add(new Text("Character " + (i + 1)));
                }
            }
            return names.ToArray();
        }

        public static string[] GetCharacterIDs(int realmID, bool isPlayers)
        {
            var folders = GetCharacters(realmID, isPlayers);
            var names = new string[folders.Length];
            for (int i = 0; i < folders.Length; i++)
            {
                names[i] = Path.GetFileName(folders[i]);
            }
            return names;
        }

        public static string GetRealmDirectory(int realmID)
        {
            return GetRealmsPath() + realmID.ToString() + "/";
        }

        // each game contains a list of worlds
        public static string LoadWorldID(int realmID)
        {
            var gamePath = SaveUtilities.GetRealmsPath() + realmID + "/";
            if (!Directory.Exists(gamePath))
            {
                return "";
            }
            var saveWorldsPath = gamePath + "Worlds/";
            UnityEngine.Debug.Log("Save World Path is: " + saveWorldsPath);
            var folders = Directory.GetDirectories(saveWorldsPath);
            if (folders.Length > 0)
            {
                return Path.GetFileName(folders[0]);
            }
            return "";
        }
        
        public static void CreateNewRealmFolders(string realmName, int realmID)
        {
            string saveGamePath = SaveUtilities.GetRealmsPath() + realmID.ToString() + "/";
            if (!Directory.Exists(saveGamePath))
            {
                Directory.CreateDirectory(saveGamePath);
            }
            var gameDataPath = saveGamePath + "/RealmName.zox";
            File.WriteAllText(gameDataPath, realmName);
            // create games sub paths
            string playersPath = saveGamePath + "Players/";
            if (!Directory.Exists(playersPath))
            {
                Directory.CreateDirectory(playersPath);
            }
            // sub path for chunks for now, when multiple worlds comes i will fix this
            string worldsPath = saveGamePath + "Worlds/";
            if (!Directory.Exists(worldsPath))
            {
                Directory.CreateDirectory(worldsPath);
            }
        }

        public static void CreateNewWorldFolder(int realmID, int worldID)
        {
            var worldPath = SaveUtilities.GetRealmsPath() + realmID.ToString() + "/Worlds/" + worldID + "/";
            if (!Directory.Exists(worldPath))
            {
                Directory.CreateDirectory(worldPath);
            }
            var chunksPath = worldPath + "Chunks/";
            if (!Directory.Exists(chunksPath))
            {
                Directory.CreateDirectory(chunksPath);
            }
            var charactersPath = worldPath + "Characters/";
            if (!Directory.Exists(charactersPath))
            {
                Directory.CreateDirectory(charactersPath);
            }
            var chunksPath2 = worldPath + "ChunkCharacters/";
            if (!Directory.Exists(chunksPath2))
            {
                Directory.CreateDirectory(chunksPath2);
            }
        }
        
        public static string GetRealmPath(int realmID)
        {
            return GetRealmsPath() + realmID.ToString() + "/";
        }

        public static int GetFirstWorldID(int realmID)
        {
            var realmPath = GetRealmPath(realmID) + "Worlds/";
            var folders = Directory.GetDirectories(realmPath);
            if (folders.Length == 0)
            {
                return 0;
            }
            return int.Parse(Path.GetFileName(folders[0]));
        }

        // old use
        /*public static void DeleteRealm(string realmID)
        {
            string saveGamePath = SaveUtilities.GetRealmsPath() + realmID + "/";
            if (Directory.Exists(saveGamePath))
            {
                Debug.Log("Deleting Realm Save Path: " + saveGamePath);
                Directory.Delete(saveGamePath, true);
            }
        }*/

        //! \todo Add Confirmation UI for this.
        public static void DeleteRealm(int realmID)
        {
            string saveGamePath = SaveUtilities.GetRealmsPath() + realmID.ToString() + "/";
            if (Directory.Exists(saveGamePath))
            {
                Debug.Log("Deleting Realm Save Path: " + saveGamePath);
                Directory.Delete(saveGamePath, true);
            }
        }

        public static bool DeleteCharacter(int realmID, int characterID, bool isPlayer)
        {
            string saveGamePath = SaveUtilities.GetRealmsPath() + realmID.ToString() + "/";
            if (isPlayer)
            {
                saveGamePath += "Players/";
            }
            else
            {
                saveGamePath += "Characters/";
            }
            var characterSavePath = saveGamePath + characterID.ToString() + "/";
            if (Directory.Exists(characterSavePath))
            {
                Debug.Log("Deleting Character Save Path: " + characterSavePath);
                Directory.Delete(characterSavePath, true);
                return true;
            }
            else
            {
                Debug.Log("Character Save Path Did not exist: " + characterSavePath);
                return false;
            }
        }

        public static bool DeletePlayerCharacter(int realmID, int characterID)
        {
            var characterSavePath = SaveUtilities.GetRealmsPath() + realmID.ToString() + "/Players/" + characterID.ToString() + "/";
            if (Directory.Exists(characterSavePath))
            {
                Debug.Log("Deleting Character Save Path: " + characterSavePath);
                Directory.Delete(characterSavePath, true);
                return true;
            }
            else
            {
                Debug.Log("Character Save Path Did not exist: " + characterSavePath);
                return false;
            }
        }
    }
}