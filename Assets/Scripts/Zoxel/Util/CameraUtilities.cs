using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel
{
    //! Util functions used for IO operations.
    public static class CameraUtil
    {
        public static float3 ScreenToWorldPoint(float3 screenPosition, float4x4 cameraToWorldMatrix, float4x4 projectionMatrix, float3 cameraPosition)
        {
            cameraToWorldMatrix[2][0] *= -1;
            cameraToWorldMatrix[2][1] *= -1;
            cameraToWorldMatrix[2][2] *= -1;
            var clipPosition = new float3((screenPosition.x * 2.0f) - 1.0f, (2.0f * screenPosition.y) - 1.0f, screenPosition.z);
            var viewPosition = math.transform(math.inverse(projectionMatrix), clipPosition);
            var projectedPosition = math.transform(cameraToWorldMatrix, viewPosition);
            var direction = math.normalize(projectedPosition - cameraPosition);
            return cameraPosition + direction * screenPosition.z;
        }

        public static float3 ScreenToWorldPoint(float3 screenPosition, float4x4 cameraToWorldMatrix, float4x4 projectionMatrix)
        {
            var clipPosition = new float3((screenPosition.x * 2.0f) - 1.0f, (2.0f * screenPosition.y) - 1.0f, screenPosition.z);
            var viewPosition = math.transform(math.inverse(projectionMatrix), clipPosition);
            viewPosition.z *= -1;
            viewPosition *= screenPosition.z;
            //UnityEngine.Debug.LogError("screenPosition: " + screenPosition + " - viewPosition: " + viewPosition);
            return math.transform(cameraToWorldMatrix, viewPosition);
        }

        public static float3 GetCameraRayDirection(float3 screenPosition, float4x4 cameraToWorldMatrix, float4x4 projectionMatrix, float3 cameraPosition)
        {
            var clipPosition = new float3((screenPosition.x * 2.0f) - 1.0f, (2.0f * screenPosition.y) - 1.0f, screenPosition.z);
            var viewPosition = math.transform(math.inverse(projectionMatrix), clipPosition);
            viewPosition.z *= -1;
            viewPosition *= screenPosition.z;
            //UnityEngine.Debug.LogError("screenPosition: " + screenPosition + " - viewPosition: " + viewPosition);
            var projectedPosition = math.transform(cameraToWorldMatrix, viewPosition);
            return math.normalize(projectedPosition - cameraPosition);
        }
    }
}

/*cameraToWorldMatrix[2][0] *= -1;
cameraToWorldMatrix[2][1] *= -1;
cameraToWorldMatrix[2][2] *= -1;*/

/*cameraToWorldMatrix[2][0] *= viewPosition.z;
cameraToWorldMatrix[2][1] *= viewPosition.z;
cameraToWorldMatrix[2][2] *= viewPosition.z;*/

//return projectedPosition;

// direction has to be camera forward direction
// and it has to be parallel to the screen position depth
// but also with the projected position

// basically the depth is wrong, but the projection is right

// get 
/*var projectedPosition2 = math.transform(cameraToWorldMatrix, new float3(0, 0, screenPosition.z));
var direction = math.normalize(projectedPosition - cameraPosition);
return cameraPosition + direction * projectedPosition2.z;*/

/*var viewPosition2 = math.transform(math.inverse(projectionMatrix), float3.zero);
var projectedPosition2 = math.transform(cameraToWorldMatrix, viewPosition2);*/ 