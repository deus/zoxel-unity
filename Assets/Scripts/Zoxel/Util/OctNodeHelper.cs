using Unity.Mathematics;

namespace Zoxel
{
    public static class OctNodeHelper
    {
        public static readonly float3[] positions = new float3[]
        {
            new float3(0, 0, 0),
            new float3(0, 0, 1),
            new float3(0, 1, 0),
            new float3(0, 1, 1),
            new float3(1, 0, 0),
            new float3(1, 0, 1),
            new float3(1, 1, 0),
            new float3(1, 1, 1)
        };

        public static byte GetDimensions(byte depth)
        {
            var depth2 = depth;
            var dimensions = (byte) 1;
            while (depth >= 1)
            {
                dimensions *= 2;
                depth--;
            }
            // UnityEngine.Debug.LogError("dimensions: " + dimensions + " from depth: " + depth2);
            return dimensions;
        }
    }
}