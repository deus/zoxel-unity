using Unity.Mathematics;

namespace Zoxel // .Textures
{
    public static class TextureUtil
    {
        public static UnityEngine.Texture2D CreateBlankTexture()
        {
            var texture = new UnityEngine.Texture2D(1, 1,
                UnityEngine.Experimental.Rendering.DefaultFormat.LDR,
                UnityEngine.Experimental.Rendering.TextureCreationFlags.None);
            var pixels = new UnityEngine.Color[1];
            pixels[0] = new UnityEngine.Color(0, 0, 0, 0);
            texture.SetPixels(pixels);
            texture.Apply();
            return texture;
        }

        public static UnityEngine.Texture2D CreateWhiteTexture()
        {
            var texture = new UnityEngine.Texture2D(1, 1,
                UnityEngine.Experimental.Rendering.DefaultFormat.LDR,
                UnityEngine.Experimental.Rendering.TextureCreationFlags.None);
            var pixels = new UnityEngine.Color[1];
            pixels[0] = new UnityEngine.Color(1, 1, 1, 1);
            texture.SetPixels(pixels);
            texture.Apply();
            return texture;
        }
    }
}