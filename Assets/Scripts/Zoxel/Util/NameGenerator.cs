using System.Collections;
using System.Collections.Generic;
using Unity.Collections;

namespace Zoxel
{
    public static class NameGenerator
    {
        public static string GenerateName()
        {
            string MyVoxelName = "";
            var MySyla = GetSyllabells();
            int SylabelCount = (int)UnityEngine.Random.Range(2, 4);
            for (int i = 0; i < SylabelCount; i++)
            {
                MyVoxelName += MySyla[(int)UnityEngine.Random.Range(0, MySyla.Count - 1)];
            }
            MyVoxelName = MyVoxelName.Substring(0, 1).ToUpper() + MyVoxelName.Substring(1, MyVoxelName.Length - 1);
            return MyVoxelName;
        }

        public static string GenerateName(ref Unity.Mathematics.Random random)
        {
            string MyVoxelName = "";
            var MySyla = GetSyllabells();
            int SylabelCount = 2 + random.NextInt(2); //(int)UnityEngine.Random.Range(2, 4);
            for (int i = 0; i < SylabelCount; i++)
            {
                MyVoxelName += MySyla[(int)random.NextInt(MySyla.Count - 1)];
            }
            MyVoxelName = MyVoxelName.Substring(0, 1).ToUpper() + MyVoxelName.Substring(1, MyVoxelName.Length - 1);
            return MyVoxelName;
        }

        public static Text GenerateName2(ref Unity.Mathematics.Random random)
        {
            var newName = new Text();
            // var syllabells = GetSyllabells();
            var sylabelCount = 2 + random.NextInt(2); //(int)UnityEngine.Random.Range(2, 4);
            var characters = new NativeList<byte>();
            for (int i = 0; i < sylabelCount; i++)
            {
                // generate a syllabell
                // add to list
                var firstLetterType = random.NextInt(10);
                if (firstLetterType == 0)
                {
                    characters.Add(Text.ConvertCharToByte('b'));
                }
                else if (firstLetterType == 1)
                {
                    characters.Add(Text.ConvertCharToByte('c'));
                }
                else if (firstLetterType == 2)
                {
                    characters.Add(Text.ConvertCharToByte('d'));
                }
                else if (firstLetterType == 3)
                {
                    characters.Add(Text.ConvertCharToByte('f'));
                }
                else if (firstLetterType == 4)
                {
                    characters.Add(Text.ConvertCharToByte('g'));
                }
                else if (firstLetterType == 5)
                {
                    characters.Add(Text.ConvertCharToByte('h'));
                }
                else if (firstLetterType == 6)
                {
                    characters.Add(Text.ConvertCharToByte('j'));
                }
                else if (firstLetterType == 7)
                {
                    characters.Add(Text.ConvertCharToByte('m'));
                }
                else if (firstLetterType == 8)
                {
                    characters.Add(Text.ConvertCharToByte('z'));
                }
                var secondLetterType = random.NextInt(4);
                if (secondLetterType == 0)
                {
                    characters.Add(Text.ConvertCharToByte('a'));
                }
                else if (secondLetterType == 1)
                {
                    characters.Add(Text.ConvertCharToByte('e'));
                }
                else if (secondLetterType == 2)
                {
                    characters.Add(Text.ConvertCharToByte('i'));
                }
                else if (secondLetterType == 3)
                {
                    characters.Add(Text.ConvertCharToByte('o'));
                }
                else if (secondLetterType == 4)
                {
                    characters.Add(Text.ConvertCharToByte('u'));
                }
            }
            newName.textData = new BlitableArray<byte>(characters.Length, Allocator.Persistent);
            for (int i = 0; i < characters.Length; i++)
            {
                newName.textData[i] = characters[i];
            }
            newName[0] = (byte) (newName[0] + 26);  // to upper
            characters.Dispose();
            // MyVoxelName = MyVoxelName.Substring(0, 1).ToUpper() + MyVoxelName.Substring(1, MyVoxelName.Length - 1);
            return newName;
        }

        public static List<string> GetSyllabells()
        {
            var MyData = new List<string>();
            MyData.Add("mo");
            MyData.Add("monn");
            MyData.Add("fay");
            MyData.Add("shi");
            MyData.Add("zag");
            MyData.Add("zen");
            MyData.Add("tex");
            MyData.Add("zel");
            MyData.Add("pie");

            // 2 letters
            MyData.Add("ze");
            MyData.Add("zi");
            MyData.Add("me");
            MyData.Add("mi");
            MyData.Add("el");
            MyData.Add("te");
            MyData.Add("ex");

            MyData.Add("te");
            MyData.Add("bi");
            MyData.Add("si");
            MyData.Add("le");
            MyData.Add("ga");
            MyData.Add("ta");
            MyData.Add("ba");
            return MyData;
        }
    }
}