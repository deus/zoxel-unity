using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel
{
    //! Util functions used for IO operations.
    public static class ByteUtil
    {
        public static bool ConvertToBool(byte input)
        {
            if (input == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public static byte ConvertFromBool(bool input)
        {
            if (input)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }
        
        public static void SetText(ref BlitableArray<byte> data, int index, in Text value)
        {
            for (int i = index; i < index + value.Length; i++)
            {
                data[i] = value[i - index];
            }
        }

        public static void GetText(in BlitableArray<byte> array, int index, out Text text)
        {
            text = new Text();
            text.textData = new BlitableArray<byte>(array.Length - index, Allocator.Persistent);
            for (int i = index; i < index + text.Length; i++)
            {
                text.textData[i - index] = array[i];
            }
        }

        public static void SetFloat4(ref BlitableArray<byte> data, int index, in float4 value)
        {
            SetFloat(ref data, index + 0, value.x);
            SetFloat(ref data, index + 4, value.y);
            SetFloat(ref data, index + 8, value.z);
            SetFloat(ref data, index + 12, value.w);
        }

        public static float4 GetFloat4(in byte[] array, int index)
        {
            var value = new float4();
            value.x = GetFloat(in array, index + 0);
            value.y = GetFloat(in array, index + 4);
            value.z = GetFloat(in array, index + 8);
            value.w = GetFloat(in array, index + 12);
            return value;
        }

        public static float4 GetFloat4(in BlitableArray<byte> array, int index)
        {
            var value = new float4();
            value.x = GetFloat(in array, index + 0);
            value.y = GetFloat(in array, index + 4);
            value.z = GetFloat(in array, index + 8);
            value.w = GetFloat(in array, index + 12);
            return value;
        }

        public static void SetFloat3(ref BlitableArray<byte> data, int index, in float3 value)
        {
            SetFloat(ref data, index + 0, value.x);
            SetFloat(ref data, index + 4, value.y);
            SetFloat(ref data, index + 8, value.z);
        }

        public static void GetFloat3(in BlitableArray<byte> array, int index, out float3 value)
        {
            value = new float3();
            value.x = GetFloat(in array, index + 0);
            value.y = GetFloat(in array, index + 4);
            value.z = GetFloat(in array, index + 8);
        }

        public static float3 GetFloat3(in BlitableArray<byte> array, int index)
        {
            var value = new float3();
            value.x = GetFloat(in array, index + 0);
            value.y = GetFloat(in array, index + 4);
            value.z = GetFloat(in array, index + 8);
            return value;
        }

        public static float3 GetFloat3(in byte[] array, int index)
        {
            var value = new float3();
            value.x = GetFloat(in array, index + 0);
            value.y = GetFloat(in array, index + 4);
            value.z = GetFloat(in array, index + 8);
            return value;
        }

        public static void AddInt3(ref NativeList<byte> data, int3 value)
        {
            AddInt(ref data, value.x);
            AddInt(ref data, value.y);
            AddInt(ref data, value.z);
        }

        public static void SetInt3(ref BlitableArray<byte> data, int index, in int3 value)
        {
            SetInt(ref data, index + 0, value.x);
            SetInt(ref data, index + 4, value.y);
            SetInt(ref data, index + 8, value.z);
        }

        public static void GetInt3(in BlitableArray<byte> array, int index, out int3 value)
        {
            value = new int3();
            value.x = GetInt(in array, index + 0);
            value.y = GetInt(in array, index + 4);
            value.z = GetInt(in array, index + 8);
        }

        public static int3 GetInt3(in BlitableArray<byte> array, int index)
        {
            var value = new int3();
            value.x = GetInt(in array, index + 0);
            value.y = GetInt(in array, index + 4);
            value.z = GetInt(in array, index + 8);
            return value;
        }

        public static int3 GetInt3(in byte[] array, int index)
        {
            var value = new int3();
            value.x = GetInt(in array, index + 0);
            value.y = GetInt(in array, index + 4);
            value.z = GetInt(in array, index + 8);
            return value;
        }

        public static void SetInt(ref BlitableArray<byte> data, int index, int value)
        {
            // Reversed
            data[index] = (byte)(value);
            data[index + 1] = (byte)(value >> 8);
            data[index + 2] = (byte)(value >> 16);
            data[index + 3] = (byte)(value >> 24);
        }

        public static void SetInt(ref NativeArray<byte> data, int index, int value)
        {
            // Reversed
            data[index] = (byte)(value);
            data[index + 1] = (byte)(value >> 8);
            data[index + 2] = (byte)(value >> 16);
            data[index + 3] = (byte)(value >> 24);
        }

        public static void AddInt(ref NativeList<byte> data, int value)
        {
            // Reversed
            data.Add((byte)(value));
            data.Add((byte)(value >> 8));
            data.Add((byte)(value >> 16));
            data.Add((byte)(value >> 24));
        }

        public static int GetInt(in BlitableArray<byte> array, int index)
        {
            return  ((array[index + 3]        ) << 24) |
                    ((array[index + 2]  & 0xff) << 16) |
                    ((array[index + 1]  & 0xff) <<  8) |
                    ((array[index]      & 0xff)      );
        }

        public static int GetInt(in byte[] array, int index)
        {
            return  ((array[index + 3]        ) << 24) |
                    ((array[index + 2]  & 0xff) << 16) |
                    ((array[index + 1]  & 0xff) <<  8) |
                    ((array[index]      & 0xff)      );
        }

        public static void SetFloat(ref BlitableArray<byte> data, int index, float value)
        {
            var uitos = new UInt32ToSingle { Single = value };
            data[index] = uitos.Byte0;
            data[index + 1] = uitos.Byte1;
            data[index + 2] = uitos.Byte2;
            data[index + 3] = uitos.Byte3;
        }

        public static float GetFloat(in byte[] array, int index)
        {
            var int32 = new UInt32ToSingle
            {
                Byte0 = array[index],
                Byte1 = array[index + 1],
                Byte2 = array[index + 2],
                Byte3 = array[index + 3],
            };
            return int32.Single;
        }

        public static float GetFloat(in BlitableArray<byte> array, int index)
        {
            var int32 = new UInt32ToSingle
            {
                Byte0 = array[index],
                Byte1 = array[index + 1],
                Byte2 = array[index + 2],
                Byte3 = array[index + 3],
            };
            return int32.Single;
        }

        public static void AddColorRGB(ref NativeList<byte> data, ColorRGB value)
        {
            data.Add(value.red);
            data.Add(value.green);
            data.Add(value.blue);
        }

        public static ColorRGB GetColorRGB(in byte[] array, int index)
        {
            return new ColorRGB(array[index], array[index + 1], array[index + 2]);
        }

        public static ColorRGB GetColorRGB(in BlitableArray<byte> array, int index)
        {
            return new ColorRGB(array[index], array[index + 1], array[index + 2]);
        }

        public static void SetDouble(ref BlitableArray<byte> data, int index, double value)
        {
            /*data[index] = (byte)(value);
            data[index + 1] = (byte)(value >> 8);
            data[index + 2] = (byte)(value >> 16);
            data[index + 3] = (byte)(value >> 24);
            data[index + 4] = (byte)(value >> 32);
            data[index + 5] = (byte)(value >> 40);
            data[index + 6] = (byte)(value >> 48);
            data[index + 7] = (byte)(value >> 56);*/
        }

        public static double GetDouble(in BlitableArray<byte> array, int index)
        {
            return  ((array[index + 7]        ) << 56) |
                    ((array[index + 6]  & 0xff) << 48) |
                    ((array[index + 5]  & 0xff) << 40) |
                    ((array[index + 4]  & 0xff) << 32) |
                    ((array[index + 3]  & 0xff) << 24) |
                    ((array[index + 2]  & 0xff) << 16) |
                    ((array[index + 1]  & 0xff) <<  8) |
                    ((array[index]      & 0xff)      );
        }
    }
}


/*data[index] = (byte)(value >> 24);
data[index + 1] = (byte)(value >> 16);
data[index + 2] = (byte)(value >> 8);
data[index + 3] = (byte)(value);*/