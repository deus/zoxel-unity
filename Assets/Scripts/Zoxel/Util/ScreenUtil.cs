using System;
using Unity.Mathematics;
using UnityEngine;

namespace Zoxel
{
    public static class ScreenUtil
    {
        public static byte DrawGUI(byte fullScreenMode) 
        {
            // toggle full screen
            if (GUILayout.Button("Toggle FullScreen"))
            {
                Screen.fullScreen = !Screen.fullScreen;
                if (!Screen.fullScreen)
                {
                    Resolution resolution = Screen.currentResolution;
                    Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
                }
            }
            //var fullScreenMode2 = GUILayout.Toggle(isFullScreen, "Full Screen: ");
            var fullScreenMode2 = fullScreenMode;
            if (fullScreenMode != (byte) FullScreenMode.Windowed && GUILayout.Button("Windowed"))
            {
                fullScreenMode2 = (byte) FullScreenMode.Windowed;
            }
            if (fullScreenMode != (byte) FullScreenMode.MaximizedWindow && GUILayout.Button("MaximizedWindow"))
            {
                fullScreenMode2 = (byte) FullScreenMode.MaximizedWindow;
            }

            if (fullScreenMode != (byte) FullScreenMode.ExclusiveFullScreen && GUILayout.Button("ExclusiveFullScreen"))
            {
                fullScreenMode2 = (byte) FullScreenMode.ExclusiveFullScreen;
            }
            if (fullScreenMode != (byte) FullScreenMode.FullScreenWindow && GUILayout.Button("FullScreenWindow"))
            {
                fullScreenMode2 = (byte) FullScreenMode.FullScreenWindow;
            }

            if (fullScreenMode != fullScreenMode2)
            {
                fullScreenMode = SetScreenMode(fullScreenMode, fullScreenMode2);
            }
            SwitchMonitors();
            return fullScreenMode;
        }

        public static void ToggleFullScreen()
        {
            var fullScreenMode = (byte) PlayerPrefs.GetInt("FullScreenMode", (byte) FullScreenMode.ExclusiveFullScreen);
            if (fullScreenMode == (byte) FullScreenMode.Windowed || fullScreenMode == (byte) FullScreenMode.MaximizedWindow)
            {
                SetScreenMode(fullScreenMode, (byte) FullScreenMode.ExclusiveFullScreen);
            }
            else
            {
                SetScreenMode(fullScreenMode, (byte) FullScreenMode.Windowed);
            }
        }

        public static byte SetScreenMode(byte fullScreenMode, byte newScreenMode)
        {
            fullScreenMode = newScreenMode;
            PlayerPrefs.SetInt("FullScreenMode", fullScreenMode);
            if (fullScreenMode == (byte) FullScreenMode.Windowed || fullScreenMode == (byte) FullScreenMode.MaximizedWindow)
            {
                //P/layerSettings.resizableWindow = true;
                Screen.fullScreenMode = FullScreenMode.Windowed;
                var multiplier = 0.6f;
                if (fullScreenMode == (byte) FullScreenMode.MaximizedWindow)
                {
                    multiplier = 1f;
                    Screen.SetResolution(
                        (int)(Display.main.systemWidth * multiplier),
                        (int)(Display.main.systemHeight * multiplier) - 40,
                        Screen.fullScreenMode); // , 60);
                }
                else
                {
                    Screen.SetResolution(
                        Display.main.systemWidth / 2,
                        Display.main.systemHeight / 2,
                        Screen.fullScreenMode); // , 60);
                }
                Screen.fullScreen = false;
            }
            else if (fullScreenMode == (byte) FullScreenMode.FullScreenWindow || fullScreenMode == (byte) FullScreenMode.ExclusiveFullScreen)
            {
                //PlayerSettings.resizableWindow = false;
                Screen.fullScreenMode = (FullScreenMode) fullScreenMode;
                Screen.SetResolution(Display.main.systemWidth, Display.main.systemHeight, Screen.fullScreenMode); // , 60);
                Screen.fullScreen = true;
            }
            return fullScreenMode;
        }

        private static void SwitchMonitors()
        {
            GUILayout.Label("Current Screen: " + Screen.width + "x" + Screen.height + " " +  Screen.currentResolution.refreshRate + "Hz");
            var cameras = GameObject.FindObjectsOfType<UnityEngine.Camera>();
            GUILayout.Label("Total Cameras: " + cameras.Length);
            GUILayout.Label("Total Displays: " + Display.displays.Length);
            for (int i = 0; i < Display.displays.Length; i++)
            {
                var display = Display.displays[i];
                int resWidth = display.systemWidth;
                int resHeight = display.systemHeight;
                //GUILayout.Label(i + " " + Display.displays[i].name);
                if (GUILayout.Button("  Switch Screen [" + resWidth + "x" + resHeight + "] to " + i))
                {
                    cameras[0].targetDisplay = i;
                    SetScreen(i);
                }
                //GUILayout.Label(i + " " + Display.displays[i].name);
                if (GUILayout.Button("  Switch Camera to " + i))
                {
                    cameras[0].targetDisplay = i;
                }
            }
            // Display cameras here too
            // Link 2player camera to second screen!
        }
                
        private static void SetScreen(int targetDisplay)
        {
            PlayerPrefs.SetInt("UnityMonitor", targetDisplay); // Select monitor
            var display = Display.displays[targetDisplay];
            int screenWidth = display.systemWidth;
            int screenHeight = display.systemHeight;
            Screen.SetResolution(screenWidth, screenHeight, Screen.fullScreen);
            // this only works in unity 2021
            // Screen.MoveMainWindow(display, new Vector2Int());

            // activate second display for 2 player
            // display.Activate();
            // cameras[0].targetDisplay = targetDisplay;

            for (int j = 0; j < Display.displays.Length; j++)
            {
                if (targetDisplay != j)
                {
                    //Display.displays[j].Deactivate();
                }
            }
        }
    }
}