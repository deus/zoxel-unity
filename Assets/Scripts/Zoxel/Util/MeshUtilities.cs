﻿using Unity.Mathematics;
using Unity.Collections;
using UnityEngine;

namespace Zoxel
{
    //! Keeps hold of Cube Mesh Data. Generates Primitive Shapes.
    public static class MeshUtilities
    {
        public static Mesh GenerateCube(float3 size, bool isUVs = false, bool isNormals = false)
        {
            var mesh = new Mesh();
            //mesh.name = "Procedural Cube";
            var verts = new Vector3[cubeVerticesVector3.Length];
            for (int i = 0; i < verts.Length; i++)
            {
                verts[i] = cubeVerticesVector3[i];
                verts[i] -= new Vector3(0.5f, 0.5f, 0.5f);
                verts[i] = new Vector3(verts[i].x * size.x, verts[i].y * size.y, verts[i].z * size.z);
            }
            mesh.vertices = verts;
            var triangles = new int[cubeTriangles2.Length];
            for (int i = 0; i < triangles.Length; i++)
            {
                triangles[i] = cubeTriangles2[i];
            }
            mesh.triangles = triangles;
            if (isUVs)
            {
                var uvs = new Vector2[verts.Length];
                for (int i = 0; i < 6; i++)
                {
                    uvs[i * 4] = cubeUVs[0];
                    uvs[i * 4 + 1] = cubeUVs[1];
                    uvs[i * 4 + 2] = cubeUVs[2];
                    uvs[i * 4 + 3] = cubeUVs[3];
                }
                mesh.uv = uvs;
            }
            /*var normals = new Vector3[4]
            {
                -Vector3.forward,
                -Vector3.forward,
                -Vector3.forward,
                -Vector3.forward
            };*/
            if (isNormals)
            {
                var normals = new NativeArray<float3>(24, Allocator.Temp);
                for (int i = 0; i < 6; i++)
                {
                    normals[i * 4] = cubeNormals[i];
                    normals[i * 4 + 1] = cubeNormals[i];
                    normals[i * 4 + 2] = cubeNormals[i];
                    normals[i * 4 + 3] = cubeNormals[i];
                }
                mesh.SetNormals(normals); // normals = cubeNormals;
                normals.Dispose();
            }
            /*var uvs = new Vector2[4];
            uvs[0] = new Vector2(0, 0);
            uvs[1] = new Vector2(1, 0);
            uvs[2] = new Vector2(1, 1);
            uvs[3] = new Vector2(0, 1);
            mesh.uv = uvs;*/
            return mesh;
        }

        public static Mesh CreateQuadMesh(float2 size, byte horizontalAlignment = 0, byte verticalAlignment = 0)
        {
            return CreateQuadMesh(size, horizontalAlignment, verticalAlignment, new Vector3());
        }

        public static Mesh CreateQuadMesh(float2 size, byte horizontalAlignment, byte verticalAlignment, float3 offset)
        {
            float healthBackBuffer = 0;
            var mesh = new Mesh();
            var newVerts = new Vector3[4];
            if (horizontalAlignment == (byte)TextHorizontalAlignment.Left)
            {
                newVerts[0] = new Vector3(0 * size.x - healthBackBuffer, -0.5f * size.y - healthBackBuffer, 0);
                newVerts[1] = new Vector3(1 * size.x + healthBackBuffer, -0.5f * size.y - healthBackBuffer, 0);
                newVerts[2] = new Vector3(1 * size.x + healthBackBuffer, 0.5f * size.y + healthBackBuffer, 0);
                newVerts[3] = new Vector3(0 * size.x - healthBackBuffer, 0.5f * size.y + healthBackBuffer, 0);
            }
            else if (horizontalAlignment == (byte)TextHorizontalAlignment.Middle)
            {
                newVerts[0] = new Vector3(-0.5f * size.x - healthBackBuffer, -0.5f * size.y - healthBackBuffer, 0);
                newVerts[1] = new Vector3(0.5f * size.x + healthBackBuffer, -0.5f * size.y - healthBackBuffer, 0);
                newVerts[2] = new Vector3(0.5f * size.x + healthBackBuffer, 0.5f * size.y + healthBackBuffer, 0);
                newVerts[3] = new Vector3(-0.5f * size.x - healthBackBuffer, 0.5f * size.y + healthBackBuffer, 0);
            }
            else if (horizontalAlignment == (byte)TextHorizontalAlignment.Right)
            {
                newVerts[0] = new Vector3(-1 * size.x - healthBackBuffer, -0.5f * size.y - healthBackBuffer, 0);
                newVerts[1] = new Vector3(0 * size.x + healthBackBuffer, -0.5f * size.y - healthBackBuffer, 0);
                newVerts[2] = new Vector3(0 * size.x + healthBackBuffer, 0.5f * size.y + healthBackBuffer, 0);
                newVerts[3] = new Vector3(-1 * size.x - healthBackBuffer, 0.5f * size.y + healthBackBuffer, 0);
            }
            if (verticalAlignment == TextVerticalAlignment.Bottom)
            {
                newVerts[0] = new Vector3(newVerts[0].x, 0, newVerts[0].z);
                newVerts[1] = new Vector3(newVerts[1].x, 0, newVerts[1].z);
                newVerts[2] = new Vector3(newVerts[2].x, size.y, newVerts[2].z);
                newVerts[3] = new Vector3(newVerts[3].x, size.y, newVerts[3].z);
            }
            else if (verticalAlignment == TextVerticalAlignment.Top)
            {
                newVerts[0] = new Vector3(newVerts[0].x, -size.y, newVerts[0].z);
                newVerts[1] = new Vector3(newVerts[1].x, -size.y, newVerts[1].z);
                newVerts[2] = new Vector3(newVerts[2].x, 0, newVerts[2].z);
                newVerts[3] = new Vector3(newVerts[3].x, 0, newVerts[3].z);
            }
            var offset2 = new Vector3(offset.x, offset.y, offset.z);
            newVerts[0] += offset2; // new Vector3(0, size.y / 2f, 0);
            newVerts[1] += offset2; // new Vector3(0, size.y / 2f, 0);
            newVerts[2] += offset2; // new Vector3(0, size.y / 2f, 0);
            newVerts[3] += offset2; // new Vector3(0, size.y / 2f, 0);

            mesh.vertices = newVerts;
            int[] indices = new int[6];
            indices[0] = 2;
            indices[1] = 1;
            indices[2] = 0;
            indices[3] = 3;
            indices[4] = 2;
            indices[5] = 0;
            mesh.SetIndices(indices, MeshTopology.Triangles, 0);
            var uvs = new Vector2[4];
            uvs[0] = new Vector2(0, 0);
            uvs[1] = new Vector2(1, 0);
            uvs[2] = new Vector2(1, 1);
            uvs[3] = new Vector2(0, 1);
            mesh.uv = uvs;
            mesh.colors = new UnityEngine.Color[] { UnityEngine.Color.white, UnityEngine.Color.white, UnityEngine.Color.white, UnityEngine.Color.white };
            return mesh;
        }

        public static Mesh CreateQuadMesh2(float2 size, bool isLeftAligned = false)
        {
            float healthBackBuffer = 0;
            Mesh mesh = new Mesh();
            Vector3[] newVerts = new Vector3[4];
            newVerts[0] = new Vector3(-0.5f * size.x - healthBackBuffer, -0.5f * size.y - healthBackBuffer, 0);
            newVerts[1] = new Vector3(0.5f * size.x + healthBackBuffer, -0.5f * size.y - healthBackBuffer, 0);
            newVerts[2] = new Vector3(-0.5f * size.x - healthBackBuffer, 0.5f * size.y + healthBackBuffer, 0);
            newVerts[3] = new Vector3(0.5f * size.x + healthBackBuffer, 0.5f * size.y + healthBackBuffer, 0);
            mesh.vertices = newVerts;
            int[] indices = new int[6];

            indices[0] = 0;
            indices[1] = 2;
            indices[2] = 1;
            indices[3] = 2;
            indices[4] = 3;
            indices[5] = 1;

            mesh.SetIndices(indices, MeshTopology.Triangles, 0);
            Vector2[] uvs = new Vector2[4];
            uvs[0] = new Vector2(0, 0);
            uvs[1] = new Vector2(1, 0);
            uvs[2] = new Vector2(0, 1);
            uvs[3] = new Vector2(1, 1);
            mesh.uv = uvs;
            mesh.colors = new UnityEngine.Color[] { UnityEngine.Color.white, UnityEngine.Color.white, UnityEngine.Color.white, UnityEngine.Color.white };

            var normals = new Vector3[4]
            {
                -Vector3.forward,
                -Vector3.forward,
                -Vector3.forward,
                -Vector3.forward
            };
            mesh.normals = normals;

            //mesh.RecalculateBounds();
            //mesh.RecalculateNormals();
            //mesh.RecalculateTangents();
            return mesh;
        }
        
        public static Mesh CreateQuadMeshXZ(float2 size)
        {
            var mesh = new Mesh();
            var newVerts = new Vector3[4];
            newVerts[0] = new Vector3(-0.5f * size.x, 0, 0.5f * size.y);
            newVerts[1] = new Vector3(0.5f * size.x, 0, 0.5f * size.y);
            newVerts[2] = new Vector3(0.5f * size.x, 0, -0.5f * size.y);
            newVerts[3] = new Vector3(-0.5f * size.x, 0, -0.5f * size.y);
            mesh.vertices = newVerts;
            int[] indices = new int[6];
            indices[0] = 0;
            indices[1] = 1;
            indices[2] = 2;
            indices[3] = 0;
            indices[4] = 2;
            indices[5] = 3;
            mesh.SetIndices(indices, MeshTopology.Triangles, 0);
            var uvs = new Vector2[4];
            uvs[0] = new Vector2(0, 0);
            uvs[1] = new Vector2(1, 0);
            uvs[2] = new Vector2(1, 1);
            uvs[3] = new Vector2(0, 1);
            mesh.uv = uvs;
            mesh.colors = new UnityEngine.Color[] { UnityEngine.Color.white, UnityEngine.Color.white, UnityEngine.Color.white, UnityEngine.Color.white };
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            return mesh;
        }

        public static readonly float3[] cubeNormals = new float3[]
        {
            new float3(-1, 0, 0),
            new float3(1, 0, 0),
            new float3(0, -1, 0),
            new float3(0, 1, 0),
            new float3(0, 0, -1),
            new float3(0, 0, 1)
        };
            
        public static readonly float3[] cubeVertices = new float3[]
        {
            // left
            new float3(0, 0, 0),
            new float3(0, 1, 0),
            new float3(0, 1, 1),
            new float3(0, 0, 1),
            // right
            new float3(1, 0, 0),
            new float3(1, 1, 0),
            new float3(1, 1, 1),
            new float3(1, 0, 1),
            // down
            new float3(0, 0, 0),
            new float3(1, 0, 0),
            new float3(1, 0, 1),
            new float3(0, 0, 1),
            // up
            new float3(0, 1, 0),
            new float3(1, 1, 0),
            new float3(1, 1, 1),
            new float3(0, 1, 1),
            // forward
            new float3(0, 0, 0),
            new float3(1, 0, 0),
            new float3(1, 1, 0),
            new float3(0, 1, 0),
            // back
            new float3(0, 0, 1),
            new float3(1, 0, 1),
            new float3(1, 1, 1),
            new float3(0, 1, 1)
        };

        public static readonly uint[] cubeTriangles = new uint[]
        {
            // left
            3, 1, 0, 3, 2, 1,
            // right
            0, 2, 3, 0, 1, 2,
            // down
            0, 2, 3, 0, 1, 2,
            // up
            3, 1, 0, 3, 2, 1,
            // forward
            3, 1, 0, 3, 2, 1,
            // back
            0, 2, 3, 0, 1, 2
        };

        public static readonly int[] cubeTriangles2 = new int[]
        {
            3, 1, 0, 3, 2, 1,

            0+4, 2+4, 3+4, 0+4, 1+4, 2+4,

            3+8, 1+8, 0+8, 3+8, 2+8, 1+8,

            0+12, 2+12, 3+12, 0+12, 1+12, 2+12,

            3+16, 1+16, 0+16, 3+16, 2+16, 1+16,

            0+20, 2+20, 3+20, 0+20, 1+20, 2+20
        };

        public static readonly float2[] cubeUVs = new float2[]
        {
            new float2(0, 0),
            new float2(1, 0),
            new float2(1, 1),
            new float2(0, 1)
        };

        public static readonly Vector3[] cubeVerticesVector3 = new Vector3[]
        {
            // up
            new Vector3(0, 1, 0),
            new Vector3(1, 1, 0),
            new Vector3(1, 1, 1),
            new Vector3(0, 1, 1),
            // down
            new Vector3(0, 0, 0),
            new Vector3(1, 0, 0),
            new Vector3(1, 0, 1),
            new Vector3(0, 0, 1),
            // left
            new Vector3(0, 0, 0),
            new Vector3(0, 1, 0),
            new Vector3(0, 1, 1),
            new Vector3(0, 0, 1),
            // right
            new Vector3(1, 0, 0),
            new Vector3(1, 1, 0),
            new Vector3(1, 1, 1),
            new Vector3(1, 0, 1),
            // forward
            new Vector3(0, 0, 0),
            new Vector3(1, 0, 0),
            new Vector3(1, 1, 0),
            new Vector3(0, 1, 0),
            // back
            new Vector3(0, 0, 1),
            new Vector3(1, 0, 1),
            new Vector3(1, 1, 1),
            new Vector3(0, 1, 1)
        };
    }
}

        /*private Mesh CreateQuadMesh(float2 panelSize, bool isLeftAligned)
        {
            Mesh mesh = new Mesh();
            Vector3[] newVerts = new Vector3[4];
            if (isLeftAligned)
            {
                newVerts[0] = new Vector3(0, 0.5f * panelSize.y, healthbarDepth);
                newVerts[1] = new Vector3(panelSize.x, 0.5f * panelSize.y, healthbarDepth);
                newVerts[2] = new Vector3(panelSize.x, -0.5f * panelSize.y, healthbarDepth);
                newVerts[3] = new Vector3(0, -0.5f * panelSize.y, healthbarDepth);
            }
            else
            {
                // Back Bar
                newVerts[0] = new Vector3(-0.5f * panelSize.x - healthBackBuffer, 0.5f * panelSize.y + healthBackBuffer, 0);
                newVerts[1] = new Vector3(0.5f * panelSize.x + healthBackBuffer, 0.5f * panelSize.y + healthBackBuffer, 0);
                newVerts[2] = new Vector3(0.5f * panelSize.x + healthBackBuffer, -0.5f * panelSize.y - healthBackBuffer, 0);
                newVerts[3] = new Vector3(-0.5f * panelSize.x - healthBackBuffer, -0.5f * panelSize.y - healthBackBuffer, 0);
            }
            mesh.vertices = newVerts;
            int[] indices = new int[6];
            indices[0] = 0;
            indices[1] = 1;
            indices[2] = 2;
            indices[3] = 0;
            indices[4] = 2;
            indices[5] = 3;
            mesh.SetIndices(indices, MeshTopology.Triangles, 0);
            Vector2[] uvs = new Vector2[4];
            uvs[0] = new Vector2(0, 0);
            uvs[1] = new Vector2(1, 0);
            uvs[2] = new Vector2(1, 1);
            uvs[3] = new Vector2(0, 1);
            mesh.uv = uvs;
            mesh.colors = new UnityEngine.Color[] { UnityEngine.Color.white, UnityEngine.Color.white, UnityEngine.Color.white, UnityEngine.Color.white };
            mesh.RecalculateBounds();
            mesh.RecalculateNormals();
            return mesh;
        }*/
        

        /*public static readonly Vector2[,] cubeUvs = new Vector2[,]
        {
            // Top
            {
                new Vector3(0, 0),
                new Vector3(0, 1),
                new Vector3(1, 1),
                new Vector3(1, 0)
            },
            // Bottom
			{
                new Vector3(1, 0),
                new Vector3(0, 0),
                new Vector3(0, 1),
                new Vector3(1, 1),
            },
            // Left
            {
                new Vector3(0, 0),
                new Vector3(0, 1),
                new Vector3(1, 1),
                new Vector3(1, 0)
            },
            // Right
            {
                new Vector3(0, 0),
                new Vector3(0, 1),
                new Vector3(1, 1),
                new Vector3(1, 0)
            },
            // Front
			{
                new Vector3(0, 0),
                new Vector3(1, 0),
                new Vector3(1, 1),
                new Vector3(0, 1),
            },
            // Back
			{
                new Vector3(1, 0),
                new Vector3(0, 0),
                new Vector3(0, 1),
                new Vector3(1, 1),
            }
        };*/
            /*var p1 = new Vector3(-midSize.x, -midSize.y, -midSize.z );
            var p2 = new Vector3(midSize.x,-midSize.y,-midSize.z);
            var p3 = new Vector3(-midSize.x,midSize.y,-midSize.z);
            var p4 = new Vector3(-midSize.x,-midSize.y,midSize.z);
            var p5 = new Vector3(midSize.x,-midSize.y,midSize.z);
            var p6 = new  Vector3(midSize.x, midSize.y,-midSize.z);
            var p7 = new Vector3(-midSize.x, midSize.y, midSize.z);
            var p8 = new  Vector3(midSize.x, midSize.y, midSize.z);
            var newVertices = new Vector3[] {p1,p2,p3,p4,p5,p6,p7,p8};
            var newTriangles = new int[] {
                0,2,1, 1,2,5,
                3,0,1, 1,4,3,
                0,3,2, 2,3,6,
                1,5,4, 5,7,4,
                6,3,4, 6,4,7,
                6,5,2, 7,5,6
            };*/