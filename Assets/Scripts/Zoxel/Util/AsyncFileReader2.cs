/*using Unity.IO.LowLevel.Unsafe;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Zoxel
{
    public static class AsyncReadState
    {
        public const byte None = 0;
        public const byte PreReadFileInfo = 1;
        public const byte ReadFileInfo = 2;
        public const byte OpenFile = 3;
        public const byte OpenedFile = 4;
        public const byte PreReadFile = 5;
        public const byte ReadFile = 6;
        public const byte Complete = 7;
    }
    public unsafe struct AsyncFileReader
    {
        public byte state;
        public FileInfoResult* fileInfoResult;
        public byte openFileStatus;
        public FileHandle fileHandle;
        public ReadHandle readHandle;
        public ReadCommand readCommand;
        // used for read only
        // public BlitableArray<ReadCommand> readCommands;

        public void CreateFileInfoResult()
        {
            fileInfoResult = (FileInfoResult*) UnsafeUtility.Malloc(UnsafeUtility.SizeOf<FileInfoResult>(), 0, Allocator.Persistent);
        }

        public void ReadFileInfoAsync(string filePath)
        {
            // fileInfoResult = (FileInfoResult*) UnsafeUtility.Malloc(UnsafeUtility.SizeOf<FileInfoResult>(), 0, Allocator.Persistent);
            readHandle = AsyncReadManager.GetFileInfo(filePath, fileInfoResult); // (ReadCommand*) readCommands.GetUnsafePtr());
        }

        public bool IsFileInfoComplete()
        {
            return readHandle.Status != ReadStatus.InProgress;
        }
        
        public void OpenFileAsync(string filePath)
        {
            openFileStatus = 0; // FileStatus.Pending;
            fileHandle = AsyncReadManager.OpenFileAsync(filePath);
        }

        public void ReadOpenFileStatus()
        {
            if (openFileStatus == 1)
            {
                return;
            }
            if (fileHandle.JobHandle.IsCompleted) // .Status != FileStatus.Pending)
            {
                openFileStatus = 1;
            }
            else
            {
                openFileStatus = 0;
            }
        }

        public bool IsOpenFileComplete()
        {
            return openFileStatus == 1; // openFileStatus != FileStatus.Pending; // JobHandle.IsCompleted;
        }
        
        public void ReadFileAsync()
        {
            readHandle.Dispose();
            var fileInfoResult2 = UnsafeUtility.ReadArrayElement<FileInfoResult>(fileInfoResult, 0); //(FileInfoResult) fileInfoResult;
            //readCommand = ReadHandleUtility.CreateReadCommand(fileInfoResult2.FileSize);
            //ReadCommand readCommand;
            readCommand.Offset = 0;
            readCommand.Size = fileInfoResult2.FileSize;
            readCommand.Buffer = (byte*) UnsafeUtility.Malloc(readCommand.Size, 0, Allocator.Persistent);
        }
        
        public void ReadFileAsync2()
        {
            ReadCommandArray readCmdArray;
            var readCommand2 = readCommand;
            readCmdArray.ReadCommands = &readCommand2;
            readCmdArray.CommandCount = 1;
            readHandle = AsyncReadManager.Read(fileHandle, readCmdArray);
            // fileHandle.Close();
        }

        public bool DoesFileExist()
        {
            return UnsafeUtility.ReadArrayElement<FileInfoResult>(fileInfoResult, 0).FileState == FileState.Exists;
        }

        public bool IsFileReadComplete()
        {
            return readHandle.Status != ReadStatus.InProgress;
        }

        public bool IsSuccess()
        {
            return readHandle.Status == ReadStatus.Complete;
        }
         
        public byte ReadArrayElement(int index)
        {
            return UnsafeUtility.ReadArrayElement<byte>(readCommand.Buffer, index);
        }

        public void Dispose()
        {
            // file doesn't exist
            if (state == AsyncReadState.ReadFileInfo)
            {
                UnsafeUtility.Free(fileInfoResult, Allocator.Persistent);
                readHandle.Dispose();
            }
            // disposes handles
            else if (state == AsyncReadState.ReadFile)
            {
                UnsafeUtility.Free(fileInfoResult, Allocator.Persistent);
                UnsafeUtility.Free(readCommand.Buffer, Allocator.Persistent);
                // UnsafeUtility.Free(readCommands[0].Buffer, Allocator.Persistent);
                // readCommands.Dispose();
                readHandle.Dispose();
                fileHandle.Close();
            }
        }
    }
}*/

/*public unsafe static WriteHandle CreateWriteHandle(string filePath, in BlitableArray<WriteCommand> blitableArray)
{
    return AsyncWriteManager.Write(filePath, (WriteCommand*) blitableArray.GetUnsafePtr(), 1); //  cmds.GetUnsafePtr()
}*/

/*public unsafe static byte* GetBuffer(in BlitableArray<ReadCommand> blitableArray)
{
    return (byte*) blitableArray[0].Buffer;
}*/

//readCommands = new BlitableArray<ReadCommand>(1, Allocator.Persistent);
//readCommands[0] = readCommand;
/*ReadCommandArray readCmdArray;
readCmdArray.ReadCommands = &readCommand;
readCmdArray.CommandCount = 1;
// readHandle = ReadHandleUtility.CreateReadHandle(fileHandle, in readCommands);
readHandle = AsyncReadManager.Read(fileHandle, readCmdArray); // (ReadCommand*) readCommands.GetUnsafePtr()); // , 1);*/

//! AWEAWEew
/*public unsafe static class ReadHandleUtility
{

    public static ReadCommand CreateReadCommand(long length)
    {
        ReadCommand readCommand;
        readCommand.Offset = 0;
        readCommand.Size = length;
        readCommand.Buffer = (byte*) UnsafeUtility.Malloc(readCommand.Size, 0, Allocator.Persistent);
        return readCommand;
    }

    //public unsafe static ReadHandle CreateReadHandle(string filePath, in BlitableArray<ReadCommand> blitableArray)
}*/