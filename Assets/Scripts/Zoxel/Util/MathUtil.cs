using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel
{
    //! Some helper functions to check mathematics struct validation.
    public static class MathUtil
    {
        public static bool IsValid(float4x4 value)
        {
            return IsValid(value[0]) && IsValid(value[1]) && IsValid(value[2]) && IsValid(value[3]);
        }

        public static bool IsValid(float4 value)
        {
            return IsValid(value.x) && IsValid(value.y) && IsValid(value.z) && IsValid(value.w);
        }

        public static bool IsValid(float3 value)
        {
            return IsValid(value.x) && IsValid(value.y) && IsValid(value.z);
        }

        public static bool IsValid(float value)
        {
            return !float.IsNaN(value) && !float.IsInfinity(value);
        }
    }
}