namespace Zoxel
{
    public static class ZoxelSystemStats
    {
        public static int totalVertices;
        public static int totalTriangles;
        public static int totalCharacterTriangles;
        public static int totalMapTriangles;
        public static int totalMinivoxTriangles;
    }
}