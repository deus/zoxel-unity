using System;

namespace Zoxel
{
    public static class IDUtil
    {
        public static int GenerateUniqueID()
        {
            return System.Guid.NewGuid().GetHashCode();
        }
    }
}