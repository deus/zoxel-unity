﻿using Unity.Mathematics;

namespace Zoxel
{
    //! Some helper functions to interpolate between quaternions and angle functions.
    public static class QuaternionHelpers
    {
        public static quaternion slerp(quaternion q1, quaternion q2, float t)
        {
            float dt = math.dot(q1, q2);
            if (dt < 0.0f)
            {
                dt = -dt;
                q2.value = -q2.value;
            }

            if (dt < 0.9995f)
            {
                float angle = math.acos(dt);
                float s = math.rsqrt(1.0f - dt * dt);    // 1.0f / sin(angle)
                float w1 = math.sin(angle * (1.0f - t)) * s;
                float w2 = math.sin(angle * t) * s;
                return math.quaternion(q1.value * w1 + q2.value * w2);
            }
            else
            {
                // if the angle is small, use linear interpolation
                return nlerp(q1, q2, t);
            }
        }

        public static quaternion nlerp(quaternion q1, quaternion q2, float t)
        {
            float dt = math.dot(q1, q2);
            if (dt < 0.0f)
            {
                q2.value = -q2.value;
            }

            return math.normalize(math.quaternion(math.lerp(q1.value, q2.value, t)));
        }

        public static quaternion slerpSafe(quaternion q1, quaternion q2, float t)
        {
            float dt = math.dot(q1, q2);
            if (dt < 0.0f)
            {
                dt = -dt;
                q2.value = -q2.value;
            }

            if (dt < 0.9995f)
            {
                float angle = math.acos(dt);
                float s = math.rsqrt(1.0f - dt * dt);    // 1.0f / sin(angle)
                float w1 = math.sin(angle * (1.0f - t)) * s;
                float w2 = math.sin(angle * t) * s;
                return math.quaternion(q1.value * w1 + q2.value * w2);
            }
            else
            {
                // if the angle is small, use linear interpolation
                return nlerpSafe(q1, q2, t);
            }
        }

        public static quaternion nlerpSafe(quaternion q1, quaternion q2, float t)
        {
            float dt = math.dot(q1, q2);
            if (dt < 0.0f)
            {
                q2.value = -q2.value;
            }

            return math.normalizesafe(math.quaternion(math.lerp(q1.value, q2.value, t)));
        }
        
        public static quaternion GetAxisRotationX(this quaternion quaternion)
        {
            var a = math.sqrt((quaternion.value.w * quaternion.value.w) + (quaternion.value.x * quaternion.value.x));
            return new quaternion(quaternion.value.x, 0, 0, quaternion.value.w / a);
        }

        public static quaternion GetAxisRotationY(this quaternion quaternion)
        {
            var a = math.sqrt((quaternion.value.w * quaternion.value.w) + (quaternion.value.y * quaternion.value.y));
            return new quaternion (0, quaternion.value.y, 0, quaternion.value.w / a);
        }
 
        public static quaternion GetAxisRotationZ(this quaternion quaternion)
        {
            var a = math.sqrt((quaternion.value.w * quaternion.value.w) + (quaternion.value.z * quaternion.value.z));
            return new quaternion(0, 0, quaternion.value.z, quaternion.value.w / a);
        }

        // Roll
        public static float ToEulerX(this quaternion quaternion)
        {
            var roationFloat4 = quaternion.value;
            var sinr_cosp = 2 * (roationFloat4.w * roationFloat4.x + roationFloat4.y * roationFloat4.z);
            var cosr_cosp = 1 - 2 * (roationFloat4.x * roationFloat4.x + roationFloat4.y * roationFloat4.y);
            return math.atan2(sinr_cosp, cosr_cosp);
        }

        // yaw
        public static float ToEulerZ(this quaternion quaternion)
        {
            var roationFloat4 = quaternion.value;
            var siny_cosp = 2 * (roationFloat4.w * roationFloat4.z + roationFloat4.x * roationFloat4.y);
            var cosy_cosp = 1 - 2 * (roationFloat4.y * roationFloat4.y + roationFloat4.z * roationFloat4.z);
            return math.atan2(siny_cosp, cosy_cosp);
        }

        // pitch
        public static float ToEulerY(this quaternion quaternion)
        {
            var roationFloat4 = quaternion.value;
            float pitch = 0;
            var sinp = 2 * (roationFloat4.w * roationFloat4.y - roationFloat4.z * roationFloat4.x);
            if (math.abs(sinp) >= 1)
            {
                pitch = math.PI / 2 * math.sign(sinp); // use 90 degrees if out of range   // math.pi / 2, 
            }
            else
            {
                pitch = math.asin(sinp);
            }
            return pitch;
        }

        //! Converts quaternion representation to euler.
        public static float3 ToEuler(this quaternion quaternion)
        {
            //! \todo Fix this. Doesn't end up the same as the slow version.
            return new float3(quaternion.ToEulerX(), quaternion.ToEulerY(), quaternion.ToEulerZ());
        }
        
        public static float3 ToEulerSlow(this quaternion quaternion)
        {
            return new UnityEngine.Quaternion(quaternion.value.x, quaternion.value.y, quaternion.value.z, quaternion.value.w).eulerAngles;
        }
    }
}
            /*var yaw = quaternion.GetEulerX();
            var degreesToRadians = ((math.PI * 2) / 360f);
            if (math.abs(yaw / degreesToRadians) == 180f)
            {
                // UnityEngine.Debug.LogError("yaw is 180 [pitch]: " + (pitch / degreesToRadians) + " [roll] " + (roll / degreesToRadians) + " [yaw] " + (yaw / degreesToRadians));
                // -90 to 0
                if (pitch < 0)
                {
                    pitch = -yaw - pitch;
                }
                // 90 to 0
                else
                {
                    pitch = yaw - pitch;
                }
            }*/

            
            /*float4 roationFloat4 = quaternion.value;
            double3 res;
            double sinr_cosp = +2.0 * (roationFloat4.w * roationFloat4.x + roationFloat4.y * roationFloat4.z);
            double cosr_cosp = +1.0 - 2.0 * (roationFloat4.x * roationFloat4.x + roationFloat4.y * roationFloat4.y);
            res.x = math.atan2(sinr_cosp, cosr_cosp);
            double sinp = +2.0 * (roationFloat4.w * roationFloat4.y - roationFloat4.z * roationFloat4.x);
            if (math.abs(sinp) >= 1)
            {
                res.y = math.PI / 2 * math.sign(sinp);
            }
            else
            {
                res.y = math.asin(sinp);
            }
            double siny_cosp = +2.0 * (roationFloat4.w * roationFloat4.z + roationFloat4.x * roationFloat4.y);
            double cosy_cosp = +1.0 - 2.0 * (roationFloat4.y * roationFloat4.y + roationFloat4.z * roationFloat4.z);
            res.z = math.atan2(siny_cosp, cosy_cosp);
            return (float3) res;*/