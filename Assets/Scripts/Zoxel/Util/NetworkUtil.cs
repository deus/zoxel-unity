using UnityEngine;
using UnityEngine.Rendering;

namespace Zoxel
{
    public static class NetworkUtil
    {
        public static bool IsHeadless()
        {
            return SystemInfo.graphicsDeviceType == GraphicsDeviceType.Null;
        }
    }
}