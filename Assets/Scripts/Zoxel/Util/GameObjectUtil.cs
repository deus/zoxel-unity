using UnityEngine;

namespace Zoxel
{
    public static class ObjectUtil
    {
        public static void Destroy(Object gameObject)
        {
            if (gameObject != null)
            {
                #if UNITY_EDITOR
                if (!UnityEditor.EditorApplication.isPlaying)
                {
                    GameObject.DestroyImmediate(gameObject);
                    return;
                }
                else
                {
                    GameObject.Destroy(gameObject);
                }
                #else
                GameObject.Destroy(gameObject);
                #endif
            }
        }
    }
}