﻿using Unity.Entities;

namespace Zoxel
{
    //! A ComponentSystemGroup used for Zoxel generic systems.
    public partial class ZoxelGenericSystemGroup : ComponentSystemGroup  { }
}