using Unity.Entities;

namespace Zoxel
{
    //! Links to a BoneLinks Entity.
    public struct SkeletonLink : IComponentData
    {
        public Entity skeleton;

        public SkeletonLink(Entity skeleton)
        {
            this.skeleton = skeleton;
        }
    }
}