using Unity.Entities;

namespace Zoxel
{
    //! Links to a PlayerHome Entity.
    public struct PlayerHomeLink : IComponentData
    {
        public Entity playerHome;

        public PlayerHomeLink(Entity playerHome)
        {
            this.playerHome = playerHome;
        }
    }
}