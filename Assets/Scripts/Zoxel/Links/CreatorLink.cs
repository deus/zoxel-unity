using Unity.Entities;

namespace Zoxel
{
    //! Links to a the Entity that created it.
    public struct CreatorLink : IComponentData
    {        
        public Entity creator;
        
        public CreatorLink(Entity creator)
        {
            this.creator = creator;
        }
    }
}