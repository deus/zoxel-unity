using Unity.Entities;
using Unity.Collections;

namespace Zoxel // .Worlds
{
    //! Links to a array of Planet Entity's.
    public struct WorldLinks : IComponentData
    {
        public BlitableArray<Entity> worlds;

        public void Dispose()
        {
            if (worlds.Length > 0)
            {
                worlds.Dispose();
            }
        }

        public void AddNew(byte newChildrenCount)
        {
            var newChildren = new BlitableArray<Entity>(worlds.Length + newChildrenCount, Allocator.Persistent);
            for (int i = 0; i < worlds.Length; i++)
            {
                newChildren[i] = worlds[i];
            }
            for (int i = worlds.Length; i < newChildren.Length; i++)
            {
                newChildren[i] = new Entity();
            }
            if (worlds.Length > 0)
            {
                worlds.Dispose();
            }
            worlds = newChildren;
        }

        public void AddNew(int newChildrenCount)
        {
            var newChildren = new BlitableArray<Entity>(worlds.Length + newChildrenCount, Allocator.Persistent);
            for (int i = 0; i < worlds.Length; i++)
            {
                newChildren[i] = worlds[i];
            }
            for (int i = worlds.Length; i < newChildren.Length; i++)
            {
                newChildren[i] = new Entity();
            }
            if (worlds.Length > 0)
            {
                worlds.Dispose();
            }
            worlds = newChildren;
        }

        public void Add(Entity newChild)
        {
            var newChildren = new BlitableArray<Entity>(worlds.Length + 1, Allocator.Persistent);
            for (int i = 0; i < worlds.Length; i++)
            {
                newChildren[i] = worlds[i];
            }
            newChildren[worlds.Length] = newChild;
            if (worlds.Length > 0)
            {
                worlds.Dispose();
            }
            worlds = newChildren;
        }
    }
}