﻿using Unity.Entities;

namespace Zoxel
{
    //! Links to a Controller Entity.
    public struct ControllerLink : IComponentData
    {
        public Entity controller;

        public ControllerLink(Entity controller)
        {
            this.controller = controller;
        }
    }
}