﻿using Unity.Entities;

namespace Zoxel
{
    //! Contains an Entity link to a camera.
    public struct CameraLink : IComponentData
    {
        public Entity camera;

        public CameraLink(Entity camera)
        {
            this.camera = camera;
        }
    }
}