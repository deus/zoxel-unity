﻿using Unity.Entities;

namespace Zoxel //./Voxels
{
    //! Links to a Planet Entity.
    public struct VoxLink : IComponentData
    {
        public Entity vox;

        public VoxLink(Entity vox)
        {
            this.vox = vox;
        }
    }
}