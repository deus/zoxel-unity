using Unity.Entities;

namespace Zoxel // .Voxels
{
    //! A link to a material, used for voxels.
    public struct MaterialLink : IComponentData
    {
        public Entity material;

        public MaterialLink(Entity material)
        {
            this.material = material;
        }
    }
}