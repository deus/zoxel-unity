using Unity.Entities;

namespace Zoxel
{
    //! Links to a Character Entity.
    public struct CharacterLink : IComponentData
    {
        public Entity character;

        public CharacterLink(Entity character)
        {
            this.character = character;
        }
    }
}