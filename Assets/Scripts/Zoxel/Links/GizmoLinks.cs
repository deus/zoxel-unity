using Unity.Entities;

namespace Zoxel // .Voxels.Interaction
{
    //! Links to a voxel gizmo used to show what voxel is selected.
    public struct GizmoLinks : IComponentData
    {
        public Entity voxelGizmo;
        // public double lastUpdatedTime;
    }
}