using Unity.Entities;

namespace Zoxel // .Items
{
    //! Links entity to an Item entity.
    public struct ItemLink : IComponentData
    {
        public Entity item;

        public ItemLink(Entity item)
        {
            this.item = item;
        }
    }
}