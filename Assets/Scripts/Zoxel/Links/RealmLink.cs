using Unity.Entities;

namespace Zoxel
{
    //! Links to a Realm Entity.
    public struct RealmLink : IComponentData
    {
        public Entity realm;

        public RealmLink(Entity realm)
        {
            this.realm = realm;
        }
    }
}