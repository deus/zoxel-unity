using Unity.Entities;

namespace Zoxel // .Players
{
    //! Has links to all the player entities.
    public struct PlayerLink : IComponentData
    {
        public Entity player;

        public PlayerLink(Entity player)
        {
            this.player = player;
        }
    }
}