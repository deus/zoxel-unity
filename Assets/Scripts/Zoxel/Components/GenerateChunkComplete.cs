using Unity.Entities;

namespace Zoxel // .Worlds
{
    //! Chunk has finishd generating.
    public struct GenerateChunkComplete : IComponentData { }
}