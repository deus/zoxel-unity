using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel
{
    //! A generic seed component as an integer.
    public struct Seed : IComponentData
    {
        public int seed;

        public Seed(int seed)
        {
            this.seed = seed;
        }

        public uint GetChunkSeed(int3 chunkPosition)
        {
            return (uint)(seed + chunkPosition.x * 16 + chunkPosition.z * (4096));
        }

        public uint GetChunkSeed(float3 chunkPosition)
        {
            return (uint)(seed + chunkPosition.x * 4096 + chunkPosition.z * (16 * 4096));
        }

        public bool SetSeed(int seed)
        {
            if (this.seed != seed)
            {
                this.seed = seed;
                return true;
            }
            return false;
        }
    }
}