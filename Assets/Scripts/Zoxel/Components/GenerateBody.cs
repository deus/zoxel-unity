using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel // .Characters
{
    //! Generates user body, humanoid, items and such.
    public struct GenerateBody : IComponentData
    {
        public byte state;
        public float3 voxScale;
    }
}