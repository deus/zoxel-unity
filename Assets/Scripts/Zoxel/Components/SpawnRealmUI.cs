using Unity.Entities;

namespace Zoxel // .Players.UI
{
    //! Opens game uis. This includes extras.
    public struct SpawnRealmUI : IComponentData
    {
        public byte type;

        public SpawnRealmUI(byte type)
        {
            this.type = type;
        }
    }
}