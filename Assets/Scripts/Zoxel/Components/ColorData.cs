using Unity.Entities;

namespace Zoxel
{
    //! Holds some basic Color data.
    public struct ColorData : IComponentData
    {
        public Color color;

        public ColorData(Color color)
        {
            this.color = color;
        }
    }
}