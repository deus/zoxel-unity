using Unity.Entities;

namespace Zoxel // .VoxelInteraction
{
    //! Closes a players game uis.
    public struct CloseRealmUI : IComponentData { }
}