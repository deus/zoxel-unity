using Unity.Entities;

namespace Zoxel // .Worlds
{
    //! The 2D position of a planet chunk!
    public struct QuadrantPosition2D : IComponentData
    {
        public byte quadrant;
        public int2 position;

        public QuadrantPosition2D(int3 position)
        {
            this.quadrant = (byte) position.z;
            this.position = new int2(position.x, position.y);
        }

        public QuadrantPosition2D(int2 position, byte quadrant)
        {
            this.quadrant = quadrant;
            this.position = new int2(position.x, position.y);
        }

        public QuadrantPosition2D(byte quadrant, int2 position)
        {
            this.quadrant = quadrant;
            this.position = position;
        }

        public int3 GetDictionaryPosition()
        {
            return new int3(position.x, position.y, (int) quadrant);
        }

        public bool IsChunkPosition(int3 chunkPosition)
        {
            if (quadrant == PlanetSide.Up || quadrant == PlanetSide.Down)
            {
                if (position.x == chunkPosition.x && position.y == chunkPosition.z)
                {
                    return true;
                }
            }
            else if (quadrant == PlanetSide.Left || quadrant == PlanetSide.Right)
            {
                if (position.x == chunkPosition.y && position.y == chunkPosition.z)
                {
                    return true;
                }
            }
            else if (quadrant == PlanetSide.Forward || quadrant == PlanetSide.Backward)
            {
                if (position.x == chunkPosition.x && position.y == chunkPosition.y)
                {
                    return true;
                }
            }
            return false;
        }

        public int3 GetVoxelPosition(int3 voxelDimensions)
        {
            if (quadrant == PlanetSide.Up || quadrant == PlanetSide.Down)
            {
                return new int3(position.x * voxelDimensions.x, 0, position.y * voxelDimensions.z);
            }
            else if (quadrant == PlanetSide.Left || quadrant == PlanetSide.Right)
            {
                return new int3(0, position.x * voxelDimensions.y, position.y * voxelDimensions.z);
            }
            else if (quadrant == PlanetSide.Forward || quadrant == PlanetSide.Backward)
            {
                return new int3(position.x * voxelDimensions.x, position.y * voxelDimensions.y, 0);
            }
            return new int3(position.x, position.y, 0);
        }

        public int2 GetVoxelPosition2D(int3 voxelDimensions, int megaChunkDivision)
        {
            if (quadrant == PlanetSide.Up || quadrant == PlanetSide.Down)
            {
                return new int2(position.x * voxelDimensions.x, position.y * voxelDimensions.z) * megaChunkDivision;
            }
            else if (quadrant == PlanetSide.Left || quadrant == PlanetSide.Right)
            {
                return new int2(position.x * voxelDimensions.y, position.y * voxelDimensions.z) * megaChunkDivision;
            }
            else if (quadrant == PlanetSide.Forward || quadrant == PlanetSide.Backward)
            {
                return new int2(position.x * voxelDimensions.x, position.y * voxelDimensions.y) * megaChunkDivision;
            }
            return new int2(position.x, position.y) * megaChunkDivision;
        }

		public void GetChunkBounds(int3 voxelDimensions, out int3 chunkLowerBounds, out int3 chunkUpperBounds)
		{
			var voxelPosition = GetVoxelPosition(voxelDimensions);
			chunkLowerBounds = voxelPosition; // new int3();
            chunkUpperBounds = chunkLowerBounds;
            if (quadrant == PlanetSide.Up || quadrant == PlanetSide.Down)
            {
			    chunkUpperBounds = voxelPosition + new int3(voxelDimensions.x, 0, voxelDimensions.z);
            }
            else if (quadrant == PlanetSide.Left || quadrant == PlanetSide.Right)
            {
			    chunkUpperBounds = voxelPosition + new int3(0, voxelDimensions.y, voxelDimensions.z);
            }
            else if (quadrant == PlanetSide.Forward || quadrant == PlanetSide.Backward)
            {
			    chunkUpperBounds = voxelPosition + new int3(voxelDimensions.x, voxelDimensions.y, 0);
            }
		}
    }
}