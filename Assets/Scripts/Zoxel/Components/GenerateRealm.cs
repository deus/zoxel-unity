using Unity.Entities;

namespace Zoxel
{
    //! An event that generates our realm data.
    /**
    *   \todo Node Graph - to show how voxels are spawned per realm - showing how it comes up with game rules, tiers, etc
    */
    public struct GenerateRealm : IComponentData
    {
        public byte state;
    }
}