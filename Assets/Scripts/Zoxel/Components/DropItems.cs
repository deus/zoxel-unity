using Unity.Entities;

namespace Zoxel.Items
{
    //! Makes an entity drop items from its Inventory into the world.
    public struct DropItems : IComponentData { }
}