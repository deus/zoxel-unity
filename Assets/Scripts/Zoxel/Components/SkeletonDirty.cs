using Unity.Entities;
using Unity.Collections;

namespace Zoxel // .Skeletons
{
    //! An event that signifies Equipment has updated and the BoneLinks needs rebuilding.
    public struct SkeletonDirty : IComponentData
    {
        public byte state;
    }
}