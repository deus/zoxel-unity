using Unity.Entities;

namespace Zoxel
{
    //! Creates character save folders.
    public struct InitializeSaver : IComponentData { }
}