using Unity.Entities;

namespace Zoxel
{
    //! For Entities that save to a io path.
    /**
    *   \todo Remove realmID/worldID and just use generically with a save path.
    */
    public struct EntitySaver : IComponentData 
    {
        public Text savePath;   //! Cached SavePath for character folder.
        public int realmID;      //! Cached realmID for the remaining things without BinaryWriters
        public int worldID;

        public EntitySaver(int realmID, int worldID)
        {
            this.savePath = new Text();
            this.realmID = realmID;
            this.worldID = worldID;
        }

        public void Dispose()
        {
            savePath.Dispose();
        }

        public void SetPath(int characterID)
        {
            Dispose();
            var characterSavePath = GetCharacterPath(realmID, worldID, characterID);
            savePath = new Text(characterSavePath);
        }

        public void SetPath(int characterID, bool isPlayer)
        {
            Dispose();
            string characterSavePath;
            if (isPlayer)
            {
                characterSavePath = GetPlayerPath(realmID, characterID);
            }
            else
            {
                characterSavePath = GetCharacterPath(realmID, worldID, characterID);
            }
            savePath = new Text(characterSavePath);
        }

        public string GetPath()
        {
            if (savePath.Length == 0)
            {
                return "";
            }
            return SaveUtilities.GetRealmsPath() + savePath.ToString();
        }

        // SaveUtilities
        public static string GetCharacterPath(int realmID, int worldID, int characterID)
        {
            return realmID + "/Worlds/" + worldID + "/Characters/" + characterID.ToString() + "/";
                // "/Chunk_" + chunkPosition.x + "_" + chunkPosition.y + "_" + chunkPosition.z + "/" +
        }

        public static string GetPlayerPath(int realmID, int characterID)
        {
            return realmID + "/Players/" + characterID + "/";
        }
    }
}

        /*public string GetPath(int worldID, int characterID, int3 chunkPosition, bool isPlayer)
        {
            if (savePath.Length == 0)
            {
                var characterSavePath = SaveUtilities.GetCharacterPath(realmID, worldID, characterID, chunkPosition, isPlayer);
                savePath = new Text(characterSavePath);
            }
            return SaveUtilities.GetRealmsPath() + savePath.ToString();
        }*/