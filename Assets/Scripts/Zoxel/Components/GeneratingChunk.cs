using Unity.Entities;

namespace Zoxel // .Worlds
{
    //! An event for generating a chunk's terrain.
    public struct GenerateChunk : IComponentData
    {
        public byte state;
    }
}