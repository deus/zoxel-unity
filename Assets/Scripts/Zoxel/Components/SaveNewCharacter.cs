using Unity.Entities;

namespace Zoxel // .Characters
{
    //! An io event to save all the character datas.
    public struct SaveNewCharacter : IComponentData { }
}