using Unity.Entities;

namespace Zoxel
{
    //! Disables IO operations on an entity.
    public struct DisableSaving : IComponentData { }
}