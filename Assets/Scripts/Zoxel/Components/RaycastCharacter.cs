﻿using Unity.Entities;
using Unity.Mathematics;
// todo: Seperate this out to Raycaster and TargetVoxel (and rename Target to TargetCharacter)
// Can also add data whether it passes through certain voxels or types of voxels
// type if 1 - hit character

namespace Zoxel // .VoxelInteraction
{
    //! Raycasting a character info.
    public struct RaycastCharacter : IComponentData
    {
        public Entity character;
        public float3 hitCharacterPosition;
        public float hitCharacterHeight;

        public void OnHitFailed()
        {
            this.character = new Entity();
        }

        public void OnHitCharacter(Entity character)
        {
            this.character = character;
        }
    }
}