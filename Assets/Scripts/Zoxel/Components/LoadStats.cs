using Unity.Entities;

namespace Zoxel // .Stats
{
    //! Loads Stats onto an entity.
    /**
    *   \todo Add Async File Loading with AsyncFileReader.
    */
    public struct LoadStats : IComponentData
    {
        public Text loadPath;
        public AsyncFileReader reader;

        public void Dispose()
        {
            loadPath.Dispose();
            reader.Dispose();
        }
    }
}