using Unity.Entities;

namespace Zoxel
{
    //! A tag for an entity that saves things
    public struct Saver : IComponentData { }
}