﻿using Unity.Entities;

namespace Zoxel.Transforms
{
    //! Deals with parenting and positioning.
    [UpdateAfter(typeof(AnimationSystemGroup))]
    public partial class TransformSystemGroup : ComponentSystemGroup
    {
        public const int maxParents = 64;
    }
}