using Unity.Entities;
using Zoxel.Transforms;

namespace Zoxel // .Skeletons
{
    //! Updates bone matrcies and other bone stuff.
    /**
    *   Needs most recent transform data.
    */
    [UpdateAfter(typeof(TransformSystemGroup))]
    public partial class SkeletonSystemGroup : ComponentSystemGroup { }
}