﻿using Unity.Entities;
// Updates after SkeletonGroup

namespace Zoxel // .Cameras
{
    //! Handles Cameras and UIs based on that.
    [UpdateAfter(typeof(PhysicsSystemGroup))]
    public partial class CameraSystemGroup : ComponentSystemGroup { }
}