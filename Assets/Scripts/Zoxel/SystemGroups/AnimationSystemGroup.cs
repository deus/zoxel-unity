﻿using Unity.Entities;
using Zoxel.Transforms;

namespace Zoxel //.Animations
{
    //! Moves transforms around!
    /**
    *   Should this go before or after transforms?
    */
    [UpdateAfter(typeof(CameraSystemGroup))]
    public partial class AnimationSystemGroup : ComponentSystemGroup { }
}
