﻿using Unity.Entities;
// The Order is:
//      Movement
//      Collision
//      Camera - Updates UI Positions
//      TransformSystemGroup
//      Zender
// [UpdateBefore(typeof(CollisionSystemGroup))]

namespace Zoxel //.Movement
{
    //! Updates positions and rotations using forces.
    public partial class PhysicsSystemGroup : ComponentSystemGroup { }
}