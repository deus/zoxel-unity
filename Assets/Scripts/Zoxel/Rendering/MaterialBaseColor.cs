using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel
{
    //! A base color value used inside the shaders.
    public struct MaterialBaseColor : IComponentData
    {
        public float4 Value;

        public MaterialBaseColor(Color color)
        {
            this.Value = color.ToFloat4();
        }

        public MaterialBaseColor(UnityEngine.Color color)
        {
            this.Value = new float4(color.r, color.g, color.b, color.a);
        }

        public MaterialBaseColor(float4 Value)
        {
            this.Value = Value;
        }

        public MaterialBaseColor(float x, float y, float z, float w)
        {
            Value = new float4(x, y, z, w);
        }
    }
}

//[Serializable]
//[MaterialProperty("_BaseColor", MaterialPropertyFormat.Float4)]