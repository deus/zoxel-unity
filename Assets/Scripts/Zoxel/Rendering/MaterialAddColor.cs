using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel
{
    //! A addition color value used inside the shaders.
    /**
    *   Used for damage flash effects.
    */
    public struct MaterialAddColor : IComponentData
    {
        public float4 Value;
    }
}