using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel
{
    //! A frame color value used inside the ui shaders.
    public struct MaterialFrameColor : IComponentData
    {
        public float4 Value;

        public MaterialFrameColor(float4 Value)
        {
            this.Value = Value;
        }

        public MaterialFrameColor(Color color)
        {
            this.Value = color.ToFloat4();
        }
    }
}