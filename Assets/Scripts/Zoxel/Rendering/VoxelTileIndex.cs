using Unity.Entities;

namespace Zoxel
{
    //! A tile index value used inside the world voxel shaders.
    public struct VoxelTileIndex : IComponentData
    { 
        public int index;

        public VoxelTileIndex(int index)
        {
            this.index = index;
        }
    }
}