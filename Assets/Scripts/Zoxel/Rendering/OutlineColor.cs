using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel
{
    //! A outline color value used inside the grass shaders.
    public struct OutlineColor : IComponentData
    {
        public float4 color;

        public OutlineColor(float x, float y, float z, float w)
        {
            color = new float4(x, y, z, w);
        }
    }
}