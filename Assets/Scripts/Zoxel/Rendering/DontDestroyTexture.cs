using Unity.Entities;

namespace Zoxel
{
    //! Added to an entity that doesn't destroy the texture data on cleanup.
    public struct DontDestroyTexture : IComponentData {  }
}