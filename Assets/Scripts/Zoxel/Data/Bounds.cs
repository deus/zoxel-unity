using System;
using Unity.Mathematics;

namespace Zoxel
{
    //! Contains a position and extends (size).
    /**
    *   Has an intersect function as well.
    */
    public struct Bounds
    {
        public float3 position;
        public float3 extents;

        public Bounds(float3 position, float3 extents)
        {
            this.position = position;
            this.extents = extents;
        }

        public bool Intersects(in Bounds bounds)
        {
            return !(position.x + extents.x < bounds.position.x - bounds.extents.x
                || position.x - extents.x > bounds.position.x + bounds.extents.x
                || position.y + extents.y < bounds.position.y - bounds.extents.y
                || position.y - extents.y > bounds.position.y + bounds.extents.y
                || position.z + extents.z < bounds.position.z - bounds.extents.z
                || position.z - extents.z > bounds.position.z + bounds.extents.z);
        }
    }
}