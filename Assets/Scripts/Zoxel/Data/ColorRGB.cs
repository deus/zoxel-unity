using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using System;

namespace Zoxel
{
    //! RGB colors for our game. Stored in bytes.
    /**
    *   \todo Implement hash code?
    */
    [Serializable]
    public struct ColorRGB : IEquatable<ColorRGB>
    {
        public byte red;
        public byte green;
        public byte blue;

        public ColorRGB(float3 color)
        {
            this.red = (byte) ((int) (255 * color.x));
            this.green = (byte) ((int) (255 * color.y));
            this.blue = (byte) ((int) (255 * color.z));
        }

        public ColorRGB(byte red_, byte green_, byte blue_)
        {
            red = red_; green = green_; blue = blue_;
        }

        public ColorRGB(float red, float green, float blue)
        {
            this.red = (byte) ((int) (255 * red));
            this.green = (byte) ((int) (255 * green));
            this.blue = (byte) ((int) (255 * blue));
        }
        
        public ColorRGB(UnityEngine.Color color)
        {
            red = ((byte)(255 * color.r));
            green = ((byte)(255 * color.g));
            blue = ((byte)(255 * color.b));
        }

        public override string ToString()
        {
            return red + ":" + green + ":" + blue;
        }

        public ColorRGB Invert()
        {
            var color = new ColorRGB(
                red < 128 ? ((byte)(red + 128)) : ((byte)(red - 128)), 
                green < 128 ? ((byte)(green + 128)) : ((byte)(green - 128)),
                blue < 128 ? ((byte)(blue + 128)) : ((byte)(blue - 128)));
            return new ColorRGB(color.green, color.blue, color.red);
        }

        public ColorRGB Invert2()
        {
            return new ColorRGB(
                red < 128 ? ((byte)(red + 128)) : ((byte)(red - 128)), 
                green < 128 ? ((byte)(green + 128)) : ((byte)(green - 128)),
                blue < 128 ? ((byte)(blue + 128)) : ((byte)(blue - 128)));
        }

        public Color ToColor()
        {
            return new Color(red, green, blue, 255);
        }
        
        public Color GetColorRGBA()
        {
            return new Color(red, green, blue, 255);
        }

        public UnityEngine.Color GetColor()
        {
            var red2 = ((int)red) / 255f;
            var green2 = ((int)green) / 255f;
            var blue2 = ((int)blue) / 255f;
            return new UnityEngine.Color(red2, green2, blue2, 1);
           // return new UnityEngine.Color((int)red, (int) green, (int) blue, (int) alpha);
        }

        public float3 ToFloat3()
        {
            return new float3(((int)red), ((int)green), ((int)blue)) / 255f;
        }



        public bool Equals(ColorRGB p)
        {
            return red == p.red && green == p.green && blue == p.blue;
        }
        public override bool Equals(object obj)
        {
            return obj is ColorRGB && Equals((ColorRGB) obj);
        }

        public static bool operator ==(ColorRGB a, ColorRGB b)
        {
            return a.red == b.red && a.green == b.green && a.blue == b.blue;
        }

        public static bool operator !=(ColorRGB a, ColorRGB b)
        {
            return a.red != b.red || a.green != b.green || a.blue != b.blue;
        }
        
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = red.GetHashCode();
                hash ^= green.GetHashCode();
                hash ^= blue.GetHashCode();
                return hash;
            }
        }

        public ColorRGB MultiplyBrightness(float valueMultiplier)
        {
            var hsv = this.ToColor().GetHSV();
            hsv.z *= valueMultiplier;
            /*var rgb = GetColorFromHSV(hsv);
            this.red = rgb.red;
            this.green = rgb.green;
            this.blue = rgb.blue;*/
            return Color.GetColorFromHSV(hsv).ToColorRGB();
        }
    }
}