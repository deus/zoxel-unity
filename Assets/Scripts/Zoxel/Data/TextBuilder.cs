using Unity.Entities;
using Unity.Collections;

namespace Zoxel
{
    //! Used to build lots of Text's together. \todo Use Memory operations to directly copy the byte arrays into the new Text when building.
    public struct TextBuilder
    {
        public BlitableArray<Text> texts;

        public TextBuilder(int count)
        {
            this.texts = new BlitableArray<Text>(count, Allocator.Temp);
        }

        public Text Build()
        {
            var count = 0;
            for (int i = 0; i < texts.Length; i++)
            {
                count += texts[i].Length;
            }
            var builtText = new Text(count);
            var index = 0;
            for (int i = 0; i < texts.Length; i++)
            {
                var text = texts[i];
                for (int j = 0; j < text.Length; j++)
                {
                    builtText[index] = text[j];
                    index++;
                }
            }
            return builtText;
        }

        public void DisposeFinal()
        {
            this.texts.DisposeFinal();
        }
    }
}