using System;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Zoxel
{
	//! A memory container (void*) that stores a array of generic data.
	public struct BlitableHashmap<K, V> : IDisposable
        where K : struct, IEquatable<K>
        where V : struct
	{
        // [StructLayout(LayoutKind.Sequential)]
        public struct Entry
        {
            public K key;
            public V value;
            public int next;
        }
        internal BlitableArray<int> buckets;
        internal BlitableArray<Entry> data;
        internal int halfPv;

		public int Length
        {
            get
            { 
                return data.Length;
            }
        }

		public BlitableHashmap(int dataLength, Allocator allocator, int bucketsLength = 16)
		{
            this.buckets = new BlitableArray<int>(bucketsLength, allocator);
            this.data = new BlitableArray<Entry>(dataLength, allocator);
            this.halfPv = 0;
		}

		public void Dispose()
		{
            if (Length > 0)
            {
                buckets.Dispose();
                data.Dispose();
            }
		}
 
		public V this[K key]
        {
			get
			{
                if (this.halfPv <= 0 || this.Length <= 5)
                {
                    for (int i = 0, c = this.Length; i < c; ++i)
                    {
                        var entry = this.data[i];
                        if (entry.key.Equals(key))
                        {
                            return entry.value;
                        }
                    }
                }
                else
                {
                    int bucketIndex = key.GetHashCode() % this.halfPv;
                    bucketIndex = this.buckets[bucketIndex + this.halfPv];
                    while (bucketIndex != -1)
                    {
                        var entry = this.data[bucketIndex];
                        var entryKey = entry.key;
                        if (entryKey.Equals(key))
                        {
                            return entry.value;
                        }
                        bucketIndex = entry.next;
                    }
                }
                // return new V();
                throw new Exception(); // KeyNotFoundException();
            }
        }

        //! \todo Add KeyValuePair to hashmap.
        public bool Add(ref K key, ref V value)
        {
            var bucketsCount = buckets.Length;
            var length = this.Length;
            var index = key.GetHashCode() & (bucketsCount - 1);
            var newEntry = new Entry { key = key, value = value, next = index };
            return true;
        }
    
        public bool ContainsKey(in K key)
        {
            var length = this.Length;
            if (this.halfPv <= 0 || length <= 5)
            {
                for (int i = 0; i < length; i++)
                {
                    var entry = this.data[i];
                    if (entry.key.Equals(key))
                    {
                        return true;
                    }
                }
            }
            else
            {
                int bucketIndex = key.GetHashCode() % this.halfPv;
                bucketIndex = this.buckets[bucketIndex + this.halfPv];
                while (bucketIndex != -1)
                {
                    var entry = this.data[bucketIndex];
                    if (entry.key.Equals(key))
                    {
                        return true;
                    }
                    bucketIndex = entry.next;
                }
            }
            return false;
        }
 
        public NativeArray<K> GetKeys(Allocator allocator = Allocator.Temp)
        {
            var array = new NativeArray<K>(Length, allocator, NativeArrayOptions.UninitializedMemory);
            for (var i = 0; i < Length; ++i)
            {
                var entry = data[i];
                array[i] = entry.key;
            }
            return array;
        }

		/*unsafe public T this[int index]
		{
			get
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				if (m_AllocatorLabel == Allocator.Invalid)
					throw new ArgumentException("AutoGrowArray was not initialized.");

				if (index >= Length)
					throw new IndexOutOfRangeException();
#endif

				return UnsafeUtility.ReadArrayElement<T>(m_Buffer, index);
			}
			set
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				if (m_AllocatorLabel == Allocator.Invalid)
					throw new ArgumentException("AutoGrowArray was not initialized.");

				if (index >= Length)
					throw new IndexOutOfRangeException();
#endif
				UnsafeUtility.WriteArrayElement(m_Buffer, index, value);
			}
		}

		public BlitableArray(int m_Length, Allocator allocator)
		{
			this.m_AllocatorLabel = allocator;
			this.m_Length = m_Length;
			m_Buffer = UnsafeUtility.Malloc(m_Length * UnsafeUtility.SizeOf<T>(), UnsafeUtility.AlignOf<T>(), allocator);
		}

		public BlitableArray(ref NativeArray<T> oldNativeArray, Allocator allocator)
		{
			this.m_AllocatorLabel = allocator;
			this.m_Length = oldNativeArray.Length;
			// m_Buffer = UnsafeUtility.Malloc(m_Length * UnsafeUtility.SizeOf<T>(), UnsafeUtility.AlignOf<T>(), m_AllocatorLabel);
			m_Buffer = oldNativeArray.GetUnsafePtr();	//  NativeArrayUnsafeUtility.GetUnsafePtr<T>(m_Buffer, this.m_Length * UnsafeUtility.SizeOf<T>(), allocator);
		}

		public BlitableArray(in NativeList<T> oldNativeList, Allocator allocator)
		{
			this.m_AllocatorLabel = allocator;
			this.m_Length = oldNativeList.Length;
			m_Buffer = UnsafeUtility.Malloc(m_Length * UnsafeUtility.SizeOf<T>(), UnsafeUtility.AlignOf<T>(), allocator);
			for (var i = 0; i < Length; i++)
			{
				this[i] = oldNativeList[i];
			}
		}

		public BlitableArray<T> Clone()
		{
			if (m_AllocatorLabel != Allocator.Persistent)
			{
				return new BlitableArray<T>();
			}
			var clone = new BlitableArray<T>(m_Length, m_AllocatorLabel);
			var stride = UnsafeUtility.SizeOf<T>();
			UnsafeUtility.MemCpy(clone.m_Buffer, m_Buffer, stride * m_Length);

			// UnsafeUtility.MemCpy(m_Buffer, clone.m_Buffer, m_Length);
			// var stride = UnsafeUtility.SizeOf<T>();
			// UnsafeUtility.MemCpyStride(m_Buffer, stride, clone.m_Buffer, stride, stride, m_Length);
			// UnsafeUtility.MemCpyReplicate(m_Buffer, clone.m_Buffer, stride, m_Length);
			return clone;
		}

		public NativeArray<T> ToNativeArray(Allocator allocator)
		{
			var nArray = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<T>(m_Buffer, m_Length * UnsafeUtility.SizeOf<T>(), allocator);
#if ENABLE_UNITY_COLLECTIONS_CHECKS
    		NativeArrayUnsafeUtility.SetAtomicSafetyHandle(ref nArray, AtomicSafetyHandle.Create());
#endif
			return nArray;
		}

		public NativeArray<T> ToNativeArray(int length, Allocator allocator)
		{
			var nArray = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<T>(m_Buffer, length * UnsafeUtility.SizeOf<T>(), allocator);
#if ENABLE_UNITY_COLLECTIONS_CHECKS
    		NativeArrayUnsafeUtility.SetAtomicSafetyHandle(ref nArray, AtomicSafetyHandle.Create());
#endif
			return nArray;
		}

		public T[] ReadIntoArray(ref T[] array)
		{
			if (array.Length != Length)
			{
				array = new T[Length];
			}
			for (var i = 0; i < Length; i++)
			{
				array[i] = this[i];
			}
			return array;
		}

		public T[] ToArray()
		{
			var array = new T[Length];
			for (var i = 0; i < Length; i++)
			{
				array[i] = this[i];
			}
			return array;
		}

		public NativeArray<T> CloneTemp()
		{
			var nativeArray = new NativeArray<T>(m_Length, Allocator.Temp);
			for (int i = 0; i < nativeArray.Length; i++)
			{
				nativeArray[i] = this[i];
			}
			return nativeArray;
		}*/
	}
}