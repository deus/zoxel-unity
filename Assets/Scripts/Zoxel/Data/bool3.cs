using System;
using Unity.Mathematics;

namespace Zoxel
{
    //! Stores bools in a byte! Currently supports 3.
    [Serializable]
    public struct bool3 : IEquatable<bool3>
    {
        public byte data;

        public bool3(bool a, bool b, bool c)
        {
            this.data = 0;
            SetBoolA(a);
            SetBoolB(b);
            SetBoolC(c);
        }
        
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                return data.GetHashCode();
            }
        }

        public override bool Equals(object obj)
        {
            return obj is bool3 && Equals((bool3) obj);
        }
        
        public bool Equals(bool3 a)
        {
            return data == a.data;
        }

        public void SetBoolA(bool state)
        {
            SetBool(0, state);
        }

        public void SetBoolB(bool state)
        {
            SetBool(1, state);
        }

        public void SetBoolC(bool state)
        {
            SetBool(2, state);
        }

        public bool GetBoolA()
        {
            return GetBool(0);
        }

        public bool GetBoolB()
        {
            return GetBool(1);
        }

        public bool GetBoolC()
        {
            return GetBool(2);
        }

        public void Reset()
        {
            data = 0;
        }

		public void SetBool(int index, bool state)
		{
            if (state)
            {
                data |= (byte)(1 << index);
            }
            else
            {
                data |= (byte)(0 << index);
            }
		}

		public bool GetBool(int index)
		{
			if ((data & (1 << index)) > 0) 
			{
				return true;
			}
			else
			{
				return false;
			}
		}
    }
}