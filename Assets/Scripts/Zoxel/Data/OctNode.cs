using System;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Mathematics;

namespace Zoxel
{
	//! A memory container (void*) that stores a array of generic data.
	unsafe public struct OctNode<T> : IDisposable where T : struct
	{
        public T value;
		private void* pointer;
		private Allocator allocator;

        // Opens a closed node
		public OctNode(T value)
		{
            this.value = value;
			this.allocator = Allocator.None;
			this.pointer = null;
		}

        // Opens a opened node
		public OctNode(T value, Allocator allocator)
		{
            this.value = value;
			this.allocator = allocator;
			this.pointer = UnsafeUtility.Malloc(8 * UnsafeUtility.SizeOf<OctNode<T>>(), UnsafeUtility.AlignOf<OctNode<T>>(), allocator);
            for (byte i = 0; i < 8; i++)
            {
                this[i] = new OctNode<T>(value);
            }
		}

        //! Opens an open node of depth level
		public OctNode(T value, Allocator allocator, byte depth)
		{
            this.value = value;
            this.allocator = allocator;
            if (depth == 0)
            {
                this.pointer = null;
            }
            else
            {
                depth = (byte) (depth - 1);
                this.pointer = UnsafeUtility.Malloc(8 * UnsafeUtility.SizeOf<OctNode<T>>(), UnsafeUtility.AlignOf<OctNode<T>>(), allocator);
                for (byte i = 0; i < 8; i++)
                {
                    this[i] = new OctNode<T>(value, allocator, depth);
                }
            }
		}

		unsafe public void DisposeFinal()
		{
			if (IsOpen)
			{
                for (byte i = 0; i < Length; i++)
                {
                    this[i].DisposeFinal();
                }
				UnsafeUtility.Free(pointer, allocator);
                pointer = null;
			}
		}

		unsafe public void Dispose()
		{
			if (IsOpen)
			{
                for (byte i = 0; i < Length; i++)
                {
                    this[i].Dispose();
                }
				UnsafeUtility.Free(pointer, allocator);
                pointer = null;
			}
		}
        
        public bool IsOpen
		{
			get
			{
				return pointer != null; // allocator != Allocator.None;
			}
		}
        
        public int Length
		{
			get
			{
                if (!IsOpen)
                {
                    return 0;
                }
				return 8;
			}
		}
        
        public OctNode<T> this[int index]
		{
			get
			{
				return this[(byte) index];
			}
			set
			{
                this[(byte) index] = value;
			}
		}
        
        unsafe public OctNode<T> this[byte index]
		{
			get
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				if (allocator == Allocator.Invalid)
					throw new ArgumentException("AutoGrowArray was not initialized.");

				if (index >= Length)
					throw new IndexOutOfRangeException();
#endif
				return UnsafeUtility.ReadArrayElement<OctNode<T>>(pointer, index);
			}
			set
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				if (allocator == Allocator.Invalid)
					throw new ArgumentException("AutoGrowArray was not initialized.");

				if (index >= Length)
					throw new IndexOutOfRangeException();
#endif
				UnsafeUtility.WriteArrayElement(pointer, index, value);
			}
		}

        //! Used to convert voxel position from a depth 4 to a lesser depth gravity octree.
        public T GetValueDepthDifference(byte3 position, byte inputDepth, byte depth)
        {
            if (inputDepth < depth)
            {
                return GetValue(position, depth);
            }
            var depthDifference = (byte) (inputDepth - depth);
            var dimensionsDifference = OctNodeHelper.GetDimensions(depthDifference);
            // UnityEngine.Debug.LogError("depthDifference: " + depthDifference + ", Depth Difference: " + dimensionsDifference);
            var position2 = position / dimensionsDifference;
            return GetValue(position2, depth);
        }

        public T GetValue(byte3 position, byte depth)
        {
            if (depth == 0)
            {
                return value;
            }
            else
            {
                if (!IsOpen)
                {
                    return value;
                }
                var dimensions = OctNodeHelper.GetDimensions(depth);
                var localPosition = GetLocalNodePosition(position, dimensions);
                var arrayIndex = GetNodeArrayIndex(localPosition);
                if (arrayIndex < 0 || arrayIndex >= 8)
                {
                    //UnityEngine.Debug.LogError("ArrayIndex out of bounds: " + arrayIndex + " : " + position);
                    return value;
                }
                var innerNodePosition = GetInnerNodePosition(position, dimensions);
                return this[arrayIndex].GetValue(innerNodePosition, (byte) (depth - 1));
            }
        }

        public void SetValue(byte3 position, byte depth, byte dimensions, T value)
        {
            if (depth == 0)
            {
                //UnityEngine.Debug.LogError("Set: " + value + " : " + position);
                this.value = value;
            }
            else
            {
                if (!IsOpen)
                {
                    //UnityEngine.Debug.LogError("!IsOpen - Set: " + position);
                    this.value = value;
                    return;
                }
                // var dimensions2 = GetDimensions(depth);
                var localPosition = GetLocalNodePosition(position, dimensions);
                var arrayIndex = GetNodeArrayIndex(localPosition);
                if (arrayIndex < 0 || arrayIndex >= 8)
                {
                    //UnityEngine.Debug.LogError("ArrayIndex out of bounds: " + arrayIndex + " : " + position);
                    return;
                }
                var childNode = this[arrayIndex];
                var innerNodePosition = GetInnerNodePosition(position, dimensions);
                //UnityEngine.Debug.LogError("GetInnerNodePosition position: " + position + ", innerNodePosition: " + innerNodePosition +
                //    ", localPosition: " + localPosition + ":" + arrayIndex + ", depth: " + depth + ", dimensions: " + dimensions);
                childNode.SetValue(innerNodePosition, (byte) (depth - 1), (byte) (dimensions / 2), value);
                this[arrayIndex] = childNode;
            }
        }

        private static int GetNodeArrayIndex(byte3 position)
        {
            return (position.z + 2 * (position.y + 2 * position.x));
        }

        private static byte3 GetInnerNodePosition(byte3 position, byte dimensions)
        {
            dimensions = (byte) (dimensions / 2);
            if (position.x >= dimensions)
            {
                position.x -= dimensions;
            }
            if (position.y >= dimensions)
            {
                position.y -= dimensions;
            }
            if (position.z >= dimensions)
            {
                position.z -= dimensions;
            }
            return position;
        }

        private static byte3 GetLocalNodePosition(byte3 position, byte dimensions)
        {
            var outputPosition = new byte3();
            if (position.x >= dimensions / 2)
            {
                outputPosition.x = 1;
            }
            else
            {
                outputPosition.x = 0;
            }
            if (position.y >= dimensions / 2)
            {
                outputPosition.y = 1;
            }
            else
            {
                outputPosition.y = 0;
            }
            if (position.z >= dimensions / 2)
            {
                outputPosition.z = 1;
            }
            else
            {
                outputPosition.z = 0;
            }
            return outputPosition;
            /*if (position.x >= 2)
            {
                if (position.x >= dimensions / 2)
                {
                    position.x = 1;
                }
                else
                {
                    position.x = 0;
                }
            }
            if (position.y >= 2)
            {
                if (position.y >= dimensions / 2)
                {
                    position.y = 1;
                }
                else
                {
                    position.y = 0;
                }
            }
            if (position.z >= 2)
            {
                if (position.z >= dimensions / 2)
                {
                    position.z = 1;
                }
                else
                {
                    position.z = 0;
                }
            }*/
            /*var newPosition = position;
            if (position.x >= 2)
            {
                newPosition.x %= 2;
            }
            if (position.y >= 2)
            {
                newPosition.y %= 2;
            }
            if (position.z >= 2)
            {
                newPosition.z %= 2;
            }
            return newPosition;*/
        }

        public void OpenNode()
        {
            if (!IsOpen)
            {
                this.pointer = UnsafeUtility.Malloc(8 * UnsafeUtility.SizeOf<OctNode<T>>(), UnsafeUtility.AlignOf<OctNode<T>>(), allocator);
                for (byte i = 0; i < 8; i++)
                {
                    this[i] = new OctNode<T>(value, allocator);
                }
            }
        }

        public void CloseNode()
        {
            if (!IsOpen)
            {
                for (byte i = 0; i < Length; i++)
                {
                    this[i].DisposeFinal();
                }
                this.pointer = null;
            }
        }
	}
}