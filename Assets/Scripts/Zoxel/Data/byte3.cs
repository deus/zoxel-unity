using Unity.Mathematics;
using System;

namespace Zoxel
{
	//! An byte, but 3 of them. Used for alot of voxel spatial coordinates.
    [Serializable]
    public struct byte3 : IEquatable<byte3>, IComparable<byte3>
    {
        public byte x;
        public byte y;
        public byte z;

        public byte3(byte x_, byte y_, byte z_)
        {
            x = x_; y = y_; z = z_;
        }

        public byte3(byte3 newOne)
        {
            x = newOne.x;
            y = newOne.y; 
            z = newOne.z;
        }

        public byte3(float3 newOne)
        {
            x = (byte) newOne.x;
            y = (byte) newOne.y; 
            z = (byte) newOne.z;
        }

        public int CompareTo(byte3 other)
        {
            if (other.x == x && other.y == y && other.z == z)
            {
                return 0;
            }
            if (other.x > x)
            {
                return 1;
            }
            else if (other.x < x)
            {
                return -1;
            }
            if (other.z > z)
            {
                return 1;
            }
            else if (other.z < z)
            {
                return -1;
            }
            if (other.y > y)
            {
                return 1;
            }
            else if (other.y < y)
            {
                return -1;
            }
            return 0;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                /*byte hash = 17;
                hash = hash * 23 + x.GetHashCode();
                hash = hash * 23 + y.GetHashCode();
                hash = hash * 23 + z.GetHashCode();
                return hash;*/
                /*int result = 0;
                result = (byte)(value >> 8);
                data[index + 2] = (byte)(value >> 16);
                data[index + 3] = (byte)(value >> 24);
                result = (result * 31) ^ x;
                result = (result * 31) ^ y;
                result = (result * 31) ^ z;
                    return result;*/


                /*const int p = 16777619;
                int hash = (int)2166136261;

                hash = (hash ^ x) * p;
                hash = (hash ^ y) * p;
                hash = (hash ^ z) * p;

                hash += hash << 13;
                hash ^= hash >> 7;
                hash += hash << 3;
                hash ^= hash >> 17;
                hash += hash << 5;
                return hash;*/
                // Shifts bytes into an integer
                return  (z  & 0xff << 16) |
                        (y  & 0xff <<  8) |
                        (x  & 0xff);
            }
        }

        public override bool Equals(object obj)
        {
            return obj is byte3 && Equals((byte3) obj);
        }
        
        public bool Equals(byte3 p)
        {
            return x == p.x && y == p.y && z == p.z;
        }
        
        public static bool operator ==(byte3 a, byte3 b)
            => a.x == b.x && a.z == b.z && a.y == b.y;

        public static bool operator !=(byte3 a, byte3 b)
            => a.x != b.x || a.z != b.z || a.y != b.y;

        public static byte3 operator /(byte3 a, float b)
            => new byte3((byte)(a.x / b), (byte)(a.y / b), (byte)(a.z / b));
        
        public static byte3 operator /(byte3 a, int b)
            => new byte3((byte)(a.x / b), (byte)(a.y / b), (byte)(a.z / b));

        public override string ToString()
        {
            return "" + x + "," + y + "," + z;
        }

        /*public static byte3 operator -(byte3 a)
            => new byte3(- a.x, - a.y, - a.z);
        
        public static byte3 operator +(byte3 a, byte3 b)
            => new byte3(a.x + b.x, a.y + b.y, a.z + b.z);
        
        public static byte3 operator -(byte3 a, byte3 b)
            => new byte3(a.x - b.x, a.y - b.y, a.z - b.z);

        public static byte3 operator *(byte3 a, float b)
            => new byte3((byte)(a.x * b), (byte)(a.y * b), (byte)(a.z * b));

        public static byte3 operator /(byte3 a, float b)
            => new byte3((byte)(a.x / b), (byte)(a.y / b), (byte)(a.z / b));

        public static byte3 operator *(byte3 a, byte b)
            => new byte3(a.x * b, a.y * b, a.z * b);
        
        public static byte3 operator /(byte3 a, byte b)
            => new byte3(a.x / b, a.y / b, a.z / b);*/

        public float3 ToFloat3()
        {
            return new float3(x, y, z);
        }

        public int3 Left()
        {
            return new int3(x - 1, y, z);
        }

        public int3 Right()
        {
            return new int3(x + 1, y, z);
        }

        public int3 Forward()
        {
            return new int3(x, y, z + 1);
        }

        public int3 Backward()
        {
            return new int3(x, y, z - 1);
        }

        public int3 Up()
        {
            return new int3(x, y + 1, z);
        }

        public int3 Down()
        {
            return new int3(x, y - 1, z);
        }

        public static byte3 zero
        {
            get
            { 
                return new byte3(0, 0, 0);
            }
        }

        public static int3 up
        {
            get
            { 
                return new int3(0, 1, 0);
            }
        }

        public static int3 down
        {
            get
            { 
                return new int3(0, -1, 0);
            }
        }

        public static int3 back
        {
            get
            { 
                return new int3(0, 0, -1);
            }
        }

        public static int3 forward
        {
            get
            { 
                return new int3(0, 0, 1);
            }
        }

        public static int3 left
        {
            get
            { 
                return new int3(-1, 0, 0);
            }
        }

        public static int3 right
        {
            get
            { 
                return new int3(1, 0, 0);
            }
        }

        /*public byte min
        {
            get
            { 
                return math.min(math.min(x, z), y);
            }
        }*/
    }
}
                /*hash = (hash * 16777619) ^ x.GetHashCode();
                hash = (hash * 16777619) ^ y.GetHashCode();
                hash = (hash * 16777619) ^ z.GetHashCode();*/
            /*byte result = (byte) (x ^ (x >>> 32));
            result = 31 * result + (byte) (y ^ (y >>> 32));
            result = 31 * result + (byte) (z ^ (z >>> 32));
            return result;*/
            //return 256 * x + 128 * y + 64 * z; // Or something like that