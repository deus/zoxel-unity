﻿using System;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Zoxel
{
	//! A memory container (void*) that stores a array of generic data.
	public unsafe struct BlitableArray<T> : IDisposable where T : struct
	{
		//! This attribute (NativeDisableUnsafePtrRestriction) was added so I can push Text into systems.
        [Unity.Collections.LowLevel.Unsafe.NativeDisableUnsafePtrRestriction]
		private void* pointer;
		private int length;
		private Allocator allocator;
/*#if ENABLE_UNITY_COLLECTIONS_CHECKS
        internal int                      m_MinIndex;
        internal int                      m_MaxIndex;
        internal AtomicSafetyHandle       m_Safety;
        internal DisposeSentinel          m_DisposeSentinel;
#endif*/

		public BlitableArray(int length, Allocator allocator)
		{
			this.allocator = allocator;
			this.length = length;
			this.pointer = UnsafeUtility.Malloc(length * UnsafeUtility.SizeOf<T>(), UnsafeUtility.AlignOf<T>(), allocator);
/*#if ENABLE_UNITY_COLLECTIONS_CHECKS
            m_MinIndex = 0;
            m_MaxIndex = length - 1;
            m_Safety = AtomicSafetyHandle.Create();
            m_DisposeSentinel = null;
#endif*/
		}

		public BlitableArray(int length, Allocator allocator, int dataSize)
		{
			this.allocator = allocator;
			this.length = length;
			this.pointer = UnsafeUtility.Malloc(length * dataSize, UnsafeUtility.AlignOf<T>(), allocator);
		}

		public BlitableArray(int length, Allocator allocator, int dataSize, int alignment)
		{
			this.allocator = allocator;
			this.length = length;
			this.pointer = UnsafeUtility.Malloc(length * dataSize, alignment, allocator);
		}

		//! Turns a NativeArray into a BlitableArray.
		public BlitableArray(ref NativeArray<T> oldNativeArray, Allocator allocator)
		{
			this.allocator = allocator; // oldNativeArray.m_AllocatorLabel;
			this.length = oldNativeArray.Length;
			pointer = oldNativeArray.GetUnsafePtr();
		}

		//! Clones from old array, with the same length.
		public BlitableArray(NativeArray<T> oldNativeArray, Allocator allocator)
		{
			this.allocator = allocator;
			this.length = oldNativeArray.Length;
			this.pointer = UnsafeUtility.Malloc(length * UnsafeUtility.SizeOf<T>(), UnsafeUtility.AlignOf<T>(), allocator);
			var oldPointer = oldNativeArray.GetUnsafePtr();
			var stride = UnsafeUtility.SizeOf<T>();
			UnsafeUtility.MemCpy(this.pointer, oldPointer, stride * length);
		}

		//! Clones from old array, with a new length.
		public BlitableArray(int length, Allocator allocator, BlitableArray<T> oldArray)
		{
			this.allocator = allocator;
			this.length = length;
			this.pointer = UnsafeUtility.Malloc(length * UnsafeUtility.SizeOf<T>(), UnsafeUtility.AlignOf<T>(), allocator);
			var oldPointer = oldArray.GetUnsafePtr();
			var stride = UnsafeUtility.SizeOf<T>();
			if (length < oldArray.Length)
			{
				UnsafeUtility.MemCpy(pointer, oldPointer, stride * length);
			}
			else
			{
				UnsafeUtility.MemCpy(pointer, oldPointer, stride * oldArray.Length);
			}
		}

		//! Clones from old array, with a new length, with a startIndex
		public BlitableArray(int length, Allocator allocator, BlitableArray<T> oldArray, byte startIndex)
		{
			this.allocator = allocator;
			this.length = length;
			var stride = UnsafeUtility.SizeOf<T>();
			this.pointer = UnsafeUtility.Malloc(length * UnsafeUtility.SizeOf<T>(), UnsafeUtility.AlignOf<T>(), allocator);
			var oldPointer = (byte*) oldArray.GetUnsafePtr() + stride * startIndex;
			if (length < oldArray.Length)
			{
				UnsafeUtility.MemCpy(pointer, oldPointer, stride * length);
			}
			else
			{
				UnsafeUtility.MemCpy(pointer, oldPointer, stride * oldArray.Length);
			}
		}

		public BlitableArray(in T[] oldArray, Allocator allocator)
		{
			this.allocator = allocator;
			this.length = oldArray.Length;
			var stride = UnsafeUtility.SizeOf<T>();
			pointer = UnsafeUtility.Malloc(length * stride, UnsafeUtility.AlignOf<T>(), allocator);
			for (var i = 0; i < length; i++)
			{
				this[i] = oldArray[i];
			}
		}

		// unsafe 
		public void Dispose()
		{
/*#if ENABLE_UNITY_COLLECTIONS_CHECKS
            DisposeSentinel.Dispose(ref m_Safety, ref m_DisposeSentinel);
#endif*/
            if (pointer != (void*)0)
            {
                UnsafeUtility.Free((void*) pointer, allocator);
                pointer = (void*) 0;
                length = 0;
            }
			/*if (this.length != 0)
			{
				UnsafeUtility.Free(pointer, allocator);
			}
			this.length = 0;*/
		}

		public void DisposeFinal()
		{
/*#if ENABLE_UNITY_COLLECTIONS_CHECKS
            DisposeSentinel.Dispose(ref m_Safety, ref m_DisposeSentinel);
#endif*/
            if (pointer != (void*)0)
            {
                UnsafeUtility.Free((void*) pointer, allocator);
			}
			/*if (this.length != 0)
			{
				UnsafeUtility.Free(pointer, allocator);
			}*/
		}

		public BlitableArray<T> Clone()
		{
			if (allocator != Allocator.Persistent)
			{
				return new BlitableArray<T>();
			}
			var clone = new BlitableArray<T>(length, allocator);
			var stride = UnsafeUtility.SizeOf<T>();
			UnsafeUtility.MemCpy(clone.pointer, pointer, stride * length);
			return clone;
		}

		public BlitableArray<T> Clone(Allocator newAllocator)
		{
			var clone = new BlitableArray<T>(length, newAllocator);
			var stride = UnsafeUtility.SizeOf<T>();
			UnsafeUtility.MemCpy(clone.pointer, pointer, stride * length);
			return clone;
		}

		public int Length
		{
			get
			{
				return length;
			}
		}

		public T this[int index]
		{
			get
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				if (allocator == Allocator.Invalid)
					throw new ArgumentException("AutoGrowArray was not initialized.");

				if (index >= Length)
					throw new IndexOutOfRangeException();
#endif
				return UnsafeUtility.ReadArrayElement<T>(pointer, index);
			}
			set
			{
#if ENABLE_UNITY_COLLECTIONS_CHECKS
				if (allocator == Allocator.Invalid)
					throw new ArgumentException("AutoGrowArray was not initialized.");

				if (index >= Length)
					throw new IndexOutOfRangeException();
#endif
				UnsafeUtility.WriteArrayElement(pointer, index, value);
			}
		}

		public void* GetUnsafePtr()
		{
			return pointer;
		}

		public void Set(void* pointer, int length, Allocator allocator)
		{
			this.allocator = allocator;
			this.length = length;
			this.pointer = pointer; // UnsafeUtility.Malloc(length * UnsafeUtility.SizeOf<T>(), UnsafeUtility.AlignOf<T>(), allocator);
		}

		public NativeArray<T> ToNativeArray()
		{
			var nArray = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<T>(pointer, length * UnsafeUtility.SizeOf<T>(), allocator);
#if ENABLE_UNITY_COLLECTIONS_CHECKS
    		NativeArrayUnsafeUtility.SetAtomicSafetyHandle(ref nArray, AtomicSafetyHandle.Create());
#endif
			return nArray;
		}

		public NativeArray<T> ToNativeArray(Allocator allocator)
		{
			var nArray = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<T>(pointer, length * UnsafeUtility.SizeOf<T>(), allocator);
#if ENABLE_UNITY_COLLECTIONS_CHECKS
    		NativeArrayUnsafeUtility.SetAtomicSafetyHandle(ref nArray, AtomicSafetyHandle.Create());
#endif
			return nArray;
		}

		public NativeArray<T> ToNativeArray(int length, Allocator allocator)
		{
			var nArray = NativeArrayUnsafeUtility.ConvertExistingDataToNativeArray<T>(pointer, length * UnsafeUtility.SizeOf<T>(), allocator);
#if ENABLE_UNITY_COLLECTIONS_CHECKS
    		NativeArrayUnsafeUtility.SetAtomicSafetyHandle(ref nArray, AtomicSafetyHandle.Create());
#endif
			return nArray;
		}

		public NativeArray<T> CloneTemp()
		{
			var nativeArray = new NativeArray<T>(length, Allocator.Temp);
			for (int i = 0; i < nativeArray.Length; i++)
			{
				nativeArray[i] = this[i];
			}
			return nativeArray;
		}

		public T[] ToArray()
		{
			var array = new T[Length];
			for (var i = 0; i < Length; i++)
			{
				array[i] = this[i];
			}
			return array;
		}
	}
}

		/*public T[] ReadIntoArray(ref T[] array)
		{
			if (array.Length != Length)
			{
				array = new T[Length];
			}
			for (var i = 0; i < Length; i++)
			{
				array[i] = this[i];
			}
			return array;
		}*/

// UnsafeUtility.MemCpy(pointer, clone.pointer, length);
// var stride = UnsafeUtility.SizeOf<T>();
// UnsafeUtility.MemCpyStride(pointer, stride, clone.pointer, stride, stride, length);
// UnsafeUtility.MemCpyReplicate(pointer, clone.pointer, stride, length);

// Fuck this is horrible - make sure to get array every time unless used once... still bad! pass it through!
/*public static implicit operator T[] (BlitableArray<T> array)
{
	return array.AsArray().ToArray();
}

public T[] ToArray(int length)
{
	var res = new T[length];
	for (var i = 0; i < length; i++)
		res[i] = this[i];
	return res;
}*/

/*public NativeArray<T> ToCloneNativeArray(Allocator allocator)
{
	var nativeArray = new NativeArray<T>(length, Allocator.Temp);
	for (int i = 0; i < nativeArray.Length; i++)
	{
		nativeArray[i] = this[i];
	}
	return nativeArray;
}*/

/*public BlitableArray(T[] array, Allocator allocator)
{
	allocator = allocator;
	length = array.Length;
	var elementSize = UnsafeUtility.SizeOf<T>();
	pointer = UnsafeUtility.Malloc(length * elementSize, UnsafeUtility.AlignOf<T>(), allocator);
	for (var i = 0; i < Length; i++)
		this[i] = array[i];
}*/

/*unsafe public void Allocate(T[] array, Allocator allocator)
{
	Allocate(array.Length, allocator);
	for (var i = 0; i < Length; i++)
		this[i] = array[i];
}*/

/*unsafe public void Allocate(int size, Allocator allocator)
{
	var elementSize = UnsafeUtility.SizeOf<T>();
	this.allocator = allocator;
	this.length = size;
	this.pointer = UnsafeUtility.Malloc(size * elementSize, UnsafeUtility.AlignOf<T>(), allocator);
}*/

		/*public BlitableArray(in NativeList<T> oldNativeList, Allocator allocator)
		{
			this.allocator = allocator;
			this.length = oldNativeList.Length;
			pointer = UnsafeUtility.Malloc(length * UnsafeUtility.SizeOf<T>(), UnsafeUtility.AlignOf<T>(), allocator);
			for (var i = 0; i < Length; i++)
			{
				this[i] = oldNativeList[i];
			}
		}*/
/*#if ENABLE_UNITY_COLLECTIONS_CHECKS
			if (length < 0 || length >= int.MaxValue)
				throw new Exception();
#endif*/

		/*public BlitableArray(in NativeArray<T> nativeArray, Allocator allocator, byte nothing)
		{
			this.allocator = allocator;
			this.length = nativeArray.Length;
			pointer = UnsafeUtility.Malloc(length * UnsafeUtility.SizeOf<T>(), UnsafeUtility.AlignOf<T>(), allocator);
			var oldPointer = nativeArray.GetUnsafePtr();
			var stride = UnsafeUtility.SizeOf<T>();
			UnsafeUtility.MemCpy(pointer, oldPointer, stride * length);
		}*/
		/*for (var i = 0; i < Length; i++)
		{
			this[i] = oldArray[i];
		}*/
			// this.allocator = allocator;
			// pointer = UnsafeUtility.Malloc(length * UnsafeUtility.SizeOf<T>(), UnsafeUtility.AlignOf<T>(), allocator);
			//  NativeArrayUnsafeUtility.GetUnsafePtr<T>(pointer, this.length * UnsafeUtility.SizeOf<T>(), allocator);
			//this.allocator = allocator;
			//this.length = oldNativeArray.Length;
			// pointer = UnsafeUtility.Malloc(length * UnsafeUtility.SizeOf<T>(), UnsafeUtility.AlignOf<T>(), allocator);
			// this.pointer = oldNativeArray.GetUnsafePtr();
			//  NativeArrayUnsafeUtility.GetUnsafePtr<T>(pointer, this.length * UnsafeUtility.SizeOf<T>(), allocator);