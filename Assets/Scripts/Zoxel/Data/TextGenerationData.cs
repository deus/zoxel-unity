using System;
using Unity.Mathematics;

namespace Zoxel
{
    //! Data to generate font textures.
    [Serializable]
    public struct TextGenerationData
    {
        //! Default 32 bit.
        public int resolution;
        //! Default 1 pixel size.
        public float outlineThickness;
        //! A font zigel is made up of lines.
        public float2 pointSize;
        //! Each Font point can be displaced by a variance value.
        public float2 pointNoise;
        //! Each Font point can vary in size.
        public float pointVariation;
    }
}