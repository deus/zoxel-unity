using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using System;
using System.Runtime.InteropServices;

namespace Zoxel
{
    //! RGBA colors for our game. Stored in bytes.
    [Serializable, StructLayout(LayoutKind.Sequential)]
    public struct Color
    {
        public byte red;
        public byte green;
        public byte blue;
        public byte alpha;

        public Color(byte red, byte green, byte blue)
        {
            this.red = red;
            this.green = green;
            this.blue = blue;
            this.alpha = 255;
        }
        
        public Color(byte red, byte green, byte blue, byte alpha)
        {
            this.red = red;
            this.green = green;
            this.blue = blue;
            this.alpha = alpha;
        }

        public Color(float red, float green, float blue)
        {
            this.red = (byte) (red * 255);
            this.green = (byte) (green * 255);
            this.blue = (byte) (blue * 255);
            this.alpha = 255;
        }
        
        public Color(float3 color)
        {
            this.red = (byte) (color.x * 255);
            this.green = (byte) (color.y * 255);
            this.blue = (byte) (color.z * 255);
            alpha = 255;
        }
        
        public Color(UnityEngine.Color color)
        {
            red = ((byte)(255 * color.r));
            green = ((byte)(255 * color.g));
            blue = ((byte)(255 * color.b));
            alpha = ((byte)(255 * color.a));
        }
        
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = red.GetHashCode();
                hash ^= green.GetHashCode();
                hash ^= blue.GetHashCode();
                hash ^= alpha.GetHashCode();
                return hash;
            }
        }

        public bool Equals(Color p)
        {
            return red == p.red && green == p.green && blue == p.blue && alpha == p.alpha;
        }
        public override bool Equals(object obj)
        {
            return obj is Color && Equals((Color) obj);
        }
        
        public static bool operator ==(Color a, Color b)
            => a.red == b.red && a.blue == b.blue && a.green == b.green && a.alpha == b.alpha;

        public static bool operator !=(Color a, Color b)
            => a.red != b.red || a.blue != b.blue || a.green != b.green || a.alpha != b.alpha;

        public static Color operator *(Color a, float b)
            => new Color((byte) (a.red * b), (byte) (a.green * b), (byte) (a.blue * b), (byte) (a.alpha * b));

        public static Color operator /(Color a, int b)
            => new Color((byte) (a.red / b), (byte) (a.green / b), (byte) (a.blue / b)); // , (byte) (a.alpha / b));

        public static Color operator -(Color a)
            => new Color(- a.red, - a.green, - a.blue);
        
        public static Color operator +(Color a, Color b)
            => new Color((byte) (a.red + b.red), (byte) (a.green + b.green), (byte) (a.blue + b.blue));

        public static Color operator -(Color a, Color b)
            => new Color((byte) (a.red - b.red), (byte) (a.green - b.green), (byte) (a.blue - b.blue));

        public override string ToString()
        {
            return "[" + red + ":" + green + ":" + blue + "]";
        }
        
        public void SetColor(UnityEngine.Color color)
        {
            red = (byte) (255 * color.r);
            green = (byte) (255 * color.g);
            blue = (byte) (255 * color.b);
            alpha = (byte) (255 * color.a);
        }

        public UnityEngine.Color ToUnityColor()
        {
            var red2 = ((int) red) / 255f;
            var green2 = ((int) green) / 255f;
            var blue2 = ((int) blue) / 255f;
            var alpha2 = ((int) alpha) / 255f;
            return new UnityEngine.Color(red2, green2, blue2, alpha2);
        }

        // depreciated warning?
        public UnityEngine.Color GetColor()
        {
            var red2 = ((int) red) / 255f;
            var green2 = ((int) green) / 255f;
            var blue2 = ((int) blue) / 255f;
            var alpha2 = ((int) alpha) / 255f;
            return new UnityEngine.Color(red2, green2, blue2, alpha2);
        }

        public float4 ToFloat4()
        {
            var red2 = ((int) red) / 255f;
            var green2 = ((int) green) / 255f;
            var blue2 = ((int) blue) / 255f;
            var alpha2 = ((int) alpha) / 255f;
            return new float4(red2, green2, blue2, alpha2);
        }


        public UnityEngine.Color GetColorRGB()
        {
            var red2 = ((int) red) / 255f;
            var green2 = ((int) green) / 255f;
            var blue2 = ((int) blue) / 255f;
            return new UnityEngine.Color(red2, green2, blue2, 1f);
        }

        public Color Invert()
        {
            return new Color(255 - red, 255 - green, 255 - blue);
        }

        public Color InvertGB()
        {
            return new Color(red, 255 - green, 255 - blue);
        }

        public Color MultiplyBrightness(float valueMultiplier)
        {
            var hsv = GetHSV();
            hsv.z *= valueMultiplier;
            return GetColorFromHSV(hsv);
        }

        public Color Saturate(float saturationMultiplier)
        {
            var hsv = GetHSV();
            hsv.y *= saturationMultiplier;
            return GetColorFromHSV(hsv);
        }

        public Color ShiftHue(float hueShift)
        {
            var hsv = GetHSV();
            hsv.x += hueShift;
            hsv.x %= 360;
            return GetColorFromHSV(hsv);
        }

        public float3 ToFloat3()
        {
            var red2 = ((int)red) / 255f;
            var green2 = ((int)green) / 255f;
            var blue2 = ((int)blue) / 255f;
            return new float3(red2, green2, blue2);
        }

        public void MultiplyRGB(float multiplier)
        {
            var redFloat = multiplier * ((float) ((int)red)) / 255f;
            var greenFloat = multiplier * ((float) ((int)green)) / 255f;
            var blueFloat = multiplier * ((float) ((int)blue)) / 255f;
            red = (byte) (255 * redFloat);
            green = (byte) (255 * greenFloat);
            blue = (byte) (255 * blueFloat);
        }

        public ColorRGB ToColorRGB()
        {
            return new ColorRGB(red, green, blue);
        }
        
        public float3 GetHSV()  // , out double hue, out double saturation, out double value
        {
            var saturation = GetSaturation();
            var value = GetValue();
            var hue = GetHue();
            //UnityEngine.Debug.LogError("saturation got: " + saturation + " from " + GetColor().ToString());
            return new float3(hue, saturation, value);
        }

        public int GetValue()
        {
            var redFloat = (int) red / 255f;
            var greenFloat = (int) green / 255f;
            var blueFloat = (int) blue / 255f;
            var max = math.max(redFloat, math.max(greenFloat, blueFloat));
            // var min = math.min((int)red, math.min((int)green, (int)blue));
            return (int) math.round(max * 100f);
        }

        public int GetSaturation()
        {
            var redFloat = (int) red / 255f;
            var greenFloat = (int) green / 255f;
            var blueFloat = (int) blue / 255f;
            var max = math.max(redFloat, math.max(greenFloat, blueFloat));
            var min = math.min(redFloat, math.min(greenFloat, blueFloat));
            var delta = (max - min);
            if (max == 0)
            {
                return 0;
            }
            // UnityEngine.Debug.LogError("Getting Saturation: " + delta + " :: " + (delta / max));
            return (int) math.round(100f * (delta / max));
        }

        public int GetHue()
        {
            var redFloat = (int) red / 255f;
            var greenFloat = (int) green / 255f;
            var blueFloat = (int) blue / 255f;
            var max = math.max(redFloat, math.max(greenFloat, blueFloat));
            var min = math.min(redFloat, math.min(greenFloat, blueFloat));
            var delta = (max - min);
            int multi = 60; // 42; // 60
            int hue = 0;
            if (redFloat == max)
            {   
                hue = (int) math.ceil(multi * ((greenFloat - blueFloat) / delta)); //  % 6;
            }
            else if (greenFloat == max)
            {   
                hue = (int) math.ceil(multi * ((int) 2 + ((blueFloat - redFloat) / delta)));
                //return 120 + 120 * ((blueFloat - redFloat) / delta);
            }
            else    // blue is max
            {   
                hue = (int) math.ceil(multi * ((int) 4 + ((redFloat - greenFloat) / delta)));
                //return 240 + 120 * ((redFloat - greenFloat) / delta);
            }
            if (hue < 0)
            {
                hue = 360 + hue;
            }
            return hue;
            // Depending on what RGB color channel is the max value. The three different formulas are:
            // If Red is max, then Hue = (green-blue)/(max-min)
            // If Green is max, then Hue = 2.0 + (blue-red)/(max-min)
            // If Blue is max, then Hue = 4.0 + (red-green)/(max-min)
        }

        public static Color GetColorFromHSV(float3 HSV)
        {
            return FromHSV(HSV);
        }

        public static Color FromHSV(float3 HSV)
        {
            return new Color(UnityEngine.Color.HSVToRGB(HSV.x / 360f, HSV.y / 100f, HSV.z / 100f, true));
        }

        public static bool TestHSVConversion()
        {
            UnityEngine.Debug.Log("Tessting HSV Conversion.");
            // fire
            var colorA = new Color(181, 137, 152);
            if (!colorA.TestColorHSVConversion())
            {
                return false;
            }
            // blue lime
            var colorC = new Color(77, 45, 167);
            if (!colorC.TestColorHSVConversion())
            {
                return false;
            }
            // lime
            var colorB = new Color(77, 135, 69);
            if (!colorB.TestColorHSVConversion())
            {
                return false;
            }
            UnityEngine.Debug.Log("All Tests Success");
            return true;
        }

        public bool TestColorHSVConversion()
        {
            var HSV = this.GetHSV();
            //var rgb = HUEtoRGB(HSV.x);
            //UnityEngine.Debug.LogError("Float rbg: " + rgb);
            var colorB = Color.GetColorFromHSV(HSV);
            var closeEnough = 2;
            var isCloseEnough = this.red >= colorB.red - closeEnough && this.red <= colorB.red + closeEnough
                && this.green >= colorB.green - closeEnough && this.green <= colorB.green + closeEnough
                && this.blue >= colorB.blue - closeEnough && this.blue <= colorB.blue + closeEnough;
            if (!isCloseEnough)
            {
                UnityEngine.Debug.LogError("Failed HSV Conversion. From [" + this + "  to  " + colorB + "] With HSV: " + HSV);
            }
            return isCloseEnough;
            // todo: Generate Colours of a chunkBiome here
            //          Assign the colours to soil, grass, wood, leaf, brick, tiles, road voxels
        }
    }
}

// const int maxColorChecksCount = 16 * 1024;
// test HSV


        /*private static float min3(float a, float b, float c)
        {
            return math.min(math.min(a, b), c);
        }

        private static float3 HUEtoRGB(float h)
        {
            var kr = (5 + h * 6) % 6;
            var kg = (3 + h * 6) % 6;
            var kb = (1 + h * 6) % 6;
            var r = 1 - math.max(min3(kr, 4 - kr, 1), 0);
            var g = 1 - math.max(min3(kg, 4 - kg, 1), 0);
            var b = 1 - math.max(min3(kb, 4 - kb, 1), 0);
            
            return new float3(r, g, b);
        }*/
            /*float r = math.abs(h * 6 - 3) - 1;
            float g = 2 - math.abs(h * 6 - 2);
            float b = 2 - math.abs(h * 6 - 4);
            return new float3(math.clamp(r, 0, 1), math.clamp(g, 0, 1), math.clamp(b, 0, 1));*/
        /*var rgb = HUEtoRGB(HSV.x);
        var vc = ((rgb - new float3(1, 1, 1)) * HSV.y + new float3(1, 1, 1)) * HSV.z;
        return new Color(vc.x, vc.y, vc.z);*/

        //public static Color FromHSV(float3 HSV)
        //{
            //return new Color(UnityEngine.Color.HSVToRGB(HSV.x, HSV.y, HSV.z));
            //var hue = HSV.x; // red;
            /*var hue = (int) HSV.x; // 360 * (((float)red) / 255f);
            var saturation = HSV.y / 100f; // ((float)green) / 255f;
            var value = HSV.z / 100f; //  ((float)blue) / 255f; // blue;
            var c = saturation * value;
            var x = c * (1 - ((hue / 60) % 2 - 1));
            var m = value - c;
            double red = 0;
            double green = 0;
            double blue = 0;
            if (value <= 0)
            {
                red = green = blue = 0;
            }
            else if (saturation <= 0)
            {
                red = green = blue = value;
            }
            else
            {
                double hf = hue / 60.0;
                int i = (int)Math.Floor(hf);
                double f = hf - i;
                double pv = value * (1 - saturation);
                double qv = value * (1 - saturation * f);
                double tv = value * (1 - saturation * (1 - f));
                switch (i)
                {
                    // Red is the dominant color
                    case 0:
                        red = value;
                        green = tv;
                        blue = pv;
                        break;

                    // Green is the dominant color
                    case 1:
                        red = qv;
                        green = value;
                        blue = pv;
                        break;
                    case 2:
                        red = pv;
                        green = value;
                        blue = tv;
                        break;
                    // Blue is the dominant color
                    case 3:
                        red = pv;
                        green = qv;
                        blue = value;
                        break;
                    case 4:
                        red = tv;
                        green = pv;
                        blue = value;
                        break;
                    // Red is the dominant color
                    case 5:
                        red = value;
                        green = pv;
                        blue = qv;
                        break;
                    // Just in case we overshoot on our math by a little
                    case 6:
                        red = value;
                        green = tv;
                        blue = pv;
                        break;
                    case -1:
                        red = value;
                        green = pv;
                        blue = qv;
                        break;
                }
            }
            return new Color(
                (int)((red) * 255),
                (int)((green) * 255),
                (int)((blue) * 255));*/
        //}