using System;

namespace Zoxel
{
    //! Data for a black and white frame UI texture.
    [Serializable]
    public struct FrameGenerationData
    {
        //! Default 128 bits.
        public int resolution;  
        //! Default 6.       
        public int thickness;
        //! The color of the frame.
        public Color color;

        public FrameGenerationData SetColor(UnityEngine.Color color)
        {
            var frameGenerationData = new FrameGenerationData();
            frameGenerationData.resolution = resolution;
            frameGenerationData.thickness = thickness;
            frameGenerationData.color = new Color(color);
            return frameGenerationData;
        }
    }
}