using Unity.Mathematics;

namespace Zoxel
{
	//! A mirror of the unity engine rect.
    public struct Rect
    {
        public float2 position;
        public float2 size;

        public Rect(float2 position, float2 size)
        {
            this.position = position;
            this.size = size;
        }

        public Rect(float x, float y, float x2, float y2)
        {
            this.position = new float2(x, y);
            this.size = new float2(x2, y2);
        }

        public UnityEngine.Rect GetRect()
        {
            return new UnityEngine.Rect(position.x, position.y, size.x, size.y);
        }
    }
}