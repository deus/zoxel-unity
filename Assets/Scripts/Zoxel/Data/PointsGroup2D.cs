using Unity.Entities;

namespace Zoxel // .Textures
{
    //! Holds a series of 2D points.
    public struct PointsGroup2D // : IComponentData
    {
        public BlitableArray<int2> points;

        public void Dispose()
        {
            if (points.Length > 0)
            {
                points.Dispose();
            }
        }
    }
}