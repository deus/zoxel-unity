using Unity.Entities;

namespace Zoxel // .Worlds
{
    public static class GenerateChunkState
    {
        public const byte Begin = 0;
        public const byte Loading = 1;
        public const byte LinkMegaChunk = 2;
        public const byte LinkMegaChunk2D = 3;
        public const byte LinkPlanetChunk2Ds = 4;
        public const byte GenerationStart = 5;
    }
}