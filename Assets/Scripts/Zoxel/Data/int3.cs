using System;
using Unity.Mathematics;

namespace Zoxel
{
	//! An int, but 3 of them. Used for alot of voxel spatial coordinates.
    [Serializable]
    public struct int3 : IEquatable<int3>, IComparable<int3>
    {
        public int x;
        public int y;
        public int z;

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                hash = hash * 23 + x.GetHashCode();
                hash = hash * 23 + y.GetHashCode();
                hash = hash * 23 + z.GetHashCode();
                return hash;
            }
        }

        public int3(int x_, int y_, int z_)
        {
            x = x_; y = y_; z = z_;
        }

        public int3(int3 newOne)
        {
            x = newOne.x;
            y = newOne.y; 
            z = newOne.z;
        }

        public int3(float3 newOne)
        {
            x = (int) newOne.x;
            y = (int) newOne.y; 
            z = (int) newOne.z;
        }

        public byte3 ToByte3()
        {
            return new byte3((byte) x, (byte) y, (byte) z);
        }

        public int2 ToXZ()
        {
            return new int2(x, z);
        }

        public static int3 Round(float3 value)
        {
            return new int3((int) math.round(value.x), (int) math.round(value.y), (int) math.round(value.z));
        }

        public int CompareTo(int3 other)
        {
            if (other.x == x && other.y == y && other.z == z)
            {
                return 0;
            }
            if (other.x > x)
            {
                return 1;
            }
            else if (other.x < x)
            {
                return -1;
            }
            if (other.z > z)
            {
                return 1;
            }
            else if (other.z < z)
            {
                return -1;
            }
            if (other.y > y)
            {
                return 1;
            }
            else if (other.y < y)
            {
                return -1;
            }
            return 0;
        }

        public override bool Equals(object obj)
        {
            return obj is int3 && Equals((int3) obj);
        }
        
        public bool Equals(int3 p)
        {
            return x == p.x && y == p.y && z == p.z;
        }
        
        public static bool operator ==(int3 a, int3 b)
            => a.x == b.x && a.z == b.z && a.y == b.y;

        public static bool operator !=(int3 a, int3 b)
            => a.x != b.x || a.z != b.z || a.y != b.y;

        public override string ToString()
        {
            return "" + x + "," + y + "," + z;
        }

        public static int3 operator -(int3 a)
            => new int3(- a.x, - a.y, - a.z);
        
        public static int3 operator +(int3 a, int3 b)
            => new int3(a.x + b.x, a.y + b.y, a.z + b.z);
        
        public static int3 operator -(int3 a, int3 b)
            => new int3(a.x - b.x, a.y - b.y, a.z - b.z);

        public static int3 operator *(int3 a, float b)
            => new int3((int)(a.x * b), (int)(a.y * b), (int)(a.z * b));

        public static int3 operator *(int3 a, int b)
            => new int3(a.x * b, a.y * b, a.z * b);

        public static int3 operator /(int3 a, float b)
            => new int3((int)(a.x / b), (int)(a.y / b), (int)(a.z / b));
        
        public static int3 operator /(int3 a, int b)
            => new int3(a.x / b, a.y / b, a.z / b);

        public float3 ToFloat3()
        {
            return new float3(x, y, z);
        }

        public int3 Left()
        {
            return new int3(x - 1, y, z);
        }

        public int3 Right()
        {
            return new int3(x + 1, y, z);
        }

        public int3 Forward()
        {
            return new int3(x, y, z + 1);
        }

        public int3 Backward()
        {
            return new int3(x, y, z - 1);
        }

        public int3 Up()
        {
            return new int3(x, y + 1, z);
        }

        public int3 Down()
        {
            return new int3(x, y - 1, z);
        }

        public static int3 zero
        {
            get
            { 
                return new int3(0, 0, 0);
            }
        }

        public static int3 up
        {
            get
            { 
                return new int3(0, 1, 0);
            }
        }

        public static int3 down
        {
            get
            { 
                return new int3(0, -1, 0);
            }
        }

        public static int3 back
        {
            get
            { 
                return new int3(0, 0, -1);
            }
        }

        public static int3 forward
        {
            get
            { 
                return new int3(0, 0, 1);
            }
        }

        public static int3 left
        {
            get
            { 
                return new int3(-1, 0, 0);
            }
        }

        public static int3 right
        {
            get
            { 
                return new int3(1, 0, 0);
            }
        }

        public int min
        {
            get
            { 
                return math.min(math.min(x, z), y);
            }
        }
    }
}
                /*hash = (hash * 16777619) ^ x.GetHashCode();
                hash = (hash * 16777619) ^ y.GetHashCode();
                hash = (hash * 16777619) ^ z.GetHashCode();*/
            /*int result = (int) (x ^ (x >>> 32));
            result = 31 * result + (int) (y ^ (y >>> 32));
            result = 31 * result + (int) (z ^ (z >>> 32));
            return result;*/
            //return 256 * x + 128 * y + 64 * z; // Or something like that