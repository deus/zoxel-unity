using System;
using Unity.Mathematics;

namespace Zoxel
{
	//! An int, but 2 of them. Used for alot of tile spatial coordinates.
    [Serializable]
    public struct int2 : IEquatable<int2>
    {
        public int x;
        public int y;

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;
                hash = hash * 23 + x.GetHashCode();
                hash = hash * 23 + y.GetHashCode();
                return hash;
            }
        }

        public int2(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public int2(float x, float y)
        {
            this.x = (int) x;
            this.y = (int) y;
        }

        public static int2 zero
        {
            get
            { 
                return new int2(0, 0);
            }
        }

        public int2 FlipY()
        {
            return new int2(x, -y);
        }

        public override string ToString()
        {
            return "" + x + "," + y;
        }

        public override bool Equals(object obj)
        {
            return obj is int2 && Equals((int2) obj);
        }
        
        public bool Equals(int2 p)
        {
            return x == p.x && y == p.y;
        }
        
        public static bool operator ==(int2 a, int2 b)
            => a.x == b.x && a.y == b.y;

        public static bool operator !=(int2 a, int2 b)
            => a.x != b.x || a.y != b.y;
        
        public static int2 operator +(int2 a, int2 b)
            => new int2(a.x + b.x, a.y + b.y);

        public static int2 operator -(int2 a, int2 b)
            => new int2(a.x - b.x, a.y - b.y);

        public static int2 operator *(int2 a, int b)
            => new int2(a.x * b, a.y * b);

        public static int2 operator /(int2 a, int b)
            => new int2(a.x / b, a.y / b);

        public static int2 operator *(int2 a, float b)
        {
            return new int2((int) (a.x * b), (int) (a.y * (b)));
        }

        public float2 ToFloat2()
        {
            return new float2((float) x, (float) y);
        }

        public int2 MoveUp(int2 size)
        {
            var newPoint = new int2(x, y + 1);
            if (newPoint.y >= size.y)
            {
                newPoint.y = 0;
            }
            return newPoint;
        }
        public int2 MoveDown(int2 size)
        {
            var newPoint = new int2(x, y - 1);
            if (newPoint.y < 0)
            {
                newPoint.y = size.y - 1;
            }
            return newPoint;
        }

        public int2 MoveRight(int2 size)
        {
            var newPoint = new int2(x + 1, y);
            if (newPoint.x >= size.x)
            {
                newPoint.x = 0;
            }
            return newPoint;
        }
        public int2 MoveLeft(int2 size)
        {
            var newPoint = new int2(x - 1, y);
            if (newPoint.x < 0)
            {
                newPoint.x = size.x - 1;
            }
            return newPoint;
        }

        public int2 Up()
        {
            return new int2(x, y + 1);
        }
        public int2 Down()
        {
            return new int2(x, y - 1);
        }

        public int2 Right()
        {
            return new int2(x + 1, y);
        }
        
        public int2 Left()
        {
            return new int2(x - 1, y);
        }

        public int GetArrayIndex(int2 size)
        {
            return x + y * size.x;
        }
    }
}