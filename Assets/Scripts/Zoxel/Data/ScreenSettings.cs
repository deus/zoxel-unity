using System;
using UnityEngine;

namespace Zoxel
{
    [Serializable]
    public struct ScreenSettings
    {
        public byte fullScreenMode;

        public void OnStart()
        {
            fullScreenMode = (byte) PlayerPrefs.GetInt("FullScreenMode", (int) FullScreenMode.ExclusiveFullScreen);
            ScreenUtil.SetScreenMode(255, fullScreenMode);
        }
    }
}