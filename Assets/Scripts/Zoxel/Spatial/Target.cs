using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel // .AI
{
    //! Keeps data about targeting other characters
    /**
    *   TargetSystem uses it to allocate targets
    */
    public struct Target : IComponentData
    {
        public Entity target;
        public float3 targetPosition;
        public float targetDistance;
        public float targetAngle;           // between 0 and 180 (+ or -)

        public Target(Entity target)
        {
            this.target = target;
            this.targetPosition = new float3();
            this.targetDistance = 0;
            this.targetAngle = 0;
        }
    }
}