using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel // .Movement
{
    //! Lerps gravity rotation to a new rotation. Also lerps the pitch of a character.
    public struct LerpGravityQuadrant : IComponentData
    {
        public quaternion beginRotation;
        public quaternion targetRotation;
        public quaternion beginPitch;
        public quaternion targetPitch;
        public double timeStarted;

        public LerpGravityQuadrant(quaternion beginRotation, quaternion targetRotation, quaternion beginPitch, quaternion targetPitch, double timeStarted)
        {
            this.beginRotation = beginRotation;
            this.targetRotation = targetRotation;
            this.beginPitch = beginPitch;
            this.targetPitch = targetPitch;
            this.timeStarted = timeStarted;
        }
    }
}
