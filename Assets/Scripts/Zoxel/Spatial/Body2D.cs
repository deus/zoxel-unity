using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel // .Zoxel2D
{
    //! A generic body component with a float2 size.
    public struct Body2D : IComponentData
    {
        public float2 size;

        public Body2D(float2 size)
        {
            this.size = size;
        }
    }
}

/* AABB.prototype.intersects = function(other) {
return !(
this.max.X < other.min.X || 
this.max.Y < other.min.Y || 
this.min.X > other.max.X || 
this.min.Y > other.max.Y
);*/