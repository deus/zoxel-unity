﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel // .Movement
{
    //! A generic body component with a float3 size.
    public struct Body : IComponentData
    {
        public float3 size;

        public Body(float3 size)
        {
            this.size = size;
        }
    }
}