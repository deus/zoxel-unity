using Unity.Entities;
using Unity.Mathematics;
// this is kind of like planet / movement information
// So i'll put this in base Zoxel

// keep in namespace as it breaks assembly
namespace Zoxel // .Movement
{
    //! Signals a PlanetSide quadrant and a rotation (based on that side).
    public struct GravityQuadrant : IComponentData
    {
        public byte quadrant;
        public quaternion rotation;

        public GravityQuadrant(byte quadrant)
        {
            this.quadrant = quadrant;
            this.rotation = quaternion.identity;
            this.rotation = GetBaseRotation(this.quadrant); // quaternion.identity;
        }

        public void SetQuadrant(byte quadrant)
        {
            this.quadrant = quadrant;
            this.rotation = GetBaseRotation(this.quadrant);
        }


        public void AlignGravityAxis(ref float3 value, float3 alignedValue)
        {
            if (quadrant == PlanetSide.Left || quadrant == PlanetSide.Right)
            {
                value.x = alignedValue.x;
            }
            else if (quadrant == PlanetSide.Down || quadrant == PlanetSide.Up)
            {
                value.y = alignedValue.y;
            }
            else if (quadrant == PlanetSide.Backward || quadrant == PlanetSide.Forward)
            {
                value.z = alignedValue.z;
            }
        }

        public void ZeroGravityAxis(ref float3 value)
        {
            if (quadrant == PlanetSide.Left || quadrant == PlanetSide.Right)
            {
                value.x = 0;
            }
            else if (quadrant == PlanetSide.Down || quadrant == PlanetSide.Up)
            {
                value.y = 0;
            }
            else if (quadrant == PlanetSide.Backward || quadrant == PlanetSide.Forward)
            {
                value.z = 0;
            }
        }

        public quaternion GetBaseRotation(byte oldQuadrant, ref quaternion pitchRotation)
        {
            var degreesToRadians = ((math.PI * 2) / 360f);
            var forwardDirection = new float3(0, 0, 1);
            if (quadrant == PlanetSide.Up)
            {
                forwardDirection = new float3(0, 0, 1);
                if (oldQuadrant == PlanetSide.Left)
                {
                    // UnityEngine.Debug.LogError("Up from Left.");
                    pitchRotation = math.mul(quaternion.EulerXYZ(new float3(0, -90, 0) * degreesToRadians), pitchRotation);
                }
                else if (oldQuadrant == PlanetSide.Right)
                {
                    // UnityEngine.Debug.LogError("Up from Right.");
                    pitchRotation = math.mul(quaternion.EulerXYZ(new float3(0, 90, 0) * degreesToRadians), pitchRotation);
                }
                else if (oldQuadrant == PlanetSide.Backward)
                {
                    // UnityEngine.Debug.LogError("Up from Backward.");
                    pitchRotation = math.mul(quaternion.EulerXYZ(new float3(0, -180, 0) * degreesToRadians), pitchRotation);
                }
                else if (oldQuadrant == PlanetSide.Forward)
                {
                    // UnityEngine.Debug.LogError("Up from Forward.");
                }
            }
            else if (quadrant == PlanetSide.Down)
            {
                forwardDirection = new float3(0, 0, -1);
                if (oldQuadrant == PlanetSide.Left)
                {
                    // UnityEngine.Debug.LogError("Down from Left.");
                    pitchRotation = math.mul(quaternion.EulerXYZ(new float3(0, 90, 0) * degreesToRadians), pitchRotation);
                }
                else if (oldQuadrant == PlanetSide.Right)
                {
                    // UnityEngine.Debug.LogError("Down from Right.");
                    pitchRotation = math.mul(quaternion.EulerXYZ(new float3(0, -90, 0) * degreesToRadians), pitchRotation);
                }
                else if (oldQuadrant == PlanetSide.Backward)
                {
                    // UnityEngine.Debug.LogError("Down from Backward.");
                    pitchRotation = math.mul(quaternion.EulerXYZ(new float3(0, 180, 0) * degreesToRadians), pitchRotation);
                }
                else if (oldQuadrant == PlanetSide.Forward)
                {
                    // UnityEngine.Debug.LogError("Down from Forward.");
                }
            }
            else if (quadrant == PlanetSide.Left)
            {
                forwardDirection = new float3(0, -1, 0);
                if (oldQuadrant == PlanetSide.Up)
                {
                    // UnityEngine.Debug.LogError("Left from Up.");
                    pitchRotation = math.mul(quaternion.EulerXYZ(new float3(0, 90, 0) * degreesToRadians), pitchRotation);
                }
                else if (oldQuadrant == PlanetSide.Down)
                {
                    // UnityEngine.Debug.LogError("Left from Down.");
                    pitchRotation = math.mul(quaternion.EulerXYZ(new float3(0, -90, 0) * degreesToRadians), pitchRotation);
                }
                else if (oldQuadrant == PlanetSide.Backward)
                {
                    // UnityEngine.Debug.LogError("Left from Backward.");
                }
                else if (oldQuadrant == PlanetSide.Forward)
                {
                    // UnityEngine.Debug.LogError("Left from Forward.");
                }
            }
            else if (quadrant == PlanetSide.Right)
            {
                forwardDirection = new float3(0, -1, 0);
                if (oldQuadrant == PlanetSide.Up)
                {
                    // UnityEngine.Debug.LogError("Right from Up.");
                    pitchRotation = math.mul(quaternion.EulerXYZ(new float3(0, -90, 0) * degreesToRadians), pitchRotation);
                }
                else if (oldQuadrant == PlanetSide.Down)
                {
                    // UnityEngine.Debug.LogError("Right from Down.");
                    pitchRotation = math.mul(quaternion.EulerXYZ(new float3(0, 90, 0) * degreesToRadians), pitchRotation);
                }
                else if (oldQuadrant == PlanetSide.Backward)
                {
                    // UnityEngine.Debug.LogError("Right from Backward.");
                }
                else if (oldQuadrant == PlanetSide.Forward)
                {
                    // UnityEngine.Debug.LogError("Right from Forward.");
                }
            }
            else if (quadrant == PlanetSide.Backward)
            {
                forwardDirection = new float3(0, -1, 0);
                if (oldQuadrant == PlanetSide.Up)
                {
                    // UnityEngine.Debug.LogError("Backward from Up.");
                    pitchRotation = math.mul(quaternion.EulerXYZ(new float3(0, 180, 0) * degreesToRadians), pitchRotation);
                }
                else if (oldQuadrant == PlanetSide.Down)
                {
                    // UnityEngine.Debug.LogError("Backward from Down.");
                    pitchRotation = math.mul(quaternion.EulerXYZ(new float3(0, -180, 0) * degreesToRadians), pitchRotation);
                }
                else if (oldQuadrant == PlanetSide.Left)
                {
                    // UnityEngine.Debug.LogError("Backward from Left.");
                }
                else if (oldQuadrant == PlanetSide.Right)
                {
                    // UnityEngine.Debug.LogError("Backward from Right.");
                }
            }
            else if (quadrant == PlanetSide.Forward)
            {
                forwardDirection = new float3(0, -1, 0);
                if (oldQuadrant == PlanetSide.Up)
                {
                    //UnityEngine.Debug.LogError("Forward from Up.");
                }
                else if (oldQuadrant == PlanetSide.Down)
                {
                    // UnityEngine.Debug.LogError("Forward from Down.");
                }
                else if (oldQuadrant == PlanetSide.Left)
                {
                    // UnityEngine.Debug.LogError("Forward from Left.");
                }
                else if (oldQuadrant == PlanetSide.Right)
                {
                    // UnityEngine.Debug.LogError("Forward from Right.");
                }
            }
            return quaternion.LookRotation(forwardDirection, GetUpDirection());
        }

        public quaternion GetBaseRotation(byte oldQuadrant)
        {
            var forwardDirection = new float3(0, 0, 1);
            if (quadrant == PlanetSide.Up)
            {
                forwardDirection = new float3(0, 0, 1);
            }
            else if (quadrant == PlanetSide.Down)
            {
                forwardDirection = new float3(0, 0, -1);
            }
            else if (quadrant == PlanetSide.Left)
            {
                forwardDirection = new float3(0, -1, 0);
            }
            else if (quadrant == PlanetSide.Right)
            {
                forwardDirection = new float3(0, -1, 0);
            }
            else if (quadrant == PlanetSide.Backward)
            {
                forwardDirection = new float3(0, -1, 0);
            }
            else if (quadrant == PlanetSide.Forward)
            {
                forwardDirection = new float3(0, -1, 0);
            }
            return quaternion.LookRotation(forwardDirection, GetUpDirection());
        }

        public float3 GetUpDirection()
        {
            var direction = float3.zero;
            if (quadrant == PlanetSide.Down)
            {
                direction = math.down();
            }
            else if (quadrant == PlanetSide.Up)
            {
                direction = math.up();
            }
            else if (quadrant == PlanetSide.Left)
            {
                direction = math.left();
            }
            else if (quadrant == PlanetSide.Right)
            {
                direction = math.right();
            }
            else if (quadrant == PlanetSide.Backward)
            {
                direction = math.back();
            }
            else if (quadrant == PlanetSide.Forward)
            {
                direction = math.forward();
            }
            return direction;
        }
    }
}