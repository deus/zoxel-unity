using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel
{
    //! A component for size in the 2d plane.
    public struct Size2D : IComponentData
    {
        public float2 size;

        public Size2D(float2 size)
        {
            this.size = size;
        }
    }
}