namespace Zoxel
{
	//! A side of a planet or cube, contains None as 0.
	public static class PlanetSide
	{
		public const byte None = 0;
		public const byte Left = 1;
		public const byte Right = 2;
		public const byte Down = 3;
		public const byte Up = 4;
		public const byte Backward = 5;
		public const byte Forward = 6;

		public static int3 GetOffset(byte direction)
		{
			if (direction == PlanetSide.Left)
			{
				return new int3(-1, 0, 0);
			}
			else if (direction == PlanetSide.Right)
			{
				return new int3(1, 0, 0);
			}
			else if (direction == PlanetSide.Down)
			{
				return new int3(0, -1, 0);
			}
			else if (direction == PlanetSide.Up)
			{
				return new int3(0, 1, 0);
			}
			else if (direction == PlanetSide.Backward)
			{
				return new int3(0, 0, -1);
			}
			else if (direction == PlanetSide.Forward)
			{
				return new int3(0, 0, 1);
			}
			else
			{
				return new int3();
			}
		}
	}
}