namespace Zoxel // .Input
{
    //! A type of button used on UI Interaction.
    public static class ButtonActionType
    {
        public const byte None = 0;
        public const byte Confirm = 1;
        public const byte Cancel = 2;
        public const byte Action1 = 3;
        public const byte Action2 = 4;
        public const byte Action3 = 5;
        public const byte Action4 = 6;
        public const byte Left1 = 7;
        public const byte Right1 = 8;
    }
}