namespace Zoxel
{
    public static class DeviceType
    {
		public const byte None = 0;
		public const byte Keyboard = 1;
		public const byte Mouse = 2;
		public const byte Gamepad = 3;
		public const byte Touch = 4;
    }
}