namespace Zoxel // .Characters
{
    //! The type of npc to generate. Used for now until a better alternative forms.
    public static class NPCType
    {
        public const byte slem = 0; // little slem
        public const byte glem = 1; // medium slem
        public const byte chen = 2; // the big guy
        public const byte dave = 3; // a humanoid
        public const byte minty = 4; // salesman
        public const byte jona = 6; // questgiver
        public const byte player = 5;
    }
}