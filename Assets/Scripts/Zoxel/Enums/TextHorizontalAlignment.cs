namespace Zoxel
{
    //! Alignment of text horizontally.
    public static class TextHorizontalAlignment
    {
        public const byte Middle = 0;
        public const byte Left = 1;
        public const byte Right = 2;
    }
}