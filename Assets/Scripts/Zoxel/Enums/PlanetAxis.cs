namespace Zoxel
{
	//! Planet Axis Type
	public static class PlanetAxis
	{
		public const byte None = 0;
		public const byte Vertical = 1;
		public const byte Horizontal = 2;
		public const byte Depth = 3;
	}
}