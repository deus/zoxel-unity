using Unity.Entities;

namespace Zoxel
{
    //! Each state generates different data.
    public enum GenerateRealmState : byte
    {
        GenerateMaterials,
        GenerateStats,  // do this first
        GenerateStatsLinking,  // do this first
        GenerateSkills,
        GenerateSkillTree,
        GenerateClasses,
        GenerateVoxels,
        GenerateVoxelsLinking,
        GenerateItems,
        GenerateRaces,
        GenerateRaces2,
        GenerateQuests,
        GenerateQuests2,
        GenerateColors,
        GenerateClans,
        GenerateClans2,
        RefreshWorlds
    }
}