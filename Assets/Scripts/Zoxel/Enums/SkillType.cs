namespace Zoxel // .Skillz
{
    public static class SkillType
    {
        public const byte None = 0;
        public const byte PhysicalAttack = 1;
        public const byte Projectile = 2;
        public const byte SpawnVoxel = 3;
        public const byte SummonMinion = 4;
        public const byte SummonTurret = 5;
        public const byte Augmentation = 6;
        public const byte Shield = 7;
    }
}