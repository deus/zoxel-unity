namespace Zoxel
{
    //! Alignment of text vertically.
    public static class TextVerticalAlignment
    {
        public const byte Middle = 0;
        public const byte Bottom = 1;
        public const byte Top = 2;
    }
}