namespace Zoxel // .Voxels
{
	public enum VoxelTextureType : byte
	{
		Soil,
		SoilGrass,
		Grass,
		Sand,		// bottom of sea
		Stone,		// below soil
		Bricks,		// harder to destroy
		Tiles,		// bottom of town floors
		Wood,		// core of trees
		WoodTop,		// core of trees
		Leaf,		// top of trees
		Bedrock,	// bottom of earth
		MossStone,	// mosstone for ruins left behind
		SandStone,
		CobbleStone,
		Portal,		// todo: fix/finish portals
		Water,	// bottom of earth
		Ore		// todo: Generate ore texture todo: ore to drop special ore items
	}
}