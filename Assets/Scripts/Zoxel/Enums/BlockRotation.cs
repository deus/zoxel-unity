using Unity.Mathematics;

namespace Zoxel
{
	//! The direction the player is facing on a surface of planet
	public static class BlockRotationRegion
	{
		public const byte Forward = 0;
		public const byte Right = 1;
		public const byte Backward = 2;
		public const byte Left = 3;
	}
	//! Voxel Side Type
	public static class BlockRotation
	{
		// Right to Forward
		// public const byte None = 0;	// remove this when can be bothered..
		public const byte Left = 0;
		public const byte Right = 1;
		public const byte Down = 2;
		public const byte Up = 3;
		public const byte Backward = 4;
		public const byte Forward = 5;
		// Rotations for all voxels
		// Left
		public const byte Left90 = 6;
		public const byte Left180 = 7;
		public const byte Left270 = 8;
		// Right
		public const byte Right90 = 9;
		public const byte Right180 = 10;
		public const byte Right270 = 11;
		// Down
		public const byte Down90 = 12;
		public const byte Down180 = 13;
		public const byte Down270 = 14;
		// Up
		public const byte Up90 = 15;
		public const byte Up180 = 16;
		public const byte Up270 = 17;
		// Backward
		public const byte Backward90 = 18;
		public const byte Backward180 = 19;
		public const byte Backward270 = 20;
		// Forward
		public const byte Forward90 = 21;
		public const byte Forward180 = 22;
		public const byte Forward270 = 23;

		public static byte GetBlockRotation(byte planetSide, byte blockRotationRegion)
		{
			if (planetSide == PlanetSide.Left)
			{
				if (blockRotationRegion == BlockRotationRegion.Backward)
				{
					return BlockRotation.Left;
				}
				else if (blockRotationRegion == BlockRotationRegion.Right)
				{
					return BlockRotation.Left90;
				}
				else if (blockRotationRegion == BlockRotationRegion.Forward)
				{
					return BlockRotation.Left180;
				}
				else if (blockRotationRegion == BlockRotationRegion.Left)
				{
					return BlockRotation.Left270;
				}
				return BlockRotation.Left;
			}
			else if (planetSide == PlanetSide.Right)
			{
				if (blockRotationRegion == BlockRotationRegion.Backward)
				{
					return BlockRotation.Right;
				}
				else if (blockRotationRegion == BlockRotationRegion.Right)
				{
					return BlockRotation.Right90;
				}
				else if (blockRotationRegion == BlockRotationRegion.Forward)
				{
					return BlockRotation.Right180;
				}
				else if (blockRotationRegion == BlockRotationRegion.Left)
				{
					return BlockRotation.Right270;
				}
				return BlockRotation.Right;
			}
			else if (planetSide == PlanetSide.Down)
			{
				if (blockRotationRegion == BlockRotationRegion.Backward)
				{
					return BlockRotation.Down;
				}
				else if (blockRotationRegion == BlockRotationRegion.Right)
				{
					return BlockRotation.Down90;
				}
				else if (blockRotationRegion == BlockRotationRegion.Forward)
				{
					return BlockRotation.Down180;
				}
				else if (blockRotationRegion == BlockRotationRegion.Left)
				{
					return BlockRotation.Down270;
				}
				return BlockRotation.Down;
			}
			else if (planetSide == PlanetSide.Up)
			{
				if (blockRotationRegion == BlockRotationRegion.Backward)
				{
					return BlockRotation.Up;
				}
				else if (blockRotationRegion == BlockRotationRegion.Right)
				{
					return BlockRotation.Up90;
				}
				else if (blockRotationRegion == BlockRotationRegion.Forward)
				{
					return BlockRotation.Up180;
				}
				else if (blockRotationRegion == BlockRotationRegion.Left)
				{
					return BlockRotation.Up270;
				}
				return BlockRotation.Up;
			}
			else if (planetSide == PlanetSide.Backward)
			{
				if (blockRotationRegion == BlockRotationRegion.Backward)
				{
					return BlockRotation.Backward;
				}
				else if (blockRotationRegion == BlockRotationRegion.Right)
				{
					return BlockRotation.Backward90;
				}
				else if (blockRotationRegion == BlockRotationRegion.Forward)
				{
					return BlockRotation.Backward180;
				}
				else if (blockRotationRegion == BlockRotationRegion.Left)
				{
					return BlockRotation.Backward270;
				}
				return BlockRotation.Backward;
			}
			else if (planetSide == PlanetSide.Forward)
			{
				if (blockRotationRegion == BlockRotationRegion.Backward)
				{
					return BlockRotation.Forward;
				}
				else if (blockRotationRegion == BlockRotationRegion.Right)
				{
					return BlockRotation.Forward90;
				}
				else if (blockRotationRegion == BlockRotationRegion.Forward)
				{
					return BlockRotation.Forward180;
				}
				else if (blockRotationRegion == BlockRotationRegion.Left)
				{
					return BlockRotation.Forward270;
				}
				return BlockRotation.Forward;
			}
			return 0;
		}

		public static quaternion GetRotation(byte blockRotation)
		{
			var degreesToRadians = ((math.PI * 2) / 360f);
			// Left #
			if (blockRotation == BlockRotation.Left)
			{
                return quaternion.LookRotation(new float3(0, -1, 0), new float3(-1, 0, 0));
				//return quaternion.EulerXYZ(new float3(0, 0, 90) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Left90)
			{
                return quaternion.LookRotation(new float3(0, 0, 1), new float3(-1, 0, 0));
				//return quaternion.EulerXYZ(new float3(0, 90, 90) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Left180)
			{
                return quaternion.LookRotation(new float3(0, 1, 0), new float3(-1, 0, 0));
				//return quaternion.EulerXYZ(new float3(0, 180, 90) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Left270)
			{
                return quaternion.LookRotation(new float3(0, 0, -1), new float3(-1, 0, 0));
				//return quaternion.EulerXYZ(new float3(0, 270, 90) * degreesToRadians);
			}

			// Right #
			else if (blockRotation == BlockRotation.Right)
			{
                return quaternion.LookRotation(new float3(0, -1, 0), new float3(1, 0, 0));
				// return quaternion.EulerXYZ(new float3(0, 270, -90) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Right90)
			{
                return quaternion.LookRotation(new float3(0, 0, -1), new float3(1, 0, 0));
				// return quaternion.EulerXYZ(new float3(0, 0, -90) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Right180)
			{
                return quaternion.LookRotation(new float3(0, 1, 0), new float3(1, 0, 0));
				// return quaternion.EulerXYZ(new float3(0, 90 , -90) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Right270)
			{
                return quaternion.LookRotation(new float3(0, 0, 1), new float3(1, 0, 0));
				// return quaternion.EulerXYZ(new float3(0, 180, -90) * degreesToRadians);
			}

			// Down
			else if (blockRotation == BlockRotation.Down)
			{
                return quaternion.LookRotation(new float3(0, 0, -1), new float3(0, -1, 0));
				//return quaternion.EulerXYZ(new float3(180, 0, 0) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Down90) //  2)
			{
                return quaternion.LookRotation(new float3(1, 0, 0), new float3(0, -1, 0));
				//return quaternion.EulerXYZ(new float3(180, 90, 0) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Down180) // 1)
			{
                return quaternion.LookRotation(new float3(0, 0, 1), new float3(0, -1, 0));
				//return quaternion.EulerXYZ(new float3(180, 180, 0) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Down270) // 3)
			{
                return quaternion.LookRotation(new float3(-1, 0, 0), new float3(0, -1, 0));
				//return quaternion.EulerXYZ(new float3(180, 270, 0) * degreesToRadians);
			}

			// Up #
			else if (blockRotation == BlockRotation.Up)
			{
				return quaternion.EulerXYZ(new float3(0, 0, 0) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Up90) //  2)
			{
				return quaternion.EulerXYZ(new float3(0, 90, 0) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Up180) // 1)
			{
				return quaternion.EulerXYZ(new float3(0, 180, 0) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Up270) // 3)
			{
				return quaternion.EulerXYZ(new float3(0, 270, 0) * degreesToRadians);
			}

			// Backward
			else if (blockRotation == BlockRotation.Backward)
			{
                return quaternion.LookRotation(new float3(0, -1, 0), new float3(0, 0, -1));
				// return quaternion.EulerXYZ(new float3(-90, 0, 0) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Backward90) //  2)
			{
                return quaternion.LookRotation(new float3(-1, 0, 0), new float3(0, 0, -1));
				//return quaternion.EulerXYZ(new float3(-90, 0, 90) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Backward180) // 1)
			{
                return quaternion.LookRotation(new float3(0, 1, 0), new float3(0, 0, -1));
				//return quaternion.EulerXYZ(new float3(-90, 0, 180) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Backward270) // 3)
			{
                return quaternion.LookRotation(new float3(1, 0, 0), new float3(0, 0, -1));
				//return quaternion.EulerXYZ(new float3(-90, 0, 270) * degreesToRadians);
			}

			// Forward #
			else if (blockRotation == BlockRotation.Forward)
			{
				return quaternion.EulerXYZ(new float3(90, 0, 0) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Forward90) //  2)
			{
				return quaternion.EulerXYZ(new float3(90, 0, 90) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Forward180) // 1)
			{
				return quaternion.EulerXYZ(new float3(90, 0, 180) * degreesToRadians);
			}
			else if (blockRotation == BlockRotation.Forward270) // 3)
			{
				return quaternion.EulerXYZ(new float3(90, 0, 270) * degreesToRadians);
			}
			return quaternion.identity;
		}

		public static byte GetRawAxis(byte blockRotation)
		{
			if (blockRotation == Left || blockRotation == Left90 || blockRotation == Left180 || blockRotation == Left270)
			{
				return BlockRotation.Left;
			}
			else if (blockRotation == Right || blockRotation == Right90 || blockRotation == Right180 || blockRotation == Right270)
			{
				return BlockRotation.Right;
			}
			else if (blockRotation == Down || blockRotation == Down90 || blockRotation == Down180 || blockRotation == Down270)
			{
				return BlockRotation.Down;
			}
			else if (blockRotation == Up || blockRotation == Up90 || blockRotation == Up180 || blockRotation == Up270)
			{
				return BlockRotation.Up;
			}
			else if (blockRotation == Backward || blockRotation == Backward90 || blockRotation == Backward180 || blockRotation == Backward270)
			{
				return BlockRotation.Backward;
			}
			else if (blockRotation == Forward || blockRotation == Forward90 || blockRotation == Forward180 || blockRotation == Forward270)
			{
				return BlockRotation.Forward;
			}
			return 0;
		}

		public static byte GetAxis(byte blockRotation)
		{
			if (blockRotation == Left || blockRotation == Left90 || blockRotation == Left180 || blockRotation == Left270)
			{
				return PlanetSide.Left;
			}
			else if (blockRotation == Right || blockRotation == Right90 || blockRotation == Right180 || blockRotation == Right270)
			{
				return PlanetSide.Right;
			}
			else if (blockRotation == Down || blockRotation == Down90 || blockRotation == Down180 || blockRotation == Down270)
			{
				return PlanetSide.Down;
			}
			else if (blockRotation == Up || blockRotation == Up90 || blockRotation == Up180 || blockRotation == Up270)
			{
				return PlanetSide.Up;
			}
			else if (blockRotation == Backward || blockRotation == Backward90 || blockRotation == Backward180 || blockRotation == Backward270)
			{
				return PlanetSide.Backward;
			}
			else if (blockRotation == Forward || blockRotation == Forward90 || blockRotation == Forward180 || blockRotation == Forward270)
			{
				return PlanetSide.Forward;
			}
			return 0;
		}
	}
}