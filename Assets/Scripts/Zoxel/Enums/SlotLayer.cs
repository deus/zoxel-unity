namespace Zoxel // .Skeletons
{
    //! A layer for the equipment to be placed in body.
    public static class SlotLayer
    {
        public const byte Body = 0;
        public const byte Gear = 1;
    }
}