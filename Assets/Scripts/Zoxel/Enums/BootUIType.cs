using System;

namespace Zoxel
{
    [Serializable]
    public enum BootUIType : byte
    {
        None,
        Menu,
        BasicConsole,
        Networking,
        AudioOptions,
        RenderOptions,
        VideoOptions,
        MouseOptions,
        CheatMode,
        VoxelOptions,
        StatsOptions,
        ItemOptions,
        CharacterOptions,
        OptionsEnd,
        // Players
        FPS,
        TouchInput,
        VoxelSelection,
        GameStats,
        GameItems,
        GameVoxels,
        ChunkCharacters, 
        MapVoxels,
        MapBiomes,
        GameData,
        Gravity,
        NPCAI,
        Worldmap,           // show the planet of the chunks
        PlayerActions,
        Input,
        PlayerStats,
        PlayerInventory,
        PlayerControllers,
        PlayerAnimator,
        PlayerTarget,
        PlayerUIs,
        PlayerCollider,
        PlayerTraveler,
        Cameras,
        Tilemap,            // show the voxels texture planet for the planet
        End
    }
}