using Unity.Entities;

namespace Zoxel // .Players.UI
{
    public static class SpawnRealmUIType
    {
        public const byte Default = 0;
        public const byte SpawnExperiencebar = 1;
        public const byte DisableSpawnActionbar = 2;
        public const byte ChestUI = 3;
    }
}