using UnityEngine;
// move this into options? should not keep in main

namespace Zoxel
{
    //! Options used when building a build.
    public static class BuildOptions 
    {
        private const string filename = "buildoptions.zox";
        public const byte bytesLength = 2;
        // options
        public static bool isDemo;
        public static byte gameType;
        
        public static string gameName
        {
            get
            {
                if (BuildOptions.gameType == GameType.Zoxel)
                {
                    return "zoxel";
                }
                else if (BuildOptions.gameType == GameType.Woxel)
                {
                    return "woxel";
                }
                else if (BuildOptions.gameType == GameType.Zixel)
                {
                    return "zixel";
                }
                else if (BuildOptions.gameType == GameType.Zusix)
                {
                    return "zusix";
                }
                else
                {
                    return "";
                }
            }
        }

        private static string GetFilepath()
        {
            return Application.streamingAssetsPath + "/" + filename;
        }

        static BuildOptions()
        {
            LoadBuildOptions();
        }

        [RuntimeInitializeOnLoadMethod]
        static void LoadBuildOptions()
        {
            UnityEngine.Debug.Log("Loading BuildOptions!");
            var filepath = GetFilepath();
            UnityEngine.Debug.Log("Loading BuildOptions [" + filepath + "]");
            try
            {
                var bytes = System.IO.File.ReadAllBytes(filepath);
                if (bytes.Length != bytesLength)
                {
                    SetDefaults();
                    return;
                }
                isDemo = ByteUtil.ConvertToBool(bytes[0]);
                gameType = bytes[1];
                UnityEngine.Debug.Log(" - Loaded isDemo [" + isDemo + "]");
                UnityEngine.Debug.Log(" - Loaded gameType [" + gameType + "]");
            }
            catch (System.Exception e)
            {
                UnityEngine.Debug.LogError("E: " + e);
                SetDefaults();
            }
        }

        public static void SetDefaults()
        {
            isDemo = true;
            gameType = GameType.Zoxel;
        }

        public static void SaveOptions()
        {
            var filepath = GetFilepath();
            UnityEngine.Debug.Log("Saving BuildOptions [" + filepath + "] of bytes [" + bytesLength + "]");
            // for all properties
            var bytes = new byte[bytesLength];
            bytes[0] = ByteUtil.ConvertFromBool(isDemo);
            bytes[1] = gameType;
            // we're done
            System.IO.File.WriteAllBytes(filepath, bytes);
        }
    }
}
        
        /*[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        static void LoadBuildOptions2()
        {
            LoadBuildOptions();
        }

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterAssembliesLoaded)]
        static void LoadBuildOptions3()
        {
            LoadBuildOptions();
        }*/
    // [InitializeOnLoad]