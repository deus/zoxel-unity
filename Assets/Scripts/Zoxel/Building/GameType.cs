using UnityEngine;
// move this into options? should not keep in main

namespace Zoxel
{
    //! For each Game I make!
    public static class GameType
    {
        public const byte None = 0;
        public const byte Zoxel = 1;
        public const byte Woxel = 2;
        public const byte Zixel = 3;
        public const byte Zusix = 4;
        public const byte Zower = 5;
    }
}