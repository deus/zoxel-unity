using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Generic
{
    //! Sets editor entity name.
    [BurstCompile, UpdateInGroup(typeof(ZoxelGenericSystemGroup))]
    public partial class EditorNameSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithChangeFilter<EditorName>()
                .ForEach((Entity e, in EditorName editorName) =>
            {
                EntityManager.SetName(e, editorName.name.ToString());
			}).WithoutBurst().Run();
        }
    }
}