using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Generic
{
	//! Adds a DestroyEntity event in an amount of frames.
    [BurstCompile, UpdateInGroup(typeof(ZoxelGenericSystemGroup))]
    public partial class EntityDestroyDelaySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.ForEach((Entity e, int entityInQueryIndex, ref DestroyEntityInFrames destroyEntityInFrames) =>
			{
				destroyEntityInFrames.count--;
				if (destroyEntityInFrames.count == 0)
				{
					PostUpdateCommands.RemoveComponent<DestroyEntityInFrames>(entityInQueryIndex, e);
					PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e);
				}
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}