using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Generic
{
	//! Destroys all entities with a DestroyEntity component.
	/**
	*	\todo Put this in System Group after LateSimulationSystemGroup
	*/
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class EntityDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery processQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			processQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntityDirect>(),
                ComponentType.ReadOnly<DestroyEntity>());
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
			SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).DestroyEntity(processQuery);
		}
	}
}