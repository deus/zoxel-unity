using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Generic
{
    //! An event system for Initialization.
	[BurstCompile, UpdateInGroup(typeof(ZoxelGenericSystemGroup))]
    public partial class EntityInitializeSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<InitializeEntity>());
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
			SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<InitializeEntity>(processQuery);
		}
	}
}