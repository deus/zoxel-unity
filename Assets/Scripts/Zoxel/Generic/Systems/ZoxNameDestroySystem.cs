using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Generic
{
    //! Cleans up ZoxName components using the DestroyEntity event.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ZoxNameDestroySystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, ZoxName>()
                .ForEach((in ZoxName zoxName) =>
            {
                zoxName.DisposeFinal();
			}).ScheduleParallel();
        }
    }
}