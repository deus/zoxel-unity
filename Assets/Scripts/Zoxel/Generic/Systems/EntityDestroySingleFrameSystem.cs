using Unity.Entities;
using Unity.Burst;

namespace Zoxel
{
    public struct EntityDestroySingleFrame : IComponentData { }
    //! Destroys GenericEvent entities.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class EntityDestroySingleFrameSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			processQuery = GetEntityQuery(
                ComponentType.Exclude<DelayEvent>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<EntityDestroySingleFrame>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AddComponent<DestroyEntity>(processQuery);
        }
    }
}