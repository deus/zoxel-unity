using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Generic
{
    //! Completes DyingEntity event.
    [BurstCompile, UpdateInGroup(typeof(ZoxelGenericSystemGroup))]
    public partial class EntityDyingEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			processQuery = GetEntityQuery(ComponentType.ReadOnly<DyingEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            commandBufferSystem.RemoveComponent<DyingEntity>(processQuery);
            commandBufferSystem.AddComponent(processQuery, new DeadEntity(elapsedTime));
        }
    }
}