using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;

namespace Zoxel
{
    public struct GenerateName : IComponentData { }

    [BurstCompile, UpdateInGroup(typeof(ZoxelGenericSystemGroup))]
    public partial class NameGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<GenerateSeed>()
                .WithAll<GenerateName>()
                .ForEach((Entity e, int entityInQueryIndex, ref ZoxName zoxName, in ZoxID zoxID) =>
            {
                PostUpdateCommands.RemoveComponent<GenerateName>(entityInQueryIndex, e);
                zoxName.name.Dispose();
                var random = new Random();
                random.InitState((uint) zoxID.id);
                zoxName.name = NameGenerator.GenerateName2(ref random);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}