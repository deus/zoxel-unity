using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Generic
{
    //! Cleans up NearbyEntities.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class NearbyEntitiesDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, NearbyEntities>()
                .ForEach((in NearbyEntities nearbyEntities) =>
			{
                nearbyEntities.DisposeFinal();
			}).ScheduleParallel();
        }
    }
}