﻿using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Generic
{
    //! Destroys an Entity in a set amount of time.
    [BurstCompile, UpdateInGroup(typeof(ZoxelGenericSystemGroup))]
    public partial class EntityDestroyInTimeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity, DestroyEntityDirect>()
                .ForEach((Entity e, int entityInQueryIndex, in DestroyEntityInTime destroyEntityInTime) =>
            {
                if (destroyEntityInTime.HasFinished(elapsedTime))
                {
                    // PostUpdateCommands.RemoveComponent<DestroyEntityInTime>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
    //! Destroys them directly in time.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class EntityDestroyDirectInTimeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<DestroyEntityDirect>()
                .ForEach((Entity e, int entityInQueryIndex, in DestroyEntityInTime destroyEntityInTime) =>
            {
                if (destroyEntityInTime.HasFinished(elapsedTime))
                {
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}