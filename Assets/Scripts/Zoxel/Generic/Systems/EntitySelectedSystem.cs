using Unity.Entities;
using Unity.Burst;

namespace Zoxel.VoxelInteraction
{
    //! Handles raycasting against grass.
    [BurstCompile, UpdateInGroup(typeof(ZoxelGenericSystemGroup))]
    public partial class EntitySelectedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var elapsedTime = World.Time.ElapsedTime;
            Dependency = Entities
                .WithAll<OnEntitySelected>()
                .ForEach((Entity e, int entityInQueryIndex, in OnEntitySelected onEntitySelected) =>
            {
                if (elapsedTime - onEntitySelected.timeSelected >= 3f)
                {
                    PostUpdateCommands.RemoveComponent<OnEntitySelected>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<OnEntityDeselected>()
                .ForEach((Entity e, int entityInQueryIndex, in OnEntityDeselected onEntityDeselected) =>
            {
                if (elapsedTime - onEntityDeselected.timeDeselected >= 3f)
                {
                    PostUpdateCommands.RemoveComponent<OnEntityDeselected>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}