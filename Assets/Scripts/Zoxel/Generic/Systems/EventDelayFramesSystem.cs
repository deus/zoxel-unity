using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Generic
{
    //! Delay event entities with a set amount of frames.
    [BurstCompile, UpdateInGroup(typeof(ZoxelGenericSystemGroup))]
    public partial class EventDelayFramesSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref DelayEventFrames delayEventFrames) =>
            {
                delayEventFrames.count++;
                if (delayEventFrames.count >= delayEventFrames.frameDelay)
                {
                    PostUpdateCommands.RemoveComponent<DelayEventFrames>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}