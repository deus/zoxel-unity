using Unity.Entities;
using Unity.Burst;

namespace Zoxel
{
    //! \todo Generate these IDs in system.
    [BurstCompile, UpdateInGroup(typeof(ZoxelGenericSystemGroup))]
    public partial class SeedGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var newSeed = IDUtil.GenerateUniqueID();
            Dependency = Entities
                .WithAll<GenerateSeed>()
                .ForEach((Entity e, int entityInQueryIndex, ref Seed seed) =>
            {
                PostUpdateCommands.RemoveComponent<GenerateSeed>(entityInQueryIndex, e);
                seed.seed = newSeed;
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<GenerateSeed>()
                .ForEach((ref ZoxID zoxID) =>
            {
                zoxID.id = newSeed;
            }).ScheduleParallel(Dependency);
        }
    }
}