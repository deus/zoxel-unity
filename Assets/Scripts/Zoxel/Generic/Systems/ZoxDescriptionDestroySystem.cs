using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Generic
{
    //! Cleans up ZoxDescription components using the DestroyEntity event.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ZoxDescriptionDestroySystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, ZoxDescription>()
                .ForEach((in ZoxDescription zoxDescription) =>
            {
                zoxDescription.Dispose();
			}).ScheduleParallel();
        }
    }
}