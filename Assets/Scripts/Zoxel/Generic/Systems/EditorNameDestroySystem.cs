using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Generic
{
    //! Cleans up ZoxName components using the DestroyEntity event.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class EditorNameDestroySystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, EditorName>()
                .ForEach((in EditorName editorName) =>
            {
                editorName.Dispose();
			}).ScheduleParallel();
        }
    }
}