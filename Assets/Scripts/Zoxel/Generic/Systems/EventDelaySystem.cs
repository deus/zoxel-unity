using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Generic
{
    //! Delay event entities with a timed component. Removed after time runs out.
    [BurstCompile, UpdateInGroup(typeof(ZoxelGenericSystemGroup))]
    public partial class EventDelaySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in DelayEvent delayEvent) =>
            {
                if (elapsedTime - delayEvent.timeStarted >= delayEvent.timeDelay)
                {
                    PostUpdateCommands.RemoveComponent<DelayEvent>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}