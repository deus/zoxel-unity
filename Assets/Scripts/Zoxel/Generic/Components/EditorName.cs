using Unity.Entities;

namespace Zoxel
{
    //! A generic Text name used in many entities.
    public struct EditorName : IComponentData
    {
        public Text name;

        public EditorName(string name)
        {
            this.name = new Text(name);
        }

        public EditorName(Text name)
        {
            this.name = name;
        }

        public void Dispose()
        {
            name.Dispose();
        }

        public void SetName(Text name)
        {
            Dispose();
            this.name = name;
        }
 
#if UNITY_EDITOR
    void OnEditorGUI(string label)
    {
        var name2 = name.ToString();
        var name3 = UnityEditor.EditorGUILayout.TextField(label, name2);
        if (name3 != name2)
        {
            this.Dispose();
            this.name = new Text(name3);
        }
    }
#endif
    }
}