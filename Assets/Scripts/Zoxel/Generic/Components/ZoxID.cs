﻿using Unity.Entities;
using System;

namespace Zoxel
{
    //! A generic integer ID used in many entities.
    [Serializable]
    public struct ZoxID : IComponentData
    {
        public int id;
        
        public ZoxID(int id)
        {
            this.id = id;
        }
    }
}