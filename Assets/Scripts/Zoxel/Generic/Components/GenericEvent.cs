using Unity.Entities;

namespace Zoxel
{
    //! An event gets destroyed after one frame.
    public struct GenericEvent : IComponentData { }
}