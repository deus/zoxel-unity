using Unity.Entities;

namespace Zoxel
{
    //! Event for Entity being deselected.
    public struct OnEntityDeselected : IComponentData
    {
        public double timeDeselected;

        public OnEntityDeselected(double timeDeselected)
        {
            this.timeDeselected = timeDeselected;
        }
    }
}