﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel
{
    //! Data that contains a character entity and some extra data.
    public struct NearbyEntity
    {
        public Entity character;
        public Entity clan;
        public float distance;
        public float3 position;
    }

    //! Contains a bunch of nearby entities.
    /**
    *   Basically stuff that's in the grid but not the grid.
    *   \todo Probably need to make this dynamic. Seems like a waste to allocate alot.
    */
    public struct NearbyEntities : IComponentData
    {
        //! This uses a FixedArray! \todo Make a proper data structure for one.
        public BlitableArray<NearbyEntity> characters;
        public int count;
        public int totalNearby;
        
        public void DisposeFinal()
        {
            characters.DisposeFinal();
        }
        
        public void Dispose()
        {
            characters.Dispose();
        }

        public void Clear()
        {
            count = 0;
        }

        public int Length
        {
            get
            { 
                return count;
            }
        }

        public void Add(NearbyEntity character)
        {
            // does exist
            for (int i = 0; i < count; i++)
            {
                if (characters[i].character == character.character)
                {
                    return;
                }
            }
            if (count < characters.Length)
            {
                characters[count] = character;
                count++;
            }
        }
        
        public bool HasMax()
        {
            return (count >= characters.Length);
        }

        public void Initialize()
        {
            Dispose();
            characters = new BlitableArray<NearbyEntity>(8, Unity.Collections.Allocator.Persistent);
            for (int i = 0; i < characters.Length; i++)
            {
                characters[i] = new NearbyEntity();
            }
        }
    }
}