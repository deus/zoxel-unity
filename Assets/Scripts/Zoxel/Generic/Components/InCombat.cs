using Unity.Entities;

namespace Zoxel
{
    //! If an Entity is in combat.
    /**
    *   Added to entities during combat, so other modules may use the information.
    */
    public struct InCombat : IComponentData { }
}