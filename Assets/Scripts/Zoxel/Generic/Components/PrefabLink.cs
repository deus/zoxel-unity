using Unity.Entities;

namespace Zoxel
{
    //! An event used for reviving a dead entity.
    public struct PrefabLink : IComponentData
    {
        public Entity prefab;

        public PrefabLink(Entity prefab)
        {
            this.prefab = prefab;
        }
    }
}