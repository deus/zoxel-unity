using Unity.Entities;

namespace Zoxel
{
    //! If an Entity is Generating.
    public struct EntityGenerating : IComponentData { }
}