using Unity.Entities;

namespace Zoxel
{
    //! The reference to the Realm or Unique data of the User data entity.
    public struct MetaData : IComponentData
    {
        public Entity data;

        public MetaData(Entity data)
        {
            this.data = data;
        }
    }
}