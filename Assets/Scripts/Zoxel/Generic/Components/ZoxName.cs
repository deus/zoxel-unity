using Unity.Entities;
using System;

namespace Zoxel
{
    //! A generic Text name used in many entities.
    [Serializable]
    public struct ZoxName : IComponentData
    {
        public Text name;

        public ZoxName(Text name)
        {
            this.name = name;
        }

        public ZoxName(string name)
        {
            this.name = new Text(name);
        }
        
        public void DisposeFinal()
        {
            name.DisposeFinal();
        }

        public void Dispose()
        {
            name.Dispose();
        }

        public void SetName(Text name)
        {
            Dispose();
            this.name = name;
        }
    }
}