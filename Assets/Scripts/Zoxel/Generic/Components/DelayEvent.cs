using Unity.Entities;

namespace Zoxel
{
    //! Delays an event entity by a time delay.
    public struct DelayEvent : IComponentData
    {
        public double timeStarted;
        public double timeDelay;

        public DelayEvent(double timeStarted, double timeDelay)
        {
            this.timeDelay = timeDelay;
            this.timeStarted = timeStarted;
        }
    }
}