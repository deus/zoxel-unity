using Unity.Entities;

namespace Zoxel
{
    //! If an Entity is loading.
    public struct EntityLoading : IComponentData { }
}