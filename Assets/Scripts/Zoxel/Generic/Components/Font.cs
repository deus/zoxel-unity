using Unity.Entities;
using Unity.Collections;
// todo: implement more characters [, ], (, )

namespace Zoxel
{
    //! Every font is made up of zigels. Each zigel is made up of lines.
    public struct Font : IComponentData
    {
        public BlitableArray<PointsGroup2D> zigels;

        public void Dispose()
        {
            for (int i = 0; i < zigels.Length; i++)
            {
                zigels[i].Dispose();
            }
            zigels.Dispose();
        }

        public void SetPoints(int index, in int2[] characterPoints)
        {
            var pointsGroup = this.zigels[index];
            pointsGroup.points = new BlitableArray<int2>(characterPoints.Length, Allocator.Persistent);
            for (int i = 0; i < pointsGroup.points.Length; i++)
            {
                pointsGroup.points[i] = characterPoints[i];
            }
            this.zigels[index] = pointsGroup;
        }

        public void InitializeZigelSet()
        {
            this.zigels = new BlitableArray<PointsGroup2D>(Text.charactersList.Length, Allocator.Persistent);
            for (int i = 0; i < Text.charactersList.Length; i++)
            {
                var letter = Text.charactersList[i];
                InitializeZigel(i, letter);
            }
        }

        public void InitializeZigel(int index, char letter)
        {
            if (letter == '0')
            {
                SetPoints(index, in number0);
            }
            else if (letter == '1')
            {
                SetPoints(index, in number1);
            }
            else if (letter == '2')
            {
                SetPoints(index, in number2);
            }
            else if (letter == '3')
            {
                SetPoints(index, in number3);
            }
            else if (letter == '4')
            {
                SetPoints(index, in number4);
            }
            else if (letter == '5')
            {
                SetPoints(index, in number5);
            }
            else if (letter == '6')
            {
                SetPoints(index, in number6);
            }
            else if (letter == '7')
            {
                SetPoints(index, in number7);
            }
            else if (letter == '8')
            {
                SetPoints(index, in number8);
            }
            else if (letter == '9')
            {
                SetPoints(index, in number9);
            }

            else if (letter == '/')
            {
                SetPoints(index, in specialSlash);
            }
            else if (letter == '.')
            {
                SetPoints(index, in specialDot);
            }
            else if (letter == ',')
            {
                SetPoints(index, in specialComma);
            }
            else if (letter == '?')
            {
                SetPoints(index, in specialQuestionMark);
            }
            else if (letter == '\'')
            {
                SetPoints(index, in specialApostrophe);
            }
            else if (letter == '-')
            {
                SetPoints(index, in specialHyphen);
            }
            else if (letter == '<')
            {
                SetPoints(index, in specialArrowLeft);
            }
            else if (letter == '>')
            {
                SetPoints(index, in specialArrowRight);
            }
            else if (letter == '+')
            {
                SetPoints(index, in specialPlus);
            }
            else if (letter == '!')
            {
                SetPoints(index, in specialExclemation);
            }
            else if (letter == '%')
            {
                SetPoints(index, in specialPercentage);
            }
            else if (letter == ':')
            {
                SetPoints(index, in specialDoubleDots);
            }
            else if (letter == '[')
            {
                SetPoints(index, in leftSquareBrackets);
            }
            else if (letter == ']')
            {
                SetPoints(index, in rightSquareBrackets);
            }

            else if (letter == 'a')
            {
                SetPoints(index, in lowerA);
            }
            else if (letter == 'A')
            {
                SetPoints(index, in upperA);
            }
            else if (letter == 'b')
            {
                SetPoints(index, in lowerB);
            }
            else if (letter == 'B')
            {
                SetPoints(index, in upperB);
            }
            else if (letter == 'c')
            {
                SetPoints(index, in lowerC);
            }
            else if (letter == 'C')
            {
                SetPoints(index, in upperC);
            }
            else if (letter == 'd')
            {
                SetPoints(index, in lowerD);
            }
            else if (letter == 'D')
            {
                SetPoints(index, in upperD);
            }
            else if (letter == 'e')
            {
                SetPoints(index, in lowerE);
            }
            else if (letter == 'E')
            {
                SetPoints(index, in upperE);
            }
            else if (letter == 'f')
            {
                SetPoints(index, in lowerF);
            }
            else if (letter == 'F')
            {
                SetPoints(index, in upperF);
            }
            else if (letter == 'g')
            {
                SetPoints(index, in lowerG);
            }
            else if (letter == 'G')
            {
                SetPoints(index, in upperG);
            }
            else if (letter == 'h')
            {
                SetPoints(index, in lowerH);
            }
            else if (letter == 'H')
            {
                SetPoints(index, in upperH);
            }
            else if (letter == 'i')
            {
                SetPoints(index, in lowerI);
            }
            else if (letter == 'I')
            {
                SetPoints(index, in upperI);
            }
            else if (letter == 'j')
            {
                SetPoints(index, in lowerJ);
            }
            else if (letter == 'J')
            {
                SetPoints(index, in upperJ);
            }
            else if (letter == 'k')
            {
                SetPoints(index, in lowerK);
            }
            else if (letter == 'K')
            {
                SetPoints(index, in upperK);
            }
            else if (letter == 'l')
            {
                SetPoints(index, in lowerL);
            }
            else if (letter == 'L')
            {
                SetPoints(index, in upperL);
            }
            else if (letter == 'm')
            {
                SetPoints(index, in lowerM);
            }
            else if (letter == 'M')
            {
                SetPoints(index, in upperM);
            }
            else if (letter == 'n')
            {
                SetPoints(index, in lowerN);
            }
            else if (letter == 'N')
            {
                SetPoints(index, in upperN);
            }
            else if (letter == 'o')
            {
                SetPoints(index, in lowerO);
            }
            else if (letter == 'O')
            {
                SetPoints(index, in upperO);
            }
            else if (letter == 'p')
            {
                SetPoints(index, in lowerP);
            }
            else if (letter == 'P')
            {
                SetPoints(index, in upperP);
            }
            else if (letter == 'q')
            {
                SetPoints(index, in lowerQ);
            }
            else if (letter == 'Q')
            {
                SetPoints(index, in upperQ);
            }
            else if (letter == 'r')
            {
                SetPoints(index, in lowerR);
            }
            else if (letter == 'R')
            {
                SetPoints(index, in upperR);
            }
            else if (letter == 's')
            {
                SetPoints(index, in lowerS);
            }
            else if (letter == 'S')
            {
                SetPoints(index, in upperS);
            }
            else if (letter == 't')
            {
                SetPoints(index, in lowerT);
            }
            else if (letter == 'T')
            {
                SetPoints(index, in upperT);
            }
            else if (letter == 'u')
            {
                SetPoints(index, in lowerU);
            }
            else if (letter == 'U')
            {
                SetPoints(index, in upperU);
            }
            else if (letter == 'v')
            {
                SetPoints(index, in lowerV);
            }
            else if (letter == 'V')
            {
                SetPoints(index, in upperV);
            }
            else if (letter == 'w')
            {
                SetPoints(index, in lowerW);
            }
            else if (letter == 'W')
            {
                SetPoints(index, in upperW);
            }
            else if (letter == 'x')
            {
                SetPoints(index, in lowerX);
            }
            else if (letter == 'X')
            {
                SetPoints(index, in upperX);
            }
            else if (letter == 'y')
            {
                SetPoints(index, in lowerY);
            }
            else if (letter == 'Y')
            {
                SetPoints(index, in upperY);
            }
            else if (letter == 'z')
            {
                SetPoints(index, in lowerZ);
            }
            else if (letter == 'Z')
            {
                SetPoints(index, in upperZ);
            }
            else if (letter == '↓')
            {
                SetPoints(index, in downArrow);
            }
            else if (letter == '↑')
            {
                SetPoints(index, in upArrow);
            }
        }
        
        const int upperTopSide = 13;
        const int lowerTopSide = 9;
        const int bottomSide = 2;
        const int leftSide = 2;
        const int rightSide = 13;
        const int middleSideX = 8;
        const int upperMiddleSideY = 8;
        const int lowerMiddleSideY = 5;
        const int elStart = 6;
        const int curve = 4;
        const int lowerCurve = 2;

        private readonly static int2[] number0 = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(rightSide, upperTopSide),
            new int2(rightSide, upperTopSide), new int2(rightSide, bottomSide),
            new int2(leftSide, upperTopSide), new int2(leftSide, bottomSide),
            new int2(leftSide, bottomSide), new int2(rightSide, bottomSide)
        };
        
        private readonly static int2[] number1 = new int2[]
        {
            new int2(middleSideX, bottomSide), new int2(middleSideX, upperTopSide),
            new int2(middleSideX, upperTopSide), new int2(middleSideX - curve, upperTopSide - curve),
            new int2(middleSideX, bottomSide), new int2(leftSide + 1, bottomSide),
            new int2(middleSideX, bottomSide), new int2(rightSide - 1, bottomSide),
        };

        private readonly static int2[] number2 = new int2[]
        {
            new int2(leftSide, upperTopSide - 3), new int2(leftSide, upperTopSide),
            new int2(leftSide, upperTopSide), new int2(rightSide - 3, upperTopSide),
            new int2(rightSide - 3, upperTopSide), new int2(rightSide, upperTopSide - 4),
            new int2(rightSide, upperTopSide - 4), new int2(leftSide, bottomSide),
            new int2(leftSide, bottomSide), new int2(rightSide, bottomSide)
        };

        private readonly static int2[] number3 = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(rightSide - 3, upperTopSide),
            new int2(rightSide - 3, upperTopSide), new int2(rightSide, upperTopSide - 3),
            new int2(rightSide, upperTopSide - 3), new int2(rightSide, upperMiddleSideY + 3),
            new int2(rightSide, upperMiddleSideY + 3), new int2(rightSide - 3, upperMiddleSideY),
            new int2(rightSide - 3, upperMiddleSideY), new int2(leftSide - 4, upperMiddleSideY),

            new int2(leftSide - 4, upperMiddleSideY), new int2(rightSide - 3, upperMiddleSideY),
            new int2(rightSide - 3, upperMiddleSideY), new int2(rightSide, upperMiddleSideY - 3),
            new int2(rightSide, upperMiddleSideY - 3), new int2(rightSide, bottomSide + 3),
            new int2(rightSide, bottomSide + 3), new int2(rightSide - 3, bottomSide),
            new int2(rightSide - 3, bottomSide), new int2(leftSide, bottomSide)
        };

        private readonly static int2[] number4 = new int2[]
        {
            new int2(middleSideX, upperTopSide), new int2(leftSide, upperMiddleSideY),
            new int2(middleSideX, upperTopSide), new int2(upperMiddleSideY, bottomSide),
            new int2(leftSide, upperMiddleSideY), new int2(rightSide, upperMiddleSideY)
};

        private readonly static int2[] number5 = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(rightSide, upperTopSide),
            new int2(leftSide, upperTopSide), new int2(leftSide, upperMiddleSideY),
            new int2(leftSide, upperMiddleSideY), new int2(rightSide - 3, upperMiddleSideY),
            new int2(rightSide - 3, upperMiddleSideY), new int2(rightSide, upperMiddleSideY - 3),
            new int2(rightSide, upperMiddleSideY - 3), new int2(rightSide, bottomSide + 3),
            new int2(rightSide, bottomSide + 3), new int2(rightSide - 3, bottomSide),
            new int2(rightSide - 3, bottomSide), new int2(leftSide, bottomSide)
        };

        private readonly static int2[] number6 = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(rightSide, upperTopSide),
            new int2(rightSide, upperTopSide), new int2(rightSide, upperTopSide - 2),
            new int2(leftSide, upperTopSide), new int2(leftSide, upperMiddleSideY),
            new int2(leftSide, upperMiddleSideY), new int2(rightSide - 3, upperMiddleSideY),
            new int2(rightSide - 3, upperMiddleSideY), new int2(rightSide, upperMiddleSideY - 3),
            new int2(rightSide, upperMiddleSideY - 3), new int2(rightSide, bottomSide + 3),
            new int2(rightSide, bottomSide + 3), new int2(rightSide - 3, bottomSide),
            new int2(rightSide - 3, bottomSide), new int2(leftSide, bottomSide),
            new int2(rightSide - 3, bottomSide), new int2(leftSide, bottomSide),
            new int2(leftSide, bottomSide), new int2(leftSide, upperMiddleSideY)
        };

        private readonly static int2[] number7 = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(rightSide, upperTopSide),
            new int2(rightSide, upperTopSide), new int2(leftSide, bottomSide)
        };

        private readonly static int2[] number8 = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(rightSide, upperTopSide),
            new int2(leftSide, upperMiddleSideY), new int2(rightSide, upperMiddleSideY),
            new int2(leftSide, bottomSide), new int2(rightSide, bottomSide),
            new int2(leftSide, upperTopSide), new int2(leftSide, bottomSide),
            new int2(rightSide, upperTopSide), new int2(rightSide, bottomSide)
        };

        private readonly static int2[] number9 = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(rightSide, upperTopSide),
            new int2(leftSide, upperMiddleSideY), new int2(rightSide, upperMiddleSideY),
            new int2(leftSide, bottomSide), new int2(rightSide, bottomSide),
            new int2(leftSide, upperTopSide), new int2(leftSide, upperMiddleSideY),
            new int2(rightSide, upperTopSide), new int2(rightSide, bottomSide)
        };

        private readonly static int2[] specialSlash = new int2[]
        {
            new int2(3, 3), new int2(12, 12)
        };

        private readonly static int2[] specialHyphen = new int2[]
        {
            new int2(leftSide, upperMiddleSideY), new int2(rightSide, upperMiddleSideY)
        };

        private readonly static int2[] specialArrowLeft = new int2[]
        {
            new int2(leftSide, upperMiddleSideY), new int2(rightSide, upperTopSide), 
            new int2(leftSide, upperMiddleSideY), new int2(rightSide, bottomSide)
        };

        private readonly static int2[] specialArrowRight = new int2[]
        {
            new int2(rightSide, upperMiddleSideY), new int2(leftSide, upperTopSide), 
            new int2(rightSide, upperMiddleSideY), new int2(leftSide, bottomSide)
        };

        private readonly static int2[] specialCross = new int2[]
        {
            new int2(middleSideX, upperMiddleSideY), new int2(leftSide, upperTopSide), 
            new int2(middleSideX, upperMiddleSideY), new int2(rightSide, upperTopSide),
            new int2(middleSideX, upperMiddleSideY), new int2(leftSide, bottomSide),
            new int2(middleSideX, upperMiddleSideY), new int2(rightSide, bottomSide)
        };

        

        private readonly static int2[] specialPlus = new int2[]
        {
            new int2(leftSide, upperMiddleSideY), new int2(middleSideX, upperMiddleSideY),
            new int2(middleSideX, upperMiddleSideY), new int2(rightSide, upperMiddleSideY),
            new int2(middleSideX, upperMiddleSideY), new int2(middleSideX, upperTopSide),
            new int2(middleSideX, upperMiddleSideY), new int2(middleSideX, bottomSide)
        };

        private readonly static int2[] specialPercentage = new int2[]
        {
            new int2(3, 3), new int2(12, 12),
            new int2(3, 9), new int2(4, 10),
            new int2(9, 3), new int2(10, 4)
        };
        private readonly static int2[] specialExclemation = new int2[]
        {
            new int2(7, 6), new int2(7, 13),
            new int2(8, 6), new int2(8, 13),
            new int2(9, 6), new int2(9, 13),
            new int2(7, 1), new int2(9, 1),
            new int2(7, 3), new int2(9, 3)
        };

        private readonly static int2[] leftSquareBrackets = new int2[]
        {
            new int2(middleSideX, bottomSide), new int2(middleSideX, upperTopSide),
            new int2(middleSideX, bottomSide), new int2(rightSide, bottomSide),
            new int2(middleSideX, upperTopSide), new int2(rightSide, upperTopSide)
        };

        private readonly static int2[] rightSquareBrackets = new int2[]
        {
            new int2(middleSideX, bottomSide), new int2(middleSideX, upperTopSide),
            new int2(middleSideX, bottomSide), new int2(leftSide, bottomSide),
            new int2(middleSideX, upperTopSide), new int2(leftSide, upperTopSide)
        };

        private readonly static int2[] specialDot = new int2[] 
        {
            new int2(7, 3), new int2(9, 3),
            new int2(7, 5), new int2(9, 5)
        };

        private readonly static int2[] specialComma = new int2[] 
        {
            new int2(7, 3), new int2(9, 3),
            new int2(7, 5), new int2(9, 5),
            new int2(7, 3), new int2(3, 1),
            new int2(9, 3), new int2(3, 1)
        };

        // todo: this
        private readonly static int2[] specialQuestionMark = new int2[] 
        {
            new int2(3, 12), new int2(10, 12),  // top line
            // side lines
            new int2(3, 12), new int2(3, 7),
            new int2(10, 12), new int2(10, 7),

            new int2(10, 7), new int2(5, 7), // right middle to middle
            new int2(5, 7), new int2(5, 4), // middle to down
            new int2(4, 2), new int2(6, 2), // dot
        };

        private readonly static int2[] specialApostrophe = new int2[] 
        {
            new int2(7, 10), new int2(9, 10),
            new int2(7, 11), new int2(9, 11),
            new int2(7, 12), new int2(9, 12),
            new int2(7, 13), new int2(9, 13)
        };

        private readonly static int2[] specialDoubleDots = new int2[] 
        {
            new int2(middleSideX, 4), new int2(middleSideX + 2, 4),
            new int2(middleSideX + 2, 4), new int2(middleSideX + 2, 6),
            new int2(middleSideX, 6), new int2(middleSideX + 2, 6),
            new int2(middleSideX, 4), new int2(middleSideX, 7),
            new int2(middleSideX, 10), new int2(middleSideX + 2, 10),
            new int2(middleSideX + 2, 10), new int2(middleSideX + 2, 10),
            new int2(middleSideX, 12), new int2(middleSideX + 2, 12),
            new int2(middleSideX, 12), new int2(middleSideX, 12),
        };

        private readonly static int2[] lowerA = new int2[]
        {
            // right line
            new int2(rightSide, lowerTopSide), new int2(rightSide, lowerTopSide - curve),
            new int2(rightSide, lowerTopSide - curve), new int2(rightSide, bottomSide + curve),
            new int2(rightSide, bottomSide + curve), new int2(rightSide, bottomSide),
            // top part
            new int2(rightSide, lowerTopSide - curve), new int2(rightSide - curve, lowerTopSide),
            new int2(rightSide - curve, lowerTopSide), new int2(leftSide - curve, lowerTopSide),
            //new int2(rightSide, lowerTopSide), new int2(rightSide, lowerTopSide - curve),
            new int2(leftSide - curve, lowerTopSide), new int2(leftSide, lowerTopSide - curve),
            // bottom
            new int2(leftSide, lowerTopSide - curve), new int2(leftSide, bottomSide + curve),
            new int2(leftSide, bottomSide + curve), new int2(rightSide - curve, bottomSide),
            new int2(rightSide - curve, bottomSide), new int2(rightSide, bottomSide + curve),
        };  

        private readonly static int2[] upperA = new int2[]
        {
            new int2(leftSide, bottomSide), new int2(middleSideX, upperTopSide),
            new int2(middleSideX, upperTopSide), new int2(rightSide, bottomSide),
            new int2(leftSide + 3, upperMiddleSideY), new int2(rightSide - 3, upperMiddleSideY),
        };
        
        private readonly static int2[] lowerB = new int2[]
        {
            new int2(leftSide, upperMiddleSideY), new int2(rightSide, upperMiddleSideY),
            new int2(leftSide, bottomSide), new int2(rightSide, bottomSide),
            new int2(leftSide, upperMiddleSideY), new int2(leftSide, bottomSide),
            new int2(rightSide, upperMiddleSideY), new int2(rightSide, bottomSide),
            new int2(leftSide, upperTopSide), new int2(leftSide, upperMiddleSideY)
        };

        private readonly static int2[] upperB = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(leftSide, upperMiddleSideY),
            new int2(leftSide, upperMiddleSideY), new int2(leftSide, bottomSide),
            new int2(leftSide, upperMiddleSideY), new int2(rightSide - curve, upperMiddleSideY),
            // bottom loop
            new int2(leftSide, bottomSide), new int2(rightSide - curve, bottomSide),
            new int2(rightSide - curve, bottomSide), new int2(rightSide, bottomSide + curve),
            new int2(rightSide, bottomSide + curve), new int2(rightSide, upperMiddleSideY - curve),
            new int2(rightSide, upperMiddleSideY - curve), new int2(rightSide - curve, upperMiddleSideY),
            // top loop
            new int2(leftSide, upperTopSide), new int2(rightSide - curve, upperTopSide),
            new int2(rightSide - curve, upperTopSide), new int2(rightSide, upperTopSide - curve),
            new int2(rightSide, upperTopSide - curve), new int2(rightSide, upperMiddleSideY + curve),
            new int2(rightSide, upperMiddleSideY + curve), new int2(rightSide - curve, upperMiddleSideY),
        };
        
        private readonly static int2[] lowerC = new int2[]
        {
            new int2(leftSide + lowerCurve, lowerTopSide), new int2(rightSide - lowerCurve, lowerTopSide),
            new int2(rightSide - lowerCurve, lowerTopSide), new int2(rightSide, lowerTopSide - lowerCurve / 2),

            new int2(leftSide + lowerCurve, lowerTopSide), new int2(leftSide, lowerTopSide - lowerCurve),
            new int2(leftSide, lowerTopSide - lowerCurve), new int2(leftSide, bottomSide + lowerCurve),

            new int2(leftSide, bottomSide + lowerCurve), new int2(leftSide + lowerCurve, bottomSide),
            new int2(leftSide + lowerCurve, bottomSide), new int2(rightSide - lowerCurve, bottomSide),
            new int2(rightSide - lowerCurve, bottomSide), new int2(rightSide, bottomSide + lowerCurve / 2),
        };

        private readonly static int2[] upperC = new int2[]
        {
            new int2(rightSide, upperTopSide - curve), new int2(rightSide - curve, upperTopSide),
            new int2(rightSide - curve, upperTopSide), new int2(leftSide + curve, upperTopSide),

            new int2(leftSide + curve, upperTopSide), new int2(leftSide, upperTopSide - curve),
            new int2(leftSide, upperTopSide - curve), new int2(leftSide, bottomSide + curve),

            new int2(leftSide, bottomSide + curve), new int2(leftSide + curve, bottomSide),
            new int2(leftSide + curve, bottomSide), new int2(rightSide - curve, bottomSide),
            new int2(rightSide - curve, bottomSide), new int2(rightSide, bottomSide + curve),
        };
        
        private readonly static int2[] lowerD = new int2[]
        {
            new int2(leftSide, middleSideX), new int2(rightSide, middleSideX),
            new int2(leftSide, bottomSide), new int2(rightSide, bottomSide),
            new int2(leftSide, middleSideX), new int2(leftSide, bottomSide),
            new int2(rightSide, middleSideX), new int2(rightSide, bottomSide),
            new int2(rightSide, upperTopSide), new int2(rightSide, middleSideX)
        };

        private readonly static int2[] upperD = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(leftSide, bottomSide),
            new int2(leftSide, upperTopSide), new int2(middleSideX, upperTopSide),
            new int2(middleSideX, upperTopSide), new int2(rightSide, upperTopSide - curve),
            new int2(rightSide, upperTopSide - curve), new int2(rightSide, bottomSide + curve),
            new int2(rightSide, bottomSide + curve), new int2(middleSideX, bottomSide),
            new int2(middleSideX, bottomSide), new int2(leftSide, bottomSide)
        };
        
        private readonly static int2[] lowerE = new int2[]
        {
            new int2(leftSide, lowerTopSide), new int2(rightSide, lowerTopSide),
            new int2(rightSide, lowerTopSide), new int2(rightSide, lowerMiddleSideY),
            new int2(rightSide, lowerMiddleSideY), new int2(leftSide, lowerMiddleSideY),
            new int2(leftSide, lowerMiddleSideY), new int2(leftSide, lowerTopSide),
            new int2(leftSide, lowerMiddleSideY), new int2(leftSide, bottomSide),
            new int2(leftSide, bottomSide), new int2(rightSide, bottomSide),
            new int2(rightSide, bottomSide), new int2(rightSide, bottomSide + 1)
        };

        private readonly static int2[] upperE = new int2[]
        {
            new int2(leftSide, bottomSide), new int2(leftSide, upperTopSide),
            new int2(leftSide, bottomSide), new int2(rightSide, bottomSide),
            new int2(leftSide, upperTopSide), new int2(rightSide, upperTopSide),
            new int2(leftSide, upperMiddleSideY), new int2(rightSide - 3, upperMiddleSideY)
        };
        
        private readonly static int2[] lowerF = new int2[]
        {
            new int2(middleSideX, upperTopSide), new int2(middleSideX, middleSideX),
            new int2(middleSideX, upperTopSide), new int2(rightSide, upperTopSide),
            new int2(middleSideX, middleSideX), new int2(middleSideX, bottomSide),
            new int2(leftSide, middleSideX), new int2(rightSide, middleSideX)
        };

        private readonly static int2[] upperF = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(leftSide, middleSideX),
            new int2(leftSide, upperTopSide), new int2(rightSide, upperTopSide),
            new int2(leftSide, middleSideX), new int2(leftSide, bottomSide),
            new int2(leftSide, middleSideX), new int2(rightSide, middleSideX)
        };
        
        private readonly static int2[] lowerG = new int2[]
        {
            new int2(leftSide, lowerTopSide), new int2(rightSide, lowerTopSide),
            new int2(rightSide, lowerTopSide), new int2(rightSide, lowerMiddleSideY),
            new int2(rightSide, lowerMiddleSideY), new int2(leftSide, lowerMiddleSideY),
            new int2(leftSide, lowerTopSide), new int2(leftSide, lowerMiddleSideY),
            new int2(rightSide, lowerMiddleSideY), new int2(rightSide, bottomSide),
            new int2(rightSide, bottomSide), new int2(leftSide, bottomSide)
        };

        private readonly static int2[] upperG = new int2[]
        {
            new int2(rightSide, upperTopSide - curve), new int2(rightSide - curve, upperTopSide),
            new int2(rightSide - curve, upperTopSide), new int2(leftSide + curve, upperTopSide),
            new int2(leftSide + curve, upperTopSide), new int2(leftSide, upperTopSide - curve),
            new int2(leftSide, upperTopSide - curve), new int2(leftSide, bottomSide + curve),
            new int2(leftSide, bottomSide + curve), new int2(leftSide + curve, bottomSide),
            new int2(leftSide + curve, bottomSide), new int2(rightSide - curve, bottomSide),
            new int2(rightSide - curve, bottomSide), new int2(rightSide, bottomSide + curve + 2),
            new int2(rightSide, bottomSide + curve + 2), new int2(rightSide - curve - 1, bottomSide + curve),
        };
        
        private readonly static int2[] lowerH = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(leftSide, middleSideX),
            new int2(leftSide, middleSideX), new int2(leftSide, bottomSide),
            new int2(leftSide, middleSideX), new int2(rightSide, middleSideX),
            new int2(rightSide, middleSideX), new int2(rightSide, bottomSide)
        };

        private readonly static int2[] upperH = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(leftSide, middleSideX),
            new int2(leftSide, middleSideX), new int2(leftSide, bottomSide),
            new int2(leftSide, middleSideX), new int2(rightSide, middleSideX),
            new int2(rightSide, middleSideX), new int2(rightSide, bottomSide),
            new int2(rightSide, middleSideX), new int2(rightSide, upperTopSide)
        };
        
        private readonly static int2[] lowerI = new int2[]
        {
            new int2(middleSideX, bottomSide), new int2(middleSideX, middleSideX + 1),
            new int2(middleSideX, middleSideX + 4), new int2(middleSideX, middleSideX + 6)
        };

        private readonly static int2[] upperI = new int2[]
        {
            new int2(middleSideX, upperTopSide), new int2(middleSideX, bottomSide),
            new int2(leftSide, upperTopSide), new int2(middleSideX, upperTopSide),
            new int2(rightSide, upperTopSide), new int2(middleSideX, upperTopSide),
            new int2(leftSide, bottomSide), new int2(middleSideX, bottomSide),
            new int2(rightSide, bottomSide), new int2(middleSideX, bottomSide)
        };
        
        private readonly static int2[] lowerJ = new int2[]
        {
            new int2(middleSideX, lowerTopSide), new int2(middleSideX, bottomSide + 2),
            new int2(middleSideX - 2, bottomSide), new int2(middleSideX, bottomSide + 2),
            new int2(middleSideX - 2, bottomSide), new int2(middleSideX - 3, bottomSide),
        };

        private readonly static int2[] upperJ = new int2[]
        {
            new int2(middleSideX, upperTopSide), new int2(middleSideX, bottomSide + 2),
            new int2(middleSideX - 2, bottomSide), new int2(middleSideX, bottomSide + 2),
            new int2(middleSideX - 2, bottomSide), new int2(middleSideX - 3, bottomSide),
        };
        
        private readonly static int2[] lowerK = new int2[]
        {
            new int2(leftSide, lowerTopSide), new int2(leftSide, lowerMiddleSideY),
            new int2(leftSide, lowerMiddleSideY), new int2(leftSide, bottomSide),
            new int2(leftSide, lowerMiddleSideY), new int2(rightSide, lowerTopSide),
            new int2(leftSide, lowerMiddleSideY), new int2(rightSide, bottomSide),
        };

        private readonly static int2[] upperK = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(leftSide, middleSideX),
            new int2(leftSide, middleSideX), new int2(leftSide, bottomSide),
            new int2(leftSide, middleSideX), new int2(rightSide, upperTopSide),
            new int2(leftSide, middleSideX), new int2(rightSide, bottomSide),
        };
        
        private readonly static int2[] lowerL = new int2[]
        {
            new int2(elStart, bottomSide), new int2(elStart, upperTopSide),
            new int2(elStart, bottomSide), new int2(elStart + curve, bottomSide),
            new int2(elStart + curve, bottomSide), new int2(elStart + curve, bottomSide + curve)
        };

        private readonly static int2[] upperL = new int2[]
        {
            new int2(leftSide, bottomSide), new int2(leftSide, upperTopSide),
            new int2(leftSide, bottomSide), new int2(rightSide, bottomSide),
            new int2(rightSide, bottomSide), new int2(rightSide, bottomSide + 2)
        };
        
        private readonly static int2[] lowerM = new int2[]
        {
            new int2(leftSide, bottomSide), new int2(leftSide, lowerTopSide),
            new int2(rightSide, bottomSide), new int2(rightSide, lowerTopSide),
            new int2(middleSideX, bottomSide), new int2(rightSide, lowerTopSide),
            new int2(middleSideX, bottomSide), new int2(leftSide, lowerTopSide),
        };

        // copy of lowerM
        private readonly static int2[] upperM = new int2[]
        {
            new int2(leftSide, bottomSide), new int2(leftSide, upperTopSide),
            new int2(rightSide, bottomSide), new int2(rightSide, upperTopSide),
            new int2(middleSideX, bottomSide), new int2(rightSide, upperTopSide),
            new int2(middleSideX, bottomSide), new int2(leftSide, upperTopSide),
        };
        
        private readonly static int2[] lowerN = new int2[]
        {
            new int2(leftSide, bottomSide), new int2(leftSide, lowerTopSide),
            new int2(leftSide, bottomSide), new int2(rightSide, lowerTopSide),
            new int2(rightSide, bottomSide), new int2(rightSide, lowerTopSide),
        };

        // copy of lowerN
        private readonly static int2[] upperN = new int2[]
        {
            new int2(leftSide, bottomSide), new int2(leftSide, upperTopSide),
            new int2(leftSide, bottomSide), new int2(rightSide, upperTopSide),
            new int2(rightSide, bottomSide), new int2(rightSide, upperTopSide),
        };
        
        private readonly static int2[] lowerO = new int2[]
        {
            new int2(leftSide, lowerTopSide), new int2(rightSide, lowerTopSide),
            new int2(rightSide, lowerTopSide), new int2(rightSide, bottomSide),
            new int2(leftSide, lowerTopSide), new int2(leftSide, bottomSide),
            new int2(leftSide, bottomSide), new int2(rightSide, bottomSide)
        };

        // copy of lowerO
        private readonly static int2[] upperO = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(rightSide, upperTopSide),
            new int2(rightSide, upperTopSide), new int2(rightSide, bottomSide),
            new int2(leftSide, upperTopSide), new int2(leftSide, bottomSide),
            new int2(leftSide, bottomSide), new int2(rightSide, bottomSide)
        };
        
        private readonly static int2[] lowerP = new int2[]
        {
            new int2(leftSide, lowerTopSide), new int2(rightSide, lowerTopSide),
            new int2(rightSide, lowerTopSide), new int2(rightSide, lowerMiddleSideY),
            new int2(rightSide, lowerMiddleSideY), new int2(leftSide, lowerMiddleSideY),
            new int2(leftSide, lowerMiddleSideY), new int2(leftSide, lowerTopSide),
            new int2(leftSide, lowerMiddleSideY), new int2(leftSide, bottomSide)
        };

        // copy of lowerP
        private readonly static int2[] upperP = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(rightSide, upperTopSide),
            new int2(rightSide, upperTopSide), new int2(rightSide, middleSideX),
            new int2(rightSide, middleSideX), new int2(leftSide, middleSideX),
            new int2(leftSide, middleSideX), new int2(leftSide, upperTopSide),
            new int2(leftSide, middleSideX), new int2(leftSide, bottomSide)
        };
        
        private readonly static int2[] lowerQ = new int2[]
        {
            new int2(leftSide, lowerTopSide), new int2(rightSide, lowerTopSide),
            new int2(rightSide, lowerTopSide), new int2(rightSide, lowerMiddleSideY),
            new int2(rightSide, lowerMiddleSideY), new int2(leftSide, lowerMiddleSideY),
            new int2(leftSide, lowerMiddleSideY), new int2(leftSide, lowerTopSide),
            new int2(rightSide, lowerMiddleSideY), new int2(rightSide, bottomSide)
        };

        private readonly static int2[] upperQ = new int2[]
        {
            // top part
            new int2(leftSide, upperTopSide), new int2(rightSide, upperTopSide),
            new int2(rightSide, upperTopSide), new int2(rightSide, middleSideX),
            new int2(rightSide, middleSideX), new int2(leftSide, middleSideX),
            new int2(leftSide, middleSideX), new int2(leftSide, upperTopSide),
            new int2(rightSide, middleSideX), new int2(rightSide, bottomSide)
        };
        
        private readonly static int2[] lowerR = new int2[]
        {
            new int2(leftSide, middleSideX + 2), new int2(leftSide, bottomSide),
            new int2(leftSide, middleSideX - 3), new int2(leftSide + 3, middleSideX),
            new int2(leftSide + 3, middleSideX), new int2(rightSide, middleSideX),
        };

        private readonly static int2[] upperR = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(rightSide, upperTopSide),
            new int2(rightSide, upperTopSide), new int2(rightSide, middleSideX),
            new int2(rightSide, middleSideX), new int2(leftSide, middleSideX),
            new int2(leftSide, middleSideX), new int2(leftSide, upperTopSide),
            new int2(leftSide, middleSideX), new int2(leftSide, bottomSide),
            new int2(leftSide, middleSideX), new int2(rightSide, bottomSide)
        };
        
        private readonly static int2[] lowerS = new int2[]
        {
            new int2(leftSide, lowerTopSide), new int2(rightSide, lowerTopSide),
            new int2(leftSide, lowerTopSide), new int2(leftSide, lowerMiddleSideY),
            new int2(leftSide, lowerMiddleSideY), new int2(rightSide, lowerMiddleSideY),
            new int2(rightSide, lowerMiddleSideY), new int2(rightSide, bottomSide),
            new int2(rightSide, bottomSide), new int2(leftSide, bottomSide),
        };

        private readonly static int2[] upperS = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(rightSide, upperTopSide),
            new int2(leftSide, upperTopSide), new int2(leftSide, middleSideX),
            new int2(leftSide, middleSideX), new int2(rightSide, middleSideX),
            new int2(rightSide, middleSideX), new int2(rightSide, bottomSide),
            new int2(rightSide, bottomSide), new int2(leftSide, bottomSide),
        };
        
        private readonly static int2[] lowerT = new int2[]
        {
            new int2(middleSideX, bottomSide), new int2(middleSideX, middleSideX),
            new int2(middleSideX, middleSideX), new int2(middleSideX, upperTopSide),
            new int2(middleSideX, middleSideX), new int2(leftSide, middleSideX),
            new int2(middleSideX, middleSideX), new int2(rightSide, middleSideX),
            new int2(middleSideX, bottomSide), new int2(middleSideX + 3, bottomSide),
            new int2(middleSideX + 3, bottomSide), new int2(middleSideX + 3, bottomSide + 2),
        };

        private readonly static int2[] upperT = new int2[]
        {
            new int2(middleSideX, upperTopSide), new int2(middleSideX, bottomSide),
            new int2(leftSide, upperTopSide), new int2(middleSideX, upperTopSide),
            new int2(rightSide, upperTopSide), new int2(middleSideX, upperTopSide)
        };
        
        // Needs more curve
        private readonly static int2[] lowerU = new int2[]
        {
            new int2(leftSide, lowerTopSide), new int2(leftSide, bottomSide),
            new int2(leftSide, bottomSide), new int2(rightSide, bottomSide),
            new int2(rightSide, bottomSide), new int2(rightSide, lowerTopSide)
        };

        // copied lowerU
        private readonly static int2[] upperU = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(leftSide, bottomSide),
            new int2(leftSide, bottomSide), new int2(rightSide, bottomSide),
            new int2(rightSide, bottomSide), new int2(rightSide, upperTopSide)
        };
        
        private readonly static int2[] lowerV = new int2[]
        {
            new int2(leftSide, lowerTopSide), new int2(middleSideX, bottomSide),
            new int2(middleSideX, bottomSide), new int2(rightSide, lowerTopSide)
        };

        // copied lowerV
        private readonly static int2[] upperV = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(middleSideX, bottomSide),
            new int2(middleSideX, bottomSide), new int2(rightSide, upperTopSide)
        };
        
        private readonly static int2[] lowerW = new int2[]
        {
            new int2(leftSide, bottomSide), new int2(leftSide, lowerTopSide),
            new int2(rightSide, bottomSide), new int2(rightSide, lowerTopSide),
            new int2(middleSideX, lowerTopSide), new int2(rightSide, bottomSide),
            new int2(middleSideX, lowerTopSide), new int2(leftSide, bottomSide),
        };

        // copied lowerW
        private readonly static int2[] upperW = new int2[]
        {
            new int2(leftSide, bottomSide), new int2(leftSide, upperTopSide),
            new int2(rightSide, bottomSide), new int2(rightSide, upperTopSide),
            new int2(middleSideX, upperTopSide), new int2(rightSide, bottomSide),
            new int2(middleSideX, upperTopSide), new int2(leftSide, bottomSide),
        };
        
        private readonly static int2[] lowerX = new int2[]
        {
            new int2(leftSide, bottomSide), new int2(middleSideX, lowerMiddleSideY),
            new int2(middleSideX, lowerMiddleSideY), new int2(rightSide, lowerTopSide),
            new int2(leftSide, lowerTopSide), new int2(middleSideX, lowerMiddleSideY),
            new int2(middleSideX, lowerMiddleSideY), new int2(rightSide, bottomSide)
        };

        // copied lowerX
        private readonly static int2[] upperX = new int2[]
        {
            new int2(leftSide, bottomSide), new int2(middleSideX, upperMiddleSideY),
            new int2(middleSideX, upperMiddleSideY), new int2(rightSide, upperTopSide),
            new int2(leftSide, upperTopSide), new int2(middleSideX, upperMiddleSideY),
            new int2(middleSideX, upperMiddleSideY), new int2(rightSide, bottomSide)
        };
        
        private readonly static int2[] lowerY = new int2[]
        {
            new int2(leftSide, lowerTopSide), new int2(leftSide, lowerMiddleSideY),
            new int2(leftSide, lowerMiddleSideY), new int2(rightSide, lowerMiddleSideY),
            new int2(rightSide, lowerMiddleSideY), new int2(rightSide, lowerTopSide),
            new int2(rightSide, lowerMiddleSideY), new int2(rightSide, bottomSide),
            new int2(rightSide, bottomSide), new int2(leftSide + 2, bottomSide),
            new int2(leftSide + 2, bottomSide), new int2(leftSide + 2, bottomSide + 1),
        };

        // copied lowerY
        private readonly static int2[] upperY = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(leftSide, upperMiddleSideY),
            new int2(leftSide, upperMiddleSideY), new int2(rightSide, upperMiddleSideY),
            new int2(rightSide, upperMiddleSideY), new int2(rightSide, upperTopSide),
            new int2(rightSide, upperMiddleSideY), new int2(rightSide, bottomSide),
            new int2(rightSide, bottomSide), new int2(leftSide + 2, bottomSide),
            new int2(leftSide + 2, bottomSide), new int2(leftSide + 2, bottomSide + 1),
        };
        
        private readonly static int2[] lowerZ = new int2[]
        {
            new int2(leftSide, lowerTopSide), new int2(rightSide, lowerTopSide),
            new int2(rightSide, lowerTopSide), new int2(leftSide, bottomSide),
            new int2(leftSide, bottomSide), new int2(rightSide, bottomSide)
        };

        // copied lowerZ
        private readonly static int2[] upperZ = new int2[]
        {
            new int2(leftSide, upperTopSide), new int2(rightSide, upperTopSide - 1),
            new int2(rightSide, upperTopSide - 1), new int2(leftSide, bottomSide + 1),
            new int2(leftSide, bottomSide + 1), new int2(rightSide, bottomSide)
        };

        private readonly static int2[] downArrow = new int2[]
        {
            new int2(leftSide, upperMiddleSideY), new int2(middleSideX, bottomSide),
            new int2(middleSideX, upperTopSide), new int2(middleSideX, bottomSide),
            new int2(rightSide, upperMiddleSideY), new int2(middleSideX, bottomSide)
        };
        private readonly static int2[] upArrow = new int2[]
        {
            new int2(leftSide, upperMiddleSideY), new int2(middleSideX, upperTopSide),
            new int2(middleSideX, upperTopSide), new int2(middleSideX, bottomSide),
            new int2(rightSide, upperMiddleSideY), new int2(middleSideX, upperTopSide)
        };
        
    }
}