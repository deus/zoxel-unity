using Unity.Entities;

namespace Zoxel
{
    //! Destroys an Entity after an event.
    public struct DestroyAfterEvent : IComponentData { }
}