using Unity.Entities;

namespace Zoxel
{
    //! Event for Entity being selected.
    public struct OnEntitySelected : IComponentData
    {
        public double timeSelected;

        public OnEntitySelected(double timeSelected)
        {
            this.timeSelected = timeSelected;
        }
    }
}