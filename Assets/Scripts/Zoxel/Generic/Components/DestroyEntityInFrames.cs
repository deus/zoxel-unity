using Unity.Entities;

namespace Zoxel
{
    //! Destroy Entity in Frames.
    public struct DestroyEntityInFrames : IComponentData
    {
        public byte count;

        public DestroyEntityInFrames(byte count)
        {
            this.count = count;
        }
    }
}