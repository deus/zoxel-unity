using Unity.Entities;
// For example, a characters corpse, something that is still there until removed

namespace Zoxel
{
    //! Signifies that an entity is dead. Can be revived or destroyed.
    public struct DeadEntity : IComponentData
    {
        public double deathTime;

        public DeadEntity(double deathTime)
        {
            this.deathTime = deathTime;
        }
    }
}