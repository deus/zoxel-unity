using Unity.Entities;

namespace Zoxel
{
    //! An event added onto a prefab entity, used for initializing data.
    public struct InitializeEntity : IComponentData { }
}