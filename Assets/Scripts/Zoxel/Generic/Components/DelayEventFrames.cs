using Unity.Entities;

namespace Zoxel
{
    //! Delays an event entity by an amount of frames.
    public struct DelayEventFrames : IComponentData
    {
        public byte count;
        public byte frameDelay;

        public DelayEventFrames(byte frameDelay)
        {
            this.count = 0;
            this.frameDelay = frameDelay;
        }
    }
}