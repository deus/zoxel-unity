using Unity.Entities;

namespace Zoxel
{
	//! Path that entity loaded with.
	public struct LoadedPath : IComponentData
	{
		public Text path;

		public LoadedPath(Text path)
		{
			this.path = path;
		}

		public void DisposeFinal()
		{
			path.DisposeFinal();
		}
	}
}