using Unity.Entities;

namespace Zoxel
{
    //! Generic event to load entity from file.
	public struct LoadEntity : IComponentData
	{
		public Text loadPath;

		public LoadEntity(string loadPath)
		{
			this.loadPath = new Text(loadPath);
		}

		public LoadEntity(Text loadPath)
		{
			this.loadPath = loadPath;
		}

		public void Dispose()
		{
			loadPath.Dispose();
		}
	}
}
   // public struct LoadEntity : IComponentData { }