using Unity.Entities;

namespace Zoxel
{
    //! An event that signifies an entity has started to die.
    /**
    *   Normally DeadEntity is added after this event.
    */
    public struct DyingEntity : IComponentData
    {
        public byte deathType;

        public DyingEntity(byte deathType)
        {
            this.deathType = deathType;
        }
    }
}