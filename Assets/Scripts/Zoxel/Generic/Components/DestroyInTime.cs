using Unity.Entities;

namespace Zoxel
{
    //! Destroys an Entity after a set amount of time.
    public struct DestroyEntityInTime : IComponentData
    {
        public double beginTime;
        public float lifeTime;

        public DestroyEntityInTime(double beginTime, float lifeTime)
        {
            this.beginTime = beginTime;
            this.lifeTime = lifeTime;
        }

        public bool HasFinished(double elapsedTime)
        {
            return (elapsedTime - beginTime >= lifeTime);
        }
    }
}