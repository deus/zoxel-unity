using Unity.Entities;

namespace Zoxel
{
	//! Saves a Entity to File.
	/**
	*	\todo Make a general saving system, that uses the generics to write the component as bytes to a file. Use this for settings.
	*/
	public struct SaveEntity : IComponentData { }
}