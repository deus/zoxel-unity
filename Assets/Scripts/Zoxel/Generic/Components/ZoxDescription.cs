using Unity.Entities;

namespace Zoxel
{
    //! A generic Text description used in many entities.
    public struct ZoxDescription : IComponentData
    {
        public Text description;

        public ZoxDescription(Text description)
        {
            this.description = description;
        }

        public ZoxDescription(string description)
        {
            this.description = new Text(description);
        }

        public void SetDescription(Text description)
        {
            Dispose();
            this.description = description;
        }

        public void Dispose()
        {
            if (description.Length > 0)
            {
                description.Dispose();
            }
        }
    }
}