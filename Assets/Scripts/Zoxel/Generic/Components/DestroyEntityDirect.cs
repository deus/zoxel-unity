using Unity.Entities;

namespace Zoxel
{
    //! Attached to entities to signal a quick death.
    /**
    *   When attached to an entity, a system knows to destroy the entity directly rather than adding a DestroyEntity event onto it.
    */
    public struct DestroyEntityDirect : IComponentData { }
}