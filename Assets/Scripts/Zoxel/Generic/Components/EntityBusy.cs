using Unity.Entities;

namespace Zoxel
{
    //! If an Entity is busy.
    /**
    *   Added to entities during events, so other modules may use the information.
    */
    public struct EntityBusy : IComponentData { }
}