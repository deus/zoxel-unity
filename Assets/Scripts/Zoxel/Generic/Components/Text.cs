using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel
{
    //! Blitable text for our components! Stores the characters in bytes.
    public struct Text // : IEquatable<Text>
    {
        public BlitableArray<byte> textData;

        public Text(string newText)
        {
            textData = new BlitableArray<byte>(); // 0, Allocator.Persistent);
            byte updated;
            ConvertToBytes(newText, out updated);
        }

        public Text(int count)
        {
            textData = new BlitableArray<byte>(count, Allocator.Persistent);
        }

        public void DisposeFinal()
        {
            textData.DisposeFinal();
        }

        public void Dispose()
        {
            textData.Dispose();
        }

        public Text Clone()
        {
            var text = new Text();
            text.textData = new BlitableArray<byte>(this.textData.Length, Allocator.Persistent);
            for (int i = 0; i < this.textData.Length; i++)
            {
                //if (this.textData[i] != null)
                text.textData[i] = this.textData[i];
            }
            return text;
        }

        //! Checks if contains another text.
        public bool Contains(in Text text)
        {
            if (text.Length == 0)
            {
                return true;
            }
            var j = -1;
            for (int i = 0; i < this.Length; i++)
            {
                if (j == -1)
                {
                    if (this[i] == text[0])
                    {
                        // Starts reading
                        j = 1;
                        if (j >= text.Length)
                        {
                            return true;
                        }
                        // UnityEngine.Debug.LogError("Started checking at " + i + " " + this[i]);
                    }
                }
                else
                {
                    if (this[i] == text[j])
                    {
                        // Keeps reading
                        j++;
                        if (j >= text.Length)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        // UnityEngine.Debug.LogError("Ending checking at " + i + " " + this[i] + " : " + text[j]);
                        // End reading, failed to find ending.
                        j = -1;
                    }
                }
            }
            return false;
        }

		public byte this[int index]
		{
			get
			{
                return textData[index];
			}
			set
			{
                textData[index] = value;
			}
		}

        public int Length
        {
            get
            { 
                return textData.Length;
            }
        }

        public override string ToString()
        {
            return BytesToString(in textData);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode(); //1371622046 + EqualityComparer<Mesh>.Default.GetHashCode(mesh);
        }

        public override bool Equals(object obj)
        {
            return ToString() == obj.ToString();
        }

        public static bool operator ==(Text a, Text b)
        {
            if (a.textData.Length != b.textData.Length)
            {
                return false;
            }
            for (int i = 0; i < a.textData.Length; i++)
            {
                if (a.textData[i] != b.textData[i])
                {
                    return false;
                }
            }
            return true;
        }

        public static bool operator !=(Text a, Text b)
        {
            if (a.textData.Length != b.textData.Length)
            {
                return true;
            }
            for (int i = 0; i < a.textData.Length; i++)
            {
                if (a.textData[i] != b.textData[i])
                {
                    return true;
                }
            }
            return false;
        }

        public int GetSize()
        {
            return textData.Length;
        }

        public void InsertChar(char newChar)
        {
            InsertChar(ConvertCharToByte(newChar));
        }
        
        public void InsertChar(byte newChar)
        {
            var newText = new BlitableArray<byte>(textData.Length + 1, Allocator.Persistent);
            newText[0] = newChar; // Text.ConvertCharToByte(newChar);
            for (int i = 0; i < textData.Length; i++)
            {
                newText[i] = textData[i + 1];
            }
            textData.Dispose();
            textData = newText;
        }

        /*public static Text IntegerToText(int value)
        {
            var numberLength = ((value == 0) ? 1 : ((int)math.floor(math.log10(math.abs(value))) + 1));
            var text = new Text();
            text.textData = new BlitableArray<byte>(numberLength, Allocator.Persistent);
            int digitMultiplier = 1;
            for (int i = numberLength - 1; i >= 0; i--)
            {
                text.textData[i] = (byte) ((value / digitMultiplier) % 10);
                digitMultiplier *= 10;
            }
            UnityEngine.Debug.LogError("Integer to text: " + value + " to " + text.ToString() + " and length " + text.Length
                + " ? " + text.textData[0]);
            return text;
        }*/

        public static Text IntegerToText(int value)
        {
            return IntegerToText(value, Allocator.Persistent);
        }

        public static Text IntegerToText(int value, Allocator allocator)
        {
            var valueLength = ((value == 0) ? 1 : ((int) math.floor(math.log10(math.abs(value))) + 1));
            var isNegative = value < 0;
            if (isNegative)
            {
                valueLength++;
                value = - value;
            }
            var data = new BlitableArray<byte>(valueLength, allocator);
            var startIndex = 0;
            if (isNegative)
            {
                data[0] = ConvertCharToByte('-');
                startIndex = 1;
            }
            int digitMultiplier = 1;
            for (int i = startIndex; i < valueLength; i++)
            {
                var reversedIndex = valueLength - 1 - i + startIndex;
                data[reversedIndex] = (byte) ((value / digitMultiplier) % 10);
                digitMultiplier *= 10;
            }
            var text = new Text();
            text.textData = data;
            //UnityEngine.Debug.LogError("Integer to text: " + value + " to " + text.ToString() + " and length " + text.Length
            //    + " ? " + text.textData[0]);
            return text;
        }

        public void AddInteger(int value)
        {
            var valueLength = ((value == 0) ? 1 : ((int)math.floor(math.log10(math.abs(value))) + 1));
            var isNegative = value < 0;
            if (isNegative)
            {
                valueLength++;
                value = - value;
            }
            var newBytes = new NativeArray<byte>(valueLength, Allocator.Temp);
            var startIndex = 0;
            if (isNegative)
            {
                newBytes[0] = ConvertCharToByte('-');
                startIndex = 1;
            }
            int digitMultiplier = 1;
            for (int i = startIndex; i < valueLength; i++)
            {
                newBytes[i] = (byte) ((value / digitMultiplier) % 10);
                digitMultiplier *= 10;
            }
            var reversedBytes = new NativeArray<byte>(valueLength, Allocator.Temp);
            if (isNegative)
            {
                reversedBytes[0] = newBytes[0];
            }
            for (int i = startIndex; i < valueLength; i++)
            {
                var reverseIndex = valueLength - 1 - i + startIndex;    // max index - current one + startIndex again
                reversedBytes[i] = newBytes[reverseIndex];
            }
            // Add these bytes onto
            var newText = new BlitableArray<byte>(textData.Length + reversedBytes.Length, Allocator.Persistent);
            for (int i = 0; i < textData.Length; i++)
            {
                newText[i] = textData[i];
            }
            for (int i = textData.Length; i < newText.Length; i++)
            {
                newText[i] = reversedBytes[i - textData.Length];
            }
            textData.Dispose();
            textData = newText;
            newBytes.Dispose();
            reversedBytes.Dispose();
        }

        public void AddChar(char newChar)
        {
            AddChar(ConvertCharToByte(newChar));
        }

        public void AddChar(byte newChar)
        {
            var newText = new BlitableArray<byte>(textData.Length + 1, Allocator.Persistent);
            for (int i = 0; i < textData.Length; i++)
            {
                newText[i] = textData[i];
            }
            newText[textData.Length] = newChar; // Text.ConvertCharToByte(newChar);
            textData.Dispose();
            textData = newText;
        }

        public void AddText(in Text addText)
        {
            var textData2 = new BlitableArray<byte>(textData.Length + addText.Length, Allocator.Persistent, textData);
            /*for (int i = 0; i < textData.Length; i++)
            {
                newText[i] = textData[i];
            }*/
            // textData2.CopyTo(textData.Length, addText);
            //! \todo Use CopyTo function, memory operations instead of setting each individually.
            for (int i = textData.Length; i < textData2.Length; i++)
            {
                textData2[i] = addText[i - textData.Length];
            }
            textData.Dispose();
            textData = textData2;
        }

        public void RemoveLastLine()
        {
            var lastLineIndex = 0;
            for (int i = textData.Length - 1; i >= 0; i--)
            {
                if (textData[i] == 254)
                {
                    lastLineIndex = i;
                    break;
                }
            }
            var newTextData = new BlitableArray<byte>(lastLineIndex, Allocator.Persistent);
            for (int i = 0; i < lastLineIndex; i++)
            {
                newTextData[i] = textData[i];
            }
            Dispose();
            textData = newTextData;
        }

        public int GetLineCount()
        {
            var newLinesCount = 1;
            for (int i = 0; i < textData.Length; i++)
            {
                if (textData[i] == 254)
                {
                    newLinesCount++;
                }
            }
            return newLinesCount;
        }

        public void Initialize(int count)
        {
            Dispose();
            textData = new BlitableArray<byte>(count, Allocator.Persistent);
        }

        public void SetText(string text)
        {
            byte updated;
            ConvertToBytes(text, out updated);
        }

        public void SetText(string text, out byte updated)
        {
            ConvertToBytes(text, out updated);
        }

        public void SetTextArray(BlitableArray<byte> textArray)
        {
            Dispose();
            textData = textArray;
        }


        public int GetTotalLines()
        {
            var sizeY = 1;
            for (int i = 0; i < textData.Length - 1; i++)
            {
                if (textData[i] == 254)
                {
                    sizeY++;
                }
            }
            return sizeY;
        }
        public int GetSizeX(int lineCount)
        {
            int largestX = 0;
            int startX = 0;
            int thisLineCount = 0;
            for (int i = 0; i < textData.Length; i++)
            {
                if (textData[i] == 254)
                {
                    if (thisLineCount == lineCount)
                    {
                        largestX = i - startX; // - 1;
                    }
                    startX = i + 1;
                    thisLineCount++;
                }
            }
            if (thisLineCount == lineCount)
            {
                largestX = textData.Length - startX;
            }
            //UnityEngine.Debug.LogError("X(linCount): " + largestX);
            return largestX;
        }

        public int GetSizeX()
        {
            int largestX = 0;
            int startX = 0;
            for (int i = 0; i < textData.Length; i++)
            {
                if (textData[i] == 254)
                {
                    var sizeX = i - startX;//  - 1; //  - 1
                    if (sizeX > largestX)
                    {
                        largestX = sizeX;
                    }
                    startX = i + 1;
                }
            }
            var sizeX2 = textData.Length - startX;   //  - 1
            if (sizeX2 > largestX)
            {
                largestX = sizeX2;
            }
            //UnityEngine.Debug.LogError("X: " + largestX);
            return largestX;
        }

        public BlitableArray<byte> SubString(int endIndex)
        {
            var culledArray = new BlitableArray<byte>(endIndex, Allocator.Persistent);
            //Debug.LogError("Created SubString: " + culledArray.Length);
            for (int i = 0; i < culledArray.Length; i++)
            {
                culledArray[i] = textData[i];
            }
            return culledArray;
        }

        public static string BytesToString(in BlitableArray<byte> textData)
        {
            // string output = "";
            //var aInt = (byte)'a';
            //var zInt = (byte)'z';
            // var stringBuilder = new System.Text.StringBuilder("", textData.Length);
            var length = textData.Length;
            var stringBuilder = new System.Text.StringBuilder(length);
            for (int i = 0; i < length; i++)
            {
                var characterByte = textData[i];
                if (characterByte == 255)
                {
                    // output += ' ';
                    stringBuilder.Append(' ');
                }
                else if (characterByte == 254)
                {
                    // output += '\n';
                    stringBuilder.Append('\n');
                }
                else if (characterByte < charactersList.Length)    // characterByte >= 0 && 
                {
                    // output += charactersList[characterByte];
                    stringBuilder.Append(charactersList[characterByte]);
                    //fontIndex = (byte)((singleDigit - aInt) + numbersCount);
                }
                /*else
                {
                    // UnityEngine.Debug.LogWarning("Character byte out of bounds: " + characterByte);
                    // output += ' ';
                    stringBuilder.Append(' ');
                }*/
            }
            return stringBuilder.ToString(); // output;
        }

        //! \todo Usehashmap char -> byte
        public static byte ConvertCharToByte(char textCharacter)
        {
            const int numbersLength = 10;
            const int lowerCaseLettersLength = 26;
            const int bigAInt = (int)'A';
            const int bigZInt = (int)'Z';
            const int aInt = (int)'a';
            const int zInt = (int)'z';
            var asciiInt = (int) textCharacter;
            byte fontIndex = 0;
            /*if (textCharacter == ' ')
            {
                // byte is 255 for spaces!
                fontIndex = (byte) 255;
            }
            else */
            if (textCharacter == '\n')
            {
                fontIndex = (byte) 254;
            }
            else if (asciiInt >= aInt && asciiInt <= zInt)
            {
                fontIndex = (byte)((asciiInt - aInt) + numbersLength);
            }
            else if (asciiInt >= bigAInt && asciiInt <= bigZInt)
            {
                fontIndex = (byte)((asciiInt - bigAInt) + numbersLength + lowerCaseLettersLength);
            }
            else
            {
                bool notFound = true;
                // check numbers
                for (int j = 0; j < 10; j++)
                {
                    if (textCharacter == charactersList[j])
                    {
                        fontIndex = (byte)(j);
                        notFound = false;
                        break;
                    }
                }
                if (notFound)
                {
                    for (int j = 0; j < specialCharactersList.Length; j++)
                    {
                        if (textCharacter == specialCharactersList[j])
                        {
                            fontIndex = (byte)(j + numbersLength + lowerCaseLettersLength + lowerCaseLettersLength);
                            notFound = false;
                            break;
                        }
                    }
                }
            }
            return fontIndex;
        }

        // first 10 are digits
        // next 26 are lower case
        // next 26 are upper case
        public void ConvertToBytes(string text, out byte updated)
        {
            if (text == null)
            {
                updated = 0;
                return;
            }
            // if the same, return
            if (textData.Length == text.Length)
            {
                bool isSame = true;
                for (int i = 0; i < textData.Length; i++)
                {
                    var textCharacter = ConvertCharToByte(text[i]);
                    if (textData[i] != textCharacter)
                    {
                        isSame = false;
                        break;
                    }
                }
                if (isSame)
                {
                    updated = 0;
                    return;
                }
            }
            updated = 1;
            var bytes = new BlitableArray<byte>(text.Length, Allocator.Persistent);
            for (int i = 0; i < text.Length; i++)
            {
                bytes[i] = ConvertCharToByte(text[i]);
            }
            textData.Dispose();
            textData = bytes;
            updated = 1;
        }
        
        public static readonly char[] charactersList = new char[]
        {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 
            'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 
            'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 
            'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 
            'U', 'V', 'W', 'X', 'Y', 'Z',
            // special characters
            ' ', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '+',
            '.', ':', ',', '/', '\'', '[', ']', '?', '<', '>', ';', '↓', '↑'
        };
        
        public static readonly char[] specialCharactersList = new char[]
        {
            ' ', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '+',
            '.', ':', ',', '/', '\'', '[', ']', '?', '<', '>', ';', '↓', '↑'
        };
    }
}