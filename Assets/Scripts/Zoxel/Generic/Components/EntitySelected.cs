using Unity.Entities;

namespace Zoxel
{
    //! A tag for when entity is selected.
    public struct EntitySelected : IComponentData { }
}