using Unity.Entities;

namespace Zoxel
{
    //! An event used for reviving a dead entity.
    public struct RevivingEntity : IComponentData { }
}