using Unity.Entities;

namespace Zoxel
{
    //! The main way to clean up entities. Since I use alot of unsafe memory pointers.
    public struct DestroyEntity : IComponentData { }
}