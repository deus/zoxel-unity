using Unity.Entities;

namespace Zoxel
{
    //! An event used when spawning an entity.
    public struct SpawnEntity : IComponentData { }
}