using Unity.Entities;

namespace Zoxel
{
    //! Stores the time an entity was born.
    public struct BornTime : IComponentData
    {
        public double time;

        public BornTime(double time)
        {
            this.time = time;
        }
    }
}