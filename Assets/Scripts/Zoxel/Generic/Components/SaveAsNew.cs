using Unity.Entities;

namespace Zoxel
{
	//! Saves as a new File.
	public struct SaveAsNew : IComponentData { }
}