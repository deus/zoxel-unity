using Unity.Entities;

namespace Zoxel
{
    //! An index of a data item.
    public struct UserDataIndex : IComponentData
    {
        public int index;

        public UserDataIndex(int index)
        {
            this.index = index;
        }
    }
}