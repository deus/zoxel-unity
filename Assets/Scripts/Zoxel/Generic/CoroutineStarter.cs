using UnityEngine;

namespace Zoxel
{
    //! Runs coroutines from static classes.
    public partial class CoroutineStarter : MonoBehaviour
    {
        public static CoroutineStarter instance;

        public void Awake()
        {
            instance = this;
        }
    }
}