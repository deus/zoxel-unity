﻿using Unity.Entities;

namespace Zoxel // .Skeletons
{
    //! Is a tag for a Skeleton entity.
    public struct Skeleton : IComponentData { }
}