using Unity.Entities;

namespace Zoxel // .Characters
{
    //! A Character that is controlled by a Player.
    public struct PlayerCharacter : IComponentData { }
}