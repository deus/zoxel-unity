using Unity.Entities;

namespace Zoxel
{
    //! A character being edited by the CharacterMaker.
    public struct CharacterMakerCharacter : IComponentData { }
}