using Unity.Entities;

namespace Zoxel
{
    //! A non playable Character tag.
    public struct NPCCharacter : IComponentData { }
}