using Unity.Entities;

namespace Zoxel
{
    //! A player normally has a Controller and interacts with the Realm.
    public struct Player : IComponentData { }
}