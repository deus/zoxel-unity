﻿using Unity.Entities;

namespace Zoxel // .Bullets
{
    //! Bullets are thrown towards an enemy or friend.
    public struct Bullet : IComponentData
    {
        public float damage;

        public Bullet(float damage)
        {
            this.damage = damage;
        }
    }
}