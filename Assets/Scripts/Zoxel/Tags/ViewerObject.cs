using Unity.Entities;

namespace Zoxel
{
    //! An object within a ViewerUI.
    public struct ViewerObject : IComponentData { }
}