using Unity.Entities;
// todo: replace with DestroyEntityInTime component - just use this for tags

namespace Zoxel
{
    //! An entity that has been summoned by another.
    /**
    *   \todo remove the timed stuff from here.
    */
    public struct SummonedEntity : IComponentData
    {
        public double timeBegun;
        public double timeAlive;

        public SummonedEntity(double timeBegun, double timeAlive)
        {
            this.timeBegun = timeBegun;
            this.timeAlive = timeAlive;
        }
    }
}