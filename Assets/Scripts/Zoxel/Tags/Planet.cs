using Unity.Entities;
using Unity.Mathematics;
// TODO: Set first town in center
//      new town must be 100 voxels away minimum
//      Generate path between town gates
//      Generate Lines for the town wall and vary the shape
//      For new buildings, check doesn't overlap over buildings, check it doesn't overlap side of town

namespace Zoxel // .Voxels
{
    //! A planet is what holds voxels and characters.
    public struct Planet : IComponentData
    {
        public int radius;
        public int voxelDimensions;

        public Planet(int radius, int voxelDimensions)
        {
            this.radius = radius;
            this.voxelDimensions = voxelDimensions;
        }

        public int GetChunkRadius()
        {
            return (int) math.ceil(radius / (float) voxelDimensions);
        }
    }
}