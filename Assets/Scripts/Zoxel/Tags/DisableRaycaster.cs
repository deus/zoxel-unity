using Unity.Entities;
// Move to Zoxel.Raycasting soon

namespace Zoxel // .Voxels.Interaction
{
    //! When mouse movement is disabled on a PlayerCharacter.
    public struct DisableRaycaster : IComponentData { }
}