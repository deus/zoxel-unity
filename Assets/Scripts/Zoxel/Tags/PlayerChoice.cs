using Unity.Entities;
// player can chose this data in UI

namespace Zoxel
{
    //! A specific data entity that can be chosen in CharacterMaker for a player.
    public struct PlayerChoice : IComponentData { }
}