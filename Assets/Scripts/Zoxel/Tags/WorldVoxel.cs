using Unity.Entities;

namespace Zoxel
{
    //! A voxel model in the world.
    public struct WorldVoxel : IComponentData { }
}