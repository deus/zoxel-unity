﻿using Unity.Entities;

namespace Zoxel
{
    //! Each realm contains all the games resources.
    public struct Realm : IComponentData { }
}