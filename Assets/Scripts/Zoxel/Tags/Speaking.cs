using Unity.Entities;

namespace Zoxel // .Dialogue
{
    //! When a character is speaking to another.
    public struct Speaking : IComponentData { }
}