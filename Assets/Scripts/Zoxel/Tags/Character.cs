﻿using Unity.Entities;

namespace Zoxel // .Characters
{
    //! A character that inhabits a place in our zoxel voxiverse.
    public struct Character : IComponentData { }
}