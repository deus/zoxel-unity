﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel // .Portals
{
    //! A rift in the Realm that tears through physics.
    public struct Portal : IComponentData
    {
        public Entity linkedPortal;
    }
}
