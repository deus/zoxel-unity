using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;

namespace Zoxel.AI
{
    //! Every x seconds it will wander or wait.
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class SpawnPointInitializeSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;
        private EntityQuery processQuery2;
        private EntityQuery voxesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            chunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkPosition>());
            voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxScale>());
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (!processQuery.IsEmpty)
            {
                var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
                Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
                var chunks = GetComponentLookup<Chunk>(true);
                var chunkPositions = GetComponentLookup<ChunkPosition>(true);
                chunkEntities.Dispose();
                Dependency = Entities
                    .WithStoreEntityQueryInField(ref processQuery)
                    .WithAll<InitializeEntity>() // , SaveSpawnPoint>()
                    .ForEach((ref SpawnPoint spawnPoint, in GravityQuadrant gravityQuadrant, in ChunkLink chunkLink) =>
                {
                    if (!chunkPositions.HasComponent(chunkLink.chunk))
                    {
                        return;
                    }
                    var chunkPosition = chunkPositions[chunkLink.chunk];
                    var voxelDimensions = chunks[chunkLink.chunk].voxelDimensions;
                    var chunkVoxelPosition = chunkPosition.GetVoxelPosition(voxelDimensions) + voxelDimensions / 2;
                    spawnPoint.position = chunkVoxelPosition;
                    spawnPoint.rotation = gravityQuadrant.quadrant;
                })  .WithReadOnly(chunks)
                    .WithReadOnly(chunkPositions)
                    .ScheduleParallel(Dependency);
            }
            if (!processQuery2.IsEmpty)
            {
                SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<SetSpawnPoint>(processQuery2);
                var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
                Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
                var voxScales = GetComponentLookup<VoxScale>(true);
                voxEntities.Dispose();
                Dependency = Entities
                    .WithStoreEntityQueryInField(ref processQuery2)
                    .WithAll<InitializeEntity>() // , SaveSpawnPoint>()
                    .ForEach((ref SpawnPoint spawnPoint, in GravityQuadrant gravityQuadrant, in SetSpawnPoint setSpawnPoint, in VoxLink voxLink) =>
                {
                    if (!voxScales.HasComponent(voxLink.vox))
                    {
                        return;
                    }
                    var voxScale = voxScales[voxLink.vox].scale;
                    spawnPoint.position = VoxelUtilities.GetVoxelPosition(setSpawnPoint.position, voxScale);
                    spawnPoint.rotation = gravityQuadrant.quadrant;
                })  .WithReadOnly(voxScales)
                    .ScheduleParallel(Dependency);
            }
        }
    }
}