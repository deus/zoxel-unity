using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.AI.Authoring;
using Zoxel.Transforms;
using Zoxel.Voxels;

namespace Zoxel.AI
{
    //! Every x seconds it will wander or wait.
    [UpdateInGroup(typeof(AISystemGroup))]
    public partial class WanderDebugSystem : SystemBase
	{
        protected override void OnCreate()
        {
            RequireForUpdate<AISettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var aiSettings = GetSingleton<AISettings>();
            if (!aiSettings.isDebugWander)
            {
                return;
            }
            Entities.ForEach((in SpawnPoint spawnPoint, in Translation translation) =>
            {
                UnityEngine.Debug.DrawLine(translation.Value, spawnPoint.realPosition, UnityEngine.Color.red);
            }).WithoutBurst().Run();
            Entities.ForEach((in Translation translation, in DynamicChunkLink dynamicChunkLink) =>
            {
                var voxelDimensions = new int3(16, 16, 16);
				var voxelPosition = VoxelUtilities.GetChunkVoxelPosition(dynamicChunkLink.chunkPosition, voxelDimensions) + voxelDimensions / 2;
                UnityEngine.Debug.DrawLine(translation.Value, voxelPosition.ToFloat3() + new float3(1,1,1) / 2f, UnityEngine.Color.cyan);
            }).WithoutBurst().Run();
        }
    }
}