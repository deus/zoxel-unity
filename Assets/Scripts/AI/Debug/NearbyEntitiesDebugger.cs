using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Players;
using Zoxel.AI;
using Zoxel.Voxels;

namespace Zoxel
{
    //! Debugs the raycasting with the UI
    public partial class NearbyEntitiesDebugger : MonoBehaviour
    {
        public static NearbyEntitiesDebugger instance;
        private int fontSize = 26;
        private UnityEngine.Color fontColor = UnityEngine.Color.green;
        public bool isDebugLines;
        public bool isDebugUI;

        void Awake()
        {
            instance = this;
        }

        public void OnGUI()
        {
            if (isDebugUI)
            {
                GUI.skin.label.fontSize = fontSize;
                GUI.color = fontColor;
                DebugRaycasting();
            }
        }

        public void Update()
        {
            if (isDebugLines)
            {
                DebugLines();
            }
        }

        public EntityManager EntityManager
        {
            get
            { 
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        public void DebugRaycasting()
        {
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            if (playerLinks.players.Length > 0)
            {
                var player = playerLinks.players[0];
                DebugUI(player);
            }
        }

        private void DebugUI(Entity playerEntity)
        {
            var characterEntity = EntityManager.GetComponentData<CharacterLink>(playerEntity).character;
            GUILayout.Label("Debugging Raycasting of Player [" + characterEntity.Index + "]");
            if (!EntityManager.HasComponent<NearbyEntities>(characterEntity))
            {
                GUILayout.Label("Player Has no NearbyEntities Component.");
            }
            else
            {
                var position = EntityManager.GetComponentData<Translation>(characterEntity);
                var chunkLink = EntityManager.GetComponentData<ChunkLink>(characterEntity);
                var nearbyEntities = EntityManager.GetComponentData<NearbyEntities>(characterEntity);
                GUILayout.Label("Player at [" + position.Value + "] Has NearbyEntities: " + nearbyEntities.Length
                    + " with chunk: " + chunkLink.chunk.Index + ", totalNearby: " + nearbyEntities.totalNearby);
                var target = EntityManager.GetComponentData<Target>(characterEntity);
                GUILayout.Label("   Target: " + target.target.Index);
                for (int i = 0; i < nearbyEntities.Length; i++)
                {
                    var nearbyCharacter = nearbyEntities.characters[i];
                    // var position = EntityManager.GetComponentData<Translation>(nearbyCharacter.character);
                    UnityEngine.GUILayout.Label("       character [" + i + "] index [" + nearbyCharacter.character.Index + "] dist: " + nearbyCharacter.distance);
                    UnityEngine.Debug.DrawLine(position.Value, nearbyCharacter.position, UnityEngine.Color.white);
                }
            }
        }

        public void DebugLines()
        {
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            if (playerLinks.players.Length > 0)
            {
                var player = playerLinks.players[0];
                DebugLines2(player);
            }
        }

        private void DebugLines2(Entity playerEntity)
        {
            var characterEntity = EntityManager.GetComponentData<CharacterLink>(playerEntity).character;
            if (EntityManager.HasComponent<NearbyEntities>(characterEntity))
            {
                var position = EntityManager.GetComponentData<Translation>(characterEntity);
                var chunkLink = EntityManager.GetComponentData<ChunkLink>(characterEntity);
                var nearbyEntities = EntityManager.GetComponentData<NearbyEntities>(characterEntity);
                var target = EntityManager.GetComponentData<Target>(characterEntity);
                for (int i = 0; i < nearbyEntities.Length; i++)
                {
                    var nearbyCharacter = nearbyEntities.characters[i];
                    UnityEngine.Debug.DrawLine(position.Value, nearbyCharacter.position, UnityEngine.Color.white, 0.01f);
                }
            }
        }
    }
}