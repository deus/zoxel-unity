﻿using System;

namespace Zoxel.AI
{
    [Serializable]
    public struct SeekData
    {
        public float attackRange;
        public float maintainRange;
        public float seekRange;
        public float seekCooldown;
    }
}