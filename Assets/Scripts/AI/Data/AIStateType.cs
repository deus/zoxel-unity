namespace Zoxel.AI
{
    //! A state of AI for an NPC.
    public static class AIStateType
    {
        public const byte None = 0;
        public const byte Idle = 1;
        public const byte Wander = 2;
        public const byte Follow = 3;
        public const byte Flee = 4;
        public const byte Attack = 5;
        public const byte Gaurd = 6;
        public const byte MoveToPoint = 7;
    }
}