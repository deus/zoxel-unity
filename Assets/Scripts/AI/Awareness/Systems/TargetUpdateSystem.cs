using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Clans;
using Zoxel.Movement;
using Zoxel.Movement.Authoring;

namespace Zoxel.AI
{
    //! Dynamically updates target information for other systems.
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class TargetUpdateSystem : SystemBase
    {
        private EntityQuery characterQuery;

        protected override void OnCreate()
        {
            characterQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.ReadOnly<ClanLink>(), 
                ComponentType.ReadOnly<Translation>());
            RequireForUpdate<PhysicsSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var physicsSettings = GetSingleton<PhysicsSettings>();
            var maxChaseDistance = physicsSettings.maxChaseDistance; // 14;
            var characterEntities = characterQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var positions = GetComponentLookup<Translation>(true);
            characterEntities.Dispose();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .ForEach((ref Target target, in Translation position, in Rotation rotation, in GravityQuadrant gravityQuadrant) =>
            {
                if (target.target.Index == 0)
                {
                    return;
                }
                if (!positions.HasComponent(target.target)) //  && !HasComponent<DeadEntity>(target.target))
                {
                    target.target = new Entity();
                    target.targetAngle = 0;
                    return;
                }
                target.targetPosition = positions[target.target].Value;
                var targetPosition = target.targetPosition;
                target.targetDistance = math.distance(targetPosition, position.Value);
                // targetPosition.y = position.Value.y;
                var normalBetween = math.normalizesafe(targetPosition - position.Value);
                var targetRotation = quaternion.LookRotationSafe(normalBetween, gravityQuadrant.GetUpDirection());
                var rotationBetween = (math.mul(math.inverse(rotation.Value), targetRotation));    
                var angle = CalculateAngle(rotation.Value, targetRotation);
                target.targetAngle = angle / 180f; // euler.y;
                if (target.targetDistance >= maxChaseDistance)    
                {
                    target.target = new Entity();
                }
            })  .WithReadOnly(positions)
                .ScheduleParallel(Dependency);
        }

        // Note: You can use the sign to determine if on left or right of rotation
        public static float CalculateAngle(quaternion a, quaternion b)
        {
            var f = math.dot(a, b);
            /*if (f < 0)
            {
                return -math.acos (math.min (math.abs (f), 1f)) * 2f * 57.29578f;
            }*/
            return math.acos (math.min (math.abs (f), 1f)) * 2f * 57.29578f;
        }
    }
}
                // var targetRotation = quaternion.LookRotationSafe(normalBetween, gravityQuadrant.rotation.ToEulerSlow()); // gravityQuadrant.GetUpDirection());
                //   math.up()); 
                // now subtract the needed angle
                // math.normalize
                // get angle X
                //var euler = math.euler();
                //var heldRotation = quaternion.EulerXYZ(new float3(90, 0, 0) * degreesToRadians);
                // var euler = rotationBetween.EulerXYZ();
                // var upAngle = quaternion.EulerXYZ(new float3(0, 90, 0) * degreesToRadians);
                // var upAngle = quaternion.EulerXYZ(new float3(0, 90, 0) * degreesToRadians);
                // float angle = UnityEngine.Quaternion.Angle(rotationBetween, upAngle);
                // var angle = CalculateAngle(rotationBetween, upAngle);
                //var euler = new UnityEngine.Quaternion(rotationBetween.value.x, rotationBetween.value.y, rotationBetween.value.z, rotationBetween.value.w).ToEulerAngles();
                //var euler = math.euler();
                //var euler = new UnityEngine.Quaternion(rotationBetween.value.x, rotationBetween.value.y, rotationBetween.value.z, rotationBetween.value.w).ToEulerAngles();
                    //target.targetRotation = euler.y;

                // var euler = GetQuaternionEulerAngles(rotationBetween); // quaternion.EulerXYZ(rotationBetween * degreesToRadians);
                //euler = UnityEngine.Quaternion.Normalize(euler);
                //target.targetRotation = euler.eulerAngles.y; // ToEulerAngles().y;
                /*if (gravityQuadrant.quadrant == PlanetSide.Down || gravityQuadrant.quadrant == PlanetSide.Up)
                {
                    target.targetRotation = euler.y;
                }
                else if (gravityQuadrant.quadrant == PlanetSide.Left || gravityQuadrant.quadrant == PlanetSide.Right)
                {
                    target.targetRotation = euler.x;
                }
                else if (gravityQuadrant.quadrant == PlanetSide.Backward || gravityQuadrant.quadrant == PlanetSide.Forward)
                {
                    target.targetRotation = euler.z;
                }*/
                // target.targetDistanceXZ = math.distance(targetPosition, position.Value);


        /*public static float3 GetQuaternionEulerAngles(quaternion rot)
        {
            float4 q1 = rot.value;
            float sqw = q1.w * q1.w;
            float sqx = q1.x * q1.x;
            float sqy = q1.y * q1.y;
            float sqz = q1.z * q1.z;
            float unit = sqx + sqy + sqz + sqw; // if normalised is one, otherwise is correction factor
            float test = q1.x * q1.w - q1.y * q1.z;
            float3 v;
        
            if (test > 0.4995f * unit)
            { // singularity at north pole
                v.y = 2f * math.atan2(q1.y, q1.x);
                v.x = math.PI / 2f;
                v.z = 0;
                return NormalizeAngles(v);
            }
            if (test < -0.4995f * unit)
            { // singularity at south pole
                v.y = -2f * math.atan2(q1.y, q1.x);
                v.x = -math.PI / 2;
                v.z = 0;
                return NormalizeAngles(v);
            }
        
            rot = new quaternion(q1.w, q1.z, q1.x, q1.y);
            v.y = math.atan2(2f * rot.value.x * rot.value.w + 2f * rot.value.y * rot.value.z, 1 - 2f * (rot.value.z * rot.value.z + rot.value.w * rot.value.w));     // Yaw
            v.x = math.asin(2f * (rot.value.x * rot.value.z - rot.value.w * rot.value.y));                             // Pitch
            v.z = math.atan2(2f * rot.value.x * rot.value.y + 2f * rot.value.z * rot.value.w, 1 - 2f * (rot.value.y * rot.value.y + rot.value.z * rot.value.z));      // Roll
            return NormalizeAngles(v);
        }
        
        static float3 NormalizeAngles(float3 angles)
        {
            angles.x = NormalizeAngle(angles.x);
            angles.y = NormalizeAngle(angles.y);
            angles.z = NormalizeAngle(angles.z);
            return angles;
        }
        
        static float NormalizeAngle(float angle)
        {
            while (angle > math.PI * 2f)
                angle -= math.PI * 2f;
            while (angle < 0)
                angle += math.PI * 2f;
            return angle;
        }*/