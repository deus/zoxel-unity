﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;

namespace Zoxel.AI
{
    //! Adds Separation forces onto characters.
    /**
    *   Bases forces on nearbyb enemies.
    *   \todo Base upward force removal on gravity quadrant.
    */
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class SeparationSystem : SystemBase
    {
        public static float distanceToAvoid = 1f;
        public static float avoidanceForce = 4f;

        [BurstCompile]
        protected override void OnUpdate()
        {
            var distanceToAvoid = SeparationSystem.distanceToAvoid;
            var avoidanceForce = SeparationSystem.avoidanceForce;
            Entities.ForEach((Entity e, ref BodyForce bodyForce, in NearbyEntities nearbyEntities, in Translation position, in GravityQuadrant gravityQuadrant) =>
            {
                for (int i = 0; i < nearbyEntities.count; i++)
                {
                    var nearbyEntity = nearbyEntities.characters[i];
                    /*if (nearbyEntity.character == e)
                    {
                        UnityEngine.Debug.LogError("NearbyEntity is this.");
                        continue;
                    }*/
                    var otherPosition = nearbyEntity.position;
                    float distanceTo = nearbyEntity.distance; // math.distance(position.Value, otherPosition);
                    if (distanceTo < distanceToAvoid)
                    {
                        // add force away from it
                        var normalBetween = math.normalizesafe(position.Value - otherPosition);
                        var newForce = avoidanceForce * (distanceToAvoid - distanceTo) * normalBetween;
                        gravityQuadrant.ZeroGravityAxis(ref newForce);
                        bodyForce.acceleration += newForce;
                        //UnityEngine.Debug.DrawLine(position.Value, position.Value + newForce, UnityEngine.Color.red, 1f);
                        //UnityEngine.Debug.DrawLine(position.Value, otherPosition, UnityEngine.Color.cyan, 1f);
                    }
                }
            }).ScheduleParallel();
		}
    }
}

// normalBetween = math.mul(gravityQuadrant.rotation, normalBetween);
// gravityQuadrant.ZeroGravityAxis(ref normalBetween);
// normalBetween = float3.zero;
// newForce.y = 0;
// gravityQuadrant.ZeroGravityAxis(ref newForce);