﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;

using Zoxel.Clans;
// [UpdateAfter(typeof(NearbyCharactersSystem))]
// Problem:
//      Target seeking relies on cached character data on nearby targets of each unit

// maintain character planet inside each chunk
// containing list of their ids
// containing their positions? - a quadrant index (like 4x4x4, if a character in there or a surrounding quad, then its nearby)
// for targetting system use these lookups
// when character moving and reaches new voxel position - update in lookup

// To do:
// Use maps to store the characters in chunks
// Only check self and surrounding chunks for targets
// Do a vision check on target using a vision radius


namespace Zoxel.AI
{
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class TargetSeekSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // if doesn't have target, seek a new one
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity, SetBrainState>()
                .WithAll<Brain>()
                .ForEach((Entity e, int entityInQueryIndex, ref Target target, in NearbyEntities nearbyEntities, in ClanLink clanLink) =>
            {
                if (target.target.Index <= 0)
                {
                    // Seek new Target
                    // Am i seeking ally or target?
                    // if Gaurd is on, find ally, and follow them
                    float closestDistance = 8; //targeter.Value.seekRange;
                    int targetIndex = -1;
                    for (int i = 0; i < nearbyEntities.count; i++)
                    {
                        var nearbyEntity = nearbyEntities.characters[i];
                        // if not this one, and not dead, then can attack! (And not from same clan)
                        if (clanLink.clan.Index == 0 || nearbyEntity.clan.Index == 0 || nearbyEntity.clan != clanLink.clan)
                        {
                            //float newDistance = math.distance(positionValue, character.position);
                            if (nearbyEntity.distance < closestDistance)
                            {
                                closestDistance = nearbyEntity.distance;
                                targetIndex = i;
                            }
                        }
                    }
                    if (targetIndex != -1)
                    {
                        var nearbyEntity = nearbyEntities.characters[targetIndex];
                        target.target = nearbyEntity.character;
                        if (HasComponent<Aggressive>(e) && nearbyEntity.clan != clanLink.clan)    //aiState.isAggressive == 1)
                        {
                            //brain.newState = AIStateType.Attack;
                            PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetBrainState(AIStateType.Attack));
                        }
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}