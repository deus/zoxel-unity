using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.AI
{
    //! Rotates player towards npc.
    /**
    *   \todo Rotate head a little, and only rotate body too if head rotation too much.
    */
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class RotateToSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
			var deltaTime = World.Time.DeltaTime * 3f;
            Entities
                .WithNone<DestroyEntity, DeadEntity>()
                .WithAll<RotateTowards>()
                .ForEach((ref Rotation rotation, in Translation position, in Target target, in GravityQuadrant gravityQuadrant) =>
            {
                var targetPosition = target.targetPosition;
                gravityQuadrant.AlignGravityAxis(ref targetPosition, position.Value);
                var normalBetween = math.normalizesafe(targetPosition - position.Value);
                var targetRotation = quaternion.LookRotationSafe(normalBetween, gravityQuadrant.GetUpDirection());
                rotation.Value = QuaternionHelpers.slerp(rotation.Value, targetRotation, deltaTime);
            }).ScheduleParallel();
        }
    }
}