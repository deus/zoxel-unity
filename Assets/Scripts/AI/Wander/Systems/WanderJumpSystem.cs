using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Movement;

namespace Zoxel.AI
{
    //! Every x seconds while wandering it has a chance to jump.
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class WanderJumpSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
			var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<Jumping>()
                .ForEach((Entity e, int entityInQueryIndex, ref Wander wander, in BodyForce bodyForce) =>
            {
                if (wander.moving == 1)
                {
                    if (elapsedTime - wander.lastWanderJumped >= wander.waitCooldown * 0.5f)
                    {
                        wander.lastWanderJumped = elapsedTime;
                        // if (wander.random.NextInt(1000) >= 997)
                        if (wander.random.NextInt(100) >= 80)
                        {
                            PostUpdateCommands.AddComponent<Jumping>(entityInQueryIndex, e);
                        }
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}