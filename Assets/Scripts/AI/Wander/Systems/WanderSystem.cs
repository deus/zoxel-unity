﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.AI.Authoring;
using Zoxel.Transforms;
using Zoxel.Movement;

namespace Zoxel.AI
{
    //! Every x seconds it will wander or wait.
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class WanderSystem : SystemBase
	{
        protected override void OnCreate()
        {
            RequireForUpdate<AISettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var aiSettings = GetSingleton<AISettings>();
            var maxWanderDistance = aiSettings.maxWanderDistance; // 16f;
            var wanderSteerForce = aiSettings.wanderSteerForce; // 3.4f;
            var wanderSlowRate = aiSettings.wanderSlowRate; // 0.16f;
            var wanderVariance = aiSettings.wanderVariance; // 0.09f;
			var time = World.Time.ElapsedTime;
			var deltaTime = World.Time.DeltaTime;
            Dependency = Entities
                .WithNone<InitializeEntity>()
                .ForEach((ref Wander wander, ref BodyForce bodyForce, ref Rotation rotation, ref Translation translation, in BodyInnerForce bodyInnerForce, in SpawnPoint spawnPoint,
                    in GravityQuadrant gravityQuadrant) =>
            {
                var accelaration = float3.zero;
                if (wander.moving == 0)
                {
                    if (time - wander.lastWandered >= wander.waitCooldown)
                    {
                        wander.lastWandered = time;
                        wander.OnWaited();
                        wander.RandomizeTargetRotation(rotation.Value);
                    }
                }
                else
                {
                    accelaration = new float3(0, 0, bodyInnerForce.movementForce * wanderSlowRate);
                    if (time - wander.lastWandered >= wander.wanderCooldown)
                    {
                        wander.lastWandered = time;
                        wander.OnWandered();
                    }
                    wander.RandomizeAdditionRotation(rotation.Value, wanderVariance);
                }
                accelaration = math.rotate(rotation.Value, accelaration);
                // adjust vector to be towards point
                if (wander.moving == 1)
                {
                    var wanderRealPoint = spawnPoint.realPosition; // .ToFloat3() + new float3(1, 1, 1) / 2f;
                    if (math.distance(translation.Value, wanderRealPoint) >= maxWanderDistance)
                    {
                        accelaration = float3.zero;
                        translation.Value = wanderRealPoint;
                    }
                    else
                    {
                        var towardsMiddleChunk = - math.normalize(translation.Value - wanderRealPoint);
                        towardsMiddleChunk = math.rotate(math.inverse(rotation.Value), towardsMiddleChunk);
                        gravityQuadrant.ZeroGravityAxis(ref towardsMiddleChunk);
                        towardsMiddleChunk *= wanderSteerForce;
                        accelaration += math.rotate(rotation.Value, towardsMiddleChunk);
                    }
                }
                bodyForce.acceleration += accelaration;
                rotation.Value = QuaternionHelpers.slerp(rotation.Value, wander.targetAngle, deltaTime * bodyInnerForce.movementTorque);
                // new quaternion(newAngle.value.x, newAngle.value.y, newAngle.value.z, newAngle.value.w);
                // UnityEngine.Debug.DrawLine(translation.Value, translation.Value + math.rotate(wander.targetAngle, new float3(0, 0, 1)), UnityEngine.Color.red);
            }).ScheduleParallel(Dependency);
        }
    }
}
                    //var randomEuler = math.rotate(baseRotation, new float3(0, random.NextFloat(0, 360), 0));
                    //wander.targetAngle = math.rotate(rotation.Value, new float3(0, wander.random.NextFloat(-wanderVariance, wanderVariance), 0));

// Remove thinking from this
// Add State changes to idle (including idle system which involves looking at nearby units) to AIStateSystem
/// How can I link the bodyForce up to pathing
/// Have a byte array for masking - using chunks
/// Link the bodyForce to the planet that its in - planet being the ChunkGroup - but for terrain
/// Input into the job - the planet array?
/// Another system before bodyForce system, that processes pathing - only has to do it when chunks update
///		- creates a bitmask of the chunks that can walk on (just 1 voxel units above for ground units) - ChunkPathing
///		- These component datas will be used by the bodys
///		- bodyForce needs bound values as well - can check if the bounds intersects with any of the voxels - a simple AAB check
///		- BodyAI (used for wandering) use floodfill to find out if it can get to new target - with maximum checks
///		- Body uses this to keep height for now
/// Finally spawn 2000 agents to check how well they wander
///  - make a small youtube video
///  - add text between cuts
///  - 'Near Instanteous ECS Rendering of voxels'
///  - 'ECS AI and movement, up to 5000 agents'
/// Maybe, changing state will add/remove components?
///     Flee, FleeSystem
///     Attack, AttackSystem
///     Wander, WanderSystem
///     Idle, thats all folks
///     Patrol, PatrolSystem