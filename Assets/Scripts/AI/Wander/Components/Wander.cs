﻿using Unity.Entities;
using Unity.Mathematics;

using Unity.Collections;

namespace Zoxel.AI
{
    public struct WanderData
    {
        public float wanderCooldownMin;
        public float wanderCooldownMax;
        public float waitCooldownMin;
        public float waitCooldownMax;
    }
    //! Wanders around area.
    /**
    *   \todo Clean up data. Initialize random when wandering starts. TargetAngle to also set when starting.
    */
    public struct Wander : IComponentData
	{
        public byte moving;
        public Random random;
        public double lastWandered;
        public double lastWanderJumped;
        public quaternion targetAngle;
        public WanderData data;
        public float wanderCooldown;
        public float waitCooldown;

        public Wander(int seed, quaternion rotation)
        {
            this.random = new Random();
            this.random.InitState((uint) seed);
            this.targetAngle = rotation;
            var wanderData = new WanderData();
            wanderData.waitCooldownMin = 1;
            wanderData.waitCooldownMax = 3;
            wanderData.wanderCooldownMin = 3;
            wanderData.wanderCooldownMax = 12;
            this.data = wanderData;
            this.moving = 0;
            this.lastWandered = 0;
            this.lastWanderJumped = 0;
            this.wanderCooldown = random.NextFloat(data.wanderCooldownMin, data.wanderCooldownMax);
            this.waitCooldown = random.NextFloat(data.waitCooldownMin, data.waitCooldownMax);
        }

        public void OnWaited()
        {
            this.wanderCooldown = random.NextFloat(data.wanderCooldownMin, data.wanderCooldownMax);
            moving = 1;
        }

        public void RandomizeTargetRotation(quaternion baseRotation)
        {
            var degreesToRadians = ((math.PI * 2) / 360f);
            var euler = new float3(0, random.NextFloat(0, 360), 0) * degreesToRadians;
            targetAngle = math.mul(baseRotation, quaternion.EulerXYZ(euler));
        }

        public void RandomizeAdditionRotation(quaternion baseRotation, float wanderVariance)
        {
            var degreesToRadians = ((math.PI * 2) / 360f);
            //var randomEuler = math.rotate(baseRotation, new float3(0, random.NextFloat(-wanderVariance, wanderVariance), 0));
            //targetAngle = quaternion.EulerXYZ(randomEuler * degreesToRadians);
            targetAngle = math.mul(baseRotation,
                quaternion.EulerXYZ(new float3(0,  random.NextFloat(-wanderVariance, wanderVariance), 0) * degreesToRadians));
            //var randomEuler = math.rotate(baseRotation, new float3(0, random.NextFloat(0, 360), 0));
            //wander.targetAngle = math.rotate(rotation.Value, new float3(0, wander.random.NextFloat(-wanderVariance, wanderVariance), 0));
        }

        public void OnWandered()
        {
            if (data.waitCooldownMin != 0 && data.waitCooldownMax != 0)
            {
                this.waitCooldown = random.NextFloat(data.waitCooldownMin, data.waitCooldownMax);
                this.moving = 0;
            }
        }
    }
}
