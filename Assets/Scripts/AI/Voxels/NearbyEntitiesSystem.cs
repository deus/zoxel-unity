﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Clans;
using Zoxel.Voxels;
using Zoxel.Characters.World;

namespace Zoxel.AI
{
    //! Gets nearby characters.
    /**
    *   Uses ChunkCharacterLinks perchunk to query nearby characters.
    */
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
	public partial class NearbyEntitiesSystem : SystemBase
    {
        private EntityQuery characters;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            characters = GetEntityQuery(
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.ReadOnly<Character>(),
                ComponentType.ReadOnly<ClanLink>(),
                ComponentType.ReadOnly<Translation>());
            chunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkNeighbors>(),
                ComponentType.ReadOnly<ChunkCharacters>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var maxDistance = 10f;
            var characterEntities = characters.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var clanLinks = GetComponentLookup<ClanLink>(true);
            var translations = GetComponentLookup<Translation>(true);
            var chunkEntities = characters.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunkCharacters = GetComponentLookup<ChunkCharacters>(true);
            var chunkNeighbors = GetComponentLookup<ChunkNeighbors>(true);
            //characterEntities.Dispose();
            //chunkEntities.Dispose();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref NearbyEntities nearbyEntities, in Translation translation, in ChunkLink chunkLink) =>
            {
                nearbyEntities.Clear();
                if (!chunkCharacters.HasComponent(chunkLink.chunk))
                {
                    return;
                }
                var chunkNeighbors2 = chunkNeighbors[chunkLink.chunk];
                var neighborChunks = chunkNeighbors2.GetNeighbors();
                var characters = new NativeList<Entity>();
                if (chunkCharacters.HasComponent(chunkLink.chunk))
                {
                    var chunkCharacters2 = chunkCharacters[chunkLink.chunk];
                    for (int j = 0; j < chunkCharacters2.characters.Length; j++)
                    {
                        var characterEntity = chunkCharacters2.characters[j];
                        if (characterEntity == e)
                        {
                            continue;
                        }
                        if (clanLinks.HasComponent(characterEntity))
                        {
                            characters.Add(characterEntity);
                        }
                    }
                }
                for (int i = 0; i < neighborChunks.Length; i++)
                {
                    var chunkEntity = neighborChunks[i];
                    if (!chunkCharacters.HasComponent(chunkEntity))
                    {
                        continue;
                    }
                    var chunkCharacters2 = chunkCharacters[chunkEntity];
                    for (int j = 0; j < chunkCharacters2.characters.Length; j++)
                    {
                        var characterEntity = chunkCharacters2.characters[j];
                        if (characterEntity == e)
                        {
                            continue;
                        }
                        if (clanLinks.HasComponent(characterEntity))
                        {
                            characters.Add(characterEntity);
                        }
                    }
                }
                for (var distanceCheck = 1; distanceCheck <= maxDistance; distanceCheck++)
                {
                    for (var i = 0; i < characters.Length; i++)
                    {
                        var e2 = characters[i];
                        var otherTranslation = translations[e2];
                        var distanceTo = math.distance(translation.Value, otherTranslation.Value);
                        if (distanceTo < distanceCheck)
                        {
                            var clanLink = clanLinks[e2];
                            var nearbyOne = new NearbyEntity
                            {
                                character = e2,
                                clan = clanLink.clan,
                                position = otherTranslation.Value,
                                distance = distanceTo
                            };
                            nearbyEntities.Add(nearbyOne);
                            if (nearbyEntities.HasMax())
                            {
                                break;
                            }
                        }
                    }
                }
                nearbyEntities.totalNearby = characters.Length;
                characters.Dispose();
                neighborChunks.Dispose();
                if (characterEntities.Length > 0) { var e2 = characterEntities[0]; }
                if (chunkEntities.Length > 0) { var e2 = chunkEntities[0]; }
            })  .WithReadOnly(clanLinks)
                .WithReadOnly(translations)
                .WithReadOnly(chunkCharacters)
                .WithReadOnly(chunkNeighbors)
                .WithReadOnly(characterEntities)
                .WithDisposeOnCompletion(characterEntities)
                .WithReadOnly(chunkEntities)
                .WithDisposeOnCompletion(chunkEntities)
                .ScheduleParallel(Dependency);
		}
    }
}