using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;

namespace Zoxel.AI
{
    //! When npc is attacked, it shall respond by attacking back.
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class SpawnPointRealSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;

        protected override void OnCreate()
        {
            voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxScale>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxScales = GetComponentLookup<VoxScale>(true);
            voxEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, InitializeEntity>()
                .ForEach((ref SpawnPoint spawnPoint, in VoxLink voxLink) =>
            {
                if (!voxScales.HasComponent(voxLink.vox))
                {
                    return;
                }
                var voxScale = voxScales[voxLink.vox].scale;
                var wanderRealPoint = spawnPoint.position.ToFloat3() + new float3(1, 2, 1) / 2f;
                wanderRealPoint.x *= voxScale.x;
                wanderRealPoint.y *= voxScale.y;
                wanderRealPoint.z *= voxScale.z;
                spawnPoint.realPosition = wanderRealPoint;
            })  .WithReadOnly(voxScales)
                .ScheduleParallel(Dependency);
        }
    }
}