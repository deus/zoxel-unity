using Unity.Entities;
// using Unity.Mathematics;
using UnityEngine;

namespace Zoxel.AI.Authoring
{
    //! Settings data for our ai namespace.
    // [GenerateAuthoringComponent]
    public struct AISettings : IComponentData
    {
        public float maxWanderDistance; // 26f
        public float wanderSteerForce;  // 3f
        public float wanderSlowRate;    // 0.16f
        public float wanderVariance;    // 0.09f
        public bool isDebugWander;
    }

    public class AISettingsAuthoring : MonoBehaviour
    {
        public float maxWanderDistance; // 26f
        public float wanderSteerForce;  // 3f
        public float wanderSlowRate;    // 0.16f
        public float wanderVariance;    // 0.09f
        public bool isDebugWander;
    }

    public class AISettingsAuthoringBaker : Baker<AISettingsAuthoring>
    {
        public override void Bake(AISettingsAuthoring authoring)
        {
            AddComponent(new AISettings
            {
                maxWanderDistance = authoring.maxWanderDistance,
                wanderSteerForce = authoring.wanderSteerForce,
                wanderSlowRate = authoring.wanderSlowRate,
                wanderVariance = authoring.wanderVariance,
                isDebugWander = authoring.isDebugWander,
            });       
        }
    }
}

/*public void DrawUI()
{
    GUILayout.Label(" [AI] ");
}

public void DrawDebug()
{
    GUILayout.Label("AI");
    // disableNighttime = GUILayout.Toggle(disableNighttime, "Disable Nighttime");
}*/