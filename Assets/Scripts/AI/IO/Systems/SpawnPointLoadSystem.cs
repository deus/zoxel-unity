using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.AI
{
    //! Loads Equipment from file.
    /**
    *   - Load System -
    */
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class SpawnPointLoadSystem : SystemBase
    {
        const string filename = "SpawnPoint.zox";
        const int bytesPerData = 13;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithAll<SpawnPoint>()
                .ForEach((ref LoadSpawnPoint loadSpawnPoint, in EntitySaver entitySaver) =>
            {
                if (loadSpawnPoint.loadPath.Length == 0)
                {
                    loadSpawnPoint.loadPath = new Text(entitySaver.GetPath() + filename);
                }
                loadSpawnPoint.reader.UpdateOnMainThread(in loadSpawnPoint.loadPath);
            }).WithoutBurst().Run();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithAll<SpawnPoint, EntitySaver>()
                .ForEach((Entity e, int entityInQueryIndex, ref SpawnPoint spawnPoint, ref LoadSpawnPoint loadSpawnPoint) =>
            {
                if (loadSpawnPoint.reader.IsReadFileInfoComplete())
                {
                    if (loadSpawnPoint.reader.DoesFileExist())
                    {
                        loadSpawnPoint.reader.state = AsyncReadState.StartOpenFile;
                    }
                    else
                    {
                        // UnityEngine.Debug.LogError("File Did not exist.");
                        loadSpawnPoint.Dispose();
                        PostUpdateCommands.RemoveComponent<LoadSpawnPoint>(entityInQueryIndex, e);
                    }
                }
                else if (loadSpawnPoint.reader.IsReadFileComplete())
                {
                    if (loadSpawnPoint.reader.DidFileReadSuccessfully())
                    {
                        var bytes = loadSpawnPoint.reader.GetBytes();
                        if (bytes.Length == 13)
                        {
                            spawnPoint.position = ByteUtil.GetInt3(in bytes, 0);
                            spawnPoint.rotation = bytes[12];
                        }
                    }
                    // Dispose
                    loadSpawnPoint.Dispose();
                    PostUpdateCommands.RemoveComponent<LoadSpawnPoint>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}