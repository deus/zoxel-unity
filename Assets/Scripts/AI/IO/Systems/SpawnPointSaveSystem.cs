using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using System;
using System.IO;

namespace Zoxel.AI
{
    //! Saves user equipment to a file.
    /**
    *   - Save System -
    */
    [UpdateInGroup(typeof(AISystemGroup))]
    public partial class SpawnPointSaveSystem : SystemBase
    {
        const string filename = "SpawnPoint.zox";
        const int bytesPerData = 13;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands.RemoveComponent<SaveSpawnPoint>(processQuery);
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<InitializeSaver, DisableSaving>()
                .WithNone<InitializeEntity, LoadSpawnPoint>()
                .WithAll<SaveSpawnPoint>()
                .ForEach((in EntitySaver entitySaver, in SpawnPoint spawnPoint) =>
            {
                var filepath = entitySaver.GetPath() + filename;
                var bytes = new BlitableArray<byte>(bytesPerData, Allocator.Temp);
                ByteUtil.SetInt3(ref bytes, 0, spawnPoint.position);
                bytes[12] = spawnPoint.rotation;
                #if WRITE_ASYNC
                File.WriteAllBytesAsync(filepath, bytes.ToArray());
                #else
                File.WriteAllBytes(filepath, bytes.ToArray());
                #endif
                bytes.Dispose();
            }).WithoutBurst().Run();
        }
    }
}