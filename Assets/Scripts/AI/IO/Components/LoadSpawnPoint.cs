using Unity.Entities;

namespace Zoxel.AI
{
    public struct LoadSpawnPoint : IComponentData
    {
        public Text loadPath;
        public AsyncFileReader reader;

        public void Dispose()
        {
            loadPath.Dispose();
            reader.Dispose();
        }
    }
}