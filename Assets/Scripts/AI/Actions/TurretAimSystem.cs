﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Actions;

namespace Zoxel.AI.Actionz
{
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class AimSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
		{
            var deltaTime = World.Time.DeltaTime * 1.5f;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref Translation position, ref Rotation rotation, in Aimer aimer, in Target target) =>
            {
				if (target.target.Index > 0)
                {
                    var positionValue = position.Value;
                    var targetPosition = target.targetPosition + new float3(0, 0.2f, 0);
                    var angle = math.normalizesafe(targetPosition - positionValue);
                    var targetRotation = quaternion.LookRotationSafe(angle, new float3(0,1,0));
                    // tell AI to shoot
                    rotation.Value = QuaternionHelpers.slerpSafe(rotation.Value, targetRotation, deltaTime * aimer.turnSpeed);
                    position.Value = aimer.originalPosition + math.mul(targetRotation, new float3(0, 0, aimer.offsetZ / 4f));
                    // Add Trigger here?
                    if (!HasComponent<ActivateAction>(e))
                    {
                       PostUpdateCommands.AddComponent<ActivateAction>(entityInQueryIndex, e);
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
	}
}