using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Zoxel.Movement;
using Zoxel.Actions;
using Zoxel.Skills;

namespace Zoxel.AI.Actionz
{
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class AttackStartSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<ActivateAction, ActivatedAction>()
                .WithNone<DeadEntity, DestroyEntity, SetBrainState>()
                .ForEach((Entity e, int entityInQueryIndex, ref Attack attack, ref UserActionLinks userActionLinks, in Target target, in BodyInnerForce bodyInnerForce, in Brain brain) =>
            {
                if (brain.state == AIStateType.Attack)
                {
                    if (target.target.Index <= 0)
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetBrainState(AIStateType.Idle));
                    }
                    else
                    {
                        if(userActionLinks.skillType == SkillType.PhysicalAttack)
                        {
                            attack.attackDistance = 0.8f;
                            PostUpdateCommands.AddComponent<ActivateAction>(entityInQueryIndex, e);
                        }
                        else
                        {
                            attack.attackDistance = 3;
                            if (!HasComponent<ActivateAction>(e))
                            {
                                PostUpdateCommands.AddComponent<ActivateAction>(entityInQueryIndex, e);
                            }
                        }
                        attack.moveSpeed = bodyInnerForce.movementForce;
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}