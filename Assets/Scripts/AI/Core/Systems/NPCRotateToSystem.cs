﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.AI
{
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class NPCRotateToSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
			var deltaTime = World.Time.DeltaTime;
            Entities
                .WithAll<Brain, RotateTowards>()
                .ForEach((ref Rotation rotation, in Translation position, in Target target) =>    // in BodyTorque bodyTorque, 
            {
                var normalBetween = math.normalizesafe(target.targetPosition - position.Value);
                var targetAngle = quaternion.LookRotationSafe(0.05f * normalBetween, math.up());
                var newAngle = QuaternionHelpers.slerp(rotation.Value, targetAngle, deltaTime);
                //var newAngle2 = new UnityEngine.Quaternion(newAngle.value.x, newAngle.value.y, newAngle.value.z, newAngle.value.w);
                rotation.Value = newAngle;
            }).ScheduleParallel();
        }
    }
}