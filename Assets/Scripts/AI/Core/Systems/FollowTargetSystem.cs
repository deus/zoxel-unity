﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Movement;

namespace Zoxel.AI
{
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class FollowTargetSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            // var nearDistance = 1.23f; // attack.attackDistance;
            var nearDistance = 1.23f; // attack.attackDistance;
            var moveSpeed = 2; // attack.moveSpeed;
            var nearSpeedMultiplier = 0.13f;    // 0.1f;
			var deltaTime = World.Time.DeltaTime * 2.5f;
            Entities
                .WithAll<Follow>()
                .ForEach((ref Rotation rotation, in Translation position, in BodyInnerForce bodyInnerForce, in Target target, in GravityQuadrant gravityQuadrant) =>
            {
                var targetPosition = target.targetPosition;
                // targetPosition.y = position.Value.y;
                gravityQuadrant.AlignGravityAxis(ref targetPosition, position.Value);
                var normalBetween = math.normalizesafe(targetPosition - position.Value);
                // var targetRotation = quaternion.LookRotationSafe(normalBetween, gravityQuadrant.rotation.ToEulerSlow()); // gravityQuadrant.GetUpDirection()); // math.up());  0.05f * 
                var targetRotation = quaternion.LookRotationSafe(normalBetween, gravityQuadrant.GetUpDirection()); // math.up());  0.05f * 
                rotation.Value = QuaternionHelpers.slerp(rotation.Value, targetRotation, deltaTime); // bodyInnerForce.movementTorque);
                //var newAngle2 = new quaternion(newAngle.value.x, newAngle.value.y, newAngle.value.z, newAngle.value.w);
            }).ScheduleParallel();
            Entities
                .WithAll<Follow>()
                .ForEach((ref BodyForce bodyForce, in Rotation rotation, in Translation position, in Target target, in BodyInnerForce bodyInnerForce, in GravityQuadrant gravityQuadrant) =>
            {
                //! \todo Account for angle accuracy when adding force.
                var targetPosition = target.targetPosition;
                // targetPosition.y = position.Value.y;
                gravityQuadrant.AlignGravityAxis(ref targetPosition, position.Value);
                var attackForce = new float3();
                if (target.targetDistance >= nearDistance * 2f)  // if above 2 units away go at full speed
                {
                    attackForce = math.rotate(rotation.Value, new float3(0, 0, moveSpeed));
                }
                else if (target.targetDistance >= nearDistance)
                {
                    attackForce = math.rotate(rotation.Value, new float3(0, 0, (target.targetDistance - nearDistance) * moveSpeed * 0.9f));   // 
                }
                else if (target.targetDistance < nearDistance)
                {
                    // at 0 distance of 0, go minus 2 speed away from target
                    // return;
                    // UnityEngine.Debug.LogError("Stopping.");
                    attackForce = math.rotate(rotation.Value, new float3(0, 0, (target.targetDistance - nearDistance) * moveSpeed * nearSpeedMultiplier)); // new float3(0, 0, -(stopDistance - distanceTo) * body.movementSpeed);  // if 1, go -1 minus
                }
                gravityQuadrant.ZeroGravityAxis(ref attackForce);
                // attackForce.y = 0;
                bodyForce.acceleration += attackForce * 2;
            }).ScheduleParallel();
        }
    }
}