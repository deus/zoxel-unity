using Unity.Entities;
using Unity.Burst;

namespace Zoxel.AI
{
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class IdleSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var elapsedTime = (float)World.Time.ElapsedTime;
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity, SetBrainState>()
                .ForEach((Entity e, int entityInQueryIndex, ref Idle idle, in Brain brain) =>
            {
                if (idle.idleTime != 0)
                {
                    if (elapsedTime - idle.lastIdled >= idle.idleTime)
                    {
                        idle.lastIdled = elapsedTime;
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetBrainState(AIStateType.Wander));
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}