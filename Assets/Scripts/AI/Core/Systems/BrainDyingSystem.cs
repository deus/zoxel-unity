using Unity.Entities;
using Unity.Burst;
using Zoxel.AI;

namespace Zoxel.AI
{
    //! Handles when a character dies using the DyingEntity event.
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
	public partial class BrainDyingSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DyingEntity, Brain>()
                .ForEach((Entity e, int entityInQueryIndex, in Brain brain) =>
            { 
                if (brain.state == AIStateType.Wander)
                {
                    PostUpdateCommands.RemoveComponent<Wander>(entityInQueryIndex, e);
                }
                else if (brain.state == AIStateType.Attack)
                {
                    PostUpdateCommands.RemoveComponent<Attack>(entityInQueryIndex, e);
                }
                else if (brain.state == AIStateType.Idle)
                {
                    PostUpdateCommands.RemoveComponent<Idle>(entityInQueryIndex, e);
                }
                else if (brain.state == AIStateType.Follow)
                {
                    PostUpdateCommands.RemoveComponent<Follow>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}