using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.AI
{
    //! When npc is attacked, it shall respond by attacking back.
    /**
    *   \todo Create a threat generation system.
    *   \todo When hit, Screenshake by shaking camera.
    */
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class OnAttackedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // Handle responding to attacks
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity, SetBrainState>()
                .ForEach((Entity e, int entityInQueryIndex, ref Target target, in OnAttacked onAttacked, in Translation translation, in Brain brain) => 
            {
                target.target = onAttacked.attacker;
                //target.targetDistance = math.distance(onAttacked.attackerPosition, translation.Value);
                var targetPosition = onAttacked.attackerPosition;
                target.targetDistance = math.distance(targetPosition, translation.Value);
                // targetPosition.y = translation.Value.y;
                // target.targetDistanceXZ = math.distance(targetPosition, translation.Value);
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetBrainState(AIStateType.Attack));
                PostUpdateCommands.RemoveComponent<OnAttacked>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

            Dependency = Entities
                .WithNone<Brain>()
                .ForEach((Entity e, int entityInQueryIndex, ref Target target, in OnAttacked onAttacked) =>
            {
                PostUpdateCommands.RemoveComponent<OnAttacked>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}