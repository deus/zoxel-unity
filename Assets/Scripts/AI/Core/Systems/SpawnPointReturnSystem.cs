using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;

namespace Zoxel.AI
{
    //! When npc is attacked, it shall respond by attacking back.
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class SpawnPointReturnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, ReturnToSpawnPoint>()
                .WithAll<SpawnPoint>()
                .ForEach((Entity e, int entityInQueryIndex, in Translation translation) =>
            {
                if (!MathUtil.IsValid(translation.Value))
                {
                    // UnityEngine.Debug.LogError("!MathUtil.IsValid: " + e.Index);
                    PostUpdateCommands.AddComponent<ReturnToSpawnPoint>(entityInQueryIndex, e);
                    return;
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<ReturnToSpawnPoint>()
                .ForEach((Entity e, int entityInQueryIndex, ref Translation translation, ref Rotation rotation, ref BodyForce bodyForce, in SpawnPoint spawnPoint) =>
            {
                PostUpdateCommands.RemoveComponent<ReturnToSpawnPoint>(entityInQueryIndex, e);
                // var wanderRealPoint = spawnPoint.position.ToFloat3() + new float3(1, 2, 1) / 2f;
                translation.Value = spawnPoint.realPosition;
                rotation.Value = spawnPoint.GetRotation();
                bodyForce.Clear();
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}