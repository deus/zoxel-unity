using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Movement;

namespace Zoxel.AI
{
    // todo: if creator in combat, fight their target

    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class GaurdSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
			var time = World.Time.ElapsedTime;
			var deltaTime = World.Time.DeltaTime;
            var nearDistance = 1.23f; // attack.attackDistance;
            var moveSpeed = 2; // attack.moveSpeed;
            Entities
                .WithAll<Gaurd>()
                .ForEach((ref Target target, in CreatorLink creatorLink) =>
            {
                target.target = creatorLink.creator;
            }).ScheduleParallel();

            // todo: add a micro component - FollowTarget - rather then using follow and attack doing same functionality, remove it when switching behaviour
            Entities
                .WithAll<Gaurd>()
                .ForEach((ref BodyForce bodyForce, ref Rotation rotation, in Translation position, in Target target) =>
            {
                var targetPosition = target.targetPosition;
                targetPosition.y = position.Value.y;
                var attackForce = new float3();
                if (target.targetDistance >= nearDistance)  // if above 2 units away go at full speed
                {
                    attackForce = math.rotate(rotation.Value, new float3(0, 0, moveSpeed));
                }
                else if (target.targetDistance >= nearDistance)
                {
                    attackForce = math.rotate(rotation.Value, new float3(0, 0, (target.targetDistance - nearDistance) * moveSpeed * 0.9f));   // 
                }
                else if (target.targetDistance < nearDistance)
                {
                    // at 0 distance of 0, go minus 2 speed away from target
                    attackForce = math.rotate(rotation.Value, new float3(0, 0, (target.targetDistance - nearDistance) * moveSpeed * 0.1f)); // new float3(0, 0, -(stopDistance - distanceTo) * body.movementSpeed);  // if 1, go -1 minus
                }
                attackForce.y = 0;
                bodyForce.acceleration += attackForce * 2;
                var normalBetween = math.normalizesafe(targetPosition - position.Value);
                var targetAngle = quaternion.LookRotationSafe(0.05f * normalBetween, math.up());
                var newAngle = QuaternionHelpers.slerp(rotation.Value, targetAngle, deltaTime);
                //var newAngle2 = new quaternion(newAngle.value.x, newAngle.value.y, newAngle.value.z, newAngle.value.w);
                rotation.Value = newAngle;
            }).ScheduleParallel();
        }
    }
}