﻿
    /*using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;

using Unity.Burst;

namespace Zoxel
{
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class MoveToSystem : JobComponentSystem
	{
		protected override JobHandle OnUpdate(JobHandle inputDeps)
        {
            return new MoveToJob { }.Schedule(this, inputDeps);
		}

		[BurstCompile]
		struct MoveToJob : IJobForEach<Mover, BodyForce, Translation, Rotation, Brain>
        {
			public void Execute(ref Mover mover, ref BodyForce body, ref Translation position, ref Rotation rotation, ref Brain aiState)
            {
                if (mover.disabled == 0)
                {
                    // Move to point
                    float distanceTo = math.distance(mover.target, position.Value);// math.max(0.5f, math.distance(moveto.target, position.Value));
                    if (distanceTo >= mover.stopDistance + 2f)  // if above 2 units away go at full speed
                    {
                        body.velocity = math.rotate(rotation.Value, new float3(0, 0, aiState.moveSpeed));
                    }
                    // if within range 2 of targetslow down
                    else if (distanceTo >= mover.stopDistance)
                    {
                        body.velocity = math.rotate(rotation.Value, new float3(0, 0, (distanceTo - mover.stopDistance) * aiState.moveSpeed * 0.9f));   // 
                    }
                    // else distanceTo must be between 0 and stop distance, then go backwards
                    else if (distanceTo < mover.stopDistance)
                    {
                        // at 0 distance of 0, go minus 2 speed away from target
                        body.velocity = math.rotate(rotation.Value, new float3(0, 0, (distanceTo - mover.stopDistance) * aiState.moveSpeed)); // new float3(0, 0, -(stopDistance - distanceTo) * body.movementSpeed);  // if 1, go -1 minus
                    }
                }
            }
        }
	}
}*/