using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;

namespace Zoxel.AI
{
    //! Allows antities to follow another. \todo Reuse FollowSystem functions for this.
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class AttackSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var rotationSpeed = World.Time.DeltaTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity, DeadEntity, SetBrainState>()
                .ForEach((Entity e, int entityInQueryIndex, ref BodyForce bodyForce, ref Rotation rotation, in Translation translation,
                    in Attack attack, in Target target, in GravityQuadrant gravityQuadrant) =>
            {
                if (target.target.Index <= 0 || HasComponent<DeadEntity>(target.target))
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetBrainState(AIStateType.Idle));
                    return;
                }
                var targetPosition = target.targetPosition;
                gravityQuadrant.AlignGravityAxis(ref targetPosition, translation.Value);
                // var attackForce = new float3();
                var nearDistance = attack.attackDistance;
                var moveSpeed = attack.moveSpeed;
                var movementForce = float3.zero;
                if (target.targetDistance >= nearDistance)  // if above 2 units away go at full speed
                {
                    movementForce = new float3(0, 0, moveSpeed);
                }
                else if (target.targetDistance >= nearDistance)
                {
                    movementForce = new float3(0, 0, (target.targetDistance - nearDistance) * moveSpeed * 0.9f);   // 
                }
                else if (target.targetDistance < nearDistance)
                {
                    // at 0 distance of 0, go minus 2 speed away from target
                    movementForce = new float3(0, 0, (target.targetDistance - nearDistance) * moveSpeed * 0.4f); // new float3(0, 0, -(stopDistance - distanceTo) * body.movementSpeed);  // if 1, go -1 minus
                }
                // slow down if rotation not within target as well
                var targetAngleAbs = math.abs(target.targetAngle);
                rotationSpeed *= 1f + 2f * targetAngleAbs;
                if (targetAngleAbs >= 0.2f)
                {
                    // movementForce *= 0.1f + 0.9f - targetAngle2;
                    movementForce *= 1f - targetAngleAbs;
                }
                movementForce = math.rotate(rotation.Value, movementForce);
                gravityQuadrant.ZeroGravityAxis(ref movementForce);
                bodyForce.acceleration += movementForce;
                var normalBetween = math.normalizesafe(targetPosition - translation.Value);
                var targetRotation = quaternion.LookRotationSafe(normalBetween, gravityQuadrant.GetUpDirection());
                rotation.Value = QuaternionHelpers.slerp(rotation.Value, targetRotation, rotationSpeed);
                /*UnityEngine.Debug.DrawLine(translation.Value, translation.Value + normalBetween, UnityEngine.Color.cyan);
                UnityEngine.Debug.DrawLine(translation.Value, targetPosition, UnityEngine.Color.grey);*/
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

// math.up());
// var targetRotation = quaternion.LookRotationSafe(normalBetween, gravityQuadrant.rotation.ToEulerSlow());
// var targetAngle = quaternion.LookRotationSafe(normalBetween, gravityQuadrant.GetUpDirection()); // math.up());
//var newAngle2 = new Quaternion(newAngle.value.x, newAngle.value.y, newAngle.value.z, newAngle.value.w);

//UnityEngine.Debug.DrawLine(translation.Value, translation.Value + math.rotate(targetAngle, new float3(0, 0, 1)), UnityEngine.Color.red);
//UnityEngine.Debug.DrawLine(translation.Value, translation.Value + gravityQuadrant.GetUpDirection(), UnityEngine.Color.green);


/*if (voxelCollider.isOnGround == 0)
{
    movementForce /= 10;
}*/
                /*if (gravityQuadrant.quadrant == PlanetSide.Down || gravityQuadrant.quadrant == PlanetSide.Up)
                {
                    movementForce.y = 0;
                }
                else if (gravityQuadrant.quadrant == PlanetSide.Left || gravityQuadrant.quadrant == PlanetSide.Right)
                {
                    movementForce.x = 0;
                }
                else if (gravityQuadrant.quadrant == PlanetSide.Backward || gravityQuadrant.quadrant == PlanetSide.Forward)
                {
                    movementForce.z = 0;
                }*/
                /*if (gravityQuadrant.quadrant == PlanetSide.Down || gravityQuadrant.quadrant == PlanetSide.Up)
                {
                    targetPosition.y = translation.Value.y;
                }
                else if (gravityQuadrant.quadrant == PlanetSide.Left || gravityQuadrant.quadrant == PlanetSide.Right)
                {
                    targetPosition.x = translation.Value.x;
                }
                else if (gravityQuadrant.quadrant == PlanetSide.Backward || gravityQuadrant.quadrant == PlanetSide.Forward)
                {
                    targetPosition.z = translation.Value.z;
                }*/