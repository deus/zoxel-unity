﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;

namespace Zoxel.AI
{
    //! Sets components whhen state changes.
    /**
    *   \todo Use ChangeFilter on Brain instead of a component to change it.
    */
    [BurstCompile, UpdateInGroup(typeof(AISystemGroup))]
    public partial class AIStateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<SetBrainState>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var time = (float)World.Time.ElapsedTime;
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DeadEntity, DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref Brain brain, in SetBrainState setBrainState, in ZoxID zoxID, in Rotation rotation) =>
            {
                // PostUpdateCommands.RemoveComponent<SetBrainState>(entityInQueryIndex, e);
                if (brain.state != setBrainState.newState)
                {
                    if (brain.state == AIStateType.Wander)
                    {
                        PostUpdateCommands.RemoveComponent<Wander>(entityInQueryIndex, e);
                    }
                    else if (brain.state == AIStateType.Attack)
                    {
                        PostUpdateCommands.RemoveComponent<Attack>(entityInQueryIndex, e);
                    }
                    else if (brain.state == AIStateType.Idle)
                    {
                        PostUpdateCommands.RemoveComponent<Idle>(entityInQueryIndex, e);
                    }
                    else if (brain.state == AIStateType.Follow)
                    {
                        PostUpdateCommands.RemoveComponent<Follow>(entityInQueryIndex, e);
                    }
                    else if (brain.state == AIStateType.Gaurd)
                    {
                        PostUpdateCommands.RemoveComponent<Gaurd>(entityInQueryIndex, e);
                    }
                    brain.state = setBrainState.newState;
                    if (brain.state == AIStateType.Wander)
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new Wander(zoxID.id, rotation.Value));
                    }
                    else if (brain.state == AIStateType.Attack)
                    {
                        PostUpdateCommands.AddComponent<Attack>(entityInQueryIndex, e);
                    }
                    else if (brain.state == AIStateType.Idle)
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new Idle(3));
                    }
                    else if (brain.state == AIStateType.Follow)
                    {
                        PostUpdateCommands.AddComponent<Follow>(entityInQueryIndex, e);
                    }
                    else if (brain.state == AIStateType.Gaurd)
                    {
                        PostUpdateCommands.AddComponent<Gaurd>(entityInQueryIndex, e);
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}