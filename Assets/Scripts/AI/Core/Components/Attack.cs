using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.AI
{
    public struct Attack : IComponentData
    {
        public float attackDistance;
        public float moveSpeed;
    }
}