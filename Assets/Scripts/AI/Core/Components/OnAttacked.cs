using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.AI
{
    public struct OnAttacked : IComponentData
    {
        public Entity attacker;
        public float3 attackerPosition;
    }
}