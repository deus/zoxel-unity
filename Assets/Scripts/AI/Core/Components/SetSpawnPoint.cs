using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.AI
{
    public struct SetSpawnPoint : IComponentData
    {
        public float3 position;

        public SetSpawnPoint(float3 position)
        {
            this.position = position;
        }
    }
}