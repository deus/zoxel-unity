using Unity.Entities;

namespace Zoxel.AI
{
    public struct Idle : IComponentData
    { 
        public float lastIdled;
        public float idleTime;

        public Idle(int idleTime)
        {
            this.idleTime = idleTime;
            this.lastIdled = 0;
        }
    }
}