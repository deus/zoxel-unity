using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.AI
{
    //! The original SpawnPoint of an npc.
    public struct SpawnPoint : IComponentData
	{
        //! The global voxel position where an npc wanders around.
        public int3 position;
        //! The original gravity rotation value of a character.
        public byte rotation;
        //! The global voxel real position (translated by world transform).
        public float3 realPosition;

        public quaternion GetRotation()
        {
            return BlockRotation.GetRotation(rotation);
        }
    }
}
