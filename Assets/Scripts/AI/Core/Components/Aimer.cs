﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.AI
{
    public struct Aimer : IComponentData
    {
        public float turnSpeed;
        public float3 originalPosition;
        public float offsetZ;

        public Aimer(float turnSpeed, float3 position, float offsetZ)
        {
            this.turnSpeed = turnSpeed;
            this.originalPosition = position;
            this.offsetZ = offsetZ;
        }
    }
}