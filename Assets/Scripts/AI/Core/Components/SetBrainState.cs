using Unity.Entities;

namespace Zoxel.AI
{
    //! Sets a new AI state.
    public struct SetBrainState : IComponentData
    {
        public byte newState;

        public SetBrainState(byte state)
        {
            this.newState = state;
        }
    }
}