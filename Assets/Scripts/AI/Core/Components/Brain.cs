﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.AI
{
    //! Holds current AI state.
    public struct Brain : IComponentData
    {
        public byte state;
        public byte isAggressive;

        public Brain(byte state, byte isAggressive = 0)
        {
            this.state = state;
            this.isAggressive = isAggressive;
        }
    }
}