using UnityEngine;

namespace Zoxel.Turrets
{
    //! Holds authored data for userStatLinks.
    public partial class TurretsManager : MonoBehaviour
    {
        public static TurretsManager instance;

        [SerializeField]
        public TurretSettingsDatam TurretSettings;

        [HideInInspector]
        public TurretSettings realTurretSettings;

        public TurretSettings turretSettings
        {
            get
            { 
                return realTurretSettings;
            }
        }

        public void Awake()
        {
            instance = this;
            realTurretSettings = TurretSettings.turretSettings;
        }
    }
}