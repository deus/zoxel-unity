﻿using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Voxels;
using System;

namespace Zoxel.Turrets
{
    [UnityEngine.CreateAssetMenu(fileName = "Turret", menuName = "Zoxel/Turret")]//, order = 0)]
    public partial class TurretDatam : UnityEngine.ScriptableObject
    {
        public TurretData data;
        public VoxDatam headVox;
        public VoxDatam baseVox;
    }
}