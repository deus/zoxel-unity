using Unity.Entities;
using Unity.Mathematics;
using System;
using Zoxel.AI;

namespace Zoxel.Turrets
{
    [Serializable]
    public struct TurretData // : IComponentData
    {
        // public int id;
        public float turnSpeed;
        public float deathTime;
        public SeekData seek;
    }
}

/*public float health;
public float healthRegen;
public float attackSpeed;
public float attackDamage;
public float attackForce;*/
//public float goldCost;