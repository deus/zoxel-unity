using UnityEngine;

namespace Zoxel.Turrets
{
    [CreateAssetMenu(fileName = "TurretSettings", menuName = "ZoxelSettings/TurretSettings")]
    public partial class TurretSettingsDatam : ScriptableObject
    {
        public TurretSettings turretSettings;
        
        public TurretDatam turretDatam;
    }
}