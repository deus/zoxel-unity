using Unity.Entities;

namespace Zoxel.Turrets
{
    public struct TurretBase : IComponentData
    {
        public Entity turretCore;
        
        public TurretBase(Entity turretCore)
        {
            this.turretCore = turretCore;
        }
    }
}