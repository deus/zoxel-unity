using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Turrets
{
    public struct SpawnTurret : IComponentData
    {
        public int spawnID;
        public int metaID;
        public Entity summoner;
        public float3 position;
    }
}