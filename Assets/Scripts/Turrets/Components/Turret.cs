using Unity.Entities;

namespace Zoxel.Turrets
{
    public struct Turret : IComponentData
    {
        public Entity baseTurret;
        
        public Turret(Entity baseTurret)
        {
            this.baseTurret = baseTurret;
        }
    }
}