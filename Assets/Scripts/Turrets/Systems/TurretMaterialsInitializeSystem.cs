using Unity.Entities;
using Zoxel.Voxels;
using Zoxel.Rendering;

namespace Zoxel.Turrets
{
    //! Initializes bullet materials!
    [UpdateInGroup(typeof(TurretSystemGroup))]
    public partial class TurretMaterialsInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null) return;
            var bulletMaterial = MaterialsManager.instance.materials.bulletMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, Turret>()
                .ForEach((Entity e) =>
            {
                PostUpdateCommands.SetSharedComponentManaged(e, new EntityMaterials(bulletMaterial));
            }).WithoutBurst().Run();
            Entities
                .WithAll<InitializeEntity, TurretBase>()
                .ForEach((Entity e) =>
            {
                PostUpdateCommands.SetSharedComponentManaged(e, new EntityMaterials(bulletMaterial));
            }).WithoutBurst().Run();
        }
    }
}