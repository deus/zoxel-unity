﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using Zoxel.Transforms;
using Zoxel.AI;
using Zoxel.Movement;
using Zoxel.UI;
using Zoxel.Stats;
using Zoxel.Skills;
using Zoxel.Actions;
using Zoxel.Clans;
using Zoxel.Rendering;
using Zoxel.Voxels;
using Zoxel.Voxels.Authoring;

namespace Zoxel.Turrets
{
    //! Spawns turret into the game.
    /**
    *   - Spawn System -
    *   \todo Convert to Parallel. (ID Generation)
    *   \todo Spawn as Minivox.
    *   \todo Save/Load userStatLinks as minivox userStatLinks.
    *   \todo VoxelDamage to be generic and effect turrets.
    *   \todo Debug systems on turret AI, doesn't work atm.
    */
    [BurstCompile, UpdateInGroup(typeof(TurretSystemGroup))]
    public partial class TurretSpawnSystem : SystemBase
    {
        public static TurretDatam turretDatam;
        private NativeArray<TurretData> turretDatas;
        private NativeArray<VoxDataEditor> voxDatas;
        private bool hasSetVoxScale;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity turretHeadPrefab;
        private Entity turretBasePrefab;
        private EntityQuery processQuery;
        private EntityQuery summonerQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            summonerQuery = GetEntityQuery(
                ComponentType.ReadOnly<Summoner>(),
                ComponentType.ReadOnly<ClanLink>(),
                ComponentType.ReadOnly<VoxLink>());
            voxDatas = new NativeArray<VoxDataEditor>(2, Allocator.Persistent);
            turretDatas = new NativeArray<TurretData>(1, Allocator.Persistent);
            var turretHeadArchtype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(SpawnVoxChunk),
                // typeof(UpdateModel),
                typeof(GenerateStats),
                typeof(GenerateSkills),
                typeof(ApplyAttributes),
                typeof(DestroyEntityInTime),
                typeof(SetBrainState),
                // ----===---===----
                typeof(SummonedEntity),
                typeof(Turret),
                typeof(ZoxID),
                typeof(CreatorLink),
                typeof(ClanLink),
                typeof(UILink),
                typeof(UserStatLinks),
                typeof(UserSkillLinks),
                typeof(UserActionLinks),
                typeof(Shooter),
                typeof(Model),
                typeof(Chunk),
                typeof(Vox),
                typeof(ChunkDimensions),
                typeof(VoxScale),
                typeof(VoxColors),
                typeof(VoxLink),
                typeof(EntityMaterials),
                typeof(Body),
                typeof(NearbyEntities),
                typeof(Brain),
                typeof(Target),
                typeof(Targeter),
                typeof(Aimer),
                typeof(Aggressive),
                // ----===---===----
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld)
            );
            turretHeadPrefab = EntityManager.CreateEntity(turretHeadArchtype);
            EntityManager.SetComponentData(turretHeadPrefab, new Rotation { Value = quaternion.identity });
            EntityManager.SetComponentData(turretHeadPrefab, new NonUniformScale { Value = new float3(1f, 1f, 1f) });
            var turretBaseArchtype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(SpawnVoxChunk),
                // typeof(UpdateModel),
                typeof(ZoxID),
                typeof(TurretBase),
                typeof(Model),
                typeof(Chunk),
                typeof(Vox),
                typeof(ChunkDimensions),
                typeof(VoxScale),
                typeof(VoxColors),
                typeof(VoxLink),
                typeof(EntityMaterials),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld)
            );
            turretBasePrefab = EntityManager.CreateEntity(turretBaseArchtype);
            EntityManager.SetComponentData(turretBasePrefab, new Rotation { Value = quaternion.identity });
            EntityManager.SetComponentData(turretBasePrefab, new NonUniformScale { Value = new float3(1f, 1f, 1f) });
            RequireForUpdate<ModelSettings>();
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            voxDatas.Dispose();
            turretDatas.Dispose();
        }

        private void InitializePrefabs()
        {
            if (!hasSetVoxScale)
            {
                hasSetVoxScale = true;
                var modelSettings = GetSingleton<ModelSettings>();
                var voxelScale = modelSettings.GetVoxelScale();
                EntityManager.SetComponentData(this.turretHeadPrefab, new VoxScale { scale = voxelScale });
                EntityManager.SetComponentData(this.turretBasePrefab, new VoxScale { scale = voxelScale });
            }
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var turretDatam = TurretsManager.instance.TurretSettings.turretDatam;
            if (turretDatam == null || turretDatam.baseVox == null || turretDatam.headVox == null)
            {
                return;
            }
            InitializePrefabs();
            var modelSettings = GetSingleton<ModelSettings>();
            var voxelScale = modelSettings.GetVoxelScale();
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities.ForEach((Entity e, ref SpawnTurret spawnTurret) =>
            {
                spawnTurret.spawnID = IDUtil.GenerateUniqueID();
            }).WithoutBurst().Run();
            var turretData = turretDatam.data;
            var voxDatas = this.voxDatas;
            var turretDatas = this.turretDatas;
            voxDatas[0] = turretDatam.headVox.data;
            voxDatas[1] = turretDatam.baseVox.data;
            turretDatas[0] = turretData;
            var turretBasePrefab = this.turretBasePrefab;
            var turretHeadPrefab = this.turretHeadPrefab;
            var summonerEntities = summonerQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            var clanLinks = GetComponentLookup<ClanLink>(true);
            summonerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, in SpawnTurret spawnTurret) =>
            {
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                var turretData = turretDatas[0];
                var headVoxData = voxDatas[0];
                var baseVoxData = voxDatas[1];
                var spawnID = spawnTurret.spawnID;
                var metaID = spawnTurret.metaID;
                var summoner = spawnTurret.summoner;
                var spawnPosition = spawnTurret.position + new float3(0.5f, 0, 0.5f);
                var clanLink = clanLinks[summoner];
                var voxLink = voxLinks[summoner];
                // Set Data
                var basePosition = spawnPosition + (new float3(0, baseVoxData.GetSize(voxelScale).y, 0));
                var headPosition = basePosition + (new float3(0, baseVoxData.GetSize(voxelScale).y + headVoxData.GetSize(voxelScale).y, 1f / 32f));
                // Spawn Head
                var entityHead = PostUpdateCommands.Instantiate(entityInQueryIndex, turretHeadPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, new ZoxID(spawnID));
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, new CreatorLink(summoner));
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, clanLink);
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, new Aimer(turretData.turnSpeed, headPosition, headVoxData.GetSize(voxelScale).z));
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, new Targeter { data = turretData.seek });
                //var nearbyEntities = new NearbyEntities { };
                //nearbyEntities.Initialize();
                //PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, nearbyEntities);
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, voxLink);
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, new GenerateStats(1));
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, new GenerateSkills(1));
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, new Translation { Value = headPosition });
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, new DestroyEntityInTime(elapsedTime, turretData.deathTime));
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, new Chunk(headVoxData.size, headVoxData.data.Clone()));
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, new ChunkDimensions(headVoxData.size));
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, new VoxColors(headVoxData.colors));
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, new SetBrainState(AIStateType.Attack));
                // Spawn Base
                var entityBase = PostUpdateCommands.Instantiate(entityInQueryIndex, turretBasePrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityBase, voxLink);
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityBase, new ZoxID(spawnID));
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityBase, new TurretBase(entityHead));
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityBase, new Translation { Value = basePosition });
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityBase, new Chunk(baseVoxData.size, baseVoxData.data.Clone()));
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityBase, new ChunkDimensions(baseVoxData.size));
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityBase, new VoxColors(in baseVoxData.colors));
                // Link Them
                PostUpdateCommands.SetComponent(entityInQueryIndex, entityHead, new Turret(entityBase));
            })  .WithReadOnly(voxLinks)
                .WithReadOnly(clanLinks)
                .WithReadOnly(voxDatas)
                .WithReadOnly(turretDatas)
                .WithNativeDisableContainerSafetyRestriction(voxDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static void SummonTurret(EntityCommandBuffer PostUpdateCommands, Entity summoner, int metaID, float3 position)
        {
            PostUpdateCommands.AddComponent(PostUpdateCommands.CreateEntity(), new SpawnTurret
            {
                summoner = summoner,
                metaID = metaID,
                position = position
            });
        }

        public static void SummonTurret(EntityManager EntityManager, Entity summoner, float3 position, int turretIndex)
        {
            EntityManager.AddComponentData(EntityManager.CreateEntity(), new SpawnTurret
            {
                summoner = summoner,
                position = position
            });
        }
    }
}