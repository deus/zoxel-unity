using Unity.Entities;
using Unity.Burst;
using Zoxel.AI;

namespace Zoxel.Turrets
{
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class TurretDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithAll<DestroyEntity, Turret>()
                .ForEach((Entity e, int entityInQueryIndex, in Turret turret) =>
			{
                PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, turret.baseTurret);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}