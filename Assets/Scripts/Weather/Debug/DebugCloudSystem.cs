using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Lines;

namespace Zoxel.Weather
{
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class DebugCloudSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (WeatherManager.instance == null) return;
            if (!WeatherManager.instance.weatherSettings.debugClouds) return;
            var elapsedTime = World.Time.ElapsedTime;
            var linePrefab = RenderLineGroup.linePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<Cloud>()
                .ForEach((in Body body, in Translation position) =>
            {
                RenderLineGroup.CreateCubeLines(PostUpdateCommands, linePrefab, elapsedTime, position.Value, quaternion.identity, body.size, 1); 
            }).Run();
        }

    }
}