using Unity.Entities;

namespace Zoxel.Weather.Skybox
{
    // Attached to Camera
    public struct SetSkyColor : IComponentData
    {
        public Color skyColor;
        public double startedTime;
        public float delayTime;

        public SetSkyColor(Color skyColor)
        {
            this.skyColor = skyColor;
            this.startedTime = 0;
            this.delayTime = 0;
        }

        public SetSkyColor(Color skyColor, double startedTime, float delayTime)
        {
            this.skyColor = skyColor;
            this.startedTime = startedTime;
            this.delayTime = delayTime;
        }
    }
}