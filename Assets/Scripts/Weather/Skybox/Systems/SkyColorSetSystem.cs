using Unity.Entities;

namespace Zoxel.Weather.Skybox
{
    //! Used to set colour to the sky of the camera.
    /**
    *   Use a MaterialSkyColor component on top of cameras. Then another system that pushes that change into backgroundColor, or directly to SkyMaterial.
    */
    [UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class SkyColorSetSystem : SystemBase
    {
        const string skyColorName = "skyColor";
        const string skyDarknessName = "skyDarkness";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (WeatherManager.instance == null) return;
            var elapsedTime = World.Time.ElapsedTime;
            var skyMaterial = WeatherManager.instance.WeatherSettings.skyMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            Entities.ForEach((Entity e, in SetSkyColor setSkyColor) =>
            {
                if (elapsedTime - setSkyColor.startedTime >= setSkyColor.delayTime)
                {
                    PostUpdateCommands.RemoveComponent<SetSkyColor>(e);
                    skyMaterial.SetColor(skyColorName, setSkyColor.skyColor.GetColor());
                    skyMaterial.SetFloat(skyDarknessName, 1f);
                }
            }).WithoutBurst().Run();
        }
    }
}