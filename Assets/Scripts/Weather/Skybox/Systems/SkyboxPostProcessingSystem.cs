using Unity.Entities;
using Zoxel.Transforms;
using Zoxel.Cameras.PostProcessing;

namespace Zoxel.Weather.Skybox
{
    //! Updates the camera to the sky PostExposure value.
    [UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class SkyboxPostProcessingSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((ref PostExposure postExposure, in UpdatePostProcessing updatePostProcessing, in VoxLink voxLink) =>
            {
                if (updatePostProcessing.type == PostProcessingProfileType.Game)
                {
                    if (HasComponent<Sky>(voxLink.vox))
                    {
                        var sky = EntityManager.GetComponentData<Sky>(voxLink.vox);
                        postExposure.postExposure = sky.postExposure;
                    }
                }
            }).WithoutBurst().Run();
        }
    }
}
                        /*var cameraObject = gameObjectLink.gameObject;
                        var cameraVolume = cameraObject.GetComponent<Volume>();
                        var volume = cameraObject.GetComponent<Volume>();
                        ColorAdjustments colorAdjustment;
                        volume.profile.TryGet<ColorAdjustments>(out colorAdjustment);
                        colorAdjustment.postExposure.value = sky.postExposure;*/