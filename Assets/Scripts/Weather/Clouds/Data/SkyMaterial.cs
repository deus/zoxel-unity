using Unity.Entities;
using System;

namespace Zoxel.Weather
{
    //! The material of the worlds sky.
    public struct SkyMaterial : ISharedComponentData, IEquatable<SkyMaterial>
    {
        public UnityEngine.Material skyMaterial;

        public SkyMaterial(UnityEngine.Material skyMaterial)
        {
            this.skyMaterial = skyMaterial;
        }

        public bool Equals(SkyMaterial obj)
        {
            return skyMaterial == obj.skyMaterial;
        }

        public override int GetHashCode()
        {
            return skyMaterial.GetHashCode(); //1371622046 + EqualityComparer<Mesh>.Default.GetHashCode(mesh);
        }
    }
}