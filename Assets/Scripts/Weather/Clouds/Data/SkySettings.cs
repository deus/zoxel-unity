using Unity.Entities;
using System;

namespace Zoxel.Weather
{
    //! Settings for our sky.
    [Serializable]
    public struct SkySettings
    {
        public Color color;
        public Color fogColor;
        public float fogDensity;
        public float lightValue;
        // post processing
        public float postExposure;
        public float noiseIntensity;
        public float vignetteIntensity;
        // skybox
        public float skyDarkness;
        public float sunSize;
        public float sunSizeConvergence;
        public float atmosphereThickness;
        public float exposure;
    }
}