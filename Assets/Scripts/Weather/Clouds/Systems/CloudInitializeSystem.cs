using Unity.Entities;
using Zoxel.Rendering;
using Zoxel.Voxels;

namespace Zoxel.Weather
{
    //! Initialize Clouds material.
    /**
    *   - Material Initialize System -
    */
    [UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class CloudInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null) return;
            var cloudMaterial = MaterialsManager.instance.materials.cloudMaterial;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, Cloud>()
                .ForEach((Entity e) =>
            {
                PostUpdateCommands2.SetSharedComponentManaged(e, new EntityMaterials(cloudMaterial));
            }).WithoutBurst().Run();
        }
    }
}