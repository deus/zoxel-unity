using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Weather
{
	//! Destroys rain cloud with the link to it.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class RainCloudLinkDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithAll<DestroyEntity, RainCloudLink>()
				.ForEach((int entityInQueryIndex, in RainCloudLink rainCloudLink) =>
			{
				if (HasComponent<RainCloud>(rainCloudLink.rainCloud))
				{
					PostUpdateCommands.DestroyEntity(entityInQueryIndex, rainCloudLink.rainCloud);
				}
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}