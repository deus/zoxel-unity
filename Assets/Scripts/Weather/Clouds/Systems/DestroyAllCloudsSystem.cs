using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Weather
{
    //! Destroys all clouds in existence.
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class DestroyAllCloudsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery clouds;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            clouds = GetEntityQuery(
                ComponentType.ReadOnly<Cloud>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var cloudEntities = clouds.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<DestroyAllClouds>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                for (int i = 0; i < cloudEntities.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, cloudEntities[i]);
                }
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
            })  .WithReadOnly(cloudEntities)
                .WithDisposeOnCompletion(cloudEntities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}