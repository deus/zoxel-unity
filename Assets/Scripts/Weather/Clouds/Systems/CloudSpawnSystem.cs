﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Animations;
using Zoxel.Rendering;

namespace Zoxel.Weather
{
    //! Spawns cloud objects into the world!
    /**
    *   \todo Lighting to effect clouds
    *   \todo Prepopulate clouds.
    *   \todo Spawn clouds around planet sides.
    *   \todo Base cloud voxels on points in space rather than voxels, use voxels to just render them
    *   \todo Clouds to combine when moving differently.
    *   \todo Rain clouds - gray - rain drops fall out of them.
    *   \todo Rain drop sounds - when they hit the ground.
    *   \todo Move clouds based on voxel winds.
    */
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class CloudSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery cloudsQuery;
        private EntityQuery planetsQuery;
        private Entity cloudPrefab;
        const int maxChecks = 200;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            cloudsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Cloud>(),
                ComponentType.ReadOnly<Translation>());
            planetsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<VoxScale>());
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(SpawnVoxChunk),
                typeof(GenerateVox),
                typeof(Seed),
                typeof(GenerateVoxData),
                typeof(DestroyEntityInTime),
                typeof(LerpPosition),
                typeof(Cloud), 
                typeof(ZoxID),
                typeof(Body),
                typeof(DisableAO),
                typeof(DisableVoxelNoise),
                typeof(Model),
                typeof(Vox),
                typeof(ChunkDimensions),
                typeof(VoxScale),
                typeof(VoxColors),
                typeof(VoxLink),
                typeof(EntityMaterials),
                typeof(MaterialBaseColor),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld)
            );
            cloudPrefab = EntityManager.CreateEntity(archetype);
            EntityManager.SetComponentData(cloudPrefab, new NonUniformScale { Value = new float3(1,1,1) });
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (WeatherManager.instance == null)
            {
                return;
            }
            // Settings
            var cloudSpawnRate = WeatherManager.instance.weatherSettings.cloudSpawnRate;
            var cloudLifetime = WeatherManager.instance.weatherSettings.cloudLifetime; // new float2(240f, 280f);
            var cloudScale = WeatherManager.instance.weatherSettings.cloudScale;
            var minSize = WeatherManager.instance.weatherSettings.cloudMinSize; // new int3(16, 12, 16);
            var maxSize = WeatherManager.instance.weatherSettings.cloudMaxSize; // new int3(48, 32, 48);
            var inputVoxScale = new float3(1f, 1f, 1f) * cloudScale;
            var cloudHeightVariation = WeatherManager.instance.weatherSettings.cloudHeightVariation;
            var elapsedTime = World.Time.ElapsedTime;
            var cloudPrefab = this.cloudPrefab;
            var cloudSpawnChance = 90;
            var cloudSpawnVariation = 20f;
            var cloudHeightRadiusMultiplier = 1.7f;
            var cloudWidthRadiusMultiplier = 2.2f;
            // Spawn initialize 100 clouds
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var cloudEntities = cloudsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var cloudTranslations = GetComponentLookup<Translation>(true);
            var cloudBodies = GetComponentLookup<Body>(true);
            // var planetEntities = planetsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            // Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var planets = GetComponentLookup<Planet>(true);
            var voxScales = GetComponentLookup<VoxScale>(true);
            // planetEntities.Dispose();
            Dependency = Entities
                .WithAll<InitializeEntity, CloudSpawner>()
                .ForEach((Entity e, int entityInQueryIndex, ref CloudSpawner cloudSpawner, in VoxLink voxLink) => // in Planet planet, in VoxScale voxScale) =>
            {
                if (!planets.HasComponent(voxLink.vox))
                {
                    return;
                }
                var planet = planets[voxLink.vox];
                var voxScale = voxScales[voxLink.vox];
                var outputVoxScale = voxScale.scale;
                outputVoxScale.x *= inputVoxScale.x;
                outputVoxScale.y *= inputVoxScale.y;
                outputVoxScale.z *= inputVoxScale.z;
                cloudSpawner.lastSpawned = elapsedTime;
                var spawnHeight = planet.radius * cloudHeightRadiusMultiplier * voxScale.scale.y;
                var spawnWidth = (int) (planet.radius * cloudWidthRadiusMultiplier * voxScale.scale.z);
                var spawnID = entityInQueryIndex + ((int) (elapsedTime * 1000f) + 666) % 1000000; // IDUtil.GenerateUniqueID();
                var random = new Random();
                random.InitState((uint)(spawnID));
                for (float i = -spawnWidth; i <= spawnWidth; i += cloudSpawnVariation)
                {
                    for (float j = -spawnWidth; j <= spawnWidth; j += cloudSpawnVariation)
                    {
                        if (random.NextInt(0, 100) >= cloudSpawnChance)
                        {
                            continue;
                        }
                        var spawnPosition = new float3(i, random.NextFloat(spawnHeight, spawnHeight + cloudHeightVariation), j);
                        var endPosition = spawnPosition;
                        endPosition.z = -spawnWidth;
                        var voxelDimensions = new int3(random.NextInt(minSize.x, maxSize.z), random.NextInt(minSize.y, maxSize.y), random.NextInt(minSize.z, maxSize.z));
                        var lifeTimePercentage = j / spawnWidth;
                        SpawnCloud(PostUpdateCommands, entityInQueryIndex, cloudPrefab, elapsedTime, spawnPosition, endPosition, voxelDimensions, spawnID, in random,
                            outputVoxScale, lifeTimePercentage * cloudLifetime);
                    }
                }
            })  .WithReadOnly(planets)
                .WithReadOnly(voxScales)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<InitializeEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref CloudSpawner cloudSpawner, in VoxLink voxLink) =>
            {
                if (!(elapsedTime - cloudSpawner.lastSpawned >= cloudSpawnRate))
                {
                    return;
                }
                if (!planets.HasComponent(voxLink.vox))
                {
                    return;
                }
                var planet = planets[voxLink.vox];
                var voxScale = voxScales[voxLink.vox];
                cloudSpawner.lastSpawned = elapsedTime;
                var outputVoxScale = voxScale.scale;
                outputVoxScale.x *= inputVoxScale.x;
                outputVoxScale.y *= inputVoxScale.y;
                outputVoxScale.z *= inputVoxScale.z;
                var spawnHeight = planet.radius * cloudHeightRadiusMultiplier * voxScale.scale.y;
                var spawnWidth = (int) (planet.radius * cloudWidthRadiusMultiplier * voxScale.scale.z);
                var positionDifference = new float3(0, 0, spawnWidth);
                //! \todo Get a position that's not intersecting any of the other clouds!
                var spawnID = entityInQueryIndex + ((int) (elapsedTime * 1000f) + 666) % 1000000; //IDUtil.GenerateUniqueID();
                var random = new Random();
                random.InitState((uint) spawnID);
                // find a position that doesn't intersect other clouds
                var spawnPosition = new float3(random.NextFloat(-spawnWidth, spawnWidth), random.NextFloat(spawnHeight, spawnHeight + cloudHeightVariation), 0);
                var voxelDimensions = new int3(random.NextInt(minSize.x, maxSize.z), random.NextInt(minSize.y, maxSize.y), random.NextInt(minSize.z, maxSize.z));
                var found = false;
                var checks = 0;
                var newPositionBounds = new Bounds();
                var otherBounds = new Bounds();
                while(!found && checks < maxChecks)
                {
                    checks++;
                    spawnPosition = new float3(random.NextFloat(-spawnWidth, spawnWidth), random.NextFloat(spawnHeight, spawnHeight + cloudHeightVariation), spawnWidth);
                    voxelDimensions = new int3(random.NextInt(minSize.x, maxSize.z), random.NextInt(minSize.y, maxSize.y), random.NextInt(minSize.z, maxSize.z));
                    newPositionBounds = new Bounds(spawnPosition, outputVoxScale * voxelDimensions.ToFloat3() / 2f);
                    // is position close to other clouds
                    var anyIntersects = false;
                    for (int i = 0; i < cloudEntities.Length; i++)
                    {
                        var cloudEntity = cloudEntities[i];
                        var cloudPosition = cloudTranslations[cloudEntity].Value;
                        var cloudBody = cloudBodies[cloudEntity].size;
                        otherBounds = new Bounds(cloudPosition, cloudBody);
                        if (newPositionBounds.Intersects(otherBounds))
                        {
                            anyIntersects = true;
                            // UnityEngine.Debug.LogError("Cloud Intersects " + checks);
                            break;
                        }
                    }
                    if (!anyIntersects)
                    {
                        found = true;
                        break;
                    }
                }
                if (checks == maxChecks)
                {
                    return;
                }
                var spawnPosition2 = spawnPosition;
                var endPosition = spawnPosition - positionDifference * 2f;
                SpawnCloud(PostUpdateCommands, entityInQueryIndex, cloudPrefab, elapsedTime, spawnPosition2, endPosition,  voxelDimensions, spawnID, in random,
                    outputVoxScale, cloudLifetime);
                // UnityEngine.Debug.DrawLine(position, position + math.rotate(rotation, new float3(0, 0, 3)), UnityEngine.Color.red);
                // PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                // UnityEngine.Debug.LogError("Cloud Spawned at: " + spawnPosition + " with Body: " + size + " at time " + elapsedTime);
            })  .WithReadOnly(cloudEntities)
                .WithDisposeOnCompletion(cloudEntities)
                .WithReadOnly(cloudTranslations)
                .WithReadOnly(cloudBodies)
                .WithReadOnly(planets)
                .WithReadOnly(voxScales)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static Entity SpawnCloud(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity cloudPrefab, double elapsedTime,
            float3 spawnPosition, float3 endPosition, int3 voxelDimensions, int spawnID, in Random random,
            float3 voxScale, float2 cloudLifetime)
        {
            var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, cloudPrefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ZoxID(spawnID));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ChunkDimensions(voxelDimensions));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new VoxScale(voxScale));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Translation { Value = spawnPosition });
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Rotation { Value = quaternion.identity });
            var voxelColors = new VoxColors();
            var brightness = random.NextInt(180, 255);
            voxelColors.SetAsColor(new Color(brightness, brightness, brightness));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, voxelColors);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Seed(spawnID));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new GenerateVoxData(GenerateVoxType.Cloud));
            var lifetime = random.NextFloat(cloudLifetime.x, cloudLifetime.y);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new DestroyEntityInTime(elapsedTime, lifetime));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new LerpPosition(elapsedTime, lifetime, spawnPosition, endPosition));
            var size = new float3(voxScale.x * voxelDimensions.x, voxScale.y * voxelDimensions.y, voxScale.z * voxelDimensions.z);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Body(size / 2f));
            return e2;
        }
    }
}
            // var spawnCloudEntity = EntityManager.CreateEntity();
            // EntityManager.AddComponent<CloudSpawner>(spawnCloudEntity);
            // EntityManager.AddComponentData(spawnCloudEntity, new CloudSpawner { lastSpawned = - (cloudSpawnRate * 0.9f)});
            // test bounds
            /*var boundsA = new Bounds(new float3(), new float3(3, 3, 3));
            var boundsB = new Bounds(new float3(2, 5, 2), new float3(4, 1, 4));
            UnityEngine.Debug.LogError("Intersects Test: " + boundsA.Intersects(boundsB));*/
        //const float spawnHeight = 142f;
        //const float spawnWidth = 120f;