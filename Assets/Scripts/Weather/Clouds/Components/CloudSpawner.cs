using Unity.Entities;

namespace Zoxel.Weather
{
    public struct CloudSpawner : IComponentData
    {
        public double lastSpawned;
    }
}