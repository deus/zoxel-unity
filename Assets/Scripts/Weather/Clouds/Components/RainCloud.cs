using Unity.Entities;

namespace Zoxel.Weather
{
    public struct RainCloud : IComponentData { public double lastRained; }
}