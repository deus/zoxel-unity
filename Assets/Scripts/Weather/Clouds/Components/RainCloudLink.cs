using Unity.Entities;

namespace Zoxel.Weather
{
    public struct RainCloudLink : IComponentData
    {
        public Entity rainCloud;

        public RainCloudLink(Entity rainCloud)
        {
            this.rainCloud = rainCloud;
        }
    }
}