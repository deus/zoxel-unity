using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;
using Zoxel.Voxels.Lighting;
using Zoxel.Voxels.Lighting.Authoring;

namespace Zoxel.Weather.Lights
{
    //! Converts Dirt to Grass or Grass to Dirt based on Sunlight.
    /**
    *   Every 20 seconds, do a check if grass has sunlight above it or not.
    *   Check also if dirt has sunlight above it.
    *   Uses light source from neighboring chunks.
    *   Only update mesh but not light or minivoxes.
    *   \todo Link a Soil to a Grass for growth and decay.
    *   \todo Base grass growth on gravity direction as well.
    *   \todo don't update chunk if voxels arround it are all solid
    */
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class GrassSunSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;
        private EntityQuery chunksQuery;
        private EntityQuery voxelsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            voxesQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.Exclude<BecomeDay>(),
				ComponentType.Exclude<BecomeNight>(),
				ComponentType.Exclude<RainTime>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<DayTime>(),
                ComponentType.ReadOnly<Planet>());
            chunksQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.ReadOnly<Chunk>(),
				ComponentType.ReadOnly<ChunkNeighbors>());
            voxelsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Voxel>(),
                ComponentType.ReadOnly<BlockDimensions>());
            RequireForUpdate<LightsSettings>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var lightsSettings = GetSingleton<LightsSettings>();
            if (lightsSettings.disableGrassSun)
            {
                return;
            }
            var seed = IDUtil.GenerateUniqueID();
            var maxGrassSunDivision = lightsSettings.maxGrassSunDivision;
            var sunlight = lightsSettings.sunlight;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxelLinks2 = GetComponentLookup<VoxelLinks>(true);
            var becomeDays = GetComponentLookup<BecomeDay>(true);
            var becomeNights = GetComponentLookup<BecomeNight>(true);
            var dayTimes = GetComponentLookup<DayTime>(true);
            var rainTimes = GetComponentLookup<RainTime>(true);
            voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunkLights2 = GetComponentLookup<ChunkLights>(true);
            chunkEntities.Dispose();
            var voxelEntities = voxelsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var voxelSoils = GetComponentLookup<VoxelSoil>(true);
            var voxelGrasss = GetComponentLookup<VoxelGrass>(true);
            voxelEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity, EntityBusy>()
                .WithAll<PlanetChunk>()
                .ForEach((Entity e, int entityInQueryIndex, ref Chunk chunk, ref ChunkTimer chunkTimer, in ChunkNeighbors chunkNeighbors, in ChunkLights chunkLights, in ChunkDivision chunkDivision, in VoxLink voxLink) =>
            {
                if (chunk.voxels.Length == 0 || chunkDivision.viewDistance >= maxGrassSunDivision || elapsedTime - chunkTimer.lastTicked < 15 || !voxelLinks2.HasComponent(voxLink.vox)
                    || becomeDays.HasComponent(voxLink.vox) || becomeNights.HasComponent(voxLink.vox) || !dayTimes.HasComponent(voxLink.vox) || rainTimes.HasComponent(voxLink.vox))
                {
                    return;
                }
                chunkTimer.lastTicked = elapsedTime;
                var voxelDimensions = chunk.voxelDimensions;
				var chunkLightsAdjacent = new ChunkLightsAdjacentNeighborsData(in chunkNeighbors, in chunkLights2);
                var voxelLinks = voxelLinks2[voxLink.vox];
                var random = new Random();
                random.InitState((uint) (seed + entityInQueryIndex * 66));
                var voxelArrayIndex = 0;
                // var position = new int3();
                var soilUpdates = new NativeList<int>();
                var grassUpdates = new NativeList<int>();
				int3 localPosition;
				for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
				{
					for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
					{
						for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
						{
                            var voxelIndex = chunk.voxels[voxelArrayIndex];
                            var voxelIndexMinusAir = voxelIndex - 1;
                            if (voxelIndexMinusAir >= voxelLinks.voxels.Length)
                            {
                                voxelArrayIndex++;
                                continue;
                            }
                            var voxelEntity = voxelLinks.voxels[voxelIndexMinusAir];
                            var isSoil = voxelSoils.HasComponent(voxelEntity);
                            var isGrass = voxelGrasss.HasComponent(voxelEntity);
                            if (!isSoil && !isGrass)
                            {
                                voxelArrayIndex++;
                                continue;
                            }
                            // get adjacent lights using ChunkLightsAdjacent
							// Get our Neighbor Voxels!
							var lightLeft = chunkLightsAdjacent.GetLightValue(in chunkLights, localPosition.Left(), voxelDimensions);
							var lightRight = chunkLightsAdjacent.GetLightValue(in chunkLights, localPosition.Right(), voxelDimensions);
							var lightDown = chunkLightsAdjacent.GetLightValue(in chunkLights, localPosition.Down(), voxelDimensions);
							var lightUp = chunkLightsAdjacent.GetLightValue(in chunkLights, localPosition.Up(), voxelDimensions);
							var lightBackward = chunkLightsAdjacent.GetLightValue(in chunkLights, localPosition.Backward(), voxelDimensions);
							var lightForward = chunkLightsAdjacent.GetLightValue(in chunkLights, localPosition.Forward(), voxelDimensions);
                            if (isSoil)
                            {
                                //! \todo Should actually set grass based on lit up sides
                                if (lightLeft == sunlight || lightRight == sunlight || lightDown == sunlight || lightUp == sunlight || lightBackward == sunlight || lightForward == sunlight)
                                {
                                    soilUpdates.Add(voxelArrayIndex);
                                }
                            }
                            else if (isGrass)
                            {
                                var minGrassSunlight = sunlight - 3;
                                if (lightLeft < minGrassSunlight && lightRight < minGrassSunlight && lightDown < minGrassSunlight && lightUp < minGrassSunlight && lightBackward < minGrassSunlight && lightForward < minGrassSunlight)
                                {
                                    grassUpdates.Add(voxelArrayIndex);
                                }
                            }
                            voxelArrayIndex++;
                        }
                    }
                }
                // if updated, update with chunk builder
                if (soilUpdates.Length > 0 || grassUpdates.Length > 0)
                {
                    if (soilUpdates.Length > 0)
                    {
                        var soilUpdateIndex = soilUpdates[random.NextInt(0, soilUpdates.Length)];
                        chunk.voxels[soilUpdateIndex] = (byte) (chunk.voxels[soilUpdateIndex] + 1);
                    }
                    if (grassUpdates.Length > 0)
                    {
                        var grassUpdateIndex = grassUpdates[random.NextInt(0, grassUpdates.Length)];
                        chunk.voxels[grassUpdateIndex] = (byte) (chunk.voxels[grassUpdateIndex] - 1);
                    }
                    PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<VoxelBuilderMeshOnly>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<SaveChunk>(entityInQueryIndex, e);
                }
                soilUpdates.Dispose();
                grassUpdates.Dispose();
            })  .WithReadOnly(voxelLinks2)
                .WithReadOnly(becomeDays)
                .WithReadOnly(becomeNights)
                .WithReadOnly(dayTimes)
                .WithReadOnly(rainTimes)
				.WithReadOnly(chunkLights2)
                .WithReadOnly(voxelSoils)
                .WithReadOnly(voxelGrasss)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}