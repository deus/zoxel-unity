using Unity.Entities;

namespace Zoxel.Weather.UI
{
    //! An event to spawn Weaher's UI.
    public struct SpawnWeatherUI : IComponentData { }
}