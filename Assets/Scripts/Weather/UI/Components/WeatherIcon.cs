using Unity.Entities;

namespace Zoxel.Weather.UI
{
    //! A tag for the Time's UI.
    public struct WeatherIcon : IComponentData { }
}