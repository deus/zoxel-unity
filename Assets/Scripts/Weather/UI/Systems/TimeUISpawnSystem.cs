using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Textures;
using Zoxel.Cameras;
using Zoxel.UI;
using Zoxel.Transforms;

namespace Zoxel.Weather.UI
{
    //! Spawns a WeatherUI Entity!
    /**
    *   - UI Spawn System -
    *   Has a clock render text on the left side.
    *   On the right side lay an icon for weather, sunny, or rain cloud icons.
    */
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class TimeUISpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private Entity weatherUIPrefab;
        private Entity weatherLabelPrefab;
        private Entity timePrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.ReadOnly<SpawnWeatherUI>(),
                ComponentType.ReadOnly<CharacterLink>(),
                ComponentType.Exclude<DelayEvent>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            InitializePrefabs();
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var whiteColor = new Color(UnityEngine.Color.white);
            var uiDatam = UIManager.instance.uiDatam;
            var weatherUIStyle = uiDatam.weatherUI.GetStyle(uiScale);
            var gameOrbitDepth = CameraManager.instance.cameraSettings.uiDepth;
            var iconSize = uiDatam.GetIconSize(uiScale);
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            var actionbarPanelColor = new Color(uiDatam.actionbarPanelColor);
            var weatherUIPrefab = this.weatherUIPrefab;
            var timePrefab = this.timePrefab;
            var weatherLabelPrefab = this.weatherLabelPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var spawnWeatherUIEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var characterLinks = GetComponentLookup<CharacterLink>(true);
            Dependency = Entities
                .ForEach((Entity e, int entityInQueryIndex, in UILink uiLink, in CameraLink cameraLink, in VoxLink voxLink) =>
            {
                if (cameraLink.camera.Index == 0)
                {
                    return;
                }
                for (int i = 0; i < spawnWeatherUIEntities.Length; i++)
                {
                    var e2 = spawnWeatherUIEntities[i];
                    var characterLink = characterLinks[e2];
                    if (characterLink.character != e)
                    {
                        continue;
                    }
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e2);   // destroy spawn event
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));
                    // Spawn Weather UI
                    var panelSize = new float2(iconSize.x * 2.2f, iconSize.y);
                    var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, weatherUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CharacterLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CameraLink(cameraLink.camera));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new Size2D(panelSize));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new PanelPosition(new float3(- 0.6f * iconSize.y, - 3.5f * iconSize.y, 0)));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new UIAnchor(AnchorUIType.TopRight));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new UIPosition(gameOrbitDepth));
                    UICoreSystem.SetTextureFrame(PostUpdateCommands, entityInQueryIndex, e3, in weatherUIStyle.frameGenerationData); // weatherUIStyle.frameColor, 32);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new MaterialBaseColor { Value = weatherUIStyle.color.ToFloat4() });

                    // Spawn TimeUI - Label - On left
                    var timeUISize = new float2(panelSize.x - textPadding.x / 2f, panelSize.y / 2f);
                    var timeUIPosition = new float3(0, -timeUISize.y / 2f, 0);
                    var timeUI = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, timePrefab, e3, timeUIPosition,
                        timeUISize, weatherUIStyle.color);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, timeUI, 0);
                    UICoreSystem.SetRenderText(PostUpdateCommands, entityInQueryIndex, timeUI,
                        weatherUIStyle.textColor, weatherUIStyle.textOutlineColor,
                        new Text(), iconSize.y, textPadding / 5f);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, timeUI, voxLink);

                    // Spawn WeatherIcon - Icon - On Right
                    var weatherLabelTextPosition = new float3(0, timeUISize.y / 2f, 0);
                    var weatherLabelEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, weatherLabelPrefab, e3, weatherLabelTextPosition,
                        timeUISize, weatherUIStyle.color);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, weatherLabelEntity, 1);
                    var weatherLabelText = new Text();
                    /*weatherLabelText.AddChar('C');
                    weatherLabelText.AddChar('l');
                    weatherLabelText.AddChar('o');
                    weatherLabelText.AddChar('u');
                    weatherLabelText.AddChar('d');
                    weatherLabelText.AddChar('y');*/
                    UICoreSystem.SetRenderText(PostUpdateCommands, entityInQueryIndex, weatherLabelEntity,
                        weatherUIStyle.textColor, weatherUIStyle.textOutlineColor,
                        weatherLabelText, iconSize.y, textPadding / 5f);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, weatherLabelEntity, voxLink);

                    // Link to our weatherUI
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawned(2));
                    break;  // only one crosshair per character atm
                }
            })  .WithReadOnly(spawnWeatherUIEntities)
                .WithDisposeOnCompletion(spawnWeatherUIEntities)
                .WithReadOnly(characterLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private void InitializePrefabs()
        {
            // Get PanelBasic, Label and Icon prefabs from UICoreSystem.
            if (weatherUIPrefab.Index != 0)
            {
                return;
            }
            weatherUIPrefab = EntityManager.Instantiate(UICoreSystem.panelPrefab);
            EntityManager.AddComponent<Prefab>(weatherUIPrefab);
            EntityManager.AddComponent<WeatherUI>(weatherUIPrefab);
            EntityManager.AddComponent<GameOnlyUI>(weatherUIPrefab);
            EntityManager.AddComponent<VoxLink>(weatherUIPrefab);
            EntityManager.AddComponent<OnChildrenSpawned>(weatherUIPrefab);
            EntityManager.SetComponentData(weatherUIPrefab, new PanelUI(PanelType.WeatherUI));
            EntityManager.AddComponent<PanelPosition>(weatherUIPrefab);
            EntityManager.RemoveComponent<GridUI>(weatherUIPrefab);
            EntityManager.SetComponentData(weatherUIPrefab, new NonUniformScale { Value = new float3(1, 1, 1) });
            EntityManager.SetComponentData(weatherUIPrefab, new MaterialBaseColor { Value = new float4(1, 0, 0, 1) });
            timePrefab = EntityManager.Instantiate(UICoreSystem.labelPrefab);
            EntityManager.AddComponent<Prefab>(timePrefab);
            EntityManager.AddComponent<TimeUI>(timePrefab);
            EntityManager.AddComponent<RenderTextFit>(timePrefab);
            EntityManager.RemoveComponent<RenderTextDirty>(timePrefab);
            EntityManager.AddComponent<VoxLink>(timePrefab);
            weatherLabelPrefab = EntityManager.Instantiate(UICoreSystem.labelPrefab);
            EntityManager.AddComponent<Prefab>(weatherLabelPrefab);
            EntityManager.AddComponent<RenderTextFit>(weatherLabelPrefab);
            EntityManager.AddComponent<DaysPassedUI>(weatherLabelPrefab);
            EntityManager.RemoveComponent<RenderTextDirty>(weatherLabelPrefab);
            EntityManager.AddComponent<VoxLink>(weatherLabelPrefab);
        }
    }
}