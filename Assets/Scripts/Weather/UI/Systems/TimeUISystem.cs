using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;

namespace Zoxel.Weather.UI
{
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class TimeUISystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery worldsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            worldsQuery = GetEntityQuery(ComponentType.ReadOnly<WorldTime>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (worldsQuery.IsEmpty)
            {
                return;
            }
			// pass in WorldTimes
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var worldEntities = worldsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var worldTimes = GetComponentLookup<WorldTime>(true);
            var onMinutePasseds = GetComponentLookup<OnMinutePassed>(true);
            var worldTimeLoadeds = GetComponentLookup<WorldTimeLoaded>(true);
            worldEntities.Dispose();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity, RenderTextDirty>()
                .WithAll<TimeUI>()
                .ForEach((Entity e, int entityInQueryIndex, ref RenderText renderText, in VoxLink voxLink) =>
            {
                if ((!onMinutePasseds.HasComponent(voxLink.vox) && !worldTimeLoadeds.HasComponent(voxLink.vox)) || !worldTimes.HasComponent(voxLink.vox))
                {
                    return;
                }
                var worldTime = worldTimes[voxLink.vox];
                UpdateTimeUI(PostUpdateCommands, e, entityInQueryIndex, in worldTime, ref renderText);
            })  .WithReadOnly(worldTimes)
                .WithReadOnly(onMinutePasseds)
                .WithReadOnly(worldTimeLoadeds)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<DestroyEntity, RenderTextDirty>()
                .WithAll<InitializeEntity, TimeUI>()
                .ForEach((Entity e, int entityInQueryIndex, ref RenderText renderText, in VoxLink voxLink) =>
            {
                if (!worldTimes.HasComponent(voxLink.vox))
                {
                    return;
                }
                var worldTime = worldTimes[voxLink.vox];
                UpdateTimeUI(PostUpdateCommands, e, entityInQueryIndex, in worldTime, ref renderText);
            })  .WithReadOnly(worldTimes)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static void UpdateTimeUI(EntityCommandBuffer.ParallelWriter PostUpdateCommands, Entity e, int entityInQueryIndex, in WorldTime worldTime, ref RenderText renderText)
        {
            // UnityEngine.Debug.LogError("World TimeUI Updated: " + worldTime.hoursPassed);
            var text = new Text();
            if (worldTime.hoursPassed < 10)
            {
                text.AddInteger(0);
            }
            text.AddInteger(worldTime.hoursPassed);
            text.AddChar(':');
            if (worldTime.minutesPassed < 10)
            {
                text.AddInteger(0);
            }
            text.AddInteger(worldTime.minutesPassed);
            /*text.AddChar('1');
            text.AddChar('1');
            text.AddChar(':');
            text.AddChar('3');
            text.AddChar('6');*/
            // if hours passed is greater than 12, make PM
            if (worldTime.isPM == 1)
            {
                text.AddChar('P');
            }
            else
            {
                text.AddChar('A');
            }
            text.AddChar('M');
            if (renderText.SetText(text))
            {
                PostUpdateCommands.AddComponent<RenderTextDirty>(entityInQueryIndex, e);
            }
            else
            {
                text.Dispose();
            }
        }
    }
}