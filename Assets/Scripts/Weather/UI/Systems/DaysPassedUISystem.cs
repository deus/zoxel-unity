using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;

namespace Zoxel.Weather.UI
{
    //! Sets the day in the UI
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class DaysPassedUISystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery worldsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            worldsQuery = GetEntityQuery(ComponentType.ReadOnly<WorldTime>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (worldsQuery.IsEmpty)
            {
                return;
            }
			// pass in WorldTimes
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var worldEntities = worldsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var worldTimes = GetComponentLookup<WorldTime>(true);
            var onDayPasseds = GetComponentLookup<OnDayPassed>(true);
            var worldTimeLoadeds = GetComponentLookup<WorldTimeLoaded>(true);
            worldEntities.Dispose();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity, RenderTextDirty>()
                .WithAll<DaysPassedUI>()
                .ForEach((Entity e, int entityInQueryIndex, ref RenderText renderText, in VoxLink voxLink) =>
            {
                if (!onDayPasseds.HasComponent(voxLink.vox) && !worldTimeLoadeds.HasComponent(voxLink.vox))
                {
                    return;
                }
                if (!worldTimes.HasComponent(voxLink.vox))
                {
                    return;
                }
                var worldTime = worldTimes[voxLink.vox];
                UpdateDaysUI(PostUpdateCommands, e, entityInQueryIndex, in worldTime, ref renderText);
            })  .WithReadOnly(worldTimes)
                .WithReadOnly(onDayPasseds)
                .WithReadOnly(worldTimeLoadeds)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<DestroyEntity, RenderTextDirty>()
                .WithAll<InitializeEntity, DaysPassedUI>()
                .ForEach((Entity e, int entityInQueryIndex, ref RenderText renderText, in VoxLink voxLink) =>
            {
                if (!worldTimes.HasComponent(voxLink.vox))
                {
                    return;
                }
                var worldTime = worldTimes[voxLink.vox];
                UpdateDaysUI(PostUpdateCommands, e, entityInQueryIndex, in worldTime, ref renderText);
            })  .WithReadOnly(worldTimes)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static void UpdateDaysUI(EntityCommandBuffer.ParallelWriter PostUpdateCommands, Entity e, int entityInQueryIndex, in WorldTime worldTime, ref RenderText renderText)
        {
            var text = new Text();
            text.AddChar('D');
            text.AddChar('a');
            text.AddChar('y');
            text.AddChar(' ');
            text.AddInteger(worldTime.daysPassed);
            //text.AddChar('1');
            //text.AddChar('2');
            if (renderText.SetText(text))
            {
                PostUpdateCommands.AddComponent<RenderTextDirty>(entityInQueryIndex, e);
            }
            else
            {
                text.Dispose();
            }
        }
    }
}