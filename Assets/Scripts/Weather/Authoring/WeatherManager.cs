using Unity.Mathematics;
using UnityEngine;
using Zoxel.Audio;

namespace Zoxel.Weather
{
    //! Managers weather settings.
    public partial class WeatherManager : MonoBehaviour
    {
        public static WeatherManager instance;
        [Header("Data")]
        [SerializeField] public WeatherSettingsDatam WeatherSettings;
        [HideInInspector] public WeatherSettings realWeatherSettings;
        public MusicDatam rainMusic;
        // public UnityEngine.Light directionalLight;

        public WeatherSettings weatherSettings
        {
            get
            { 
                return realWeatherSettings;
            }
        }

        public void Awake()
        {
            instance = this;
            realWeatherSettings = WeatherSettings.weatherSettings;
            var isHeadlessMode = NetworkUtil.IsHeadless();
            if (!isHeadlessMode)
            {
                return;
            }
            UnityEngine.RenderSettings.fog = !WeatherManager.instance.weatherSettings.disableFog;
            if (WeatherManager.instance.weatherSettings.disableFog)
            {
                UnityEngine.RenderSettings.fogDensity = 0;
            }
        }
    }
}