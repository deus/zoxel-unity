using UnityEngine;

namespace Zoxel.Weather
{
    [CreateAssetMenu(fileName = "WeatherSettings", menuName = "ZoxelSettings/WeatherSettings")]
    public partial class WeatherSettingsDatam : ScriptableObject
    {
        public WeatherSettings weatherSettings;
        public Material skyMaterial;
    }
}