using UnityEngine;
using Unity.Mathematics;
using System;

namespace Zoxel.Weather
{
    //! Settings data for our weather namespace.
    [Serializable]
    public struct WeatherSettings
    {
        [Header("Day Cycle Speeds")]
        public float timeSpeed;             
        public float timePerDay;            // = 90;
        public float timePerNight;          // = 60;

        [Header("Lighting Times")]
        public int morningTime;
        public int nightTime;

        [Header("Lighting Transitions")]
        //! How fast the lighting transition goes for.
        public float dayTimeTransitionTime;     // = 6;
        //! How fast each tick goes for.
        public byte dayTimeCounter;         // = 128;
        
        [Header("Fog")]
        public SkySettings daySettings;
        public SkySettings nightSettings;

        [Header("Clouds")]
        public float cloudSpawnRate;        // 4f
        public float2 cloudLifetime;        // new float2(240f, 280f);
        public float cloudScale;            // 1f
        public int3 cloudMinSize;           // new int3(16, 12, 16);
        public int3 cloudMaxSize;           // new int3(48, 32, 48);
        public int cloudHeightVariation;      // 142

        [Header("Rain")]
        public bool forceRain;

        [Header("SunGrass")]
        public int maxGrassSunDivision;     // 3?

        [Header("Disables")]
        public bool disableNighttime;
        public bool disableFog;
        public bool disableClouds;
        
        [Header("Debug Draws")]
        public bool debugClouds;

        public void DrawUI()
        {
            GUILayout.Label(" [voxels] ");
        }

        public void DrawDebug()
        {
            GUILayout.Label("Stats");
            disableNighttime = GUILayout.Toggle(disableNighttime, "Disable Nighttime");
        }
    }
}