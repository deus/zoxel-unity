using Unity.Entities;
using Zoxel.Cameras;
using Zoxel.Transforms;
using Zoxel.Cameras.PostProcessing;

namespace Zoxel.Weather
{
    //! Used to set colour to the sky of the camera based on world.
    [UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class CameraSetWorldSkySystem : SystemBase
    {
        const string skyColorName = "skyColor";
        const string skyDarknessName = "skyDarkness";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (WeatherManager.instance == null) return;
            var elapsedTime = World.Time.ElapsedTime;
            var skyMaterial2 = WeatherManager.instance.WeatherSettings.skyMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            Entities.ForEach((Entity e, ref PostExposure postExposure, in SetCameraAsSky setCameraAsSky, in VoxLink voxLink, in GameObjectLink gameObjectLink) =>
            {
                if (elapsedTime - setCameraAsSky.startedTime >= setCameraAsSky.delayTime)
                {
                    PostUpdateCommands.RemoveComponent<SetCameraAsSky>(e);
                    if (voxLink.vox.Index == 0)
                    {
                        return;
                    }
                    var sky = EntityManager.GetComponentData<Sky>(voxLink.vox);
                    var skyMaterial = EntityManager.GetSharedComponentManaged<SkyMaterial>(voxLink.vox);
                    // skyMaterial2.SetColor(skyColorName, sky.color.GetColor());
                    // gameObjectLink.gameObject.GetComponent<UnityEngine.Camera>().backgroundColor = sky.color.GetColor();
                    postExposure.postExposure = sky.postExposure;
                    PostUpdateCommands.AddComponent(e, new UpdatePostProcessing(PostProcessingProfileType.Game));
                    CameraSetWorldSkySystem.SetSkyRenderSettings(in sky, in skyMaterial.skyMaterial);
                    // UnityEngine.Debug.LogError("2 sky.postExposure set to: " + sky.postExposure);
                    /*var volume = gameObjectLink.gameObject.GetComponent<Volume>();
                    ColorAdjustments colorAdjustment;
                    volume.profile.TryGet<ColorAdjustments>(out colorAdjustment);
                    colorAdjustment.postExposure.value = sky.postExposure;*/
                }
            }).WithoutBurst().Run();
        }
        
        // Can I do this per camera? Skybox per camera?
        public static void SetSkyRenderSettings(in Sky sky, in UnityEngine.Material skyMaterial)
        {
            UnityEngine.RenderSettings.fogColor = sky.fogColor.GetColor() * sky.skyDarkness;
            if (!WeatherManager.instance.weatherSettings.disableFog)
            {
                UnityEngine.RenderSettings.fogDensity = sky.fogDensity;
            }
            // UnityEngine.RenderSettings.skybox
            if (skyMaterial != null)
            {
                skyMaterial.SetFloat(skyDarknessName, sky.skyDarkness);
                skyMaterial.SetColor(skyColorName, sky.color.GetColor());
            }
        }
    }
}

/*Bloom bloom;
ChromaticAberration chromaticAberration;
Vignette vignette;
FilmGrain filmGrain;
LensDistortion lensDistortion;
ColorAdjustments colorAdjustment;*///
// var skyMaterial = EntityManager.GetSharedComponentData<SkyMaterial>(voxLink.vox);
// skyMaterial.skyMaterial.SetColor("_BaseColor", sky.color.GetColor());