using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;
using Zoxel.Cameras;
using Zoxel.Voxels.Lighting;
using Zoxel.Voxels.Lighting.Authoring;

namespace Zoxel.Weather
{
    //! Handles BecomeNight event.
    /**
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class BecomeNightSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery camerasQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            camerasQuery = GetEntityQuery(
                ComponentType.ReadOnly<Camera>(),
                ComponentType.ReadOnly<VoxLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate<LightsSettings>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var lightsSettings = GetSingleton<LightsSettings>();
            byte dayTimeCounter = WeatherManager.instance.weatherSettings.dayTimeCounter;
            var sunlight = lightsSettings.sunlight;
            var rainLight = lightsSettings.rainLight;
            var moonlight = lightsSettings.moonlight;
            var deltaTime = World.Time.DeltaTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var cameraEntities = camerasQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<SetWorldLight>()
                .WithAll<BecomeNight>()
                .ForEach((Entity e, int entityInQueryIndex, ref Sky sky, ref Skylight skylight, ref BecomeNight becomeNight, in Vox vox, in RainCloudLink rainCloudLink) =>
            {
                if (becomeNight.init == 0)
                {
                    becomeNight.init = 1;
                    becomeNight.timePassed = 0;
                    PostUpdateCommands.AddComponent<NightTime>(entityInQueryIndex, e);
                    if (HasComponent<DayTime>(e))
                    {
                        PostUpdateCommands.RemoveComponent<DayTime>(entityInQueryIndex, e);
                    }
                    if (skylight.skylight == 0)
                    {
                        sky.firstLight = sunlight;
                    }
                    else
                    {
                        sky.firstLight = skylight.skylight;
                    }
                    sky.targetLight = moonlight;
                }
                becomeNight.timePassed += deltaTime;
                var lerp = ((float) becomeNight.timePassed) / sky.fadeTime;
                // SetAsNight(PostUpdateCommands, ref sky, vox.chunks.Length > 0, in skyMaterial, lerp, true, false);
                BecomeDaySystem.SetAsNight(PostUpdateCommands, entityInQueryIndex, ref sky, lerp, cameraEntities, HasComponent<RainTime>(e));
                if (sky.counter == dayTimeCounter)
                {
                    sky.counter = 0;
                    var newLight = (byte) math.lerp(sky.firstLight, sky.targetLight, lerp);
                    if (newLight != skylight.skylight)
                    {
                        skylight.skylight = newLight;
                        PostUpdateCommands.AddComponent<SetWorldLight>(entityInQueryIndex, e);
                    }
                }
                else
                {
                    sky.counter++;
                }
                if (becomeNight.timePassed >= sky.fadeTime)
                {
                    PostUpdateCommands.RemoveComponent<BecomeNight>(entityInQueryIndex, e);
                    if (sky.targetLight != skylight.skylight)
                    {
                        skylight.skylight = sky.targetLight;
                        PostUpdateCommands.AddComponent<SetWorldLight>(entityInQueryIndex, e);
                    }
                }
            })  .WithReadOnly(cameraEntities)
                .WithDisposeOnCompletion(cameraEntities)
				.ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}