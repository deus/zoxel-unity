using Unity.Entities;
using Unity.Burst;
using Zoxel.Cameras;

namespace Zoxel.Weather
{
    //! Turns world to the Daylight.
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class SkyMaterialSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
				.WithChangeFilter<Sky>()
                .ForEach((in SkyMaterial skyMaterial, in Sky sky) =>
            {
                CameraSetWorldSkySystem.SetSkyRenderSettings(in sky, in skyMaterial.skyMaterial);
            }).WithoutBurst().Run();
        }
    }
}