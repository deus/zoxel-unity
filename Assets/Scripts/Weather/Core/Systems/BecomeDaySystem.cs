using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Audio;
using Zoxel.Audio.Music;
using Zoxel.Cameras;
using Zoxel.Cameras.PostProcessing;
using Zoxel.Voxels;
using Zoxel.Voxels.Lighting;
using Zoxel.Voxels.Lighting.Authoring;

namespace Zoxel.Weather
{
    //! Turns world to the Daylight.
    /**
    * During Realm Time - update sky material per planet
    *   Attach sky material per planet
    *   if player enters new planet change sky material (portals)
    *
    *   \todo Find new way to play rain music, MusicDatams, etc.
    *   \todo Keep counting time when transitioning.
    *   \todo Should just lerp sky settings, keep them as a component, and lerp the new ones with a TargetSkySettings component.
    *   \todo Make states of day generic, so I can add them and their times triggered in the weather settings as a list.
    *
    *   \todo Make Chunklike cubes around edge of planets, when you go through them, the atmosphere changes
    *   \todo Display the sky on these cube chunks on edge of planets.
    *
    *   \todo Animate the post processing over time, before pausing. Only spawn Pause UI after 1 second of animations. (add eye squint animation over screen)
    */
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class BecomeDaySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery camerasQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            camerasQuery = GetEntityQuery(
                ComponentType.ReadOnly<Camera>(),
                ComponentType.ReadOnly<VoxLink>(),
                ComponentType.ReadOnly<MusicLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate<LightsSettings>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var lightsSettings = GetSingleton<LightsSettings>();
            var dayTimeCounter = WeatherManager.instance.weatherSettings.dayTimeCounter;
            var sunlight = lightsSettings.sunlight;
            var rainLight = lightsSettings.rainLight;
            var moonlight = lightsSettings.moonlight;
            var deltaTime = World.Time.DeltaTime;
            var forceRain = WeatherManager.instance.weatherSettings.forceRain;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var cameraEntities = camerasQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var cameraVoxLinks = GetComponentLookup<VoxLink>(true);
            var musicLinks = GetComponentLookup<MusicLink>(true);
            Dependency = Entities
                .WithNone<SetWorldLight>()
                .ForEach((ref BecomeDay becomeDay, in Seed seed, in WorldTime worldTime) =>
            {
                if (!becomeDay.init)
                {
                    var random = new Random();
                    random.InitState((uint) (seed.seed + worldTime.secondsPassed * 3));
                    becomeDay.isRain = forceRain || random.NextInt(0, 100) <= 20;
                }
            }).ScheduleParallel(Dependency);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<SetWorldLight>()
                .ForEach((Entity e, int entityInQueryIndex, ref BecomeDay becomeDay, ref Sky sky, ref Skylight skylight, in Vox vox,
                    in RainCloudLink rainCloudLink) =>
            {
                if (!becomeDay.init)
                {
                    becomeDay.init = true;
                    PostUpdateCommands.AddComponent<DayTime>(entityInQueryIndex, e);
                    if (HasComponent<NightTime>(e))
                    {
                        PostUpdateCommands.RemoveComponent<NightTime>(entityInQueryIndex, e);
                    }
                    if (skylight.skylight == 0)
                    {
                        sky.firstLight = sunlight;
                    }
                    else
                    {
                        sky.firstLight = skylight.skylight;
                    }
                    if (becomeDay.isRain)
                    {
                        PostUpdateCommands.RemoveComponent<DisableRain>(entityInQueryIndex, rainCloudLink.rainCloud);
                        // make it rain
                        // for any camera linked to this world, play rain music
                        for (int i = 0; i < cameraEntities.Length; i++)
                        {
                            var cameraEntity = cameraEntities[i];
                            var cameraVoxLink = cameraVoxLinks[cameraEntity];
                            if (cameraVoxLink.vox == e)
                            {
                                var musicEntity = musicLinks[cameraEntity].music;
                                PostUpdateCommands.AddComponent<DisableMusic>(entityInQueryIndex, musicEntity);
                            }
                        }
                        // rainMusic.Play();
                        // RainSystem.isRain = true;
                        PostUpdateCommands.SetComponent(entityInQueryIndex, rainCloudLink.rainCloud, new MusicEnabledState(true));
                        sky.targetLight = rainLight;
                        if (!HasComponent<RainTime>(e))
                        {
                            PostUpdateCommands.AddComponent<RainTime>(entityInQueryIndex, e);
                        }
                    }
                    else
                    {
                        PostUpdateCommands.AddComponent<DisableRain>(entityInQueryIndex, rainCloudLink.rainCloud);
                        // RainSystem.isRain = false;
                        for (int i = 0; i < cameraEntities.Length; i++)
                        {
                            var cameraEntity = cameraEntities[i];
                            var cameraVoxLink = cameraVoxLinks[cameraEntity];
                            if (cameraVoxLink.vox == e)
                            {
                                var musicEntity = musicLinks[cameraEntity].music;
                                if (HasComponent<DisableMusic>(musicEntity))
                                {
                                    PostUpdateCommands.RemoveComponent<DisableMusic>(entityInQueryIndex, musicEntity);
                                }
                            }
                        }
                        PostUpdateCommands.SetComponent(entityInQueryIndex, rainCloudLink.rainCloud, new MusicEnabledState(false));
                        // rainMusic.Stop();
                        sky.targetLight = sunlight;
                        if (HasComponent<RainTime>(e))
                        {
                            PostUpdateCommands.RemoveComponent<RainTime>(entityInQueryIndex, e);
                        }
                    }
                }
                becomeDay.timePassed += deltaTime;
                var lerp = ((float) becomeDay.timePassed) / sky.fadeTime;
                // should just lerp sky settings, keep them as a component, and lerp the new ones with a TargetSkySettings component.
                SetAsDay(PostUpdateCommands, entityInQueryIndex, ref sky, lerp, cameraEntities, becomeDay.isRain);
                if (sky.counter == dayTimeCounter)
                {
                    sky.counter = 0;
                    var newLight = (byte) math.lerp(sky.firstLight, sky.targetLight, lerp);
                    if (newLight != skylight.skylight)
                    {
                        skylight.skylight = newLight;
                        PostUpdateCommands.AddComponent<SetWorldLight>(entityInQueryIndex, e);
                    }
                }
                else
                {
                    sky.counter++;
                }
                if (becomeDay.timePassed >= sky.fadeTime)
                {
                    PostUpdateCommands.RemoveComponent<BecomeDay>(entityInQueryIndex, e);
                    if (sky.targetLight != skylight.skylight)
                    {
                        skylight.skylight = sky.targetLight;
                        PostUpdateCommands.AddComponent<SetWorldLight>(entityInQueryIndex, e);
                    }
                }
            })  .WithReadOnly(cameraEntities)
                .WithDisposeOnCompletion(cameraEntities)
                .WithReadOnly(cameraVoxLinks)
                .WithReadOnly(musicLinks)
				.ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        
        public static void SetAsNight(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, ref Sky sky,
            float lerp, in NativeArray<Entity> cameraEntities, bool isRainyDay = false)
        {
            BecomeDaySystem.SetSky(PostUpdateCommands, entityInQueryIndex, ref sky, lerp, sky.daySettings, sky.nightSettings, cameraEntities, isRainyDay);
        }

        public static void SetAsDay(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, ref Sky sky,
            float lerp, in NativeArray<Entity> cameraEntities, bool isRainyDay = false)
        {
            SetSky(PostUpdateCommands, entityInQueryIndex, ref sky, lerp, in sky.nightSettings, in sky.daySettings, cameraEntities, isRainyDay);
        }

        //! \todo Handle camera not being in the sky - before entering world in main menu.
        public static void SetSky(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, ref Sky sky, float lerp,
            in SkySettings startSkySettings, in SkySettings targetSkySettings, in NativeArray<Entity> cameraEntities, bool isRainyDay)
        {
            lerp = math.clamp(lerp, 0, 1);
            var startColor = startSkySettings.color.GetColor();
            if (isRainyDay)
            {
                startColor = (startSkySettings.color / 4).GetColor();
            }
            sky.color = new Color(UnityEngine.Color.Lerp(startColor, targetSkySettings.color.GetColor(), lerp));
            sky.fogColor = new Color(UnityEngine.Color.Lerp(startSkySettings.fogColor.GetColor(), targetSkySettings.fogColor.GetColor(), lerp));
            sky.fogDensity = math.lerp(startSkySettings.fogDensity, targetSkySettings.fogDensity, lerp);
            sky.postExposure = math.lerp(startSkySettings.postExposure, targetSkySettings.postExposure, lerp);
            sky.skyDarkness = math.lerp(startSkySettings.skyDarkness, targetSkySettings.skyDarkness, lerp);
            // UnityEngine.Debug.LogError("sky.postExposure set to: " + sky.postExposure);
            for (int i = 0; i < cameraEntities.Length; i++)
            {
                var cameraEntity = cameraEntities[i];
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new PostExposure(sky.postExposure));
            }
        }

        //! \todo Handle camera not being in the sky - before entering world in main menu.
        public static void SetSky(ref Sky sky, in SkySettings skySettings, bool isRainyDay)
        {
            var color = skySettings.color;
            if (isRainyDay)
            { 
                color = (skySettings.color / 4);
            }
            sky.color = color;
            sky.fogColor = skySettings.fogColor;
            sky.fogDensity = skySettings.fogDensity;
            sky.postExposure = skySettings.postExposure;
            sky.skyDarkness = skySettings.skyDarkness;
        }
    }
}