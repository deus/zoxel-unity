using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Animations;
using Zoxel.Cameras;
using Zoxel.Particles;
// using Zoxel.Particles.VoxelParticles;
using Zoxel.Audio.Music;
// two systems
// one for movement
// one for spawning - takes a random position on edge of voxel mesh and replaces it with particle spawn - spawn system
// ahh one for fading
// todo: procedurally create rain hitting voxels sounds
// todo: 3d rain sound
// todo: prcoedural thunder clapping sound
// todo: Sky flicker during thunder
// todo: use gravity and physics on rain drops
// todo: blow wind on rain during fall
// todo: if rain hits side - shrink thing differently

namespace Zoxel.Weather
{
    //! Spawns rain particlesystems.
    /**
    *   \todo Support rain for multiple cameras
    */
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class RainSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity rainPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var rainArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(RainCloud),
                typeof(CloudSpawner),
                typeof(VoxLink),
                typeof(DisableRain),
                typeof(MusicEnabledState),
                typeof(GameObjectLink)  // music gameobject
            );
            rainPrefab = EntityManager.CreateEntity(rainArchetype);
            // UnityEngine.Application.targetFrameRate = 12;
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var particleSystemPrefab = ParticleSystemGroup.particleSystemPrefab;
            var mainCamera = CameraReferences.GetMainCamera(EntityManager);
            if (mainCamera == null)
            {
                return;
            }
            var rainStep = 0.1;
            var rainHeight = 20;
            var cloudRadius = 16; // random.NextFloat(6f, 16f);
            var rainDensity = 10;
            var cameraPosition = (float3) mainCamera.transform.position;
            var random = new Random();
            random.InitState((uint) IDUtil.GenerateUniqueID());
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DisableRain>()
                .ForEach((Entity e, int entityInQueryIndex, ref RainCloud rainCloud, in VoxLink voxLink) =>
            {
                var timePassed = elapsedTime - rainCloud.lastRained;
                if (!(timePassed >= rainStep))
                {
                    return;
                }
                var timeOverLimit = timePassed - rainStep;
                var percentageOfSpawn = 1f + ((float) timeOverLimit / (float) rainStep);
                // UnityEngine.Debug.LogError("timeOverLimit: " + timeOverLimit + " percentageOfSpawn:" + percentageOfSpawn + ":" + rainStep);
                if (percentageOfSpawn > 10f)
                {
                    percentageOfSpawn = 10f;
                }
                rainCloud.lastRained = elapsedTime;
                var spawnAmount = (int) math.ceil(cloudRadius * rainDensity * percentageOfSpawn);
                // spawn particles
                var particlesSystemEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, particleSystemPrefab);
                // color should depend on voxel
                PostUpdateCommands.SetComponent(entityInQueryIndex, particlesSystemEntity,
                    new ParticleSystem
                    {
                        timeSpawn = 0, // 0.001,
                        spawnRate = spawnAmount,
                        random = random,
                        lifeTime = (float) rainStep,
                        timeBegun = elapsedTime,
                        spawnSize = new float3(cloudRadius, 0.5f, cloudRadius),
                        baseColor = new Color(15, 15, 155),
                        colorVariance = new float3(0.15f, 0.15f, 0.15f), 
                        positionAdd = new float3(0, -rainHeight, 0),
                        particleLife = 6,
                        hasCollision = 1
                    });
                PostUpdateCommands.SetComponent(entityInQueryIndex, particlesSystemEntity, new Translation { Value = cameraPosition + new float3(0, rainHeight / 2f, 0) });
                PostUpdateCommands.SetComponent(entityInQueryIndex, particlesSystemEntity, voxLink);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

        }
    }
}