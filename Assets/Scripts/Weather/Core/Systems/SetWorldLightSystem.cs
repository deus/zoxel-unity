using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Zoxel.Voxels;
using Zoxel.Cameras;
// During Realm Time - update sky material per planet
// Attach sky material per planet
//  if player enters new planet change sky material (portals)

namespace Zoxel.Weather
{
    //! Sets the world lights.
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class SetWorldLightSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var deltaTime = World.Time.DeltaTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in SetWorldLight setWorldLight, in Vox vox) =>
            {
                for (int i = 0; i < vox.chunks.Length; i++)
                {
                    var chunkEntity = vox.chunks[i];
                    if (HasComponent<DestroyEntity>(chunkEntity) || !HasComponent<Chunk>(chunkEntity) || HasComponent<EntityBusy>(chunkEntity))
                    {
                        return;
                    }
                }
                PostUpdateCommands.RemoveComponent<SetWorldLight>(entityInQueryIndex, e);
                // var chunksArray = vox.chunks.ToNativeArray(Allocator.None); // Allocator.Temp);
                var chunksArray = vox.chunks.CloneTemp(); // ToNativeArray(); // Allocator.Temp);
                PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, chunksArray);
                PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, chunksArray);
                PostUpdateCommands.AddComponent<VoxelBuilderLightsOnly>(entityInQueryIndex, chunksArray);
                chunksArray.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                /*for (int i = 0; i < vox.chunks.Length; i++)
                {
                    var chunkEntity = vox.chunks[i];
                    PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, chunkEntity);
                    PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, chunkEntity);
                    PostUpdateCommands.AddComponent<VoxelBuilderLightsOnly>(entityInQueryIndex, chunkEntity);
                }*/