using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Audio;

namespace Zoxel.Weather
{
    //! Spawns camera game object based on prefab.
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class RainMusicInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (WeatherManager.instance == null) return;
            var rainMusic = WeatherManager.instance.rainMusic.music;
            var musicPrefab = AudioManager.instance.musicPrefab;
            var PostUpdateCommands  = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, RainCloud>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                var gameObject = UnityEngine.GameObject.Instantiate(musicPrefab);
                gameObject.SetActive(true);
                PostUpdateCommands.SetSharedComponentManaged(e, new GameObjectLink(gameObject));
                var audioSource = gameObject.GetComponent<UnityEngine.AudioSource>();
                audioSource.clip = rainMusic;
                // CameraEnabledSystem.UpdateEnabledState(cameraObject, in cameraEnabledState);
            }).WithoutBurst().Run();
        }
    }
}