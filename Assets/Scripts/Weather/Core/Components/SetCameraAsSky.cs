using Unity.Entities;

namespace Zoxel.Weather
{
    //! Sets the camera as the world sky color.
    /**
    *   Compared to the non world sky colour of our main menu scene.
    */
    public struct SetCameraAsSky : IComponentData
    {
        public double startedTime;
        public float delayTime;

        public SetCameraAsSky(double startedTime, float delayTime)
        {
            this.startedTime = startedTime;
            this.delayTime = delayTime;
        }
    }
}