using Unity.Entities;

namespace Zoxel.Weather
{
    public struct BecomeNight : IComponentData
    {
        public byte init;
        public double timePassed;
    }
}