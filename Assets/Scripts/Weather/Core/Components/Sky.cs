using Unity.Entities;

namespace Zoxel.Weather
{
    //! Contains a bunch of data for the sky and fog.
    public struct Sky : IComponentData
    {
        public float fadeTime;
        public SkySettings daySettings;
        public SkySettings nightSettings;
        public byte counter;
        public byte firstLight;
        public byte targetLight;
        public Color color;
        public Color fogColor;
        public float fogDensity;
        public float postExposure;
        public float skyDarkness;

        public void InitializeSkySettings(Color skyColor, in SkySettings daySettings, in SkySettings nightSettings)
        {
            this.daySettings = daySettings;
            this.nightSettings = nightSettings;
            this.daySettings.color = skyColor;
            // this.daySettings.fogColor = skyColor - new Color(25, 25, 25);
            this.daySettings.fogColor = skyColor.MultiplyBrightness(0.5f);
            this.nightSettings.color = skyColor.MultiplyBrightness(0.4f);
            this.nightSettings.fogColor = skyColor.MultiplyBrightness(0.44f);
        }

        public void SetAllLights(byte light)
        {
            // this.skylight = light;
            this.firstLight = light;
            this.targetLight = light;
        }
    }
}