using Unity.Entities;

namespace Zoxel.Weather
{
    //! A weather event to become day time.
    public struct BecomeDay : IComponentData
    {
        public bool init;
        public bool isRain;
        public double timePassed;
    }
}