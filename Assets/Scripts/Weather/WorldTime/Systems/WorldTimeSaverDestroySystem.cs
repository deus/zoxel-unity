using Unity.Entities;

namespace Zoxel.Weather
{
    //! Cleans up WorldTimeSaver data in memory.
    /**
    *   - Destroy System -
    */
    [UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class WorldTimeSaverDestroySystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, WorldTimeSaver>()
                .ForEach((in WorldTimeSaver worldTimeSaver) =>
            {
                worldTimeSaver.Dispose();
            }).WithoutBurst().Run();
        }
    }
}