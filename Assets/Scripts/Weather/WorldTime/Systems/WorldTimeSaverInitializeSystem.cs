using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using System;
using System.Text;
using System.IO;
using Zoxel.Voxels;

namespace Zoxel.Weather
{
    //! Saves world time to file.
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class WorldTimeSaverInitializeSystem : SystemBase
    {
        private const string filename = "time.zox";
        private const string worldsLabel = "/Worlds/";
        private const string slash = "/";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.ReadOnly<Realm>());
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (VoxelManager.instance.voxelSettings.disablePlanetIO)
            {
                return;
            }
            var realmsPath = SaveUtilities.GetRealmsPath();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var realmIDs = GetComponentLookup<ZoxID>(true);
            realmEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DisableSaving>()
                .WithAll<InitializeEntity, WorldTime>()
                .ForEach((Entity e, in ZoxID zoxID, in RealmLink realmLink) =>
            {
                var realmID = realmIDs[realmLink.realm].id;
                var filepath = realmsPath + realmID.ToString() + worldsLabel + zoxID.id + slash + filename;
                // UnityEngine.Debug.LogError("Save Location: " + e.Index + " is: " + filePath);
                var worldTimeSaver = new WorldTimeSaver();
                if (worldTimeSaver.CreateStream(filepath, 8))
                {
                    PostUpdateCommands.SetSharedComponentManaged(e, worldTimeSaver);
                }
            })  .WithReadOnly(realmIDs)
                .WithoutBurst().Run();
        }
    }
}