using Unity.Entities;
using System;

namespace Zoxel.Weather
{
    //! Saves world time to file.
    [UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class WorldTimeSaveSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithNone<DisableSaving, DestroyEntity, InitializeEntity>()
                .WithAll<OnMinutePassed>()
                .ForEach((in WorldTimeSaver worldTimeSaver, in WorldTime worldTime) =>
            {
                var bytesArray = BitConverter.GetBytes(worldTime.secondsPassed);
                worldTimeSaver.WriteToFile(in bytesArray);
            }).WithoutBurst().Run();
        }
    }
}