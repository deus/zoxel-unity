
using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Zoxel.Voxels.Lighting;
using Zoxel.Voxels.Lighting.Authoring;

namespace Zoxel.Weather
{
    //! Loads world time from file.
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class WorldTimeLoadSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.ReadOnly<Realm>());
            RequireForUpdate(processQuery);
            RequireForUpdate<LightsSettings>();
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var realmsPath = SaveUtilities.GetRealmsPath();
            var lightsSettings = GetSingleton<LightsSettings>();
            var morningTime = WeatherManager.instance.weatherSettings.morningTime;      // 6
            var nightTime = WeatherManager.instance.weatherSettings.nightTime;          // 20;
            var sunlight = lightsSettings.sunlight;
            var moonlight = lightsSettings.moonlight;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var realmIDs = GetComponentLookup<ZoxID>(true);
            realmEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DisableSaving>()
                .WithAll<InitializeEntity, WorldTime>()
                .ForEach((Entity e, ref WorldTime worldTime, ref Sky sky, ref Skylight skylight, in RealmLink realmLink, in ZoxID zoxID, in SkyMaterial skyMaterial) =>
            {
                PostUpdateCommands.AddComponent<WorldTimeLoaded>(e);
                if (!realmIDs.HasComponent(realmLink.realm))
                {
                    return;
                }
                var worldID = zoxID.id;
                var realmID = realmIDs[realmLink.realm].id;
                var directory = realmsPath + realmID.ToString() + "/Worlds/" + worldID + "/";
                var filePath = directory + "time.zox";
                if (File.Exists(filePath))
                {
                    var stream = new StreamReader(filePath);
                    var buffer = new byte[stream.BaseStream.Length];
                    stream.BaseStream.Read(buffer, 0, buffer.Length);
                    worldTime.secondsPassed = BitConverter.ToDouble(buffer, 0);
                    stream.Close();
                    stream.Dispose();
                    //UnityEngine.Debug.LogError("Loaded: " + worldTime.secondsPassed + " from: " + filePath + " :: " + buffer.Length
                    //    + "\n" + buffer[0] + ":" + buffer[4] + ":" + buffer[7]);
                }
                worldTime.UpdateHoursPassed();
                worldTime.UpdateMinutesPassed();
                var hoursPassedToday = worldTime.GetHoursPassedToday();
                var isDayTimeHours = worldTime.IsDayTime(morningTime, nightTime);
                if (isDayTimeHours)
                {
                    PostUpdateCommands.AddComponent<DayTime>(e);
                    BecomeDaySystem.SetSky(ref sky, in sky.daySettings, false);
                    sky.SetAllLights(sunlight);
                    skylight.skylight = sunlight;
                }
                else
                {
                    PostUpdateCommands.AddComponent<NightTime>(e);
                    BecomeDaySystem.SetSky(ref sky, in sky.nightSettings, false);
                    sky.SetAllLights(moonlight);
                    skylight.skylight = moonlight;
                }
            })  .WithReadOnly(realmIDs)
                .WithoutBurst().Run();
        }
    }
}