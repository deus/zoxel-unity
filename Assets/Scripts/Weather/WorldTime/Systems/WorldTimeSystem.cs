using Unity.Entities;
using Unity.Burst;
using Unity.Collections;

namespace Zoxel.Weather
{
    //! Advances world time.
    /**
    *   \todo Morning to be more blue light or something. Basically add more light changes during the day.
    *   \todo Cloudy days to be darker.
    */
    [BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class WorldTimeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (WeatherManager.instance == null) return;
            var morningTime = WeatherManager.instance.weatherSettings.morningTime;      // 6
            var nightTime = WeatherManager.instance.weatherSettings.nightTime;          // 20
            var secondsPassed = World.Time.ElapsedTime;
            var deltaTime = World.Time.DeltaTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity>()
                .WithNone<BecomeDay, BecomeNight>()
                .WithAll<OnDayPassed>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<OnDayPassed>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<InitializeEntity>()
                .WithNone<BecomeDay, BecomeNight>()
                .WithAll<OnMinutePassed>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<OnMinutePassed>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<InitializeEntity>()
                .WithNone<BecomeDay, BecomeNight>()
                .WithNone<OnDayPassed, OnMinutePassed>()
                .ForEach((Entity e, int entityInQueryIndex, ref WorldTime worldTime) =>
            {
                worldTime.secondsPassed += deltaTime * worldTime.timeSpeed;
                var minutesPassedUpdated = worldTime.UpdateMinutesPassed();
                if (minutesPassedUpdated)
                {
                    PostUpdateCommands.AddComponent<OnMinutePassed>(entityInQueryIndex, e);
                }
                var daysPassedUpdated = worldTime.UpdateHoursPassed();
                if (daysPassedUpdated)
                {
                    PostUpdateCommands.AddComponent<OnDayPassed>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<InitializeEntity>()
                .WithNone<BecomeDay, BecomeNight>()
                .WithAll<OnMinutePassed>()
                .WithAll<DayTime>()
                .ForEach((Entity e, int entityInQueryIndex, ref WorldTime worldTime) =>
            {
                if (!worldTime.IsDayTime(morningTime, nightTime))
                {
                    PostUpdateCommands.AddComponent<BecomeNight>(entityInQueryIndex, e);
                    // UnityEngine.Debug.LogError("Become Night Added.");
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<InitializeEntity>()
                .WithNone<BecomeDay, BecomeNight>()
                .WithNone<DayTime>()
                .WithAll<OnMinutePassed>()
                .ForEach((Entity e, int entityInQueryIndex, ref WorldTime worldTime) =>
            {
                if (worldTime.IsDayTime(morningTime, nightTime))
                {
                    PostUpdateCommands.AddComponent<BecomeDay>(entityInQueryIndex, e);
                    // UnityEngine.Debug.LogError("Become Night Added.");
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}