using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Weather
{
	//! Removes WorldTimeLoaded after event is used.
    /**
    *   - End System -
    */
	[BurstCompile, UpdateInGroup(typeof(WeatherSystemGroup))]
    public partial class WorldTimeLoadedEndSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<WorldTimeLoaded>());
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
			SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<WorldTimeLoaded>(processQuery);
		}
	}
}