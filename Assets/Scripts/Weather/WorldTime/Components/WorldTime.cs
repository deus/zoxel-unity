using Unity.Entities;

namespace Zoxel.Weather
{
    //! The worlds time, used for weather and events.
    public struct WorldTime : IComponentData
    {
        public double secondsPassed;    // total
        public byte minutesPassed;
        public byte hoursPassed;
        public int daysPassed;
        public byte isPM;
        public double timeSpeed;
        public double timePerDay;   // = 300;
        public double timePerNight;   // = 300;

        public WorldTime(double timeSpeed, double timePerDay, double timePerNight)
        {
            this.timeSpeed = timeSpeed;
            this.timePerDay = timePerDay;
            this.timePerNight = timePerNight;
            this.secondsPassed = 60 * 60 * 6;   // start at 6am
            this.minutesPassed = 0;
            this.hoursPassed = 0;
            this.daysPassed = 0;
            this.isPM = 0;
        }

        //! Updates all the time variables. Returns true if days passed updated.
        public bool UpdateHoursPassed()
        {
            var minutesPassed = secondsPassed / 60;
            var hoursPassed2 = minutesPassed / 60;
            // minutesPassed = minutesPassed % 60;
            var daysPassed2 = hoursPassed2 / 24;
            hoursPassed2 = hoursPassed2 % 24;
            var isPM2 = hoursPassed2 >= 12;
            if (hoursPassed2 >= 13)
            {
                hoursPassed2 -= 12;
            }
            if (isPM2)
            {
                isPM = 1;
            }
            else
            {
                isPM = 0;
            }
            this.hoursPassed = (byte) hoursPassed2;
            if (daysPassed2 != daysPassed)
            {
                daysPassed = (int) daysPassed2;
                return true;
            }
            return false;
        }

        public bool UpdateMinutesPassed()
        {
            var minutesPassed2 = secondsPassed / 60;
            minutesPassed2 = minutesPassed2 % 60;
            var minutesPassed3 = (byte) minutesPassed2;
            if (minutesPassed != minutesPassed3)
            {
                minutesPassed = minutesPassed3;
                return true;
            }
            return false;
        }

        public bool IsDayTime(int morningTime, int nightTime)
        {
            var hoursPassedToday = GetHoursPassedToday();
            return (!(hoursPassedToday < morningTime || hoursPassedToday >= nightTime));
        }

        public byte GetHoursPassedToday()
        {
            if (hoursPassed == 12)
            {
                if (isPM == 1)
                {
                    return hoursPassed;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                if (isPM == 1)
                {
                    return (byte) (hoursPassed + 12);
                }
                else
                {
                    return hoursPassed;
                }
            }
        }
    }
}