using Unity.Entities;
using System;
using System.IO;

namespace Zoxel.Weather
{
    //! Stores the BaseColor's ComputeBuffer used to link to the shaders memory.
    /**
    *   - Binary Writer Container -
    */
    public struct WorldTimeSaver : ISharedComponentData, IEquatable<WorldTimeSaver>
    {
        public BinaryWriter stream;

        public bool Equals(WorldTimeSaver other)
        {
            return stream == other.stream;
        }
        
        public override int GetHashCode()
        {
            if (stream == null)
            {
                return 0;
            }
            return stream.GetHashCode();
        }

        public void Dispose()
        {
            if (stream != null)
            {
                stream.Close();
                stream.Dispose();
            }
        }

        public bool CreateStream(string filePath, int floatsLength)
        {
            if (stream == null)
            {
                try
                {
                    stream = new BinaryWriter(File.OpenWrite(filePath));
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.LogError("Exception Opening Saver Stream: " + filePath + " :: " + e);
                    return false;
                }
            }
            return true;
        }

        public void WriteToFile(in byte[] bytes)
        {
            if (stream != null)
            {
                stream.Seek(0, SeekOrigin.Begin);
                stream.Write(bytes, 0, bytes.Length);
            }
        }
    }
}