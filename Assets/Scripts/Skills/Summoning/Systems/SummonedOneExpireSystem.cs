using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Skills
{
    //! Removes a summoned entity after time.
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class SummonedOneExpireSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in SummonedEntity summonedEntity) =>
            {
                if (summonedEntity.timeAlive != 0 && elapsedTime - summonedEntity.timeBegun >= summonedEntity.timeAlive)
                {
                    PostUpdateCommands.RemoveComponent<SummonedEntity>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new DyingEntity(1));
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new DestroyEntityInTime(elapsedTime, 10));
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}