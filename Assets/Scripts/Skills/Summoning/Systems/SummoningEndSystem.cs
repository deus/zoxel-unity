using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Skills.Authoring;
using Zoxel.Turrets;
using Zoxel.Characters;
using Zoxel.Characters.World;
using Zoxel.Clans;
using Zoxel.Voxels;

namespace Zoxel.Skills
{
    //! Ends Summoning System.
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class SummoningEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<SkillsSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var skillsSettings = GetSingleton<SkillsSettings>();
            var isSummonMany = skillsSettings.isSummonMany;
            // var summonMany = false;
            var summonManyCount = 16;
            var mrPenguinID = 2066248945;
            var minionModels = VoxelManager.instance.minionModels;
            for (int i = 0; i < minionModels.Count; i++)
            {
                if (minionModels[i].name.Contains("Penguin"))
                {
                    mrPenguinID = minionModels[i].data.id;
                    break;
                }
            }
            // UnityEngine.Debug.LogError("Mr Penguin Vox Summoned: " + mrPenguinID);
            var spawnCharacterPrefab = CharactersSystemGroup.spawnCharacterPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                // .WithAll<ActivateAction>()
                .ForEach((Entity e, int entityInQueryIndex, ref Summoner summoner, in VoxLink voxLink, in ClanLink clanLink, in GravityQuadrant gravityQuadrant, in RealmLink realmLink) =>
            {
                if (summoner.state != 1)
                {
                    return;
                }
                summoner.state = 0;
                /*if (!chunkCharacters.HasComponent(summoner.chunk))
                {
                    return;
                }*/
                var amount = 1;
                if (isSummonMany)
                {
                    amount = summonManyCount;
                }
                var spawnRotation = BlockRotation.GetRotation((byte)(gravityQuadrant.quadrant - 1));
                // UnityEngine.Debug.LogError("summoner.state: " + summoner.chunk.Index + " - " + summoner.spawnPosition + " : " + spawnRotation);
                // maybe connect skill to the summoned creature
                // and set the character in RealmCharacters to use
                // isGenerateGameData = 1,     // Should we summon generated creatures to fight for us?
                for (int i = 0; i < amount; i++)
                {
                    var spawnCharacter = new SpawnCharacter
                    {
                        creator = e,
                        prefabType = CharacterPrefabType.blobAuthoredNew,
                        voxDataID = mrPenguinID,
                        chunk = summoner.chunk,
                        planet = voxLink.vox,
                        realm = realmLink.realm,
                        position = summoner.spawnPosition,
                        rotation = spawnRotation,
                        quadrant = gravityQuadrant.quadrant
                    };
                    var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnCharacterPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, spawnCharacter);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, clanLink);
                }
                // UnityEngine.Debug.LogError("Mr Penguin Vox Summoned: " + mrPenguinID);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}