using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Stats;
using Zoxel.Actions;
using Zoxel.Clans;
using Zoxel.Voxels;
using Zoxel.Turrets;
using Zoxel.Characters;
using Zoxel.VoxelInteraction;

namespace Zoxel.Skills
{
    //! Summons Characters from a Character!
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class SummoningSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity spawnTurretPrefab;
        private EntityQuery processQuery;
        private EntityQuery skillsQuery;
        private EntityQuery voxesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var archetype = EntityManager.CreateArchetype(
                typeof(SpawnTurret),
                typeof(Prefab));
            spawnTurretPrefab = EntityManager.CreateEntity(archetype);
            skillsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Skill>(),
                ComponentType.Exclude<DestroyEntity>());
            voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxScale>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            // var summonMany = false;
            // var summonManyCount = 1;
            // var elapsedTime = World.Time.ElapsedTime;
            var spawnTurretPrefab = this.spawnTurretPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // todo: check character has selected a voxel
            var skillEntities = skillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var skillSummonCharacters = GetComponentLookup<SkillSummonCharacter>(true);
            var skillSummonTurrets = GetComponentLookup<SkillSummonTurret>(true);
            skillEntities.Dispose();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxScales = GetComponentLookup<VoxScale>(true);
            voxEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref Summoner summoner, in RaycastVoxel raycastVoxel, in ActivateSkill activateSkill) =>
            {
                if (summoner.state != 0)
                {
                    return;
                }
                var voxScale = voxScales[raycastVoxel.normalVoxel.vox].scale;
                var voxelSpawnPosition = VoxelUtilities.GetRealPosition(raycastVoxel.normalVoxel.position, voxScale);
                summoner.spawnPosition = voxelSpawnPosition; // .ToFloat3() + new float3(0.5f, 0.5f, 0.5f);
                summoner.chunk = raycastVoxel.hitVoxel.chunk;
                var skillEntity = activateSkill.skill;
                if (HasComponent<SkillSummonCharacter>(skillEntity))
                {
                    var skillSummonCharacter = skillSummonCharacters[skillEntity];
                    summoner.state = 1;
                }
                else if (HasComponent<SkillSummonTurret>(skillEntity))
                {
                    var skillSummonTurret = skillSummonTurrets[skillEntity];
                    // First check if any other turret is spawned there
                    // also check is voxel placing on solid ground (mesh type of 0)
                    // has to take in body size of thing being summoned
                    var command = new SpawnTurret
                    {
                        summoner = e,
                        position = summoner.spawnPosition - new float3(0.5f, 0.5f, 0.5f)
                    };
                    var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnTurretPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, command);
                }
            })  .WithReadOnly(skillSummonCharacters)
                .WithReadOnly(skillSummonTurrets)
                .WithReadOnly(voxScales)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}