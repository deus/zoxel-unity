using Unity.Entities;

namespace Zoxel.Skills
{
    //! Data for a physical attack.
    /**
    *   Using base damage, speed
    *   Plus a stat modifiers
    */
    public struct SkillPhysicalAttack : IComponentData { }
}
        /*public float baseSpeed;
        public float attackForce;

        public SkillPhysicalAttack(float baseSpeed, float attackForce)
        {
            this.baseSpeed = baseSpeed;
            this.attackForce = attackForce;
        }*/