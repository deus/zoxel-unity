using Unity.Entities;

namespace Zoxel.Skills
{
    //! Data for a projectile.
    public struct SkillProjectile : IComponentData
    {
        public float attackDamage;
        public float attackSpeed;
        public float attackForce;
        public Color color;
        public byte modelType;  // sphere, cube, etc
        // more then one bullet?
        // path (can be wave, or orbital paths around player)

        public SkillProjectile(float attackDamage, float attackSpeed, float attackForce)
        {
            this.attackDamage = attackDamage;
            this.attackSpeed = attackSpeed;
            this.attackForce = attackForce;
            this.color = new Color(200, 25, 25);
            this.modelType = 0;
        }
    }
}