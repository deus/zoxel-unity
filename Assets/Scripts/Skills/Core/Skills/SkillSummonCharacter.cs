using Unity.Entities;

namespace Zoxel.Skills
{
    //! Skill data for the summoned creature
    /**
    *   Spawns a portal where a creature(s) comes from.
    *   The most basic spawn skill that uses a new summoned creature every time.
    */
    public struct SkillSummonCharacter : IComponentData
    {
        public int spawnAmount;
        public int spawnMetaID;
        //public byte isSpawnHostile;

        public SkillSummonCharacter(int spawnAmount)
        {
            this.spawnAmount = spawnAmount;
            this.spawnMetaID = 0;
        }
    }
}