using Unity.Entities;

namespace Zoxel.Skills
{
    //! Augmentation gives the user buffs or userStatLinks.
    public struct SkillAugmentation : IComponentData
    {
        public float amount;

        public SkillAugmentation(float amount)
        {
            this.amount = amount;
        }
    }
}