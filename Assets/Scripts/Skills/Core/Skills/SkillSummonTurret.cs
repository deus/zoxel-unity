using Unity.Entities;

namespace Zoxel.Skills
{
    //! Skill data for the summoned creature
    /**
    *   Spawns a portal where a creature(s) comes from.
    *   The most basic spawn skill that uses a new summoned creature every time.
    */
    public struct SkillSummonTurret : IComponentData
    {
        public int spawnMetaID;
        //public byte isSpawnHostile;

        public SkillSummonTurret(int spawnMetaID)
        {
            this.spawnMetaID = spawnMetaID;
        }
    }
}