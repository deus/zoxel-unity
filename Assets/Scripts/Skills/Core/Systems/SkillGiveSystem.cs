using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Skills
{
    //! Gives an entity a new skill! Isn't that nice.
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class SkillGiveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var userSkillPrefab = SkillSystemGroup.userSkillPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .ForEach((Entity e, int entityInQueryIndex, in GiveSkill giveSkill, in UserSkillLinks userSkillLinks) =>
            {
                PostUpdateCommands.RemoveComponent<GiveSkill>(entityInQueryIndex, e);
                // set damage + 5,  set to use energy
                var skillEntity = giveSkill.skill;
                if (skillEntity.Index == 0)
                {
                    return;
                }
                var userSkillEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, userSkillPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new MetaData(skillEntity));
                PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new CharacterLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new UserDataIndex((byte) userSkillLinks.skills.Length));
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUserSkillSpawned((byte) 1));
                PostUpdateCommands.AddComponent<SaveSkills>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new AddSkillAction(userSkillEntity, skillEntity));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}