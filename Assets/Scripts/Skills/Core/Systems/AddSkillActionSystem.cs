using Unity.Entities;
using Unity.Burst;
using Zoxel.Actions;

namespace Zoxel.Skills
{
    //! Adds an action for the latest skill added.
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class AddSkillActionSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .ForEach((Entity e, int entityInQueryIndex, ref UserActionLinks userActionLinks, in AddSkillAction addSkillAction) =>
            {
                PostUpdateCommands.RemoveComponent<AddSkillAction>(entityInQueryIndex, e);
                // find new action spot, then add skill action
                for (int j = 0; j < userActionLinks.actions.Length; j++)
                {
                    var target = userActionLinks.actions[j].target;
                    if (target.Index == 0)
                    {
                        var newAction = new Action(addSkillAction.userSkill, addSkillAction.skill);
                        userActionLinks.actions[j] = newAction;
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ActionTargetUpdated(addSkillAction.userSkill, addSkillAction.skill));
                        if (HasComponent<EntitySaver>(e))
                        {
                            PostUpdateCommands.AddComponent<SaveActions>(entityInQueryIndex, e);
                        }
                        break;
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}