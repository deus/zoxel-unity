using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Stats;

namespace Zoxel.Skills
{
    //! Heals an entity.
    /**
    *   \todo Spawn particles and sounds on healing.
    */
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class HealSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userStatsQuery;
        public static Entity healingEventPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.ReadOnly<StateStat>(),
                ComponentType.Exclude<DestroyEntity>());
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(GenericEvent),
                typeof(Healing));
            healingEventPrefab = EntityManager.CreateEntity(archetype);
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var statValues = GetComponentLookup<StatValue>(false);
            var statValueMaxs = GetComponentLookup<StatValueMax>(false);
            userStatEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((in Healing healing) =>
            {
                if (!statValues.HasComponent(healing.userStat))
                {
                    return;
                }
                var stateStat = statValues[healing.userStat];
                var statValueMax = statValueMaxs[healing.userStat];
                if (stateStat.value != statValueMax.value) // !stateStat.IsMaxValue())
                {
                    stateStat.value = math.min(stateStat.value + healing.value, statValueMax.value - 0.01f);
                    statValues[healing.userStat] = stateStat;
                }
            })  .WithNativeDisableContainerSafetyRestriction(statValues)
                .WithReadOnly(statValueMaxs)
                .ScheduleParallel(Dependency);
        }
    }
}