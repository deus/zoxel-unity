using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Stats;
using Zoxel.Actions;

namespace Zoxel.Skills
{
    //! Augments oneself!
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class AugmentationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery skillsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            skillsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Skill>(),
                ComponentType.ReadOnly<SkillCost>(),
                ComponentType.ReadOnly<SkillAugmentation>());
			RequireForUpdate(processQuery);
			RequireForUpdate(skillsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var healingEventPrefab = HealSystem.healingEventPrefab;
            var skillEntities = skillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var skillAugmentations = GetComponentLookup<SkillAugmentation>(true);
            skillEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .ForEach((int entityInQueryIndex, in UserActionLinks userActionLinks, in UserStatLinks userStatLinks, in ActivateSkill activateSkill) =>
            {
                var skillEntity = activateSkill.skill;
                if (!skillAugmentations.HasComponent(skillEntity))
                {
                    return;
                }
                var skillAugmentation = skillAugmentations[skillEntity];
                // apply augmentation effect
                //   todo: Make a StartBuffing component and a buff system that takes it away
                //          Make temporaryRegens with timers and apply all these in another system
                // use heals
                var amount = skillAugmentation.amount;
                var healthUserStat = new Entity();
                for (int i = 0; i < userStatLinks.stats.Length; i++)
                {
                    if (HasComponent<HealthStat>(userStatLinks.stats[i]))
                    {
                        healthUserStat = userStatLinks.stats[i];
                        break;
                    }
                }
                if (healthUserStat.Index > 0)
                {
                    var healingEventEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, healingEventPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, healingEventEntity, new Healing(amount, healthUserStat));
                }
            })  .WithReadOnly(skillAugmentations)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}