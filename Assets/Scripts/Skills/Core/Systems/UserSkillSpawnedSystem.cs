using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Skills
{
    //! Links user items to inventory user after they spawn.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class UserSkillSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userSkillsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<OnUserSkillSpawned>(processQuery);
            PostUpdateCommands2.RemoveComponent<NewUserSkill>(userSkillsQuery);
            //! First initializes children.
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref UserSkillLinks userSkillLinks, in OnUserSkillSpawned onUserSkillSpawned) =>
            {
                if (onUserSkillSpawned.spawned != 0)
                {
                    userSkillLinks.Add(onUserSkillSpawned.spawned);
                }
            }).ScheduleParallel(Dependency);
            //! For each child, using the index, sets into parents children that is passed in.
            var parentEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var parentSkills = GetComponentLookup<UserSkillLinks>(false);
            parentEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref userSkillsQuery)
                .WithAll<NewUserSkill>()
                .ForEach((Entity e, in UserDataIndex userDataIndex, in CharacterLink characterLink) =>
            {
                var userSkillLinks = parentSkills[characterLink.character];
                #if DATA_COUNT_CHECKS
                if (userDataIndex.index >= userSkillLinks.skills.Length)
                {
                    // UnityEngine.Debug.LogError("Index out of bounds: " + child.index + " to parent " + parentLink.parent.Index);
                    return;
                }
                #endif
                userSkillLinks.skills[userDataIndex.index] = e;
            })  .WithNativeDisableContainerSafetyRestriction(parentSkills)
                .ScheduleParallel(Dependency);
        }
    }
}