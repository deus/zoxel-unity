using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Skills.Authoring;
using Zoxel.Stats;
using Zoxel.Actions;

namespace Zoxel.Skills
{
    //! Triggers a Skill (generic event)
    /**
    *   Adds a generic event onto characters for them to activate a skill using ActivateAction event.
    *   UserSkillLinks use StatValue as a resource.
    *   UserSkillLinks have different animation, activation (cast) and cooldown times.
    *   UserSkillLinks have animations that are used. They may even have particle effects.
    *   UserSkillLinks may link to summoned characters or bullets or turrets. They can be anything really.
    *   \todo Alter userSkillLinks data when userStatLinks are updated or should I calculate them on the fly here? Probaly the latter..
    */
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class SkillTriggerSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userSkillsQuery;
        private EntityQuery skillsQuery;
        private EntityQuery userStatsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userSkillsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserSkill>(),
                ComponentType.Exclude<DestroyEntity>());
            skillsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Skill>(),
                ComponentType.ReadOnly<SkillCost>(),
                ComponentType.Exclude<DestroyEntity>());
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate<SkillsSettings>();
			RequireForUpdate(processQuery);
			RequireForUpdate(userSkillsQuery);
			RequireForUpdate(skillsQuery);
			RequireForUpdate(userStatsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var skillsSettings = GetSingleton<SkillsSettings>();
            var isInstantCastSkills = skillsSettings.isInstantCastSkills;
            var isUnlimitedSkillUse = skillsSettings.isUnlimitedSkillUse;
            var isNoCooldowns = skillsSettings.isNoCooldowns;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userSkillEntities = userSkillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var activatedTimes = GetComponentLookup<ActivatedTime>(false);
            var userSkillMetaDatas = GetComponentLookup<MetaData>(true);
            userSkillEntities.Dispose();
            var skillEntities = skillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var skillCosts = GetComponentLookup<SkillCost>(true);
            var skillCooldowns = GetComponentLookup<SkillCooldown>(true);
            var skillCastTimes = GetComponentLookup<SkillCastTime>(true);
            skillEntities.Dispose();
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var userStatMetaDatas = GetComponentLookup<MetaData>(true);
            var statValues = GetComponentLookup<StatValue>(false);
            var statValueMaxs = GetComponentLookup<StatValueMax>(true);
            userStatEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, DeadEntity>()
                .WithNone<ActivateSkill, ActivatedAction>()
                .WithAll<ActivateAction>()
                .ForEach((Entity e, int entityInQueryIndex, in UserStatLinks userStatLinks, in UserActionLinks userActionLinks) =>
            {
                var userSkillEntity = userActionLinks.GetSelectedTarget();
                if (!activatedTimes.HasComponent(userSkillEntity))
                {
                    return;
                }
                var activatedTime = activatedTimes[userSkillEntity];
                var skillEntity = userSkillMetaDatas[userSkillEntity].data;
                // if skill is Physical attack type
                var skillCost = skillCosts[skillEntity];
                var cooldown = 0f;
                var castTime = 0f;
                if (!isNoCooldowns && skillCooldowns.HasComponent(skillEntity))
                {
                    cooldown = skillCooldowns[skillEntity].cooldown;
                }
                if (!isInstantCastSkills && skillCastTimes.HasComponent(skillEntity))
                {
                    castTime = skillCastTimes[skillEntity].castTime;
                }
                // Check cooldown
                if (activatedTime.lastActivated != 0 && !(elapsedTime - activatedTime.lastActivated > castTime + cooldown))
                {
                    return;
                }
                // Check Resource to use
                if (!isUnlimitedSkillUse && !HasResource(in userStatLinks, in userStatMetaDatas, in statValues, skillCost.stat, skillCost.cost))
                {
                    return;
                }
                // Okay, we good
                activatedTime.lastActivated = (float) elapsedTime;
                activatedTimes[userSkillEntity] = activatedTime;
                Entity userStatCost;
                if (IsResourceMax(in userStatLinks, in userStatMetaDatas, in statValues, skillCost.stat, out userStatCost, in statValueMaxs))
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new StartRegening(userStatCost));
                }
                if (!isUnlimitedSkillUse)
                {
                    UseResource(in userStatLinks, in userStatMetaDatas, ref statValues, skillCost.stat, skillCost.cost);
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ActivatedAction(userActionLinks.selectedAction, elapsedTime, castTime, cooldown));
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ActivateSkill(userSkillEntity, userStatMetaDatas[userSkillEntity].data));
            })  .WithNativeDisableContainerSafetyRestriction(activatedTimes)
                .WithReadOnly(userSkillMetaDatas)
                .WithReadOnly(skillCosts)
                .WithReadOnly(skillCooldowns)
                .WithReadOnly(skillCastTimes)
                .WithReadOnly(userStatMetaDatas)
                .WithReadOnly(statValueMaxs)
                .WithNativeDisableContainerSafetyRestriction(statValues)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
        
        public static bool HasResource(in UserStatLinks userStatLinks, in ComponentLookup<MetaData> userStatMetaDatas, in ComponentLookup<StatValue> statValues, Entity targetStatEntity, float value)
        {
            if (value == 0)
            {
                return true;
            }
            for (int i = 0; i < userStatLinks.stats.Length; i++)
            {
                var userStatEntity = userStatLinks.stats[i];
                var statEntity = userStatMetaDatas[userStatEntity].data;
                if (statEntity == targetStatEntity)
                {
                    var stateStat = statValues[userStatEntity];
                    return (stateStat.value >= value);
                }
            }
            return false;
        }

        public static bool IsResourceMax(in UserStatLinks userStatLinks, in ComponentLookup<MetaData> userStatMetaDatas, in ComponentLookup<StatValue> statValues, Entity targetStatEntity,
            out Entity userStatCost, in ComponentLookup<StatValueMax> statValueMaxs)
        {
            for (int i = 0; i < userStatLinks.stats.Length; i++)
            {
                var userStatEntity = userStatLinks.stats[i];
                var statEntity = userStatMetaDatas[userStatEntity].data;
                if (statEntity == targetStatEntity)
                {
                    var stateStat = statValues[userStatEntity];
                    userStatCost = userStatEntity;
                    var statValueMax = statValueMaxs[userStatEntity];
                    return (stateStat.value == statValueMax.value);
                }
            }
            userStatCost = new Entity();
            return false;
        }
        
        public static void UseResource(in UserStatLinks userStatLinks, in ComponentLookup<MetaData> userStatMetaDatas, ref ComponentLookup<StatValue> statValues, Entity targetStatEntity, float value)
        {
            if (value == 0)
            {
                return;
            }
            for (int i = 0; i < userStatLinks.stats.Length; i++)
            {
                var userStatEntity = userStatLinks.stats[i];
                var statEntity = userStatMetaDatas[userStatEntity].data;
                if (statEntity == targetStatEntity)
                {
                    var stateStat = statValues[userStatEntity];
                    if (stateStat.value >= value)
                    {
                        stateStat.value -= value;
                        statValues[userStatEntity] = stateStat;
                    }
                    return;
                }
            }
        }
    }
}

// Note: I am trying to make a generic skill activation system for my other userSkillLinks
//      to stream line adding new userSkillLinks
// Example: Punch Cooldown 0 seconds
//  But Start of animation is 0.5 seconds
//  And End of animation is 0.5 seconds
// Example: Shoot Cooldown 1 seconds
//  Cast Time is 1 second
//  But Start of animation is 0 seconds
//  And End of animation is 0 seconds
// Actually, I dont have a way to handle castTime atm? Casting is more or less instant
// todo: Handle casting with an animation
// todo: Multiply CastTime and Cooldown Time by AttackSpeed
// Times can be effected by userStatLinks..! Alter cooldown? Alter resource cost?
//  Alter attack speed (animation times)?