﻿using Unity.Entities;
using Unity.Mathematics;
// basically a skill that spawns bullets and adds force to them!

namespace Zoxel.Skills
{
    public struct Shooter : IComponentData
    {
        public float2 attackDamage;
        public float3 shootPosition;
        public quaternion shootRotation;
        public byte triggered;
        /*
        public double lastTriggered;  // last shot time
        public bool CanTrigger(double time, float attackSpeed)
        {
            var canTrigger = time - lastTriggered >= attackSpeed; 
            if (canTrigger)
            {
                lastTriggered = time;
            }
            return canTrigger;
        }*/
    }
}