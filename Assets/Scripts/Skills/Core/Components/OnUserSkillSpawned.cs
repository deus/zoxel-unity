using Unity.Entities;

namespace Zoxel.Skills
{
    //! Relinks userskills to a user.
    public struct OnUserSkillSpawned : IComponentData
    {
        public byte spawned;

        public OnUserSkillSpawned(byte spawned)
        {
            this.spawned = spawned;
        }
    }
}