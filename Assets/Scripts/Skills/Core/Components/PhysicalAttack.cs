﻿using Unity.Entities;

namespace Zoxel.Skills
{
    //! An event for attacking something.
    public struct PhysicalAttacking : IComponentData
    {
        public byte attackState;
        public double attackingStartTime;
        public float attackDamage;
        public float attackSpeed;

        public PhysicalAttacking(double attackingStartTime, float attackDamage, float attackSpeed)
        {
            this.attackState = 0;
            this.attackingStartTime = attackingStartTime;
            this.attackDamage = attackDamage;
            this.attackSpeed = attackSpeed;
        }
    }

    //! A tag for what skill to use.
    public struct PhysicalAttack : IComponentData { }
}
        // public int seedID;
        // public int3 targetVoxel;

        // Todo: Make this ActivateAction Second State? Remove after!
        //          But using attack speed of this userSkillLinks data
        // public byte triggered;
        // public double lastTriggered;

        /*public bool CanTrigger(double time, float attackCooldown)
        {
            if (isAttacking != 0)
            {
                return false;
            }
            var canTrigger = time - lastTriggered >= attackCooldown; 
            if (canTrigger)
            {
                lastTriggered = time;
            }
            return canTrigger;
        }*/