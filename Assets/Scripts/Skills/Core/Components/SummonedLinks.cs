using Unity.Entities;

namespace Zoxel.Skills
{
    /**
        Links to Creatures the character has summoned.
    */
    public struct SummonedLinks : IComponentData
    {
        public BlitableArray<Entity> summoned;
    }
}