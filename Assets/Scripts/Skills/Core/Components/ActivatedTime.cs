using Unity.Entities;

namespace Zoxel.Skills
{
    //! An entities last activated time.
    /**
    *   \todo Use this for UserItemActivated as well.
    */
    public struct ActivatedTime : IComponentData
    {
        public float lastActivated;

        public ActivatedTime(float lastActivated)
        {
            this.lastActivated = lastActivated;
        }
    }
}