using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Skills
{
    public struct Summoner : IComponentData
    {
        public byte state;
        public byte isActivating;
        public Entity chunk;
        public float3 spawnPosition;
    }
}
/*
        public byte triggered;
        public double lastTriggered;
        public bool CanTrigger(double time, float attackCooldown)
        {
            if (isActivating == 1)
            {
                return false;
            }
            var canTrigger = time - lastTriggered >= attackCooldown; 
            if (canTrigger)
            {
                lastTriggered = time;
            }
            return canTrigger;
        }*/