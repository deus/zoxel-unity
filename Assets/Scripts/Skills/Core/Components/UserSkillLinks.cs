﻿using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Skills
{
    //! An entity that can use skills can effect the world.
    /**
    *   \todo Put attackAngle into skill
    *   \todo Make skills separate entitys that I can use for data
    */
    public struct UserSkillLinks : IComponentData
    {
        //! Links to UserSkill entities.
        public BlitableArray<Entity> skills;

        public void DisposeFinal()
        {
            skills.DisposeFinal();
        }
        
        public void Dispose()
        {
            skills.Dispose();
        }

        public void RemoveAt(int index)
        {
            var skills2 = new BlitableArray<Entity>(skills.Length - 1, Allocator.Persistent, skills);
            for (int i = index; i < skills2.Length; i++)
            {
                skills2[i] = skills[i + 1];
            }
            Dispose();
            skills = skills2;
        }

        public void Initialize(int skillsCount)
        {
            Dispose();
            this.skills = new BlitableArray<Entity>(skillsCount, Allocator.Persistent);
        }

        public void Add(byte newCount)
        {
            var skills2 = new BlitableArray<Entity>(skills.Length + newCount, Allocator.Persistent, skills);
            Dispose();
            skills = skills2;
        }
    }
}