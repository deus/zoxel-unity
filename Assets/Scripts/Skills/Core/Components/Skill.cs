﻿using Unity.Entities;

namespace Zoxel.Skills
{
    //! A tag for skill data.
    public struct Skill : IComponentData
    {
        public byte type;   //! Just a reference for what kind of skill it is

        public Skill(byte type)
        {
            this.type = type;
        }
    }
}