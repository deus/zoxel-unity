using Unity.Entities;

namespace Zoxel.Skills
{
    //! Heals an entitys state stat.
    public struct Healing : IComponentData
    {
        public float value;
        public Entity userStat;

        public Healing(float value, Entity userStat)
        {
            this.value = value;
            this.userStat = userStat;
        }
    }
}