using Unity.Entities;

namespace Zoxel.Skills
{
    //! Event for activating a skill
    public struct ActivateSkill : IComponentData
    {
        public Entity userSkill;
        public Entity skill;

        public ActivateSkill(Entity userSkill, Entity skill)
        {
            this.userSkill = userSkill;
            this.skill = skill;
        }
    }
}