using Unity.Entities;

namespace Zoxel.Skills
{
    //! A tag for a new UserSkill.
    public struct NewUserSkill : IComponentData { }
}