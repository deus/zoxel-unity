using Unity.Entities;

namespace Zoxel.Skills
{
    //! Gives a user a new skill.
    public struct GiveSkill : IComponentData
    {
        public Entity skill;

        public GiveSkill(Entity skill)
        {
            this.skill = skill;
        }
    }
}