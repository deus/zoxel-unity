using Unity.Entities;

namespace Zoxel.Skills
{
    //! Adds a action based on the latest added user skill / skill.
    public struct AddSkillAction : IComponentData
    {
        public Entity userSkill;
        public Entity skill;

        public AddSkillAction(Entity userSkill, Entity skill)
        {
            this.userSkill = userSkill;
            this.skill = skill;
        }
    }
}