using Unity.Entities;

namespace Zoxel.Skills
{
    //! The Damage of a skill.
    public struct SkillDamage : IComponentData
    {
        public byte damageType;
        public float amount;

        public SkillDamage(DamageType damageType, float amount)
        {
            this.damageType = (byte) damageType;
            this.amount = amount;
        }
    }
}