using Unity.Entities;

namespace Zoxel.Skills
{
    public enum DamageType : byte
    {
        Physical,
        Magical,
        Chaos,
        Healing
    }
}