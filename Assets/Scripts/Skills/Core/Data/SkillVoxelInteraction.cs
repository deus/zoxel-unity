using Unity.Entities;

namespace Zoxel.Skills
{
    //! Added to a skill that interacts with Voxels.
    public struct SkillVoxelInteraction : IComponentData { }
}