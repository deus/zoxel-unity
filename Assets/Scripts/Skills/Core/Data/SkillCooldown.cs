using Unity.Entities;

namespace Zoxel.Skills
{
    //! The cooldown of a skill.
    public struct SkillCooldown : IComponentData
    {
        public float cooldown;

        public SkillCooldown(float cooldown)
        {
            this.cooldown = cooldown;
        }
    }
}