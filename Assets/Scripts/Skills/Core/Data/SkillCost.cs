using Unity.Entities;

namespace Zoxel.Skills
{
    //! The price of skill useage.
    public struct SkillCost : IComponentData
    {
        public Entity stat;
        public float cost;

        public SkillCost(Entity stat, float cost)
        {
            this.stat = stat;
            this.cost = cost;
        }
    }
}