﻿using Unity.Entities;

namespace Zoxel.Skills
{
    //! UserSkillLinks! UserActionLinks use userSkillLinks. UserSkillLinks use animations, userStatLinks and particles.
    [AlwaysUpdateSystem]
    public partial class SkillSystemGroup : ComponentSystemGroup
    {
        public static int skillsCount;
		private EntityQuery skillsQuery;
        public static int userSkillsCount;
		private EntityQuery userSkillsQuery;
        public static Entity userSkillPrefab;
        public static Entity skillPrefab;

        protected override void OnCreate()
        {
            base.OnCreate();
			skillsQuery = GetEntityQuery(ComponentType.ReadOnly<Skill>());
			userSkillsQuery = GetEntityQuery(ComponentType.ReadOnly<UserSkill>());
            var userSkillArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(UserSkill),
                typeof(ActivatedTime),
                typeof(MetaData),
                typeof(UserDataIndex),
                typeof(CharacterLink),
                typeof(NewUserSkill)
            );
            userSkillPrefab = EntityManager.CreateEntity(userSkillArchetype);
            var skillArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(ZoxID),
                typeof(ZoxName),
                typeof(Skill),
                typeof(SkillCost));
            skillPrefab = EntityManager.CreateEntity(skillArchetype);
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();
            skillsCount = skillsQuery.CalculateEntityCount();
            userSkillsCount = userSkillsQuery.CalculateEntityCount();
        }
    }
}
