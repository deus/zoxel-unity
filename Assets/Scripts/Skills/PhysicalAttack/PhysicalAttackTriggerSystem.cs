using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Stats;
using Zoxel.Actions;
// should be a system:
//      Controller, UserSkillLinks, Stats, Shooter
//          Controller for input
//          UserSkillLinks for skill data - cast time etc
//          Stats for mana and damage modifiers
//          Shooter for actual shooting, like shoot position etc
//      Do this for other systems / userSkillLinks - their own unique systems
//      Idea: BulletTargettingSystem - bullet will target a npc and move closer to them

namespace Zoxel.Skills.Physical
{
    //! Triggers a PhysicalAttack!
    /**
    *   \todo Add a component on to do an attack rather than storing it in 'PhysicalAttack' kinda thing.
    */
    [UpdateAfter(typeof(PhysicalAttackSystem))]
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class PhysicalAttackTriggerSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        //private EntityQuery skillsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            //skillsQuery = GetEntityQuery(ComponentType.ReadOnly<Skill>(), ComponentType.ReadOnly<SkillPhysicalAttack>(), ComponentType.Exclude<DestroyEntity>());
			RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            /*var skillEntities = skillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var skillPhysicalAttacks = GetComponentLookup<SkillPhysicalAttack>(true);*/
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .WithAll<ActivateSkill>()
                .ForEach((Entity e, int entityInQueryIndex, ref PhysicalAttack physicalAttack, in ActivatedAction activatedAction, in ZoxID zoxID) =>
            {
                var attackSpeed = activatedAction.timeLengthGrow + activatedAction.timeLengthShrink;
                // var skillPhysicalAttack = skillPhysicalAttacks[userSkill.skill];
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new PhysicalAttacking(elapsedTime, 1f, attackSpeed));
                // UnityEngine.Debug.LogError("Triggering Attack: " + e.Index);
            })  /*.WithReadOnly(skillEntities)
                .WithDisposeOnCompletion(skillEntities)
                .WithReadOnly(skillCosts)
                .WithReadOnly(skillPhysicalAttacks)*/
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
               
                /*if(userActionLinks.selectedSkillIndex >= 0 && userActionLinks.selectedSkillIndex < userSkillLinks.skills.Length && skillEntities.Length > 0)
                {
                    var userSkill = userSkillLinks.skills[actions.selectedSkillIndex];
                    // if skill is Physical attack type
                    if (HasComponent<SkillPhysicalAttack>(userSkill.skill))
                    {
                        var skillCost = skillCosts[userSkill.skill];
                        var skillPhysicalAttack = skillPhysicalAttacks[userSkill.skill];
                        var attackSpeed = skillPhysicalAttack.attackSpeed;
                        if (physicalAttack.CanTrigger(elapsedTime, attackSpeed) && userStatLinks.HasResource(skillCost.statID, skillCost.cost))
                        {
                            var attackDamage = skillPhysicalAttack.attackDamage;
                            // increase damage depending on strength attribute
                            if (userStatLinks.attributes.Length >= 1)
                            {
                                attackDamage += userStatLinks.attributes[0].value;  // Strength
                            }
                            if (userStatLinks.attributes.Length >= 2)
                            {
                                attackSpeed -= userStatLinks.attributes[1].value * 0.05f;   // Agility
                            }
                            var attackDamageMax = attackDamage;
                            PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ActivatedAction(userActionLinks.selectedAction, elapsedTime,
                                    physicalAttack.attackSpeed / 2f, physicalAttack.attackSpeed / 2f));
                            if (userStatLinks.IsResourceMax(skillCost.statID))
                            {
                                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new StartRegening(userStatLinks.GetStateIndex(skillCost.statID)));
                            }
                            userStatLinks.UseResource(skillCost.statID, skillCost.cost);
                        }
                    }
                }*/