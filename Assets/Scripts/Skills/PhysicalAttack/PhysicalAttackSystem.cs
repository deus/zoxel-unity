﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Stats;
using Zoxel.Stats.Authoring;
using Zoxel.Animations;
using Zoxel.Particles;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;
using Zoxel.Voxels;
using Zoxel.VoxelInteraction;
using Zoxel.Voxels.Minivoxes;

namespace Zoxel.Skills.Physical
{
    //! Hits a character in melee range!
    /**
    *   Check if hits other body, the angle of hit, timing and animations.
    *   \todo Use animation event system for doing these userActionLinks.
    */
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class PhysicalAttackSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        // todo: Get these from Stats
        // public static float attackAngle = 0.76f;
        public static float attackDistance = 1.2f;
        public static float attackAngle = 0.22f;
        public static float attackDistance2 = 0.8f;
        public static float attackAngle2 = 0.42f;
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxScale>());
			RequireForUpdate(processQuery);
			RequireForUpdate(voxesQuery);
            RequireForUpdate<StatSettings>();
            RequireForUpdate<SoundSettings>();
		}
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var statSettings = GetSingleton<StatSettings>();
            var soundSettings = GetSingleton<SoundSettings>();
            var sampleRate = soundSettings.sampleRate;
            var instantKO = statSettings.instantKO;
            var attackedCharacterNote = 36;
            var attackedVoxelNote = 32;
            var attackedMissedNote = 42;
            var noteVariance = 4;
            var voxelDamagePrefab = VoxelHealthDamageSystem.voxelDamagePrefab;
            var damagePrefab = StatsSystemGroup.damagePrefab;
            var particleSystemPrefab = ParticleSystemGroup.particleSystemPrefab;
            var punchSoundVolume = soundSettings.skillSoundVolume;
            var soundPrefab = SoundPlaySystem.soundPrefab;
            var attackAngle = PhysicalAttackSystem.attackAngle;
            var attackDistance = PhysicalAttackSystem.attackDistance;
            var attackAngle2 = PhysicalAttackSystem.attackAngle2;
            var attackDistance2 = PhysicalAttackSystem.attackDistance2;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var elapsedTime = World.Time.ElapsedTime;
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity, OnRaycastVoxel>()
                .ForEach((Entity e, int entityInQueryIndex, ref PhysicalAttacking physicalAttacking, ref Animator animator, in Target target, in ZoxID zoxID, in Translation translation) =>
            {
                if (physicalAttacking.attackState == 0)
                {
                    // start punch animation
                    if (!HasComponent<Skeleton>(e))
                    {
                        if (animator.coreState == AnimationState.Attacking)
                        {
                            animator.coreState = AnimationState.None;
                            return;
                        }
                        animator.setCoreState = AnimationState.Attacking;
                    }
                    else
                    {
                        if (animator.rightArmState == AnimationState.Attacking)
                        {
                            animator.setRightArmState = AnimationState.None;
                            return;
                        }
                        animator.setRightArmState = AnimationState.Attacking;
                    }
                    physicalAttacking.attackState = 1;
                    animator.animationSpeed = physicalAttacking.attackSpeed;
                    // UnityEngine.Debug.LogError("Started Attack State.");
                    // Bone Collider System will handle collision with bone collider and other character
                    // play sound when attacking
                    var soundSeed = zoxID.id + (int)(256 * elapsedTime);
                    var random = new Random();
                    random.InitState((uint)soundSeed);
                    var soundEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, soundPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Seed(soundSeed));
                    var generateSound = new GenerateSound();
                    generateSound.CreateMusicSound((byte)(28 + random.NextInt(4)), 2, sampleRate);
                    generateSound.timeLength = 0.8f;
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new SoundData(ref random, generateSound.timeLength, generateSound.sampleRate, 1, punchSoundVolume));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new DelayEvent(elapsedTime, 0.05));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Translation { Value = translation.Value });
                }
                // collide time
                else if (physicalAttacking.attackState == 1)
                {
                    if (elapsedTime - physicalAttacking.attackingStartTime >= physicalAttacking.attackSpeed / 2)
                    {
                        var targetAlive = HasComponent<Character>(target.target) && !HasComponent<DeadEntity>(target.target);
                        var absAngle = math.abs(target.targetAngle);
                        if (targetAlive &&
                            ((target.targetDistance <= attackDistance && absAngle <= attackAngle)
                             || (target.targetDistance <= attackDistance2 && absAngle <= attackAngle2)))
                        {
                            physicalAttacking.attackState = 3; // hit
                        }
                        else
                        {
                            physicalAttacking.attackState = 4; // missed
                        }
                    }
                }
                // end attacking
                else if (physicalAttacking.attackState == 5)
                {
                    if (elapsedTime - physicalAttacking.attackingStartTime >= physicalAttacking.attackSpeed)
                    {
                        physicalAttacking.attackState = 6;
                        PostUpdateCommands.RemoveComponent<PhysicalAttacking>(entityInQueryIndex, e);
                        // UnityEngine.Debug.LogError("Finished Attack State.");
                        if (!HasComponent<Skeleton>(e))
                        {
                            animator.setCoreState = AnimationState.Idle;
                        }
                        else
                        {
                            animator.setRightArmState = AnimationState.PreAttack;
                        }
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            
            // The two below are did the punch hit
            // One for npc, one for players
            // the player one hits voxels too
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity, OnRaycastVoxel>()
                .WithNone<RaycastVoxel>()
                .ForEach((Entity e, int entityInQueryIndex, ref PhysicalAttacking physicalAttacking, in Target target,
                    in VoxLink voxLink, in Translation translation, in ZoxID zoxID) =>
            {
                if (physicalAttacking.attackState != 3 && physicalAttacking.attackState != 4)
                {
                    return;
                }
                // play sound when attacking
                var soundSeed = zoxID.id + (int)(256 * elapsedTime);
                var random = new Random();
                random.InitState((uint)soundSeed);
                var soundEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, soundPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Seed(soundSeed));
                var generateSound = new GenerateSound();
                generateSound.CreateMusicSound((byte)(28 + random.NextInt(4)), 2, sampleRate);
                generateSound.timeLength = 0.8f;
                if (physicalAttacking.attackState == 3)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, damagePrefab),
                        new ApplyDamage(target.target, e, physicalAttacking.attackDamage, 0, translation.Value, 1));
                    generateSound.CreateMusicSound((byte)(attackedCharacterNote + random.NextInt(noteVariance)), 0, sampleRate);
                }
                // missed
                else if (physicalAttacking.attackState == 4)
                {
                    generateSound.CreateMusicSound((byte)(attackedMissedNote + random.NextInt(noteVariance)), 0, sampleRate);
                }
               // sound.Initialize(soundSeed, generateSound.timeLength, generateSound.sampleRate, 1, punchSoundVolume);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new SoundData(ref random, generateSound.timeLength, generateSound.sampleRate, 1, punchSoundVolume));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new DelayEvent(elapsedTime, 0.05));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Translation { Value = translation.Value });
                physicalAttacking.attackState = 5;
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

            // Player Hit Voxel
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxScales = GetComponentLookup<VoxScale>(true);
            voxEntities.Dispose();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity, OnRaycastVoxel>()
                .ForEach((Entity e, int entityInQueryIndex, ref PhysicalAttacking physicalAttacking, in ZoxID zoxID,
                    in RaycastVoxel raycastVoxel, in VoxLink voxLink, in Translation translation, in GravityQuadrant gravityQuadrant) =>
            {
                if (physicalAttacking.attackState != 4)
                {
                    return;
                }
                // play sound when attacking
                var soundSeed = zoxID.id + (int)(256 * elapsedTime);    // physicalAttacking.seedID + 
                var random = new Random();
                random.InitState((uint)soundSeed);
                var soundEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, soundPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Seed(soundSeed));
                var generateSound = new GenerateSound();
                if (raycastVoxel.rayHitType == (byte) RayHitType.Voxel)
                {
                    var targetVoxel = raycastVoxel.hitVoxel.position;
                    var voxelHitDamage = physicalAttacking.attackDamage;
                    if (instantKO)
                    {
                        voxelHitDamage = 99999;
                    }
                    //! \todo Use Generic Damage with a target component of Voxel instead of VoxelDamage.
                    var voxelDamage = new VoxelDamage
                    {
                        damage = voxelHitDamage,
                        hitType = 0,
                        direction = gravityQuadrant.quadrant,
                        voxelMeta = raycastVoxel.hitVoxel.voxel,
                        position = targetVoxel,
                        vox = voxLink.vox,
                        chunk = raycastVoxel.hitVoxel.chunk,
                        chunkVoxelPosition = raycastVoxel.hitVoxel.chunkVoxelPosition,
                        chunkPosition = raycastVoxel.hitVoxel.chunkPosition,
                        minivox = raycastVoxel.hitVoxel.hitMinivox
                    };
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, voxelDamagePrefab), voxelDamage);
                    generateSound.CreateMusicSound((byte)(attackedVoxelNote + random.NextInt(noteVariance)), 0, sampleRate);
                    // spawn particles
                    // color should depend on voxel
                    var positionAdd = gravityQuadrant.GetUpDirection();
                    var voxScale = voxScales[raycastVoxel.hitVoxel.vox].scale;
                    positionAdd.x *= voxScale.x;
                    positionAdd.y *= voxScale.y;
                    positionAdd.z *= voxScale.z;
                    var sizeBuff = random.NextFloat(0.1f);
                    var spawnSize = new float3(0.5f + sizeBuff, 0.5f + sizeBuff, 0.5f + sizeBuff);
                    spawnSize.x *= voxScale.x;
                    spawnSize.y *= voxScale.y;
                    spawnSize.z *= voxScale.z;
                    var particlesSystemEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, particleSystemPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, particlesSystemEntity,
                        new ParticleSystem
                        {
                            timeSpawn = 0, // 0.001,
                            spawnRate = 16,
                            random = random,
                            lifeTime = 0.4 + random.NextFloat(0.2f),
                            timeBegun = elapsedTime,
                            spawnSize = spawnSize,
                            baseColor = new Color(14, 8, 8),
                            colorVariance = new float3(0.15f, 0.15f, 0.15f), 
                            positionAdd = positionAdd,
                            particleLife = 2
                        });
                    var targetVoxelRealPosition = VoxelUtilities.GetRealPosition(targetVoxel, voxScale);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, particlesSystemEntity, new Translation { Value = targetVoxelRealPosition });
                    PostUpdateCommands.SetComponent(entityInQueryIndex, particlesSystemEntity, voxLink);
                }
                else
                {
                    generateSound.CreateMusicSound((byte)(attackedMissedNote + random.NextInt(noteVariance)), 0, sampleRate);
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new SoundData(ref random, generateSound.timeLength, generateSound.sampleRate, 1, punchSoundVolume));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new DelayEvent(elapsedTime, 0.05));
                PostUpdateCommands.AddComponent(entityInQueryIndex, soundEntity, new Translation { Value = translation.Value });
                physicalAttacking.attackState = 5;
            })  .WithReadOnly(voxScales)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

            // Player Hits Character
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity, OnRaycastVoxel>()
                .ForEach((Entity e, int entityInQueryIndex, ref PhysicalAttacking physicalAttacking, in Target target, in ZoxID zoxID,
                    in VoxLink voxLink, in Translation translation) =>
            {
                if (physicalAttacking.attackState != 3)
                {
                    return;
                }
                // play sound when attacking
                var soundSeed = zoxID.id + (int)(256 * elapsedTime);    // physicalAttacking.seedID + 
                var random = new Random();
                random.InitState((uint)soundSeed);
                var soundEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, soundPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Seed(soundSeed));
                var generateSound = new GenerateSound();
                generateSound.CreateMusicSound((byte)(attackedCharacterNote + random.NextInt(noteVariance)), 0, sampleRate);
                generateSound.timeLength = 0.8f;
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new SoundData(ref random, generateSound.timeLength, generateSound.sampleRate, 1, punchSoundVolume));
                // if target is in range
                // if target is in direction
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, damagePrefab),
                    new ApplyDamage(target.target, e, physicalAttacking.attackDamage, 0, translation.Value, 1));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new DelayEvent(elapsedTime, 0.05));
                PostUpdateCommands.AddComponent(entityInQueryIndex, soundEntity, new Translation { Value = translation.Value });
                physicalAttacking.attackState = 5;
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}
                    // new float3(0, 0, 0);
                    /*if (gravityQuadrant.quadrant == PlanetSide.Down)
                    {
                        positionAdd.y = -1;
                    }
                    else if (gravityQuadrant.quadrant == PlanetSide.Up)
                    {
                        positionAdd.y = 1;
                    }
                    else if (gravityQuadrant.quadrant == PlanetSide.Left)
                    {
                        positionAdd.x = -1;
                    }
                    else if (gravityQuadrant.quadrant == PlanetSide.Right)
                    {
                        positionAdd.x = 1;
                    }
                    else if (gravityQuadrant.quadrant == PlanetSide.Backward)
                    {
                        positionAdd.z = -1;
                    }
                    else if (gravityQuadrant.quadrant == PlanetSide.Forward)
                    {
                        positionAdd.z = 1;
                    }*/