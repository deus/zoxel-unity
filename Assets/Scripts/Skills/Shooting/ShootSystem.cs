using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;
using Zoxel.Cameras;
using Zoxel.Bullets;
using Zoxel.Clans;

namespace Zoxel.Skills.Range
{
    //! Spawns bullets from a character.
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class ShootSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery cameraQuery;
        private Entity spawnBulletPrefab;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnBulletArchtype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnBullet),
                typeof(GenericEvent),
                typeof(DelayEvent));
            spawnBulletPrefab = EntityManager.CreateEntity(spawnBulletArchtype);
            cameraQuery = GetEntityQuery(
                ComponentType.ReadOnly<Camera>(),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<Rotation>());
            RequireForUpdate(processQuery);
            RequireForUpdate<SoundSettings>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var soundSettings = GetSingleton<SoundSettings>();
            var sampleRate = soundSettings.sampleRate;
            var soundVolume = soundSettings.skillSoundVolume;
            var soundPrefab = SoundPlaySystem.soundPrefab;
            var spawnBulletPrefab2 = spawnBulletPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<CameraLink>()
                .ForEach((Entity e, int entityInQueryIndex, ref Shooter shooter, in Translation position, in Rotation rotation) =>
            {
                if (shooter.triggered == 1)
                {
                    shooter.triggered = 2;
                    shooter.shootPosition = position.Value;
                    shooter.shootRotation = rotation.Value;
                }
            }).ScheduleParallel(Dependency);
            if (cameraQuery.CalculateEntityCount() != 0)
            {
                var cameraEntities = cameraQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
                Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
                var cameraPositions = GetComponentLookup<Translation>(true);
                var cameraRotitions = GetComponentLookup<Rotation>(true);
                cameraEntities.Dispose();
                Dependency = Entities
                    .WithAll<CameraLink>()
                    .ForEach((Entity e, int entityInQueryIndex, ref Shooter shooter, in CameraLink cameraLink) =>
                {
                    if (shooter.triggered == 1 && cameraPositions.HasComponent(cameraLink.camera))
                    {
                        shooter.triggered = 2;
                        shooter.shootPosition = cameraPositions[cameraLink.camera].Value;
                        shooter.shootRotation = cameraRotitions[cameraLink.camera].Value;
                    }
                })  .WithReadOnly(cameraPositions)
                    .WithReadOnly(cameraRotitions)
                    .ScheduleParallel(Dependency);
            }

            // Pass in entities that it spawns from
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref Shooter shooter, in Body body, in VoxLink voxLink, in ClanLink clanLink, in GravityQuadrant gravityQuadrant, in ZoxID zoxID) =>
            {
                if (shooter.triggered == 2)
                {
                    shooter.triggered = 0;
                    var shootPosition = shooter.shootPosition;
                    var shootRotation = shooter.shootRotation;
                    var bulletDepth = 0; //bulletDatam.model.data.size.z * (1/32f);
                    var shooterDepth = body.size.z;
                    var spawnPosition = shootPosition + math.mul(shootRotation, new float3(0, 0, bulletDepth + math.max(body.size.x, body.size.z)));
                    // play sound when attacking
                    var soundSeed = zoxID.id + (int)(2048 * elapsedTime);
                    var random = new Random();
                    random.InitState((uint)soundSeed);
                    var soundEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, soundPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new DelayEvent(elapsedTime, 0.05));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Translation { Value = shootPosition });
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Seed(soundSeed));
                    var generateSound = new GenerateSound();
                    generateSound.CreateMusicSound((byte)(26 + random.NextInt(8)), 1, sampleRate);
                    generateSound.timeLength = 0.4f;
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new SoundData(ref random, generateSound.timeLength, generateSound.sampleRate, 1, soundVolume));

                    /*var generateSound = new GenerateSound();
                    var sound = new Sound();
                    sound.Initialize(soundSeed, generateSound.timeLength, generateSound.sampleRate, 1,soundVolume);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, sound);*/

                    //var random = new Random();
                    //random.InitState((uint)(zoxID.id + elapsedTime));
                    var attackDamage = random.NextFloat(shooter.attackDamage.x, shooter.attackDamage.y);
                    var spawnBullet = new SpawnBullet(e, voxLink.vox, spawnPosition, shootRotation, attackDamage, clanLink.clan, zoxID.id, gravityQuadrant.quadrant);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, spawnBulletPrefab2), spawnBullet);
                
                    // UnityEngine.Debug.DrawLine(shootPosition, shootPosition + math.rotate(shootRotation, new float3(0, 0, 1)), UnityEngine.Color.red);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

//Debug.LogError("Shooting Bullet:");
/*Entity camera;
if (HasComponent<CameraLink>(e))
{
    camera = EntityManager.GetComponentData<CameraLink>(e).camera;
}
else
{
    camera = new Entity();
}*/

// spraying like a shotgun
/*}
else
{
    var betweenSpread = bulletData.betweenSpread;
    var spreadAmount = bulletData.spread; // new float2(15, 30);
    var max = spreadAmount / 2;
    var min = -max;
    var originalRotation = new Quaternion(
        shootRotation.value.x, shootRotation.value.y,
        shootRotation.value.z, shootRotation.value.w).eulerAngles;
    var tempRotation = originalRotation;
    for (float x = min.x; x <= max.x; x += betweenSpread)
    {
        tempRotation.x = (originalRotation.x + x) % 360;
        for (float y = min.y; y <= max.y; y += betweenSpread)
        {
            tempRotation.y = (originalRotation.y + y) % 360;
            BulletSystem.SpawnBullet(PostUpdateCommands, EntityManager,
                spawnMetaID,
                shootPosition + math.mul(shootRotation, new float3(0, 0, 0.3f)),
                Quaternion.Euler(tempRotation),
                e);
        }
    }
}*/