using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Zoxel.Skills.Authoring
{
    //! Settings for userSkillLinks, including disables and cheats.
    // [GenerateAuthoringComponent]
    public struct SkillsSettings : IComponentData
    {
        public int2 skillTextureSize;       // 32, 32
        public bool disableSkillAnimations;
        public bool isUnlimitedSkillPoints;
        public bool isInstantLevelUpSkills;
        public bool isInstantCastSkills;
        public bool isUnlimitedSkillUse;
        public bool isNoCooldowns;
        public bool isSummonMany;
    }

    public class SkillsSettingsAuthoring : MonoBehaviour
    {
        public int2 skillTextureSize;       // 32, 32
        public bool disableSkillAnimations;
        public bool isUnlimitedSkillPoints;
        public bool isInstantLevelUpSkills;
        public bool isInstantCastSkills;
        public bool isUnlimitedSkillUse;
        public bool isNoCooldowns;
        public bool isSummonMany;
    }

    public class SkillsSettingsAuthoringBaker : Baker<SkillsSettingsAuthoring>
    {
        public override void Bake(SkillsSettingsAuthoring authoring)
        {
            AddComponent(new SkillsSettings
            {
                skillTextureSize = authoring.skillTextureSize,
                disableSkillAnimations = authoring.disableSkillAnimations,
                isUnlimitedSkillPoints = authoring.isUnlimitedSkillPoints,
                isInstantLevelUpSkills = authoring.isInstantLevelUpSkills,
                isInstantCastSkills = authoring.isInstantCastSkills,
                isUnlimitedSkillUse = authoring.isUnlimitedSkillUse,
                isNoCooldowns = authoring.isNoCooldowns,
                isSummonMany = authoring.isSummonMany
            });       
        }
    }
}

        /*

        public void DrawUI()
        {
            GUILayout.Label("   > UserSkillLinks [" + SkillSystemGroup.skillsCount + "]");
            GUILayout.Label("   > User UserSkillLinks [" + SkillSystemGroup.userSkillsCount + "]");
            realSkillsSettings.DrawUI();
            realSkillsSettings.DrawDisableUI();
            realSkillsSettings.DrawCheatsUI();
        }
        
        public void DrawUI()
        {
            GUILayout.Label(" [userSkillLinks] ");
            //GUILayout.Label("   Minivox Render Distance");
            // = int.Parse(GUILayout.TextField(.ToString()));
        }

        public void DrawDisableUI()
        {
            GUILayout.Label(" [debug userSkillLinks] ");
            // isUnlimitedSkillPoints = GUILayout.Toggle(isUnlimitedSkillPoints, "Unlimited Skill Points");
        }

        public void DrawCheatsUI()
        {
            GUILayout.Label(" [debug userSkillLinks] ");
            isUnlimitedSkillPoints = GUILayout.Toggle(isUnlimitedSkillPoints, "Unlimited Skill Points");
            isInstantCastSkills = GUILayout.Toggle(isInstantCastSkills, "Instant Cast Skills");
            isUnlimitedSkillUse = GUILayout.Toggle(isUnlimitedSkillUse, "Infinite Skill Use");
            isNoCooldowns = GUILayout.Toggle(isNoCooldowns, "No Cooldowns");
            isSummonMany = GUILayout.Toggle(isSummonMany, "Summon Many");
        }*/