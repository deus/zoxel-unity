using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Stats;
using Zoxel.Actions;

namespace Zoxel.Skills
{
    //! Triggers a ranged attack!
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class ShieldTriggerSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery skillsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            skillsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Skill>(),
                // ComponentType.ReadOnly<SkillDamage>(),
                ComponentType.ReadOnly<SkillShield>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(skillsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var skillEntities = skillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            // var skillDamages = GetComponentLookup<SkillDamage>(true);
            skillEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .WithAll<ShieldTrigger>()
                .ForEach((Entity e, int entityInQueryIndex, in ActivateSkill activateSkill) =>
            {
                var skillEntity = activateSkill.skill;
                // var skillDamage = skillDamages[skillEntity];
                /*shooter.triggered = 1;
                shooter.attackDamage = skillDamage.amount; // new float2(attackDamage, attackDamageMax);*/
                if (!HasComponent<Shielding>(e))
                {
                    PostUpdateCommands.AddComponent<Shielding>(entityInQueryIndex, e);
                }
                else
                {
                    PostUpdateCommands.RemoveComponent<Shielding>(entityInQueryIndex, e);
                }
            })  // .WithReadOnly(skillDamages)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}