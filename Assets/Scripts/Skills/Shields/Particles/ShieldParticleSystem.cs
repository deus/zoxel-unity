using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Particles;

namespace Zoxel.Skills
{
    //! Triggers a ranged attack!
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class ShieldParticleSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var particleSystemPrefab = ParticleSystemGroup.particleSystemPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .WithAll<Shielding>()
                .ForEach((Entity e, int entityInQueryIndex, in Translation translation, in Rotation rotation, in Seed seed, in VoxLink voxLink, in Body body,
                    in EntityLighting entityLighting) =>
            {
                var brightness = entityLighting.GetBrightness();
                var shieldColor = new Color(14, 155, 200, 65) * brightness;
                var colorVariance = new float3(0.1f, 0.1f, 0.1f) * brightness;
                shieldColor.alpha = 42;
                var spawnSize = new float3(body.size.x * 1.2f, body.size.y * 1.1f, body.size.z * 1.2f); // new float3(1f, 1f, 1f);
                var random = new Random();
                random.InitState((uint) (elapsedTime * 100 + seed.seed));
                var particlesSystemEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, particleSystemPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, particlesSystemEntity,
                    new ParticleSystem
                    {
                        random = random,
                        timeBegun = elapsedTime,
                        timeSpawn = 0.02,
                        spawnRate = 8,
                        particleLife = 2,
                        lifeTime = 0.12 + random.NextFloat(-0.06f, 0.06f),
                        spawnSize = spawnSize,
                        baseColor = shieldColor,
                        colorVariance = colorVariance
                    });
                PostUpdateCommands.SetComponent(entityInQueryIndex, particlesSystemEntity, translation);
                PostUpdateCommands.SetComponent(entityInQueryIndex, particlesSystemEntity, rotation);
                PostUpdateCommands.SetComponent(entityInQueryIndex, particlesSystemEntity, voxLink);
                // rotate particle system too
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}