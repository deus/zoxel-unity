using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Stats;
using Zoxel.Stats.Authoring;

namespace Zoxel.Skills
{
    //! Generates UserSkillLinks for the Realm.
    /**
    *   - Data Generation System -
    *   Generates UserSkillLinks for the Realm.
    *   \todo Use userStatLinks for skill attack speed.
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class SkillsGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<StatSettings>();
        }

        protected override void OnUpdate()
        {
            var statSettings = GetSingleton<StatSettings>();
            var skillPrefab = SkillSystemGroup.skillPrefab;
            var defaultAttackSpeed = statSettings.defaultAttackSpeed;
            var defaultResourceCost = statSettings.defaultResourceCost;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); //.AsParallelWriter();
            Entities.ForEach((Entity e, ref GenerateRealm generateRealm, ref SkillLinks skillLinks, in StatLinks statLinks, in ZoxID zoxID) =>
            {
                if (generateRealm.state != (byte) GenerateRealmState.GenerateSkills)
                {
                    return; // wait for thingo
                }
                var realmSeed = zoxID.id;
                if (realmSeed == 0)
                {
                    return;
                }
                generateRealm.state = (byte) GenerateRealmState.GenerateSkillTree;
                // states
                var healthEntity = new Entity();
                var energyEntity = new Entity();
                var manaEntity = new Entity();
                var stateStatsCount = 0;
                for (int i = 0; i < statLinks.userStatLinks.Length; i++)
                {
                    var statEntity = statLinks.userStatLinks[i];
                    if (HasComponent<RealmStateStat>(statEntity))
                    {
                        if (stateStatsCount == 0)
                        {
                            healthEntity = statEntity;
                        }
                        else if (stateStatsCount == 1)
                        {
                            energyEntity = statEntity;
                        }
                        else if (stateStatsCount == 2)
                        {
                            manaEntity = statEntity;
                        }
                        stateStatsCount++;
                    }
                }
                // Destroy UserSkillLinks and SkillTrees
                for (int i = 0; i < skillLinks.skills.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(skillLinks.skills[i]);
                }
                skillLinks.Dispose();
                // Spawn Skill Entities
                var skillsCount = 9;
                skillLinks.Initialize(skillsCount);
                for (int i = 0; i < skillLinks.skills.Length; i++)
                {
                    var dataID = realmSeed + 3000 + i * 1;
                    var random = new Random();
                    random.InitState((uint)dataID);
                    // spawn skill
                    var e2 = EntityManager.Instantiate(skillPrefab);
                    skillLinks.skills[i] = e2;
                    skillLinks.skillsHash[dataID] = e2;
                    var skillName = NameGenerator.GenerateName(ref random);
                    var skillType = SkillType.None;
                    var statUsed = new Entity();
                    var resourceCost = defaultResourceCost;
                    // Punch
                    if (i == 0 || i == 5)
                    {
                        statUsed = energyEntity;
                        resourceCost = 0.1f;
                        skillType = SkillType.PhysicalAttack;
                        var damage = 2f;
                        if (i == 5)
                        {
                            damage = 4f;
                            resourceCost = 0.3f;
                        }
                        PostUpdateCommands.AddComponent(e2, new SkillDamage(DamageType.Physical, damage));
                        PostUpdateCommands.AddComponent(e2, new SkillCastTime(defaultAttackSpeed / 1.5f));
                        PostUpdateCommands.AddComponent(e2, new SkillCooldown(defaultAttackSpeed / 1.5f));
                        PostUpdateCommands.AddComponent<SkillPhysicalAttack>(e2);
                        PostUpdateCommands.AddComponent<SkillVoxelInteraction>(e2);
                    }
                    // rogue stab
                    else if (i == 7)
                    {
                        statUsed = energyEntity;
                        resourceCost = 0.06f;
                        skillType = SkillType.PhysicalAttack;
                        var damage = 1f;
                        var speed = defaultAttackSpeed / 2.3f;
                        PostUpdateCommands.AddComponent(e2, new SkillDamage(DamageType.Physical, damage));
                        PostUpdateCommands.AddComponent(e2, new SkillCastTime(speed));
                        PostUpdateCommands.AddComponent(e2, new SkillCooldown(speed));
                        PostUpdateCommands.AddComponent<SkillPhysicalAttack>(e2);
                        PostUpdateCommands.AddComponent<SkillVoxelInteraction>(e2);
                    }
                    // Shoot
                    else if (i == 1 || i == 6)
                    {
                        statUsed = manaEntity;
                        resourceCost = 0.5f;
                        var damage = 2f;
                        if (i == 6)
                        {
                            damage += 2;
                            resourceCost = 1f;
                        }
                        skillType = SkillType.Projectile;
                        PostUpdateCommands.AddComponent(e2, new SkillCastTime(1));
                        PostUpdateCommands.AddComponent(e2, new SkillCooldown(1));
                        PostUpdateCommands.AddComponent(e2, new SkillDamage(DamageType.Magical, damage));
                        PostUpdateCommands.AddComponent(e2, new SkillProjectile(1, 1f, 1f));
                    }
                    // Heal Self
                    else if (i == 3)
                    {
                        // cast buff on self
                        //      spawn particles on self - white ones
                        //      Play bell sound
                        //      Heal every 3 seconds for 9 seconds
                        //      Show heal buff above statbars
                        statUsed = manaEntity;
                        resourceCost = 3f;
                        skillType = SkillType.Augmentation;
                        PostUpdateCommands.AddComponent(e2, new SkillCastTime(3));
                        PostUpdateCommands.AddComponent(e2, new SkillCooldown(15));
                        PostUpdateCommands.AddComponent(e2, new SkillDamage(DamageType.Healing, 1f));
                        PostUpdateCommands.AddComponent(e2, new SkillAugmentation(5));
                    }
                    // Summon Character
                    else if (i == 2)
                    {
                        statUsed = manaEntity;
                        resourceCost = 4;
                        skillType = SkillType.SummonMinion;
                        PostUpdateCommands.AddComponent(e2, new SkillCastTime(5));
                        PostUpdateCommands.AddComponent(e2, new SkillCooldown(24));
                        PostUpdateCommands.AddComponent(e2, new SkillSummonCharacter(1));
                        PostUpdateCommands.AddComponent<SkillVoxelInteraction>(e2);
                    }
                    // Summon Turret
                    else if (i == 4)
                    {
                        statUsed = manaEntity;
                        resourceCost = 3;
                        skillType = SkillType.SummonTurret;
                        PostUpdateCommands.AddComponent(e2, new SkillCastTime(5));
                        PostUpdateCommands.AddComponent(e2, new SkillCooldown(15));
                        PostUpdateCommands.AddComponent(e2, new SkillSummonTurret());
                        PostUpdateCommands.AddComponent<SkillVoxelInteraction>(e2);
                    }
                    // Mana Shield
                    else if (i == 8)
                    {
                        // cast buff on self
                        //      spawn particles on self - white ones
                        //      Play bell sound
                        //      Heal every 3 seconds for 9 seconds
                        //      Show heal buff above statbars
                        statUsed = manaEntity;
                        resourceCost = 1f;
                        skillType = SkillType.Shield;
                        PostUpdateCommands.AddComponent(e2, new SkillCastTime(1f));
                        PostUpdateCommands.AddComponent(e2, new SkillCooldown(1f));
                        // how much damage absorb per mana point
                        // how much cost per second activated
                        PostUpdateCommands.AddComponent(e2, new SkillShield());
                    }
                    PostUpdateCommands.SetComponent(e2, new Skill(skillType));
                    PostUpdateCommands.SetComponent(e2, new ZoxID(dataID));
                    PostUpdateCommands.SetComponent(e2, new ZoxName(skillName));
                    PostUpdateCommands.SetComponent(e2, new SkillCost(statUsed, resourceCost));
                }
            }).WithStructuralChanges().Run();   // spawns skill tree and skill data
        }
    }
}