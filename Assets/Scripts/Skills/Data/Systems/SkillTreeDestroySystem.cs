using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Skills
{
    //! Destroys all children and disposes of the links using DestroyEntity event.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
	public partial class SkillTreeDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyEntity, Skilltree>()
                .ForEach((Entity e, int entityInQueryIndex, in Skilltree skilltree) =>
            {
               // UnityEngine.Debug.LogError("Children Count: " + childrens.children.Length);
                for (int i = 0; i < skilltree.nodes.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, skilltree.nodes[i]);
                }
                skilltree.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<DestroyEntity, SkilltreeNode>()
                .ForEach((Entity e, int entityInQueryIndex, in SkilltreeNode skilltreeNode) =>
            {
                skilltreeNode.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}