using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Zoxel.Stats;
// Todo: Generate skills here too per skill tree

namespace Zoxel.Skills
{
    //! Generates SkillTrees for the Realm.
    /**
    *   - Data Generation System -
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class SkilltreeGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity skillTreePrefab;
        private Entity skillTreeNodePrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var skillTreeArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(Skilltree));
            skillTreePrefab = EntityManager.CreateEntity(skillTreeArchetype);
            var skillNodeArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SkilltreeNode));
            skillTreeNodePrefab = EntityManager.CreateEntity(skillNodeArchetype);
        }

        protected override void OnUpdate()
        {
            if (StatsManager.instance == null) return;
            var energyID = StatsManager.instance.StatSettings.energyDatam.Value.id;   // 1415374457;
            var manaID = StatsManager.instance.StatSettings.manaDatam.Value.id;   // 1044131199;
            var skillTreePrefab =  this.skillTreePrefab;
            var skillTreeNodePrefab = this.skillTreeNodePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); //.AsParallelWriter();
            Entities.ForEach((Entity e, ref SkilltreeLinks skilltreeLinks, ref GenerateRealm generateRealm, in ZoxID zoxID, in SkillLinks skillLinks) =>
            {
                if (generateRealm.state != (byte) GenerateRealmState.GenerateSkillTree)
                {
                    return; // wait for thingo
                }
                var realmSeed = zoxID.id;
                if (realmSeed == 0)
                {
                    return;
                }
                generateRealm.state = (byte) GenerateRealmState.GenerateClasses;
                for (int i = 0; i < skilltreeLinks.skilltrees.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(skilltreeLinks.skilltrees[i]);
                }
                skilltreeLinks.Dispose();
                skilltreeLinks.Initialize(7);
                /*var skills = new NativeArray<Entity>(skillLinks.skills.Length, Allocator.Temp);
                for (int i = 0; i < skillLinks.skills.Length; i++)
                {
                    skills[i] = skillLinks.skills[i];
                }*/
                for (int i = 0; i < skilltreeLinks.skilltrees.Length; i++)
                {
                    var skillTreeEntity = EntityManager.Instantiate(skillTreePrefab);
                    skilltreeLinks.skilltrees[i] = skillTreeEntity;
                    var skilltree = new Skilltree();
                    var punchLvl1Entity = skillLinks.skills[0];
                    var fireballLvl1Entity = skillLinks.skills[1];
                    var summonMinionLvl1Entity = skillLinks.skills[2];
                    var healLvl1Entity = skillLinks.skills[3];
                    var summonTurrentLvl1Entity = skillLinks.skills[4];
                    var punchLvl2Entity = skillLinks.skills[5];
                    var fireballLvl2Entity = skillLinks.skills[6];
                    var stabLvl1Entity = skillLinks.skills[7];
                    var shieldLvl1Entity = skillLinks.skills[8];
                    if (i == 0)
                    {
                        skilltree.Initialize(2);
                        // Skill Nodes
                        skilltree.nodes[1] = EntityManager.Instantiate(skillTreeNodePrefab);
                        PostUpdateCommands.SetComponent(skilltree.nodes[1], new SkilltreeNode(new int2(1, 2), punchLvl2Entity));
                        skilltree.nodes[0] = EntityManager.Instantiate(skillTreeNodePrefab);
                        PostUpdateCommands.SetComponent(skilltree.nodes[0], new SkilltreeNode(new int2(1, 0), punchLvl1Entity, skilltree.nodes[1]));
                    }
                    // Mage
                    else if (i == 1)
                    {
                        skilltree.Initialize(3);
                        // Skill Nodes
                        skilltree.nodes[1] = EntityManager.Instantiate(skillTreeNodePrefab);
                        PostUpdateCommands.SetComponent(skilltree.nodes[1], new SkilltreeNode(new int2(3, 2), fireballLvl2Entity));
                        skilltree.nodes[0] = EntityManager.Instantiate(skillTreeNodePrefab);
                        PostUpdateCommands.SetComponent(skilltree.nodes[0], new SkilltreeNode(new int2(3, 0), fireballLvl1Entity, skilltree.nodes[1]));
                        skilltree.nodes[2] = EntityManager.Instantiate(skillTreeNodePrefab);
                        PostUpdateCommands.SetComponent(skilltree.nodes[2], new SkilltreeNode(new int2(5, 0), shieldLvl1Entity));
                        // for now
                        PostUpdateCommands.AddComponent<StartingSkilltreeNode>(skilltree.nodes[2]);
                    }
                    // Summoner
                    else if (i == 3)
                    {
                        skilltree.Initialize(2);
                        // Skill Nodes
                        skilltree.nodes[0] = EntityManager.Instantiate(skillTreeNodePrefab);
                        PostUpdateCommands.SetComponent(skilltree.nodes[0], new SkilltreeNode(new int2(2, 0), fireballLvl1Entity));
                        skilltree.nodes[1] = EntityManager.Instantiate(skillTreeNodePrefab);
                        PostUpdateCommands.SetComponent(skilltree.nodes[1], new SkilltreeNode(new int2(4, 0), summonMinionLvl1Entity));
                        PostUpdateCommands.AddComponent<StartingSkilltreeNode>(skilltree.nodes[1]);
                    }
                    // Engineer
                    else if (i == 4)
                    {
                        skilltree.Initialize(2);
                        // Skill Nodes
                        skilltree.nodes[0] = EntityManager.Instantiate(skillTreeNodePrefab);
                        PostUpdateCommands.SetComponent(skilltree.nodes[0], new SkilltreeNode(new int2(2, 0), punchLvl1Entity));
                        skilltree.nodes[1] = EntityManager.Instantiate(skillTreeNodePrefab);
                        PostUpdateCommands.SetComponent(skilltree.nodes[1], new SkilltreeNode(new int2(4, 0), summonTurrentLvl1Entity));
                    }
                    // Healer
                    else if (i == 5)
                    {
                        skilltree.Initialize(2);
                        // Skill Nodes
                        skilltree.nodes[0] = EntityManager.Instantiate(skillTreeNodePrefab);
                        PostUpdateCommands.SetComponent(skilltree.nodes[0], new SkilltreeNode(new int2(2, 0), fireballLvl1Entity));
                        skilltree.nodes[1] = EntityManager.Instantiate(skillTreeNodePrefab);
                        PostUpdateCommands.SetComponent(skilltree.nodes[1], new SkilltreeNode(new int2(4, 0), healLvl1Entity));
                    }
                    // Rogue or random ones
                    else
                    {
                        skilltree.Initialize(1);
                        skilltree.nodes[0] = EntityManager.Instantiate(skillTreeNodePrefab);
                        PostUpdateCommands.SetComponent(skilltree.nodes[0], new SkilltreeNode(new int2(2, 0), stabLvl1Entity));
                    }
                    // start with first node
                    PostUpdateCommands.AddComponent<StartingSkilltreeNode>(skilltree.nodes[0]);
                    PostUpdateCommands.SetComponent(skillTreeEntity, skilltree);
                }
                // skills.Dispose();
            }).WithStructuralChanges().Run();   // spawns skill tree and skill data
        }
    }
}
                    // For now just give them all skills
                    /*for (int j = 0; j < 5; j++)
                    {
                        skilltree.nodes[j] = EntityManager.Instantiate(skillTreeNodePrefab);
                        PostUpdateCommands.SetComponent(skilltree.nodes[j], new SkilltreeNode(new int2(j, 0), skillLinks.skills[j]));
                    }*/
                    /*var meleeSkillTreeNode = EntityManager.Instantiate(skillTreeNodePrefab);
                    var meleeSkillTreeNode = EntityManager.Instantiate(skillTreeNodePrefab);
                    skilltree.nodes[0] = new SkilltreeNode(new int2(0, 0), skillIDs[0], new int[] { } );    //  skillIDs[1]
                    //skilltree.nodes[1] = new SkilltreeNode(new int2(1, 1), skillIDs[1], new int[] { skillIDs[3] } );
                    //skilltree.nodes[2] = new SkilltreeNode(new int2(1, 3), skillIDs[3], new int[] { skillIDs[5] } );
                    //skilltree.nodes[3] = new SkilltreeNode(new int2(2, 3), skillIDs[5], new int[] {  } );
                    skilltree.nodes[1] = new SkilltreeNode(new int2(1, 0), skillIDs[1], new int[] { } );   // skillIDs[7]
                    //skilltree.nodes[5] = new SkilltreeNode(new int2(4, 1), skillIDs[7], new int[] { skillIDs[8] } );
                    //skilltree.nodes[6] = new SkilltreeNode(new int2(4, 2), skillIDs[8], new int[] {  } );
                    skilltree.nodes[2] = new SkilltreeNode(new int2(2, 0), skillIDs[2], new int[] { } );
                    skilltree.nodes[3] = new SkilltreeNode(new int2(4, 0), skillIDs[3], new int[] { } );
                    skilltree.nodes[4] = new SkilltreeNode(new int2(6, 0), skillIDs[4], new int[] { } );*/

                        //skilltree.nodes[2] = EntityManager.Instantiate(skillTreeNodePrefab);
                        //PostUpdateCommands.SetComponent(skilltree.nodes[6], new SkilltreeNode(new int2(1, 2), skillLinks.skills[6]));
                        // SkilltreeNodeLinks
                        // PostUpdateCommands.SetComponent(skilltree.nodes[0], new SkilltreeNode(new int2(0, 0), punchLvl1Entity, skilltree.nodes[1]));
                        // PostUpdateCommands.SetComponent(skilltree.nodes[1], new SkilltreeNode(new int2(1, 0), skillLinks.skills[1], skilltree.nodes[6]));