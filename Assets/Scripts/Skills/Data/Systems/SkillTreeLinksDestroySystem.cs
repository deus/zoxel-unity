using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Skills
{
    //! Disposes of SkilltreeLinks made up of Skilltree datas.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class SkillTreeLinksDestroySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithAll<DestroyEntity, SkilltreeLinks>()
                .ForEach((int entityInQueryIndex, in SkilltreeLinks skilltreeLinks) =>
			{
                for (int i = 0; i < skilltreeLinks.skilltrees.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, skilltreeLinks.skilltrees[i]);
                }
                skilltreeLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}