using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Skills
{
    //! Disposes of SkillLinks made up of skill datas.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class SkillLinksDestroySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithAll<DestroyEntity, SkillLinks>()
                .ForEach((int entityInQueryIndex, in SkillLinks skillLinks) =>
			{
                for (int i = 0; i < skillLinks.skills.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, skillLinks.skills[i]);
                }
                skillLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}