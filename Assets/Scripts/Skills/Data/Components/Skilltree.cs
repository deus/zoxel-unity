using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Skills
{
    //! Contains skill nodes to unlock for a character.
    public struct Skilltree : IComponentData
    {
        public BlitableArray<Entity> nodes;

        public void Dispose()
        {
            nodes.Dispose();
        }

        public void Initialize(int count)
        {
            nodes = new BlitableArray<Entity>(count, Allocator.Persistent);
        }
    }
}