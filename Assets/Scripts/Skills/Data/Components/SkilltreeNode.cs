using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Skills
{
    //! A node on the Skilltree.
    public struct SkilltreeNode : IComponentData
    {
        //! Position on skilltree
        public int2 position;
        //! Skill unlocked by purchasing the node
        public Entity skill;
        //! Links to next skill tree nodes
        public BlitableArray<Entity> nodes;

        //! For single nodes
        public SkilltreeNode(int2 position, Entity skill)
        {
            this.position = position;
            this.skill = skill;
            this.nodes = new BlitableArray<Entity>();
        }

        //! For Nodes linked to others
        public SkilltreeNode(int2 position, Entity skill, Entity dependentNode)
        {
            this.position = position;
            this.skill = skill;
            this.nodes = new BlitableArray<Entity>(1, Allocator.Persistent);
            this.nodes[0] = dependentNode;
        }

        //! For Nodes linked to others
        public SkilltreeNode(int2 position, Entity skill, Entity[] nodes)
        {
            this.position = position;
            this.skill = skill;
            this.nodes = new BlitableArray<Entity>(nodes.Length, Allocator.Persistent);
            for (int i = 0; i < nodes.Length; i++)
            {
                this.nodes[i] = nodes[i];
            }
        }

        public void Dispose()
        {
            nodes.Dispose();
        }

        public void Initialize(int count)
        {
            nodes = new BlitableArray<Entity>(count, Allocator.Persistent);
        }
    }
}