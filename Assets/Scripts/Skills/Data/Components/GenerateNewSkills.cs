using Unity.Entities;

namespace Zoxel.Skills
{
    public struct GenerateSkills : IComponentData
    {
        public byte spawnType;
        
        public GenerateSkills(byte spawnType)
        {
            this.spawnType = spawnType;
        }
    }
}