using Unity.Entities;

namespace Zoxel.Skills
{
    //! A tag for a starting skill of a class, job, race, etc.
    public struct StartingSkilltreeNode : IComponentData { }
}