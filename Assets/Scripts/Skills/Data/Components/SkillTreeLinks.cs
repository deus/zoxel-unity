using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Skills
{
    // Meta data for userSkillLinks now generated per game
    public struct SkilltreeLinks : IComponentData
    {
        public BlitableArray<Entity> skilltrees;

        public void Dispose()
        {
            skilltrees.Dispose();
        }

        public void Initialize(int count)
        {
            skilltrees = new BlitableArray<Entity>(count, Allocator.Persistent);
        }
    }
}