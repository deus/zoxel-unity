using Unity.Entities;

namespace Zoxel.Skills
{
    //! Holds a link to the item held in the right hand based on action selected.
    public struct SkilltreeLink : IComponentData
    {
        public Entity skilltree;

        public SkilltreeLink(Entity skilltree)
        {
            this.skilltree = skilltree;
        }
    }
}