using Unity.Entities;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Zoxel.Skills
{
    //! Meta data for skills now generated per game.
    public struct SkillLinks : IComponentData
    {
        public BlitableArray<Entity> skills;
        public UnsafeParallelHashMap<int, Entity> skillsHash;

        public void Dispose()
        {
            skills.Dispose();
			if (skillsHash.IsCreated)
			{
				skillsHash.Dispose();
			}
        }

        public void Initialize(int count)
        {
            Dispose();
            this.skills = new BlitableArray<Entity>(count, Allocator.Persistent);
            this.skillsHash = new UnsafeParallelHashMap<int, Entity>(count, Allocator.Persistent);
        }

        public Entity GetSkill(int skillID)
        {
            if (skillsHash.IsCreated)
            {
                Entity outputEntity;
                if (skillsHash.TryGetValue(skillID, out outputEntity))
                {
                    return outputEntity;
                }
            }
            return new Entity();
        }
    }
}