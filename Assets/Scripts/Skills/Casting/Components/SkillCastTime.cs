using Unity.Entities;

namespace Zoxel.Skills
{
    //! Information about a Skill's CastTime.
    /**
    *   Used currently for fireballs.
    */
    public struct SkillCastTime : IComponentData
    {
        public float castTime;

        public SkillCastTime(float castTime)
        {
            this.castTime = castTime;
        }
    }
}