using Unity.Entities;

namespace Zoxel.Skills
{
    public struct LoadSkills : IComponentData
    {
        public Text loadPath;
        public AsyncFileReader reader;

        public void Dispose()
        {
            loadPath.Dispose();
            reader.Dispose();
        }
    }
}