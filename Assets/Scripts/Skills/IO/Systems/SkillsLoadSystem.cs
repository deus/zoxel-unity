using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Skills
{
    //! Loads the userSkillLinks from memory.
    /**
    *   - Load System -
    */
    [BurstCompile, UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class SkillsLoadSystem : SystemBase
    {
        const string filename = "Skills.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<SkillLinks>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithNone<LoadStats>()
                .WithAll<UserSkillLinks, RealmLink>()
                .ForEach((ref LoadSkills loadSkills, in EntitySaver entitySaver) =>
            {
                if (loadSkills.loadPath.Length == 0)
                {
                    loadSkills.loadPath = new Text(entitySaver.GetPath() + filename);
                }
                loadSkills.reader.UpdateOnMainThread(in loadSkills.loadPath);
            }).WithoutBurst().Run();
            var userSkillPrefab = SkillSystemGroup.userSkillPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var realmSkillLinks = GetComponentLookup<SkillLinks>(true);
            realmEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity, InitializeSaver>()
                .WithNone<LoadStats>()
                .WithAll<UserSkillLinks, EntitySaver>()
                .ForEach((Entity e, int entityInQueryIndex, ref LoadSkills loadSkills, in RealmLink realmLink) =>
            {
                if (loadSkills.reader.IsReadFileInfoComplete())
                {
                    if (loadSkills.reader.DoesFileExist())
                    {
                        loadSkills.reader.state = AsyncReadState.StartOpenFile;
                    }
                    else
                    {
                        // UnityEngine.Debug.LogError("File Did not exist.");
                        loadSkills.Dispose();
                        PostUpdateCommands.RemoveComponent<LoadSkills>(entityInQueryIndex, e);
                    }
                }
                else if (loadSkills.reader.IsReadFileComplete())
                {
                    if (loadSkills.reader.DidFileReadSuccessfully())
                    {
                        var bytes = loadSkills.reader.GetBytes();
                        var realmSkillLinks2 = realmSkillLinks[realmLink.realm];
                        var skillsCount = (byte) (bytes.Length / 4);
                        // userSkillLinks.Initialize(bytes.Length / 4);
                        var userSkillEntities = new NativeArray<Entity>(skillsCount, Allocator.Temp);
                        PostUpdateCommands.Instantiate(entityInQueryIndex, userSkillPrefab, userSkillEntities);
                        for (int i = 0; i < skillsCount; i++)
                        {
                            var skillID = ByteUtil.GetInt(in bytes, i * 4);
                            // UnityEngine.Debug.LogError("Could not find SkillID " + skillID);
                            var skillEntity = realmSkillLinks2.GetSkill(skillID);
                            var userSkillEntity = userSkillEntities[i];
                            PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new MetaData(skillEntity));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new UserDataIndex(i));
                            // PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, characterLink);
                        }
                        PostUpdateCommands.AddComponent(entityInQueryIndex, userSkillEntities, new CharacterLink(e));
                        userSkillEntities.Dispose();
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUserSkillSpawned(skillsCount));
                    }
                    // Dispose
                    loadSkills.Dispose();
                    PostUpdateCommands.RemoveComponent<LoadSkills>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(realmSkillLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}