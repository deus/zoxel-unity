using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using System;
using System.IO;

namespace Zoxel.Skills
{
    //! Saves the userSkillLinks to memory.
    /**
    *   - Save System -
    */
    [UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class SkillsSaveSystem : SystemBase
    {
        const string filename = "Skills.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery processQuery2;
        private EntityQuery userSkillsQuery;
        private EntityQuery skillsQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery2 = GetEntityQuery(
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<DisableSaving>(),
                ComponentType.ReadOnly<SaveSkills>(),
                ComponentType.ReadOnly<UserSkillLinks>());
            userSkillsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserSkill>(),
                ComponentType.Exclude<DestroyEntity>());
            skillsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Skill>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<DestroyEntity>());
        }

        protected override void OnUpdate()
        {
            if (!processQuery2.IsEmpty)
            {
                var PostUpdateCommands3 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
                PostUpdateCommands3.RemoveComponent<SaveSkills>(processQuery2);
            }
            if (processQuery.IsEmpty)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands.RemoveComponent<SaveSkills>(processQuery);
            var userSkillEntities = userSkillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var userSkillMetaDatas = GetComponentLookup<MetaData>(true);
            var skillEntities = skillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var skillIDs = GetComponentLookup<ZoxID>(true);
            userSkillEntities.Dispose();
            skillEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<LoadSkills, GenerateSkills, OnUserSkillSpawned>()
                .WithNone<InitializeSaver, DisableSaving>()
                .WithAll<SaveSkills>()
                .ForEach((in EntitySaver entitySaver, in UserSkillLinks userSkillLinks) =>
            {
                var filepath = entitySaver.GetPath() + filename;
                var bytes = new BlitableArray<byte>(userSkillLinks.skills.Length * 4, Allocator.Temp);
                for (int i = 0; i < userSkillLinks.skills.Length; i++)
                {
                    var userSkillEntity = userSkillLinks.skills[i];
                    if (userSkillEntity.Index == 0 || !userSkillMetaDatas.HasComponent(userSkillEntity))
                    {
                        ByteUtil.SetInt(ref bytes, i * 4, 0);
                        continue;
                    }
                    var skillEntity = userSkillMetaDatas[userSkillEntity].data;
                    if (skillEntity.Index == 0 || !skillIDs.HasComponent(skillEntity))
                    {
                        ByteUtil.SetInt(ref bytes, i * 4, 0);
                        continue;
                    }
                    ByteUtil.SetInt(ref bytes, i * 4, skillIDs[skillEntity].id);
                }
                #if WRITE_ASYNC
                File.WriteAllBytesAsync(filepath, bytes.ToArray());
                #else
                File.WriteAllBytes(filepath, bytes.ToArray());
                #endif
                bytes.Dispose();
            })  .WithReadOnly(userSkillMetaDatas)
                .WithReadOnly(skillIDs)
                .WithoutBurst().Run();
        }
    }
}

// first byte will be skill type

/*saveString += skill.name.ToString() + newLine;
saveString += skill.skillType + newLine;
saveString += skill.statUsedID + newLine;
saveString += skill.statUsedValue + newLine;
saveString += skill.cooldown + newLine;
saveString += skill.attackDamage + newLine;
saveString += skill.attackSpeed + newLine;
saveString += skill.attackForce + newLine;*/
// depending on type, data will be stored and loaded differently


/*PostUpdateCommands.RemoveComponent<SaveSkills>(e);
var saveString = "";
for (int i = 0; i < userSkillLinks.skills.Length; i++)
{
    var userSkillEntity = userSkillLinks.skills[i];
    var userSkill = EntityManager.GetComponentData<UserSkill>(userSkillEntity);
    var skillID = EntityManager.GetComponentData<ZoxID>(userSkill.skill).id;
    saveString += skillID + newLine;
}
try
{
    // UnityEngine.Debug.LogError("Saved userSkillLinks to: " + filepath + " with: " + userSkillLinks.skills.Length + "\n" + saveString);
    File.WriteAllText(filepath, saveString);
}
catch (Exception exception)
{
    UnityEngine.Debug.LogError("Error Saving UserSkillLinks: " + exception);
}*/