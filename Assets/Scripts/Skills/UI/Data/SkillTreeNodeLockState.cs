namespace Zoxel.Skills
{
    public static class SkillteeNodeLockState
    {
        public const byte Unlocked = 0;
        public const byte CanUnlock = 1;
        public const byte Locked = 2;
    }
}