using Unity.Entities;

namespace Zoxel.Skills.UI.Skilltrees
{
    //! A generic event for spawning a SkilltreeUI Entity.
    public struct SpawnSkilltreeUI : IComponentData { }
}