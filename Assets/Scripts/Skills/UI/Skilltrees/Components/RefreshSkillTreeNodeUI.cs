using Unity.Entities;

namespace Zoxel.Skills.UI.Skilltrees
{
    //! Refreshes a skillnode ui entities colors.
    public struct RefreshSkilltreeNodeUI : IComponentData
    {
        public byte skilltreeNodeState;

        public RefreshSkilltreeNodeUI(byte skilltreeNodeState)
        {
            this.skilltreeNodeState = skilltreeNodeState;
        }
    }
}