using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Skills.UI.Skilltrees
{
    //! A tag for a SkilltreeUI Button Entity.
    public struct SkilltreeButton : IComponentData
    {
        public Entity skilltree;
        public Entity skilltreeNode;
        public byte skilltreeNodeState;

        public SkilltreeButton(Entity skilltree, Entity skilltreeNode, byte skilltreeNodeState)
        {
            this.skilltree = skilltree;
            this.skilltreeNode = skilltreeNode;
            this.skilltreeNodeState = skilltreeNodeState;
        }
    }
}