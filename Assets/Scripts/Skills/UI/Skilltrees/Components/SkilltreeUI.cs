using Unity.Entities;

namespace Zoxel.Skills.UI.Skilltrees
{
    //! A tag for a SkilltreeUI entity.
    public struct SkilltreeUI : IComponentData { }
}