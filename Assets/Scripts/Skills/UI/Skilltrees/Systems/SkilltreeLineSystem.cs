using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Lines;

namespace Zoxel.Skills.UI.Skilltrees
{
    //! Summons a SpellTree UI.
    /**
    *   \todo Make permanent line entities instead.
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(SkillUISystemGroup))]
    public partial class SkilltreeLineSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var deltaTime = (float) World.Time.DeltaTime;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var panelStyle = uiDatam.panelStyle.GetStyle();
            var iconSize = uiDatam.GetIconSize(uiScale);
            var uiDepth = UIManager.instance.uiSettings.uiDepth;
            var lineWidth = 0.0042f * uiDepth * 3f;
            var unlockedNodeColor = new Color(UIManager.instance.uiDatam.unlockedNodeColor);
            var canUnlockNodeColor = new Color(UIManager.instance.uiDatam.canUnlockNodeColor);
            var lockedNodeColor = new Color(UIManager.instance.uiDatam.lockedNodeColor);
            var elapsedTime = World.Time.ElapsedTime;
            var linePrefab = RenderLineGroup.linePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .ForEach((Entity e, in Translation translation, in SkilltreeButton skilltreeButton, in ParentLink parentLink) =>
            {
                var position1 = translation.Value;
                if (!HasComponent<Childrens>(parentLink.parent))
                {
                    return;
                }
                var childrens = EntityManager.GetComponentData<Childrens>(parentLink.parent);
                var skilltreeNode = EntityManager.GetComponentData<SkilltreeNode>(skilltreeButton.skilltreeNode);
                for (int i = 0; i < skilltreeNode.nodes.Length; i++)
                {
                    var skilltreeNodeEntity = skilltreeNode.nodes[i];
                    // find the frame that corresponds to the node
                    for (int j = 0; j < childrens.children.Length; j++)
                    {
                        var frameEntity = childrens.children[j];
                        if (frameEntity == e)
                        {
                            continue;
                        }
                        var skilltreeButton2 = EntityManager.GetComponentData<SkilltreeButton>(frameEntity);
                        if (skilltreeNodeEntity == skilltreeButton2.skilltreeNode)
                        {
                            var skilltreeNodeState = skilltreeButton2.skilltreeNodeState;
                            var lineColor = unlockedNodeColor;
                            if (skilltreeNodeState == SkillteeNodeLockState.CanUnlock)
                            {
                                lineColor = canUnlockNodeColor;
                            }
                            else if (skilltreeNodeState == SkillteeNodeLockState.Locked)
                            {
                                lineColor = lockedNodeColor;
                            }
                            var translation2 = EntityManager.GetComponentData<Translation>(frameEntity);
                            var position2 = translation2.Value;
                            var direction = math.normalize(position1 - position2);
                            position1 -= direction * iconSize.y / 2f;
                            position2 += direction * iconSize.y / 2f;
                            RenderLineGroup.SpawnLine(PostUpdateCommands, linePrefab, elapsedTime, position1, position2, lineColor, lineWidth, deltaTime * 8f); // 0.04f);
                            // UnityEngine.Debug.DrawLine(position1, position2, UnityEngine.Color.red, 1f);
                            // UnityEngine.Debug.LogError("Positions: " + position1 + " :: " + position2);
                            break;
                        }
                    }
                }
            }).WithoutBurst().Run();
        }
    }
}