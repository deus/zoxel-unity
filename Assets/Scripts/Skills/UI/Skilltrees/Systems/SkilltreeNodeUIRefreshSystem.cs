using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Transforms;
using Zoxel.Textures;
using Zoxel.UI;
using Zoxel.Lines;

namespace Zoxel.Skills.UI.Skilltrees
{
    //! Refreshes a SkillltreeNodeUI after state has changed.
    [BurstCompile, UpdateInGroup(typeof(SkillUISystemGroup))]
    public partial class SkilltreeNodeUIRefreshSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var nodeBaseColor = new Color(UIManager.instance.uiDatam.nodeBaseColor);
            var unlockedNodeColor = new Color(UIManager.instance.uiDatam.unlockedNodeColor);
            var canUnlockNodeColor = new Color(UIManager.instance.uiDatam.canUnlockNodeColor);
            var lockedNodeColor = new Color(UIManager.instance.uiDatam.lockedNodeColor);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<RefreshSkilltreeNodeUI>()
                .ForEach((Entity e, int entityInQueryIndex, ref SkilltreeButton skilltreeButton, in RefreshSkilltreeNodeUI refreshSkilltreeNodeUI) =>
            {
                PostUpdateCommands.RemoveComponent<RefreshSkilltreeNodeUI>(entityInQueryIndex, e);
                // set to can unlock
                skilltreeButton.skilltreeNodeState = refreshSkilltreeNodeUI.skilltreeNodeState;
                var skilltreeNodeState = skilltreeButton.skilltreeNodeState;
                var baseColor = nodeBaseColor;
                var frameColor = unlockedNodeColor;
                if (skilltreeNodeState == SkillteeNodeLockState.CanUnlock)
                {
                    frameColor = canUnlockNodeColor;
                }
                else if (skilltreeNodeState == SkillteeNodeLockState.Locked)
                {
                    frameColor = lockedNodeColor;
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, e, new MaterialBaseColor(baseColor));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e, new MaterialFrameColor(frameColor));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e, new NavigationVisuals(baseColor, baseColor.MultiplyBrightness(1.4f), frameColor, frameColor.MultiplyBrightness(1.4f)));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}