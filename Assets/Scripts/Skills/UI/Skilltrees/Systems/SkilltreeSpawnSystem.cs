﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Classes;
using Zoxel.Lines;

namespace Zoxel.Skills.UI.Skilltrees
{
    //! Spawns a Skilltree UI.
    /**
    *   - UI Spawn System -
    *   \todo Add Skill Points on footer of UI.
    *   \todo Make lines permanent using LinkLinks. Add LinkLinksDestroySystem.
    */
    [BurstCompile, UpdateInGroup(typeof(SkillUISystemGroup))]
    public partial class SkilltreeSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity skilltreePrefab;
        public static Entity spawnSkilltreePrefab;
        public static IconPrefabs iconPrefabs;
        private EntityQuery processQuery;
        private EntityQuery classesQuery;
        private EntityQuery skilltreesQuery;
        private EntityQuery skilltreeNodesQuery;
        private EntityQuery userSkillsQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnSkilltreeUI),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnSkilltreePrefab = EntityManager.CreateEntity(spawnArchetype);
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DelayEvent>(),
                ComponentType.ReadOnly<SpawnSkilltreeUI>(),
                ComponentType.ReadOnly<CharacterLink>());
            classesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Class>(),
                ComponentType.ReadOnly<SkilltreeLink>());
            skilltreesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Skilltree>());
            skilltreeNodesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<SkilltreeNode>());
            userSkillsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserSkill>());
            RequireForUpdate(processQuery);
        }

        private void InitializePrefabs()
        {
            if (SkilltreeSpawnSystem.skilltreePrefab.Index != 0)
            {
                return;
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var panelStyle = uiDatam.panelStyle.GetStyle();
            var iconSize = uiDatam.GetIconSize(uiScale);
            var padding = uiDatam.GetIconPadding(uiScale); // iconSize / 8f;
            var margins = uiDatam.GetIconMargins(uiScale); // iconSize / 6f;
            var gridUI = new GridUI(new int2(6, 6), padding, margins);
            var skilltreePrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            SkilltreeSpawnSystem.skilltreePrefab = skilltreePrefab;
            EntityManager.RemoveComponent<GridUIDirty>(skilltreePrefab);
            EntityManager.AddComponent<Prefab>(skilltreePrefab);
            EntityManager.AddComponent<SkilltreeUI>(skilltreePrefab);
            EntityManager.SetComponentData(skilltreePrefab, new PanelUI(PanelType.SkilltreeUI));
            EntityManager.AddComponent<MeshUIDirty>(skilltreePrefab);
            EntityManager.SetComponentData(skilltreePrefab, gridUI);
            EntityManager.SetComponentData(skilltreePrefab, new Size2D(gridUI.GetSize(iconSize)));
            iconPrefabs.frame = EntityManager.Instantiate(UICoreSystem.iconPrefabs.frame);
            EntityManager.RemoveComponent<InitializeButtonColors>(iconPrefabs.frame);
            EntityManager.AddComponent<Prefab>(iconPrefabs.frame);
            EntityManager.AddComponent<SkilltreeButton>(iconPrefabs.frame);
            EntityManager.AddComponent<SetGridPosition>(iconPrefabs.frame);
            EntityManager.AddComponent<RefreshSkilltreeNodeUI>(iconPrefabs.frame);
            iconPrefabs.icon = UICoreSystem.iconPrefabs.icon;
            iconPrefabs.label = UICoreSystem.iconPrefabs.label;
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var uiDatam = UIManager.instance.uiDatam;
            var iconStyle = uiDatam.iconStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var skilltreePrefab = SkilltreeSpawnSystem.skilltreePrefab;
            var iconPrefabs = SkilltreeSpawnSystem.iconPrefabs;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var spawnEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var characterLinks = GetComponentLookup<CharacterLink>(true);
            var prefabLinks = GetComponentLookup<PrefabLink>(true);
            var uiAnchors = GetComponentLookup<UIAnchor>(true);
            var classEntities = classesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var skilltreeLinks = GetComponentLookup<SkilltreeLink>(true);
            classEntities.Dispose();
            var skilltreeEntities = skilltreesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var skilltrees = GetComponentLookup<Skilltree>(true);
            skilltreeEntities.Dispose();
            var skilltreeNodeEntities = skilltreesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
			var skilltreeNodes = GetComponentLookup<SkilltreeNode>(true);
            skilltreeNodeEntities.Dispose();
            var userSkillEntities = userSkillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleE);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleE);
			var userSkillMetaDatas = GetComponentLookup<MetaData>(true);
            userSkillEntities.Dispose();
            Dependency = Entities
                .WithAll<UILink>()
                .ForEach((Entity e, int entityInQueryIndex, in UserSkillLinks userSkillLinks, in ClassLink classLink, in CameraLink cameraLink) =>
            {
                // Does spawn a invenntory ui?
                var spawnEntity = new Entity();
                for (int i = 0; i < spawnEntities.Length; i++)
                {
                    var e2 = spawnEntities[i];
                    var characterLink = characterLinks[e2];
                    if (characterLink.character == e)
                    {
                        spawnEntity = e2;
                        break;  // only one UI per character atm
                    }
                }
                if (spawnEntity.Index == 0)
                {
                    return;
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));
                var prefab = skilltreePrefab;
                if (prefabLinks.HasComponent(spawnEntity))
                {
                    prefab = prefabLinks[spawnEntity].prefab;
                }
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CharacterLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLink);
                if (uiAnchors.HasComponent(spawnEntity))
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, uiAnchors[spawnEntity]);
                }
                var skilltreeEntity = skilltreeLinks[classLink.classs].skilltree;
                var skilltree = skilltrees[skilltreeEntity];
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawned((byte) skilltree.nodes.Length));
                for (int i = 0; i < skilltree.nodes.Length; i++)
                {
                    var seed = (int) (128 + elapsedTime + i * 2048 + 4196 * entityInQueryIndex);
                    var frameEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.frame, e3);
                    UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, frameEntity, e3);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, frameEntity, i);
                    UICoreSystem.SetSound(PostUpdateCommands, entityInQueryIndex, frameEntity, seed, iconStyle.soundVolume);
                    UICoreSystem.SetSeed(PostUpdateCommands, entityInQueryIndex, frameEntity, seed);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, frameEntity, new Seed(seed));
                    UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.icon, frameEntity);
                    UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.label, frameEntity);
                    // var dataEntity = userSkillLinks.skills[i];
                    var nodeEntity = skilltree.nodes[i];
                    var skilltreeNode = skilltreeNodes[nodeEntity];
                    var dataEntity = skilltreeNode.skill; // skillLinks.GetSkill(EntityManager, skilltreeNode.skill);
                    if (dataEntity.Index != 0)
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, frameEntity, new SetIconData(dataEntity));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, frameEntity, new SetGridPosition(skilltreeNode.position));
                        // Find out if skill is locked
                        var skilltreeNodeState = SkillteeNodeLockState.CanUnlock;
                        // check if has unlocked any surrounding skilltreeNode userSkillLinks
                        if (HasSkill(in userSkillLinks, dataEntity, in userSkillMetaDatas))
                        {
                            skilltreeNodeState = SkillteeNodeLockState.Unlocked;
                        }
                        else
                        {
                            // check surrounding skill nodes if they are unlocked
                            for (int j = 0; j < skilltree.nodes.Length; j++)
                            {
                                if (i != j)
                                {
                                    //var otherNode = skilltree.nodes[j];
                                    var otherNode = skilltreeNodes[skilltree.nodes[j]];
                                    for (int a = 0; a < otherNode.nodes.Length; a++)
                                    {
                                        var thirdSkillNodeEntity = otherNode.nodes[a];
                                        var thirdNodeSkill = skilltreeNodes[thirdSkillNodeEntity].skill;
                                        if (thirdNodeSkill == skilltreeNode.skill)
                                        {
                                            // this skilltreeNode is a requirement for current skilltreeNode
                                            // does user have this pre requirement
                                            if (!HasSkill(in userSkillLinks, otherNode.skill, in userSkillMetaDatas))
                                            {
                                                skilltreeNodeState = SkillteeNodeLockState.Locked;
                                                j = skilltree.nodes.Length;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        PostUpdateCommands.SetComponent(entityInQueryIndex, frameEntity, new SkilltreeButton(skilltreeEntity, nodeEntity, skilltreeNodeState));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, frameEntity, new RefreshSkilltreeNodeUI(skilltreeNodeState));
                    }
                    //UnityEngine.Debug.LogError("skillNode " + j + ": " + skilltreeNode.position);
                }
                // UnityEngine.Debug.LogError("Spawned " + childIndex + " skill skilltreeNode uis.");
            })  .WithReadOnly(spawnEntities)
                .WithDisposeOnCompletion(spawnEntities)
                .WithReadOnly(characterLinks)
                .WithReadOnly(prefabLinks)
                .WithReadOnly(uiAnchors)
                .WithReadOnly(skilltreeLinks)
                .WithReadOnly(skilltrees)
                .WithReadOnly(skilltreeNodes)
                .WithReadOnly(userSkillMetaDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static bool HasSkill(in UserSkillLinks userSkillLinks, Entity skillEntity, in ComponentLookup<MetaData> userSkillMetaDatas)
        {
            for (int i = 0; i < userSkillLinks.skills.Length; i++)
            {
                var userSkillEntity = userSkillLinks.skills[i];
                if (!userSkillMetaDatas.HasComponent(userSkillEntity))
                {
                    continue;
                }
                var userSkillMetaDataEntity = userSkillMetaDatas[userSkillEntity].data;
                if (userSkillMetaDataEntity == skillEntity)
                {
                    return true;
                }
            }
            return false;
        }
    }
}