using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Lines;
using Zoxel.Stats;

namespace Zoxel.Skills.UI.Skilltrees
{
    //! Handles events for Selecting a Skilltree Button.
    /**
    *   - UISelectEvent System -
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(SkillUISystemGroup))]
    public partial class SkilltreeSelectSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((in SkilltreeButton skilltreeButton, in IconData iconData, in PanelLink panelLink, in UISelectEvent uiSelectEvent) =>
            {
                var controllerEntity = uiSelectEvent.character;
                var skillEntity = iconData.data;
                if (skillEntity.Index <= 0)
                {
                    UnityEngine.Debug.LogError("Issue with skilltree: " + skillEntity.Index);
                    return;
                }
                var selectedSkillName = EntityManager.GetComponentData<ZoxName>(skillEntity).name;
                var selectedSkillType = EntityManager.GetComponentData<Skill>(skillEntity).type;
                var tooltipText = "";
                if (skilltreeButton.skilltreeNodeState == SkillteeNodeLockState.Unlocked)
                {
                    tooltipText = selectedSkillName.ToString() + " Lvl 0\n";
                }
                else if (skilltreeButton.skilltreeNodeState == SkillteeNodeLockState.CanUnlock)
                {
                    tooltipText = selectedSkillName.ToString() + "\n";
                }
                else if (skilltreeButton.skilltreeNodeState == SkillteeNodeLockState.Locked)
                {
                    tooltipText = "Locked\n";
                    tooltipText += selectedSkillName.ToString() + "\n";
                }
                // show type if not locked
                if (skilltreeButton.skilltreeNodeState != SkillteeNodeLockState.Locked)
                {
                    tooltipText += "Type: ";
                    if (selectedSkillType == SkillType.PhysicalAttack)
                    {
                        tooltipText += "Melee";
                    }
                    else if (selectedSkillType == SkillType.SummonMinion)
                    {
                        tooltipText += "Summon Minion";
                    }
                    else if (selectedSkillType == SkillType.SummonTurret)
                    {
                        tooltipText += "Summon Turret";
                    }
                    else if (selectedSkillType == SkillType.Projectile)
                    {
                        tooltipText += "Magic";
                    }
                    else if (selectedSkillType == SkillType.Augmentation)
                    {
                        tooltipText += "Augmentation";
                    }
                    else if (selectedSkillType == SkillType.Shield)
                    {
                        tooltipText += "Shield";
                    }
                    else
                    {
                        tooltipText += selectedSkillType;
                    }
                }
                if (skilltreeButton.skilltreeNodeState == SkillteeNodeLockState.CanUnlock)
                {
                    tooltipText += "\nUnlock for 1sp";
                }
                if (skilltreeButton.skilltreeNodeState == SkillteeNodeLockState.Unlocked)
                {
                    var skillCost = EntityManager.GetComponentData<SkillCost>(skillEntity);
                    var skillCooldown = EntityManager.GetComponentData<SkillCooldown>(skillEntity);
                    if (skillCost.stat.Index != 0)
                    {
                        var name = EntityManager.GetComponentData<ZoxName>(skillCost.stat).name.ToString();
                        tooltipText += "\nCost " + skillCost.cost + " " + name;
                    }
                    if (skillCooldown.cooldown != 0)
                    {
                        tooltipText += "\nCooldown: " + skillCooldown.cooldown + "s";
                    }
                    else
                    {
                        tooltipText += "\nNo Cooldown";
                    }
                    //tooltipText += "\nDamage: " + selected.attackDamage + " per " + selected.attackSpeed + "s";
                    /*if (selected.attackDamage != 0)
                    {
                        tooltipText += "\nDamage: " + selected.attackDamage + " per " + (selected.attackSpeed + selected.cooldown) + "s";
                    }*/
                }
                var panelEntity = panelLink.panel;
                if (HasComponent<PanelTooltipLink>(panelEntity))
                {
                    var tooltip = EntityManager.GetComponentData<PanelTooltipLink>(panelEntity).tooltip;
                    if (HasComponent<RenderText>(tooltip))
                    {
                        PostUpdateCommands.AddComponent(tooltip, new SetRenderText(new Text(tooltipText)));
                    }
                }
                var playerTooltipLinks = EntityManager.GetComponentData<PlayerTooltipLinks>(controllerEntity);
                if (skilltreeButton.skilltreeNodeState == SkillteeNodeLockState.CanUnlock)
                {
                    playerTooltipLinks.SetTooltipText(PostUpdateCommands, controllerEntity, "[action1] to Purchase Skill");
                }
                else
                {
                    playerTooltipLinks.SetTooltipText(PostUpdateCommands, controllerEntity, "");
                }
            }).WithoutBurst().Run();
        }
    }
}