using Unity.Entities;
using Unity.Burst;
using Zoxel.Skills.Authoring;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Lines;
using Zoxel.Stats;

namespace Zoxel.Skills.UI.Skilltrees
{
    //! Handles events for Clicking a Skilltree Button.
    /**
    *   - UIClickEvent System -
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(SkillUISystemGroup))]
    public partial class SkilltreeClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<SkillsSettings>();
        }

        protected override void OnUpdate()
        {
            var skillsSettings = GetSingleton<SkillsSettings>();
            var isUnlimitedSkillPoints = skillsSettings.isUnlimitedSkillPoints;
            var updateStatPrefab = StatsSystemGroup.updateStatPrefab;
            // var skillPointID = StatsManager.instance.StatSettings.skillPoint.Value.id;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<UIClickEvent, SkilltreeButton>()
                .ForEach((Entity e, in SkilltreeButton skilltreeButton, in IconData iconData, in ParentLink parentLink, in UIClickEvent uiClickEvent) =>
            {
                var controllerEntity = uiClickEvent.controller;
                var characterEntity = EntityManager.GetComponentData<CharacterLink>(controllerEntity).character;
                if (!HasComponent<UserStatLinks>(characterEntity))
                {
                    UnityEngine.Debug.LogError("Character " + characterEntity.Index + " does not have UserSkillLinks component.");
                    return;
                }
                var buttonType = uiClickEvent.buttonType;
                var skillEntity = iconData.data;
                // should be like - hold A
                if (buttonType != ButtonActionType.Confirm)
                {
                    return;
                }
                if (skilltreeButton.skilltreeNodeState != SkillteeNodeLockState.CanUnlock)
                {
                    // play failure sound
                    return;
                }
                var userStatLinks = EntityManager.GetComponentData<UserStatLinks>(characterEntity);
                var statPointUserStatEntity = new Entity();
                for (int i = 0; i < userStatLinks.stats.Length; i++)
                {
                    var statEntity = userStatLinks.stats[i];
                    if (HasComponent<SkillPoint>(statEntity))
                    {
                        statPointUserStatEntity = statEntity;
                        break;
                    }
                }
                if (statPointUserStatEntity.Index == 0)
                {
                    return;
                }
                if (!isUnlimitedSkillPoints)
                {
                    var skillPoint = EntityManager.GetComponentData<StatValue>(statPointUserStatEntity); // baseStats[statPointUserStatEntity];
                    if (skillPoint.value == 0)
                    {
                        return;
                    }
                    // decrease skill points
                    PostUpdateCommands.SetComponent(PostUpdateCommands.Instantiate(updateStatPrefab), new UpdateEntityStat(characterEntity, statPointUserStatEntity, -1));
                }
                // check if node is activated in tree
                // check if has skill points to spend on new skill
                // give skill
                // if does not have skill, give skill
                PostUpdateCommands.AddComponent(characterEntity, new GiveSkill(skillEntity));
                PostUpdateCommands.AddComponent(e, new RefreshSkilltreeNodeUI(SkillteeNodeLockState.Unlocked));
                // refresh next skill frame
                // for all forward input nodes
                var childrens = EntityManager.GetComponentData<Childrens>(parentLink.parent);
                var skilltreeNode = EntityManager.GetComponentData<SkilltreeNode>(skilltreeButton.skilltreeNode);
                for (int i = 0; i < skilltreeNode.nodes.Length; i++)
                {
                    var skilltreeNodeEntity = skilltreeNode.nodes[i];
                    // find the frame that corresponds to the node
                    for (int j = 0; j < childrens.children.Length; j++)
                    {
                        var frameEntity = childrens.children[j];
                        if (frameEntity == e)
                        {
                            continue;
                        }
                        var skilltreeButton2 = EntityManager.GetComponentData<SkilltreeButton>(frameEntity);
                        if (skilltreeNodeEntity == skilltreeButton2.skilltreeNode)
                        {
                            //! \todo Check if all input nodes are unlocked, before making CanUnlock state.
                            //UnityEngine.Debug.LogError("Found forward node at: " + j + " from i: " + i
                            //    + " node: " + skilltreeButton.skilltreeNode.Index + " to " + skilltreeNodeEntity.Index);
                            PostUpdateCommands.AddComponent(frameEntity, new RefreshSkilltreeNodeUI(SkillteeNodeLockState.CanUnlock));
                            break;
                        }
                    }
                }
            }).WithoutBurst().Run();
        }
    }
}
                    // var unlockedSkillNode = EntityManager.GetComponentData<SkilltreeNode>(skilltreeNodeEntity).skill;
                    /*for (int i = 0; i < childrens.children.Length; i++)
                    {
                        var frameEntity = childrens.children[i];
                        if (frameEntity == e)
                        {
                            continue;
                        }
                        var skilltreeNodeEntity = skilltree.nodes[i];
                        var skilltreeNode = EntityManager.GetComponentData<SkilltreeNode>(skilltreeNodeEntity);
                        // if (skillEntity == skilltreeNode.skill)
                        if (skilltreeNode.nodes.Length > 0 && skilltreeButton.skilltreeNode == skilltreeNode.nodes[0])
                        {
                            PostUpdateCommands.AddComponent(frameEntity, new RefreshSkilltreeNodeUI(SkillteeNodeLockState.CanUnlock));
                        }
                    }*/
                    /*var skilltree = EntityManager.GetComponentData<Skilltree>(skilltreeButton.skilltree);
                    if (childrens.children.Length != skilltree.nodes.Length)
                    {
                        // UnityEngine.Debug.LogError("Childrens not equal to nodes length: " + skilltree.nodes.Length);
                        return;
                    }*/
                    //PostUpdateCommands.AddComponent(childrens.children[arrayIndex]);
                        // unlocks these nodes
                        // find any indexes for those userSkillLinks in the tree
                        /*for (int j = 0; j < skilltree.nodes.Length; j++)
                        {
                            var frameEntity = childrens.children[j];
                            if (frameEntity == e)
                            {
                                continue;
                            }
                            var secondSkill = EntityManager.GetComponentData<SkilltreeNode>(skilltreeNode.nodes[j]).skill;
                            if (unlockedSkill == secondSkill)
                            {
                                PostUpdateCommands.AddComponent(frameEntity, new RefreshSkilltreeNodeUI(SkillteeNodeLockState.CanUnlock));
                            }
                        }*/


                    /*if (baseStatsCount == 0)
                    {
                        skillPointEntity = statEntity;
                    }
                    else if (baseStatsCount == 1)
                    {
                        statPointEntity = statEntity;
                    }*/
                        /*var updateEntityStat = new UpdateEntityStat { character = characterEntity };
                        updateEntityStat.SetValues(
                            new StatTypeEditor[] { StatTypeEditor.Base },
                            new int[] { skillPointIndex },
                            new int[] { -1 });
                        var e2 = PostUpdateCommands.CreateEntity();
                        PostUpdateCommands.SetComponent(e2, updateEntityStat);*/

/*StatUpdateSystem.UpdateStats(PostUpdateCommands, characterEntity,
    new StatTypeEditor[] { StatTypeEditor.Base }, 
    new int[] { skillPointIndex },
    new int[] { -1  });*/
    

                    /*int skillPointIndex = -1;
                    for (int i = 0; i < userStatLinks.stats.Length; i++)
                    {
                        if (userStatLinks.stats[i].stat == skillPointEntity)
                        {
                            skillPointIndex = i;
                            break;
                        }
                    }
                    if (skillPointIndex == -1)
                    {
                        return;
                    }*/