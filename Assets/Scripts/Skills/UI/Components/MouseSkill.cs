using Unity.Entities;

namespace Zoxel.Skills.UI
{
    //! Holds UserSkill and Skill entity references inside a MouseUI.
    public struct MouseSkill : IComponentData
    {
        public Entity userSkill;
        public Entity skill;

        public MouseSkill(Entity userSkill, Entity skill)
        {
            this.userSkill = userSkill;
            this.skill = skill;
        }
    }
}