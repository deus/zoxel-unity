/*using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Transforms;
using Zoxel.Textures;
using Zoxel.Audio;
using Zoxel.UI;
using Zoxel.Lines;

namespace Zoxel.Skills.UI
{
    //! Summons a SpellTree UI.
    [BurstCompile, UpdateInGroup(typeof(SkillUISystemGroup))]
    public partial class SkillTreeNodeLineSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var linePrefab = RenderLineGroup.linePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<NodeLine>()
                .ForEach((Entity e, in Translation translation, in NodeLine nodeLine) =>
            {
                for (int i = 0; i < nodeLine.nextNodes.Length; i++)
                {
                    var translation2 = EntityManager.GetComponentData<Translation>(nodeLine.nextNodes[i]);
                    var direction = 0.004f * math.normalize(translation.Value - translation2.Value);
                    RenderLineGroup.SpawnLine(PostUpdateCommands, linePrefab, elapsedTime, translation.Value - direction, translation2.Value + direction,
                        /, 0.0015f, 0.1f);
                }
            }).WithoutBurst().Run();
        }
    }
}*/
    /**
    *   \todo Fix as lines arn't working.
    *   \todo Make permanent line entities instead.
    *   \todo Convert to Parallel.
    */