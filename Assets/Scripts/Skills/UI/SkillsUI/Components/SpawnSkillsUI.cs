using Unity.Entities;

namespace Zoxel.Skills.UI
{
    //! A generic event for spawning a SkillsUI Entity.
    public struct SpawnSkillsUI : IComponentData { }
}