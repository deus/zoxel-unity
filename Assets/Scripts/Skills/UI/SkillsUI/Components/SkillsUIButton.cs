using Unity.Entities;

namespace Zoxel.Skills.UI
{
    //! A tag for a SkillsUI Button Entity.
    public struct SkillsUIButton : IComponentData { }
}