using Unity.Entities;

namespace Zoxel.Skills.UI
{
    //! A tag for a SkillsUI entity.
    public struct SkillsUI : IComponentData { }
}