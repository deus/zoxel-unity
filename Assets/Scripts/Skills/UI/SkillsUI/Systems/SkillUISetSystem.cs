using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.UI;
using Zoxel.Textures;

namespace Zoxel.Skills.UI
{
    //! Sets the ItemUI's icon and texture.
    /**
    *   - UI Update System -
    */
    [BurstCompile, UpdateInGroup(typeof(SkillUISystemGroup))]
    public partial class SkillUISetSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userSkillsQuery;
        private EntityQuery skillsQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userSkillsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserSkill>(),
                ComponentType.ReadOnly<MetaData>());
            skillsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Skill>(),
                ComponentType.ReadOnly<ZoxID>());
            RequireForUpdate(processQuery);
            RequireForUpdate(userSkillsQuery);
            RequireForUpdate(userSkillsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userSkillEntities = userSkillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var userSkillMetaDatas = GetComponentLookup<MetaData>(true);
            userSkillEntities.Dispose();
            var skillEntities = skillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var skills = GetComponentLookup<Skill>(true);
			var skillIDs = GetComponentLookup<ZoxID>(true);
            skillEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEventFrames, InitializeEntity>()
                .WithAll<SetIconData>()
                .ForEach((Entity e, int entityInQueryIndex, ref IconData iconData, in SetIconData setIconData, in Childrens childrens) =>
            {
                if (iconData.data == setIconData.data)
                {
                    return;
                }
                if (userSkillMetaDatas.HasComponent(setIconData.data))
                {
                    // UnityEngine.Debug.LogError("Set to new Skill: " + setIconData.data.Index);
                    iconData.data = setIconData.data;
                    var iconEntity = childrens.children[0];
                    var metaEntity = userSkillMetaDatas[iconData.data].data;
                    if (skills.HasComponent(metaEntity))
                    {
                        var skillType = skills[metaEntity].type;
                        var skillID = skillIDs[metaEntity].id;
                        PostUpdateCommands.AddComponent(entityInQueryIndex, iconEntity, new GenerateSkillTexture(skillID, skillType));
                        var textEntity = childrens.children[1];
                        PostUpdateCommands.AddComponent<ClearRenderText>(entityInQueryIndex, textEntity);
                        // UnityEngine.Debug.LogError("Update AcctionUI Skill (2): " + skillID);
                    }
                }
                else if (skills.HasComponent(setIconData.data))
                {
                    // UnityEngine.Debug.LogError("Set to new Skill: " + setIconData.data.Index);
                    iconData.data = setIconData.data;
                    var iconEntity = childrens.children[0];
                    var metaEntity = iconData.data;
                    if (skills.HasComponent(metaEntity))
                    {
                        var skillType = skills[metaEntity].type;
                        var skillID = skillIDs[metaEntity].id;
                        PostUpdateCommands.AddComponent(entityInQueryIndex, iconEntity, new GenerateSkillTexture(skillID, skillType));
                        var textEntity = childrens.children[1];
                        PostUpdateCommands.AddComponent<ClearRenderText>(entityInQueryIndex, textEntity);
                    }
                }
            })  .WithReadOnly(userSkillMetaDatas)
                .WithReadOnly(skills)
                .WithReadOnly(skillIDs)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}