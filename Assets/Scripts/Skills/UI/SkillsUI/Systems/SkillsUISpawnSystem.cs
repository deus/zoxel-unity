﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

using Zoxel.UI;
using Zoxel.Textures;
using Zoxel.Transforms;
// Show different tooltip if Unlocked (grayed out as well)
// Remove Grid and position like a tree
// Trees to be based on class the character has - generate the class tree structure in game data

namespace Zoxel.Skills.UI
{
    //! Summons a SpellBook UI.
    /**
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(SkillUISystemGroup))]
    public partial class SkillsUISpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity spellbookPrefab;
        public static IconPrefabs iconPrefabs;
        public static Entity spawnSkillbookPrefab;
        private EntityQuery processQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnSkillsUI),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnSkillbookPrefab = EntityManager.CreateEntity(spawnArchetype);
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DelayEvent>(),
                ComponentType.ReadOnly<SpawnSkillsUI>(),
                ComponentType.ReadOnly<CharacterLink>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        private void InitializePrefabs()
        {
            if (SkillsUISpawnSystem.spellbookPrefab.Index == 0)
            {
                var spellbookPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
                EntityManager.AddComponent<Prefab>(spellbookPrefab);
                EntityManager.AddComponent<SkillsUI>(spellbookPrefab);
                EntityManager.SetComponentData(spellbookPrefab, new PanelUI(PanelType.SkillsUI));
                SkillsUISpawnSystem.spellbookPrefab = spellbookPrefab;
            }
            if (iconPrefabs.frame.Index == 0)
            {
                iconPrefabs.frame = EntityManager.Instantiate(UICoreSystem.iconPrefabs.frame);
                EntityManager.AddComponent<Prefab>(iconPrefabs.frame);
                EntityManager.AddComponent<SkillsUIButton>(iconPrefabs.frame);
                EntityManager.AddComponent<SetIconData>(iconPrefabs.frame);
                EntityManager.SetComponentData(iconPrefabs.frame, new OnChildrenSpawned(2));
                iconPrefabs.icon = UICoreSystem.iconPrefabs.icon;
                iconPrefabs.label = UICoreSystem.iconPrefabs.label;
            }
        }

        protected override void OnUpdate()
        {
            InitializePrefabs();
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var uiDatam = UIManager.instance.uiDatam;
            var iconStyle = uiDatam.iconStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var spellbookPrefab = SkillsUISpawnSystem.spellbookPrefab;
            var iconPrefabs = SkillsUISpawnSystem.iconPrefabs;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var spawnEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var characterLinks = GetComponentLookup<CharacterLink>(true);
            var prefabLinks = GetComponentLookup<PrefabLink>(true);
            var uiAnchors = GetComponentLookup<UIAnchor>(true);
            Dependency = Entities
                .WithAll<UILink>()
                .ForEach((Entity e, int entityInQueryIndex, in UserSkillLinks userSkillLinks, in CameraLink cameraLink) =>
            {
                // Does spawn a invenntory ui?
                var spawnEntity = new Entity();
                for (int i = 0; i < spawnEntities.Length; i++)
                {
                    var e2 = spawnEntities[i];
                    var characterLink = characterLinks[e2];
                    if (characterLink.character == e)
                    {
                        spawnEntity = e2;
                        break;  // only one UI per character atm
                    }
                }
                if (spawnEntity.Index == 0)
                {
                    return;
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));
                var prefab = spellbookPrefab;
                if (prefabLinks.HasComponent(spawnEntity))
                {
                    prefab = prefabLinks[spawnEntity].prefab;
                }
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CharacterLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLink);
                if (uiAnchors.HasComponent(spawnEntity))
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, uiAnchors[spawnEntity]);
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawned((byte) userSkillLinks.skills.Length));
                for (int i = 0; i < userSkillLinks.skills.Length; i++)
                {
                    var seed = (int) (128 + elapsedTime + i * 2048 + 4196 * entityInQueryIndex);
                    var frameEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.frame, e3);
                    UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, frameEntity, e3);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, frameEntity, i);
                    UICoreSystem.SetSound(PostUpdateCommands, entityInQueryIndex, frameEntity, seed, iconStyle.soundVolume);
                    UICoreSystem.SetSeed(PostUpdateCommands, entityInQueryIndex, frameEntity, seed);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, frameEntity, new Seed(seed));
                    UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.icon, frameEntity);
                    UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.label, frameEntity);
                    var dataEntity = userSkillLinks.skills[i];
                    if (dataEntity.Index != 0)
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, frameEntity, new SetIconData(dataEntity));
                    }
                }
            })  .WithReadOnly(spawnEntities)
                .WithDisposeOnCompletion(spawnEntities)
                .WithReadOnly(characterLinks)
                .WithReadOnly(prefabLinks)
                .WithReadOnly(uiAnchors)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}