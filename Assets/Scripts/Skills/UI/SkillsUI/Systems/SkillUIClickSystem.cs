using Unity.Entities;
using Zoxel.UI;
using Zoxel.Textures;
using Zoxel.Transforms;
using Zoxel.UI.MouseFollowing;

namespace Zoxel.Skills.UI
{
    //! Handles events for Clicking a SkillBook Button.
    /**
    *   - UIClickEvent System -
    *   \todo Convert to Parallel.
    *   Show different tooltip if Unlocked (grayed out as well)
    *   Remove Grid and position like a tree
    *   Trees to be based on class the character has - generate the class tree structure in game data
    */
    [UpdateInGroup(typeof(SkillUISystemGroup))]
    public partial class SkillUIClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<UIClickEvent, SkillsUIButton>()
                .ForEach((Entity e, in IconData iconData, in ParentLink parentLink, in UIClickEvent uiClickEvent) =>
            {
                var controllerEntity = uiClickEvent.controller;
                var characterEntity = EntityManager.GetComponentData<CharacterLink>(controllerEntity).character;
                var panelUI = parentLink.parent;
                var userSkillEntity = iconData.data;
                if (!HasComponent<UserSkill>(userSkillEntity))
                {
                    return;
                }
                if (uiClickEvent.buttonType != ButtonActionType.Confirm)
                {
                    return;
                }
                var cameraEntity = EntityManager.GetComponentData<CameraLink>(controllerEntity).camera;
                var skillEntity = EntityManager.GetComponentData<MetaData>(userSkillEntity).data;
                if (skillEntity.Index <= 0)
                {
                    return;
                }
                var mouseUIEntity = EntityManager.GetComponentData<MouseUILink>(controllerEntity).mouseUI;
                var mouseUI = EntityManager.GetComponentData<MouseUI>(mouseUIEntity);
                mouseUI.panelUI = panelUI;
                mouseUI.userEntity = userSkillEntity;
                PostUpdateCommands.SetComponent(mouseUIEntity, mouseUI);
                if (uiClickEvent.deviceType == DeviceType.Gamepad)
                {
                    var actionbar = EntityManager.GetComponentData<UILink>(characterEntity).GetUI<Actionbar>(EntityManager);
                    if (actionbar.Index != 0)
                    {
                        PostUpdateCommands.AddComponent<NavigationDirty>(actionbar);
                    }
                }
                // Clear Action Label
                if (HasComponent<Childrens>(mouseUIEntity))
                {
                    var frameChildrens = EntityManager.GetComponentData<Childrens>(mouseUIEntity);
                    // set icon texture
                    SetIconTexture(PostUpdateCommands, frameChildrens.children[0], characterEntity, skillEntity);
                    // Clear Text
                    PostUpdateCommands.AddComponent<ClearRenderText>(frameChildrens.children[1]);
                }
                PostUpdateCommands.AddComponent(mouseUIEntity, new MouseSkill(userSkillEntity, skillEntity));
                PostUpdateCommands.AddComponent(mouseUIEntity, new UserSkill());
                PostUpdateCommands.SetComponent(mouseUIEntity, new CharacterLink(controllerEntity));
                PostUpdateCommands.SetComponent(mouseUIEntity, new MouseUI(characterEntity, panelUI, e, userSkillEntity));
                var uiDepth = EntityManager.GetComponentData<UIPosition>(panelUI).GetDepth();
                PostUpdateCommands.AddComponent(mouseUIEntity, new FollowingMouse(cameraEntity, uiDepth));
            }).WithoutBurst().Run();
        }
        
        private void SetIconTexture(EntityCommandBuffer PostUpdateCommands, Entity iconEntity, Entity character, Entity skillEntity)
        {
            var skillType = EntityManager.GetComponentData<Skill>(skillEntity).type;
            var skillID = EntityManager.GetComponentData<ZoxID>(skillEntity).id;
            if (!HasComponent<Texture>(iconEntity))
            {
                PostUpdateCommands.AddComponent<Texture>(iconEntity);
            }
            PostUpdateCommands.AddComponent(iconEntity, new GenerateSkillTexture(skillID, skillType));
        }
    }
}