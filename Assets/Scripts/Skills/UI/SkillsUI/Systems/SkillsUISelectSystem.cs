using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Textures;
using Zoxel.Stats;
using Zoxel.UI;
using Zoxel.Transforms;
// Show different tooltip if Unlocked (grayed out as well)
// Remove Grid and position like a tree
// Trees to be based on class the character has - generate the class tree structure in game data

namespace Zoxel.Skills.UI
{
    //!  Handles events for Selecting a SkillBook Button.
    /**
    *   - UISelectEvent System -
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(SkillUISystemGroup))]
    public partial class SkillsUISelectSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((Entity e, in SkillsUIButton skillbookButton, in Child child, in PanelLink panelLink, in UISelectEvent uiSelectEvent) =>
            {
                var controllerEntity = uiSelectEvent.character;
                var characterEntity = EntityManager.GetComponentData<CharacterLink>(controllerEntity).character;
                var arrayIndex = child.index;
                var userSkillLinks = EntityManager.GetComponentData<UserSkillLinks>(characterEntity);
                var userSkillEntity = userSkillLinks.skills[arrayIndex];
                var userSkill = EntityManager.GetComponentData<MetaData>(userSkillEntity);
                var skillEntity = userSkill.data;
                if (skillEntity.Index <= 0)
                {
                    return;
                }
                var skillName = EntityManager.GetComponentData<ZoxName>(skillEntity).name;
                var skillType = EntityManager.GetComponentData<Skill>(skillEntity).type;
                var skillCost = EntityManager.GetComponentData<SkillCost>(skillEntity);
                var skillCooldown = EntityManager.GetComponentData<SkillCooldown>(skillEntity);
                var tooltipText = "";
                // if (selected.id != 0) // && meta.ContainsKey(selected.id))
                {
                    tooltipText = skillName.ToString() + "\nLvl 0: 0%"; // meta[selected.id].name;
                }
                tooltipText += "\n";
                if (skillType == SkillType.PhysicalAttack)
                {
                    tooltipText += "Melee";
                }
                else if (skillType == SkillType.SummonMinion)
                {
                    tooltipText += "Summon Minion";
                }
                else if (skillType == SkillType.SummonTurret)
                {
                    tooltipText += "Summon Turret";
                }
                else if (skillType == SkillType.Projectile)
                {
                    tooltipText += "Magic";
                }
                else if (skillType == SkillType.Augmentation)
                {
                    tooltipText += "Augmentation";
                }
                else
                {
                    tooltipText += skillType;
                }
                if (skillCost.cost != 0)
                {
                    if (skillCost.stat.Index == 0)
                    {
                        UnityEngine.Debug.LogError("Skill cost stat is null.");
                    }
                    else
                    {
                        tooltipText += "\nCost " + skillCost.cost + " " + EntityManager.GetComponentData<ZoxName>(skillCost.stat).name;
                    }
                }
                if (skillCooldown.cooldown != 0)
                {
                    tooltipText += "\nCooldown: " + skillCooldown.cooldown + "s";
                }
                else
                {
                    tooltipText += "\nNo Cooldown";
                }
                /*if (selected.attackDamage != 0)
                {
                    tooltipText += "\nDamage: " + selected.attackDamage + " per " + (selected.attackSpeed + selected.cooldown) + "s";
                }*/
                /*var panelEntity = panelLink.panel;
                if (HasComponent<PanelTooltipLink>(panelEntity))
                {
                    var tooltip = EntityManager.GetComponentData<PanelTooltipLink>(panelEntity).tooltip;
                    if (HasComponent<RenderText>(tooltip))
                    {
                        PostUpdateCommands.AddComponent(tooltip, new SetRenderText(new Text(tooltipText)));
                    }
                }*/
                PostUpdateCommands.AddComponent(panelLink.panel, new SetPanelTooltip(new Text(tooltipText)));
                // var playerTooltipLinks = EntityManager.GetComponentData<PlayerTooltipLinks>(controllerEntity);
                // playerTooltipLinks.SetTooltipText(PostUpdateCommands, controllerEntity, "[action1] to Assign Skill");
                PostUpdateCommands.AddComponent(controllerEntity, new SetPlayerTooltip(new Text("[action1] to Assign Skill")));
            }).WithoutBurst().Run();   
        }
    }
}