using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.UI;
using Zoxel.UI.MouseFollowing;

namespace Zoxel.Skills.UI
{
    //! Hides the MouseSkill entity.
    /**
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(SkillUISystemGroup))]
    public partial class MouseSkillCloseSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            //! When closing MouseSkill Mouse UIs!
            Entities
                .WithNone<DelayEventFrames>()
                .WithAll<HideMouseUI, MouseSkill>()
                .ForEach((Entity e, in CharacterLink characterLink, in MouseUI mouseUI) =>
            {
                DestroyItemUI(PostUpdateCommands, e);
                PostUpdateCommands.RemoveComponent<MouseSkill>(e);
                PostUpdateCommands.RemoveComponent<FollowingMouse>(e);
                PostUpdateCommands.RemoveComponent<HideMouseUI>(e);
            }).WithoutBurst().Run();
        }

        private void DestroyItemUI(EntityCommandBuffer PostUpdateCommands, Entity frameEntity)
        {
            var childrens = EntityManager.GetComponentData<Childrens>(frameEntity);
            var iconEntity = childrens.children[0];
            Janitor.DestroyTexture(EntityManager, iconEntity);
            var renderer = EntityManager.GetSharedComponentManaged<ZoxMesh>(iconEntity);
            if (renderer.material)
            {
                renderer.material.SetTexture("_BaseMap", TextureUtil.CreateBlankTexture());
                PostUpdateCommands.SetSharedComponentManaged(iconEntity, renderer);
            }
            var textEntity = childrens.children[1];
            if (HasComponent<RenderText>(textEntity))
            {
                var renderText = EntityManager.GetComponentData<RenderText>(textEntity);
                if (renderText.ClearText())
                {
                    PostUpdateCommands.SetComponent(textEntity, renderText);
                    PostUpdateCommands.AddComponent<RenderTextDirty>(textEntity);
                }
            }
        }
    }
}