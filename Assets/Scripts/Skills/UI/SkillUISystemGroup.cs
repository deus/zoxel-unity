﻿using Unity.Entities;

namespace Zoxel.Skills.UI
{
    //! UserSkillLinks! UserActionLinks use userSkillLinks. UserSkillLinks use animations, userStatLinks and particles.
    [UpdateInGroup(typeof(SkillSystemGroup))]
    public partial class SkillUISystemGroup : ComponentSystemGroup { }
}
