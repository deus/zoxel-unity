using Unity.Entities;

namespace Zoxel.Lores.UI
{
    //! A tag for the Lore's UI.
    public struct LoreUI : IComponentData { }
}