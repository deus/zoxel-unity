using Unity.Entities;

namespace Zoxel.Lores
{
    //! A tag for the lore Entity.
    public struct Lore : IComponentData { }
}