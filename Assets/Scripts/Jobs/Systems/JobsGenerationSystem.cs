using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

namespace Zoxel.Jobs
{
    //! Generates Realm Jobs.
    /**
    *   - Data Generation System -
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(JobSystemGroup))]
    public partial class JobsGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity jobPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(Job),
                typeof(ZoxID),
                typeof(ZoxName),
                typeof(ZoxDescription));
            jobPrefab = EntityManager.CreateEntity(archetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(jobPrefab); // EntityManager.AddComponentData(jobPrefab, new EditorName("[job]"));
            #endif
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); //.AsParallelWriter();
            /*Entities.ForEach((Entity e, ref GenerateRealm generateRealm, ref ClassLinks classLinks, in ZoxID zoxID) =>
            {
                if (generateRealm.state != (byte)GenerateRealmState.GenerateJobs)
                {
                    return; // wait for thingo
                }
                var realmSeed = zoxID.id;
                if (realmSeed == 0)
                {
                    return;
                }
                generateRealm.state = (byte) GenerateRealmState.GenerateItems;
                // Destroy UserSkillLinks and SkillTrees
                for (int i = 0; i < classLinks.data.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(classLinks.data[i]);
                }
                classLinks.Dispose();
                // Spawn Skill Entities
                var dataCount = 6;
                classLinks.Initialize(dataCount);
                for (int i = 0; i < dataCount; i++)
                {
                    var e2 = EntityManager.Instantiate(classPrefab);
                    classLinks.data[i] = e2;
                    var dataID = realmSeed + 2000 + i * 1;
                    var random = new Random();
                    random.InitState((uint)dataID);
                    PostUpdateCommands.SetComponent(e2, new ZoxID(dataID));
                    var description = new Text();
                    if (i < 3)
                    {
                        if (i == 0)
                        {
                            PostUpdateCommands.SetComponent(e2, new ZoxName("Warrior"));
                            description.SetText("A strong melee focused fighter.");
                        }
                        else if (i == 1)
                        {
                            PostUpdateCommands.SetComponent(e2, new ZoxName("Mage"));
                            description.SetText("A powerful ranged mana focused fighter.");
                        }
                        else if (i == 2)
                        {
                            PostUpdateCommands.SetComponent(e2, new ZoxName("Rogue"));
                            description.SetText("A silent ambush focused assassin.");
                        }
                    }
                    else
                    {
                        var dataName = NameGenerator.GenerateName2(ref random);
                        PostUpdateCommands.SetComponent(e2, new ZoxName(dataName));
                    }
                    PostUpdateCommands.SetComponent(e2, new ZoxDescription(description));
                    var classData = new Class();
                    // set class Data
                    PostUpdateCommands.SetComponent(e2, classData);
                }
            }).WithStructuralChanges().Run();   // spawns skill tree and skill data
            */
        }
    }
}