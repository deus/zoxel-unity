using Unity.Entities;
// todo: when saving, save ID, link to games data

namespace Zoxel.Jobs
{
    public struct JobLink : IComponentData
    {
        public Entity data;

        public JobLink(Entity data)
        {
            this.data = data;
        }
    }
}