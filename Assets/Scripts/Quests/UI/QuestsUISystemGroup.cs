﻿using Unity.Entities;

namespace Zoxel.Quests.UI
{
    [UpdateInGroup(typeof(QuestSystemGroup))]
    public partial class QuestsUISystemGroup : ComponentSystemGroup { }
}