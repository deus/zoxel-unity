using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.UI;

namespace Zoxel.Quests.UI
{
    //! Questlog click handler.
    /**
    *   - UIClickEvent System -
    */
    [UpdateInGroup(typeof(QuestsUISystemGroup))]
    public partial class QuestlogUIClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<QuestlogButton>()
                .ForEach((Entity e, in Zoxel.Transforms.Child child, in UIClickEvent uiClickEvent) =>
            {
                var controllerEntity = uiClickEvent.controller;
                var characterEntity = EntityManager.GetComponentData<CharacterLink>(controllerEntity).character;
                var questlog = EntityManager.GetComponentData<Questlog>(characterEntity);
                var userQuestEntity = questlog.quests[child.index];
                var questTracking = userQuestEntity;
                // If quest is handed in, and you are trying to track it, stop doing that.
                if (questTracking != userQuestEntity)
                {
                    var userQuest = EntityManager.GetComponentData<UserQuest>(userQuestEntity);
                    if (userQuest.handedIn == 1)
                    {
                        return;
                    }
                }
                // If already tracking, stop tracking it.
                if (questlog.questTracking == userQuestEntity)
                {
                    questTracking = new Entity();
                }
                if (questlog.questTracking != questTracking)
                {
                    questlog.questTracking = questTracking;
                    PostUpdateCommands.SetComponent(characterEntity, questlog);
                    PostUpdateCommands.AddComponent<SaveQuestlog>(characterEntity);
                    PostUpdateCommands.AddComponent(e, new UISelectEvent(controllerEntity));
                }
            }).WithoutBurst().Run();
        }
    }
}