using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Cameras;
using Zoxel.UI;

namespace Zoxel.Quests.UI
{
    //! Spawns a QuestTracker UI!
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(QuestsUISystemGroup))]
    public partial class QuestTrackerSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private Entity questTrackerPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.ReadOnly<SpawnQuestTracker>(),
                ComponentType.ReadOnly<CharacterLink>(),
                ComponentType.Exclude<DelayEvent>());
            var questTrackerArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(QuestTracker),
                typeof(GameOnlyUI),
                typeof(RenderText),
                typeof(RenderTextData),
                typeof(NewCharacterUI),
                typeof(CharacterUI),
                typeof(PanelUI),
                typeof(PanelPosition),
                typeof(UIAnchor),
                typeof(UIPosition),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(OrbitTransform),
                typeof(OrbitPosition),
                typeof(CharacterLink),
                typeof(CameraLink),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld));
            questTrackerPrefab = EntityManager.CreateEntity(questTrackerArchetype);
            EntityManager.SetComponentData(questTrackerPrefab, new NonUniformScale { Value = new float3(1, 1, 1) } );
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            if (this.questTrackerPrefab.Index == 0)
            {
                // expand from panel ui basic - UICoreSystem
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDepth = CameraManager.instance.cameraSettings.uiDepth;
            var uiDatam = UIManager.instance.uiDatam;
            var panelStyle = uiDatam.panelStyle.GetStyle();
            var iconSize = uiDatam.GetIconSize(uiScale);
            var padding = uiDatam.GetIconPadding(uiScale); // new float2(0.0006f, 0.0006f);
            // var genericMargins = uiDatam.GetIconMargins(uiScale); // new float2(0.001f, 0.001f);
            var questTrackerTextColor = new Color(uiDatam.questTrackerTextColor);
            var questTrackerTextOutlineColor = new Color(uiDatam.questTrackerTextOutlineColor);
            var questTrackerGenerationData = uiDatam.questTrackerGenerationData;
            var questTrackerPrefab = this.questTrackerPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var spawnQuestTrackerEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var characterLinks = GetComponentLookup<CharacterLink>(true);
            Dependency = Entities
                .ForEach((Entity e, int entityInQueryIndex, in UILink uiLink, in CameraLink cameraLink) =>
            {
                if (cameraLink.camera.Index == 0)
                {
                    return;
                }
                for (int i = 0; i < spawnQuestTrackerEntities.Length; i++)
                {
                    var e2 = spawnQuestTrackerEntities[i];
                    var characterLink = characterLinks[e2];
                    if (characterLink.character == e)
                    {
                        PostUpdateCommands.DestroyEntity(entityInQueryIndex, e2);   // Destroy spawn event
                        PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));
                        var labelSize = 2f * padding.y + iconSize.x / 2.5f;
                        // var spawnPosition = new float3(0, -iconSize.y / 2f, 0);
                        var positionOffset = new float3(0, - 0.35f * iconSize.y, 0);  // originally 0.5f
                        var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, questTrackerPrefab);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CharacterLink(e));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CameraLink(cameraLink.camera));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new PanelUI(PanelType.QuestTracker));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new PanelPosition(positionOffset));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new UIAnchor(AnchorUIType.TopMiddle));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new UIPosition(uiDepth));
                        UICoreSystem.SetRenderText(PostUpdateCommands, entityInQueryIndex, e3, questTrackerTextColor, questTrackerTextOutlineColor,
                            questTrackerGenerationData, new Text(), labelSize, padding);
                        break;  // only one crosshair per character atm
                    }
                }
            })  .WithReadOnly(spawnQuestTrackerEntities)
                .WithDisposeOnCompletion(spawnQuestTrackerEntities)
                .WithReadOnly(characterLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}