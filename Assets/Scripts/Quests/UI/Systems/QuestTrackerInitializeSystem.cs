using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Races;  // includes RealmLink and RaceLinks and CharacterLink

namespace Zoxel.Quests.UI
{
    //! Dynamically sets the quest tracker ui text.
    /**
    *   \todo Use quest events to update the UI.
    *   \todo Track more than one quest at a time.
    *   \todo Convert To Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(QuestsUISystemGroup))]
    public partial class QuestTrackerInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<RenderTextDirty>()
                .WithAll<QuestTracker>() // InitializeEntity,  // InitializeQuestTrackerUI>()
                .ForEach((Entity e, int entityInQueryIndex, ref RenderText renderText, in CharacterLink characterLink) =>
            {
                var character = characterLink.character;
                if (HasComponent<Questlog>(character))
                {
                    var questlog = EntityManager.GetComponentData<Questlog>(character);
                    var isTrackingQuest = false;
                    var userQuestEntity = questlog.questTracking;
                    if (questlog.quests.Length != 0 && userQuestEntity.Index != 0 && HasComponent<UserQuest>(userQuestEntity))
                    {
                        var userQuest = EntityManager.GetComponentData<UserQuest>(userQuestEntity);
                        if (userQuest.handedIn == 0)
                        {
                            isTrackingQuest = true;
                            var questEntity = EntityManager.GetComponentData<MetaData>(userQuestEntity).data;
                            var quest = EntityManager.GetComponentData<Quest>(questEntity);
                            var tooltipText = ""; // "Slain " + quest.blocks[0].completed + "/" + quest.blocks[0].maxCompleted + " Monster's";
                            var realmEntity = EntityManager.GetComponentData<RealmLink>(character).realm;
                            if (quest.blocks.Length > 0)
                            {
                                var raceEntity = quest.blocks[0].target;
                                // var raceLinks = EntityManager.GetComponentData<RaceLinks>(realmEntity);
                                var raceName = EntityManager.GetComponentData<ZoxName>(raceEntity).name.ToString();
                                if (userQuest.completed[0] == 0)
                                {
                                    tooltipText = "Slay " + quest.blocks[0].maxCompleted + " " + raceName + "'s";
                                }
                                else if (userQuest.completed[0] != quest.blocks[0].maxCompleted)
                                {
                                    tooltipText = "Slain " + userQuest.completed[0] + "/" + quest.blocks[0].maxCompleted + " " + raceName + "'s";
                                }
                                else
                                {
                                    tooltipText = "Hand in Quest to Jerry";
                                }
                            }
                            else
                            {
                                tooltipText = "No visible objectives";
                            }
                            // UnityEngine.Debug.LogError("- Set Quest Tracker Text to: " + tooltipText);
                            if (renderText.SetText(tooltipText))
                            {
                                // UnityEngine.Debug.LogError("Set Quest Tracker Text to: " + tooltipText);
                                PostUpdateCommands2.AddComponent<RenderTextDirty>(e);
                            }
                        }
                    }
                    if (!isTrackingQuest)
                    {
                        if (renderText.SetText(""))
                        {
                            PostUpdateCommands2.AddComponent<RenderTextDirty>(e);
                        }
                    }
                }
                // PostUpdateCommands.RemoveComponent<InitializeQuestTrackerUI>(e);
            }).WithoutBurst().Run(); // ScheduleParallel(Dependency);
            // // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

                            /*if (quest.handedIn == 1)
                            {
                                tooltipText += "\nQuest Handed In";
                            }
                            else
                            {
                                if (quest.rewards.Length == 0)
                                {
                                    tooltipText += "\nNo Rewards";
                                }
                                else
                                {
                                    var itemLinks = EntityManager.GetComponentData<ItemLinks>(realmEntity);
                                    var itemEntity = itemLinks.GetItem(EntityManager, quest.rewards[0].targetID);
                                    var item = EntityManager.GetComponentData<RealmItem>(itemEntity).item;
                                    tooltipText += "\nRewarded " + item.name.ToString() + " x" + quest.rewards[0].quantity;
                                }
                            }*/