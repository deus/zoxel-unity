using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Items;
using Zoxel.Races;

namespace Zoxel.Quests.UI
{
    //! Handles UISelectEvent's for QuestlogButton entities.
    [BurstCompile, UpdateInGroup(typeof(QuestsUISystemGroup))]
    public partial class QuestUISelectSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<QuestlogButton>()
                .ForEach((Entity e, in Child child, in UISelectEvent uiSelectEvent, in PanelLink panelLink) =>
            {
                var controllerEntity = uiSelectEvent.character;
                var characterEntity = EntityManager.GetComponentData<CharacterLink>(controllerEntity).character;
                var panelEntity = panelLink.panel;
                var arrayIndex = child.index;
                var game = EntityManager.GetComponentData<RealmLink>(characterEntity).realm;
                var raceLinks = EntityManager.GetComponentData<RaceLinks>(game);
                var itemLinks = EntityManager.GetComponentData<ItemLinks>(game);
                var questlog = EntityManager.GetComponentData<Questlog>(characterEntity);
                var userQuestEntity = questlog.quests[arrayIndex];
                var userQuest = EntityManager.GetComponentData<UserQuest>(userQuestEntity);
                var questEntity = EntityManager.GetComponentData<MetaData>(userQuestEntity).data;
                var quest = EntityManager.GetComponentData<Quest>(questEntity);
                var questName = EntityManager.GetComponentData<ZoxName>(questEntity);
                var tooltipText = questName.name.ToString();
                if (userQuest.handedIn == 1)
                {
                    tooltipText += "\nQuest Handed In";
                }
                else
                {
                    if (quest.blocks.Length > 0)
                    {
                        var raceEntity = quest.blocks[0].target;
                        var raceName = "monster";
                        if (raceEntity.Index != 0)
                        {
                            raceName = EntityManager.GetComponentData<ZoxName>(raceEntity).name.ToString();
                        }
                        tooltipText += "\nSlain " + userQuest.completed[0] + "/" + quest.blocks[0].maxCompleted + " " + raceName + "'s";
                    }
                    else
                    {
                        tooltipText += "\nNo visible objectives.";
                    }
                    // tooltipText += "\nNo Rewards\n";
                    if (quest.rewards.Length == 0)
                    {
                        tooltipText += "\nNo Rewards";
                    }
                    else
                    {
                        //tooltipText += "\nRewards [" + quest.rewards.Length + "]";
                        tooltipText += "\nRewards " + quest.rewards.Length;
                        for (int i = 0; i < quest.rewards.Length; i++)
                        {
                            var reward = quest.rewards[i];
                            if (reward.type == (byte) QuestRewardType.Item)
                            {
                                var itemEntity = reward.targetEntity; // itemLinks.GetItem(EntityManager, reward.targetEntity);
                                if (itemEntity.Index > 0)
                                {
                                    var itemName = EntityManager.GetComponentData<ZoxName>(itemEntity).name;
                                    tooltipText += "\n" + itemName.ToString() + " x" + reward.quantity;
                                }
                                else
                                {
                                    tooltipText += "\nUnknown x" + reward.quantity;
                                }
                            }
                            else if (reward.type == (byte) QuestRewardType.Experience)
                            {
                                tooltipText += "\n" + reward.quantity + " experience";
                            }
                        }
                    }
                }
                //tooltipText += "\nGiven by: " + quest.questRequester.Index;
                /*if (HasComponent<PanelTooltipLink>(panelEntity))
                {
                    var tooltip = EntityManager.GetComponentData<PanelTooltipLink>(panelEntity).tooltip;
                    if (HasComponent<RenderText>(tooltip))
                    {
                        PostUpdateCommands.AddComponent(tooltip, new SetRenderText(new Text(tooltipText)));
                    }
                }*/
                PostUpdateCommands.AddComponent(panelEntity, new SetPanelTooltip(new Text(tooltipText)));
                var actionTooltip = "";
                if (userQuest.handedIn == 1)
                {
                    actionTooltip = "Cannot Track a Handed In Quest";
                }
                else if (questlog.questTracking == userQuestEntity)
                {
                    actionTooltip = "[action1] to Stop Tracking"; 
                }
                else if (questlog.questTracking != userQuestEntity)
                {
                    actionTooltip = "[action1] to Track Quest";
                }
                //var playerTooltipLinks = EntityManager.GetComponentData<PlayerTooltipLinks>(controllerEntity);
                //playerTooltipLinks.SetTooltipText(PostUpdateCommands, controllerEntity, actionTooltip);
                PostUpdateCommands.AddComponent(controllerEntity, new SetPlayerTooltip(new Text(actionTooltip)));
            }).WithoutBurst().Run();
        }
    }
}