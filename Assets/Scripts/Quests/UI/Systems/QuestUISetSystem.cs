using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.UI;
using Zoxel.Textures;

namespace Zoxel.Quests.UI
{
    //! Sets the ItemUI's icon and texture.
    /**
    *   - UI Update System -
    */
    [BurstCompile, UpdateInGroup(typeof(QuestsUISystemGroup))]
    public partial class QuestUISetSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery statsQuery;
        private EntityQuery userStatsQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            statsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Quest>(),
                ComponentType.Exclude<DestroyEntity>());
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserQuest>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userStatEntities = userStatsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var metaDatas = GetComponentLookup<MetaData>(true);
            var userQuests = GetComponentLookup<UserQuest>(true);
            userStatEntities.Dispose();
            var questEntities = statsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var quests = GetComponentLookup<Quest>(true);
            questEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEventFrames, InitializeEntity>()
                .WithAll<SetIconData>()
                .ForEach((Entity e, int entityInQueryIndex, ref IconData iconData, in SetIconData setIconData, in Childrens childrens) =>
            {
                if (!HasComponent<UserQuest>(setIconData.data))
                {
                    return;
                }
                // UnityEngine.Debug.LogError("    > SetIconData");
                var userQuestEntity = setIconData.data;
                iconData.data = setIconData.data;
                if (childrens.children.Length >= 1)
                {
                    //var metaEntity = EntityManager.GetComponentData<MetaData>(iconData.data).data;
                    var iconEntity = childrens.children[0];
                    // ItemIconSetSystem.SetIconTextureEvent(EntityManager, PostUpdateCommands, iconEntity, metaEntity);
                    //var statEntity = metaDatas[userStatEntity].data;
                    //PostUpdateCommands.SetComponent(entityInQueryIndex, iconEntity, quests[statEntity].Clone());
                    //PostUpdateCommands.AddComponent<TextureDirty>(entityInQueryIndex, iconEntity);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, iconEntity, new GenerateSkillTexture(0, 0));
                    //! \todo Unique quests for quests
                }
                if (childrens.children.Length >= 2)
                {
                    var questEntity = metaDatas[userQuestEntity].data;
                    var userQuest = userQuests[userQuestEntity];
                    var quest = quests[questEntity];
                    var completedTotal = (int) userQuest.GetCompleted();
                    var maxCompleted = (int) quest.GetMaxCompleted();
                    var label = new Text();
                    label.AddInteger(completedTotal);
                    label.AddChar('/');
                    label.AddInteger(maxCompleted);
                    // completedTotal + "/" + maxCompleted;
                    var textEntity = childrens.children[1];
                    PostUpdateCommands.AddComponent(entityInQueryIndex, textEntity, new SetRenderText(label));
                }
                // UnityEngine.Debug.LogError("Set to new Item: " + e.Index + " :: " + setIconData.data.Index + " with item meta: " + metaEntity.Index);
            })  .WithReadOnly(metaDatas)
                .WithReadOnly(userQuests)
                .WithReadOnly(quests)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}