﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Audio;
using Zoxel.Transforms;

namespace Zoxel.Quests.UI
{
    //! Spawns QuestUI elements.
    /**
    *   - UI Spawn System -
    *   Label on icon spans entire width compared to half width defaults.
    */
    [BurstCompile, UpdateInGroup(typeof(QuestsUISystemGroup))]
    public partial class QuestlogUISpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity questlogUIPrefab;
        public static IconPrefabs iconPrefabs;
        public static Entity spawnQuestlogUIPrefab;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnQuestlogUI),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnQuestlogUIPrefab = EntityManager.CreateEntity(spawnArchetype);
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DelayEvent>(),
                ComponentType.ReadOnly<SpawnQuestlogUI>(),
                ComponentType.ReadOnly<CharacterLink>());
            RequireForUpdate(processQuery);
        }

        private void InitializePrefabs()
        {
            if (QuestlogUISpawnSystem.questlogUIPrefab.Index == 0)
            {
                var uiDatam = UIManager.instance.uiDatam;
                var iconStyle = uiDatam.iconStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
                var labelPosition = new float3(0, -(iconStyle.iconSize.y / 2) + (iconStyle.fontSize / 2f), 0);
                var labelSize = new float2(iconStyle.iconSize.x, iconStyle.fontSize);
                var questlogUIPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
                QuestlogUISpawnSystem.questlogUIPrefab = questlogUIPrefab;
                EntityManager.AddComponent<Prefab>(questlogUIPrefab);
                EntityManager.AddComponent<QuestlogUI>(questlogUIPrefab);
                EntityManager.SetComponentData(questlogUIPrefab, new PanelUI(PanelType.QuestlogUI));
                iconPrefabs.frame = EntityManager.Instantiate(UICoreSystem.iconPrefabs.frame);
                EntityManager.AddComponent<Prefab>(iconPrefabs.frame);
                EntityManager.AddComponent<QuestlogButton>(iconPrefabs.frame);
                iconPrefabs.icon = UICoreSystem.iconPrefabs.icon;
                iconPrefabs.label = EntityManager.Instantiate(UICoreSystem.iconPrefabs.label);
                EntityManager.AddComponent<Prefab>(iconPrefabs.label);
                EntityManager.SetComponentData(iconPrefabs.label, new LocalPosition(labelPosition));
                EntityManager.SetComponentData(iconPrefabs.label, new Size2D(labelSize));
            }
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var uiDatam = UIManager.instance.uiDatam;
            var iconStyle = uiDatam.iconStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var questlogUIPrefab = QuestlogUISpawnSystem.questlogUIPrefab;
            var iconPrefabs = QuestlogUISpawnSystem.iconPrefabs;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var spawnEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var characterLinks = GetComponentLookup<CharacterLink>(true);
            var prefabLinks = GetComponentLookup<PrefabLink>(true);
            var uiAnchors = GetComponentLookup<UIAnchor>(true);
            Dependency = Entities
                .WithAll<UILink>()
                .ForEach((Entity e, int entityInQueryIndex, in Questlog questlog, in CameraLink cameraLink) =>
            {
                var spawnEntity = new Entity();
                for (int i = 0; i < spawnEntities.Length; i++)
                {
                    var e2 = spawnEntities[i];
                    var characterLink = characterLinks[e2];
                    if (characterLink.character == e)
                    {
                        spawnEntity = e2;
                        break;  // only one UI per character atm
                    }
                }
                if (spawnEntity.Index == 0)
                {
                    return;
                }
                var prefab = questlogUIPrefab;
                if (prefabLinks.HasComponent(spawnEntity))
                {
                    prefab = prefabLinks[spawnEntity].prefab;
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CharacterLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLink);
                if (uiAnchors.HasComponent(spawnEntity))
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, uiAnchors[spawnEntity]);
                }
                // spawnedUI event
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawned((byte) questlog.quests.Length));
                for (int i = 0; i < questlog.quests.Length; i++)
                {
                    var seed = (int) (128 + elapsedTime + i * 2048 + 4196 * entityInQueryIndex);
                    var frameEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.frame, e3);
                    UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, frameEntity, e3);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, frameEntity, i);
                    UICoreSystem.SetSound(PostUpdateCommands, entityInQueryIndex, frameEntity, seed, iconStyle.soundVolume);
                    UICoreSystem.SetSeed(PostUpdateCommands, entityInQueryIndex, frameEntity, seed);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, frameEntity, new Seed(seed));
                    UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.icon, frameEntity);
                    UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.label, frameEntity); // , labelPosition, labelSize);
                    var dataEntity = questlog.quests[i];
                    if (dataEntity.Index != 0)
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, frameEntity, new SetIconData(dataEntity));
                    }
                }
            })  .WithReadOnly(spawnEntities)
                .WithDisposeOnCompletion(spawnEntities)
                .WithReadOnly(characterLinks)
                .WithReadOnly(prefabLinks)
                .WithReadOnly(uiAnchors)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}