using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Textures;
using Zoxel.Items;
using Zoxel.Stats;
using Zoxel.Races;

namespace Zoxel.Quests
{
    //! Generates Items for the Realm.
    /**
    *   - Data Generation System -
    *   Generates simple quests.
    *   \todo Generate more quest types based on biomes.
    *   \todo Display Quest in a graph that are generated.
    *   \todo Generate Unique Quests.
    */
    [BurstCompile, UpdateInGroup(typeof(QuestSystemGroup))]
    public partial class QuestsGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery itemQuery;
        private EntityQuery racesQuery;
        private NativeArray<Text> texts;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            itemQuery = GetEntityQuery(
                ComponentType.ReadOnly<RealmItem>(),
                ComponentType.ReadOnly<ZoxName>());
            racesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Race>(),
                ComponentType.ReadOnly<ZoxName>());
            texts = new NativeArray<Text>(3, Allocator.Persistent);
            texts[0] = new Text("Coin");
            texts[1] = new Text(" Slayer");
            texts[2] = new Text("[quest]");
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var texts = this.texts;
            var questPrefab = QuestSystemGroup.questPrefab;
            // var levelID = StatsManager.instance.StatSettings.soul.Value.id;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var itemEntities = itemQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var itemNames = GetComponentLookup<ZoxName>(true);
            var raceEntities = racesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var raceNames = GetComponentLookup<ZoxName>(true);
            itemEntities.Dispose();
            raceEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref GenerateRealm generateRealm, ref QuestLinks questLinks, in ZoxID zoxID, in ItemLinks itemLinks, in StatLinks statLinks,
                    in RaceLinks raceLinks) =>
            {
                if (generateRealm.state == (byte) GenerateRealmState.GenerateQuests2)
                {
                    generateRealm.state = (byte) GenerateRealmState.GenerateColors;
                    return;
                }
                if (generateRealm.state != (byte) GenerateRealmState.GenerateQuests)
                {
                    return; // wait for thingo
                }
                var realmSeed = zoxID.id;
                if (realmSeed == 0)
                {
                    return;
                }
                generateRealm.state = (byte) GenerateRealmState.GenerateQuests2;
                var coinItemEntity = new Entity();
                // find coin
                var coinLabel = texts[0];
                for (int i = 0; i < itemLinks.items.Length; i++)
                {
                    var itemEntity = itemLinks.items[i];
                    // Shouldn't I just get currency value and if item is currency?
                    var itemName = itemNames[itemEntity].name;
                    if (itemName.Contains(in coinLabel)) // "Coin"))
                    {
                        //UnityEngine.Debug.LogError("Found Coin at: " + i);
                        coinItemEntity = itemEntity;
                        break;
                    }
                }
                /*if (coinItemEntity.Index == 0)
                {
                    UnityEngine.Debug.LogError("Could not find Coin Item.");
                }*/
                var soulStatEntity = new Entity();
                for (int i = 0; i < statLinks.userStatLinks.Length; i++)
                {
                    var statEntity = statLinks.userStatLinks[i];
                    if (HasComponent<RealmLevelStat>(statEntity))
                    {
                        soulStatEntity = statEntity;
                        break;
                    }
                }
                // Destroy UserSkillLinks and SkillTrees
                for (int i = 0; i < questLinks.quests.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, questLinks.quests[i]);
                }
                questLinks.Dispose();
                //  var newQuests = new NativeList<Entity>();
                var slayerLabel = texts[1];
                for (int i = 0; i < 3; i++)
                {
                    var questID = realmSeed + (i + 1) * 1237;
                    var random = new Random();
                    random.InitState((uint) questID);
                    var questEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, questPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, questEntity, new ZoxID(questID));
                    var raceEntity = raceLinks.races[1 + i];
                    //var raceID = EntityManager.GetComponentData<ZoxID>(race).id;
                    var questName = raceNames[raceEntity].name.Clone();
                    // var questName = raceName + " slayer";
                    //UnityEngine.Debug.LogError("Quest 2 " + raceNames[raceEntity].name.ToString());
                    questName.AddText(in slayerLabel);
                    //UnityEngine.Debug.LogError("Quest " + questName);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, questEntity, new ZoxName(questName));
                    var quest = new Quest();
                    // add objectives
                    var objectiveA = new QuestBlock();
                    objectiveA.target = raceEntity; // 0;
                    objectiveA.targetType = QuestBlockType.KillCharacter;
                    objectiveA.maxCompleted = 3 + random.NextInt(3);
                    // need to have a race ID - pass in games data here and chose a monster one
                    quest.SetObjective(objectiveA);
                    // todo: add quest rewards
                    // UnityEngine.Debug.LogError("Item Rewarding: " + itemID);
                    quest.AddReward(new QuestReward(QuestRewardType.Item, coinItemEntity, (byte) (3 + random.NextInt(3))));
                    quest.AddReward(new QuestReward(QuestRewardType.Experience, soulStatEntity, (byte) (5 + random.NextInt(3))));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, questEntity, quest);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, questEntity, new QuestIndex(i));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, questEntity, new QuestHolderLink(e));
                    // newQuests.Add(questEntity);
                    #if UNITY_EDITOR
                    var editorName = texts[2].Clone();
                    editorName.AddText(questName);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, questEntity, new EditorName(editorName));
                    #endif
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnQuestSpawned((byte) 3));
            })  .WithReadOnly(itemNames)
                .WithReadOnly(raceNames)
                .WithReadOnly(texts)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

        }
    }
}