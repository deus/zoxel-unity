using Unity.Entities;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Zoxel.Quests
{
    //! Links to (Realm) Quest's data!
    public struct QuestLinks : IComponentData
    {
        public BlitableArray<Entity> quests;
        public UnsafeParallelHashMap<int, Entity> questsHash;

        public void Dispose()
        {
            if (quests.Length > 0)
            {
                quests.Dispose();
            }
			if (questsHash.IsCreated)
			{
				questsHash.Dispose();
			}
        }

        public void SetAs(byte count)
        {
			if (quests.Length != count)
			{
				Dispose();
				this.quests = new BlitableArray<Entity>(count, Allocator.Persistent);
                this.questsHash = new UnsafeParallelHashMap<int, Entity>(count, Allocator.Persistent);
				/*for (int i = 0; i < count; i++)
				{
					this.quests[i] = new Entity();
				}*/
			}
        }

        public void Initialize(int count)
        {
            Dispose();
            this.quests = new BlitableArray<Entity>(count, Allocator.Persistent);
            this.questsHash = new UnsafeParallelHashMap<int, Entity>(count, Allocator.Persistent);
        }

        public Entity GetQuest(int targetID)
        {
            if (questsHash.IsCreated)
            {
                Entity outputEntity;
                if (questsHash.TryGetValue(targetID, out outputEntity))
                {
                    return outputEntity;
                }
            }
            return new Entity();
        }
    }
}