﻿using Unity.Entities;
using Zoxel.Textures;

namespace Zoxel.Quests
{
    //! Holds questy things.
    public partial class QuestSystemGroup : ComponentSystemGroup
    {
        public static Entity questPrefab;
        public static Entity userQuestPrefab;

        protected override void OnCreate()
        {
            base.OnCreate();
            var questArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(ZoxName),
                typeof(ZoxID),
                typeof(Quest),
                typeof(QuestIndex),
                typeof(QuestHolderLink),
                typeof(Texture));
            questPrefab = EntityManager.CreateEntity(questArchetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(questPrefab); // EntityManager.AddComponentData(questPrefab, new EditorName("[quest]"));
            #endif
            var userQuestArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(NewUserQuest),
                typeof(UserQuest),
                typeof(MetaData),
                typeof(UserDataIndex),
                typeof(QuestHolderLink));
            userQuestPrefab = EntityManager.CreateEntity(userQuestArchetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(userQuestPrefab); // EntityManager.AddComponentData(questPrefab, new EditorName("[userquest]"));
            #endif
        }
    }
}