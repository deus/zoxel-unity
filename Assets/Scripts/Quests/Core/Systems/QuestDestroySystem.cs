using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Quests
{
    // Cleans up Quest's.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class QuestDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, Quest>()
                .ForEach((in Quest quest) =>
			{
                quest.Dispose();
			}).ScheduleParallel();
        }
    }
}