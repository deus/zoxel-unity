using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Stats;

namespace Zoxel.Quests
{
    //! Gives user a quest from the Realm's QuestLinks.
    /**
    *   \todo Give quests a difficulty level and base event off that.
    */
    [BurstCompile, UpdateInGroup(typeof(QuestSystemGroup))]
    public partial class GiveRealmQuestSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;
        private EntityQuery questsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			realmQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<QuestLinks>(),
                ComponentType.Exclude<DestroyEntity>());
			questsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Quest>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var userQuestPrefab = QuestSystemGroup.userQuestPrefab;
            // var levelID = StatsManager.instance.StatSettings.soul.Value.id;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var realmQuestLinks = GetComponentLookup<QuestLinks>(true);
            var questEntities = questsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var quests = GetComponentLookup<Quest>(true);
            realmEntities.Dispose();
            questEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<GiveRealmQuest>()
                .ForEach((Entity e, int entityInQueryIndex, in Questlog questlog, in ZoxID zoxID, in RealmLink realmLink) =>
            {
                if (zoxID.id == 0)
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<GiveRealmQuest>(entityInQueryIndex, e);
                var random = new Random();
                random.InitState((uint)zoxID.id);
                // if (random.NextFloat(100) >= 40)
                var realmQuestLinks2 = realmQuestLinks[realmLink.realm];
                var questEntity = realmQuestLinks2.quests[random.NextInt(3)];
                var quest = quests[questEntity];
                var userQuestEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, userQuestPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, userQuestEntity, new QuestHolderLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, userQuestEntity, new UserDataIndex((byte) questlog.quests.Length));
                PostUpdateCommands.SetComponent(entityInQueryIndex, userQuestEntity, new UserQuest(questEntity, quest));
                PostUpdateCommands.SetComponent(entityInQueryIndex, userQuestEntity, new MetaData(questEntity));
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUserQuestSpawned((byte) 1));
            })  .WithReadOnly(realmQuestLinks)
                .WithReadOnly(quests)
                .ScheduleParallel(Dependency);
           // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}