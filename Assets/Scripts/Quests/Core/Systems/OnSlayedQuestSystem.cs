using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Stats;
using Zoxel.Races;

namespace Zoxel.Quests
{
    //! Handles OnKilledEntity event for Questlog's.
    [BurstCompile, UpdateInGroup(typeof(QuestSystemGroup))]
    public partial class OnSlayedQuestSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery aliveCharactersQuery;
        private EntityQuery deadCharactersQuery;
        private EntityQuery questsQuery;
        private EntityQuery userQuestsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            aliveCharactersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Character>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DyingEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            deadCharactersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Character>(),
                ComponentType.ReadOnly<DyingEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            questsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Quest>(),
                ComponentType.Exclude<DestroyEntity>());
            userQuestsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserQuest>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var aliveEntities = aliveCharactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var questlogs = GetComponentLookup<Questlog>(true);
            aliveEntities.Dispose();
            var deadEntities = deadCharactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var raceLinks = GetComponentLookup<RaceLink>(true);
            deadEntities.Dispose();
            var questEntities = questsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var quests = GetComponentLookup<Quest>(true);
            questEntities.Dispose();
            var userQuestEntities = userQuestsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var userQuests = GetComponentLookup<UserQuest>(true);
            var userQuestMetaDatas = GetComponentLookup<MetaData>(true);
            userQuestEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((int entityInQueryIndex, in OnKilledEntity onKilledEntity) =>
            {
                var killerEntity = onKilledEntity.killer;
                var enemyEntity = onKilledEntity.enemy;
                // if killer dies in same frame
                if (!questlogs.HasComponent(killerEntity) || !raceLinks.HasComponent(enemyEntity))
                {
                    return;
                }
                var questlog = questlogs[killerEntity];
                var enemyRace = raceLinks[enemyEntity].race;
                var didUpdateQuestlog = false;
                // UnityEngine.Debug.LogError("OnKilledEntity: " + OnKilledEntity.enemy.Index);
                for (int i = 0; i < questlog.quests.Length; i++)
                {
                    var userQuestEntity = questlog.quests[i];
                    /*if (!userQuests.HasComponent(userQuestEntity) || !userQuestMetaDatas.HasComponent(userQuestEntity))
                    {
                        UnityEngine.Debug.LogError("E 1");
                        continue;
                    }*/
                    if (!userQuests.HasComponent(userQuestEntity))
                    {
                        UnityEngine.Debug.LogError("E 1");
                        continue;
                    }
                    if (!userQuestMetaDatas.HasComponent(userQuestEntity))
                    {
                        UnityEngine.Debug.LogError("E 2");
                        continue;
                    }
                    var userQuest = userQuests[userQuestEntity];
                    var questEntity = userQuestMetaDatas[userQuestEntity].data;
                    if (!quests.HasComponent(questEntity))
                    {
                        UnityEngine.Debug.LogError("E 3");
                        continue;
                    }
                    var quest = quests[questEntity];
                    var didUpdateQuest = false;
                    for (byte j = 0; j < quest.blocks.Length; j++)
                    {
                        var block = quest.blocks[j];
                        if(userQuest.OnKilledEntity(j, in block, enemyRace))
                        {
                            didUpdateQuest = true;
                        }
                    }
                    if (didUpdateQuest)
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, userQuestEntity, userQuest);
                        didUpdateQuestlog = true;
                    }
                }
                if (didUpdateQuestlog && HasComponent<EntitySaver>(killerEntity))
                {
                    PostUpdateCommands.AddComponent<SaveQuestlog>(entityInQueryIndex, killerEntity);
                }
            })  .WithReadOnly(questlogs)
                .WithReadOnly(raceLinks)
                .WithReadOnly(quests)
                .WithReadOnly(userQuests)
                .WithReadOnly(userQuestMetaDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}