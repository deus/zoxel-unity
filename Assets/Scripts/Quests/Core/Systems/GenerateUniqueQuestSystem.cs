using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Items;
using Zoxel.Stats;
using Zoxel.Races;

namespace Zoxel.Quests
{
    //! Generates unique quests per character.
    /**
    *   \todo Generate random quest based on npcs needs. Give this quest a unique texture / frame to identify it. Save/Load Unique Quests.
    */
    [BurstCompile, UpdateInGroup(typeof(QuestSystemGroup))]
    public partial class GenerateUniqueQuestSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (StatsManager.instance == null) return;
            var userQuestPrefab = QuestSystemGroup.userQuestPrefab;
            var levelID = StatsManager.instance.StatSettings.soul.Value.id;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<GenerateQuest>()
                .ForEach((Entity e, int entityInQueryIndex, in Questlog questlog, in QuestLinks questLinks, in ZoxID zoxID, in RealmLink realmLink) =>
            {
                if (zoxID.id == 0)
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<GenerateQuest>(entityInQueryIndex, e);
                // Give entity a unique quest
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}