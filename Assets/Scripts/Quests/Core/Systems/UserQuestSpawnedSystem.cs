using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Quests
{
    //! Links user items to inventory user after they spawn.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(QuestSystemGroup))]
    public partial class UserQuestSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userQuestsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userQuestsQuery = GetEntityQuery(
                ComponentType.ReadOnly<NewUserQuest>(),
                ComponentType.ReadOnly<UserQuest>(),
                ComponentType.ReadOnly<QuestHolderLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<OnUserQuestSpawned>(processQuery);
            PostUpdateCommands2.RemoveComponent<NewUserQuest>(userQuestsQuery);
            //! First initializes children.
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity>()
                .ForEach((ref Questlog questlog, in OnUserQuestSpawned onUserQuestSpawned) =>
            {
                if (onUserQuestSpawned.spawned != 0)
                {
                    questlog.SetAs(onUserQuestSpawned.spawned);
                }
            }).ScheduleParallel(Dependency);
            //! For each child, using the index, sets into parents children that is passed in.
            var parentEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var questlog = GetComponentLookup<Questlog>(false);
            // parentEntities.Dispose();
            Dependency = Entities
                .WithAll<NewUserQuest>()
                .ForEach((Entity e, in UserDataIndex userDataIndex, in QuestHolderLink questHolderLink) =>
            {
                var questlog2 = questlog[questHolderLink.questHolder];
                questlog2.quests[userDataIndex.index] = e;
                if (parentEntities.Length > 0) { var e2 = parentEntities[0]; }
            })  .WithReadOnly(parentEntities)
                .WithDisposeOnCompletion(parentEntities)
                .WithNativeDisableContainerSafetyRestriction(questlog)
                .ScheduleParallel(Dependency);
            //! Set quest tracking on linking.
            Dependency = Entities
                .WithNone<InitializeEntity>()
                .WithAll<OnUserQuestSpawned>()
                .ForEach((ref Questlog questlog) =>
            {
                if (questlog.questTracking.Index == 0 && questlog.quests.Length > 0)
                {
                    questlog.questTracking = questlog.quests[questlog.quests.Length - 1];
                }
            })  .ScheduleParallel(Dependency);
        }
    }
}