using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Quests
{
    //! Links user items to inventory user after they spawn.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(QuestSystemGroup))]
    public partial class QuestSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery questsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            questsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Quest>(),
                ComponentType.ReadOnly<QuestHolderLink>(),
                ComponentType.ReadOnly<QuestIndex>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var questEntities = questsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var questHolderLinks = GetComponentLookup<QuestHolderLink>(true);
            var questIndexes = GetComponentLookup<QuestIndex>(true);
            var questIDs = GetComponentLookup<ZoxID>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref QuestLinks questLinks, ref OnQuestSpawned onQuestSpawned) =>
            {
                if (onQuestSpawned.spawned != 255)
                {
                    questLinks.SetAs(onQuestSpawned.spawned);
                    onQuestSpawned.spawned = 255;
                }
                var count = 0;
                for (int i = 0; i < questEntities.Length; i++)
                {
                    var e2 = questEntities[i];
                    var questHolder = questHolderLinks[e2].questHolder;
                    if (questHolder == e)
                    {
                        var index = questIndexes[e2].index;
                        questLinks.quests[index] = e2;
                        var questID = questIDs[e2].id;
                        questLinks.questsHash[questID] = e2;
                        count++;
                        if (count == questLinks.quests.Length)
                        {
                            PostUpdateCommands.RemoveComponent<OnQuestSpawned>(entityInQueryIndex, e);
                            break;
                        }
                    }
                }
            })  .WithReadOnly(questEntities)
                .WithDisposeOnCompletion(questEntities)
                .WithReadOnly(questHolderLinks)
                .WithReadOnly(questIndexes)
                .WithReadOnly(questIDs)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}