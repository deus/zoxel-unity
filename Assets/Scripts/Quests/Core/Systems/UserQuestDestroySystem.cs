using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Quests
{
    // Cleans up UserQuest's.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class UserQuestDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, UserQuest>()
                .ForEach((in UserQuest userQuest) =>
			{
                userQuest.Dispose();
			}).ScheduleParallel();
        }
    }
}