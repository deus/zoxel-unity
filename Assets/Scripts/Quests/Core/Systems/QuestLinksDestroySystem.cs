using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Quests
{
    //! Disposes of QuestLinks on Realm entity.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class QuestLinksDestroySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithAll<DestroyEntity, QuestLinks>()
                .ForEach((int entityInQueryIndex, in QuestLinks questLinks) =>
			{
                for (int i = 0; i < questLinks.quests.Length; i++)
                {
                    var e = questLinks.quests[i];
                    if (HasComponent<Quest>(e))
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e);
                    }
                }
                questLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}