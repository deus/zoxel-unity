using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Quests
{
    //! Cleans up Questlog's with DestroyEntity.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class QuestlogDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithAll<DestroyEntity, Questlog>()
                .ForEach((int entityInQueryIndex, in Questlog questlog) =>
			{
                for (int i = 0; i < questlog.quests.Length; i++)
                {
                    var userQuestEntity = questlog.quests[i];
                    if (HasComponent<UserQuest>(userQuestEntity))
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, userQuestEntity);
                    }
                }
                questlog.DisposeFinal();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}