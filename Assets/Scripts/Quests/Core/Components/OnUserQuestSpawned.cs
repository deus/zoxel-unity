using Unity.Entities;

namespace Zoxel.Quests
{
    //! An event for linking up UserQuest's after spawning.
    public struct OnUserQuestSpawned : IComponentData
    {
        public byte spawned;

        public OnUserQuestSpawned(byte spawned)
        {
            this.spawned = spawned;
        }
    }
}