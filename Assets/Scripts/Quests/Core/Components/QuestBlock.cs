using Unity.Entities;
using System;

namespace Zoxel.Quests
{
    //! A block of data, a small goal, a part of a quest.
    [Serializable]
    public struct QuestBlock : IComponentData
    {
        //! The targget amount of objectives.
        public int maxCompleted;
        //! The type of objective.
        public byte targetType;
        //! The target Meta Data of the quest block, Race for kill tarets.
        public Entity target;
    }
}