using Unity.Entities;

namespace Zoxel.Quests
{
    //! Links to a entity with QuestLinks.
    public struct QuestHolderLink : IComponentData
    {
        public Entity questHolder;

        public QuestHolderLink(Entity questHolder)
        {
            this.questHolder = questHolder;
        }
    }
}