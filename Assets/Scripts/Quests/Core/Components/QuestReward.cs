using Unity.Entities;
using System;

namespace Zoxel.Quests
{
    //! A single reward for a given quest.
    /**
    *   \todo Give a skill point for completing a major quest!
    */
    [Serializable]
    public struct QuestReward : IComponentData
    {
        public Entity targetEntity;        // replace with target entity
        public byte type;
        public byte quantity;

        public QuestReward(byte type, Entity targetEntity, byte quantity)
        {
            this.type = type;
            this.targetEntity = targetEntity;
            this.quantity = quantity;
        }
    }
}