using Unity.Collections;
using Unity.Entities;

namespace Zoxel.Quests
{
    //! Contains quest information for user to do.
    public struct Quest : IComponentData
    {
        public BlitableArray<QuestBlock> blocks;
        public BlitableArray<QuestReward> rewards;

        public void Dispose()
        {
            DisposeBlocks();
            DisposeRewards();
        }

        public void DisposeBlocks()
        {
            blocks.Dispose();
        }

        public void DisposeRewards()
        {
            rewards.Dispose();
        }

        public int GetMaxCompleted()
        {
            int completed = 0;
            for (byte i = 0; i < this.blocks.Length; i++)
            {
                completed += blocks[i].maxCompleted;
            }
            return completed;
        }

        public void SetObjective(QuestBlock newBlock)
        {
            DisposeBlocks();
            blocks = new BlitableArray<QuestBlock>(1, Allocator.Persistent);
            blocks[0] = newBlock;
        }

        public void AddReward(QuestReward newReward)
        {
            var rewards2 = new BlitableArray<QuestReward>(rewards.Length + 1, Allocator.Persistent);
            for (int i = 0; i < rewards.Length; i++)
            {
                rewards2[i] = rewards[i];
            }
            rewards2[rewards.Length] = newReward;
            DisposeRewards();
            rewards = rewards2;
        }
    }
}