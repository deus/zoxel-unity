using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Quests
{
    //! Stores user data for a Quest.
    public struct UserQuest : IComponentData
    {
        //! Has the quest been handed in.
        public byte handedIn;
        //! Keep ID as they will unload.
        public int questRequesterID;
        //! Keeps how many of the objective has been completed.
        public BlitableArray<byte> completed;

        public UserQuest(Entity questEntity, Quest quest)
        {
            this.handedIn = 0;
            this.completed = new BlitableArray<byte>(quest.blocks.Length, Allocator.Persistent);
            for (int i = 0; i < this.completed.Length; i++)
            {
                this.completed[i] = 0;
            }
            this.questRequesterID = 0;
        }

        public UserQuest(UserQuest userQuest, int questRequesterID)
        {
            this.handedIn = 0;
            this.questRequesterID = questRequesterID;
            this.completed = new BlitableArray<byte>(userQuest.completed.Length, Allocator.Persistent);
            for (byte i = 0; i < this.completed.Length; i++)
            {
                this.completed[i] = 0;
            }
        }

        public void Dispose()
        {
            completed.Dispose();
        }
        
        public byte GetCompleted()
        {
            var completedTotal = (byte) 0;
            for (byte i = 0; i < this.completed.Length; i++)
            {
                completedTotal += this.completed[i];
            }
            return completedTotal;
        }

        //! Returns true if updated for UI.
        public bool OnKilledEntity(byte index, in QuestBlock questBlock, Entity metaEntity)
        {
            if (questBlock.targetType == QuestBlockType.KillCharacter)
            {
                if (questBlock.target == metaEntity)
                {
                    var completed2 = completed[index];
                    if (completed2 != questBlock.maxCompleted)
                    {
                        completed2++;
                        completed[index] = completed2;
                        return true;
                    }
                }
            }
            return false;
        }

        public bool HasCompleted(in Quest quest)
        {
            // UnityEngine.Debug.LogError("Completed: " + GetCompleted() + " max: " + GetMaxCompleted());
            return quest.GetMaxCompleted() == GetCompleted();
        }
    }
}