using Unity.Entities;

namespace Zoxel.Quests
{
    //! Gives user a quest from the Realm's QuestLinks.
    public struct GiveRealmQuest : IComponentData { }
}