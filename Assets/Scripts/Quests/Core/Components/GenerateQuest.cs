using Unity.Entities;

namespace Zoxel.Quests
{
    //! Generates a unique quest for an npc to give out.
    public struct GenerateQuest : IComponentData { }
}