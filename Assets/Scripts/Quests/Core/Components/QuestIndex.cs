using Unity.Entities;

namespace Zoxel.Quests
{
    public struct QuestIndex : IComponentData
    {
        public int index;

        public QuestIndex(int index)
        {
            this.index = index;
        }
    }
}