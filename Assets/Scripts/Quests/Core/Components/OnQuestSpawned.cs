using Unity.Entities;

namespace Zoxel.Quests
{
    //! An event for linking up UserQuest's after spawning.
    public struct OnQuestSpawned : IComponentData
    {
        public byte spawned;

        public OnQuestSpawned(byte spawned)
        {
            this.spawned = spawned;
        }
    }
}