using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Quests
{
    //! Contains UserQuest links.
    public struct Questlog : IComponentData
    {
        public Entity questTracking;             //! Link to UserQuest we are tracking.
        public BlitableArray<Entity> quests;     //! Links to UserQuest entities.

        public void DisposeFinal()
        {
            quests.DisposeFinal();
        }

        public void Dispose()
        {
            this.quests.Dispose();
        }

        public void SetAs(byte newCount)
        {
			if (quests.Length != newCount)
			{
				Dispose();
				quests = new BlitableArray<Entity>(newCount, Allocator.Persistent);
				for (int i = 0; i < newCount; i++)
				{
					quests[i] = new Entity();
				}
			}
        }

        public bool HasCompletedQuest(EntityManager EntityManager, int targetID)
        {
            for (int i = 0; i < this.quests.Length; i++)
            {
                var userQuestEntity = quests[i];
                var questEntity = EntityManager.GetComponentData<MetaData>(userQuestEntity).data;
                var userQuestID = EntityManager.GetComponentData<ZoxID>(questEntity).id;
                if (userQuestID == targetID)
                {
                    var userQuest = EntityManager.GetComponentData<UserQuest>(userQuestEntity);
                    var quest = EntityManager.GetComponentData<Quest>(questEntity);
                    return userQuest.HasCompleted(in quest);
                }
            }
            // UnityEngine.Debug.LogError("Did not find quest: " + questID + " out of " + quests.Length);
            return false;
        }

        public bool HasHandedInQuest(EntityManager EntityManager, int targetID)
        {
            for (int i = 0; i < this.quests.Length; i++)
            {
                var userQuestEntity = quests[i];
                var questEntity = EntityManager.GetComponentData<MetaData>(userQuestEntity).data;
                var userQuestID = EntityManager.GetComponentData<ZoxID>(questEntity).id;
                if (userQuestID == targetID)
                {
                    var userQuest = EntityManager.GetComponentData<UserQuest>(userQuestEntity);
                    return userQuest.handedIn == 1;
                }
            }
            return false;
        }

        public void OnQuestHandedIn(EntityManager EntityManager, EntityCommandBuffer PostUpdateCommands, int targetID)
        {
            for (int i = 0; i < this.quests.Length; i++)
            {
                var userQuestEntity = quests[i];
                var userQuest = EntityManager.GetComponentData<UserQuest>(userQuestEntity);
                var questEntity =EntityManager.GetComponentData<MetaData>(userQuestEntity).data;
                var userQuestID = EntityManager.GetComponentData<ZoxID>(questEntity).id;
                if (userQuestID == targetID)
                {
                    // var quest = this.quests[i];
                    if (userQuestEntity == questTracking)
                    {
                        questTracking = new Entity();
                    }
                    userQuest.handedIn = 1;
                    PostUpdateCommands.SetComponent(userQuestEntity, userQuest);
                    break;
                }
            }
        }

        public bool HasQuest(EntityManager EntityManager, int targetID)
        {
            for (int i = 0; i < this.quests.Length; i++)
            {
                var questEntity = EntityManager.GetComponentData<MetaData>(quests[i]).data;
                var userQuestID = EntityManager.GetComponentData<ZoxID>(questEntity).id;
                if (userQuestID == targetID)
                {
                    return true;
                }
            }
            return false;
        }

        public Entity GetQuest(EntityManager EntityManager, int targetID)
        {
            for (int i = 0; i < this.quests.Length; i++)
            {
                var questEntity = EntityManager.GetComponentData<MetaData>(quests[i]).data;
                var userQuestID = EntityManager.GetComponentData<ZoxID>(questEntity).id;
                if (userQuestID == targetID)
                {
                    return quests[i];
                }
            }
            // failed to find quest
            return new Entity();
        }
    }
}