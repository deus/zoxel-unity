namespace Zoxel.Quests
{
    //! The type of QuestBlock.
    public static class QuestBlockType
    {
        public const byte KillCharacter = 0;
        public const byte CollectItem = 1;
        public const byte EnterArea = 2;
    }
}