using System;

namespace Zoxel.Quests
{
    /*public enum QuestRewardType : byte
    {
        None,
        Item,
        Experience,
        Stat
    }*/
    public static class QuestRewardType
    {
        public const byte None = 0;
        public const byte Item = 1;
        public const byte Experience = 2;
        public const byte Stat = 3;
    }
}