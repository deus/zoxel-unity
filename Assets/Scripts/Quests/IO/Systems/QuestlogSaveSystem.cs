using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using System;
using System.IO;

namespace Zoxel.Quests
{
    //! Saves the Questlog to memory.
    /**
    *   Saves questID, QuestRequesterID, handedIn, completedLength, completed (list).
    *   - Save System -
    */
    [UpdateInGroup(typeof(QuestSystemGroup))]
    public partial class QuestlogSaveSystem : SystemBase
    {
        const string filename = "Questlog.zox";
        const int bytesPerData = 10;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userQuestsQuery;
        private EntityQuery questsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userQuestsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserQuest>(),
                ComponentType.Exclude<DestroyEntity>());
            questsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Quest>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands.RemoveComponent<SaveQuestlog>(processQuery);
            var userQuestEntities = userQuestsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var metaDatas = GetComponentLookup<MetaData>(true);
            var userQuests = GetComponentLookup<UserQuest>(true);
            var questEntities = questsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var questIDs = GetComponentLookup<ZoxID>(true);
            userQuestEntities.Dispose();
            questEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<OnUserQuestSpawned>()
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<LoadQuestlog, GiveRealmQuest>()
                .WithNone<InitializeSaver, DisableSaving>()
                .WithAll<SaveQuestlog>()
                .ForEach((in EntitySaver entitySaver, in Questlog questlog) =>
            {
                var filepath = entitySaver.GetPath() + filename;
                QuestlogSaveSystem.SaveQuestlog(filepath, in questlog, in metaDatas, in userQuests, in questIDs);
            })  .WithReadOnly(metaDatas)
                .WithReadOnly(userQuests)
                .WithReadOnly(questIDs)
                .WithoutBurst().Run();
        }

        public static void SaveQuestlog(string filepath, in Questlog questlog, in ComponentLookup<MetaData> metaDatas,
            in ComponentLookup<UserQuest> userQuests, in ComponentLookup<ZoxID> questIDs)
        {
            // var bytes = new BlitableArray<byte>(4 + questlog.quests.Length * bytesPerData, Allocator.Temp);
            var bytes = new NativeList<byte>();
            if (questlog.questTracking.Index > 0 && metaDatas.HasComponent(questlog.questTracking))
            {
                var questTrackingEntity = metaDatas[questlog.questTracking].data;
                var questTrackingID = questIDs[questTrackingEntity].id;
                ByteUtil.AddInt(ref bytes, questTrackingID);
            }
            else
            {
                ByteUtil.AddInt(ref bytes, 0);
            }
            for (int i = 0; i < questlog.quests.Length; i++)
            {
                var userQuestEntity = questlog.quests[i];
                if (userQuestEntity.Index == 0 || !metaDatas.HasComponent(userQuestEntity))
                {
                    for (var j = 0; j < bytesPerData; j++)
                    {
                        bytes.Add(0);
                    }
                    continue;
                }
                var metaEntity = metaDatas[userQuestEntity].data;
                if (metaEntity.Index == 0 || !questIDs.HasComponent(metaEntity))
                {
                    for (var j = 0; j < bytesPerData; j++)
                    {
                        bytes.Add(0);
                    }
                    continue;
                }
                var questID = questIDs[metaEntity].id;
                var userQuest = userQuests[userQuestEntity];
                // QuestID
                ByteUtil.AddInt(ref bytes, questID);
                // QuestRequesterID
                ByteUtil.AddInt(ref bytes, userQuest.questRequesterID);
                // HandedIn
                bytes.Add(userQuest.handedIn);
                // Completed
                var completedLength = (byte) (userQuest.completed.Length);
                bytes.Add(completedLength);
                for (byte j = 0; j < userQuest.completed.Length; j++)
                {
                    bytes.Add(userQuest.completed[j]);
                }
            }
            #if WRITE_ASYNC
            File.WriteAllBytesAsync(filepath, bytes.AsArray().ToArray());
            #else
            File.WriteAllBytes(filepath, bytes.AsArray().ToArray());
            #endif
            bytes.Dispose();
        }
    }
}