using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Quests
{
    //! Loads the Questlog from memory.
    /**
    *   - Load System -
    */
    [BurstCompile, UpdateInGroup(typeof(QuestSystemGroup))]
    public partial class QuestlogLoadSystem : SystemBase
    {
        const string filename = "Questlog.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<QuestLinks>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithAll<Questlog, RealmLink>()
                .ForEach((ref LoadQuestlog loadQuestlog, ref Questlog questlog, in EntitySaver entitySaver) =>
            {
                if (loadQuestlog.loadPath.Length == 0)
                {
                    loadQuestlog.loadPath = new Text(entitySaver.GetPath() + filename);
                }
                loadQuestlog.reader.UpdateOnMainThread(in loadQuestlog.loadPath);
            }).WithoutBurst().Run();
            var userQuestPrefab = QuestSystemGroup.userQuestPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            //PostUpdateCommands.RemoveComponentForEntityQuery<LoadQuestlog>(processQuery);
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var questLinks = GetComponentLookup<QuestLinks>(true);
            realmEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity, InitializeSaver>()
                .WithAll<Questlog, EntitySaver>()
                .ForEach((Entity e, int entityInQueryIndex, ref LoadQuestlog loadQuestlog, ref Questlog questlog, in RealmLink realmLink) =>
            {
                if (loadQuestlog.reader.IsReadFileInfoComplete())
                {
                    if (loadQuestlog.reader.DoesFileExist())
                    {
                        loadQuestlog.reader.state = AsyncReadState.StartOpenFile;
                    }
                    else
                    {
                        // UnityEngine.Debug.LogError("File Did not exist.");
                        loadQuestlog.Dispose();
                        PostUpdateCommands.RemoveComponent<LoadQuestlog>(entityInQueryIndex, e);
                    }
                }
                else if (loadQuestlog.reader.IsReadFileComplete())
                {
                    if (loadQuestlog.reader.DidFileReadSuccessfully())
                    {
                        var bytes = loadQuestlog.reader.GetBytes();
                        var questLinks2 = questLinks[realmLink.realm];
                        var byteIndex = 0;
                        var questTrackingID = ByteUtil.GetInt(in bytes, byteIndex);
                        byteIndex += 4;
                        questlog.questTracking = questLinks2.GetQuest(questTrackingID);
                        var questHolderLink = new QuestHolderLink(e);
                        byte count = 0;
                        var userQuestEntities = new NativeList<Entity>();
                        // maybe record how many quests there are?
                        /*var userEntities = new NativeArray<Entity>(count, Allocator.Temp);
                        PostUpdateCommands.Instantiate(entityInQueryIndex, userPrefab, userEntities);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, userEntities, new CreatorLink(e));*/
                        while (byteIndex < bytes.Length)
                        {
                            // Spawn Item
                            var userQuestEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, userQuestPrefab);
                            userQuestEntities.Add(userQuestEntity);
                            // PostUpdateCommands.SetComponent(entityInQueryIndex, userQuestEntity, questHolderLink);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, userQuestEntity, new UserDataIndex(count));
                            var userQuest = new UserQuest();
                            // Data
                            var questID = ByteUtil.GetInt(in bytes, byteIndex);
                            byteIndex += 4;
                            userQuest.questRequesterID = ByteUtil.GetInt(in bytes, byteIndex);
                            byteIndex += 4;
                            userQuest.handedIn = bytes[byteIndex];
                            byteIndex += 1;
                            var completedLength = bytes[byteIndex];
                            byteIndex += 1;
                            userQuest.completed = new BlitableArray<byte>(completedLength, Allocator.Persistent);
                            for (byte i = 0; i < completedLength; i++)
                            {
                                userQuest.completed[i] = bytes[byteIndex];
                                byteIndex += 1;
                            }
                            PostUpdateCommands.SetComponent(entityInQueryIndex, userQuestEntity, userQuest);
                            var questEntity = questLinks2.GetQuest(questID);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, userQuestEntity, new MetaData(questEntity));
                            // finally increase count
                            count++;
                        }
                        var userQuestEntitiesArray = userQuestEntities.AsArray();
                        PostUpdateCommands.AddComponent(entityInQueryIndex, userQuestEntitiesArray, questHolderLink);
                        userQuestEntities.Dispose();
                        userQuestEntitiesArray.Dispose();
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUserQuestSpawned(count));
                    }
                    // Dispose
                    loadQuestlog.Dispose();
                    PostUpdateCommands.RemoveComponent<LoadQuestlog>(entityInQueryIndex, e);
                }
                // UnityEngine.Debug.LogError("Loaded Unique Items [" + count + "] at: " + filepath);
            })  .WithReadOnly(questLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}