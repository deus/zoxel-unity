using Unity.Entities;

namespace Zoxel.Quests
{
    //! Loads user quests.
    public struct LoadQuestlog : IComponentData
    {
        public Text loadPath;
        public AsyncFileReader reader;

        public void Dispose()
        {
            loadPath.Dispose();
            reader.Dispose();
        }
    }
}