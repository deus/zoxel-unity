/*using Unity.Collections;
using Unity.Entities;
using System;

namespace Zoxel.Quests
{
    // Stores UserQuest data in IO using serialization.
    [Serializable]
    public struct SerializeableUserQuest
    {
        public int questID;
        public int questRequesterID;
        public byte handedIn;
        public byte[] completed;

        public SerializeableUserQuest(EntityManager EntityManager, Entity userQuestEntity)
        {
            var userQuest = EntityManager.GetComponentData<UserQuest>(userQuestEntity);
            var questID = EntityManager.GetComponentData<ZoxID>(userQuest.quest).id;
            this.questID = questID;
            this.questRequesterID = userQuest.questRequesterID;
            this.completed = userQuest.completed.AsArray().ToArray();
            this.handedIn = userQuest.handedIn;
        }

        public UserQuest GetRealOne()
        {
            var userQuest = new UserQuest();
            // userQuest.id = id;
            userQuest.questRequesterID = questRequesterID;
            userQuest.handedIn = handedIn;
            if (completed != null)
            {
                userQuest.completed = new BlitableArray<byte>(completed.Length, Allocator.Persistent);
                for (int i = 0; i < userQuest.completed.Length; i++)
                {
                    userQuest.completed[i] = completed[i];
                }
            }
            else
            {
                userQuest.completed = new BlitableArray<byte>();
            }
            return userQuest;
        }
    }
}*/