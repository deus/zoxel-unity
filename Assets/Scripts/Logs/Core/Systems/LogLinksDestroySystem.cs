using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Logs
{
    //! Cleans up LogLinks data in memory.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class LogLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Entities
                .WithAll<DestroyEntity, LogLinks>()
                .ForEach((int entityInQueryIndex, in LogLinks logLinks) =>
			{
                for (int i = 0; i < logLinks.logs.Length; i++)
                {
                    var e = logLinks.logs[i];
                    if (HasComponent<Log>(e))
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e);
                    }
                }
                logLinks.DisposeFinal();
			}).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}