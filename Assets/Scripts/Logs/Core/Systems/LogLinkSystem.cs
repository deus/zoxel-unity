// link any new logs
using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Logs
{
    //! A link event system to link logs after they spawn.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(LogSystemGroup))]
    public partial class LogLinkSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery newLogsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            newLogsQuery = GetEntityQuery(
                ComponentType.ReadOnly<NewLog>(),
                ComponentType.ReadOnly<LogHolderLink>());
            RequireForUpdate(processQuery);
            RequireForUpdate(newLogsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands.RemoveComponent<OnLogsSpawned>(processQuery);
            PostUpdateCommands.RemoveComponent<NewLog>(newLogsQuery);
            var newLogEntities = newLogsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var logHolderLinks = GetComponentLookup<LogHolderLink>(true);
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<OnLogsSpawned>()
                .ForEach((Entity e, ref LogLinks logLinks) =>
            {
                for (int i = 0; i < newLogEntities.Length; i++)
                {
                    var logEntity = newLogEntities[i];
                    var logHolder = logHolderLinks[logEntity].logHolder;
                    if (logHolder == e)
                    {
                        logLinks.Add(logEntity);
                    }
                }
            })  .WithReadOnly(newLogEntities)
                .WithReadOnly(logHolderLinks)
                .WithDisposeOnCompletion(newLogEntities)
                .ScheduleParallel();
            // Debug Logs
            /*Entities
                .WithAll<OnLogsSpawned>()
                .ForEach((in LogLinks logLinks) =>
            {
                // UnityEngine.Debug.LogError("logLinks spawned: " + logLinks.logs.Length);
                for (int i = 0; i < logLinks.logs.Length; i++)
                {
                    var logEntity = logLinks.logs[i];
                    var logText = EntityManager.GetComponentData<LogText>(logEntity).text;
                    UnityEngine.Debug.LogError("    [" + i + "]: " + logText);
                }
            }).WithoutBurst().Run();*/
        }
    }
}