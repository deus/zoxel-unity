using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Logs
{
    //! Cleans up LogText data in memory.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class LogTextDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, LogText>()
                .ForEach((in LogText logText) =>
			{
                logText.DisposeFinal();
			}).ScheduleParallel();
        }
    }
}