using Unity.Entities;

namespace Zoxel.Logs
{
    //! A link to a LogHolder.
    public struct LogHolderLink : IComponentData
    {
        public Entity logHolder;

        public LogHolderLink(Entity logHolder)
        {
            this.logHolder = logHolder;
        }
    }
}