using Unity.Entities;

namespace Zoxel.Logs
{
    //! A tag for a Log entity.
    public struct Log : IComponentData { }
}