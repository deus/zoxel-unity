using Unity.Entities;

namespace Zoxel.Logs
{
    //! A tag for a Log entity.
    public struct LogText : IComponentData
    {
        public Text text;

        public LogText(Text text)
        {
            this.text = text;
        }

        public void DisposeFinal()
        {
            text.DisposeFinal();
        }
    }
}