using Unity.Entities;

namespace Zoxel.Logs
{
    //! A tag for a New Log entity.
    public struct OnLogsSpawned : IComponentData { }
}