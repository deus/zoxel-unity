using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Logs
{
    //! Has links to all the log entities.
    public struct LogLinks : IComponentData
    {
        //! Links an entity to many log entities
        public BlitableArray<Entity> logs;

		public void DisposeFinal()
		{
            logs.DisposeFinal();
		}

		public void Dispose()
		{
            logs.Dispose();
		}

        public void Add(Entity e)
        {
            var newLogs = new BlitableArray<Entity>(logs.Length + 1, Allocator.Persistent, logs);
            newLogs[logs.Length] = e;
            Dispose();
            logs = newLogs;
        }
    }
}