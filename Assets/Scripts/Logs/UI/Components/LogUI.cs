using Unity.Entities;

namespace Zoxel.Logs.UI
{
    //! A tag for the Log's UI.
    public struct LogUI : IComponentData { }
}