/*using Unity.Entities;
using Zoxel.Rendering;
using Zoxel.UI;

namespace Zoxel.Logs.UI
{
    //! Links to the LogUI.
    public struct LogUILink : IComponentData
    {
        public Entity logUI;

        public LogUILink(Entity logUI)
        {
            this.logUI = logUI;
        }

        public void AddToLogUI(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, ref Text tooltipText, double elapsedTime)
        {
            tooltipText.AddChar('\n');
            PostUpdateCommands.AddComponent(entityInQueryIndex, logUI, new AddRenderText(tooltipText));
            PostUpdateCommands.AddComponent(entityInQueryIndex, logUI, new FaderStayVisible(elapsedTime));
        }

        public void AddToLogUI(EntityCommandBuffer PostUpdateCommands, string tooltipText, double elapsedTime)
        {
            PostUpdateCommands.AddComponent(logUI, new AddRenderText(new Text(tooltipText + "\n")));
            PostUpdateCommands.AddComponent(logUI, new FaderStayVisible(elapsedTime));
        }

        public void ClearLog(EntityCommandBuffer PostUpdateCommands)
        {
            PostUpdateCommands.AddComponent(logUI, new SetRenderText(new Text("")));
        }
    }
}*/