using Unity.Entities;

namespace Zoxel.Logs.UI
{
    //! An event for spawning a LogUI.
    public struct SpawnLogUI : IComponentData { }
}