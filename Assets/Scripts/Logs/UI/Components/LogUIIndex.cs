using Unity.Entities;

namespace Zoxel.Logs.UI
{
    //! A tag for the Log's UI.
    public struct LogUIIndex : IComponentData
    {
        public int index;

        public LogUIIndex(int index)
        {
            this.index = index;
        }
    }
}