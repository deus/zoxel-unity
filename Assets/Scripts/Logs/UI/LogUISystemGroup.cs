using Unity.Entities;

namespace Zoxel.Logs.UI
{
    //! Hosts LogUI systems.
    [UpdateAfter(typeof(LogLinkSystem))]
    [UpdateInGroup(typeof(LogSystemGroup))]
    public partial class LogUISystemGroup : ComponentSystemGroup { }
}
