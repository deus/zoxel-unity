using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.UI;

namespace Zoxel.Logs.UI
{
    //! Spawns a LogUI ui!
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(LogUISystemGroup))]
    public partial class LogUISpawnSystem : SystemBase
    {
        public const int maxLogs = 6;
        public const float fadeOutDelay = 4f;
        private const float fadeOutTime = 4f;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity spawnPrefab;
        private Entity panelPrefab;
        private Entity labelPrefab;
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;
        private EntityQuery logsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnLogUI),
                typeof(CharacterLink),
                typeof(GenericEvent),
                typeof(DelayEvent));
            spawnPrefab = EntityManager.CreateEntity(spawnArchetype);
            uiHoldersQuery = GetEntityQuery(
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<UILink>(),
                ComponentType.ReadOnly<LogLinks>());
            logsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Log>(),
                ComponentType.ReadOnly<LogText>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var iconSize = uiDatam.GetIconSize(uiScale);
            var elapsedTime = World.Time.ElapsedTime;
            // prefabs
            var panelPrefab = this.panelPrefab;
            var labelPrefab = this.labelPrefab;
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var characterEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
			var logLinks = GetComponentLookup<LogLinks>(true);
            characterEntities.Dispose();
            var logEntities = logsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var logTexts = GetComponentLookup<LogText>(true);
            logEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<SpawnLogUI>()
                .ForEach((int entityInQueryIndex, in CharacterLink characterLink) =>
            {
                var uiEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, panelPrefab);
                if (characterLink.character.Index == 0)
                {
                    return;
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, uiEntity, characterLink);
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab),
                    new OnUISpawned(characterLink.character, 1));
                if (cameraLinks.HasComponent(characterLink.character))
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, uiEntity, cameraLinks[characterLink.character]);
                }
                if (!logLinks.HasComponent(characterLink.character))
                {
                    return;
                }
                // just spawn the last 10
                var logLinks2 = logLinks[characterLink.character];
                var startIndex = 0;
                if (logLinks2.logs.Length >= LogUISpawnSystem.maxLogs)
                {
                    startIndex = logLinks2.logs.Length - LogUISpawnSystem.maxLogs;
                    // UnityEngine.Debug.LogError("startIndex: " + startIndex + " logLinks2.logs.Length: " + logLinks2.logs.Length);
                }
                var spawnedCount = (byte) 0;
                for (int i = startIndex; i < logLinks2.logs.Length; i++)
                {
                    var logIndex = i;
                    var uiIndex = i - startIndex;
                    // UnityEngine.Debug.LogError("Spawned - logIndex: " + logIndex + " uiIndex: " + uiIndex);
                    LogUISpawnSystem.SpawnLogUIElement(PostUpdateCommands, entityInQueryIndex, uiEntity, labelPrefab,
                        logIndex, uiIndex, elapsedTime, in logLinks2, in logTexts);
                    spawnedCount++;
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, uiEntity, new OnChildrenSpawned(spawnedCount));
                if (startIndex != 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, uiEntity, new LogUIIndex(startIndex));
                }
            })  .WithReadOnly(cameraLinks)
                .WithReadOnly(logLinks)
                .WithReadOnly(logTexts)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static void SpawnLogUIElement(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity uiEntity,
            Entity labelPrefab, int logIndex, int uiIndex, double elapsedTime, in LogLinks logLinks2, in ComponentLookup<LogText> logTexts)
        {
            var seed = (int) (128 + elapsedTime + logIndex * 2048 + 4196 * entityInQueryIndex);
            var logEntity = logLinks2.logs[logIndex];
            var logText = logTexts[logEntity].text.Clone();
            var e4 = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, labelPrefab, uiEntity);
            UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, e4, uiIndex);
            UICoreSystem.SetSeed(PostUpdateCommands, entityInQueryIndex, e4, seed);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e4, new RenderText(logText));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e4, new FadeDelay(elapsedTime, fadeOutDelay));
        }

        private void InitializePrefabs()
        {
            if (panelPrefab.Index != 0)
            {
                return;
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var iconSize = uiDatam.GetIconSize(uiScale);
            var buttonStyle = uiDatam.mainMenuButtonStyle.GetStyle(uiScale);
            var padding = uiDatam.GetIconPadding(uiScale);
            panelPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(panelPrefab);
            EntityManager.AddComponent<LogUI>(panelPrefab);
            EntityManager.AddComponent<LogUIIndex>(panelPrefab);
            EntityManager.AddComponent<GameOnlyUI>(panelPrefab);
            EntityManager.SetComponentData(panelPrefab, new PanelUI(PanelType.LogUI));
            EntityManager.SetComponentData(panelPrefab, new UIAnchor(AnchorUIType.BottomLeft));
            // EntityManager.SetComponentData(panelPrefab, new Size2D(new float2(iconSize.x * 10, iconSize.y * 8)));
            var fontSize = buttonStyle.fontSize / 1.7f;
            var textPadding = buttonStyle.textPadding / 3f;
            // decrease padding for grid ui
            var gridUI = new GridUI(0.5f * padding, 0.5f * new float2(fontSize, fontSize));
            EntityManager.SetComponentData(panelPrefab, gridUI);
            EntityManager.SetComponentData(panelPrefab, new GridUISize(new float2(fontSize * 16f + textPadding.x * 2f, fontSize + textPadding.y * 2f)));
            EntityManager.RemoveComponent<SpawnHeaderUI>(panelPrefab);
            EntityManager.RemoveComponent<NavigationDirty>(panelPrefab);
            EntityManager.AddComponent<AutoGridY>(panelPrefab);
            // EntityManager.AddComponent<DisableGridResize>(panelPrefab);
            // hide background
            EntityManager.SetComponentData(panelPrefab, new MaterialBaseColor { Value = new float4() });
            EntityManager.SetComponentData(panelPrefab, new MaterialFrameColor { Value = new float4() });
            EntityManager.AddComponentData(panelPrefab, new GridUIStackType(GridPositionType.StackUp));
            // label
            labelPrefab = EntityManager.Instantiate(UICoreSystem.labelPrefab);
            EntityManager.AddComponent<Prefab>(labelPrefab);
            UICoreSystem.SetRenderTextData(EntityManager, labelPrefab,
                buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor,
                fontSize, textPadding);
            //EntityManager.AddComponent<LogLabel>(labelPrefab);
            EntityManager.AddComponentData(panelPrefab, new ListPrefabLink(labelPrefab));
            EntityManager.AddComponent<FadeOut>(labelPrefab);
            EntityManager.AddComponent<FadeDelay>(labelPrefab);
            EntityManager.AddComponentData(labelPrefab, new Fader(1f, 0, 1f, fadeOutTime));
            // fade out after creation
            // organize grid from bottom up, instead of centering
        }
    }
}