using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Rendering;
using Zoxel.UI;
using Zoxel.Transforms;

namespace Zoxel.Logs.UI
{
    //! Spawns new LogUIElements after data is added to an entity.
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(LogSystemGroup))]
    public partial class LogUIAddedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery logUIsQuery;
        private EntityQuery logsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            logUIsQuery = GetEntityQuery(
                ComponentType.ReadOnly<LogUI>(),
                ComponentType.ReadOnly<LogText>());
            logsQuery = GetEntityQuery(
                ComponentType.ReadOnly<NewLog>(),
                ComponentType.ReadOnly<LogText>());
            RequireForUpdate(processQuery);
            RequireForUpdate(logsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var fadeOutDelay = LogUISpawnSystem.fadeOutDelay;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var logUIEntities = logUIsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var logUIs = GetComponentLookup<LogUI>(true);
			var logUIChildrens = GetComponentLookup<Childrens>(true);
			var listPrefabLinks = GetComponentLookup<ListPrefabLink>(true);
			var logUIIndexes = GetComponentLookup<LogUIIndex>(false);
            logUIEntities.Dispose();
            var logEntities = logsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var logTexts = GetComponentLookup<LogText>(true);
            logEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<OnLogsSpawned>()
                .ForEach((int entityInQueryIndex, in LogLinks logLinks, in UILink uiLink) =>
            {
                // Find LogUI Entity
                var logUIEntity = new Entity();
                for (int i = 0; i < uiLink.uis.Length; i++)
                {
                    var uiEntity = uiLink.uis[i];
                    if (logUIs.HasComponent(uiEntity))
                    {
                        // add to list the log text
                        logUIEntity = uiEntity;
                        break;
                    }
                }
                if (logUIEntity.Index == 0)
                {
                    return;
                }
                // spawn new elements in list!
                var oldLogIndex = logUIIndexes[logUIEntity].index;
                var logUIChildrens2 = logUIChildrens[logUIEntity];
                var labelPrefab = listPrefabLinks[logUIEntity].prefab;
                var newLogIndex = 0;
                if (logLinks.logs.Length >= LogUISpawnSystem.maxLogs)
                {
                    newLogIndex = logLinks.logs.Length - LogUISpawnSystem.maxLogs;
                }
                var deleteCount = (byte) 0;
                if (newLogIndex != oldLogIndex)
                {
                    deleteCount = (byte) (newLogIndex - oldLogIndex);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, logUIEntity, new DestroyChildrenFromStart(deleteCount));
                    logUIIndexes[logUIEntity] = new LogUIIndex(newLogIndex);
                    /*UnityEngine.Debug.LogError("Destroying: " + deleteCount + " newLogIndex: " + newLogIndex + " oldLogIndex: " + oldLogIndex
                        + " logUIChildrens2.children.Length: " + logUIChildrens2.children.Length
                        + " logLinks2.logs.Length: " + logLinks.logs.Length);*/
                }
                // keep only 10 of these, delete others
                var spawnedCount = (byte) 0;
                var uiIndexStart = (logUIChildrens2.children.Length - deleteCount);
                for (int i = newLogIndex + uiIndexStart; i < logLinks.logs.Length; i++)
                {
                    var logIndex = i;
                    var uiIndex = uiIndexStart + spawnedCount;
                    // UnityEngine.Debug.LogError("Added - logIndex: " + logIndex + " uiIndex: " + uiIndex + " uiIndexStart: " + uiIndexStart);
                    LogUISpawnSystem.SpawnLogUIElement(PostUpdateCommands, entityInQueryIndex, logUIEntity, labelPrefab,
                        logIndex, uiIndex, elapsedTime, in logLinks, in logTexts);
                    spawnedCount++;
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, logUIEntity, new OnChildrenSpawned(spawnedCount));
                PostUpdateCommands.AddComponent<GridUIDirty>(entityInQueryIndex, logUIEntity);
            })  .WithReadOnly(logUIs)
                .WithReadOnly(logUIChildrens)
                .WithReadOnly(listPrefabLinks)
                .WithReadOnly(logTexts)
                .WithNativeDisableContainerSafetyRestriction(logUIIndexes)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}