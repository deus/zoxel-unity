using Unity.Entities;

namespace Zoxel.Logs
{
    //! Hosts Log systems.
    public partial class LogSystemGroup : ComponentSystemGroup
    {
        public static Entity logPrefab;

        protected override void OnCreate()
        {
            base.OnCreate();
            var logArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(NewLog),
                typeof(Log),
                typeof(LogText),
                typeof(LogHolderLink));
            logPrefab = EntityManager.CreateEntity(logArchetype);
        }
    }
}