using Unity.Entities;

namespace Zoxel.Achievements
{
    //! Increases an achievements value count by 1.
    /**
    *   An example of use, you destroy a voxel. It increments Destroy 1000 voxels achievement by 1.
    */
    public struct IncrementAchievement : IComponentData { }
}