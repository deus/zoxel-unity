using Unity.Entities;

namespace Zoxel.Achievements
{
    //! A tag for a achievement entity.
    public struct Achievement : IComponentData { }
}