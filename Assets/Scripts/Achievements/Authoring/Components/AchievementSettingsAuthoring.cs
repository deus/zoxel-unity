using Unity.Entities;
using UnityEngine;

namespace Zoxel.Achievements.Authoring
{
    //! A data setting test.
    // [GenerateAuthoringComponent]
    public struct AchievementSettings : IComponentData
    {
        public int huntingAchievements;
        public int levelUpAchievements;
    }

    public class AchievementSettingsAuthoring : MonoBehaviour
    {
        public int huntingAchievements;
        public int levelUpAchievements;
    }

    public class AchievementSettingsAuthoringBaker : Baker<AchievementSettingsAuthoring>
    {
        public override void Bake(AchievementSettingsAuthoring authoring)
        {
            AddComponent(new AchievementSettings
            {
                huntingAchievements = authoring.huntingAchievements,
                levelUpAchievements = authoring.levelUpAchievements
            });       
        }
    }

    /*// [GenerateAuthoringComponent]
    public struct MaterialTest : ISharedComponentData, IEquatable<MaterialTest>
    {
        public UnityEngine.Material material;
        
        public bool Equals(UnityMaterial other)
        {
            return material == other.material;
        }
        
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                if (ReferenceEquals(material, null))
                {
                    return 0;
                }
                else
                {
                    return material.GetHashCode();
                }
            }
        }
    }*/
}