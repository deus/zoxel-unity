using Unity.Entities;
using Unity.Burst;
using Zoxel.Items;

namespace Zoxel.Actions
{
    //! Disposes of HeldActionItemLink.
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class HeldItemDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyEntity, HeldActionItemLink>()
                .ForEach((int entityInQueryIndex, in HeldActionItemLink heldActionItemLink) =>
            {
                if (heldActionItemLink.item.Index > 0)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, heldActionItemLink.item);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}