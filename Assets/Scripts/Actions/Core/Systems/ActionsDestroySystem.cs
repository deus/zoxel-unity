using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Actions
{
    //! Cleans up UserActionLinks data in memory.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ActionsDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, UserActionLinks>()
                .ForEach((in UserActionLinks userActionLinks) =>
			{
                userActionLinks.DisposeFinal();
			}).ScheduleParallel();
        }
    }
}