using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Actions
{
    //! This system ends some one frame events.
    /** 
    *   - End System -
    *   They include: ActivateAction, ActivatedAction, ActivateWorldAction, CancelWorldAction.
    *   Todo: Support multiple Activation events for userActionLinks - so I can do hotkeys etc.
    *   This would involve a separate entity for the action events.
    */
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class ActivatingActionEndSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<ActivateAction>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<ActivateAction>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            var elapsedTime = World.Time.ElapsedTime;
            Dependency = Entities
                .ForEach((Entity e, int entityInQueryIndex, in ActivatedAction activatedAction) =>
            {
                PostUpdateCommands.RemoveComponent<ActivatedAction>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<ActivateWorldAction>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<ActivateWorldAction>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<CancelWorldAction>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<CancelWorldAction>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}