namespace Zoxel.Actions
{
    public static class ActionType
    {
        public const byte None = 0;
        public const byte Skill = 1;
        public const byte Item = 2;
        public const byte Emoticon = 3;
    }
}
    /*public enum ActionType : byte
    {
        None,
        Skill,
        Item,
        Emoticon
    }*/
