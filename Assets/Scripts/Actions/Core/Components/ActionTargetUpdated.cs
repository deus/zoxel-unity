using Unity.Entities;

namespace Zoxel.Actions
{
    //! Update an ActionUI after action has updated.
    public struct ActionTargetUpdated : IComponentData
    {
        public Entity target;
        public Entity targetMeta;
        public byte isZeroQuantity;

        public ActionTargetUpdated(Entity target, Entity targetMeta, byte isZeroQuantity = 0)
        {
            this.target = target;
            this.targetMeta = targetMeta;
            this.isZeroQuantity = isZeroQuantity;
        }
    }
}