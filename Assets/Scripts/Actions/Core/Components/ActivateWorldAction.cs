using Unity.Entities;

namespace Zoxel.Actions
{
    //! A different type of Action to activate, when interacting with doors and chests.
    public struct ActivateWorldAction : IComponentData { }
}