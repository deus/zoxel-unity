using Unity.Entities;

namespace Zoxel.Actions
{
    //! Generates User UserActionLinks after userSkillLinks have been added to a new character.
    public struct GenerateUserActions : IComponentData { }
}