using Unity.Entities;

namespace Zoxel.Actions
{
    //! An event that sends data from the ActivateAction trigger to the OverlayUI's.
    public struct ActivatedAction : IComponentData
    {
        public int index;
        public double timeStarted;
        public float timeLengthGrow;
        public float timeLengthShrink;

        public ActivatedAction(int index, double timeStarted, float timeLengthGrow, float timeLengthShrink)
        {
            this.index = index;
            this.timeStarted = timeStarted;
            this.timeLengthGrow = timeLengthGrow;
            this.timeLengthShrink = timeLengthShrink;
        }
    }
}