using Unity.Entities;

namespace Zoxel.Actions
{
    //! When a UserSkill or UserItem updates it will trigger an event to update.
    /**
    *   \todo Make external for future scaling. To support many things updating at once.
    */
    public struct ActionTargetQuantityUpdated : IComponentData
    {
        public Entity target;
        public Entity targetMeta;

        public ActionTargetQuantityUpdated(Entity target, Entity targetMeta)
        {
            this.target = target;
            this.targetMeta = targetMeta;
        }
    }
}