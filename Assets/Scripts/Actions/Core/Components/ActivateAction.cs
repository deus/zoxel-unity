using Unity.Entities;

namespace Zoxel.Actions
{
    //! When input of the player is done, this triggers userSkillLinks to activate.
    public struct ActivateAction : IComponentData { }
}