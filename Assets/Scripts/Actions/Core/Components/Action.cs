using Unity.Entities;

namespace Zoxel.Actions
{
    //! An action contains links to the target user data.
    public struct Action : IComponentData
    {
        public Entity target;       //! The target UserSkill or UserItem entity.
        public Entity targetMeta;   //! The target Skill or Item entity data.

        public Action(Entity target, Entity targetMeta)
        {
            this.target = target;
            this.targetMeta = targetMeta;
        }
    }
}