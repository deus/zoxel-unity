using Unity.Entities;

namespace Zoxel.Actions
{
    //! A character that presses the 'B' button!
    public struct CancelWorldAction : IComponentData { }
}