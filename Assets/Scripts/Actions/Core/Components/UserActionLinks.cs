﻿using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Actions
{
    //! A bunch of actions that a user can use.
    /**
    *   \todo Rename to UserActionLinks
    */
    public struct UserActionLinks : IComponentData
    {
        //! An array of data that links to other user data.
        public BlitableArray<Action> actions;
        //! Selected Action index to array.
        public byte selectedAction;
        //! The last selectedAction index, used during pausing.
        public byte lastSelectedAction;
        //! Used to remember what skill was added last time for components.
        public byte skillType;

        //! Disposes of actions array.
        public void Dispose()
        {
            actions.Dispose();
        }

        //! Disposes of actions array.
        public void DisposeFinal()
        {
            actions.DisposeFinal();
        }

        //! Initializes of actions array.
        public void InitializeActions(int count)
        {
            if (actions.Length != count)
            {
                Dispose();
                actions = new BlitableArray<Action>(count, Allocator.Persistent);
                for (int j = 0; j < actions.Length; j++)
                {
                    actions[j] = new Action();
                }
            }
        }

        //! Gets the target entity of selectedAction's Action. Either UserItem or UserSkill.
        public Entity GetSelectedTarget()
        {
            if (selectedAction >= 0 && selectedAction < actions.Length)
            {
                return actions[selectedAction].target;
            }
            return new Entity();
        }
    }
}