using Unity.Entities;

namespace Zoxel.Actions
{
    //! After death, clears all the item userActionLinks in the user's UserActionLinks.
    public struct ClearItemActions : IComponentData { }
}