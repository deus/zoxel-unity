using Unity.Entities;

namespace Zoxel.Actions
{
    //! When a user updates their action selected.
    public struct SelectedActionUpdated : IComponentData
    {
        public byte selected;

        public SelectedActionUpdated(byte selected)
        {
            this.selected = selected;
        }
    }
}