using Unity.Entities;

namespace Zoxel.Actions
{
    //! Holds a link to the item held in the right hand based on action selected.
    public struct HeldActionItemLink : IComponentData
    {
        public Entity item;

        public HeldActionItemLink(Entity item)
        {
            this.item = item;
        }
    }
}