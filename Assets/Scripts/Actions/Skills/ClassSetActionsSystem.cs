using Unity.Entities;
using Unity.Burst;
using Zoxel.Classes;

namespace Zoxel.Actions
{
    //! Handles Class changes on a character.
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class ClassSetActionsSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithChangeFilter<ClassLink>()
				.WithNone<DestroyEntity, InitializeEntity>()
				.ForEach((Entity e, int entityInQueryIndex) =>
			{
                PostUpdateCommands.AddComponent<GenerateUserActions>(entityInQueryIndex, e);
                // UnityEngine.Debug.LogError("ClassSetActionsSystem GenerateUserActions: " + e.Index);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}