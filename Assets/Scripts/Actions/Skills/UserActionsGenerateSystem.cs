using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Skills;

namespace Zoxel.Actions
{
    //! Generates new userSkillLinks for a character.
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class UserActionsGenerateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userSkillsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userSkillsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserSkill>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<GenerateUserActions>(processQuery);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userSkillEntities = userSkillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var userSkillMetaDatas = GetComponentLookup<MetaData>(true);
            userSkillEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, DeadEntity, OnUserSkillSpawned>()
                .WithAll<GenerateUserActions>()
                .ForEach((Entity e, int entityInQueryIndex, ref UserActionLinks userActionLinks, in UserSkillLinks userSkillLinks) =>
            {
                // UnityEngine.Debug.LogError("GenerateUserActions " + userSkillLinks.skills.Length);
                userActionLinks.InitializeActions(10);
                for (int i = 0; i < userActionLinks.actions.Length; i++)
                {
                    if (i < userSkillLinks.skills.Length)
                    {
                        // UnityEngine.Debug.LogError("Skill " + i);
                        var userSkillEntity = userSkillLinks.skills[i];
                        var targetMeta = new Entity();
                        if (userSkillMetaDatas.HasComponent(userSkillEntity))
                        {
                            targetMeta = userSkillMetaDatas[userSkillEntity].data;
                            if (HasComponent<Skill>(targetMeta))
                            {
                                userActionLinks.actions[i] = new Action(userSkillEntity, targetMeta);
                                // UnityEngine.Debug.LogError("Skill Action " + i);
                            }
                        }
                    }
                }
                if (!HasComponent<ViewerObject>(e))
                {
                    PostUpdateCommands.AddComponent<SelectedActionUpdated>(entityInQueryIndex, e);
                }
                PostUpdateCommands.AddComponent<SaveActions>(entityInQueryIndex, e);
            })  .WithReadOnly(userSkillMetaDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}