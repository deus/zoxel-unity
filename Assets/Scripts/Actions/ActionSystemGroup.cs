using Unity.Entities;

namespace Zoxel.Actions
{
    //! Hosts action systems.
    public partial class ActionSystemGroup : ComponentSystemGroup { }
}
