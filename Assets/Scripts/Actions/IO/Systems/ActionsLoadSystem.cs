using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Skills;
using Zoxel.Items;

namespace Zoxel.Actions
{
    //! Loads a user's UserActionLinks from file.
    /**
    *   - Load System -
    */
    [UpdateInGroup(typeof(ActionIOSystemGroup))]
    public partial class ActionsLoadSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userSkillsQuery;
        private EntityQuery skillsQuery;
        private EntityQuery userItemsQuery;
        private EntityQuery itemsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userSkillsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserSkill>());
            skillsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Skill>(),
                ComponentType.ReadOnly<ZoxID>());
            userItemsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserItem>(),
                ComponentType.ReadOnly<MetaData>());
            itemsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Item>(),
                ComponentType.ReadOnly<ZoxID>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithNone<LoadSkills, OnUserSkillSpawned>()
                .WithNone<LoadInventory, OnUserItemsSpawned>()
                .WithAll<UserSkillLinks, Inventory>()
                .ForEach((ref LoadActions loadActions, ref UserActionLinks userActionLinks, in EntitySaver entitySaver) =>
            {
                if (loadActions.loadPath.Length == 0)
                {
                    loadActions.loadPath = new Text(entitySaver.GetPath() + ActionIOSystemGroup.filename);
                }
                loadActions.reader.UpdateOnMainThread(in loadActions.loadPath);
            }).WithoutBurst().Run();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userSkillEntities = userSkillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var userSkills = GetComponentLookup<MetaData>(true);
            userSkillEntities.Dispose();
            var skillEntities = skillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var skillIDs = GetComponentLookup<ZoxID>(true);
            skillEntities.Dispose();
            var userItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var userItems = GetComponentLookup<MetaData>(true);
            userItemEntities.Dispose();
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var itemIDs = GetComponentLookup<ZoxID>(true);
            itemEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithNone<LoadSkills, OnUserSkillSpawned>()
                .WithNone<LoadInventory, OnUserItemsSpawned>()
                .WithAll<EntitySaver>()
                .ForEach((Entity e, int entityInQueryIndex, ref LoadActions loadActions, ref UserActionLinks userActionLinks, in UserSkillLinks userSkillLinks, in Inventory inventory) =>
            {
                if (loadActions.reader.IsReadFileInfoComplete())
                {
                    if (loadActions.reader.DoesFileExist())
                    {
                        loadActions.reader.state = AsyncReadState.StartOpenFile;
                    }
                    else
                    {
                        // UnityEngine.Debug.LogError("File Did not exist.");
                        userActionLinks.InitializeActions(10);
                        loadActions.Dispose();
                        PostUpdateCommands.RemoveComponent<LoadActions>(entityInQueryIndex, e);
                    }
                }
                else if (loadActions.reader.IsReadFileComplete())
                {
                    var count = 10;
                    userActionLinks.InitializeActions(count);
                    if (loadActions.reader.DidFileReadSuccessfully())
                    {
                        var bytes = loadActions.reader.GetBytes();
                        userActionLinks.selectedAction = bytes[0];
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SelectedActionUpdated(userActionLinks.selectedAction));
                        /*var userEntities = new NativeArray<Entity>(count, Allocator.Temp);
                        PostUpdateCommands.Instantiate(entityInQueryIndex, userPrefab, userEntities);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, userEntities, new CreatorLink(e));*/
                        for (int i = 0; i < count; i++)
                        {
                            var actionID = ByteUtil.GetInt(in bytes, 1 + i * 5);
                            var actionType = bytes[1 + i * 5 + 4];
                            var target = new Entity();
                            var targetMeta = new Entity();
                            if (actionType == ActionType.Item)
                            {
                                // UnityEngine.Debug.LogError("Loading Item: " + actionID);
                                for (int j = 0; j < inventory.items.Length; j++)
                                {
                                    var userItemEntity = inventory.items[j];
                                    var itemEntity = userItems[userItemEntity].data;
                                    if (itemIDs.HasComponent(itemEntity))
                                    {
                                        var itemID = itemIDs[itemEntity].id;
                                        if (actionID == itemID)
                                        {
                                            target = userItemEntity;
                                            targetMeta = itemEntity;
                                            break;
                                        }
                                    }
                                }
                                /*if (targetMeta.Index == 0)
                                {
                                    UnityEngine.Debug.LogError("Failed to Find Item: " + actionID);
                                }*/
                            }
                            else if (actionType == ActionType.Skill)
                            {
                                // UnityEngine.Debug.LogError("Loading Skill: " + actionID);
                                for (int j = 0; j < userSkillLinks.skills.Length; j++)
                                {
                                    var userItemEntity = userSkillLinks.skills[j];
                                    var skillEntity = userSkills[userItemEntity].data;
                                    if (skillIDs.HasComponent(skillEntity))
                                    {
                                        var skillID = skillIDs[skillEntity].id;
                                        if (actionID == skillID)
                                        {
                                            target = userItemEntity;
                                            targetMeta = skillEntity;
                                            break;
                                        }
                                    }
                                }
                                /*if (targetMeta.Index == 0)
                                {
                                    UnityEngine.Debug.LogError("Failed to Find Skill: " + actionID);
                                }*/
                            }
                            userActionLinks.actions[i] = new Action(target, targetMeta);
                        }
                    }
                    // Dispose
                    loadActions.Dispose();
                    PostUpdateCommands.RemoveComponent<LoadActions>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(userSkills)
                .WithReadOnly(skillIDs)
                .WithReadOnly(userItems)
                .WithReadOnly(itemIDs)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}