using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using System.IO;
using Zoxel.Skills;
using Zoxel.Items;

namespace Zoxel.Actions
{
    //! Saves a user's UserActionLinks to a bytes.
    /**
    *   - Save System -
    */
    [UpdateInGroup(typeof(ActionIOSystemGroup))]
    public partial class ActionsSaveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userSkillsQuery;
        private EntityQuery skillsQuery;
        private EntityQuery userItemsQuery;
        private EntityQuery itemsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userSkillsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserSkill>(),
                ComponentType.ReadOnly<MetaData>(),
                ComponentType.Exclude<DestroyEntity>());
            skillsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Skill>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<DestroyEntity>());
            userItemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserItem>(),
                ComponentType.ReadOnly<MetaData>(),
                ComponentType.Exclude<DestroyEntity>());
            itemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Item>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(userSkillsQuery);
            RequireForUpdate(skillsQuery);
            RequireForUpdate(userItemsQuery);
            RequireForUpdate(itemsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands.RemoveComponent<SaveActions>(processQuery);
            var userSkillEntities = userSkillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var userSkills = GetComponentLookup<UserSkill>(true);
            var userSkillMetaDatas = GetComponentLookup<MetaData>(true);
            userSkillEntities.Dispose();
            var skillEntities = skillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var skillIDs = GetComponentLookup<ZoxID>(true);
            skillEntities.Dispose();
            var userItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var userItems = GetComponentLookup<UserItem>(true);
            var userItemMetaDatas = GetComponentLookup<MetaData>(true);
            userItemEntities.Dispose();
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var itemIDs = GetComponentLookup<ZoxID>(true);
            itemEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<LoadActions, GenerateUserActions>()
                .WithNone<InitializeSaver, DisableSaving>()
                .WithAll<EntitySaver>()
                .ForEach((ref SaveActions saveActions, in UserActionLinks userActionLinks) =>
            {
                saveActions.bytes = new BlitableArray<byte>(userActionLinks.actions.Length * 5 + 1, Allocator.Persistent);
                saveActions.bytes[0] = userActionLinks.selectedAction;
                for (int i = 0; i < userActionLinks.actions.Length; i++)
                {
                    var target = userActionLinks.actions[i].target;
                    if (target.Index <= 0)
                    {
                        ByteUtil.SetInt(ref saveActions.bytes, 1 + i * 5, 0);
                        saveActions.bytes[(1 + i * 5 + 4)] = 0;
                        continue;
                    }
                    var id = 0;
                    var actionType = ActionType.None;
                    if (userSkills.HasComponent(target))
                    {
                        var skillEntity = userSkillMetaDatas[target].data;
                        if (skillIDs.HasComponent(skillEntity))
                        {
                            actionType = ActionType.Skill;
                            id = skillIDs[skillEntity].id;
                            // UnityEngine.Debug.LogError("Saving Skill: " + id);
                        }
                    }
                    else if (userItems.HasComponent(target))
                    {
                        var itemEntity = userItemMetaDatas[target].data;
                        if (itemIDs.HasComponent(itemEntity))
                        {
                            actionType = ActionType.Item;
                            id = itemIDs[itemEntity].id;
                            // UnityEngine.Debug.LogError("Saving Item: " + id);
                        }
                    }
                    ByteUtil.SetInt(ref saveActions.bytes, 1 + i * 5, id);
                    saveActions.bytes[(1 + i * 5 + 4)] = actionType;
                }
            })  .WithReadOnly(userSkills)
                .WithReadOnly(userSkillMetaDatas)
                .WithReadOnly(skillIDs)
                .WithReadOnly(userItems)
                .WithReadOnly(userItemMetaDatas)
                .WithReadOnly(itemIDs)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<LoadActions, GenerateUserActions>()
                .WithNone<InitializeSaver, DisableSaving>()
                .WithAll<UserActionLinks>()
                .ForEach((ref SaveActions saveActions, in EntitySaver entitySaver) =>
            {
                var filepath = entitySaver.GetPath() + ActionIOSystemGroup.filename;
                var bytesArray = saveActions.bytes.ToArray();
                #if WRITE_ASYNC
                File.WriteAllBytesAsync(filepath, bytesArray);
                #else
                File.WriteAllBytes(filepath, bytesArray);
                #endif
            }).WithoutBurst().Run();
        }
    }
}