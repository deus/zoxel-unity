using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Actions
{
    //! Removes SaveActions after use if DisableSaving is on entity.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(ActionIOSystemGroup))]
    public partial class ActionsDisableSavingEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<DisableSaving>(),
                ComponentType.ReadOnly<SaveActions>(),
                ComponentType.ReadOnly<UserActionLinks>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<SaveActions>(processQuery);
        }
    }
}