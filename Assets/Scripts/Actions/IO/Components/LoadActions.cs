using Unity.Entities;

namespace Zoxel.Actions
{
    //! Loads user UserActionLinks from file.
    public struct LoadActions : IComponentData
    {
        public Text loadPath;
        public AsyncFileReader reader;

        public void Dispose()
        {
            loadPath.Dispose();
            reader.Dispose();
        }
    }
}