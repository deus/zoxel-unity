using Unity.Entities;

namespace Zoxel.Actions
{
    //! Saves user UserActionLinks to file.
    public struct SaveActions : IComponentData
    {
        public BlitableArray<byte> bytes;

        public void Dispose()
        {
            bytes.Dispose();
        }
    }
}