using Unity.Entities;

namespace Zoxel.Actions
{
    //! Hosts action systems.
    [UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class ActionIOSystemGroup : ComponentSystemGroup
    {
        public const string filename = "Actions.zox";
    }
}
