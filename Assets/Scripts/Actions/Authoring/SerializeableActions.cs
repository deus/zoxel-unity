using System;

namespace Zoxel.Actions
{
    [Serializable]
    public struct SerializeableActions
    {
        public int selectedAction;            // selectedAction index
        public double lastCasted;
        public int[] actionIDs;
        public byte[] actionTypes;
    }
}