using Unity.Entities;

namespace Zoxel.Actions.UI
{
    public struct InitializeActionButton : IComponentData
    {
        public Action action;

        public InitializeActionButton(Action action)
        {
            this.action = action;
        }
    }
}