using Unity.Entities;

namespace Zoxel.Actions.UI
{
    //! Attached to an ActionUI's overlay when it is animating.
    public struct AnimatingActionOverlayUI : IComponentData
    {
        public double timeStarted;
        public double timeLengthGrow;
        public double timeLengthShrink; 

        public AnimatingActionOverlayUI(double timeStarted, double timeLengthGrow, double timeLengthShrink)
        {
            this.timeStarted = timeStarted;
            this.timeLengthGrow = timeLengthGrow;
            this.timeLengthShrink = timeLengthShrink;
        }
    }
}