using Unity.Entities;

namespace Zoxel.Actions.UI
{
    //! The overlay visual on top of every ActionUI.
    public struct ActionOverlayUI : IComponentData
    {
        public Entity character;
        public int actionIndex; 

        public ActionOverlayUI(Entity character, int actionIndex)
        {
            this.character = character;
            this.actionIndex = actionIndex;
        }
    }
}