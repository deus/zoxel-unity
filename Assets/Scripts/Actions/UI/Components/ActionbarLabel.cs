using Unity.Entities;

namespace Zoxel.Actions.UI
{
    //! A UIElement that show's the name of the selected ActionUI.
    public struct ActionbarLabel : IComponentData { }
}