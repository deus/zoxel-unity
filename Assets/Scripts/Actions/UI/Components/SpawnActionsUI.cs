using Unity.Entities;

namespace Zoxel.Actions.UI
{
    //! Spawns a bunch of ActionUI's for an Actionbar.
    public struct SpawnActionsUI : IComponentData { }
}