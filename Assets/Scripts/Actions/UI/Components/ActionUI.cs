using Unity.Entities;

namespace Zoxel.Actions.UI
{
    //! The tag for the ActionUI element.
    public struct ActionUI : IComponentData { }
}