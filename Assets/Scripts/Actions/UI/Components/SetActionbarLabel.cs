using Unity.Entities;

namespace Zoxel.Actions.UI
{
    public struct SetActionbarLabel : IComponentData
    {
        public Text text;
        
        public SetActionbarLabel(Text text)
        {
            this.text = text;
        }
    }
}