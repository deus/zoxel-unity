using Unity.Entities;

namespace Zoxel.Actions.UI
{
    //! An event for setting a new ActionUI's Action.
    public struct SetActionIcon : IComponentData
    {
        public Action action;

        public SetActionIcon(Action action)
        {
            this.action = action;
        }
    }
}