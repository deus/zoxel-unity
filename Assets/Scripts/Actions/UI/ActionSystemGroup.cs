using Unity.Entities;

namespace Zoxel.Actions.UI
{
    //! Hosts action systems.
    [UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class ActionUISystemGroup : ComponentSystemGroup { }
}
