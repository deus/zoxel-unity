using Unity.Entities;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Items;

namespace Zoxel.Actions.UI
{
    //! Updates just action text when it updates.
    /**
    *   \todo Make this use UpdateIconLabel component event.
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(ActionUISystemGroup))]
    public partial class ActionUITextUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((Entity e, in UserActionLinks userActionLinks, in Inventory inventory, in UILink uiLink, in ActionTargetQuantityUpdated actionTargetQuantityUpdated) =>
            {
                var actionbar = uiLink.GetUI<Actionbar>(EntityManager);
                if (actionbar.Index != 0)
                {
                    var childrens = EntityManager.GetComponentData<Childrens>(actionbar);
                    for (int i = 0; i < childrens.children.Length; i++)
                    {
                        if (HasComponent<InitializeActionButton>(childrens.children[i]))
                        {
                            return;
                        }
                    }
                    if (childrens.children.Length == 0)
                    {
                        // UnityEngine.Debug.LogError("Action UIs 0: " + childrens.children.Length);
                        return;
                    }
                    var updatedTarget = new Entity();
                    if (HasComponent<Item>(actionTargetQuantityUpdated.targetMeta))
                    {
                        updatedTarget = actionTargetQuantityUpdated.targetMeta;
                    }
                    // UnityEngine.Debug.LogError("Text Updated with ID: " + updatedTargetID);
                    for (int i = 0; i < userActionLinks.actions.Length; i++)
                    {
                        var action = userActionLinks.actions[i];
                        var target = action.target;
                        var targetMeta = action.targetMeta;
                        if (targetMeta == updatedTarget)
                        {
                            UpdateActionUIText(PostUpdateCommands, e, action, i, in childrens, in inventory);
                        }
                    }
                }
                PostUpdateCommands.RemoveComponent<ActionTargetQuantityUpdated>(e);
            }).WithoutBurst().Run();
        }

        private void UpdateActionUIText(EntityCommandBuffer PostUpdateCommands, Entity e, Action action, int actionIndex,
            in Childrens childrens, in Inventory inventory)
        {
            if (actionIndex < 0 || actionIndex >= childrens.children.Length)
            {
                UnityEngine.Debug.LogError("Action out of bounds: " + actionIndex + " bounds: " + childrens.children.Length);
                return;
            }
            var frameEntity = childrens.children[actionIndex];
            var frameChildren = EntityManager.GetComponentData<Childrens>(frameEntity);
            if (frameChildren.children.Length >= 2)
            {
                var renderTextEntity = frameChildren.children[1];
                // destroy previous frame Texture, and icon Texture
                var subText = "";
                var target = action.target;
                var targetMeta = action.targetMeta;
                if (HasComponent<UserItem>(target))
                {
                    var quantity = inventory.GetItemQuantity(EntityManager, targetMeta);
                    subText = ((int)quantity).ToString();
                    if (quantity == 0 || quantity == 1)
                    {
                        subText = "";
                    }
                }
                PostUpdateCommands.AddComponent(renderTextEntity, new SetRenderText(new Text(subText)));
                // UnityEngine.Debug.LogError("(2) Updating Text to: " + subText + " of " + renderTextEntity.Index);
            }
        }
    }
}

/*var renderText = EntityManager.GetComponentData<RenderText>(renderTextEntity);
if (renderText.SetText(subText))
{
    PostUpdateCommands.AddComponent<RenderTextDirty>(renderTextEntity);
    PostUpdateCommands.SetComponent(renderTextEntity, renderText);
}*/