using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Actions.UI
{
    //! Animates an overlay over ActionUIButton.
    /**
    *   First it grows, then it shrinks based on animation and cooldown of skill.
    *   Todo: Perhaps I can lerp colours too.
    */
    [BurstCompile, UpdateInGroup(typeof(ActionUISystemGroup))]
    public partial class ActionOverlayAnimationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var halfPI = math.PI / 2f;
            var oneFloat3 = new float3(1,1,1);
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref LocalScale localScale, in AnimatingActionOverlayUI animatingActionOverlayUI) =>
            {
                var timePassed = elapsedTime - animatingActionOverlayUI.timeStarted;
                var timeLengthGrow = animatingActionOverlayUI.timeLengthGrow;
                if (timePassed <= timeLengthGrow)
                {
                    var timeLength = timeLengthGrow;
                    var lerp = (float) (math.sin(halfPI * (timePassed / timeLength)));
                    localScale.scale = oneFloat3 * lerp;
                }
                else if (timePassed < timeLengthGrow + animatingActionOverlayUI.timeLengthShrink)
                {
                    timePassed -= timeLengthGrow;
                    var timeLength = animatingActionOverlayUI.timeLengthShrink;
                    var lerp = (float) (math.sin(halfPI + halfPI * (timePassed / timeLength)));
                    localScale.scale = oneFloat3 * lerp;
                }
                // finished
                else
                {
                    // UnityEngine.Debug.LogError("RemoveComponent AnimatingActionOverlayUI: " + elapsedTime);
                    localScale.scale = new float3();
                    PostUpdateCommands.RemoveComponent<AnimatingActionOverlayUI>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}