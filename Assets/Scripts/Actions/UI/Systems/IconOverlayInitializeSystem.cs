using Unity.Entities;
using Zoxel.Rendering;
using Zoxel.Textures;
using Zoxel.UI;

namespace Zoxel.Actions.UI
{
    [UpdateInGroup(typeof(ActionUISystemGroup))]
    public partial class IconOverlayInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var cooldownOverlayMaterial = UIManager.instance.materials.cooldownOverlayMaterial;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, ActionOverlayUI>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in UIMeshData uiMeshData, in Size2D size2D) =>
            {
                var size = size2D.size;
                if (!HasComponent<AuthoredTexture>(e))
                {
                    var material = new UnityEngine.Material(cooldownOverlayMaterial);
                    if (!HasComponent<DontDestroyTexture>(e))
                    {
                        material.SetTexture("_BaseMap", TextureUtil.CreateWhiteTexture());
                    }
                    zoxMesh.material = material;
                }
                zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size, uiMeshData.horizontalAlignment, uiMeshData.verticalAlignment, uiMeshData.offset);
                zoxMesh.layer = worldUILayer;
                PostUpdateCommands2.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
        }
    }
}