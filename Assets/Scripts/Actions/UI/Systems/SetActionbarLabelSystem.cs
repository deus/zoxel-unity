using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Rendering;

namespace Zoxel.Actions.UI
{
    [BurstCompile, UpdateInGroup(typeof(ActionUISystemGroup))]
    public partial class SetActionbarLabelSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var labelDelay = UIManager.instance.uiDatam.actionLabelFadeTime; //  2; // 2.2f;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<Actionbar>()
                .ForEach((Entity e, int entityInQueryIndex, in Childrens childrens, in SetActionbarLabel setActionbarLabel) =>
            {
                if (setActionbarLabel.text.Length > 0)
                {
                    for (int i = 0; i < childrens.children.Length; i++)
                    {
                        var ui = childrens.children[i];
                        if (HasComponent<ActionbarLabel>(ui))
                        {
                            PostUpdateCommands.AddComponent(entityInQueryIndex, ui, new SetRenderText(setActionbarLabel.text));
                            PostUpdateCommands.AddComponent(entityInQueryIndex, ui, new FaderStayVisible(elapsedTime));
                            break;
                        }
                    }
                }
                PostUpdateCommands.RemoveComponent<SetActionbarLabel>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}