using Unity.Entities;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Items;
using Zoxel.Items.UI;
using Zoxel.Skills;
using Zoxel.Voxels;
using Zoxel.Textures;
using Zoxel.UI.MouseFollowing;

namespace Zoxel.Actions.UI
{
    //! Updates action icon and label when a new one is set.
    /**
    *   \todo Make this use SetIconData event.
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(ActionUISystemGroup))]
    public partial class ActionIconUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((Entity e, ref UserActionLinks userActionLinks, in UILink uiLink, in ActionTargetUpdated actionTargetUpdated) =>
            {
                var actionbar = uiLink.GetUI<Actionbar>(EntityManager);
                if (actionbar.Index == 0)
                {
                    return;
                }
                var isHidingMouse = false;
                var controllerEntity = EntityManager.GetComponentData<ControllerLink>(e).controller;
                if (EntityManager.HasComponent<MouseUILink>(controllerEntity))
                {
                    var mouseUIEntity = EntityManager.GetComponentData<MouseUILink>(controllerEntity).mouseUI;
                    isHidingMouse = HasComponent<HideMouseUI>(mouseUIEntity);
                    if (isHidingMouse)
                    {
                        return;
                    }
                }
                PostUpdateCommands.RemoveComponent<ActionTargetUpdated>(e);
                // UnityEngine.Debug.LogError("Updated Action of Index: " + updatedTargetID);
                var didRemoveAction = false;
                var isZeroQuantity = actionTargetUpdated.isZeroQuantity != 0;
                for (int i = 0; i < userActionLinks.actions.Length; i++)
                {
                    var action = userActionLinks.actions[i];
                    if (isZeroQuantity && action.targetMeta == actionTargetUpdated.targetMeta)
                    {
                        // UnityEngine.Debug.LogError("Zerod 2");
                        userActionLinks.actions[i] = new Action();
                        action = userActionLinks.actions[i];
                        didRemoveAction = true;
                        UpdateActionUI(PostUpdateCommands, e, action, actionbar, i);
                    }
                    else if (!isZeroQuantity && action.target == actionTargetUpdated.target)
                    {
                        UpdateActionUI(PostUpdateCommands, e, action, actionbar, i);
                    }
                }
                if (didRemoveAction)
                {
                    PostUpdateCommands.AddComponent<SaveActions>(e);
                    PostUpdateCommands.AddComponent(e, new SelectedActionUpdated(userActionLinks.selectedAction));
                }
            }).WithoutBurst().Run();
        }

        private void UpdateActionUI(EntityCommandBuffer PostUpdateCommands, Entity characterEntity, Action action, Entity actionbar, int actionIndex)
        {
            var childrens = EntityManager.GetComponentData<Childrens>(actionbar);
            if (actionIndex < 0 || actionIndex >= childrens.children.Length)
            {
                // UnityEngine.Debug.LogError("Action Index out of child bounds: " + actionIndex);
                return;
            }
            var frameEntity = childrens.children[actionIndex];
            var frameChildren = EntityManager.GetComponentData<Childrens>(frameEntity);
            var iconEntity = frameChildren.children[0];
            var controllerEntity = EntityManager.GetComponentData<ControllerLink>(characterEntity).controller;
            var subText = "";
            if (HasComponent<UserSkill>(action.target))
            {
                // Set Skill Icon
                var skillEntity = EntityManager.GetComponentData<MetaData>(action.target).data;
                var skillType = EntityManager.GetComponentData<Skill>(skillEntity).type;
                var skillID = EntityManager.GetComponentData<ZoxID>(skillEntity).id;
                PostUpdateCommands.AddComponent(iconEntity, new GenerateSkillTexture(skillID, skillType));
                // UnityEngine.Debug.LogError("Update AcctionUI Skill: " + skillID);
            }
            else if (HasComponent<UserItem>(action.target))
            {
                // Set Label
                var targetMetaEntity = EntityManager.GetComponentData<MetaData>(action.target).data;
                var inventory = EntityManager.GetComponentData<Inventory>(characterEntity);
                var quantity = inventory.GetItemQuantity(EntityManager, targetMetaEntity);
                // have to add mouse incase it was holding items
                var mouseUserItem = new UserItemQuantity();
                var mouseMetaEntity = new Entity();
                var isHidingMouse = false;
                if (EntityManager.HasComponent<MouseUILink>(controllerEntity))
                {
                    var mouseUIEntity = EntityManager.GetComponentData<MouseUILink>(controllerEntity).mouseUI;
                    isHidingMouse = HasComponent<HideMouseUI>(mouseUIEntity);
                    if (HasComponent<MouseUserItem>(mouseUIEntity))
                    {
                        mouseUserItem = EntityManager.GetComponentData<UserItemQuantity>(mouseUIEntity);
                        mouseMetaEntity = EntityManager.GetComponentData<MetaData>(mouseUIEntity).data;
                    }
                }
                if (isHidingMouse)
                {
                    targetMetaEntity = mouseMetaEntity;
                }
                if (mouseMetaEntity == targetMetaEntity)
                {
                    // UnityEngine.Debug.LogError("Adding quantity of mouse item: " + mouseUserItem.quantity + ", " + quantity);
                    quantity += mouseUserItem.quantity;
                }
                subText = ((int) quantity).ToString();
                if (quantity == 0 || quantity == 1)
                {
                    subText = "";
                }
                //UnityEngine.Debug.LogError("Updating UserItem Action: " + actionID + "x" + subText + " :: " + itemEntity.Index + " -- " + action.target.Index + " == " + isHidingMouse);
                // Set Icon! 
                PostUpdateCommands.AddComponent(frameEntity, new SetIconData(action.target));
            }
            else
            {
                PostUpdateCommands.AddComponent(iconEntity, new GenerateEmptyIcon(32, 1));
            }
            var renderTextEntity = frameChildren.children[1];
            PostUpdateCommands.AddComponent(renderTextEntity, new SetRenderText(new Text(subText)));
            // UnityEngine.Debug.LogError("(1) Updating Text to: " + subText + " of " + renderTextEntity.Index);
        }
    }
}