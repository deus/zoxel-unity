﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.UI;

namespace Zoxel.Actions.UI
{
    //! Spawns ActionFrameIcons and children.
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(ActionUISystemGroup))]
    public partial class ActionbarSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;
        public Entity actionbarPrefab;
        public IconPrefabs iconPrefabs;
        private Entity overlayPrefab;
        private Entity actionbarLabelPrefab;
        public static Entity spawnActionbarPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            uiHoldersQuery = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<EntityBusy>(),
                ComponentType.Exclude<LoadActions>(),
                ComponentType.Exclude<SaveNewCharacter>(),
                ComponentType.Exclude<GenerateUserActions>(),
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<UserActionLinks>(),
                ComponentType.ReadOnly<UILink>());
            var spawnActionbarArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnActionbar),
                typeof(CharacterLink),
                // typeof(GenericEvent),
                typeof(DelayEvent));
            spawnActionbarPrefab = EntityManager.CreateEntity(spawnActionbarArchetype);
            RequireForUpdate(processQuery);
            RequireForUpdate(uiHoldersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var uiDatam = UIManager.instance.uiDatam;
            var iconStyle = uiDatam.iconStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var actionbarPrefab = this.actionbarPrefab;
            var iconPrefabs = this.iconPrefabs;
            var actionbarLabelPrefab = this.actionbarLabelPrefab;
            var overlayPrefab = this.overlayPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uiHolderEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
			var userActionLinks = GetComponentLookup<UserActionLinks>(true);
            uiHolderEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<SpawnActionbar>()
                .ForEach((Entity e, int entityInQueryIndex, in CharacterLink characterLink) =>
            {
                if (!cameraLinks.HasComponent(characterLink.character))
                {
                    return;
                }
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);  // Destroy Event Entity
                var userActionLinks2 = userActionLinks[characterLink.character];
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, actionbarPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new NavigationDirty(userActionLinks2.selectedAction));
                // spawnedUI event
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawned((byte) (userActionLinks2.actions.Length + 1)));
                for (byte i = 0; i < userActionLinks2.actions.Length; i++)
                {
                    // Data
                    var action = userActionLinks2.actions[i];
                    var seed = (int) (128 + elapsedTime + i * 2048 + 4196 * entityInQueryIndex);
                    // Frame
                    var frameEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.frame, e3);
                    UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, frameEntity, e3);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, frameEntity, i);
                    UICoreSystem.SetSound(PostUpdateCommands, entityInQueryIndex, frameEntity, seed, iconStyle.soundVolume);
                    UICoreSystem.SetSeed(PostUpdateCommands, entityInQueryIndex, frameEntity, seed);
                    // Icon
                    UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.icon, frameEntity);
                    UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.label, frameEntity);
                    // Overlay UI
                    var overlayUI = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, overlayPrefab, frameEntity);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, overlayUI, new ActionOverlayUI(characterLink.character, i));
                    if (action.target.Index > 0)
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, frameEntity, new SetIconData(action.target));
                    }
                }
                //! Spawn Action Label
                var actionLabelEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, actionbarLabelPrefab, e3);
                UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, actionLabelEntity, userActionLinks2.actions.Length);
                if (characterLink.character.Index > 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, characterLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab),
                        new OnUISpawned(characterLink.character, 1));
                    if (cameraLinks.HasComponent(characterLink.character))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLinks[characterLink.character]);
                    }
                }
                // UnityEngine.Debug.LogError("Navigation Actionbar: " + userActionLinks.selectedAction);
                // UnityEngine.Debug.LogError("Spawning Actionbar Icons on: " + e.Index + " at time " + World.Time.ElapsedTime);
            })  .WithReadOnly(cameraLinks)
                .WithReadOnly(userActionLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private void InitializePrefabs()
        {
            if (actionbarPrefab.Index != 0)
            {
                return;
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var actionbarStyle = uiDatam.actionbarStyle.GetStyle();
            var panelStyle = uiDatam.panelStyle.GetStyle();
            var gridSize = new int2(10, 1);
            var iconSize = uiDatam.GetIconSize(uiScale);
            var padding = uiDatam.GetIconPadding(uiScale); // iconSize / 8f;
            var margins = uiDatam.GetIconMargins(uiScale); // iconSize / 6f;
            var positionOffset = new float3(0, 0.15f * iconSize.y, 0);  // originally 0.5f
            var actionOverlayColor = new Color(uiDatam.actionOverlayColor);
            var actionbarLabelColor = new Color(uiDatam.actionbarLabelColor);
            var iconStyle = uiDatam.iconStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var actionLabelSize = iconSize.x / 3;
            var statbarsHeight = 2f * uiDatam.GetPlayerStatbarPadding(uiScale) + (iconSize.y / uiDatam.playerStatbarDivision);
            var acctionbarHeight = (iconSize.y / 2f) + margins.y;   // half height
            var actionLabelPosition = new float3(0, actionLabelSize / 2f + acctionbarHeight + 2f * statbarsHeight, 0);
            var actionbarLabelFadeTime = 1.2f; // 3.8f;
            // expand from panel ui basic - UICoreSystem
            actionbarPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(actionbarPrefab);
            EntityManager.AddComponent<Actionbar>(actionbarPrefab);
            EntityManager.RemoveComponent<SpawnPanelTooltip>(actionbarPrefab);
            EntityManager.AddComponent<SelectFirstNavigation>(actionbarPrefab);
            EntityManager.AddComponent<GameAndPauseUI>(actionbarPrefab);
            EntityManager.RemoveComponent<SpawnHeaderUI>(actionbarPrefab);
            EntityManager.SetComponentData(actionbarPrefab, new UIAnchor(AnchorUIType.BottomMiddle));
            EntityManager.SetComponentData(actionbarPrefab, new GridUI(gridSize, padding, margins));
            EntityManager.SetComponentData(actionbarPrefab, new PanelUI(PanelType.Actionbar));
            EntityManager.AddComponentData(actionbarPrefab, new PanelPosition(positionOffset));
            // Icons
            iconPrefabs.frame = EntityManager.Instantiate(UICoreSystem.iconPrefabs.frame);
            EntityManager.AddComponent<Prefab>(iconPrefabs.frame);
            EntityManager.AddComponent<ActionUI>(iconPrefabs.frame);
            EntityManager.SetComponentData(iconPrefabs.frame, new OnChildrenSpawned(3));
            EntityManager.AddComponent<DisableUIRaycast>(iconPrefabs.frame);
            iconPrefabs.icon = (UICoreSystem.iconPrefabs.icon);
            iconPrefabs.label = (UICoreSystem.iconPrefabs.label);
            // Overlay
            overlayPrefab = EntityManager.Instantiate(UICoreSystem.iconOverlayPrefab);
            EntityManager.AddComponent<Prefab>(overlayPrefab);
            EntityManager.AddComponent<ActionOverlayUI>(overlayPrefab);
            EntityManager.AddComponentData(overlayPrefab, new LocalScale());
            EntityManager.SetComponentData(overlayPrefab, new MaterialBaseColor(actionOverlayColor));
            EntityManager.SetComponentData(overlayPrefab, new Zoxel.Transforms.Child(2));
            // Spawn the Switch Action Label!
            actionbarLabelPrefab = EntityManager.Instantiate(UICoreSystem.toastUIPrefab);
            EntityManager.AddComponent<Prefab>(actionbarLabelPrefab);
            EntityManager.AddComponent<ActionbarLabel>(actionbarLabelPrefab);
            EntityManager.SetComponentData(actionbarLabelPrefab, new LocalPosition(actionLabelPosition));
            EntityManager.SetComponentData(actionbarLabelPrefab, new RenderTextData(actionbarLabelColor, actionbarLabelColor, actionLabelSize));
            EntityManager.SetComponentData(actionbarLabelPrefab, new Fader(0, actionbarLabelColor.alpha / 255f, actionbarLabelFadeTime));
            EntityManager.SetComponentData(actionbarLabelPrefab, new ReverseFader(actionbarLabelFadeTime));
        }
    }
}