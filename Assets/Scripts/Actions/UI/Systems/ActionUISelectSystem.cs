using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Voxels;
using Zoxel.Items;
using Zoxel.Input;
using Zoxel.UI.MouseFollowing;

namespace Zoxel.Actions.UI
{
    //! Handles ButtonSelectvnt for ActionUI.
    [BurstCompile, UpdateInGroup(typeof(ActionUISystemGroup))]
    public partial class ActionUISelectSystem : SystemBase
    {
        private NativeArray<Text> texts;
        private Text labelA;
        private Text labelB;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllerQuery;
        private EntityQuery charactersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllerQuery = GetEntityQuery(
                ComponentType.ReadOnly<Controller>(),
                ComponentType.ReadOnly<CharacterLink>());
            charactersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Character>(),
                ComponentType.ReadOnly<UserActionLinks>());
            texts = new NativeArray<Text>(2, Allocator.Persistent);
            texts[0] = new Text("[action1] to Assign");
            texts[1] =  new Text("[action2] to Unassign");
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var texts = this.texts;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = controllerQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var controllers = GetComponentLookup<Controller>(true);
            var characterLinks = GetComponentLookup<CharacterLink>(true);
            var playerTooltipLinks = GetComponentLookup<PlayerTooltipLinks>(true);
            var mouseUILinks = GetComponentLookup<MouseUILink>(true);
            controllerEntities.Dispose();
            var characterEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var userActionLinks = GetComponentLookup<UserActionLinks>(true);
            characterEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<ActionUI>()
                .ForEach((int entityInQueryIndex, in Child child, in UISelectEvent uiSelectEvent) =>
            {
                var controllerEntity = uiSelectEvent.character;
                var controller = controllers[controllerEntity];
                var characterEntity = characterLinks[controllerEntity].character;
                var actions2 = userActionLinks[characterEntity];
                // needs none for when loading character
                if (controller.mapping == ControllerMapping.InGame || controller.mapping == ControllerMapping.None)
                {
                    if (actions2.selectedAction != (byte) child.index)
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, characterEntity, new SelectedActionUpdated((byte) child.index));
                    }
                }
                // I need to check if has selected item
                else if (HasComponent<PlayerTooltipLinks>(controllerEntity))
                {
                    var playerTooltipLinks2 = playerTooltipLinks[controllerEntity];
                    var mouseUIEntity = mouseUILinks[controllerEntity].mouseUI;
                    if (HasComponent<FollowingMouse>(mouseUIEntity))
                    {
                        playerTooltipLinks2.SetTooltipText(PostUpdateCommands, entityInQueryIndex, controllerEntity, texts[0].Clone(), 0);  
                    }
                    else
                    {
                        var action = actions2.actions[child.index];
                        if (action.target.Index != 0)
                        {
                            playerTooltipLinks2.SetTooltipText(PostUpdateCommands, entityInQueryIndex, controllerEntity, texts[1].Clone(), 0);  
                        }
                        else
                        {
                            playerTooltipLinks2.SetTooltipText(PostUpdateCommands, entityInQueryIndex, controllerEntity, new Text(), 0);   
                        }
                    }
                }
                //UnityEngine.Debug.LogError("Selected Action: " + child.index + " with mapping: " + (controller.mapping));
            })  .WithReadOnly(controllers)
                .WithReadOnly(characterLinks)
                .WithReadOnly(playerTooltipLinks)
                .WithReadOnly(mouseUILinks)
                .WithReadOnly(userActionLinks)
                .WithReadOnly(texts)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
        
/*
Note: Use this to check if burst is used.

[BurstDiscard]
private static void UnSetIfNotBurst(ref bool answer)
{
    answer = false;
}

public static bool IsBursted()
{
    var ret = true;
    UnSetIfNotBurst(ref ret);
    return ret;
}*/