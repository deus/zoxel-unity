using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Actions.UI
{
    //! Uses the ActivatedAction event of the character to start a UI animation.
    [UpdateAfter(typeof(ActionOverlayAnimationSystem))] // as sometimes it removes, gotta add after
    [BurstCompile, UpdateInGroup(typeof(ActionUISystemGroup))]
    public partial class ActionOverlayUISystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery characterQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            characterQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserActionLinks>(),
                ComponentType.ReadOnly<ActivatedAction>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DestroyEntity>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (characterQuery.IsEmpty)
            {
                return;
            }
            // if any processQuery? ActionOverlayUI
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var characterEntities = characterQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var activatedActions = GetComponentLookup<ActivatedAction>(true);
            characterEntities.Dispose();
            Dependency = Entities
                // .WithNone<AnimatingActionOverlayUI>()
                .ForEach((Entity e, int entityInQueryIndex, in ActionOverlayUI actionOverlayUI) =>
            {
                if (activatedActions.HasComponent(actionOverlayUI.character))
                {
                    var activateAction = activatedActions[actionOverlayUI.character];
                    if (activateAction.index == actionOverlayUI.actionIndex && activateAction.timeLengthGrow != 0)
                    {
                        // UnityEngine.Debug.LogError("AddComponent AnimatingActionOverlayUI: " + elapsedTime);
                        var animatingActionOverlayUI = new AnimatingActionOverlayUI(elapsedTime, activateAction.timeLengthGrow, activateAction.timeLengthShrink);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, animatingActionOverlayUI);
                    }
                }
            })  .WithReadOnly(activatedActions)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}