using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Items.UI;
using Zoxel.Skills.UI;
using Zoxel.Items;
using Zoxel.UI.MouseFollowing;

namespace Zoxel.Actions.UI
{
    //! Handles assigning new things to userActionLinks ui.
    /**
    *   - UIClickEvent System -
    *   UI Raycasting should be disabled during game
    */
    [BurstCompile, UpdateInGroup(typeof(ActionUISystemGroup))]
    public partial class ActionUIClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllerQuery;
        private EntityQuery charactersQuery;
        private EntityQuery mouseUIQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllerQuery = GetEntityQuery(
                ComponentType.ReadOnly<Player>(),
                ComponentType.ReadOnly<CharacterLink>());
            charactersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Character>(),
                ComponentType.ReadOnly<UserActionLinks>());
            mouseUIQuery = GetEntityQuery(
                ComponentType.ReadOnly<MouseUI>(),
                ComponentType.ReadOnly<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = controllerQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var characterLinks = GetComponentLookup<CharacterLink>(true);
            var mouseUILinks = GetComponentLookup<MouseUILink>(true);
            controllerEntities.Dispose();
            var characterEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var userActionLinks = GetComponentLookup<UserActionLinks>(true);
            characterEntities.Dispose();
            var mouseUIEntities = mouseUIQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var mouseUIs = GetComponentLookup<MouseUI>(true);
            var mouseUserItems = GetComponentLookup<MouseUserItem>(true);
            var mouseSkills = GetComponentLookup<MouseSkill>(true);
            var mouseMetaDatas = GetComponentLookup<MetaData>(true);
            mouseUIEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<ActionUI>()
                .ForEach((int entityInQueryIndex, in Child child, in UIClickEvent uiClickEvent) =>
            {
                var controllerEntity = uiClickEvent.controller;
                var characterEntity = characterLinks[controllerEntity].character;
                var buttonType = uiClickEvent.buttonType;
                var arrayIndex = child.index;
                var mouseUIEntity = mouseUILinks[controllerEntity].mouseUI;
                var isAssigningThing = HasComponent<FollowingMouse>(mouseUIEntity);
                var actions2 = userActionLinks[characterEntity];
                if (buttonType == ButtonActionType.Confirm)
                {
                    if (isAssigningThing)
                    {
                        PostUpdateCommands.AddComponent<HideMouseUI>(entityInQueryIndex, mouseUIEntity);
                        var target = new Entity();
                        var targetMeta = new Entity();
                        if (HasComponent<MouseUserItem>(mouseUIEntity))
                        {
                            // stop moving item 
                            // handle no space for items'
                            var mouseUserItem = mouseUserItems[mouseUIEntity];
                            var mouseMetaEntity = mouseMetaDatas[mouseUIEntity].data;
                            if (HasComponent<ConsumableItem>(mouseMetaEntity) || HasComponent<VoxelItem>(mouseMetaEntity))
                            {
                                target = mouseUserItem.userItemEntity;
                                targetMeta = mouseMetaEntity;
                                actions2.actions[arrayIndex] = new Action(target, targetMeta);
                                PostUpdateCommands.AddComponent(entityInQueryIndex, characterEntity, new OnUserItemPlaced(target, targetMeta));
                            }
                            else
                            {
                                // UnityEngine.Debug.Log("Item was Equipable. Cannot assign to actionbar.");
                                return;
                            }
                        }
                        else if (HasComponent<MouseSkill>(mouseUIEntity))
                        {
                            var mouseSkill = mouseSkills[mouseUIEntity];
                            target = mouseSkill.userSkill;
                            targetMeta = mouseSkill.skill;
                            actions2.actions[arrayIndex] = new Action(target, targetMeta);
                        }
                        else
                        {
                            UnityEngine.Debug.LogError("Clicked when using another UI");
                            return;
                        }
                        PostUpdateCommands.SetComponent(entityInQueryIndex, characterEntity, actions2);
                        PostUpdateCommands.AddComponent<SaveActions>(entityInQueryIndex, characterEntity);
                        // Update UI of userActionLinks
                        PostUpdateCommands.AddComponent(entityInQueryIndex, characterEntity, new ActionTargetUpdated(target, targetMeta));
                    }
                }
                else if (buttonType == ButtonActionType.Action3)
                {
                    if (isAssigningThing)
                    {
                        PostUpdateCommands.AddComponent<HideMouseUI>(entityInQueryIndex, mouseUIEntity);
                    }
                    var action = actions2.actions[arrayIndex];
                    if (action.target.Index != 0)
                    {
                        //UnityEngine.Debug.LogError("Removing Action " + arrayIndex);
                        actions2.actions[arrayIndex] = new Action();
                        PostUpdateCommands.SetComponent(entityInQueryIndex, characterEntity, actions2);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, characterEntity, new ActionTargetUpdated());
                        PostUpdateCommands.AddComponent<SaveActions>(entityInQueryIndex, characterEntity);
                    }
                }
                else if (buttonType == ButtonActionType.Cancel)
                {
                    if (isAssigningThing)
                    {
                        //UnityEngine.Debug.LogError("Canceling Assigning Item");
                        PostUpdateCommands.AddComponent<HideMouseUI>(entityInQueryIndex, mouseUIEntity);
                    }
                }
                else
                {
                    return;
                }
                if (isAssigningThing)
                {
                    PostUpdateCommands.AddComponent<NavigationDirty>(entityInQueryIndex, mouseUIs[mouseUIEntity].panelUI);
                }
            })  .WithReadOnly(characterLinks)
                .WithReadOnly(mouseUILinks)
                .WithReadOnly(userActionLinks)
                .WithReadOnly(mouseUIs)
                .WithReadOnly(mouseUserItems)
                .WithReadOnly(mouseSkills)
                .WithReadOnly(mouseMetaDatas)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}