using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Animations;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Items;
using Zoxel.Skills;
using Zoxel.Players;
using Zoxel.Actions.UI;
using Zoxel.Skeletons;
using Zoxel.Voxels;
using Zoxel.Voxels.Lighting;
using Zoxel.Voxels.Authoring;
using Zoxel.Items.Authoring;

namespace Zoxel.Actions
{ 
    //! Spawns an item in the hand of a BoneLinks.
    /**
    *   - Spawn System -
    *   Uses HeldActionItemLink.
    */
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class HeldItemSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery itemsQuery;
        private EntityQuery voxelsQuery;
        private EntityQuery userItemsQuery;
        private Entity voxModelPrefab;
        private Entity voxelModelPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			itemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Item>(),
                ComponentType.Exclude<DestroyEntity>());
			voxelsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Voxel>(),
                ComponentType.Exclude<DestroyEntity>());
            userItemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserItem>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate<ModelSettings>();
            RequireForUpdate(processQuery);
        }

        private void InitializePrefabs()
        {
            if (this.voxelModelPrefab.Index != 0)
            {
                return;
            }
            var modelSettings = GetSingleton<ModelSettings>();
            var voxScale = modelSettings.GetVoxelScale();
            // Vox!
            this.voxModelPrefab = EntityManager.Instantiate(VoxelSystemGroup.worldVoxPrefab);
            EntityManager.SetComponentData(this.voxModelPrefab, new VoxScale(voxScale));
            EntityManager.AddComponent<Prefab>(this.voxModelPrefab);
            EntityManager.AddComponent<CharacterLink>(this.voxModelPrefab);
            EntityManager.AddComponent<ParentLink>(this.voxModelPrefab);
            EntityManager.AddComponent<LocalPosition>(this.voxModelPrefab);
            EntityManager.AddComponent<LocalRotation>(this.voxModelPrefab);
            EntityManager.AddComponent<WorldVoxItem>(this.voxModelPrefab);
            EntityManager.AddComponent<EntityLighting>(this.voxModelPrefab);
            // Voxel!
            this.voxelModelPrefab = EntityManager.Instantiate(VoxelSystemGroup.worldVoxelPrefab);
            EntityManager.SetComponentData(this.voxelModelPrefab, new Scale { Value = 0.2f });
            EntityManager.AddComponent<Prefab>(this.voxelModelPrefab);
            EntityManager.AddComponent<InitializeEntity>(this.voxelModelPrefab);
            EntityManager.AddComponent<CharacterLink>(this.voxelModelPrefab);
            EntityManager.AddComponent<ParentLink>(this.voxelModelPrefab);
            EntityManager.AddComponent<LocalPosition>(this.voxelModelPrefab);
            EntityManager.AddComponent<LocalRotation>(this.voxelModelPrefab);
            EntityManager.AddComponent<EntityLighting>(this.voxelModelPrefab);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var itemsSettings = GetSingleton<ItemsSettings>();
			var degreesToRadians = ((math.PI * 2) / 360f);
			var heldRotation = quaternion.EulerXYZ(new float3(90, 0, 0) * degreesToRadians);
            var voxelModelPrefab = this.voxelModelPrefab;
            var voxModelPrefab = this.voxModelPrefab;
            var heldVoxelOffset = itemsSettings.heldVoxelOffset;
            var heldVoxOffset = itemsSettings.heldVoxOffset;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var voxelItems = GetComponentLookup<VoxelItem>(true);
			var itemVoxDatas = GetComponentLookup<Chunk>(true);
			var itemVoxColors = GetComponentLookup<VoxColors>(true);
			var itemGenerateVoxDatas = GetComponentLookup<GenerateVoxData>(true);
			var itemChunkDimensions = GetComponentLookup<ChunkDimensions>(true);
            itemEntities.Dispose();
            var voxelEntities = voxelsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var voxelGenerateVoxDatas = GetComponentLookup<GenerateVoxData>(true);
			var voxelChunkDimensions = GetComponentLookup<ChunkDimensions>(true);
			var voxelVoxDatas = GetComponentLookup<Chunk>(true);
			var voxelVoxColors = GetComponentLookup<VoxColors>(true);
            voxelEntities.Dispose();
            var userItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var userItems = GetComponentLookup<UserItemQuantity>(true);
            var userMetaDatas = GetComponentLookup<MetaData>(true);
            userItemEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity, EntityBusy>()
                .WithNone<ActivateAction>()
                .WithAll<SelectedActionUpdated>()
                .ForEach((Entity e, int entityInQueryIndex, in UserActionLinks userActionLinks, in VoxLink voxLink, in BoneLinks boneLinks, in EntityLighting entityLighting) =>
            {
                var target = userActionLinks.GetSelectedTarget();
                if (!HasComponent<UserItem>(target))
                {
                    return;
                }
                var selectedInventoryItem = userItems[target];
                if (selectedInventoryItem.quantity == 0)
                {
                    return;
                }
                if (boneLinks.bones.Length == 0)
                {
                    ///UnityEngine.Debug.LogError("No Bones on Entity: " + e.Index);
                    return;
                }
                var handBone = new Entity();
                var handIndex = 0;
                for (int i = 0; i < boneLinks.bones.Length; i++)
                {
                    var bone = boneLinks.bones[i];
                    if (HasComponent<HandBone>(bone))
                    {
                        if (handIndex == 1)
                        {
                            handBone = bone;
                            break;
                        }
                        handIndex++;
                    }
                }
                if (handBone.Index == 0)
                {
                    //UnityEngine.Debug.LogError("Could not find Hand Bone: " + boneLinks.bones.Length + " : " + handIndex
                    // + ", bones spawned? " + HasComponent<OnBonesSpawned>(e)
                    //    + " e: " + e.Index);
                    handBone = e;   // Slime to hold items?
                    // return;
                }
                var itemEntity = userMetaDatas[target].data;
                var seedID = 666;
                var worldItemEntity = new Entity();
                var heldItemRotation = quaternion.identity;
                if (HasComponent<VoxelItem>(itemEntity))
                {
                    // UnityEngine.Debug.LogError("Spawning Voxel in hand: " + selectedItem.data.voxelType);
                    // find hand
                    var voxelItem = voxelItems[itemEntity];
                    var voxelEntity = voxelItem.voxel;
                    // Voxel Minivox / Vox Models
                    if (HasComponent<MinivoxVoxel>(voxelEntity))
                    {
                        // UnityEngine.Debug.LogError("Todo: Held Voxel Minivox world items.");
                        worldItemEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, voxModelPrefab);
                        // UnityEngine.Debug.LogError("Spawning Held Item with MaterialBaseColor " + entityLighting.lightValue);
                        if (HasComponent<GenerateVoxData>(voxelEntity))
                        {
                            // Torch for example
                            if (voxelGenerateVoxDatas.HasComponent(voxelEntity))
                            {
                                var voxelGenerateVoxData = voxelGenerateVoxDatas[voxelEntity];
                                PostUpdateCommands.AddComponent<GenerateVox>(entityInQueryIndex, worldItemEntity);
                                PostUpdateCommands.AddComponent(entityInQueryIndex, worldItemEntity, new Seed(seedID));
                                PostUpdateCommands.AddComponent(entityInQueryIndex, worldItemEntity, new GenerateVoxData(voxelGenerateVoxData.generationType));
                                PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, voxelChunkDimensions[voxelEntity]);
                                PostUpdateCommands.AddComponent<GenerateVoxColors>(entityInQueryIndex, worldItemEntity);
                                PostUpdateCommands.RemoveComponent<Chunk>(entityInQueryIndex, worldItemEntity);
                                heldItemRotation = heldRotation;
                            }
                        }
                        else if (HasComponent<Chunk>(voxelEntity))
                        {
                            if (voxelVoxDatas.HasComponent(voxelEntity))
                            {
                                var voxData = voxelVoxDatas[voxelEntity];
                                PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, new Chunk(voxData.voxelDimensions, voxData.voxels.Clone()));
                                PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, new ChunkDimensions(voxData.voxelDimensions));
                                PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, new VoxColors(voxelVoxColors[voxelEntity].colors.Clone()));
                            }
                        }
                        // link to character
                        PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, new ZoxID(seedID));
                    }
                    else
                    {
                        worldItemEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, voxelModelPrefab);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, worldItemEntity, new VoxelLink(voxelItem.voxel));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, new MaterialBaseColor(entityLighting.GetBrightnessFloat4()));
                    }
                    //! \todo Spawn vox item here for generated voxes!
                    // check Voxel's mesh type, pass in voxel data
                    // shouldn't item link to voxel entity in realm? it's better then just the index!
                }
                else if (HasComponent<ConsumableItem>(itemEntity))
                {
                    // link to character
                    worldItemEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, voxModelPrefab);
                    // UnityEngine.Debug.LogError("Todo: Held Vox World Items.");
                    // else, does it contain GenerateVoxData?
                    if (HasComponent<GenerateVoxData>(itemEntity))
                    {
                        if (itemGenerateVoxDatas.HasComponent(itemEntity))
                        {
                            var itemGenerateVoxData = itemGenerateVoxDatas[itemEntity];
                            PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, itemChunkDimensions[itemEntity]);
                            PostUpdateCommands.AddComponent<GenerateVox>(entityInQueryIndex, worldItemEntity);
                            PostUpdateCommands.AddComponent(entityInQueryIndex, worldItemEntity, new Seed(seedID));
                            PostUpdateCommands.AddComponent(entityInQueryIndex, worldItemEntity, new GenerateVoxData(itemGenerateVoxData.generationType));
                            PostUpdateCommands.AddComponent<GenerateVoxColors>(entityInQueryIndex, worldItemEntity);
                            PostUpdateCommands.RemoveComponent<Chunk>(entityInQueryIndex, worldItemEntity);
                        }
                    }
                    else if (HasComponent<Chunk>(itemEntity)) // && itemVoxDatas.HasComponent(selectedInventoryItem.item))
                    {
                        // UnityEngine.Debug.LogError("Spawned Mushroom?");
                        if (itemVoxDatas.HasComponent(itemEntity))
                        {
                            var voxData = itemVoxDatas[itemEntity];
                            PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, new Chunk(voxData.voxelDimensions, voxData.voxels.Clone()));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, new ChunkDimensions(voxData.voxelDimensions));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, new VoxColors(itemVoxColors[itemEntity].colors.Clone()));
                        }
                    }
                    PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, new ZoxID(seedID));
                }
                else
                {
                    return;
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, new CharacterLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, voxLink);
                PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, new ParentLink(handBone));
                PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, new LocalPosition(heldVoxOffset));
                PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, new LocalRotation(heldItemRotation));
                PostUpdateCommands.SetComponent(entityInQueryIndex, worldItemEntity, entityLighting);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e, new HeldActionItemLink(worldItemEntity));
            })  .WithReadOnly(userItems)
                .WithReadOnly(userMetaDatas)
                .WithReadOnly(voxelItems)
                .WithReadOnly(itemVoxDatas)
                .WithReadOnly(itemVoxColors)
                .WithReadOnly(itemGenerateVoxDatas)
                .WithReadOnly(itemChunkDimensions)
                .WithReadOnly(voxelGenerateVoxDatas)
                .WithReadOnly(voxelChunkDimensions)
                .WithReadOnly(voxelVoxDatas)
                .WithReadOnly(voxelVoxColors)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}