using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Actions
{
    //! Sets action index with the SelectedActionUpdated event.
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class ActionSetSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<EntityBusy, InitializeEntity, DestroyEntity>()
                .WithNone<ActivateAction>()
                .ForEach((ref UserActionLinks userActionLinks, in SelectedActionUpdated selectedActionUpdated) =>
            {
                userActionLinks.lastSelectedAction = userActionLinks.selectedAction;
                userActionLinks.selectedAction = selectedActionUpdated.selected;
            }).ScheduleParallel();
        }
    }
}