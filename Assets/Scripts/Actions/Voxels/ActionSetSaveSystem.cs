using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Actions
{ 
    //! Saves userActionLinks whenever it is updated.
    /**
    *   \todo Don't save the first time after loading userActionLinks.
    *   \todo Place voxel animation
    */
    [UpdateAfter(typeof(ActionSetSystem))]
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class ActionSetSaveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity, InitializeEntity, EntityBusy>()
                .WithNone<ActivateAction, HasSetFirstAction>()
                .WithAll<SelectedActionUpdated, EntitySaver>()
                .ForEach((Entity e, int entityInQueryIndex, in UserActionLinks userActionLinks) =>
            {
                if(userActionLinks.selectedAction != 255)
                {
                    PostUpdateCommands.AddComponent<HasSetFirstAction>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<DestroyEntity, InitializeEntity, EntityBusy>()
                .WithNone<ActivateAction>()
                .WithAll<SelectedActionUpdated, HasSetFirstAction, EntitySaver>()
                .ForEach((Entity e, int entityInQueryIndex, in UserActionLinks userActionLinks) =>
            {
                if(userActionLinks.selectedAction != 255)
                {
                    // if first action?
                    PostUpdateCommands.AddComponent<SaveActions>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}