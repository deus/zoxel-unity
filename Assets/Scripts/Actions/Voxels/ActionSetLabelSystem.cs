using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Items;
using Zoxel.Skills;
using Zoxel.UI;
using Zoxel.Actions.UI;

namespace Zoxel.Actions
{ 
    //! Sets action label when an action is set based on item or skill name.
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class ActionSetLabelSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery itemsQuery;
        private EntityQuery skillsQuery;
        private EntityQuery userItemsQuery;
        private EntityQuery userSkillsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			itemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Item>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.ReadOnly<ZoxName>(),
                ComponentType.Exclude<DestroyEntity>());
            skillsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Skill>(),
                ComponentType.ReadOnly<ZoxName>(),
                ComponentType.Exclude<DestroyEntity>());
            userItemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserItem>(),
                ComponentType.Exclude<DestroyEntity>());
            userSkillsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserSkill>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var itemNames = GetComponentLookup<ZoxName>(true);
            var skillEntities = skillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var skillNames = GetComponentLookup<ZoxName>(true);
            var userItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var userItems = GetComponentLookup<MetaData>(true);
            var userSkillEntities = userSkillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var userSkills = GetComponentLookup<MetaData>(true);
            itemEntities.Dispose();
            skillEntities.Dispose();
            userItemEntities.Dispose();
            userSkillEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<EntityBusy>()
                .WithNone<InitializeEntity, DestroyEntity, ActivateAction>()
                .WithAll<SelectedActionUpdated>()
                .ForEach((int entityInQueryIndex, ref UserActionLinks userActionLinks, in SelectedActionUpdated selectedActionUpdated, in Inventory inventory, in UserSkillLinks userSkillLinks, in UILink uiLink) =>
            {
                var target = userActionLinks.GetSelectedTarget();
                if (target.Index == 0)
                {
                    return;
                }
                // Set Actionbar label system - when action is set
                for (int i = 0; i < uiLink.uis.Length; i++)
                {
                    var ui = uiLink.uis[i];
                    if (HasComponent<Actionbar>(ui))
                    {
                        var label = new Text();
                        // if(userActionLinks.selectedSkillIndex != -1 && skillEntities.Length > 0)
                        if (HasComponent<UserSkill>(target))
                        {
                            var skillEntity = userSkills[target].data; // userSkillLinks.skills[actions.selectedSkillIndex].skill;
                            if (HasComponent<ZoxName>(skillEntity))
                            {
                                var skillName = skillNames[skillEntity];
                                if (skillName.name.Length > 0)
                                {
                                    label = skillName.name.Clone();
                                    //UnityEngine.Debug.LogError("2Found Actionbar: " + i);
                                }
                            }
                        }
                        else if (HasComponent<UserItem>(target))
                        //else if(userActionLinks.selectedItem != -1)
                        {
                            var itemEntity = userItems[target].data; // inventory.items[actions.selectedItem].item;
                            if (HasComponent<ZoxName>(itemEntity))
                            {
                                var itemSelectedName = itemNames[itemEntity].name;
                                if (itemSelectedName.Length > 0)
                                {
                                    //UnityEngine.Debug.LogError("3Found Actionbar: " + i);
                                    label = itemSelectedName.Clone();
                                }
                            }
                        }
                        PostUpdateCommands.AddComponent(entityInQueryIndex, ui, new SetActionbarLabel(label));
                        //UnityEngine.Debug.LogError("Found Actionbar: " + i);
                        break;
                    }
                }
            })  .WithReadOnly(userSkills)
                .WithReadOnly(userItems)
                .WithReadOnly(itemNames)
                .WithReadOnly(skillNames)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}