using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Items;
using Zoxel.Skills;
using Zoxel.VoxelInteraction;

namespace Zoxel.Actions
{ 
    //! Spawns an item in the hand of a BoneLinks.
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class ActionSelectedInteractSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery metaDatasQuery;
        private EntityQuery userItemsQuery;
        private EntityQuery userSkillsQuery;
        private EntityQuery skillsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            metaDatasQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<MetaData>());
            userItemsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserItem>());
            userSkillsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserSkill>());
            skillsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Skill>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var metaDataEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleZ);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleZ);
            var metaDatas = GetComponentLookup<MetaData>(true);
            metaDataEntities.Dispose();
            var userItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var userItems = GetComponentLookup<UserItem>(true);
            userItemEntities.Dispose();
            var userSkillEntities = userSkillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var userSkills = GetComponentLookup<UserSkill>(true);
            userSkillEntities.Dispose();
            var skillEntities = skillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var skillVoxelInteractions = GetComponentLookup<SkillVoxelInteraction>(true);
            skillEntities.Dispose();
            //! Handle voxel interaction types
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity, EntityBusy>()
                .WithNone<ActivateAction>()
                .WithAll<SelectedActionUpdated>()
                .ForEach((Entity e, int entityInQueryIndex, in UserActionLinks userActionLinks) =>
            {
                var target = userActionLinks.GetSelectedTarget();
                var isVoxelInteraction = false;
                if (target.Index == 0)
                {
                    isVoxelInteraction = false;
                }
                else if (metaDatas.HasComponent(target))
                {
                    var metaEntity = metaDatas[target].data;
                    if (userItems.HasComponent(target))
                    {
                        isVoxelInteraction = !HasComponent<ConsumableItem>(metaEntity);
                    }
                    else if (userSkills.HasComponent(target))
                    {
                        isVoxelInteraction = skillVoxelInteractions.HasComponent(metaEntity);
                    }
                }
                var hasDisableVoxelInteraction = HasComponent<DisableVoxelInteraction>(e);
                if (!hasDisableVoxelInteraction && !isVoxelInteraction)
                {
                    PostUpdateCommands.AddComponent<BeginDisableVoxelInteraction>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<DisableVoxelInteraction>(entityInQueryIndex, e);
                }
                else if (hasDisableVoxelInteraction && isVoxelInteraction)
                {
                    PostUpdateCommands.AddComponent<EndDisableVoxelInteraction>(entityInQueryIndex, e);
                    PostUpdateCommands.RemoveComponent<DisableVoxelInteraction>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(metaDatas)
                .WithReadOnly(userItems)
                .WithReadOnly(userSkills)
                .WithReadOnly(skillVoxelInteractions)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            //! Handle PlacingVoxelItem Component for placing item VoxelItem.
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity, EntityBusy>()
                .WithNone<ActivateAction>()
                .WithNone<PlacingVoxelItem>()
                .WithAll<SelectedActionUpdated>()
                .ForEach((Entity e, int entityInQueryIndex, in UserActionLinks userActionLinks) =>
            {
                var target = userActionLinks.GetSelectedTarget();
                if (!metaDatas.HasComponent(target))
                {
                    return;
                }
                var itemEntity = metaDatas[target].data;
                if (HasComponent<VoxelItem>(itemEntity))
                {
                    PostUpdateCommands.AddComponent<BeginPlacingVoxelItem>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new PlacingVoxelItem(itemEntity));
                }
            })  .WithReadOnly(metaDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity, EntityBusy>()
                .WithNone<ActivateAction>()
                .WithAll<SelectedActionUpdated, PlacingVoxelItem>()
                .ForEach((Entity e, int entityInQueryIndex, in UserActionLinks userActionLinks) =>
            {
                var target = userActionLinks.GetSelectedTarget();
                if (!metaDatas.HasComponent(target))
                {
                    return;
                }
                var itemEntity = metaDatas[target].data;
                if (HasComponent<VoxelItem>(itemEntity))
                {
                    PostUpdateCommands.AddComponent<BeginPlacingVoxelItem>(entityInQueryIndex, e);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e, new PlacingVoxelItem(itemEntity));
                }
            })  .WithReadOnly(metaDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity, EntityBusy>()
                .WithNone<ActivateAction>()
                .WithAll<SelectedActionUpdated, PlacingVoxelItem>()
                .ForEach((Entity e, int entityInQueryIndex, in UserActionLinks userActionLinks) =>
            {
                var target = userActionLinks.GetSelectedTarget();
                if (!metaDatas.HasComponent(target))
                {
                    return;
                }
                var itemEntity = metaDatas[target].data;
                if (!HasComponent<VoxelItem>(itemEntity))
                {
                    PostUpdateCommands.AddComponent<EndPlacingVoxelItem>(entityInQueryIndex, e);
                    PostUpdateCommands.RemoveComponent<PlacingVoxelItem>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(metaDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                    /*if (skills.HasComponent(skillEntity))
                    {
                        var skillType = skills[skillEntity].type;
                        isDisableVoxelInteraction = skillType == SkillType.Projectile
                            || skillType == SkillType.Augmentation
                            || skillType == SkillType.Shield
                            || skillType == SkillType.None;
                    }*/
                /*if (userItemsMetaDatas.HasComponent(target))
                {
                    var itemEntity = userItemsMetaDatas[target].data;
                    isVoxelInteraction = !HasComponent<ConsumableItem>(itemEntity);
                    UnityEngine.Debug.LogError("userItemsMetaDatas HasComponent: " + itemEntity.Index);
                }
                else if (userSkillMetaDatas.HasComponent(target))
                {
                    var skillEntity = userSkillMetaDatas[target].data;
                    isVoxelInteraction = skillVoxelInteractions.HasComponent(skillEntity);
                    UnityEngine.Debug.LogError("Checking Skill Voxel Interaction: " + skillEntity.Index);
                }*/