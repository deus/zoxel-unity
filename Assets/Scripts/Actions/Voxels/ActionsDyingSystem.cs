using Unity.Entities;
using Unity.Burst;
using Zoxel.Skills;

namespace Zoxel.Actions
{
    //! Handles when a character dies using the DyingEntity event.
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
	public partial class ActionsDyingSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DyingEntity, UserActionLinks>()
                .ForEach((Entity e, int entityInQueryIndex, ref UserActionLinks userActionLinks) =>
            {
                ActionSetSkillSystem.RemoveSkillComponent(PostUpdateCommands, entityInQueryIndex, e, userActionLinks.skillType);
                userActionLinks.skillType = SkillType.None;
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}