using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Skeletons;
using Zoxel.Skills;

namespace Zoxel.Actions
{ 
    //! Repositions weapon based on action set.
    [UpdateAfter(typeof(ActionSetSkillSystem))]
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class ActionSetWeaponsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userSkillsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userSkillsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserSkill>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var degreesToRadians = ((math.PI * 2) / 360f);
            var weaponBackRotation = quaternion.EulerXYZ(new float3(-75, 90, 0) * degreesToRadians);
            var weaponBackPosition = new int3(3, 16, -6).ToFloat3() / 48f;
            var weaponHandPosition = new int3(0, 4, 8).ToFloat3() / 48f; // 11);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // Weapon Links
            Dependency = Entities
                .WithNone<EntityBusy>()
                .WithNone<InitializeEntity, DestroyEntity, ActivateAction>()
                .WithAll<SelectedActionUpdated>()
                .ForEach((Entity e, int entityInQueryIndex, ref HeldActionItemLink heldActionItemLink) =>
            {
                if (heldActionItemLink.item.Index > 0)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, heldActionItemLink.item);
                    heldActionItemLink.item = new Entity();
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            var userSkillEntities = userSkillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var userSkills = GetComponentLookup<MetaData>(true);
            userSkillEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<EntityBusy>()
                .WithNone<InitializeEntity, DestroyEntity, ActivateAction>()
                .WithAll<SelectedActionUpdated>()
                .ForEach((Entity e, int entityInQueryIndex, in HeldWeaponLinks heldWeaponLinks, in UserActionLinks userActionLinks) =>
            {
                // Only re position if weapon is equiped
                if (heldWeaponLinks.heldWeaponRightHand.Index == 0)
                {
                    return;
                }
                var positionWeaponsToChest = true;
                var target = userActionLinks.GetSelectedTarget();
                // if old skilltype if attack - it was positioned in hand
                if(userActionLinks.skillType == SkillType.PhysicalAttack)
                {
                    //UnityEngine.Debug.LogError("Changing Action of " + e.Index + " to " + userActionLinks.selectedSkillIndex);
                    if (HasComponent<UserSkill>(target))
                    {
                        var skillSelected = userSkills[target].data;
                        if(userActionLinks.skillType == SkillType.PhysicalAttack)
                        {
                            positionWeaponsToChest = false;
                        }
                    }
                }
                else
                {
                    if (HasComponent<UserSkill>(target))
                    {
                        var skillEntity = userSkills[target].data;
                        if (HasComponent<SkillPhysicalAttack>(skillEntity))
                        {
                            positionWeaponsToChest = false;
                        }
                    }
                }
                if (positionWeaponsToChest)
                {
                    // Positions weapon back to Chest
                    PostUpdateCommands.SetComponent(entityInQueryIndex, heldWeaponLinks.heldWeaponRightHand, new ParentLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, heldWeaponLinks.heldWeaponRightHand, new LocalPosition(weaponBackPosition));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, heldWeaponLinks.heldWeaponRightHand, new LocalRotation(weaponBackRotation));
                }
                else
                {
                    // Positions weapon back to Hand
                    PostUpdateCommands.SetComponent(entityInQueryIndex, heldWeaponLinks.heldWeaponRightHand, new ParentLink(heldWeaponLinks.rightHand));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, heldWeaponLinks.heldWeaponRightHand, new LocalPosition(weaponHandPosition));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, heldWeaponLinks.heldWeaponRightHand, LocalRotation.Identity);
                }
            })  .WithReadOnly(userSkills)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}