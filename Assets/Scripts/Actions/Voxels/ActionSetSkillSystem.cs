using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Skills;

namespace Zoxel.Actions
{
    //! Sets component on character depending on what skill is set in the action.
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class ActionSetSkillSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userSkillsQuery;
        private EntityQuery skillsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userSkillsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserSkill>(),
                ComponentType.Exclude<DestroyEntity>());
            skillsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Skill>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userSkillEntities = userSkillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var userSkillsMetaDatas = GetComponentLookup<MetaData>(true);
            userSkillEntities.Dispose();
            var skillEntities = skillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var skills = GetComponentLookup<Skill>(true);
            skillEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<EntityBusy>()
                .WithNone<InitializeEntity, DestroyEntity, ActivateAction>()
                .WithAll<SelectedActionUpdated>()
                .ForEach((Entity e, int entityInQueryIndex, ref UserActionLinks userActionLinks, in UserSkillLinks userSkillLinks) =>
            {
                var oldSkillType = userActionLinks.skillType;
                var target = userActionLinks.GetSelectedTarget();
                //UnityEngine.Debug.LogError("Changing Action of " + e.Index + " to " + userActionLinks.selectedSkillIndex);
                if (userSkillsMetaDatas.HasComponent(target))
                {
                    var userSkill = userSkillsMetaDatas[target];
                    var skillEntity = userSkill.data;
                    if (skillEntity.Index <= 0 || !skills.HasComponent(skillEntity))
                    {
                        return;
                    }
                    var skillType = skills[skillEntity].type;
                    if (oldSkillType != skillType)
                    {
                        RemoveSkillComponent(PostUpdateCommands, entityInQueryIndex, e, oldSkillType);
                        userActionLinks.skillType = skillType;
                        AddSkillComponent(PostUpdateCommands, entityInQueryIndex, e, skillType);
                        // UnityEngine.Debug.LogError("Set Skill to " + ((SkillType)skillType) + " at skill: " + userActionLinks.selectedSkillIndex);
                    }
                }
                else
                {
                    //UnityEngine.Debug.LogError("Removed Skill " + ((SkillType)oldSkillType));
                    if (oldSkillType != SkillType.None)
                    {
                        RemoveSkillComponent(PostUpdateCommands, entityInQueryIndex, e, oldSkillType);
                        userActionLinks.skillType = SkillType.None;
                    }
                }
            })  .WithReadOnly(userSkillsMetaDatas)
                .WithReadOnly(skills)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static void AddSkillComponent(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e, byte skillType)
        {
            if (skillType == SkillType.PhysicalAttack)
            {
                PostUpdateCommands.AddComponent<PhysicalAttack>(entityInQueryIndex, e);
            }
            else if (skillType == SkillType.Projectile)
            {
                PostUpdateCommands.AddComponent<Shooter>(entityInQueryIndex, e);
            }
            else if (skillType == SkillType.SummonTurret || skillType == SkillType.SummonMinion)
            {
                PostUpdateCommands.AddComponent<Summoner>(entityInQueryIndex, e);
            }
            else if (skillType == SkillType.Shield)
            {
                PostUpdateCommands.AddComponent<ShieldTrigger>(entityInQueryIndex, e);
            }
        }

        public static void RemoveSkillComponent(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e, byte skillType)
        {
            if (skillType == SkillType.PhysicalAttack)
            {
                PostUpdateCommands.RemoveComponent<PhysicalAttack>(entityInQueryIndex, e);
            }
            else if (skillType == SkillType.Projectile)
            {
                PostUpdateCommands.RemoveComponent<Shooter>(entityInQueryIndex, e);
            }
            else if (skillType == SkillType.SummonTurret || skillType == SkillType.SummonMinion)
            {
                PostUpdateCommands.RemoveComponent<Summoner>(entityInQueryIndex, e);
            }
            else if (skillType == SkillType.Shield)
            {
                PostUpdateCommands.RemoveComponent<ShieldTrigger>(entityInQueryIndex, e);
            }
        }
    }
}