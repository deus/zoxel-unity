using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Items;
using Zoxel.Skills;

namespace Zoxel.Actions
{
    //! Sets a tooltip based on action selected.
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class ActionSetTooltipSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllersQuery;
        private EntityQuery userItemsQuery;
        private EntityQuery userSkillsQuery;
        private NativeArray<Text> tooltips;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Player>(),
                ComponentType.ReadOnly<PlayerTooltipLinks>());
            userItemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserItem>(),
                ComponentType.Exclude<DestroyEntity>());
            userSkillsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserSkill>(),
                ComponentType.Exclude<DestroyEntity>());
            tooltips = new NativeArray<Text>(9, Allocator.Persistent);
            tooltips[0] = new Text("[trigger1] to Consume Food");
            tooltips[1] = new Text("[trigger1] to Place Voxel");
            tooltips[2] = new Text("[trigger1] to ???");
            tooltips[3] = new Text("[trigger1] to Attack");
            tooltips[4] = new Text("[trigger1] to Shoot");
            tooltips[5] = new Text("[trigger1] to Summon Turret");
            tooltips[6] = new Text("[trigger1] to Summon Minion");
            tooltips[7] = new Text("[trigger1] to Heal Self");
            tooltips[8] = new Text("[trigger1] to Assign");
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < tooltips.Length; i++)
            {
                tooltips[i].Dispose();
            }
            tooltips.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var tooltips = this.tooltips;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var playerTooltipLinks2 = GetComponentLookup<PlayerTooltipLinks>(true);
            controllerEntities.Dispose();
            var userItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var userItems = GetComponentLookup<UserItemQuantity>(true);
            var userItemMetaDatas = GetComponentLookup<MetaData>(true);
            userItemEntities.Dispose();
            var userSkillEntities = userSkillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var userSkills = GetComponentLookup<MetaData>(true);
            userSkillEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, EntityBusy,  DestroyEntity>()
                .WithNone<ActivateAction>()
                .WithAll<SelectedActionUpdated, HasActionTooltips>()
                .ForEach((Entity e, int entityInQueryIndex, in UserActionLinks userActionLinks, in ControllerLink controllerLink) =>
            {
                var controllerEntity = controllerLink.controller;
                var playerTooltipLinks = playerTooltipLinks2[controllerEntity];
                var playerTooltipText = new Text();
                var target = userActionLinks.GetSelectedTarget();
                // if melee attack
                // if selected character add to talk on
                // if shooting, say to shoot
                if (HasComponent<UserItem>(target))
                {
                    var selectedInventoryItem = userItems[target];
                    if (selectedInventoryItem.quantity > 0)
                    {
                        var userItemMetaEntity = userItemMetaDatas[target].data;
                        // check it type
                        if (HasComponent<ConsumableItem>(userItemMetaEntity))
                        {
                            playerTooltipText = tooltips[0]; // "[trigger1] to Consume Food";
                        }
                        else if (HasComponent<VoxelItem>(userItemMetaEntity))
                        {
                            playerTooltipText = tooltips[1]; // "[trigger1] to Place Voxel";
                            // todo: add voxel item name to tooltip
                        }
                        else
                        {
                            playerTooltipText = tooltips[2]; // "[trigger1] to ???";
                        }
                    }
                }
                else if (HasComponent<UserSkill>(target))
                {
                    var skillEntity = userSkills[target].data;
                    // shoot
                    if (HasComponent<SkillPhysicalAttack>(skillEntity)) // skillSelected.skillType == SkillType.PhysicalAttack)
                    {
                        playerTooltipText = tooltips[3]; // "[trigger1] to Attack";
                    }
                    else if (HasComponent<SkillProjectile>(skillEntity)) 
                    {
                        playerTooltipText = tooltips[4]; // "[trigger1] to Shoot";
                    }
                    else if (HasComponent<SkillSummonTurret>(skillEntity)) 
                    {
                        playerTooltipText = tooltips[5]; // "[trigger1] to Summon Turret";
                    }
                    else if (HasComponent<SkillSummonCharacter>(skillEntity)) 
                    {
                        playerTooltipText = tooltips[6]; // "[trigger1] to Summon Minion";
                    }
                    else if (HasComponent<SkillAugmentation>(skillEntity)) 
                    {
                        playerTooltipText = tooltips[7]; // "[trigger1] to Heal Self";
                    }
                    else
                    {
                        playerTooltipText = tooltips[2];
                    }
                }
                playerTooltipLinks.SetTooltipText(PostUpdateCommands, entityInQueryIndex, controllerEntity, playerTooltipText, 1);
            })  .WithReadOnly(userSkills)
                .WithReadOnly(userItems)
                .WithReadOnly(userItemMetaDatas)
                .WithReadOnly(playerTooltipLinks2)
                .WithReadOnly(tooltips)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}