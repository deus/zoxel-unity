using Unity.Entities;
using Unity.Burst;
using Zoxel.Items;

namespace Zoxel.Actions
{
    //! Handles OnUserItemPlaced for UserActionLinks's.
    /**
    *   During Inventory UI userActionLinks, these events handle target UserItem's of userActionLinks.
    */
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class ItemMovedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // Temporarily set userActionLinks with that target UserItem to empty.
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref UserActionLinks userActionLinks, in OnUserItemPickedUp onUserItemPickedUp) =>
            {
                PostUpdateCommands.RemoveComponent<OnUserItemPickedUp>(entityInQueryIndex, e);
                var userItemPickedUp = onUserItemPickedUp.userItemPickedUp;
                for (int i = 0; i < userActionLinks.actions.Length; i++)
                {
                    var action = userActionLinks.actions[i];
                    if (action.target == userItemPickedUp)
                    {
                        action.target = new Entity();
                        userActionLinks.actions[i] = action;
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // When mouse item closes - remember to add this event
            // Now set any that were placed with a new target
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref UserActionLinks userActionLinks, in OnUserItemPlaced onUserItemPlaced) =>
            {
                PostUpdateCommands.RemoveComponent<OnUserItemPlaced>(entityInQueryIndex, e);
                var userItemPlaced = onUserItemPlaced.userItemPlaced;
                for (int i = 0; i < userActionLinks.actions.Length; i++)
                {
                    var action = userActionLinks.actions[i];
                    if (action.targetMeta.Index != 0 && action.target.Index == 0)
                    {
                        action.target = userItemPlaced;
                        userActionLinks.actions[i] = action;
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // Temporarily set userActionLinks with that target UserItem to empty.
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref UserActionLinks userActionLinks, in OnUserItemSwapped onUserItemSwapped) =>
            {
                PostUpdateCommands.RemoveComponent<OnUserItemSwapped>(entityInQueryIndex, e);
                var userItemPickedUp = onUserItemSwapped.userItemPickedUp;
                var itemPickedUp = onUserItemSwapped.itemPickedUp;
                var lastUserItemPickedUp = onUserItemSwapped.lastUserItemPickedUp;
                var lastItemPickedUp = onUserItemSwapped.lastItemPickedUp;
                for (int i = 0; i < userActionLinks.actions.Length; i++)
                {
                    var action = userActionLinks.actions[i];
                    if (action.target == userItemPickedUp)
                    {
                        action.target = new Entity();
                        userActionLinks.actions[i] = action;
                    }
                }
                for (int i = 0; i < userActionLinks.actions.Length; i++)
                {
                    var action = userActionLinks.actions[i];
                    if (action.targetMeta == lastItemPickedUp && action.target.Index == 0)
                    {
                        action.target = userItemPickedUp;
                        userActionLinks.actions[i] = action;
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}