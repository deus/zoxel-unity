using Unity.Entities;
using Unity.Burst;
using Zoxel.Items;

namespace Zoxel.Actions
{
    //! Clears a users userActionLinks if they are tareting item''s.
    /**
    *   Added for after death of a character.
    */
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
	public partial class ClearItemActionsSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<ClearItemActions>()
                .ForEach((Entity e, int entityInQueryIndex, ref UserActionLinks userActionLinks) =>
            {
                PostUpdateCommands.RemoveComponent<ClearItemActions>(entityInQueryIndex, e);
                for (int i = 0; i < userActionLinks.actions.Length; i++)
                {
                    var target = userActionLinks.actions[i].target;
                    if (HasComponent<UserItem>(target))
                    {
                        userActionLinks.actions[i] = new Action();
                    }
                }
                PostUpdateCommands.AddComponent<SaveActions>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}