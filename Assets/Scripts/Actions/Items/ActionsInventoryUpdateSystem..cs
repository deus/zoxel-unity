using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Items;

namespace Zoxel.Actions
{
    //! Updates userActionLinks when inventory updates.
    /**
    *   \todo Make pickup item work only after the animation
    *   \todo Make trigger only if -Can- pickup in the first place - checking availability in entity
    *   \todo Kill item only if picked up - otherwise it will just stay - so i dont run out of places   
    */
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class ActionsInventoryUpdateSystem : SystemBase
    {        
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userItemsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userItemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserItem>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var userItems = GetComponentLookup<UserItem>(true);
            var userItemMetaDatas = GetComponentLookup<MetaData>(true);
            userItemEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity, SaveActions>()
                .ForEach((Entity e, int entityInQueryIndex, ref UserActionLinks userActionLinks, in OnInventoryUpdated onInventoryUpdated) =>
            {
                PostUpdateCommands.RemoveComponent<OnInventoryUpdated>(entityInQueryIndex, e);
                var actionsDirty = false;
                var updatedSelectedAction = false;
                var actionsUpdated = new NativeList<Entity>();
                var actionsUpdatedMeta = new NativeList<Entity>();
                var actionTextsUpdated = new NativeList<Entity>();
                var actionsTextsUpdatedMeta = new NativeList<Entity>();
                for (var i = 0; i < onInventoryUpdated.itemQuantitiesUpdated.Length; i++)
                {
                    var userItemEntity = onInventoryUpdated.itemQuantitiesUpdated[i];
                    var userItem = userItemMetaDatas[userItemEntity];
                    var userItemMeta = userItem.data;
                    var doesActionExist = false;
                    for (int j = 0; j < userActionLinks.actions.Length; j++)
                    {
                        var target = userActionLinks.actions[j].target;
                        var targetMeta = userActionLinks.actions[j].targetMeta;
                        if (userItems.HasComponent(target) && userItemMeta == targetMeta)
                        {
                            doesActionExist = true;
                            break;
                        }
                    }
                    if (doesActionExist)
                    {
                        if (!actionTextsUpdated.Contains(userItemEntity))
                        {
                            actionTextsUpdated.Add(userItemEntity);
                            var targetMeta = new Entity();
                            if (userItems.HasComponent(userItemEntity))
                            {
                                targetMeta = userItemMetaDatas[userItemEntity].data;
                            }
                            actionsTextsUpdatedMeta.Add(targetMeta);
                        }
                    }
                }
                for (var i = 0; i < onInventoryUpdated.itemsUpdated.Length; i++)
                {
                    var userItemEntity = onInventoryUpdated.itemsUpdated[i];
                    var userItem = userItemMetaDatas[userItemEntity];
                    var userItemMeta = userItem.data;
                    // --- Add as action ---
                    var doesActionExist = false;
                    for (int j = 0; j < userActionLinks.actions.Length; j++)
                    {
                        var target = userActionLinks.actions[j].target;
                        var targetMeta = userActionLinks.actions[j].targetMeta;
                        if (userItems.HasComponent(target) && userItemMeta == targetMeta)
                        {
                            doesActionExist = true;
                            break;
                        }
                    }
                    if (doesActionExist)
                    {
                        // if already exxists, just updated the text
                        if (!actionTextsUpdated.Contains(userItemEntity))
                        {
                            actionTextsUpdated.Add(userItemEntity);
                            var targetMeta = new Entity();
                            if (userItems.HasComponent(userItemEntity))
                            {
                                targetMeta = userItemMetaDatas[userItemEntity].data;
                            }
                            actionsTextsUpdatedMeta.Add(targetMeta);
                        }
                    }
                    else
                    {
                        var blankActionID = -1;
                        for (int j = 0; j < userActionLinks.actions.Length; j++)
                        {
                            var action = userActionLinks.actions[j];
                            if (action.target.Index == 0)
                            {
                                blankActionID = j;
                                break;
                            }
                        }
                        if (blankActionID != -1)
                        {
                            // UnityEngine.Debug.LogError("Adding action to " + j + ":" + worldItem.data.id + " e: " + e.Index);
                            actionsDirty = true;
                            var targetMeta = new Entity();
                            if (userItems.HasComponent(userItemEntity))
                            {
                                targetMeta = userItemMetaDatas[userItemEntity].data;
                            }
                            var newAction = new Action(userItemEntity, targetMeta);
                            userActionLinks.actions[blankActionID] = newAction;
                            if (!actionsUpdated.Contains(userItemEntity))
                            {
                                actionsUpdated.Add(userItemEntity);
                                actionsUpdatedMeta.Add(targetMeta);
                            }
                            if (blankActionID == userActionLinks.selectedAction)
                            {
                                updatedSelectedAction = true;
                            }
                        }
                    }
                }
                //! \todo Support multiple action updates at once!
                // can only do this once, instead add a component for many updated
                //for (int i = 0; i < actionsUpdated.Length; i++)
                if (actionsUpdated.Length > 0)
                {
                    // if is in userActionLinks then update userActionLinks
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ActionTargetUpdated(actionsUpdated[0], actionsUpdatedMeta[0]));
                }
                //for (int i = 0; i < actionTextsUpdated.Length; i++)
                if (actionTextsUpdated.Length > 0)
                {
                    // if is in userActionLinks then update userActionLinks
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ActionTargetQuantityUpdated(actionTextsUpdated[0], actionsTextsUpdatedMeta[0]));
                }
                if (updatedSelectedAction)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SelectedActionUpdated(userActionLinks.selectedAction));
                }
                if (actionsDirty && HasComponent<EntitySaver>(e))
                {
                    PostUpdateCommands.AddComponent<SaveActions>(entityInQueryIndex, e);
                }
                onInventoryUpdated.Dispose();
                actionsUpdated.Dispose();
                actionsUpdatedMeta.Dispose();
                actionTextsUpdated.Dispose();
                actionsTextsUpdatedMeta.Dispose();
            })  .WithReadOnly(userItems)
                .WithReadOnly(userItemMetaDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}