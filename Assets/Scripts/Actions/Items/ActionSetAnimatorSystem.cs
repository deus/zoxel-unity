using Unity.Entities;
using Unity.Burst;
using Zoxel.Animations;

namespace Zoxel.Actions
{
    //! Sets arm animation based on skill and item set, triggered by SelectedActionUpdated event.
    /**
    *   \todo Add Animation type to UserSkill and UserItem's
    */
    [BurstCompile, UpdateInGroup(typeof(ActionSystemGroup))]
    public partial class ActionSetAnimatorSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<EntityBusy>()
                .WithNone<InitializeEntity, DestroyEntity, ActivateAction>()
                .WithAll<SelectedActionUpdated>()
                .ForEach((ref UserActionLinks userActionLinks, ref Animator animator) =>
            {
                // animator states caused userActionLinks to be stuck
                // can change action when animator is not switching states
                // set this based on action
                // arm to be in attacking position if attack skill selected
                if (animator.setRightArmState == animator.rightArmState)
                {
                    var target = userActionLinks.GetSelectedTarget();
                    //! If ActionTarget is not UserItem or UserSkill then play Idle animation, otherwise play PreAttack.
                    if (target.Index == 0)
                    {
                        animator.setRightArmState = AnimationState.Idle;
                    }
                    else
                    {
                        animator.setRightArmState = AnimationState.PreAttack;
                    }
                }
            }).ScheduleParallel();
        }
    }
}