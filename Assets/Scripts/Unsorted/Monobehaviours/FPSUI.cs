using UnityEngine;
using UnityEngine.InputSystem;

namespace Zoxel
{
    //! A quick and dirty fps ui.
    public class FPSUI : UnityEngine.MonoBehaviour
    {
        public bool isDrawUI;
        private float deltaTime = 0.0f;
        public int fontSize = 34;
        public UnityEngine.Color fontColor = UnityEngine.Color.green;
        public UnityEngine.Font font;

        void Update()
        {
            if (UnityEngine.InputSystem.Keyboard.current == null)
            {
                return;
            }
            var keyboard = UnityEngine.InputSystem.Keyboard.current;
            // if (keyboard.backquoteKey.wasPressedThisFrame)
            if (keyboard.tabKey.wasPressedThisFrame)
            {
                isDrawUI = !isDrawUI;
            }
        }

        public void OnGUI()
        {
            if (!isDrawUI)
            {
                return;
            }
            if (font)
            {
                UnityEngine.GUI.skin.font = font;
            }
            UnityEngine.GUI.skin.label.fontSize = fontSize;
            UnityEngine.GUI.color = fontColor;
            deltaTime += (UnityEngine.Time.unscaledDeltaTime - deltaTime) * 0.1f;
            UnityEngine.GUILayout.Label(GetFPSText());
        }

        private string GetFPSText()
        {
            var msec = deltaTime * 1000.0f;
            var fps = 1.0f / deltaTime;
            return string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
        }
    }
}