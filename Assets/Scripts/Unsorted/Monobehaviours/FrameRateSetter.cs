using UnityEngine;

namespace Zoxel
{
    public partial class FrameRateSetter : MonoBehaviour
    {
        public int targetFrameRate;

        void Start()
        {
            if (targetFrameRate != 0)
            {
                UnityEngine.Application.targetFrameRate = targetFrameRate;
            }
        }
    }
}