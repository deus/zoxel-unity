/*#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Experimental.Rendering;
using System;
using System.IO;
using System.Collections;
using System.Reflection;

namespace Zoxel
{
    //! Some helper funcctions for the editor game view.
    public static class GameViewUtility
    {
        private static Vector2 GetGameViewScale()
        {
            Type gameViewType = GetGameViewType();
            UnityEditor.EditorWindow gameViewWindow = GetGameViewWindow(gameViewType);
    
            if (gameViewWindow == null)
            {
                Debug.LogError("GameView is null!");
                return new Vector2();
            }
            var defScaleField = gameViewType.GetField("m_defaultScale", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            //whatever scale you want when you click on play
            // float defaultScale = 0.1f;
            var areaField = gameViewType.GetField("m_ZoomArea", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            var areaObj = areaField.GetValue(gameViewWindow);
            var scaleField = areaObj.GetType().GetField("m_Scale", System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic);
            // scaleField.SetValue(areaObj, new Vector2(defaultScale, defaultScale));
            return (Vector2) scaleField.GetValue(areaObj);
        }
 
        private static Type GetGameViewType()
        {
            Assembly unityEditorAssembly = typeof(UnityEditor.EditorWindow).Assembly;
            Type gameViewType = unityEditorAssembly.GetType("UnityEditor.GameView");
            return gameViewType;
        }
    
        private static UnityEditor.EditorWindow GetGameViewWindow(Type gameViewType)
        {
            UnityEngine.Object[] obj = Resources.FindObjectsOfTypeAll(gameViewType);
            if (obj.Length > 0)
            {
                return obj[0] as UnityEditor.EditorWindow;
            }
            return null;
        }

        public static int2 GetGameViewSize()
        {
            var gameviewCamera = Camera.main;
            Debug.Log(gameviewCamera.pixelRect);
            // return new int2(gameviewCamera.pixelWidth, gameviewCamera.pixelHeight);
            return new int2(gameviewCamera.scaledPixelWidth, gameviewCamera.scaledPixelHeight);
        }
    }
}
#endif*/
            /*System.Type gameView = System.Type.GetType("UnityEditor.GameView, UnityEditor");
            System.Reflection.MethodInfo camera = T.GetMethod("camera", System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Static);
            
            var camera = gameView.camera;*/

            /*System.Reflection.MethodInfo GetSizeOfMainGameView = T.GetMethod("GetSizeOfMainGameView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
            System.Object res = GetSizeOfMainGameView.Invoke(null, null);
            var res2 = (Vector2) res;*/

            /*var sceneView = UnityEditor.GameView.lastActiveSceneView;
            // UnityEngine.Debug.LogError("Scene View Position: " + sceneView.position);
            //UnityEngine.Debug.LogError("titleContent height: " + sceneView.titleContent.image.height);
            var sceneViewCamera = sceneView.camera;
            pointer.x -= sceneView.position.x;
            pointer.y = sceneViewCamera.pixelHeight - pointer.y;
            pointer.y += sceneView.position.y;
            // comes to about 136
            pointer.y += sceneView.titleContent.image.height;
            pointer.y += 30;   // this represents the bar on top of sceneview*/

            /*System.Type T = System.Type.GetType("UnityEditor.GameView, UnityEditor");
            System.Reflection.MethodInfo GetSizeOfMainGameView = T.GetMethod("GetSizeOfMainGameView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
            System.Object res = GetSizeOfMainGameView.Invoke(null, null);
            var res2 = (Vector2) res;
            return new int2(res2.x, res2.y);*/