﻿using Unity.Entities;
using System.Collections.Generic;
using Zoxel.Transforms;

namespace Zoxel.Cameras
{
    //! References of cameras used for billboard and other scripts.
    public static class CameraReferences
    {
        public static List<Entity> cameras = new List<Entity>();
        public static List<Entity> firstPersonCameras = new List<Entity>();

        public static Entity GetMainCameraEntity(EntityManager EntityManager)
        {
            foreach (var cameraEntity in cameras)
            {
                if (EntityManager.GetComponentData<CameraEnabledState>(cameraEntity).state == 0)
                {
                    continue;
                }
                return cameraEntity;
            }
            return new Entity();
        }

        public static UnityEngine.Camera GetMainCamera(EntityManager EntityManager)
        {
            foreach (var cameraEntity in cameras)
            {
                if (EntityManager.GetComponentData<CameraEnabledState>(cameraEntity).state == 0)
                {
                    continue;
                }
                var cameraObject = EntityManager.GetSharedComponentManaged<GameObjectLink>(cameraEntity).gameObject;
                if (cameraObject == null)
                {
                    return null;
                }
                return cameraObject.GetComponent<UnityEngine.Camera>();
            }
            return null;
        }
    }
}


        //public static Dictionary<int, UnityEngine.GameObject> cameras = new Dictionary<int, UnityEngine.GameObject>();

        // used for camera facer system
        /*public static UnityEngine.Camera GetMainCamera()
        {
            foreach (var camera in cameras.Values)
            {
                return camera.GetComponent<UnityEngine.Camera>();
            }
            return null;
        }*/

        /*public static UnityEngine.Transform GetMainCameraTransform()
        {
            foreach (var camera in cameras.Values)
            {
                return camera.transform;
            }
            if (UnityEngine.Camera.main)
            {
                return UnityEngine.Camera.main.transform;
            }
            return null;
        }*/