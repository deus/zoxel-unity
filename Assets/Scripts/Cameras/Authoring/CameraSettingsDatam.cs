using UnityEngine;

namespace Zoxel.Cameras
{
    [CreateAssetMenu(fileName = "CameraSettings", menuName = "Zoxel/Settings/CameraSettings")]
    public partial class CameraSettingsDatam : ScriptableObject
    {
        [Header("Settings")]
        public CameraSettings cameraSettings;

        [Header("Prefabs")]
        public GameObject playerCameraPrefab;
        public GameObject firstPersonCameraPrefab;
        public GameObject voxelViewerCameraPrefab;
        public GameObject characterViewerCameraPrefab;
        public GameObject portalCameraPrefab;
    }
}