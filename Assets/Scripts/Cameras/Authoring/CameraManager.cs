using UnityEngine;
using UnityEngine.Rendering;    // post processing
using Unity.Mathematics;

namespace Zoxel.Cameras
{
    //! Manages camera settings and things
    /**
    *   \todo Remove gameobject use, just import settings from CameraSettings data. Create UnityEngine / GameObject prefab in systems based on that data.
    */
    public partial class CameraManager : MonoBehaviour
    {
        public static CameraManager instance;
        [Header("Data")]  
        [SerializeField] public CameraSettingsDatam CameraSettings;
        [HideInInspector] public CameraSettings realCameraSettings;

        public CameraSettings cameraSettings
        {
            get
            { 
                return realCameraSettings;
            }
        }

        public void Awake()
        {
            instance = this;
            realCameraSettings = CameraSettings.cameraSettings;
            if (CameraSettings.playerCameraPrefab)
            {
                var unityCamera = CameraSettings.playerCameraPrefab.GetComponent<UnityEngine.Camera>();
                this.realCameraSettings.mainMenuBackgroundColor = new Color(unityCamera.backgroundColor);
                this.realCameraSettings.fieldOfView = unityCamera.fieldOfView;
                if (CameraSettings.playerCameraPrefab.activeSelf)
                {
                    CameraSettings.playerCameraPrefab.SetActive(false);
                }
            }
            if (CameraSettings.firstPersonCameraPrefab && CameraSettings.firstPersonCameraPrefab.activeSelf)
            {
                CameraSettings.firstPersonCameraPrefab.SetActive(false);
            }
        }
    }
}

// [HideInInspector] public FirstPersonCameraSettings realFirstPersonCameraSettings;
// [HideInInspector] public MouseSettings realMouseSettings;
// realFirstPersonCameraSettings = CameraSettings.firstPersonCameraSettings;
// realMouseSettings = CameraSettings.mouseSettings;
/*public FirstPersonCameraSettings firstPersonCameraSettings
{
    get
    { 
        return realFirstPersonCameraSettings;
    }
}*/

/*public MouseSettings mouseSettings
{
    get
    { 
        return realMouseSettings;
    }
}*/