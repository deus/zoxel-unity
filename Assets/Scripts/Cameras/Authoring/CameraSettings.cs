using System;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Cameras
{
    //! Settins for our camera data.
    [Serializable]
    public struct CameraSettings
    {
        [Header("Main Camera")]
        public float uiDepth;               // 0.1f;

        [Header("Top Down")]
        public bool isTopDownCamera;    // later replace with cameraType

        [Header("Viewers")]
        public int characterViewerResolution;   // 256
        public int voxelViewerResolution;   // 32

        // hiddens
        [HideInInspector] public Color mainMenuBackgroundColor;
        [HideInInspector] public float fieldOfView; // = 90f;
    }
}

/*public void DrawGUI(EntityManager EntityManager, Entity game) 
{

}*/
// public float3 spawnCameraPosition;  // 0?