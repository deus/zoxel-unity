#if UNITY_RP
using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace Zoxel.Cameras.PostProcessing
{
    //! Pushes FieldOfView data to unity camera component.
    [BurstCompile, UpdateInGroup(typeof(PostProcessingSystemGroup))]
    public partial class PostExposureUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<InitializeEntity>()
                .WithChangeFilter<PostExposure>()
                .ForEach((in PostExposure postExposure, in GameObjectLink gameObjectLink) =>
            {
                Update(gameObjectLink.gameObject, postExposure.postExposure);
            }).WithoutBurst().Run();
            Entities
                .WithNone<InitializeEntity>()
                .WithChangeFilter<GameObjectLink>()
                .ForEach((in PostExposure postExposure, in GameObjectLink gameObjectLink) =>
            {
                Update(gameObjectLink.gameObject, postExposure.postExposure);
            }).WithoutBurst().Run();
        }

        private void Update(UnityEngine.GameObject gameObject, float postExposure)
        {
            if (gameObject == null)
            {
                return;
            }
            var volume = gameObject.GetComponent<Volume>();
            if (volume == null)
            {
                return;
            }
            ColorAdjustments colorAdjustment;
            if (volume.profile.TryGet<ColorAdjustments>(out colorAdjustment))
            {
                colorAdjustment.postExposure.value = postExposure;
            }
        }
    }
}
#endif