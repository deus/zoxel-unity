#if UNITY_RP
using Unity.Entities;
using Zoxel.Transforms;
using UnityEngine.Rendering;

namespace Zoxel.Cameras.PostProcessing
{
    //! Sets post processing profile based on a byte.
    [UpdateInGroup(typeof(PostProcessingSystemGroup))]
    public partial class PostProcessingUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (PostProcessingManager.instance == null)
            {
                return;
            }
            var gameProfile = PostProcessingManager.instance.postProcessingDatam.gameProfile;
            var pauseProfile = PostProcessingManager.instance.postProcessingDatam.pauseProfile;
            var menuProfile = PostProcessingManager.instance.postProcessingDatam.menuProfile;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<InitializeEntity>()
                .ForEach((Entity e, in UpdatePostProcessing updatePostProcessing, in GameObjectLink gameObjectLink) =>
            {
                PostUpdateCommands.RemoveComponent<UpdatePostProcessing>(e);
                var cameraObject = gameObjectLink.gameObject;
                if (cameraObject == null)
                {
                    return;
                }
                var cameraVolume = cameraObject.GetComponent<Volume>();
                if (cameraVolume == null)
                {
                    return;
                }
                // UnityEngine.Debug.LogError("Setting Post Processing to: " + updatePostProcessing.type);
                if (updatePostProcessing.type == PostProcessingProfileType.MainMenu)
                {
                    cameraVolume.profile = menuProfile;
                }
                else if (updatePostProcessing.type == PostProcessingProfileType.Game)
                {
                    cameraVolume.profile = gameProfile;
                }
                else if (updatePostProcessing.type == PostProcessingProfileType.Paused)
                {
                    cameraVolume.profile = pauseProfile;
                }
            }).WithoutBurst().Run();
        }
    }
}
#endif