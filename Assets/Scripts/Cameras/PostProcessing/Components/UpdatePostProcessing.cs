using Unity.Entities;

namespace Zoxel.Cameras.PostProcessing
{
    public struct UpdatePostProcessing : IComponentData
    {
        public byte type;

        public UpdatePostProcessing(byte type)
        {
            this.type = type;
        }
    }
}