using Unity.Entities;

namespace Zoxel.Cameras.PostProcessing
{
    // Attached to Camera
    public struct PostExposure : IComponentData
    {
        public float postExposure;

        public PostExposure(float postExposure)
        {
            this.postExposure = postExposure;
        }
    }
}