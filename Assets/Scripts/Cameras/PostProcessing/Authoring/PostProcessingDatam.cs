using UnityEngine;
using UnityEngine.Rendering;    // post processing

namespace Zoxel.Cameras
{
    [CreateAssetMenu(fileName = "PostProcessingSettings", menuName = "Zoxel/Settings/PostProcessing")]
    public partial class PostProcessingDatam : ScriptableObject
    {
        #if UNITY_RP
        [Header("Profiles")]
        public VolumeProfile menuProfile;
        public VolumeProfile gameProfile;
        public VolumeProfile pauseProfile;
        #endif
    }
}