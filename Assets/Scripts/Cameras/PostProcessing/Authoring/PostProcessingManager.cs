using UnityEngine;

namespace Zoxel.Cameras.PostProcessing
{
    //! Holds post processing data
    public partial class PostProcessingManager : MonoBehaviour
    {
        public static PostProcessingManager instance;
        [SerializeField] public PostProcessingDatam postProcessingDatam;

        public void Awake()
        {
            instance = this;
        }
    }
}