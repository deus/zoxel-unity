using Unity.Entities;

namespace Zoxel.Cameras.PostProcessing
{
    //! Handles Cameras and UIs based on that.
    [UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class PostProcessingSystemGroup : ComponentSystemGroup { }
}