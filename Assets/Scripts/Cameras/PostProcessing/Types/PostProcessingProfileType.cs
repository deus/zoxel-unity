namespace Zoxel.Cameras.PostProcessing
{
    //! Which post processing profile to switch to.
    public static class PostProcessingProfileType
    {
        public const byte MainMenu = 0;
        public const byte Game = 1;
        public const byte Paused = 2;
    }
}