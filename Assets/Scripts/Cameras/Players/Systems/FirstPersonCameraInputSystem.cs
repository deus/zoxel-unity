﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

using Zoxel.Transforms;
using Zoxel.Input;
using Zoxel.Cameras.Authoring;

namespace Zoxel.Cameras.Input
{
    //! Move the first person camera around with player input.
    /**
    *   Sets camera input to characters one, sets characters angle to camera one?
    *   \todo This has issues during lerping due to boundary issues. And until fully rotated it will have issues rotating around Y axis.
    *   \todo Usses gravityQuadrant.rotation in another system that works on npcs too.
    *   \todo When die - set angle to along ground - drip blood down screen
    */
    [BurstCompile, UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class FirstPersonCameraInputSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery controllerQuery;
        private EntityQuery charactersQuery;

        protected override void OnCreate()
        {
            controllerQuery = GetEntityQuery(
                ComponentType.Exclude<ControllerDisabled>(),
                ComponentType.ReadOnly<Controller>(),
                ComponentType.ReadOnly<CameraLink>());
            controllerQuery.SetChangedVersionFilter(typeof(Controller));
            charactersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                //ComponentType.Exclude<RotateTowards>(),
                ComponentType.ReadOnly<Character>(),
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadWrite<Rotation>());
            RequireForUpdate<MouseSettings>();
            RequireForUpdate(processQuery);
            RequireForUpdate(controllerQuery);
            RequireForUpdate(charactersQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var mouseSettings = GetSingleton<MouseSettings>();
            var rotationBoundsX = mouseSettings.rotationBoundsX;
            var mouseSpeed = mouseSettings.mouseSpeed;
            var gamepadSpeed = mouseSettings.gamepadCameraSpeed;
            var degreesToRadians = ((math.PI * 2) / 360f);
            var deltaTime = World.Time.DeltaTime;
            var characterEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var rotations = GetComponentLookup<Rotation>(false);
            var gravityQuadrants = GetComponentLookup<GravityQuadrant>(true);
            characterEntities.Dispose();
            var controllerEntities = controllerQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var characterLinks = GetComponentLookup<CharacterLink>(true);
            var controllers = GetComponentLookup<Controller>(true);
            var deviceTypeDatas = GetComponentLookup<DeviceTypeData>(true);
            controllerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DisableFirstPersonCamera, DetatchCamera>() // , RotateTowards>()   // RotateTowards
                .WithAll<FirstPersonCamera>()
                .ForEach((Entity e, ref LocalRotation localRotation, in ControllerLink controllerLink) =>
            {
                if (HasComponent<ControllerDisabled>(controllerLink.controller))
                {
                    return;
                }
                var controllerEntity = controllerLink.controller;
                if (!characterLinks.HasComponent(controllerEntity))
                {
                    return;
                }
                var characterEntity = characterLinks[controllerEntity].character;
                if (!rotations.HasComponent(characterEntity)) // || HasComponent<RotateTowards>()
                {
                    return;
                }
                var controller = controllers[controllerEntity];
                var deviceTypeData = deviceTypeDatas[controllerEntity];
                byte isGamepadUse = 0;
                var movementSpeed = float2.zero;
                var rightStick = float2.zero;
                if (controller.mapping == ControllerMapping.InGame)
                {
                    // another system to set stick
                    if (deviceTypeData.type == DeviceType.Gamepad)
                    {
                        movementSpeed = gamepadSpeed;
                        isGamepadUse = 1;
                    }
                    else
                    {
                        movementSpeed = mouseSpeed;
                        isGamepadUse = 0;
                    }
                    rightStick = controller.gamepad.rightStick;
                }
                var cameraInput = new float2(-rightStick.y, rightStick.x);
                var cameraInput2 = 0f;
                var characterInput = 0f;
                if (isGamepadUse == 1)
                {
                    cameraInput2 += cameraInput.x * movementSpeed.y * deltaTime;
                    characterInput += cameraInput.y * movementSpeed.x * deltaTime;
                }
                else
                {
                    cameraInput2 += cameraInput.x * movementSpeed.y;
                    characterInput += cameraInput.y * movementSpeed.x;
                }
                if (cameraInput2 != 0)
                {
                    localRotation.rotation = math.mul(localRotation.rotation, quaternion.EulerXYZ(new float3(cameraInput2, 0, 0) * degreesToRadians));
                    // limit camera rotation
                    var rollRotation = localRotation.rotation.ToEulerX();
                    if (rotationBoundsX.x != 0 && rollRotation < rotationBoundsX.x * degreesToRadians)
                    {
                        rollRotation = rotationBoundsX.x * degreesToRadians;
                    }
                    else if (rotationBoundsX.y != 0 && rollRotation > rotationBoundsX.y * degreesToRadians)
                    {
                        rollRotation = rotationBoundsX.y * degreesToRadians;
                    }
                    localRotation.rotation = quaternion.EulerXYZ(new float3(rollRotation, 0, 0)); //  / degreesToRadians);
                }
                // UnityEngine.Debug.LogError("axisAngle: " + axisAngle);
                if (characterInput != 0)
                {
                    var gravityQuadrant = gravityQuadrants[characterEntity];
                    var upDirection = new float3(0, 1, 0);
                    // var upDirection = gravityQuadrant.GetUpDirection();
                    var characterRotation = rotations[characterEntity];
                    characterRotation.Value  = math.mul(characterRotation.Value, quaternion.EulerXYZ(upDirection * characterInput * degreesToRadians));
                    rotations[characterEntity] = characterRotation;
                }
            })  .WithReadOnly(controllers)
                .WithReadOnly(deviceTypeDatas)
                .WithReadOnly(characterLinks)
                .WithReadOnly(gravityQuadrants)
                .WithNativeDisableContainerSafetyRestriction(rotations)
                .ScheduleParallel(Dependency);
        }
    }
}