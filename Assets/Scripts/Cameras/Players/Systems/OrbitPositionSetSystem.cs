using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Cameras;

// using Zoxel.Cameras.Players
namespace Zoxel.Transforms.Orbit
{
    //! Positions the UI to face the camera in an orbit position.
    /**
    *   Updates after all parent transforms are updated as Camera uses those.
    *   Then it needs to move any children uis of the Orbit parents.
    */
    /*[UpdateBefore(typeof(ParentPositionSystem))]
    [BurstCompile, UpdateInGroup(typeof(Zoxel.Transforms.TransformSystemGroup))]
    public partial class OrbitPositionSetSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery cameraQuery;
        private EntityQuery panelUIQuery;

        protected override void OnCreate()
        {
            cameraQuery = GetEntityQuery(ComponentType.ReadOnly<Camera>());
            panelUIQuery = GetEntityQuery(ComponentType.ReadOnly<PanelUI>());
            RequireForUpdate(processQuery);
            RequireForUpdate(cameraQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var cameraEntities = cameraQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var cameras = GetComponentLookup<Camera>(true);
            cameraEntities.Dispose();
            var panelEntities = panelUIQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var panelPositions = GetComponentLookup<PanelPosition>(true);
            var anchorUIs = GetComponentLookup<UIAnchor>(true);
            panelEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<OrbitTransform>()
                .ForEach((Entity e, ref OrbitPosition orbitPosition, in CameraLink cameraLink, in UIPosition uiPosition, in Size2D size2D) =>
            {
                var cameraEntity = cameraLink.camera;
                if (cameraEntity.Index == 0 || !cameras.HasComponent(cameraEntity))
                {   
                    return;
                }
                var camera = cameras[cameraEntity];
                if (camera.aspectRatio == 0)
                {   
                    return;
                }
                // Set Position
                var panelSize = size2D.size;
                var anchor = (byte) 0;          // panelUI.anchor;
                var panelPosition = float3.zero;  // panelUI.anchor;
                if (HasComponent<RenderText>(e))
                {
                    panelSize = float2.zero;
                }
                if (anchorUIs.HasComponent(e))
                {
                    anchor = anchorUIs[e].anchor;
                }
                if (panelPositions.HasComponent(e))
                {
                    panelPosition = panelPositions[e].GetPositionOffset(panelSize, anchor);
                }
                else
                {
                    panelPosition = PanelPosition.GetPosition(panelSize, anchor);
                }
                var orbitPosition2 = AnchorPresets.GetOrbitAnchors(anchor, uiPosition.position);
                var frustrumSize = camera.GetFrustrumSize();
                orbitPosition.position = new float3(orbitPosition2.x * frustrumSize.x, -orbitPosition2.y * frustrumSize.y, orbitPosition2.z) + panelPosition;
			})  .WithReadOnly(panelPositions)
                .WithReadOnly(anchorUIs)
                .WithReadOnly(cameras)
                .WithDisposeOnCompletion(panelPositions)
                .WithDisposeOnCompletion(anchorUIs)
                .WithDisposeOnCompletion(cameras)
                .ScheduleParallel(Dependency);
        }
    }*/
}