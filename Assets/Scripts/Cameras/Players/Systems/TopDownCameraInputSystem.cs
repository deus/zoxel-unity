﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Input;
using Zoxel.Cameras.TopDown;

namespace Zoxel.Cameras.Input
{
    //! Input for our TopDownCamera..
    [BurstCompile, UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class TopDownCameraInputSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery controllersQuery;

        protected override void OnCreate()
        {
            controllersQuery = GetEntityQuery(
                // ComponentType.ReadOnly<Player>(),
                ComponentType.ReadOnly<Controller>());
            RequireForUpdate(processQuery);
            RequireForUpdate(controllersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var movementBuff = World.Time.DeltaTime * 10f;
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var controllers = GetComponentLookup<Controller>(true);
            controllerEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<TopDownCamera>()
                .ForEach((ref Translation translation, in Rotation rotation, in ControllerLink controllerLink) =>
            {
                if (!controllers.HasComponent(controllerLink.controller))
                {
                    return;
                }
                var controller = controllers[controllerLink.controller];
                var movement = float3.zero;
                movement.x = controller.gamepad.leftStick.x * movementBuff;
                movement.y = controller.gamepad.leftStick.y * movementBuff;
                /*movement = math.rotate(-rotation.Value.ToEulerY(), movement);
                var movement2 = float3.zero;
                movement2.z = controller.gamepad.leftStick.y * movementBuff;
                movement2 = math.rotate(-rotation.Value, movement2);
                movement += movement2;*/
                // var rotation2 = quaternion.EulerXYZ(new float3(90, 0, 0) * degreesToRadians);
                //var rotation2 = quaternion.EulerXYZ(new float3(90, -45, 0) * degreesToRadians);
                //movement = math.rotate(rotation2, math.rotate(rotation.Value.value, movement));

                movement = math.rotate(rotation.Value.value, movement);

                movement.y = 0f;
                if (controller.mouse.scroll.y > 0)
                {
                    movement.y = 1f;
                }
                else if (controller.mouse.scroll.y < 0)
                {
                    movement.y = -1f;
                }
                // movement.y = controller.gamepad.rightStick.y;
                translation.Value += movement;
                if (controller.keyboard.spaceKey.wasPressedThisFrame == 1)
                {
                    translation.Value = new float3(0, 96, 0);
                }
            })  .WithReadOnly(controllers)
                .ScheduleParallel();
        }
    }
}