using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Cameras
{
    //! Used to calculate screen dimensions.
    public struct FirstPersonCameraOffset : IComponentData
    {
        public float3 localPosition;

        public FirstPersonCameraOffset(float3 localPosition)
        {
            this.localPosition = localPosition;
        }
    }
}