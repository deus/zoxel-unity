using Unity.Entities;

namespace Zoxel.Cameras
{
    public struct CameraEnabledState : IComponentData
    {
        public byte state;

        public CameraEnabledState(byte state)
        {
            this.state = state;
        }
    }
}