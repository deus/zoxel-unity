using Unity.Entities;
using System;

namespace Zoxel.Cameras
{
    //! Links to a Render texture on a PortalCamera.
    public struct RenderTextureLink : ISharedComponentData, IEquatable<RenderTextureLink>
    {
        public UnityEngine.RenderTexture renderTexture;

        public RenderTextureLink(UnityEngine.RenderTexture renderTexture)
        {
            this.renderTexture = renderTexture;
        }

        public bool Equals(RenderTextureLink other)
        {
            return renderTexture == other.renderTexture;
        }
        
        public override int GetHashCode()
        {
            return renderTexture.GetHashCode();
        }
    }
}