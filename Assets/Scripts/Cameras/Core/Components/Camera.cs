using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Cameras
{
    //! Attached to all camera entities.
    /**
    *   Has screen dimensions, fov, depth, aspect ratio and a calculated frustrum.
    */
    public struct Camera : IComponentData
    {
        public int2 screenDimensions;
        public float depth;
        public float aspectRatio;
        public float2 frustrum;

        public Camera(int2 screenDimensions, float depth, float fov)
        {
            this.screenDimensions = screenDimensions;
            this.depth = depth;
            this.frustrum = float2.zero;
            this.aspectRatio = 1;
            CalculateAspectRatio();
            CalculateFrustrum(fov);
        }

        public void SetScreenDimensions(int2 screenDimensions2, in CameraScreenRect cameraScreenRect, float fov)
        {
            var screenDimensions = new int2(
                (int) (cameraScreenRect.rect.size.x * screenDimensions2.x),
                (int) (cameraScreenRect.rect.size.y * screenDimensions2.y));
            SetScreenDimensions(screenDimensions, fov);
        }

        public void SetScreenDimensions(int2 screenDimensions, float fov)
        {
            if (this.screenDimensions != screenDimensions)
            {
                this.screenDimensions = screenDimensions;
                CalculateAspectRatio();
                CalculateFrustrum(fov);
            }
        }

        //! Returns frustrum.
        public float2 GetFrustrumSize() 
        {
            return frustrum;
        }

        //! Calculates aspectRatio based on screenDimensions.
        public void CalculateAspectRatio()
        {
            aspectRatio = (screenDimensions.x) / (float)(screenDimensions.y);
        }

        //! Calculates frustrum based on fov, aspectRatio & depth.
        public void CalculateFrustrum(float fov)
        {
            frustrum.y = depth * 2.0f * math.tan(math.radians(fov) * 0.5f);
            frustrum.x = frustrum.y * aspectRatio;
        }
    }
}