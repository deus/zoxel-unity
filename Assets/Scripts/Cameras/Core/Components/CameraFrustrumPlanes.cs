using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Cameras
{
    //! Attached to all camera entities.
    public struct CameraFrustrumPlanes : IComponentData
    {
        public BlitableArray<UnityEngine.Plane> planes;

        public void Dispose()
        {
            planes.Dispose();
        }
    }
}