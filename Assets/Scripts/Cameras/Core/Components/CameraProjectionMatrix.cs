using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Cameras
{
    //! The projection matrix of a camera.
    /**
    *   \todo Calculate projection matrix manually for jobs.
    *   https://forum.unity.com/threads/calculating-world-to-screen-projection-manully.1197199/
    */
    public struct CameraProjectionMatrix : IComponentData
    {
        public float4x4 projectionMatrix;
    }
}