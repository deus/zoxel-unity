using Unity.Entities;

namespace Zoxel.Cameras
{
    public struct DisableFirstPersonCamera : IComponentData { }
}