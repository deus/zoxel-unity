using Unity.Entities;

namespace Zoxel.Cameras
{
    //! A camera that is free.
    public struct FreeCamera : IComponentData { }
}