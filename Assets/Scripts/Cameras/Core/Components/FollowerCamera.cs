﻿using Unity.Entities;

namespace Zoxel.Cameras
{
    //! A camera that follows around something
    public struct FollowerCamera : IComponentData
    {
        public int characterID;
        public FollowCameraData Value;
    }
}