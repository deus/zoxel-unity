using Unity.Entities;

namespace Zoxel.Cameras
{
    //! Field of view property for the camera.
    public struct FieldOfView : IComponentData
    {
        public float fov;

        public FieldOfView(float fov)
        {
            this.fov = fov;
        }
    }
}