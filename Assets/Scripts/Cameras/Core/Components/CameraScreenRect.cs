using Unity.Entities;

namespace Zoxel.Cameras
{
    //! Used to calculate screen dimensions.
    public struct CameraScreenRect : IComponentData
    {
        public Rect rect;

        public CameraScreenRect(Rect rect)
        {
            this.rect = rect;
        }
    }
}