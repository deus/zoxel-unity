using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Cameras
{
    //! Detatches when ends game?
    [BurstCompile, UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class CameraDetatchSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DetatchCamera>()
                .ForEach((Entity e, int entityInQueryIndex, ref ParentLink parentLink, ref CharacterLink characterLink, ref LocalPosition localPosition,
                    ref Translation translation, ref Rotation rotation) =>
            {
                localPosition.position = float3.zero;
                parentLink.parent = new Entity();
                characterLink.character = new Entity();
                translation.Value = float3.zero;
                rotation.Value = quaternion.identity;
                PostUpdateCommands.RemoveComponent<DetatchCamera>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}