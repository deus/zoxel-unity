using Unity.Mathematics;
using System;

namespace Zoxel.Cameras
{
    [Serializable]
    public struct FollowCameraData
    {
        public float2 lerpSpeed;
        public float3 cameraAddition;
        public float3 cameraRotation;
        public float3 targetPosition;
        public quaternion targetRotation;
    }
}