using Unity.Entities;

namespace Zoxel.Cameras
{
    //! An event that spawns a free camera.
    public struct TransitionCamera : IComponentData
    {
        public Entity beforeCamera;
        public Entity afterCamera;

        public TransitionCamera(Entity beforeCamera, Entity afterCamera)
        {
            this.beforeCamera = beforeCamera;
            this.afterCamera = afterCamera;
        }
    }
}