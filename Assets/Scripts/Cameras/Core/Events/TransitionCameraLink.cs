using Unity.Entities;

namespace Zoxel.Cameras
{
    //! An event that spawns a free camera.
    public struct TransitionCameraLink : IComponentData
    {
        public Entity camera;
        public double elapsedTime;
        public float transitionTime;

        public TransitionCameraLink(Entity camera, double elapsedTime, float transitionTime)
        {
            this.camera = camera;
            this.elapsedTime = elapsedTime;
            this.transitionTime = transitionTime;
        }
    }
}