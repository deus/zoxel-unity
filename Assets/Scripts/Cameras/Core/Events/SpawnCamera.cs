using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Cameras
{
    //! An event that spawns a free camera.
    public struct SpawnCamera : IComponentData
    {
        public float3 position;
        public quaternion rotation;

        public SpawnCamera(float3 position)
        {
            this.position = position;
            this.rotation = quaternion.identity;
        }

        public SpawnCamera(float3 position, quaternion rotation)
        {
            this.position = position;
            this.rotation = rotation;
        }
    }
}