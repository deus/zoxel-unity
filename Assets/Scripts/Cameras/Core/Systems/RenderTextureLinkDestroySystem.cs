using Unity.Entities;

namespace Zoxel.Cameras
{
    //! Cleans up game objects connected to an Entity.
    [UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class RenderTextureLinkDestroySystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, RenderTextureLink>()
                .ForEach((in RenderTextureLink renderTextureLink) =>
            {
                if (renderTextureLink.renderTexture != null)
                {
                    ObjectUtil.Destroy(renderTextureLink.renderTexture);
                }
            }).WithoutBurst().Run();
        }
    }
}