using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;

namespace Zoxel.Cameras
{
    //! Pushes FieldOfView data to unity camera component.
    [BurstCompile, UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class CameraFieldOfViewSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            Dependency = Entities
                .WithNone<ViewerCamera>()
                .WithChangeFilter<FieldOfView>()
                // .WithAll<InitializeEntity, Camera>()
                .ForEach((ref Camera camera, in FieldOfView fieldOfView) =>
            {
                camera.SetScreenDimensions(new int2(camera.screenDimensions.x, camera.screenDimensions.y / 2), fieldOfView.fov);
            }).ScheduleParallel(Dependency);
            Entities
                .WithChangeFilter<FieldOfView>()
                .WithNone<InitializeEntity>()
                .ForEach((in FieldOfView fieldOfView, in GameObjectLink gameObjectLink) =>
            {
                //UnityEngine.Debug.LogError("FieldOfView Updated: " + fieldOfView.fov);
                UpdateFieldOfView(gameObjectLink.gameObject, fieldOfView.fov);
            }).WithoutBurst().Run();
            Entities
                .WithChangeFilter<GameObjectLink>()
                .WithNone<InitializeEntity>()
                .ForEach((in FieldOfView fieldOfView, in GameObjectLink gameObjectLink) =>
            {
                //UnityEngine.Debug.LogError("FieldOfView2 Updated: " + fieldOfView.fov);
                UpdateFieldOfView(gameObjectLink.gameObject, fieldOfView.fov);
            }).WithoutBurst().Run();
        }

        private void UpdateFieldOfView(UnityEngine.GameObject gameObject, float fov)
        {
            if (gameObject == null)
            {
                return;
            }
            var camera = gameObject.GetComponent<UnityEngine.Camera>();
            if (camera == null)
            {
                return;
            }
            camera.fieldOfView = fov;
            if (gameObject.transform.childCount > 0)
            {
                var cameraB = gameObject.transform.GetChild(0).GetComponent<UnityEngine.Camera>();
                if (cameraB == null)
                {
                    return;
                }
                cameraB.fieldOfView = fov;
            }
        }
    }
}