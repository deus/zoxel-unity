using Unity.Entities;
using Zoxel.Transforms;

namespace Zoxel.Cameras
{
    //! CameraScreenRect will update UnityEngine camera rect.
    [UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class CameraScreenRectSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithChangeFilter<CameraScreenRect>()
                .ForEach((in GameObjectLink gameObjectLink, in CameraScreenRect cameraScreenRect) =>
            {
                var gameObject = gameObjectLink.gameObject;
                if (gameObject)
                {
                    var unityCamera = gameObject.GetComponent<UnityEngine.Camera>();
                    if (unityCamera)
                    {
                        unityCamera.rect = cameraScreenRect.rect.GetRect();
                        if (unityCamera.transform.childCount > 0)
                        {
                            var uiCamera = unityCamera.transform.GetChild(0).GetComponent<UnityEngine.Camera>();
                            uiCamera.rect = cameraScreenRect.rect.GetRect();
                        }
                    }
                }
            }).WithoutBurst().Run();
        }
    }
}