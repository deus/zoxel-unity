using Unity.Entities;

namespace Zoxel.Cameras
{
    //! Removes camera entity from CameraReferences upon destroyed.
    [UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class CameraDestroySystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, Camera>()
                .ForEach((Entity e) =>
            {
                if (CameraReferences.cameras.Contains(e))
                {
                    CameraReferences.cameras.Remove(e);
                }
            }).WithoutBurst().Run();
            Entities
                .WithAll<DestroyEntity, FirstPersonCamera>()
                .ForEach((Entity e) =>
            {
                if (CameraReferences.firstPersonCameras.Contains(e))
                {
                    CameraReferences.firstPersonCameras.Remove(e);
                }
            }).WithoutBurst().Run();
        }
    }
}