using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Cameras
{
    //! Sets the cameras projection matrix data.
    /**
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(CameraSystemGroup))]
	public partial class CameraProjectionMatrixSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities.ForEach((ref CameraProjectionMatrix cameraProjectionMatrix, in GameObjectLink gameObjectLink) =>
            {
                var gameObject = gameObjectLink.gameObject;
                if (gameObject)
                {
                    var camera = gameObject.GetComponent<UnityEngine.Camera>();
                    cameraProjectionMatrix.projectionMatrix = camera.projectionMatrix;
                }
            }).WithoutBurst().Run();
        }
    }
}