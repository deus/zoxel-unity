﻿using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;

namespace Zoxel.Cameras
{
    //! Updates screen dimensions and recalculates Camera data.
    [BurstCompile, UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class CameraScreenDimensionsSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            var screenDimensions = new int2(UnityEngine.Screen.width, UnityEngine.Screen.height);
            #if UNITY_EDITOR
                Entities
                    .WithNone<SceneViewLink>()
                    .ForEach((ref Camera camera, in CameraScreenRect cameraScreenRect, in FieldOfView fieldOfView) =>
                {
                    camera.SetScreenDimensions(screenDimensions, cameraScreenRect, fieldOfView.fov);
                }).ScheduleParallel();
                Entities.ForEach((ref Camera camera, in SceneViewLink sceneViewLink, in FieldOfView fieldOfView) =>
                {
                    var sceneView = sceneViewLink.sceneView;
                    if (sceneView && sceneView.camera)
                    {
                        camera.SetScreenDimensions(new int2((int) sceneView.camera.pixelWidth, (int) sceneView.camera.pixelHeight), fieldOfView.fov);
                    }
                }).WithoutBurst().Run();
            #else
                Entities.ForEach((ref Camera camera, in CameraScreenRect cameraScreenRect, in FieldOfView fieldOfView) =>
                {
                    camera.SetScreenDimensions(screenDimensions, cameraScreenRect, fieldOfView.fov);
                }).ScheduleParallel();
            #endif
        }
    }
}