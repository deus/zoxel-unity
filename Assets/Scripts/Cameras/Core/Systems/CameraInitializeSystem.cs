using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Cameras
{
    //! Spawns camera game object based on prefab.
    [UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class CameraInitializeSystem : SystemBase
    {
        private static Rect[] singleScreenRect = new Rect[1]
        {
            new Rect(0, 0, 1, 1f)
        };
        private static Rect[] dualScreenRects = new Rect[2]
        {
            new Rect(0, 0.5f, 1, 0.5f),
            new Rect(0, 0, 1, 0.5f)
        };
        private static Rect[] quadScreenRects = new Rect[4]
        {
            new Rect(0, 0.5f, 0.5f, 0.5f),
            new Rect(0.5f, 0.5f, 0.5f, 0.5f),
            new Rect(0, 0, 0.5f, 0.5f),
            new Rect(0.5f, 0, 0.5f, 0.5f)
        };
        private static Rect[] octScreenRects = new Rect[16]
        {
            new Rect(0, 0.75f, 0.25f, 0.25f),
            new Rect(0.25f, 0.75f, 0.25f, 0.25f),
            new Rect(0.5f, 0.75f, 0.25f, 0.25f),
            new Rect(0.75f, 0.75f, 0.25f, 0.25f),
            new Rect(0, 0.5f, 0.25f, 0.25f),
            new Rect(0.25f, 0.5f, 0.25f, 0.25f),
            new Rect(0.5f, 0.5f, 0.25f, 0.25f),
            new Rect(0.75f, 0.5f, 0.25f, 0.25f),
            new Rect(0, 0.25f, 0.25f, 0.25f),
            new Rect(0.25f, 0.25f, 0.25f, 0.25f),
            new Rect(0.5f, 0.25f, 0.25f, 0.25f),
            new Rect(0.75f, 0.25f, 0.25f, 0.25f),
            new Rect(0, 0, 0.25f, 0.25f),
            new Rect(0.25f, 0, 0.25f, 0.25f),
            new Rect(0.5f, 0, 0.25f, 0.25f),
            new Rect(0.75f, 0, 0.25f, 0.25f),
        };
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery camerasInitializeQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            camerasInitializeQuery = GetEntityQuery(
                ComponentType.Exclude<ViewerCamera>(),
                ComponentType.ReadOnly<InitializeEntity>(),
                ComponentType.ReadOnly<FirstPersonCamera>(),
                ComponentType.ReadWrite<CameraScreenRect>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (CameraManager.instance == null) return;
            var playerCameraPrefab = CameraManager.instance.CameraSettings.playerCameraPrefab;
            var firstPersonCameraPrefab = CameraManager.instance.CameraSettings.firstPersonCameraPrefab;
            var PostUpdateCommands  = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, Camera>()
                .ForEach((Entity e) =>
            {
                CameraReferences.cameras.Add(e);
            }).WithoutBurst().Run();
            Entities
                .WithNone<FirstPersonCamera>()
                .WithAll<InitializeEntity, Camera>()
                .ForEach((ref CameraScreenRect cameraScreenRect) =>
            {
                cameraScreenRect.rect = new Rect(0, 0, 1, 1);
            }).WithoutBurst().Run();
            if (!camerasInitializeQuery.IsEmpty)
            {
                var totalAdditionalCamerasCount = camerasInitializeQuery.CalculateEntityCount();
                var newCamerasCount = CameraReferences.firstPersonCameras.Count + totalAdditionalCamerasCount;
                var rects = CameraInitializeSystem.singleScreenRect;
                if (newCamerasCount == 2)
                {
                    rects = CameraInitializeSystem.dualScreenRects;
                }
                else if (newCamerasCount > 64)
                {
                    rects = GenerateRects(8);
                    // return; // failed
                }
                else if (newCamerasCount > 16)
                {
                    rects = GenerateRects(8);
                }
                else if (newCamerasCount > 4)
                {
                    rects = CameraInitializeSystem.octScreenRects;
                }
                else if (newCamerasCount > 2)
                {
                    rects = CameraInitializeSystem.quadScreenRects;
                }
                // get highest power, then use that to generate rects
                Entities
                    .WithNone<ViewerCamera, InitializeEntity>()
                    .WithAll<FirstPersonCamera>()
                    .ForEach((int entityInQueryIndex, ref CameraScreenRect cameraScreenRect) =>
                {
                    cameraScreenRect.rect = rects[entityInQueryIndex];
                }).WithoutBurst().Run();
                Entities
                    .WithNone<ViewerCamera>()
                    .WithAll<InitializeEntity, FirstPersonCamera>()
                    .ForEach((int entityInQueryIndex, ref CameraScreenRect cameraScreenRect) =>
                {
                    var rectIndex = entityInQueryIndex + CameraReferences.firstPersonCameras.Count;
                    if (rectIndex >= rects.Length)
                    {
                        return;
                    }
                    cameraScreenRect.rect = rects[rectIndex];
                }).WithoutBurst().Run();
                Entities
                    .WithNone<ViewerCamera>()
                    .WithAll<InitializeEntity, FirstPersonCamera>()
                    .ForEach((Entity e) =>
                {
                    CameraReferences.firstPersonCameras.Add(e);
                }).WithoutBurst().Run();
            }
            Entities
                .WithNone<ViewerCamera>()
                .WithAll<InitializeEntity, Camera>()
                .ForEach((Entity e, int entityInQueryIndex, in CameraScreenRect cameraScreenRect, in CameraEnabledState cameraEnabledState) =>
            {
                var prefab = playerCameraPrefab;
                if (HasComponent<FirstPersonCamera>(e))
                {
                    prefab = firstPersonCameraPrefab;
                }
                var cameraObject = UnityEngine.GameObject.Instantiate(prefab);
                cameraObject.name = "Camera [" + (entityInQueryIndex + CameraReferences.cameras.Count) + "]";
                cameraObject.SetActive(true);
                PostUpdateCommands.SetSharedComponentManaged(e, new GameObjectLink(cameraObject));
                var unityCamera = cameraObject.GetComponent<UnityEngine.Camera>();
                unityCamera.rect = cameraScreenRect.rect.GetRect();
                CameraEnabledSystem.UpdateEnabledState(cameraObject, in cameraEnabledState);
                if (unityCamera.transform.childCount > 0)
                {
                    var uiCameraTransform = unityCamera.transform.GetChild(0);
                    if (uiCameraTransform != null)
                    {
                        var uiCamera = uiCameraTransform.GetComponent<UnityEngine.Camera>();
                        if (uiCamera != null)
                        {
                            uiCamera.rect = cameraScreenRect.rect.GetRect();
                        }
                    }
                }
            }).WithoutBurst().Run();
        }

        private static Rect[] GenerateRects(int rowsCount)
        {
            var moreRects = new Rect[rowsCount * rowsCount];
            var size = 1f / ((float)rowsCount);
            var k = 0;
            for (int j = rowsCount - 1; j >= 0; j--)
            {
                for (int i = 0; i < rowsCount; i++)
                {
                    moreRects[k] = new Rect(i * size, j * size, size, size);
                    k++;
                }
            }
            return moreRects;
        }
    }
}
            /*for (int i = 0; i < CameraReferences.cameras.Count; i++)
            {
                var cameraEntity = CameraReferences.cameras[i];
                PostUpdateCommands.SetComponent(cameraEntity, new CameraScreenRect(rects[i]));
            }*/