using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Cameras
{
    //! Pushes CameraEnabledState data to unity camera component.
    [BurstCompile, UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class CameraLinkTransitionSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref CameraLink cameraLink, in TransitionCameraLink transitionCameraLink) =>
            {
                if (elapsedTime - transitionCameraLink.elapsedTime >= transitionCameraLink.transitionTime)
                {
                    PostUpdateCommands.RemoveComponent<TransitionCameraLink>(entityInQueryIndex, e);
                    cameraLink.camera = transitionCameraLink.camera;
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}