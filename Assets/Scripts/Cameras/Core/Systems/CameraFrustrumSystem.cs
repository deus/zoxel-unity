using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Cameras
{
    //! When FieldOfView updates so does cameras frustrum data.
    /**
    *   Also generates frustrum planes, used for camera render culling.
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class CameraFrustrumSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithChangeFilter<FieldOfView>()
                .ForEach((ref Camera camera, in FieldOfView fieldOfView) =>
            {
                camera.CalculateFrustrum(fieldOfView.fov);
            }).ScheduleParallel();
			Entities
                .WithNone<InitializeEntity, DestroyEntity>()
				.ForEach((ref CameraFrustrumPlanes cameraFrustrumPlanes, in GameObjectLink gameObjectLink) =>
			{
                if (gameObjectLink.gameObject == null)
                {
                    return;
                }
                if (cameraFrustrumPlanes.planes.Length == 0)
                {
                    cameraFrustrumPlanes.planes = new BlitableArray<UnityEngine.Plane>(6, Allocator.Persistent);
                }
                var camera = gameObjectLink.gameObject.GetComponent<UnityEngine.Camera>();
                if (camera == null)
                {
                    return;
                }
                var unityPlanes = UnityEngine.GeometryUtility.CalculateFrustumPlanes(camera);
                for (int i = 0; i < unityPlanes.Length; i++)
                {
                    cameraFrustrumPlanes.planes[i] = unityPlanes[i];
                }
            }).WithoutBurst().Run();
        }
    }
}