using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Cameras
{
    //! Cleans up CameraFrustrumPlanes planes data.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class CameraFrustrumPlanesDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, CameraFrustrumPlanes>()
				.ForEach((in CameraFrustrumPlanes cameraFrustrumPlanes) =>
			{
                cameraFrustrumPlanes.Dispose();
            }).ScheduleParallel();
        }
    }
}