using Unity.Entities;
using Zoxel.Transforms;

namespace Zoxel.Cameras
{
    //! Pushes CameraEnabledState data to unity camera component.
    [UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class CameraEnabledSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithChangeFilter<CameraEnabledState>()
                .WithNone<InitializeEntity>()
                .ForEach((in CameraEnabledState cameraEnabledState, in GameObjectLink gameObjectLink) =>
            {
                UpdateEnabledState(gameObjectLink.gameObject, in cameraEnabledState);
            }).WithoutBurst().Run();
        }

        public static void UpdateEnabledState(UnityEngine.GameObject gameObject, in CameraEnabledState cameraEnabledState)
        {
            if (gameObject == null)
            {
                return;
            }
            var camera = gameObject.GetComponent<UnityEngine.Camera>();
            if (camera == null)
            {
                return;
            }
            if (cameraEnabledState.state == 1)
            {
                camera.enabled = true;
            }
            else
            {
                camera.enabled = false;
            }
        }
    }
}