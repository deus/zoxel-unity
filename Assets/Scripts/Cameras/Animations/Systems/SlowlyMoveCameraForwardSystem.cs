using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Cameras
{
    //! Slowly move the camera forward.. for funsies.
    [BurstCompile, UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class SlowlyMoveCameraForwardSystem : SystemBase
	{
        [BurstCompile]
        protected override void OnUpdate()
        {
            var minDistance = -0.016f;
            var distance = 0.01f;
            var speed = 0.2f;
            var elapsedTime = (float) World.Time.ElapsedTime * speed;
            Entities
                .WithAll<SlowlyMoveCameraForward>()
                .ForEach((Entity e, ref Translation translation) =>
            {
                translation.Value = new float3(0, 0, minDistance - distance + math.sin(elapsedTime + math.PI / 2f) * distance * 2f);   // direction * deltaTime;
            }).ScheduleParallel();
        }
	}
}
            // var deltaTime = World.Time.DeltaTime * 0.05f;
            // var direction = new float3(0, 0, -1f);