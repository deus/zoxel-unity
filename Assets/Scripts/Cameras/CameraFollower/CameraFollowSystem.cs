﻿/*using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Burst;

namespace Zoxel.Cameras
{
    [BurstCompile, UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class CameraFollowSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            var degreesToRadians = ((math.PI * 2) / 360f);
            Entities.ForEach((ref FollowerCamera camera, in CharacterLink characterToCamera) =>
            {
                var newPosition = characterToCamera.position + new float3(0, camera.Value.cameraAddition.y, 0);
                if (camera.Value.cameraAddition.z != 0)
                {
                    newPosition += math.rotate(characterToCamera.rotation, new float3(0, 0, camera.Value.cameraAddition.z));
                }
                if (!System.Single.IsNaN(newPosition.x) && !System.Single.IsNaN(newPosition.y) && !System.Single.IsNaN(newPosition.z))
                {
                    camera.Value.targetPosition = newPosition;
                }
                else
                {
                    camera.Value.targetPosition = characterToCamera.position + new float3(0, camera.Value.cameraAddition.y, 
                        camera.Value.cameraAddition.z);
                }
                camera.Value.targetRotation = quaternion.EulerXYZ(camera.Value.cameraRotation * degreesToRadians); //UnityEngine.Quaternion.Euler(camera.Value.cameraRotation);
            }).ScheduleParallel();
		}
    }
}*/