﻿/*using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;


/// <summary>
/// Flagged for rewrite - Due to using Referencing to lists of character entities
/// </summary>

namespace Zoxel
{
    public partial class CameraMovementSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            //var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); //.AsParallelWriter();
            var delta = World.Time.DeltaTime;
            Entities.ForEach((ref Translation position, ref Rotation rotation, in FollowerCamera camera) =>
            {
                var newPosition = camera.Value.targetPosition;
                newPosition.x = math.max(newPosition.x, -999);
                newPosition.x = math.min(newPosition.x, 999);
                newPosition.y = math.max(newPosition.y, -999);
                newPosition.y = math.min(newPosition.y, 999);
                newPosition.z = math.max(newPosition.z, -999);
                newPosition.z = math.min(newPosition.z, 999);
                position.Value = math.lerp(position.Value, newPosition, delta * camera.Value.lerpSpeed.x);
                //position.Value = newPosition;
                rotation.Value = QuaternionHelpers.slerpSafe(rotation.Value.value, camera.Value.targetRotation.value, delta * camera.Value.lerpSpeed.y);
            }).ScheduleParallel();
        }
    }
}
*/