using Unity.Entities;
using Unity.Burst;
using Zoxel.Audio;
using Zoxel.Audio.Music;

namespace Zoxel.Cameras.Spawning
{
    //! Transitions between two cameras.
    [BurstCompile, UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class CameraTransitionSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DelayEvent>()
                .ForEach((int entityInQueryIndex, in TransitionCamera transitionCamera) =>
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, transitionCamera.beforeCamera, new CameraEnabledState(0));
                PostUpdateCommands.SetComponent(entityInQueryIndex, transitionCamera.afterCamera, new CameraEnabledState(1));
                PostUpdateCommands.AddComponent<DisableMusic>(entityInQueryIndex, transitionCamera.beforeCamera);
                PostUpdateCommands.AddComponent<EnableMusic>(entityInQueryIndex, transitionCamera.afterCamera);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}