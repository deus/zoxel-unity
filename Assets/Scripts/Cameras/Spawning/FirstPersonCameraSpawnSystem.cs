﻿using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms; // Parent Link - attaches to character head bone
using Zoxel.Audio;
using Zoxel.Audio.Music;
using Zoxel.Cameras.PostProcessing;

namespace Zoxel.Cameras.Spawning
{
    //! Spawns a first person camera.
    /**
    *   - Spawn System -
    *   Has a ParentLink as attaches to head bone.
    */
    [BurstCompile, UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class FirstPersonCameraSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity firstPersonCameraPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var firstPersonCameraArchtype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(CameraEnabledState),
                typeof(ZoxID),
                typeof(FirstPersonCamera),
                typeof(Camera),
                typeof(CameraProjectionMatrix),
                typeof(CameraFrustrumPlanes),
                typeof(FieldOfView),
                typeof(CameraScreenRect),
                typeof(PostExposure),
                typeof(ControllerLink),
                typeof(CharacterLink),
                typeof(VoxLink),            // used for sky color
                typeof(SpawnMusic),
                typeof(MusicLink),
                // typeof(SlowlyMoveCameraForward),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(ParentLink),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(GameObjectLink)
            );
            firstPersonCameraPrefab = EntityManager.CreateEntity(firstPersonCameraArchtype);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (CameraManager.instance == null) return;
            var id = IDUtil.GenerateUniqueID();  // todo: generate ids inside jobs
            var screenDimensions = new int2(UnityEngine.Screen.width, UnityEngine.Screen.height);
            var orbitDepth = CameraManager.instance.cameraSettings.uiDepth;
            var fov = CameraManager.instance.cameraSettings.fieldOfView;
            var firstPersonCameraPrefab = FirstPersonCameraSpawnSystem.firstPersonCameraPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .ForEach((Entity e, int entityInQueryIndex, in SpawnFirstPersonCamera spawnFirstPersonCamera) =>
            {
                // PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                var cameraEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, firstPersonCameraPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new ZoxID(id));
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new Camera(screenDimensions, orbitDepth, fov));
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new FieldOfView(fov));
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new Translation { Value = spawnFirstPersonCamera.position });
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new Rotation { Value = spawnFirstPersonCamera.rotation });
                if (spawnFirstPersonCamera.controller.Index > 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new ControllerLink(spawnFirstPersonCamera.controller));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnFirstPersonCamera.controller, new CameraLink(cameraEntity));
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static Entity SpawnFirstPersonCamera(EntityCommandBuffer PostUpdateCommands, in SpawnFirstPersonCamera spawnFirstPersonCamera)
        {
            var id = IDUtil.GenerateUniqueID();  // todo: generate ids inside jobs
            var screenDimensions = new int2(UnityEngine.Screen.width, UnityEngine.Screen.height);
            var orbitDepth = CameraManager.instance.cameraSettings.uiDepth;
            var fov = CameraManager.instance.cameraSettings.fieldOfView;
            var firstPersonCameraPrefab = FirstPersonCameraSpawnSystem.firstPersonCameraPrefab;
            var cameraEntity = PostUpdateCommands.Instantiate(firstPersonCameraPrefab);
            PostUpdateCommands.SetComponent(cameraEntity, new ZoxID(id));
            PostUpdateCommands.SetComponent(cameraEntity, new Camera(screenDimensions, orbitDepth, fov));
            PostUpdateCommands.SetComponent(cameraEntity, new FieldOfView(fov));
            PostUpdateCommands.SetComponent(cameraEntity, new Translation { Value = spawnFirstPersonCamera.position });
            PostUpdateCommands.SetComponent(cameraEntity, new Rotation { Value = spawnFirstPersonCamera.rotation });
            if (spawnFirstPersonCamera.controller.Index > 0)
            {
                PostUpdateCommands.SetComponent(cameraEntity, new ControllerLink(spawnFirstPersonCamera.controller));
                PostUpdateCommands.SetComponent(spawnFirstPersonCamera.controller, new CameraLink(cameraEntity));
            }
            return cameraEntity;
        }
    }
}