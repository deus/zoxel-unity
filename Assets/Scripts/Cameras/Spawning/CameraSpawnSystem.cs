using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms; // Parent Link - attaches to character head bone
using Zoxel.Audio;
using Zoxel.Audio.Music;
using Zoxel.Cameras.PostProcessing;
using Zoxel.Weather.Skybox;

namespace Zoxel.Cameras.Spawning
{
    //! Spawns a first person camera.
    /**
    *   - Spawn System -
    *   Has a ParentLink as attaches to head bone.
    */
    [BurstCompile, UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class CameraSpawnSystem : SystemBase
    {
        public static Entity spawnCameraPrefab;
        public static Entity spawnFirstPersonCameraPrefab;
        private Entity cameraPrefab;

        protected override void OnCreate()
        {
            var cameraArchtype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SlowlyMoveCameraForward),
                typeof(InitializeEntity),
                typeof(SetSkyColor),
                typeof(UpdatePostProcessing),
                typeof(ZoxID),
                typeof(FreeCamera),
                typeof(Camera),
                typeof(CameraEnabledState),
                typeof(CameraProjectionMatrix),
                typeof(FieldOfView),
                typeof(CameraScreenRect),
                typeof(PostExposure),
                typeof(VoxLink),                // used for sky color
                // typeof(SpawnMusic),
                typeof(MusicLink),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(GameObjectLink)
            );
            cameraPrefab = EntityManager.CreateEntity(cameraArchtype);
            EntityManager.SetComponentData(cameraPrefab, new CameraEnabledState(1));
            EntityManager.SetComponentData(cameraPrefab, new Rotation { Value = quaternion.identity });
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(cameraPrefab); // EntityManager.AddComponentData(cameraPrefab, new EditorName("[camera]"));
            #endif
            CameraSpawnSystem.InitializePrefabs(EntityManager);
        }

        private static void InitializePrefabs(EntityManager EntityManager)
        {
            if (spawnCameraPrefab.Index != 0)
            {
                return;
            }
            var spawnCameraArchtype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(GenericEvent),
                typeof(SpawnCamera));
            spawnCameraPrefab = EntityManager.CreateEntity(spawnCameraArchtype);
            EntityManager.SetComponentData(spawnCameraPrefab, new SpawnCamera(float3.zero, quaternion.identity));
            var spawnFirstPersonCameraArchtype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(GenericEvent),
                typeof(SpawnFirstPersonCamera));
            spawnFirstPersonCameraPrefab = EntityManager.CreateEntity(spawnFirstPersonCameraArchtype);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (CameraManager.instance == null) return;
            var cameraID = IDUtil.GenerateUniqueID();  // todo: generate ids inside jobs
            var screenDimensions = new int2(UnityEngine.Screen.width, UnityEngine.Screen.height);
            var uiDepth = CameraManager.instance.cameraSettings.uiDepth;
            var fov = CameraManager.instance.cameraSettings.fieldOfView;
            var cameraPrefab = this.cameraPrefab;
            var mainMenuBackgroundColor = CameraManager.instance.cameraSettings.mainMenuBackgroundColor;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in SpawnCamera spawnCamera) =>
            {
                var cameraEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, cameraPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new Translation { Value = spawnCamera.position });
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new Rotation { Value = spawnCamera.rotation });
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new ZoxID(cameraID));
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new Camera(screenDimensions, uiDepth, fov));
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new FieldOfView(fov));
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new UpdatePostProcessing(PostProcessingProfileType.MainMenu));
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new SetSkyColor(mainMenuBackgroundColor));
                if (HasComponent<SpawnMusic>(e))
                {
                    PostUpdateCommands.AddComponent<SpawnMusic>(entityInQueryIndex, cameraEntity);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        //! Called from Bootstrap to spawn a camera.
        public static void SpawnCamera(EntityManager EntityManager, bool isSpawnMusic)
        {
            CameraSpawnSystem.InitializePrefabs(EntityManager);
            var spawnEntity = EntityManager.Instantiate(spawnCameraPrefab);
            if (isSpawnMusic)
            {
                EntityManager.AddComponent<SpawnMusic>(spawnEntity);
            }
        }
    }
}