using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Zoxel.Cameras.Authoring
{
    //! Settins for our FirstPersonCamera.
    // [GenerateAuthoringComponent]
    public struct MouseSettings : IComponentData
    {
        public float2 rotationBoundsX;          // -70, 85
        public float2 mouseSpeed;               // 0.22, 0.18
        public float2 gamepadCameraSpeed;       // 160, 80
        public float mouseLerpSpeed;            // 28
        public bool disableCameraHeadOffset;    // false
        public bool isLerpRotation;             // false
        public bool isAverageInput;             // false
    }

    public class MouseSettingsAuthoring : MonoBehaviour
    {
        public float2 rotationBoundsX;          // -70, 85
        public float2 mouseSpeed;               // 0.22, 0.18
        public float2 gamepadCameraSpeed;       // 160, 80
        public float mouseLerpSpeed;            // 28
        public bool disableCameraHeadOffset;    // false
        public bool isLerpRotation;             // false
        public bool isAverageInput;             // false
    }

    public class MouseSettingsAuthoringBaker : Baker<MouseSettingsAuthoring>
    {
        public override void Bake(MouseSettingsAuthoring authoring)
        {
            AddComponent(new MouseSettings
            {
                rotationBoundsX = authoring.rotationBoundsX,
                mouseSpeed = authoring.mouseSpeed,
                gamepadCameraSpeed = authoring.gamepadCameraSpeed,
                mouseLerpSpeed = authoring.mouseLerpSpeed,
                disableCameraHeadOffset = authoring.disableCameraHeadOffset,
                isLerpRotation = authoring.isLerpRotation,
                isAverageInput = authoring.isAverageInput
            });       
        }
    }
}


        // [UnityEngine.Header("Realm")]
        // [UnityEngine.Header("Mouse Movement")]
        /*public void DrawGUI() 
        {
            isAverageInput = GUILayout.Toggle(isAverageInput, "isAverageInput:");
            {
                GUILayout.Label("   mouseSpeed:");
                mouseSpeed.x = float.Parse(GUILayout.TextField(mouseSpeed.x.ToString()));
                mouseSpeed.y = float.Parse(GUILayout.TextField(mouseSpeed.y.ToString()));
            }
            isLerpRotation = GUILayout.Toggle(isLerpRotation, "isLerpRotation:");
            if (isLerpRotation)
            {
                GUILayout.Label("   mouseLerpSpeed:");
                mouseLerpSpeed = float.Parse(GUILayout.TextField(mouseLerpSpeed.ToString()));
            }
            // Debug Input last 10 of them?
        }*/

        /*[UnityEngine.Header("Mouse Physics")]
        public float2 mouseSpeed2;              // 16, 0.5
        public bool isAccelaration;             // false
        public float mouseSlowdownRate;         // 0.4f
        public float2 maxMouseVelocity;         // 2, 2
        public float2 maxMouseVelocity2;        // 3, 3        
        public float2 maxMouseVelocityCap1;     // 1, 1    */
        //public float2 mouseSensitivity2;     // 16, 4
        //public float2 mouseSensitivity3;     // 16, 4
        //public float2 mouseLevels; 
        //public float2 mouseLevels2; 

        /*[UnityEngine.Header("Removed")]
        public float minMoveX;              // 0
        public float minMoveY;              // 0
        public float2 mouseClamp;           // 1, 1*/
            /*isAccelaration = GUILayout.Toggle(isAccelaration, "isAccelaration:");
            if (isAccelaration)
            {
                GUILayout.Label("   mouseSpeed2:");
                mouseSpeed2.x = float.Parse(GUILayout.TextField(mouseSpeed2.x.ToString()));
                mouseSpeed2.y = float.Parse(GUILayout.TextField(mouseSpeed2.y.ToString()));
                GUILayout.Label("mouseSlowdownRate:");
                mouseSlowdownRate = float.Parse(GUILayout.TextField(mouseSlowdownRate.ToString()));
                GUILayout.Label("   maxMouseVelocity:");
                maxMouseVelocity.x = float.Parse(GUILayout.TextField(maxMouseVelocity.x.ToString()));
                maxMouseVelocity.y = float.Parse(GUILayout.TextField(maxMouseVelocity.y.ToString()));
                GUILayout.Label("   maxMouseVelocity2:");
                maxMouseVelocity2.x = float.Parse(GUILayout.TextField(maxMouseVelocity2.x.ToString()));
                maxMouseVelocity2.y = float.Parse(GUILayout.TextField(maxMouseVelocity2.y.ToString()));
                GUILayout.Label("   maxMouseVelocityCap1:");
                maxMouseVelocityCap1.x = float.Parse(GUILayout.TextField(maxMouseVelocityCap1.x.ToString()));
                maxMouseVelocityCap1.y = float.Parse(GUILayout.TextField(maxMouseVelocityCap1.y.ToString()));
                if (GUILayout.Button("Disable Y"))
                {
                    maxMouseVelocity.y = 0;
                    maxMouseVelocity2.y = 0;
                }
            }
            else*/