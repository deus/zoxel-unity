using Unity.Entities;
using UnityEngine;

namespace Zoxel.Cameras.Authoring
{
    //! Settins for our FirstPersonCamera.
    // [GenerateAuthoringComponent]
    public struct FirstPersonCameraSettings : IComponentData
    {
        public float cameraBobBaseSpeed;                // 0.6f
        public float cameraBobSpeedMultiplier;          // 3f
        public float cameraBobDistance;                 // 0.01f
        public float walkingDistanceDistanceMultiplier; // 1.8f
        public float cameraBobWalkingSpeedMultiplier;   // 1.6f
        public float cameraBobHeightLerpSpeed;          // 0.14f
        public bool disableHeadBob;
    }

    public class FirstPersonCameraSettingsAuthoring : MonoBehaviour
    {
        public float cameraBobBaseSpeed;                // 0.6f
        public float cameraBobSpeedMultiplier;          // 3f
        public float cameraBobDistance;                 // 0.01f
        public float walkingDistanceDistanceMultiplier; // 1.8f
        public float cameraBobWalkingSpeedMultiplier;   // 1.6f
        public float cameraBobHeightLerpSpeed;          // 0.14f
        public bool disableHeadBob;
    }

    public class FirstPersonCameraSettingsAuthoringBaker : Baker<FirstPersonCameraSettingsAuthoring>
    {
        public override void Bake(FirstPersonCameraSettingsAuthoring authoring)
        {
            AddComponent(new FirstPersonCameraSettings
            {
                cameraBobBaseSpeed = authoring.cameraBobBaseSpeed,
                cameraBobSpeedMultiplier = authoring.cameraBobSpeedMultiplier,
                cameraBobDistance = authoring.cameraBobDistance,
                walkingDistanceDistanceMultiplier = authoring.walkingDistanceDistanceMultiplier,
                cameraBobWalkingSpeedMultiplier = authoring.cameraBobWalkingSpeedMultiplier,
                cameraBobHeightLerpSpeed = authoring.cameraBobHeightLerpSpeed,
                disableHeadBob = authoring.disableHeadBob,
            });       
        }
    }
}