﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Rendering;

namespace Zoxel.Cameras
{
    //! Makes a UI face the camera!
    /**
    *   \todo Support Multiple Cameras
    */
    [BurstCompile, UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class FadeInCameraDistanceSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery camerasQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        	camerasQuery = GetEntityQuery(ComponentType.ReadOnly<Camera>(), ComponentType.ReadOnly<Translation>());
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var fadeOutDistance = 7.8f;
            var fadeInDistance = 4.6f;
            var degreesToRadians = ((math.PI * 2) / 360f);
            var mathPi2 = math.PI * 2;
            var rotation2 = quaternion.EulerXYZ(new float3(0, mathPi2, 0) * degreesToRadians).value; //  * degreesToRadians).value;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var cameraEntities = camerasQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var cameraEnabledStates = GetComponentLookup<CameraEnabledState>(true);
			var cameraPositions = GetComponentLookup<Translation>(true);
            Dependency = Entities
                .WithNone<FadeOut, FadeIn>()
                .WithAll<DistanceFaderUI>()
                .ForEach((Entity e, int entityInQueryIndex, in Translation translation) =>    // ref MaterialBaseColor materialBaseColor, 
            {
                var cameraEntity = new Entity();
                for (int i = 0; i < cameraEntities.Length; i++)
                {
                    var cameraEntity2 = cameraEntities[i];
                    var cameraEnabledState = cameraEnabledStates[cameraEntity2];
                    if (cameraEnabledState.state == 1)
                    {
                        cameraEntity = cameraEntity2;
                        break;
                    }
                }
                if (cameraEntity.Index == 0)
                {
                    return;
                }
                var cameraPosition = cameraPositions[cameraEntity].Value;
                var distanceTo = math.distance(translation.Value, cameraPosition);
                // var alpha = 0f;
                if (distanceTo >= fadeOutDistance)
                {
                    if (!HasComponent<FaderInvisible>(e))
                    {
                        PostUpdateCommands.AddComponent<FadeOut>(entityInQueryIndex, e);
                    }
                }
                else if (distanceTo <= fadeInDistance)
                {
                    if (!HasComponent<FaderVisible>(e))
                    {
                        PostUpdateCommands.AddComponent<FadeIn>(entityInQueryIndex, e);
                    }
                }
                //materialBaseColor.Value.w = alpha;
            })  .WithReadOnly(cameraEntities)
                .WithDisposeOnCompletion(cameraEntities)
                .WithReadOnly(cameraEnabledStates)
                .WithReadOnly(cameraPositions)
                .WithNativeDisableContainerSafetyRestriction(cameraPositions)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
	}
}