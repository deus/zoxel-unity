﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Transforms;

namespace Zoxel.Cameras
{
    //! Makes a UI face the camera!
    /**
    *   \todo Support Multiple Cameras
    *   \todo Use CameraLink?
    */
    [BurstCompile, UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class FaceCameraSystem : SystemBase
	{
		private EntityQuery camerasQuery;

        protected override void OnCreate()
        {
        	camerasQuery = GetEntityQuery(
                ComponentType.ReadOnly<CameraEnabledState>(),
                ComponentType.ReadOnly<Camera>(),
                ComponentType.ReadOnly<Rotation>());
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var degreesToRadians = ((math.PI * 2) / 360f);
            var mathPi2 = math.PI * 2;
            var flipRotation = quaternion.EulerXYZ(new float3(0, mathPi2, 0) * degreesToRadians).value; //  * degreesToRadians).value;
            var cameraEntities = camerasQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
			var cameraEnabledStates = GetComponentLookup<CameraEnabledState>(true);
			var cameraRotations = GetComponentLookup<Rotation>(true);
			var cameraTranslations = GetComponentLookup<Translation>(true);
            Dependency = Entities
                .WithAll<FaceCamera>()
                .ForEach((ref Rotation rotation, in Translation translation) =>
            {
                var closestDistance = 64f;
                var closestCamera = new Entity();
                for (int i = 0; i < cameraEntities.Length; i++)
                {
                    var cameraEntity = cameraEntities[i];
                    var cameraEnabledState = cameraEnabledStates[cameraEntity];
                    if (cameraEnabledState.state == 1)
                    {
                        var distanceTo = math.distance(translation.Value, cameraTranslations[cameraEntity].Value);
                        if (distanceTo < closestDistance)
                        {
                            closestDistance = distanceTo;
                            closestCamera = cameraEntity;
                        }
                    }
                }
                if (closestCamera.Index > 0)
                {
                    rotation.Value = math.mul(flipRotation, cameraRotations[closestCamera].Value);
                }
            })  .WithReadOnly(cameraEntities)
                .WithDisposeOnCompletion(cameraEntities)
                .WithReadOnly(cameraEnabledStates)
                .WithReadOnly(cameraRotations)
                .WithNativeDisableContainerSafetyRestriction(cameraRotations)
                .WithReadOnly(cameraTranslations)
                .ScheduleParallel(Dependency);
        }
	}
}