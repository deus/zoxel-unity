using Unity.Entities;
using Zoxel.Transforms;
// this won't update unless its for all screens kinda thing
// save resolution to cameras and do it if camera detects change

namespace Zoxel.Cameras
{
    //! Cleans up ViewerCameras by also removing the dictionary entries.
    [ UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ViewerCameraDestroySystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, ViewerCamera>()
                .ForEach((in GameObjectLink gameObjectLink) =>
            {
                if (gameObjectLink.gameObject == null)
                {
                    return;
                }
                var camera = gameObjectLink.gameObject.GetComponent<UnityEngine.Camera>();
                if (camera != null && camera.targetTexture != null)
                {
                    ObjectUtil.Destroy(camera.targetTexture);
                }
            }).WithoutBurst().Run();
        }
    }
}