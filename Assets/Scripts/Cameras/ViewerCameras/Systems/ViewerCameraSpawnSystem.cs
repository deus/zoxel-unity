using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.UI;                         // uses ViewerUI
using Zoxel.Rendering;

namespace Zoxel.Cameras
{
    //! Spawns a camera for the ViewerUI connected to a RenderTexture.
    /**
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class ViewerCameraSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity cameraViewerPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var cameraViewerArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(ViewerCamera),
                typeof(GameObjectLink),
                typeof(Translation),
                typeof(Rotation),
                typeof(LocalToWorld),
                typeof(ZoxID),
                typeof(CreatorLink)
            );
            cameraViewerPrefab = EntityManager.CreateEntity(cameraViewerArchetype);
        }

        // this won't update unless its for all screens kinda thing
        // save resolution to cameras and do it if cameraEntity detects change
        protected override void OnUpdate()
        {
            if (CameraManager.instance == null) return;
            // Destroy previous texture
            Entities.ForEach((in SpawnViewerCamera spawnViewerCamera, in ZoxMesh zoxMesh) =>
            {
                // wait for the thing to spawn
                if (spawnViewerCamera.state < 1)
                {
                    return;
                }
                var previousTexture = zoxMesh.material.GetTexture("_BaseMap");
                if (previousTexture)
                {
                    ObjectUtil.Destroy(previousTexture);
                }
            }).WithoutBurst().Run();
            var characterViewerResolution = CameraManager.instance.cameraSettings.characterViewerResolution;
            var voxelViewerResolution = CameraManager.instance.cameraSettings.voxelViewerResolution;
            var voxelViewerCameraPrefab = CameraManager.instance.CameraSettings.voxelViewerCameraPrefab;
            var characterViewerCameraPrefab = CameraManager.instance.CameraSettings.characterViewerCameraPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((Entity e, ref SpawnViewerCamera spawnViewerCamera, in ZoxMesh zoxMesh) =>
            {
                // wait for the thing to spawn
                if (spawnViewerCamera.state < 1)
                {
                    spawnViewerCamera.state++;
                    return;
                }
                PostUpdateCommands.RemoveComponent<SpawnViewerCamera>(e);
                var viewerResolution = characterViewerResolution;
                if (spawnViewerCamera.cameraType == 0)
                {
                    viewerResolution = voxelViewerResolution;
                }
                var viewerDepth = 16;
                if (spawnViewerCamera.cameraType == 0)
                {
                    viewerDepth = 0;
                }
                var cameraEntity = PostUpdateCommands.Instantiate(cameraViewerPrefab);
                PostUpdateCommands.SetComponent(cameraEntity, new CreatorLink(e));
                PostUpdateCommands.SetComponent(cameraEntity, new ZoxID(spawnViewerCamera.id));
                PostUpdateCommands.SetComponent(cameraEntity, new Translation { Value = spawnViewerCamera.position });
                PostUpdateCommands.SetComponent(cameraEntity, new Rotation { Value = spawnViewerCamera.rotation });
                PostUpdateCommands.SetComponent(e, new ViewerCameraLink(cameraEntity));
                // Create RenderTexture
                var renderTexture = new UnityEngine.RenderTexture(viewerResolution, viewerResolution, viewerDepth, UnityEngine.RenderTextureFormat.ARGB32);
                renderTexture.filterMode = UnityEngine.FilterMode.Point;
                // renderTexture.useMipMap = true;
                renderTexture.Create();
                zoxMesh.material.SetTexture("_BaseMap", renderTexture);
                UnityEngine.GameObject cameraObject;
                if (spawnViewerCamera.cameraType == 0)
                {
                    cameraObject = UnityEngine.GameObject.Instantiate(voxelViewerCameraPrefab);
                }
                else
                {
                    cameraObject = UnityEngine.GameObject.Instantiate(characterViewerCameraPrefab);
                }
                cameraObject.SetActive(true);
                cameraObject.name = "ViewerCamera";
                var camera2 = cameraObject.GetComponent<UnityEngine.Camera>();
                camera2.targetTexture = renderTexture;
                PostUpdateCommands.SetSharedComponentManaged(cameraEntity, new GameObjectLink(cameraObject));
            }).WithoutBurst().Run();
        }

        public static int SpawnViewerCamera(EntityCommandBuffer PostUpdateCommands, Entity ui, Entity targetEntity, float3 position, quaternion rotation, byte cameraType)
        {
            var id = IDUtil.GenerateUniqueID();
            PostUpdateCommands.AddComponent(ui, new SpawnViewerCamera
            {
                id = id,
                targetEntity = targetEntity,
                position = position,
                rotation = rotation,
                cameraType = cameraType
            });
            return id;
        }
    }
}