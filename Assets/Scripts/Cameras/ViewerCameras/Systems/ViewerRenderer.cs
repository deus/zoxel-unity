﻿#if UNITY_RP
using Unity.Entities;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Cameras
{
    //! Uses scriptableRender pipeline to render viewer uis.
    /**
    *   \todo Remove use of dictionary from here somehow.
    */
    public partial class ViewerRenderer : ScriptableRendererFeature
    {
        private ViewerRenderPass viewerRenderPass;
        
        public override void Create()
        {
            viewerRenderPass = new ViewerRenderPass();
            viewerRenderPass.renderPassEvent = RenderPassEvent.AfterRendering;
        }
        
        public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
        {
            renderer.EnqueuePass(viewerRenderPass);
        }
        
        class ViewerRenderPass : ScriptableRenderPass
        {
            public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
            {
                if (World.DefaultGameObjectInjectionWorld == null)
                {
                    return;
                }
                var camera = renderingData.cameraData.camera;
                var commandBuffer = CommandBufferPool.Get();
                var src = BuiltinRenderTextureType.CameraTarget;
                var dst = BuiltinRenderTextureType.CurrentActive;
                //! Uses Entity data for Blitting.
                var EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
                for (int i = 0; i < ViewerCameraSystem.viewerCameras.Count; i++)
                {
                    var e = ViewerCameraSystem.viewerCameras[i];
                    if (!EntityManager.Exists(e))
                    {
                        continue;
                    }
                    var gameObjectLink = EntityManager.GetSharedComponentManaged<GameObjectLink>(e);
                    var creatorLink = EntityManager.GetComponentData<CreatorLink>(e);
                    if (!EntityManager.Exists(creatorLink.creator))
                    {
                        continue;
                    }
                    var camera2 = gameObjectLink.gameObject.GetComponent<UnityEngine.Camera>();
                    if (camera2)
                    {
                        var zoxMesh = EntityManager.GetSharedComponentManaged<ZoxMesh>(creatorLink.creator);
                        var material = zoxMesh.material;
                        if (material)
                        {
                            Blit(commandBuffer, camera2.targetTexture, src, material);
                        }
                    }
                };
                commandBuffer.Blit(src, dst);
                context.ExecuteCommandBuffer(commandBuffer);
                CommandBufferPool.Release(commandBuffer);
            }
        }
    }
}
#endif