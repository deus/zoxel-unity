using Unity.Entities;
using System.Collections.Generic;

namespace Zoxel.Cameras
{
    //! Keeps a list of viewer camera entities for ViewerRenderer pipeline.
    [AlwaysUpdateSystem]
    [UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class ViewerCameraSystem : SystemBase
    {
        public static List<Entity> viewerCameras = new List<Entity>();

        protected override void OnUpdate()
        {
            viewerCameras.Clear();
            Entities
                .WithAll<ViewerCamera>()
                .ForEach((Entity e) =>
            {
                viewerCameras.Add(e);
            }).WithoutBurst().Run();
        }
    }
}