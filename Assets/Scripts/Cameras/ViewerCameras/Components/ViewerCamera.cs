using Unity.Entities;

namespace Zoxel.Cameras
{
    //! A tag for a viewer camera. Connected to a ViewerUI by CreatorLink.
    public struct ViewerCamera : IComponentData { }
}