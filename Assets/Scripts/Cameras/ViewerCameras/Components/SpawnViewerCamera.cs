using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Cameras
{
    public struct SpawnViewerCamera : IComponentData
    {
        public byte state;
        public int id;
        public byte cameraType;
        public Entity targetEntity;
        public float3 position;
        public quaternion rotation;
    }
}