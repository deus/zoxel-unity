﻿using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Zoxel.Transforms; // Parent Link - attaches to character head bone
using Zoxel.Rendering;
using Zoxel.Voxels;
using Zoxel.Players;
using Zoxel.Realms;
using Zoxel.Worlds;
using Zoxel.VoxelInteraction;
using Zoxel.Cameras.PostProcessing;
using Zoxel.Movement;
using Zoxel.Audio.Music;
using Zoxel.Rendering.Authoring;

namespace Zoxel.Cameras.TopDown
{
    //! Spawns a first person camera.
    /**
    *   - Spawn System -
    *   Has a ParentLink as attaches to head bone.
    *   \todo Use base camera prefab to build this one, from CameraSystemGroup.
    */
    [BurstCompile, UpdateInGroup(typeof(CameraSystemGroup))]
    public partial class TopDownCameraSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private bool hasSetSettings;
        public static Entity spawnTopDownCameraEventPrefab;
        private Entity topDownCameraPrefab;
        private NativeArray<Text> texts;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnTopDownCameraEventArchtype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnTopDownCamera),
                typeof(GenericEvent),
                typeof(DelayEvent));
            spawnTopDownCameraEventPrefab = EntityManager.CreateEntity(spawnTopDownCameraEventArchtype);
            var cameraArchtype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(ZoxID),
                typeof(TopDownCamera),
                typeof(Camera),
                typeof(CameraProjectionMatrix),
                typeof(CameraFrustrumPlanes),
                typeof(FieldOfView),
                typeof(CameraScreenRect),
                typeof(CameraEnabledState),
                typeof(PostExposure),
                typeof(ControllerLink),
                typeof(CharacterLink),
                typeof(ChunkStreamPoint),
                typeof(EntityVoxelPosition),
                typeof(DisableGravityRotation),
                typeof(GravityQuadrant),
                typeof(VoxLink),            // used for sky color
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(GameObjectLink),
                typeof(SpawnMusic),
                typeof(MusicLink),
                // Raycastinng
                typeof(NearbyEntities),
                typeof(ChunkLink),
                typeof(DynamicChunkLink),
                typeof(MouseRaycaster),
                typeof(RaycasterOrigin),
                typeof(RaycastCharacter),
                typeof(RaycastVoxel),
                typeof(SpawnPreviewMesh),
                typeof(GizmoLinks)
            );
            topDownCameraPrefab = EntityManager.CreateEntity(cameraArchtype);
            EntityManager.SetComponentData(topDownCameraPrefab, new GravityQuadrant(PlanetSide.Down));
            EntityManager.SetComponentData(topDownCameraPrefab, new CameraEnabledState(1));
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(topDownCameraPrefab); // , new EditorName("[top-down-camera]"));
            #endif
            texts = new NativeArray<Text>(2, Allocator.Persistent);
            texts[0] = new Text("[top-down-camera]");
            texts[1] = new Text("[test-realm]");
            RequireForUpdate<RenderSettings>();
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        private void InitializePrefabSettings()
        {
            if (!hasSetSettings)
            {
                hasSetSettings = true;
                var renderSettings = GetSingleton<RenderSettings>();
                var renderDistance = (byte) renderSettings.renderDistance;
                EntityManager.SetComponentData(topDownCameraPrefab, new ChunkStreamPoint(renderDistance));
            }
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var playerHome = PlayerSystemGroup.playerHome;
            var realmPrefab = RealmSystemGroup.realmPrefab;
            InitializePrefabSettings();
            var id = IDUtil.GenerateUniqueID();  // todo: generate ids inside jobs
            var screenDimensions = new int2(UnityEngine.Screen.width, UnityEngine.Screen.height);
            var orbitDepth = CameraManager.instance.cameraSettings.uiDepth;
            var fov = CameraManager.instance.cameraSettings.fieldOfView;
            var topDownCameraPrefab = this.topDownCameraPrefab;
            var spawnPlanetPrefab = WorldSystemGroup.spawnPlanetPrefab;
            var texts = this.texts;
            var mainMenuCameraEntity = new Entity();
            if (CameraReferences.cameras.Count > 0)
            {
                mainMenuCameraEntity = CameraReferences.cameras[0];
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DelayEvent>()
                .ForEach((Entity e, int entityInQueryIndex, in SpawnTopDownCamera spawnTopDownCamera) =>
            {
                var cameraEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, topDownCameraPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new ZoxID(id));
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new Camera(screenDimensions, orbitDepth, fov));
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new FieldOfView(fov));
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new Translation { Value = spawnTopDownCamera.position });
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new Rotation { Value = spawnTopDownCamera.rotation });
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new ControllerLink(spawnTopDownCamera.controller));
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnTopDownCamera.controller, new CameraLink(cameraEntity));
                #if UNITY_EDITOR
                PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new EditorName(texts[0].Clone()));
                #endif
                // destroy main menu camera
                if (mainMenuCameraEntity.Index > 0)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, mainMenuCameraEntity);
                }
                // Spawn realm and world!
                var realmEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, realmPrefab);
                PostUpdateCommands.AddComponent<GenerateSeed>(entityInQueryIndex, realmEntity);
                PostUpdateCommands.AddComponent<GenerateName>(entityInQueryIndex, realmEntity);
                PostUpdateCommands.SetComponent(entityInQueryIndex, realmEntity, new PlayerHomeLink(playerHome));
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnTopDownCamera.controller, new RealmLink(realmEntity));
                PostUpdateCommands.AddComponent<DisableSaving>(entityInQueryIndex, realmEntity);
                #if UNITY_EDITOR
                PostUpdateCommands.SetComponent(entityInQueryIndex, realmEntity, new EditorName(texts[1].Clone()));
                #endif
                var spawnPlanetEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnPlanetPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnPlanetEntity, new SpawnPlanet(realmEntity, spawnTopDownCamera.controller, 0));
                PostUpdateCommands.AddComponent<SpawnNewPlanet>(entityInQueryIndex, spawnPlanetEntity);
                PostUpdateCommands.AddComponent<DisableSaving>(entityInQueryIndex, spawnPlanetEntity);
            })  
                #if UNITY_EDITOR
                .WithReadOnly(texts)
                #endif
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                // typeof(LocalPosition),
                // typeof(LocalRotation),
                /// typeof(ParentLink),