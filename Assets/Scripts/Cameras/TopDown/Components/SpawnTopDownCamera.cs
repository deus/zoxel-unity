using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Cameras.TopDown
{
    //! An event that spawns a TopDownCamera entity.
    public struct SpawnTopDownCamera : IComponentData
    {
        public Entity controller;
        public float3 position;
        public quaternion rotation;

        public SpawnTopDownCamera(Entity controller, float3 position)
        {
            this.controller = controller;
            this.position = position;
            // this.rotation = quaternion.identity;
            var degreesToRadians = ((math.PI * 2) / 360f);
            // this.rotation = quaternion.EulerXYZ(new float3(90, 0, 0) * degreesToRadians);
            // this.rotation = quaternion.EulerXYZ(new float3(60, -45, 0) * degreesToRadians);
            this.rotation = quaternion.EulerXYZ(new float3(75, -45, 0) * degreesToRadians);
        }
    }
}