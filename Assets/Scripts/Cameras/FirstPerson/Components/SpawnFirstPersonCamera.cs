using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Cameras
{
    //! An event that spawns a first person camera.
    public struct SpawnFirstPersonCamera : IComponentData
    {
        public Entity controller;
        public float3 position;
        public quaternion rotation;

        public SpawnFirstPersonCamera(Entity controller, float3 position)
        {
            this.controller = controller;
            this.position = position;
            this.rotation = quaternion.identity;
        }

        public SpawnFirstPersonCamera(float3 position)
        {
            this.controller = new Entity();
            this.position = position;
            this.rotation = quaternion.identity;
        }
    }
}