﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Cameras
{
    //! A tag for a first person camera.
    public struct FirstPersonCamera : IComponentData { }
}