namespace Zoxel.Skeletons
{
    //! The HumanoidBoneType for editor.
    public enum HumanoidBoneTypeEditor : byte
    {
        None,
        Chest,
        Head,
        Shoulder,
        Bicep,
        Forearm,
        Hand,
        Hips,
        Thigh,
        Calf,
        Foot,
        Held,
        Hat
    }
}