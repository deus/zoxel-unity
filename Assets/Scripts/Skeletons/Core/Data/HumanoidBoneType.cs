namespace Zoxel.Skeletons
{
    //! The type of humanoid bone.
    public static class HumanoidBoneType
    {
        public const byte None = 0;
        public const byte Chest = 1;
        public const byte Head = 2;
        public const byte Shoulder = 3;
        public const byte Bicep = 4;
        public const byte Forearm = 5;
        public const byte Hand = 6;
        public const byte Hips = 7;
        public const byte Thigh = 8;
        public const byte Calf = 9;
        public const byte Foot = 10;
        public const byte Hat = 11;
        public const byte Held = 12;
    }
}