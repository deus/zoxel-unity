namespace Zoxel.Skeletons
{
    //! An operation on a Vox during the body build process.
    /**
    *   Any oprations i want to perform on the vox data itself as it gets used in the equipment system
    *   \todo Add Grow, Shrink modifiers.
    *   \todo Add Noise modifier.
    */
    public static class VoxModifier
    {
        public const byte None = 0;
        public const byte RotateXY = 1;
        public const byte RotateX = 2;
        public const byte RotateY = 3;
    }
}