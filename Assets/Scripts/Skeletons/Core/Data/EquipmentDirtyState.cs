using Unity.Entities;

namespace Zoxel.Skeletons
{
    //! The state during which the boneLinks vox body generates.
    public static class EquipmentDirtyState
    {
        public const byte BuildSkeleton = 0;
        public const byte ResizingModel = 1;
        public const byte PlaceVoxels = 2;
        public const byte SetSkeletonSize = 3;
        public const byte SpawnBones = 4;
        public const byte SetBones = 5;
        public const byte SetBonesWaiting = 6;
        public const byte BuildHeldItems = 7;
    }
}