namespace Zoxel.Skeletons
{
    //! Data for performing an operation on a vox based on the slot it's added into.
    public static class VoxOperation
    {
        public const byte None = 0;
        public const byte FlipX = 1;
    }
}