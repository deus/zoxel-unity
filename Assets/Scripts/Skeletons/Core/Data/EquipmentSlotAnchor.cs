using System;

namespace Zoxel.Skeletons
{
    //! Used to equip an equipment piece to a parent one.
    public static class EquipmentSlotAnchor
    {
        public const byte Center = 0;
        public const byte Left = 1;
        public const byte Right = 2;
        public const byte Bottom = 3;
        public const byte Top = 4;
        public const byte Back = 5;
        public const byte Front = 6;
    }
}