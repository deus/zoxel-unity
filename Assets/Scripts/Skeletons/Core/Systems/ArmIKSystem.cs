using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Animations;
using Zoxel.Movement;

namespace Zoxel.Skeletons
{
    //! Makes the arm point at things.
    /**
    *   Uses DEBUG_ARM_IK to debug.
    */
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class ArmIKSystem : SystemBase
    {
        private EntityQuery boneQuery;

        protected override void OnCreate()
        {
            boneQuery = GetEntityQuery(
                ComponentType.ReadOnly<Bone>(),
                ComponentType.ReadOnly<ParentLink>(),
                ComponentType.ReadWrite<LocalRotation>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (boneQuery.IsEmpty)
            {
                return;
            }
            var degreesToRadians = ((math.PI * 2) / 360f);
            var elapsedTime = World.Time.ElapsedTime;
            var deltaTime = World.Time.DeltaTime;
            var turnSpeed = 6f;
            var ninteyDegrees = quaternion.EulerXYZ(new float3(-90, 0, 0) * degreesToRadians);
            var sixetyDegrees = quaternion.EulerXYZ(new float3(-76, 0, 0) * degreesToRadians);
            var boneEntities = boneQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var parentLinks = GetComponentLookup<ParentLink>(true);
            var bonePositions = GetComponentLookup<Translation>(true);
            var boneRotations = GetComponentLookup<Rotation>(true);
            var localRotations = GetComponentLookup<LocalRotation>(false);
            var uniqueRotations = GetComponentLookup<BoneUniqueRotation>(false);
            var cameraBobs = GetComponentLookup<CameraBob>(true);
            var shoulderBones = GetComponentLookup<ShoulderBone>(true);
            var bicepBones = GetComponentLookup<BicepBone>(true);
            boneEntities.Dispose();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity, SkeletonDirty>()
                .ForEach((Entity e, in BoneLinks boneLinks, in Translation translation, in Rotation rotation, in Animator animator, in GravityQuadrant gravityQuadrant) =>
            {
                var lerpSpeed = turnSpeed;
                var cameraPosition = float3.zero;
                var cameraRotation = quaternion.identity;
                if (HasComponent<CameraLink>(e))
                {
                    cameraPosition = animator.cameraPosition;
                    cameraRotation = animator.cameraRotation;
                }
                else
                {
                    for (var i = 0; i < boneLinks.bones.Length; i++)
                    {
                        var boneEntity = boneLinks.bones[i];
                        if (cameraBobs.HasComponent(boneEntity)) // BicepBone
                        {
                            cameraPosition = bonePositions[boneEntity].Value;
                            cameraRotation = boneRotations[boneEntity].Value;
                            break;
                        }
                    }
                }
                var armIndex = 0;
                var boneRotation = quaternion.identity;
                for (var i = 0; i < boneLinks.bones.Length; i++)
                {
                    var boneEntity = boneLinks.bones[i];
                    if (shoulderBones.HasComponent(boneEntity)) // BicepBone
                    {
                        if (!(animator.rightArmState == AnimationState.PreAttack || animator.rightArmState == AnimationState.Attacking))
                        {
                            continue;
                        }
                        armIndex++;
                        if (armIndex == 1)
                        {
                            continue;
                        }
                        boneRotation = localRotations[boneEntity].rotation;
                        break;
                    }
                }

                var targetRotation = gravityQuadrant.rotation;
                // calculate IK Target
                var targetPosition = cameraPosition + 
                    //math.rotate(cameraRotation, new float3(0, 0, 0.3f));
                    math.rotate(math.inverse(rotation.Value), math.rotate(cameraRotation, new float3(0, 0, 0.3f))); //  + new float3(0, valueY, - valueY)));
                var targetDirection = math.normalizesafe(targetPosition - cameraPosition);   // safe
                var targetLookDirection = quaternion.LookRotation(targetDirection, new float3(0, 1, 0));
                targetRotation = math.mul(ninteyDegrees, targetLookDirection);

                // debug lines
                //UnityEngine.Debug.DrawLine(cameraPosition, targetPosition, UnityEngine.Color.red);
                #if DEBUG_ARM_IK
                    UnityEngine.Debug.DrawLine(cameraPosition, cameraPosition + math.rotate(rotation.Value, math.rotate(targetLookDirection, new float3(0, 0, 1))), UnityEngine.Color.red);
                    UnityEngine.Debug.DrawLine(cameraPosition, cameraPosition + math.rotate(rotation.Value, math.rotate(targetRotation, new float3(0, 0, 1))), UnityEngine.Color.grey);
                #endif
                if (animator.rightArmState == AnimationState.Attacking)
                {
                    var timePassed = elapsedTime - animator.rightArmAnimationStartTime;
                    timePassed /= animator.animationSpeed;
                    //timePassed *= 0.5f;
                    var valueY = math.sin((float) timePassed * math.PI);
                    //var rotateBy = QuaternionHelpers.slerp(quaternion.identity, sixetyDegrees, valueY);
                    //targetRotation = math.mul(rotateBy, targetRotation);
                    targetRotation = QuaternionHelpers.slerp(targetRotation, math.mul(sixetyDegrees, targetRotation), valueY);
                    lerpSpeed = 60;
                    //UnityEngine.Debug.LogError("valueY: " + valueY);
                }
                armIndex = 0;
                for (var i = 0; i < boneLinks.bones.Length; i++)
                {
                    var boneEntity = boneLinks.bones[i];
                    if (cameraBobs.HasComponent(boneEntity)) // BicepBone
                    {
                        var uniqueRotation = uniqueRotations[boneEntity];
                        //uniqueRotation.rotation = QuaternionHelpers.slerpSafe(uniqueRotation.rotation, targetLookDirection, deltaTime * turnSpeed);
                        //uniqueRotations[boneEntity] = uniqueRotation;
                    }
                    else if (shoulderBones.HasComponent(boneEntity)) // BicepBone
                    {
                        if (!(animator.rightArmState == AnimationState.PreAttack || animator.rightArmState == AnimationState.Attacking))
                        {
                            continue;
                        }
                        armIndex++;
                        if (armIndex == 1)
                        {
                            continue;
                        }
                        var parentSynch = localRotations[boneEntity];
                        if (lerpSpeed == 60)
                        {
                            parentSynch.rotation = targetRotation;
                        }
                        else
                        {
                            parentSynch.rotation = QuaternionHelpers.slerpSafe(parentSynch.rotation, targetRotation, deltaTime * lerpSpeed);
                        }
                        localRotations[boneEntity] = parentSynch;
                        // for forearm bone
                        var bicepBoneEntity = new Entity();
                        for (var j = 0; j < boneLinks.bones.Length; j++)
                        {
                            if (i != j)
                            {
                                var boneEntity2 = boneLinks.bones[j];
                                if (bicepBones.HasComponent(boneEntity2) && parentLinks[boneEntity2].parent == boneEntity)  // ForearmBone
                                {
                                    bicepBoneEntity = boneEntity2;
                                }
                            }
                        }
                        // Rotate Bicep as well
                        /*var parentSynch2 = localRotations[bicepBoneEntity];
                        //parentSynch2.rotation.value.y = sinAddition;
                        parentSynch.rotation = targetRotation;
                        localRotations[bicepBoneEntity] = parentSynch2;*/
                        // UnityEngine.Debug.LogError("targetRotation: " + targetRotation);
                    }
                }
            })  .WithReadOnly(parentLinks)
                .WithReadOnly(bonePositions)
                .WithReadOnly(boneRotations)
                .WithNativeDisableContainerSafetyRestriction(localRotations)
                .WithNativeDisableContainerSafetyRestriction(uniqueRotations)
                .WithReadOnly(cameraBobs)
                .WithReadOnly(shoulderBones)
                .WithReadOnly(bicepBones)
                .ScheduleParallel(Dependency);
        }
    }
}

//var sinAddition = 0.1f * math.sin((float)elapsedTime);
//parentSynch.rotation.value.y = sinAddition;
//parentSynch.rotation = targetRotation;
/*public static class MoreMath
{
    /// <summary>
    /// Converts quaternion representation to euler
    /// </summary>
    public static float3 ToEuler(this quaternion quaternion)
    {
        float4 q = quaternion.value;
        double3 res;

        double sinr_cosp = +2.0 * (q.w * q.x + q.y * q.z);
        double cosr_cosp = +1.0 - 2.0 * (q.x * q.x + q.y * q.y);
        res.x = math.atan2(sinr_cosp, cosr_cosp);

        double sinp = +2.0 * (q.w * q.y - q.z * q.x);
        if (math.abs(sinp) >= 1) {
            res.y = math.PI / 2 * math.sign(sinp);
        } else {
            res.y = math.asin(sinp);
        }

        double siny_cosp = +2.0 * (q.w * q.z + q.x * q.y);
        double cosy_cosp = +1.0 - 2.0 * (q.y * q.y + q.z * q.z);
        res.z = math.atan2(siny_cosp, cosy_cosp);

        return (float3) res;
    }
}*/