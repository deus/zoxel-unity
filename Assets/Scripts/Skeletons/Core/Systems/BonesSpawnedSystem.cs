using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Skeletons
{
    //! Links the bones to the boneLinks after spawning.
    /**
    *   - Linking System -
    *   \todo SkeletonBuilt event that camera system uses to reposition first person camera.
    */
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class BonesSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery boneQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();;
            boneQuery = GetEntityQuery(
                ComponentType.ReadOnly<NewBone>(),
                ComponentType.ReadOnly<Bone>(),
                ComponentType.ReadOnly<SkeletonLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<OnBonesSpawned>(processQuery);
            PostUpdateCommands2.RemoveComponent<NewBone>(boneQuery);
            //! First initializes bones.
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((ref BoneLinks boneLinks, in OnBonesSpawned onBonesSpawned) =>
            {
                if (onBonesSpawned.spawned != 0)
                {
                    boneLinks.Initialize(onBonesSpawned.spawned);
                }
            }).ScheduleParallel(Dependency);
            //! For each bone, using the index, sets into parents bones that is passed in.
            var parentEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var boneLinks = GetComponentLookup<BoneLinks>(false);
            parentEntities.Dispose();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<NewBone, Bone>()
                .ForEach((Entity e, in Bone bone, in SkeletonLink skeletonLink) =>
            {
                var boneLinks2 = boneLinks[skeletonLink.skeleton];
                boneLinks2.bones[bone.index] = e;
            })  .WithNativeDisableContainerSafetyRestriction(boneLinks)
                .ScheduleParallel(Dependency);
        }
    }
}