using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Skeletons
{
    //! Disposes of Skeletons
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class BoneLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithAll<DestroyEntity, BoneLinks>()
                .ForEach((int entityInQueryIndex, in BoneLinks boneLinks) =>
			{
                for (int i = 0; i < boneLinks.bones.Length; i++)
                {
                    var bone = boneLinks.bones[i];
                    if (HasComponent<Bone>(bone))
                    {
                        PostUpdateCommands.DestroyEntity(entityInQueryIndex, bone);
                    }
                }
                boneLinks.DisposeFinal();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}