using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Skeletons
{
    //! Disposes of HeldWeaponLinks.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class HeldWeaponLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyEntity, HeldWeaponLinks>()
                .ForEach((int entityInQueryIndex, in HeldWeaponLinks heldWeaponLinks) =>
            {
                if (heldWeaponLinks.heldWeaponRightHand.Index > 0)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, heldWeaponLinks.heldWeaponRightHand);
                }
                if (heldWeaponLinks.heldWeaponLeftHand.Index > 0)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, heldWeaponLinks.heldWeaponLeftHand);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}