using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Skeletons
{
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class ChestBreathSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            var breatheAmount = 0.08f;
            var breatheSpeed = 1.3f;
            var elapsedTime = (float) World.Time.ElapsedTime  * breatheSpeed;
            Entities
                .WithAll<ChestBone>()
                .ForEach((ref BoneUniqueScale boneUniqueScale) =>
            {
                var sinAddition = breatheAmount * math.sin(elapsedTime);
                boneUniqueScale.scale = new float3(1, 1, 1 + sinAddition);
            }).ScheduleParallel();
        }
    }
}

/*var sinAmount = 0.05f;
Entities
    .WithAll<BicepBone>()
    .ForEach((ref ParentSynch parentSynch) =>
{
    var sinAddition = sinAmount * math.sin((float)elapsedTime);
    parentSynch.rotation.value.y = sinAddition;
}).ScheduleParallel();
Entities
    .WithAll<ForearmBone>()
    .ForEach((ref ParentSynch parentSynch) =>
{
    var sinAddition = sinAmount * math.sin((float)elapsedTime);
    parentSynch.rotation.value.y = sinAddition;
}).ScheduleParallel();
Entities
    .WithAll<HandBone>()
    .ForEach((ref ParentSynch parentSynch) =>
{
    var sinAddition = sinAmount * math.sin((float)elapsedTime);
    parentSynch.rotation.value.y = sinAddition;
}).ScheduleParallel();*/