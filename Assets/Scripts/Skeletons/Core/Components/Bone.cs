using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Skeletons
{
    //! A transform used for animating a mesh.
    public struct Bone : IComponentData
    {
        public byte index;       // used for referencing
        // Voxel Data
        public int3 bonePosition;
        public int3 size;
        public int3 influenceMin;
        public int3 influenceMax;
        // Skinning Data
        public UnityEngine.Matrix4x4 originalMatrix;
        public UnityEngine.Matrix4x4 skinningMatrix;
        public UnityEngine.Matrix4x4 inverseSkinningMatrix;

        public float3 GetSkinningBonePosition()
        {
            float3 skinningBonePosition = ((UnityEngine.Vector3)skinningMatrix.GetColumn(3));
            return skinningBonePosition;
        }

        public float3 GetOriginalLocalPosition(in Bone parentBone)
        {
            return GetOriginalPosition() - parentBone.GetOriginalPosition();
        }

        public float3 GetOriginalPosition()
        {
            return (float3) ((UnityEngine.Vector3)originalMatrix.GetColumn(3));
        }

        public quaternion GetOriginalRotation()
        {
            return originalMatrix.rotation;
        }

        public float3 GetInfluenceSize(float3 voxelSize)
        {
            return (influenceMax - influenceMin).ToFloat3() / voxelSize;    // 1.03
        }

        public float3 GetInfluenceOffset(float3 voxelSize)
        {
            var midPoint = GetInfluenceSize(voxelSize) / 2f;
            return (influenceMax.ToFloat3() / voxelSize) - midPoint;
        }
    }
}