using Unity.Entities;

namespace Zoxel.Skeletons
{
    //! Data for a bone type.
    public struct BoneType : IComponentData
    {
        //! Relating to HumanoidBoneType. Used for special components and IK
        public byte boneType;

        public BoneType(byte boneType)
        {
            this.boneType = boneType;
        }
    }
}