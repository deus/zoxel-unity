using Unity.Entities;

namespace Zoxel.Skeletons
{
    public struct HeldWeapon : IComponentData
    {
        public byte heldWeaponIndex;

        public HeldWeapon(byte heldWeaponIndex)
        {
            this.heldWeaponIndex = heldWeaponIndex;
        }
    }
}