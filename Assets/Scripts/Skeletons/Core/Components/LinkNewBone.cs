using Unity.Entities;

namespace Zoxel.Skeletons
{
    //! Used to identify a Bone that needs to link to it's Skeleton.
    public struct NewBone : IComponentData { }
}