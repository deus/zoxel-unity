using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Skeletons
{
    //! Holds links to bones!
    public struct BoneLinks : IComponentData
    {
        public BlitableArray<Entity> bones;

        public void Initialize(int bonesCount)
        {
            this.Dispose();
            this.bones = new BlitableArray<Entity>(bonesCount, Allocator.Persistent);
        }

        public void DisposeFinal()
        {
            bones.DisposeFinal();
        }

        public void Dispose()
        {
            bones.Dispose();
        }
    }
}