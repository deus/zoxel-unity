using Unity.Entities;

namespace Zoxel.Skeletons
{
    public struct OnSpawnedHeldWeapons : IComponentData
    {
        public byte heldWeaponsSpawned;

        public OnSpawnedHeldWeapons(byte heldWeaponsSpawned)
        {
            this.heldWeaponsSpawned = heldWeaponsSpawned;
        }
    }
}