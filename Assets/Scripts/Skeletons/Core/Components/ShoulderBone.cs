using Unity.Entities;

namespace Zoxel.Skeletons
{
    public struct ChestBone : IComponentData { }
    public struct HipsBone : IComponentData { }
    public struct HeadBone : IComponentData { }
    public struct ShoulderBone : IComponentData { }
    public struct BicepBone : IComponentData { }
    public struct ForearmBone : IComponentData { }
    public struct HandBone : IComponentData { }
    public struct HeldBone : IComponentData { }
    public struct ThighBone : IComponentData { }
}