using Unity.Entities;

namespace Zoxel.Skeletons
{
    //! An event for linking up Bone entities after spawning.
    public struct OnBonesSpawned : IComponentData
    {
        public byte spawned;

        public OnBonesSpawned(byte spawned)
        {
            this.spawned = spawned;
        }
    }
}