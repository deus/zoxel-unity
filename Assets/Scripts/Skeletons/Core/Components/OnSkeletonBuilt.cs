using Unity.Entities;

namespace Zoxel.Skeletons
{
    //! Event for boneLinks built, which reuploads new data to gpu.
    public struct OnSkeletonBuilt : IComponentData { }
}