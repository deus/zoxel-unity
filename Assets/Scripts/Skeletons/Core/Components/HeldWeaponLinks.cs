using Unity.Entities;

namespace Zoxel.Skeletons
{
    //! Links to held items
    public struct HeldWeaponLinks : IComponentData
    {
        public Entity heldWeaponRightHand;
        public Entity heldWeaponLeftHand;
        public Entity rightHand;
        public Entity leftHand;
    }
}