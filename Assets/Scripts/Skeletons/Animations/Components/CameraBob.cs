using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Skeletons
{
    //! Bobs the camera bone up and down.
    public struct CameraBob : IComponentData
    {
        public float time;
        public float3 position;
        public float bobHeight;
    }
}