﻿using Unity.Entities;
using Unity.Burst;
using Zoxel.Movement;
using Zoxel.Animations;

namespace Zoxel.Skeletons
{
    // Make a system that sets the animators state depending on the bodyForce speed
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class AnimatorWalkSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
		{
            float walkingThreshold = 0.08f;
            Entities
                .WithNone<DisableForce>()
                .ForEach((ref Animator animator, in BodyForce bodyForce, in GravityQuadrant gravityQuadrant) =>
            {
                var isIdle = bodyForce.velocity.z >= -walkingThreshold && bodyForce.velocity.z <= walkingThreshold
                    && bodyForce.velocity.x >= -walkingThreshold && bodyForce.velocity.x <= walkingThreshold;
                if (gravityQuadrant.quadrant == PlanetSide.Down || gravityQuadrant.quadrant == PlanetSide.Up)
                {
                    isIdle = bodyForce.velocity.z >= -walkingThreshold && bodyForce.velocity.z <= walkingThreshold
                        && bodyForce.velocity.x >= -walkingThreshold && bodyForce.velocity.x <= walkingThreshold;
                }
                else if (gravityQuadrant.quadrant == PlanetSide.Left || gravityQuadrant.quadrant == PlanetSide.Right)
                {
                    isIdle = bodyForce.velocity.z >= -walkingThreshold && bodyForce.velocity.z <= walkingThreshold
                        && bodyForce.velocity.y >= -walkingThreshold && bodyForce.velocity.y <= walkingThreshold;
                }
                else if (gravityQuadrant.quadrant == PlanetSide.Backward || gravityQuadrant.quadrant == PlanetSide.Forward)
                {
                    isIdle = bodyForce.velocity.x >= -walkingThreshold && bodyForce.velocity.x <= walkingThreshold
                        && bodyForce.velocity.y >= -walkingThreshold && bodyForce.velocity.y <= walkingThreshold;
                }
                if (isIdle)
                {
                    if (animator.legsState != AnimationState.Idle)
                    {
                        animator.setLegsState = AnimationState.Idle;
                    }
                    if (animator.leftArmState != AnimationState.Idle)
                    {
                        animator.setLeftArmState = AnimationState.Idle;
                    }
                    if (animator.rightArmState == AnimationState.Walking)
                    {
                        animator.setRightArmState = AnimationState.Idle;
                    }
                }
                else
                {
                    if (animator.legsState != AnimationState.Walking)
                    {
                        animator.setLegsState = AnimationState.Walking;
                    }
                    if (animator.leftArmState != AnimationState.Walking)
                    {
                        animator.setLeftArmState = AnimationState.Walking;
                    }
                    if (animator.rightArmState == AnimationState.Idle)
                    {
                        animator.setRightArmState = AnimationState.Walking;
                    }
                }
            }).ScheduleParallel();
        }
    }
}