﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Animations;

namespace Zoxel.Skeletons
{
    //! Sets arms to swing or not.
    /**
    *   \todo Set ComponentLookup bone rotations rather than PostUpdateCommands
    */
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class SkeletonArmsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var elapsedTime = World.Time.ElapsedTime;
            Dependency = Entities
                .WithNone<EntityBusy, SkeletonDirty>()
                .ForEach((Entity e, int entityInQueryIndex, ref Animator animator, in BoneLinks boneLinks) =>
            {
                if (boneLinks.bones.Length == 0)
                {
                    return;
                }
                /*for (byte i = 0; i < boneLinks.bones.Length; i++) 
                {
                    var boneEntity = boneLinks.bones[i];
                    if (boneEntity.Index <= 0 || !HasComponent<Bone>(boneEntity) || HasComponent<DestroyEntity>(boneEntity))
                    {
                        return;
                    }
                }*/
                if (animator.leftArmState != animator.setLeftArmState)
                {
                    animator.leftArmState = animator.setLeftArmState;
                    animator.leftArmAnimationStartTime = elapsedTime;
                    var shoulderIndex = (byte) 255;
                    var armCount = 0;
                    for (byte i = 0; i < boneLinks.bones.Length; i++) 
                    {
                        var boneEntity = boneLinks.bones[i];
                        if (HasComponent<ShoulderBone>(boneEntity))
                        {
                            if (armCount == 0)
                            {
                                shoulderIndex = i;
                                break;
                            }
                            armCount++;
                        }
                    }
                    // UnityEngine.Debug.LogError("Left shoulder index: " + shoulderIndex);
                    if (shoulderIndex == 255)
                    {
                        return;
                    }
                    SetArmAnimations(PostUpdateCommands, entityInQueryIndex, in boneLinks, 1, shoulderIndex, animator.leftArmState, animator.animationSpeed, elapsedTime);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<EntityBusy, SkeletonDirty, OnBonesSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref Animator animator, in BoneLinks boneLinks) =>
            {
                if (boneLinks.bones.Length == 0)
                {
                    return;
                }
                if (animator.rightArmState != animator.setRightArmState)
                {
                    var previousState = animator.rightArmState;
                    animator.rightArmState = animator.setRightArmState;
                    animator.rightArmAnimationStartTime = elapsedTime;
                    // Get Indexes
                    var shoulderIndex = (byte) 255;
                    var shoulderIndex2 = (byte) 255;
                    var armCount = 0;
                    for (byte i = 0; i < boneLinks.bones.Length; i++) 
                    {
                        var boneEntity = boneLinks.bones[i];
                        if (HasComponent<ShoulderBone>(boneEntity))
                        {
                            if (armCount == 0)
                            {
                                shoulderIndex2 = i;
                            }
                            else if (armCount == 1)
                            {
                                shoulderIndex = i;
                                break;
                            }
                            armCount++;
                        }
                    }
                    // UnityEngine.Debug.LogError("Right shoulder index: " + shoulderIndex);
                    if (shoulderIndex == 255)
                    {
                        return;
                    }
                    // UnityEngine.Debug.LogError("Set rightArmState to: " + (AnimationState)animator.rightArmState);
                    if (animator.rightArmState == AnimationState.PreAttack || animator.rightArmState == AnimationState.Attacking)
                    {
                        var shoulderBone = boneLinks.bones[shoulderIndex];
                        if (HasComponent<LerpRotation>(shoulderBone))
                        {
                            PostUpdateCommands.RemoveComponent<LerpRotation>(entityInQueryIndex, shoulderBone);
                        }
                        return;
                    }
                    SetArmAnimations(PostUpdateCommands, entityInQueryIndex, in boneLinks, 0, shoulderIndex, animator.rightArmState, animator.animationSpeed, elapsedTime);
                    if ((previousState == AnimationState.PreAttack || previousState == AnimationState.Attacking)
                        && (animator.leftArmState == animator.setLeftArmState)
                        && HasComponent<LerpRotation>(boneLinks.bones[shoulderIndex2]))
                    {
                        SetArmAnimations(PostUpdateCommands, entityInQueryIndex, in boneLinks, 1, shoulderIndex2, animator.leftArmState, animator.animationSpeed, elapsedTime);
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static void SetArmAnimations(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, in BoneLinks boneLinks,
            byte armIndex, byte shoulderIndex, byte state, float animationSpeed, double elapsedTime) 
        {
            if (state == AnimationState.Idle)
            {
                SetArmIdle(PostUpdateCommands, entityInQueryIndex, in boneLinks, armIndex, shoulderIndex, elapsedTime);
            }
            else if (state == AnimationState.Walking)
            {
                SetArmWalking(PostUpdateCommands, entityInQueryIndex, in boneLinks, armIndex, shoulderIndex, elapsedTime);
            }
        }

        // todo: make it just slightly rotate arms instead
        public static void SetArmIdle(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, in BoneLinks boneLinks, byte armIndex,
            byte shoulderIndex, double elapsedTime) 
        {
            float swingRotation = (8 / 32f) * 32;
            SetArmSwing(PostUpdateCommands, entityInQueryIndex, in boneLinks, swingRotation, 1.14f, armIndex, shoulderIndex, elapsedTime);
        }

        
        public static void SetArmWalking(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, in BoneLinks boneLinks, byte armIndex,
            byte shoulderIndex, double elapsedTime) 
        {
            float swingRotation = (8 / 32f) * 64 * 2;
            SetArmSwing(PostUpdateCommands, entityInQueryIndex, in boneLinks, swingRotation, 0.3f, armIndex, shoulderIndex, elapsedTime);
        }

        public static void SetArmSwing(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, in BoneLinks boneLinks, 
            float swingRotation, float swingSpeed, byte armIndex, byte shoulderIndex, double elapsedTime) 
        {
            // dynamic boneEntity positions for arms
            var boneEntity = boneLinks.bones[shoulderIndex];
            var degreesToRadians = ((math.PI * 2) / 360f);
            var rotationOffset = new float3(swingRotation, 0, 0) * degreesToRadians;
            var rotationBegin = quaternion.EulerXYZ(rotationOffset);
            var rotationEnd = quaternion.EulerXYZ(-rotationOffset);
            var lerpRotation = new LerpRotation
            {
                createdTime = elapsedTime,
                lifeTime = swingSpeed,
                rotationBegin = rotationBegin,
                rotationEnd = rotationEnd,
                loop = AnimationLoopType.Loop,
                type = 1
            };
            if (armIndex == 1)
            { 
                lerpRotation.rotationBegin = rotationEnd;
                lerpRotation.rotationEnd = rotationBegin;
            }
            // UnityEngine.Debug.LogError("Set Arm Swing! BoneIndex: " + boneEntity.Index + " :: ArmIndex: " + armIndex);
            PostUpdateCommands.AddComponent(entityInQueryIndex, boneEntity, lerpRotation);
        }
    }
}
// TODO:
//  Give Shoulder IK component - and links to other bones in series
//      For each bone in link - set rotation - to reach target hand point
//  Debug lines for arm to reach point
//  Set reach point in front of camera
// Another system that sets a reach point based on camera
// Entities.ForEach processes each set of ComponentData on the main thread. This is not the recommended
// method for best performance. However, we start with it here to demonstrate the clearer separation
// between SystemBase Update (logic) and ComponentData (data).
// There is no update logic on the individual ComponentData.
// a component system that sets the renderers values if the animator state has changed last frame
// atm  bones get added animation components every time state changes
// Switch this to sending signals to the bones once a state changes
// Signals can propogate through the heirarchy of bones as well with delays
// States can be for walking, and attacking, at the same time
// send commands to bones to animate a certain way

/*for (byte i = 0; i < boneLinks.bones.Length; i++) 
{
    var boneEntity = boneLinks.bones[i];
    if (boneEntity.Index <= 0 || !HasComponent<Bone>(boneEntity) || HasComponent<DestroyEntity>(boneEntity))
    {
        return;
    }
}*/