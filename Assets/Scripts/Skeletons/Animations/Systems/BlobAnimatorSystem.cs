using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Zoxel.Animations;

namespace Zoxel.Skeletons
{
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class BlobAnimatorSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var elapsedTime = World.Time.ElapsedTime;
            var defaultScale = new float3(1,1,1);
            var growthAttackScale = defaultScale * 1.2f;
            Dependency = Entities
                .WithNone<BoneLinks, ScaleLerpOnce>()
                .ForEach((Entity e, int entityInQueryIndex, ref Animator animator) =>
            {
                if (animator.coreState != animator.setCoreState)
                {
                    animator.coreState = animator.setCoreState;
                    animator.coreAnimationStartTime = elapsedTime;
                    // Add Scaler animation for NPCs
                    if (animator.coreState == AnimationState.Attacking)
                    {
                        var lerpScale = new LerpScale(elapsedTime, animator.animationSpeed / 2f,
                                    defaultScale, growthAttackScale, AnimationLoopType.Reverse);
                        if (!HasComponent<LerpScale>(e))
                        {
                            PostUpdateCommands.AddComponent(entityInQueryIndex, e, lerpScale);
                        }
                        else
                        {
                            PostUpdateCommands.SetComponent(entityInQueryIndex, e, lerpScale);
                        }
                        PostUpdateCommands.AddComponent<ScaleLerpOnce>(entityInQueryIndex, e);
                    }
                    else if (HasComponent<LerpScale>(e))
                    {
                        PostUpdateCommands.RemoveComponent<LerpScale>(entityInQueryIndex, e);
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}