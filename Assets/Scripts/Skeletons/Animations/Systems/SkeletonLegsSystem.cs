﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;

using Zoxel.Transforms;
using Zoxel.Animations;
// todo: rotate legs instead
// todo: position feet to touch ground during walk cycle
// todo: bones to rotate towards their positions in the cycle rather then directly teleport back to them

namespace Zoxel.Skeletons
{
    //! Sends commands to bones to animate a certain way!
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class SkeletonLegsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery bonesQuery;
        const float legSwingLength = 1.4f / 32f;
        const float swingSpeed = 0.34f;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.ReadWrite<Animator>(),
                ComponentType.ReadOnly<BoneLinks>(),
                ComponentType.Exclude<EntityBusy>());
            bonesQuery = GetEntityQuery(ComponentType.ReadOnly<Bone>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            // Re set components, maybbe components should use entity as reference
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<OnSkeletonBuilt>()
                .ForEach((int entityInQueryIndex, ref Animator animator, in BoneLinks boneLinks) =>
            {
                animator.leftArmState = AnimationState.None;
                animator.rightArmState = AnimationState.None;
                animator.legsState = AnimationState.None;
                for (int i = 0; i < boneLinks.bones.Length; i++)
                {
                    var boneEntity = boneLinks.bones[i];
                    if (HasComponent<LerpPosition>(boneEntity))
                    {
                        PostUpdateCommands.RemoveComponent<LerpPosition>(entityInQueryIndex, boneEntity);
                    }
                    if (HasComponent<LerpRotation>(boneEntity))
                    {
                        PostUpdateCommands.RemoveComponent<LerpRotation>(entityInQueryIndex, boneEntity);
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            if (processQuery.IsEmpty)
            {
                return;
            }
            var elapsedTime = World.Time.ElapsedTime;
            var boneEntities = bonesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var bones = GetComponentLookup<Bone>(true);
            boneEntities.Dispose();
            Dependency = Entities
                .WithNone<OnSkeletonBuilt>()
                .WithNone<EntityBusy, SkeletonDirty, OnBonesSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref Animator animator, in BoneLinks boneLinks) =>
            {
                // set renderer if updated
                if (animator.legsState != animator.setLegsState)
                {
                    byte leftLegBegin = 255;
                    byte rightLegBegin = 255;
                    for (byte i = 0; i < boneLinks.bones.Length; i++)
                    {
                        var boneEntity = boneLinks.bones[i];
                        if (!HasComponent<Bone>(boneEntity))
                        {
                            return; // bone is dead
                        }
                        if (leftLegBegin == 255)
                        {
                            if (HasComponent<ThighBone>(boneEntity))
                            {
                                leftLegBegin = i;
                            }
                        }
                        else 
                        {
                            if (HasComponent<ThighBone>(boneEntity))
                            {
                                rightLegBegin = i;
                                break;
                            }
                        }
                    }
                    // Get Indexes
                    byte hipsIndex = 255;
                    for (byte i = 0; i < boneLinks.bones.Length; i++) 
                    {
                        var boneEntity = boneLinks.bones[i];
                        if (HasComponent<HipsBone>(boneEntity))
                        {
                            hipsIndex = i;
                            break;
                        }
                    }
                    if (leftLegBegin == 255 || rightLegBegin == 255 || hipsIndex == 255)
                    {
                        return;
                    }
                    // UnityEngine.Debug.LogError("leftLegBegin: " + leftLegBegin + ", rightLegBegin " + rightLegBegin + ", hipsIndex " + hipsIndex);
                    animator.legsState = animator.setLegsState;
                    animator.legAnimationStartTime = elapsedTime;
                    // UnityEngine.Debug.LogError("Set legs to: " + (AnimationState)animator.legsState);
                    if (animator.legsState == AnimationState.Idle)
                    {
                        //SetLegsIdle(PostUpdateCommands, boneLinks);
                        for (int i = 0; i < boneLinks.bones.Length; i++) 
                        {
                            if (i >= rightLegBegin && i <= rightLegBegin + 2 || i >= leftLegBegin && i <= leftLegBegin + 2)
                            {
                                var bone = boneLinks.bones[i];
                                if (HasComponent<LerpPosition>(bone))
                                {
                                    PostUpdateCommands.RemoveComponent<LerpPosition>(entityInQueryIndex, bone);
                                }
                            }
                        }
                    }
                    else if (animator.legsState == AnimationState.Walking)
                    {
                        var bonesList = new NativeArray<Bone>(boneLinks.bones.Length, Allocator.Temp);
                        for (int i = 0; i < boneLinks.bones.Length; i++)
                        {
                            bonesList[i] = bones[boneLinks.bones[i]];
                        }
                        SetLegsWalking(PostUpdateCommands, entityInQueryIndex, in boneLinks, elapsedTime, in bonesList, hipsIndex, leftLegBegin, rightLegBegin);
                        bonesList.Dispose();
                    }
                }
            })  .WithReadOnly(bones)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
        
        public static void SetLegsWalking(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, in BoneLinks boneLinks,
            double elapsedTime, in NativeArray<Bone> bones, byte hipsIndex, byte leftLegBegin, byte rightLegBegin) 
        {
            SetBoneLerp(PostUpdateCommands, entityInQueryIndex, in boneLinks, leftLegBegin,      0, 0, elapsedTime, in bones, hipsIndex, leftLegBegin, rightLegBegin);
            SetBoneLerp(PostUpdateCommands, entityInQueryIndex, in boneLinks, (byte) (leftLegBegin + 1),  1, 0, elapsedTime, in bones, hipsIndex, leftLegBegin, rightLegBegin);
            SetBoneLerp(PostUpdateCommands, entityInQueryIndex, in boneLinks, (byte) (leftLegBegin + 2),  2, 0, elapsedTime, in bones, hipsIndex, leftLegBegin, rightLegBegin);
            SetBoneLerp(PostUpdateCommands, entityInQueryIndex, in boneLinks, rightLegBegin,     0, 1, elapsedTime, in bones, hipsIndex, leftLegBegin, rightLegBegin);
            SetBoneLerp(PostUpdateCommands, entityInQueryIndex, in boneLinks, (byte) (rightLegBegin + 1), 1, 1, elapsedTime, in bones, hipsIndex, leftLegBegin, rightLegBegin);
            SetBoneLerp(PostUpdateCommands, entityInQueryIndex, in boneLinks, (byte) (rightLegBegin + 2), 2, 1, elapsedTime, in bones, hipsIndex, leftLegBegin, rightLegBegin);
        }

        public static void SetBoneLerp(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, in BoneLinks boneLinks,
            byte index, byte legIndex, byte legSide, double elapsedTime, in NativeArray<Bone> bones, byte hipsIndex, byte leftLegBegin, byte rightLegBegin)
        {
            // i - leftLegBegin
            var boneEntity = boneLinks.bones[index];
            var bone = bones[index];
            var legSwingLength2 = legIndex * legSwingLength;
            var parentIndex = index - 1;
            if (index == leftLegBegin || index == rightLegBegin)
            {
                parentIndex = hipsIndex;
            }
            var parentBone = bones[parentIndex];
            var localPosition = bone.GetOriginalLocalPosition(in parentBone);
            // UnityEngine.Debug.LogError("SetBoneLerp - Local Bone " + index + " position: " + localPosition);
            var positionBegin = localPosition + new float3(0, 0, legSwingLength  / 4f + legSwingLength2);
            var positionEnd = localPosition - new float3(0, 0, legSwingLength  / 4f + legSwingLength2);
            var lerpPosition = new LerpPosition
            {
                createdTime = elapsedTime,
                lifeTime = swingSpeed,
                loop = AnimationLoopType.Loop
            };
            if (legSide == 0)
            {
                lerpPosition.positionBegin = positionBegin;
                lerpPosition.positionEnd = positionEnd;
            }
            else
            {
                lerpPosition.positionBegin = positionEnd;
                lerpPosition.positionEnd = positionBegin;
            }
            PostUpdateCommands.AddComponent(entityInQueryIndex, boneEntity, lerpPosition);
        }
    }
}