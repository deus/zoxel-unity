using Unity.Entities;
using Zoxel.Rendering;
using Zoxel.Voxels;

namespace Zoxel.Skeletons
{
    //! Uses OnSkeletonBuilt event to push the boneIndexes to the shader.
    [UpdateAfter(typeof(ModelRenderBoneSystem))]
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class ModelBoneIndexUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            #if DISABLE_COMPUTER_BUFFERS
            return;
            #endif
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithNone<EntityBusy, ChunkRenderBuilder>()
                .ForEach((Entity e, BoneIndexBuffer boneIndexBuffer, in SkeletonWeights skeletonWeights, in ZoxMesh zoxMesh) =>
            {
                // this data set is per vertex
                var onSkeletonBuilt = HasComponent<OnSkeletonBuilt>(e);
                var didResize = boneIndexBuffer.Resize(skeletonWeights.boneIndexes.Length);
                if (didResize || onSkeletonBuilt)  // or did boneLinks update?
                {
                    if (boneIndexBuffer.buffer == null)
                    {
                        return;
                    }
                    var material = zoxMesh.material;
                    if (material == null)
                    {
                        return;
                    }
                    var boneIndexes = boneIndexBuffer.buffer.BeginWrite<uint>(0, skeletonWeights.boneIndexes.Length);
                    for (int i = 0; i < skeletonWeights.boneIndexes.Length; i++)
                    {
                        boneIndexes[i] = skeletonWeights.boneIndexes[i];
                    }
                    boneIndexBuffer.buffer.EndWrite<uint>(skeletonWeights.boneIndexes.Length);
                    material.SetBuffer("boneIndexes", boneIndexBuffer.buffer);
                    if (didResize)
                    {
                        PostUpdateCommands.SetSharedComponentManaged(e, boneIndexBuffer);
                    }
                }
            }).WithoutBurst().Run();
        }
    }
}