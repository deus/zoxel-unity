using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Rendering;

namespace Zoxel.Skeletons
{
    //! Pushes bone matrices (currentBones and originalBones) to the GPU.
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class ModelRenderBoneSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private const string originalBonesName = "originalBones";
        private const string currentBonesName = "currentBones";
        private EntityQuery skeletonQuery;
        private EntityQuery bonesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            skeletonQuery = GetEntityQuery(
                ComponentType.ReadOnly<BoneLinks>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<EntityBusy>());
            bonesQuery = GetEntityQuery(ComponentType.ReadOnly<Bone>());
        }

        protected override void OnUpdate()
        {
            /*#if DISABLE_COMPUTER_BUFFERS
            return;
            #endif
            var disableAnimations = VoxelManager.instance.voxelSettings.disableAnimations;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var skeletonEntities = skeletonQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var skeletons = GetComponentLookup<BoneLinks>(true);
            var skeletonMatrices = GetComponentLookup<LocalToWorld>(true);
            // skeletonEntities.Dispose();
            var boneEntities = bonesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var bones = GetComponentLookup<Bone>(true);
            var boneTransforms = GetComponentLookup<LocalToWorld>(true);
            // boneEntities.Dispose();
            Entities
                .WithNone<InitializeEntity, DestroyEntity, EntityBusy>()
                .WithNone<ChunkRenderBuilder>()
                .ForEach((Entity e, OriginalBoneBuffer originalBoneBuffer, CurrentBoneBuffer currentBoneBuffer, in VoxLink voxLink, in ZoxMesh zoxMesh) =>
            {
                // if not using skeletonEntities, the filter doesn't work        
                var skeletonEntity = voxLink.vox;
                if (!skeletons.HasComponent(skeletonEntity) || zoxMesh.material == null || HasComponent<SkeletonDirty>(skeletonEntity)
                    || HasComponent<DeadEntity>(skeletonEntity) || !skeletonMatrices.HasComponent(skeletonEntity) || HasComponent<DestroyEntity>(skeletonEntity))
                {
                    return;
                }
                // Check if bones are all set
                var boneLinks = skeletons[skeletonEntity];
                for (int i = 0; i < boneLinks.bones.Length; i++)
                {
                    var boneEntity = boneLinks.bones[i];
                    if (boneEntity.Index == 0 || !HasComponent<Bone>(boneEntity) || !bones.HasComponent(boneEntity) || !boneTransforms.HasComponent(boneEntity)
                        || HasComponent<DestroyEntity>(boneEntity))
                    {
                        return;
                    }
                }
                var onSkeletonBuilt = HasComponent<OnSkeletonBuilt>(e);
                var material = zoxMesh.material;
                var skeletonMatrix = math.inverse(skeletonMatrices[skeletonEntity].Value);
                var bonesLength = boneLinks.bones.Length;
                var didResizeOriginals = originalBoneBuffer.Resize(bonesLength);  // make sure to resize it
                if (didResizeOriginals || onSkeletonBuilt)
                {
                    var originalBoneBuffer2 = originalBoneBuffer.buffer;
                    if (originalBoneBuffer2 != null)
                    {
                        var originalBones = originalBoneBuffer2.BeginWrite<float4x4>(0, bonesLength);
                        for (int i = 0; i < bonesLength; i++)
                        {
                            var boneEntity = boneLinks.bones[i];
                            originalBones[i] = bones[boneEntity].inverseSkinningMatrix;
                        }
                        originalBoneBuffer2.EndWrite<uint>(bonesLength);
                        material.SetBuffer(originalBonesName, originalBoneBuffer2);
                    }
                    if (didResizeOriginals)
                    {
                        PostUpdateCommands.SetSharedComponentManaged(e, originalBoneBuffer);
                    }
                }
                var didResize = currentBoneBuffer.Resize(bonesLength);  // make sure to resize it
                var currentBoneBuffer2 = currentBoneBuffer.buffer;
                if (currentBoneBuffer2 != null && (!disableAnimations || (disableAnimations && onSkeletonBuilt)))
                {
                    var currentBones = currentBoneBuffer2.BeginWrite<float4x4>(0, bonesLength);
                    for (int i = 0; i < bonesLength; i++)
                    {
                        // reverse bones from the boneLinks matrix
                        var boneEntity = boneLinks.bones[i];
                        currentBones[i] = math.mul(skeletonMatrix, boneTransforms[boneEntity].Value);
                    }
                    currentBoneBuffer2.EndWrite<float4x4>(bonesLength);
                    material.SetBuffer(currentBonesName, currentBoneBuffer2);
                }
                if (didResize)
                {
                    PostUpdateCommands.SetSharedComponentManaged(e, currentBoneBuffer);
                }
                if (skeletonEntities.Length > 0) { var e2 = skeletonEntities[0]; }
                if (boneEntities.Length > 0) { var e2 = boneEntities[0]; }
            })  .WithReadOnly(skeletons)
                .WithReadOnly(skeletonMatrices)
                .WithReadOnly(bones)
                .WithReadOnly(boneTransforms)
                .WithReadOnly(skeletonEntities)
                .WithDisposeOnCompletion(skeletonEntities)
                .WithReadOnly(boneEntities)
                .WithDisposeOnCompletion(boneEntities)
                .WithoutBurst().Run();*/
        }
    }
}