﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Voxels;
using Zoxel.Voxels.Authoring;
// prioritizing one weight over the other? when they interlap
// I can check the bone priority level - based on the weight index (bone index)
// and set them both based on that
// only need to check 3 sides anyway + diagonals
// Voxels should Only blend with their adjacent sides - not the diagonal voxels
// todo: Check if Voxel on Diagonal has a adjacent voxel that's also solid (so it connects to the first voxel)

namespace Zoxel.Skeletons
{
    //! A system that generates bone weights based on sizes of influence.
    [BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
	public partial class ChunkRenderWeightsSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            chunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkWeights>());
            RequireForUpdate<ModelSettings>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
		{
            var modelSettings = GetSingleton<ModelSettings>();
            var elapsedTime = World.Time.ElapsedTime;
            var isColourWeights = modelSettings.isColourWeights;
            var voxelSize = modelSettings.GetVoxelSize();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var chunks = GetComponentLookup<Chunk>(true);
            var chunkWeights = GetComponentLookup<ChunkWeights>(true);
            chunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .ForEach((ref SkeletonWeights skeletonWeights, ref ChunkRenderBuilder chunkRenderBuilder, ref ModelMesh modelMesh, in ChunkLink chunkLink, in ChunkSides chunkSides) =>
            {
				if (chunkRenderBuilder.chunkRenderBuildState != (byte) ChunkRenderBuildState.BakeWeights || !chunks.HasComponent(chunkLink.chunk))
                {
                    return;
                }
                var chunk = chunks[chunkLink.chunk];
                var voxelDimensions = chunk.voxelDimensions;
                if (chunk.voxels.Length != chunkSides.sides.Length)
                {
                    //UnityEngine.Debug.LogError("chunkSides not equal to voxelDimensions: " + chunkSides.sides.Length
                    //    + ":" + (voxelDimensions.x * voxelDimensions.y * voxelDimensions.z)
                    //    + ":ResizeChunkRender: " + HasComponent<ResizeChunkRender>(e));
                    return;
                }
                if (!isColourWeights)
                {
                    chunkRenderBuilder.chunkRenderBuildState = (byte) ChunkRenderBuildState.CompletedChunkRenderBuilder;
                }
                else
                {
                    chunkRenderBuilder.chunkRenderBuildState = (byte) ChunkRenderBuildState.BakeColorWeights;
                }
                skeletonWeights.Initialize(modelMesh.vertices.Length);
                var chunkWeights2 = chunkWeights[chunkLink.chunk];
                int3 position;
                byte voxelMetaIndex;
                var vertIndex = 0;
                var vertStartIndexes = new NativeArray<int>(chunk.voxels.Length, Allocator.Temp);
                var vertEndIndexes = new NativeArray<int>(chunk.voxels.Length, Allocator.Temp);
                var voxelIndex = 0;
                for (position.x = 0; position.x < voxelDimensions.x; position.x++)
                {
                    for (position.y = 0; position.y < voxelDimensions.y; position.y++)
                    {
                        for (position.z = 0; position.z < voxelDimensions.z; position.z++)
                        {
                            voxelMetaIndex = chunk.voxels[voxelIndex];
                            // if air, no need to build mesh
                            if (voxelMetaIndex == 0)
                            {
                                vertStartIndexes[voxelIndex] = -1;
                                vertEndIndexes[voxelIndex] = -1;
                                voxelIndex++;
                                continue;
                            }
                            var chunkWeight = chunkWeights2.weights[voxelIndex];
                            var vertIndexStart = -1;
                            var vertEndStart = -1;
                            for (byte sideIndex = 0; sideIndex < 6; sideIndex++)
                            {
                                // Add Voxel Side
                                if (   (sideIndex == VoxelSide.Up && chunkSides.GetSideIndex(voxelIndex, VoxelSide.Up) != 0)
                                    || (sideIndex == VoxelSide.Down && chunkSides.GetSideIndex(voxelIndex, VoxelSide.Down) != 0)
                                    || (sideIndex == VoxelSide.Left && chunkSides.GetSideIndex(voxelIndex, VoxelSide.Left) != 0)
                                    || (sideIndex == VoxelSide.Right && chunkSides.GetSideIndex(voxelIndex, VoxelSide.Right) != 0)
                                    || (sideIndex == VoxelSide.Backward && chunkSides.GetSideIndex(voxelIndex, VoxelSide.Backward) != 0)
                                    || (sideIndex == VoxelSide.Forward && chunkSides.GetSideIndex(voxelIndex, VoxelSide.Forward) != 0))
                                {
                                    if (vertIndexStart == -1)
                                    {
                                        vertIndexStart = vertIndex;
                                    }
                                    for (byte arrayIndex = 0; arrayIndex < 4; arrayIndex++)
                                    {
                                        /*if (vertIndex >= skeletonWeights.boneIndexes.Length)
                                        {
                                            // UnityEngine.Debug.LogError("Vert index out of range: " + vertIndex + ":" + skeletonWeights.boneIndexes.Length);
                                            return;
                                        }*/
                                        if (vertIndex >= skeletonWeights.boneIndexes.Length)
                                        {
                                            vertStartIndexes.Dispose();
                                            vertEndIndexes.Dispose();
                                            return;
                                        }
                                        skeletonWeights.boneIndexes[vertIndex] = chunkWeight;
                                        //vertIndex++;
                                        vertEndStart = vertIndex;
                                        vertIndex++;
                                    }
                                }
                            }
                            
                            vertStartIndexes[voxelIndex] = vertIndexStart;
                            vertEndIndexes[voxelIndex] = vertEndStart;
                            voxelIndex++;
                        }
                    }
                }
                // Now blend the verts that are in the same place
                int startIndexA;
                int endIndexA;
                voxelIndex = 0;
                for (position.x = 0; position.x < voxelDimensions.x; position.x++)
                {
                    for (position.y = 0; position.y < voxelDimensions.y; position.y++)
                    {
                        for (position.z = 0; position.z < voxelDimensions.z; position.z++)
                        {
                            startIndexA = vertStartIndexes[voxelIndex];
                            if (startIndexA == -1)
                            {
                                voxelIndex++;
                                continue;
                            }
                            endIndexA = vertEndIndexes[voxelIndex];
                            var positionDown = position.Down();
                            var positionUp = position.Up();
                            var positionLeft = position.Left();
                            var positionRight = position.Right();
                            var positionBackward = position.Backward();
                            var positionForward = position.Forward();

                            // Adjacent
                            DoWeights(in vertStartIndexes, in vertEndIndexes, in modelMesh, ref skeletonWeights,
                                positionUp, voxelDimensions, startIndexA, endIndexA);
                            DoWeights(in vertStartIndexes, in vertEndIndexes, in modelMesh, ref skeletonWeights,
                                positionDown, voxelDimensions, startIndexA, endIndexA);
                            DoWeights(in vertStartIndexes, in vertEndIndexes, in modelMesh, ref skeletonWeights,
                                positionLeft, voxelDimensions, startIndexA, endIndexA);
                            DoWeights(in vertStartIndexes, in vertEndIndexes, in modelMesh, ref skeletonWeights,
                                positionRight, voxelDimensions, startIndexA, endIndexA);
                            DoWeights(in vertStartIndexes, in vertEndIndexes, in modelMesh, ref skeletonWeights,
                                positionBackward, voxelDimensions, startIndexA, endIndexA);
                            DoWeights(in vertStartIndexes, in vertEndIndexes, in modelMesh, ref skeletonWeights,
                                positionForward, voxelDimensions, startIndexA, endIndexA);

                            // all up
                            DoWeights(in vertStartIndexes, in vertEndIndexes, in modelMesh, ref skeletonWeights,
                                positionUp.Backward(), voxelDimensions, startIndexA, endIndexA);
                            DoWeights(in vertStartIndexes, in vertEndIndexes, in modelMesh, ref skeletonWeights,
                                positionUp.Forward(), voxelDimensions, startIndexA, endIndexA);
                            DoWeights(in vertStartIndexes, in vertEndIndexes, in modelMesh, ref skeletonWeights,
                                positionUp.Left(), voxelDimensions, startIndexA, endIndexA);
                            DoWeights(in vertStartIndexes, in vertEndIndexes, in modelMesh, ref skeletonWeights,
                                positionUp.Right(), voxelDimensions, startIndexA, endIndexA);

                            // same level surroundings
                            DoWeights(in vertStartIndexes, in vertEndIndexes, in modelMesh, ref skeletonWeights,
                                positionLeft.Forward(), voxelDimensions, startIndexA, endIndexA);
                            DoWeights(in vertStartIndexes, in vertEndIndexes, in modelMesh, ref skeletonWeights,
                                positionLeft.Backward(), voxelDimensions, startIndexA, endIndexA);
                            DoWeights(in vertStartIndexes, in vertEndIndexes, in modelMesh, ref skeletonWeights,
                                positionRight.Forward(), voxelDimensions, startIndexA, endIndexA);
                            DoWeights(in vertStartIndexes, in vertEndIndexes, in modelMesh, ref skeletonWeights,
                                positionRight.Backward(), voxelDimensions, startIndexA, endIndexA);

                            voxelIndex++;
                        }
                    }
                }
                // Blend Weights
                /*for (int i = 0; i < skeletonWeights.boneIndexes.Length; i++)
                {
                    var vertexA = modelMesh.vertices[i].position;
                    var weightA = skeletonWeights.boneIndexes[i];
                    for (int j = 0; j < skeletonWeights.boneIndexes.Length; j++)
                    {
                        var vertexB = modelMesh.vertices[j].position;
                        if (vertexA.x == vertexB.x && vertexA.y == vertexB.y && vertexA.z == vertexB.z)
                        {
                            skeletonWeights.boneIndexes[j] = weightA;
                            // break;
                        }
                    }
                }*/
                vertStartIndexes.Dispose();
                vertEndIndexes.Dispose();
            })  .WithReadOnly(chunks)
                .WithReadOnly(chunkWeights)
                .ScheduleParallel(Dependency);
		}

        // Returns if blended or not
        private static bool DoWeights(in NativeArray<int> vertStartIndexes, in NativeArray<int> vertEndIndexes,
            in ModelMesh modelMesh, ref SkeletonWeights skeletonWeights,
            int3 otherPosition, int3 voxelDimensions, int startIndexA, int endIndexA)
        {
            if (VoxelUtilities.InBounds(otherPosition, voxelDimensions))
            {
                var otherVertArrayIndex = VoxelUtilities.GetVoxelArrayIndex(otherPosition, voxelDimensions);
                var startIndexB = vertStartIndexes[otherVertArrayIndex];
                if (startIndexB != -1)
                {
                    // check if blends with current verts
                    var endIndexB = vertEndIndexes[otherVertArrayIndex];
                    for (int i = startIndexA; i < endIndexA; i++)
                    {
                        var vertexA = modelMesh.vertices[i].position;
                        var weightA = skeletonWeights.boneIndexes[i];
                        for (int j = startIndexB; j < endIndexB; j++)
                        {
                            var vertexB = modelMesh.vertices[j].position;
                            if (vertexA.x == vertexB.x && vertexA.y == vertexB.y && vertexA.z == vertexB.z)
                            {
                                skeletonWeights.boneIndexes[j] = weightA;
                            }
                        }
                    }
                    return true;    // did blend
                }
            }
            return false;
        }
	}
} 

/*var influencesMin = new NativeArray<float3>(bonesLength, Allocator.Temp);
var influencesMax = new NativeArray<float3>(bonesLength, Allocator.Temp);
var bonePositions = new NativeArray<float3>(bonesLength, Allocator.Temp);
for (int i = 0; i < bonesLength; i++)
{
    var boneEntity = boneLinks.bones[i];
    if (bones.HasComponent(boneEntity))
    {
        var bone = bones[boneEntity];
        var skinningBonePosition = bone.GetSkinningBonePosition();
        var influenceSize = bone.GetInfluenceSize(voxelSize) * 1.08f;
        var influenceOffset = bone.GetInfluenceOffset(voxelSize);
        var influenceMin = skinningBonePosition + influenceOffset - influenceSize / 2f;
        var influenceMax = skinningBonePosition + influenceOffset + influenceSize / 2f;
        influencesMin[i] = influenceMin;
        influencesMax[i] = influenceMax;
        bonePositions[i] = influenceMax - influenceMin;
    }
}
skeletonWeights.Initialize(modelMesh.vertices.Length);
//skeletonWeights.disabled = 0;
for (int j = 0; j < skeletonWeights.boneIndexes.Length; j++)
{
    var vertPosition = modelMesh.vertices[j].position;
    uint closestIndex = 0;
    var smallestDistance = smallestDistancePossible;
    for (var i = 0; i < bonesLength; i++)
    {
        var influenceMin = influencesMin[i];
        var influenceMax = influencesMax[i];
        if (vertPosition.x >= influenceMin.x && vertPosition.x <= influenceMax.x
            && vertPosition.y >= influenceMin.y && vertPosition.y <= influenceMax.y
            && vertPosition.z >= influenceMin.z && vertPosition.z <= influenceMax.z)
        {
            var distance = math.distance(vertPosition, bonePositions[i]);
            if (distance <= smallestDistance)
            {
                smallestDistance = distance;
                closestIndex = (uint) i;
            }
        }
    }
    skeletonWeights.boneIndexes[j] = closestIndex;
}*/