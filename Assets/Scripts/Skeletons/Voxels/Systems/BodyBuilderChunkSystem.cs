using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Items;

namespace Zoxel.Skeletons
{
    //! Gathers items of equipment, and stacks them together into a nice Vox Body!
    [UpdateAfter(typeof(BodyBuilderSystem))] 
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class BodyBuilderChunkSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery itemQuery;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            itemQuery = GetEntityQuery(
                ComponentType.ReadOnly<ItemEquipable>(),
                ComponentType.ReadOnly<SlotLink>(),
                ComponentType.ReadOnly<ItemSlotData>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<GenerateVox>());
            chunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<SkeletonChunk>(),
                ComponentType.ReadWrite<Chunk>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var disableGear = VoxelManager.instance.voxelSettings.disableGear;
            var disableModels = VoxelManager.instance.voxelSettings.disableModels;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunks = GetComponentLookup<Chunk>(false);
            var chunkWeights = GetComponentLookup<ChunkWeights>(false);
            var itemEntities = itemQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxDatas = GetComponentLookup<Chunk>(true);
            var itemVoxColors = GetComponentLookup<VoxColors>(true);
            chunkEntities.Dispose();
            itemEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithNone<SpawnVoxChunk, OnChunksSpawned, SetModelSize>()
                .ForEach((Entity e, int entityInQueryIndex, ref SkeletonDirty skeletonDirty, ref VoxColors voxColors, in VoxBuildSteps voxBuildSteps, in Vox vox) =>
            {
                if (skeletonDirty.state == EquipmentDirtyState.ResizingModel)
                {
                    skeletonDirty.state = EquipmentDirtyState.PlaceVoxels;
                    return;
                }
                if (skeletonDirty.state != EquipmentDirtyState.PlaceVoxels || vox.chunks.Length == 0)
                {
                    return;
                }
                var chunkEntity = vox.chunks[0];
                if (HasComponent<SetModelSize>(chunkEntity) || !chunkWeights.HasComponent(chunkEntity))
                {
                    return;
                }
                var chunk = chunks[chunkEntity];
                var chunkWeights2 = chunkWeights[chunkEntity];
                if (chunk.voxels.Length == 0 || chunkWeights2.weights.Length == 0)
                {
                    return;
                }
                skeletonDirty.state = EquipmentDirtyState.SetSkeletonSize;
                var buildItems = new NativeArray<Chunk>(voxBuildSteps.steps.Length, Allocator.Temp);
                for (int i = 0; i < voxBuildSteps.steps.Length; i++)
                {
                    var voxEntity = voxBuildSteps.steps[i].voxEntity;
                    buildItems[i] = voxDatas[voxEntity];
                }
                voxColors.Dispose();
                // Set all to air
                // int voxelIndex = 0;
                var size = chunk.voxelDimensions;
                for (int i = 0; i < chunk.voxels.Length; i++)
                {
                    chunk.voxels[i] = 0;
                    chunkWeights2.weights[i] = 0;
                }
                // testing
                /*chunks[chunkEntity] = chunk;
                chunkWeights[chunkEntity] = chunkWeights2;
                PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, chunkEntity);
                voxColors.colors = new BlitableArray<float3>(64, Allocator.Persistent);
                for (var i = 0; i < voxColors.colors.Length; i++)
                {
                    voxColors.colors[i] = new float3(0, 1, 1);
                }
                return;*/

                // int voxelIndex = 0;
                for (var layerIndex = 0; layerIndex <= 1; layerIndex++)
                {
                    for (byte i = 0; i < voxBuildSteps.steps.Length; i++)
                    {
                        var voxBuildStep = voxBuildSteps.steps[i];
                        if (voxBuildStep.layer != layerIndex || (disableGear && layerIndex == SlotLayer.Gear))
                        {
                            continue;
                        }
                        if (voxBuildStep.boneType != HumanoidBoneType.Held)
                        {
                            //UnityEngine.Debug.LogError("Adding Vox Data onto Body: " + i + " with part size: " + voxBuildStep.size);
                            var itemVoxData = buildItems[i];
                            var itemColors = itemVoxColors[voxBuildStep.voxEntity];
                            AddData(ref chunk, ref chunkWeights2, ref voxColors, in itemVoxData, in itemColors, in voxBuildStep, i);
                        }
                    }
                }
                chunks[chunkEntity] = chunk;
                chunkWeights[chunkEntity] = chunkWeights2;
                PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, chunkEntity);
                buildItems.Dispose();
            })  .WithReadOnly(voxDatas)
                .WithReadOnly(itemVoxColors)
                .WithNativeDisableContainerSafetyRestriction(itemVoxColors)
                .WithNativeDisableContainerSafetyRestriction(chunks)
                .WithNativeDisableContainerSafetyRestriction(chunkWeights)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        // todo: test this again with clothes!
        public static void AddData(ref Chunk chunk, ref ChunkWeights chunkWeights, ref VoxColors voxColors, in Chunk itemVoxData, in VoxColors itemColors, in VoxBuildStep voxBuildStep, byte itemIndex)
        {
            if (itemVoxData.voxels.Length == 0)
            {
                // UnityEngine.Debug.LogError("Item Vox has no Data at " + itemIndex);
                return;
            }
            var voxelIndexes = new NativeList<byte>();      // get old indexes
            var newColors = new NativeList<float3>();
            var partPlacePosition = voxBuildStep.position;
            var operation = voxBuildStep.operation;
            // todo: add voxData modifiers back by rotatiing and flipping here
            var newPartData = itemVoxData.voxels.CloneTemp();
            var newPartSize = itemVoxData.voxelDimensions;
            if (voxBuildStep.modifier != 0)
            {
                // i guess this is for clothes?
                // UnityEngine.Debug.LogError("Modifying Vox Data Setting v2!");
                // UnityEngine.Debug.LogError("Rotating twice and flipping for step: " + itemIndex);
                int3 newSize;
                newPartData = VoxDataEditor.RotateVox(0, ref newPartData, newPartSize, out newSize);
                newPartSize = newSize;
                int3 newSize2;
                newPartData = VoxDataEditor.RotateVox(1, ref newPartData, newPartSize, out newSize2);
                newPartSize = newSize2;
                VoxDataEditor.FlipVox(2, ref newPartData, newPartSize);
            }
            // optimize new part
            //  get list of colors used in it
            // 1 - get list of indexes in new part
            byte voxelIndex;
            var voxelArrayIndex = 0;
            int3 localPosition;
            for (localPosition.x = 0; localPosition.x < newPartSize.x; localPosition.x++)
            {
                for (localPosition.y = 0; localPosition.y < newPartSize.y; localPosition.y++)
                {
                    for (localPosition.z = 0; localPosition.z < newPartSize.z; localPosition.z++)
                    {
                        voxelIndex = newPartData[voxelArrayIndex]; // VoxelUtilities.GetVoxelArrayIndex(localPosition, newPartSize)];
                        if (voxelIndex != 0 && !voxelIndexes.Contains(voxelIndex))
                        {
                            voxelIndexes.Add(voxelIndex);
                        }
                        voxelArrayIndex++;
                    }
                }
            }
            // get colors
            for (var i = 0; i < voxColors.colors.Length; i++)
            {
                newColors.Add(voxColors.colors[i]); // new ColorRGB(voxColors.colors[i]));
            }
            // create a list for replace indexes
            // need to replace voxels all at once
            // todo: replace this using NativeHashmap - for speed looking up replacements
            var newVoxelIndexes = new NativeArray<byte>(voxelIndexes.Length, Allocator.Temp);      // get old indexes
            for (var i = 0; i < voxelIndexes.Length; i++)
            {
                // add color to new colors
                voxelIndex = (byte) (voxelIndexes[i] - 1);    // remove air
                if (voxelIndex >= itemColors.colors.Length)
                {
                    // UnityEngine.Debug.LogError("Added vox color out of bounds: " + voxelIndex + " out of " + itemColors.colors.Length);
                    continue;
                }
                byte newIndex;
                var color = itemColors.colors[voxelIndex];
                if (!newColors.Contains(color))
                {
                    // var newIndex = newColors.Length + 1;
                    newColors.Add(color);
                    newIndex = (byte) (newColors.Length);
                }
                else
                {
                    // newVoxelIndexes.Add(((byte) (j + 1)));
                    newIndex = (byte) (newColors.IndexOf(color) + 1);
                }
                newVoxelIndexes[i] = newIndex;
            }
            // replace any colours, with the new indicies
            voxelArrayIndex = 0;
            for (localPosition.x = 0; localPosition.x < newPartSize.x; localPosition.x++)
            {
                for (localPosition.y = 0; localPosition.y < newPartSize.y; localPosition.y++)
                {
                    for (localPosition.z = 0; localPosition.z < newPartSize.z; localPosition.z++)
                    {
                        // int newIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, newPartSize);
                        voxelIndex = newPartData[voxelArrayIndex];
                        // Find any 'Replace' Indexes, and replace them
                        for (var i = 0; i < voxelIndexes.Length; i++)
                        {
                            if (voxelIndex == voxelIndexes[i])
                            {
                                newPartData[voxelArrayIndex] = newVoxelIndexes[i];
                                break;
                            }
                        }
                        voxelArrayIndex++;
                    }
                }
            }
            // add new colors to this
            voxColors.Dispose();
            //var colors2 = newColors.ToArray(Allocator.Persistent);
            voxColors.colors = new BlitableArray<float3>(newColors.Length, Allocator.Persistent);
            for (var i = 0; i < voxColors.colors.Length; i++)
            {
                voxColors.colors[i] = newColors[i];
                // UnityEngine.Debug.LogError("Color: " + i + " is " + newColors[i]);
                // UnityEngine.Debug.LogError("Color: " + newColors[i]);
            }
            // UnityEngine.Debug.LogError("Total Colors on part: " + newColors.Length);

            // now add in new part
            // var count = 0;
            for (localPosition.x = 0; localPosition.x < newPartSize.x; localPosition.x++)
            {
                for (localPosition.y = 0; localPosition.y < newPartSize.y; localPosition.y++)
                {
                    for (localPosition.z = 0; localPosition.z < newPartSize.z; localPosition.z++)
                    {
                        int partIndex;
                        if (operation == VoxOperation.FlipX)
                        {
                            var flippedLocalPosition = new int3(newPartSize.x - 1 - localPosition.x, localPosition.y, localPosition.z);
                            partIndex = VoxelUtilities.GetVoxelArrayIndex(flippedLocalPosition, newPartSize);
                        }
                        else
                        {
                            partIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, newPartSize);
                        }
                        if (newPartData[partIndex] != 0) 
                        {
                            var newPosition = localPosition + partPlacePosition;
                            if (VoxelUtilities.InBounds(newPosition, chunk.voxelDimensions))
                            {
                                var newIndex = VoxelUtilities.GetVoxelArrayIndex(newPosition, chunk.voxelDimensions);
                                /*if (partIndex >= newPartData.Length || newIndex >= chunk.voxels.Length)
                                {
                                    UnityEngine.Debug.LogError("Index out of bounds.");
                                    return;
                                    // continue;
                                }*/
                                chunk.voxels[newIndex] = newPartData[partIndex];
                                chunkWeights.weights[newIndex] = itemIndex;
                                // count++;
                                // UnityEngine.Debug.LogError("Updated Voxel: " + chunk.voxels[newIndex]);
                            }
                            //! \todo Figure out why placing a voxel part goes out of bounds
                            /*else
                            {
                                // UnityEngine.Debug.LogError("Position out of bounds: " + newPosition);
                                UnityEngine.Debug.LogError("Position out of bounds.");
                            }*/
                            // UnityEngine.Debug.LogError("Added Index to voxData: " + newPartData[partIndex]);
                        }
                    }
                }
            }
            voxelIndexes.Dispose();
            newColors.Dispose();
            newVoxelIndexes.Dispose();
            newPartData.Dispose();
        }
    }
}
                /*for (var i = 0; i < voxColors.colors.Length; i++)
                {
                    UnityEngine.Debug.LogError("Color: " + voxColors.colors[i]);
                }*/

                /*var solidFound = false;
                for (int i = 0; i < chunk.voxels.Length; i++)
                {
                    if (chunk.voxels[i] != 0)
                    {
                        solidFound = true;
                        break;
                    }
                }
                if (!solidFound)
                {
                    UnityEngine.Debug.LogError("2 No Solids found on model: " + e.Index);
                }
                else
                {
                    UnityEngine.Debug.LogError("2 Solids found on model: " + e.Index);
                }*/