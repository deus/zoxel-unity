using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Voxels;

namespace Zoxel.Skeletons
{
    //! Sets the body size after generated.
    [UpdateAfter(typeof(BodyBuilderChunkSystem))]
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class BodyBuilderSizeSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .ForEach((ref SkeletonDirty skeletonDirty, ref Body body, in ChunkDimensions chunkDimensions, in VoxScale voxScale) =>
            {
                if (skeletonDirty.state != EquipmentDirtyState.SetSkeletonSize)
                {
                    return;
                }
                skeletonDirty.state = EquipmentDirtyState.SpawnBones;
                body.size = 0.5f * voxScale.scale * chunkDimensions.voxelDimensions.ToFloat3();
			}).ScheduleParallel();
        }
    }
}