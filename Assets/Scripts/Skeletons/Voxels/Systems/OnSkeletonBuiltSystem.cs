using Unity.Entities;
using Unity.Burst;
using Zoxel.Voxels;

namespace Zoxel.Skeletons
{
    //! The event for after BoneLinks is built. Updates ChunkRender data.
    /**
    *   Informs the chunk renders that the boneLinks has just built.
    *   Removes all OnSkeletonBuilt events from any entities.
    */
    [UpdateAfter(typeof(BodyBuilderSystem))] 
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class OnSkeletonBuiltSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // Sends OnSkeletonBuilt commands to ChunkRenders
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity, SkeletonDirty>()
                .WithAll<OnSkeletonBuilt, Vox>()
                .ForEach((Entity e, int entityInQueryIndex, in Vox vox) =>
            {
                for (int i = 0; i < vox.chunks.Length; i++)
                {
                    var chunk = vox.chunks[i];
                    if (chunk.Index > 0 && HasComponent<Chunk>(chunk))
                    {
                        PostUpdateCommands.AddComponent<OnSkeletonBuilt>(entityInQueryIndex, chunk);
                    }
                }
                PostUpdateCommands.RemoveComponent<OnSkeletonBuilt>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithAll<OnSkeletonBuilt, Chunk>()
                .ForEach((Entity e, int entityInQueryIndex, in ChunkRenderLinks chunkRenderLinks) =>
            {
                for (int i = 0; i < chunkRenderLinks.chunkRenders.Length; i++)
                {
                    PostUpdateCommands.AddComponent<OnSkeletonBuilt>(entityInQueryIndex, chunkRenderLinks.chunkRenders[i]);
                }
                PostUpdateCommands.RemoveComponent<OnSkeletonBuilt>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // Remove any OnSkeletonBuilt commands
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithNone<EntityBusy, ChunkRenderBuilder>()
                .WithAll<OnSkeletonBuilt, ChunkRender>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<OnSkeletonBuilt>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}