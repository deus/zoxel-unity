using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;    // float3, matrix
using Unity.Collections;    // Lists, Allocator
using Zoxel.Voxels;         // VoxScale
using Zoxel.Transforms;     // ParentLink

namespace Zoxel.Skeletons
{
    //! Spawns the Bones after Body Vox is created from Equipment items.
    /**
    *   - Spawn System -
    *   Note: Camera system has to wait one frame for bone data to be set properly.
    */
    [UpdateAfter(typeof(BodyBuilderSizeSystem))]
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class BoneSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        public Entity bonePrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var boneArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(NewBone),
                typeof(Bone),
                typeof(BoneType),
                typeof(SkeletonLink),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(LocalScale),
                // unity
                // typeof(ZoxMatrix),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale)
            );
            bonePrefab = EntityManager.CreateEntity(boneArchetype);
            EntityManager.SetComponentData(bonePrefab, new NonUniformScale { Value = new float3(1, 1, 1) });
            EntityManager.SetComponentData(bonePrefab, new Rotation { Value = quaternion.identity });
            EntityManager.SetComponentData(bonePrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(bonePrefab, LocalScale.One);
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var degreesToRadians = ((math.PI * 2) / 360f);
            var totalBoneRotation = quaternion.EulerXYZ(new float3(0, 180, 0) * degreesToRadians); // UnityEngine.Quaternion.Euler(0, 180, 0);
            var disableModels = VoxelManager.instance.voxelSettings.disableModels;
            var bonePrefab = this.bonePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref SkeletonDirty skeletonDirty, in BoneLinks boneLinks,
                    in VoxBuildSteps voxBuildSteps, in VoxScale voxScale, in Body body) =>
            {
                if (skeletonDirty.state != EquipmentDirtyState.SpawnBones)
                {
                    return;
                }
                skeletonDirty.state = EquipmentDirtyState.SetBones;
                var boneList = new NativeList<Entity>();
                var boneDatas = new NativeList<Bone>();
                var boneTypes = new NativeList<byte>();
                var totalBoneOffset = -body.size;
                // Reuse some bones but reposition them
                for (int i = voxBuildSteps.steps.Length; i < boneLinks.bones.Length; i++)
                {
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, boneLinks.bones[i]);
                }
                for (byte i = 0; i < voxBuildSteps.steps.Length; i++) 
                {
                    var voxBuildStep = voxBuildSteps.steps[i];
                    var isOldBoneEntity = i < boneLinks.bones.Length;
                    Entity boneEntity;
                    if (isOldBoneEntity)
                    {
                        boneEntity = boneLinks.bones[i];
                        PostUpdateCommands.AddComponent<NewBone>(entityInQueryIndex, boneEntity);
                    }
                    else
                    {
                        boneEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, bonePrefab);
                    }
                    boneList.Add(boneEntity);
                    boneDatas.Add(CreateBoneData(in voxBuildStep, e, totalBoneOffset, totalBoneRotation, voxScale.scale, i));
                    boneTypes.Add(voxBuildStep.boneType);
                }
                for (int i = 0; i < voxBuildSteps.steps.Length; i++)
                {
                    var voxBuildStep = voxBuildSteps.steps[i];
                    var boneEntity = boneList[i];
                    var bone = boneDatas[i];
                    var isOldBoneEntity = i < boneLinks.bones.Length;
                    Entity boneParent;
                    float3 localPosition;
                    Bone parentBoneData;
                    if (voxBuildStep.parent >= 0)
                    {
                        boneParent = boneList[voxBuildStep.parent];
                        parentBoneData = boneDatas[voxBuildStep.parent];
                        localPosition = bone.GetOriginalLocalPosition(in parentBoneData);
                        // UnityEngine.Debug.LogError("Local Bone " + i + " position: " + localPosition);
                    }
                    else
                    {
                        boneParent = e;
                        localPosition = bone.GetOriginalPosition();
                    }
                    PostUpdateCommands.SetComponent(entityInQueryIndex, boneEntity, bone);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, boneEntity, new BoneType(boneTypes[i]));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, boneEntity, new SkeletonLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, boneEntity, new ParentLink(boneParent));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, boneEntity, new LocalPosition(localPosition));
                    // Remove old components
                    if (isOldBoneEntity)
                    {
                        if (voxBuildStep.boneType != HumanoidBoneType.Shoulder && HasComponent<ShoulderBone>(boneEntity))
                        {
                            PostUpdateCommands.RemoveComponent<ShoulderBone>(entityInQueryIndex, boneEntity);
                        }
                        else if (voxBuildStep.boneType != HumanoidBoneType.Head && HasComponent<HeadBone>(boneEntity))
                        {
                            PostUpdateCommands.RemoveComponent<HeadBone>(entityInQueryIndex, boneEntity);
                            PostUpdateCommands.RemoveComponent<CameraBob>(entityInQueryIndex, boneEntity);
                            PostUpdateCommands.RemoveComponent<BoneUniqueRotation>(entityInQueryIndex, boneEntity);
                        }
                        else if (voxBuildStep.boneType != HumanoidBoneType.Chest && HasComponent<ChestBone>(boneEntity))
                        {
                            PostUpdateCommands.RemoveComponent<ChestBone>(entityInQueryIndex, boneEntity);
                            PostUpdateCommands.RemoveComponent<BoneUniqueScale>(entityInQueryIndex, boneEntity);
                        }
                        else if (voxBuildStep.boneType != HumanoidBoneType.Hips && HasComponent<HipsBone>(boneEntity))
                        {
                            PostUpdateCommands.RemoveComponent<HipsBone>(entityInQueryIndex, boneEntity);
                        }
                        else if (voxBuildStep.boneType != HumanoidBoneType.Thigh && HasComponent<ThighBone>(boneEntity))
                        {
                            PostUpdateCommands.RemoveComponent<ThighBone>(entityInQueryIndex, boneEntity);
                        }
                        else if (voxBuildStep.boneType != HumanoidBoneType.Bicep && HasComponent<BicepBone>(boneEntity))
                        {
                            PostUpdateCommands.RemoveComponent<BicepBone>(entityInQueryIndex, boneEntity);
                        }
                        else if (voxBuildStep.boneType != HumanoidBoneType.Forearm && HasComponent<ForearmBone>(boneEntity))
                        {
                            PostUpdateCommands.RemoveComponent<ForearmBone>(entityInQueryIndex, boneEntity);
                        }
                        else if (voxBuildStep.boneType != HumanoidBoneType.Hand && HasComponent<HandBone>(boneEntity))
                        {
                            PostUpdateCommands.RemoveComponent<HandBone>(entityInQueryIndex, boneEntity);
                        }
                        else if (voxBuildStep.boneType != HumanoidBoneType.Held && HasComponent<HeldBone>(boneEntity))
                        {
                            PostUpdateCommands.RemoveComponent<HeldBone>(entityInQueryIndex, boneEntity);
                        }
                    }
                    // Add new bone components
                    AddBoneTypeComponent(PostUpdateCommands, entityInQueryIndex, boneEntity, voxBuildStep.boneType);
                    //Debug.LogError("Step Position [" + i + "]: " + voxBuildStep.bonePosition.x + " : " + voxBuildStep.bonePosition.y + " : " + voxBuildStep.bonePosition.z);
                    //Debug.LogError("Bone Position [" + i + "]: " + localPosition.x + " : " + localPosition.y + " : " + localPosition.z);
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnBonesSpawned((byte) boneList.Length));
                boneList.Dispose();
                boneDatas.Dispose();
                boneTypes.Dispose();
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static void AddBoneTypeComponent(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity boneEntity, byte boneType)
        {
            if (boneType == HumanoidBoneType.Shoulder)
            {
                PostUpdateCommands.AddComponent<ShoulderBone>(entityInQueryIndex, boneEntity);
            }
            else if (boneType == HumanoidBoneType.Head)
            {
                PostUpdateCommands.AddComponent<HeadBone>(entityInQueryIndex, boneEntity);
                PostUpdateCommands.AddComponent<BoneUniqueRotation>(entityInQueryIndex, boneEntity);
                PostUpdateCommands.AddComponent<CameraBob>(entityInQueryIndex, boneEntity);
                PostUpdateCommands.AddComponent<InitializeEntity>(entityInQueryIndex, boneEntity);
            }
            else if (boneType == HumanoidBoneType.Chest)
            {
                PostUpdateCommands.AddComponent<ChestBone>(entityInQueryIndex, boneEntity);
                PostUpdateCommands.AddComponent<BoneUniqueScale>(entityInQueryIndex, boneEntity);
            }
            else if (boneType == HumanoidBoneType.Hips)
            {
                PostUpdateCommands.AddComponent<HipsBone>(entityInQueryIndex, boneEntity);
            }
            else if (boneType == HumanoidBoneType.Thigh)
            {
                PostUpdateCommands.AddComponent<ThighBone>(entityInQueryIndex, boneEntity);
            }
            else if (boneType == HumanoidBoneType.Bicep)
            {
                PostUpdateCommands.AddComponent<BicepBone>(entityInQueryIndex, boneEntity);
            }
            else if (boneType == HumanoidBoneType.Forearm)
            {
                PostUpdateCommands.AddComponent<ForearmBone>(entityInQueryIndex, boneEntity);
            }
            else if (boneType == HumanoidBoneType.Hand)
            {
                PostUpdateCommands.AddComponent<HandBone>(entityInQueryIndex, boneEntity);
            }
            else if (boneType == HumanoidBoneType.Held)
            {
                PostUpdateCommands.AddComponent<HeldBone>(entityInQueryIndex, boneEntity);
            }
        }

        private static Bone CreateBoneData(in VoxBuildStep voxBuildStep, Entity skeletonEntity, float3 totalBoneOffset, quaternion totalBoneRotation, float3 voxelScale, byte index)
        {
            var size = voxBuildStep.size;
            var axes = voxBuildStep.axes;
            // Calculate Bone Position
            var bonePosition = voxBuildStep.bonePosition;
            // position relative to boneLinks and not to parent bone
            var position = bonePosition.ToFloat3() * voxelScale;
            position += totalBoneOffset;
            position = math.rotate(totalBoneRotation, position);
            var skinningMatrix = new UnityEngine.Matrix4x4();
            skinningMatrix.SetTRS(position, quaternion.identity, new float3(1,1,1));
            // Calculate bone influence size
            var halfSize = new int3((size.x / 2), (size.y / 2), (size.z / 2));
            var bodyMidpoint = voxBuildStep.position;
            if (voxBuildStep.layer == 0)
            {
                bodyMidpoint += halfSize;
            }
            var boneOffset = bodyMidpoint - bonePosition;
            if (axes == EquipmentSlotAnchor.Left || axes == EquipmentSlotAnchor.Right)
            {
                boneOffset = new int3(-boneOffset.x, -boneOffset.y, -boneOffset.z);
            }
            halfSize.x += 1;
            halfSize.y += 1;
            halfSize.z += 1;
            var influenceMin = (boneOffset - halfSize);
            var influenceMax = (boneOffset + halfSize);
            // why does this need to be adjusted?
            if (voxBuildStep.layer == 1)
            {
                influenceMin += halfSize;
                influenceMax += halfSize;
            }
            // Set Bone
            var boneMatrix = new UnityEngine.Matrix4x4();
            boneMatrix.SetTRS(position, quaternion.identity, new float3(1,1,1));
            return new Bone
            {
                index = index,
                originalMatrix = boneMatrix,
                skinningMatrix = skinningMatrix,
                inverseSkinningMatrix = math.inverse(skinningMatrix),
                influenceMin = influenceMin,
                influenceMax = influenceMax,
                bonePosition = voxBuildStep.bonePosition,
                size = size
            };
        }
    }
}