using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Items;
// Equip system will add updates / resizes to the planet if body is dirty
// note: camera system has to wait one frame for bone data to be set properly
// todo:    -> Make this spawn a chunk first and update chunks rather then Chunk Component
// todo: Optimize the updating body parts process rather then recreating the entire thing
//      - Solution: Check the zone of the Chunk updated, and readd all those models (limited only to the zone of updates)
//      Cache some of the data used to reequip - like the bodys vox chunks
//      Rather then recreating the entire body whenequiping / unequiping - maybe store it in layers? idk yet
//  Can I reorder the list based on tree level?
//      Body Chest -> Head / Arms / Hips -> Hat / Biceps / Thighs -> forearm / Calfs -> Hands / Feet -> Held / Shoes etc

namespace Zoxel.Skeletons
{
    //! Gathers items of equipment, and stacks them together into a nice Vox Body!
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class BodyBuilderSystem : SystemBase
    {
        private const byte maxBuildSteps = 64;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery itemQuery;
        private EntityQuery slotsQuery;
        private EntityQuery userEquipmentQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            itemQuery = GetEntityQuery(
                ComponentType.ReadOnly<ItemEquipable>(),
                ComponentType.ReadOnly<SlotLink>(),
                ComponentType.ReadOnly<ItemSlotData>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<GenerateVox>());
            slotsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Slot>(),
                ComponentType.Exclude<DestroyEntity>());
            userEquipmentQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserEquipment>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var itemEntities = itemQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var slotLinks = GetComponentLookup<SlotLink>(true);
            var itemSlotDatas = GetComponentLookup<ItemSlotData>(true);
            var itemVoxDatas = GetComponentLookup<Chunk>(true);
            var itemBoneTypes = GetComponentLookup<BoneType>(true);
            var generateVoxs = GetComponentLookup<GenerateVox>(true);
            itemEntities.Dispose();
            var slotEntities = slotsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var slots = GetComponentLookup<Slot>(true);
            slotEntities.Dispose();
            var userEquipmentEntities = userEquipmentQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var userEquipmentMetaDatas = GetComponentLookup<MetaData>(true);
            var equipmentItems = GetComponentLookup<EquipmentParent>(true);
            userEquipmentEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity, OnUserEquipmentSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref SkeletonDirty skeletonDirty, in Equipment equipment, in Vox vox, in ItemLinks itemLinks, in RealmLink realmLink) =>
            {
                if (skeletonDirty.state != EquipmentDirtyState.BuildSkeleton)
                {
                    return;
                }
                for (byte i = 0; i < itemLinks.items.Length; i++)
                {
                    var itemEntity = itemLinks.items[i];
                    if (!slotLinks.HasComponent(itemEntity) || generateVoxs.HasComponent(itemEntity))   // itemEntity2.Index <= 0 ||  <Item>
                    {
                        // UnityEngine.Debug.LogError("itemLinks itemEntity [" + i + "] GenerateVox.");
                        return;
                    }
                }
                for (byte i = 0; i < equipment.body.Length; i++)
                {
                    var userEquipmentEntity = equipment.body[i];
                    if (!userEquipmentMetaDatas.HasComponent(userEquipmentEntity))
                    {
                        // UnityEngine.Debug.LogError("userEquipmentEntity [" + i + "] broken.");
                        // UnityEngine.Debug.LogError("Issue with userEquipmentEntity at: " + i + "::" + userEquipmentEntity.Index);
                        return;
                    }
                    var itemEntity = userEquipmentMetaDatas[userEquipmentEntity].data;
                    if (!slotLinks.HasComponent(itemEntity))  // <Item>
                    {
                        // UnityEngine.Debug.LogError("equipment.body itemEntity [" + i + "] itemEntity.Index: " + itemEntity.Index);
                        return;
                    }
                    if (generateVoxs.HasComponent(itemEntity))  // <Item>
                    {
                        // UnityEngine.Debug.LogError("equipment.body itemEntity [" + i + "] GenerateVox.");
                        return;
                    }
                }
                var equipmentItemEntities = new NativeList<Entity>();
                var itemChunks = new NativeList<Chunk>();
                var equipmentBoneTypes = new NativeList<BoneType>();
                // First Find Core Item
                var coreItemEntity = new Entity();
                byte coreIndex = 0;
                for (byte i = 0; i < equipment.body.Length; i++)
                {
                    var userEquipmentEntity = equipment.body[i];
                    var itemEntity = userEquipmentMetaDatas[userEquipmentEntity].data;
                    equipmentItemEntities.Add(itemEntity);
                    itemChunks.Add(itemVoxDatas[itemEntity]);
                    equipmentBoneTypes.Add(itemBoneTypes[itemEntity]);
                    var coreSlotEntity = slotLinks[itemEntity].slot;
                    var coreSlot = slots[coreSlotEntity];
                    if (coreSlot.core == 1)
                    {
                        coreItemEntity = itemEntity;
                        coreIndex = i;
                    }
                }
                // Build tree from core item first
                if (coreItemEntity.Index == 0)
                {
                    // UnityEngine.Debug.LogError("Core not found.");
                    equipmentItemEntities.Dispose();
                    itemChunks.Dispose();
                    equipmentBoneTypes.Dispose();
                    PostUpdateCommands.RemoveComponent<SkeletonDirty>(entityInQueryIndex, e);
                    PostUpdateCommands.RemoveComponent<EntityBusy>(entityInQueryIndex, e);
                    return;
                }
                skeletonDirty.state = EquipmentDirtyState.ResizingModel;
                var steps = new NativeList<VoxBuildStep>();
                var attachItemSlotEntity = slotLinks[coreItemEntity].slot;
                var maleSlot = slots[attachItemSlotEntity];
                var itemSlotData = itemSlotDatas[coreItemEntity];
                var coreModel = itemVoxDatas[coreItemEntity];
                var coreBoneType = itemBoneTypes[coreItemEntity];
                var voxBuildStep = new VoxBuildStep();
                voxBuildStep.voxEntity = coreItemEntity;
                voxBuildStep.axes = EquipmentSlotAnchor.Center;
                voxBuildStep.parent = -1;
                voxBuildStep.bonePosition = coreModel.voxelDimensions / 2;
                voxBuildStep.boneType = coreBoneType.boneType;
                steps.Add(voxBuildStep);
                AttachAllPartsToParentItem(in equipment, ref steps, VoxOperation.None, in maleSlot, in itemSlotData, coreModel, equipment.body[coreIndex], int3.zero, 0,
                    voxBuildStep.bonePosition, in equipmentItemEntities, in itemChunks, in equipmentBoneTypes, in slotLinks, in slots, in itemSlotDatas,
                    in userEquipmentMetaDatas, in equipmentItems);
                var voxBuildSteps = new VoxBuildSteps();
                voxBuildSteps.steps = new BlitableArray<VoxBuildStep>(steps.Length, Allocator.Persistent);
                for (int i = 0; i < steps.Length; i++)
                {
                    voxBuildSteps.steps[i] = steps[i];
                }
                voxBuildSteps.size = ProcessBodyBuilderData(ref voxBuildSteps);
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, voxBuildSteps);
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetModelSize(voxBuildSteps.size));
                // UnityEngine.Debug.LogError("BodyBuilder new voxBuildSteps size: " + voxBuildSteps.size);
                equipmentItemEntities.Dispose();
                itemChunks.Dispose();
                equipmentBoneTypes.Dispose();
                steps.Dispose();
            })  .WithReadOnly(slotLinks)
                .WithReadOnly(itemSlotDatas)
                .WithReadOnly(itemVoxDatas)
                .WithReadOnly(itemBoneTypes)
                .WithReadOnly(generateVoxs)
                .WithReadOnly(slots)
                .WithReadOnly(userEquipmentMetaDatas)
                .WithReadOnly(equipmentItems)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        // this finds a slot where I can attach the part
        // for all female slots, find an empty one if it is the slot of our male one
        private static void AttachAllPartsToParentItem(in Equipment equipment, ref NativeList<VoxBuildStep> steps, byte lastOperation,
            in Slot parentBodyPart, in ItemSlotData itemSlotData, in Chunk parentVoxData,
            Entity userEquipmentParentEntity, int3 position, int parentIndex, int3 parentBonePosition,
            in NativeList<Entity> equipmentItemEntities, in NativeList<Chunk> itemChunks, in NativeList<BoneType> equipmentBoneTypes,
            in ComponentLookup<SlotLink> slotLinks, in ComponentLookup<Slot> slots,
            in ComponentLookup<ItemSlotData> itemSlotDatas,
            in ComponentLookup<MetaData> userEquipmentMetaDatas, in ComponentLookup<EquipmentParent> equipmentItems)
        {
            if (steps.Length >= maxBuildSteps)
            {
                return;
            }
            for (byte slotIndex = 0; slotIndex < parentBodyPart.femaleSlots.Length; slotIndex++)
            {
                var femaleSlotEntity = parentBodyPart.femaleSlots[slotIndex];
                byte femaleModifier = 0;
                if (slotIndex < parentBodyPart.femaleModifiers.Length)
                {
                    femaleModifier = parentBodyPart.femaleModifiers[slotIndex];
                }
                var femaleOffset = int3.zero;
                if (slotIndex < itemSlotData.femaleOffsets.Length)
                {
                    femaleOffset = itemSlotData.femaleOffsets[slotIndex];
                }
                // find child index - bodyIndex is childIndex
                for (int bodyIndex2 = 0; bodyIndex2 < equipment.body.Length; bodyIndex2++)
                {
                    var userEquipmentEntity = equipment.body[bodyIndex2];
                    if (userEquipmentParentEntity != userEquipmentEntity)
                    {
                        var attachedBodyPart = equipmentItems[userEquipmentEntity];
                        if (attachedBodyPart.parent == userEquipmentParentEntity && attachedBodyPart.parentSlotIndex == slotIndex)
                        {
                            var attachItemEntity = userEquipmentMetaDatas[userEquipmentEntity].data;
                            var maleSlot = slots[femaleSlotEntity];
                            var itemSlotData2 = itemSlotDatas[attachItemEntity];
                            var attachVoxData = itemChunks[bodyIndex2];
                            var attachBoneType = equipmentBoneTypes[bodyIndex2];
                            AttachPart(userEquipmentEntity, in equipment, ref steps, in parentVoxData, in maleSlot, in itemSlotData2, in attachVoxData, attachBoneType,
                                lastOperation, femaleModifier, position, femaleOffset, parentIndex, equipmentItemEntities[bodyIndex2], in equipmentItemEntities, in itemChunks,
                                in equipmentBoneTypes, in slotLinks, in slots, in itemSlotDatas, in userEquipmentMetaDatas, in equipmentItems);
                            break;
                        }
                    }
                }
            }
        }

        //! Modifiers can rotate or flip body parts when attaching to the body.
        private static void AttachPart(Entity userEquipmentParentEntity, in Equipment equipment, ref NativeList<VoxBuildStep> steps, in Chunk parentModel, in Slot maleSlot,
            in ItemSlotData itemSlotData, in Chunk attachedModel, BoneType attachBoneType, byte lastOperation, byte femaleModifier, int3 position, int3 femaleOffset, int parentIndex,
            Entity attachModelEntity, in NativeList<Entity> equipmentItemEntities, in NativeList<Chunk> itemChunks, in NativeList<BoneType> equipmentBoneTypes,
            in ComponentLookup<SlotLink> slotLinks, in ComponentLookup<Slot> slots,
            in ComponentLookup<ItemSlotData> itemSlotDatas, in ComponentLookup<MetaData> userEquipmentMetaDatas, in ComponentLookup<EquipmentParent> equipmentItems)
        {
            var size = attachedModel.voxelDimensions;
            var modifier = maleSlot.modifier;
            if (modifier == VoxModifier.RotateXY)
            {
                // UnityEngine.Debug.LogError("Modifying Vox Data Setting!");
                // UnityEngine.Debug.LogError("VoxModifier.RotateXY: " + position);
                // rotate the sizes twice
                size = new int3(size.y, size.x, size.z); // X Y
                size = new int3(size.y, size.x, size.z); // X Y
            }
            var offset = itemSlotData.offset;
            var operation = lastOperation;
            if (femaleModifier != VoxOperation.None)
            {
                operation = femaleModifier;
            }
            var newPosition = CalculateNewBodyPartPosition(position, size, offset, parentModel.voxelDimensions, maleSlot.anchor, lastOperation, operation, femaleOffset);
            var bonePosition = CalculateBonePosition(newPosition, size, operation, maleSlot.layer, maleSlot.anchor);
            var voxBuildStep = new VoxBuildStep();
            voxBuildStep.layer = maleSlot.layer;
            voxBuildStep.voxEntity = attachModelEntity;
            voxBuildStep.size = size;
            voxBuildStep.modifier = modifier;
            voxBuildStep.position = newPosition;
            voxBuildStep.operation = operation;
            voxBuildStep.axes = maleSlot.anchor;
            voxBuildStep.parent = parentIndex;
            voxBuildStep.bonePosition = bonePosition;
            voxBuildStep.boneType = attachBoneType.boneType;
            if (voxBuildStep.layer == SlotLayer.Gear)
            {
                voxBuildStep.bonePosition += voxBuildStep.size / 2;
                // UnityEngine.Debug.LogError("Attaching Gear!");
            }
            steps.Add(voxBuildStep);
            AttachAllPartsToParentItem(in equipment, ref steps, operation, in maleSlot, in itemSlotData, attachedModel, userEquipmentParentEntity, newPosition,
                (steps.Length - 1), bonePosition, in equipmentItemEntities, in itemChunks, in equipmentBoneTypes, in slotLinks, in slots, in itemSlotDatas,
                in userEquipmentMetaDatas, in equipmentItems);
        }

        //! Calculates a bone position using anchors and a layer check.
        private static int3 CalculateBonePosition(int3 newPosition, int3 childModelSize, byte operation, byte femaleSlotLayer, byte slotAxis)
        {
            var childHalfSize = childModelSize / 2;
            var bonePosition = newPosition;
            if (femaleSlotLayer == SlotLayer.Body)
            {
                if (slotAxis == EquipmentSlotAnchor.Bottom || slotAxis == EquipmentSlotAnchor.Top)
                {
                    bonePosition.x += childHalfSize.x;
                    bonePosition.z += childHalfSize.z;
                }
                else if (slotAxis == EquipmentSlotAnchor.Left || slotAxis == EquipmentSlotAnchor.Right)
                {
                    bonePosition.y += childHalfSize.y;
                    bonePosition.z += childHalfSize.z;
                    if (operation == VoxOperation.FlipX)
                    {
                        bonePosition.x += childModelSize.x;
                    }
                }
            }
            return bonePosition;
        }

        // Using sizes of models, operation type, and the axis information
        //      We then calculation where to position the new body part
        private static int3 CalculateNewBodyPartPosition(int3 position, int3 childModelSize, int3 attachItemOffset, int3 parentModelSize, byte slotAxis,
            byte lastOperation, byte operation, int3 femaleOffset)
        {
            var newPosition = position;
            // using axis, calculate position offset
            var childHalfSize = childModelSize / 2;
            var parentHalfSize = parentModelSize / 2;
            if (slotAxis == EquipmentSlotAnchor.Bottom)
            { 
                newPosition += new int3(0, -childModelSize.y, 0);
                newPosition.x += parentHalfSize.x - childHalfSize.x;
                newPosition.z += parentHalfSize.z - childHalfSize.z;
                if (lastOperation == VoxOperation.FlipX && operation == VoxOperation.FlipX && parentModelSize.x % 2 == 0 && childModelSize.x % 2 != 0)
                {
                    // UnityEngine.Debug.LogError("EquipmentSlotAnchor.Bottom childModelSize.x: " + childModelSize.x);
                    newPosition.x -= childModelSize.x % 2;
                }
            }
            else if (slotAxis == EquipmentSlotAnchor.Top)
            { 
                newPosition += new int3(0, parentModelSize.y, 0);
                newPosition.x += parentHalfSize.x - childHalfSize.x;
                newPosition.z += parentHalfSize.z - childHalfSize.z;
            }
            else if (slotAxis == EquipmentSlotAnchor.Left)
            { 
                newPosition.y += parentHalfSize.y - childHalfSize.y;
                newPosition.z += parentHalfSize.z - childHalfSize.z;
                if (operation == VoxOperation.FlipX)
                {
                    newPosition += new int3(parentModelSize.x, 0, 0);
                }
                else
                {
                    newPosition += new int3(-childModelSize.x, 0, 0);
                }
            }
            else if (slotAxis == EquipmentSlotAnchor.Right)
            { 
                newPosition.y += parentHalfSize.y - childHalfSize.y;
                newPosition.z += parentHalfSize.z - childHalfSize.z;
                if (operation == VoxOperation.FlipX)
                {
                    newPosition += new int3(-childModelSize.x, 0, 0);
                }
                else
                {
                    newPosition += new int3(parentModelSize.x, 0, 0);
                }
            }
            // add offsets
            var offset = int3.zero;
            offset += attachItemOffset;
            // for right boot
            if (operation == VoxOperation.FlipX && slotAxis == EquipmentSlotAnchor.Center)
            {
                offset.x -= attachItemOffset.x;
            }
            offset += femaleOffset;
            // offset.x -= femaleOffset.x;
            // apply operations
            if (operation == VoxOperation.FlipX)
            {
                offset = new int3(-offset.x,  offset.y,  offset.z);
                // for hands
                if (slotAxis == EquipmentSlotAnchor.Bottom || slotAxis == EquipmentSlotAnchor.Top)
                {
                    if (childModelSize.x % 2 == 0 && parentModelSize.x % 2 != 0)
                    {
                        /*if (slotAxis == EquipmentSlotAnchor.Bottom)
                        {
                            UnityEngine.Debug.LogError("Offset is being offset: " + parentModelSize.x);
                        }*/
                        offset.x += parentModelSize.x % 2;
                    }
                }
            }
            // after offset is flipped or whatever
            newPosition += offset;
            return newPosition;
        }

        public static int3 ProcessBodyBuilderData(ref VoxBuildSteps voxBuildSteps)
        {
            var combinedPositions = new NativeList<int3>();
            int3 min;
            int3 max;
            var size = CalculateSize(in voxBuildSteps, out min, out max);
            var addition = CalculateAddition(min, max);
            voxBuildSteps.AddToPositions(addition);
            voxBuildSteps.AddToBonePositions(addition);
            combinedPositions.Dispose();
            return size;
        }

        public static int3 CalculateAddition(int3 min, int3 max)
        {
            var addition = new int3(0, 0, 0);
            if (min.x < 0)
            {
                addition.x = math.abs(min.x);
            }
            if (min.y < 0)
            {
                addition.y = math.abs(min.y);
            }
            if (min.z < 0)
            {
                addition.z = math.abs(min.z);
            }
            return addition;
        }

        public static int3 CalculateSize(in VoxBuildSteps voxBuildSteps, out int3 min, out int3 max)
        {
            min = new int3(666,666,666);
            max = new int3(-666,-666,-666);
            for (int i = 0; i < voxBuildSteps.steps.Length; i++)
            {
                var voxBuildStep = voxBuildSteps.steps[i];
                if (voxBuildStep.boneType != HumanoidBoneType.Held)
                {
                    var lhs = voxBuildStep.position; // - new int3(1, 1, 1); // positions[i];
                    var rhs = lhs + voxBuildStep.size; // + new int3(1, 1, 1);
                    if (lhs.x < min.x) min.x = lhs.x;
                    if (lhs.y < min.y) min.y = lhs.y;
                    if (lhs.z < min.z) min.z = lhs.z;
                    if (rhs.x > max.x) max.x = rhs.x;
                    if (rhs.y > max.y) max.y = rhs.y;
                    if (rhs.z > max.z) max.z = rhs.z;
                }
            }
            var outputSize = max;
            if (min.x < 0)
            {
                outputSize.x += math.abs(min.x);
            }
            if (min.y < 0)
            {
                outputSize.y += math.abs(min.y);
            }
            if (min.z < 0)
            {
                outputSize.z += math.abs(min.z);
            }
            return outputSize;
        }
    }
}
                /*if (coreModel.voxelDimensions.y % 2 == 0)
                {
                    UnityEngine.Debug.LogError("Chest voxelDimensions.y: " + coreModel.voxelDimensions.y);
                    // voxBuildStep.bonePosition.y -= voxBuildStep.bonePosition.y % 2;
                } */