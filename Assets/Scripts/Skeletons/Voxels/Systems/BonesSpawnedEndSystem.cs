using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Skeletons
{
    //! Links the bones to the boneLinks after spawning.
    /**
    *   \todo SkeletonBuilt event that camera system uses to reposition first person camera.
    */
    [UpdateAfter(typeof(BoneSpawnSystem))]
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class BonesSpawnedEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<OnBonesSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref SkeletonDirty skeletonDirty) =>
            {
                if (skeletonDirty.state == EquipmentDirtyState.SetBones)
                {
                    skeletonDirty.state = EquipmentDirtyState.SetBonesWaiting;
                }
                else if (skeletonDirty.state == EquipmentDirtyState.SetBonesWaiting)
                {
                    // UnityEngine.Debug.LogError("Bones set: " + count);
                    skeletonDirty.state = EquipmentDirtyState.BuildHeldItems;
                    if (HasComponent<CameraLink>(e))
                    {
                        PostUpdateCommands.AddComponent<SetCameraBone>(entityInQueryIndex, e);
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}