using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Animations;
using Zoxel.Voxels;
using Zoxel.Items;
using Zoxel.Voxels.Authoring;
// Equip system will add updates / resizes to the planet if body is dirty
// note: camera system has to wait one frame for bone data to be set properly
// [UpdateAfter(typeof(BoneCameraSystem))] 
// todo:    -> Make this spawn a chunk first and update chunks rather then Chunk Component
// todo: Optimize the updating body parts process rather then recreating the entire thing
//      - Solution: Check the zone of the Chunk updated, and readd all those models (limited only to the zone of updates)
//      Cache some of the data used to reequip - like the bodys vox chunks
//      Rather then recreating the entire body whenequiping / unequiping - maybe store it in layers? idk yet
//  Can I reorder the list based on tree level?
//      Body Chest -> Head / Arms / Hips -> Hat / Biceps / Thighs -> forearm / Calfs -> Hands / Feet -> Held / Shoes etc

namespace Zoxel.Skeletons
{
    //! Spawns Items in Hand from Equipment.
    /**
    *   - Linking System -
    */
    [UpdateAfter(typeof(BonesSpawnedSystem))]
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class BodyBuilderHeldItemsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery itemQuery;
        private Entity worldVoxPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            itemQuery = GetEntityQuery(
                ComponentType.ReadOnly<RealmItem>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<GenerateVox>());
            RequireForUpdate<ModelSettings>();
            RequireForUpdate(processQuery);
        }

        private void InitializePrefabs()
        {
            if (this.worldVoxPrefab.Index != 0)
            {
                return;
            }
            var modelSettings = GetSingleton<ModelSettings>();
            var voxelScale = modelSettings.GetVoxelScale();
            this.worldVoxPrefab = EntityManager.Instantiate(VoxelSystemGroup.worldVoxPrefab);
            EntityManager.AddComponent<Prefab>(this.worldVoxPrefab);
            EntityManager.AddComponent<HeldWeapon>(this.worldVoxPrefab);
            EntityManager.AddComponent<CharacterLink>(this.worldVoxPrefab);
            EntityManager.AddComponent<ParentLink>(this.worldVoxPrefab);
            EntityManager.AddComponent<LocalPosition>(this.worldVoxPrefab);
            EntityManager.AddComponent<LocalRotation>(this.worldVoxPrefab);
            EntityManager.AddComponent<WorldVoxItem>(this.worldVoxPrefab);
            EntityManager.AddComponent<EntityLighting>(this.worldVoxPrefab);
            EntityManager.SetComponentData(worldVoxPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(this.worldVoxPrefab, new VoxScale { scale = voxelScale });
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var modelSettings = GetSingleton<ModelSettings>();
            var voxelScale2 = modelSettings.GetVoxelScale();
            var itemVoxScale = voxelScale2 * 0.9f; // 1.03f;
            var voxelScale = 48f;
            var worldVoxPrefab = this.worldVoxPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var itemEntities = itemQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var itemVoxDatas = GetComponentLookup<Chunk>(true);
            var itemVoxColors = GetComponentLookup<VoxColors>(true);
            itemEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity, InitializeEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref HeldWeaponLinks heldWeaponLinks, in SkeletonDirty skeletonDirty, in Vox vox, in BoneLinks boneLinks,
                    in VoxBuildSteps voxBuildSteps, in EntityLighting entityLighting) =>
            {
                if (skeletonDirty.state != EquipmentDirtyState.BuildHeldItems)
                {
                    return;
                }
                //! Ends SkeletonDirty process.
                PostUpdateCommands.RemoveComponent<VoxBuildSteps>(entityInQueryIndex, e);
                PostUpdateCommands.RemoveComponent<SkeletonDirty>(entityInQueryIndex, e);
                PostUpdateCommands.RemoveComponent<EntityBusy>(entityInQueryIndex, e);
                if (heldWeaponLinks.heldWeaponRightHand.Index > 0)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, heldWeaponLinks.heldWeaponRightHand);
                    heldWeaponLinks.heldWeaponRightHand = new Entity();
                }
                if (heldWeaponLinks.heldWeaponLeftHand.Index > 0)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, heldWeaponLinks.heldWeaponLeftHand);
                    heldWeaponLinks.heldWeaponLeftHand = new Entity();
                }
                // Spawn Held Vox
                byte heldCount = 0;
                for (int i = 0; i < voxBuildSteps.steps.Length; i++)
                {
                    var voxBuildStep = voxBuildSteps.steps[i];
                    if (voxBuildStep.boneType == HumanoidBoneType.Held)
                    {
                        var itemData = itemVoxDatas[voxBuildStep.voxEntity];
                        var voxColors = itemVoxColors[voxBuildStep.voxEntity];
                        var parentIndex = voxBuildStep.parent;
                        var handBoneStep = voxBuildSteps.steps[parentIndex];
                        var handBone = boneLinks.bones[parentIndex];
                        // get hand
                        if (SpawnWorldVox(ref heldWeaponLinks, PostUpdateCommands, e, entityInQueryIndex, worldVoxPrefab, in voxBuildSteps, in itemData, in voxColors,
                            in handBoneStep, in handBone, in boneLinks, itemVoxScale, in entityLighting, heldCount, voxelScale))
                        {
                            heldCount++;
                            // UnityEngine.Debug.LogError("Spawned Held Item: " + heldCount);
                        }
                    }
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnSpawnedHeldWeapons(heldCount));
                PostUpdateCommands.AddComponent<OnSkeletonBuilt>(entityInQueryIndex, e);
                voxBuildSteps.Dispose();
            })  .WithReadOnly(itemVoxDatas)
                .WithReadOnly(itemVoxColors)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static bool SpawnWorldVox(ref HeldWeaponLinks heldWeaponLinks, EntityCommandBuffer.ParallelWriter PostUpdateCommands, Entity e, int entityInQueryIndex, Entity worldVoxPrefab,
            in VoxBuildSteps voxBuildSteps, in Chunk heldVox, in VoxColors voxColors, in VoxBuildStep handVoxBuildStep, in Entity handBone,
            in BoneLinks boneLinks, float3 itemVoxScale, in EntityLighting entityLighting, byte heldCount, float voxelScale)
        {
            // spawn the weapon seperately in players hand
            if (handBone.Index == 0)
            {
                return false;
            }
            // UnityEngine.Debug.LogError("Spawning " + heldCount + " Hand Held!");
            if (handVoxBuildStep.operation != VoxOperation.None) //heldCount == 0)
            {
                heldWeaponLinks.rightHand = handBone;
                // UnityEngine.Debug.LogError("Spawning Right Hand!");
            }
            else
            {
                heldWeaponLinks.leftHand = handBone;
                // UnityEngine.Debug.LogError("Spawning Left Hand!");
            }
            var itemOffset = new int3(0, 4, 8).ToFloat3() / voxelScale; // 48f;
            var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, worldVoxPrefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ParentLink(handBone));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new LocalPosition(itemOffset));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new CharacterLink(e));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new HeldWeapon(heldCount));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new VoxScale(itemVoxScale));
            var newPartSize = heldVox.voxelDimensions;
            // if (handVoxBuildStep.modifier == VoxModifier.RotateXY)
            // if (heldCount == 0)
            {
                // UnityEngine.Debug.LogError("Rotating Pickaxe at: " + itemOffset);
                var newPartData = heldVox.voxels.CloneTemp();
                var newPartData2 = heldVox.voxels.CloneTemp();
                var newPartData3 = heldVox.voxels.CloneTemp();
                int3 newSize;
                VoxDataEditor.RotateVox(0, in newPartData, ref newPartData2, newPartSize, out newSize);
                newPartSize = newSize;
                int3 newSize2;
                VoxDataEditor.RotateVox(1, in newPartData2, ref newPartData3, newPartSize, out newSize2);
                newPartSize = newSize2;
                VoxDataEditor.FlipVox(2, ref newPartData3, newPartSize);
                var heldVoxData = new Chunk(newPartSize, new BlitableArray<byte>(newPartData3, Allocator.Persistent));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, heldVoxData);
                newPartData.Dispose();
                newPartData2.Dispose();
                newPartData3.Dispose();
            }
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ChunkDimensions(newPartSize));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new VoxColors(voxColors.colors.Clone()));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, entityLighting);
            if (handVoxBuildStep.operation != VoxOperation.None) //heldCount == 0)
            {
                PostUpdateCommands.AddComponent<RightHeld>(entityInQueryIndex, e2);
            }
            else
            {
                PostUpdateCommands.AddComponent<LeftHeld>(entityInQueryIndex, e2);
            }
            return true;
        }
    }
}