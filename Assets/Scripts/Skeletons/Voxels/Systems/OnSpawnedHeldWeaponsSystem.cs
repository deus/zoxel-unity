using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Skeletons
{
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class OnSpawnedHeldWeaponsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery heldItemQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            heldItemQuery = GetEntityQuery(
                ComponentType.ReadOnly<HeldWeapon>(),
                ComponentType.ReadOnly<CharacterLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var weaponEntities = heldItemQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            // var heldWeapons = GetComponentLookup<HeldWeapon>(true);
            var characterLinks = GetComponentLookup<CharacterLink>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref HeldWeaponLinks heldWeaponLinks, in OnSpawnedHeldWeapons onSpawnedHeldWeapons) =>
            {
                if (onSpawnedHeldWeapons.heldWeaponsSpawned == 0)
                {
                    PostUpdateCommands.RemoveComponent<OnSpawnedHeldWeapons>(entityInQueryIndex, e);
                    return;
                }
                var foundWeapons = 0;
                for (int i = 0; i < weaponEntities.Length; i++)
                {
                    var e2 = weaponEntities[i];
                    var character = characterLinks[e2].character;
                    if (character == e)
                    {
                        // var heldWeapon = heldWeapons[e2];
                        if (HasComponent<RightHeld>(e2))
                        {
                            heldWeaponLinks.heldWeaponRightHand = e2;
                        }
                        else if (HasComponent<LeftHeld>(e2))
                        {
                            heldWeaponLinks.heldWeaponLeftHand = e2;
                        }
                        foundWeapons++;
                        if (foundWeapons == onSpawnedHeldWeapons.heldWeaponsSpawned)
                        {
                            PostUpdateCommands.RemoveComponent<OnSpawnedHeldWeapons>(entityInQueryIndex, e);
                            return;
                        }
                    }
                }
            })  .WithReadOnly(weaponEntities)
                .WithDisposeOnCompletion(weaponEntities)
                // .WithReadOnly(heldWeapons)
                .WithReadOnly(characterLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}