using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;  // SkeletonWeights
using Zoxel.Voxels;     // Chunks
using Zoxel.Voxels.Authoring;

namespace Zoxel.Skeletons
{
    //! A system that recolours the vertices of a mesh based on the bone weights.
    [BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
	public partial class ChunkRenderColorWeightsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery skeletonQuery;
        private EntityQuery bonesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            skeletonQuery = GetEntityQuery(
                ComponentType.ReadOnly<BoneLinks>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<EntityBusy>());
            bonesQuery = GetEntityQuery(ComponentType.ReadOnly<Bone>());
            RequireForUpdate<ModelSettings>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
		{
            var modelSettings = GetSingleton<ModelSettings>();
            var voxelSize = modelSettings.GetVoxelSize();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var skeletonEntities = skeletonQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var boneLinks = GetComponentLookup<BoneLinks>(true);
            var skeletonIDs = GetComponentLookup<ZoxID>(true);
            skeletonEntities.Dispose();
            var boneEntities = bonesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var bones = GetComponentLookup<Bone>(true);
            boneEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref SkeletonWeights skeletonWeights, ref ChunkRenderBuilder chunkRenderBuilder,
                ref ModelMesh modelMesh, in VoxLink voxLink) =>
            {
				if (chunkRenderBuilder.chunkRenderBuildState != (byte)ChunkRenderBuildState.BakeColorWeights || !boneLinks.HasComponent(voxLink.vox))
                {
                    return;
                }
                chunkRenderBuilder.chunkRenderBuildState = (byte) ChunkRenderBuildState.CompletedChunkRenderBuilder;
                var boneLinks2 = boneLinks[voxLink.vox];
                var boneColors = new NativeArray<float3>(boneLinks2.bones.Length, Allocator.Temp);
                var random = new Random();
                random.InitState((uint) skeletonIDs[voxLink.vox].id);
                for (byte i = 0; i < boneLinks2.bones.Length; i++)
                {
                    var boneEntity = boneLinks2.bones[i];
                    if (bones.HasComponent(boneEntity))
                    {
                        var bone = bones[boneEntity];
                        if (i == 0)
                        {
                            boneColors[i] = new Color(205, 45, 45).ToFloat3();
                        }
                        else
                        {
                            boneColors[i] = new Color(random.NextInt(255), random.NextInt(255), random.NextInt(255)).ToFloat3();
                        }
                    }
                }
                for (int j = 0; j < skeletonWeights.boneIndexes.Length; j++)
                {
                    var closestIndex = skeletonWeights.boneIndexes[j];
                    var vert = modelMesh.vertices[j];
                    vert.color = boneColors[(int)closestIndex];
                    modelMesh.vertices[j] = vert;
                }
                boneColors.Dispose();
            })  .WithReadOnly(boneLinks)
                .WithReadOnly(skeletonIDs)
                .WithReadOnly(bones)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}