using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Skeletons
{
    public struct VoxBuildSteps : IComponentData
    {
        public int3 size;
        public BlitableArray<VoxBuildStep> steps;

        public void Dispose()
        {
            if (steps.Length > 0)
            {
                steps.Dispose();
            }
        }

        public void AddToPositions(int3 addition)
        {
            for (int i = 0; i < steps.Length; i++)
            {
                var step = steps[i];
                step.position += addition;
                steps[i] = step;
            }
        }
        
        public void AddToBonePositions(int3 addition)
        {
            for (int i = 0; i < steps.Length; i++)
            {
                var step = steps[i];
                step.bonePosition += addition;
                steps[i] = step;
            }
        }
    }
}