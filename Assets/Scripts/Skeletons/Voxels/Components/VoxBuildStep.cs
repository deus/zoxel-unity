using Unity.Entities;

namespace Zoxel.Skeletons
{
    //! A single build step in the body building process.
    /**
    *   Contains a link to a Vox model entity and some information on how to add it to the body.
    */
    public struct VoxBuildStep
    {
        public int3 size;
        public Entity voxEntity;
        public byte modifier;

        public byte layer;
        public byte boneType;
        public byte axes;
        public int parent;
        public int3 position;
        public int3 bonePosition;
        public byte operation;
    }
}