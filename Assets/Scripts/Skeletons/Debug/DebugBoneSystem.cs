﻿using Unity.Entities;
using Unity.Burst;

using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Lines;
using Zoxel.Voxels;

namespace Zoxel.Skeletons.Debug
{
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class DebugBoneSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static float boneDebugSize = 1 / 32f;
        public static float shrinkSize = 64;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (VoxelManager.instance == null) return;
            // Need a SkeletonManager
            if (!VoxelManager.instance.voxelSettings.isDrawBoneBoxes)
            {
                return;
            }
            //var voxelSize = VoxelManager.instance.GetVoxelScale();
            var elapsedTime = World.Time.ElapsedTime;
            var boneDebugSize = DebugBoneSystem.boneDebugSize;
            var boneSize =  1.2f * (new float3(boneDebugSize, boneDebugSize, boneDebugSize));
            var linePrefab = RenderLineGroup.linePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<Bone>()
                .ForEach((Entity e, in ParentLink parent, in Translation translation, in Rotation rotation) =>
            {
                var worldPosition = translation.Value;
                var worldRotation = rotation.Value;
                RenderLineGroup.CreateCubeLines(PostUpdateCommands, linePrefab, elapsedTime, worldPosition, worldRotation, boneSize); 
                RenderLineGroup.SpawnLine(PostUpdateCommands, linePrefab, elapsedTime, worldPosition, worldPosition + math.rotate(worldRotation, new float3(0, 0, 0.3f)), 1);
            }).Run();
        }
    }
}
                // if (HasComponent<Bone>(parent.parent))
                // {
                //     //var parentBone = EntityManager.GetComponentData<Bone>(parent.parent);
                //     //var parentWorldPosition = parentBone.GetWorldPosition(EntityManager, parent.parent);
                //     //UnityEngine.Debug.DrawLine(worldPosition, parentWorldPosition, UnityEngine.Color.white); //, 0.01f);
                // }
                //Matrix4x4 localToWorldPos = localToWorld.Value;
                //Vector3 position = localToWorldPos.GetColumn(3);
                //var boneLinks = EntityManager.GetComponentData<BoneLinks>(bone.boneLinks);
                //var skeletonPosition = EntityManager.GetComponentData<Translation>(bone.boneLinks);
                //var skeletonRotation = EntityManager.GetComponentData<Rotation>(bone.boneLinks);
                //var worldPosition = skeletonPosition.Value + math.rotate(skeletonRotation.Value, bone.GetSkinningBonePosition());
                // var rotation = skeletonRotation.Value;
                //var newPosition = math.rotate(boneLinks.mulRotation, position);
                //worldRotation = math.mul(skeletonRotation.Value, worldRotation);
                //DebugLines.DrawCubeLines(worldPosition, worldRotation, boneSize, UnityEngine.Color.green);
                //var influenceSize = bone.GetInfluenceSize(voxelSize);
                //var influenceOffset = bone.GetInfluenceOffset(voxelSize);
                // should put the elapsed time in InitializeSystem
                //DebugLines.DrawCubeLines(worldPosition + influenceOffset, worldRotation, 0.5f * influenceSize, UnityEngine.Color.red);
