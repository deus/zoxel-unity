using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Movement;
using Zoxel.Transforms;
using Zoxel.Cameras;
using Zoxel.Cameras.Authoring;

namespace Zoxel.Skeletons.Cameras
{
    //! Bobs the heead bone up and down ussing character movement
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class CameraBobSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery charactersQuery;

        protected override void OnCreate()
        {
            charactersQuery = GetEntityQuery(
                ComponentType.Exclude<FlyMode>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<BoneLinks>(),
                ComponentType.ReadOnly<Rotation>(),
                ComponentType.ReadOnly<BodyForce>(),
                ComponentType.ReadOnly<IsOnGround>());
            RequireForUpdate<FirstPersonCameraSettings>();
            RequireForUpdate(processQuery);
            RequireForUpdate(charactersQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var firstPersonCameraSettings = GetSingleton<FirstPersonCameraSettings>();
            if (firstPersonCameraSettings.disableHeadBob)
            {
                return;
            }
            var idleBobDistance = firstPersonCameraSettings.cameraBobDistance; // 0.022f; // 0.016f;
            var bobWalkingDistance = idleBobDistance * firstPersonCameraSettings.walkingDistanceDistanceMultiplier; // 0.038f;
            var cameraBobBaseSpeed = firstPersonCameraSettings.cameraBobBaseSpeed; // 2f;
            var cameraBobSpeedMultiplier = firstPersonCameraSettings.cameraBobSpeedMultiplier; // 2f;
            var cameraBobWalkingSpeedMultiplier = firstPersonCameraSettings.cameraBobWalkingSpeedMultiplier; // 2f;
            var cameraBobHeightLerpSpeed = firstPersonCameraSettings.cameraBobHeightLerpSpeed;
            var deltaTime = World.Time.DeltaTime;
            var characterEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var bodyRotations = GetComponentLookup<Rotation>(true);
            var bodyForces = GetComponentLookup<BodyForce>(true);
            var isOnGrounds = GetComponentLookup<IsOnGround>(true);
            characterEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity>()
                .ForEach((ref LocalPosition localPosition, ref CameraBob cameraBob, in SkeletonLink skeletonLink) =>
            {
                var skeletonEntity = skeletonLink.skeleton;
                if (!bodyForces.HasComponent(skeletonEntity))
                {
                    return;
                }
                // get bobbing speed from character
                var bodyRotation = bodyRotations[skeletonEntity].Value;
                var bodyForce = bodyForces[skeletonEntity].velocity;
                bodyForce = math.mul(math.inverse(bodyRotation), bodyForce);
                var isOnGround = isOnGrounds[skeletonEntity].isOnGround;
                var combinedMovementForce = 0f;
                if (isOnGround)
                {
                    var forceX = math.abs(bodyForce.x);
                    var forceZ = math.abs(bodyForce.z);
                    if (forceX > forceZ)
                    {
                        combinedMovementForce = forceX;
                    }
                    else
                    {
                        combinedMovementForce = forceZ;
                    }
                    //UnityEngine.Debug.LogError("combinedMovementForce: " + combinedMovementForce + " : " + bodyForce);
                } 
                /*else
                {
                    UnityEngine.Debug.LogError("Not on ground.");
                }*/
                var isWalking = combinedMovementForce >= 0.03f;    // 0.02
                // Get the deltaTime of force XZ
                var bobbingSpeed = cameraBobBaseSpeed + combinedMovementForce;
                if (isWalking)
                {
                    bobbingSpeed = cameraBobBaseSpeed + combinedMovementForce * cameraBobWalkingSpeedMultiplier;
                }
                cameraBob.time += bobbingSpeed * cameraBobSpeedMultiplier * deltaTime;
                var bobDistance = idleBobDistance;
                if (isWalking)
                {
                    //UnityEngine.Debug.LogError("Is Moving.");
                    bobDistance = bobWalkingDistance;
                }
                cameraBob.bobHeight = math.lerp(cameraBob.bobHeight, bobDistance, deltaTime * cameraBobHeightLerpSpeed);
                localPosition.position = cameraBob.position + new float3(0, math.sin(cameraBob.time) * cameraBob.bobHeight, 0);
            })  .WithReadOnly(bodyRotations)
                .WithReadOnly(bodyForces)
                .WithReadOnly(isOnGrounds)
                .ScheduleParallel(Dependency);
        }
    }
}