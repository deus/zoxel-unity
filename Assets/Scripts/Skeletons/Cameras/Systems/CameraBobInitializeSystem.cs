using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Cameras;
using Zoxel.Cameras.Authoring;

namespace Zoxel.Skeletons.Cameras
{
    //! Bobs the heead bone up and down ussing character movement
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class CameraBobInitializeSystem : SystemBase
    {
        protected override void OnCreate()
        {
            RequireForUpdate<FirstPersonCameraSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var firstPersonCameraSettings = GetSingleton<FirstPersonCameraSettings>();
            var idleBobDistance = firstPersonCameraSettings.cameraBobDistance; // 0.022f; // 0.016f;
            Dependency = Entities
                .WithAll<InitializeEntity, CameraBob>()
                .ForEach((ref CameraBob cameraBob, in LocalPosition localPosition) =>
            {
                cameraBob.position = localPosition.position;
                cameraBob.bobHeight = idleBobDistance;
            }).ScheduleParallel(Dependency);
        }
    }
}