using Unity.Entities;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Cameras;
using Zoxel.Voxels;

namespace Zoxel.Skeletons.Cameras
{
    //! Sets Camera position on the boneLinks.
    /**
    *   \todo Base positioning on vox model itself and if it's air or solid where the camera can be -instead of bounding box
    */
    [BurstCompile, UpdateInGroup(typeof(SkeletonSystemGroup))]
    public partial class BoneCameraSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery boneQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            boneQuery = GetEntityQuery(
                ComponentType.ReadOnly<Bone>(),
                ComponentType.ReadOnly<BoneType>(),
                ComponentType.ReadOnly<Translation>());
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            // const bool thirdPerson = false;
            var voxelCameraOffset = 0.5f; // -1f; // 0.5f; // 2f; // 0.5f;
            var disableCameraHeadOffset = false;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var boneEntities = boneQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var bones = GetComponentLookup<Bone>(true);
            var boneTypes = GetComponentLookup<BoneType>(true);
            var bonePositions = GetComponentLookup<Translation>(true);
            boneEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<SetCameraBone>()
                .ForEach((Entity e, int entityInQueryIndex, in BoneLinks boneLinks, in VoxScale voxScale, in CameraLink cameraLink) =>
            {
                var cameraEntity = cameraLink.camera;
                if (HasComponent<Camera>(cameraEntity))
                {
                    var localPosition = new float3();
                    var bonePosition = new float3();
                    var targetBone = new Entity();
                    for (int i = 0; i < boneLinks.bones.Length; i++)
                    {
                        var boneEntity = boneLinks.bones[i];
                        if (!bones.HasComponent(boneEntity))
                        {
                            return;
                        }
                        var boneType = boneTypes[boneEntity];
                        if (boneType.boneType == HumanoidBoneType.Head)
                        {
                            var boneData = bones[boneEntity];
                            localPosition = float3.zero;
                            if (!disableCameraHeadOffset)
                            {
                                localPosition.z -= ((float) boneData.size.z) / 2;
                                localPosition.z -= voxelCameraOffset;
                            }
                            localPosition.y += (2 * boneData.size.y) / 3;
                            localPosition.z *= -1;
                            localPosition.x *= voxScale.scale.x;
                            localPosition.y *= voxScale.scale.y;
                            localPosition.z *= voxScale.scale.z;
                            targetBone = boneEntity;
                            bonePosition = bonePositions[boneEntity].Value;
                            break;
                        }
                    }
                    /*if (thirdPerson)
                    {
                        localPosition.z -= 1f;
                    }*/
                    PostUpdateCommands.AddComponent(entityInQueryIndex, cameraEntity, new FirstPersonCameraOffset(localPosition));
                    PostUpdateCommands.AddComponent(entityInQueryIndex, cameraEntity, new AttachToEntity(targetBone, localPosition));
                }
                PostUpdateCommands.RemoveComponent<SetCameraBone>(entityInQueryIndex, e);
            })  .WithReadOnly(bones)
                .WithReadOnly(boneTypes)
                .WithReadOnly(bonePositions)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}