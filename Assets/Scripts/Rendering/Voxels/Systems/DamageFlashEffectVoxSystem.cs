

using Unity.Entities;
using Unity.Burst;
using Zoxel.Voxels;

namespace Zoxel.Rendering
{
    //! This pushes DamageFlashEffect updates from Vox to Renders.
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class DamageFlashEffectVoxSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in Vox vox, in DamageFlashEffect damageFlashEffect) =>
            {
                for (int i = 0; i < vox.chunks.Length; i++)
                {
                    var chunk = vox.chunks[i];
                    PostUpdateCommands.AddComponent(entityInQueryIndex, chunk, damageFlashEffect);
                }
                PostUpdateCommands.RemoveComponent<DamageFlashEffect>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in ChunkRenderLinks chunkRenderLinks, in DamageFlashEffect damageFlashEffect) =>
            {
                for (int i = 0; i < chunkRenderLinks.chunkRenders.Length; i++)
                {
                    var chunkRenderEntity = chunkRenderLinks.chunkRenders[i];
                    PostUpdateCommands.AddComponent(entityInQueryIndex, chunkRenderEntity, damageFlashEffect);
                }
                PostUpdateCommands.RemoveComponent<DamageFlashEffect>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

        }
    }
}