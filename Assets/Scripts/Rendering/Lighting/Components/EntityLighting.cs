using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Rendering
{
	//! Simple Lighting on an Entity.
	public struct EntityLighting : IComponentData
	{
		public byte lightValue;

		public EntityLighting(byte lightValue)
		{
			this.lightValue = lightValue;
		}

		public float GetBrightness()
		{
			return LightingCurve.lightValues[lightValue];
		}

		public float4 GetBrightnessFloat4()
		{
			var brightness = GetBrightness();
			return new float4(brightness, brightness, brightness, 1);
		}
	}
}