using Unity.Entities;

namespace Zoxel.Rendering
{
	public static class LightingCurve
	{
        //! Used to curve light values \todo plot this graph on a curve editor
        public static readonly float[] lightValues = new float[]
        {
            0.02f,
            0.06f,
            0.12f,
            0.18f,
            0.24f,
            0.31f,
            0.37f,
            0.43f,
            // 8
            0.49f,
            0.55f,
            0.6f,
            0.68f,
            0.75f,
            0.84f,
            0.92f,
            1f,
            // 16
            1.2f
        };
    }
}