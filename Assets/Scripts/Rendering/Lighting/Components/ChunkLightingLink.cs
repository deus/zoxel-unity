using Unity.Entities;

namespace Zoxel.Rendering
{
    //! Dynamically updates an entity's lighting.
    /**
    *   \todo Remove this and use generic EntityInChunk / VoxelPositionMoved event.
    */
    public struct ChunkLightingLink : IComponentData
	{
        public int3 chunkPosition;
        public Entity chunk;
        public int3 lastLightPosition;
        public byte wasChunkBuilding;

        public ChunkLightingLink(Entity chunk, int3 chunkPosition)
        {
            this.chunk = chunk;
            this.chunkPosition = chunkPosition;
            this.lastLightPosition = int3.zero;
            this.wasChunkBuilding = 0;
        }
    }
}