using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Rendering
{
	//! Clears SkeletonWeights after Destroyed.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class SkeletonWeightsDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, SkeletonWeights>()
                .ForEach((in SkeletonWeights skeletonWeights) =>
			{
                skeletonWeights.Dispose();
			}).ScheduleParallel();
		}
	}
}