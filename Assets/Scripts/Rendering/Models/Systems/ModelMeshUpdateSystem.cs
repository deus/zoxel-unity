﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.Rendering;

namespace Zoxel.Rendering
{
    //! Converts component ModelMesh data into a unity mesh data for gpu use.
    /**
    *   \todo Add dynamic skeleton - render bounds updating.
    */
    [UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ModelMeshUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private VertexAttributeDescriptor[] vertLayoutUV;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            vertLayoutUV = new[]
            {
                new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 3),
                new VertexAttributeDescriptor(VertexAttribute.Normal, VertexAttributeFormat.Float32, 3),
                new VertexAttributeDescriptor(VertexAttribute.Color, VertexAttributeFormat.Float32, 3)
            };
        }

        protected override void OnUpdate()
		{
            var vertLayoutUV = this.vertLayoutUV;
            Entities
                .WithNone<InitializeEntity>()
                .WithAll<MeshDirty>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in ModelMesh modelMesh) =>
            {
                if (zoxMesh.mesh == null)
                {
                    // UnityEngine.Debug.LogWarning("ZoxMeshUpdateSystem ZoxMesh mesh is null!");
                    UnityEngine.Debug.LogError("ZoxMeshUpdateSystem ZoxMesh mesh is null!");
                    // zoxMesh.mesh = new UnityEngine.Mesh();
                    // PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
                    return;
                }
                if (modelMesh.vertices.Length == 0)
                {
                    zoxMesh.mesh.Clear();
                    // UnityEngine.Debug.LogError("ZoxMeshUpdateSystem ZoxMesh Vertices Empty [" + e.Index + "]");
                    return;
                }
                // UnityEngine.Debug.LogError("vertexCount: " + zoxMesh.vertices.Length);
                var mesh = zoxMesh.mesh;
                var vertexCount = modelMesh.vertices.Length;
                var trianglesCount = modelMesh.triangles.Length;
                var vertices = modelMesh.GetVertexNativeArray();
                var triangles = modelMesh.GetTriangleNativeArray();
                mesh.SetVertexBufferParams(vertexCount, vertLayoutUV);
                mesh.SetVertexBufferData(vertices, 0, 0, vertexCount);
                // max is x billion for 32 bits uint
                mesh.SetIndexBufferParams(trianglesCount, IndexFormat.UInt32); //  IndexFormat.UInt32  IndexFormat.UInt16
                mesh.SetIndexBufferData(triangles, 0, 0, trianglesCount);
                mesh.SetSubMesh(0, new SubMeshDescriptor()
                {
                    baseVertex = 0,
                    firstVertex = 0,
                    indexStart = 0,
                    bounds = default,
                    vertexCount = vertexCount,
                    indexCount = trianglesCount,
                    topology = UnityEngine.MeshTopology.Triangles
                });
                mesh.UploadMeshData(false);
                if (HasComponent<SkeletonBounds>(e))
                {
                    // As boneLinks can move it's arms around - it may grow in shape - needs bigger bounds
                    // get biggest extend, times that by 3
                    // mesh.bounds = new UnityEngine.Bounds(float3.zero, new float3(16, 16, 16)); //  modelMesh.extents * 32f);
                    mesh.bounds = new UnityEngine.Bounds(float3.zero, modelMesh.extents * 3f);
                }
                else if (HasComponent<MinivoxBounds>(e))
                {
                    var positionOffset = new float3();
                    positionOffset.y = modelMesh.position.y - 0.5f;
                    mesh.bounds = new UnityEngine.Bounds(positionOffset, modelMesh.extents * 2f);
                    // UnityEngine.Debug.LogError("modelMesh.position.y: " + modelMesh.position.y + " : " + positionOffset.y);
                }
                // centred bounds
                else
                {
                    mesh.bounds = new UnityEngine.Bounds(float3.zero, modelMesh.extents * 2f);
                }
                // UnityEngine.Debug.LogError("ZoxMeshUpdateSystem ZoxMesh Updated: " + e.Index + "] Verts: " + vertexCount);
            }).WithoutBurst().Run();
        }
    }
}

// testing mesh bounds
/*var oldBounds = mesh.bounds;
mesh.RecalculateBounds();
var newBounds = mesh.bounds;
if (oldBounds != newBounds)
{
    UnityEngine.Debug.LogError("My Calculated: " + oldBounds.ToString() + "\nUnity Bounds: " + newBounds.ToString());
}*/