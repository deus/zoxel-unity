using Unity.Entities;

namespace Zoxel.Rendering
{
	//! Disposes of Bone Buffer data with DestroyEntity.
    [UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class BoneIndexBufferDestroySystem : SystemBase
	{
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, BoneIndexBuffer>()
                .ForEach((in BoneIndexBuffer boneIndexBuffer) =>
			{
                boneIndexBuffer.Dispose();
			}).WithoutBurst().Run();
			Entities
                .WithAll<DestroyEntity, OriginalBoneBuffer>()
                .ForEach((in OriginalBoneBuffer originalBoneBuffer) =>
			{
                originalBoneBuffer.Dispose();
			}).WithoutBurst().Run();
			Entities
                .WithAll<DestroyEntity, CurrentBoneBuffer>()
                .ForEach((in CurrentBoneBuffer currentBoneBuffer) =>
			{
                currentBoneBuffer.Dispose();
			}).WithoutBurst().Run();
		}
	}
}