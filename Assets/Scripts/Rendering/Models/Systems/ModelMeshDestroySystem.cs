using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Rendering
{
	//! Cleans up ModelMesh after destroyed.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ModelMeshDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, ModelMesh>()
                .ForEach((in ModelMesh modelMesh) =>
			{
                modelMesh.Dispose();
			}).ScheduleParallel();
		}
	}
}