using Unity.Entities;
using System;
using UnityEngine;

namespace Zoxel.Rendering
{
    //! Dynamic Buffer for our bone data.
    /**
    *   These need to set and resize the buffers, compared to the others.
    */
    public struct OriginalBoneBuffer : ISharedComponentData, IEquatable<OriginalBoneBuffer>
    {
        public ComputeBuffer buffer;

        public bool Equals(OriginalBoneBuffer other)
        {
            return object.ReferenceEquals(other.buffer, this.buffer);
        }
        
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                if (buffer == null)
                {
                    return 0;
                }
                return buffer.GetHashCode();
            }
        }

        public void Dispose()
        {
            if (buffer != null)
            {
                buffer.Release();
            }
        }

        public bool Resize(int newCount)
        {
            if (buffer == null || buffer.count != newCount)
            {
                if (buffer != null)
                {
                    buffer.Release();
                }
                if (newCount == 0)
                {
                    if (buffer == null)
                    {
                        return false;
                    }
                    buffer = null;
                }
                else
                {
                    this.buffer = new ComputeBuffer(newCount, 64, ComputeBufferType.Default, ComputeBufferMode.SubUpdates);
                }
                return true;
            }
            return false;
        }
    }
}