/*using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Rendering
{
	//! Like a TerrainMesh but without UVs
    public struct ZoxMushColored : IComponentData
    {
		public float3 position;
		public float3 extents;
        public BlitableArray<ModelVertex> vertices;
        public BlitableArray<uint> triangles;
        
		public void DisposeZoxMushColored()
		{
			if (vertices.Length > 0)
			{
				vertices.Dispose();
			}
			if (triangles.Length > 0)
			{
				triangles.Dispose();
			}
        }

		public NativeArray<ModelVertex> GetVertexNativeArray()
		{
			return vertices.ToNativeArray(Allocator.None);
		}

		public NativeArray<uint> GetTriangleNativeArray()
		{
			return triangles.ToNativeArray(Allocator.None);
		}
    }
}*/