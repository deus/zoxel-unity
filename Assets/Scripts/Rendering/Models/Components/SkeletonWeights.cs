using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Rendering
{
    //! Holds bone indexes for animation data.
    public struct SkeletonWeights : IComponentData
    {
        public BlitableArray<uint> boneIndexes;

        public void Dispose()
        {
            if (boneIndexes.Length > 0)
            {
                boneIndexes.Dispose();
            }
        }

        public void Initialize(int verticesCount)
        {
            if (verticesCount != boneIndexes.Length)
            {
                Dispose();
                boneIndexes = new BlitableArray<uint>(verticesCount, Allocator.Persistent);
            }
        }
    }
}