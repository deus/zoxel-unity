using Unity.Mathematics;
using System.Runtime.InteropServices;

namespace Zoxel.Rendering
{
    [StructLayout(LayoutKind.Sequential)]
	public struct ModelVertex
	{
		public float3 position;
		public float3 normal;
		public float3 color;
	}
}