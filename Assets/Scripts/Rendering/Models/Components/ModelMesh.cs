using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
//public static readonly int maxEverTris = 65536; //65536;67860

namespace Zoxel.Rendering
{
	//! Holds editable mesh data on an entity.
    public struct ModelMesh : IComponentData
    {
		public float3 position;
		public float3 extents;
        public BlitableArray<ModelVertex> vertices;
        public BlitableArray<uint> triangles;
        
		public void Dispose()
		{
			if (vertices.Length > 0)
			{
				vertices.Dispose();
			}
			if (triangles.Length > 0)
			{
				triangles.Dispose();
			}
        }

		public NativeArray<ModelVertex> GetVertexNativeArray()
		{
			return vertices.ToNativeArray(Allocator.None);
		}

		public NativeArray<uint> GetTriangleNativeArray()
		{
			return triangles.ToNativeArray(Allocator.None);
		}
    }
}