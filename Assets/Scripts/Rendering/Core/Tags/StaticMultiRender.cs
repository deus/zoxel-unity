using Unity.Entities;

namespace Zoxel.Rendering
{
    //! A render entity with multiple materials.
    public struct StaticMultiRender : IComponentData { }
}