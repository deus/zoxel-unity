using Unity.Entities;

namespace Zoxel.Rendering
{
    //! A render entity with a non changing transform value.
    public struct StaticRender : IComponentData { }
}