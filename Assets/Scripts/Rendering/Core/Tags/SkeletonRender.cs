using Unity.Entities;

namespace Zoxel.Rendering
{
    //! A render entity with weight data.
    public struct SkeletonRender : IComponentData { }
}