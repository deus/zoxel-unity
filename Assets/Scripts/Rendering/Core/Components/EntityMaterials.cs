using Unity.Entities;
using System;

namespace Zoxel.Rendering
{
    //! Stores materials used for Vox's.
    public struct EntityMaterials : ISharedComponentData, IEquatable<EntityMaterials>
    {
        public UnityEngine.Material[] materials;

        // ModelChunks
        public EntityMaterials(UnityEngine.Material material)
        {
            this.materials = new UnityEngine.Material[] { material };
        }

        // PlanetChunks
        public EntityMaterials(UnityEngine.Material[] materials)
        {
            this.materials = materials;
        }

        public bool Equals(EntityMaterials obj)
        {
            return materials == obj.materials;
        }

        public override int GetHashCode()
        {
            if (materials == null)
            {
                return 0;
            }
            return materials.GetHashCode(); //1371622046 + EqualityComparer<Mesh>.Default.GetHashCode(mesh);
        }
    }
}