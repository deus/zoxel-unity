using Unity.Entities;
using System;
using UnityEngine;

namespace Zoxel.Rendering
{
    //! A Mesh holding component.
    /**
    *   \todo Seperate entity perhaps from ChunkRender's due to ECS chunk layout:
    *   https://docs.unity.cn/Packages/com.unity.entities@0.51/manual/shared_component_data.html#avoid-high-proportions-of-unique-shared-component-values
    */
    public struct ZoxMesh : ISharedComponentData, IEquatable<ZoxMesh>
    {
        public byte layer;
        public Mesh mesh;
        public Material material;
        
        public bool Equals(ZoxMesh other)
        {
            return mesh == other.mesh && material == other.material;
        }
        
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = layer.GetHashCode();
                if (!ReferenceEquals(mesh, null))
                    hash ^= mesh.GetHashCode();
                if (!ReferenceEquals(material, null))
                    hash ^= material.GetHashCode();
                return hash;
            }
        }
    }
}