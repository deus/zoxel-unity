using Unity.Entities;
using System;

namespace Zoxel.Rendering
{
    //! Stores the AddColor's ComputeBuffer used to link to the shaders memory.
    public struct AddColorBuffer : ISharedComponentData, IEquatable<AddColorBuffer>
    {
        public UnityEngine.ComputeBuffer buffer;

        public AddColorBuffer(UnityEngine.ComputeBuffer buffer)
        {
            this.buffer = buffer;
        }

        public bool Equals(AddColorBuffer other)
        {
            return buffer == other.buffer;
        }
        
        public override int GetHashCode()
        {
            return buffer.GetHashCode();
        }

        public void Dispose()
        {
            buffer.Release();
        }
    }
}