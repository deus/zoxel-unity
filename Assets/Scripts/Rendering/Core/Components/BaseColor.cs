using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Rendering
{
	//! A base color before lighting is applied.
	public struct BaseColor : IComponentData
	{
		public float4 color;
	}
}