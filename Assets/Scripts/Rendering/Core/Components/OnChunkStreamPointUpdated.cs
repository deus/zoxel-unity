using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Rendering
{
    public struct OnChunkStreamPointUpdated : IComponentData
    {
        public float3 worldPosition;
        
        public OnChunkStreamPointUpdated(float3 worldPosition)
        {
            this.worldPosition = worldPosition;
        }
    }
}