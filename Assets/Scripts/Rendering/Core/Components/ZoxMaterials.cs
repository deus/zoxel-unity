using Unity.Entities;
using System;
using UnityEngine;

namespace Zoxel.Rendering
{
    //! Holds an array of materials at different divisions.
    public struct ZoxMaterials : ISharedComponentData, IEquatable<ZoxMaterials>
    {
        public Material[] materials;

        public ZoxMaterials(Material[] materials)
        {
            this.materials = materials;
        }
        
        public bool Equals(ZoxMaterials other)
        {
            if (materials == null && other.materials == null)
            {
                return true;
            }
            else if ((materials == null && other.materials != null)
                || (materials != null && other.materials == null))
            {
                return false;
            }
            else if (materials.Length != other.materials.Length)
            {
                return false;
            }
            // if both are not null and same length
            for (int i = 0; i < materials.Length; i++)
            {
                if (materials[i] != other.materials[i])
                {
                    return false;
                }
            }
            return true;
        }
        
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                if (materials == null)
                {
                    return 0;
                }
                int hash = 0;
                for (int i = 0; i < materials.Length; i++)
                {
                    if (!ReferenceEquals(materials[i], null))
                        hash ^= materials[i].GetHashCode();
                }
                return hash;
            }
        }
    }
}