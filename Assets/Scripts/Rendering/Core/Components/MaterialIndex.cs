using Unity.Entities;

namespace Zoxel.Rendering
{
    public struct MaterialIndex : IComponentData
    {
        public byte index;
        
        public MaterialIndex(byte index)
        {
            this.index = index;
        }
    }
}