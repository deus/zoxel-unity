using Unity.Entities;

namespace Zoxel.Rendering
{
    public struct MaterialFader : IComponentData
    {
        public double delay;
        public float fadeInTime;

        public MaterialFader(double delay, float fadeInTime)
        {
            this.delay = delay;
            this.fadeInTime = fadeInTime;
        }
    }
}