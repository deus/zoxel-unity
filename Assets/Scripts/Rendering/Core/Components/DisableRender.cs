using Unity.Entities;

namespace Zoxel.Rendering
{
    //! Disables a render from rendering.
    public struct DisableRender : IComponentData { }
}