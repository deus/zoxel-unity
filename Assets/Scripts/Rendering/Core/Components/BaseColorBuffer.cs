using Unity.Entities;
using System;
using UnityEngine;

namespace Zoxel.Rendering
{
    //! Stores the BaseColor's ComputeBuffer used to link to the shaders memory.
    public struct BaseColorBuffer : ISharedComponentData, IEquatable<BaseColorBuffer>
    {
        public ComputeBuffer buffer;

        public BaseColorBuffer(ComputeBuffer buffer)
        {
            this.buffer = buffer;
        }

        public bool Equals(BaseColorBuffer other)
        {
            return buffer == other.buffer;
        }
        
        public override int GetHashCode()
        {
            if (buffer == null)
            {
                return 0;
            }
            return buffer.GetHashCode();
        }

        public void Dispose()
        {
            buffer.Release();
        }
    }
}