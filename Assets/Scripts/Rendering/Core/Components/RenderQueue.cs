using Unity.Entities;

namespace Zoxel.Rendering
{
    //! This corresponds to material render queue. Used in UI.
    public struct RenderQueue : IComponentData
    {
        public int layer;
        
        public RenderQueue(int layer)
        {
            this.layer = layer;
        }
    }
}