using Unity.Entities;

namespace Zoxel.Rendering
{
    public struct DontDestroyMesh : IComponentData { }
}