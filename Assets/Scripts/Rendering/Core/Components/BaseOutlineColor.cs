using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Rendering
{
	//! A base outline color before lighting is applied.
	public struct BaseOutlineColor : IComponentData
	{
		public float4 outlineColor;
	}
}