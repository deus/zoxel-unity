using Unity.Entities;
using System;
using UnityEngine;

namespace Zoxel.Rendering
{
    //! A Material holding component.
    public struct ZoxMaterial : ISharedComponentData, IEquatable<ZoxMaterial>
    {
        public Material material;

        public bool Equals(ZoxMaterial other)
        {
            return material == other.material;
        }
        
        public override int GetHashCode()
        {
            if (material == null)
            {
                return 0;
            }
            return material.GetHashCode();
        }
    }
}