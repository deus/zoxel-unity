using Unity.Entities;

namespace Zoxel.Rendering
{
    //! Which LayerMask is used to render the obbject.
    public struct RenderLayer : IComponentData
    {
        public byte layer;
        
        public RenderLayer(byte layer)
        {
            this.layer = layer;
        }
    }
}