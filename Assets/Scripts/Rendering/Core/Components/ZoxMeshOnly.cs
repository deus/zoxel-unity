using Unity.Entities;
using System;
using UnityEngine;

namespace Zoxel.Rendering
{
    //! A Mesh holding component.
    public struct ZoxMeshOnly : ISharedComponentData, IEquatable<ZoxMeshOnly>
    {
        public Mesh mesh;

        public ZoxMeshOnly(Mesh mesh)
        {
            this.mesh = mesh;
        }
        
        public bool Equals(ZoxMeshOnly other)
        {
            return mesh == other.mesh;
        }
        
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 0;
                if (!ReferenceEquals(mesh, null))
                    hash ^= mesh.GetHashCode();
                return hash;
            }
        }
    }
}