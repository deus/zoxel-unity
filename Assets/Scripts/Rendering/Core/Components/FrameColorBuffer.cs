using Unity.Entities;
using System;
using UnityEngine;

namespace Zoxel.Rendering
{
    //! Stores a compute buffer used for frame colors.
    public struct FrameColorBuffer : ISharedComponentData, IEquatable<FrameColorBuffer>
    {
        public ComputeBuffer buffer;

        public FrameColorBuffer(ComputeBuffer buffer)
        {
            this.buffer = buffer;
        }

        public bool Equals(FrameColorBuffer other)
        {
            return buffer == other.buffer;
        }
        
        public override int GetHashCode()
        {
            if (buffer == null)
            {
                return 0;
            }
            return buffer.GetHashCode();
        }

        public void Dispose()
        {
            buffer.Release();
        }
    }
}