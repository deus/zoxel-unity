using Unity.Entities;

namespace Zoxel.Rendering
{
    public struct MaterialFadeIn : IComponentData
    {
        public double delay;
        public float fadeInTime;
        public byte started;
        public double timeStarted;
        public double timePassed;

        public MaterialFadeIn(double delay, float fadeInTime) // double timeStarted, 
        {
            this.delay = delay;
            this.fadeInTime = fadeInTime;
            this.started = 0;
            this.timePassed = 0;
            this.timeStarted = 0;
        }
    }
}