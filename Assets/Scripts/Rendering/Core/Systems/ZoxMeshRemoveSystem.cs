using Unity.Entities;

namespace Zoxel.Rendering
{
    //! Clears the ZoxMesh on an entity.
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class ZoxMeshRemoveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        protected override void OnUpdate()
		{
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<RemoveZoxMesh>(processQuery);
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<RemoveZoxMesh>()
                .ForEach((in ZoxMesh zoxMesh) =>
            {
                zoxMesh.mesh.Clear();
            }).WithoutBurst().Run();
        }
    }
}