using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Rendering
{
    //! Disposes the TerrainMesh vertex data after pushed to the GPU.
    /**
    *   - End System -
    *   \todo Maybe Dispose EditableMesh datas after uploaded?
    */
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class MeshDirtyEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<MeshDirty>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
		{
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<MeshDirty>(processQuery);
        }
    }
}