using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Rendering
{
    //! updates AddColor material property.
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class MaterialAddColorSystem : SystemBase
    {
        const string colorName = "additionColor";
        
        protected override void OnUpdate()
        {
            Entities
                .WithNone<DestroyEntity>()
                .WithChangeFilter<MaterialAddColor>()
                .ForEach((in MaterialAddColor materialAddColor, in ZoxMesh zoxMesh) =>
            {
                UpdateColor(in materialAddColor, in zoxMesh.material);
            }).WithoutBurst().Run();
            Entities
                .WithNone<DestroyEntity>()
                .WithChangeFilter<ZoxMesh>()
                .ForEach((in MaterialAddColor materialAddColor, in ZoxMesh zoxMesh) =>
            {
                UpdateColor(in materialAddColor, in zoxMesh.material);
            }).WithoutBurst().Run();
        }

        private void UpdateColor(in MaterialAddColor materialAddColor, in UnityEngine.Material material)
        {
            if (material != null)
            {
                material.SetVector(colorName, new UnityEngine.Vector4(
                    materialAddColor.Value.x, materialAddColor.Value.y,
                    materialAddColor.Value.z, materialAddColor.Value.w));
            }
        }
    }
}