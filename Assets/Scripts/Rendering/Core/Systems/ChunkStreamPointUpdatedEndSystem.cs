using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Rendering
{
    //! Removes OnChunkStreamPointUpdated event.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class ChunkStreamPointUpdatedEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<OnChunkStreamPointUpdated>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<OnChunkStreamPointUpdated>(processQuery);
        }
    }
}