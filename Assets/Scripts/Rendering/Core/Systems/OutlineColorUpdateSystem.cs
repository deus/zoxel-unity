using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Rendering
{
    //! Uploads UIElement's OutlineColor's to the GPU.
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class OutlineColorUpdateSystem : SystemBase
    {
        const string colorName = "outlineColor";

        protected override void OnUpdate()
        {
            Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithChangeFilter<OutlineColor>()
                .ForEach((in OutlineColor outlineColor, in ZoxMesh zoxMesh) =>
            {
                UpdateColor(in outlineColor, in zoxMesh.material);
            }).WithoutBurst().Run();
            Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithChangeFilter<ZoxMesh>()
                .ForEach((in OutlineColor outlineColor, in ZoxMesh zoxMesh) =>
            {
                UpdateColor(in outlineColor, in zoxMesh.material);
            }).WithoutBurst().Run();
        }

        private void UpdateColor(in OutlineColor outlineColor, in UnityEngine.Material material)
        {
            if (material != null)
            {
                material.SetVector(colorName, new UnityEngine.Vector3(
                    outlineColor.color.x, outlineColor.color.y, outlineColor.color.z));
            }
        }
    }
}