using Unity.Entities;

namespace Zoxel.Rendering
{
    //! Updates UnityMaterial with render queue.
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class RenderQueueSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            Entities
                .WithChangeFilter<RenderQueue>()
                .WithNone<InitializeEntity>()
                .ForEach((in RenderQueue renderQueue, in ZoxMesh zoxMesh) =>
            {
                UpdateRenderQueue(zoxMesh.material, renderQueue.layer);
            }).WithoutBurst().Run();
            Entities
                .WithChangeFilter<ZoxMesh>()
                .WithNone<InitializeEntity>()
                .ForEach((in RenderQueue renderQueue, in ZoxMesh zoxMesh) =>
            {
                UpdateRenderQueue(zoxMesh.material, renderQueue.layer);
            }).WithoutBurst().Run();
        }

        private void UpdateRenderQueue(UnityEngine.Material material, int renderQueue)
        {
            if (material != null)
            {
                material.renderQueue = renderQueue;
            }
        }
    }
}