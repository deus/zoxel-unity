using Unity.Entities;

namespace Zoxel.Rendering
{
    //! Uploads UIElement's MaterialFrameColor's to the GPU.
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class MaterialFrameColorSystem : SystemBase
    {
        const string colorName = "frameColor";

        protected override void OnUpdate()
        {
            Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithChangeFilter<MaterialFrameColor>()
                .ForEach((in ZoxMesh zoxMesh, in MaterialFrameColor materialBaseColor) =>
            {
                UpdateColor(in zoxMesh.material, in materialBaseColor);
            }).WithoutBurst().Run();
            Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithChangeFilter<ZoxMesh>()
                .ForEach((in ZoxMesh zoxMesh, in MaterialFrameColor materialBaseColor) =>
            {
                UpdateColor(in zoxMesh.material, in materialBaseColor);
            }).WithoutBurst().Run();
        }

        private void UpdateColor(in UnityEngine.Material material, in MaterialFrameColor materialBaseColor)
        {
            if (material != null)
            {
                material.SetVector(colorName, new UnityEngine.Vector4(
                    materialBaseColor.Value.x, materialBaseColor.Value.y,
                    materialBaseColor.Value.z, materialBaseColor.Value.w));
            }
        }
    }
}