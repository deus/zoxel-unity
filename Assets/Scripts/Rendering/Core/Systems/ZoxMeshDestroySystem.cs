using Unity.Entities;

namespace Zoxel.Rendering
{
    //! Cleans up ZoxMesh when destroyed.
    [UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ZoxMeshDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

		protected override void OnUpdate()
		{
			Entities
                .WithNone<DontDestroyTexture>()
                .WithAll<DestroyEntity, ZoxMesh>()
                .ForEach((in ZoxMesh zoxMesh) =>
			{
                if (zoxMesh.material)
                {
                    var texture = zoxMesh.material.GetTexture("_BaseMap");
                    if (texture != null)
                    {
                        ObjectUtil.Destroy(texture);
                    }
                }
			}).WithoutBurst().Run();
			Entities
                .WithNone<DontDestroyMaterial>()
                .WithAll<DestroyEntity, ZoxMesh>()
                .ForEach((in ZoxMesh zoxMesh) =>
			{
                if (zoxMesh.material)
                {
                    ObjectUtil.Destroy(zoxMesh.material);
                }
			}).WithoutBurst().Run();
			Entities
                .WithNone<DontDestroyMesh>()
                .WithAll<DestroyEntity, ZoxMesh>()
                .ForEach((in ZoxMesh zoxMesh) =>
			{
                if (zoxMesh.mesh)
                {
                    ZoxelSystemStats.totalVertices -= zoxMesh.mesh.vertexCount;
                    ZoxelSystemStats.totalTriangles -= zoxMesh.mesh.triangles.Length;
                    ZoxelSystemStats.totalMapTriangles -= zoxMesh.mesh.triangles.Length;
                    ObjectUtil.Destroy(zoxMesh.mesh);
                }
			}).WithoutBurst().Run();
		}
	}
}