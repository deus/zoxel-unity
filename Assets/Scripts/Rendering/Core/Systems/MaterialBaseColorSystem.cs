using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Rendering
{
    //! Uploads UIElement's MaterialBaseColor's to the GPU.
    /**
    *   This ZoxMesh gets set in InitializeEntity.
    */
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class MaterialBaseColorSystem : SystemBase
    {
        const string colorName = "color";

        protected override void OnUpdate()
        {
            Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithChangeFilter<MaterialBaseColor>()
                .ForEach((in ZoxMesh zoxMesh, in MaterialBaseColor materialBaseColor) =>
            {
                UpdateColor(in zoxMesh.material, in materialBaseColor);
            }).WithoutBurst().Run();
            Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithChangeFilter<ZoxMesh>()
                .ForEach((in ZoxMesh zoxMesh, in MaterialBaseColor materialBaseColor) =>
            {
                UpdateColor(in zoxMesh.material, in materialBaseColor);
            }).WithoutBurst().Run();
        }

        private void UpdateColor(in UnityEngine.Material material, in MaterialBaseColor materialBaseColor)
        {
            if (material != null)
            {
                material.SetVector(colorName, new UnityEngine.Vector4(
                    materialBaseColor.Value.x, materialBaseColor.Value.y,
                    materialBaseColor.Value.z, materialBaseColor.Value.w));
            }
        }
    }
}