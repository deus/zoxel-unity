using Unity.Mathematics;
using System.Runtime.InteropServices;

namespace Zoxel.Rendering
{
    [StructLayout(LayoutKind.Sequential)]
	public struct ZoxColorVertex
	{
		public float3 position;
		public float3 color;
	}
}