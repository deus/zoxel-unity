using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;

namespace Zoxel.Rendering
{
    //! Starts FadeOut by passing along fading events to children.
    [BurstCompile, UpdateInGroup(typeof(FadeSystemGroup))]
    public partial class FadeOutStartSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<FadeOut>(processQuery);
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<FadeDelay>()
                .WithNone<InitializeEntity, DestroyEntity, DisableFader>()
                .WithAll<FadeOut>()
                .ForEach((Entity e, int entityInQueryIndex, in Fader fader) =>
            {
                if (!HasComponent<FadingOut>(e))
                {
                    if (HasComponent<FaderVisible>(e))
                    {
                        PostUpdateCommands.RemoveComponent<FaderVisible>(entityInQueryIndex, e);
                    }
                    PostUpdateCommands.AddComponent<FaderInvisible>(entityInQueryIndex, e);
                    // timenow - timeleft
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new FadingOut(elapsedTime - (fader.fadeTime - fader.timePassed)));
                    if (HasComponent<FadingIn>(e))
                    {
                        PostUpdateCommands.RemoveComponent<FadingIn>(entityInQueryIndex, e);
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}