using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;


namespace Zoxel.Rendering
{
    //! Sets the alpha on a fader component by lerping over time.
    [BurstCompile, UpdateInGroup(typeof(FadeSystemGroup))]
    public partial class FadingOutSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            // var halfPI = math.PI / 2f;
            // var fadeOutLerp = new float2(1f, 0);
            // var fadeInLerp = new float2(0, 1f);
            // Need to do a check for timing with taking damage
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DisableFader>()
                .ForEach((Entity e,  int entityInQueryIndex, ref Fader fader, in FadingOut fadingOut) =>
            {
                fader.timePassed = (elapsedTime - fadingOut.timeStarted);
                if (fader.timePassed < fader.fadeTime)
                {
                    var lerpTime =  (float) (fader.timePassed / fader.fadeTime);
                    //lerpTime = (float) (math.sin(halfPI * lerpTime));
                    fader.alpha = math.lerp(fader.maxAlpha, fader.lowAlpha, lerpTime);
                }
                // Finished Fading
                else // if (timePassed >= fader.fadeTime)
                {
                    fader.timePassed = fader.fadeTime;
                    fader.alpha = 0;
                    if (HasComponent<ReverseFade>(e))
                    {
                        PostUpdateCommands.AddComponent<ReverseFade>(entityInQueryIndex, e, new ReverseFade(1, elapsedTime));
                    }
                    PostUpdateCommands.RemoveComponent<FadingOut>(entityInQueryIndex, e);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}