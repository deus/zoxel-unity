using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Rendering
{
    //! Starts FadeIn by passing along fading events to children.
    [BurstCompile, UpdateInGroup(typeof(FadeSystemGroup))]
    public partial class FadeDelaySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities
                .WithNone<InitializeEntity, DestroyEntity, DisableFader>()
                .ForEach((Entity e, int entityInQueryIndex, in FadeDelay fadeDelay) =>
            {
                if (elapsedTime - fadeDelay.timeStarted >= fadeDelay.timeDelay)
                {
                    PostUpdateCommands.RemoveComponent<FadeDelay>(entityInQueryIndex, e);
                }
			}).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}