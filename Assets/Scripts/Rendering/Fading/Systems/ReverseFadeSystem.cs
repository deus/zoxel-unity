using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;

namespace Zoxel.Rendering
{
    //! Reverses a fader, used for ActionLabel.
    [BurstCompile, UpdateInGroup(typeof(FadeSystemGroup))]
    public partial class ReverseFadeUISystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<FadeIn, FadeOut>()
                .WithNone<DisableFader>()
                .ForEach((Entity e, int entityInQueryIndex, in Fader fader, in ReverseFader reverseFader, in ReverseFade reverseFade) =>
            {
                if (reverseFade.init == 1 && elapsedTime - reverseFade.timeStarted >= reverseFader.reverseAfterDelay)
                {
                    if (HasComponent<FaderVisible>(e)) // fader.visible == 1)
                    {
                        PostUpdateCommands.AddComponent<FadeOut>(entityInQueryIndex, e);
                    }
                    else
                    {
                        PostUpdateCommands.AddComponent<FadeIn>(entityInQueryIndex, e);
                    }
                    PostUpdateCommands.RemoveComponent<ReverseFade>(entityInQueryIndex, e);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // If Fading out, stop and fade in
            // reset time if time started, during reverse fades delay
            Dependency = Entities
                .WithNone<FadeIn, FadeOut, ReverseFade>()
                .WithNone<FadingIn>()
                .WithNone<DisableFader>()
                .WithAll<FaderStayVisible>()
                .ForEach((Entity e,  int entityInQueryIndex) =>
            {
                if (HasComponent<FadingOut>(e))
                {
                    PostUpdateCommands.RemoveComponent<FadingOut>(entityInQueryIndex, e);
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ReverseFade(0, elapsedTime));    // fade in
                PostUpdateCommands.AddComponent<FadeIn>(entityInQueryIndex, e);    // fade in
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<FadeIn, FadeOut>()
                .WithNone<DisableFader>()
                .WithAll<FaderStayVisible>()
                .ForEach((Entity e,  int entityInQueryIndex, ref ReverseFade reverseFade) =>
            {
                if (reverseFade.init == 1)
                {
                    reverseFade.timeStarted = elapsedTime;
                }
			}).ScheduleParallel(Dependency);
            Dependency = Entities
                .WithNone<FadeIn, FadeOut>()
                .WithNone<DisableFader>()
                .WithAll<FaderStayVisible>()
                .ForEach((Entity e,  int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<FaderStayVisible>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

            
		}
	}
}