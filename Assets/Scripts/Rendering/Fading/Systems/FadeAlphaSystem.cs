using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;

namespace Zoxel.Rendering
{
    //! Sets the alpha on a fader component by lerping over time.
    [BurstCompile, UpdateInGroup(typeof(FadeSystemGroup))]
    public partial class FadeAlphaSystem : SystemBase
	{
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<DisableFader>()
                .WithChangeFilter<Fader>()
                .ForEach((ref MaterialBaseColor materialBaseColor, in Fader fader) =>
            {
                materialBaseColor.Value.w = fader.alpha;
			}).ScheduleParallel();
            Entities
                .WithNone<DisableFader>()
                .WithChangeFilter<Fader>()
                .ForEach((ref MaterialFrameColor materialFrameColor, in Fader fader) =>
            {
                materialFrameColor.Value.w = fader.alpha;
			}).ScheduleParallel();
		}
	}
}