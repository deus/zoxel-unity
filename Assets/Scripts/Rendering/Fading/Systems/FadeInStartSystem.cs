using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;

namespace Zoxel.Rendering
{
    //! Starts FadeIn by passing along fading events to children.
    [BurstCompile, UpdateInGroup(typeof(FadeSystemGroup))]
    public partial class FadeInStartSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<FadeDelay>()
                .WithNone<InitializeEntity, DestroyEntity, DisableFader>()
                .WithAll<FadeIn>()
                .ForEach((Entity e,  int entityInQueryIndex, in Fader fader) =>
            {
                if (!HasComponent<FadingIn>(e))
                {
                    if (HasComponent<FaderInvisible>(e))
                    {
                        PostUpdateCommands.RemoveComponent<FaderInvisible>(entityInQueryIndex, e);
                    }
                    PostUpdateCommands.AddComponent<FaderVisible>(entityInQueryIndex, e);
                    // timenow - timeleft
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new FadingIn(elapsedTime - (fader.fadeTime - fader.timePassed)));
                    if (HasComponent<FadingOut>(e))
                    {
                        PostUpdateCommands.RemoveComponent<FadingOut>(entityInQueryIndex, e);
                    }
                }
                PostUpdateCommands.RemoveComponent<FadeIn>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}