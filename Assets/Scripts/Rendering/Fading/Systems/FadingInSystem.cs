using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;


namespace Zoxel.Rendering
{
    //! Sets the alpha on a fader component by lerping over time.
    [BurstCompile, UpdateInGroup(typeof(FadeSystemGroup))]
    public partial class FadingInSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            // var halfPI = math.PI / 2f;
            // Need to do a check for timing with taking damage
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DisableFader>()
                .ForEach((Entity e, int entityInQueryIndex, ref Fader fader, in FadingIn fadingIn) =>
            {
                fader.timePassed = (elapsedTime - fadingIn.timeStarted);
                if (fader.timePassed < fader.fadeTime)
                {
                    var lerpTime = (float)(fader.timePassed / fader.fadeTime);
                    // fader.alpha = fader.maxAlpha * math.lerp(0f, 1f, lerpTime);
                    fader.alpha = math.lerp(fader.lowAlpha, fader.maxAlpha, lerpTime);
                }
                // Finished Fading
                else // if (timePassed >= fader.fadeTime)
                {
                    fader.timePassed = fader.fadeTime;
                    fader.alpha = fader.maxAlpha;
                    if (HasComponent<ReverseFader>(e))
                    {
                        PostUpdateCommands.AddComponent<ReverseFade>(entityInQueryIndex, e, new ReverseFade(1, elapsedTime));
                    }
                    // UnityEngine.Debug.LogError("(1) Removing FadingIn: " + e.Index);
                    PostUpdateCommands.RemoveComponent<FadingIn>(entityInQueryIndex, e);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}