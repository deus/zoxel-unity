using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;

namespace Zoxel.Rendering
{
    //! Starts FadeOut by passing along fading events to children.
    [BurstCompile, UpdateInGroup(typeof(FadeSystemGroup))]
    public partial class ChildrensFadeSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // FadeOut Start
            Entities
                .WithNone<FadeDelay>()
                .WithNone<InitializeEntity, DestroyEntity, DisableFader>()
                .WithAll<FadeOut, Fader>()
                .ForEach((int entityInQueryIndex, in Childrens childrens) =>
            {
                // UnityEngine.Debug.LogError("Children is Fading Out: " + e.Index + "  :: " + childrens.children.Length);
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    var child = childrens.children[i];
                    if (!HasComponent<DestroyEntity>(child) && HasComponent<Fader>(child) && !HasComponent<DisableFader>(child))
                    {
                        PostUpdateCommands.AddComponent<FadeOut>(entityInQueryIndex, child);
                        if (HasComponent<FadingIn>(child))
                        {
                            // UnityEngine.Debug.LogError("(3) Removing FadingIn: " + child.Index);
                            PostUpdateCommands.RemoveComponent<FadingIn>(entityInQueryIndex, child);
                        }
                    }
                }
			}).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // Copy of FadeOut Start
            Entities
                .WithNone<FadeDelay>()
                .WithNone<InitializeEntity, DestroyEntity, DisableFader>()
                .WithAll<FadeIn, Fader>()
                .ForEach((Entity e, int entityInQueryIndex, in Childrens childrens) =>
            {
                // UnityEngine.Debug.LogError("Children is Fading Out: " + e.Index + "  :: " + childrens.children.Length);
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    var child = childrens.children[i];
                    if (!HasComponent<DestroyEntity>(child) && HasComponent<Fader>(child) && !HasComponent<DisableFader>(child))
                    {
                        PostUpdateCommands.AddComponent<FadeIn>(entityInQueryIndex, child);
                        if (HasComponent<FadingOut>(child))
                        {
                            PostUpdateCommands.RemoveComponent<FadingOut>(entityInQueryIndex, child);
                        }
                    }
                }
			}).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}