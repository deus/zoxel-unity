using Unity.Entities;

namespace Zoxel.Rendering
{
    //! Handles simple material fading.
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class FadeSystemGroup : ComponentSystemGroup { }
}