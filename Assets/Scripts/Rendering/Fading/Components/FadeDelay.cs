using Unity.Entities;

namespace Zoxel.Rendering
{
    //! Delays fadin for a certain time.
    public struct FadeDelay : IComponentData
    {
        public double timeStarted;
        public double timeDelay;

        public FadeDelay(double timeStarted, float timeDelay)
        {
            this.timeStarted = timeStarted;
            this.timeDelay = timeDelay;
        }
    }
}