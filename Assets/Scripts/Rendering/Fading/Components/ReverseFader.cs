using Unity.Entities;

namespace Zoxel.Rendering
{
    //! After a time delay it reverses the animation of fading.
    public struct ReverseFader : IComponentData
    {
        public double reverseAfterDelay;

        public ReverseFader(double reverseAfterDelay)
        {
            this.reverseAfterDelay = reverseAfterDelay;
        }
    }
}