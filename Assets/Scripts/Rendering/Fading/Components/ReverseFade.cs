using Unity.Entities;

namespace Zoxel.Rendering
{
    public struct ReverseFade : IComponentData
    {
        public byte init;
        public double timeStarted;

        public ReverseFade(byte init, double timeStarted)
        {
            this.init = init;
            this.timeStarted = timeStarted;
        }
    }
}