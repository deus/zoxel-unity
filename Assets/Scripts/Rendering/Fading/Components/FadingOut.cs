using Unity.Entities;

namespace Zoxel.Rendering
{
    public struct FadingOut : IComponentData
    {
        public double timeStarted;

        public FadingOut(double timeStarted)
        {
            this.timeStarted = timeStarted;
        }
    }
}