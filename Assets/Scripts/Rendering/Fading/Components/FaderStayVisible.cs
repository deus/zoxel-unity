using Unity.Entities;

namespace Zoxel.Rendering
{
    public struct FaderStayVisible : IComponentData
    {
        public double timeStarted;

        public FaderStayVisible(double timeStarted)
        {
            this.timeStarted = timeStarted;
        }
    }
}