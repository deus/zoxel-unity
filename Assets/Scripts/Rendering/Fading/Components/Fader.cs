using Unity.Entities;

namespace Zoxel.Rendering
{
    //! Contains fade data for a Entity.
    public struct Fader : IComponentData
    {
        //! This is the total alpha value. Move this to its own component data.
        public float alpha;
        public float lowAlpha;         // rename to max alpha
        public float maxAlpha;         // rename to max alpha
        public double fadeTime;
        public double timePassed;

        public Fader(float alpha, float lowAlpha, float maxAlpha, double fadeTime)
        {
            this.alpha = alpha;
            this.lowAlpha = lowAlpha;
            this.maxAlpha = maxAlpha;
            this.fadeTime = fadeTime;
            this.timePassed = fadeTime;
        }

        public Fader(float alpha, float maxAlpha, double fadeTime)
        {
            this.alpha = alpha;
            this.lowAlpha = alpha;
            this.maxAlpha = maxAlpha;
            this.fadeTime = fadeTime;
            this.timePassed = fadeTime;
        }
    }
}