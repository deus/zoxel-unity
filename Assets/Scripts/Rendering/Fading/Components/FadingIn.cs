using Unity.Entities;

namespace Zoxel.Rendering
{
    public struct FadingIn : IComponentData
    {
        public double timeStarted;

        public FadingIn(double timeStarted)
        {
            this.timeStarted = timeStarted;
        }
    }
}