using Unity.Entities;
using Zoxel.Transforms;
using Zoxel.Cameras;
using Zoxel.Rendering.Authoring;

namespace Zoxel.Rendering.Renderer
{
    //! Renders minivoxes.
    /**
    *   - Render System -
    */
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public partial class StaticRenderSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var isShadows = false;
            var isRenderToAllCameras = true;
            if (HasSingleton<RenderSettings>())
            {
                var renderSettings = GetSingleton<RenderSettings>();
                isShadows = renderSettings.isShadows;
                isRenderToAllCameras = renderSettings.isRenderToAllCameras;
            }
            UnityEngine.Camera renderCamera = null;
            if (!isRenderToAllCameras)
            {
                renderCamera = CameraReferences.GetMainCamera(EntityManager);
            }
            Entities
                .WithNone<InitializeEntity, DestroyEntity, DisableRender>()
                .WithAll<StaticRender>()
                .ForEach((Entity e, in ZoxMesh zoxMesh, in ZoxMatrix zoxMatrix) =>
            {
                UnityEngine.Graphics.DrawMesh(zoxMesh.mesh, zoxMatrix.matrix, zoxMesh.material, 0,
                    renderCamera, 0, null, isShadows, isShadows);
            }).WithoutBurst().Run();
        }
    }
}

// no matrix - that's set in init
/*UnityEngine.Graphics.DrawMeshInstancedIndirect(
    zoxMesh.mesh,
    0,                      // sub mesh inex
    zoxMesh.material,
    modelChunkRender.bounds);*/