using Unity.Entities;
using Zoxel.Transforms;
using Zoxel.Cameras;
using Zoxel.Rendering.Authoring;

namespace Zoxel.Rendering.Renderer
{
    // todo: use attribute to make this work in editor
    /**
    *   - Render System -
    *   This renders multiple materials per mesh using an index.
    *   \todo Render call for the player cameras, portal cameras, but not the viewer cameras!
    *   ? submesh index in intermediate renderer is out of bounds
    */
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public partial class StaticMultiRenderSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var isShadows = false;
            var isRenderToAllCameras = true;
            if (HasSingleton<RenderSettings>())
            {
                var renderSettings = GetSingleton<RenderSettings>();
                isShadows = renderSettings.isShadows;
                isRenderToAllCameras = renderSettings.isRenderToAllCameras;
            }
            UnityEngine.Camera renderCamera = null;
            if (!isRenderToAllCameras)
            {
                renderCamera = CameraReferences.GetMainCamera(EntityManager);
            }
            Entities
                .WithNone<InitializeEntity, DestroyEntity, DisableRender>()
                .WithAll<StaticMultiRender>()
                .ForEach((Entity e, in ZoxMatrix zoxMatrix, in ZoxMeshOnly zoxMeshOnly, in ZoxMaterials zoxMaterials, in RenderLayer renderLayer, in MaterialIndex materialIndex) =>
			{
                if (zoxMaterials.materials == null || materialIndex.index >= zoxMaterials.materials.Length)
                {
                    return;
                }
                var material = zoxMaterials.materials[materialIndex.index];
                UnityEngine.Graphics.DrawMesh(zoxMeshOnly.mesh, zoxMatrix.matrix, material, renderLayer.layer,
                    renderCamera, 0, null, isShadows, isShadows);
			}).WithoutBurst().Run();
        }
    }
}