using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering.Authoring;
using Zoxel.Cameras;

namespace Zoxel.Rendering.Renderer
{
    //! Renders model chunk renders.
    /**
    *   - Render System -
    */
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public partial class BasicRenderSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            var isShadows = false;
            var isRenderToAllCameras = true;
            if (HasSingleton<RenderSettings>())
            {
                var renderSettings = GetSingleton<RenderSettings>();
                isShadows = renderSettings.isShadows;
                isRenderToAllCameras = renderSettings.isRenderToAllCameras;
            }
            UnityEngine.Camera renderCamera = null;
            if (!isRenderToAllCameras)
            {
                renderCamera = CameraReferences.GetMainCamera(EntityManager);
            }
            Entities
                .WithNone<DestroyEntity, InitializeEntity, DisableRender>()
                #if DISABLE_COMPUTER_BUFFERS
                .WithNone<SkeletonRender>()
                #endif
                .WithAll<BasicRender>()         
                .ForEach((in LocalToWorld localToWorld, in ZoxMesh zoxMesh) =>
            {
                if (!MathUtil.IsValid(localToWorld.Value))
                {
                    //#if UNITY_EDITOR
                    // UnityEngine.Debug.LogError("Entity not valid: " + e.Index);
                    // UnityEngine.Debug.LogError("!MathUtil.IsValid");
                    //#endif
                    return;
                }
                UnityEngine.Graphics.DrawMesh(zoxMesh.mesh, localToWorld.Value, zoxMesh.material, (int) zoxMesh.layer,
                    renderCamera, 0, null, isShadows, isShadows);
            }).WithoutBurst().Run();
        }
    }
}