using UnityEngine;

namespace Zoxel.Rendering
{
    //! Stores the games materials.
    [CreateAssetMenu(fileName = "Materials", menuName = "Zoxel/Settings/Materials")]
    public partial class MaterialsDatam : ScriptableObject
    {
        [Header("Characters")]
        public UnityEngine.Material characterMaterial;
        public UnityEngine.Material characterAnimatedMaterial;
        public UnityEngine.Material characterTransparentMaterial;
        public UnityEngine.Material characterAnimatedTransparentMaterial;

        [Header("Bullets")]
        public UnityEngine.Material bulletMaterial;

        [Header("Items")]
        public UnityEngine.Material itemMaterial;
        public UnityEngine.Material voxelItemMaterial;

        [Header("Voxel Interaction")]
        public UnityEngine.Material voxelDamageMaterial;

        [Header("Gizmos")]
        public UnityEngine.Material voxelGizmoMaterial;
        public UnityEngine.Material lineMaterial;
        public UnityEngine.Material lineMaterial2;

        [Header("Weather")]
        public UnityEngine.Material skyMaterial;
        public UnityEngine.Material cloudMaterial;

        [Header("Other")]
        public UnityEngine.Material particlesMaterial;
        public UnityEngine.Material portalMaterial;
        public UnityEngine.Material outlineMaterial;

        [Header("2D")]
        public UnityEngine.Material tilesMaterial;
        public UnityEngine.Material playerCharacter2DMaterial;
        public UnityEngine.Material[] character2DMaterials;
    }
}