using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Zoxel.Rendering.Authoring
{
    //! Settings for our render systems.
    /**
    *   \todo Set Voxel Scale (to 0.66 instead of 1 per voxel)
    *   \todo Set Minivox Resolution - to 32 or something instead of 16x16x16 - changes scale and size
    */
    // [GenerateAuthoringComponent]
    public struct RenderSettings : IComponentData
    {
        public byte lightingRenderDistance;      // 3
        public byte renderDistance;              // 8
        public byte lowerMeshResolutionDistance; // 6
        public byte invisibleRenderDistance;     // 11
        public int megaChunkDivision;           // 32
        public float mapVoxScale;               // 1
        public float2 flashTime;                        // 0.3, 0.5
        public Color flashColor;                        // 118, 3, 92, 255
        public int voxelDestructionTextureResolution;   // 32
        public bool isVSynch;                                       // false
        public bool isRenderToAllCameras;                           // true
        public bool isShadows;                                      // false
        public UnityEngine.Rendering.ShadowCastingMode shadowMode;  // off
        public bool disableParticles;
        public bool disableCulling;
        public bool disableCharacterRenderCulling;
    }

    public class RenderSettingsAuthoring : MonoBehaviour
    {
        public byte lightingRenderDistance;      // 3
        public byte renderDistance;              // 8
        public byte lowerMeshResolutionDistance; // 6
        public byte invisibleRenderDistance;     // 11
        public int megaChunkDivision;           // 32
        public float mapVoxScale;               // 1
        public float2 flashTime;                        // 0.3, 0.5
        public Color flashColor;                        // 118, 3, 92, 255
        public int voxelDestructionTextureResolution;   // 32
        public bool isVSynch;                                       // false
        public bool isRenderToAllCameras;                           // true
        public bool isShadows;                                      // false
        public UnityEngine.Rendering.ShadowCastingMode shadowMode;  // off
        public bool disableParticles;
        public bool disableCulling;
        public bool disableCharacterRenderCulling;
    }

    public class RenderSettingsAuthoringBaker : Baker<RenderSettingsAuthoring>
    {
        public override void Bake(RenderSettingsAuthoring authoring)
        {
            AddComponent(new RenderSettings
            {
                lightingRenderDistance = authoring.lightingRenderDistance,
                renderDistance = authoring.renderDistance,
                lowerMeshResolutionDistance = authoring.lowerMeshResolutionDistance,
                invisibleRenderDistance = authoring.invisibleRenderDistance,
                megaChunkDivision = authoring.megaChunkDivision,
                mapVoxScale = authoring.mapVoxScale,
                flashTime = authoring.flashTime,
                flashColor = authoring.flashColor,
                voxelDestructionTextureResolution = authoring.voxelDestructionTextureResolution,
                isVSynch = authoring.isVSynch,
                isRenderToAllCameras = authoring.isRenderToAllCameras,
                isShadows = authoring.isShadows,
                shadowMode = authoring.shadowMode,
                disableParticles = authoring.disableParticles,
                disableCulling = authoring.disableCulling,
                disableCharacterRenderCulling = authoring.disableCharacterRenderCulling
            });       
        }
    }
}