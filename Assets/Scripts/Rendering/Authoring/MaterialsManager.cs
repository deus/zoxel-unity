using UnityEngine;

namespace Zoxel.Rendering
{
    //! Holds authored data for rendering.
    /**
    *   \todo Rename to MaterialsManager
    */
    public class MaterialsManager : MonoBehaviour
    {
        public static MaterialsManager instance;
        [SerializeField] public MaterialsDatam materials;

        public void Awake()
        {
            instance = this;
        }
    }
}