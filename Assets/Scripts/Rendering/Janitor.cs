using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel
{
    ///! Wow this class is old... Like a Janitor!
    public static class Janitor
    {
        // used for updating icons atm
        public static void DestroyTexture(EntityManager EntityManager, Entity e)
        {
            if (EntityManager.HasComponent<ZoxMesh>(e))
            {
                var renderMesh = EntityManager.GetSharedComponentManaged<ZoxMesh>(e);
                if (renderMesh.material != null)
                {
                    var unityTexture = renderMesh.material.GetTexture("_BaseMap");
                    if (unityTexture != null)
                    {
                        ObjectUtil.Destroy(unityTexture);
                    }
                }
            }
        }
    }
}
        /*public static void RemoveZoxMesh(in ZoxMesh zoxMesh, bool isDestroyMaterial, bool isDestroyTexture)
        {
            // Destroy material texture and mesh
            if (zoxMesh.material != null)
            {
                if (isDestroyTexture)
                {
                    var unityTexture = zoxMesh.material.GetTexture("_BaseMap");
                    if (unityTexture != null)
                    {
                        ObjectUtil.Destroy(unityTexture);
                    }
                }
                if (isDestroyMaterial)
                {
                    ObjectUtil.Destroy(zoxMesh.material);
                }
            }
            if (zoxMesh.mesh != null)
            {
                ObjectUtil.Destroy(zoxMesh.mesh);
            }
        }*/
