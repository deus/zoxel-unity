using Unity.Entities;
using Zoxel.Transforms;

namespace Zoxel.Rendering
{
    //! Updates after Transforms, as it sends data to the gpu.
    [UpdateAfter(typeof(SkeletonSystemGroup))]
    public partial class RenderSystemGroup : ComponentSystemGroup { }
}