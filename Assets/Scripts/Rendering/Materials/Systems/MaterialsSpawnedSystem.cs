using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Rendering
{
    //! Links user materials to materialLinks user after they spawn.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(MaterialSystemGroup))]
    public partial class MaterialsSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery materialsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            materialsQuery = GetEntityQuery(ComponentType.ReadOnly<MaterialData>());
            RequireForUpdate(processQuery);
            RequireForUpdate(materialsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var materialEntities = materialsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var creatorLinks = GetComponentLookup<CreatorLink>(true);
            var dataIndexes = GetComponentLookup<UserDataIndex>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref MaterialLinks materialLinks, ref OnMaterialsSpawned onMaterialsSpawned) =>
            {
                if (onMaterialsSpawned.spawned != 255)
                {
                    materialLinks.Initialize(onMaterialsSpawned.spawned);
                    onMaterialsSpawned.spawned = 255;
                }
                var count = 0;
                for (int i = 0; i < materialEntities.Length; i++)
                {
                    var e2 = materialEntities[i];
                    var creator = creatorLinks[e2].creator;
                    if (creator == e)
                    {
                        var index = dataIndexes[e2].index;
                        materialLinks.materials[index] = e2;
                        count++;
                        if (count == materialLinks.materials.Length)
                        {
                            PostUpdateCommands.RemoveComponent<OnMaterialsSpawned>(entityInQueryIndex, e);
                            break;
                        }
                    }
                }
            })  .WithReadOnly(materialEntities)
                .WithDisposeOnCompletion(materialEntities)
                .WithReadOnly(creatorLinks)
                .WithReadOnly(dataIndexes)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}