using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Rendering
{
    //! Cleans up MaterialLinks on DestroyEntity.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class MaterialLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithAll<DestroyEntity, MaterialLinks>()
				.ForEach((int entityInQueryIndex, in MaterialLinks materialLinks) =>
			{
                for (int i = 0; i < materialLinks.materials.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, materialLinks.materials[i]);
                }
                materialLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}