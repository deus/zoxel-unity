using Unity.Entities;

namespace Zoxel.Rendering
{
    //! Updates material systems.
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class MaterialSystemGroup : ComponentSystemGroup { }
}