using Unity.Entities;

namespace Zoxel.Rendering
{
    //! Different types for our materials
    public enum MaterialTypeEditor : byte
    {
        Diffuse,
        Transparent,
        Translucent
    }
	public static class MaterialType
	{
		public const byte Diffuse = 0;
		public const byte Transparent = 1;
		public const byte Translucent = 2;
	}
}