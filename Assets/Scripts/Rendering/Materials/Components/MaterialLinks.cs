using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Rendering
{
    //! Links a Realm to materials.
    public struct MaterialLinks : IComponentData
    {
        public BlitableArray<Entity> materials;

        public void Dispose()
        {
            materials.Dispose();
        }

        public void Initialize(int count)
        {
            Dispose();
            this.materials = new BlitableArray<Entity>(count, Allocator.Persistent);
            for (int i = 0; i < count; i++)
            {
                this.materials[i] = new Entity();
            }
        }
    }

}