using Unity.Entities;

namespace Zoxel.Rendering
{
	//! Material meta data.
    /**
    *   For animated voxel materials, we will use an extra (separate) entity. Textures will be generated based on it. (Water, TeleportCube)
    */
	public struct MaterialData : IComponentData
	{
        //! What kind of culling rules will be used on this material.
        public byte type;
        //! The number of resolutions per material. (MipMapping)
        public byte resolutions;
        //! The highest texture resolution of the textures.
        public byte textureResolution;

        public MaterialData(byte type, byte resolutions, byte textureResolution)
        {
            this.type = (byte) type;
            this.resolutions = resolutions;
            this.textureResolution = textureResolution;
        }
    }
}