/*
using Unity.Entities;
using System;

namespace Zoxel.Voxels
{
    //! Stores materials used for Vox's.
    public struct Material : ISharedComponentData, IEquatable<Material>
    {
        public UnityEngine.Material material;

        public Material(UnityEngine.Material material)
        {
            this.material = material;
        }

        public bool Equals(Material other)
        {
            return material == other.material;
        }

        public override int GetHashCode()
        {
            if (material == null)
            {
                return 0;
            }
            return material.GetHashCode(); //1371622046 + EqualityComparer<Mesh>.Default.GetHashCode(mesh);
        }
    }
}*/