using Unity.Entities;

namespace Zoxel.Rendering
{
    //! An event for linking up Materials after spawning.
    public struct OnMaterialsSpawned : IComponentData
    {
        public byte spawned;

        public OnMaterialsSpawned(byte spawned)
        {
            this.spawned = spawned;
        }
    }
}