

using Unity.Entities;

namespace Zoxel.Rendering
{
    public struct DamageFlashEffect : IComponentData
    {
        public double timeStarted;

        public DamageFlashEffect(double elapsedTime)
        {
            this.timeStarted = elapsedTime;
        }
    }
}