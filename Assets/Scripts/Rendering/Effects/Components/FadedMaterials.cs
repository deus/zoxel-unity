/*using Unity.Entities;
using System;

namespace Zoxel
{
    public struct FadedMaterials : ISharedComponentData, IEquatable<FadedMaterials>
    {
        public UnityEngine.Material opaqueMaterial;
        public UnityEngine.Material transparentMaterial;


        public bool Equals(FadedMaterials other)
        {
            return opaqueMaterial == other.opaqueMaterial && transparentMaterial == other.transparentMaterial;
        }
        
        public override int GetHashCode()
        {
            int hash = 0;
            if (!ReferenceEquals(opaqueMaterial, null))
                hash ^= opaqueMaterial.GetHashCode();
            if (!ReferenceEquals(transparentMaterial, null))
                hash ^= transparentMaterial.GetHashCode();
            return hash;
        }
    }
}*/