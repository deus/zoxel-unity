

using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
// using Unity.Collections;
// using Zoxel.Voxels;
// two systems
// one for movement
// one for spawning - takes a random position on edge of voxel mesh and replaces it with particle spawn - spawn system
// ahh one for fading
// when completes fading in - load opaque material

namespace Zoxel.Rendering
{
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class MaterialFadeInSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var fadedColor = new float4(1, 1, 1, 0);
            var defaultColor = new float4(1, 1, 1, 1);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<InitializeEntity>() // , ChunkRender>()
                .ForEach((Entity e, int entityInQueryIndex, ref MaterialBaseColor materialBaseColor, ref MaterialFadeIn materialFadeIn) =>
            {
                materialFadeIn.timeStarted = elapsedTime + materialFadeIn.delay;
                materialBaseColor.Value = new float4(materialBaseColor.Value.x, materialBaseColor.Value.y, materialBaseColor.Value.z,
                    fadedColor.w);
			}).ScheduleParallel(Dependency);
            
            Dependency = Entities
                .WithNone<EntityBusy>() // ChunkRenderBuilder
                .ForEach((Entity e, int entityInQueryIndex, ref MaterialBaseColor materialBaseColor, ref MaterialFadeIn materialFadeIn) =>
            {
                if (materialFadeIn.started == 0) 
                {
                    materialFadeIn.started = 1;
                    materialFadeIn.timeStarted = elapsedTime + materialFadeIn.delay;
                    //materialBaseColor.Value = fadedColor;
                    materialBaseColor.Value = new float4(materialBaseColor.Value.x, materialBaseColor.Value.y, materialBaseColor.Value.z,
                        fadedColor.w);
                }
                if (materialFadeIn.started == 1)
                {
                    materialFadeIn.timePassed = elapsedTime - materialFadeIn.timeStarted;
                    if (materialFadeIn.timePassed < 0)
                    {
                        materialFadeIn.timePassed = 0;
                    }
                    if (materialFadeIn.timePassed >= materialFadeIn.fadeInTime)
                    {
                        // materialBaseColor.Value = defaultColor;
                        materialBaseColor.Value = new float4(materialBaseColor.Value.x, materialBaseColor.Value.y, materialBaseColor.Value.z,
                            defaultColor.w);
                    }
                    else
                    {
                        var lerpedTime = (float) materialFadeIn.timePassed /  materialFadeIn.fadeInTime;
                        var lerpedValue = math.lerp(fadedColor, defaultColor, lerpedTime);
                        materialBaseColor.Value = new float4(materialBaseColor.Value.x, materialBaseColor.Value.y, materialBaseColor.Value.z,
                            lerpedValue.w);
                    }
                }
                else if (materialFadeIn.started == 2)
                {
                   // materialBaseColor.Value = defaultColor;
                    materialBaseColor.Value = new float4(materialBaseColor.Value.x, materialBaseColor.Value.y, materialBaseColor.Value.z,
                        defaultColor.w);
                    PostUpdateCommands.RemoveComponent<MaterialFadeIn>(entityInQueryIndex, e);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}