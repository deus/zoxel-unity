

using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Rendering.Authoring;

namespace Zoxel.Rendering
{
    //! animates the damage flash on materials.
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class DamagedFlashSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<RenderSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var renderSettings = GetSingleton<RenderSettings>();
            var flashColor2 = renderSettings.flashColor.ToFloat3();
            var flashTime = renderSettings.flashTime;
            var flashColor = new float4(flashColor2.x, flashColor2.y, flashColor2.z, 0);
            var defaultColor = new float4(0, 0, 0, 0);
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref MaterialAddColor materialAddColor, in DamageFlashEffect flashDamageEffect) =>
            {
                var timePassed = elapsedTime - flashDamageEffect.timeStarted;
                // flash has finished
                if (timePassed >= flashTime.x + flashTime.y)
                {
                    materialAddColor.Value = defaultColor;
                    PostUpdateCommands.RemoveComponent<DamageFlashEffect>(entityInQueryIndex, e);
                    //UnityEngine.Debug.LogError("Flash Finished.");
                }
                // flash is fading in
                else if (timePassed < flashTime.x)
                {
                    var lerpedTime = (float) timePassed / flashTime.x;
                    materialAddColor.Value = math.lerp(defaultColor, flashColor, lerpedTime);
                    //UnityEngine.Debug.LogError("Flash Fading In.");
                }
                // flash is fading out
                else if (timePassed >= flashTime.x)
                {
                    var lerpedTime = (float) (timePassed - flashTime.x) / flashTime.y;
                    materialAddColor.Value = math.lerp(flashColor, defaultColor, lerpedTime);
                    //UnityEngine.Debug.LogError("Flash Fading Out.");
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
            //var flashColor = new float4(1,1,1,0);
            //var flashTime = 0.13f;
            //var unflashTime = 0.06f;