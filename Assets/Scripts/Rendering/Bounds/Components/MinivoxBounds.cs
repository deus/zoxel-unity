using Unity.Entities;

namespace Zoxel.Rendering
{
    //! Special bounds calculations for Minivox Entities.
    public struct MinivoxBounds : IComponentData { }
}