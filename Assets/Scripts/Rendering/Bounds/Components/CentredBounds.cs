using Unity.Entities;

namespace Zoxel.Rendering
{
    //! Calculates render bounds for our PlanetRender's.
    public struct CentredBounds : IComponentData { }
}