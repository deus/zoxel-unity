using Unity.Entities;

namespace Zoxel.Rendering
{
    //! Placeholder for positioning.
    /**
    *   Note: Placeholder for positioning.
    */
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class RenderBoundsPostUpdateSystem : SystemBase
    {
        protected override void OnUpdate() { }
    }
}