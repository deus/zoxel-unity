using Unity.Entities;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Zoxel2D
{
    //! Initializes a Editor Mesh.
    /*[BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class EditorMeshInitializationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<InitializeEntity>()
                .WithAll<InitializeEditorMesh, EditorMesh>()
                .ForEach((Entity e, ZoxMesh zoxMesh) =>
            {
                PostUpdateCommands.RemoveComponent<InitializeEditorMesh>(e);
                // If Editor Mesh - Create GameObject here and link to zoxmesh things
                var gameObject = new UnityEngine.GameObject();
                gameObject.name = "Entity [" + e.Index + "]";
                gameObject.tag = "Entity";
                gameObject.AddComponent<UnityEngine.MeshFilter>().mesh = zoxMesh.mesh;
                gameObject.AddComponent<UnityEngine.MeshRenderer>().material = zoxMesh.material;
                PostUpdateCommands.AddharedComponentManaged(e, new GameObjectLink(gameObject));
            }).WithoutBurst().Run();
        }
    }*/
}