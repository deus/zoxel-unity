
using Unity.Entities;

namespace Zoxel.Rendering
{
    //! A mesh rendered using a GO.
    public struct EditorMesh : IComponentData { }
}