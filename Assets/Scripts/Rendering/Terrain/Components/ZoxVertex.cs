using Unity.Mathematics;
using System.Runtime.InteropServices;

namespace Zoxel.Rendering
{
    [StructLayout(LayoutKind.Sequential)]
	public struct ZoxVertex
	{
		public float3 position;
		public float3 color;
		public float2 uv;
	}
}