using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Rendering
{
	//! Cleans up TerrainMesh after destroyed.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class TerrainMeshDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, TerrainMesh>()
                .ForEach((in TerrainMesh terrainMesh) =>
			{
                terrainMesh.Dispose();
			}).ScheduleParallel();
		}
	}
}