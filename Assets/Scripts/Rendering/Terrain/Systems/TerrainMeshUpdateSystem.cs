﻿using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.Rendering;

namespace Zoxel.Rendering
{
    //! Converts component mesh data into a unity mesh data for gpu use.
    /**
    *   \todo Make the Bounds calculation set inside the TerrainMesh rather than using tags here. (Centred or not, positioned)
    *   \todo Make models use a different vert type - without uvs.
    */
    [UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class TerrainMeshUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private VertexAttributeDescriptor[] vertLayoutUV;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            vertLayoutUV = new[]
            {
                new VertexAttributeDescriptor(VertexAttribute.Position,  VertexAttributeFormat.Float32, 3),
                new VertexAttributeDescriptor(VertexAttribute.Color,  VertexAttributeFormat.Float32, 3),
                new VertexAttributeDescriptor(VertexAttribute.TexCoord0,  VertexAttributeFormat.Float32, 2)
            };
        }

        protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<InitializeEntity>()
                .WithAll<MeshDirty>()
                .ForEach((Entity e, ZoxMeshOnly zoxMeshOnly, in TerrainMesh terrainMesh) =>
            {
                if (zoxMeshOnly.mesh == null)
                {
                    // UnityEngine.Debug.LogWarning("ZoxMeshUpdateSystem ZoxMesh mesh is null!");
                    zoxMeshOnly.mesh = new UnityEngine.Mesh();
                    PostUpdateCommands.SetSharedComponentManaged(e, zoxMeshOnly);
                }
                if (terrainMesh.vertices.Length == 0)
                {
                    zoxMeshOnly.mesh.Clear();
                    // UnityEngine.Debug.LogError("ZoxMeshUpdateSystem ZoxMesh Vertices Empty [" + e.Index + "]");
                    return;
                }
                // UnityEngine.Debug.LogError("vertexCount: " + terrainMesh.vertices.Length);
                var mesh = zoxMeshOnly.mesh;
                var vertexCount = terrainMesh.vertices.Length;
                var trianglesCount = terrainMesh.triangles.Length;
                var vertices = terrainMesh.GetVertexNativeArray();
                var triangles = terrainMesh.GetTriangleNativeArray();
                mesh.SetVertexBufferParams(vertexCount, vertLayoutUV);
                mesh.SetVertexBufferData(vertices, 0, 0, vertexCount);
                // max is x billion for 32 bits uint
                mesh.SetIndexBufferParams(trianglesCount,  IndexFormat.UInt32); //  IndexFormat.UInt32  IndexFormat.UInt16
                mesh.SetIndexBufferData(triangles, 0, 0, trianglesCount);
                mesh.SetSubMesh(0, new SubMeshDescriptor()
                {
                    baseVertex = 0,
                    firstVertex = 0,
                    indexStart = 0,
                    bounds = default,   // ? default? wut,
                    vertexCount = vertexCount,
                    indexCount = trianglesCount,
                    topology = UnityEngine.MeshTopology.Triangles
                });
                mesh.UploadMeshData(false);
                mesh.bounds = new UnityEngine.Bounds(terrainMesh.position, terrainMesh.extents * 2f);
                // UnityEngine.Debug.LogError("ZoxMeshUpdateSystem ZoxMesh Updated: " + e.Index + "] Verts: " + vertexCount);
            }).WithoutBurst().Run();
        }
    }
}

// testing mesh bounds
/*var oldBounds = mesh.bounds;
mesh.RecalculateBounds();
var newBounds = mesh.bounds;
if (oldBounds != newBounds)
{
    UnityEngine.Debug.LogError("My Calculated: " + oldBounds.ToString() + "\nUnity Bounds: " + newBounds.ToString());
}*/