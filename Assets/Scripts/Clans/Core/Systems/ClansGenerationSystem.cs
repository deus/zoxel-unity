using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Clans
{
    //! Generates clans for the Realm.
    /**
    *   - Data Generation System -
    */
    [BurstCompile, UpdateInGroup(typeof(ClanSystemGroup))]
    public partial class ClansGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity clanPrefab;
        private NativeArray<Text> texts;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var clanArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(Clan),
                typeof(ZoxID),
                typeof(ZoxName),
                typeof(ZoxDescription),
                typeof(UserDataIndex),
                typeof(CreatorLink));
            clanPrefab = EntityManager.CreateEntity(clanArchetype);
            texts = new NativeArray<Text>(6, Allocator.Persistent);
            texts[0] = new Text("Legion");
            texts[1] = new Text("They cooperate as monsters to raid the legged ones villages.");
            texts[2] = new Text("Alliance");
            texts[3] = new Text("Greed rules over an alliance of beings, a cooperation for profits."); //Humans, Elves and Dwarves often work together.");
            texts[4] = new Text("Horde");
            texts[5] = new Text("Those with with the greatest power rule over the Horde.");
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var texts = this.texts;
            var clanPrefab = this.clanPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<OnClansSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref GenerateRealm generateRealm, ref ClanLinks clanLinks, in ZoxID zoxID) =>
            {
                if (generateRealm.state == (byte) GenerateRealmState.GenerateClans2)
                {
                    generateRealm.state = (byte) GenerateRealmState.RefreshWorlds;
                    return;
                }
                if (generateRealm.state != (byte) GenerateRealmState.GenerateClans)
                {
                    return; // wait for thingo
                }
                var realmSeed = zoxID.id;
                if (realmSeed == 0)
                {
                    return;
                }
                generateRealm.state = (byte) GenerateRealmState.GenerateClans2;
                // Destroy UserSkillLinks and SkillTrees
                for (int i = 0; i < clanLinks.clans.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, clanLinks.clans[i]);
                }
                clanLinks.Dispose();
                // Spawn Skill Entities
                var dataCount = (byte) 3;
                var creatorLink = new CreatorLink(e);
                for (byte i = 0; i < dataCount; i++)
                {
                    var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, clanPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, creatorLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new UserDataIndex(i));
                    var dataID = realmSeed + 6421 + i * 1;
                    var random = new Random();
                    random.InitState((uint)dataID);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ZoxID(dataID));
                    var name = new Text();
                    var description = new Text();
                    if (i == 0)
                    {
                        name = texts[0];
                        description = texts[1];
                    }
                    else if (i == 1)
                    {
                        name = texts[2];
                        description = texts[3];
                        PostUpdateCommands.AddComponent<PlayerChoice>(entityInQueryIndex, e2);
                    }
                    else if (i == 2)
                    { 
                        name = texts[4];
                        description = texts[5];
                        PostUpdateCommands.AddComponent<PlayerChoice>(entityInQueryIndex, e2);
                    }
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ZoxName(name.Clone()));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ZoxDescription(description.Clone()));
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnClansSpawned(dataCount));
            })  .WithReadOnly(texts)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}