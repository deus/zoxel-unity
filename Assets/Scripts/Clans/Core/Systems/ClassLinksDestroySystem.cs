using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Clans
{
    //! Cleans up ClanLinks clans.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ClanLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithAll<DestroyEntity, ClanLinks>()
				.ForEach((int entityInQueryIndex, in ClanLinks clanLinks) =>
			{
                for (int i = 0; i < clanLinks.clans.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, clanLinks.clans[i]);
                }
                clanLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}