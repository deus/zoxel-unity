using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Clans
{
    //! Links user clans to clanLinks user after they spawn.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(ClanSystemGroup))]
    public partial class ClansSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery clansQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            clansQuery = GetEntityQuery(ComponentType.ReadOnly<Clan>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var clanEntities = clansQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var creatorLinks = GetComponentLookup<CreatorLink>(true);
            var dataIndexes = GetComponentLookup<UserDataIndex>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref ClanLinks clanLinks, ref OnClansSpawned onClansSpawned) =>
            {
                if (onClansSpawned.spawned != 255)
                {
                    clanLinks.Initialize(onClansSpawned.spawned);
                    onClansSpawned.spawned = 255;
                }
                byte count = 0;
                for (int i = 0; i < clanEntities.Length; i++)
                {
                    var e2 = clanEntities[i];
                    var creator = creatorLinks[e2].creator;
                    if (creator == e)
                    {
                        var index = dataIndexes[e2].index;
                        clanLinks.clans[index] = e2;
                        count++;
                        if (count == clanLinks.clans.Length)
                        {
                            PostUpdateCommands.RemoveComponent<OnClansSpawned>(entityInQueryIndex, e);
                            break;
                        }
                    }
                }
            })  .WithReadOnly(clanEntities)
                .WithDisposeOnCompletion(clanEntities)
                .WithReadOnly(creatorLinks)
                .WithReadOnly(dataIndexes)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}