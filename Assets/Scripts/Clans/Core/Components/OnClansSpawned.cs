using Unity.Entities;

namespace Zoxel.Clans
{
    //! An event for linking up Clans after spawning.
    public struct OnClansSpawned : IComponentData
    {
        public byte spawned;

        public OnClansSpawned(byte spawned)
        {
            this.spawned = spawned;
        }
    }
}