using Unity.Entities;

namespace Zoxel.Clans
{
    //! A link to a specific clan entity.
    public struct ClanLink : IComponentData
    {
        public Entity clan;
        // public int clanID;

        public ClanLink(Entity clan) //, int clanID)
        {
            this.clan = clan;
            //this.clanID = clanID;
        }
    }
}