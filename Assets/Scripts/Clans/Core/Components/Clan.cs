using Unity.Entities;

namespace Zoxel.Clans
{
    //! A tag for a clan entity.
    public struct Clan : IComponentData { }
}