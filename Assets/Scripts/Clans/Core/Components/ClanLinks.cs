using Unity.Entities;
using Unity.Collections;
// Realm's Classes

namespace Zoxel.Clans
{
    //! Links to clan meta datas.
    public struct ClanLinks : IComponentData
    {
        public BlitableArray<Entity> clans;

        public void Dispose()
        {
            if (clans.Length > 0)
            {
                clans.Dispose();
            }
        }

        public void Initialize(int count)
        {
            clans = new BlitableArray<Entity>(count, Allocator.Persistent);
        }

        public Entity GetData(EntityManager EntityManager, int targetID)
        {
            for (int i = 0; i < clans.Length; i++)
            {
                if (EntityManager.GetComponentData<ZoxID>(clans[i]).id == targetID)
                {
                    return clans[i];
                }
            }
            return new Entity();
        }
    }
}