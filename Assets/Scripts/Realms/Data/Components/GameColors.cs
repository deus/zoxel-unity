using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Realms
{
    // Meta data for userSkillLinks now generated per game
    public struct GameColors : IComponentData
    {
        public BlitableArray<Color> colors;

        public void Dispose()
        {
            if (colors.Length > 0)
            {
                colors.Dispose();
            }
        }

        public void Initialize(int count)
        {
            colors = new BlitableArray<Color>(count, Allocator.Persistent);
        }
    }
}