using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Zoxel.Voxels;
using Zoxel.Worlds;
using Zoxel.Textures;

namespace Zoxel.Realms
{
    //! Regenerates realm's WorldLinks.
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class RealmsRefreshWorldsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var seedID = IDUtil.GenerateUniqueID();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .ForEach((Entity e, int entityInQueryIndex, in WorldLinks worldLinks, in ZoxID zoxID, in GenerateRealm generateRealm) =>
            {
                if (generateRealm.state == (byte) GenerateRealmState.RefreshWorlds)
                {
                    for (int i = 0; i < worldLinks.worlds.Length; i++)
                    {
                        var planet = worldLinks.worlds[i];
                        if (planet.Index > 0) // HasComponent<ZoxID>(planet))
                        {
                            PostUpdateCommands.SetComponent(entityInQueryIndex, planet, new ZoxID(seedID)); // zoxID);
                            PostUpdateCommands.AddComponent<InitializeEntity>(entityInQueryIndex, planet);  // reset materials!
                            PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, planet);
                            PostUpdateCommands.AddComponent<GeneratePlanet>(entityInQueryIndex, planet);
                        }
                    }
                    PostUpdateCommands.RemoveComponent<EntityBusy>(entityInQueryIndex, e);
                    PostUpdateCommands.RemoveComponent<GenerateRealm>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}