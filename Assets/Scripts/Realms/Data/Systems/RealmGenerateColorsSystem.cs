using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;

namespace Zoxel.Realms
{
    //! Kinda not used atm.
    /**
    *   - Data Generation System -
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class RealmGenerateColorsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); //.AsParallelWriter();
            Entities.ForEach((Entity e, ref GenerateRealm generateRealm, ref GameColors gameColors, in Realm game, in ZoxID zoxID) =>
            {
                if (generateRealm.state != (byte)GenerateRealmState.GenerateColors)
                {
                    return; // wait for thingo
                }
                generateRealm.state = (byte) GenerateRealmState.GenerateClans;
                var realmSeed = zoxID.id;
                if (realmSeed == 0)
                {
                    return;
                }
                gameColors.Dispose();
                // Realm Colours
                var randomer = new Random();
                randomer.InitState((uint)realmSeed);
                gameColors.Initialize(12);
                var variance = new int3(160, 160, 160);
                var baseColor = new int3(80, 80, 80);
                //var randomValue = 0.5f * ( 1 + SimplexNoise.Generate(666 * 0.0007f, -666 * 0.0007f) );
                // randomValue = 
                for (int i = 0; i < gameColors.colors.Length; i += 2)
                {
                    var red = (byte)(baseColor.x + (int)(variance.x * randomer.NextFloat(1)));
                    //randomValue = 0.5f * ( 1 + SimplexNoise.Generate(1222 * 0.0007f, -666 * 0.0007f) );
                    var green = (byte)(baseColor.y + (int)(variance.y * randomer.NextFloat(1)));
                    //randomValue = 0.5f * ( 1 + SimplexNoise.Generate(1888 * 0.0007f, -666 * 0.0007f) );
                    var blue = (byte)(baseColor.z + (int)(variance.z * randomer.NextFloat(1)));
                    var fillColor = new Color(red, green, blue);
                    gameColors.colors[i] = fillColor;
                    gameColors.colors[i + 1] = fillColor.Invert();
                }
            }).WithoutBurst().Run();   // spawns skill tree and skill data
        }
    }
}