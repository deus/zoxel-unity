﻿using Unity.Entities;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Players;
// Realm links to all things
using Zoxel.Stats;
using Zoxel.Classes;
using Zoxel.Races;
using Zoxel.Voxels;
using Zoxel.Quests;
using Zoxel.Audio;
using Zoxel.Items;
using Zoxel.Skills;
using Zoxel.Clans;
using Zoxel.Rendering;
// [UpdateAfter(typeof(Zoxel.Transforms.TransformSystemGroup))]

namespace Zoxel.Realms
{
    //! Realm contains all things!
    /**
    *   \todo Use RealmColors for UI - making text lighter
    */
    [AlwaysUpdateSystem]
    [UpdateAfter(typeof(TransformSystemGroup))]
    public partial class RealmSystemGroup : ComponentSystemGroup
    {
        public static Entity realmPrefab;
        public static Entity realm;
        private EntityQuery realmQuery;

        protected override void OnCreate()
        {
            base.OnCreate();
            var realmArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(EntityBusy),
                typeof(GenerateRealm),
                typeof(Realm),
                typeof(Seed),
                typeof(ZoxID),
                typeof(ZoxName),
                typeof(WorldLinks),
                typeof(VoxelLinks),
                typeof(PlayerHomeLink),     // PlayerLinks are kept in a separate entity.
                typeof(MaterialLinks),
                typeof(StatLinks),
                typeof(ItemLinks),
                typeof(SlotLinks),
                typeof(SkillLinks),
                typeof(SkilltreeLinks),
                typeof(RaceLinks),
                typeof(ClassLinks),
                typeof(ClanLinks),
                typeof(QuestLinks),
                typeof(GameColors)
            );
            realmPrefab = EntityManager.CreateEntity(realmArchetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(realmPrefab); // , new EditorName("[top-down-camera]"));
            #endif
			realmQuery = GetEntityQuery(ComponentType.ReadOnly<Realm>());
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();
            var realmEntities = realmQuery.ToEntityArray(World.UpdateAllocator.ToAllocator);
            if (realmEntities.Length > 0)
            {
                realm = realmEntities[0];
            }
            else
            {
                realm = new Entity();
            }
            realmEntities.Dispose();
        }
    }
}
