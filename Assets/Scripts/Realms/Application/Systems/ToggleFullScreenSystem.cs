using Unity.Entities;
using Unity.Burst;
using Zoxel.Input;

namespace Zoxel.Realms
{
    //! Toggles full screen mode.
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class ToggleFullScreenSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity toggleFullScreenEventPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(ToggleFullScreen),
                typeof(GenericEvent));
            toggleFullScreenEventPrefab = EntityManager.CreateEntity(archetype);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var toggleFullScreenEventPrefab = this.toggleFullScreenEventPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .ForEach((int entityInQueryIndex, in  Controller controller) =>
            {
                if ((controller.keyboard.leftAltKey.isPressed == 1 && controller.keyboard.enterKey.wasPressedThisFrame == 1)
                    || (controller.keyboard.rightAltKey.isPressed == 1 && controller.keyboard.enterKey.wasPressedThisFrame == 1))
                {
                    // Toggle Screen FullScreen
                    PostUpdateCommands.Instantiate(entityInQueryIndex, toggleFullScreenEventPrefab);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

            Entities.WithAll<ToggleFullScreen>().ForEach(() =>
            {
                ScreenUtil.ToggleFullScreen();
			}).WithoutBurst().Run();
		}
    }
}