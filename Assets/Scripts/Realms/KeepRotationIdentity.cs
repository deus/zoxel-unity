using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using Unity.Entities;
using Zoxel.Transforms;
using Zoxel.Cameras;

namespace Zoxel
{
    //! Used for skybox.
    public class KeepRotationIdentity : MonoBehaviour
    {
        void Start()
        {
            /*if (transform.parent == null)
            {
                enabled = false;
            }*/
            GetComponent<MeshFilter>().mesh = GenerateCube();
        }

        void OnDestroy()
        {
            Destroy(GetComponent<MeshFilter>().mesh);
        }

        void Update()
        {
            if (World.DefaultGameObjectInjectionWorld == null)
            {
                return;
            }
            var EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            var mainCameraEntity = CameraReferences.GetMainCameraEntity(EntityManager);
            if (mainCameraEntity.Index == 0)
            {
                return;
            }
            var mainCameraPosition = EntityManager.GetComponentData<Translation>(mainCameraEntity).Value;
            transform.position = mainCameraPosition; // Camera.main.transform.position;
            // transform.rotation = quaternion.identity;
            // if (Camera.main)
            {
            }
        }

        public static Mesh GenerateCube()
        {
            var size = new float3(1, 1, 1);
            var mesh = new Mesh();
            mesh.name = "Sky Cube";
            var verts = new Vector3[MeshUtilities.cubeVerticesVector3.Length];
            for (int i = 0; i < verts.Length; i++)
            {
                verts[i] = MeshUtilities.cubeVerticesVector3[i];
                verts[i] -= new Vector3(0.5f, 0.5f, 0.5f);
                verts[i] = new Vector3(verts[i].x * size.x, verts[i].y * size.y, verts[i].z * size.z);
            }
            mesh.vertices = verts;
            var triangles = new int[MeshUtilities.cubeTriangles2.Length];
            for (int i = 0; i < triangles.Length; i++)
            {
                triangles[i] = MeshUtilities.cubeTriangles2[i];
            }
            mesh.triangles = triangles;
            var uvs = new Vector2[verts.Length];
            for (int i = 0; i < verts.Length; i++)
            {
                var vert = verts[i];
                var uv = new float2();
                if (vert.y > 0)
                {
                    uv.y = 1;
                }
                else
                {
                    uv.y = - 1;
                }
                if (vert.x > 0)
                {
                    uv.x = 1;
                }
                else
                {
                    uv.x = - 1;
                }
                uvs[i] = uv;
            }
            mesh.uv = uvs;
            return mesh;
        }
    }

}