using Unity.Entities;

namespace Zoxel.Realms
{
    [UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class MainMenuClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<OpenTwitter>()
                .ForEach((Entity e) =>
            {
                UnityEngine.Application.OpenURL("https://twitter.com/deusxyz");
                PostUpdateCommands.DestroyEntity(e);
            }).WithoutBurst().Run();
        }
    }
}