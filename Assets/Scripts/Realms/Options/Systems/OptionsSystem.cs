using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Characters;
using Zoxel.Players;
using Zoxel.Rendering;
using Zoxel.Cameras;
using Zoxel.Rendering.Authoring;

namespace Zoxel.Realms
{
    //! Handles option events.
    /**
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class OptionsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<SoundSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<DecreaseMusicVolume>()
                .ForEach((Entity e, in ParentLink parentLink, in Zoxel.Transforms.Child child) =>
            {
                PostUpdateCommands2.RemoveComponent<DecreaseMusicVolume>(e);
                if (AudioManager.instance.musicLevel > 0)
                {
                    AudioManager.instance.musicLevel--;
                    //UnityEngine.Debug.LogError("Decreasing Music Volume: " + AudioManager.instance.musicLevel);
                    PostUpdateCommands2.AddComponent(e, new SetRenderText(new Text("- Music Volume " + AudioManager.instance.musicLevel)));
                    var otherButtonEntity = EntityManager.GetComponentData<Childrens>(parentLink.parent).children[child.index - 1];
                    PostUpdateCommands2.AddComponent(otherButtonEntity, new SetRenderText(new Text("+ Music Volume " + AudioManager.instance.musicLevel)));
                }
            }).WithoutBurst().Run();            Entities
                .WithAll<IncreaseMusicVolume>()
                .ForEach((Entity e, in ParentLink parentLink, in Zoxel.Transforms.Child child) =>
            {
                PostUpdateCommands2.RemoveComponent<IncreaseMusicVolume>(e);
                if (AudioManager.instance.musicLevel < 10)
                {
                    AudioManager.instance.musicLevel++;
                    //UnityEngine.Debug.LogError("Increasing Music Volume: " + AudioManager.instance.musicLevel);
                    PostUpdateCommands2.AddComponent(e, new SetRenderText(new Text("+ Music Volume " + AudioManager.instance.musicLevel)));
                    var otherButtonEntity = EntityManager.GetComponentData<Childrens>(parentLink.parent).children[child.index + 1];
                    PostUpdateCommands2.AddComponent(otherButtonEntity, new SetRenderText(new Text("- Music Volume " + AudioManager.instance.musicLevel)));
                    //AudioManager.instance.music.volume = AudioManager.instance.musicLevel / 5f;
                }
            }).WithoutBurst().Run();

            var soundSettings = GetSingleton<SoundSettings>();
            Entities
                .WithAll<IncreaseSoundVolume>()
                .ForEach((Entity e, in ParentLink parentLink, in Zoxel.Transforms.Child child) =>
            {
                PostUpdateCommands2.RemoveComponent<IncreaseSoundVolume>(e);
                if (soundSettings.soundLevel < 10)
                {
                    soundSettings.soundLevel++;
                    //UnityEngine.Debug.LogError("Increasing Music Volume: " + AudioManager.instance.musicLevel);
                    PostUpdateCommands2.AddComponent(e, new SetRenderText(new Text("+ Sound Volume " + soundSettings.soundLevel)));
                    var otherButtonEntity = EntityManager.GetComponentData<Childrens>(parentLink.parent).children[child.index + 1];
                    PostUpdateCommands2.AddComponent(otherButtonEntity, new SetRenderText(new Text("- Sound Volume " + soundSettings.soundLevel)));
                    //AudioManager.instance.sounds.volume = AudioManager.instance.soundLevel / 10f;
                }
            }).WithoutBurst().Run();

            Entities
                .WithAll<DecreaseSoundVolume>()
                .ForEach((Entity e, in ParentLink parentLink, in Zoxel.Transforms.Child child) =>
            {
                PostUpdateCommands2.RemoveComponent<DecreaseSoundVolume>(e);
                if (soundSettings.soundLevel > 0)
                {
                    soundSettings.soundLevel--;
                    //UnityEngine.Debug.LogError("Decreasing Music Volume: " + AudioManager.instance.musicLevel);
                    PostUpdateCommands2.AddComponent(e, new SetRenderText(new Text("- Sound Volume " + soundSettings.soundLevel)));
                    var otherButtonEntity = EntityManager.GetComponentData<Childrens>(parentLink.parent).children[child.index - 1];
                    PostUpdateCommands2.AddComponent(otherButtonEntity, new SetRenderText(new Text("+ Sound Volume " + soundSettings.soundLevel)));
                    //AudioManager.instance.sounds.volume = AudioManager.instance.soundLevel / 10f;
                }
            }).WithoutBurst().Run();
            SetSingleton<SoundSettings>(soundSettings);

            var renderSettings = GetSingleton<RenderSettings>();
            Entities
                .WithAll<IncreaseRenderDistance>()
                .ForEach((Entity e, in ParentLink parentLink, in Zoxel.Transforms.Child child) =>
            {
                PostUpdateCommands2.RemoveComponent<IncreaseRenderDistance>(e);
                if (renderSettings.renderDistance < 20)
                {
                    renderSettings.renderDistance++;
                    PostUpdateCommands2.AddComponent(e, new SetRenderText(new Text("+ Render Distance " + renderSettings.renderDistance)));
                    var otherButtonEntity = EntityManager.GetComponentData<Childrens>(parentLink.parent).children[child.index + 1];
                    PostUpdateCommands2.AddComponent(otherButtonEntity, new SetRenderText(new Text("- Render Distance " + renderSettings.renderDistance)));
                    UpdateRenderDistance(EntityManager, renderSettings.renderDistance);
                }
            }).WithoutBurst().Run();
            Entities
                .WithAll<DecreaseRenderDistance>()
                .ForEach((Entity e, in ParentLink parentLink, in Zoxel.Transforms.Child child) =>
            {
                PostUpdateCommands2.RemoveComponent<DecreaseRenderDistance>(e);
                if (renderSettings.renderDistance > 3)
                {
                    renderSettings.renderDistance--;
                    PostUpdateCommands2.AddComponent(e, new SetRenderText(new Text("- Render Distance " + renderSettings.renderDistance)));
                    var otherButtonEntity = EntityManager.GetComponentData<Childrens>(parentLink.parent).children[child.index - 1];
                    PostUpdateCommands2.AddComponent(otherButtonEntity, new SetRenderText(new Text("+ Render Distance " + renderSettings.renderDistance)));
                    UpdateRenderDistance(EntityManager, renderSettings.renderDistance);
                }
            }).WithoutBurst().Run();
            SetSingleton<RenderSettings>(renderSettings);
        }

        private static void UpdateRenderDistance(EntityManager EntityManager, byte renderDistance)
        {
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            for (int i = 0; i < playerLinks.players.Length; i++)
            {
                var playerEntity = playerLinks.players[i];
                if (EntityManager.HasComponent<ChunkStreamPoint>(playerEntity))
                {
                    EntityManager.AddComponentData(playerEntity, new OnRenderDistanceUpdated(renderDistance));
                }
            }
            UnityEngine.RenderSettings.fogStartDistance = math.max(16, (renderDistance - 2) * 16);
            UnityEngine.RenderSettings.fogEndDistance = math.max(32, (renderDistance - 1) * 16);
        }
    }
}