using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Cameras;

namespace Zoxel.Realms
{
    //! Handles option events.
    /**
    *   \todo Make options linked to Realm.. or a Application level entity. Maybe realm is best.
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class CameraOptionsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQueryA;
        private EntityQuery processQueryB;
        private EntityQuery camerasQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQueryA = GetEntityQuery(ComponentType.ReadOnly<SettingsDecreaseFov>());
            processQueryB = GetEntityQuery(ComponentType.ReadOnly<SettingsIncreaseFov>());
            camerasQuery = GetEntityQuery(
                ComponentType.ReadOnly<Camera>(),
                ComponentType.ReadWrite<FieldOfView>(),
                ComponentType.Exclude<DestroyEntity>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (processQueryA.IsEmpty && processQueryB.IsEmpty)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var cameraFieldOfViewLabel = " Fov ";
            var minFov = 30;
            var maxFov = 150;
            var incrementFov = 5;
            var cameraEntities = camerasQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var fieldOfViews = GetComponentLookup<FieldOfView>(false);
            if (!processQueryA.IsEmpty)
            {
                Entities
                    .WithAll<SettingsDecreaseFov>()
                    .ForEach((Entity e, in ParentLink parentLink, in Zoxel.Transforms.Child child) =>
                {
                    PostUpdateCommands2.RemoveComponent<SettingsDecreaseFov>(e);
                    if (CameraManager.instance.cameraSettings.fieldOfView > minFov)
                    {
                        CameraManager.instance.realCameraSettings.fieldOfView -= incrementFov;
                        for (int i = 0; i < cameraEntities.Length; i++)
                        {
                            var e2 = cameraEntities[i];
                            var fieldOfView = fieldOfViews[e2];
                            fieldOfView.fov = CameraManager.instance.cameraSettings.fieldOfView;
                            fieldOfViews[e2] = fieldOfView;
                        }
                        PostUpdateCommands2.AddComponent(e, new SetRenderText(new Text("- " + cameraFieldOfViewLabel + CameraManager.instance.cameraSettings.fieldOfView)));
                        var otherButtonEntity = EntityManager.GetComponentData<Childrens>(parentLink.parent).children[child.index + 1];
                        PostUpdateCommands2.AddComponent(otherButtonEntity, new SetRenderText(new Text("+ " + cameraFieldOfViewLabel + CameraManager.instance.cameraSettings.fieldOfView)));
                    }
                })  .WithDisposeOnCompletion(cameraEntities)
                    .WithReadOnly(cameraEntities)
                    .WithNativeDisableContainerSafetyRestriction(fieldOfViews)
                    .WithoutBurst().Run();
            }
            else
            {
                Entities
                    .WithAll<SettingsIncreaseFov>()
                    .ForEach((Entity e, in ParentLink parentLink, in Zoxel.Transforms.Child child) =>
                {
                    PostUpdateCommands2.RemoveComponent<SettingsIncreaseFov>(e);
                    if (CameraManager.instance.cameraSettings.fieldOfView < maxFov)
                    {
                        CameraManager.instance.realCameraSettings.fieldOfView += incrementFov;
                        for (int i = 0; i < cameraEntities.Length; i++)
                        {
                            var e2 = cameraEntities[i];
                            var fieldOfView = fieldOfViews[e2];
                            fieldOfView.fov = CameraManager.instance.cameraSettings.fieldOfView;
                            fieldOfViews[e2] = fieldOfView;
                        }
                        PostUpdateCommands2.AddComponent(e, new SetRenderText(new Text("+ " + cameraFieldOfViewLabel + CameraManager.instance.cameraSettings.fieldOfView)));
                        var otherButtonEntity = EntityManager.GetComponentData<Childrens>(parentLink.parent).children[child.index - 1];
                        PostUpdateCommands2.AddComponent(otherButtonEntity, new SetRenderText(new Text("- " + cameraFieldOfViewLabel + CameraManager.instance.cameraSettings.fieldOfView)));
                    }
                })  .WithDisposeOnCompletion(cameraEntities)
                    .WithReadOnly(cameraEntities)
                    .WithNativeDisableContainerSafetyRestriction(fieldOfViews)
                    .WithoutBurst().Run();
            }
        }
    }
}