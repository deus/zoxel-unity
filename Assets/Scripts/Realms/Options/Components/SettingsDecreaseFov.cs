using Unity.Entities;

namespace Zoxel.Realms
{
    public struct SettingsDecreaseFov : IComponentData { }
    public struct SettingsIncreaseFov : IComponentData { }
}