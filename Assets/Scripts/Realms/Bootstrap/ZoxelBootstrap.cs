﻿using UnityEngine;
using Unity.Entities;
using Zoxel.Realms;
using Zoxel.Players;
using Zoxel.Realms.UI;
using Zoxel.Cameras.Spawning;
using Zoxel.Input;
using Zoxel.Audio.Music.UI;
using Zoxel.Networking.UI;

namespace Zoxel.Realms
{
    //! Spawns Zoxel Players, Music and Title Screen.
    /**
    *   Remember: Turn StripAll on with v2 of hybrid renderer
    *   A log that shows planet generation happening when making the game
    *       Procedurally create factions at start of game generation - save game data
    *       also create a planet chunkBiome planet
    *       on top of this create town locations
    *       on top of this create rivers and mountains
    *       genetics that creates character models
    *
    *   \todo Custom Entities Bootstrap Code and disable unities. Create World and systems from scratch. This will lead to less reliance on Unity changes.
    *       UNITY_DISABLE_AUTOMATIC_SYSTEM_BOOTSTRAP
    *
    *   ZOXEL_AUTO_BOOTSTRAP & UNITY_DISABLE_AUTOMATIC_SYSTEM_BOOTSTRAP can be added for custom bootstrap
    *       * disabled for now
    */
    public partial class ZoxelBootstrap : MonoBehaviour
    {
        private bool isInitialized;
        public static ZoxelBootstrap instance;
        [Header("Booting")]
        public bool isSpawnDevices;
        public bool isSpawnPlayers;
        public bool isSpawnCamera;
        public bool isSpawnMusic;

        [Header("Cube")]
        public bool isSpawnCube;
        public UnityEngine.Material startScreenCubeMaterial;

        [Header("Testing")]
        public bool isTestMusicUI;
        public bool isTestServerUI;
        public bool isTestChatUI;

        public void Awake()
        {
            instance = this;
        }

        public void Update()
        {
            Initialize();
        }

        private void Initialize()
        {
            if (isInitialized)
            {
                return;
            }
            if (World.DefaultGameObjectInjectionWorld == null)
            {
                UnityEngine.Debug.LogError("Default Entities World not created.");
                return;
            }
            isInitialized = true;
            if (NetworkUtil.IsHeadless())
            {
                return;
            }
            var EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            if (isSpawnDevices)
            {
                InputSystemGroup.SpawnDevices(EntityManager);
            }
            if (isSpawnPlayers)
            {
                PlayerSystemGroup.SpawnPlayers(EntityManager);
            }
            if (isSpawnCamera)
            {
                CameraSpawnSystem.SpawnCamera(EntityManager, isSpawnMusic);
            }
            if (isSpawnCube)
            {
                StartScreenCubeSpawnSystem.Initialize(EntityManager, startScreenCubeMaterial);
            }
            if (isTestMusicUI)
            {
                MusicUISpawnSystem.TestUI(EntityManager);
            }
            if (isTestServerUI)
            {
                ServerUISpawnSystem.TestUI(EntityManager);
            }
            if (isTestChatUI)
            {
                ChatUISpawnSystem.TestUI(EntityManager);
            }
        }
    }
}

/*#if UNITY_WEBGL
PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
PlayerSettings.WebGL.threadsSupport = true;
PlayerSettings.WebGL.memorySize = 512; // tweak this value for your project
#endif*/