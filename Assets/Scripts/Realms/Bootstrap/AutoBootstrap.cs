/*#if UNITY_DISABLE_AUTOMATIC_SYSTEM_BOOTSTRAP && ZOXEL_AUTO_BOOTSTRAP
using System;
using System.Collections.Generic;
using System.Reflection;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;
     
namespace Zoxel.Realms.Bootstrap
{
    //! Automatically spawns systems and groups.
    public class AutoBootstrap : IDisposable
    {
        private static AutoBootstrap instance;
        private Unity.Entities.World world;

#if UNITY_EDITOR
        static AutoBootstrap()
        {
            UnityEditor.EditorApplication.playModeStateChanged += ModeChanged;
        }

        static void ModeChanged(UnityEditor.PlayModeStateChange playModeState)
        {
            if (instance == null)
            {
                return;
            }
            if (!UnityEditor.EditorApplication.isPlayingOrWillChangePlaymode && UnityEditor.EditorApplication.isPlaying) 
            {
                instance.Dispose();
            }
        }
#endif

        public void Dispose()
        {
            UnityEngine.Debug.Log("AutoBootstrap Disposed.");
            Unity.Entities.World.DisposeAllWorlds();
            instance = null;
            world = null;
        }
        
        // or BeforeSplashScreen
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Initialize()
        {
            if (instance == null)
            {
                instance = new AutoBootstrap();
            }
            Unity.Entities.World.DisposeAllWorlds();
            #if UNITY_EDITOR
            if (!UnityEditor.EditorApplication.isPlaying)
            {
                return;
            }
            #endif
            instance.CreateWorld();
        }

        //! Create a entities world and call spawn systems.
        public void CreateWorld()
        {
            world = new World("Zoxel_Auto");
            World.DefaultGameObjectInjectionWorld = world;
            SpawnSystems();
            ScriptBehaviourUpdateOrder.AppendWorldToCurrentPlayerLoop(world);
        }

        //! Spawns our systems using namespace fetches.
        private void SpawnSystems()
        {
            // find system groups and systems
            var systemGroupTypes = GetNamespaceTypes("Zoxel", typeof(ComponentSystemGroup));
            var systemTypes = GetNamespaceTypes("Zoxel", typeof(SystemBase));
            // spawn system groups
            var initializationSystemGroup = world.GetOrCreateSystem<InitializationSystemGroup>();
            var simulationSystemGroup = world.GetOrCreateSystem<SimulationSystemGroup>();
            var lateSimulationSystemGroup = world.GetOrCreateSystem<LateSimulationSystemGroup>();
            var presentationSystemGroup = world.GetOrCreateSystem<PresentationSystemGroup>();
            // spawn systems
            initializationSystemGroup.AddSystemToUpdateList(world.GetOrCreateSystem<UpdateWorldTimeSystem>());
            initializationSystemGroup.AddSystemToUpdateList(world.GetOrCreateSystem<ConvertToEntitySystem>());
            initializationSystemGroup.AddUnmanagedSystemToUpdateList(world.GetOrCreateUnmanagedSystem(typeof(WorldUpdateAllocatorResetSystem)));
            
            simulationSystemGroup.AddSystemToUpdateList(world.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>());

            simulationSystemGroup.AddSystemToUpdateList(lateSimulationSystemGroup);
            var systemGroups = world.GetOrCreateSystemsAndLogException(systemGroupTypes.AsArray());
            AddSystemsToParent(world, simulationSystemGroup, systemGroups);
            var systems = world.GetOrCreateSystemsAndLogException(systemTypes.AsArray());
            AddSystemsToParent(world, simulationSystemGroup, systems);
            simulationSystemGroup.SortSystems();
        }

        //! Adds a bunch of systems to their proper parents.
        private void AddSystemsToParent(World world, in ComponentSystemGroup defaultGroup, in ComponentSystemBase[] systems)
        {
            // Add systems to their groups, based on the [BurstCompile, UpdateInGroup] attribute.
            foreach (var system in systems)
            {
                if (system == null)
                {
                    continue;
                }
                var updateInGroupAttributes = TypeManager.GetSystemAttributes(system.GetType(), typeof(UpdateInGroupAttribute));
                if (updateInGroupAttributes.Length == 0)
                {
                    defaultGroup.AddSystemToUpdateList(system);
                }
                var type = system.GetType();
                foreach (var attr in updateInGroupAttributes)
                {
                    var group = FindGroup(world, type, attr);
                    group?.AddSystemToUpdateList(system);
                }
            }
        }

        //! Uses System.Reflection to find types.
        private List<Type> GetNamespaceTypes(string desiredNamespace, Type targetBaseType)
        {
            var allowedTypes = new List<Type>();
            foreach(var assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach(var type in assembly.GetTypes())
                {
                    if (type.Namespace != null && type.Namespace.Contains(desiredNamespace) && IsSubclassOfType(type, targetBaseType))
                    {
                        // UnityEngine.Debug.LogError("Allowed System in [" + desiredNamespace + "]: " + type.ToString());
                        allowedTypes.Add(type);
                    }
                }
            }
            return allowedTypes;
        }

        //! Uses Recurseion to check if type inherits another type.
        private static bool IsSubclassOfType(Type myType, Type targetBaseType)
        {
            while (myType != null && myType != typeof(object))
            {
                if (myType == targetBaseType)
                {
                    return true;
                }
                myType = myType.BaseType;
            }
            return false;
        }
     
        //! Finds the parent system groups, using the attributes on the systems.
        private static ComponentSystemGroup FindGroup(World world, Type systemType, Attribute attr)
        {
            var uga = attr as UpdateInGroupAttribute;
            if (uga == null)
                return null;
    
            var groupType = uga.GroupType;
            if (!TypeManager.IsSystemAGroup(groupType))
            {
                throw new InvalidOperationException($"Invalid [BurstCompile, UpdateInGroup] attribute for {systemType}: {uga.GroupType} must be derived from ComponentSystemGroup.");
            }
    
            if (uga.OrderFirst && uga.OrderLast)
            {
                throw new InvalidOperationException($"The system {systemType} can not specify both OrderFirst=true and OrderLast=true in its [BurstCompile, UpdateInGroup] attribute.");
            }
    
            var groupSys = world.GetExistingSystem(groupType);
            if (groupSys == null) //  && !excludedTypes.Contains(groupType))
            {
                // Warn against unexpected behaviour combining DisableAutoCreation and UpdateInGroup
                var parentDisableAutoCreation = TypeManager.GetSystemAttributes(uga.GroupType, typeof(DisableAutoCreationAttribute)).Length > 0;
                Debug.LogWarning(parentDisableAutoCreation
                                    ? $"A system {systemType} wants to execute in {uga.GroupType} but this group has [DisableAutoCreation] and {systemType} does not. The system will not be added to any group and thus not update."
                                    : $"A system {systemType} could not be added to group {uga.GroupType}, because the group was not created. Fix these errors before continuing. The system will not be added to any group and thus not update.");
            }
    
            return groupSys as ComponentSystemGroup;
        }
    }
}
#endif*/