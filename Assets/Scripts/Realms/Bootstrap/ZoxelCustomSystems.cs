#if UNITY_DISABLE_AUTOMATIC_SYSTEM_BOOTSTRAP && !ZOXEL_AUTO_BOOTSTRAP
using UnityEngine;
using System;
using Unity.Entities;
using Zoxel.Generic;
using Zoxel.Transforms;
using Zoxel.Transforms.Orbit;
using Zoxel.Rendering;
using Zoxel.Rendering.Renderer;
using Zoxel.Players;
using Zoxel.Players.UI;
using Zoxel.Players.Spawning;
using Zoxel.Cameras;
using Zoxel.Input;
using Zoxel.Audio;
using Zoxel.Audio.Music;
using Zoxel.Textures;
using Zoxel.Textures.Font;
using Zoxel.UI;
using Zoxel.UI.Players;
using Zoxel.UI.MouseFollowing;
using Zoxel.Animations;
using Zoxel.Voxels;
using Zoxel.Realms;
using Zoxel.Realms.UI;

//! \todo Replace HasComponents with TryGetComponent for ComponentDataEntity objects.

namespace Zoxel.Realms.Bootstrap
{
    //! Spawns all entities system groups.
    /**
    *   Built for Il2CPP. Helping to organize my code too.
    *   Add UNITY_DISABLE_AUTOMATIC_SYSTEM_BOOTSTRAP for the magic.
    *   todo:
    *       - [ ] Game UIs (RealmSelect/Maker, CharacterSelect/Maker, Options)
    *       - [ ] Stats
    *       - [ ] Skills
    *       - [ ] Items
    *       - [ ] UserActionLinks
    *       - [ ] Physics (rename from Movement)
    *       - [ ] Characters
    *       - [ ] Voxels
    *       - [ ] Worlds
    *       - [ ] Weather
    *       - [ ] Minivoxes
    *       - [ ] VoxelInteract
    *       - [ ] Skeletons
    *       - [ ] Quests
    *       - [ ] Maps
    *       - [ ] Races
    *       - [ ] Makers
    *       - [ ] AI
    *       - [ ] Portals
    *       - [ ] Particles
    *       - [ ] Turrets
    *       - [ ] Jobs
    *       - [ ] Genetics
    *       - [ ] Achievements
    *       - [ ] Lore
    */
    public class ZoxelCustomSystems : ICustomBootstrap // MonoBehaviour, 
    {
        private static ZoxelCustomSystems instance;
        private Unity.Entities.World entitiesWorld;
        
        // BeforeSplashScreen
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        private static void Initialize()
        {
            if (instance == null)
            {
                instance = new ZoxelCustomSystems();
            }
            instance.CreateSystems();
        }

        void OnDestroy()
        {
            Unity.Entities.World.DisposeAllWorlds();
        }
 
        public bool Initialize(string defaultWorldName)
        {
            return true;
        }

        public void CreateSystems()
        {
            Unity.Entities.World.DisposeAllWorlds();
            UnityEngine.Debug.Log("Creating Zoxel ECS Systems.");
            entitiesWorld = new Unity.Entities.World("Zoxel");
            var initializationSystemGroup = entitiesWorld.GetOrCreateSystem<InitializationSystemGroup>();
            initializationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<UpdateWorldTimeSystem>());
            initializationSystemGroup.AddUnmanagedSystemToUpdateList(entitiesWorld.GetOrCreateUnmanagedSystem(typeof(WorldUpdateAllocatorResetSystem)));
            
            var simulationSystemGroup = entitiesWorld.GetOrCreateSystem<SimulationSystemGroup>();
            var lateSimulationSystemGroup = entitiesWorld.GetOrCreateSystem<LateSimulationSystemGroup>();
            var presentationSystemGroup = entitiesWorld.GetOrCreateSystem<PresentationSystemGroup>();
            // -= Zoxel System Groups =-
            AddGeneric(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup);
            AddRealms(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup);
            // -= input & players =-
            AddInput(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup);
            AddPlayers(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup);
            // -= audio, textures, voxels =-
            AddAudio(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup);
            AddTextures(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup);
            AddVoxels(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup);
            AddMusic(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup);
            // -= UI =-
            AddUI(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup);
            AddText(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup);
            AddGameUI(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup);
            // -= animations, transforms, cameras, renders =-
            AddTransforms(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup);
            AddTransformSynchs(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup);
            AddAnimations(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup);
            AddCameras(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup);
            AddRenders(entitiesWorld, simulationSystemGroup, lateSimulationSystemGroup, presentationSystemGroup);
            // -= late simu and command buffer =-
            simulationSystemGroup.AddSystemToUpdateList(lateSimulationSystemGroup);
            simulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>());
            // finish
            simulationSystemGroup.SortSystems();
            World.DefaultGameObjectInjectionWorld = entitiesWorld;
            ScriptBehaviourUpdateOrder.AppendWorldToCurrentPlayerLoop(entitiesWorld);
        }

        private void AddVoxels(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup, LateSimulationSystemGroup lateSimulationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<VoxelSystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            // systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<StartScreenCubeSpawnSystem>());
        }

        private void AddRealms(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup, LateSimulationSystemGroup lateSimulationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<RealmSystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<StartScreenCubeSpawnSystem>());
        }

        private void AddGeneric(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup, LateSimulationSystemGroup lateSimulationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<ZoxelGenericSystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EditorNameSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EventDelaySystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EventDelayFramesSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EntityDestroyInTimeSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EntityDyingEndSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EntityDestroyInTimeSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EntityDestroyInTimeSystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EventDestroySystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EditorNameDestroySystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EntityInitializeSystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EntityDestroySystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ZoxNameDestroySystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ZoxDescriptionDestroySystem>());
        }

        private void AddInput(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup, LateSimulationSystemGroup lateSimulationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<InputSystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<DeviceSpawnSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<DeviceSpawnedSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<DeviceConnectorSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<DeviceDisconnectorSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ConnectedDevicesDisconnectSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<DeviceRemoveSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<DevicesReconnectSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ControllerDevicesSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<SetMappingSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<CursorStateSystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ConnectedDevicesDestroySystem>());
        }

        private void AddPlayers(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup,
            LateSimulationSystemGroup lateSimulationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<PlayerSystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            // Core
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<PlayerSpawnSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<PlayerSpawnedSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<PlayerRemoveSystem>());
            // Player UI
            // systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<PlayerClickSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<PlayerNavigateUISystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<TooltipTranslateSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<NavigateDelayEndSystem>());
            // Destroy
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<PlayerLinksDestroySystem>());
            // player character
            //systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<CameraToggleSystem>());
            //systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<CrosshairColorSystem>());
            // Realms UI
            //systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<MainMenuPlayerOneSystem>());
        }

        private void AddAnimations(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup, LateSimulationSystemGroup lateSimulationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<AnimationSystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EternalRotationSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EternalScalingSystem>());
        }

        private void AddTransforms(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup, LateSimulationSystemGroup lateSimulationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<Zoxel.Transforms.TransformSystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            // hierarchy
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<OrbitPositionSetSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ParentPositionSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ParentRotationSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ParentScaleSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ChildrensDestroyOnlySystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ChildrenRemoveSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ChildrensLinkSystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ChildrensDestroySystem>());
            // Core
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<TransformSetPositionSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EntityAttachSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<TransformSystem>());
        }

        private void AddTransformSynchs(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup, LateSimulationSystemGroup lateSimulationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<TransformSynchSystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<HybridTransformSynchSystem>());
            //systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<TransformRotationSynchSystem>());
            //systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<TransformScaleSynchSystem>());
            #if UNITY_EDITOR
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EditorCameraSynchSystem>());
            #endif
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<GameObjectReverseSyncSystem>());
            // destroy
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<GameObjectDestroySystem>());
        }

        private void AddCameras(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup, LateSimulationSystemGroup lateSimulationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<CameraSystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<CameraEnabledSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<CameraFieldOfViewSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<CameraFrustrumSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<CameraInitializeSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<CameraProjectionMatrixSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<CameraSpawnSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<PostProcessingUpdateSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<CameraScreenRectSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<CameraScreenDimensionsSystem>());
            // Destroy
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<CameraFrustrumPlanesDestroySystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<RenderTextureLinkDestroySystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<CameraDestroySystem>());
        }

        private void AddRenders(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup,
            LateSimulationSystemGroup lateSimulationSystemGroup, PresentationSystemGroup presentationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<RenderSystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            // Mesh
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ZoxMeshRemoveSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<MeshDirtyEndSystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ZoxMeshDestroySystem>());
            // Materials
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<RenderQueueSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<MaterialBaseColorSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<MaterialFrameColorSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<OutlineColorUpdateSystem>());
            presentationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<BasicRenderSystem>());
            presentationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<StaticMultiRenderSystem>());
            presentationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<StaticRenderSystem>());
            // Remove this and use only BasicRender for cube
            presentationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<StartScreenCubeRenderSystem>());
        }

        private void AddTextures(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup, LateSimulationSystemGroup lateSimulationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<TextureSystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            // core
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<TextureResolutionSetSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<TextureDirtyDelaySystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<TextureUpdateSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ZoxMeshTextureSetSystem>());
            // textures
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<TextureFontGenerationSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<TextureFrameGenerationSystem>());
            // icons
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<EmptyIconGenerationSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<SquareIconGenerateSystem>());
            // destroy
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<TextureDestroySystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<UnityTextureDestroySystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<UnityMaterialDestroySystem>());
        }

        private void AddAudio(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup, LateSimulationSystemGroup lateSimulationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<AudioSystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<SoundInitializeSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<SoundPlaySystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<SoundGenerationSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<SoundDestroyAfterPlaySystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<SoundDestroySystem>());
        }

        private void AddMusic(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup, LateSimulationSystemGroup lateSimulationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<MusicSystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<MusicEnableSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<MusicInitializeSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<MusicSpawnSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<MusicPlaySystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<MusicLinkDestroySystem>());
        }

        private void AddUI(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup, LateSimulationSystemGroup lateSimulationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<UISystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            // core
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<UICoreSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ElementPositionSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ButtonColorInitializeSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<UIMeshGenerateSystem>());
            // lists
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<UIListSpawnSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ButtonsListSpawnSystem>());
            // Grid
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<GridUIResizeSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<GridUIDirtySystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<GridUIPositionSystem>());
            // buttons
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<LabelInitializeSystem>());
            // panels
            // systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<PanelTooltipSetSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<PanelInitializeSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<SubPanelInitializeSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<MeshUIDirtySystem>());
            // Headers
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<HeaderSpawnSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<HeaderUpdateSystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<HeaderDestroySystem>());
            // ui link
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<UISpawnedSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<UILinkRemoveSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<UILinkUpdatedEndSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<UILinkDestroyOnlySystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<UILinkDestroySystem>());
            // Tooltips
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<PanelTooltipSpawnSystem>());
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<PanelTooltipDestroySystem>());
            // icons
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<IconFrameInitializeSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<IconInitializeSystem>());
            // UI.Players
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<NavigationGenerationSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<MouseRaycastUISystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<InputUISystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<TooltipDeselectSystem>());
            // Mouse UI
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<MouseUISpawnSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<MouseFollowUISystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<PanelCloseMouseUISystem>());
            // Navigation
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<NavigationAnimationEndSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<NavigationDeselectedSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<NavigationSelectedSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<SelectedUIFillColorSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<SelectedUIFrameColorSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<UIRaycastDisableSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<UISelectionEndSystem>());
        }

        private void AddText(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup, LateSimulationSystemGroup lateSimulationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<ZigelSystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<RenderTextSetSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<RenderTextResizedSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<RenderTextClearSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<RenderTextSpawnSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<RenderTextSpawnedSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ZigelInitializeSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<ZigelZenderInitializeSystem>());
            // animated text
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<AnimatedTextSystem>());
            // destroy
            lateSimulationSystemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<RenderTextDestroySystem>());
        }

        private void AddGameUI(Unity.Entities.World entitiesWorld, SimulationSystemGroup simulationSystemGroup,
            LateSimulationSystemGroup lateSimulationSystemGroup)
        {
            var systemGroup = entitiesWorld.GetOrCreateSystem<RealmUISystemGroup>();
            simulationSystemGroup.AddSystemToUpdateList(systemGroup);
            // Titlescreen
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<TitlescreenUISpawnSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<TitlescreenProgressedSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<TitlescreenRespawnSystem>());
            // main menu
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<MainMenuSpawnSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<MainMenuPlayerOneSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<PlayersMainMenuSpawnSystem>());
            // device ui
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<PlayerDevicesRefreshSystem>());
            systemGroup.AddSystemToUpdateList(entitiesWorld.GetOrCreateSystem<PlayerDeviceIconInitializeSystem>());
        }
    }
}
#endif

// initializationSystemGroup.AddUnmanagedSystemToUpdateList(entitiesWorld.Unmanaged.CreateUnmanagedSystem(entitiesWorld, typeof(WorldUpdateAllocatorResetSystem)));
/*var unmanagedTypes = new System.Collections.Generic.List<Type>();
unmanagedTypes.Add(typeof(WorldUpdateAllocatorResetSystem));
var handles = entitiesWorld.Unmanaged.GetOrCreateUnmanagedSystem(entitiesWorld, unmanagedTypes);
initializationSystemGroup.AddUnmanagedSystemToUpdateList(handles[0]);
handles.Dispose();*/