using UnityEngine;
using Unity.Entities;
using Zoxel.UI;

namespace Zoxel.Realms.UI
{
    //! Tests RTS game on start screen.
    /**
    *   The OnStartScreenEvent will triggger. When it trigggers, it will trigger this.
    *       - Spawns a RTS camera.
    *       - Spawns a Realm with saving disabled.
    */
    public partial class TestRTSStartScreen : MonoBehaviour
    {
        public static TestRTSStartScreen instance;

        public void Awake()
        {
            instance = this;
        }

        public void OnStartScreen()
        {
            
        }
    }
}