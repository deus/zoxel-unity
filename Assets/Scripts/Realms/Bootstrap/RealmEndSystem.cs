using Unity.Entities;
using Unity.Burst;
using Zoxel.Rendering;
using Zoxel.Cameras;
using Zoxel.UI;
// Destroy All things
using Zoxel.Items;
using Zoxel.Bullets;
using Zoxel.Characters;
using Zoxel.Stats;
using Zoxel.Players;
using Zoxel.Players.UI;
using Zoxel.Weather;

// At the end of fading out event!
// Just create a RealmEnd event and let other modules clean up from that.

namespace Zoxel.Realms.UI
{
    //! Ends the realm! Starts the game again from the main menu.
    /**
    *   \todo Make this only destroy things connected to the worlds in the Realm, instead of all things.
    */
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class RealmEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var elapsedTime = World.Time.ElapsedTime;
            var fadeoutScreenTime = UIManager.instance.uiSettings.fadeoutScreenTime; // 1f;
            var fadeoutScreenWaitTime = UIManager.instance.uiSettings.fadeoutScreenWaitTime; // 1f;
            var playerHome = PlayerSystemGroup.playerHome;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref EndRealm endRealm, in PlayerHomeLink playerHomeLink, in RealmLink realmLink) =>
            {
                var playerHome = playerHomeLink.playerHome;
                if (endRealm.endRealmState == 0)
                {
                    endRealm.endRealmState = 1;
                    endRealm.timeStarted = elapsedTime;
                    PostUpdateCommands.AddComponent<FadeOutPlayerScreens>(entityInQueryIndex, playerHome);
                }
                else if (endRealm.endRealmState == 1)
                {
                    if (elapsedTime - endRealm.timeStarted >= fadeoutScreenTime)
                    {
                        endRealm.endRealmState = 2;
                        endRealm.timeStarted = elapsedTime;
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, realmLink.realm);
                        PostUpdateCommands.AddComponent<DestroyPlayerCharacters>(entityInQueryIndex, playerHome);
                        PostUpdateCommands.AddComponent<DestroyAllClouds>(entityInQueryIndex, PostUpdateCommands.CreateEntity(entityInQueryIndex));
                        PostUpdateCommands.AddComponent<DestroyAllItems>(entityInQueryIndex, PostUpdateCommands.CreateEntity(entityInQueryIndex));
                        PostUpdateCommands.AddComponent<DestroyAllSummonedCharacters>(entityInQueryIndex, PostUpdateCommands.CreateEntity(entityInQueryIndex));
                        var destroyAllBulletsEntity = PostUpdateCommands.CreateEntity(entityInQueryIndex);
                        PostUpdateCommands.AddComponent<DestroyAllBullets>(entityInQueryIndex, destroyAllBulletsEntity);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, destroyAllBulletsEntity, new DelayEvent(elapsedTime, 0.1));
                    }
                }
                else if (endRealm.endRealmState == 2)
                {
                    if (elapsedTime - endRealm.timeStarted >= fadeoutScreenWaitTime)
                    {
                        endRealm.endRealmState = 3;
                        endRealm.timeStarted = elapsedTime;
                        PostUpdateCommands.AddComponent<FadeInPlayerScreens>(entityInQueryIndex, playerHome);
                    }
                }
                else if (endRealm.endRealmState == 3)
                {
                    if (elapsedTime - endRealm.timeStarted >= fadeoutScreenTime)
                    {
                        PostUpdateCommands.AddComponent<PlayersSpawnMainMenu>(entityInQueryIndex, playerHome);
                        PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}