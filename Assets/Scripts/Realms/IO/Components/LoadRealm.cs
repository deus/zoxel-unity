using Unity.Entities;

namespace Zoxel.Realms
{
    //! Event for loading realm.
    public struct LoadRealm : IComponentData
    {
        public Entity player;
        public int realmID;
        public Text realmName;
    }
}