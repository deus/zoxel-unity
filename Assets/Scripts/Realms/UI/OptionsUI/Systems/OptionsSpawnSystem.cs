using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Rendering.Authoring;
using Zoxel.Cameras;

namespace Zoxel.Realms.UI
{
    //! Spawns the OptionsUI.
    /**
    *   - UI Spawn System -
    *   \todo Add [x] close button instead of back!
    *   \todo Add Slider for RenderDistance/FOV/Music/Sound.
    *   \todo Add Toggle buttons for Off/On uis.
    *   Write text like a book would be written by the AI
    */
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class OptionsSpawnSystem : SystemBase
    {
        private NativeArray<Text> texts;
        private NativeArray<Text> extras;
        private float2 panelSize;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity spawnPanelPrefab;
        private Entity panelPrefab;
        private Entity buttonPrefab;
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            texts = new NativeArray<Text>(11, Allocator.Persistent);
            texts[0] = new Text("+ Music Volume ");
            texts[1] = new Text("- Music Volume ");
            texts[2] = new Text("+ Sound Volume ");
            texts[3] = new Text("- Sound Volume ");
            texts[4] = new Text("+ Render Distance ");
            texts[5] = new Text("- Render Distance ");
            texts[6] = new Text("- Fov ");
            texts[7] = new Text("+ Fov ");
            texts[8] = new Text("Twitter");
            texts[9] = new Text("Back");
            texts[10] = new Text("Options");
            extras = new NativeArray<Text>(4, Allocator.Persistent);
            extras[0] = new Text();
            extras[1] = new Text();
            extras[2] = new Text();
            extras[3] = new Text();
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnOptionsUI),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnPanelPrefab = EntityManager.CreateEntity(spawnArchetype);
            uiHoldersQuery = GetEntityQuery(
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<UILink>());
            RequireForUpdate(processQuery);
            RequireForUpdate<RenderSettings>();
            RequireForUpdate<SoundSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var renderSettings = GetSingleton<RenderSettings>();
            var soundSettings = GetSingleton<SoundSettings>();
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var headerStyle = uiDatam.headerStyle.GetStyle(uiScale);
            for (int i = 0; i < this.extras.Length; i++)
            {
                this.extras[i].Dispose();
            }
            this.extras[0] = new Text("" + (int) AudioManager.instance.musicLevel);
            this.extras[1] = new Text("" + (int) soundSettings.soundLevel);
            this.extras[2] = new Text("" + (int) renderSettings.renderDistance);
            this.extras[3] = new Text("" + (int) CameraManager.instance.cameraSettings.fieldOfView);
            var extras = this.extras;
            var texts = this.texts;
            HeaderSpawnSystem.InitializePrefabs(EntityManager);
            var headerPrefab = HeaderSpawnSystem.headerPrefab;
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var panelPrefab = this.panelPrefab;
            var buttonPrefab = this.buttonPrefab;
            var panelSize = this.panelSize;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uiHolderEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
            uiHolderEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<SpawnOptionsUI>()
                .ForEach((int entityInQueryIndex, in CharacterLink characterLink) =>
            {
                var texts2 = new NativeArray<Text>(texts.Length - 1, Allocator.Temp);
                for (byte i = 0; i < texts2.Length; i++)
                {
                    var clone = texts[i].Clone();
                    if (i < 8)
                    {
                        Text extra;
                        if (i == 0 || i == 1)
                        {
                            extra = extras[0];
                        }
                        else if (i == 2 || i == 3)
                        {
                            extra = extras[1];
                        }
                        else if (i == 4 || i == 5)
                        {
                            extra = extras[2];
                        }
                        else // if (i == 6 || i == 7)
                        {
                            extra = extras[3];
                        }
                        clone.AddText(in extra);
                    }
                    texts2[i] = clone;
                }
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, panelPrefab);
                PostUpdateCommands.AddComponent(entityInQueryIndex, e3, new ListPrefabLink(buttonPrefab));
                PostUpdateCommands.AddComponent(entityInQueryIndex, e3, new SpawnButtonsList(in texts2, 0));
                texts2.Dispose();
                var headerText = texts[10].Clone();
                HeaderSpawnSystem.SpawnHeaderUI(PostUpdateCommands, entityInQueryIndex, headerPrefab, e3, panelSize, headerStyle.fontSize, headerStyle.textPadding,
                    in headerText);
                if (characterLink.character.Index > 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, characterLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab),
                        new OnUISpawned(characterLink.character, 1));
                    if (cameraLinks.HasComponent(characterLink.character))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLinks[characterLink.character]);
                    }
                }
			})  .WithReadOnly(texts)
                .WithReadOnly(extras)
                .WithReadOnly(cameraLinks)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private void InitializePrefabs()
        {
            if (panelPrefab.Index != 0)
            {
                return;
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var buttonStyle = uiDatam.buttonStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var iconSize = uiDatam.GetIconSize(uiScale) * 0.36f;
            var padding = iconSize * 0.4f;
            var margins = iconSize * 1.6f;
            var minSize = new float2(9 * iconSize.x, 0);
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            // panel
            panelPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(panelPrefab);
            EntityManager.RemoveComponent<SpawnHeaderUI>(panelPrefab);
            EntityManager.AddComponent<OptionsUI>(panelPrefab);
            EntityManager.SetComponentData(panelPrefab, new PanelUI(PanelType.Options));
            var gridUI = new GridUI();
            gridUI.padding = padding;
            gridUI.margins = margins;
            // gridUI.iconSize = new float2(iconSize.x * 22 + textPadding.x * 2f, iconSize.y + textPadding.y * 2f);
            gridUI.gridSize = new int2(1, texts.Length - 1);
            var gridUISize = new GridUISize(new float2(iconSize.x * 22 + textPadding.x * 2f, iconSize.y + textPadding.y * 2f));
            EntityManager.SetComponentData(panelPrefab, gridUI);
            EntityManager.SetComponentData(panelPrefab, gridUISize);
            this.panelSize = gridUI.GetSize(gridUISize.size);
            EntityManager.SetComponentData(panelPrefab, new Size2D(this.panelSize));
            // button
            buttonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(buttonPrefab);
            EntityManager.AddComponent<OptionsButton>(buttonPrefab);
            UICoreSystem.SetRenderTextData(EntityManager, buttonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, iconSize.y, textPadding);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
            for (int i = 0; i < extras.Length; i++)
            {
                extras[i].Dispose();
            }
            extras.Dispose();
        }
    }
}