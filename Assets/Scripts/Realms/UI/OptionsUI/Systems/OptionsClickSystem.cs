using Unity.Entities;
using Unity.Burst;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Cameras;

namespace Zoxel.Realms.UI
{
    //! Handles OptionsButton UIClickEvent.
    /**
    *   - UIClickEvent System -
    */
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class OptionsClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var spawnMainMenuPrefab = MainMenuSpawnSystem.spawnPanelPrefab;
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var spawnCharacterUIPrefab = UICoreSystem.spawnCharacterUIPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<OptionsButton, UIClickEvent>()
                .ForEach((Entity e, int entityInQueryIndex, in Child child, in UIClickEvent uiClickEvent, in ParentLink parentLink) =>
            {
                var buttonType = uiClickEvent.buttonType;
                var arrayIndex = child.index;
                var character = uiClickEvent.controller;
                var ui = parentLink.parent;
                if (ui.Index == 0 || !HasComponent<UILink>(character))
                {
                    //UnityEngine.Debug.LogError("Player is null.");
                    return;
                }
                var goToMainMenu = false;
                if (buttonType == ButtonActionType.Cancel)
                {
                    goToMainMenu = true;
                }
                else if (buttonType == ButtonActionType.Confirm)
                {
                    if (arrayIndex == 0)
                    {
                        PostUpdateCommands.AddComponent<IncreaseMusicVolume>(entityInQueryIndex, e);
                    }
                    else if (arrayIndex == 1)
                    {
                        PostUpdateCommands.AddComponent<DecreaseMusicVolume>(entityInQueryIndex, e);
                    }
                    else if (arrayIndex == 2)
                    {
                        PostUpdateCommands.AddComponent<IncreaseSoundVolume>(entityInQueryIndex, e);
                    }
                    else if (arrayIndex == 3)
                    {
                        PostUpdateCommands.AddComponent<DecreaseSoundVolume>(entityInQueryIndex, e);
                    }
                    else if (arrayIndex == 4)
                    {
                        PostUpdateCommands.AddComponent<IncreaseRenderDistance>(entityInQueryIndex, e);
                    }
                    else if (arrayIndex == 5)
                    {
                        PostUpdateCommands.AddComponent<DecreaseRenderDistance>(entityInQueryIndex, e);
                    }
                    else if (arrayIndex == 6)
                    {
                        PostUpdateCommands.AddComponent<SettingsDecreaseFov>(entityInQueryIndex, e);
                    }
                    else if (arrayIndex == 7)
                    {
                        PostUpdateCommands.AddComponent<SettingsIncreaseFov>(entityInQueryIndex, e);
                    }
                    else if (arrayIndex == 8)
                    {
                        PostUpdateCommands.AddComponent<OpenTwitter>(entityInQueryIndex, PostUpdateCommands.CreateEntity(entityInQueryIndex));
                    }
                    else
                    {
                        goToMainMenu = true;
                    }
                }
                if (goToMainMenu)
                {
                    var removeUIEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, removeCharacterUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, removeUIEntity, new RemoveUI(character, ui));
                    var spawnMainMenuEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnMainMenuPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMainMenuEntity, new CharacterLink(character));
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}