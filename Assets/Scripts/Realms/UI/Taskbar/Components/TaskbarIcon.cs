using Unity.Entities;

namespace Zoxel.Realms.UI
{
    //! Taskbar Icon tag.
    public struct TaskbarIcon : IComponentData { }
}