using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;
using Zoxel.Transforms;
using Zoxel.Textures;

namespace Zoxel.Realms.UI
{
    //! Spawns a Map UI!
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class TaskbarSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private Entity taskbarPrefab;
        public IconPrefabs iconPrefabs;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.ReadOnly<SpawnTaskbar>(),
                ComponentType.ReadOnly<CharacterLink>(),
                ComponentType.Exclude<DelayEvent>());
            RequireForUpdate(processQuery);
        }

        private void InitializePrefabs()
        {
            if (taskbarPrefab.Index != 0)
            {
                return;
            }
            taskbarPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(taskbarPrefab);
            EntityManager.AddComponent<Taskbar>(taskbarPrefab);
            EntityManager.SetComponentData(taskbarPrefab, new PanelUI(PanelType.Taskbar));
            EntityManager.SetComponentData(taskbarPrefab, new UIAnchor(AnchorUIType.TopMiddle));
            EntityManager.AddComponentData(taskbarPrefab, new ToggleGroup(255));
            EntityManager.AddComponent<SetToggleGroup>(taskbarPrefab);
            EntityManager.RemoveComponent<SpawnHeaderUI>(taskbarPrefab);
            EntityManager.RemoveComponent<SpawnPanelTooltip>(taskbarPrefab);
            iconPrefabs.frame = EntityManager.Instantiate(UICoreSystem.iconPrefabs.frame);
            EntityManager.AddComponent<Prefab>(iconPrefabs.frame);
            EntityManager.RemoveComponent<SetIconData>(iconPrefabs.frame);
            EntityManager.AddComponent<TaskbarButton>(iconPrefabs.frame);
            EntityManager.AddComponent<Toggle>(iconPrefabs.frame);
            EntityManager.SetComponentData(iconPrefabs.frame, new OnChildrenSpawned(1));
            iconPrefabs.icon = EntityManager.Instantiate(UICoreSystem.iconPrefabs.icon);
            EntityManager.AddComponent<Prefab>(iconPrefabs.icon);
            EntityManager.RemoveComponent<Icon>(iconPrefabs.icon);
            EntityManager.AddComponent<AuthoredTexture>(iconPrefabs.icon);
            EntityManager.AddComponent<DontDestroyTexture>(iconPrefabs.icon);
            EntityManager.AddComponent<TaskbarIcon>(iconPrefabs.icon);
            RequireForUpdate<SoundSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var soundSettings = GetSingleton<SoundSettings>();
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var soundVolume = soundSettings.uiSoundVolume;
            var taskbarPrefab = this.taskbarPrefab;
            var iconPrefabs = this.iconPrefabs;
            var uiDatam = UIManager.instance.uiDatam;
            var setStyle = uiDatam.panelStyle.GetStyle();
            var iconSize = uiDatam.GetIconSize(uiScale);
            var padding = uiDatam.GetIconPadding(uiScale); // iconSize / 8f;
            var margins = uiDatam.GetIconMargins(uiScale); // iconSize / 6f;
            // var margins = iconSize * 0.2f;
            var tabButtonsLength = uiDatam.uiTabTextures.Length;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var spawnMapEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var characterLinks = GetComponentLookup<CharacterLink>(true);
            Dependency = Entities
                .WithAll<UILink>()
                .ForEach((Entity e, int entityInQueryIndex, in CameraLink cameraLink) =>
            {
                if (cameraLink.camera.Index == 0)
                {
                    return;
                }
                for (int i = 0; i < spawnMapEntities.Length; i++)
                {
                    var e2 = spawnMapEntities[i];
                    var characterLink = characterLinks[e2];
                    if (characterLink.character != e)
                    {
                        continue;
                    }
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e2);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));
                    var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, taskbarPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CharacterLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CameraLink(cameraLink.camera));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawned((byte) tabButtonsLength));
                    for (int j = 0; j < tabButtonsLength; j++)
                    {
                        var seed = (int) (128 + elapsedTime + j * 2048 + 4196 * entityInQueryIndex);
                        var frameEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.frame, e3);
                        UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, frameEntity, e3);
                        UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, frameEntity, j);
                        UICoreSystem.SetSound(PostUpdateCommands, entityInQueryIndex, frameEntity, seed, soundVolume);
                        UICoreSystem.SetSeed(PostUpdateCommands, entityInQueryIndex, frameEntity, seed);
                        UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.icon, frameEntity);
                    }
                    var gridUI = new GridUI();
                    gridUI.gridSize = new int2(tabButtonsLength, 1);
                    gridUI.margins = margins; // new float2(iconSize.x, iconSize.y) * 0.26f;
                    gridUI.padding = padding; // new float2(iconSize.x * 0.2f, 0);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e3, gridUI);
                    break;  // only one crosshair per character atm
                }
			})  .WithReadOnly(spawnMapEntities)
                .WithDisposeOnCompletion(spawnMapEntities)
                .WithReadOnly(characterLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}