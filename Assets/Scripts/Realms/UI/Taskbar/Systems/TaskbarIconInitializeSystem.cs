using Unity.Entities;
using Zoxel.Rendering;
using Zoxel.Transforms;
using Zoxel.UI;

namespace Zoxel.Realms.UI
{
    //! Initializes a Crosshair materials.
    [UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class TaskbarIconInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var uiDatam = UIManager.instance.uiDatam;
            var uiTabTextures = uiDatam.uiTabTextures;
            var iconMaterial = UIManager.instance.materials.iconMaterial;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, TaskbarIcon>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in Size2D size2D, in ParentLink parentLink) =>
            {
                zoxMesh.layer = worldUILayer;
                if (size2D.size.x != 0 && size2D.size.y != 0)
                {
                    zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size2D.size, 0, 0);
                }
                zoxMesh.material = new UnityEngine.Material(iconMaterial);
                var child = EntityManager.GetComponentData<Child>(parentLink.parent);
                //UnityEngine.Debug.LogError("Set TabTexture: " + child.index);
                zoxMesh.material.SetTexture("_BaseMap", uiTabTextures[child.index]);
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
                #if UNITY_EDITOR
                if (!UnityEditor.EditorApplication.isPlaying && ZoxelEditorSettings.isEditorMeshes)
                {
                    PostUpdateCommands.AddComponent<InitializeEditorMesh>(e);
                    PostUpdateCommands.AddComponent<EditorMesh>(e);
                }
                #endif
            }).WithoutBurst().Run();
        }
    }
}