﻿using Unity.Entities;

namespace Zoxel.Realms.UI
{
    //! RealmUISystemGroup contains things for the player ui.
    // [UpdateAfter(typeof(RealmSystemGroup))]
    [UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class RealmUISystemGroup : ComponentSystemGroup { }
}
