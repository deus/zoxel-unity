﻿// todo: Rework this per Manager
//      This monobehaviour will just manager the other UI code per manager
//      Otherwise it's too speghetti atm and too hard to maintain
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine.InputSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Zoxel.Voxels;
using Zoxel.Voxels.Lighting;
using Zoxel.Voxels.Grasses;
using Zoxel.Worlds;
using Zoxel.AI;
using Zoxel.Audio;
using Zoxel.UI;
using Zoxel.Cameras;
using Zoxel.Networking;
using Zoxel.Movement;
using Zoxel.Animations;
using Zoxel.Items;
using Zoxel.VoxelInteraction;
using Zoxel.VoxelInteraction.Debug;
using Zoxel.Transforms;
using Zoxel.Stats;
using Zoxel.Stats.UI;
using Zoxel.Skills;
using Zoxel.Actions;
using Zoxel.Particles;
using Zoxel.Maps;
using Zoxel.Characters;
using Zoxel.Players.UI;
using Zoxel.Players;
using Zoxel.Portals;
using Zoxel.Actions.UI;
using Zoxel.Quests;
using Zoxel.Quests.UI;
using Zoxel.Rendering;
using Zoxel.Movement.Debug;
using Zoxel.Realms;
using Zoxel.Realms.UI;
using Zoxel.Input;
// UnityEditor.UnityStats.triangles
// UnityEditor.UnityStats.vertices

namespace Zoxel
{
    //! \todo Remove this monstrosity. Split into seperate debug classes.
    public class BootGui : UnityEngine.MonoBehaviour
    {
        public static BootGui instance;
        public bool isDebugging;
        public bool isDisableController;
        public BootUIType debugGUIType;
        private float deltaTime = 0.0f;
        public int fontSize = 34;
        public UnityEngine.Color fontColor = UnityEngine.Color.green;
        public UnityEngine.Font font;
        public bool isDrawTargetLine;
        private BasicConsole basicConsole;
        public ScreenSettings screenSettings;   // for monitor
        // Generic toggles used per UnityEngine.GUI
        public static bool toggleA;
        public static bool toggleB;
        public static bool toggleC;

        public bool HasEntityManager
        {
            get
            {
                return World.DefaultGameObjectInjectionWorld != null;
            }
        }

        public EntityManager EntityManager
        {
            get
            {
                if (World.DefaultGameObjectInjectionWorld == null)
                {
                    return new EntityManager();
                }
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        public Entity Realm
        {
            get
            { 
                return RealmSystemGroup.realm;
            }
        }

        void DrawMenuUI()
        {
            if (UnityEngine.GUILayout.Button("FPS"))
            {
                debugGUIType = BootUIType.FPS;
            }
            if (UnityEngine.GUILayout.Button("Console"))
            {
                debugGUIType = BootUIType.BasicConsole;
            }
            if (UnityEngine.GUILayout.Button("Networking"))
            {
                debugGUIType = BootUIType.Networking;
            }

            // options
            UnityEngine.GUILayout.Label("===== Options =====");
            if (UnityEngine.GUILayout.Button("Voxels"))
            {
                debugGUIType = BootUIType.VoxelOptions;
            }
            if (UnityEngine.GUILayout.Button("Items"))
            {
                debugGUIType = BootUIType.ItemOptions;
            }
            if (UnityEngine.GUILayout.Button("Stats"))
            {
                debugGUIType = BootUIType.StatsOptions;
            }
            if (UnityEngine.GUILayout.Button("Characters"))
            {
                debugGUIType = BootUIType.CharacterOptions;
            }


            // Information
            UnityEngine.GUILayout.Label("===== Realm =====");
            if (UnityEngine.GUILayout.Button("Game Stats"))
            {
                debugGUIType = BootUIType.GameStats;
            }
            if (UnityEngine.GUILayout.Button("Realm Items"))
            {
                debugGUIType = BootUIType.GameItems;
            }

            // Player
            
            if (UnityEngine.GUILayout.Button("Gravity"))
            {
                debugGUIType = BootUIType.Gravity;
            }
            UnityEngine.GUILayout.Label("===== Player =====");
            if (UnityEngine.GUILayout.Button("Raycasting"))
            {
                debugGUIType = BootUIType.VoxelSelection;
            }
            if (UnityEngine.GUILayout.Button("Input"))
            {
                debugGUIType = BootUIType.Input;
            }
            if (UnityEngine.GUILayout.Button("Player Animator"))
            {
                debugGUIType = BootUIType.PlayerAnimator;
            }
            if (UnityEngine.GUILayout.Button("Player Target"))
            {
                debugGUIType = BootUIType.PlayerTarget;
            }
            if (UnityEngine.GUILayout.Button("Player Stats"))
            {
                debugGUIType = BootUIType.PlayerStats;
            }
            if (UnityEngine.GUILayout.Button("Player Inventory"))
            {
                debugGUIType = BootUIType.PlayerInventory;
            }
            if (UnityEngine.GUILayout.Button("Player Traveler"))
            {
                debugGUIType = BootUIType.PlayerTraveler;
            }
            if (UnityEngine.GUILayout.Button("Player UserActionLinks"))
            {
                debugGUIType = BootUIType.PlayerActions;
            }
            if (UnityEngine.GUILayout.Button("Uis"))
            {
                debugGUIType = BootUIType.PlayerUIs;
            }
            if (UnityEngine.GUILayout.Button("Collider"))
            {
                debugGUIType = BootUIType.PlayerCollider;
            }

            // Data
            UnityEngine.GUILayout.Label("===== Data =====");
            if (UnityEngine.GUILayout.Button("Realm Items"))
            {
                debugGUIType = BootUIType.GameItems;
            }
            if (UnityEngine.GUILayout.Button("Realm Voxels"))
            {
                debugGUIType = BootUIType.GameVoxels;
            }

            // Options
            UnityEngine.GUILayout.Label("===== Options =====");
            if (UnityEngine.GUILayout.Button("Rendering"))
            {
                debugGUIType = BootUIType.RenderOptions;
            }
            if (UnityEngine.GUILayout.Button("Audio"))
            {
                debugGUIType = BootUIType.AudioOptions;
            }
            if (UnityEngine.GUILayout.Button("Video"))
            {
                debugGUIType = BootUIType.VideoOptions;
            }
            
            UnityEngine.GUILayout.Label("===== ===== ===== ===== ===== =====");

            if (UnityEngine.GUILayout.Button("Cheats"))
            {
                debugGUIType = BootUIType.CheatMode;
            }
        }

        void Awake()
        {
            instance = this;
            basicConsole = new BasicConsole(40);
        }

        public void Update()
        {
            if (!HasEntityManager)
            {
                return;
            }
            deltaTime += (UnityEngine.Time.unscaledDeltaTime - deltaTime) * 0.1f;
            HandleInput();
            if (isDrawTargetLine)
            {
                DebugPlayerTarget();
            }
            var isStatistics = debugGUIType == BootUIType.VoxelOptions && VoxelManager.instance.voxelSettings.isStatistics;
            if (!isStatistics && (int) debugGUIType >= (int) BootUIType.Menu && (int) debugGUIType <= (int) BootUIType.OptionsEnd)
            {
                if (!EntityManager.Exists(PlayerSystemGroup.playerHome))
                {
                    return;
                }
                var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
                if (playerLinks.players.Length > 0)
                {
                    var player = playerLinks.players[0];
                    if (isDisableController && !EntityManager.HasComponent<ControllerDisabled>(player))
                    {
                        EntityManager.AddComponent<ControllerDisabled>(player);
                        basicConsole.ClearInput();    // clear input
                    }
                    var game = Realm;
                    basicConsole.UpdateConsole(EntityManager, game, player);
                }
            }
            else
            {
                if (!EntityManager.Exists(PlayerSystemGroup.playerHome))
                {
                    return;
                }
                var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
                if (playerLinks.players.Length > 0)
                {
                    var player = playerLinks.players[0];
                    if (isDisableController && EntityManager.HasComponent<ControllerDisabled>(player))
                    {
                        EntityManager.RemoveComponent<ControllerDisabled>(player);
                    }
                }
            }
        }

        public void OnGUI()
        {
            if (!HasEntityManager)
            {
                return;
            }
            if (debugGUIType == BootUIType.None)
            {
                return;
            }
            if (!font)
            {
                UnityEngine.Debug.LogError("No font found, assign one in the inspector.");
                return;
            }
            UnityEngine.GUI.skin.font = font;
            UnityEngine.GUI.skin.label.fontSize = fontSize;
            UnityEngine.GUI.color = fontColor;
            if (debugGUIType == BootUIType.Menu)
            {
                if (UnityEngine.GUILayout.Button("Exit"))
                {
                    debugGUIType = BootUIType.None;
                }
                DrawMenuUI();
                return;
            }
            if (UnityEngine.GUILayout.Button("Back"))
            {
                debugGUIType = BootUIType.Menu;
            }
            else if (debugGUIType == BootUIType.FPS)
            {
                DrawFPS();
            }
            if (debugGUIType == BootUIType.BasicConsole)
            {
                UnityEngine.GUI.skin.label.fontSize = fontSize / 2;
                basicConsole.DrawConsole();
            }
            /*else if (debugGUIType == BootUIType.Networking)
            {
                NetworkingGui.instance.DrawUI();
            }*/
            else if (debugGUIType == BootUIType.CheatMode)
            {
                //StatsManager.instance.realStatSettings.DrawStatsCheatsUI();
                // SkillsManager.instance.realSkillsSettings.DrawCheatsUI();
            }
            else if (debugGUIType == BootUIType.StatsOptions)
            {
                //StatsManager.instance.realStatSettings.DrawStatsUI();
            }
            else if (debugGUIType == BootUIType.VoxelOptions)
            {
                VoxelManager.instance.DrawUI();
            }
            /*else if (debugGUIType == BootUIType.ItemOptions)
            {
                ItemsManager.instance.DrawUI();
            }
            else if (debugGUIType == BootUIType.CharacterOptions)
            {
                CharacterManager.instance.DrawUI();
            }*/
            
            else if (debugGUIType == BootUIType.TouchInput)
            {
                DrawTouchUI();
            }
            else if (debugGUIType == BootUIType.MapBiomes)
            {
                ShowMapBiomesUI();
            }
            else if (debugGUIType == BootUIType.MapVoxels)
            {
                ShowMapVoxelsUI();
            }
            else if (debugGUIType == BootUIType.Input)
            {
                DrawPlayerInputs();
            }
            else if (debugGUIType == BootUIType.PlayerUIs)
            {
                DrawPlayerUIs();
            }
            else if (debugGUIType == BootUIType.PlayerCollider)
            {
                DrawPlayerCollider();
            }
            
            else if (debugGUIType == BootUIType.Gravity)
            {
                GravityDebugger.instance.DebugGravity();
            }
            
            else if (debugGUIType == BootUIType.PlayerStats)
            {
                DrawPlayerStats();
            }
            else if (debugGUIType == BootUIType.PlayerInventory)
            {
                DrawPlayerInventory();
            }
            else if (debugGUIType == BootUIType.PlayerTraveler)
            {
                DrawPlayerTraveler();
            }
            else if (debugGUIType == BootUIType.PlayerActions)
            {
                DrawPlayerActions();
            }
            else if (debugGUIType == BootUIType.PlayerTarget)
            {
                DebugTarget.instance.DebugTargetUI();
            }
            else if (debugGUIType == BootUIType.PlayerAnimator)
            {
                DebugAnimator();
            }
            else if (debugGUIType == BootUIType.VoxelSelection)
            {
                RaycastDebugger.instance.DebugRaycasting();
            }
            else if (debugGUIType == BootUIType.VideoOptions)
            {
                screenSettings.fullScreenMode = ScreenUtil.DrawGUI(screenSettings.fullScreenMode);
            }
            else if (debugGUIType == BootUIType.AudioOptions)
            {
                AudioOptions();
            }
            /*else if (debugGUIType == BootUIType.MouseOptions)
            {
                CameraManager.instance.realMouseSettings.DrawGUI();
            }*/
            else if (debugGUIType == BootUIType.RenderOptions)
            {
                // MaterialsManager.instance.renderSettings.DrawGUI(EntityManager, Realm);
            }
            else if (debugGUIType == BootUIType.PlayerControllers)
            {
                DrawPlayerControllersUI();
            }
            else if (debugGUIType == BootUIType.GameStats)
            {
                DrawGameStatsUI();
            }
            else if (debugGUIType == BootUIType.GameItems)
            {
                DrawGameItemsUI();
            }
            else if (debugGUIType == BootUIType.GameVoxels)
            {
                DrawGameVoxelsUI();
            }
            else if (debugGUIType == BootUIType.Tilemap)
            {
                DrawWorldTilemaps();
            }
            else if (debugGUIType == BootUIType.GameData)
            {
                //UnityEngine.GUILayout.Label(GetFPSText());
                // UnityEngine.GUILayout.Label("Debugging Data [" + booty.data.name + "]");
                // UnityEngine.GUILayout.Label("Characters " + booty.data.characters.Count);
                // UnityEngine.GUILayout.Label("Turrets " + booty.data.turrets.Count);
                UnityEngine.GUILayout.Label("Stats " + StatsManager.instance.StatSettings.stats.Length);
                UnityEngine.GUILayout.Label("Items " + ItemsManager.instance.ItemsSettings.items.Length);
                UnityEngine.GUILayout.Label("Slots " + ItemsManager.instance.ItemsSettings.slots.Length);
                UnityEngine.GUILayout.Label("Quests " + 0);// data.quests.Count);
                //UnityEngine.GUILayout.Label("Dialogues " + booty.data.dialogues.Count);
            }
            else if (debugGUIType == BootUIType.NPCAI)
            {
                // DebugAIStates();
            }
        }

        
        private void DrawWorldTilemaps()
        {
            //UnityEngine.GUILayout.Label("Realm Tilemaps");
            var realmEntity = Realm;
            if (EntityManager.Exists(realmEntity))
            {
               //  var game = EntityManager.GetComponentData<Realm>(realmEntity);
                var mapEntity = new Entity();
                var worldLinks = EntityManager.GetComponentData<WorldLinks>(realmEntity);
                if (worldLinks.worlds.Length > 0)
                {
                    mapEntity = worldLinks.worlds[0];
                }
                
                if (EntityManager.Exists(mapEntity))
                {
                    var entityMaterials = EntityManager.GetSharedComponentManaged<EntityMaterials>(mapEntity);
                    UnityEngine.GUILayout.Label("Planet [0] Materials: " + entityMaterials.materials.Length);
                    for (int i = 0; i < entityMaterials.materials.Length; i++)
                    {
                        var material = entityMaterials.materials[i];
                        var texture = material.GetTexture("_BaseMap");
                        if (texture)
                        {
                            UnityEngine.GUILayout.Label("Planet [0] Material [" + i + "] Texture: " + texture.width + ":" + texture.height);
                            UnityEngine.GUI.color = UnityEngine.Color.white;
                            UnityEngine.GUI.DrawTexture(new UnityEngine.Rect(10, 100, texture.width * 8, texture.height * 8), texture); // , ScaleMode.ScaleToFit, true, 10.0F);
                            UnityEngine.GUI.color = fontColor;
                        }
                    }
                }
            }
        }

        private void DrawPlayerControllersUI()
        {
            DrawFPS();
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            UnityEngine.GUILayout.Label("   > Players [" + playerLinks.players.Length + "]");
            for (int i = 0; i < playerLinks.players.Length; i++)
            {
                var player = playerLinks.players[i];
                if (!EntityManager.HasComponent<Controller>(player))
                {
                    continue;
                }
                var controller = EntityManager.GetComponentData<Controller>(player);
                var deviceTypeData = EntityManager.GetComponentData<DeviceTypeData>(player);
                // UnityEngine.GUILayout.Label("   > controller [" + controller.deviceID + "]");
                UnityEngine.GUILayout.Label("       - deviceType [" + (deviceTypeData.type) + "]");
                UnityEngine.GUILayout.Label("       - mapping [" + (controller.mapping) + "]");

                UnityEngine.GUILayout.Label("       - pointer [" + controller.mouse.position.x + " x " + controller.mouse.position.y + "]");

                UnityEngine.GUILayout.Label("       - leftStick [" + controller.gamepad.leftStick.x + " x " + controller.gamepad.leftStick.y + "]");
                UnityEngine.GUILayout.Label("       - rightStick [" + controller.gamepad.rightStick.x + " x " + controller.gamepad.rightStick.y + "]");
                
                UnityEngine.GUILayout.Label("       - buttonRT [" + controller.gamepad.buttonRT + "]");

                // touch
                //UnityEngine.GUILayout.Label("       - touching states [" + controller.primaryTouch.touchState + " x " + controller.secondaryTouch.touchState + "]");
                //UnityEngine.GUILayout.Label("       - first touch [" + controller.primaryTouch.firstTouchPosition.x + " x " + controller.primaryTouch.firstTouchPosition.y + "]");
                //UnityEngine.GUILayout.Label("       - second touch [" + controller.secondaryTouch.firstTouchPosition.x + " x " + controller.secondaryTouch.firstTouchPosition.y + "]");
                
                //UnityEngine.GUILayout.Label("       - touchDelta [" + controller.touchpad.touchDelta.x + " x " + controller.touchpad.touchDelta.y + "]");
                // UnityEngine.GUILayout.Label("       - screenDimensions [" + controller.screenDimensions.x + " x " + controller.screenDimensions.y + "]");
            }
        }

        private int openItem = -1;
        private int pageNumber;
        const int itemsPerPage = 20;

        private void DrawGameItemsUI()
        {
            var realmEntity = Realm;
            if (EntityManager.Exists(realmEntity))
            {
                var itemLinks = EntityManager.GetComponentData<ItemLinks>(realmEntity);
                UnityEngine.GUILayout.Label("Realm Items: " + itemLinks.items.Length);
                
                if (openItem != -1)
                {
                        var gameItem = EntityManager.GetComponentData<Item>(itemLinks.items[openItem]);
                        var itemName = EntityManager.GetComponentData<ZoxName>(itemLinks.items[openItem]).name;
                        UnityEngine.GUILayout.Label("   -> [" + openItem + "] " + itemName.ToString());
                        // UnityEngine.GUILayout.Label("     - type: " + ((ItemTypeEditor)gameItem.type).ToString());
                        /*if (gameItem.voxelType != 0)
                        {
                            UnityEngine.GUILayout.Label("     - Voxel Type: " + gameItem.voxelType);
                        }*/
                        // if has Vox (later for ones without it)
                        var itemVox = EntityManager.GetComponentData<Chunk>(itemLinks.items[openItem]);
                        UnityEngine.GUILayout.Label("     - size: " + itemVox.voxelDimensions.ToString());
                        if (UnityEngine.GUILayout.Button("Close"))
                        {
                            openItem = -1;
                        }
                }
                else
                {
                    var maxPages = itemLinks.items.Length / itemsPerPage;
                    if (pageNumber != 0 && UnityEngine.GUILayout.Button("Previous Page"))
                    {
                        pageNumber--;
                    }
                    if (pageNumber != maxPages && UnityEngine.GUILayout.Button("Next Page"))
                    {
                        pageNumber++;
                    }
                    UnityEngine.GUILayout.Label("Page [" + pageNumber + "]");
                    for (int i = pageNumber * itemsPerPage; i < math.min(itemLinks.items.Length, pageNumber * itemsPerPage + itemsPerPage); i++)
                    {
                        var itemName = EntityManager.GetComponentData<ZoxName>(itemLinks.items[i]).name;
                        if (UnityEngine.GUILayout.Button("   -> [" + i + "] " + itemName.ToString()))
                        {
                            openItem = i;
                        }
                    }
                }
            }
        }

        private void DrawGameVoxelsUI()
        {
            var realmEntity = Realm;
            if (EntityManager.Exists(realmEntity))
            {
                var planet = new Entity();
                var worldLinks = EntityManager.GetComponentData<WorldLinks>(realmEntity);
                if (worldLinks.worlds.Length > 0)
                {
                    planet = worldLinks.worlds[0];
                }
                if (!EntityManager.Exists(planet))
                {
                    UnityEngine.GUILayout.Label("Planet has not loaded yet.");
                    return;
                }
                var voxels = EntityManager.GetComponentData<VoxelLinks>(planet).voxels;
                UnityEngine.GUILayout.Label("Realm Voxels: " + voxels.Length);
                if (openItem >= voxels.Length)
                {
                    openItem = -1;
                }
                if (openItem != -1)
                {
                    var voxelEntity = voxels[openItem];
                    var voxel = EntityManager.GetComponentData<Voxel>(voxelEntity);
                    var voxelName = EntityManager.GetComponentData<ZoxName>(voxelEntity);
                    UnityEngine.GUILayout.Label("   -> [" + openItem + "] " + voxelName.name.ToString());
                    UnityEngine.GUILayout.Label("     - type: " + ((VoxelType)voxel.type).ToString());
                    UnityEngine.GUILayout.Label("     - health: " + voxel.startingHealth);
                    UnityEngine.GUILayout.Label("   Core");
                    UnityEngine.GUILayout.Label("     - mesh: " + EntityManager.GetComponentData<VoxelMeshType>(voxelEntity).meshType);
                    UnityEngine.GUILayout.Label("     - chunkBiome: " + voxel.biomeIndex);
                    UnityEngine.GUILayout.Label("     - color: " + voxel.color.GetColor().ToString());
                    UnityEngine.GUILayout.Label("   Behaviour");
                    // UnityEngine.GUILayout.Label("     - isEmitLight: " + voxel.isEmitLight);
                    UnityEngine.GUILayout.Label("     - Function: " + ((VoxelFunctionType)voxel.functionType));
                    if (openItem != 0 && UnityEngine.GUILayout.Button("Previous"))
                    {
                        openItem--;
                    }
                    if (openItem != (voxels.Length - 1) && UnityEngine.GUILayout.Button("Next"))
                    {
                        openItem++;
                    }
                    if (UnityEngine.GUILayout.Button("Close"))
                    {
                        openItem = -1;
                    }
                }
                else
                {
                    var maxPages = voxels.Length / itemsPerPage;
                    if (pageNumber != 0 && UnityEngine.GUILayout.Button("Previous Page"))
                    {
                        pageNumber--;
                    }
                    if (pageNumber != maxPages && UnityEngine.GUILayout.Button("Next Page"))
                    {
                        pageNumber++;
                    }
                    UnityEngine.GUILayout.Label("Page [" + pageNumber + "]");
                    for (int i = pageNumber * itemsPerPage; i < math.min(voxels.Length, pageNumber * itemsPerPage + itemsPerPage); i++)
                    {
                        var voxel = EntityManager.GetComponentData<Voxel>(voxels[i]);
                        var voxelName = EntityManager.GetComponentData<ZoxName>(voxels[i]);
                        if (UnityEngine.GUILayout.Button("   -> [" + i + "] " + voxelName.name.ToString()))
                        {
                            openItem = i;
                        }
                    }
                }
            }
        }

        private void DrawGameStatsUI()
        {
            var realmEntity = Realm;
            //UnityEngine.GUILayout.Label("Entities:");
            // todo Move these to their manager uis
            UnityEngine.GUILayout.Label("   > Grass Strands [" + GrassRenderBeginSystem.renderCount + " vs " + VoxelSystemGroup.grassStrandCount + "]");
            UnityEngine.GUILayout.Label("   > Particles [" + ParticleRenderUpdateSystem.renderCount + "]");
            // UnityEngine.GUILayout.Label("Render Buffers: todo");
            // UnityEngine.GUILayout.Label("   > Models [" + ModelRenderSystem.drawers.Count + "]");
            // UnityEngine.GUILayout.Label("   > Minivox [" + MinivoxRenderSystem.drawers.Count + "]");
            //UnityEngine.GUILayout.Label("   > UI Basics [" + UIRenderSystem.buffers.Count + "]");
            //UnityEngine.GUILayout.Label("   > UI Frames [" +  UIRenderSystem.frameBuffers.Count + "]");

            UnityEngine.GUILayout.Label("Render Data:");
            UnityEngine.GUILayout.Label("   > Total Vertices [" + ZoxelSystemStats.totalVertices + "]");
            UnityEngine.GUILayout.Label("   > Total Triangles [" + (ZoxelSystemStats.totalTriangles / 3) + "]");
            UnityEngine.GUILayout.Label("   > Total Planet Triangles [" + (ZoxelSystemStats.totalMapTriangles / 3) + "]");
            UnityEngine.GUILayout.Label("   > Total Character Triangles [" + (ZoxelSystemStats.totalCharacterTriangles / 3) + "]");
            UnityEngine.GUILayout.Label("   > Total Minivox Triangles [" + (ZoxelSystemStats.totalMinivoxTriangles / 3) + "]");
            if (!EntityManager.Exists(realmEntity))
            {
                UnityEngine.GUILayout.Label("No Realm is Loaded.");
            }
            else
            {
                var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
                // var game = EntityManager.GetComponentData<Realm>(realmEntity);
                var realmID = EntityManager.GetComponentData<ZoxID>(realmEntity).id;
                UnityEngine.GUILayout.Label("Realm: " + realmID);
                //UnityEngine.GUILayout.Label("   > Realm State [" + ((GameState)(game.state)) + "]");
                UnityEngine.GUILayout.Label("   > Players [" + playerLinks.players.Length + "]");
                var skillLinks = EntityManager.GetComponentData<SkillLinks>(realmEntity);
                UnityEngine.GUILayout.Label("   > UserSkillLinks [" + skillLinks.skills.Length + "]");
                var skilltreeLinks = EntityManager.GetComponentData<SkilltreeLinks>(realmEntity);
                UnityEngine.GUILayout.Label("   > Skill Trees [" + skilltreeLinks.skilltrees.Length + "]");
                var gameColors = EntityManager.GetComponentData<GameColors>(realmEntity);
                UnityEngine.GUILayout.Label("   > Realm Colors [" + gameColors.colors.Length + "]");
                var planet = new Entity();
                var worldLinks = EntityManager.GetComponentData<WorldLinks>(realmEntity);
                if (worldLinks.worlds.Length > 0)
                {
                    planet = worldLinks.worlds[0];
                }
                if (EntityManager.Exists(planet))
                {
                    var mapID = EntityManager.GetComponentData<ZoxID>(planet).id;
                    UnityEngine.GUILayout.Label("   > Planet: " + mapID);
                    var voxelLinks = EntityManager.GetComponentData<VoxelLinks>(planet);
                    UnityEngine.GUILayout.Label("       > Voxels: " + voxelLinks.voxels.Length);
                }
                else
                {
                    UnityEngine.GUILayout.Label("   > Planet: None");
                }
            }
        }

        void DrawTouchUI()
        {
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            if (playerLinks.players.Length > 0)
            {
                var player = EntityManager.GetComponentData<Controller>(playerLinks.players[0]);
                UnityEngine.GUILayout.Label("mouse.position: " + player.mouse.position);
                UnityEngine.GUILayout.Label("mouse.leftButton: " + player.mouse.leftButton);
            }
        }

        private void DebugPlayerTarget()
        {
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            if (playerLinks.players.Length > 0)
            {
                var player = playerLinks.players[0];
                var translation = EntityManager.GetComponentData<Translation>(player);
                if (EntityManager.HasComponent<Target>(player))
                {
                    var targeter = EntityManager.GetComponentData<Target>(player);
                    if (EntityManager.Exists(targeter.target))
                    {
                        var targetPosition = EntityManager.GetComponentData<Translation>(targeter.target);
                        UnityEngine.Debug.DrawLine(translation.Value, targetPosition.Value, UnityEngine.Color.red);
                    }
                }
            }
        }

        private void DebugAnimator()
        {
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            if (playerLinks.players.Length == 0)
            {
                return;
            }
            var player = playerLinks.players[0];
            UnityEngine.GUILayout.Label("Debugging Player Animator [" + player.Index + "]");
            // Targeting
            if (EntityManager.HasComponent<Animator>(player) == false)
            {
                UnityEngine.GUILayout.Label("Player Has No Animator Component.");
            }
            else
            {
                var animator = EntityManager.GetComponentData<Animator>(player);
                UnityEngine.GUILayout.Label("Legs State: " + animator.legsState);
                UnityEngine.GUILayout.Label("Right Arm State: " + animator.rightArmState);
                UnityEngine.GUILayout.Label("Left Arm State: " + animator.leftArmState);
                UnityEngine.GUILayout.Label("Core State: " + animator.coreState);

            }
        }

        public void ShowMapBiomesUI()
        {
            var planet = new Entity();
            var worldLinks = EntityManager.GetComponentData<WorldLinks>(Realm);
            if (worldLinks.worlds.Length > 0)
            {
                planet = worldLinks.worlds[0];
            }
            if (EntityManager.Exists(planet))
            {
                var mapData = EntityManager.GetComponentData<BiomeLinks>(planet);
                UnityEngine.GUILayout.Label("Biomes [" + mapData.biomes.Length + "]");
                /*for (int i = 0; i < mapData.biomes.Length; i++)
                {
                    var biome = mapData.biomes[i];
                    UnityEngine.GUILayout.Label("ChunkBiome [" + i + "] grassID: " + biome.grassID);
                }*/
            }
        }

        public void ShowMapVoxelsUI()
        {
            var mapEntity = new Entity();
            var worldLinks = EntityManager.GetComponentData<WorldLinks>(Realm);
            if (worldLinks.worlds.Length > 0)
            {
                mapEntity = worldLinks.worlds[0];
            }
            if (EntityManager.Exists(mapEntity))
            {
                var mapID = EntityManager.GetComponentData<ZoxID>(mapEntity);
                UnityEngine.GUILayout.Label("Seed [" + mapID.id + "]");
                var voxelLinks = EntityManager.GetComponentData<VoxelLinks>(mapEntity);
                UnityEngine.GUILayout.Label("Voxels in Planet [" + voxelLinks.voxels.Length + "]");
                for (int i = 0; i < voxelLinks.voxels.Length; i++)
                {
                    var voxelEntity = voxelLinks.voxels[i];
                    var voxel = EntityManager.GetComponentData<Voxel>(voxelEntity);
                    var voxelType = ((VoxelType)voxel.type);
                    if (EntityManager.HasComponent<ZoxName>(voxelEntity))
                    {
                        UnityEngine.GUILayout.Label("Voxel " + i + " [" + 
                            EntityManager.GetComponentData<ZoxName>(voxelEntity).name.ToString() + "] Type: "
                            + voxelType + ", Mesh: " + EntityManager.GetComponentData<VoxelMeshType>(voxelEntity).meshType);
                    }
                }
            }
            else
            {
                UnityEngine.GUILayout.Label("Planet is not loaded yet.");
            }
        }

        private void DrawPlayerInputs()
        {
            var deviceLinks = EntityManager.GetComponentData<DeviceLinks>(InputSystemGroup.devicesHome);
            UnityEngine.GUILayout.Label("Devices [" + deviceLinks.Length + "]");
            if (deviceLinks.devices.IsCreated)
            {
                var i = 0;
                foreach (var KVP in deviceLinks.devices)
                {
                    var deviceEntity = KVP.Value;
                    var zoxID = EntityManager.GetComponentData<ZoxID>(deviceEntity);
                    var deviceTypeData = EntityManager.GetComponentData<DeviceTypeData>(deviceEntity);
                    UnityEngine.GUILayout.Label("   > Device (" + i + ") ID [" + zoxID.id + "] Type [" + deviceTypeData.type + "]");
                    i++;
                }
            }
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            UnityEngine.GUILayout.Label("Players [" + playerLinks.players.Length + "]");
            for (int i = 0; i < playerLinks.players.Length; i++)
            {
                var player = playerLinks.players[i];
                UnityEngine.GUILayout.Label("   > Player [" + i + ":" + player.Index + "]");
                if (!EntityManager.HasComponent<Controller>(player))
                {
                    UnityEngine.GUILayout.Label("   No Controller.");
                    continue;
                }
                var controller = EntityManager.GetComponentData<Controller>(player);
                var deviceTypeData = EntityManager.GetComponentData<DeviceTypeData>(player);
                // UnityEngine.GUILayout.Label("   DeviceID: " + controller.deviceID);
                UnityEngine.GUILayout.Label("       DeviceType: " + (deviceTypeData.type));
                UnityEngine.GUILayout.Label("       Mapping: " + (controller.mapping));
                UnityEngine.GUILayout.Label("       LockState: " + (UnityEngine.CursorLockMode)(controller.lockState));
                UnityEngine.GUILayout.Label("       Mouse Input: " + controller.mouse.position);
                UnityEngine.GUILayout.Label("       Mouse Scroll: " + controller.mouse.scroll);
                UnityEngine.GUILayout.Label("");
                var navigator = EntityManager.GetComponentData<Navigator>(player);
                UnityEngine.GUILayout.Label("   UI Navigation:");
                UnityEngine.GUILayout.Label("       Navigation Datas: " + navigator.navigateData.Length);
                if (EntityManager.Exists(navigator.selected))
                {
                    if (EntityManager.HasComponent<InputActivated>(navigator.selected))
                    {
                        UnityEngine.GUILayout.Label("   Input Activated");
                    }
                    else if (EntityManager.HasComponent<InputField>(navigator.selected))
                    {
                        UnityEngine.GUILayout.Label("   Input Selected");
                    }
                    else if (EntityManager.HasComponent<Button>(navigator.selected))
                    {
                        UnityEngine.GUILayout.Label("   Button Selected");
                    }
                    else if (EntityManager.HasComponent<MapPart>(navigator.selected))
                    {
                        var position = EntityManager.GetComponentData<ChunkDataLink>(navigator.selected).position;
                        UnityEngine.GUILayout.Label("   Map Part Selected [" + position+ "]");
                    }
                    else
                    {
                        UnityEngine.GUILayout.Label("   Uknown UI Selected");
                    }
                }
                else
                {
                    UnityEngine.GUILayout.Label("   No UI Selected");
                }
                UnityEngine.GUILayout.Label("");
            }
        }

        private void DrawPlayerCollider()
        {
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            for (int i = 0; i < playerLinks.players.Length; i++)
            {
                var player = playerLinks.players[i];
                UnityEngine.GUILayout.Label("Debugging Player Voxel Collider [" + i + ":" + player.Index + "]");
                UnityEngine.GUILayout.Label("   UnityEngine.Time Scale [" + UnityEngine.Time.timeScale + "]");
                if (EntityManager.HasComponent<VoxelCollider>(player) == false)
                {
                    UnityEngine.GUILayout.Label("Player Has No VoxelCollider.");
                }
                else
                {
                    var IsOnGround = EntityManager.GetComponentData<IsOnGround>(player);
                    UnityEngine.GUILayout.Label("   On Ground? [" + IsOnGround.isOnGround + "]");
                    // UnityEngine.GUILayout.Label("   initialized? [" + voxelCollider.initialized + "]");
                }
                if (EntityManager.HasComponent<Traveler>(player) == false)
                {
                    UnityEngine.GUILayout.Label("Player Has No Traveler.");
                }
                else
                {
                    var traveler = EntityManager.GetComponentData<Traveler>(player);
                    // UnityEngine.GUILayout.Label("   Side of Portal? [" + traveler.portalSide + "]");
                    UnityEngine.GUILayout.Label("   Target Portal Position [" + traveler.originalPortalPosition + "]");
                }
                // toggle?
                if (EntityManager.HasComponent<VoxelCollider>(player))
                {
                    var voxelCollider = EntityManager.GetComponentData<VoxelCollider>(player);
                    toggleA = UnityEngine.GUILayout.Toggle(toggleA, "Collision Points: " + voxelCollider.points.Length + "]");
                    if (toggleA)
                    {
                        for (int j = 0; j < voxelCollider.points.Length; j++)
                        {
                            var voxelPoint = voxelCollider.points[j];
                            // UnityEngine.GUILayout.Label("       voxelPoint(" + j + ") [" + voxelPoint.localPosition + "] VoxelPosition [" +  voxelPoint.voxelPosition + "]");
                            UnityEngine.GUILayout.Label("       voxelPoint(" + j + ") localPosition [" + voxelPoint.localPosition + "] globalPosition [" +  voxelPoint.globalPosition + "]");
                            //UnityEngine.GUILayout.Label("           HitX: " + voxelPoint.hitX.voxelType + ", HitY: " + voxelPoint.hitY.voxelType
                            //    + ", HitZ: " + voxelPoint.hitZ.voxelType);
                        }
                    }
                }
            }
        }

        private void DrawPlayerUIs()
        {
            // var gameOrbitDepth = CameraManager.instance.cameraSettings.uiDepth;
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            for (int i = 0; i < playerLinks.players.Length; i++)
            {
                var player = playerLinks.players[i];
                UnityEngine.GUILayout.Label("Debugging Player UIs [" + i + ":" + player.Index + "]");
                if (EntityManager.HasComponent<Controller>(player))
                {
                    var controller = EntityManager.GetComponentData<Controller>(player);
                    var pointer = controller.mouse.position;
                    UnityEngine.GUILayout.Label("   pointer [" + pointer + "]");
                }
                if (!EntityManager.HasComponent<RealmUILink>(player))
                {
                    UnityEngine.GUILayout.Label("Player Has No RealmUI.");
                }
                else
                {
                    var gameUILink = EntityManager.GetComponentData<RealmUILink>(player);
                    UnityEngine.GUILayout.Label("   Realm UI Index [" + gameUILink.index + "]");
                }

                if (!EntityManager.HasComponent<UILink>(player))
                {
                    UnityEngine.GUILayout.Label("Player Has No UILink.");
                }
                else
                {
                    var uiLink = EntityManager.GetComponentData<UILink>(player);
                    UnityEngine.GUILayout.Label("   UIS [" + uiLink.uis.Length + "]");
                    for (int j = 0; j < uiLink.uis.Length; j++)
                    {
                        var ui = uiLink.uis[j];
                        var label = "       ui(" + j + ") [" + ui.Index + "]";
                        if (EntityManager.HasComponent<Actionbar>(ui))
                        {
                            label += " [actionbar]";
                        }
                        else if (EntityManager.HasComponent<Statbar>(ui))
                        {
                            label += " [statbar]";
                        }
                        else if (EntityManager.HasComponent<Taskbar>(ui))
                        {
                            label += " [gametabsui]";
                        }
                        else if (EntityManager.HasComponent<Crosshair>(ui))
                        {
                            label += " [crosshair]";
                        }
                        else if (EntityManager.HasComponent<Minimap>(ui))
                        {
                            label += " [minimap]";
                        }
                        else if (EntityManager.HasComponent<QuestTracker>(ui))
                        {
                            label += " [questtrackerui]";
                        }
                        /*else if (EntityManager.HasComponent<RealmUI>(ui))
                        {
                            label += " [gameui]";
                        }*/
                        if (!EntityManager.Exists(ui))
                        {
                            label += " Does not exist";
                        }
                        UnityEngine.GUILayout.Label(label);
                    }
                    // get buttons
                    var cameraEntity = EntityManager.GetComponentData<CameraLink>(player).camera;
                    var camera = EntityManager.GetComponentData<Zoxel.Cameras.Camera>(cameraEntity);
                    var frustrumSize = camera.frustrum; // GetFrustrumSize(orbitDepth);
                    /*var newRealmUI = uiLink.GetUI<RealmUI>(EntityManager);
                    if (EntityManager.Exists(newRealmUI))
                    {
                        var childrens = EntityManager.GetComponentData<Childrens>(newRealmUI);
                        for (int j = 0; j < childrens.children.Length; j++)
                        {
                            var child = childrens.children[j];
                            if (EntityManager.Exists(child))
                            {
                                var uiElement = EntityManager.GetComponentData<UIElement>(child);
                                var size2D = EntityManager.GetComponentData<Size2D>(child);
                                var size = size2D.size;
                                //size = new float2(size.x / frustrumSize.x, size.y / frustrumSize.y);
                                size = new float2(size.x / frustrumSize.x, size.y / frustrumSize.y);
                                UnityEngine.GUILayout.Label("       UI (" + j + ") position[" + uiElement.position + "] size[" + size + "]");
                            }
                        }
                    }*/
                }
            }
        }

        private void DrawPlayerActions()
        {
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            for (int i = 0; i < playerLinks.players.Length; i++)
            {
                var player = playerLinks.players[i];
                if (!EntityManager.HasComponent<CharacterLink>(player))
                {
                    UnityEngine.GUILayout.Label("   Debugging Player [" + i + ":" + player.Index + "] No Character");
                    continue;
                }
                var characterEntity = EntityManager.GetComponentData<CharacterLink>(player).character;
                if (EntityManager.HasComponent<UserActionLinks>(characterEntity))
                {
                    var userActionLinks = EntityManager.GetComponentData<UserActionLinks>(characterEntity);
                    UnityEngine.GUILayout.Label("   Debugging Player [" + i + ":" + player.Index + "] UserActionLinks: " + userActionLinks.actions.Length);
                    for (int j = 0; j < userActionLinks.actions.Length; j++)
                    {
                        var action = userActionLinks.actions[j];
                        UnityEngine.GUILayout.Label("Action [" + j + "]: " + action.target.Index + " - " + action.targetMeta.Index);
                        if (EntityManager.HasComponent<UserItem>(action.target))
                        {
                            UnityEngine.GUILayout.Label("   - Action has UserItem: " + EntityManager.GetComponentData<ZoxName>(action.targetMeta).name.ToString());
                        }
                        else if (EntityManager.HasComponent<UserSkill>(action.target))
                        {
                            // UnityEngine.GUILayout.Label("   - Action has UserSkill.");
                            UnityEngine.GUILayout.Label("   - Action has UserSkill: " + EntityManager.GetComponentData<ZoxName>(action.targetMeta).name.ToString());
                        }
                    }
                }
                else
                {
                    UnityEngine.GUILayout.Label("   Debugging Player [" + i + ":" + player.Index + "] No UserActionLinks");
                }
            }
        }

        private void DrawPlayerInventory()
        {
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            for (int i = 0; i < playerLinks.players.Length; i++)
            {
                var player = playerLinks.players[i];
                UnityEngine.GUILayout.Label("Debugging Player [" + i + ":" + player.Index + "]");
                
                if (EntityManager.HasComponent<Inventory>(player) == false)
                {
                    UnityEngine.GUILayout.Label("Player Has No Inventory.");
                }
                else
                {
                    UnityEngine.GUILayout.Label("----- Player Inventory -----");
                    var inventory = EntityManager.GetComponentData<Inventory>(player);
                    UnityEngine.GUILayout.Label("Items [" + inventory.items.Length + "]");
                    for (int j = 0; j < inventory.items.Length; j++)
                    {
                        // var itemID = EntityManager.GetComponentData<ZoxID>(inventory.items[j].item).id;
                        // UnityEngine.GUILayout.Label("   " + j + " ID [" + itemID + "] x" + inventory.items[j].quantity);
                    }
                }
            }
        }

        private void DrawPlayerStats()
        {
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            for (int i = 0; i < playerLinks.players.Length; i++)
            {
                var playerEntity = playerLinks.players[i];
                UnityEngine.GUILayout.Label("Debugging Player [" + i + ":" + playerEntity.Index + "]");
                var player = EntityManager.GetComponentData<CharacterLink>(playerEntity).character;
                if (!EntityManager.HasComponent<Character>(player))
                {
                    UnityEngine.GUILayout.Label("   Player haz no character.");
                    continue;
                }
                if (EntityManager.HasComponent<ZoxName>(player))
                {
                    UnityEngine.GUILayout.Label("   Name: " + EntityManager.GetComponentData<ZoxName>(player).name.ToString());
                }
                else
                {
                    UnityEngine.GUILayout.Label("   No Name");
                }
                if (EntityManager.HasComponent<Translation>(player))
                {
                    var translation = EntityManager.GetComponentData<Translation>(player);
                    UnityEngine.GUILayout.Label("   Player at: " + translation.Value.ToString());
                }
                else
                {
                    UnityEngine.GUILayout.Label("   No Translation");
                }
                /*if (EntityManager.HasComponent<RealmUILink>(player))
                {
                    UnityEngine.GUILayout.Label("   index: " + EntityManager.GetComponentData<RealmUILink>(player).index);
                }
                if (EntityManager.HasComponent<UILink>(player))
                {
                    UnityEngine.GUILayout.Label("   UIs: " + EntityManager.GetComponentData<UILink>(player).uis.Length);
                }
                else
                {
                    UnityEngine.GUILayout.Label("   No UILink");
                }*/
                /*if (EntityManager.HasComponent<Rotation>(player))
                {
                    var rotation = EntityManager.GetComponentData<Rotation>(player);
                    float3 currentAngle = new UnityEngine.Quaternion(rotation.Value.value.x, rotation.Value.value.y,
                        rotation.Value.value.z, rotation.Value.value.w).eulerAngles;
                    UnityEngine.GUILayout.Label("   Player Rot: " + currentAngle);
                }
                else
                {
                    UnityEngine.GUILayout.Label("   No Rotation");
                }*/
                // Stats
                if (EntityManager.HasComponent<UserStatLinks>(player) == false)
                {
                    UnityEngine.GUILayout.Label("Player Has No Stats.");
                }
                else
                {
                    var userStatLinks = EntityManager.GetComponentData<UserStatLinks>(player);
                    UnityEngine.GUILayout.Label("----- Player Stats [" + userStatLinks.stats.Length + "] -----");
                    for (int j = 0; j < userStatLinks.stats.Length; j++)
                    {
                        var userStatEntity = userStatLinks.stats[j];
                        var label = "   [" + j + "] ";
                        if (EntityManager.HasComponent<BaseStat>(userStatEntity))
                        {
                            label += "BaseStat : ";
                        }
                        else if (EntityManager.HasComponent<StateStat>(userStatEntity))
                        {
                            label += "StateStat : ";
                        }
                        else if (EntityManager.HasComponent<RegenStat>(userStatEntity))
                        {
                            label += "RegenStat : ";
                        }
                        else if (EntityManager.HasComponent<LevelStat>(userStatEntity))
                        {
                            label += "LevelStat : ";
                        }
                        else if (EntityManager.HasComponent<AttributeStat>(userStatEntity))
                        {
                            label += "AttributeStat : ";
                        }
                        
                        
                        if (EntityManager.HasComponent<StatValue>(userStatEntity))
                        {
                            var statValue = EntityManager.GetComponentData<StatValue>(userStatEntity);
                            // UnityEngine.GUILayout.Label("statValue [" + statValue.value + "]");
                            label += statValue.value;
                        }
                        UnityEngine.GUILayout.Label(label);
                        
                    }
                    //UnityEngine.GUILayout.Label("Player .Attributes Applied: " + userStatLinks.attributesApplied.ToString());
                    /*if (userStatLinks.levels.Length > 0)
                    {
                        UnityEngine.GUILayout.Label("LevelStat [" + userStatLinks.levels[0].value + "] " + userStatLinks.levels[0].experienceGained + " / " + userStatLinks.levels[0].experienceRequired);
                    }
                    else
                    {
                        UnityEngine.GUILayout.Label("LevelStat [None]");
                    }
                    if (userStatLinks.stats.Length > 1)
                    {
                        UnityEngine.GUILayout.Label("Stat Points [" + userStatLinks.stats[0].value + "]");
                    }
                    else
                    {
                        UnityEngine.GUILayout.Label("Stat Points [None]");
                    }
                    if (userStatLinks.stats.Length >= 2)
                    {
                        UnityEngine.GUILayout.Label("Skill Points [" + userStatLinks.stats[1].value + "]");
                    }
                    else
                    {
                        UnityEngine.GUILayout.Label("Skill Points [None]");
                    }
                    if (userStatLinks.states.Length > 1)
                    {
                        UnityEngine.GUILayout.Label("Health [" + userStatLinks.states[0].value + " out of " + userStatLinks.states[0].maxValue + "]");
                    }
                    else
                    {
                        UnityEngine.GUILayout.Label("Health [None]");
                    }
                    if (userStatLinks.regens.Length > 0)
                    {
                        UnityEngine.GUILayout.Label("HealthRegen [" + userStatLinks.regens[0].value); //  + " at rate of " + userStatLinks.regens[0].rate + "]");
                    }
                    else
                    {
                        UnityEngine.GUILayout.Label("HealthRegen [None]");
                    }
                    if (userStatLinks.attributes.Length > 0)
                    {
                        UnityEngine.GUILayout.Label("Strength [" + userStatLinks.attributes[0].value + " and bonus value of: " + +userStatLinks.attributes[0].previousAdded + "] Added to health.");
                    }
                    else
                    {
                        UnityEngine.GUILayout.Label("Strength [None]");
                    }*/
                }
                /*if (EntityManager.HasComponent<UserActionLinks>(player) == false)
                {
                    UnityEngine.GUILayout.Label("Player Has no UserActionLinks Component.");
                }
                else
                {
                    var userSkillLinks = EntityManager.GetComponentData<UserActionLinks>(player);
                    UnityEngine.GUILayout.Label("Skill Selected [" + userSkillLinks.selectedAction + "]");
                    var inventory = EntityManager.GetComponentData<Inventory>(player);
                    UnityEngine.GUILayout.Label("Displaying Items for unnamed character with max items of [" + inventory.items.Length + "].");
                }*/
            }
                    /*for (int i = 0; i < inventory.items.Length; i++)
                    {
                        if (inventory.items[i].metaID == 0)
                        {
                            //UnityEngine.GUILayout.Label("Item [" + i + "] is " + item.name + " x" + inventory.items[i].quantity);
                        }
                        else
                        {
                            //ItemDatam item = booty.GetSystems().itemSpawnSystem.meta[inventory.items[i].metaID];
                            //UnityEngine.GUILayout.Label("Item [" + i + "] is " + item.name + " x" + inventory.items[i].quantity);
                        }
                    }*/
                /*if (EntityManager.HasComponent<Character>(player))
                {
                    UnityEngine.GUILayout.Label("   MetaID: " + EntityManager.GetComponentData<Character>(player).metaID);
                }
                else
                {
                    UnityEngine.GUILayout.Label("   Not a Character");
                }*/
        }

        private void DrawPlayerTraveler()
        {
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            for (int i = 0; i < playerLinks.players.Length; i++)
            {
                var player = playerLinks.players[i];
                UnityEngine.GUILayout.Label("Debugging Player [" + i + ":" + player.Index + "]");
                if (EntityManager.HasComponent<Traveler>(player))
                {
                    var traveler = EntityManager.GetComponentData<Traveler>(player);
                    UnityEngine.GUILayout.Label("   Closest Portal: " + traveler.portal.Index);
                    // UnityEngine.GUILayout.Label("   Distance To Portal: " + traveler.distanceToPortal);
                    // UnityEngine.GUILayout.Label("   Portal Side: " + traveler.portalSide);
                }
                else
                {
                    UnityEngine.GUILayout.Label("   Not a Traveler");
                }
            }
        }


        void DrawFPS()
        {
            UnityEngine.GUILayout.Label(GetFPSText());
        }

        private string GetFPSText()
        {
            var msec = deltaTime * 1000.0f;
            var fps = 1.0f / deltaTime;
            var text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
            return text;
        }

        private void HandleInput()
        {
            if (UnityEngine.InputSystem.Keyboard.current == null)
            {
                return;
            }
            var keyboard = UnityEngine.InputSystem.Keyboard.current;
            if (keyboard.backquoteKey.wasPressedThisFrame)
            {
                if (debugGUIType == BootUIType.BasicConsole)
                {
                    debugGUIType = BootUIType.None;
                }
                else
                {
                    debugGUIType = BootUIType.BasicConsole;
                }
            }
            else if (keyboard.upArrowKey.wasPressedThisFrame)
            {
                if (debugGUIType == BootUIType.BasicConsole)
                {
                    basicConsole.SelectLastInput();
                }
            }
            else if (keyboard.leftCtrlKey.wasPressedThisFrame && keyboard.vKey.wasPressedThisFrame)
            {
                if (debugGUIType == BootUIType.BasicConsole)
                {
                    basicConsole.PasteFromClipboard();
                }
            }
        }

        private void AudioOptions()
        {
            AudioManager.instance.disableMusic = !UnityEngine.GUILayout.Toggle(!AudioManager.instance.disableMusic, "Is Music");
            AudioManager.instance.disableBass = !UnityEngine.GUILayout.Toggle(!AudioManager.instance.disableBass, "Is Bass");
        }
    }
}

/*debugGUIType == BootUIType.BasicConsole
    || debugGUIType == BootUIType.Menu
    || debugGUIType == BootUIType.CheatMode
    || debugGUIType == BootUIType.DebugVisuals
    || debugGUIType == BootUIType.DisableOptions
    || debugGUIType == BootUIType.GameOptions)*/


            /*if (UnityEngine.InputSystem.Keyboard.current.f1Key.wasPressedThisFrame)
            {
                debugGUIType = BootUIType.None;
            }
            else if (UnityEngine.InputSystem.Keyboard.current.f2Key.wasPressedThisFrame)
            {
                debugGUIType = BootUIType.Menu;
            }*/
            /*else if (UnityEngine.InputSystem.Keyboard.current.f3Key.wasPressedThisFrame)
            {
                CaptureScreen.instance.Capture();
            }*/
            /*else if (((int)debugGUIType) > 1 && UnityEngine.InputSystem.Keyboard.current.f3Key.wasPressedThisFrame)
            {
                //debugGUIType = BootUIType.PlayerStats;
                debugGUIType = (BootUIType)((int)debugGUIType - 1);
            }
            else if (debugGUIType != BootUIType.End && UnityEngine.InputSystem.Keyboard.current.f4Key.wasPressedThisFrame)
            {
                //debugGUIType = BootUIType.PlayerTarget;
                debugGUIType = (BootUIType)((int)debugGUIType + 1);
            }*/


        /*private void DebugAIStates()
        {
            //var manager = booty.GetSystems().space.EntityManager;
            //int count = 1;
            foreach (var character in booty.GetSystems().characterSystemGroup.characterSystem.characters.Values)
            {
                if (manager.HasComponent<Brain>(character))
                {
                    Brain state = manager.GetComponentData<Brain>(character);
                    ZoxID zoxID = manager.GetComponentData<ZoxID>(character);
                    UnityEngine.GUILayout.Label("[" + count + "] State: " + ((AIStateType)(state.state)) + ", Creator: " + zoxID.creator.Index + ", ID: " + zoxID.id);
                    Mover mover = manager.GetComponentData<Mover>(character);
                    Wander wander = manager.GetComponentData<Wander>(character);
                    Targeter targeter = manager.GetComponentData<Targeter>(character);
                    //UnityEngine.GUILayout.Label("       [" + count + "] Mover: " + mover.disabled + ", Wander: " + wander.disabled + ", Target ID: " + targeter.target.Index);
                }
                else
                {
                    ZoxID zoxID = manager.GetComponentData<ZoxID>(character);
                    UnityEngine.GUILayout.Label("[" + count + "], Creator: " + zoxID.creator.Index + ", ID: " + zoxID.id);
                    if (manager.HasComponent<Shooter>(character))
                    {
                        Shooter shooter = manager.GetComponentData<Shooter>(character);
                        Targeter targeter = manager.GetComponentData<Targeter>(character);
                        //UnityEngine.GUILayout.Label("       Shooter: " + Quaternion.ToEulerAngles(shooter.shootRotation).ToString() + ", Target ID: " + targeter.targetID);
                    }
                }
                count++;
                if (count == 31)
                {
                    break;
                }
            }
        }*/