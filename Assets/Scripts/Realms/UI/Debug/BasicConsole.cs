using UnityEngine;
using UnityEngine.InputSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Cameras;
using Zoxel.Movement;
using Zoxel.Particles;
using Zoxel.Items;
using Zoxel.Weather;
using Zoxel.Players;
using Zoxel.Characters;
using Zoxel.Turrets;
using Zoxel.Players.UI;
using Zoxel.Rendering;
using Zoxel.Realms;
using Zoxel.Skeletons;
using Zoxel.Quests;
using Zoxel.Cameras.Spawning;
using Zoxel.AI;
using Zoxel.Worlds;
using Zoxel.Voxels;
using Zoxel.VoxelInteraction;
// Made for testing new items and userSkillLinks in realmEntity
// Or anything else
// basically an input box
// that lets you control the realmEntity
// made with GUILayout since it's easy to add new stuff
// Systems will handle the rest

namespace Zoxel
{
    public struct BasicConsole
    {
        public string[] lastInput;
        public string[] lastCommand;
        private string input;
        //const int biomesCount = 12;
        //const int voxelsPerBiome = 10;

        public BasicConsole(int count)
        {
            this.lastInput = new string[count];
            this.lastCommand = new string[count];
            this.input = "";
        }

        public void ClearInput()
        {
            this.input = "";
        }

        public void SelectLastInput()
        {
            if (lastCommand.Length > 0)
            {
                this.input = lastCommand[0];
            }
        }

        public void PasteFromClipboard()
        {
            this.input = UnityEngine.GUIUtility.systemCopyBuffer;
        }

        void ClearConsole()
        {
            for (int i = 0; i < lastCommand.Length; i++)
            {
                lastInput[i] = "";
                lastCommand[i] = "";
            }
        }

        public void UpdateConsole(EntityManager EntityManager, Entity realmEntity, Entity playerEntity)
        {
            var biomesCount = VoxelSpawnSystem.biomesCount;
            var voxelsPerBiome = VoxelSpawnSystem.voxelsPerBiome;
            // some may not need realmEntity
            if (!Keyboard.current.enterKey.wasPressedThisFrame || input.Length == 0)
            {
                return;
            }
            var didWork = false;
            if (input.Contains("testcoop"))
            {
                input = "Testing Two Player Coop";
                EntityManager.SetComponentData(EntityManager.Instantiate(CameraSpawnSystem.spawnCameraPrefab), new SpawnCamera(new float3(32f, 0, 0)));
                didWork = true;
            }
            else if (input.Contains("testcamera") || input.Contains("addplayer"))
            {
                input = "Spawning an additional camera.";
                EntityManager.SetComponentData(EntityManager.Instantiate(CameraSpawnSystem.spawnCameraPrefab), new SpawnCamera(new float3(32f, 0, 0)));
                didWork = true;
            }
            else if (input.Contains("testoctcameras"))
            {
                input = "Spawning an additional OctaCameras.";
                for (int i = CameraReferences.firstPersonCameras.Count; i < 16; i++)
                {
                    EntityManager.SetComponentData(EntityManager.Instantiate(CameraSpawnSystem.spawnFirstPersonCameraPrefab), new SpawnFirstPersonCamera(new float3(32f, 0, 0)));
                }
                didWork = true;
            }
            else if (input.Contains("testallthecameras"))
            {
                input = "Spawning an ALL THE CAMERAS!!!!";
                for (int i = CameraReferences.cameras.Count; i < 64; i++)
                {
                    EntityManager.SetComponentData(EntityManager.Instantiate(CameraSpawnSystem.spawnCameraPrefab), new SpawnCamera(new float3(32f, 0, 0)));
                }
                didWork = true;
            }
            else if (input.Contains("testswitchcontrol"))
            {
                var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
                for (int i = 0; i < playerLinks.players.Length; i++)
                {
                    var playerEntity2 = playerLinks.players[i];
                    var j = UnityEngine.Random.Range(0, CameraReferences.cameras.Count);
                    var cameraEntity = CameraReferences.cameras[j];
                    EntityManager.SetComponentData(playerEntity2, new CameraLink(cameraEntity));
                    input = "Switching Controller to [" + j + "]";
                }
                didWork = true;
            }
            else if (input.Contains("addsecondplayer"))
            {
                input = "Adding a second player";
                // Spawn a second camera
                // Spawn a second controller entity - using gamepad
                //      Make sure first player doesn't switch to that by checking if new connecting is connected
                // Spawn a new character for the new controller!
                didWork = true;
            }
            if (input.Contains("clear"))
            {
                input = "Cleared Log";
                ClearConsole();
                didWork = true;
            }
            if (didWork)
            {
                AddToLog(input);
                input = "";
                return;
            }

            if (!EntityManager.Exists(realmEntity))
            {
                return;
            }
            var characterEntity = EntityManager.GetComponentData<CharacterLink>(playerEntity).character;
            var worldEntity = new Entity();
            var worldLinks = EntityManager.GetComponentData<WorldLinks>(realmEntity);
            if (worldLinks.worlds.Length > 0)
            {
                worldEntity = worldLinks.worlds[0];
            }
            // add to input list
            //UnityEngine.Debug.LogError("Entered new input.");
            var originalInput = input;
            AddToCommandLog(originalInput);
            if (input.Contains("daytime"))
            {
                if (!EntityManager.HasComponent<BecomeNight>(worldEntity) && !EntityManager.HasComponent<BecomeDay>(worldEntity))
                {
                    input = "Setting Time of Day to [Day]";
                    EntityManager.AddComponent<BecomeDay>(worldEntity);
                }
                else
                {
                    input = "Time of Day already Transitioning.";
                }
            }
            else if (input.Contains("enablerain") || input.Contains("raintime"))
            {
                var rainCloudEntity = EntityManager.GetComponentData<RainCloudLink>(worldEntity).rainCloud;
                if (EntityManager.HasComponent<DisableRain>(rainCloudEntity))
                {
                    input = "Enabling [Rain]";
                    EntityManager.RemoveComponent<DisableRain>(rainCloudEntity);
                    // RainSystem.forceRainNextDay = true;
                    // EntityManager.AddComponent<BecomeDay>(worldEntity);
                }
                else
                {
                    input = "Rain is already Enabled.";
                }
            }
            else if (input.Contains("disablerain"))
            {
                var rainCloudEntity = EntityManager.GetComponentData<RainCloudLink>(worldEntity).rainCloud;
                if (!EntityManager.HasComponent<DisableRain>(rainCloudEntity))
                {
                    input = "Disabling [Rain]";
                    EntityManager.AddComponent<DisableRain>(rainCloudEntity);
                }
                else
                {
                    input = "Rain is already Disabled.";
                }
            }
            else if (input.Contains("addhour"))
            {
                var worldTime = EntityManager.GetComponentData<WorldTime>(worldEntity);
                worldTime.secondsPassed += 60 * 60;
                EntityManager.SetComponentData(worldEntity, worldTime);
            }
            else if (input.Contains("add8hours"))
            {
                var worldTime = EntityManager.GetComponentData<WorldTime>(worldEntity);
                worldTime.secondsPassed += 60 * 60 * 8;
                EntityManager.SetComponentData(worldEntity, worldTime);
            }
            else if (input.Contains("nighttime"))
            {
                if (!EntityManager.HasComponent<BecomeNight>(worldEntity) && !EntityManager.HasComponent<BecomeDay>(worldEntity))
                {
                    input = "Setting Time of Day to [Night]";
                    EntityManager.AddComponent<BecomeNight>(worldEntity);
                }
                else
                {
                    input = "Time of Day already Transitioning.";
                }
            }
            else if (input.Contains("enabledebugnearbyentities"))
            {
                input = "Beginning Debugging Nearby Entities.";
                NearbyEntitiesDebugger.instance.enabled = true;
            }
            else if (input.Contains("disabledebugnearbyentities"))
            {
                input = "Ending Debugging Nearby Entities.";
                NearbyEntitiesDebugger.instance.enabled = false;
            }
            else if (input.Contains("enableflymode"))
            {
                EntityManager.AddComponent<FlyMode>(characterEntity);
                input = "Enabling Fly Mode.";
            }
            else if (input.Contains("disableflymode"))
            {
                EntityManager.RemoveComponent<FlyMode>(characterEntity);
                input = "Disabling Fly Mode.";
            }
            else if (input.Contains("enablenoclip"))
            {
                EntityManager.AddComponent<NoClip>(characterEntity);
                input = "Enabling No Clip Mode.";
            }
            else if (input.Contains("disablenoclip"))
            {
                EntityManager.RemoveComponent<NoClip>(characterEntity);
                input = "Disabling No Clip Mode.";
            }
            else if (input.Contains("spawnturret") || input.Contains("testturret"))
            {
                var hitPosition = EntityManager.GetComponentData<Zoxel.VoxelInteraction.RaycastVoxel>(characterEntity).normalVoxel.position;
                TurretSpawnSystem.SummonTurret(EntityManager, characterEntity, hitPosition.ToFloat3(), 0);    //  + new float3(0.5f, 0.5f, 0.5f)
                input = "Summoning Turret at: " + hitPosition;
            }
            else if (input.Contains("testhouse") || input.Contains("spawnhouse"))
            {
                var raycastVoxel = EntityManager.GetComponentData<RaycastVoxel>(characterEntity);
                var hitPosition = raycastVoxel.normalVoxel.position;
                var voxLink = EntityManager.GetComponentData<VoxLink>(characterEntity);
                HouseSystem.SpawnHouse(EntityManager, voxLink.vox, raycastVoxel.normalVoxel.chunk, hitPosition);
                input = "Spawned House at: " + hitPosition;
            }
            
            //else if (input.Contains("testrotate"))
            //{
            //      EntityManager.AddComponent<TestPlayerRotation>(playerEntity);
            //      input = "Testing Camera Rotation";
            //}
            else if (input.Contains("enablefly"))
            {
                if (!EntityManager.HasComponent<CanFly>(playerEntity))
                {
                    EntityManager.AddComponent<CanFly>(playerEntity);
                    input = "Enabled Fly Mode";
                }
                else
                {
                    input = "Fly mode is already active";
                }
            }
            else if (input.Contains("disablefly"))
            {
                if (EntityManager.HasComponent<CanFly>(playerEntity))
                {
                    EntityManager.RemoveComponent<CanFly>(playerEntity);
                    input = "Disabled Fly Mode";
                    if (EntityManager.HasComponent<FlyMode>(playerEntity))
                    {
                        EntityManager.RemoveComponent<FlyMode>(playerEntity);
                    }
                }
                else
                {
                    input = "Fly mode is not active";
                }
            }
            else if (input.Contains("rotatecharacter"))
            {
                var split = input.Split(' ');
                if (split.Length == 4)
                {
                    var rotation = new float3(
                        float.Parse(split[1]),
                        float.Parse(split[2]),
                        float.Parse(split[3])
                    );
                    var degreesToRadians = ((math.PI * 2) / 360f);
                    var rotatedCharacter = quaternion.EulerXYZ(rotation * degreesToRadians);
                    EntityManager.SetComponentData(characterEntity, new Rotation { Value = rotatedCharacter });
                    input = "Rotated Character by: " + rotation.x + "x" + rotation.y + ":" + rotation.z;
                }
                else
                {
                    input = "Could not rotate character.";
                }
            }
            else if (input.Contains("tptown") || input.Contains("teleporttotown") || input.Contains("teleporttohome") || input.Contains("home"))
            {
                EntityManager.AddComponent<PlaceCharacterInWorld>(characterEntity);
                EntityManager.AddComponent<EntityBusy>(characterEntity);
                EntityManager.SetComponentData(characterEntity, new BodyForce());
                input = "Teleporting To Home";
            }
            else if (input.Contains("teleporttorandomtown") || input.Contains("tprandomtown"))
            {
                var megaChunks = EntityManager.GetComponentData<MegaChunkLinks>(worldEntity).megaChunks;
                var megaChunk = EntityManager.GetComponentData<MegaChunk>(megaChunks[UnityEngine.Random.Range(0, megaChunks.Length)]);
                var targetMegaChunk = megaChunk.position;
                var targetTown = UnityEngine.Random.Range(0, megaChunk.towns.Length);
                // var targetMegaChunk = int3.zero;
                EntityManager.AddComponentData(characterEntity, new PlaceCharacterInWorld
                    { targetMegaChunk = targetMegaChunk, targetTown = targetTown });
                EntityManager.SetComponentData(characterEntity, new BodyForce());
                input = "Teleporting To Random Town: " + targetMegaChunk + " :: " + targetTown;
            }
            else if (input.Contains("listtowns"))
            {
                input = "Listing Towns";
                // var realmEntity = EntityManager.GetComponentData<RealmLink>(playerEntity).realm;
                var newLogs = new List<string>();
                var megaChunks = EntityManager.GetComponentData<MegaChunkLinks>(worldEntity).megaChunks;
                for (int i = 0; i < megaChunks.Length; i++)
                {
                    var megaChunkEntity = megaChunks[i];
                    var megaChunk = EntityManager.GetComponentData<MegaChunk>(megaChunkEntity);
                    var towns = megaChunk.towns;
                    for (int j = 0; j < towns.Length; j++)
                    {
                        var town = towns[j];
                        newLogs.Add("===] MegaChunk [" + megaChunk.position + "] - Town [" + j + "] [===");
                    }
                }
                for (int i = newLogs.Count - 1; i >= 0; i--)
                {
                    AddToLog(newLogs[i]);
                }
            }
            else if (input.Contains("enablechaos"))
            {
                // Bootstrap.instance.realDebugSettings.chaosMode = true;
                // input = "Chaos Mode has been enabled. Be careful traveler.";
            }
            else if (input.Contains("disablechaos"))
            {
                // Bootstrap.instance.realDebugSettings.chaosMode = false;
                // input = "Peaceful Mode has been enabled. You weak fool.";
            }
            else if (input.Contains("settextureresolution"))
            {
                /*var split = input.Split(' ');
                if (split.Length == 2)
                {
                    var textureResolution = int.Parse(split[1]);
                    if (textureResolution >= 1 && textureResolution <= 1024)
                    {
                        VoxelManager.instance.realVoxelSettings.voxelTextureResolution = textureResolution;
                        // set realmEntity to generate textures again
                        if (EntityManager.Exists(worldEntity))
                        {
                            EntityManager.AddComponentData(worldEntity, new SetVoxelsTextureResolution((byte) textureResolution));
                        }
                        input = "Texture Resolution has been set to: " + textureResolution;
                    }
                }*/
            }
            else if (input.Contains("increasetextureresolution"))
            {
                /*if (VoxelManager.instance.voxelSettings.voxelTextureResolution < 256)
                {
                    VoxelManager.instance.realVoxelSettings.voxelTextureResolution *= 2;
                    input = "Texture Resolution has Increased to: " + VoxelManager.instance.voxelSettings.voxelTextureResolution;
                    // set realmEntity to generate textures again
                    if (EntityManager.Exists(worldEntity))
                    {
                        EntityManager.AddComponentData(worldEntity, new SetVoxelsTextureResolution((byte) VoxelManager.instance.voxelSettings.voxelTextureResolution));
                    }
                }
                else
                {
                    input = "Cannot Increase Texture Resolution.";
                }*/
            }
            else if (input.Contains("decreasetextureresolution"))
            {
                /*if (VoxelManager.instance.voxelSettings.voxelTextureResolution > 1)
                {
                    VoxelManager.instance.realVoxelSettings.voxelTextureResolution /= 2;
                    input = "Texture Resolution has Decreased to: " + VoxelManager.instance.voxelSettings.voxelTextureResolution;
                    // set realmEntity to generate textures again
                    if (EntityManager.Exists(worldEntity))
                    {
                        EntityManager.AddComponentData(worldEntity, new SetVoxelsTextureResolution((byte) VoxelManager.instance.voxelSettings.voxelTextureResolution));
                    }
                }
                else
                {
                    input = "Cannot Decrease Texture Resolution.";
                }*/
            }
            else if (input == ("getrenderdistance "))
            {
                // input = "Render Distance is: " + MaterialsManager.instance.renderSettings.renderDistance;
            }
            else if (input.Contains("setrenderdistance "))
            {
                /*var split = input.Split(' ');
                if (split.Length == 2)
                {
                    var renderDistance = int.Parse(split[1]);
                    if (renderDistance >= 2 && renderDistance <= 42)
                    {
                        MaterialsManager.instance.realRenderSettings.renderDistance = renderDistance;
                        var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
                        for (int i = 0; i < playerLinks.players.Length; i++)
                        {
                            var playerEntity2 = playerLinks.players[i];
                            EntityManager.AddComponentData(playerEntity2, new OnRenderDistanceUpdated((byte) renderDistance));
                        }
                        input = "Render Distance has been set to: " + renderDistance;
                    }
                    else
                    {
                        input = "Cannot set Render Distance to: " + renderDistance;
                    }
                }
                else
                {
                    input = "Cannot set Render Distance. Invalid number of parameters.";
                }*/
            }
            else if (input == "getfog")
            {
                input = "Fog Density is currently: " + UnityEngine.RenderSettings.fogDensity;
            }
            else if (input.Contains("setfog "))
            {
                var split = input.Split(' ');
                if (split.Length == 2)
                {
                    var fogDensity = float.Parse(split[1]);
                    // WeatherManager.instance.realWeatherSettings.dayFogDensity = fogDensity;
                    if (EntityManager.Exists(realmEntity))
                    {
                        if (EntityManager.Exists(worldEntity))
                        {
                            var sky = EntityManager.GetComponentData<Sky>(worldEntity);
                            sky.daySettings.fogDensity = fogDensity;
                            EntityManager.SetComponentData(worldEntity, sky);
                        }
                    }
                    UnityEngine.RenderSettings.fogDensity = fogDensity;
                    input = "Fog Density has been set to: " + fogDensity;
                }
            }
            /*else if (input.Contains("setgiftime "))
            {
                var split = input.Split(' ');
                if (split.Length == 2)
                {
                    GifCapturer.instance.recordSeconds =  int.Parse(split[1]);
                    input = "Set Gif Time Length To: " + GifCapturer.instance.recordSeconds;
                }
                else
                {
                    input = "Cannot set Gif Time. Invalid number of parameters.";
                }
            }
            else if (input.Contains("setgifrate "))
            {
                var split = input.Split(' ');
                if (split.Length == 2)
                {
                    GifCapturer.instance.frameRate =  int.Parse(split[1]);
                    input = "Set Gif Frame Rate To: " + GifCapturer.instance.frameRate;
                }
                else
                {
                    input = "Cannot set Gif Time. Invalid number of parameters.";
                }
            }*/
            // if input is giveitem torch
            // add torch item to players inventory
            else if (input.Contains("giveitem "))
            {
                var split = input.Split(' ');
                if (split.Length == 2 || split.Length == 3)
                {
                    var itemName = split[1];
                    int quantity = 1;
                    if (split.Length == 3)
                    {
                        quantity = int.Parse(split[2]);
                    }
                    var foundIndex = -1;
                    var items = ItemsManager.instance.ItemsSettings.items;
                    for (int i = 0; i < items.Length; i++)
                    {
                        if (items[i].name.ToLower().Contains(itemName.ToLower()))
                        {
                            foundIndex = i;
                            break;
                        }
                    }
                    if (foundIndex != -1)
                    {
                        /*var worldItem = new WorldItem(items[foundIndex].item, quantity);
                        input = "Cmd: Giving playerEntity Item [" + itemName + "]";
                        var pickupEntity = EntityManager.Instantiate(ItemHitSystem.pickupItemPrefab);
                        EntityManager.SetComponentData(pickupEntity, new PickupItem(characterEntity, worldItem));
                        EntityManager.SetComponentData(pickupEntity, new DelayEvent(0, 0.01));*/
                        input = "Cmd: Giving playerEntity Item [disabled]";
                    }
                    else
                    {
                        input = "Cmd: No Item [" + itemName + "]";
                    }
                }
            }
            else if (input.Contains("givevoxel "))
            {
                var split = input.Split(' ');
                if (split.Length == 2 || split.Length == 3)
                {
                    byte voxelType = (byte) int.Parse(split[1]);
                    int quantity = 1;
                    if (split.Length == 3)
                    {
                        quantity = int.Parse(split[2]);
                    }
                    input = AddItemToCharacter(EntityManager, realmEntity, characterEntity, voxelType, quantity);
                }
            }
            else if (input.Contains("testtorch"))
            {
                input = AddItemToCharacter(EntityManager, realmEntity, characterEntity, (byte)(biomesCount * voxelsPerBiome + 1), 30);
            }
            else if (input.Contains("testgrass"))
            {
                var grassIndexes = new NativeList<byte>();
                var voxelLinks = EntityManager.GetComponentData<VoxelLinks>(realmEntity);
                for (int j = 0; j < voxelLinks.voxels.Length; j++)
                {
                    if (EntityManager.HasComponent<GrassVoxel>(voxelLinks.voxels[j]))
                    {
                        grassIndexes.Add((byte) (j + 1));
                    }
                }
                // (byte)(biomesCount * voxelsPerBiome + 2)
                var grassIndex = grassIndexes[UnityEngine.Random.Range(0, grassIndexes.Length)];
                input = AddItemToCharacter(EntityManager, realmEntity, characterEntity, grassIndex, 30);
            }
            else if (input.Contains("testchest"))
            {
                input = AddItemToCharacter(EntityManager, realmEntity, characterEntity, (byte)(biomesCount * voxelsPerBiome + 2), 30);
            }
            else if (input.Contains("testportal"))
            {
                input = AddItemToCharacter(EntityManager, realmEntity, characterEntity, (byte)(biomesCount * voxelsPerBiome + 3), 30);
            }
            else if (input.Contains("testwater"))
            {
                input = AddItemToCharacter(EntityManager, realmEntity, characterEntity, (byte)(biomesCount * voxelsPerBiome + 4), 30);
            }
            else if (input.Contains("testdoor"))
            {
                input = AddItemToCharacter(EntityManager, realmEntity, characterEntity, (byte)(biomesCount * voxelsPerBiome + 5), 30);
            }
            else if (input.Contains("testbed"))
            {
                input = AddItemToCharacter(EntityManager, realmEntity, characterEntity, (byte)(biomesCount * voxelsPerBiome + 6), 30);
            }
            else if (input.Contains("testpickaxe"))
            {
                input = AddItemToCharacter(EntityManager, realmEntity, characterEntity, "Pickaxe");
            }
            else if (input.Contains("testcoin"))
            {
                input = AddItemToCharacter(EntityManager, realmEntity, characterEntity, "Coin");
            }

            else if (input.Contains("testallskills"))
            {
                input = "Cmd: Giving player all userSkillLinks.";
                //! \todo Implement this testallskills function.
            }

            else if (input.Contains("testmutantarm"))
            {
                // Spawn new unique item
                var itemEntity = SpawnBicepUniqueItem(EntityManager, realmEntity, characterEntity);
                var worldItem = new WorldItem(itemEntity, (byte) 1, 0);
                var pickupEntity = EntityManager.Instantiate(ItemHitSystem.pickupItemPrefab);
                EntityManager.SetComponentData(pickupEntity, new PickupItem(characterEntity, worldItem));
                EntityManager.SetComponentData(pickupEntity, new DelayEvent(0, 0.01));
                input = "Cmd: Giving Player Item [mutant-arm]";
            }
            else if (input.Contains("testquest"))
            {
                // input = "Cmd: Testing Quest. Not.";
                var userQuestPrefab = QuestSystemGroup.userQuestPrefab;
                var realmQuests = EntityManager.GetComponentData<QuestLinks>(realmEntity);
                var givingUserQuestEntity = realmQuests.quests[0]; // EntityManager.GetComponentData<UserQuest>(userQuestEntity);
                var givingUserQuest = EntityManager.GetComponentData<Quest>(givingUserQuestEntity);
                var questlog = EntityManager.GetComponentData<Questlog>(characterEntity);
                var addUserQuestEntity = EntityManager.Instantiate(userQuestPrefab);
                EntityManager.SetComponentData(addUserQuestEntity, new QuestHolderLink(characterEntity));
                EntityManager.SetComponentData(addUserQuestEntity, new UserDataIndex(questlog.quests.Length));
                EntityManager.SetComponentData(addUserQuestEntity, new UserQuest(givingUserQuestEntity, givingUserQuest));
                EntityManager.SetComponentData(addUserQuestEntity, new MetaData(givingUserQuestEntity));
                EntityManager.AddComponentData(characterEntity, new OnUserQuestSpawned((byte) (questlog.quests.Length + 1)));
                EntityManager.AddComponent<SaveQuestlog>(characterEntity);
                var questName = EntityManager.GetComponentData<ZoxName>(givingUserQuestEntity);
                /*var controllerEntity = EntityManager.GetComponentData<ControllerLink>(characterEntity).controller;
                var logUILink = EntityManager.GetComponentData<LogUILink>(controllerEntity);
                var message = "You Started Quest " + questName.name;
                EntityManager.AddComponentData(logUILink.logUI, new AddRenderText(new Text(message + "\n")));
                EntityManager.AddComponentData(logUILink.logUI, new FaderStayVisible(UnityEngine.Time.time));*/
                input = "Cmd: Giving playerEntity Quest [" + questName.name + "]";
            }
            else if (input == "help")
            {
                input = "";
                AddToLog("===][] [] [][===");
                AddToLog("enable/disablefly] - Enables or Disables Fly Mode");
                AddToLog("enable/disablenoclip] - Enables or Disables No Clip Mode");
                AddToLog("enable/disableflymode] - Enables or Disables Fly Mode");
                AddToLog("settextureresolution x] - Sets Voxels Texture Resolution");
                AddToLog("setrenderdistance x] - Sets Chunk Render Distance");
                AddToLog("[teleporttotown] - Teleports you to town");
                AddToLog("[spawnturret] - to spawn a turret at selected location");
                AddToLog("[givevoxel x y] - x is the voxel index, y is the quantity of the item");
                AddToLog("[giveitem x] - x is the item index");
                AddToLog("[enablechaos] or [disablechaos] - if you are feeling feisty");
                AddToLog("[getfog] or [setfog x] - for fogs");
                AddToLog("[daytime] or [nighttime] - to make time your bitch");
                AddToLog("[listitems] - doesn't list all the items");
                AddToLog("===][] Listing [10] Commands [][===");
            }
            if (originalInput == "listitems")
            {
                input = "";
                AddToLog("===][] [] [][===");
                var items = ItemsManager.instance.ItemsSettings.items;
                for (int i = 0; i < items.Length; i++)
                {
                    var item = items[i];
                    AddToLog("  (" + (i + 1) + ") - [" + item.name + "]");
                }
                AddToLog("===][] Listing [" + items.Length + "] Items [][===");
            }
            if (input != "" )
            {
                AddToLog(input);
                input = "";
            }
        }

        private static Entity GetItemSlot(EntityManager EntityManager, Entity realmEntity, in Text slotName)
        {
            var slots = EntityManager.GetComponentData<SlotLinks>(realmEntity).slots;
            for (int i = 0; i < slots.Length; i++)
            {
                var slotName2 = EntityManager.GetComponentData<ZoxName>(slots[i]).name;
                if (slotName2 == slotName)
                {
                    return slots[i];
                }
            }
            return new Entity();
        }

        private Entity SpawnBicepUniqueItem(EntityManager EntityManager, Entity realmEntity, Entity characterEntity)
        {
            var prefab = BodyGenerationSystem.uniqueItemPrefab;
            var partName = new Text("Bicep");
            var itemID = IDUtil.GenerateUniqueID();
            // todo: for all sub items, do this - call individual functions merely for shape information
            var voxShapes = new VoxShapes();
            var offset = int3.zero;
            var femaleOffsets = new NativeList<int3>();
            var skinColor = new Zoxel.ColorRGB(255, 0, 0);
            var bicepScale = 6;
            
            var random = new Unity.Mathematics.Random();
            random.InitState((uint)itemID);
            var size = new int3(bicepScale, 5 + random.NextInt(5), bicepScale);
            var outerBicepSize = (size.x / 2);
            var innerBicepSize = math.max(outerBicepSize - 1, 0);
            var outerBicepHeight = math.max(size.y / 4, 2);
            voxShapes.shapes = new BlitableArray<VoxShape>(2, Allocator.Persistent);
            voxShapes.shapes[0] = new VoxShape(skinColor, VoxShapeType.Cube, new int3(0, 0, 0), new int3(innerBicepSize, size.y / 2, innerBicepSize));
            voxShapes.shapes[1] = new VoxShape(skinColor, VoxShapeType.Cube, new int3(0, -size.y / 2, 0), new int3(outerBicepSize, outerBicepHeight, outerBicepSize));
            var itemEntity = EntityManager.Instantiate(prefab);
            EntityManager.SetComponentData(itemEntity, new ZoxID(itemID));
            EntityManager.SetComponentData(itemEntity, new Seed(itemID));
            EntityManager.SetComponentData(itemEntity, new ZoxName("Mutant " + partName.ToString()));
            EntityManager.SetComponentData(itemEntity, new BoneType(HumanoidBoneType.Bicep));
            EntityManager.SetComponentData(itemEntity, new SlotLink(GetItemSlot(EntityManager, realmEntity, in partName)));
            EntityManager.SetComponentData(itemEntity, new ItemSlotData(offset, femaleOffsets));
            EntityManager.SetComponentData(itemEntity, voxShapes);
            EntityManager.SetComponentData(itemEntity, new ChunkDimensions(size));
            EntityManager.SetComponentData(itemEntity, new Chunk(size, 1));
            EntityManager.SetComponentData(itemEntity, new CreatorLink(characterEntity));
            EntityManager.AddComponentData(characterEntity, new OnUniqueItemsSpawned(1));
            EntityManager.AddComponent<SaveUniqueItems>(characterEntity);
            partName.Dispose();
            return itemEntity;
        }

        private string AddItemToCharacter(EntityManager EntityManager, Entity realmEntity, Entity characterEntity, byte voxelType, int quantity)
        {
            var items = EntityManager.GetComponentData<ItemLinks>(realmEntity).items;
            // get itemEntity
            // var voxelTypePlusAir = (byte)(voxelType + 1);
            var itemEntity = new Entity();
            var itemIndex = 0;
            for (int i = 0; i < items.Length; i++)
            {
                if (!EntityManager.HasComponent<VoxelItem>(items[i]))
                {
                    continue;
                }
                var voxelItem = EntityManager.GetComponentData<VoxelItem>(items[i]);
                if (voxelItem.voxelType == voxelType)
                {
                    itemEntity = items[i];
                    break;
                }
            }
            if (itemEntity.Index == 0)
            {
                input = "Cmd: Voxel [" + voxelType + "] Not Found.";
            }
            else
            {
                var worldItem = new WorldItem(itemEntity, (byte) quantity, 0);
                var pickupEntity = EntityManager.Instantiate(ItemHitSystem.pickupItemPrefab);
                EntityManager.SetComponentData(pickupEntity, new PickupItem(characterEntity, worldItem));
                EntityManager.SetComponentData(pickupEntity, new DelayEvent(0, 0.01));
                input = "Cmd: Giving playerEntity Voxel [" + voxelType + "] at index (" + itemIndex + ")";
            }
            return input;
        }

        private string AddItemToCharacter(EntityManager EntityManager, Entity realmEntity, Entity characterEntity, string itemName, int quantity = 1)
        {
            var items = EntityManager.GetComponentData<ItemLinks>(realmEntity).items;
            // get itemEntity
            var itemEntity = new Entity();
            var itemIndex = 0;
            for (int i = 0; i < items.Length; i++)
            {
                var zoxName = EntityManager.GetComponentData<ZoxName>(items[i]).name;
                if (itemName == zoxName.ToString())
                {
                    itemEntity = items[i];
                    break;
                }
            }
            if (itemEntity.Index == 0)
            {
                input = "Cmd: Item [" + itemName + "] Not Found.";
            }
            else
            {
                var worldItem = new WorldItem(itemEntity, (byte) quantity, 0);
                var pickupEntity = EntityManager.Instantiate(ItemHitSystem.pickupItemPrefab);
                EntityManager.SetComponentData(pickupEntity, new PickupItem(characterEntity, worldItem));
                EntityManager.SetComponentData(pickupEntity, new DelayEvent(0, 0.01));
                input = "Cmd: Giving playerEntity Item [" + itemName + "] at index (" + itemIndex + ")";
            }
            return input;
        }

        private void AddToCommandLog(string newEntry)
        {
            for (int i = lastCommand.Length - 1; i >= 1; i--)
            {
                lastCommand[i] = lastInput[i - 1];
            }
            lastCommand[0] = newEntry;
        }

        private void AddToLog(string newEntry)
        {
            for (int i = lastInput.Length - 1; i >= 1; i--)
            {
                lastInput[i] = lastInput[i - 1];
            }
            lastInput[0] = newEntry;
        }

        public void DrawConsole()
        {
            // enter in commands
            GUILayout.Label("Welcome to Zoxel's Admin Console. If you are new here, try typing 'help'.");
            var newInput = GUILayout.TextField(input);
            if (newInput != input)
            {
                input = newInput;
            }
            // show previous commands
            for (int i = 0; i < lastInput.Length; i++)
            {
                GUILayout.Label(lastInput[i]);
            }
        }
    }
}