using Unity.Entities;
using Zoxel.Transforms;
using Zoxel.Players;
using Zoxel.Stats;
using Zoxel.Actions;
using Zoxel.Skills;
using Zoxel.Voxels;
using UnityEngine;

namespace Zoxel
{
    //! Debugs players target with various information.
    public partial class DebugTarget : MonoBehaviour
    {
        public static DebugTarget instance;
        private int fontSize = 22;
        private UnityEngine.Color fontColor = UnityEngine.Color.green;

        void Awake()
        {
            instance = this;
        }

        public void OnGUI()
        {
            GUI.skin.label.fontSize = fontSize;
            GUI.color = fontColor;
            DebugTargetUI();
        }

        public EntityManager EntityManager
        {
            get
            { 
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        public void DebugTargetUI()
        {
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            if (playerLinks.players.Length == 0)
            {
                return;
            }
            var player = playerLinks.players[0];
            GUILayout.Label("Debugging Player Target [" + player.Index + "]");
            var characterEntity = EntityManager.GetComponentData<CharacterLink>(player).character;
            // Targeting
            if (EntityManager.HasComponent<Target>(characterEntity) == false)
            {
                GUILayout.Label("Player Has No Target Component.");
            }
            else
            {
                var target = EntityManager.GetComponentData<Target>(characterEntity);
                var targetEntity = target.target;
                if (!EntityManager.Exists(targetEntity))
                {
                    GUILayout.Label("Player Has No Target.");
                }
                else
                {
                    //var targetPosition = EntityManager.GetComponentData<Translation>(targeter.target);
                    GUILayout.Label("   Target Distance [ " + target.targetDistance + "]");
                    GUILayout.Label("   Target Angle [ " + target.targetAngle + "]");
                    if (EntityManager.HasComponent<UserStatLinks>(targetEntity))
                    {
                        var targetStats = EntityManager.GetComponentData<UserStatLinks>(targetEntity);
                        GUILayout.Label("   userStatLinks [" + targetStats.stats.Length + "]");
                        for (int i = 0; i < targetStats.stats.Length; i++)
                        {
                            var userStatEntity = targetStats.stats[i];
                            if (EntityManager.HasComponent<SoulStat>(userStatEntity))
                            {
                                var soulStat = EntityManager.GetComponentData<LevelStat>(userStatEntity);
                                GUILayout.Label("   soulStat Lvl [" + soulStat.value + "] xp [" + soulStat.experienceRequired + " / " + soulStat.experienceGained + "]");
                            }
                            else if (EntityManager.HasComponent<StateStat>(userStatEntity))
                            {
                                var statValue = EntityManager.GetComponentData<StatValue>(userStatEntity);
                                var statValueMax = EntityManager.GetComponentData<StatValueMax>(userStatEntity);
                                GUILayout.Label("   stateStat [" + statValue.value + " / " + statValueMax.value + "]");
                            }
                        }
                        /*if (targetStats.states.Length > 0)
                        {
                            GUILayout.Label("   with [" + targetStats.states[0].value + "hp]");
                        }
                        else
                        {
                            GUILayout.Label("Target has no health.");
                        }
                        if (targetStats.states.Length > 1)
                        {
                            GUILayout.Label("   with [" + targetStats.states[1].value + "ep]");
                        }
                        else
                        {
                            GUILayout.Label("Target has no energy.");
                        }
                        if (targetStats.states.Length > 2)
                        {
                            GUILayout.Label("   with [" + targetStats.states[2].value + "mp]");
                        }
                        else
                        {
                            GUILayout.Label("Target has no mana.");
                        }*/
                    }
                    if (EntityManager.HasComponent<UserActionLinks>(targetEntity))
                    {
                        var targetActions = EntityManager.GetComponentData<UserActionLinks>(targetEntity);
                        var targetSkills = EntityManager.GetComponentData<UserSkillLinks>(targetEntity);
                        // var skillSelected = targetSkills.userSkillLinks[targetActions.selectedSkillIndex];
                        // GUILayout.Label("   with SkillType [" + ((SkillType)skillSelected.skillType) + "]");
                        GUILayout.Label("   with UserActionLinks Selected Action [" + targetActions.selectedAction + "]");
                        var action =  targetActions.actions[targetActions.selectedAction];
                        GUILayout.Label("   with Action [" + targetActions.selectedAction + "] [" 
                            + action.target.Index  + "]");
                    }
                    if (EntityManager.HasComponent<ChunkLink>(targetEntity))
                    {
                        var chunkLink = EntityManager.GetComponentData<ChunkLink>(targetEntity);
                        var chunkPosition = EntityManager.GetComponentData<ChunkPosition>(chunkLink.chunk);
                        GUILayout.Label("   Chunk Link Position [" + chunkPosition.position + "]");
                        var position = EntityManager.GetComponentData<Translation>(targetEntity).Value;
                        var chunkPosition2 = VoxelUtilities.GetChunkPosition(new int3(position), new int3(16, 16, 16));
                        GUILayout.Label("   Translation Chunk Position [" + chunkPosition2 + "]");
                    }
                }
            }
        }
    }
}