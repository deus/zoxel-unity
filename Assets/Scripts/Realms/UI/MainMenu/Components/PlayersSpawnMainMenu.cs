using Unity.Entities;

namespace Zoxel.Realms.UI
{
    //! All players spawn a main menu.
    public struct PlayersSpawnMainMenu : IComponentData { }
}