using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    public struct IconsSize : IComponentData
    {
        public float2 size;

        public IconsSize(float2 size)
        {
            this.size = size;
        }
    }
}