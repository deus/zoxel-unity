using Unity.Entities;
using Unity.Burst;
using Zoxel.Players;
using Zoxel.UI;
using Zoxel.Input;

namespace Zoxel.Realms.UI
{
    //! All players spawn a main menu.
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class PlayersMainMenuSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var spawnMainMenuPrefab = MainMenuSpawnSystem.spawnPanelPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<OnPlayerSpawned>()
                .WithAll<PlayersSpawnMainMenu>()
                .ForEach((Entity e, int entityInQueryIndex, in PlayerLinks playerLinks) =>
            {
                PostUpdateCommands.RemoveComponent<PlayersSpawnMainMenu>(entityInQueryIndex, e);
                if (playerLinks.players.Length > 0)
                {
                    var playerEntity = playerLinks.players[0];
                    var spawnMainMenuEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnMainMenuPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMainMenuEntity, new CharacterLink(playerEntity));
                }
                for (int i = 0; i < playerLinks.players.Length; i++)
                {
                    var playerEntity = playerLinks.players[i];
                    PostUpdateCommands.AddComponent(entityInQueryIndex, playerEntity, new SetControllerMapping(ControllerMapping.Menu));
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}