using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Input;
using Zoxel.Players;
using Zoxel.Textures;
using Zoxel.Rendering;

namespace Zoxel.Realms.UI
{
    //! Adds RefreshPlayerDevicesUI to playerDevicesUI when devices get updated.
    /**
    *   - Event System -
    *   \todo Move to Zoxel.Input.UI.
    */
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class PlayerDevicesTriggerRefreshSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery processQuery5;
        private EntityQuery processQuery6;
        private EntityQuery processQuery7;
        private EntityQuery processQuery8;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<PlayerDevicesUI>());
            processQuery5 = GetEntityQuery(ComponentType.ReadOnly<OnDeviceConnected>());
            processQuery6 = GetEntityQuery(ComponentType.ReadOnly<OnDeviceDisconnected>());
            processQuery7 = GetEntityQuery(ComponentType.ReadOnly<OnPlayerSpawned>());
            processQuery8 = GetEntityQuery(ComponentType.ReadOnly<OnPlayerDestroyed>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (!processQuery5.IsEmpty || !processQuery6.IsEmpty || !processQuery7.IsEmpty || !processQuery8.IsEmpty)
            {
                SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AddComponent<RefreshPlayerDevicesUI>(processQuery);
                return;
            }
        }
    }
}