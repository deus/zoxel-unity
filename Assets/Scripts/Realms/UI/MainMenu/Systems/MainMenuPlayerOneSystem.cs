using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Players;
using Zoxel.Players.Spawning;

namespace Zoxel.Realms.UI
{
    //! Spawns player entities with controllers.
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class MainMenuPlayerOneSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var spawnMainMenuPrefab = MainMenuSpawnSystem.spawnPanelPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref PlayerLinks playerLinks, in OnPlayerDestroyed onPlayerDestroyed) =>
            {
                if (playerLinks.players.Length > 0 && HasComponent<PlayerOne>(onPlayerDestroyed.player))
                {
                    // spawn new main menu for player 2 when player 1 is removed
                    var spawnMainMenuEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnMainMenuPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMainMenuEntity, new CharacterLink(playerLinks.players[0]));
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}