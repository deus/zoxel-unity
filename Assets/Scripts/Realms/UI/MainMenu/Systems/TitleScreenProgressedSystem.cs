using Unity.Entities;
using Unity.Burst;
using Zoxel.Players;

namespace Zoxel.Realms.UI
{
    //! Spawns main menu when a controller connects on start scene.
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class TitlescreenProgressedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<OnTitlescreenProgressed>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<OnTitlescreenProgressed>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<PlayersSpawnMainMenu>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}