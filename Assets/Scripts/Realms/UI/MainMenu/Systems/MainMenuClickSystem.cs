using Unity.Entities;
using Unity.Burst;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Characters;
using Zoxel.Networking.UI;

namespace Zoxel.Realms.UI
{
    //! Handles MainMenuButton's UIClickEvent's.
    /**
    *   - UIClickEvent System -
    */
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class MainMenuClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var spawnOptionsUIPrefab = OptionsSpawnSystem.spawnPanelPrefab;
            var spawnRealmLoadUIPrefab = RealmLoadUISpawnSystem.spawnPanelPrefab;
            var spawnServerUIPrefab = ServerUISpawnSystem.spawnPanelPrefab;
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var spawnCharacterUIPrefab = UICoreSystem.spawnCharacterUIPrefab;
            var exitGamePrefab = ExitGameSystem.exitGamePrefab;
            const float fadeOutExitTime = 3f;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities
                .WithAll<UIClickEvent, MainMenuButton>()
                .ForEach((Entity e, int entityInQueryIndex, in Child child, in ParentLink parentLink, in UIClickEvent uiClickEvent) =>
            {
                var controllerEntity = uiClickEvent.controller;
                var buttonType = uiClickEvent.buttonType;
                var arrayIndex = child.index;
                var panelEntity = parentLink.parent;
                if (panelEntity.Index == 0 || !HasComponent<UILink>(controllerEntity) || buttonType != ButtonActionType.Confirm)
                {
                    return;
                }
                if (arrayIndex == 0)
                {
                    var removeUIPrefab =  PostUpdateCommands.Instantiate(entityInQueryIndex, removeCharacterUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, removeUIPrefab, new RemoveUI(controllerEntity, panelEntity));
                    var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnRealmLoadUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new CharacterLink(controllerEntity));
                }
                else if (arrayIndex == 1)
                {
                    var removeUIPrefab =  PostUpdateCommands.Instantiate(entityInQueryIndex, removeCharacterUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, removeUIPrefab, new RemoveUI(controllerEntity, panelEntity));
                    var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnServerUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new CharacterLink(controllerEntity));
                }
                else if (arrayIndex == 2)
                {
                    // options
                    var removeUIPrefab = PostUpdateCommands.Instantiate(entityInQueryIndex, removeCharacterUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, removeUIPrefab, new RemoveUI(controllerEntity, panelEntity));
                    var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnOptionsUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new CharacterLink(controllerEntity));
                    // UICoreSystem.SpawnCharacterUI(PostUpdateCommands, entityInQueryIndex, spawnCharacterUIPrefab, controllerEntity, PanelType.Options, elapsedTime, 0.1f);
                }
                else if (arrayIndex == 3)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, removeCharacterUIPrefab), new RemoveUI(controllerEntity, panelEntity));
                    // Exit Realm System? create one for fading out
                    PostUpdateCommands.AddComponent(entityInQueryIndex, controllerEntity, new FadeOutScreen(fadeOutExitTime));
                    // TODO: Add sleep Animation here or fade out
                    var exitGameEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, exitGamePrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, exitGameEntity, new DelayEvent(elapsedTime, fadeOutExitTime));
                }
			}).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                    // PostUpdateCommands.AddComponent<SpawnServerUI>(entityInQueryIndex, spawnEntity);