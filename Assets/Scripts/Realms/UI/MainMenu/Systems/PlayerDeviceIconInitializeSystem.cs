using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Input;

namespace Zoxel.Realms.UI
{
    //! Initializes a PlayerDeviceIcon materials.
    /**
    *   - Initialize System -
    *   \todo Move to Zoxel.Input.UI.
    */
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class PlayerDeviceIconInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var uiDatam = UIManager.instance.uiDatam;
            var playersConnected = uiDatam.playersConnected;
            var playersDisconnected = uiDatam.playersDisconnected;
            var devices = uiDatam.devices;
            var iconMaterial = UIManager.instance.materials.iconMaterial;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, PlayerIcon>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in Size2D size2D, in Zoxel.Transforms.Child child) =>
            {
                zoxMesh.layer = worldUILayer;
                if (size2D.size.x != 0 && size2D.size.y != 0)
                {
                    zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size2D.size, 0, 0);
                }
                zoxMesh.material = new UnityEngine.Material(iconMaterial);
                var texture = playersDisconnected[child.index];
                zoxMesh.material.SetTexture("_BaseMap", texture);
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
            Entities
                .WithAll<InitializeEntity, DeviceIcon>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in Size2D size2D, in DeviceTypeData deviceTypeData) =>
            {
                zoxMesh.layer = worldUILayer;
                if (size2D.size.x != 0 && size2D.size.y != 0)
                {
                    zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size2D.size, 0, 0);
                }
                zoxMesh.material = new UnityEngine.Material(iconMaterial);
                var texture = devices[deviceTypeData.type];
                zoxMesh.material.SetTexture("_BaseMap", texture);
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
        }
    }
}