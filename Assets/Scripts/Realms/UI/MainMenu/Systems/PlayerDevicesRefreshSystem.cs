using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Input;
using Zoxel.Players;
using Zoxel.Textures;
using Zoxel.Rendering;

namespace Zoxel.Realms.UI
{
    //! Spawns the PlayerDevices ui.
    /**
    *   - UI Spawn System -
    *   Add / Remove Player Icons
    *   Add / Remove Device Icons
    *   If Player loses it's devices, it will become disconnected, refresh texture based on that.
    *   \todo Move to Zoxel.Input.UI.
    */
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class PlayerDevicesRefreshSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery devicesQuery;
        private EntityQuery playersQuery;
        private EntityQuery processQuery2;
        private EntityQuery processQuery3;
        public static Entity playerIconPrefab;
        public static Entity deviceIconPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery2 = GetEntityQuery(ComponentType.ReadOnly<DeviceLinks>());
            processQuery3 = GetEntityQuery(ComponentType.ReadOnly<PlayerLinks>());
            devicesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Device>(),
                ComponentType.ReadOnly<DeviceTypeData>());
            playersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Player>(),
                ComponentType.ReadOnly<ColorData>());
            RequireForUpdate(processQuery);
        }

        public static void InitializePrefabs(EntityManager EntityManager)
        {
            if (PlayerDevicesRefreshSystem.playerIconPrefab.Index != 0)
            {
                return;
            }
            var playerIconPrefab = EntityManager.Instantiate(UICoreSystem.iconPrefabs.icon);
            PlayerDevicesRefreshSystem.playerIconPrefab = playerIconPrefab;
            EntityManager.AddComponent<Prefab>(playerIconPrefab);
            EntityManager.RemoveComponent<Icon>(playerIconPrefab);
            EntityManager.AddComponent<AuthoredTexture>(playerIconPrefab);
            EntityManager.AddComponent<DontDestroyTexture>(playerIconPrefab);
            var deviceIconPrefab = EntityManager.Instantiate(playerIconPrefab);
            PlayerDevicesRefreshSystem.deviceIconPrefab = deviceIconPrefab;
            EntityManager.AddComponent<Prefab>(deviceIconPrefab);
            EntityManager.AddComponent<DeviceIcon>(deviceIconPrefab);
            EntityManager.AddComponent<DeviceTypeData>(deviceIconPrefab);
            // EntityManager.SetComponentData(deviceIconPrefab, new LocalScale(new float3(1.3f, 1.3f, 1.3f)));
            // EntityManager.SetComponentData(deviceIconPrefab, new NonUniformScale { Value = new float3(1.3f, 1.3f, 1.3f) });
            EntityManager.AddComponent<PlayerIcon>(playerIconPrefab);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs(EntityManager);
            var devicesHome = (InputSystemGroup.devicesHome);
            var playerHome = (PlayerSystemGroup.playerHome);
            var deviceIconPrefab = PlayerDevicesRefreshSystem.deviceIconPrefab;
            var playerIconPrefab = PlayerDevicesRefreshSystem.playerIconPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<RefreshPlayerDevicesUI>(processQuery);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var deviceHomeEntities = processQuery2.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var deviceLinks = GetComponentLookup<DeviceLinks>(true);
            deviceHomeEntities.Dispose();
            var playerHomeEntities = processQuery3.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var playerLinks = GetComponentLookup<PlayerLinks>(true);
            playerHomeEntities.Dispose();
            var deviceEntities = devicesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
			var deviceTypeDatas = GetComponentLookup<DeviceTypeData>(true);
			var deviceConnecteds = GetComponentLookup<DeviceConnected>(true);
            deviceEntities.Dispose();
            var playerEntities = playersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleE);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleE);
			var colorDatas = GetComponentLookup<ColorData>(true);
            playerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<RefreshPlayerDevicesUI>()
                .ForEach((Entity e, int entityInQueryIndex, ref Childrens childrens, in IconsSize iconsSize) =>
            {
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, childrens.children[i]);
                }
                childrens.Dispose();
                // if removing some
                // if adding more
                var deviceLinks2 = deviceLinks[devicesHome];
                var playerLinks2 = playerLinks[playerHome];
                var iconSize = iconsSize.size;
                PlayerDevicesRefreshSystem.SpawnPlayerDeviceIcons(PostUpdateCommands, entityInQueryIndex, deviceIconPrefab, playerIconPrefab, e,
                    in deviceLinks2, in playerLinks2, iconSize, in deviceTypeDatas, in deviceConnecteds, in colorDatas);
            })  .WithReadOnly(playerLinks)
                .WithReadOnly(deviceLinks)
                .WithReadOnly(deviceTypeDatas)
                .WithReadOnly(deviceConnecteds)
                .WithReadOnly(colorDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static void SpawnPlayerDeviceIcons(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex,
            Entity deviceIconPrefab, Entity playerIconPrefab, Entity parent,
            in DeviceLinks deviceLinks, in PlayerLinks playerLinks, float2 iconSize,
            in ComponentLookup<DeviceTypeData> deviceTypeDatas, in ComponentLookup<DeviceConnected> deviceConnecteds,
            in ComponentLookup<ColorData> colorDatas)
        {
            // players connected ui - link to playerHome
            byte count = 0;
            var totalPlayers = (float) playerLinks.players.Length;
            for (byte j = 0; j < playerLinks.players.Length; j++)
            {
                var playerEntity = playerLinks.players[j];
                var colorData = colorDatas[playerEntity];
                var localPosition = new float3(iconSize.x / 2f + -iconSize.x * (totalPlayers / 2f) + iconSize.x * j, 0, 0);
                var iconEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, playerIconPrefab, parent, localPosition, iconSize);
                UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, iconEntity, count);
                PostUpdateCommands.SetComponent(entityInQueryIndex, iconEntity, new MaterialBaseColor(colorData.color.ToFloat4()));
                count++;
            }
            // for (byte j = 0; j < deviceLinks.devices.Length; j++)
            if (deviceLinks.devices.IsCreated)
            {
                var totalDevices = (float) deviceLinks.Length;
                byte j = 0;
                foreach (var KVP in deviceLinks.devices)
                {
                    var deviceEntity = KVP.Value;
                    var deviceTypeData = deviceTypeDatas[deviceEntity];
                    var localPosition = new float3(iconSize.x / 2f + -iconSize.x * (totalDevices / 2f) + iconSize.x * j, - iconSize.y, 0);
                    var iconEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, deviceIconPrefab, parent, localPosition, iconSize);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, iconEntity, count);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, iconEntity, deviceTypeData);
                    j++;
                    count++;
                    if (deviceConnecteds.HasComponent(deviceEntity))
                    {
                        var playerEntity = deviceConnecteds[deviceEntity].player;
                        var colorData = colorDatas[playerEntity];
                        PostUpdateCommands.SetComponent(entityInQueryIndex, iconEntity, new MaterialBaseColor(colorData.color.ToFloat4()));
                    }
                }
            }
            PostUpdateCommands.AddComponent(entityInQueryIndex, parent, new OnChildrenSpawned(count));
        }
    }
}