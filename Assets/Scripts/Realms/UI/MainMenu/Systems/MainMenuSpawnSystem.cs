﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;
using Zoxel.Input;
using Zoxel.Players;

namespace Zoxel.Realms.UI
{
    //! Spawns the MainMenu ui.
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class MainMenuSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private NativeArray<Text> texts;
        private float2 panelSize;
        private Entity panelPrefab;
        private Entity buttonPrefab;
        public static Entity spawnPanelPrefab;
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;
        private EntityQuery processQuery2;
        private EntityQuery processQuery3;
        private EntityQuery devicesQuery;
        private EntityQuery playersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnMainMenu),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnPanelPrefab = EntityManager.CreateEntity(spawnArchetype);
            texts = new NativeArray<Text>(6, Allocator.Persistent);
            texts[0] = new Text("Start");
            texts[1] = new Text("Online");
            texts[2] = new Text("Options");
            texts[3] = new Text("Escape");
            // this is done to give spacing for the devices ui
            texts[4] = new Text("     \n     ");
            texts[5] = new Text("Zoxel");
            uiHoldersQuery = GetEntityQuery(
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<UILink>());
            processQuery2 = GetEntityQuery(ComponentType.ReadOnly<DeviceLinks>());
            processQuery3 = GetEntityQuery(ComponentType.ReadOnly<PlayerLinks>());
            devicesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Device>(),
                ComponentType.ReadOnly<DeviceTypeData>());
            playersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Player>(),
                ComponentType.ReadOnly<ColorData>());
            RequireForUpdate(processQuery);
            RequireForUpdate<SoundSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var soundSettings = GetSingleton<SoundSettings>();
            HeaderSpawnSystem.InitializePrefabs(EntityManager);
            var headerPrefab = HeaderSpawnSystem.headerPrefab;
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var soundVolume = soundSettings.uiSoundVolume;
            var uiDatam = UIManager.instance.uiDatam;
            var buttonStyle = uiDatam.mainMenuButtonStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var headerStyle = uiDatam.headerStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var iconSize = new float2(buttonStyle.fontSize, buttonStyle.fontSize);
            var texts = this.texts;
            var panelSize = this.panelSize;
            var playerIconPrefab = PlayerDevicesRefreshSystem.playerIconPrefab;
            var deviceIconPrefab = PlayerDevicesRefreshSystem.deviceIconPrefab;
            var buttonPrefab = this.buttonPrefab;
            var panelPrefab = this.panelPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var devicesHome = (InputSystemGroup.devicesHome);
            var playerHome = (PlayerSystemGroup.playerHome);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uiHolderEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
            uiHolderEntities.Dispose();
            var deviceHomeEntities = processQuery2.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var deviceLinks = GetComponentLookup<DeviceLinks>(true);
            deviceHomeEntities.Dispose();
            var playerHomeEntities = processQuery3.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var playerLinks = GetComponentLookup<PlayerLinks>(true);
            playerHomeEntities.Dispose();
            var deviceEntities = devicesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
			var deviceTypeDatas = GetComponentLookup<DeviceTypeData>(true);
			var deviceConnecteds = GetComponentLookup<DeviceConnected>(true);
            deviceEntities.Dispose();
            var playerEntities = playersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleE);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleE);
			var colorDatas = GetComponentLookup<ColorData>(true);
            playerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<SpawnMainMenu>()
                .ForEach((int entityInQueryIndex, in CharacterLink characterLink) =>
            {
                var deviceLinks2 = deviceLinks[devicesHome];
                var playerLinks2 = playerLinks[playerHome];
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, panelPrefab);
                var headerText = texts[5].Clone();
                HeaderSpawnSystem.SpawnHeaderUI(PostUpdateCommands, entityInQueryIndex, headerPrefab, e3, panelSize, headerStyle.fontSize, headerStyle.textPadding,
                    in headerText);
                var buttonsCount = (byte) (texts.Length - 1);
                PostUpdateCommands.AddComponent(entityInQueryIndex, e3, new OnChildrenSpawned(buttonsCount));
                for (byte i = 0; i < buttonsCount; i++)
                {
                    var menuText = texts[i].Clone();
                    var seed = (int) (128 + elapsedTime + i * 2048 + 4196 * entityInQueryIndex);
                    var buttonEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, buttonPrefab, e3);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, buttonEntity, i);
                    UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, buttonEntity, e3);
                    UICoreSystem.SetSound(PostUpdateCommands, entityInQueryIndex, buttonEntity, seed, soundVolume);
                    UICoreSystem.SetSeed(PostUpdateCommands, entityInQueryIndex, buttonEntity, seed);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, buttonEntity, new RenderText(menuText));
                    // if last button
                    if (i == buttonsCount - 1)
                    {
                        PostUpdateCommands.AddComponent<PlayerDevicesUI>(entityInQueryIndex, buttonEntity);
                        PostUpdateCommands.AddComponent<Childrens>(entityInQueryIndex, buttonEntity);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, buttonEntity, new IconsSize(iconSize));
                        // players connected ui - link to playerHome
                        PlayerDevicesRefreshSystem.SpawnPlayerDeviceIcons(PostUpdateCommands, entityInQueryIndex, deviceIconPrefab, playerIconPrefab, buttonEntity,
                            in deviceLinks2, in playerLinks2, iconSize, in deviceTypeDatas, in deviceConnecteds, in colorDatas);
                    }
                }
                if (characterLink.character.Index > 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, characterLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab),
                        new OnUISpawned(characterLink.character, 1));
                    if (cameraLinks.HasComponent(characterLink.character))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLinks[characterLink.character]);
                    }
                }
            })  .WithReadOnly(texts)
                .WithReadOnly(cameraLinks)
                .WithReadOnly(playerLinks)
                .WithReadOnly(deviceLinks)
                .WithReadOnly(deviceTypeDatas)
                .WithReadOnly(deviceConnecteds)
                .WithReadOnly(colorDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private void InitializePrefabs()
        {
            if (panelPrefab.Index != 0)
            {
                return;
            }
            PlayerDevicesRefreshSystem.InitializePrefabs(EntityManager);
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var buttonStyle = uiDatam.mainMenuButtonStyle.GetStyle(uiScale);
            var minSize = new float2(9 * buttonStyle.fontSize, 0);
            panelPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(panelPrefab);
            EntityManager.AddComponent<MainMenu>(panelPrefab);
            EntityManager.AddComponent<DeviceConnector>(panelPrefab);
            EntityManager.SetComponentData(panelPrefab, new PanelUI(PanelType.MainMenu));
            EntityManager.AddComponentData(panelPrefab, new PanelMinSize(minSize));
            EntityManager.RemoveComponent<SpawnHeaderUI>(panelPrefab);
            //! \todo Get AutoY to work on different size elements.
            // EntityManager.AddComponent<AutoGridY>(panelPrefab);
            var gridUI = new GridUI();
            gridUI.gridSize = new int2(1, texts.Length);
            gridUI.margins = 0.2f * new float2(buttonStyle.fontSize, buttonStyle.fontSize);
            gridUI.padding = uiDatam.GetMenuPaddings(uiScale);
            var gridUISize = new GridUISize(new float2(buttonStyle.fontSize * 8 + buttonStyle.textPadding.x * 2f, buttonStyle.fontSize + buttonStyle.textPadding.y * 2f));
            EntityManager.SetComponentData(panelPrefab, gridUI);
            EntityManager.SetComponentData(panelPrefab, gridUISize);
            panelSize = gridUI.GetSize(gridUISize.size);
            EntityManager.SetComponentData(panelPrefab, new Size2D(panelSize));
            buttonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(buttonPrefab);
            EntityManager.AddComponent<MainMenuButton>(buttonPrefab);
            UICoreSystem.SetRenderTextData(EntityManager, buttonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, buttonStyle.fontSize, buttonStyle.textPadding);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }
    }
}