using Unity.Entities;

namespace Zoxel.Realms.UI
{
    //! For initially opening or switching between RealmUIs.
    public struct OpenRealmUI : IComponentData
    {
        public byte type;

        public OpenRealmUI(byte type)
        {
            this.type = type;
        }
    }
}