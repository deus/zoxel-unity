using Unity.Entities;

namespace Zoxel.Realms.UI
{
    //! Delays game ui changes too fast
    public struct RealmUIDelay : IComponentData
    {
        public byte count;
    }
}