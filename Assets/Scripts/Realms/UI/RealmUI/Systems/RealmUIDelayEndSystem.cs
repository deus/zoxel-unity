using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Realms.UI
{
    //! This system removes control away from the player while they use game uis & changes post processing
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class RealmUIDelayEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref RealmUIDelay realmUIDelay) =>
            {
                realmUIDelay.count++;
                if (realmUIDelay.count == 16)
                {
                    PostUpdateCommands.RemoveComponent<RealmUIDelay>(entityInQueryIndex, e);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}