using Unity.Entities;
using Unity.Burst;
using Zoxel.UI;
using Zoxel.Items.UI;
using Zoxel.Stats.UI;
using Zoxel.Realms.UI;
using Zoxel.Players.UI;
using Zoxel.Skills.UI;
using Zoxel.Maps;
using Zoxel.Quests.UI;
using Zoxel.Crafting.UI;
using Zoxel.Skills.UI.Skilltrees;

namespace Zoxel.Players.UI
{
    //! Sets RealmUILink after uis are updated.
    /**
    *   \todo Link with PanelType instead to know whats open.
    *   \todo This linking is awkard for crafting atm.
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class RealmUILinkUpdatedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<OnUILinkUpdated>()
                .ForEach((Entity e, int entityInQueryIndex, ref RealmUILink realmUILink, in UILink uiLink) =>
            {
                for (int i = 0; i < uiLink.uis.Length; i++)
                {
                    var ui = uiLink.uis[i];
                    if (HasComponent<CraftUI>(ui))
                    {
                        realmUILink.index = (byte) PanelType.CraftUI;
                        realmUILink.previousUI2 = ui;
                        return;
                    }
                }
                for (int i = 0; i < uiLink.uis.Length; i++)
                {
                    var ui = uiLink.uis[i];
                    var panelType = PanelType.None;
                    // todo: remove this from here!
                    if (HasComponent<PauseMenu>(ui))
                    {
                        panelType = PanelType.PauseMenu;
                    }
                    else if (HasComponent<InventoryUI>(ui))
                    {
                        panelType = PanelType.InventoryUI;
                    }
                    else if (HasComponent<EquipmentUI>(ui))
                    {
                        panelType = PanelType.EquipmentUI;
                    }
                    else if (HasComponent<StatsUI>(ui))
                    {
                        panelType = PanelType.StatsUI;
                    }
                    else if (HasComponent<StatusUI>(ui))
                    {
                        panelType = PanelType.StatusUI;
                    }
                    else if (HasComponent<SkillsUI>(ui))
                    {
                        panelType = PanelType.SkillsUI;
                    }
                    else if (HasComponent<SkilltreeUI>(ui))
                    {
                        panelType = PanelType.SkilltreeUI;
                    }
                    else if (HasComponent<QuestlogUI>(ui))
                    {
                        panelType = PanelType.QuestlogUI;
                    }
                    else if (HasComponent<MapUI>(ui) && !HasComponent<Minimap>(ui))
                    {
                        panelType = PanelType.Map;
                    }
                    if (panelType != PanelType.None)
                    {
                        realmUILink.index = (byte) panelType;
                        realmUILink.previousUI = ui;
                        realmUILink.previousUI2 = new Entity();
                        // PostUpdateCommands.SetComponent(entityInQueryIndex, e, new RealmUILink(panelType, ui));
                        break;
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
} 
