using Unity.Entities;
using Unity.Burst;
using Zoxel.UI;
using Zoxel.Stats.UI;
using Zoxel.Actions.UI;

namespace Zoxel.Realms.UI
{
    //! Removes Crosshair, Actionbar and ExperienceBar UILink entities.
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class RealmUICloseSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var delayTime = 0.0;
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<CloseRealmUI>()
                .ForEach((Entity e, int entityInQueryIndex, in UILink uiLink) =>
            {
                PostUpdateCommands.RemoveComponent<CloseRealmUI>(entityInQueryIndex, e);
                for (int i = 0; i < uiLink.uis.Length; i++)
                {
                    var ui = uiLink.uis[i];
                    if (HasComponent<Crosshair>(ui))
                    {
                        UICoreSystem.RemoveUI(PostUpdateCommands, entityInQueryIndex, removeCharacterUIPrefab, e, ui, elapsedTime, delayTime);
                    }
                    else if (HasComponent<Actionbar>(ui))
                    {
                        UICoreSystem.RemoveUI(PostUpdateCommands, entityInQueryIndex, removeCharacterUIPrefab, e, ui, elapsedTime, delayTime);
                    }
                    else if (HasComponent<ExperienceBar>(ui))
                    {
                        UICoreSystem.RemoveUI(PostUpdateCommands, entityInQueryIndex, removeCharacterUIPrefab, e, ui, elapsedTime, delayTime);
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}