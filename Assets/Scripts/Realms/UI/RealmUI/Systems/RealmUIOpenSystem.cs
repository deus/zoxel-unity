using Unity.Entities;
using Unity.Burst;
using Zoxel.Cameras;
using Zoxel.UI;
using Zoxel.Actions;
using Zoxel.UI.Players;
using Zoxel.Maps;
using Zoxel.Weather.UI;
using Zoxel.Stats;
using Zoxel.Stats.UI;
using Zoxel.Quests.UI;
using Zoxel.Actions.UI;
using Zoxel.Logs.UI;
using Zoxel.Input.UI;

namespace Zoxel.Realms.UI
{
    //! Spawns the generic Zoxel game UI entities.
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class RealmUIOpenSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity spawnQuestTrackerPrefab;
        private Entity spawnWeatherUIPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnQuestTrackerArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnQuestTracker),
                typeof(CharacterLink),
                typeof(DelayEvent));
            spawnQuestTrackerPrefab = EntityManager.CreateEntity(spawnQuestTrackerArchetype);
            var spawnWeatherUIArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnWeatherUI),
                typeof(CharacterLink),
                typeof(DelayEvent));
            spawnWeatherUIPrefab = EntityManager.CreateEntity(spawnWeatherUIArchetype);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var disableActionbar = UIManager.instance.uiSettings.disableActionbar;
            var disableStatbars = UIManager.instance.uiSettings.disableStatbars;
            var disableCrosshair = UIManager.instance.uiSettings.disableCrosshair;
            var disableTimeUI = UIManager.instance.uiSettings.disableTimeUI;
            const float spawnDelay = 0.0f;
            var elapsedTime = World.Time.ElapsedTime;
            var disableMinimap = UIManager.instance.uiSettings.disableMinimap;
            var disableQuestTracker = UIManager.instance.uiSettings.disableQuestTracker;
            var spawnCharacterUIPrefab = UICoreSystem.spawnCharacterUIPrefab;
            var spawnCrosshairPrefab = CrosshairSpawnSystem.spawnCrosshairPrefab;
            var spawnQuestTrackerPrefab = this.spawnQuestTrackerPrefab;
            var spawnMinimapPrefab = MinimapSpawnSystem.spawnPrefab;
            var spawnLogUIPrefab = LogUISpawnSystem.spawnPrefab;
            var spawnWeatherUIPrefab = this.spawnWeatherUIPrefab;
            var spawnActionbarPrefab = ActionbarSpawnSystem.spawnActionbarPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities
                .WithNone<DeadEntity, DestroyEntity, InitializeEntity>()
                .ForEach((Entity e, int entityInQueryIndex, in SpawnRealmUI spawnRealmUI, in ControllerLink controllerLink) =>
            {
                // complete event
                PostUpdateCommands.RemoveComponent<SpawnRealmUI>(entityInQueryIndex, e);
                if (spawnRealmUI.type == SpawnRealmUIType.ChestUI)
                {
                    // Spawn Croshair
                    if (!disableCrosshair)
                    {
                        var spawnCrosshairEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnCrosshairPrefab);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, spawnCrosshairEntity, new CharacterLink(e));
                        // PostUpdateCommands.SetComponent(entityInQueryIndex, spawnCrosshairEntity, new DelayEvent(elapsedTime, spawnDelay));
                    }
                    // Spawn XPbar
                    if (!disableStatbars)
                    {
                        PostUpdateCommands.AddComponent<SpawnExperienceBar>(entityInQueryIndex, e);
                    }
                    // Spawn Actionbar
                    if (!disableActionbar)
                    {
                        var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnActionbarPrefab);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new CharacterLink(e));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new DelayEvent(elapsedTime, spawnDelay));
                    }
                    return;
                }
                if (!disableStatbars)
                {
                    if (spawnRealmUI.type == SpawnRealmUIType.SpawnExperiencebar)
                    {
                        PostUpdateCommands.AddComponent<SpawnExperienceBar>(entityInQueryIndex, e);
                    }
                    else
                    {
                        PostUpdateCommands.AddComponent<SpawnStatbar>(entityInQueryIndex, e);
                    }
                }
                if (!disableCrosshair)
                {
                    var spawnCrosshairEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnCrosshairPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnCrosshairEntity, new CharacterLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnCrosshairEntity, new DelayEvent(elapsedTime, spawnDelay));
                }
                if (spawnRealmUI.type != SpawnRealmUIType.DisableSpawnActionbar && !disableActionbar)
                {
                    var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnActionbarPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new CharacterLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new DelayEvent(elapsedTime, spawnDelay));
                }
                if (!disableQuestTracker)
                {
                    var spawnQuestTrackerEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnQuestTrackerPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnQuestTrackerEntity, new CharacterLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnQuestTrackerEntity, new DelayEvent(elapsedTime, spawnDelay));
                }
                if (!disableMinimap)
                {
                    var spawnMinimapEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnMinimapPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMinimapEntity, new CharacterLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMinimapEntity, new DelayEvent(elapsedTime, spawnDelay));
                }
                if (!disableTimeUI)
                {
                    var spawnWeatherUIEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnWeatherUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnWeatherUIEntity, new CharacterLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnWeatherUIEntity, new DelayEvent(elapsedTime, spawnDelay));
                }
                // if (!disableMinimap)
                {
                    var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnLogUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new CharacterLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new DelayEvent(elapsedTime, spawnDelay));
                }
			}).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}