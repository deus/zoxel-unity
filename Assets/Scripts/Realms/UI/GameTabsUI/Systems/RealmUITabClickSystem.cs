using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Input;

namespace Zoxel.Realms.UI
{
    //! Handles UIClickEvent for TaskbarButton buttons.
    /**
    *   - UIClickEvent System -
    */
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class RealmUITabClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllersQuery = GetEntityQuery(
                ComponentType.ReadOnly<CharacterLink>(),
                ComponentType.ReadOnly<Controller>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = (float) World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var characterLinks = GetComponentLookup<CharacterLink>(true);
            controllerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<TaskbarButton>()
                .ForEach((Entity e, int entityInQueryIndex, in Child child, in ParentLink parentLink, in UIClickEvent uiClickEvent) =>
            {
                // UnityEngine.Debug.LogError("RealmUITabClickSystem: " + child.index);
                var controllerEntity = uiClickEvent.controller;
                var characterEntity = characterLinks[controllerEntity].character;
                if (!HasComponent<RealmUIDelay>(characterEntity) && !HasComponent<SwitchRealmUI>(characterEntity))
                {
                    PostUpdateCommands.AddComponent<RealmUIDelay>(entityInQueryIndex, characterEntity);
                    var arrayIndex = (byte) child.index;
                    PostUpdateCommands.AddComponent(entityInQueryIndex, characterEntity, new SwitchRealmUI(arrayIndex));
                }
			})  .WithReadOnly(characterLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}