using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Maps;
using Zoxel.Items.UI;
using Zoxel.Crafting.UI;
using Zoxel.Stats.UI;
using Zoxel.Quests.UI;
using Zoxel.Skills.UI;
using Zoxel.Skills.UI.Skilltrees;

namespace Zoxel.Realms.UI
{
    //! Switches between character game uis.
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class RealmUISwitchSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var spawnPauseMenuPrefab = PauseMenuSpawnSystem.spawnPrefab;
            var spawnMapPrefab = MapSpawnSystem.spawnMapPrefab;
            var spawnStatusUIPrefab = StatusUISpawnSystem.spawnStatusUIPrefab;
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var spawnCharacterUIPrefab = UICoreSystem.spawnCharacterUIPrefab;
            var spawnInventoryUIPrefab = ItemUISpawnSystem.spawnInventoryUIPrefab;
            var spawnEquipmentUIPrefab = EquipmentUISpawnSystem.spawnEquipmentUIPrefab;
            var spawnStatsUIPrefab = StatsUISpawnSystem.spawnStatsUIPrefab;
            var spawnCraftUIPrefab = CraftUISpawnSystem.spawnCraftUIPrefab;
            var spawnQuestlogUIPrefab = QuestlogUISpawnSystem.spawnQuestlogUIPrefab;
            var spawnSkillbookPrefab = SkillsUISpawnSystem.spawnSkillbookPrefab;
            var spawnSkilltreePrefab = SkilltreeSpawnSystem.spawnSkilltreePrefab;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity, RealmUIDelay>()
                .ForEach((Entity e, int entityInQueryIndex, ref RealmUILink realmUILink, in SwitchRealmUI switchRealmUI, in UILink uiLink) =>
            {
                PostUpdateCommands.RemoveComponent<SwitchRealmUI>(entityInQueryIndex, e);
                if (HasComponent<RealmUIDelay>(e) || realmUILink.index == switchRealmUI.newUI)
                {
                    return;
                }
                UICoreSystem.RemoveUI(PostUpdateCommands, entityInQueryIndex, removeCharacterUIPrefab, e, realmUILink.previousUI);
                if (realmUILink.previousUI2.Index > 0)
                {
                    UICoreSystem.RemoveUI(PostUpdateCommands, entityInQueryIndex, removeCharacterUIPrefab, e, realmUILink.previousUI2);
                }
                realmUILink.index = switchRealmUI.newUI;
                realmUILink.previousUI = new Entity();
                realmUILink.previousUI2 = new Entity();
                RealmUISpawnSystem.SpawnRealmUI(PostUpdateCommands, entityInQueryIndex, elapsedTime, e, realmUILink.index,
                    spawnPauseMenuPrefab, spawnCharacterUIPrefab, spawnInventoryUIPrefab,
                    spawnEquipmentUIPrefab, spawnCraftUIPrefab, spawnStatusUIPrefab, spawnStatsUIPrefab, spawnQuestlogUIPrefab, spawnSkillbookPrefab,
                    spawnSkilltreePrefab, spawnMapPrefab);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Entities
                .WithNone<DestroyEntity>()
                .WithAll<InitializeEntity, Taskbar>()
                .ForEach((Entity e, int entityInQueryIndex, in Childrens childrens, in CharacterLink characterLink) =>
            {
                var index = EntityManager.GetComponentData<RealmUILink>(characterLink.character).index;
                PostUpdateCommands2.AddComponent(e, new SetToggleGroup(index));
			}).WithoutBurst().Run();
        }
    }
}