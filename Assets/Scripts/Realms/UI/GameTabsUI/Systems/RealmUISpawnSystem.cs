using Unity.Entities;
using Unity.Burst;
using Zoxel.UI;
using Zoxel.Actions;
using Zoxel.Cameras;
using Zoxel.Maps;
using Zoxel.Items.UI;
using Zoxel.Crafting.UI;
using Zoxel.Stats.UI;
using Zoxel.Quests.UI;
using Zoxel.Skills.UI;
using Zoxel.Skills.UI.Skilltrees;

namespace Zoxel.Realms.UI
{
    //! Removes control away from the player while they use game uis, also changes post processing.
    /**
    *   - UI Spawn System -
    *   \todo Fix. This crashes when Inventory + Crafting UI spawns at same time.
    */
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class RealmUISpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var spawnPauseMenuPrefab = PauseMenuSpawnSystem.spawnPrefab;
            var spawnMapPrefab = MapSpawnSystem.spawnMapPrefab;
            var spawnStatusUIPrefab = StatusUISpawnSystem.spawnStatusUIPrefab;
            var spawnCharacterUIPrefab = UICoreSystem.spawnCharacterUIPrefab;
            var spawnInventoryUIPrefab = ItemUISpawnSystem.spawnInventoryUIPrefab;
            var spawnCraftUIPrefab = CraftUISpawnSystem.spawnCraftUIPrefab;
            var spawnEquipmentUIPrefab = EquipmentUISpawnSystem.spawnEquipmentUIPrefab;
            var spawnStatsUIPrefab = StatsUISpawnSystem.spawnStatsUIPrefab;
            var spawnQuestlogUIPrefab = QuestlogUISpawnSystem.spawnQuestlogUIPrefab;
            var spawnSkillbookPrefab = SkillsUISpawnSystem.spawnSkillbookPrefab;
            var spawnSkilltreePrefab = SkilltreeSpawnSystem.spawnSkilltreePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .WithAll<OpenRealmUI>()
                .ForEach((Entity e, int entityInQueryIndex, in OpenRealmUI openRealmUI) =>
            {
                PostUpdateCommands.RemoveComponent<OpenRealmUI>(entityInQueryIndex, e);
                SpawnRealmUI(PostUpdateCommands, entityInQueryIndex, elapsedTime, e, openRealmUI.type, spawnPauseMenuPrefab,
                    spawnCharacterUIPrefab, spawnInventoryUIPrefab, spawnEquipmentUIPrefab,
                    spawnCraftUIPrefab, spawnStatusUIPrefab, spawnStatsUIPrefab, spawnQuestlogUIPrefab, spawnSkillbookPrefab, spawnSkilltreePrefab, spawnMapPrefab);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static void SpawnRealmUI(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, double elapsedTime, Entity e, byte spawnType2,
            Entity spawnPauseMenuPrefab, Entity spawnCharacterUIPrefab, Entity spawnInventoryUIPrefab, Entity spawnEquipmentUIPrefab,
            Entity spawnCraftUIPrefab, Entity spawnStatusUIPrefab,
            Entity spawnStatsUIPrefab, Entity spawnQuestlogUIPrefab, Entity spawnSkillbookPrefab, Entity spawnSkilltreePrefab, Entity spawnMapPrefab)
        {
            const float spawnDelay = 0f;
            var spawnType = (PanelType) spawnType2;
            var spawnEntity = new Entity();
            if (spawnType == PanelType.PauseMenu)
            {
                spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnPauseMenuPrefab);
            }
            else if (spawnType == PanelType.Map)
            {
                spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnMapPrefab);
            }
            else if (spawnType == PanelType.StatusUI)
            {
                spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnStatusUIPrefab);
            }
            else if (spawnType == PanelType.InventoryUI)
            {
                spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnInventoryUIPrefab);
            }
            else if (spawnType == PanelType.EquipmentUI)
            {
                spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnEquipmentUIPrefab);
            }
            else if (spawnType == PanelType.StatsUI)
            {
                spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnStatsUIPrefab);
            }
            else if (spawnType == PanelType.QuestlogUI)
            {
                spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnQuestlogUIPrefab);
            }
            else if (spawnType == PanelType.SkillsUI)
            {
                spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnSkillbookPrefab);
            }
            else if (spawnType == PanelType.SkilltreeUI)
            {
                spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnSkilltreePrefab);
            }
            else if (spawnType == PanelType.CraftUI)
            {
                spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnInventoryUIPrefab);
                PostUpdateCommands.AddComponent(entityInQueryIndex, spawnEntity, new UIAnchor(AnchorUIType.RightMiddle));
                var spawnEntity2 = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnCraftUIPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity2, new CharacterLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity2, new DelayEvent(elapsedTime, 0.2));
                PostUpdateCommands.AddComponent(entityInQueryIndex, spawnEntity2, new UIAnchor(AnchorUIType.LeftMiddle));
            }
            /*else
            {
                // UICoreSystem.SpawnCharacterUI(PostUpdateCommands, entityInQueryIndex, spawnCharacterUIPrefab, e, spawnType, elapsedTime, spawnDelay);
            }*/
            if (spawnEntity.Index != 0)
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new CharacterLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new DelayEvent(elapsedTime, spawnDelay));
            }
        }
    }
}