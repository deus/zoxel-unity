using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.UI;

namespace Zoxel.Realms.UI
{
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class TaskbarSelectSystem : SystemBase
    {
        private NativeArray<Text> texts;
        // private NativeList<Text> texts2;

        protected override void OnCreate()
        {
            /*texts2 = new NativeList<Text>();
            texts2.Add(new Text("Paused"));
            texts2.Add(new Text("Status"));
            texts2.Add(new Text("Inventory"));
            texts2.Add(new Text("Skills"));
            texts2.Add(new Text("Stats"));
            texts2.Add(new Text("Equipment"));
            texts2.Add(new Text("Map"));
            texts2.Add(new Text("Questlog"));
            texts2.Add(new Text("Skilltree"));
            texts2.Add(new Text("Craft"));
            texts = texts2.AsArray(); // ToArray(Allocator.Persistent);*/

            texts = new NativeArray<Text>(10, Allocator.Persistent);
            texts[0] = new Text("Paused");
            texts[1] = new Text("Status");
            texts[2] = new Text("Inventory");
            texts[3] = new Text("Skillbook");
            texts[4] = new Text("Stats");
            texts[5] = new Text("Equipment");
            texts[6] = new Text("Map");
            texts[7] = new Text("Questlog");
            texts[8] = new Text("Skilltree");
            texts[9] = new Text("Craft");
            // texts2.Dispose();
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var texts = this.texts;
            var commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var PostUpdateCommands = commandBufferSystem.CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<UISelectEvent, TaskbarButton>()
                .ForEach((Entity e, int entityInQueryIndex, in Child child, in UISelectEvent uiSelectEvent) =>
            {
                var tooltip = texts[child.index].Clone();
                var controllerEntity = uiSelectEvent.character;
                // PostUpdateCommands.AddComponent(entityInQueryIndex, controllerEntity, new SetTooltipText(tooltip));
                PostUpdateCommands.AddComponent(entityInQueryIndex, controllerEntity, new SetPlayerTooltip(tooltip));
            })  .WithReadOnly(texts)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // var ecbSystem = World.GetOrCreateSystemManaged<NonSingletonECBSystem>();
            // var ecb = ecbSystem.CreateCommandBuffer();
        }
    }
}
            