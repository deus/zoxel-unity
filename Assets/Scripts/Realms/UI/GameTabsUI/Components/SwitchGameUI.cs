using Unity.Entities;

namespace Zoxel.Realms.UI
{
    public struct SwitchRealmUI : IComponentData
    {
        public byte newUI;

        public SwitchRealmUI(byte newUI)
        {
            this.newUI = newUI;
        }
    }
}