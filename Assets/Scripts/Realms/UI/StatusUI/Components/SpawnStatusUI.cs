using Unity.Entities;

namespace Zoxel.Realms.UI
{
    //! A general UI that shows a character's status.
    public struct SpawnStatusUI : IComponentData { }
}