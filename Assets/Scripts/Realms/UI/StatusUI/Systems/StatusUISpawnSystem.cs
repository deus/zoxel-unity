using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Cameras;
using Zoxel.Audio;
using Zoxel.UI;
using Zoxel.Races;
using Zoxel.Classes;
using Zoxel.Clans;

namespace Zoxel.Realms.UI
{
    //! Spawns a StatusUI connected to a user.
    /**
    *   - UI Spawn System -
    *   StatusUI will show Player class, name, clan, etc
    */
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class StatusUISpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private NativeArray<Text> texts;
        public static Entity spawnStatusUIPrefab;
        public static Entity statusUIPrefab;
        public static Entity statusLabelPrefab;
        private EntityQuery processQuery;
        private EntityQuery charactersQuery;
        private EntityQuery namesQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnInventoryUIArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnStatusUI),
                typeof(CharacterLink),
                typeof(GenericEvent),
                typeof(DelayEvent));
            spawnStatusUIPrefab = EntityManager.CreateEntity(spawnInventoryUIArchetype);
            processQuery = GetEntityQuery(
                ComponentType.ReadOnly<SpawnStatusUI>(),
                ComponentType.ReadOnly<CharacterLink>(),
                ComponentType.Exclude<DelayEvent>());
            charactersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Character>(),
                ComponentType.Exclude<DestroyEntity>());
            namesQuery = GetEntityQuery(
                ComponentType.ReadOnly<ZoxName>(),
                ComponentType.Exclude<DestroyEntity>());
            // Name, Race, Class, Clan
            texts = new NativeArray<Text>(5, Allocator.Persistent);
            texts[0] = new Text("Name");
            texts[1] = new Text("Race");
            texts[2] = new Text("Class");
            texts[3] = new Text("Clan");
            texts[4] = new Text("None");
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs(EntityManager);
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var statusLabelPrefab = StatusUISpawnSystem.statusLabelPrefab;
            var statusUIPrefab = StatusUISpawnSystem.statusUIPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var texts = this.texts;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var spawnEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var characterLinks = GetComponentLookup<CharacterLink>(true);
            var prefabLinks = GetComponentLookup<PrefabLink>(true);
            var uiAnchors = GetComponentLookup<UIAnchor>(true);
            var nameEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var zoxNames = GetComponentLookup<ZoxName>(true);
            nameEntities.Dispose();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in CameraLink cameraLink, in ZoxName zoxName,
                in RaceLink raceLink, in ClassLink classLink, in ClanLink clanLink) =>
            {
                // Does spawn a invenntory ui?
                var spawnEntity = new Entity();
                for (int i = 0; i < spawnEntities.Length; i++)
                {
                    var e2 = spawnEntities[i];
                    var characterLink = characterLinks[e2];
                    if (characterLink.character == e)
                    {
                        spawnEntity = e2;
                        break;  // only one UI per character atm
                    }
                }
                if (spawnEntity.Index == 0)
                {
                    return;
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));
                // destroy event entity
                // PostUpdateCommands.DestroyEntity(entityInQueryIndex, spawnEntity);
                var prefab = statusUIPrefab;
                if (prefabLinks.HasComponent(spawnEntity))
                {
                    prefab = prefabLinks[spawnEntity].prefab;
                }
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CharacterLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLink);
                if (uiAnchors.HasComponent(spawnEntity))
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, uiAnchors[spawnEntity]);
                }

                // Spawn Name
                var labels = new BlitableArray<Text>(8, Allocator.Temp);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawned((byte) labels.Length));
                // Name, Race, Class, Clan
                labels[0] = texts[0].Clone();
                labels[1] = zoxName.name.Clone();
                labels[2] = texts[1].Clone();
                labels[4] = texts[2].Clone();
                labels[6] = texts[3].Clone();
                if (zoxNames.HasComponent(raceLink.race))
                {
                    labels[3] = zoxNames[raceLink.race].name.Clone();
                }
                else
                {
                    labels[3] = texts[4].Clone();
                }
                if (zoxNames.HasComponent(classLink.classs))
                {
                    labels[5] = zoxNames[classLink.classs].name.Clone();
                }
                else
                {
                    labels[5] = texts[4].Clone();
                }
                if (zoxNames.HasComponent(clanLink.clan))
                {
                    labels[7] = zoxNames[clanLink.clan].name.Clone();
                }
                else
                {
                    labels[7] = texts[4].Clone();
                }
                // Spawn Title(s)
                for (byte i = 0; i < labels.Length; i++)
                {
                    var seed = (int) (128 + elapsedTime + i * 2048 + 4196 * entityInQueryIndex);
                    var e4 = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, statusLabelPrefab, e3);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, e4, i);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e4, new RenderText(labels[i]));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e4, new Seed(seed));
                }
                labels.Dispose();
            })  .WithReadOnly(spawnEntities)
                .WithDisposeOnCompletion(spawnEntities)
                .WithReadOnly(characterLinks)
                .WithReadOnly(prefabLinks)
                .WithReadOnly(uiAnchors)
                .WithReadOnly(texts)
                .WithReadOnly(zoxNames)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static void InitializePrefabs(EntityManager EntityManager)
        {
            if (StatusUISpawnSystem.statusUIPrefab.Index != 0)
            {
                return;
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var buttonStyle = uiDatam.buttonStyle.GetStyle(uiScale);
            var panelStyle = uiDatam.panelStyle.GetStyle();
            var gridSize = new int2(1, 8);
            var iconSize = uiDatam.GetIconSize(uiScale);
            var padding = iconSize / 12f;
            var margins = iconSize / 4f;
            iconSize.x *= 6f;
            iconSize.y = buttonStyle.fontSize / 2.6f;
            iconSize.y += buttonStyle.textPadding.y * 2f;
            var statusUIPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            StatusUISpawnSystem.statusUIPrefab = statusUIPrefab;
            EntityManager.AddComponent<Prefab>(statusUIPrefab);
            EntityManager.AddComponent<StatusUI>(statusUIPrefab);
            EntityManager.SetComponentData(statusUIPrefab, new PanelUI(PanelType.StatusUI));
            EntityManager.SetComponentData(statusUIPrefab, new GridUI(gridSize, padding, margins));
            EntityManager.SetComponentData(statusUIPrefab, new GridUISize(iconSize));
            statusLabelPrefab = EntityManager.Instantiate(UICoreSystem.labelPrefab);
            EntityManager.AddComponent<Prefab>(statusLabelPrefab);
            iconSize.y -= buttonStyle.textPadding.y * 2f;
            UICoreSystem.SetRenderTextData(EntityManager, statusLabelPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, iconSize.y, buttonStyle.textPadding);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }
    }
}