﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Worlds;
using Zoxel.Textures;
using Zoxel.Cameras;
using Zoxel.Players.UI;

namespace Zoxel.Realms.UI
{
    //! Spawns a RealmMaker UI.
    /**
    *   - UI Spawn System -
    *   \todo Readd map ui back. Or a solar system ui.
    *   \todo Add better padding.
    *   \todo Add Solar Map
    *   \todo Add Planet map - Fix Maps
    *   \todo Fix VoxelModelViewer
    *   \todo Add Minivox Voxels to Viewer!
    */
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class RealmMakerSpawnSystem : SystemBase
    {
        private NativeArray<Text> texts;
        private float2 panelSize;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity spawnPanelPrefab;
        private Entity panelPrefab;
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            texts = new NativeArray<Text>(1, Allocator.Persistent);
            texts[0] = new Text("New Realm");
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(NewRealmMaker),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnPanelPrefab = EntityManager.CreateEntity(spawnArchetype);
            uiHoldersQuery = GetEntityQuery(
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<UILink>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            HeaderSpawnSystem.InitializePrefabs(EntityManager);
            var headerPrefab = HeaderSpawnSystem.headerPrefab;
            var uiDatam = UIManager.instance.uiDatam;
            var headerStyle = uiDatam.headerStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var panelPrefab = this.panelPrefab;
            var texts = this.texts;
            var panelSize = this.panelSize;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uiHolderEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
            uiHolderEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<NewRealmMaker>()
                .ForEach((int entityInQueryIndex, in CharacterLink characterLink) =>
            {
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, panelPrefab);
                var headerText = texts[0].Clone();
                HeaderSpawnSystem.SpawnHeaderUI(PostUpdateCommands, entityInQueryIndex, headerPrefab, e3, panelSize, headerStyle.fontSize, headerStyle.textPadding,
                    in headerText);
                if (characterLink.character.Index > 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, characterLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab),
                        new OnUISpawned(characterLink.character, 1));
                    if (cameraLinks.HasComponent(characterLink.character))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLinks[characterLink.character]);
                    }
                }
            })  .WithReadOnly(texts)
                .WithReadOnly(cameraLinks)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private void InitializePrefabs()
        {
            if (panelPrefab.Index != 0)
            {
                return;
            }
            var uiDatam = UIManager.instance.uiDatam;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var iconSize = uiDatam.GetIconSize(uiScale);
            var totalSize = new float2(18, 12) * iconSize.y;
            this.panelSize = totalSize;
            panelPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(panelPrefab);
            EntityManager.RemoveComponent<SpawnHeaderUI>(panelPrefab);
            EntityManager.RemoveComponent<GridUIDirty>(panelPrefab);
            EntityManager.RemoveComponent<GridUI>(panelPrefab);
            EntityManager.AddComponent<RealmMakerUI>(panelPrefab);
            EntityManager.SetComponentData(panelPrefab, new PanelUI(PanelType.NewGameMenu));
            EntityManager.SetComponentData(panelPrefab, new Size2D(totalSize));
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }
    }
}

/// Handles Realm in NewGameState (move to game systems)
///     - First disable all cameras but first controllerEntity
///     - Then spawn UI for Realm Generation
///     - Shows Planet Planet and a generate button
///     - also a confirm and back button
///     [   m   a   p             ]
///     [back] [generate] [confirm]
///     - Once confirm - save that seed in a game data
///     - use LoadTransformSystem to save Realm folder
///     - UI Flow:
///         - NewRealmUI
///         - New Character UI (show character in middle - class choice to be anchored on right)
///         - Race Choice anchored on left next
///         - Custom character UI - chose between different heads - chests - etc (body parts)
///         - then chose a starting Location
///         - Chose name for character and it will fade out screen and fade in to character (load it into new planet)
