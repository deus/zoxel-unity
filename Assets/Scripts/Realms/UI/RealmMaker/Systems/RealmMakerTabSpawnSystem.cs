using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.UI.Voxels;

namespace Zoxel.Realms.UI
{
    //! Spawns RealmMaker sub panels as tabs.
    /**
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class RealmMakerTabSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity nameButtonPrefab;
        private Entity nameInputPrefab;
        private Entity labelPrefab;
        private NativeArray<Text> nameLabels;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            nameLabels = new NativeArray<Text>(3, Allocator.Persistent);
            nameLabels[0] = new Text("Name");
            nameLabels[1] = new Text("84912937");
            nameLabels[2] = new Text("New Name");
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            InitializePrefabs();
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            var iconSize = uiDatam.GetIconSize(uiScale);
            var totalSize = new float2(18, 12) * iconSize.y;
            var viewerSize = new float2(iconSize.x * 6.1f, iconSize.x * 6.1f);
            var viewerPosition = new float3(0, iconSize.y * 2, 0);
            var smallButtonSize = iconSize.y * 0.6f;
            var smallestButtonSize = iconSize.y * 0.4f;
            var lowestButtonsPosition = new float2(-(totalSize.x / 2f - 4 * (smallButtonSize / 2f)), -(totalSize.y / 2f));
            var leftSidePanel = new float2(-(totalSize.x / 2f), 0);
            var rightPanelIndex = 3;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<InitializeEntity, MeshUIDirty>()
                .WithAll<RealmMakerUI>()
                .ForEach((Entity e, in Childrens childrens, in CharacterLink characterLink, in SpawnTabUI spawnTabUI, in RealmMakerUI realmMakerUI) =>
            {
                var playerEntity = characterLink.character;
                // var realm = EntityManager.GetComponentData<RealmLink>(player).realm;
                var realmEntity = realmMakerUI.realm;
                if (!HasComponent<Realm>(realmEntity))
                {
                    return;
                }
                var rightPanel = childrens.children[rightPanelIndex];
                PostUpdateCommands.AddComponent<DestroyChildren>(rightPanel);
                if (spawnTabUI.typeType == (byte) RealmMakerButtonType.Name)
                {
                    var characterName = EntityManager.GetComponentData<ZoxName>(realmEntity).name;
                    nameLabels[1].Dispose();
                    nameLabels[1] = characterName.Clone();
                    PostUpdateCommands.AddComponent(rightPanel, new SpawnUIList(nameButtonPrefab, labelPrefab, nameInputPrefab,
                        nameLabels, iconSize.y / 2f, textPadding));
                }
                else if (spawnTabUI.typeType == (byte) RealmMakerButtonType.Voxels)
                {
                    // VoxelViewerUI (Move to tab for voxels)
                    var gridSize = new int2(5, 5);
                    PostUpdateCommands.AddComponent(rightPanel, new SpawnVoxelGroupViewer(realmEntity,
                        playerEntity, new float3(iconSize.y * 0.5f - iconSize.y * 0.5f * ((float)gridSize.x), iconSize.y * 0.5f * ((float)gridSize.y), 0), gridSize));
                }
                PostUpdateCommands.RemoveComponent<SpawnTabUI>(e);
            }).WithoutBurst().Run();
        }

        private void InitializePrefabs()
        {
            if (labelPrefab.Index != 0)
            {
                return;
            }
            labelPrefab = EntityManager.Instantiate(UICoreSystem.labelPrefab);
            EntityManager.AddComponent<Prefab>(labelPrefab);
            EntityManager.AddComponent<SetUIElementPosition>(labelPrefab);
            nameButtonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(nameButtonPrefab);
            EntityManager.AddComponentData(nameButtonPrefab, new RealmMakerButton(RealmMakerButtonType.Name));
            nameInputPrefab = EntityManager.Instantiate(UICoreSystem.inputPrefab);
            EntityManager.AddComponent<Prefab>(nameInputPrefab);
            EntityManager.AddComponentData(nameInputPrefab, new RealmMakerButton(RealmMakerButtonType.Name));

            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var headerStyle = uiDatam.headerStyle.GetStyle(uiScale);
            var buttonStyle = uiDatam.buttonStyle.GetStyle(uiScale);
            var iconSize = uiDatam.GetIconSize(uiScale) * 0.4f;
            var fontSize = iconSize.y;
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            var textMargins = uiDatam.GetMenuMargins(uiScale);
            UICoreSystem.SetRenderTextData(EntityManager, labelPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, nameButtonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, nameInputPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < nameLabels.Length; i++)
            {
                nameLabels[i].Dispose();
            }
            nameLabels.Dispose();
        }
    }
}