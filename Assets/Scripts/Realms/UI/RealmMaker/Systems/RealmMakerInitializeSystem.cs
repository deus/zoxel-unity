using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Cameras;
using Zoxel.Maps;
using Zoxel.Textures;
using Zoxel.Players;
using Zoxel.Voxels;
using Zoxel.Rendering;
using Zoxel.UI.Voxels;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;
using Zoxel.Worlds;

namespace Zoxel.Realms.UI
{
    //! Initializes the Realm Maker UI!
    /**
    *   - UI Initialize System -
    */
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class RealmMakerInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity backNextButtonPrefab;    // back, done
        private Entity tabButtonPrefab;
        private Entity seedButtonPrefab;
        private Entity seedInputPrefab;
        private Entity labelPrefab;
        private NativeArray<Text> tabLabels;
        private NativeArray<Text> seedLabels;
        private NativeArray<Text> backNextLabels;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            tabLabels = new NativeArray<Text>(8, Allocator.Persistent);
            tabLabels[0] = new Text("Name");
            tabLabels[1] = new Text("Planets");
            tabLabels[2] = new Text("Voxels");
            tabLabels[3] = new Text("Items");
            tabLabels[4] = new Text("Stats");
            tabLabels[5] = new Text("Races");
            tabLabels[6] = new Text("Classes");
            tabLabels[7] = new Text("Jobs");
            //tabLabels[2] = new Text("Class");
            seedLabels = new NativeArray<Text>(3, Allocator.Persistent);
            seedLabels[0] = new Text("Seed");
            seedLabels[1] = new Text("84912937");
            seedLabels[2] = new Text("New Realm");
            backNextLabels = new NativeArray<Text>(2, Allocator.Persistent);
            backNextLabels[0] = new Text("Back");
            backNextLabels[1] = new Text("Done");
            RequireForUpdate<SoundSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var soundSettings = GetSingleton<SoundSettings>();
            var soundVolume = soundSettings.uiSoundVolume;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var panelStyle = uiDatam.panelStyle.GetStyle();
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            var iconSize = uiDatam.GetIconSize(uiScale);
            var totalSize = new float2(18, 12) * iconSize.y;
            var viewerSize = new float2(iconSize.x * 6.1f, iconSize.x * 6.1f);
            var viewerPosition = new float3(0, iconSize.y * 2, 0);
            var smallButtonSize = iconSize.y * 0.6f;
            var smallestButtonSize = iconSize.y * 0.4f;
            var lowestButtonsPosition = new float2(-(totalSize.x / 2f - 4 * (smallButtonSize / 2f)), -(totalSize.y / 2f));
            var leftSidePanel = new float2(-(totalSize.x / 2f), 0);
            var tabLabels = this.tabLabels;
            var seedLabels = this.seedLabels;
            var backNextLabels = this.backNextLabels;
            var characterPosition = new float3(-256, -256, -256);
            var subPanelStyle = uiDatam.subPanelStyle.GetStyle();
            var menuPaddings = uiDatam.GetMenuPaddings(uiScale);
            var buttonStyle = uiDatam.buttonStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            // Prefabs
            var backNextButtonPrefab = this.backNextButtonPrefab;
            var labelPrefab = this.labelPrefab;
            var tabButtonPrefab = this.tabButtonPrefab;
            var seedButtonPrefab = this.seedButtonPrefab;
            var seedInputPrefab = this.seedInputPrefab;
            var spawnPlanetPrefab = WorldSystemGroup.spawnPlanetPrefab;
            var toggleGroupPrefab = UICoreSystem.toggleGroupPrefab;
            var subPanelGridPrefab = UICoreSystem.subPanelGridPrefab;
            // spawns buttons, viewer and sub panels
            var realmPrefab = RealmSystemGroup.realmPrefab;
            var playerHome = PlayerSystemGroup.playerHome;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<MeshUIDirty>()
                .WithAll<InitializeEntity, RealmMakerUI>()
                .ForEach((Entity e, int entityInQueryIndex, ref RealmMakerUI realmMakerUI, in Size2D size2D, in CharacterLink characterLink) =>
            {
                // spawn children
                var children = new NativeList<SpawnUIData>();
                children.Add(new SpawnUIData
                {
                    label = backNextLabels[1].Clone(), // new Text("Done"),
                    position = new float3(-lowestButtonsPosition.x - textPadding.x, lowestButtonsPosition.y + smallButtonSize / 2f + textPadding.y, 0),
                    size = smallButtonSize
                });
                children.Add(new SpawnUIData
                {
                    label = backNextLabels[0].Clone(),
                    position = new float3(lowestButtonsPosition.x + textPadding.x, lowestButtonsPosition.y + smallButtonSize / 2f + textPadding.y, 0),
                    size = smallButtonSize
                });

                for (int i = 0; i < children.Length; i++)
                {
                    var spawnData = children[i];
                    if (spawnData.type == 0)
                    {
                        // spawn button
                        UICoreSystem.SpawnButton(PostUpdateCommands, entityInQueryIndex,
                            backNextButtonPrefab, e, i,
                            spawnData.position, spawnData.label, spawnData.size, menuPaddings,
                            buttonStyle.color, buttonStyle.color, buttonStyle.textColor, buttonStyle.textColor,
                            (i + 1) * 666, soundVolume);
                    }
                }
                // spawn invinsible UI - child - w ith size of a third of panel
                var panelPositionX = leftSidePanel.x + (size2D.size.x / 3f) / 2f;
                var leftPanel = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, toggleGroupPrefab, e, 
                    new float3(panelPositionX, 0, 0), new float2(size2D.size.x / 3f, size2D.size.y), subPanelStyle.color);
                UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, leftPanel, children.Length);
                UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, leftPanel, e);
                UICoreSystem.SetTextureFrame(PostUpdateCommands, entityInQueryIndex, leftPanel, in subPanelStyle.frameGenerationData);
                PostUpdateCommands.SetComponent(entityInQueryIndex, leftPanel, new GridUISize(iconSize));
                PostUpdateCommands.AddComponent<AutoGridY>(entityInQueryIndex, leftPanel);
                PostUpdateCommands.AddComponent<DisableGridResize>(entityInQueryIndex, leftPanel);
                // , iconSize.y / 2f, textPadding
                PostUpdateCommands.AddComponent(entityInQueryIndex, leftPanel, new SpawnButtonsList(in tabLabels));
                PostUpdateCommands.AddComponent(entityInQueryIndex, leftPanel, new ListPrefabLink(tabButtonPrefab));

                var rightPanel = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, toggleGroupPrefab, e, 
                    new float3(-panelPositionX, 0, 0), new float2(size2D.size.x / 3f, size2D.size.y), subPanelStyle.color);
                UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, rightPanel, children.Length + 1);
                UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, rightPanel, e);
                UICoreSystem.SetTextureFrame(PostUpdateCommands, entityInQueryIndex, rightPanel, in subPanelStyle.frameGenerationData);
                PostUpdateCommands.SetComponent(entityInQueryIndex, rightPanel, new GridUISize(iconSize));
                PostUpdateCommands.AddComponent<AutoGridY>(entityInQueryIndex, rightPanel);
                PostUpdateCommands.AddComponent<DisableGridResize>(entityInQueryIndex, rightPanel);

                // just put seed here
                var middlePanel = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, subPanelGridPrefab, e, 
                    new float3(0, - totalSize.y / 2f + (size2D.size.y / 3f) / 2f, 0), new float2(size2D.size.x / 3f, size2D.size.y / 3f), subPanelStyle.color);
                UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, middlePanel, children.Length + 2);
                UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, middlePanel, e);
                UICoreSystem.SetTextureFrame(PostUpdateCommands, entityInQueryIndex, middlePanel, in subPanelStyle.frameGenerationData);
                PostUpdateCommands.SetComponent(entityInQueryIndex, middlePanel, new GridUISize(iconSize));
                PostUpdateCommands.AddComponent<AutoGridY>(entityInQueryIndex, middlePanel);
                PostUpdateCommands.AddComponent<DisableGridResize>(entityInQueryIndex, rightPanel);
                // SpawnUIList with Label, Input and Button
                PostUpdateCommands.AddComponent(entityInQueryIndex, middlePanel, new SpawnUIList(seedButtonPrefab, labelPrefab, seedInputPrefab,
                    seedLabels, iconSize.y / 2f, textPadding));
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChildrenSpawned((byte)(children.Length + 3)));
                children.Dispose();
                // Finished Spawning UI
                PostUpdateCommands.AddComponent<MeshUIDirty>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<NavigationDirty>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SpawnTabUI((byte) RealmMakerButtonType.Name));
                var player = characterLink.character;
                // Spawn Realm Entity
                var realmEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, realmPrefab);
                PostUpdateCommands.AddComponent<GenerateSeed>(entityInQueryIndex, realmEntity);
                PostUpdateCommands.AddComponent<GenerateName>(entityInQueryIndex, realmEntity);
                PostUpdateCommands.SetComponent(entityInQueryIndex, realmEntity, new PlayerHomeLink(playerHome));
                PostUpdateCommands.SetComponent(entityInQueryIndex, player, new RealmLink(realmEntity));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e, new RealmMakerUI(realmEntity));
            })  .WithReadOnly(tabLabels)
                .WithReadOnly(seedLabels)
                .WithReadOnly(backNextLabels)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        /*public static void SpawnButton(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex,
            Entity backNextButtonPrefab, Entity parent, Text text, int childIndex, float fontSize,
            float3 position, float2 menuPaddings, Color menuButtonColor, Color menuTextColor,
            int seed, float2 soundVolume)
        {
            UICoreSystem.SpawnButton(PostUpdateCommands, entityInQueryIndex, backNextButtonPrefab, parent,
                childIndex, position, text, fontSize, menuPaddings, 
                menuButtonColor, menuButtonColor, menuTextColor, menuTextColor,
                seed, soundVolume);
        }*/

        private void InitializePrefabs()
        {
            if (backNextButtonPrefab.Index != 0)
            {
                return;
            }
            backNextButtonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(backNextButtonPrefab);
            EntityManager.AddComponent<SetUIElementPosition>(backNextButtonPrefab);
            EntityManager.AddComponentData(backNextButtonPrefab, new RealmMakerButton(RealmMakerButtonType.BackDone));
            labelPrefab = EntityManager.Instantiate(UICoreSystem.labelPrefab);
            EntityManager.AddComponent<Prefab>(labelPrefab);
            EntityManager.AddComponent<SetUIElementPosition>(labelPrefab);
            tabButtonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(tabButtonPrefab);
            EntityManager.AddComponentData(tabButtonPrefab, new RealmMakerButton(RealmMakerButtonType.TabButtons));
            EntityManager.AddComponent<Toggle>(tabButtonPrefab);
            seedButtonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(seedButtonPrefab);
            EntityManager.AddComponentData(seedButtonPrefab, new RealmMakerButton(RealmMakerButtonType.Seed));
            seedInputPrefab = EntityManager.Instantiate(UICoreSystem.inputPrefab);
            EntityManager.AddComponent<Prefab>(seedInputPrefab);
            EntityManager.AddComponent<InputNumbersOnly>(seedInputPrefab);
            EntityManager.AddComponentData(seedInputPrefab, new RealmMakerButton(RealmMakerButtonType.Seed));
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var headerStyle = uiDatam.headerStyle.GetStyle(uiScale);
            var buttonStyle = uiDatam.buttonStyle.GetStyle(uiScale);
            var iconSize = uiDatam.GetIconSize(uiScale) * 0.4f;
            var fontSize = iconSize.y;
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            var textMargins = uiDatam.GetMenuMargins(uiScale);
            UICoreSystem.SetRenderTextData(EntityManager, backNextButtonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, labelPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, tabButtonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, seedButtonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, seedInputPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < tabLabels.Length; i++)
            {
                tabLabels[i].Dispose();
            }
            tabLabels.Dispose();
            for (int i = 0; i < seedLabels.Length; i++)
            {
                seedLabels[i].Dispose();
            }
            seedLabels.Dispose();
            for (int i = 0; i < backNextLabels.Length; i++)
            {
                backNextLabels[i].Dispose();
            }
            backNextLabels.Dispose();
        }
    }
}