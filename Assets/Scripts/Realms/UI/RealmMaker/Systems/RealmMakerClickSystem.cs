using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Voxels;
using Zoxel.Worlds;
using Zoxel.Cameras;
using Zoxel.Transforms;
using Zoxel.Characters;
using Zoxel.Maps;
using Zoxel.Characters.UI;

namespace Zoxel.Realms.UI
{
    //! Handles ClickEvent's on RealmMakerUIs.
    /**
    *   - UIClickEvent System -
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class RealmMakerClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);

            Entities
                .WithAll<ToggleClickEvent>()
                .ForEach((Entity e, in Zoxel.Transforms.Child child, in RealmMakerButton realmMakerButton, in PanelLink panelLink) =>
            {
                var panel = panelLink.panel;
                var arrayIndex = child.index;
                if (realmMakerButton.buttonType == (byte) RealmMakerButtonType.TabButtons)
                {
                    PostUpdateCommands.AddComponent(panel, new SpawnTabUI((byte) arrayIndex));
                }
            }).WithoutBurst().Run();

            // When clicking new game UI
            Entities
                .WithAll<UIClickEvent, RealmMakerButton>()
                .ForEach((Entity e, in Zoxel.Transforms.Child child, in RealmMakerButton realmMakerButton, in UIClickEvent uiClickEvent,
                    in ParentLink parentLink, in PanelLink panelLink) =>
            {
                var player = uiClickEvent.controller;
                var panel = panelLink.panel;
                var parent = parentLink.parent;
                var arrayIndex = child.index;
                var buttonType = uiClickEvent.buttonType;
                var realmEntity = EntityManager.GetComponentData<RealmMakerUI>(panelLink.panel).realm;
                if (realmMakerButton.buttonType == (byte) RealmMakerButtonType.BackDone)
                {
                    if (arrayIndex == 0)
                    {
                        ConfirmNewGame(PostUpdateCommands, player, panel);
                    }
                    else if (arrayIndex == 1)
                    {
                        CancelNewGame(PostUpdateCommands, player, panel);
                    }
                }
                /*else if (realmMakerButton.buttonType == (byte) RealmMakerButtonType.TabButtons)
                {
                    PostUpdateCommands.AddComponent(panel, new SpawnTabUI((byte)arrayIndex));
                }*/
                else if (realmMakerButton.buttonType == (byte) RealmMakerButtonType.Name)
                {
                    if (arrayIndex == 2)
                    {
                        GenerateName(PostUpdateCommands, realmEntity, player, parentLink.parent, arrayIndex);
                    }
                }
                else if (realmMakerButton.buttonType == (byte) RealmMakerButtonType.Seed)
                {
                    if (arrayIndex == 2)
                    {
                        GenerateSeed(PostUpdateCommands, realmEntity, player, parent, arrayIndex);
                    }
                }
                /*else if (arrayIndex == 8)
                {
                    //UnityEngine.Debug.LogError("Zooming In on voxEntity.");
                    var mapUI = EntityManager.GetComponentData<Childrens>(panel).children[8];
                    PostUpdateCommands.AddComponent<MapZoomIn>(mapUI);
                }
                else if (arrayIndex == 9)
                {
                    //UnityEngine.Debug.LogError("Zooming Out on voxEntity.");
                    var mapUI = EntityManager.GetComponentData<Childrens>(panel).children[8];
                    PostUpdateCommands.AddComponent<MapZoomOut>(mapUI);
                }*/
            }).WithoutBurst().Run();
            Entities
                .WithAll<InputFinished>()
                .ForEach((Entity e, in Zoxel.Transforms.Child child, in PanelLink panelLink, in RenderText renderText, in ParentLink parentLink,
                    in RealmMakerButton realmMakerButton) =>
            {
                var arrayIndex = child.index;
                var realmEntity = EntityManager.GetComponentData<RealmMakerUI>(panelLink.panel).realm;
                if (realmMakerButton.buttonType == (byte) RealmMakerButtonType.Name)
                {
                    if (arrayIndex == 1)
                    {
                        OnNameUpdated(PostUpdateCommands, realmEntity, in renderText, parentLink.parent, arrayIndex);
                    }
                }
                else if (realmMakerButton.buttonType == (byte) RealmMakerButtonType.Seed)
                {
                    if (arrayIndex == 1)
                    {
                        OnSeedUpdated(PostUpdateCommands, realmEntity, in renderText, parentLink.parent, arrayIndex);
                    }
                }
            }).WithoutBurst().Run();
        }

        void GenerateName(EntityCommandBuffer PostUpdateCommands, Entity realmEntity, Entity player, Entity parentUI, int arrayIndex)
        {
            // var realm = EntityManager.GetComponentData<RealmLink>(player).realm; // get game from player object
            var newName = NameGenerator.GenerateName();
            var input = EntityManager.GetComponentData<Childrens>(parentUI).children[arrayIndex - 1];
            var renderText = EntityManager.GetComponentData<RenderText>(input);
            if (renderText.SetText(newName.ToString()))
            {
                PostUpdateCommands.AddComponent<RenderTextDirty>(input);
                PostUpdateCommands.SetComponent(input, renderText);
            }
            PostUpdateCommands.SetComponent(realmEntity, new ZoxName(new Text(newName)));
        }
        
        public void OnNameUpdated(EntityCommandBuffer PostUpdateCommands, Entity realmEntity, in RenderText renderText, Entity parentUI, int arrayIndex)
        {
            var zoxName = EntityManager.GetComponentData<ZoxName>(realmEntity);
            var name = renderText.text.Clone();
            if (name.Length == 0)
            {
                var random = new Random();
                random.InitState((uint) IDUtil.GenerateUniqueID());
                name = NameGenerator.GenerateName2(ref random);
                var input = EntityManager.GetComponentData<Childrens>(parentUI).children[arrayIndex];
                var renderText2 = EntityManager.GetComponentData<RenderText>(input);
                if (renderText2.SetText(name.ToString()))
                {
                    PostUpdateCommands.AddComponent<RenderTextDirty>(input);
                    PostUpdateCommands.SetComponent(input, renderText2);
                }
            }
            zoxName.SetName(name);
            PostUpdateCommands.SetComponent(realmEntity, zoxName);
        }

        public void OnSeedUpdated(EntityCommandBuffer PostUpdateCommands, Entity realmEntity, in RenderText renderText, Entity parentUI, int arrayIndex)
        {
            var seed = 0;
            try
            {
                seed = int.Parse(renderText.text.ToString());
            }
            catch (System.Exception e)
            {
                UnityEngine.Debug.LogError("OnSeedUpdated: Wrong input: " + e);
            }
            if (seed == 0)
            {
                seed = IDUtil.GenerateUniqueID();
                var input = EntityManager.GetComponentData<Childrens>(parentUI).children[arrayIndex];
                var renderText2 = EntityManager.GetComponentData<RenderText>(input);
                if (renderText2.SetText(seed.ToString()))
                {
                    PostUpdateCommands.AddComponent<RenderTextDirty>(input);
                    PostUpdateCommands.SetComponent(input, renderText2);
                }
            }
            PostUpdateCommands.SetComponent(realmEntity, new ZoxID(seed));
            PostUpdateCommands.AddComponent<EntityBusy>(realmEntity);
            PostUpdateCommands.AddComponent<GenerateRealm>(realmEntity);
        }

        // Generate New Seed & refresh voxEntity!
        private void GenerateSeed(EntityCommandBuffer PostUpdateCommands, Entity realmEntity, Entity player, Entity panel, int arrayIndex)
        {
            if (HasComponent<OnChildrenSpawned>(panel))
            {
                return;
            }
            // var realm = EntityManager.GetComponentData<RealmLink>(player).realm; // get game from player object
            if (!HasComponent<GenerateRealm>(realmEntity))
            {
                var voxLink = EntityManager.GetComponentData<VoxLink>(player);
                if (!HasComponent<EntityBusy>(voxLink.vox))
                {
                    var childrens = EntityManager.GetComponentData<Childrens>(panel);
                    // UnityEngine.Debug.LogError("Children Length: " + childrens.children.Length);
                    var seed = IDUtil.GenerateUniqueID();
                    var seedString = seed.ToString();    // NameGenerator.GenerateName();
                    var input = childrens.children[arrayIndex - 1];
                    var renderText = EntityManager.GetComponentData<RenderText>(input);
                    if (renderText.SetText(seedString))
                    {
                        PostUpdateCommands.AddComponent<RenderTextDirty>(input);
                        PostUpdateCommands.SetComponent(input, renderText);
                    }
                    PostUpdateCommands.SetComponent(realmEntity, new ZoxID(seed));
                    PostUpdateCommands.AddComponent<EntityBusy>(realmEntity);
                    PostUpdateCommands.AddComponent<GenerateRealm>(realmEntity);
                }
            }
        }

        // When confirming new game I have to unload chunks
        public void ConfirmNewGame(EntityCommandBuffer PostUpdateCommands, Entity player, Entity panel)
        {
            var spawnCharacterMakerPrefab = CharacterMakerSpawnSystem.spawnPanelPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            /*var voxLink = EntityManager.GetComponentData<VoxLink>(player);
            var voxEntity = voxLink.vox;
            if (!EntityManager.Exists(voxEntity))
            {
                // UnityEngine.Debug.LogError("voxEntity does not exist.");
                return;
            }
            var mapID = EntityManager.GetComponentData<ZoxID>(voxEntity).id;
            */
            var realmEntity = EntityManager.GetComponentData<RealmLink>(player).realm; // get game from player object
            // Start new Character Process
            var newCharacterEntity = PostUpdateCommands.Instantiate(spawnCharacterMakerPrefab);
            PostUpdateCommands.SetComponent(newCharacterEntity, new CharacterLink(player));
            PostUpdateCommands.SetComponent(newCharacterEntity, new DelayEvent(elapsedTime, 0.2));
            var realmName = EntityManager.GetComponentData<ZoxName>(realmEntity).name.ToString();
            var realmID = EntityManager.GetComponentData<ZoxID>(realmEntity).id; // get game from player object
            SaveUtilities.CreateNewRealmFolders(realmName, realmID);
            UICoreSystem.RemoveUI(PostUpdateCommands, player, panel);
            // Remove All Chunks
            // PostUpdateCommands.RemoveComponent<MapUIOnly>(voxEntity);
            // PostUpdateCommands.AddComponent<RemoveVoxChunks>(voxEntity);
        }

        // When canceling new game I have to destroy voxEntity
        public void CancelNewGame(EntityCommandBuffer PostUpdateCommands, Entity player, Entity panel)
        {
            var spawnRealmLoadUIPrefab = RealmLoadUISpawnSystem.spawnPanelPrefab;
            UICoreSystem.RemoveUI(PostUpdateCommands, player, panel);
            var spawnEntity = PostUpdateCommands.Instantiate(spawnRealmLoadUIPrefab);
            PostUpdateCommands.SetComponent(spawnEntity, new CharacterLink(player));
            // Realm
            var realm = EntityManager.GetComponentData<RealmLink>(player).realm; // get game from player object
            PostUpdateCommands.AddComponent<DestroyEntity>(realm);
        }
    }
}