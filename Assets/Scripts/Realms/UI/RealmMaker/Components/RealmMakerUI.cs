using Unity.Entities;

namespace Zoxel.Realms.UI
{
    //! A UIElement for editing and making a Realm.
    public struct RealmMakerUI : IComponentData
    {
        public Entity realm;

        public RealmMakerUI(Entity realm)
        {
            this.realm = realm;
        }
    }
}