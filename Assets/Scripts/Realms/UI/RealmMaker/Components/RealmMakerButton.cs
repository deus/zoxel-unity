using Unity.Entities;

namespace Zoxel.Realms.UI
{
    public enum RealmMakerButtonType : byte
    {
        // Tabs
        Name,
        Planets,
        Voxels,
        Items,
        Stats,
        Races,
        Classes,
        Jobs,
        // Towns,
        // other buttons
        BackDone,
        Seed,
        TabButtons
    }
    //! A UIElement for buttons of the RealmMaker.
    public struct RealmMakerButton : IComponentData
    { 
        public byte buttonType;
        public RealmMakerButton(RealmMakerButtonType buttonType)
        {
            this.buttonType = (byte) buttonType;
        }
    }
}