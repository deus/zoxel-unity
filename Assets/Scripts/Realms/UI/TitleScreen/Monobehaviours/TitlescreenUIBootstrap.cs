using UnityEngine;
using Unity.Entities;
using Zoxel.UI;

namespace Zoxel.Realms.UI
{
    //! Spawns Titlesreen UIs.
    public partial class TitlescreenUIBootstrap : MonoBehaviour
    {
        public static TitlescreenUIBootstrap instance;
        private bool isInitialized;

        [Header("Title")]
        public string text = "Zoxel";
        public RenderTextDataEditor textData;

        [Header("Sub Title")]
        public string subText = "The End is Cube";
        public RenderTextDataEditor subTextData;

        [Header("Demo")]
        public string demoText = "Demo";
        public RenderTextDataEditor demoTextData;

        public void Awake()
        {
            instance = this;
        }

        public void Update()
        {
            Initialize();
        }

        public void Initialize()
        {
            if (isInitialized)
            {
                return;
            }
            if (World.DefaultGameObjectInjectionWorld == null)
            {
                UnityEngine.Debug.LogError("Default Entities World not created.");
                return;
            }
            isInitialized = true;
            var EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            SpawnTitleUI(EntityManager);
        }

        public void SpawnTitleUI(EntityManager EntityManager)
        {
            if (NetworkUtil.IsHeadless())
            {
                return;
            }
            TitlescreenUISpawnSystem.SpawnTitleUI(EntityManager, text, subText, demoText, textData, subTextData, demoTextData);
        }
    }
}