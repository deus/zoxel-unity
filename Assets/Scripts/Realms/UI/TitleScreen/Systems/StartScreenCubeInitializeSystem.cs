using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Rendering;

namespace Zoxel.Realms
{
    //! Initializes a basic startscreen cube.
    /**
    *   - Material Initialize System -
    */
    [UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class StartScreenCubeInitializeSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        protected override void OnUpdate()
        {
            var outlineMaterial = StartScreenCubeSpawnSystem.startScreenCubeMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, StartScreenCube>()
                .ForEach((Entity e) =>
			{
                var mesh = MeshUtilities.GenerateCube(new float3(1, 1, 1));
                PostUpdateCommands.SetSharedComponentManaged(e, new ZoxMesh
                {
                    mesh = mesh,
                    material = outlineMaterial
                });
                PostUpdateCommands.SetComponent(e, new EditorName("[start-cube]"));
			}).WithoutBurst().Run();
        }
    }
}