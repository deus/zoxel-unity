using Unity.Entities;
using Zoxel.Players;

namespace Zoxel.Realms.UI
{
    //! Spawns player entities with controllers.
    [UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class TitlescreenRespawnSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            if (TitlescreenUIBootstrap.instance == null)
            {
                return;
            }
            Entities
                .WithNone<DestroyEntity>()
                .WithAll<OnPlayerDestroyed>()
                .ForEach((in PlayerLinks playerLinks) =>
            {
                // UnityEngine.Debug.LogError("Spawning new Title UI: " + playerLinks.players.Length);
                if (playerLinks.players.Length == 0)
                {
                    TitlescreenUIBootstrap.instance.SpawnTitleUI(EntityManager);
                }
            }).WithStructuralChanges().Run();
        }
    }
}