using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Cameras;
using Zoxel.Players;
using Zoxel.Input;

namespace Zoxel.Realms.UI
{
    //! Spawns a TitleScene Entity with underlying texts.
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class TitlescreenUISpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity startScreenPrefab;
        private Entity labelNoBackgroundPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var startScreenArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(OnChildrenSpawned),
                typeof(DestroyOnConnectedPlayer),
                typeof(DeviceConnector),
                typeof(StartScreen),
                typeof(Childrens),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            startScreenPrefab = EntityManager.CreateEntity(startScreenArchetype);
            EntityManager.SetComponentData(startScreenPrefab, new NonUniformScale { Value = new float3(1,1,1) });
            EntityManager.SetComponentData(startScreenPrefab, new Rotation { Value = quaternion.identity });
        }

        public static void SpawnTitleUI(EntityManager EntityManager, string text, string subText, string demoText,
            RenderTextDataEditor textData, RenderTextDataEditor subTextData, RenderTextDataEditor demoTextData)
        {
            var spawnEntity = EntityManager.CreateEntity();
            EntityManager.AddComponentData(spawnEntity, new SpawnStartScreen(
                new Text(text), new Text(subText), new Text(demoText),
                textData.GetData(), subTextData.GetData(), demoTextData.GetData()));
        }

        void InitializePrefabs()
        {
            if (labelNoBackgroundPrefab.Index != 0)
            {
                return;
            }
            labelNoBackgroundPrefab = EntityManager.Instantiate(UICoreSystem.labelNoBackgroundPrefab);
            EntityManager.AddComponent<Prefab>(labelNoBackgroundPrefab);
            EntityManager.SetComponentData(labelNoBackgroundPrefab, new NonUniformScale { Value = new float3(1,1,1) });
            EntityManager.SetComponentData(labelNoBackgroundPrefab, new Rotation { Value = quaternion.identity });
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var isDemo = BuildOptions.isDemo;
            InitializePrefabs();
            var uiDatam = UIManager.instance.uiDatam;
            var spawnCameraPosition = float3.zero; // CameraManager.instance.cameraSettings.spawnCameraPosition;
            var position = spawnCameraPosition + uiDatam.titlePosition;
            var titleSize = float2.zero; // uiDatam.titleSize;
            var position2 = spawnCameraPosition + uiDatam.subTitlePosition;
            var size2 = float2.zero; // uiDatam.subTitleSize;
            var position3 = position + (position2 - position) / 3f;
            var labelNoBackgroundPrefab = this.labelNoBackgroundPrefab;
            var startScreenPrefab = this.startScreenPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in SpawnStartScreen spawnStartScreen) =>
            {
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                var childrenCount = (byte) 2;
                var startScreen = PostUpdateCommands.Instantiate(entityInQueryIndex, startScreenPrefab);
                var label1 = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, labelNoBackgroundPrefab, startScreen, position, titleSize);
                UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, label1, 0);
                PostUpdateCommands.SetComponent(entityInQueryIndex, label1, spawnStartScreen.textData);
                PostUpdateCommands.SetComponent(entityInQueryIndex, label1, new RenderText(spawnStartScreen.text));
                var label2 = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, labelNoBackgroundPrefab, startScreen, position2, size2);
                UICoreSystem.SetChild(PostUpdateCommands,entityInQueryIndex, label2, 1);
                PostUpdateCommands.SetComponent(entityInQueryIndex, label2, spawnStartScreen.subTextData);
                PostUpdateCommands.SetComponent(entityInQueryIndex, label2, new RenderText(spawnStartScreen.subText));
                if (isDemo && spawnStartScreen.demoText.Length > 0)
                {
                    var label3 = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, labelNoBackgroundPrefab, startScreen, position3, size2);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, label3, 2);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, label3, spawnStartScreen.demoTextData);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, label3, new RenderText(spawnStartScreen.demoText));
                    childrenCount++;
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, startScreen, new OnChildrenSpawned(childrenCount));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}