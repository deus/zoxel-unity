using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Animations;
using Zoxel.Rendering;

namespace Zoxel.Realms
{
    //! Spawns a cube to show on the start screen.
    /**
    *   \todo Spawn alot of these in main menu.
    *   \todo Show all players connected to ZoxNet in main menu *differ their x y z based on player id*
    */
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class StartScreenCubeSpawnSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public Entity startScreenCubePrefab;
        public static UnityEngine.Material startScreenCubeMaterial;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(EditorName),
                typeof(StartScreenCube),
                typeof(ZoxMesh),
                typeof(ZoxMatrix),
                typeof(EternalScaling),
                typeof(EternalRotation),
                typeof(BasicRender),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(Scale));
            startScreenCubePrefab = EntityManager.CreateEntity(archetype);
            EntityManager.SetComponentData(startScreenCubePrefab, new Rotation { Value = quaternion.identity });
            EntityManager.SetComponentData(startScreenCubePrefab, new Scale { Value = 1 });
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var startScreenCubePrefab = this.startScreenCubePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities
                .WithAll<SpawnStartScreenCube>()
                .ForEach((int entityInQueryIndex) =>
			{
                var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, startScreenCubePrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Translation { Value = new float3(0, 0, 1.6f) });
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ZoxMatrix(new float3(0, 0, 1.6f)));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new EternalScaling(0.7f, 0.11f));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new EternalRotation(3f, new float3(45, 45, 0)));
			}).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static void Initialize(EntityManager EntityManager, UnityEngine.Material startScreenCubeMaterial)
        {
            StartScreenCubeSpawnSystem.startScreenCubeMaterial = startScreenCubeMaterial;
            var spawnEntity = EntityManager.CreateEntity();
            EntityManager.AddComponentData(spawnEntity, new SpawnStartScreenCube());
            EntityManager.AddComponentData(spawnEntity, new GenericEvent());
        }
    }
}