using Unity.Entities;

namespace Zoxel.Realms
{
    //! A tag for our cube.
    public struct StartScreenCube : IComponentData { }
}