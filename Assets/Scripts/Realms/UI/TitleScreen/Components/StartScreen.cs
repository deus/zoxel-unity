using Unity.Entities;

namespace Zoxel.Realms
{
    //! A tag for our start screen.
    public struct StartScreen : IComponentData { }
}