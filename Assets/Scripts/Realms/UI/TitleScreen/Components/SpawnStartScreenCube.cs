using Unity.Entities;

namespace Zoxel.Realms
{
    //! An event to spawn our cube.
    public struct SpawnStartScreenCube : IComponentData { }
}