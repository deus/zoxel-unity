using Unity.Entities;
using Zoxel.UI;

namespace Zoxel.Realms.UI
{
    public struct SpawnStartScreen : IComponentData
    {
        public Text text;
        public Text subText;
        public Text demoText;
        public RenderTextData textData;
        public RenderTextData subTextData;
        public RenderTextData demoTextData;

        public SpawnStartScreen(Text text, Text subText, Text demoText, RenderTextData textData, RenderTextData subTextData, RenderTextData demoTextData)
        {
            this.text = text;
            this.subText = subText;
            this.demoText = demoText;
            this.textData = textData;
            this.subTextData = subTextData;
            this.demoTextData = demoTextData;
        }
    }
}