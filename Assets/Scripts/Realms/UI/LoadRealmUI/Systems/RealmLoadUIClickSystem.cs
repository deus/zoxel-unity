using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Characters;

namespace Zoxel.Realms.UI
{
    //! When RealmLoadButton is clicked!
    /**
    *   - UIClickEvent System -
    *   \todo Convert to Parallel.
    *   \todo Add confirm UI to deleting a save file.
    */
    [UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class RealmLoadUIClickSystem : SystemBase
    {
        private const byte maxSaveRealms = 6;  // 0 for unlimited realms.  \todo Load from settings file.
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        protected override void OnUpdate()
        {
            var spawnDelay = 0.0f;
            var spawnRealmMakerPrefab = RealmMakerSpawnSystem.spawnPanelPrefab;
            var spawnMainMenuPrefab = MainMenuSpawnSystem.spawnPanelPrefab;
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var spawnCharacterUIPrefab = UICoreSystem.spawnCharacterUIPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<RealmLoadButton>()
                .ForEach((Entity e, in Child child, in PanelLink panelLink, in ParentLink parentLink, in UIClickEvent uiClickEvent) =>
            {
                var buttonType = uiClickEvent.buttonType;
                if (buttonType != ButtonActionType.Confirm)
                {
                    return;
                }
                var arrayIndex = child.index;
                var panelEntity = panelLink.panel;
                var controllerEntity = uiClickEvent.controller;
                if (!HasComponent<PanelUI>(panelEntity))
                {
                    return;
                }
                var panelChildren = EntityManager.GetComponentData<Childrens>(panelEntity);
                var saveListEntity = panelChildren.children[0];
                var saveFilesList = EntityManager.GetComponentData<ScrollListUI>(saveListEntity);
                var selectedToggleIndex = EntityManager.GetComponentData<ToggleGroup>(saveListEntity).selectedIndex;
                var saveSlotIndex = saveFilesList.index + selectedToggleIndex;
                var saveFilesListChildrens = EntityManager.GetComponentData<Childrens>(saveListEntity);
                var currentSaveRealms = saveFilesListChildrens.children.Length;
                if (selectedToggleIndex >= currentSaveRealms)
                {
                    UnityEngine.Debug.LogError("Togggle Index out of bounds: " + selectedToggleIndex + " : " + currentSaveRealms);
                    return;
                }
                // Return to MainMenu
                if (arrayIndex == 0)
                {
                    var removeUI = PostUpdateCommands.Instantiate(removeCharacterUIPrefab);
                    PostUpdateCommands.SetComponent(removeUI, new RemoveUI(controllerEntity, panelEntity));
                    var spawnMainMenuEntity = PostUpdateCommands.Instantiate(spawnMainMenuPrefab);
                    PostUpdateCommands.SetComponent(spawnMainMenuEntity, new CharacterLink(controllerEntity));
                    return;
                }
                // New Realm
                else if (arrayIndex == 2)
                {
                    var removeUI = PostUpdateCommands.Instantiate(removeCharacterUIPrefab);
                    PostUpdateCommands.SetComponent(removeUI, new RemoveUI(controllerEntity, panelEntity));
                    var newRealmEntity = PostUpdateCommands.Instantiate(spawnRealmMakerPrefab);
                    PostUpdateCommands.SetComponent(newRealmEntity, new CharacterLink(controllerEntity));
                    PostUpdateCommands.SetComponent(newRealmEntity, new DelayEvent(elapsedTime, spawnDelay));
                    return;
                }
                // Increase index
                else if (arrayIndex == 4)
                {
                    PostUpdateCommands.AddComponent<ScrollDown>(saveListEntity);
                    return;
                }
                // Decrease Index
                else if (arrayIndex == 5)
                {
                    PostUpdateCommands.AddComponent<ScrollUp>(saveListEntity);
                    return;
                }
                if (currentSaveRealms == 0)
                {
                    return;
                }
                var selectedSaveButton = saveFilesListChildrens.children[selectedToggleIndex];
                var realmName = EntityManager.GetComponentData<RenderText>(selectedSaveButton).text;
                var realmID = EntityManager.GetComponentData<SaveID>(selectedSaveButton).id;
                if (realmID == 0)
                {
                    UnityEngine.Debug.LogError("RealmID is 0. Cannot load.");
                    return;
                }
                // var saveSlots = SaveUtilities.GetSaveGameIDs();
                // Delete Realm
                if (arrayIndex == 1)
                {
                    try
                    {
                        SaveUtilities.DeleteRealm(realmID); 
                    }
                    catch
                    {
                        UnityEngine.Debug.LogError("Error trying to delete save realm: " + realmID);
                    }
                    saveFilesList.RemoveAt(saveSlotIndex);
                    PostUpdateCommands.SetComponent(saveListEntity, saveFilesList);
                    PostUpdateCommands.AddComponent<ScrollRefresh>(saveListEntity);
                }
                // Load Realm
                else if (arrayIndex == 3)
                {
                    PostUpdateCommands.AddComponent(PostUpdateCommands.CreateEntity(), new LoadRealm
                    { 
                        realmID = realmID,
                        realmName = realmName.Clone(),
                        player = controllerEntity
                    });
                    UICoreSystem.RemoveUI(PostUpdateCommands, controllerEntity, panelEntity);
                }
            }).WithoutBurst().Run();
        }
    }
}