using System;
using System.IO;
using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Cameras;
using Zoxel.Characters;
using Zoxel.Voxels;
using Zoxel.Worlds;
using Zoxel.Players;
using Zoxel.Players.UI;
using Zoxel.Characters.UI;

namespace Zoxel.Realms.UI
{
    //! Spawns a Realm Entity with load components.
    /**
    *   \todo Convert to Parallel.
    *   \todo Handle Errors - When fails to load - Spawn main menu again & Display a generic error message.
    *   - Load System -
    */
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class RealmLoadSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var realmPrefab = RealmSystemGroup.realmPrefab;
            var spawnCharacterLoadUIPrefab = CharacterLoadUISpawnSystem.spawnPanelPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((Entity e, in LoadRealm loadRealm) =>
            {
                if (loadRealm.realmID == 0)
                {
                    return;
                }
                PostUpdateCommands.DestroyEntity(e);
                var realmID = loadRealm.realmID;
                var playerEntity = loadRealm.player;
                // Spawn Realm Entity
                var realmEntity = PostUpdateCommands.Instantiate(realmPrefab);
                RealmSystemGroup.realm = realmEntity;
                PostUpdateCommands.AddComponent(realmEntity, new OnPlayerSpawned(playerEntity));
                PostUpdateCommands.SetComponent(realmEntity, new PlayerHomeLink(PlayerSystemGroup.playerHome));
                var realmLink = new RealmLink(realmEntity);
                PostUpdateCommands.SetComponent(playerEntity, realmLink);
                PostUpdateCommands.SetComponent(realmEntity, new ZoxID(realmID));
                PostUpdateCommands.SetComponent(realmEntity, new ZoxName(loadRealm.realmName));
                PostUpdateCommands.SetComponent(PlayerSystemGroup.playerHome, realmLink);
                // Character Selection
                var spawnCharacterLoadUIEntity = PostUpdateCommands.Instantiate(spawnCharacterLoadUIPrefab);
                PostUpdateCommands.SetComponent(spawnCharacterLoadUIEntity, new CharacterLink(playerEntity));
                // Save to last load time
                var lastLoadTimeFileName = SaveUtilities.GetRealmDirectory(realmID) + "LastLoadTime.zox";
                UnityEngine.Debug.Log("Updating Last Logged On Time: " + lastLoadTimeFileName);
                try
                {
                    var bytes = new byte[0];
                    #if WRITE_ASYNC
                    File.WriteAllBytesAsync(lastLoadTimeFileName, bytes);
                    #else
                    File.WriteAllBytes(lastLoadTimeFileName, bytes);
                    #endif
                }
                catch (Exception exception)
                {
                    UnityEngine.Debug.LogError("Error Writing to file: " + lastLoadTimeFileName + "\n" + exception.ToString());
                }
            }).WithoutBurst().Run();
        }
    }
}
                // Spawn Planet
                /*var worldIDString = SaveUtilities.LoadWorldID(realmID);
                UnityEngine.Debug.Log("Save WorldID is: " + worldIDString);
                var worldID = 0;
                if (worldIDString != "")
                {
                    worldID = int.Parse(worldIDString);
                }
                else
                {
                    worldID = IDUtil.GenerateUniqueID();
                    SaveUtilities.CreateWorldFolder(realmID, worldID);
                    UnityEngine.Debug.LogWarning("World was wiped. Creating new World for Realm [" + realmID + "] with ID: " + worldID);
                }*/