using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.UI;

namespace Zoxel.Realms.UI
{
    //! A link event system to link children after they spawn.
    /**
    *   - Post Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class RealmLoadUISpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<OnChildrenSpawned, RealmLoadUIList>()
                .ForEach((int entityInQueryIndex, in Childrens childrens, in ScrollListUI scrollListUI) =>
            {
                for (var i = 0; i < childrens.children.Length; i++)
                {
                    var scrollIndex = scrollListUI.index + i;
                    var saveID = scrollListUI.saveIDs[scrollIndex];
                    PostUpdateCommands.SetComponent(entityInQueryIndex, childrens.children[i], new SaveID(saveID));
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}