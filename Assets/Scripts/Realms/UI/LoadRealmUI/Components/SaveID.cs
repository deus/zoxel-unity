using Unity.Entities;

namespace Zoxel
{
    //! An ID pointing to a Save Directory.
    public struct SaveID : IComponentData
    {
        public int id;

        public SaveID(int id)
        {
            this.id = id;
        }
    }
}