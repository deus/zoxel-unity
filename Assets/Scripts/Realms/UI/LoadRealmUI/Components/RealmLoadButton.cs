using Unity.Entities;

namespace Zoxel.Realms.UI
{
    //! A button element of the RealmLoadUI.
    public struct RealmLoadButton : IComponentData { }
}