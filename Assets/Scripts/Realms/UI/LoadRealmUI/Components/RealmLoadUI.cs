using Unity.Entities;

namespace Zoxel.Realms.UI
{
    //! The tag component for the Realm loading window.
    public struct RealmLoadUI : IComponentData { }
}