using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Rendering;
using Zoxel.Players;

namespace Zoxel.Realms.UI
{
    //! Spawns a Map UI!
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class PauseMenuSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity spawnPrefab;
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;
        private Entity panelPrefab;
        private Entity buttonPrefab;
        private NativeArray<Text> texts;
        private NativeArray<Text> texts2;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnPauseMenu),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnPrefab = EntityManager.CreateEntity(spawnArchetype);
            uiHoldersQuery = GetEntityQuery(
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<UILink>());
            texts = new NativeArray<Text>(2, Allocator.Persistent);
            texts[0] = new Text("Continue");
            texts[1] = new Text("Escape");
            texts2 = new NativeArray<Text>(1, Allocator.Persistent);
            texts2[0] = new Text("Return");
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
            for (int i = 0; i < texts2.Length; i++)
            {
                texts2[i].Dispose();
            }
            texts2.Dispose();
        }

        private void InitializePrefabs()
        {
            if (panelPrefab.Index != 0)
            {
                return;
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var iconSize = 0.9f * uiDatam.GetIconSize(uiScale);
            var buttonStyle = uiDatam.buttonStyle.GetStyle(uiScale);
            var margins = uiDatam.GetIconMargins(uiScale);
            var padding = uiDatam.GetIconPadding(uiScale);
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            // panel
            panelPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(panelPrefab);
            EntityManager.AddComponent<PauseMenu>(panelPrefab);
            EntityManager.AddComponent<AutoGridY>(panelPrefab);
            EntityManager.AddComponent<SpawnButtonsList>(panelPrefab);
            EntityManager.SetComponentData(panelPrefab, new PanelUI(PanelType.PauseMenu));
            EntityManager.AddComponentData(panelPrefab, new PanelMinSize(new float2(9 * iconSize.x, 0)));
            // gridUI.iconSize = new float2(iconSize.x * 8 + textPadding.x * 2f, iconSize.y + textPadding.y * 2f);
            var gridUISize = new GridUISize(new float2(iconSize.x * 8 + textPadding.x * 2f, iconSize.y + textPadding.y * 2f));
            EntityManager.SetComponentData(panelPrefab, gridUISize);
            // button
            buttonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(buttonPrefab);
            EntityManager.AddComponent<PauseMenuButton>(buttonPrefab);
            UICoreSystem.SetRenderTextData(EntityManager, buttonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, iconSize.y * 0.7f, margins * 0.7f);
            // link list to element prefab
            EntityManager.AddComponentData(panelPrefab, new ListPrefabLink(buttonPrefab));
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var padding = uiDatam.GetIconPadding(uiScale);
            var margins = uiDatam.GetIconMargins(uiScale);
            var iconSize = 0.9f * uiDatam.GetIconSize(uiScale);
            var texts = this.texts;
            var texts2 = this.texts2;
            var buttonPrefab = this.buttonPrefab;
            var panelPrefab = this.panelPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uiHolderEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
            var controllerLinks = GetComponentLookup<ControllerLink>(true);
            uiHolderEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<SpawnPauseMenu>()
                .ForEach((int entityInQueryIndex, in CharacterLink characterLink) =>
            {
                var isPlayerOne = false;
                if (controllerLinks.HasComponent(characterLink.character))
                {
                    var controllerLink = controllerLinks[characterLink.character];
                    isPlayerOne = HasComponent<PlayerOne>(controllerLink.controller);
                }
                var texts3 = texts;
                if (!isPlayerOne)
                {
                    texts3 = texts2;
                }
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, panelPrefab);
                var gridUI = new GridUI();
                gridUI.gridSize = new int2(1, texts3.Length);
                gridUI.margins = margins;
                gridUI.padding = padding;
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, gridUI);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new SpawnButtonsList(in texts3));
                if (characterLink.character.Index > 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, characterLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab),
                        new OnUISpawned(characterLink.character, 1));
                    if (cameraLinks.HasComponent(characterLink.character))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLinks[characterLink.character]);
                    }
                }
			})  .WithReadOnly(texts)
                .WithReadOnly(texts2)
                .WithReadOnly(cameraLinks)
                .WithReadOnly(controllerLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}