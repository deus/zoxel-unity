using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.UI;

namespace Zoxel.Realms.UI
{
    //! Handls selection for a pause ui.
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class PauseUISelectSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private NativeArray<Text> texts;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            texts = new NativeArray<Text>(8, Allocator.Persistent);
            texts[0] = new Text("Keep Playing");
            texts[1] = new Text("Return to Main Menu");
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var texts = this.texts;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<PauseMenuButton>()
                .ForEach((Entity e, int entityInQueryIndex, in Child child, in UISelectEvent uiSelectEvent) =>
            {
                var tooltip = texts[child.index].Clone();
                var controllerEntity = uiSelectEvent.character;
                PostUpdateCommands.AddComponent(entityInQueryIndex, controllerEntity, new SetTooltipText(tooltip));
            })  .WithReadOnly(texts)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}