using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Players;
using Zoxel.Realms;
using Zoxel.Input;

namespace Zoxel.Realms.UI
{
    //! Handle ui UIClickEvent's for PauseMenuButton.
    /**
    *   - UIClickEvent System -
    */
    [BurstCompile, UpdateInGroup(typeof(RealmUISystemGroup))]
    public partial class PauseUIClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery characterQuery;
        private EntityQuery controllerQuery;
        private EntityQuery realmsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            characterQuery = GetEntityQuery(
                ComponentType.ReadOnly<Character>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<DeadEntity>());
            controllerQuery = GetEntityQuery(
                ComponentType.ReadOnly<Controller>(),
                ComponentType.ReadOnly<CharacterLink>(),
                ComponentType.Exclude<DestroyEntity>());
            realmsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var characterEntities = characterQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var realmLinks = GetComponentLookup<RealmLink>(true);
            var controllerEntities = controllerQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var characterLinks = GetComponentLookup<CharacterLink>(true);
            var realmEntities = controllerQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var playerHomeLinks = GetComponentLookup<PlayerHomeLink>(true);
            characterEntities.Dispose();
            controllerEntities.Dispose();
            realmEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<PauseMenuButton>()
                .ForEach((Entity e, int entityInQueryIndex, in Zoxel.Transforms.Child child, in ParentLink parentLink, in UIClickEvent uiClickEvent) =>
            {
                var ui = parentLink.parent;
                var arrayIndex = child.index;
                var controllerEntity = uiClickEvent.controller;
                var character = characterLinks[controllerEntity].character;
                if (arrayIndex == 0)
                {
                    if (!HasComponent<RealmUIDelay>(character))
                    {
                        PostUpdateCommands.AddComponent<UnPauseGame>(entityInQueryIndex, character);
                        PostUpdateCommands.AddComponent<RealmUIDelay>(entityInQueryIndex, character);
                    }
                }
                else if (arrayIndex == 1)
                {
                    var realmEntity = realmLinks[character].realm;
                    UICoreSystem.RemoveUI(PostUpdateCommands, entityInQueryIndex, removeCharacterUIPrefab, character, ui);
                    var endRealmEntity = PostUpdateCommands.CreateEntity(entityInQueryIndex);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, endRealmEntity, new EndRealm(elapsedTime));
                    PostUpdateCommands.AddComponent(entityInQueryIndex, endRealmEntity, new RealmLink(realmEntity));
                    PostUpdateCommands.AddComponent(entityInQueryIndex, endRealmEntity, playerHomeLinks[realmEntity]);
                }
            })  .WithReadOnly(realmLinks)
                .WithReadOnly(characterLinks)
                .WithReadOnly(playerHomeLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}