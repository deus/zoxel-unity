#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.Collections;
using Unity.Entities;
using Zoxel.Players;

namespace Zoxel
{
    //! Handles exiting playmode by hijacking it.
    [InitializeOnLoad]
    public static class ExitPlayModeHandler
    {
        private static WaitForEndOfFrame delay = new WaitForEndOfFrame();

        static ExitPlayModeHandler()
        {
            EditorApplication.playModeStateChanged += ModeChanged;
        }

        // [MenuItem("Zoxel/Exit Play Mode %F1")]
        [MenuItem("Zoxel/Exit Play Mode %e")]
        static void ExitPlayMode()
        {
            if (CoroutineStarter.instance == null)
            {
                UnityEngine.Debug.LogError("CoroutineStarter.instance is null.");
                return;
            }
            CoroutineStarter.instance.StartCoroutine(ExitPlayModeAsync());
            // EditorApplication.ExitPlaymode(); // string,string[]);
        }

        public static EntityManager EntityManager
        {
            get
            { 
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        public static IEnumerator ExitPlayModeAsync()
        {
            // Destroy entities - realm - players
            var playerHome = PlayerSystemGroup.playerHome;
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(playerHome);
            if (playerLinks.players.Length != 0)
            {
                var playerEntity = playerLinks.players[0];
                EntityManager.AddComponent<DestroyEntity>(playerHome);
                var realmEntity = EntityManager.GetComponentData<RealmLink>(playerEntity).realm;
                if (realmEntity.Index > 0)
                {
                    EntityManager.AddComponent<DestroyEntity>(realmEntity);
                }
            }
            for (int i = 0; i < 32; i++)
            {
                yield return delay;
            }
            EditorApplication.ExitPlaymode(); // string,string[]);
        }

        static void ModeChanged(PlayModeStateChange playModeState)
        {
            /*if (playModeState == PlayModeStateChange.EnteredEditMode) 
            {
                Debug.Log("Entered Edit mode.");
            }*/
            if (!EditorApplication.isPlayingOrWillChangePlaymode && EditorApplication.isPlaying) 
            {
                Debug.Log("Exiting playmode from ExitPlayModeHandler.");
            }
        }
    }
}
#endif