using Unity.Entities;

namespace Zoxel.Realms
{
    public struct EndRealm : IComponentData 
    {
        public byte endRealmState;
        public double timeStarted;

        public EndRealm(double timeStarted)
        {
            this.timeStarted = timeStarted;
            this.endRealmState = 0;
        }
    }
}