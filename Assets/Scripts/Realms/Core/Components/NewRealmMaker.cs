using Unity.Entities;

namespace Zoxel.Realms
{
    //! An event for spawning a new RealmMaker.
    public struct NewRealmMaker : IComponentData
    { 
        public byte state;
        public Entity newGame;
        public Entity player;
        public Entity newRealmUI;    // used during process
        public int newGameID;

        public NewRealmMaker(Entity player)
        {
            this.player = player;
            this.state = 0;
            this.newGameID = 0;
            this.newGame = new Entity();
            this.newRealmUI = new Entity();
        }
    }
}