using Unity.Entities;

namespace Zoxel.Realms
{
    //! Closes Application! No more zoxel.
    public struct ExitGame : IComponentData { }
}