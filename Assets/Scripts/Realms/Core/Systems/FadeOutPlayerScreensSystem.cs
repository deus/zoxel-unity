using Unity.Entities;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using Zoxel.UI;
using Zoxel.Cameras;
using Zoxel.Players;
using Zoxel.Rendering;
using Zoxel.Input;

namespace Zoxel.Realms
{
    //! Fade out screen for all players.
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class FadeOutPlayerScreensSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Controller>(),
                ComponentType.ReadOnly<ScreenFaderLink>(),
                ComponentType.ReadOnly<PlayerTooltipLinks>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var fadeOutTime = UIManager.instance.uiSettings.fadeoutScreenTime; // 1f;
            var fadeScreenWaitTime = UIManager.instance.uiSettings.fadeoutScreenWaitTime; // 1f;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var screenFaderLinks = GetComponentLookup<ScreenFaderLink>(true);
            var playerTooltipLinks = GetComponentLookup<PlayerTooltipLinks>(true);
            controllerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<OnPlayerSpawned>()
                .WithAll<FadeOutPlayerScreens>()
                .ForEach((Entity e, int entityInQueryIndex, in PlayerLinks playerLinks) =>
            {
                for (int i = 0; i < playerLinks.players.Length; i++)
                {
                    var playerEntity = playerLinks.players[i];
                    if (playerEntity.Index != 0)
                    {
                        // turn controls off
                        PostUpdateCommands.AddComponent(entityInQueryIndex, playerEntity, new SetControllerMapping(ControllerMapping.None));
                        // fade out screen
                        if (screenFaderLinks.HasComponent(playerEntity))
                        {
                            var screenFader = screenFaderLinks[playerEntity].screenFader;
                            PostUpdateCommands.SetComponent(entityInQueryIndex, screenFader, new Fader(0, 1, fadeOutTime));
                            PostUpdateCommands.AddComponent(entityInQueryIndex, screenFader, new ReverseFader(fadeOutTime + fadeScreenWaitTime));
                            PostUpdateCommands.AddComponent<FadeIn>(entityInQueryIndex, screenFader);
                        }
                        // clear logs
                        if (playerTooltipLinks.HasComponent(playerEntity))
                        {
                            var playerTooltipLinks2 = playerTooltipLinks[playerEntity];
                            // playerTooltipLinks2.ClearLog(PostUpdateCommands, entityInQueryIndex);
                            playerTooltipLinks2.ClearTexts(PostUpdateCommands, entityInQueryIndex);
                            // playerTooltipLinks2.SetPlayerSelectionTooltip(PostUpdateCommands, entityInQueryIndex, playerEntity, new Text());
                        }
                    }
                }
                PostUpdateCommands.RemoveComponent<FadeOutPlayerScreens>(entityInQueryIndex, e);
			})  .WithReadOnly(screenFaderLinks)
                .WithReadOnly(playerTooltipLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
            // var clearMapTime1 = 0f;
            /*var clearMapTime2 = 1 ; // 3f
            var clearMapTime3 = clearMapTime2 + 1f; // 3f
            var clearMapTime4 = clearMapTime3 + 1f; // 4
            var clearMapTime5 = clearMapTime4 + clearMapTime2; // 4
            var blackPauseDelay = clearMapTime4 - clearMapTime2; //  clearMapTime3 + clearMapTime4;
            */