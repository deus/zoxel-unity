using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Players;
using Zoxel.Input;
using Zoxel.Cameras;
using Zoxel.Cameras.PostProcessing;
using Zoxel.Weather;
using Zoxel.Weather.Skybox;

namespace Zoxel.Realms
{
    //! Destroys player characters on game end, and also sets camera background colors back to main menu.
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class PlayerCharactersDestroySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Controller>(),
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<CharacterLink>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var mainMenuBackgroundColor = CameraManager.instance.cameraSettings.mainMenuBackgroundColor;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var cameraLinks = GetComponentLookup<CameraLink>(true);
            var characterLinks = GetComponentLookup<CharacterLink>(true);
            controllerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<OnPlayerSpawned>()
                .WithAll<DestroyPlayerCharacters>()
                .ForEach((Entity e, int entityInQueryIndex, in PlayerLinks playerLinks) =>
            {
                PostUpdateCommands.RemoveComponent<DestroyPlayerCharacters>(entityInQueryIndex, e);
                for (int i = 0; i < playerLinks.players.Length; i++)
                {
                    var playerEntity = playerLinks.players[i];
                    if (playerEntity.Index != 0)
                    {
                        if (HasComponent<CameraLink>(playerEntity))
                        {
                            var cameraEntity = cameraLinks[playerEntity].camera;
                            PostUpdateCommands.AddComponent(entityInQueryIndex, cameraEntity, new SetSkyColor(mainMenuBackgroundColor));
                            PostUpdateCommands.AddComponent(entityInQueryIndex, cameraEntity, new UpdatePostProcessing(PostProcessingProfileType.MainMenu));
                            PostUpdateCommands.RemoveComponent<DisableFirstPersonCamera>(entityInQueryIndex, cameraEntity);
                            PostUpdateCommands.AddComponent<DetatchCamera>(entityInQueryIndex, cameraEntity);
                        }
                        if (HasComponent<CharacterLink>(playerEntity))
                        {
                            var characterEntity = characterLinks[playerEntity].character;
                            PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, characterEntity);
                        }
                    }
                }
			})  .WithReadOnly(cameraLinks)
                .WithReadOnly(characterLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}