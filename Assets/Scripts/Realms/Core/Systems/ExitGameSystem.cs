using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Realms
{
    //! Closes Application! No more zoxel.
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class ExitGameSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity exitGamePrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var exitGameArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(DelayEvent),
                typeof(ExitGame));
            exitGamePrefab = EntityManager.CreateEntity(exitGameArchetype);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<DelayEvent>()
                .ForEach((Entity e, int entityInQueryIndex, in ExitGame exitGame) =>
            {
                PostUpdateCommands2.DestroyEntity(e);
                UnityEngine.Application.Quit();
            }).WithoutBurst().Run();
        }
    }
}