using Unity.Entities;
using Unity.Burst;
// todo: when loading game, change UI colours based on game - making text lighter

namespace Zoxel.Realms
{
    //! Cleans up GameColors
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class GameColorsDestroySystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, GameColors>()
                .ForEach((in GameColors gameColors) =>
            {
                gameColors.Dispose();
			}).ScheduleParallel();
        }
    }
}