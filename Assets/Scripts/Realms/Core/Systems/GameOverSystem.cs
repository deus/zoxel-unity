﻿using Unity.Entities;
using Unity.Mathematics;

using Unity.Collections;

namespace Zoxel.Realms
{
    /*[BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class GameOverSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            // check for end game
            Entities.ForEach((Entity e, ref Realm game, in PlayerLinks playerLinks) =>
            {
                if (game.lastCheckedEndOfGame == 0)
                {
                    return;
                }
                var timePassed = World.Time.ElapsedTime - game.lastCheckedEndOfGame;
                if (game.state == ((byte)GameState.InGame) && timePassed >= 5)
                {
                    game.lastCheckedEndOfGame = World.Time.ElapsedTime;
                    if (playerLinks.players.Length == 0)
                    {
                        return;
                    }
                    //Debug.LogError("Checking is game over: " + timePassed + " at " + World.Time.ElapsedTime);
                    bool isGameOver = true;
                    for (int i = 0; i < playerLinks.players.Length; i++)
                    {
                        var playerEntity = playerLinks.players[i];
                        if (playerEntity.Index != -1 && !HasComponent<Character>(playerEntity))
                        {
                            //Debug.Log("Player [" + i + "] has died. Respawning them.");
                        }
                        else
                        {
                            isGameOver = false;  
                        }
                    }
                    if (isGameOver)
                    {
                        //Debug.Log("Realm Over for all players!");
                    }
                }
            }).WithoutBurst().Run();
        }
    }*/
}