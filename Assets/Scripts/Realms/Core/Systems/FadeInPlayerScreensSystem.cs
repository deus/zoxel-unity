using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Cameras;
using Zoxel.Players;
using Zoxel.Rendering;
using Zoxel.Input;
using Zoxel.Audio;
using Zoxel.Audio.Music;

namespace Zoxel.Realms
{
    //! Fades in player screens. \todo Move To players namespace?
    /**
    *   Atm it just transitions cameralink to main menu entity. And enabled main menu camera. While destroying player cameras.
    */
    [BurstCompile, UpdateInGroup(typeof(RealmSystemGroup))]
    public partial class FadeInPlayerScreensSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Controller>(),
                ComponentType.ReadWrite<CameraLink>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var mainMenuCameraEntity = new Entity();
            if (CameraReferences.cameras.Count > 0)
            {
                mainMenuCameraEntity = CameraReferences.cameras[0];
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var cameraLinks = GetComponentLookup<CameraLink>(false);
            var screenFaderLinks = GetComponentLookup<ScreenFaderLink>(true);
            controllerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<OnPlayerSpawned>()
                .WithAll<FadeInPlayerScreens>()
                .ForEach((Entity e, int entityInQueryIndex, in PlayerLinks playerLinks) =>
            {
                PostUpdateCommands.RemoveComponent<FadeInPlayerScreens>(entityInQueryIndex, e);
                PostUpdateCommands.SetComponent(entityInQueryIndex, mainMenuCameraEntity, new CameraEnabledState(1));
                PostUpdateCommands.AddComponent<EnableMusic>(entityInQueryIndex, mainMenuCameraEntity);
                for (int i = 0; i < playerLinks.players.Length; i++)
                {
                    var playerEntity = playerLinks.players[i];
                    if (playerEntity.Index != 0)
                    {
                        // fade out screen
                        if (cameraLinks.HasComponent(playerEntity))
                        {
                            var cameraLink = cameraLinks[playerEntity];
                            PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, cameraLink.camera);
                            cameraLink.camera = mainMenuCameraEntity;
                            cameraLinks[playerEntity] = cameraLink;
                            var screenFaderLink = screenFaderLinks[playerEntity];
                            PostUpdateCommands.SetComponent(entityInQueryIndex, screenFaderLink.screenFader, new CameraLink(mainMenuCameraEntity));
                            // PostUpdateCommands.AddComponent(entityInQueryIndex, cameraEntity, new UpdatePostProcessing(PostProcessingProfileType.MainMenu));
                        }
                    }
                }
			})  .WithNativeDisableContainerSafetyRestriction(cameraLinks)
                .WithReadOnly(screenFaderLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}