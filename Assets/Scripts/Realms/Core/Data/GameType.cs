namespace Zoxel.Realms
{
    public enum GameType : byte
    {
        TowerDefence,   // Spawn towers to defeat waves of enemy
        Adventure,       // top down view, rotate character around, hit things!
        God,            // fly everywhere, use tools to edit levels, switch level easily, can't die
        Survival        // enemies spawn infinitely in waves (like TD), but have to use a character to survive, spawns a shop and a heart tower to defend
    }

    public enum EndGameReason : byte
    {
        None,
        Victory,
        Defeat,
        Desertion
    }
}