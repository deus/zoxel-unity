﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Realms
{
    public struct WaveData
    {
        public int spawnAmount;
        public float spawnCooldown; // how long does it take for wave to appear
        public float3 spawnPosition;
    }
}