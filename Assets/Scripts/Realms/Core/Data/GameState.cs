namespace Zoxel.Realms
{
    public enum GameState : byte
    {
        StartScreen,
        MainMenu,
        LoadCharacter,
        InGame,
        RealmUI,
        ClearingGame
    }
}