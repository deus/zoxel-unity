namespace Zoxel.Realms
{
    public enum ClearGameState : byte
    {
        DisablePlayer,
        DetatchCamera,
        ClearMap,
        PlayersSpawnMainMenu,
        EnablePlayer
    }
}