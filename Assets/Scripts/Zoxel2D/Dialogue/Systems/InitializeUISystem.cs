using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Zoxel.Movement;
using Zoxel.UI;

namespace Zoxel.Zoxel2D
{
    //! Spawns dialogue text for our 2D game.
    /**
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class InitializeUISystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var panelMaterial = UIManager.instance.materials.panelMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, DialogueUI2D>()
                .ForEach((Entity e, ref AnimatedText animatedText, ref RenderText renderText, in ZoxID zoxID) =>
            {
                if (zoxID.id == 0)
                {
                    return;
                }
                var random = new Random();
                random.InitState((uint)zoxID.id);
                var text = "Zonk";
                var greetingsChance = random.NextInt(100);
                if (greetingsChance <= 20)
                {
                    text = "Hello " + text + ".";
                }
                else if (greetingsChance <= 40)
                {
                    text = "Hi " + text + "!";
                }
                else if (greetingsChance <= 60)
                {
                    text = "Greetings Lord " + text + ".";
                }
                else if (greetingsChance <= 80)
                {
                    text = "Good Evening Sir " + text + ".";
                }
                else // if (greetingsChance <= 100)
                {
                    text = "Yo ma man " + text + ", Wud up?";
                }
                animatedText.SetText(text);
            }).WithoutBurst().Run();
		}
    }
}