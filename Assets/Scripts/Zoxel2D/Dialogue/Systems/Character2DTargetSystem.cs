using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Dialogue.Authoring;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.UI;
using Zoxel.Dialogue;
using Zoxel.Players;
using Zoxel.Input;
using Zoxel.Rendering;

namespace Zoxel.Zoxel2D
{
    //! Spawns a 2D dialogue ui.
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class Character2DTargetSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery charactersQuery;
        private Entity dialogue2DPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            charactersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Character2D>(),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.Exclude<DestroyEntity>());
            var dialogue2DArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(ZoxID),
                typeof(DialogueUI2D),
                typeof(CharacterLink),
                typeof(NewCharacterUI),
                typeof(CharacterUI),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(RenderText),
                typeof(RenderTextData),
                typeof(AnimatedText),
                typeof(TrailerUI),
                typeof(FaceCamera),
                typeof(InvisibleUI),
                typeof(MaterialBaseColor),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld));
            dialogue2DPrefab = EntityManager.CreateEntity(dialogue2DArchetype);
            EntityManager.SetComponentData(dialogue2DPrefab, new Rotation { Value = quaternion.identity });
            RequireForUpdate<DialogueSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var dialogueSettings = GetSingleton<DialogueSettings>();
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var timePerLetter = dialogueSettings.timePerLetter;
            var uiDatam = UIManager.instance.uiDatam;
            var dialogueSpeechStyle = uiDatam.dialogueSpeechStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            float fontSize = 0.3f; // uiDatam.GetIconSize().y * 100;
            var dialogue2DPrefab2 = dialogue2DPrefab;
            var positionOffset = 0.8f;
            var elapsedTime = (float) World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var characterEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var translations = GetComponentLookup<Translation>(true);
            Dependency = Entities
                .WithNone<InitializeEntity>()
                .WithChangeFilter<Translation>()
                .ForEach((Entity e, int entityInQueryIndex, ref Target2D target2D, in Translation translation, in ZoxID zoxID) =>
            {
                // get distance to all other characters (later just check ones in this chunk)
                var position2D = new float2(translation.Value.x, translation.Value.y);
                var closestDistance = float.PositiveInfinity;
                var closestEntity = new Entity();
                var closestPosition = new float2();
                for (int i = 0; i < characterEntities.Length; i++)
                {
                    var otherCharacter = characterEntities[i];
                    if (otherCharacter != e)
                    {
                        var otherPosition = translations[otherCharacter];
                        var otherPosition2D = new float2(otherPosition.Value.x, otherPosition.Value.y);
                        var distanceToCharacter = math.distance(position2D, otherPosition2D);
                        if (distanceToCharacter < closestDistance)
                        {
                            closestDistance = distanceToCharacter;
                            closestEntity = otherCharacter;
                            closestPosition = otherPosition2D;
                        }
                    }
                }
                if (closestDistance <= 3)
                {
                    if (target2D.target != closestEntity)
                    {
                        if (!HasComponent<Controller>(target2D.target) && HasComponent<UILink>(target2D.target))
                        {
                            PostUpdateCommands.AddComponent<DestroyUIs>(entityInQueryIndex, target2D.target);
                        }
                        target2D.target = closestEntity;
                        // if target has dialogue, then open dialogue window here for npc
                        if (HasComponent<Controller>(e) && !HasComponent<Controller>(closestEntity))
                        {
                            // UnityEngine.Debug.LogError(closestEntity.Index + " Says: The King is waiting on you Zonk!");
                            var npcPanelUI = SpawnPanel(PostUpdateCommands, entityInQueryIndex, dialogue2DPrefab2, dialogueSpeechStyle.color.GetColor(), float2.zero);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, npcPanelUI, zoxID);
                            // todo: remember to destroy the last one too!
                            var renderText = new RenderTextData
                            {
                                textGenerationData = dialogueSpeechStyle.textGenerationData,
                                color = dialogueSpeechStyle.textColor,
                                outlineColor = dialogueSpeechStyle.textOutlineColor,
                                fontSize = fontSize
                            };
                            PostUpdateCommands.SetComponent(entityInQueryIndex, npcPanelUI, renderText); 
                            PostUpdateCommands.SetComponent(entityInQueryIndex, npcPanelUI, new AnimatedText(timePerLetter, elapsedTime, zoxID.id));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, npcPanelUI, new Translation { Value = new float3(0, positionOffset, 0) + new float3(closestPosition.x, closestPosition.y, 0) });
                            PostUpdateCommands.SetComponent(entityInQueryIndex, npcPanelUI, new TrailerUI(positionOffset));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, npcPanelUI, new CharacterLink(closestEntity));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(closestEntity, 1));
                        }
                    }
                }
                else
                {
                    if (!HasComponent<Controller>(target2D.target) && HasComponent<UILink>(target2D.target))
                    {
                        PostUpdateCommands.AddComponent<DestroyUIs>(entityInQueryIndex, target2D.target);
                    }
                    target2D.target = new Entity();
                }
            })  .WithReadOnly(characterEntities)
                .WithDisposeOnCompletion(characterEntities)
                .WithReadOnly(translations)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}

        public static Entity SpawnPanel(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity panelPrefab,
            UnityEngine.Color color, float2 panelSize)
        {
            var characterUI = PostUpdateCommands.Instantiate(entityInQueryIndex, panelPrefab); //  EntityManager.Instantiate(panelPrefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, characterUI, new Size2D(panelSize));
            PostUpdateCommands.SetComponent(entityInQueryIndex, characterUI, new MaterialBaseColor { Value = new float4(color.r, color.g, color.b, color.a) });
            PostUpdateCommands.SetComponent(entityInQueryIndex, characterUI, new NonUniformScale { Value = new float3(1, 1, 1) });
            // mesh and material set in InitializePanelUI
            return characterUI;
        }
    }
}