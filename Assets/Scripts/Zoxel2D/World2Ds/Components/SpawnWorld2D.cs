using Unity.Entities;

namespace Zoxel.Zoxel2D
{
    public struct SpawnWorld2D : IComponentData
    {
        public Text loadPath;

		public SpawnWorld2D(Text loadPath)
		{
			this.loadPath = loadPath;
		}
    }
}