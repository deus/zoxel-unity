using Unity.Entities;

namespace Zoxel.Zoxel2D
{
    //! Links to a World2D Entity.
    public struct World2DLink : IComponentData
    {
        public Entity world2D;

        public World2DLink(Entity world2D)
        {
            this.world2D = world2D;
        }
    }
}