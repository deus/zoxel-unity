using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Textures;
using Zoxel.Textures.Tilemaps;

namespace Zoxel.Zoxel2D
{
    //! Spawns a World2D entity.
    /**
    *   - Spawn System -
    *   Tiles Workflow:
    *       \todo TileManager - Stores TileDatams
    *       \todo Spawn/Destroy Tiles as Entities
    *       \todo Import tiles into MeshBuilder
    *       \todo Spawn/Destroy Textures as Entities
    *       \todo Generate Tilemap for TilesChunk
    *   More:
    *       \todo TileRules ^ using same methods
    *       \todo Do I want to load tiles into a new map? Save into Map file? Sounds about right.
    *       \todo Realm2D Entity / Contains meta data for game?
    */
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class World2DSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity world2DPrefab;
        public static Entity freshWorld2DPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var world2DArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(SpawnTiles),
                typeof(SpawnChunks2D),
                typeof(ZoxID),
                typeof(World2D),
                typeof(Childrens),      // links to chunks
                typeof(LoadedPath),     // IO
                typeof(Translation),
                // Tiles
                typeof(TileLinks),
                // Put tilemap here
                typeof(Tilemap),
                typeof(Texture),
                typeof(EntityMaterials)
            );
            world2DPrefab = EntityManager.CreateEntity(world2DArchetype);
            freshWorld2DPrefab = EntityManager.Instantiate(world2DPrefab);
            EntityManager.AddComponent<Prefab>(freshWorld2DPrefab);
            EntityManager.AddComponent<GenerateWorld2D>(freshWorld2DPrefab);
            EntityManager.AddComponent<LoadEntity>(world2DPrefab);
        }

        public static void SpawnWorld2D(EntityManager EntityManager, Text loadPath)
        {
            var spawnWorld2DArchetype = EntityManager.CreateArchetype(typeof(SpawnWorld2D));
            var spawnEntity = EntityManager.CreateEntity(spawnWorld2DArchetype);
            EntityManager.SetComponentData(spawnEntity, new SpawnWorld2D { loadPath = loadPath });
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var world2DPrefab = World2DSpawnSystem.world2DPrefab;
            var freshWorld2DPrefab = World2DSpawnSystem.freshWorld2DPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in SpawnWorld2D spawnWorld2D) =>
            {
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                // spawn a planet 2D
                Entity world2DEntity;
                if (spawnWorld2D.loadPath.Length != 0)
                {
                    // PostUpdateCommands.RemoveComponent<GenerateWorld2D>(entityInQueryIndex, world2DEntity);
                    world2DEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, world2DPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, world2DEntity, new LoadEntity(spawnWorld2D.loadPath));
                }
                else
                {
                    world2DEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, freshWorld2DPrefab);
                }
                // child of universe/game entity?
                // spawn player character here too
                // just give it a zoxmesh for now
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}