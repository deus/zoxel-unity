using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.UI;
using Zoxel.Cameras;
using UnityEngine.InputSystem;
using Zoxel.Players;
using System.IO;
using Zoxel.Input;

namespace Zoxel.Zoxel2D
{
    /*[BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class World2DLoadSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery characterQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            characterQuery = GetEntityQuery(
                ComponentType.Exclude<Controller>(),
                ComponentType.ReadOnly<Character2D>(),
                ComponentType.Exclude<DestroyEntity>());
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            // destroy entity for character2D
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var characterEntities = characterQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            //var translations = GetComponentLookup<Translation>(true);
            Entities
                .WithNone<SpawnChunks2D, OnChildrenSpawned>()
                .WithAll<LoadEntity, World2D>()
                .ForEach((Entity e, ref LoadedPath loadedPath, in Childrens childrens, in LoadEntity loadWorld2D) =>
            {
                PostUpdateCommands.RemoveComponent<LoadEntity>(e);
                string path;
                if (loadWorld2D.loadPath.Length != 0)
                {
                    path = loadWorld2D.loadPath.ToString();
                    loadWorld2D.Dispose();
                }
                else
                { 
                    path = "";
                    UnityEngine.Debug.LogError("Path is empty for loading game.");
                }
                loadedPath.path.SetText(path);
                if (path.Length == 0 || !File.Exists(path))
                {
                    UnityEngine.Debug.LogError("Could not load: " + path);
                    PostUpdateCommands.AddComponent<GenerateWorld2D>(e);
                    return;
                }
                for (int i = 0; i < characterEntities.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(characterEntities[i]);
                }
                var bytes = File.ReadAllBytes(path);
                // split by chunk amount? or just add athingo per..!
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    var tilesChunkEntity = childrens.children[i];
                    if (!HasComponent<TileChunk>(tilesChunkEntity))
                    {
                        UnityEngine.Debug.LogError("TileChunk Does not exist: " + i);
                        continue;
                    }
                    PostUpdateCommands.AddComponent<ChunkBuilder2D>(tilesChunkEntity);
                    var tileChunk = EntityManager.GetComponentData<TileChunk>(tilesChunkEntity);
                    // bytes from tileChunk.data.tiles
                    // save to file
                    //var fileContent = File.ReadAllBytes(path);
                    //texture.LoadImage(fileContent);
                    for (int j = 0; j < tileChunk.data.tiles.Length; j++)
                    {
                        // if byte is 255, then spawn a npc
                        var tile = bytes[j];
                        // otherwise set tile
                        if (tile == 255)
                        {
                            tileChunk.data.tiles[j] = 0;
                            if (Character2DSpawnSystem.spawnCharacter2DPrefab.Index == 0)
                            {
                                continue;
                            }
                            var spawnPosition = new int2(); // convert j to position
                            spawnPosition.x = j / tileChunk.data.size.x;
                            spawnPosition.y = j % tileChunk.data.size.x;
                            var spawnEntity = PostUpdateCommands.Instantiate(Character2DSpawnSystem.spawnCharacter2DPrefab);
                            PostUpdateCommands.SetComponent(spawnEntity, new SpawnCharacter2D
                            { 
                                spawnPosition = new float3(spawnPosition.x + 0.5f, spawnPosition.y + 0.5f, 0)
                            });
                        }
                        else
                        {
                            tileChunk.data.tiles[j] = tile;
                        }
                    }
                }
            })  .WithReadOnly(characterEntities)
                .WithDisposeOnCompletion(characterEntities)
                //.WithReadOnly(translations)
                .WithoutBurst()
                .Run();
        }
    }*/
}
                    /*#if UNITY_EDITOR
                    path = UnityEditor.EditorUtility.OpenFilePanel(
                        "Load World2D",
                        "",
                        "znk");
                    #else
                    path = "";
                    #endif*/