using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using System.IO;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.UI;
using Zoxel.Cameras;

namespace Zoxel.Zoxel2D
{
    //! Saves World2D to a file.
    /**
    *   - Save System -
    */
    /*[BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class World2DSaveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery characterQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            characterQuery = GetEntityQuery(
                ComponentType.ReadOnly<Character2D>(),
                ComponentType.Exclude<DestroyEntity>());
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // destroy entity for character2D
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var characterEntities = characterQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var translations = GetComponentLookup<Translation>(true);
            Entities
                .WithAll<SaveEntity, World2D>()
                .ForEach((Entity e, in LoadedPath loadedPath, in Childrens childrens) =>
            {
                PostUpdateCommands2.RemoveComponent<SaveEntity>(e);
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    var chunk2DEntity = childrens.children[i];
                    var tileChunk = EntityManager.GetComponentData<TileChunk>(chunk2DEntity);
                    // bytes from tileChunk.data.tiles
                    // save to file
                    string path = loadedPath.path.ToString();
                    #if UNITY_EDITOR
                    if (path.Length == 0)
                    {
                        path = UnityEditor.EditorUtility.SaveFilePanel("Save World2D", "", "planet.znk", "znk");
                    }
                    if (HasComponent<SaveAsNew>(e))
                    {
                        PostUpdateCommands2.RemoveComponent<SaveAsNew>(e);
                        path = UnityEditor.EditorUtility.SaveFilePanel("Save World2D", "", "planet.znk", "znk");
                    }
                    #endif
                    if (path.Length != 0)
                    {
                        // UnityEngine.Debug.Log("Saving World2D: " + path);
                        //var fileContent = File.ReadAllBytes(path);
                        //texture.LoadImage(fileContent);
                        var bytes = tileChunk.data.tiles.ToArray();
                        for (int j = 0; j < characterEntities.Length; j++)
                        {
                            var characterEntity = characterEntities[j];
                            var translation = translations[characterEntity];
                            var position = new int2((int)translation.Value.x, (int)translation.Value.y);
                            var arrayIndex = TileUtilities.GetTileArrayIndex(position, tileChunk.data.size);
                            bytes[arrayIndex] = 255;
                        }
                        File.WriteAllBytes(path, bytes);
                    }
                }
            })  .WithReadOnly(characterEntities)
                .WithDisposeOnCompletion(characterEntities)
                .WithReadOnly(translations)
                .WithoutBurst()
                .Run();
        }
    }*/
}