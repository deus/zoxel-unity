using Unity.Entities;
using Unity.Burst;

namespace Zoxel
{
    //! Cleans up LoadedPath with DestroyEntity.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class LoadedPathDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
            Entities
				.WithAll<DestroyEntity, LoadedPath>()
				.ForEach((in LoadedPath loadedPath) =>
			{
                loadedPath.DisposeFinal();
			}).ScheduleParallel();
		}
	}
}