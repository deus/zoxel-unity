using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Zoxel2D.Physics
{
    public struct BodyForce2D : IComponentData
    {
        public float2 acceleration;
        public float2 velocity;
        public float2 addedVelocity;

        public BodyForce2D(float2 velocity)
        {
            this.velocity = velocity;
            this.acceleration = float2.zero;
            this.addedVelocity = float2.zero;
        }
    }
}