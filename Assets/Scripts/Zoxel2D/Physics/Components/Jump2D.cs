using Unity.Entities;

namespace Zoxel.Zoxel2D.Physics
{
    public struct Jump2D : IComponentData
    {
        public float positionY;
        
        public Jump2D(float positionY)
        {
            this.positionY = positionY;
        }
    }
}