using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Zoxel2D.Physics
{
    public struct Gravity2D : IComponentData
    {
        public float2 acceleration;

        public Gravity2D(float2 acceleration)
        {
            this.acceleration = acceleration;
        }
    }
}