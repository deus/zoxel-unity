using Unity.Entities;
using Unity.Burst;
using Zoxel.Movement;

namespace Zoxel.Zoxel2D.Physics
{
    //! Adds a downward force on a 2D entity.
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class Gravity2DSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<Jump2D>()
                .WithNone<DisableForce, FlyMode, OnGround>()
                .ForEach((ref BodyForce2D bodyForce2D, in Gravity2D gravity2D) =>
            {
                bodyForce2D.acceleration += gravity2D.acceleration;
            }).ScheduleParallel();
		}
    }
}

//else // if (!HasComponent<OnGround>(e))
//{
// bodyForce2D.acceleration += gravity2D.acceleration / 2f;
//}