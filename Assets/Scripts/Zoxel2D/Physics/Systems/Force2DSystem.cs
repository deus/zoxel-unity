using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;

namespace Zoxel.Zoxel2D.Physics
{
    //! Handles 2D Forces.
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class Force2DSystem : SystemBase
    {
        const float maxDelta = 1 / 30f;
        private double lastTime;

        [BurstCompile]
        protected override void OnUpdate()
        {
            float deltaTime = World.Time.DeltaTime;
            #if UNITY_EDITOR
            if (!UnityEditor.EditorApplication.isPlaying)
            {
                deltaTime = (float) (UnityEngine.Time.realtimeSinceStartupAsDouble - lastTime);
                lastTime = UnityEngine.Time.realtimeSinceStartupAsDouble;
                // UnityEngine.Debug.Log("Time Delta: " + deltaTime + "  --  " + World.Time.DeltaTime + "  ==  " + UnityEngine.Time.deltaTime);
            }
            #endif
            if (deltaTime >= maxDelta)
            {
                deltaTime = maxDelta;
            }
            Entities
                .WithNone<DisableForce>()
                .WithChangeFilter<BodyForce2D>()
                .ForEach((ref BodyForce2D bodyForce2D, ref Translation position) =>
            {
                bodyForce2D.velocity += bodyForce2D.acceleration * deltaTime;
                bodyForce2D.addedVelocity = bodyForce2D.velocity * deltaTime;
                position.Value += new float3(bodyForce2D.velocity.x, bodyForce2D.velocity.y, 0) * deltaTime;
                bodyForce2D.acceleration.x = 0;
                bodyForce2D.acceleration.y = 0;
            }).ScheduleParallel();
            Entities
                .WithAll<DisableForce>()
                .WithChangeFilter<BodyForce2D>()
                .ForEach((ref BodyForce2D bodyForce2D) =>
            {
                bodyForce2D.velocity.x = 0;
                bodyForce2D.velocity.y = 0;
                bodyForce2D.acceleration.x = 0;
                bodyForce2D.acceleration.y = 0;
            }).ScheduleParallel();
		}
    }
}