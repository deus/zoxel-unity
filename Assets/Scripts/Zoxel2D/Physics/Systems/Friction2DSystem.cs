using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Movement;
using Zoxel.Movement.Authoring;

namespace Zoxel.Zoxel2D.Physics
{
    //! Adds counter forces to moving 2D entity.
    /**
    *   \todo Add Air Friction
    */
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class Friction2DSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<PhysicsSettings>();
        }
        
		[BurstCompile]
        protected override void OnUpdate()
        {
            var physicsSettings = GetSingleton<PhysicsSettings>();
            var airSlowMultiplier = physicsSettings.airSlowMultiplier; // 6f
            var groundFriction = physicsSettings.groundFriction;
            Dependency = Entities.ForEach((ref BodyForce2D bodyForce2D) =>
            {
                bodyForce2D.acceleration.x -= bodyForce2D.velocity.x * groundFriction;
            }).ScheduleParallel(Dependency);
            Dependency = Entities
                .WithNone<Gravity2D>()
                .ForEach((ref BodyForce2D bodyForce2D) =>
            {
                bodyForce2D.acceleration.y -= bodyForce2D.velocity.y * groundFriction;
            }).ScheduleParallel(Dependency);
		}
    }
}