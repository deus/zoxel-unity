using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Movement.Authoring;
// ToDO: Redo chunk links in this algorithm
// The problem:
//      Scales off a list of chunk renders atm
//      To get the corresponding chunk to a point I should only search from the ones connected to the character
//      i.e. inChunk, then chunkLeft, chunkRight, etc - Adjacent chunks
// todo: draw a point for each collision point

namespace Zoxel.Zoxel2D.Physics
{
    //! Handles 2D Collisions.
    [UpdateAfter(typeof(Force2DSystem))]
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class Collision2DSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunk2DQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            chunk2DQuery = GetEntityQuery(ComponentType.ReadOnly<TileChunk>());
            RequireForUpdate<PhysicsSettings>();
            RequireForUpdate(processQuery);
            RequireForUpdate(chunk2DQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var physicsSettings = GetSingleton<PhysicsSettings>();
            var bounceMultiplier = physicsSettings.bounceMultiplier;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var chunkEntities = chunk2DQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunks = GetComponentLookup<TileChunk>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DisableForce>()
                .WithChangeFilter<Translation>()
                .ForEach((Entity e, int entityInQueryIndex, ref Translation translation, ref BodyForce2D bodyForce2D, in Body2D body2D) =>
            {
                if (chunkEntities.Length == 0)
                {
                    return;
                }
                var lastVelocity = bodyForce2D.addedVelocity;
                var tileChunk = chunks[chunkEntities[0]];
                var leftSide = translation.Value.x - body2D.size.x / 2f - lastVelocity.x;
                var rightSide = translation.Value.x + body2D.size.x / 2f - lastVelocity.x;
                var bottomSide = translation.Value.y - body2D.size.y / 2f;
                var topSide = translation.Value.y + body2D.size.y / 2f;
                // normally I would add these points
                // then check if any of these points intersect with tile
                var isOnGround = false;
                if (bottomSide >= 0 && bottomSide < tileChunk.data.size.y && leftSide >= 0 && leftSide < tileChunk.data.size.x && rightSide >= 0 && rightSide < tileChunk.data.size.x)
                {
                    var pointPosition2D = new int2((int)leftSide, (int)bottomSide);
                    var tileIndex = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(pointPosition2D, tileChunk.data.size)];
                    if (tileIndex != 0)
                    {
                        isOnGround = true;
                    }
                    else
                    {
                        var pointPosition2 = new int2((int)rightSide, (int)bottomSide);
                        var tileIndex2 = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(pointPosition2, tileChunk.data.size)];
                        if (tileIndex2 != 0)
                        {
                            isOnGround = true;
                        }
                    }
                }
                var isCollideTop = false;
                if (isOnGround)
                {
                    translation.Value -= new float3(0, lastVelocity.y, 0);
                }
                // check collide top
                else if (topSide >= 0 && topSide < tileChunk.data.size.y && leftSide >= 0 && leftSide < tileChunk.data.size.x && rightSide >= 0 && rightSide < tileChunk.data.size.x)
                {
                    var pointPosition2D = new int2((int)leftSide, (int)topSide);
                    var tileIndex = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(pointPosition2D, tileChunk.data.size)];
                    if (tileIndex != 0)
                    {
                        isCollideTop = true;
                    }
                    else
                    {
                        var pointPosition2 = new int2((int)rightSide, (int)topSide);
                        var tileIndex2 = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(pointPosition2, tileChunk.data.size)];
                        if (tileIndex2 != 0)
                        {
                            isCollideTop = true;
                        }
                    }
                }
                if (isCollideTop && lastVelocity.y > 0)
                {
                    translation.Value -= new float3(0, lastVelocity.y, 0);
                    bodyForce2D.velocity.y = -bodyForce2D.velocity.y * bounceMultiplier;
                    if (HasComponent<Jump2D>(e))
                    {
                        PostUpdateCommands.RemoveComponent<Jump2D>(entityInQueryIndex, e);
                    }
                }

                leftSide = translation.Value.x - body2D.size.x / 2f;
                rightSide = translation.Value.x + body2D.size.x / 2f;
                bottomSide = translation.Value.y - body2D.size.y / 2f - lastVelocity.y;
                topSide = translation.Value.y + body2D.size.y / 2f - lastVelocity.y;
                var didCollideLeft = false;
                if (leftSide >= 0 && leftSide < tileChunk.data.size.x && bottomSide >= 0 && bottomSide < tileChunk.data.size.y && topSide >= 0 && topSide < tileChunk.data.size.y)
                {
                    var pointPosition2D = new int2((int)leftSide, (int)bottomSide);
                    var tileIndex = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(pointPosition2D, tileChunk.data.size)];
                    if (tileIndex != 0)
                    {
                        didCollideLeft = true;
                    }
                    else
                    {
                        var pointPosition2 = new int2((int)leftSide, (int)topSide);
                        var tileIndex2 = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(pointPosition2, tileChunk.data.size)];
                        if (tileIndex2 != 0)
                        {
                            didCollideLeft = true;
                            translation.Value -= new float3(lastVelocity.x, 0, 0);
                        }
                    }
                }
                if (didCollideLeft)
                {
                    translation.Value -= new float3(lastVelocity.x, 0, 0);
                }
                else if (rightSide >= 0 && rightSide < tileChunk.data.size.x && bottomSide >= 0 && bottomSide < tileChunk.data.size.y && topSide >= 0 && topSide < tileChunk.data.size.y)
                {
                    var pointPosition2D = new int2((int)rightSide, (int)bottomSide);
                    var tileIndex = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(pointPosition2D, tileChunk.data.size)];
                    if (tileIndex != 0)
                    {
                        translation.Value -= new float3(lastVelocity.x, 0, 0);
                    }
                    else
                    {
                        var pointPosition2 = new int2((int)rightSide, (int)topSide);
                        var tileIndex2 = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(pointPosition2, tileChunk.data.size)];
                        if (tileIndex2 != 0)
                        {
                            translation.Value -= new float3(lastVelocity.x, 0, 0);
                        }
                    }
                }
                if (isOnGround)
                {
                    // bottomSide = translation.Value.y - body2D.size.y / 2f; //  - 3 * lastVelocity.y;
                    if (!HasComponent<OnGround>(e))
                    {
                        if (bodyForce2D.velocity.y < 0)
                        {
                            bodyForce2D.velocity.y = -bodyForce2D.velocity.y * bounceMultiplier;
                        }
                        PostUpdateCommands.AddComponent<OnGround>(entityInQueryIndex, e);
                    }
                }
                else
                {
                    if (HasComponent<OnGround>(e))
                    {
                        PostUpdateCommands.RemoveComponent<OnGround>(entityInQueryIndex, e);
                    }
                }
            })  .WithReadOnly(chunkEntities)
                .WithDisposeOnCompletion(chunkEntities)
                .WithReadOnly(chunks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}

/*if (leftSide >= 0 && leftSide <= 16 && topSide >= 0 && topSide <= 16)
{
    var pointPosition2D = new int2((int)leftSide, (int)topSide);
    var tileIndex = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(pointPosition2D, tileChunk.data.size)];
    if (tileIndex != 0)
    {
        translation.Value += new float3(body2D.size.x / 2f, 0, 0);
    }
}
if (rightSide >= 0 && rightSide <= 16 && topSide >= 0 && topSide <= 16)
{
    var pointPosition2D = new int2((int)rightSide, (int)topSide);
    var tileIndex = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(pointPosition2D, tileChunk.data.size)];
    if (tileIndex != 0)
    {
        translation.Value -= new float3(body2D.size.x / 2f, 0, 0);
    }
}*/