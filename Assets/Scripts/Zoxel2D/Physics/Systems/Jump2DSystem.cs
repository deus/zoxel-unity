using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Movement.Authoring;

namespace Zoxel.Zoxel2D.Physics
{
    //! Handles 2D Jumping.
    [UpdateBefore(typeof(Force2DSystem))]
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class Jump2DSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<PhysicsSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var physicsSettings = GetSingleton<PhysicsSettings>();
            var jumpForce = 4 * physicsSettings.jumpForce * 0.22f;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .ForEach((Entity e, int entityInQueryIndex, ref BodyForce2D bodyForce2D, in Jump2D jump2D, in Translation translation) =>
            {
                var jumpDistance = translation.Value.y - jump2D.positionY;
                if (jumpDistance <= 0.015f)
                {
                    bodyForce2D.acceleration.y += jumpForce;
                }
                if (jumpDistance >= 0.25f)
                {
                    PostUpdateCommands.RemoveComponent<Jump2D>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}