using Unity.Entities;

namespace Zoxel.Zoxel2D
{
	//! A type of Tile Neighbor Rule.
    public static class TileNeighborRule
    {
        public const byte None = 0;
        public const byte Solid = 1;
        public const byte Air = 2;
    }
}