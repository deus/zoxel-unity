using System;

namespace Zoxel.Zoxel2D
{
    [Serializable]
    public enum TileNeighborRuleEditor
    {
        None,
        Solid,
        Air
    }
}