using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Zoxel2D
{
    //! Each tile is made up of a series of rules.
    public struct TileRuleLinks : IComponentData
    {
        public BlitableArray<Entity> rules;

        public void Dispose()
        {
            if (rules.Length > 0)
            {
                rules.Dispose();
            }
        }

        public void Initialize(int count)
        {
            rules = new BlitableArray<Entity>(count, Allocator.Persistent);
        }

        public void SetAs(int newCount)
        {
			if (rules.Length != newCount)
			{
				Dispose();
				rules = new BlitableArray<Entity>(newCount, Allocator.Persistent);
				for (int i = 0; i < newCount; i++)
				{
					rules[i] = new Entity();
				}
			}
        }
    }
}