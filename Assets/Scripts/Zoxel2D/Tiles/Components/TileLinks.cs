using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Zoxel2D
{
    //! A World2D is linked to tiles!
    public struct TileLinks : IComponentData
    {
        public BlitableArray<Entity> tiles;

        public void Dispose()
        {
            if (tiles.Length > 0)
            {
                tiles.Dispose();
            }
        }

        public void Initialize(int count)
        {
            tiles = new BlitableArray<Entity>(count, Allocator.Persistent);
        }

        public void SetAs(int newCount)
        {
			if (tiles.Length != newCount)
			{
				Dispose();
				tiles = new BlitableArray<Entity>(newCount, Allocator.Persistent);
				for (int i = 0; i < newCount; i++)
				{
					tiles[i] = new Entity();
				}
			}
        }
    }
}