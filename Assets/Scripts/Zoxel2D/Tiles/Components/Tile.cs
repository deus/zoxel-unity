using Unity.Entities;

namespace Zoxel.Zoxel2D
{
	//! A tag for a Tile entity.
    /**
    *   If a tile has no rules, just link it directly to a texture.
    */
	public struct Tile : IComponentData { }
}