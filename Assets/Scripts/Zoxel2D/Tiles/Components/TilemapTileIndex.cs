using Unity.Entities;

namespace Zoxel.Zoxel2D
{
	//! A tag for a Tile entity.
	public struct TilemapTileIndex : IComponentData
    {
        public int index;
        
        public TilemapTileIndex(int index)
        {
            this.index = index;
        }
    }
}