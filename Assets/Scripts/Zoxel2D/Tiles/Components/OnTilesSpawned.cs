using Unity.Entities;

namespace Zoxel.Zoxel2D
{
    //! Links up tiles to World2D.
    public struct OnTilesSpawned : IComponentData
    {
        public int spawned;

        public OnTilesSpawned(int spawned)
        {
            this.spawned = spawned;
        }
    }
}