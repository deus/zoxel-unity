using Unity.Entities;

namespace Zoxel.Zoxel2D
{
    //! An event to spawn tiles from the World2D entity.
    public struct SpawnTiles : IComponentData { }
}