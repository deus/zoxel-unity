using Unity.Entities;

namespace Zoxel.Zoxel2D
{
    //! Links up tiles to World2D.
    public struct OnTileRulesSpawned : IComponentData
    {
        public int spawned;

        public OnTileRulesSpawned(int spawned)
        {
            this.spawned = spawned;
        }
    }
}