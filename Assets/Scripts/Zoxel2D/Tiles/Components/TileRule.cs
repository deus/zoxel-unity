using Unity.Entities;

namespace Zoxel.Zoxel2D
{
	//! A tag for a TileRule entity.
    /**
    *   Each rule links to a tile texture and contains neirby neighbors.
    *   Each neighbor is TileRuleType type.
    */
	public struct TileRule : IComponentData
    {
        public byte tileLeft;
        public byte tileRight;
        public byte tileDown;
        public byte tileUp;
        public byte tileDownLeft;
        public byte tileDownRight;
        public byte tileUpLeft;
        public byte tileUpRight;

        public TileRule(TileRuleEditor rule)
        {
            tileLeft = (byte) rule.tileLeft;
            tileRight = (byte) rule.tileRight;
            tileDown = (byte) rule.tileDown;
            tileUp = (byte) rule.tileUp;
            tileDownLeft = (byte) rule.tileDownLeft;
            tileDownRight = (byte) rule.tileDownRight;
            tileUpLeft = (byte) rule.tileUpLeft;
            tileUpRight = (byte) rule.tileUpRight;
        }

        public bool DoesRulePass(byte tileLeft, byte tileRight, byte tileDown, byte tileUp, byte tileDownLeft, byte tileDownRight, byte tileUpLeft, byte tileUpRight)
        {
            // check if fails?
            if ((this.tileLeft == TileNeighborRule.None || this.tileLeft == TileNeighborRule.Solid && tileLeft != 0 || this.tileLeft == TileNeighborRule.Air && tileLeft == 0)
                && (this.tileRight == TileNeighborRule.None || this.tileRight == TileNeighborRule.Solid && tileRight != 0 || this.tileRight == TileNeighborRule.Air && tileRight == 0)
                && (this.tileUp == TileNeighborRule.None || this.tileUp == TileNeighborRule.Solid && tileUp != 0 || this.tileUp == TileNeighborRule.Air && tileUp == 0))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}