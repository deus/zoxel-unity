using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Zoxel2D
{
    //! Cleans up TileLinks's chunks.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class TileLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithAll<DestroyEntity, TileLinks>()
				.ForEach((int entityInQueryIndex, in TileLinks tileLinks) =>
			{
				for (int i = 0; i < tileLinks.tiles.Length; i++)
				{
					var e = tileLinks.tiles[i];
					if (HasComponent<Tile>(e))
					{
						PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e);
					}
				}
				tileLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}