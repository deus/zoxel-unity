using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Textures;

namespace Zoxel.Zoxel2D
{
    //! Links up tiles to World2D.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class TilesSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery tilesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            tilesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Tile>(),
                ComponentType.ReadOnly<World2DLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var tileEntities = tilesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var world2DLinks = GetComponentLookup<World2DLink>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref TileLinks tileLinks, ref OnTilesSpawned onTilesSpawned) =>
            {
                //UnityEngine.Debug.LogError("Tiles Connected 3: " +onTilesSpawned.spawned);
                if (onTilesSpawned.spawned != 0)
                {
                    tileLinks.SetAs(onTilesSpawned.spawned);
                    onTilesSpawned.spawned = 0;
                }
                if (tileLinks.tiles.Length == 0)
                {
                    //UnityEngine.Debug.LogError("Tiles Connected: " + tileLinks.tiles.Length);
                    PostUpdateCommands.RemoveComponent<OnTilesSpawned>(entityInQueryIndex, e);
                    return;
                }
                var count = 0;
                for (int i = 0; i < tileEntities.Length; i++)
                {
                    var e2 = tileEntities[i];
                    var world2DLink = world2DLinks[e2];
                    if (world2DLink.world2D == e)
                    {
                        tileLinks.tiles[count] = e2;
                        count++;
                        if (count >= tileLinks.tiles.Length)
                        {
                            break;
                        }
                    }
                }
                if (count == tileLinks.tiles.Length)
                {
                    //UnityEngine.Debug.LogError("Tiles Connected: " + tileLinks.tiles.Length);
                    PostUpdateCommands.RemoveComponent<OnTilesSpawned>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<GenerateTilemap>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(tileEntities)
                .WithDisposeOnCompletion(tileEntities)
                .WithReadOnly(world2DLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
} 
