using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Textures;
using Zoxel.Textures.Tilemaps;

namespace Zoxel.Zoxel2D
{
    //! Converts a bunch of Entity's Texture's into a Tilemap.
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class TilesTilemapGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery tilesQuery;
        private EntityQuery tileRuleQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            tilesQuery = GetEntityQuery(
				ComponentType.ReadOnly<Tile>(),
				ComponentType.ReadOnly<Texture>(),
                ComponentType.ReadWrite<TilemapTileIndex>(),
				ComponentType.ReadOnly<TileRuleLinks>(),
				ComponentType.Exclude<DestroyEntity>());
            tileRuleQuery = GetEntityQuery(
				ComponentType.ReadOnly<TileRule>(),
				ComponentType.ReadOnly<Texture>(),
                ComponentType.ReadWrite<TilemapTileIndex>(),
				ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var tileEntities = tilesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var textures2 = GetComponentLookup<Texture>(true);
            var tileRuleLinks = GetComponentLookup<TileRuleLinks>(true);
            var tileTilemapIndexes = GetComponentLookup<TilemapTileIndex>(false);
            var tileRuleEntities = tileRuleQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            tileEntities.Dispose();
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var textures3 = GetComponentLookup<Texture>(true);
            var ruleTilemapIndexes = GetComponentLookup<TilemapTileIndex>(false);
            tileRuleEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<GenerateTilemap, World2D>()
                .ForEach((Entity e, int entityInQueryIndex, ref Texture texture, ref Tilemap tilemap, in TileLinks tileLinks) =>
            {
                if (tileLinks.tiles.Length == 0)
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<GenerateTilemap>(entityInQueryIndex, e);
                var textures = new NativeList<Texture>();
                for (int i = 0; i < tileLinks.tiles.Length; i++)
                {
                    var e2 = tileLinks.tiles[i];
                    textures.Add(textures2[e2]);
                    var tileIndex = tileTilemapIndexes[e2];
                    tileIndex.index = textures.Length - 1;
                    tileTilemapIndexes[e2] = tileIndex;
                    // add rule textures too.
                    var tileRuleLinks2 = tileRuleLinks[e2];
                    for (int j = 0; j < tileRuleLinks2.rules.Length; j++)
                    {
                        var e3 = tileRuleLinks2.rules[j];
                        textures.Add(textures3[e3]);
                        tileIndex = ruleTilemapIndexes[e3];
                        tileIndex.index = textures.Length - 1;
                        ruleTilemapIndexes[e3] = tileIndex;
                    }
                }
                var largestSize = new float2();
                for (int i = 0; i < textures.Length; i++)
                {
                    var newTexture = textures[i];
                    var newSize = new float2(newTexture.size.x, newTexture.size.y);
                    if (newSize.x > largestSize.x)
                    {
                        largestSize.x = newSize.x;
                    }
                    if (newSize.y > largestSize.y)
                    {
                        largestSize.y = newSize.y;
                    }
                }
                var texturesCount = textures.Length;
                // Generate Texture Tilemap
                // find next power of 2
                tilemap.gridSize = 0;
                int squared = 1;
                while (squared < texturesCount)
                {
                    tilemap.gridSize++;
                    squared = tilemap.gridSize * tilemap.gridSize;
                }
                if (texturesCount == 1)
                {
                    tilemap.gridSize = 1;
                }
                // for all textures set horizontalCount, verticalCount
                CreateTileTilemap(ref texture, in textures, tilemap.gridSize, (int) largestSize.x);
                // Now create UVs
                textures.Dispose();
            })  .WithReadOnly(textures2)
                .WithNativeDisableContainerSafetyRestriction(textures2)
                .WithNativeDisableContainerSafetyRestriction(tileTilemapIndexes)
                .WithReadOnly(tileRuleLinks)
                .WithReadOnly(textures3)
                .WithNativeDisableContainerSafetyRestriction(textures3)
                .WithNativeDisableContainerSafetyRestriction(ruleTilemapIndexes)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
        
        public static void CreateTileTilemap(ref Texture tilmapTexture, in NativeList<Texture> textures, int gridSize, int textureSize = 16, bool hasAlpha = false)
        {
            if (textures.Length == 0)
            {
                //Debug.LogError("tiledTextures count is 0. Cannot create a tile planet.");
                return;
            }
            var tilemapWidth = textureSize * gridSize;
            var tilemapHeight = textureSize * gridSize;
            if (tilemapWidth == 0 || tilemapHeight == 0)
            {
                // UnityEngine.Debug.LogError("Tilemap size.x or size.y is 0.");
                return;
            }
            tilmapTexture.Initialize(new int2(tilemapWidth, tilemapHeight));
            var tileIndex = -1;                     // Our real index !
            var maxTiles = gridSize * gridSize;
            for (int i = 0; i < textures.Length; i++)
            {
                tileIndex++;
                var texture = textures[i];
                if (i < maxTiles && texture.size.x != 0 && texture.size.y != 0)
                {
                    var positionX = (tileIndex / gridSize);
                    var positionY = (tileIndex % gridSize);
                    for (int j = 0; j < textureSize; j++)
                    {
                        for (int k = 0; k < textureSize; k++)
                        {
                            var j2 = j + positionX * (textureSize);
                            var k2 = k + positionY * (textureSize);
                            var tileColorIndex = (int) math.floor(j * textureSize + k);
                            if (tileColorIndex < texture.colors.Length)
                            {
                                var color = texture.colors[tileColorIndex];
                                var tilemapColorIndex = (int) math.floor(j2 * gridSize * textureSize + k2);
                                tilmapTexture.colors[tilemapColorIndex] = color;
                            }
                        }
                    }
                }
            }
        }
    }
}