using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Zoxel2D
{
    //! Links up rules to World2D.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class TileRulesSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery tilesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            tilesQuery = GetEntityQuery(
                ComponentType.ReadOnly<TileRule>(),
                ComponentType.ReadOnly<CreatorLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var tileEntities = tilesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var creatorLinks = GetComponentLookup<CreatorLink>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref TileRuleLinks tileRuleLinks, ref OnTileRulesSpawned onTileRulesSpawned) =>
            {
                //UnityEngine.Debug.LogError("Tiles Connected 3: " + onTileRulesSpawned.spawned);
                if (onTileRulesSpawned.spawned != 0)
                {
                    tileRuleLinks.SetAs(onTileRulesSpawned.spawned);
                    onTileRulesSpawned.spawned = 0;
                }
                if (tileRuleLinks.rules.Length == 0)
                {
                    //UnityEngine.Debug.LogError("Tiles Connected: " + tileRuleLinks.rules.Length);
                    PostUpdateCommands.RemoveComponent<OnTileRulesSpawned>(entityInQueryIndex, e);
                    return;
                }
                var count = 0;
                for (int i = 0; i < tileEntities.Length; i++)
                {
                    var e2 = tileEntities[i];
                    var creatorLink = creatorLinks[e2];
                    if (creatorLink.creator == e)
                    {
                        tileRuleLinks.rules[count] = e2;
                        count++;
                        if (count >= tileRuleLinks.rules.Length)
                        {
                            break;
                        }
                    }
                }
                if (count == tileRuleLinks.rules.Length)
                {
                    //UnityEngine.Debug.LogError("Tiles Connected: " + tileRuleLinks.rules.Length);
                    PostUpdateCommands.RemoveComponent<OnTileRulesSpawned>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(tileEntities)
                .WithDisposeOnCompletion(tileEntities)
                .WithReadOnly(creatorLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
} 
