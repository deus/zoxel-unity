using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Textures;

namespace Zoxel.Zoxel2D
{
    //! Spawns a Tile entities.
    /**
    *   - Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class TilesSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity tilePrefab;
        public static Entity tileRulePrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var tileArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(OnTileRulesSpawned),
                typeof(ZoxID),
                typeof(Tile),
                typeof(TileRuleLinks),
                typeof(Texture),
                typeof(TilemapTileIndex),
                typeof(World2DLink)
            );
            tilePrefab = EntityManager.CreateEntity(tileArchetype);
            var tileRuleArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(CreatorLink),
                typeof(TileRule),
                typeof(Texture),
                typeof(TilemapTileIndex)
            );
            tileRulePrefab = EntityManager.CreateEntity(tileRuleArchetype);
        }

        public struct TileSpawnImport
        {
            public Texture defaultTexture;
            public BlitableArray<TileRule> rules;
            public BlitableArray<Texture> textures;

            public void Dispose()
            {
                rules.Dispose();
                textures.Dispose();
            }
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (TileManager.instance == null) return;
            var tileDatams = TileManager.instance.TileSettings.tiles;
            var tilesCount = TileManager.instance.TileSettings.tiles.Length;
            // Pass in tiles
            // Pass in Rules
            //! \todo This will break if I import more!
            var spawnImports = new NativeArray<TileSpawnImport>(tilesCount, Allocator.TempJob);
            for (int i = 0; i < tileDatams.Length; i++)
            {
                var tileDatam = tileDatams[i];
                var import = new TileSpawnImport();
                import.defaultTexture = new Texture(in tileDatam.texture);
                if (tileDatam.tileRules != null)
                {
                    import.rules = new BlitableArray<TileRule>(tileDatam.tileRules.Length, Allocator.TempJob);
                    import.textures = new BlitableArray<Texture>(tileDatam.tileRules.Length, Allocator.TempJob);    // Will TempJob be disposed automatically?
                    for (int j = 0; j < tileDatam.tileRules.Length; j++)
                    {
                        import.rules[j] = new TileRule(tileDatam.tileRules[j]);
                        import.textures[j] = new Texture(tileDatam.tileRules[j].texture);
                    }
                }
                spawnImports[i] = import;
            }
            // UnityEngine.Debug.LogError("Tiles Spawned: " + tilesCount);
            var tilePrefab = TilesSpawnSystem.tilePrefab;
            var tileRulePrefab = TilesSpawnSystem.tileRulePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<SpawnTiles>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnTiles>(entityInQueryIndex, e);
                // UnityEngine.Debug.LogError("Tiles Spawned: " + tilesCount);
                for (int i = 0; i < tilesCount; i++)
                {
                    var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, tilePrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new World2DLink(e));
                    var spawnImport = spawnImports[i];
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, spawnImport.defaultTexture);
                    for (int j = 0; j < spawnImport.textures.Length; j++)
                    {
                        var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, tileRulePrefab);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CreatorLink(e2));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, spawnImport.rules[j]);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, spawnImport.textures[j]);
                    }
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new OnTileRulesSpawned(spawnImport.textures.Length));
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnTilesSpawned(tilesCount));

                for (int i = 0; i < tilesCount; i++)
                {
                    var spawnImport = spawnImports[i];
                    spawnImport.Dispose();
                }
            })  .WithReadOnly(spawnImports)
                .WithDisposeOnCompletion(spawnImports)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}