using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Zoxel2D
{
    //! Cleans up TileRuleLinks's rules.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class TileRuleLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithAll<DestroyEntity, TileRuleLinks>()
				.ForEach((int entityInQueryIndex, in TileRuleLinks tileRuleLinks) =>
			{
				for (int i = 0; i < tileRuleLinks.rules.Length; i++)
				{
					var e = tileRuleLinks.rules[i];
					if (HasComponent<TileRule>(e))
					{
						PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e);
					}
				}
				tileRuleLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}