using UnityEngine;

namespace Zoxel.Zoxel2D
{
    //! A tile data in the editor.
    [CreateAssetMenu(fileName = "Tile", menuName = "ZoxelData/Tile")]
    public partial class TileDatam : ScriptableObject
    {
        public Texture2D texture;
        public TileRuleEditor[] tileRules;
    }
}