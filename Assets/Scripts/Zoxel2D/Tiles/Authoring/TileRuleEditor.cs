using UnityEngine;
using System;

namespace Zoxel.Zoxel2D
{
    [Serializable]
    public struct TileRuleEditor
    {
        public Texture2D texture;
        public TileNeighborRuleEditor tileLeft;
        public TileNeighborRuleEditor tileRight;
        public TileNeighborRuleEditor tileDown;
        public TileNeighborRuleEditor tileUp;
        public TileNeighborRuleEditor tileDownLeft;
        public TileNeighborRuleEditor tileDownRight;
        public TileNeighborRuleEditor tileUpLeft;
        public TileNeighborRuleEditor tileUpRight;
    }
}