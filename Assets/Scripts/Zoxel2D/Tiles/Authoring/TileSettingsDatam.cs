using UnityEngine;

namespace Zoxel.Zoxel2D
{
    [CreateAssetMenu(fileName = "TileSettings", menuName = "ZoxelSettings/TileSettings")]
    public partial class TileSettingsDatam : ScriptableObject
    {
        public TileDatam[] tiles;
    }
}