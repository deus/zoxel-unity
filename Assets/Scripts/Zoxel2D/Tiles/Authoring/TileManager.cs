using Unity.Mathematics;
using UnityEngine;
using System.Collections.Generic;

namespace Zoxel.Zoxel2D
{
    //! Manages TileSettings.
    public partial class TileManager : MonoBehaviour
    {
        public static TileManager instance;
        [SerializeField] public TileSettingsDatam TileSettings;
        // [HideInInspector] public TileSettings realTileSettings;
        // GUI
        private int fontSize = 26;
        private UnityEngine.Color fontColor = UnityEngine.Color.green;

        /*public TileSettings tileSettings
        {
            get
            { 
                return realTileSettings;
            }
        }*/

        public void Awake()
        {
            instance = this;
            Initialize();
        }

        public void Initialize()
        {
            // realTileSettings = TileSettings.tileSettings;
        }

        public void OnGUI()
        {
            GUI.skin.label.fontSize = fontSize;
            GUI.color = fontColor;
            DrawUI();
        }

        public void DrawUI()
        {
            //realTileSettings.DrawUI();
            //realTileSettings.DrawDisableUI();
        }
    }
}