using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Zoxel2D
{
    public struct Target2D : IComponentData
    {
        public Entity target;
        public float2 targetPosition;
        public float targetDistance;

        public Target2D(Entity target)
        {
            this.target = target;
            this.targetPosition = new float2();
            this.targetDistance = 0;
        }
    }
}