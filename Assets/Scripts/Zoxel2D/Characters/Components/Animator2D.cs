using Unity.Entities;

namespace Zoxel.Zoxel2D
{
    public struct Animator2D : IComponentData
    {
        public byte count;
        public double lastSwitched;
        public int direction;
    }
}