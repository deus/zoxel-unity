using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Zoxel2D
{
    public struct SpawnCharacter2D : IComponentData
    {
        public byte isSpawnPlayer;
        public float3 spawnPosition;
        public Entity cameraEntity;
    }
}