using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Zoxel2D
{
    //! Clears all Character2D entities.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class DestroyAllCharacter2DsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery charactersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            charactersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Character2D>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var characterEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<DestroyAllCharacter2Ds>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                for (int i = 0; i < characterEntities.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, characterEntities[i]);
                }
            })  .WithReadOnly(characterEntities)
                .WithDisposeOnCompletion(characterEntities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}