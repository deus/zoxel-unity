using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.UI;
using Zoxel.Cameras;
using Zoxel.Rendering;
using Zoxel.Input;
using Zoxel.Zoxel2D.Physics;
using Zoxel.Movement.Authoring;

namespace Zoxel.Zoxel2D
{
    //! Spawns 2D characters.
    /**
    *   - Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class Character2DSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity platformerCharacter2DPrefab;
        private Entity playerPlatformerCharacter2DPrefab;
        private Entity character2DPrefab;
        private Entity playerCharacter2DPrefab;
        private EntityQuery cameraQuery;
        private EntityQuery keyboardsQuery;
        private EntityQuery mousesQuery;
        public static Entity spawnCharacter2DPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var character2DArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(Character2D),
                typeof(Animator2D),
                typeof(ZoxID),
                typeof(Body2D),
                typeof(BodyForce2D),
                typeof(Gravity2D),
                typeof(Target2D),
                typeof(UILink),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(NonUniformScale),
                typeof(DontDestroyTexture),
                typeof(ZoxMesh)
            );
            platformerCharacter2DPrefab = EntityManager.CreateEntity(character2DArchetype);
            playerPlatformerCharacter2DPrefab = EntityManager.Instantiate(platformerCharacter2DPrefab);
            EntityManager.AddComponent<Prefab>(playerPlatformerCharacter2DPrefab);
            EntityManager.AddComponent<Controller>(playerPlatformerCharacter2DPrefab);
            EntityManager.AddComponent<DeviceTypeData>(playerPlatformerCharacter2DPrefab);
            EntityManager.AddComponent<CameraLink>(playerPlatformerCharacter2DPrefab);
            EntityManager.AddComponent<ConnectedDevices>(playerPlatformerCharacter2DPrefab);
            // without gravity2D
            character2DPrefab = EntityManager.Instantiate(platformerCharacter2DPrefab);
            EntityManager.AddComponent<Prefab>(character2DPrefab);
            EntityManager.RemoveComponent<Gravity2D>(character2DPrefab);
            playerCharacter2DPrefab = EntityManager.Instantiate(playerPlatformerCharacter2DPrefab);
            EntityManager.AddComponent<Prefab>(playerCharacter2DPrefab);
            EntityManager.RemoveComponent<Gravity2D>(playerCharacter2DPrefab);
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnCharacter2D),
                typeof(SpawnEntity),
                typeof(DelayEvent));
            spawnCharacter2DPrefab = EntityManager.CreateEntity(spawnArchetype);
            cameraQuery = GetEntityQuery(
                ComponentType.ReadOnly<Camera2D>(),
                ComponentType.ReadOnly<FollowCamera2D>(),
                ComponentType.Exclude<DestroyEntity>());
            keyboardsQuery = GetEntityQuery(ComponentType.ReadOnly<Keyboard>());
            mousesQuery = GetEntityQuery(ComponentType.ReadOnly<Mouse>());
            RequireForUpdate<PhysicsSettings>();
        }

        public static void SpawnCharacter2D(EntityManager EntityManager, float3 spawnPosition, double timeStarted, double timeDelay)
        {
            var spawnEntity = EntityManager.Instantiate(spawnCharacter2DPrefab);
            EntityManager.SetComponentData(spawnEntity, new SpawnCharacter2D { spawnPosition = spawnPosition });
            EntityManager.SetComponentData(spawnEntity, new DelayEvent(timeStarted, timeDelay));
        }

        public static void SpawnPlayer2D(EntityManager EntityManager, float3 spawnPosition)
        {
            var spawnEntity = EntityManager.Instantiate(spawnCharacter2DPrefab);
            EntityManager.SetComponentData(spawnEntity, new SpawnCharacter2D
            { 
                isSpawnPlayer = 1,
                spawnPosition = spawnPosition
            });
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (Bootstrap2D.instance == null)
            {
                return;
            }
            var physicsSettings = GetSingleton<PhysicsSettings>();
            var gravity = new float2(0, -physicsSettings.fallSpeed / 2f);
            // var deviceID = UnityEngine.InputSystem.Keyboard.current.deviceId;
            var hasGravity2D = Bootstrap2D.instance.hasGravity2D;
            var character2DPrefab = this.character2DPrefab;
            var playerCharacter2DPrefab = this.playerCharacter2DPrefab;
            if (hasGravity2D)
            {
                character2DPrefab = this.platformerCharacter2DPrefab;
                playerCharacter2DPrefab = this.playerPlatformerCharacter2DPrefab;
            }
            //UnityEngine.Debug.LogError("cameraEntity: " + cameraEntity.Index);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var cameraEntities = cameraQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var keyboardEntities = keyboardsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var mouseEntities = mousesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            
            Dependency = Entities
                .WithNone<DelayEvent>()
                .WithAll<SpawnEntity>()
                .ForEach((Entity e, int entityInQueryIndex, in SpawnCharacter2D spawnCharacter2D) =>
            {
                Entity character2DEntity;
                if (spawnCharacter2D.isSpawnPlayer == 0)
                {
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                    character2DEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, character2DPrefab);
                    // rotate it around
                }
                else
                {
                    if (cameraEntities.Length == 0 || keyboardEntities.Length == 0)
                    {
                        return;
                    }
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                    character2DEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, playerCharacter2DPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, character2DEntity, new Controller(ControllerMapping.InGame));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, character2DEntity, new DeviceTypeData(DeviceType.Keyboard));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, character2DEntity, new CameraLink(cameraEntities[0]));
                    var connectedDevices = new ConnectedDevices();
                    connectedDevices.Add(keyboardEntities[0]);
                    connectedDevices.Add(mouseEntities[0]);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, character2DEntity, connectedDevices);
                    for (int i = 0; i < connectedDevices.devices.Length; i++)
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, connectedDevices.devices[i], new DeviceConnected(character2DEntity));
                    }
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, character2DEntity, new Translation { Value = spawnCharacter2D.spawnPosition });
                PostUpdateCommands.SetComponent(entityInQueryIndex, character2DEntity, new Body2D(new float2(0.94f, 0.94f)));
                PostUpdateCommands.SetComponent(entityInQueryIndex, character2DEntity, new NonUniformScale { Value = new float3(1, 1, 1) });
                if (hasGravity2D)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, character2DEntity, new Gravity2D(gravity));
                }
            })  .WithReadOnly(cameraEntities)
                .WithDisposeOnCompletion(cameraEntities)
                .WithReadOnly(keyboardEntities)
                .WithDisposeOnCompletion(keyboardEntities)
                .WithReadOnly(mouseEntities)
                .WithDisposeOnCompletion(mouseEntities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}