using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Rendering;
using Zoxel.Input;

namespace Zoxel.Zoxel2D
{
    //! Initializes character 2Ds.
    [UpdateInGroup(typeof(TileSystemGroup))]
    public partial class Character2DInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null) return;
            var character2DMaterials = MaterialsManager.instance.materials.character2DMaterials;
            var playerCharacter2DMaterial = MaterialsManager.instance.materials.playerCharacter2DMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<Character2D, InitializeEntity>()
                .ForEach((Entity e, ZoxMesh zoxMesh, ref ZoxID zoxID) =>
            {
                zoxID.id = IDUtil.GenerateUniqueID();
                zoxMesh.mesh = MeshUtilities.CreateQuadMesh(new float2(1, 1));
                if (HasComponent<Controller>(e))
                {
                    zoxMesh.material = new UnityEngine.Material(playerCharacter2DMaterial);
                    // todo: Spawn Follow Camera on this character
                }
                else
                {
                    var random = new Random();
                    random.InitState((uint)zoxID.id);
                    zoxMesh.material = new UnityEngine.Material(character2DMaterials[random.NextInt(0, character2DMaterials.Length)]);
                }
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
                #if UNITY_EDITOR
                if (!UnityEditor.EditorApplication.isPlaying && ZoxelEditorSettings.isEditorMeshes)
                {
                    PostUpdateCommands.AddComponent<InitializeEditorMesh>(e);
                    PostUpdateCommands.AddComponent<EditorMesh>(e);
                }
                #endif
            }).WithoutBurst().Run();
        }
    }
}