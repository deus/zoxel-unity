using Unity.Entities;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Zoxel2D
{
    //! Renders character 2D's with Graphics.DrawMesh.
    /**
    *   - Render System -
    */
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public partial class Character2DRenderSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithNone<EditorMesh>()
                .WithAll<Character2D>()
                .ForEach((Entity e, in ZoxMesh zoxMesh, in LocalToWorld localToWorld) =>
			{
                UnityEngine.Graphics.DrawMesh(zoxMesh.mesh, localToWorld.Value, zoxMesh.material, 0, null);
			}).WithoutBurst().Run();
        }
    }
}