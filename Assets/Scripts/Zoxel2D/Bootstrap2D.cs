using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Zoxel2D;

namespace Zoxel
{
    //! Boots up a 2D Realm!
    /**
    *   Tiles
    *       \todo Add a Tile2DDatam that I can add each texture to.
    *       \todo An easy way I can edit tilsets for one tile type
    *       \todo Generate Tilemap for all tiles
    *       \todo Tile layers - with a button
    *       \todo tiles on the bottom (grass parts)
    *       \todo Multiple TileChunks
    *   Editor
    *       \todo UI system - Orbiting - to work in editor
    *       \todo Show what Tool I am using as a UI - Brush - Erase - Place NPC - Player
    *       \todo Place Player Spawn Point - Show as well as a rotating pixel portal
    *       \todo Grid Renderer - create grid around Tiles
    *       \todo Load Button - open a map to load it
    *       \todo Close Map Button - close current loaded map
    *       \todo Save Button - Saves current opened map
    *   Characters
    *       \todo Add Character2D datam with character sprites/tilemap connected
    *       \todo character animations?
    *   Dialogue
    *       \todo link character2D to dialogue file
    *       \todo dialogue editor
    *   More
    *       \todo interact with characters with dialogue button - emoticon above them
    *       \todo dialogue - black bars on top and bottom and camera zooms in
    *       \todo Door to enter - leads to other levels
    */
    public partial class Bootstrap2D : UnityEngine.MonoBehaviour
    {
        public static Bootstrap2D instance;
        public bool hasGravity2D;
        public bool isStartAsEditor;
        public bool isProcedural;
        public string localPath;
        public string loadPath;

        public EntityManager EntityManager
        {
            get
            {
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        public void Awake()
        {
            instance = this;
            var isHeadlessMode = NetworkUtil.IsHeadless();
            if (isHeadlessMode)
            {
                return;
            }
            // spawn 2D scene
            if (isStartAsEditor)
            {
                Camera2DSpawnSystem.SpawnEditorCamera2D(EntityManager);
            }
            else
            {
                Camera2DSpawnSystem.SpawnFollowCamera2D(EntityManager);
                Character2DSpawnSystem.SpawnPlayer2D(EntityManager,  new float3(8.5f + 120f, 5 + 8.5f, 0));
            }
            // make it load in file too
            if (!isProcedural) // loadPath != "")
            {
                var loadPath2 = "";
                #if UNITY_EDITOR
                loadPath2 = loadPath;
                #else
                loadPath2 = UnityEngine.Application.dataPath + localPath;
                // "/maps/map3.znk";
                #endif
                UnityEngine.Debug.Log("Loading Planet: " + loadPath2);
                var loadPathText = new Text(loadPath2);
                World2DSpawnSystem.SpawnWorld2D(EntityManager, loadPathText);
            }
            else
            {
                World2DSpawnSystem.SpawnWorld2D(EntityManager, new Text());
                for (int i = 0; i <= 24; i++)
                {
                    var spawnPosition = new float3(i * 5 + 10 + 120f, 8.5f, 0);
                    Character2DSpawnSystem.SpawnCharacter2D(EntityManager, spawnPosition, 0, UnityEngine.Random.Range(3f, 10f));
                }
                for (int i = 0; i <= 24; i++)
                {
                    var spawnPosition = new float3(-i * 5 + 10 + 120f, 8.5f, 0);
                    Character2DSpawnSystem.SpawnCharacter2D(EntityManager, spawnPosition, 0, UnityEngine.Random.Range(3f, 10f));
                }
            }
        }

        [UnityEngine.ContextMenu("Chose Planet")]
        public void ChosePlanet()
        {
            #if UNITY_EDITOR
            loadPath = UnityEditor.EditorUtility.OpenFilePanel(
                "Load World2D",
                "",
                "znk");
            #endif
        }

        [UnityEngine.ContextMenu("Set Local Planet")]
        public void SetLocalPlanet()
        {
            #if UNITY_EDITOR
            loadPath = UnityEngine.Application.dataPath + "/maps/map3.znk";
            #endif
        }

        /*[UnityEngine.ContextMenu("Set Procedural Planet")]
        public void SetProceduralPlanet()
        {
            loadPath = "";
        }*/
    }
}

// done:
// todo: Place Characters with editor
// todo: save and load characters in planet
// todo: DestroyCharacter2DSystem
// todo: load znk file from games folder - [gamefolder]/content/maps/map3.znk
// todo: tile collision with bottom