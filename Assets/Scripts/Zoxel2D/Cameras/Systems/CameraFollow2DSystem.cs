using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using Zoxel.Transforms;
using Zoxel.Movement;
using UnityEngine.InputSystem;
using Zoxel.Players;
using Zoxel.Input;

namespace Zoxel.Zoxel2D
{
    //! Camera 2D to follow the Character2D.
    /**
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class CameraFollow2DSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands  = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<Controller, Character2D>()
                .ForEach((Entity e, in Translation translation, in CameraLink cameraLink) =>
            {
                //var cameraPosition = UnityEngine.Camera.main.transform.position;
                if (HasComponent<Camera2D>(cameraLink.camera))
                {
                    var cameraPosition = EntityManager.GetComponentData<Translation>(cameraLink.camera);
                    cameraPosition.Value.x = translation.Value.x;
                    cameraPosition.Value.y = translation.Value.y;
                    PostUpdateCommands.SetComponent(cameraLink.camera, cameraPosition);
                }
                //UnityEngine.Camera.main.transform.position = cameraPosition;
            }).WithoutBurst().Run();
        }
    }
}