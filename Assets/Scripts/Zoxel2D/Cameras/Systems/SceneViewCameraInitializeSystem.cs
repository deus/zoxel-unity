#if UNITY_EDITOR
using Unity.Entities;
using Zoxel.Transforms;

namespace Zoxel.Zoxel2D
{
    //! Links SceneView camera gameobject to entity.
    [UpdateInGroup(typeof(TileSystemGroup))]
    public partial class SceneViewCameraInitializeSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, EditorCamera>()
                .ForEach((Entity e, in SceneViewLink sceneViewLink) =>
            {
                var sceneView = sceneViewLink.sceneView;
                if (sceneView != null && sceneView.camera != null)
                {
                    PostUpdateCommands.SetSharedComponentManaged(e, new GameObjectLink(sceneView.camera.gameObject));
                    // UnityEngine.Debug.LogError("Scene Camera Added: " + sceneViewCameraObject.name);
                }
            }).WithoutBurst().Run();
		}
    }
}
#endif