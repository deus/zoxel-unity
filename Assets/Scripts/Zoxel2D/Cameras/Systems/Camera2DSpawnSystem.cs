using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Collections;
using UnityEngine.InputSystem;
using System;
using Zoxel.Transforms;
using Zoxel.Cameras;
using Zoxel.Players;
using Zoxel.Input;

namespace Zoxel.Zoxel2D
{
    //! Spawns follower cameras.
    /**
    *   - Spawn System -
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class Camera2DSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity camera2DPrefab;
        private Entity editorCamera2DPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var camera2DArchetype = EntityManager.CreateArchetype(
                // commands
                typeof(Prefab),
                typeof(ZoxID),
                //typeof(Camera),
                typeof(Camera2D),
                typeof(CharacterLink),
                typeof(World2DLink),
                // typeof(FollowCamera2D),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(GameObjectLink)
            );
            camera2DPrefab = EntityManager.CreateEntity(camera2DArchetype);
            editorCamera2DPrefab = EntityManager.Instantiate(camera2DPrefab);
            // editor camera
            EntityManager.AddComponent<Prefab>(editorCamera2DPrefab);
            EntityManager.AddComponent<Controller>(editorCamera2DPrefab);
            EntityManager.AddComponent<TileEditorCamera>(editorCamera2DPrefab);
            EntityManager.AddComponent<TileEditor>(editorCamera2DPrefab);
            EntityManager.AddComponent<World2DLink>(editorCamera2DPrefab);
            // follow camera
            EntityManager.AddComponent<FollowCamera2D>(camera2DPrefab);
        }

        public static void SpawnEditorCamera2D(EntityManager EntityManager)
        {
            var spawnArchetype = EntityManager.CreateArchetype(typeof(SpawnCamera2D));
            var spawnEntity = EntityManager.CreateEntity(spawnArchetype);
            EntityManager.SetComponentData(spawnEntity, new SpawnCamera2D { isEditor = 1 });
        }

        public static void SpawnFollowCamera2D(EntityManager EntityManager)
        {
            var spawnArchetype = EntityManager.CreateArchetype(typeof(SpawnCamera2D));
            EntityManager.CreateEntity(spawnArchetype);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (CameraManager.instance == null) return;
            // var deviceID = UnityEngine.InputSystem.Keyboard.current.deviceId;
            var editorCamera2DPrefab2 = editorCamera2DPrefab;
            var camera2DPrefab2 = camera2DPrefab;
            var mainMenuBackgroundColor = CameraManager.instance.cameraSettings.mainMenuBackgroundColor;
            var cameraGameObjectPrefab = CameraManager.instance.CameraSettings.playerCameraPrefab;
            if (cameraGameObjectPrefab.activeSelf)
            {
                cameraGameObjectPrefab.SetActive(false);
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<SpawnCamera2D>()
                .ForEach((Entity e, in SpawnCamera2D spawnCamera2D) =>
            {
                PostUpdateCommands.DestroyEntity(e);
                var id = IDUtil.GenerateUniqueID(); 
                var cameraObject = UnityEngine.GameObject.Instantiate(cameraGameObjectPrefab);
                cameraObject.name = "Camera[" + id + "]";
                cameraObject.SetActive(true);
                cameraObject.GetComponent<UnityEngine.Camera>().backgroundColor = mainMenuBackgroundColor.ToUnityColor();
                //Debug.LogError("screenDimensions: " + screenDimensions);
                Entity cameraEntity; 
                if (spawnCamera2D.isEditor == 1)
                {
                    cameraEntity = PostUpdateCommands.Instantiate(editorCamera2DPrefab2);
                }
                else
                {
                    cameraEntity = PostUpdateCommands.Instantiate(camera2DPrefab2);
                }
                PostUpdateCommands.SetComponent(cameraEntity, new ZoxID(id));
                PostUpdateCommands.SetSharedComponentManaged(cameraEntity, new GameObjectLink(cameraObject));
                PostUpdateCommands.SetComponent(cameraEntity, new Translation { Value = cameraObject.transform.position });
                PostUpdateCommands.SetComponent(cameraEntity, new Rotation { Value = cameraObject.transform.rotation });
                // if editor Camera
                if (spawnCamera2D.isEditor == 1)
                {
                   /* var controller = new Controller
                    {
                        // deviceID = deviceID,
                        deviceType = DeviceType.Keyboard,
                        mapping = ControllerMapping.InGame
                    };
                    PostUpdateCommands.SetComponent(cameraEntity, controller);*/
                    PostUpdateCommands.SetComponent(cameraEntity, new Controller(ControllerMapping.InGame));
                    PostUpdateCommands.SetComponent(cameraEntity, new DeviceTypeData(DeviceType.Keyboard));
                }
            }).WithoutBurst().Run();   // Spawns Camera
        }
    }
}