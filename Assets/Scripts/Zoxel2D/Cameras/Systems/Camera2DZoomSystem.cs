using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Cameras;
using Zoxel.Input;

namespace Zoxel.Zoxel2D
{
    //! Zooms the camera with editor controls.
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class Camera2DZoomSystem : SystemBase
	{
		[BurstCompile]
        protected override void OnUpdate()
        {
            var delta = (float) World.Time.DeltaTime;
            var cameraMovementAmount = 2f * delta;
            var scrollAmount = 42f * delta;
            Entities
                .WithNone<ControllerDisabled>()
                .WithAll<TileEditorCamera>()
                .ForEach((ref Translation translation, in Controller controller) =>
            {
                var zAmount = 0f;
                if (controller.mouse.scroll.y < 0)
                {
                    zAmount = scrollAmount;
                }
                else if (controller.mouse.scroll.y > 0)
                {
                    zAmount = -scrollAmount;
                }
                translation.Value += new float3(controller.gamepad.leftStick.x * cameraMovementAmount, controller.gamepad.leftStick.y * cameraMovementAmount, zAmount);
            }).ScheduleParallel();
		}
    }
}