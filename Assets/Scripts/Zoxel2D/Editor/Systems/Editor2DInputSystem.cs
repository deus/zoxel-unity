using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Movement;
using Zoxel.Cameras;
using Zoxel.Transforms;
using Zoxel.Players;
using Zoxel.Input;

namespace Zoxel.Zoxel2D
{
    //! Basic editor inputs.
    /**
    *   \todo Move raycasting to RaycastTilesSystem.
    *   \todo Convert to Parallel. (first part)
    */
    [BurstCompile, UpdateInGroup(typeof(Player2DSystemGroup))]
    public partial class Editor2DInputSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery characterQuery;
        private EntityQuery spawnCharacterQuery;
        private EntityQuery cameraQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            characterQuery = GetEntityQuery(
                ComponentType.Exclude<Controller>(),
                ComponentType.ReadOnly<Character2D>(),
                ComponentType.Exclude<DestroyEntity>());
            spawnCharacterQuery = GetEntityQuery(ComponentType.ReadOnly<SpawnCharacter2D>());
            cameraQuery = GetEntityQuery(
                ComponentType.ReadOnly<Camera>(),
                ComponentType.Exclude<DestroyEntity>());
        }
        
		[BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities
                .WithAll<TileEditor>()
                .ForEach((Entity e, int entityInQueryIndex, ref Controller controller, in CameraLink cameraLink) =>
            {
                var gameObjectLink = EntityManager.GetSharedComponentManaged<GameObjectLink>(cameraLink.camera);
                if (gameObjectLink.gameObject)
                {
                    var camera = EntityManager.GetComponentData<Camera>(cameraLink.camera);
                    var unityCamera = gameObjectLink.gameObject.GetComponent<UnityEngine.Camera>();
                    var pointer = controller.mouse.GetPointer(camera.screenDimensions.ToFloat2());
                    var mousePosition = new float3(pointer.x, pointer.y, 0);
                    var ray = unityCamera.ViewportPointToRay(mousePosition);
                    controller.mouse.rayOrigin = ray.origin;
                    controller.mouse.rayDirection = ray.direction;
                    //UnityEngine.Debug.LogError("Scene Camera rayOrigin: " + controller.mouse.rayOrigin); 
                }
            }).WithoutBurst().Run();
            var planeOrigin = float3.zero;
            var planeNormal = new float3(0, 0, 1);
            var isSpawnCharactersEmpty = spawnCharacterQuery.IsEmpty;
            if (!isSpawnCharactersEmpty)
            {
                return;
            }
            var spawnCharacter2DPrefab = Character2DSpawnSystem.spawnCharacter2DPrefab;
            var characterEntities = characterQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var translations = GetComponentLookup<Translation>(true);
            var cameraEntities = cameraQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var cameras = GetComponentLookup<Camera>(true);
            Dependency = Entities
                .WithNone<ControllerDisabled, PlaceTile>()
                .ForEach((Entity e, int entityInQueryIndex, in Controller controller, in TileEditor tileEditor, in CameraLink cameraLink) =>
            {
                if (cameraEntities.Length == 0)
                {
                    return;
                }
                if (controller.mouse.leftButton.isPressed == 1) //  || controller.mouse.rightButton.isPressed == 1)
                {
                    var camera = cameras[cameraLink.camera];
                    var pointer = controller.mouse.GetPointer(camera.screenDimensions.ToFloat2());
                    if (pointer.x <= 0 || pointer.x >= 1f || pointer.y <= 0 || pointer.y >= 1f)
                    {
                        return;
                    }
                    // UnityEngine.Debug.LogError("mouseLeftHeld!!!");
                    // Place or Remove a Tile
                    var rayOrigin = controller.mouse.rayOrigin;
                    var rayDirection = controller.mouse.rayDirection;
                    float denominator = math.dot(rayDirection, planeNormal);
                    if (denominator > 0.00001f)
                    {
                        var distanceToPlane = math.dot(planeOrigin - rayOrigin, planeNormal) / denominator;
                        var planeHitPoint = rayOrigin + rayDirection * distanceToPlane;
                        // var tilePosition = new int2((int) planeHitPoint.x, (int) planeHitPoint.y);
                        var tilePosition = new int2((int) math.floor(planeHitPoint.x), (int) math.floor(planeHitPoint.y));
                        // Debug
                        /*UnityEngine.Debug.LogError("Scene Camera - Pointer: " + controller.mouse.pointer + " Ray: " + controller.mouse.rayOrigin + " - > " + planeHitPoint);
                        if (placeType == 1)
                        {
                            UnityEngine.Debug.DrawLine(rayOrigin, planeHitPoint, UnityEngine.Color.cyan, 5);
                        }
                        else
                        {
                            UnityEngine.Debug.DrawLine(rayOrigin, planeHitPoint, UnityEngine.Color.red, 5);
                        }*/
                        var isCharacterThere = false;
                        for (int j = 0; j < characterEntities.Length; j++)
                        {
                            var characterEntity = characterEntities[j];
                            var translation2 = translations[characterEntity];
                            var position = new int2((int) translation2.Value.x, (int) translation2.Value.y);
                            if (tilePosition == position)
                            {
                                if (tileEditor.placeType == 1)
                                {
                                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, characterEntity);
                                    return; // finished
                                }
                                else
                                {
                                    isCharacterThere = true;
                                    break;
                                }
                            }
                        }
                        if (!isCharacterThere)
                        {
                            if (tileEditor.placeType == 0)
                            {
                                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new PlaceTile { type = (byte) (tileEditor.placeIndex + 1), position = tilePosition });
                            }
                            else if (tileEditor.placeType == 1)
                            {
                                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new PlaceTile { type = 0, position = tilePosition });
                            }
                            else if (tileEditor.placeType == 2)
                            {
                                // UnityEngine.Debug.LogError("tilePosition: " + tilePosition);
                                var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnCharacter2DPrefab);
                                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new SpawnCharacter2D
                                { 
                                    spawnPosition = new float3(tilePosition.x + 0.5f, tilePosition.y + 0.5f, 0)
                                });
                            }
                        }
                    }
                }
            })  .WithReadOnly(characterEntities)
                .WithDisposeOnCompletion(characterEntities)
                .WithReadOnly(translations)
                .WithNativeDisableContainerSafetyRestriction(translations)
                .WithReadOnly(cameraEntities)
                .WithDisposeOnCompletion(cameraEntities)
                .WithReadOnly(cameras)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}


//a normal (defining the orientation of the plane), should be negative if we are firing the ray from above
//A plane can be defined as:
//a point representing how far the plane is from the planet origin
//We are intrerested in calculating a point in this plane called p
//A ray to point p can be defined as: l_0 + l * t = p, where:
//the origin of the ray
//The vector between p and p0 and the normal is always perpendicular: (p - p_0) . n = 0
//t is the length of the ray, which we can get by combining the above equations:
//t = ((p_0 - l_0) . n) / (l . n)
//But there's a chance that the line doesn't intersect with the plane, and we can check this by first
//calculating the denominator and see if it's not small. 
//We are also checking that the denominator is positive or we are looking in the opposite direction

/*public static Ray ViewportPointToRay(Vector2 aP, Matrix4x4 aProj, Matrix4x4 aCam)
{
    var m = aProj * aCam;
    var mInv = m.inverse;
    // near clipping plane point
    Vector4 p = new Vector4(aP.x*2-1, aP.y*2-1, -1, 1f);
    var p0 = mInv * p;
    p0 /= p0.w;
    // far clipping plane point
    p.z = 1;
    var p1 = mInv * p;
    p1 /= p1.w;
    return new Ray(p0, (p1-p0).normalized);*/