using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.UI;
using Zoxel.Cameras;
using UnityEngine.InputSystem;
using Zoxel.Players;
using Zoxel.Input;

namespace Zoxel.Zoxel2D
{
    //! 2D editor shortcuts.
    /**
    *   \todo Convert to Parallel. (first part)
    *   \todo Split into multiple systems.
    */
    /*[BurstCompile, UpdateInGroup(typeof(Player2DSystemGroup))]
    public partial class EditorShortcutsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery world2DQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            world2DQuery = GetEntityQuery(
                ComponentType.ReadOnly<World2D>(),
                ComponentType.Exclude<DestroyEntity>());
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);

            // Free Roam Camera movement!
            const float movementSpeedX = 0.05f;
            const float movementSpeedY = 0.03f;
            const float movementSpeedZ = 1.0f;
            Entities
                .WithAll<TileEditor>()
                .ForEach((in Controller controller, in CameraLink cameraLink) =>
            {
                if (controller.keyboard.leftControlKey.isPressed == 1)
                {
                    return;
                }
                var cameraPosition = EntityManager.GetComponentData<Translation>(cameraLink.camera);
                if (controller.keyboard.dKey.isPressed == 1)
                {
                    cameraPosition.Value.x += movementSpeedX;
                }
                if (controller.keyboard.aKey.isPressed == 1)
                {
                    cameraPosition.Value.x -= movementSpeedX;
                }
                if (controller.keyboard.wKey.isPressed == 1)
                {
                    cameraPosition.Value.y += movementSpeedY;
                }
                if (controller.keyboard.sKey.isPressed == 1)
                {
                    cameraPosition.Value.y -= movementSpeedY;
                }
                if (controller.mouse.scroll.y < 0)
                {
                    cameraPosition.Value.z += movementSpeedZ;
                }
                else if (controller.mouse.scroll.y > 0)
                {
                    cameraPosition.Value.z -= movementSpeedZ;
                }
                PostUpdateCommands2.SetComponent(cameraLink.camera, cameraPosition);
                if (controller.keyboard.spaceKey.isPressed == 1)
                {
                    // UnityEngine.Debug.LogError("Start!");
                    PostUpdateCommands2.SetComponent(cameraLink.camera, new Rotation { Value = quaternion.identity });
                    PostUpdateCommands2.SetComponent(cameraLink.camera, new Translation { Value = new float3(10, 10, -10f) });
                }
            }).WithoutBurst().Run();

            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var world2DEntities = world2DQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            //var cameras = GetComponentLookup<Camera2D>(true);
            // todo: add World2DLink to player
            // Use that to add Save and Load Commands
            // in save/load system - open up 
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref World2DLink world2DLink) =>
            {
                if (world2DLink.world2D.Index <= 0 && world2DEntities.Length > 0)
                {
                    world2DLink.world2D = world2DEntities[0];
                }
            })  .WithReadOnly(world2DEntities)
                .WithDisposeOnCompletion(world2DEntities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

            Dependency = Entities
                .WithAll<Camera2D>()
                .ForEach((Entity e, int entityInQueryIndex, in Controller controller, in CharacterLink characterLink) =>
            {
                if (controller.keyboard.leftControlKey.isPressed == 1)
                {
                    if (controller.keyboard.eKey.wasPressedThisFrame == 1)
                    {
                        // spawn player character at spawn point
                        PostUpdateCommands.AddComponent(entityInQueryIndex, characterLink.character, controller);
                        PostUpdateCommands.RemoveComponent<Controller>(entityInQueryIndex, e);
                        PostUpdateCommands.RemoveComponent<FollowCamera2D>(entityInQueryIndex, e);
                        PostUpdateCommands.AddComponent<TileEditorCamera>(entityInQueryIndex, e);
                        PostUpdateCommands.AddComponent<TileEditor>(entityInQueryIndex, e);
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

            Dependency = Entities
                .WithAll<TileEditor>()
                .ForEach((Entity e, int entityInQueryIndex, in World2DLink world2DLink, in Controller controller) =>
            {
                if (controller.keyboard.leftControlKey.isPressed == 1)
                { 
                    if (controller.keyboard.lKey.wasPressedThisFrame == 1)
                    {
                        PostUpdateCommands.AddComponent<LoadEntity>(entityInQueryIndex, world2DLink.world2D);
                    }
                    else if (controller.keyboard.sKey.wasPressedThisFrame == 1)
                    {
                        PostUpdateCommands.AddComponent<SaveEntity>(entityInQueryIndex, world2DLink.world2D);
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

            var tilesCount = TileManager.instance.TileSettings.tiles.Length;
            Entities.ForEach((Entity e, int entityInQueryIndex, ref TileEditor tileEditor, in Controller controller) =>
            {
                // if (controller.keyboard.leftControlKey.isPressed == 1)
                {
                    // UnityEngine.Debug.Log("leftControlKey");
                    if (controller.keyboard.digit1Key.wasPressedThisFrame == 1)
                    {
                        if (tileEditor.placeType != 0)
                        {
                            tileEditor.placeType = 0;
                            tileEditor.placeIndex = 0;
                            //UnityEngine.Debug.LogError("Place Tile Index Set to: " + tileEditor.placeIndex);
                        }
                        else
                        {
                            tileEditor.placeIndex++;
                            if (tileEditor.placeIndex >= tilesCount)
                            {
                                tileEditor.placeIndex = 0;
                            }
                            //UnityEngine.Debug.LogError("Place Tile Index Set to: " + tileEditor.placeIndex);
                        }
                    }
                    // Erase Tool
                    else if (controller.keyboard.digit2Key.wasPressedThisFrame == 1)
                    {
                        if (tileEditor.placeType != 1)
                        {
                            tileEditor.placeType = 1;
                            tileEditor.placeIndex = 0;
                            //UnityEngine.Debug.LogError("Erase Brush Set.");
                        }
                    }
                    // Minion Tool
                    else if (controller.keyboard.digit3Key.wasPressedThisFrame == 1)
                    {
                        // UnityEngine.Debug.LogError("Toggling Placement Tool.");
                        if (tileEditor.placeType != 2)
                        {
                            tileEditor.placeType = 2;
                            tileEditor.placeIndex = 0;
                            //UnityEngine.Debug.LogError("Placing Minions: "+ tileEditor.placeType);
                        }
                    }
                }
            }).ScheduleParallel();

            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<Character2D>()
                .ForEach((Entity e, int entityInQueryIndex, in Controller controller, in CameraLink cameraLink) =>
            {
                if (controller.keyboard.leftControlKey.isPressed == 1 && controller.keyboard.eKey.wasPressedThisFrame == 1)
                {
                    // destroy character2D
                    // add Editor Camera to this
                    PostUpdateCommands.AddComponent(entityInQueryIndex, cameraLink.camera, controller);
                    PostUpdateCommands.RemoveComponent<FollowCamera2D>(entityInQueryIndex, cameraLink.camera);
                    PostUpdateCommands.AddComponent<TileEditorCamera>(entityInQueryIndex, cameraLink.camera);
                    PostUpdateCommands.AddComponent<TileEditor>(entityInQueryIndex, cameraLink.camera);
                    PostUpdateCommands.RemoveComponent<Controller>(entityInQueryIndex, e);
                    // boom
                    PostUpdateCommands.SetComponent(entityInQueryIndex, cameraLink.camera, new CharacterLink(e));
                    // PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }*/
}