using Unity.Entities;

namespace Zoxel.Zoxel2D
{
    public struct TileEditor : IComponentData
    {
        public byte placeType;
        public byte placeIndex; // for example, 0 for dirt, 1 for ice, or 0 for gaurd, 1 for minion
    }
}