using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;
using Zoxel.Transforms;
// to do: make grab tiles from corresponding chunk

namespace Zoxel.Zoxel2D
{
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class Chunk2DGenerateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var tileDirt = (byte) 1;
            // var tileGrassTop = (byte) 2;
            var seed = (uint) IDUtil.GenerateUniqueID();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                // .WithNone<OnChildrenSpawned>()
                .WithAll<Chunk2DGenerate>()
                .ForEach((Entity e, int entityInQueryIndex, ref TileChunk tileChunk) =>
            {
                PostUpdateCommands.AddComponent<ChunkBuilder2D>(entityInQueryIndex, e);
                PostUpdateCommands.RemoveComponent<Chunk2DGenerate>(entityInQueryIndex, e);
                var random = new Random();
                random.InitState(seed);
                var position = int2.zero;
                var index = 0;
                for (position.x = 0; position.x < tileChunk.data.size.x; position.x++)
                {
                    var height = random.NextInt(5, 8);
                    for (position.y = 0; position.y < tileChunk.data.size.y; position.y++)
                    {
                       //  index = TileUtilities.GetTileArrayIndex(position, tileChunk.data.size);
                        var tileType = (byte) 0;
                        if (position.y <= height)
                        {
                            tileType = tileDirt;
                        }
                        /*else if (position.y < height)
                        {
                            tileType = tileDirt;
                        }
                        if (position.y == height)
                        {
                            tileType = tileGrassTop;
                        }
                        else if (position.y < height)
                        {
                            tileType = tileDirt;
                        }*/
                        tileChunk.data.tiles[index] = tileType;
                        index++;
                    }
                }
                /*for (position.x = 0; position.x < tileChunk.data.size.x; position.x++)
                {
                    for (position.y = 0; position.y < tileChunk.data.size.y; position.y++)
                    {
                        index = TileUtilities.GetTileArrayIndex(position, tileChunk.data.size);
                        var previousTileType = tileChunk.data.tiles[index];
                        if (previousTileType == tileDirt || previousTileType == tileGrassTop)
                        {
                            var tileLeft = (byte) 0;
                            if (position.x != 0)
                            {
                                tileLeft = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Left(), tileChunk.data.size)];
                            }
                            var tileRight = (byte) 0;
                            if (position.x != tileChunk.data.size.x - 1)
                            {
                                tileRight = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Right(), tileChunk.data.size)];
                            }
                            var tileUp = (byte) 0;
                            if (position.y != tileChunk.data.size.y - 1)
                            {
                                tileUp = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Up(), tileChunk.data.size)];
                            }
                            var tileDown = (byte) 0;
                            if (position.y != 0)
                            {
                                tileDown = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Down(), tileChunk.data.size)];
                            }
                            if (tileRight == 0 && tileLeft == 0)
                            {
                                if (tileUp != 0)
                                {
                                    tileChunk.data.tiles[index] = (byte) 7;
                                }
                                else
                                {
                                    tileChunk.data.tiles[index] = (byte) 8;
                                }
                            }
                            else if (tileLeft == 0)
                            {
                                if (tileUp != 0)
                                {
                                    tileChunk.data.tiles[index] = (byte) 6;
                                }
                                else
                                {
                                    tileChunk.data.tiles[index] = (byte) 3;
                                }
                            }
                            else if (tileRight == 0)
                            {
                                if (tileUp != 0)
                                {
                                    tileChunk.data.tiles[index] = (byte) 5;
                                }
                                else
                                {
                                    tileChunk.data.tiles[index] = (byte) 4;
                                }
                            }
                        }
                    }
                }
                for (position.x = 0; position.x < tileChunk.data.size.x; position.x++)
                {
                    for (position.y = 0; position.y < tileChunk.data.size.y; position.y++)
                    {
                        index = TileUtilities.GetTileArrayIndex(position, tileChunk.data.size);
                        var previousTileType = tileChunk.data.tiles[index];
                        if (previousTileType == tileDirt)
                        {
                            var tileLeft = (byte) 0;
                            if (position.x != 0)
                            {
                                tileLeft = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Left(), tileChunk.data.size)];
                            }
                            var tileRight = (byte) 0;
                            if (position.x != tileChunk.data.size.x - 1)
                            {
                                tileRight = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Right(), tileChunk.data.size)];
                            }
                            var tileUp = (byte) 0;
                            if (position.y != 0)
                            {
                                tileUp = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Up(), tileChunk.data.size)];
                            }
                            var tileDown = (byte) 0;
                            if (position.y != tileChunk.data.size.y - 1)
                            {
                                tileDown = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Down(), tileChunk.data.size)];
                            }
                            // top left corner
                            if ((tileUp == 3 || tileUp == 6) && (tileLeft == tileGrassTop || tileLeft == 3)
                                && (tileRight == 1 || tileRight == 10 || tileRight == 5))
                            {
                                tileChunk.data.tiles[index] = (byte) 9;
                            }
                            // top right corner
                            else if ((tileUp == 4 || tileUp == 5) && (tileRight == tileGrassTop || tileRight == 4)
                                && (tileLeft == 1 || tileLeft == 9 || tileLeft == 6))
                            {
                                tileChunk.data.tiles[index] = (byte) 10;
                            }
                            // top left and top right corner
                            else if ((tileUp == 7 || tileUp == 8) && (tileLeft == tileGrassTop || tileLeft == 3)
                                && (tileRight == tileGrassTop || tileRight == 4))
                            {
                                tileChunk.data.tiles[index] = (byte) 11;
                            }
                        }
                    }
                }*/
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}