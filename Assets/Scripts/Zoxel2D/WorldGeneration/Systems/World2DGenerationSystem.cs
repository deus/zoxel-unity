using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;

namespace Zoxel.Zoxel2D
{
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class World2DGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, OnChildrenSpawned, SpawnChunks2D>()
                .WithAll<GenerateWorld2D>()
                .ForEach((Entity e, int entityInQueryIndex, in Childrens childrens) =>
            {
                PostUpdateCommands.RemoveComponent<GenerateWorld2D>(entityInQueryIndex, e);
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    PostUpdateCommands.AddComponent<Chunk2DGenerate>(entityInQueryIndex, childrens.children[i]);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}