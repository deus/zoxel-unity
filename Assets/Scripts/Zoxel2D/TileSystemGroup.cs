using Unity.Entities;

namespace Zoxel.Zoxel2D
{
    public static class TileUtilities
    {
        public static int GetTileArrayIndex(int2 position, int2 size)
        {
            return position.y + size.y * position.x;
        }
        public static int GetTileArrayIndex2(int2 position, int2 size)
        {
            return position.x + size.x * position.y;
        }
    }
    //! Handles 2D Tilemap systems.
    public partial class TileSystemGroup : ComponentSystemGroup { }
}