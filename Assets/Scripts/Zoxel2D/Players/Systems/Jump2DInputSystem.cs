using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Players;
using Zoxel.Input;
using Zoxel.Zoxel2D.Physics;
// ToDO: Redo chunk links in this algorithm
// The problem:
//      Scales off a list of chunk renders atm
//      To get the corresponding chunk to a point I should only search from the ones connected to the character
//      i.e. inChunk, then chunkLeft, chunkRight, etc - Adjacent chunks
// todo: draw a point for each collision point

namespace Zoxel.Zoxel2D
{
    [BurstCompile, UpdateInGroup(typeof(Player2DSystemGroup))]
    public partial class Jump2DInputSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<ControllerDisabled, Jump2D>()
                .WithAll<OnGround, Gravity2D>()
                .ForEach((Entity e, int entityInQueryIndex, in Controller controller, in Translation translation) =>
            {
                if (controller.mapping == (ControllerMapping.InGame))    // in game
                {
                    if (controller.gamepad.buttonA.wasPressedThisFrame == 1 || controller.gamepad.buttonA.isPressed == 1)
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new Jump2D(translation.Value.y));
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}