using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Players;
using Zoxel.Input;
using Zoxel.Zoxel2D.Physics;
using Zoxel.Movement.Authoring;

namespace Zoxel.Zoxel2D
{
    //! Moves the player's character Body around using the Controller component
    /** 
    *   Notes:
    *       Move player inputs to controlling the body
    *       XY controller goes to XZ body movement
    */
    [BurstCompile, UpdateInGroup(typeof(Player2DSystemGroup))]
    public partial class Player2DInputSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<PhysicsSettings>();
        }
        
		[BurstCompile]
        protected override void OnUpdate()
        {
            var physicsSettings = GetSingleton<PhysicsSettings>();
            var airSlowMultiplier = physicsSettings.airSlowMultiplier; // 6f
            var delta = (float) World.Time.DeltaTime;
            var movementForce = 16;
            var maxVelocity = 16;
            Entities
                .WithNone<ControllerDisabled, FlyMode>()
                .ForEach((Entity e, ref BodyForce2D bodyForce2D, in Controller controller) =>
            {
                if (controller.mapping == ControllerMapping.InGame)    // in game
                {
                    // var accelaration = new float2();
                    if (math.abs(controller.gamepad.leftStick.x) >= 0.001f && math.abs(bodyForce2D.velocity.x) < maxVelocity)
                    {
                        bodyForce2D.acceleration.x += controller.gamepad.leftStick.x * movementForce;
                    }
                    if (!HasComponent<Gravity2D>(e) && math.abs(controller.gamepad.leftStick.y) >= 0.001f && math.abs(bodyForce2D.velocity.y) < maxVelocity)
                    {
                        bodyForce2D.acceleration.y += controller.gamepad.leftStick.y * movementForce;
                    }
                }
            }).ScheduleParallel();
            // reset game system
            Entities
                .WithNone<ControllerDisabled>()
                .WithAll<Character2D>()
                .ForEach((ref Translation translation, ref BodyForce2D bodyForce2D, in Controller controller) =>
            {
                if (controller.gamepad.startButton.wasPressedThisFrame == 1)
                {
                    translation.Value = new float3(8.5f, 12, 0);
                    bodyForce2D.velocity.x = 0;
                    bodyForce2D.velocity.y = 0;
                }
            }).ScheduleParallel();
		}
    }
}