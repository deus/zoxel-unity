using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Zoxel2D.Physics;

namespace Zoxel.Zoxel2D
{
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class Character2DFlipSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
		[BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<Character2D>()
                .ForEach((Entity e, int entityInQueryIndex, ref NonUniformScale scale, in BodyForce2D bodyForce2D) =>
            {
                if (bodyForce2D.velocity.x >= 0)
                {
                    scale.Value.x = 1;
                }
                else
                {
                    scale.Value.x = -1;
                }
            }).ScheduleParallel(Dependency);
		}
    }
}