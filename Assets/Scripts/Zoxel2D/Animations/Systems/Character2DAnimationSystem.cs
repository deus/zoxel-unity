using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Players;
using Zoxel.Rendering;
using Zoxel.Movement;
using Zoxel.Zoxel2D.Physics;

namespace Zoxel.Zoxel2D
{
    //! Animates the sprites of the Character2D.
    /**
    *   \todo Convert to Parallel.
    *   \todo Split between logic and material setting systems.
    */
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class Character2DAnimationSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
		[BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var animationSpeed = 0.28;
            Entities
                .WithNone<DestroyEntity, InitializeEntity>()
                .WithAll<Character2D>()
                .ForEach((Entity e, int entityInQueryIndex, ref Animator2D animator2D, in BodyForce2D bodyForce2D, in ZoxMesh zoxMesh, in NonUniformScale scale) =>
            {
                if (elapsedTime - animator2D.lastSwitched >= animationSpeed)
                {
                    animator2D.lastSwitched = elapsedTime;
                }
                else
                {
                    return;
                }
                if (math.abs(bodyForce2D.velocity.x) > 0.05f)
                {
                    if (animator2D.direction != (int) scale.Value.x)
                    {
                        animator2D.direction = (int) scale.Value.x;
                        zoxMesh.material.SetInt("_Direction", animator2D.direction);
                    }
                    if (animator2D.count == 0)
                    {
                        animator2D.count = 1;
                    }
                    else if (animator2D.count == 1)
                    {
                        animator2D.count = 2;
                    }
                    else if (animator2D.count == 2)
                    {
                        animator2D.count = 3;
                    }
                    else if (animator2D.count == 3)
                    {
                        animator2D.count = 1;
                    }
                    zoxMesh.material.SetInt("_Index", (int) animator2D.count);
                }
                else
                {
                    if (animator2D.count == 1 || animator2D.count == 2 || animator2D.count == 3)
                    {
                        animator2D.count = 0;
                        zoxMesh.material.SetInt("_Index", (int) animator2D.count);
                    }
                    animator2D.lastSwitched = elapsedTime - animator2D.lastSwitched;
                }
            }).WithoutBurst().Run();
		}
    }
}