using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Zoxel2D
{
    //! Places a tile in our World2D.
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class PlaceTileSystem : SystemBase
	{
        private EntityQuery chunk2DQuery;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            chunk2DQuery = GetEntityQuery(
                ComponentType.ReadOnly<TileChunk>(),
                ComponentType.Exclude<Chunk2DGenerate>(),
                ComponentType.Exclude<ChunkBuilder2D>());
        }
        
		[BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var chunkEntities = chunk2DQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunks = GetComponentLookup<TileChunk>(true);
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in PlaceTile placeTile) =>
            {
                if (chunkEntities.Length == 0)
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<PlaceTile>(entityInQueryIndex, e);
                var chunkEntity = chunkEntities[0];
                if (HasComponent<ChunkBuilder2D>(chunkEntity))
                {
                    return;
                }
                var tileChunk = chunks[chunkEntity];
                if (placeTile.position.x >= 0 && placeTile.position.x < tileChunk.data.size.x &&
                    placeTile.position.y >= 0 && placeTile.position.y < tileChunk.data.size.y)
                {
                    var tileIndex = TileUtilities.GetTileArrayIndex(placeTile.position, tileChunk.data.size);
                    if (tileChunk.data.tiles[tileIndex] != placeTile.type)
                    {
                        tileChunk.data.tiles[tileIndex] = placeTile.type;
                        // PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, tileChunk);
                        PostUpdateCommands.AddComponent<ChunkBuilder2D>(entityInQueryIndex, chunkEntity);
                    }
                }
                // UnityEngine.Debug.LogError("PlaceTile: " + placeTile.position);
            })  .WithReadOnly(chunkEntities)
                .WithDisposeOnCompletion(chunkEntities)
                .WithReadOnly(chunks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}