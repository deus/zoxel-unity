using Unity.Entities;

namespace Zoxel.Zoxel2D
{
    //! An event to place a tile in our World2D.
    public struct PlaceTile : IComponentData
    {
        public byte type;
        public int2 position;
    }
}