using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
// using UnityEngine.Rendering;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Textures;
using Zoxel.Textures.Tilemaps;

namespace Zoxel.Zoxel2D
{
    //! Generates a TilesChunk mesh.
    /**
    *   \todo Use Tiles and TileRules and Tilemap as data imports.
    */
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class TilesChunkBuildSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery world2DQuery;
        private EntityQuery tileQuery;
        private EntityQuery tileRuleQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            world2DQuery = GetEntityQuery(
                ComponentType.ReadOnly<World2D>(),
                ComponentType.Exclude<DestroyEntity>());
            tileQuery = GetEntityQuery(
                ComponentType.ReadOnly<Tile>(),
                ComponentType.Exclude<DestroyEntity>());
            tileRuleQuery = GetEntityQuery(
                ComponentType.ReadOnly<TileRule>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var world2DEntities = world2DQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var tilemaps = GetComponentLookup<Tilemap>(true);
            var tileLinks = GetComponentLookup<TileLinks>(true);
            world2DEntities.Dispose();
            var tileEntities = tileQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var tileTilemapIndexes = GetComponentLookup<TilemapTileIndex>(true);
            var tileRuleLinks = GetComponentLookup<TileRuleLinks>(true);
            tileEntities.Dispose();
            var tileRuleEntities = tileRuleQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var tileRules = GetComponentLookup<TileRule>(true);
            var ruleTilemapIndexes = GetComponentLookup<TilemapTileIndex>(true);
            tileRuleEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<ChunkBuilder2D>()
                .ForEach((Entity e, int entityInQueryIndex, ref TerrainMesh terrainMesh, in TileChunk tileChunk, in ParentLink parentLink) =>
            {
                PostUpdateCommands.RemoveComponent<ChunkBuilder2D>(entityInQueryIndex, e);
                var worldTileEntities = tileLinks[parentLink.parent].tiles;
                var tilemap = tilemaps[parentLink.parent];
                var tilemapLength = tilemap.gridSize;
                var maxTiles = tilemapLength * tilemapLength;
                var tilemapPercentage = 1 / ((float) tilemapLength);
                var position = int2.zero;
                var index = 0;
                var sideIndex = 4;
                var vertices = new NativeList<ZoxVertex>();
                var triangles = new NativeList<uint>();
                byte tileType;
                for (position.x = 0; position.x < tileChunk.data.size.x; position.x++)
                {
                    for (position.y = 0; position.y < tileChunk.data.size.y; position.y++)
                    {
                        tileType = tileChunk.data.tiles[index];
                        if (tileType != 0)
                        {
                            // Get corresponding tiles
                            var tileLeft = (byte) 0;
                            if (position.x != 0)
                            {
                                tileLeft = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Left(), tileChunk.data.size)];
                            }
                            var tileRight = (byte) 0;
                            if (position.x != tileChunk.data.size.x - 1)
                            {
                                tileRight = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Right(), tileChunk.data.size)];
                            }
                            var tileUp = (byte) 0;
                            if (position.y != tileChunk.data.size.y - 1)
                            {
                                tileUp = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Up(), tileChunk.data.size)];
                            }
                            var tileDown = (byte) 0;
                            if (position.y != 0)
                            {
                                tileDown = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Down(), tileChunk.data.size)];
                            }
                            var tileUpRight = (byte) 0;
                            if (position.y != tileChunk.data.size.y - 1 && position.x != tileChunk.data.size.x - 1)
                            {
                                tileUpRight = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Up().Right(), tileChunk.data.size)];
                            }
                            var tileUpLeft = (byte) 0;
                            if (position.y != tileChunk.data.size.y - 1 && position.x != 0)
                            {
                                tileUpLeft = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Up().Left(), tileChunk.data.size)];
                            }
                            var tileDownRight = (byte) 0;
                            if (position.y != 0 && position.x != tileChunk.data.size.x - 1)
                            {
                                tileDownRight = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Down().Right(), tileChunk.data.size)];
                            }
                            var tileDownLeft = (byte) 0;
                            if (position.y != 0 && position.x != 0)
                            {
                                tileDownLeft = tileChunk.data.tiles[TileUtilities.GetTileArrayIndex(position.Down().Left(), tileChunk.data.size)];
                            }

                            var tilemapIndex = (int)(tileType) - 1;
                            var tileEntity = worldTileEntities[tilemapIndex];

                            // get default tile map index of thee tile Entity
                            tilemapIndex = tileTilemapIndexes[tileEntity].index;

                            // go through rules of tile
                            var rules = tileRuleLinks[tileEntity].rules;
                            for (int j = 0; j < rules.Length; j++)
                            {
                                var ruleEntity = rules[j];
                                var tileRule = tileRules[ruleEntity];
                                if (tileRule.DoesRulePass(tileLeft, tileRight, tileDown, tileUp, tileDownLeft, tileDownRight, tileUpLeft, tileUpRight))
                                {
                                    // set tilemapIndex to the texture's tilemap index here
                                    // tilemapIndex += j;
                                    tilemapIndex = ruleTilemapIndexes[ruleEntity].index;
                                    // tilemapIndex = 2;
                                    break;
                                }
                            }

                            var positionFloat3 = new float3(position.x, position.y, 0);
                            // draw tile onto mesh
                            var vertsLength = vertices.Length;
                            var tilemapPositionX = tilemapIndex % tilemapLength;
                            var tilemapPositionY = tilemapIndex / tilemapLength;
                            var uvAddition = tilemapPercentage * (new float2(tilemapPositionX, tilemapPositionY));
                            for (var arrayIndex = 0; arrayIndex < 4; arrayIndex++)
                            {
                                var vert = new ZoxVertex();
                                vert.color = new float3(1,1,1);
                                var cubeIndex = sideIndex * 4 + arrayIndex;
                                vert.position = MeshUtilities.cubeVertices[cubeIndex] + positionFloat3;
                                vert.uv.x += tilemapPercentage * tilemapPositionX;
                                vert.uv.y += tilemapPercentage * tilemapPositionY;
                                vert.uv = uvAddition + MeshUtilities.cubeUVs[arrayIndex] * tilemapPercentage;
                                //vert.uv.x += tilemapPercentage * tilemapPositionX;
                                //vert.uv.y += tilemapPercentage * tilemapPositionY;
                                // tileIndex is close to this
                                // now divide by rows
                                // reverse Y, since we start drawing at the top
                                // tilemapPositionY = tilemapLength - tilemapPositionY - 1;
                                vertices.Add(vert);
                            }
                            for (var arrayIndex = 0; arrayIndex < 6; arrayIndex++)
                            {
                                var cubeIndex = sideIndex * 6 + arrayIndex;
                                var triangle = MeshUtilities.cubeTriangles[cubeIndex] + vertsLength; // terrainMesh.buildPointer.vertIndex;
                                triangles.Add((uint)triangle);
                            }
                        }
                        index++;
                    }
                }
                if (terrainMesh.vertices.Length > 0)
                {
                    terrainMesh.vertices.Dispose();
                }
                if (terrainMesh.triangles.Length > 0)
                {
                    terrainMesh.triangles.Dispose();
                }
                terrainMesh.vertices = new BlitableArray<ZoxVertex>(vertices.Length, Allocator.Persistent);
                for (var i = 0; i < vertices.Length; i++)
                {
                    terrainMesh.vertices[i] = vertices[i];
                }
                terrainMesh.triangles = new BlitableArray<uint>(triangles.Length, Allocator.Persistent);
                for (var i = 0; i < triangles.Length; i++)
                {
                    terrainMesh.triangles[i] = triangles[i];
                }
                PostUpdateCommands.AddComponent<ChunkMeshDirty>(entityInQueryIndex, e);
                vertices.Dispose();
                triangles.Dispose();
            })  .WithReadOnly(tileLinks)
                .WithReadOnly(tilemaps)
                .WithReadOnly(tileRuleLinks)
                .WithReadOnly(tileTilemapIndexes)
                .WithReadOnly(tileRules)
                .WithReadOnly(ruleTilemapIndexes)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

                            // Pass in tilemap index AND neighbor tiles - into tile data
                            // using tile data with inputs - get uvs - get index
                            // tilemapIndex = 0;
                            //var tilemapIndex2 = tilemapIndex; // maxTiles - 1 - tilemapIndex;
                            //  use rules to convert tile to tilemap index!
                            /*if (tileUp == 0 && tileLeft == 1 && tileRight == 1) // && tileUpLeft == 0 && tileUpRight == 0)
                            {
                                tilemapIndex = tileGrassUp;
                            }
                            else if (tileUp == 0 && tileLeft == 1 && tileRight == 0)
                            {
                                tilemapIndex = tileGrassUpRight;
                            }
                            else if (tileUp == 0 && tileLeft == 0 && tileRight == 1)
                            {
                                tilemapIndex = tileGrassUpLeft;
                            }
                            else if (tileUp == 0 && tileLeft == 0 && tileRight == 0)
                            {
                                tilemapIndex = tileGrassLeftRightUp;
                            }
                            else if (tileUp == 1 && tileLeft == 0 && tileRight == 0)
                            {
                                tilemapIndex = tileGrassLeftRight;
                            }
                            else if (tileUp == 1 && tileLeft == 0 && tileRight == 1)
                            {
                                tilemapIndex = tileGrassLeft;
                            }
                            else if (tileUp == 1 && tileLeft == 1 && tileRight == 0)
                            {
                                tilemapIndex = tileGrassRight;
                            }
                            else if (tileUp == 1 && tileLeft == 1 && tileRight == 1 && tileUpLeft == 0 && tileUpRight == 0)
                            {
                                tilemapIndex = tileGrassCornersUp;
                            }
                            else if (tileUp == 1 && tileLeft == 1 && tileRight == 1 && tileUpLeft == 0 && tileUpRight == 1)
                            {
                                tilemapIndex = tileGrassCornerUpLeft;
                            }
                            else if (tileUp == 1 && tileLeft == 1 && tileRight == 1 && tileUpLeft == 1 && tileUpRight == 0)
                            {
                                tilemapIndex = tileGrassCornerUpRight;
                            }*/
            /*var tileGrassUp = 1;
            var tileGrassRight = 4;
            var tileGrassLeft = 5;
            var tileGrassUpLeft = 2;
            var tileGrassUpRight = 3;
            var tileGrassLeftRight = 6;
            var tileGrassLeftRightUp = 7;
            var tileGrassCornerUpLeft = 8;
            var tileGrassCornerUpRight = 9;
            var tileGrassCornersUp = 10;*/