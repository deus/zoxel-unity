using Unity.Entities;
using UnityEngine.Rendering;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Zoxel2D
{
    //! Updates tileChunk mesh to gpu.
    [UpdateInGroup(typeof(TileSystemGroup))]
    public partial class TilesChunkMeshUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private VertexAttributeDescriptor[] vertLayoutUV;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            vertLayoutUV = new[]
            {
                new VertexAttributeDescriptor(VertexAttribute.Position, VertexAttributeFormat.Float32, 3),
                new VertexAttributeDescriptor(VertexAttribute.Color, VertexAttributeFormat.Float32, 3),
                new VertexAttributeDescriptor(VertexAttribute.TexCoord0, VertexAttributeFormat.Float32, 2)
            };
        }

        protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<TileChunk, ChunkMeshDirty>()
                .ForEach((Entity e, int entityInQueryIndex, ZoxMesh zoxMesh, in TerrainMesh terrainMesh) =>
            {
                PostUpdateCommands.RemoveComponent<ChunkMeshDirty>(e);
                if (terrainMesh.vertices.Length == 0)
                {
                    // UnityEngine.Debug.LogError("Chunk Meush has no Verts!");
                    return;
                }
                if (zoxMesh.mesh == null)
                {
                    UnityEngine.Debug.LogWarning("ChunkRender ZoxMesh mesh is null.");
                    zoxMesh.mesh = new UnityEngine.Mesh();
                    PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
                }
                var mesh = zoxMesh.mesh;
                var vertexCount = terrainMesh.vertices.Length;
                var trianglesCount = terrainMesh.triangles.Length;
                var vertices = terrainMesh.GetVertexNativeArray();
                var triangles = terrainMesh.GetTriangleNativeArray();
                // UnityEngine.Debug.LogError("vertexCount: " + vertexCount);
                // max is x billion for 32 bits uint
                mesh.SetVertexBufferParams(vertexCount, vertLayoutUV);
                mesh.SetVertexBufferData(vertices, 0, 0, vertexCount);
                mesh.SetIndexBufferParams(trianglesCount, IndexFormat.UInt32); //  IndexFormat.UInt32  IndexFormat.UInt16
                mesh.SetIndexBufferData(triangles, 0, 0, trianglesCount);
                mesh.SetSubMesh(0, new SubMeshDescriptor()
                {
                    baseVertex = 0,
                    firstVertex = 0,
                    indexStart = 0,
                    bounds = default,   // ? default? wut,
                    vertexCount = vertexCount,
                    indexCount = trianglesCount,
                    topology = UnityEngine.MeshTopology.Triangles
                });
                mesh.UploadMeshData(false);
                mesh.RecalculateBounds();
            }).WithoutBurst().Run();
        }
    }
}

                /*if (mesh)
                {
                    UnityEngine.GameObject.DestroyImmediate(mesh);
                    mesh = new UnityEngine.Mesh();
                    zoxMesh.mesh = mesh;
                    PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
                }*/

                /*if (mesh.vertices.Length != 0)
                {
                    return;
                }*/


                /*var vertices2 = new UnityEngine.Vector3[vertices.Length];
                var uvs2 = new UnityEngine.Vector2[vertices.Length];
                var triangles2 = new ushort[triangles.Length];
                for (var i = 0; i < vertices.Length; i++)
                {
                    vertices2[i] = vertices[i].position;
                    uvs2[i] = vertices[i].uv;
                }
                for (var i = 0; i < triangles.Length; i++)
                {
                    triangles2[i] = (ushort) triangles[i];
                }
                mesh.vertices = vertices2;
                mesh.uv = uvs2;
                mesh.SetTriangles(new System.Collections.Generic.List<ushort>(triangles2), 0, true, 0);*/

                /*var zoxMesh2 = new ZoxMesh();
                zoxMesh2.mesh = mesh;
                zoxMesh2.material = zoxMesh.material;
                // EntityManager.SetSharedComponentData(e, zoxMesh2);
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh2);
                UnityEngine.GameObject.Destroy(zoxMesh.mesh, 1);*/
                
                /*var go = UnityEngine.GameObject.Find("GameObject");
                if (go)
                {
                    go.GetComponent<UnityEngine.MeshFilter>().mesh = mesh;
                    go.GetComponent<UnityEngine.MeshRenderer>().material = zoxMesh.material;
                }*/

                // Make sure to link mesh

                // UnityEditor.EditorUtility.SetDirty(mesh);
                /*terrainMesh.Dispose();
                terrainMesh.vertices = new BlitableArray<ZoxVertex>(0, Allocator.Persistent);
                terrainMesh.triangles = new BlitableArray<uint>(0, Allocator.Persistent);*/