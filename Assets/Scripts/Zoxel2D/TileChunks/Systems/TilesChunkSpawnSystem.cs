using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Zoxel2D
{
    //! Spawns a tileChunk tilemap.
    /**
    *   - Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(TileSystemGroup))]
    public partial class TilesChunkSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity chunk2DPrefab;
        private Entity freshChunk2DPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var chunk2DArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(TileChunk),
                typeof(TerrainMesh),
                typeof(ZoxID),
                typeof(ParentLink),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(Translation),
                typeof(LocalToWorld),
                // typeof(DontDestroyMaterial),
                // typeof(DontDestroyTexture),
                typeof(ZoxMesh)
                // later when I add ChunkRender2Ds
                // typeof(Childrens),
                // later when I add HeightMap2D
                // typeof(SimplexNoise),
            );
            chunk2DPrefab = EntityManager.CreateEntity(chunk2DArchetype);
            freshChunk2DPrefab = EntityManager.Instantiate(chunk2DPrefab);
            EntityManager.AddComponent<Prefab>(freshChunk2DPrefab);
            EntityManager.AddComponent<Chunk2DGenerate>(freshChunk2DPrefab);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var chunk2DPrefab2 = chunk2DPrefab;
            var freshChunk2DPrefab2 = freshChunk2DPrefab;
            var chunk2DSize = new int2(256, 256);
            // var chunk2DSize = new int2(64, 64);
            // var chunk2DSize = new int2(16, 16);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<SpawnTiles, OnTilesSpawned>()
                .WithAll<SpawnChunks2D>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnChunks2D>(entityInQueryIndex, e);
                // spawn a planet 2D
                var isFreshWorld = HasComponent<GenerateWorld2D>(e);
                var prefab = freshChunk2DPrefab2;
                if (!isFreshWorld)
                {
                    prefab = chunk2DPrefab2;
                }
                var tileChunk = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, tileChunk, new ParentLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, tileChunk, new TileChunk(chunk2DSize));
                // spawn chunk 2D
                // spawn Chunk Render 2D
                // Add ChunkBuilder2D to chunk!
                // todo: add children in children set system
                /*childrens.children = new BlitableArray<Entity>(1, Allocator.Persistent);
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    childrens.children[i] = new Entity();
                }*/
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChildrenSpawned(1));
                if (isFreshWorld)
                {
                    PostUpdateCommands.RemoveComponent<GenerateWorld2D>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}