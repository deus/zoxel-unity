using Unity.Entities;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Zoxel2D
{
    //! Renders Tilemap Chunks!
    /**
    *   - Render System -
    */
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public partial class TilesRenderSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithNone<EditorMesh>()
                .WithAll<TileChunk>()
                .ForEach((Entity e, in ZoxMesh zoxMesh, in LocalToWorld localToWorld) =>
			{
                UnityEngine.Graphics.DrawMesh(zoxMesh.mesh, localToWorld.Value, zoxMesh.material, 0, null);
			}).WithoutBurst().Run();
        }
    }
}