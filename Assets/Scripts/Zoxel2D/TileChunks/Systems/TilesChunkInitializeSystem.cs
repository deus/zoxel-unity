using Unity.Entities;
using Zoxel.Rendering;
using Zoxel.Transforms;
using Zoxel.Textures;

namespace Zoxel.Zoxel2D
{
    //! Spawns a tileChunk tilemap.
    [UpdateInGroup(typeof(TileSystemGroup))]
    public partial class TilesChunkInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null) return;
            var tilesMaterial = MaterialsManager.instance.materials.tilesMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, TileChunk>()
                .ForEach((Entity e, ZoxMesh zoxMesh, ref ZoxID zoxID, in ParentLink parentLink) =>
            {
                zoxID.id = IDUtil.GenerateUniqueID();
                zoxMesh.mesh = new UnityEngine.Mesh();
                // generate quick tilemap here?
                // var entityMaterials = EntityManager.GetSharedComponentData<EntityMaterials>(parentLink.parent);

                zoxMesh.material = new UnityEngine.Material(tilesMaterial);

                //var tileLinks = EntityManager.GetComponentData<TileLinks>(parentLink.parent);
                //var tilemapTexture = EntityManager.GetComponentData<Texture>(tileLinks.tiles[0]).GetTexture();
                var tilemapTexture = EntityManager.GetComponentData<Texture>(parentLink.parent).GetTexture();
                zoxMesh.material.SetTexture("_BaseMap", tilemapTexture);

                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
                // If Editor Mesh - Create GameObject here and link to zoxmesh things
                #if UNITY_EDITOR
                if (!UnityEditor.EditorApplication.isPlaying && ZoxelEditorSettings.isEditorMeshes)
                {
                    PostUpdateCommands.AddComponent<InitializeEditorMesh>(e);
                    PostUpdateCommands.AddComponent<EditorMesh>(e);
                }
                #endif
            }).WithoutBurst().Run();
        }
    }
}