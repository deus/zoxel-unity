using Unity.Entities;

namespace Zoxel.Zoxel2D
{
	//! Holds tile data.
	public struct TileChunk : IComponentData
	{
		public ChunkData2D data;

		public TileChunk(int2 size)
		{
			data = new ChunkData2D();
			InitializeChunk2D(size);
		}

		public void DisposeChunk2D()
		{
			data.DisposeChunkData2D();
		}

		public bool InitializeChunk2D(int2 size)
		{
			return data.InitializeChunkData2D(size);
		}
	}
}