using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Zoxel2D
{
	public struct ChunkData2D
	{
		public BlitableArray<byte> tiles;
		public float2 scale;    // 1 x 1
		public int2 size;       // 16 x 16
		
		public bool InitializeChunkData2D(int2 size)
		{
			if (this.size.x != size.x || this.size.y != size.y )
			{
				DisposeChunkData2D();
				this.size = size;
				var xySize = (int)(size.x * size.y);
				tiles = new BlitableArray<byte>(xySize, Allocator.Persistent);
				return true;
			}
			return false;
		}

		public void DisposeChunkData2D()
		{
			if (tiles.Length > 0)
            {
				tiles.Dispose();
			}
		}
	}
}