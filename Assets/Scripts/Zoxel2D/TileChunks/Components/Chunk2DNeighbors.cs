using Unity.Entities;

namespace Zoxel.Zoxel2D
{
    public struct Chunk2DNeighbors : IComponentData
    {
		public Entity chunkUp;
		public Entity chunkDown;
		public Entity chunkLeft;
		public Entity chunkRight;
        // Cache neighbor tiles too?
    }
}