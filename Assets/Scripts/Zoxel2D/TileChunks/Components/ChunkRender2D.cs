using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Zoxel2D
{
	// holds reference to tile data
	public struct ChunkRender2D : IComponentData
	{
        [ReadOnly]
		public ChunkData2D data;
	}
}