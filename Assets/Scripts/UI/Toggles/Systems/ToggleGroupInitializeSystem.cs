using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! Sets the target toggle ui in a ToggleGroup.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class ToggleGroupInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<SetToggleGroup>(processQuery);
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<SpawnUIList, SpawnButtonsList, DestroyChildren>()
                .WithNone<InitializeEntity, DestroyEntity, OnChildrenSpawned>()
                .ForEach((int entityInQueryIndex, ref ToggleGroup toggleGroup, in Childrens childrens, in SetToggleGroup setToggleGroup) =>
            {
                // UnityEngine.Debug.LogError("Toggle SetToggleGroup: " + setToggleGroup.newUI);
                if (childrens.children.Length == 0)
                {
                    return;
                }
                var newToggleIndex = setToggleGroup.index;
                var targetEntity = childrens.children[newToggleIndex];
                if (targetEntity != toggleGroup.selected || !HasComponent<ToggleActivated>(targetEntity))
                {
                    //UnityEngine.Debug.LogError("Toggle Activated: " + e.Index + " - " + child.Index);
                    if (toggleGroup.selected.Index > 0) // 
                    {
                        //UnityEngine.Debug.LogError("Deselecting Old Toggle: " + toggleGroup.selected.Index); // e.Index + " - " + child.Index);
                        if (HasComponent<NavigationVisuals>(toggleGroup.selected))
                        {
                            PostUpdateCommands.AddComponent(entityInQueryIndex, toggleGroup.selected, NavigationAnimation.DeselectEvent(elapsedTime));
                        }
                        PostUpdateCommands.RemoveComponent<ToggleActivated>(entityInQueryIndex, toggleGroup.selected);
                    }
                    var oldSelected = toggleGroup.selectedIndex;
                    toggleGroup.selected = targetEntity;
                    toggleGroup.selectedIndex = newToggleIndex;
                    PostUpdateCommands.AddComponent<ToggleActivated>(entityInQueryIndex, toggleGroup.selected);
                    if (HasComponent<NavigationVisuals>(toggleGroup.selected))
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, toggleGroup.selected, NavigationAnimation.SelectEvent(elapsedTime));
                    }
                    // PostUpdateCommands.AddComponent<ToggleClickEvent>(entityInQueryIndex, toggleGroup.selected);
                    if (oldSelected != 255)
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, toggleGroup.selected, new ToggleClickEvent());
                    }
                    //UnityEngine.Debug.LogError("Selecting New Toggle: " + toggleGroup.selected.Index); // e.Index + " - " + child.Index);
                }
			}).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}