using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Removes ToggleClickEvent after use.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class ToggleClickEventEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<ToggleClickEvent>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<ToggleClickEvent>(processQuery);
        }
    }
}