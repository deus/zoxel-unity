using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! Sets the target toggle ui in a ToggleGroup.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class ToggleSetSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery toggleGroupsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            toggleGroupsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadWrite<ToggleGroup>(),
                ComponentType.ReadOnly<Childrens>());
            RequireForUpdate(processQuery);
            RequireForUpdate(toggleGroupsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var toggleGroupEntities = toggleGroupsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var toggleGroups = GetComponentLookup<ToggleGroup>(false);
            var childrens = GetComponentLookup<Childrens>(true);
            toggleGroupEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<ToggleActivated>()
                .WithAll<Toggle>()
                .ForEach((Entity e, int entityInQueryIndex, in Child child, in ParentLink parentLink, in UIClickEvent uiClickEvent) =>
            {
                var toggleGroupEntity = parentLink.parent;
                var childrens2 = childrens[toggleGroupEntity];
                // UnityEngine.Debug.LogError("Toggle SetToggleGroup: " + setToggleGroup.newUI);
                if (childrens2.children.Length == 0)
                {
                    return;
                }
                var newToggleIndex = (byte) child.index;
                var toggleGroup = toggleGroups[toggleGroupEntity];
                // if (child != toggleGroup.selected || !HasComponent<ToggleActivated>(toggleGroup.selected))
                //UnityEngine.Debug.LogError("Toggle Activated: " + e.Index + " - " + child.Index);
                if (toggleGroup.selected.Index > 0) // 
                {
                    //UnityEngine.Debug.LogError("Deselecting Old Toggle: " + toggleGroup.selected.Index); // e.Index + " - " + child.Index);
                    if (HasComponent<NavigationVisuals>(toggleGroup.selected))
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, toggleGroup.selected, NavigationAnimation.DeselectEvent(elapsedTime));
                    }
                    PostUpdateCommands.RemoveComponent<ToggleActivated>(entityInQueryIndex, toggleGroup.selected);
                }
                var oldSelected = toggleGroup.selectedIndex;
                toggleGroup.selected = e;
                toggleGroup.selectedIndex = newToggleIndex;
                toggleGroups[toggleGroupEntity] = toggleGroup;
                PostUpdateCommands.AddComponent<ToggleActivated>(entityInQueryIndex, toggleGroup.selected);
                if (HasComponent<NavigationVisuals>(toggleGroup.selected))
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, toggleGroup.selected, NavigationAnimation.SelectEvent(elapsedTime));
                }
                // PostUpdateCommands.AddComponent<ToggleClickEvent>(entityInQueryIndex, toggleGroup.selected);
                if (oldSelected != 255)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, toggleGroup.selected,
                        new ToggleClickEvent(uiClickEvent.controller, uiClickEvent.buttonType));
                }
                //UnityEngine.Debug.LogError("Selecting New Toggle: " + toggleGroup.selected.Index); // e.Index + " - " + child.Index);
			})  .WithNativeDisableContainerSafetyRestriction(toggleGroups)
                .WithReadOnly(childrens)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                // .WithNone<SpawnUIList, SpawnButtonsList, DestroyChildren>()
                // .WithNone<InitializeEntity, DestroyEntity, OnChildrenSpawned>()