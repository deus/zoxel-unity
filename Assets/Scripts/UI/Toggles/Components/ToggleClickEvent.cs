using Unity.Entities;

namespace Zoxel.UI
{
    //! The second event, which is called by the parent ToggleGroup, if toggle clicked is new.
    public struct ToggleClickEvent : IComponentData
    {
        public Entity controller;
        //! Button that triggered event.
        public byte buttonType;

        public ToggleClickEvent(Entity controller, byte buttonType)
        {
            this.controller = controller;
            this.buttonType = buttonType;
        }
    }
}