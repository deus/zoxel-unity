using Unity.Entities;

namespace Zoxel.UI
{
    //! Sets ToggleGroup's selected entity.
    public struct SetToggleGroup : IComponentData
    {
        public byte index;

        public SetToggleGroup(byte index)
        {
            this.index = index;
        }
    }
}