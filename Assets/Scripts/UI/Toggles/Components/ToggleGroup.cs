using Unity.Entities;

namespace Zoxel.UI
{
    //! A group ui tag for a list of uis that can have one activated.
    public struct ToggleGroup : IComponentData
    {
        public Entity selected;
        public byte selectedIndex;

        public ToggleGroup(byte selectedIndex)
        {
            this.selected = new Entity();
            this.selectedIndex = selectedIndex;
        }
    }
}