using Unity.Entities;

namespace Zoxel.UI
{
    //! A tag for a UIElement that can be off or on state.
    public struct Toggle : IComponentData { }
}