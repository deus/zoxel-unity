using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Textures;

namespace Zoxel.UI
{
    //! Spawns a tooltip under the panel ui.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class PanelTooltipSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity tooltipPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        private void InitializePrefab()
        {
            if (UIManager.instance == null) return;
            if (tooltipPrefab.Index != 0)
            {
                return;
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            tooltipPrefab = EntityManager.Instantiate(UICoreSystem.labelPrefab);
            EntityManager.AddComponent<Prefab>(tooltipPrefab);
            EntityManager.AddComponent<PanelTooltip>(tooltipPrefab);
            EntityManager.AddComponent<RenderTextResizePanel>(tooltipPrefab);
            EntityManager.AddComponent<TextureFrame>(tooltipPrefab);
            EntityManager.AddComponent<GenerateTextureFrame>(tooltipPrefab);
            EntityManager.RemoveComponent<RenderTextDirty>(tooltipPrefab);
            EntityManager.RemoveComponent<InitializeButtonColors>(tooltipPrefab);
            EntityManager.RemoveComponent<NewChild>(tooltipPrefab);
            EntityManager.RemoveComponent<Zoxel.Transforms.Child>(tooltipPrefab);
            var panelStyle = uiDatam.panelStyle.GetStyle();
            var iconStyle = uiDatam.iconStyle.GetStyle(uiScale);
            var fontSize = uiDatam.GetIconSize(uiScale).y / 2.6f;
            var textMargins = uiDatam.tooltipTextPadding * uiScale;
            var textColor = new Color(uiDatam.tooltipTextColor);
            var textOutlineColor = new Color(uiDatam.tooltipTextOutlineColor);
            var tooltipSize = (fontSize + textMargins.y * 2f);
            UICoreSystem.SetRenderTextData(EntityManager, tooltipPrefab, iconStyle.textGenerationData,
                textColor, textOutlineColor, fontSize, textMargins, 0);
            UICoreSystem.SetTextureFrame(EntityManager, tooltipPrefab, in panelStyle.frameGenerationData);
            EntityManager.SetComponentData(tooltipPrefab, new Size2D(tooltipSize));
            /*UICoreSystem.SetRenderTextData(EntityManager, this.iconTextPrefab, iconStyle.textGenerationData,
                iconStyle.textColor, iconStyle.textOutlineColor, iconStyle.fontSize, iconStyle.textPadding,
                iconStyle.labelAlignment, iconStyle.iconSize.x / 2f, iconStyle.fontSize);*/
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            InitializePrefab();
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var fontSize = uiDatam.GetIconSize(uiScale).y / 2.6f;
            var textMargins = uiDatam.tooltipTextPadding * uiScale;
            var tooltipSize = (fontSize + textMargins.y * 2f);
            var tooltipPrefab = this.tooltipPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithNone<SpawnHeaderUI, HeaderUIDirty, GridUIDirty>()
                .WithAll<SpawnPanelTooltip, PanelTooltipLink>()
                .ForEach((Entity e, int entityInQueryIndex, in PanelTooltipLink panelTooltipLink, in CharacterLink characterLink, in Size2D size2D) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnPanelTooltip>(entityInQueryIndex, e);
                if (size2D.size.y != 0)
                {
                    // destroy old tooltip?!
                    if (panelTooltipLink.tooltip.Index != 0)
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, panelTooltipLink.tooltip);
                    }
                    var tooltipPosition = new float3(0, -size2D.size.y / 2f - tooltipSize / 2f, 0);
                    var tooltip = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, tooltipPrefab, e, tooltipPosition); //, fontSize, panelStyle.color);
                    /*UICoreSystem.SetRenderText(PostUpdateCommands, entityInQueryIndex, tooltip, tooltipTextColor, tooltipTextOutlineColor, new Text(),
                        fontSize, textMargins, 0);*/
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e, new PanelTooltipLink(tooltip));
                    var controllerEntity = characterLink.character;
                    if (HasComponent<PlayerPanelTooltipLink>(controllerEntity))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, controllerEntity, new PlayerPanelTooltipLink(panelTooltipLink.tooltip));
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}