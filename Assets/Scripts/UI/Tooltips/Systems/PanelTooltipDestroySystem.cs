using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Using the DestroyEntity system, also destroys the panel Tooltip.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
	public partial class PanelTooltipDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyEntity, PanelTooltipLink>()
                .ForEach((Entity e, int entityInQueryIndex, in PanelTooltipLink panelTooltipLink) =>
            {
                if (HasComponent<UIElement>(panelTooltipLink.tooltip))
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, panelTooltipLink.tooltip);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}