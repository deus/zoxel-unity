using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    //! Spawns tooltips on the playerEntity.
    /**
    *   \todo Set more of the data in a prefab initialization.
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class TooltipSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        //private NativeArray<Text> texts;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            //texts = new NativeArray<Text>(1, Allocator.Persistent);
            //texts[0] = new Text("Welcome to Zoxel");
        }

        /*protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }*/

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var panelDepth = UIManager.instance.uiSettings.panelDepth;
            var uiDatam = UIManager.instance.uiDatam;
            var panelStyle = uiDatam.panelStyle.GetStyle();
            var textPadding = uiDatam.GetMenuPaddings(uiScale) / 2.4f;
            var fontSize = uiDatam.GetIconSize(uiScale).x / 4f;
            var tooltipTextColor = new Color(uiDatam.tooltipTextColor);
            var tooltipTextOutlineColor = new Color(uiDatam.tooltipTextOutlineColor);
            var labelDelay = uiDatam.actionLabelFadeTime;
            var playerTooltipPrefab = UICoreSystem.playerTooltipPrefab;
            // var texts = this.texts;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<SpawnTooltips>()
                .ForEach((Entity e, int entityInQueryIndex, in CameraLink cameraLink) =>
            {
                if (cameraLink.camera.Index == 0)
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<SpawnTooltips>(entityInQueryIndex, e);
                // Corner Tooltips
                var actionsTooltip = SpawnText(PostUpdateCommands, entityInQueryIndex, playerTooltipPrefab, e, panelStyle.color,
                    (byte) TextHorizontalAlignment.Right, TextVerticalAlignment.Bottom, (byte) AnchorUIType.BottomRight,
                   panelDepth, textPadding, fontSize, tooltipTextColor, tooltipTextOutlineColor, in cameraLink);
                var selectionTooltip = SpawnText(PostUpdateCommands, entityInQueryIndex, playerTooltipPrefab, e, panelStyle.color,
                    (byte) TextHorizontalAlignment.Left, TextVerticalAlignment.Top, (byte) AnchorUIType.TopLeft,
                   panelDepth, textPadding, fontSize, tooltipTextColor, tooltipTextOutlineColor, in cameraLink);
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new PlayerTooltipLinks(selectionTooltip, actionsTooltip));
                PostUpdateCommands.AddComponent(entityInQueryIndex, selectionTooltip, new SetRenderText(new Text()));
                PostUpdateCommands.AddComponent(entityInQueryIndex, actionsTooltip, new SetRenderText(new Text()));
                // Log UI
                /*var logEntity = SpawnText(PostUpdateCommands, entityInQueryIndex, playerTooltipPrefab, e, panelStyle.color,
                   (byte) TextHorizontalAlignment.Left, TextVerticalAlignment.Bottom, (byte) AnchorUIType.BottomLeft,
                   panelDepth, textPadding, fontSize, tooltipTextColor, tooltipTextOutlineColor, in cameraLink);
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new LogUILink(logEntity));
                PostUpdateCommands.AddComponent(entityInQueryIndex, logEntity, new SetRenderText(texts[0].Clone()));
                PostUpdateCommands.AddComponent(entityInQueryIndex, logEntity, new Fader(0, 1, 1f));
                PostUpdateCommands.AddComponent<FadeIn>(entityInQueryIndex, logEntity);
                PostUpdateCommands.AddComponent(entityInQueryIndex, logEntity, new ReverseFader(labelDelay));*/
			})  // .WithReadOnly(texts)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static Entity SpawnText(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab, Entity playerEntity,
            Color panelColor, byte horizontalAlignment, byte verticalAlignment, byte anchor,
            float panelDepth, float2 textPadding, float fontSize, Color tooltipTextColor, Color tooltipTextOutlineColor, in CameraLink cameraLink)
        {
            var tooltipEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, tooltipEntity, new MaterialBaseColor { Value = panelColor.ToFloat4() });
            PostUpdateCommands.SetComponent(entityInQueryIndex, tooltipEntity, new PanelUI(PanelType.PlayerTooltip));
            PostUpdateCommands.SetComponent(entityInQueryIndex, tooltipEntity, new UIAnchor(anchor));
            PostUpdateCommands.SetComponent(entityInQueryIndex, tooltipEntity, new UIPosition(panelDepth));
            var renderText = new RenderTextData
            {
                fontSize = fontSize, 
                horizontalAlignment = horizontalAlignment,
                verticalAlignment = verticalAlignment,
                margins = textPadding,
                color = tooltipTextColor,
                outlineColor = tooltipTextOutlineColor
            };
            PostUpdateCommands.SetComponent(entityInQueryIndex, tooltipEntity, renderText);
            PostUpdateCommands.SetComponent(entityInQueryIndex, tooltipEntity, new CharacterLink(playerEntity));
            PostUpdateCommands.SetComponent(entityInQueryIndex, tooltipEntity, cameraLink);
            return tooltipEntity;
        }
    }
}