using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Sets the PanelTooltipLink.tooltip text.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
	public partial class PanelTooltipSetSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in PanelTooltipLink panelTooltipLink, in SetPanelTooltip setPanelTooltip) =>
            {
                // UnityEngine.Debug.LogError("setPanelTooltip.text: " + setPanelTooltip.text);
                PostUpdateCommands.RemoveComponent<SetPanelTooltip>(entityInQueryIndex, e);
                if (HasComponent<RenderText>(panelTooltipLink.tooltip))
                {
                    // if clearing currently, remove, as it's going to set again
                    if (HasComponent<ClearRenderText>(panelTooltipLink.tooltip))
                    {
                        PostUpdateCommands.RemoveComponent<ClearRenderText>(entityInQueryIndex, panelTooltipLink.tooltip);
                    }
                    PostUpdateCommands.AddComponent(entityInQueryIndex, panelTooltipLink.tooltip, new SetTooltipText(setPanelTooltip.text));
                }
                else
                {
                    setPanelTooltip.text.Dispose();
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}