using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Sets the PlayerTooltip.selectionTooltip text.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class PlayerTooltipSetSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = UISystemGroup.elapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in SetPlayerTooltip setPlayerTooltip, in PlayerTooltipLinks playerTooltipLinks) =>
            {
                PostUpdateCommands.RemoveComponent<SetPlayerTooltip>(entityInQueryIndex, e);
                if (HasComponent<ClearRenderText>(playerTooltipLinks.selectionTooltip))
                {
                    PostUpdateCommands.RemoveComponent<ClearRenderText>(entityInQueryIndex, playerTooltipLinks.selectionTooltip);
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, playerTooltipLinks.selectionTooltip, new SetTooltipText(setPlayerTooltip.text));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}