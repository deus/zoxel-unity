using Unity.Entities;

namespace Zoxel.UI
{
    //! Links to a panel tooltip
    public struct PanelTooltipLink : IComponentData
    {
        public Entity tooltip;

        public PanelTooltipLink(Entity tooltip)
        {
            this.tooltip = tooltip;
        }
    }
}