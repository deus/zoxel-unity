using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    //! Links to various user UIs used for tooltips.
    public struct PlayerTooltipLinks : IComponentData
    {
        public Entity selectionTooltip;
        public Entity actionsTooltip;

        public PlayerTooltipLinks(Entity selectionTooltip, Entity actionsTooltip)
        {
            this.selectionTooltip = selectionTooltip;
            this.actionsTooltip = actionsTooltip;
        }

        public void SetTooltipText(EntityCommandBuffer PostUpdateCommands, Entity player, string tooltipText)
        {
            PostUpdateCommands.RemoveComponent<ClearRenderText>(selectionTooltip);
            PostUpdateCommands.AddComponent(selectionTooltip, new SetTooltipText(new Text(tooltipText)));
        }

        public void SetTooltipText(EntityCommandBuffer PostUpdateCommands, Entity player, Text tooltipText)
        {
            PostUpdateCommands.RemoveComponent<ClearRenderText>(selectionTooltip);
            PostUpdateCommands.AddComponent(selectionTooltip, new SetTooltipText(tooltipText));
        }

        public void SetTooltipText(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e, Text tooltipText, byte isClone)
        {
            if (isClone == 1)
            {
                tooltipText = tooltipText.Clone();
            }
            PostUpdateCommands.RemoveComponent<ClearRenderText>(entityInQueryIndex, selectionTooltip);
            PostUpdateCommands.AddComponent(entityInQueryIndex, selectionTooltip, new SetTooltipText(tooltipText));
        }

        public void SetPlayerSelectionTooltip(EntityCommandBuffer PostUpdateCommands, Entity player, string tooltipText)
        {
            PostUpdateCommands.RemoveComponent<ClearRenderText>(actionsTooltip);
            PostUpdateCommands.AddComponent(actionsTooltip, new SetTooltipText(new Text(tooltipText)));
        }

        public void SetPlayerSelectionTooltip(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Text tooltipText)
        {
            PostUpdateCommands.RemoveComponent<ClearRenderText>(entityInQueryIndex, actionsTooltip);
            PostUpdateCommands.AddComponent(entityInQueryIndex, actionsTooltip, new SetTooltipText(tooltipText));
        }

        public void ClearTexts(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex)
        {
            PostUpdateCommands.AddComponent<ClearRenderText>(entityInQueryIndex, selectionTooltip);
            PostUpdateCommands.AddComponent<ClearRenderText>(entityInQueryIndex, actionsTooltip);
        }
    }
}