using Unity.Entities;

namespace Zoxel.UI
{
    //! Sets the player tooltip using TooltipLink there.
    public struct SetTooltipText : IComponentData
    {
        public Text text;

        public SetTooltipText(Text text)
        {
            this.text = text;
        }
    }
}