using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{ 
    public struct SetPlayerTooltip : IComponentData
    {
        public Text text;

        public SetPlayerTooltip(Text text)
        {
            this.text = text;
        }
    }
}