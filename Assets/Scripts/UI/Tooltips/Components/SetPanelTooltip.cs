using Unity.Entities;

namespace Zoxel.UI
{
    //! Sets Panel Tooltip Text.
    public struct SetPanelTooltip : IComponentData
    {
        public Text text;

        public SetPanelTooltip(Text text)
        {
            this.text = text;
        }
    }
}