using Unity.Entities;

namespace Zoxel.UI
{
    public struct PlayerPanelTooltipLink : IComponentData
    {
        public Entity tooltip; // gamePanelTooltip;

        public PlayerPanelTooltipLink(Entity tooltip)
        {
            this.tooltip = tooltip;
        }
    }
}