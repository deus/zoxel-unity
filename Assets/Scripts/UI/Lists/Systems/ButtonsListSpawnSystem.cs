using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;

namespace Zoxel.UI
{
    //! Spawns a list of Buttons on a ui element.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class ButtonsListSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<SoundSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var soundSettings = GetSingleton<SoundSettings>();
            var soundVolume = soundSettings.uiSoundVolume;
            var uiDatam = UIManager.instance.uiDatam;
            var buttonStyle = uiDatam.buttonStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyChildren, PanelLink>()
                .ForEach((Entity e, int entityInQueryIndex, in GridUISize gridUISize, in SpawnButtonsList spawnButtonsList, in ListPrefabLink listPrefabLink) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnButtonsList>(entityInQueryIndex, e);
                // var fontSize = spawnButtonsList.fontSize;
                var textMargins = new float2(gridUISize.size.y / 5f, gridUISize.size.y / 10f);
                for (byte i = 0; i < spawnButtonsList.labels.Length; i++)
                {
                    var seed = (int) (128 + elapsedTime + i * 2048 + 4196 * entityInQueryIndex);
                    var buttonEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, listPrefabLink.prefab, e);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, buttonEntity, i);
                    UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, buttonEntity, e);
                    UICoreSystem.SetSound(PostUpdateCommands, entityInQueryIndex, buttonEntity, seed, soundVolume);
                    UICoreSystem.SetSeed(PostUpdateCommands, entityInQueryIndex, buttonEntity, seed);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, buttonEntity, new RenderText(spawnButtonsList.labels[i]));
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChildrenSpawned((byte) spawnButtonsList.labels.Length));
                PostUpdateCommands.AddComponent<GridUIDirty>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<NavigationDirty>(entityInQueryIndex, e);
                if (HasComponent<ToggleGroup>(e))
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetToggleGroup(0));
                }
                // This is to add menu UI Buttons on top!
                spawnButtonsList.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyChildren>()
                .ForEach((Entity e, int entityInQueryIndex, in GridUISize gridUISize, in SpawnButtonsList spawnButtonsList, in ListPrefabLink listPrefabLink, in PanelLink panelLink) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnButtonsList>(entityInQueryIndex, e);
                // var fontSize = spawnButtonsList.fontSize;
                var textMargins = new float2(gridUISize.size.y / 5f, gridUISize.size.y / 10f);
                for (int i = 0; i < spawnButtonsList.labels.Length; i++)
                {
                    var seed = (int) (128 + elapsedTime + i * 2048 + 4196 * entityInQueryIndex);
                    var buttonEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, listPrefabLink.prefab, e);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, buttonEntity, i);
                    UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, buttonEntity, panelLink.panel);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, buttonEntity, new RenderText(spawnButtonsList.labels[i]));
                    UICoreSystem.SetSound(PostUpdateCommands, entityInQueryIndex, buttonEntity, seed, soundVolume);
                    UICoreSystem.SetSeed(PostUpdateCommands, entityInQueryIndex, buttonEntity, seed);
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChildrenSpawned((byte) spawnButtonsList.labels.Length));
                PostUpdateCommands.AddComponent<GridUIDirty>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<NavigationDirty>(entityInQueryIndex, panelLink.panel);
                if (HasComponent<ToggleGroup>(e))
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetToggleGroup(0));
                }
                spawnButtonsList.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                    /*UICoreSystem.SetRenderText(PostUpdateCommands, entityInQueryIndex, buttonEntity,
                        buttonStyle.textColor, buttonStyle.textOutlineColor, buttonStyle.textGenerationData,
                        spawnButtonsList.labels[i], fontSize, spawnButtonsList.textPadding);*/
                    /*UICoreSystem.SetRenderText(PostUpdateCommands, entityInQueryIndex, buttonEntity,
                        buttonStyle.textColor, buttonStyle.textOutlineColor, buttonStyle.textGenerationData,
                        spawnButtonsList.labels[i], fontSize, spawnButtonsList.textPadding);*/