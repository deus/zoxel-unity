using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! Spawns an input group.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class UIListSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<SoundSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var soundSettings = GetSingleton<SoundSettings>();
            var elapsedTime = World.Time.ElapsedTime;
            var soundVolume = soundSettings.uiSoundVolume;
            var uiDatam = UIManager.instance.uiDatam;
            var buttonStyle = uiDatam.buttonStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyChildren>()
                .ForEach((Entity e, int entityInQueryIndex, in GridUISize gridUISize, in SpawnUIList spawnUIList, in PanelLink panelLink) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnUIList>(entityInQueryIndex, e);
                var fontSize = spawnUIList.fontSize; // gridUI.iconSize.y;
                var textMargins = new float2(gridUISize.size.y / 5f, gridUISize.size.y / 10f);
                for (int i = 0; i < spawnUIList.labels.Length; i++)
                {
                    var seed = (int) (128 + elapsedTime + i * 2048 + 4196 * entityInQueryIndex);
                    var elementPrefab = spawnUIList.labelPrefab;
                    if (i == 1)
                    {
                        elementPrefab = spawnUIList.inputPrefab;
                    }
                    else if (i == 2)
                    {
                        elementPrefab = spawnUIList.buttonPrefab;
                    }
                    var buttonEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, elementPrefab, e, float3.zero);
                    UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, buttonEntity, i);
                    UICoreSystem.SetRenderText(PostUpdateCommands, entityInQueryIndex, buttonEntity,
                        buttonStyle.textColor, buttonStyle.textOutlineColor, buttonStyle.textGenerationData,
                        spawnUIList.labels[i], fontSize, spawnUIList.textPadding);
                    if (i != 0)
                    {
                        UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, buttonEntity, panelLink.panel);
                        UICoreSystem.SetSound(PostUpdateCommands, entityInQueryIndex, buttonEntity, seed, soundVolume);
                        UICoreSystem.SetSeed(PostUpdateCommands, entityInQueryIndex, buttonEntity, seed);
                    }
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChildrenSpawned((byte)spawnUIList.labels.Length));
                PostUpdateCommands.AddComponent<GridUIDirty>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<NavigationDirty>(entityInQueryIndex, panelLink.panel);
                // This is to add menu UI Buttons on top!
                spawnUIList.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}