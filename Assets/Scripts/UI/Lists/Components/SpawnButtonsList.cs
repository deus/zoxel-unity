using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! Spawns a list of labels.
    public struct SpawnButtonsList : IComponentData
    {
        public BlitableArray<Text> labels;

        public void Dispose()
        {
            labels.Dispose();
        }

        public SpawnButtonsList(in NativeArray<Text> labels, byte cloneTexts = 1)
        {
            if (cloneTexts == 1)
            {
                this.labels = new BlitableArray<Text>(labels.Length, Allocator.Persistent);
                for (int i = 0; i < this.labels.Length; i++)
                {
                    this.labels[i] = labels[i].Clone();
                }
            }
            else
            {
                this.labels = new BlitableArray<Text>(labels, Allocator.Persistent);
            }
        }

        public SpawnButtonsList(in NativeList<Text> labels, byte cloneTexts = 1)
        {
            if (cloneTexts == 1)
            {
                this.labels = new BlitableArray<Text>(labels.Length, Allocator.Persistent);
                for (int i = 0; i < this.labels.Length; i++)
                {
                    this.labels[i] = labels[i].Clone();
                }
            }
            else
            {
                this.labels = new BlitableArray<Text>(labels, Allocator.Persistent);
            }
        }
    }
}