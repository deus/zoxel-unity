using Unity.Entities;

namespace Zoxel.UI
{
    //! Links to a prefab used in list initialization.
    public struct ListPrefabLink : IComponentData
    {
        public Entity prefab;

        public ListPrefabLink(Entity prefab)
        {
            this.prefab = prefab;
        }
    }
}