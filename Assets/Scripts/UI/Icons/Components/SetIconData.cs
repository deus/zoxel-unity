using Unity.Entities;

namespace Zoxel.UI
{
    //! Sets the target data to a UserItem or UserSkill. Note, I could just use filter change to trigger UI changes?
    public struct SetIconData : IComponentData
    {
        public Entity data;

        public SetIconData(Entity data)
        {
            this.data = data;
        }
    }
}