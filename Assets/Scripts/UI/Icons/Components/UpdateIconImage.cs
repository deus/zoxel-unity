using Unity.Entities;

namespace Zoxel.UI
{
    //! Used to independendly update the icon image.
    public struct UpdateIconImage : IComponentData { }
}