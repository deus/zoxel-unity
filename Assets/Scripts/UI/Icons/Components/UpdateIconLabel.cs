using Unity.Entities;

namespace Zoxel.UI
{
    //! Used to independendly update the icon label.
    public struct UpdateIconLabel : IComponentData { }
}