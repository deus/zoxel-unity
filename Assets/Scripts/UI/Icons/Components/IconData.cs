using Unity.Entities;

namespace Zoxel.UI
{
    //! The target data to a UserItem or UserSkill.
    public struct IconData : IComponentData
    {
        public Entity data;

        public IconData(Entity data)
        {
            this.data = data;
        }
    }
}