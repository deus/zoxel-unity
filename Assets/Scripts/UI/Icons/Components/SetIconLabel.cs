using Unity.Entities;

namespace Zoxel.UI
{
    public struct SetIconLabel : IComponentData
    {
        public Text text;

        public SetIconLabel(Text text)
        {
            this.text = text;
        }
    }
}