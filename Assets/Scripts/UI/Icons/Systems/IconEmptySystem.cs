using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Textures;

namespace Zoxel.UI
{
    //! Sets the Icon and label to empty.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class IconEmptySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // Clear Icon
            Dependency = Entities
                .WithNone<DelayEventFrames, InitializeEntity>()
                .WithAll<SetIconData>()
                .ForEach((Entity e, int entityInQueryIndex, ref IconData iconData, in SetIconData setIconData, in Childrens childrens) =>
            {
                // if (iconData.data == setIconData.data || setIconData.data.Index != 0)
                if (setIconData.data.Index != 0)
                {
                    return;
                }
                // UnityEngine.Debug.LogError("Set to Blank Icon!" + e.Index);
                iconData.data = setIconData.data;
                if (childrens.children.Length >= 1)
                {
                    var iconEntity = childrens.children[0];
                    PostUpdateCommands.AddComponent(entityInQueryIndex, iconEntity, new GenerateEmptyIcon(32, 1));
                }
                if (childrens.children.Length >= 2)
                {
                    var textEntity = childrens.children[1];
                    PostUpdateCommands.AddComponent<ClearRenderText>(entityInQueryIndex, textEntity);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}