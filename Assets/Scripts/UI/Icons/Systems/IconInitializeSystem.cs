using Unity.Entities;
using Zoxel.Transforms;
using Zoxel.Textures;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    //! Creates Icon mesh and material.
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class IconInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var iconMaterial = UIManager.instance.materials.iconMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, Icon>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in UIMeshData uiMeshData, in Size2D size2D, in ParentLink parentLink) =>
            {
                var size = size2D.size;
                if (!HasComponent<AuthoredTexture>(e))
                {
                    UnityEngine.Material material;
                    material = new UnityEngine.Material(iconMaterial);
                    if (!HasComponent<DontDestroyTexture>(e))
                    {
                        material.SetTexture("_BaseMap", TextureUtil.CreateBlankTexture());
                    }
                    zoxMesh.material = material;
                }
                zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size, uiMeshData.horizontalAlignment, uiMeshData.verticalAlignment, uiMeshData.offset);
                zoxMesh.layer = worldUILayer;
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
        }
    }
}