using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{ 
    //! Removes SetIconData event after use.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class IconDataSetEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DelayEventFrames>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.ReadOnly<SetIconData>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<SetIconData>(processQuery);
        }
    }
}