using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! Sets the RenderText of an icon's label.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class SetIconLabelSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in SetIconLabel setIconLabel, in Childrens childrens) =>
            {
                if (childrens.children.Length > 1)
                {
                    var textEntity = childrens.children[1];
                    PostUpdateCommands.AddComponent(entityInQueryIndex, textEntity, setIconLabel);
                }
                PostUpdateCommands.RemoveComponent<SetIconLabel>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref RenderText renderText, in SetIconLabel setIconLabel) =>
            {
                if (renderText.SetText(setIconLabel.text))
                {
                    PostUpdateCommands.AddComponent<RenderTextDirty>(entityInQueryIndex, e);
                }
                PostUpdateCommands.RemoveComponent<SetIconLabel>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}