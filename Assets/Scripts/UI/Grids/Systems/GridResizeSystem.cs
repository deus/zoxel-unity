using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Textures;

namespace Zoxel.UI
{
    //! Sets all grid element positions.
    /**
    *   \todo Handle A UIElement that has multiple lines! base panel size and spacing per element over real ui sizes instead of GridUI.iconSize.
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class GridUIResizeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, OnChildrenSpawned, DestroyChildren>()
                .WithAll<GridUIDirty, AutoGridY>()
                .ForEach((ref GridUI gridUI, in Childrens childrens, in GridUIDirty gridUIDirty) =>
            {
                gridUI.gridSize = new int2(1, childrens.children.Length);
            }).ScheduleParallel(Dependency);
            Dependency = Entities
                .WithNone<InitializeEntity, OnChildrenSpawned, DestroyChildren>()
                .WithAll<GridUIDirty, AutoGridX>()
                .ForEach((ref GridUI gridUI, in Childrens childrens) =>
            {
                gridUI.gridSize = new int2(childrens.children.Length, 1);
            }).ScheduleParallel(Dependency);
            Dependency = Entities
                .WithNone<OnChildrenSpawned, DestroyChildren, MeshUIDirty>()
                .WithNone<DisableGridResize>()
                .WithAll<GridUIDirty, PanelUI>()
                .ForEach((ref Size2D size2D, in GridUI gridUI, in GridUISize gridUISize) =>
            {
                size2D.size = gridUI.GetSize(gridUISize.size);
            }).ScheduleParallel(Dependency);
        }
    }

    //! Sets all grid element positions.
    [UpdateAfter(typeof(GridUIDirtySystem))]
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class GridUIResizeSystem2 : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<OnChildrenSpawned, DestroyChildren, MeshUIDirty>()
                .WithNone<DisableGridResize>()
                .WithAll<GridUIDirty, PanelUI>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.AddComponent<MeshUIDirty>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<OnChildrenSpawned, DestroyChildren, MeshUIDirty>()
                .WithNone<DisableGridResize>()
                .WithAll<GridUIDirty, PanelUI, TextureFrame>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.AddComponent<GenerateTextureFrame>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}