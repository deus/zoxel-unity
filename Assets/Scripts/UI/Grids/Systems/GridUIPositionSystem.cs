using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! Positions ui elements in the grid.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class GridUIPositionSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery gridUIQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            gridUIQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<GridUI>());
            RequireForUpdate(processQuery);
            RequireForUpdate(gridUIQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands.RemoveComponent<SetGridPosition>(processQuery);
            PostUpdateCommands.AddComponent<SetUIElementPosition>(processQuery);
            var gridUIEntities = gridUIQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var gridUIs = GetComponentLookup<GridUI>(true);
            var gridUISizes = GetComponentLookup<GridUISize>(true);
            var gridUIStackTypes = GetComponentLookup<GridUIStackType>(true);
            gridUIEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref LocalPosition localPosition, in ParentLink parentLink, in SetGridPosition setGridPosition) => 
            {
                if (!gridUIs.HasComponent(parentLink.parent))
                {
                    return;
                }
                var gridUIStackType = (byte) 0;
                if (gridUIStackTypes.HasComponent(parentLink.parent))
                {
                    gridUIStackType = gridUIStackTypes[parentLink.parent].type;
                }
                var gridUI = gridUIs[parentLink.parent];
                var gridUISize = gridUISizes[parentLink.parent];
                localPosition.position = GetGridPosition(setGridPosition.position, gridUI.gridSize, gridUISize.size, gridUI.margins, gridUI.padding, gridUIStackType);
                // UnityEngine.Debug.LogError("Positioned [" + entityInQueryIndex + "] " + localPosition.position);
            })  .WithReadOnly(gridUIs)
                .WithReadOnly(gridUIStackTypes)
                .ScheduleParallel();
        }

        public static float3 GetGridPosition(int2 position, int2 gridSize, float2 iconSize, float2 margins, float2 padding, byte gridPositionType = 0)
        {
            var panelSize = GetGridPanelSize(gridSize, iconSize, margins, padding);
            if (gridPositionType == GridPositionType.StackUp)
            {
                // UnityEngine.Debug.LogError("GetGridPosition position.y: " + position.y);
                return new float3(
                    margins.x + position.x * (iconSize.x + padding.x) + (iconSize.x / 2f) - (panelSize.x / 2f),
                    margins.y + position.y * (iconSize.y + padding.y) + (iconSize.y / 2f) - (panelSize.y / 2f),
                    0);
            }
            if (gridSize.x == 1 && gridSize.y == 1)
            {
                return new float3(); // new float3(margins.x, -margins.y, 0);
            }
            else if (gridSize.x == 1)
            {
                // margins.x
                return new float3(
                    0,
                    -margins.y - position.y * (iconSize.y + padding.y) + (panelSize.y / 2f) - (iconSize.y / 2f),
                    0);
            }
            else if (gridSize.y == 1)
            {
                return new float3(margins.x + position.x * (iconSize.x + padding.x) - (panelSize.x / 2f) + (iconSize.x / 2f), 0, 0);
                // -margins.y, // - position.y * (iconSize.y + padding.y),
            }
            else
            {
                return new float3(
                    margins.x + position.x * (iconSize.x + padding.x) - (panelSize.x / 2f) + (iconSize.x / 2f),
                    -margins.y - position.y * (iconSize.y + padding.y) + (panelSize.y / 2f) - (iconSize.y / 2f),
                    0);
            }
        }

        protected static float2 GetGridPanelSize(int2 gridSize, float2 iconSize, float2 margins, float2 padding)
        {
            return new float2(gridSize.x * iconSize.x + (gridSize.x - 1) * padding.x + margins.x * 2f,
                gridSize.y * iconSize.y + (gridSize.y - 1) * padding.y + margins.y * 2f);
        }
    }
}