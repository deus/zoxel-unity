﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! Sets all grid element positions.
    /**
    *   \todo Pass in UI children and position directly? Add and remove components based on that. Condense into a single event.
    *       Reposition localPosition's in this function instead of creating a new one.
    */
    [UpdateAfter(typeof(GridUIResizeSystem))]
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class GridUIDirtySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery uisQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            uisQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<IgnoreGrid>(),
                ComponentType.ReadOnly<UIElement>(),
                ComponentType.ReadWrite<LocalPosition>(),
                ComponentType.ReadOnly<ParentLink>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<GridUIDirty>(processQuery);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uiEntities = uisQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var localPositions = GetComponentLookup<LocalPosition>(false);
            var ignoreGrids = GetComponentLookup<IgnoreGrid>(true);
            uiEntities.Dispose();
            var processEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var gridUIStackTypes = GetComponentLookup<GridUIStackType>(true);
            processEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithNone<OnChildrenSpawned, DestroyChildren>()
                .WithAll<GridUIDirty>()
                .ForEach((Entity e, int entityInQueryIndex, in GridUI gridUI, in GridUISize gridUISize, in Childrens childrens) =>
            {
                if (gridUI.gridSize.x == 0 || gridUI.gridSize.y == 0)
                {
                    return;
                }
                var gridUIStackType = (byte) 0;
                if (gridUIStackTypes.HasComponent(e))
                {
                    gridUIStackType = gridUIStackTypes[e].type;
                }
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    // set positions up!
                    var childEntity = childrens.children[i];
                    if (!localPositions.HasComponent(childEntity) || ignoreGrids.HasComponent(childEntity))
                    {
                        continue;
                    }
                    int indexX = i % ((int) gridUI.gridSize.x);
                    int indexY = i / ((int) gridUI.gridSize.x);
                    if (gridUIStackType == GridPositionType.StackUp)
                    {
                        indexY = gridUI.gridSize.y - 1 - indexY;
                    }
                    var position = new int2(indexX, indexY);
                    var localPosition = GridUIPositionSystem.GetGridPosition(position, gridUI.gridSize, gridUISize.size, gridUI.margins, gridUI.padding, gridUIStackType);
                    localPositions[childEntity] = new LocalPosition(localPosition);
                    PostUpdateCommands.AddComponent<SetUIElementPosition>(entityInQueryIndex, childEntity);
                    // PostUpdateCommands.AddComponent(entityInQueryIndex, child, new SetGridPosition(position));
                }
            })  .WithNativeDisableContainerSafetyRestriction(localPositions)
                .WithReadOnly(ignoreGrids)
                .WithReadOnly(gridUIStackTypes)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}