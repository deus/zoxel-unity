using Unity.Entities;

namespace Zoxel.UI
{
    public class GridPositionType
    {
        public const byte Center = 0;
        public const byte StackUp = 1;
    }
    public struct GridUIStackType : IComponentData
    {
        public byte type;

        public GridUIStackType(byte type)
        {
            this.type = type;
        }
    }
}