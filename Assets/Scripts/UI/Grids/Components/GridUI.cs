﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! Positions elements inside a grid format.
    public struct GridUI : IComponentData
    {
        public int2 gridSize;   // 3 x 3
        public float2 margins;
        public float2 padding;

        public GridUI(int2 gridSize)
        {
            this.gridSize = gridSize;
            this.margins = float2.zero;
            this.padding = float2.zero;
        }

        public GridUI(float2 padding, float2 margins)
        {
            this.gridSize = int2.zero;
            this.margins = margins;
            this.padding = padding;
        }

        public GridUI(int2 gridSize, float2 padding, float2 margins)
        {
            this.gridSize = gridSize;
            this.margins = margins;
            this.padding = padding;
        }

        public float2 GetSize(float2 iconSize)
        {
            return new float2(gridSize.x * iconSize.x + (gridSize.x - 1) * padding.x + margins.x * 2f,
                gridSize.y * iconSize.y + (gridSize.y - 1) * padding.y + margins.y * 2f);
        }
    }
}