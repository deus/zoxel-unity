using Unity.Entities;

namespace Zoxel.UI
{
    //! An event for updating GridUI.
    public struct GridUIDirty : IComponentData { }
}