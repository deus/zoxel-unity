using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! The size of each GridUI element.
    public struct GridUISize : IComponentData
    {
        public float2 size;   // 3 x 3

        public GridUISize(float2 size)
        {
            this.size = size;
        }
    }
}