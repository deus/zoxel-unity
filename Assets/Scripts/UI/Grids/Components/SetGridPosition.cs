using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! Sets a UIElement in a grid position.
    public struct SetGridPosition : IComponentData
    {
        public int2 position;

        public SetGridPosition(int2 position)
        {
            this.position = position;
        }
    }
}