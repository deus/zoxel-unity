using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Cameras;

namespace Zoxel.UI
{
    //! Repositions ui elements into a grid.
    /**
    *   \todo Make this also dynamic if local position changes.
    *   \todo Seperate UIElement from Raycaster2DOrigin components.
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class ElementPositionSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
		private EntityQuery panelsQuery;
		private EntityQuery parentsQuery;
		private EntityQuery camerasQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        	panelsQuery = GetEntityQuery(
                ComponentType.ReadOnly<PanelUI>(),
                ComponentType.ReadOnly<CameraLink>());
        	parentsQuery = GetEntityQuery(
                ComponentType.ReadOnly<LocalPosition>(),
                ComponentType.ReadOnly<ParentLink>());
        	camerasQuery = GetEntityQuery(ComponentType.ReadOnly<Camera>());
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var panelEntities = panelsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
			var panels = GetComponentLookup<PanelUI>(true);
			var panelPositions = GetComponentLookup<PanelPosition>(true);
			var uiAnchors = GetComponentLookup<UIAnchor>(true);
			var uiPositions = GetComponentLookup<UIPosition>(true);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
			var panelUIElements = GetComponentLookup<Size2D>(true);
            panelEntities.Dispose();
            var parentEntities = parentsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var parentLocalPositions = GetComponentLookup<LocalPosition>(true);
			var parentLinks = GetComponentLookup<ParentLink>(true);
            parentEntities.Dispose();
            var cameraEntities = camerasQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var cameras = GetComponentLookup<Camera>(true);
            cameraEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity>()
                .WithAll<SetUIElementPosition>()
                .ForEach((Entity e, int entityInQueryIndex, ref RaycastUIOrigin raycastUIOrigin, in LocalPosition localPosition, in ParentLink parentLink) =>
            {
                var parentEntity = parentLink.parent;
                var count = 0;
                var orbitPosition = new float3();
                if (parentLocalPositions.HasComponent(parentEntity))
                {
                    while (count < Zoxel.Transforms.TransformSystemGroup.maxParents && parentLocalPositions.HasComponent(parentEntity))
                    {
                        parentEntity = parentLinks[parentEntity].parent;
                        count++;
                    }
                }
                // get final parent as panel
                if (!panels.HasComponent(parentEntity))
                {
                    // UnityEngine.Debug.LogError("Panels not found in ElementPositionSystem v1");
                    PostUpdateCommands.RemoveComponent<SetUIElementPosition>(entityInQueryIndex, e);
                    return;
                }
                var uiPosition = uiPositions[parentEntity];
                var cameraEntity = cameraLinks[parentEntity].camera;
                if (!cameras.HasComponent(cameraEntity))
                {
                    // UnityEngine.Debug.LogError("Camera not found in ElementPositionSystem v2");
                    PostUpdateCommands.RemoveComponent<SetUIElementPosition>(entityInQueryIndex, e);
                    return;
                }
                PostUpdateCommands.RemoveComponent<SetUIElementPosition>(entityInQueryIndex, e);
                var camera = cameras[cameraEntity];
                var anchor = (byte) 0;
                if (uiAnchors.HasComponent(parentEntity))
                {
                    anchor = uiAnchors[parentEntity].anchor;
                }
                orbitPosition = AnchorPresets.TransposePosition(uiPosition.position, anchor);
                var frustrumSize = camera.GetFrustrumSize();
                // add parents
                var parent = parentLink.parent;
                count = 0;
                while (count < Zoxel.Transforms.TransformSystemGroup.maxParents && parentLocalPositions.HasComponent(parent))
                {
                    var parentSynchPosition = parentLocalPositions[parentLink.parent].position;
                    var localPosition2 = new float3(parentSynchPosition.x / frustrumSize.x, parentSynchPosition.y / frustrumSize.y, 0);
                    orbitPosition += localPosition2;
                    if (parentLinks.HasComponent(parent))
                    {
                        parent = parentLinks[parent].parent;
                    }
                    else
                    {
                        parent = new Entity();
                    }
                    count++;
                }
                var localPosition3 = new float2(localPosition.position.x / frustrumSize.x, localPosition.position.y / frustrumSize.y);
                var panelSize = panelUIElements[parentEntity].size;
                var panelPosition = float3.zero;
                if (panelPositions.HasComponent(parentEntity))
                {
                    panelPosition = panelPositions[parentEntity].GetPositionOffset(panelSize, anchor);
                }
                else
                {
                    panelPosition = PanelPosition.GetPosition(panelSize, anchor);
                }
                localPosition3 += new float2(panelPosition.x / frustrumSize.x, panelPosition.y / frustrumSize.y);
                raycastUIOrigin.position = new float2(orbitPosition.x, orbitPosition.y) + localPosition3;
                // UnityEngine.Debug.LogError("raycastUIOrigin.position set to: " + raycastUIOrigin.position + " e.Index: " + e.Index);
            })  .WithReadOnly(panels)
                .WithReadOnly(panelPositions)
                .WithReadOnly(uiAnchors)
                .WithReadOnly(uiPositions)
                .WithReadOnly(panelUIElements)
                .WithReadOnly(cameraLinks)
                .WithReadOnly(parentLocalPositions)
                .WithReadOnly(parentLinks)
                .WithReadOnly(cameras)
                .WithNativeDisableContainerSafetyRestriction(panelUIElements)
                .WithNativeDisableContainerSafetyRestriction(parentLocalPositions)
			    .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}