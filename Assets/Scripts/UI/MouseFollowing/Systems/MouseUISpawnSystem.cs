using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.UI.MouseFollowing
{
    //! Spawns a MouseUI that follows the mouse and displays an icon frame.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
	public partial class MouseUISpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public IconPrefabs iconPrefabs;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        private void InitializePrefabs()
        {
            if (iconPrefabs.frame.Index != 0 || UICoreSystem.invisibleIconFramePrefab.Index == 0)
            {
                return;
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var iconStyle = uiDatam.iconStyle.GetStyle(uiScale);
            iconPrefabs.frame = EntityManager.Instantiate(UICoreSystem.invisibleIconFramePrefab);
            EntityManager.AddComponent<Prefab>(this.iconPrefabs.frame);
            EntityManager.AddComponent<MetaData>(this.iconPrefabs.frame);
            EntityManager.AddComponent<CharacterLink>(this.iconPrefabs.frame);
            // EntityManager.AddComponent<FollowMaterial>(this.iconPrefabs.frame);
            EntityManager.AddComponent<MouseUI>(this.iconPrefabs.frame);
            EntityManager.RemoveComponent<LocalPosition>(this.iconPrefabs.frame);
            EntityManager.RemoveComponent<LocalRotation>(this.iconPrefabs.frame);
            EntityManager.RemoveComponent<ParentLink>(this.iconPrefabs.frame);
            EntityManager.RemoveComponent<Zoxel.Transforms.Child>(this.iconPrefabs.frame);
            EntityManager.RemoveComponent<NewChild>(iconPrefabs.frame);
            // EntityManager.SetComponentData(iconPrefabs.frame, new RenderQueue(UICoreSystem.iconQueue + 9));
            iconPrefabs.icon = EntityManager.Instantiate(UICoreSystem.iconPrefabs.icon);
            EntityManager.AddComponent<Prefab>(iconPrefabs.icon);
            // EntityManager.AddComponent<FollowMaterial>(iconPrefabs.icon);
            EntityManager.AddComponent<MouseIcon>(iconPrefabs.icon);
            EntityManager.SetComponentData(iconPrefabs.icon, new RenderQueue(UICoreSystem.iconQueue + 10));
            // iconPrefabs.label = UICoreSystem.iconPrefabs.label;
            iconPrefabs.label = EntityManager.Instantiate(UICoreSystem.iconPrefabs.label);
            EntityManager.AddComponent<Prefab>(iconPrefabs.label);
            // EntityManager.AddComponent<FollowMaterial>(iconPrefabs.label);
            EntityManager.SetComponentData(iconPrefabs.label, new RenderQueue(UICoreSystem.iconLabelQueue + 10));
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var iconPrefabs = this.iconPrefabs;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<SpawnMouseUI>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnMouseUI>(entityInQueryIndex, e);
                var frameEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.frame);
                UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, frameEntity, e);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e, new MouseUILink(frameEntity));
                UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.icon, frameEntity);
                UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.label, frameEntity);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}