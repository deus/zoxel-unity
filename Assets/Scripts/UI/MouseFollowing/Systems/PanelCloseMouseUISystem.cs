using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI.MouseFollowing
{
    //! When closing Panel's - Closes the MouseUI.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class PanelCloseMouseUISystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<HideMouseUI>()
                .WithAll<FollowingMouse>()
                .ForEach((Entity e, int entityInQueryIndex, in MouseUI mouseUI) =>
            {
                if (!HasComponent<PanelUI>(mouseUI.panelUI))
                {
                    PostUpdateCommands.AddComponent<HideMouseUI>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}