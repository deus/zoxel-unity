using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Input;
using Zoxel.Cameras;

namespace Zoxel.UI.MouseFollowing
{
    //! Moves the MouseUI to follow the mouse position through a camera.
    /**
    *   \todo Remove FaceCamera from mouse ui - to stop shaking. Or FaceCameraFromAxis - face only where inventory would face - align to it's position.
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
	public partial class MouseFollowUISystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery controllersQuery;
        private EntityQuery camerasQuery;

        protected override void OnCreate()
        {
            controllersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Controller>(),
                ComponentType.ReadOnly<LocalToWorld>());
            camerasQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Camera>(),
                ComponentType.ReadOnly<LocalToWorld>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var controllers = GetComponentLookup<Controller>(true);
            var deviceTypeDatas = GetComponentLookup<DeviceTypeData>(true);
            controllerEntities.Dispose();
            var cameraEntities = camerasQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var cameras = GetComponentLookup<Camera>(true);
            var cameraScreenRects = GetComponentLookup<CameraScreenRect>(true);
            var localToWorlds = GetComponentLookup<LocalToWorld>(true);
            var cameraProjectionMatrixs = GetComponentLookup<CameraProjectionMatrix>(true);
            cameraEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((ref Translation position, in CharacterLink characterLink, in FollowingMouse followingMouse) =>
            {
                var controllerEntity = characterLink.character;
                if (!controllers.HasComponent(controllerEntity))
                {
                    return;
                }
                var controller = controllers[controllerEntity];
                var deviceTypeData = deviceTypeDatas[controllerEntity];
                var cameraEntity = followingMouse.camera;
                if (!cameras.HasComponent(cameraEntity))
                {
                    return;
                }
                var camera = cameras[cameraEntity];
                var cameraScreenRect = cameraScreenRects[cameraEntity];
                var screenDimensions = camera.screenDimensions.ToFloat2();
                var mousePosition = controller.mouse.GetPointer(screenDimensions, cameraScreenRect.rect);
                // Gives mouse position between 0 and 1 for camera.
                if (deviceTypeData.type == DeviceType.Gamepad)
                {
                    mousePosition = new float2(0.06f, 0.5f);    // gamepad will just show mouse on left side of screen
                }
                var screenPosition = new float3(mousePosition.x, mousePosition.y, followingMouse.uiDepth);
                var cameraLocalToWorld = localToWorlds[cameraEntity];
                float4x4 cameraLocalToWorld2 = cameraLocalToWorld.Value;
                var projectionMatrix = cameraProjectionMatrixs[cameraEntity].projectionMatrix;
                // Flip z for camera matrix
                position.Value = CameraUtil.ScreenToWorldPoint(screenPosition, cameraLocalToWorld2, projectionMatrix);
            })  .WithReadOnly(cameras)
                .WithReadOnly(cameraScreenRects)
                .WithReadOnly(localToWorlds)
                .WithReadOnly(cameraProjectionMatrixs)
                .WithReadOnly(controllers)
                .WithReadOnly(deviceTypeDatas)
                .ScheduleParallel();
            Entities.ForEach((ref Rotation rotation, in FollowingMouse followingMouse) =>
            {
                rotation.Value = localToWorlds[followingMouse.camera].Rotation;
            })  .WithReadOnly(localToWorlds)
                .ScheduleParallel();
        }
    }
}

// var cameraPosition = cameraLocalToWorld.Position;

//var direction = math.normalize(projectedPosition - cameraPosition);
//position.Value = cameraPosition + direction * uiDepth;
// UnityEngine.Debug.DrawLine(localToWorlds[cameraEntity].Position, position.Value, UnityEngine.Color.red, 1f);

// float4x4 projectionMatrix = CameraReferences.GetMainCamera(EntityManager).projectionMatrix;

/*public static float3 ScreenToWorldPoint(float3 screenPosition, float4x4 cameraToWorldMatrix, float4x4 projectionMatrix)
{
    var clipPosition = new float3((screenPosition.x * 2.0f) - 1.0f, (2.0f * screenPosition.y) - 1.0f, screenPosition.z);
    var viewPosition = math.transform(math.inverse(projectionMatrix), clipPosition);
    return math.transform(cameraToWorldMatrix, viewPosition);
}*/

// var unityCamera = EntityManager.GetSharedComponentData<GameObjectLink>(cameraEntity).gameObject.GetComponent<UnityEngine.Camera>();
// float4x4 cameraToWorldMatrix = unityCamera.cameraToWorldMatrix;
// var unityPosition = unityCamera.ScreenToWorldPoint(new float3(controller.mouse.pointer.x, controller.mouse.pointer.y, uiDepth));
// position.Value = unityPosition;


// [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
// todo Pass in controller
// Use cameraLink for camera
// use ScreenToWorldPoint with my own calculation

// float4x4 projectionMatrix = unityCamera.projectionMatrix;

   
// float4x4 projectionMatrix = unityCamera.projectionMatrix;
                
// UnityEngine.Debug.LogError("CalculatedPosition: " + realPosition + " - unityPosition: " + unityPosition);
/*UnityEngine.Debug.LogError("Matrix Equal? " + ((UnityEngine.Matrix4x4)cameraToWorldMatrix == (UnityEngine.Matrix4x4)cameraLocalToWorld));
UnityEngine.Debug.LogError("    cameraToWorldMatrix:\n" + ((UnityEngine.Matrix4x4) cameraToWorldMatrix));
UnityEngine.Debug.LogError("    cameraLocalToWorld:\n" + ((UnityEngine.Matrix4x4) cameraLocalToWorld));*/
// UnityEngine.Debug.LogError("    projectionMatrix:\n" + ((UnityEngine.Matrix4x4) projectionMatrix));

/*cameraLocalToWorld[8] *= -1;
cameraLocalToWorld[9] *= -1;
cameraLocalToWorld[10] *= -1;*/

/*public static float3 ScreenToWorldRay(float3 screenPosition, float4x4 cameraToWorldMatrix, float4x4 projectionMatrix, float3 cameraPosition) // float4x4 localToWorldMatrix)
{
    // float4 clipSpace = new float4((screenPosition.x * 2.0f) - 1.0f, (2.0f * screenPosition.y) - 1.0f, 0, 1.0f);
    var clipSpace = new float3((screenPosition.x * 2.0f) - 1.0f, (2.0f * screenPosition.y) - 1.0f, 0f);
    var viewSpace = math.transform(math.inverse(projectionMatrix), clipSpace);
    // viewSpace /= viewSpace.w;
    var worldSpace = math.transform(cameraToWorldMatrix, viewSpace);
    return math.normalize(worldSpace - cameraPosition);
    // return math.normalize(worldSpace.xyz - cameraPosition);
}*/
// float3 cameraOrigin = unityCamera.transform.position;
// float4x4 worldToCameraMatrix = unityCamera.worldToCameraMatrix;
/*
var rayDirection = MouseFollowUISystem.ScreenToWorldRay(screenPosition, cameraToWorldMatrix, projectionMatrix, unityCamera.transform.position);
var realPosition = cameraOrigin + uiDepth * rayDirection;
position.Value = realPosition;*/
// float4 clipSpace = new float4(((screenPosition.x * 2.0f)) - 1.0f, (1.0f - (2.0f * screenPosition.y)), 0, 1.0f);
// return worldSpace.xyz;
// return math.transform(math.inverse(localToWorldMatrix), worldSpace.xyz);

/*var world2Screen = math.mul(math.mul(projectionMatrix, worldToCameraMatrix), localToWorldMatrix);
var depth = math.transform(world2Screen, screenPosition).z;
// viewport pos (0 ,1)// screenPosition = new float3(screenPosition.x, screenDimensions.y - screenPosition.y, screenPosition.z);
var viewPos = screenPosition;
viewPos.z = (depth + 1) / 2f;
var clipSpace = viewPos * 2f - new float3(1);
return math.transform(math.inverse(math.mul(projectionMatrix, worldToCameraMatrix)), clipSpace);*/

/*var screenPoint = new float3(mousePosition.x, mousePosition.y, -uiDepth);
float4x4 projectionMatrix = math.inverse(unityCamera.worldToCameraMatrix);
var realPosition = math.transform(projectionMatrix, screenPoint);*/

//float4x4 cameraToWorld = math.inverse(unityCamera.cameraToWorldMatrix);
//float4x4 projectionInverse = math.inverse(unityCamera.projectionMatrix);

//screenPoint.x /= (float) camera.screenDimensions.x;
//screenPoint.y /= (float) camera.screenDimensions.y;
// var realPosition = unityCamera.ScreenToWorldPoint(screenPoint);
// float4x4 projectionMatrix = math.inverse(unityCamera.projectionMatrix);
// float4x4 projectionMatrix = (unityCamera.projectionMatrix);
// float4x4 projectionMatrix = math.mul(math.inverse(unityCamera.worldToCameraMatrix), (unityCamera.projectionMatrix));