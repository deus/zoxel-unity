using Unity.Entities;

namespace Zoxel.UI.MouseFollowing
{
    //! A component used for a mouse ui. Used for dragging ui elements.
    public struct MouseUI : IComponentData
    {
        //! Character or Chest entity. Where is inventory/equipment data is kept kept.
        public Entity dataEntity;
        //! The panel entity of the button entity clicked.
        public Entity panelUI;
        //! The button entity clicked.
        public Entity button;
        //! The button index clicked.
        public Entity userEntity;

        public MouseUI(Entity dataEntity, Entity panelUI, Entity button, Entity userEntity)
        {
            this.dataEntity = dataEntity;
            this.panelUI = panelUI;
            this.button = button;
            this.userEntity = userEntity;
        }
    }
}