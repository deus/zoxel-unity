using Unity.Entities;

namespace Zoxel.UI.MouseFollowing
{
    public struct MouseUILink : IComponentData
    {
        public Entity mouseUI;
        
        public MouseUILink(Entity mouseUI)
        {
            this.mouseUI = mouseUI;
        }
    }
}