using Unity.Entities;

namespace Zoxel.UI.MouseFollowing
{
    // I should update material in state 0
    public struct FollowingMouse : IComponentData
    {
        public Entity camera;        // yeah probaly neeed this, camera link tho?
        public float uiDepth;

        public FollowingMouse(Entity camera, float uiDepth)
        {
            this.camera = camera;
            this.uiDepth = uiDepth;
        }
    }
}