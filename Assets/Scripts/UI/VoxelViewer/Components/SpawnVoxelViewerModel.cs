using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI.Voxels
{
    public struct SpawnVoxelViewerModel : IComponentData
    {
        public Entity voxelMetaEntity;
        public Entity planet;
        public float3 voxelPosition;
        public float3 cameraPosition;
    }
}