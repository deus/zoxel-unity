using Unity.Entities;

namespace Zoxel.UI.Voxels
{
    public struct VoxelGroupViewerDirty : IComponentData { }
}