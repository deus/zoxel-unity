using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI.Voxels
{
    public struct SpawnVoxelGroupViewer : IComponentData
    {
        public Entity realm;
        public Entity player;
        public float3 position;
        public int2 gridSize;
        
        public SpawnVoxelGroupViewer(Entity realm, Entity player, float3 position, int2 gridSize)
        {
            this.realm = realm;
            this.player = player;
            this.position = position;
            this.gridSize = gridSize;
        }
    }
}