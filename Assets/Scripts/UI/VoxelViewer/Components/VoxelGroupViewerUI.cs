using Unity.Entities;

namespace Zoxel.UI.Voxels
{
    public struct VoxelGroupViewerUI : IComponentData
    {
        public byte isBuilding;
        public int2 gridSize;
        
        public VoxelGroupViewerUI(int2 gridSize)
        {
            this.gridSize = gridSize;
            this.isBuilding = 0;
        }
    }
}