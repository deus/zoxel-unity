using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Cameras;
using Zoxel.Transforms;
using Zoxel.Textures;
using Zoxel.Voxels;
// this won't update unless its for all screens kinda thing
// save resolution to cameras and do it if camera detects change

namespace Zoxel.UI.Voxels
{
    //! Spawns a group of voxels for the Realm.
    /**
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class VoxelGroupViewerSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var spacing = uiDatam.GetIconSize(uiScale).x;    // 0.0012f
            var viewerSize = uiDatam.GetIconSize(uiScale);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            Entities
                .WithNone<OnChildrenSpawned>()
                .WithAll<VoxelGroupViewerDirty>()
                .ForEach((Entity e, in Childrens childrens, in RealmLink realmLink, in VoxelGroupViewerUI voxelGroupViewerUI) =>
            {
                var voxelLinksEntity = realmLink.realm;
                var gridSize = voxelGroupViewerUI.gridSize;
                var voxelLinks = EntityManager.GetComponentData<VoxelLinks>(voxelLinksEntity);
                // var newUIsCount = voxelLinks.voxels.Length; // math.min(voxelLinks.voxels.Length, gridSize.x * gridSize.y);
                var newVoxelsCount = math.min(voxelLinks.voxels.Length, gridSize.x * gridSize.y);
                // Set any old uis to the right voxel data
                for (int i = 0; i <  math.min(newVoxelsCount, childrens.children.Length); i++)
                {
                    // update links of voxels
                    var uiEntity = childrens.children[i];
                    var worldVoxelEntity = EntityManager.GetComponentData<ViewerUI>(uiEntity).targetEntity;
                    var voxelMetaEntity = voxelLinks.voxels[i];
                    PostUpdateCommands.SetComponent(worldVoxelEntity, new VoxelLink(voxelMetaEntity));
                    PostUpdateCommands.SetComponent(worldVoxelEntity, new VoxelTileIndex(i));
                }
                // there is more voxels than previous realm
                if (newVoxelsCount > childrens.children.Length)
                {
                    var addCount = newVoxelsCount - childrens.children.Length;
                    for (int i = childrens.children.Length; i < newVoxelsCount; i++)
                    {
                        var voxelMetaEntity = voxelLinks.voxels[i];
                        SpawnVoxelViewerUI(PostUpdateCommands, UICoreSystem.viewerPrefab, e, voxelLinksEntity, voxelMetaEntity, i, viewerSize, gridSize, spacing);
                    }
                    PostUpdateCommands.AddComponent(e, new OnChildrenSpawned((byte) addCount));
                }
                // There is less voxels then previous realm
                else if (childrens.children.Length > newVoxelsCount)
                {
                    var removeCount = newVoxelsCount - childrens.children.Length;
                    // there is more voxels
                    for (int i = newVoxelsCount; i < childrens.children.Length; i++)
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(childrens.children[i]);
                    }
                    PostUpdateCommands.AddComponent(e, new RemoveChildren((byte) removeCount));
                }
                // Spawn a polygon Mesh based on a voxel
                // Use same components as
                PostUpdateCommands.RemoveComponent<VoxelGroupViewerDirty>(e);
            }).WithoutBurst().Run();
        }

        private static void SpawnVoxelViewerUI(EntityCommandBuffer PostUpdateCommands, Entity viewerPrefab,
            Entity parent, Entity planet,
            Entity voxelMetaEntity, int childIndex, float2 viewerSize, int2 gridSize, float spacing)
        {
            var gridPosition = new int2(childIndex % gridSize.x, childIndex / gridSize.x);
            var position = new float3(gridPosition.x * spacing, - spacing * gridPosition.y, 0);
            SpawnVoxelViewerUI(PostUpdateCommands, viewerPrefab, parent, planet,
                childIndex, viewerSize, position, voxelMetaEntity, childIndex,
                gridPosition, gridSize);
        }

        private static void SpawnVoxelViewerUI(EntityCommandBuffer PostUpdateCommands, Entity viewerPrefab,
            Entity parent, Entity planet,
            int childIndex, float2 viewerSize, float3 position, Entity voxelMetaEntity,  int voxelIndex,
            int2 gridPosition, int2 gridSize)
        {
            var positionDepth = 1f;
            var voxelViewerMaterial = UIManager.instance.materials.voxelViewerMaterial;
            var viewer = UICoreSystem.SpawnElement(PostUpdateCommands, viewerPrefab,
                parent, position, viewerSize,
                new Color(UnityEngine.Color.white),
                voxelViewerMaterial, TextureUtil.CreateBlankTexture());
            UICoreSystem.SetChild(PostUpdateCommands, viewer, childIndex);
            PostUpdateCommands.AddComponent<SetUIElementPosition>(viewer);
            PostUpdateCommands.AddComponent<GenerateMeshUI>(viewer);
            var spawnPosition = new float3(
                (((gridPosition.x - (gridSize.x / 2)) / (float) gridSize.x)) * 5 * 5,
                (((gridPosition.y - (gridSize.y / 2)) / (float) gridSize.y)) * 5 * 5,
                0);
            PostUpdateCommands.AddComponent(viewer, new SpawnVoxelViewerModel
            {
                planet = planet,
                voxelMetaEntity = voxelMetaEntity,
                cameraPosition = spawnPosition + new float3(0, 0, -positionDepth),
                voxelPosition = spawnPosition
            });
        }
    }
}