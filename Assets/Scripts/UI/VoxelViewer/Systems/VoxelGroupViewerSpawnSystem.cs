using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Transforms;

namespace Zoxel.UI.Voxels
{
    //! Spawns a VoxelGroupViewerUI.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class VoxelGroupViewerSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var viewerGroupPrefab = UICoreSystem.viewerGroupPrefab;
            var uiDatam = UIManager.instance.uiDatam;
            var iconSize = uiDatam.GetIconSize(uiScale) * 1f;
            var viewerColor = new Color(new UnityEngine.Color(1,1,1,1));
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<OnChildrenSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, in SpawnVoxelGroupViewer spawnVoxelGroupViewer) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnVoxelGroupViewer>(entityInQueryIndex, e);
                var size = new float2(iconSize.x * spawnVoxelGroupViewer.gridSize.x, iconSize.y * spawnVoxelGroupViewer.gridSize.y);
                // Spawn Voxel Group Viewer
                var voxelGroupViewerUI = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, viewerGroupPrefab, e,
                    spawnVoxelGroupViewer.position, size, viewerColor);
                UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, voxelGroupViewerUI, 0);
                PostUpdateCommands.AddComponent(entityInQueryIndex, voxelGroupViewerUI, new VoxelGroupViewerUI(spawnVoxelGroupViewer.gridSize));
                PostUpdateCommands.AddComponent<SetUIElementPosition>(entityInQueryIndex, voxelGroupViewerUI);
                PostUpdateCommands.AddComponent<VoxelGroupViewerDirty>(entityInQueryIndex, voxelGroupViewerUI);
                PostUpdateCommands.AddComponent(entityInQueryIndex, voxelGroupViewerUI, new RealmLink(spawnVoxelGroupViewer.realm));
                // Update parent
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChildrenSpawned(1));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}