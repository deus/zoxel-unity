using Unity.Entities;
using Zoxel.Transforms;
// this won't update unless its for all screens kinda thing
// save resolution to cameras and do it if camera detects change
// When Realm Regenerates, Refresh VoxelGroupViewer UI

namespace Zoxel.UI.Voxels
{
    //! Checks to see of the world has updated using EntityBusy.
    /**
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class VoxelGroupViewerDirtySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            Entities
                .WithNone<VoxelGroupViewerDirty>()
                .ForEach((Entity e, ref VoxelGroupViewerUI voxelGroupViewerUI, in RealmLink realmLink) =>
            {
				byte newBuilding;
				if (HasComponent<EntityBusy>(realmLink.realm))
				{
					newBuilding = 1;
				}
				else
				{
					newBuilding = 0;
				}
				if (voxelGroupViewerUI.isBuilding != newBuilding)
				{
					voxelGroupViewerUI.isBuilding = newBuilding;
					if (newBuilding == 0)
					{
                        PostUpdateCommands.AddComponent<VoxelGroupViewerDirty>(e);
					}
				}
			}).WithoutBurst().Run();
        }
    }
}