using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Cameras;
using Zoxel.Voxels;
using Zoxel.Rendering;

namespace Zoxel.UI.Voxels
{
    //! Spawns a voxel for the ViewerUI
    /**
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class VoxelViewerModelSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity voxelModelPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var voxelModelArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(DestroyEntityDirect),
                typeof(ViewerObject),
                typeof(VoxelViewerVoxel),
                typeof(WorldVoxel),
                typeof(VoxelLink),
                typeof(VoxelTileIndex),
                typeof(MaterialBaseColor),
                typeof(RenderLayer),
                typeof(Translation),
                typeof(Rotation),
                typeof(Scale),
                typeof(LocalToWorld)
            );
            voxelModelPrefab = EntityManager.CreateEntity(voxelModelArchetype);
            EntityManager.SetComponentData(voxelModelPrefab, new MaterialBaseColor { Value = new float4(1, 1, 1, 1) });
            EntityManager.SetComponentData(voxelModelPrefab, new Scale { Value = 0.4f });
        }

        // this won't update unless its for all screens kinda thing
        // save resolution to cameras and do it if camera detects change
        protected override void OnUpdate()
        {
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var degreesToRadians = ((math.PI * 2) / 360f);
            var rotation = quaternion.EulerXYZ(new float3(-45, 45, 0) * degreesToRadians); // UnityEngine.Quaternion.Euler(-45, 45, 0);
            var voxelModelPrefab = this.voxelModelPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((Entity e, ref ViewerUI viewerUI, in Zoxel.Transforms.Child child, in SpawnVoxelViewerModel spawnVoxelViewerModel) =>
            {
                var voxelMetaEntity = spawnVoxelViewerModel.voxelMetaEntity;
                if (!EntityManager.Exists(voxelMetaEntity))
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<SpawnVoxelViewerModel>(e);
                var voxelEntity = EntityManager.Instantiate(voxelModelPrefab);
                PostUpdateCommands.SetComponent(voxelEntity, new Translation { Value = spawnVoxelViewerModel.voxelPosition });
                PostUpdateCommands.SetComponent(voxelEntity, new Rotation { Value = rotation });
                PostUpdateCommands.SetComponent(voxelEntity, new VoxelLink(voxelMetaEntity));
                PostUpdateCommands.SetComponent(voxelEntity, new VoxelTileIndex(child.index));
                PostUpdateCommands.SetComponent(voxelEntity, new RenderLayer(worldUILayer));
                // PostUpdateCommands.AddComponent(voxelEntity, new InitializeVoxelTileIndex(child.index));
                var camera = SpawnViewerCamera(PostUpdateCommands, e, voxelEntity, spawnVoxelViewerModel.cameraPosition, quaternion.identity, 0);
                viewerUI.targetEntity = voxelEntity;
            }).WithStructuralChanges().Run();
        }

        public static int SpawnViewerCamera(EntityCommandBuffer PostUpdateCommands, Entity ui, Entity targetEntity, float3 position, quaternion rotation, byte cameraType)
        {
            var id = IDUtil.GenerateUniqueID();
            PostUpdateCommands.AddComponent(ui, new SpawnViewerCamera
            {
                id = id,
                targetEntity = targetEntity,
                position = position,
                rotation = rotation,
                cameraType = cameraType
            });
            return id;
        }
    }
}