using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class RenderTextFadeOutStartSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity, OnRenderTextSpawned>()
                .WithNone<DisableFader>()
                .WithAll<FadeOut, Fader>()
                .ForEach((Entity e, int entityInQueryIndex, in RenderText renderText) =>
            {
                for (int i = 0; i < renderText.letters.Length; i++)
                {
                    var child = renderText.letters[i];
                    if (!HasComponent<DestroyEntity>(child) && HasComponent<Fader>(child) && !HasComponent<DisableFader>(child))
                    {
                        PostUpdateCommands.AddComponent<FadeOut>(entityInQueryIndex, child);
                        if (HasComponent<FadingIn>(child))
                        {
                            // UnityEngine.Debug.LogError("(4) Removing FadingIn: " + child.Index);
                            PostUpdateCommands.RemoveComponent<FadingIn>(entityInQueryIndex, child);
                        }
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}