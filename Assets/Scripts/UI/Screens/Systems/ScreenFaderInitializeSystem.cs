using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class ScreenFaderInitializeSystem : SystemBase
    {
        const string baseMapName = "_BaseMap";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var screenFaderMaterial = UIManager.instance.materials.screenFaderMaterial;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<DestroyEntity>()
                .WithAll<InitializeEntity, ScreenFader>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in Size2D size2D) =>
            {
                zoxMesh.layer = worldUILayer;
                if (size2D.size.x != 0 && size2D.size.y != 0)
                {
                    zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size2D.size, 0, 0);
                }
                zoxMesh.material = new UnityEngine.Material(screenFaderMaterial);
                zoxMesh.material.SetTexture(baseMapName, UnityEngine.Texture2D.whiteTexture);
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
        }
    }
}