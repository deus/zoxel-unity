using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class FadeOutScreenSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            // const float fadeOutExitTime = 2f;
            // var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in ScreenFaderLink screenFaderLink, in FadeOutScreen fadeOutScreen) =>
            {
                PostUpdateCommands.RemoveComponent<FadeOutScreen>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent(entityInQueryIndex, screenFaderLink.screenFader, fadeOutScreen);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref Fader fader, in FadeOutScreen fadeOutScreen) =>
            {
                PostUpdateCommands.RemoveComponent<FadeOutScreen>(entityInQueryIndex, e);
                fader.fadeTime = fadeOutScreen.fadeTime;
                fader.timePassed = fadeOutScreen.fadeTime;
                PostUpdateCommands.AddComponent<FadeIn>(entityInQueryIndex, e);
                if (HasComponent<ReverseFader>(e))
                {
                    PostUpdateCommands.RemoveComponent<ReverseFader>(entityInQueryIndex, e);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}