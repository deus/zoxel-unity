using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class RenderTextFadeInSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity, OnRenderTextSpawned>()
                .WithNone<DisableFader>()
                .WithAll<FadeIn, Fader>()
                .ForEach((Entity e, int entityInQueryIndex, in RenderText renderText) =>
            {
                for (int i = 0; i < renderText.letters.Length; i++)
                {
                    var child = renderText.letters[i];
                    if (!HasComponent<DestroyEntity>(child) && HasComponent<Fader>(child) && !HasComponent<DisableFader>(child))
                    {
                        PostUpdateCommands.AddComponent<FadeIn>(entityInQueryIndex, child);
                        if (HasComponent<FadingOut>(child))
                        {
                            PostUpdateCommands.RemoveComponent<FadingOut>(entityInQueryIndex, child);
                        }
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}