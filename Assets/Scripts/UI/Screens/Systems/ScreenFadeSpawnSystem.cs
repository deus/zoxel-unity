using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Cameras;
using Zoxel.Textures;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    //! Spawns a ScreenFader entity.
    /**
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class ScreenFadeSpawnSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public Entity screenFaderPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var screenFaderArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(ScreenFader),
                typeof(DontDestroyTexture),
                typeof(PanelUI),
                typeof(UIPosition),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(CharacterLink),
                typeof(CameraLink),
                typeof(OrbitTransform),
                typeof(OrbitPosition),
                typeof(MaterialBaseColor),
                typeof(ZoxMesh),
                typeof(Fader),
                typeof(FaderInvisible),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld));
            screenFaderPrefab = EntityManager.CreateEntity(screenFaderArchetype);
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var screenFaderPrefab = this.screenFaderPrefab;
            var gameOrbitDepth = CameraManager.instance.cameraSettings.uiDepth;
            var screenFaderMaterial = UIManager.instance.materials.screenFaderMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            var screenWidth = UnityEngine.Screen.width;
            var screenHeight = UnityEngine.Screen.height;
            Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithAll<SpawnScreenFader>()
                .ForEach((Entity e, in CameraLink cameraLink) =>
            {
                if (cameraLink.camera.Index == 0)
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<SpawnScreenFader>(e);
                var panelSize = new float2(screenWidth, screenHeight);
                var panelColor = new UnityEngine.Color(0, 0, 0, 0); // 1 alpha to fade in
                var fader = PostUpdateCommands.Instantiate(screenFaderPrefab);
                PostUpdateCommands.SetComponent(fader, new Size2D(panelSize));
                PostUpdateCommands.SetComponent(fader, cameraLink);
                PostUpdateCommands.SetComponent(fader, new NonUniformScale { Value = new float3(1, 1, 1) });
                PostUpdateCommands.SetComponent(fader, new Fader(0, 1, 1));
                PostUpdateCommands.SetComponent(fader, new UIPosition(gameOrbitDepth));
                PostUpdateCommands.SetComponent(fader, new CharacterLink(e));
                var screenFaderLink = new ScreenFaderLink(fader);
                if (!HasComponent<ScreenFaderLink>(e))
                {
                    PostUpdateCommands.AddComponent(e, screenFaderLink);
                }
                else
                {
                    PostUpdateCommands.SetComponent(e, screenFaderLink);
                }
            }).WithoutBurst().Run();
        }
    }
}