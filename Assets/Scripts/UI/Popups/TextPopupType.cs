namespace Zoxel.UI.Animations
{
    public static class TextPopupType
    {
        public const byte Damage = 0;
        public const byte Experience = 1;
        public const byte VoxelDamage = 2;
        public const byte Shield = 3;
    }
}