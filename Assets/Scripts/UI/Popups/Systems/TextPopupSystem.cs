﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Animations;

namespace Zoxel.UI.Animations
{
    //! Spawns TextPopups, used for player feedback.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class TextPopupSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity popupPrefab;
        public static Entity spawnPopupPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var popupArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(RenderTextDirty),
                typeof(DestroyEntityInTime),
                typeof(LerpPosition),
                typeof(LerpScale),
                typeof(InvisibleUI),
                typeof(TextPopup),
                typeof(FaceCamera),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(Size2D),
                typeof(RenderText),
                typeof(RenderTextData),
                // typeof(BasicRender),
                // typeof(MaterialBaseColor),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            popupPrefab = EntityManager.CreateEntity(popupArchetype);
            var spawnPopupArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnTextPopup),
                typeof(GenericEvent));
            spawnPopupPrefab = EntityManager.CreateEntity(spawnPopupArchetype);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var popupPrefab = this.popupPrefab;
            var uiDatam = UIManager.instance.uiDatam;
            var elapsedTime = World.Time.ElapsedTime;
            var elapsedTime2 = (float) elapsedTime;
            var damagePopupSize = uiDatam.damagePopupSize;
            var popupLifetime = uiDatam.popupLifetime;
            var popupVariationX = uiDatam.popupVariationX;
            var popupVariationY = uiDatam.popupVariationY;
            var popupVariationZ = uiDatam.popupVariationZ;
            var textGenerationData = uiDatam.popupTextData;
            Entities.ForEach((Entity e, int entityInQueryIndex, ref SpawnTextPopup spawnTextPopup) =>
            {
                spawnTextPopup.seed = (uint) IDUtil.GenerateUniqueID();
            }).WithoutBurst().Run();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in SpawnTextPopup spawnTextPopup) =>
            {
                var random = new Random();
                random.InitState(spawnTextPopup.seed); // (uint) ((int) 666 * elapsedTime2));
                var position = spawnTextPopup.position;
                var damage = spawnTextPopup.damage;
                // UnityEngine.Debug.LogError("Spawned Popup: " + damage);
                if (damage.Length == 0)
                {
                    return;
                }
                var delay = 0.1f + random.NextFloat(0.1f);
                var halfWidth = (damage.Length * damagePopupSize * 0.6f) / 2f;
                var lifetime = popupLifetime.x + random.NextFloat(popupLifetime.y - popupLifetime.x);
                var positionBegin = position + new float3(0, 0.1f + damagePopupSize / 2f, 0); //  + offset;
                var positionEnd = positionBegin;
                if (spawnTextPopup.direction == PlanetSide.Down)
                { 
                    positionEnd += new float3(
                        random.NextFloat(popupVariationX.x, popupVariationX.y),
                        -random.NextFloat(popupVariationY.x, popupVariationY.y),
                        random.NextFloat(popupVariationZ.x, popupVariationZ.y));
                }
                else if (spawnTextPopup.direction == PlanetSide.Up)
                { 
                    positionEnd += new float3(
                        random.NextFloat(popupVariationX.x, popupVariationX.y),
                        random.NextFloat(popupVariationY.x, popupVariationY.y),
                        random.NextFloat(popupVariationZ.x, popupVariationZ.y));
                }
                else if (spawnTextPopup.direction == PlanetSide.Left)
                { 
                    positionEnd += new float3(
                        -random.NextFloat(popupVariationY.x, popupVariationY.y),
                        random.NextFloat(popupVariationX.x, popupVariationX.y),
                        random.NextFloat(popupVariationZ.x, popupVariationZ.y));
                }
                else if (spawnTextPopup.direction == PlanetSide.Right)
                { 
                    positionEnd += new float3(
                        random.NextFloat(popupVariationY.x, popupVariationY.y),
                        random.NextFloat(popupVariationX.x, popupVariationX.y),
                        random.NextFloat(popupVariationZ.x, popupVariationZ.y));
                }
                else if (spawnTextPopup.direction == PlanetSide.Backward)
                { 
                    positionEnd += new float3(
                        random.NextFloat(popupVariationX.x, popupVariationX.y),
                        random.NextFloat(popupVariationZ.x, popupVariationZ.y),
                        -random.NextFloat(popupVariationY.x, popupVariationY.y));
                }
                else if (spawnTextPopup.direction == PlanetSide.Forward)
                { 
                    positionEnd += new float3(
                        random.NextFloat(popupVariationX.x, popupVariationX.y),
                        random.NextFloat(popupVariationZ.x, popupVariationZ.y),
                        random.NextFloat(popupVariationY.x, popupVariationY.y));
                }
                
                var textEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, popupPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, textEntity, new LerpPosition
                {
                    createdTime = elapsedTime,
                    lifeTime = lifetime,
                    positionBegin = positionBegin,
                    positionEnd = positionEnd
                });
                PostUpdateCommands.SetComponent(entityInQueryIndex, textEntity, new LerpScale
                {
                    createdTime = elapsedTime,
                    lifeTime = lifetime - delay,
                    delay = delay,
                    scaleBegin = new float3(1, 1, 1),
                    scaleEnd = new float3(0, 0, 0)
                });
                var renderTextData = new RenderTextData
                {
                    fontSize = damagePopupSize,
                    textGenerationData = textGenerationData
                    //color = color,
                    //outlineColor = outlineColor,
                    //horizontalAlignment = horizontalAlignment,
                    //maxWidth = maxWidth,
                    //maxHeight = maxHeight,
                    //margins = margins
                };
                if (spawnTextPopup.type == TextPopupType.Damage)
                {
                    renderTextData.SetColor(UnityEngine.Color.red);
                }
                else if (spawnTextPopup.type == TextPopupType.Experience)
                {
                    renderTextData.SetColor(UnityEngine.Color.yellow);
                }
                else if (spawnTextPopup.type == TextPopupType.VoxelDamage)
                {
                    renderTextData.color = new Color(110, 60, 20);
                    // renderTextData.SetColor(UnityEngine.Color.green);
                }
                else if (spawnTextPopup.type == TextPopupType.Shield)
                {
                    renderTextData.SetColor(UnityEngine.Color.cyan);
                }
                renderTextData.outlineColor = renderTextData.color;
                PostUpdateCommands.SetComponent(entityInQueryIndex, textEntity, renderTextData);
                PostUpdateCommands.SetComponent(entityInQueryIndex, textEntity, new RenderText(damage));
                PostUpdateCommands.SetComponent(entityInQueryIndex, textEntity, new DestroyEntityInTime(elapsedTime2, lifetime));
                //UnityEngine.Debug.LogError("Spawning Popup at: " + position);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                /*if (spawnTextPopup.state != 1)
                {
                    return;
                }*/
                // PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                //if (spawnTextPopup.state == 0)
                //{
                //    spawnTextPopup.state = 1;
                //}