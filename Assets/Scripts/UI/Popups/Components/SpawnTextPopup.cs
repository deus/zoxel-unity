using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.UI.Animations
{
    public struct SpawnTextPopup : IComponentData
    {
        public Text damage; 
        public float3 position;
        public byte type;
        public uint seed;
        public byte direction;

        public SpawnTextPopup(float damage, float3 position, byte direction, byte type = 0)
        {
            this.seed = 0;
            this.direction = direction;
            this.type = type;
            this.damage = new Text();
            var textData = new NativeList<byte>();
            var damageInt = (int)damage;
            int lastDivision = 1;
            int nextDivision = 10;
            var count = 0;
            while (count < 16)
            {
                var newCharacter = (byte)(((damageInt % nextDivision) / lastDivision)); // 1 + 
                textData.Add(newCharacter);
                lastDivision *= 10;
                nextDivision *= 10;
                if (lastDivision > damageInt) 
                {
                   break;
                }
                count++;
            }
            this.damage.textData = new BlitableArray<byte>(textData.Length, Allocator.Persistent);
            for (int i = textData.Length - 1; i >= 0; i--)
            {
                this.damage.textData[textData.Length - 1 - i] = textData[i];
            }
            this.position = position;
            textData.Dispose();
        }
    }
}
            // imagine 27
            //  (27 % 10) / 1 = 7
            //  (227 % 100) / 10 = 2
            //  (X % nextDivision) / lastDivision = 2