﻿using Unity.Entities;
using Unity.Collections;

namespace Zoxel.UI
{
    //! Any viewer objects, must be spawned in their own sub systems
    /**
    *   \todo Set UI positions by pixels?
    *   \todo Add UIScaling in options.
    */
    public partial class UISystemGroup : ComponentSystemGroup
    {
        public static double elapsedTime;
        public static Entity fontEntity;
        public static Entity uiSpawnedEventPrefab;

        protected override void OnCreate()
        {
            base.OnCreate();
            fontEntity = EntityManager.CreateEntity();
            var font = new Font();
            font.InitializeZigelSet();
            EntityManager.AddComponentData(fontEntity, font);
            var uiSpawnedEventArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(OnUISpawned),
                typeof(GenericEvent));
            uiSpawnedEventPrefab = EntityManager.CreateEntity(uiSpawnedEventArchetype);
        }
    }
}