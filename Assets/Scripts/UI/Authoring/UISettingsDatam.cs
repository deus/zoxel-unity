using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using System;

namespace Zoxel.UI
{
    [CreateAssetMenu(fileName = "UISettings", menuName = "ZoxelSettings/UISettings")]
    public partial class UISettingsDatam : ScriptableObject
    {
        public UISettings uiSettings;
    }
}