using UnityEngine;
using Unity.Mathematics;
//! \todo Fix this before moving on. It's not working properly.

namespace Zoxel.UI
{
    //! Holds some ui datam, colours, paddings, font sizes, etc.
    [CreateAssetMenu(fileName = "IconStyle", menuName = "ZoxelUI/IconStyle")]
    public partial class IconStyleDatam : ScriptableObject
    {
        [Header("Frame")]
        public float2 iconSize;
        public UnityEngine.Color frameColor;
        public UnityEngine.Color selectedFrameColor;
        public FrameGenerationData frameGenerationData;

        [Header("Label")]
        public float fontSize;
        public float2 textPadding;
        public byte labelAlignment;
        public UnityEngine.Color textColor;
        public UnityEngine.Color textOutlineColor;
        public UnityEngine.Color selectedTextColor;
        public UnityEngine.Color selectedTextOutlineColor;
        public TextGenerationData textGenerationData;

        [Header("Audio")]
        public float2 soundVolume;
        
        public IconStyle GetStyle(float uiScale)
        {
            return new IconStyle
            {
                iconSize = iconSize * uiScale,
                frameColor = new Color(frameColor),
                selectedFrameColor = new Color(selectedFrameColor),
                frameGenerationData = frameGenerationData,
                fontSize = fontSize * uiScale,
                textPadding = textPadding * uiScale,
                textColor = new Color(textColor),
                textOutlineColor = new Color(textOutlineColor),
                selectedTextColor = new Color(selectedTextColor),
                selectedTextOutlineColor = new Color(selectedTextOutlineColor),
                soundVolume = soundVolume
            };
        }
    }
}