using UnityEngine;
using System;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! General settings for UI things.
    [Serializable]
    public struct UISettings
    {
        [Header("Settings")]
        public float uiScale;   // 16 or 1.6
        public float uiDepth; // = 0.1f;
        public float buttonAnimationTime;   // 0.01
        public float selectedEnlargeSize;   // 1.1
        public float panelDepth;            // 0.25, 0.5?

        [Header("Screen Fader")]
        public float fadeinScreenTime;      // 1f
        public float fadeinScreenWaitTime;    // 1f
        public float fadeoutScreenTime;     // 1f
        public float fadeoutScreenWaitTime;    // 1f
        
        [Header("CharacterMaker")]
        public float3 characterMakerCharacterPosition;  // new float3(255, 255, 255)
        public float3 characterMakerCameraPosition;     // new float3(0, 0.38f, 0.8f)
        public float3 characterMakerCameraRotation;     // new float3(25, 180, 0)
        
        [Header("Maps")]
        public int minimapRenderDistance;            // 4
        public int mapRenderDistance;

        [Header("Debug")]
        public bool isDebugMapMegaChunks;
        public bool isDebugMapMegaChunk2Ds;
        public bool isDebugMapBiomes;
        public bool isDebugMapTowns; 

        [Header("Disables")]
        public bool disableCrosshair;
        public bool disableMinimap;
        public bool disableQuestTracker;
        public bool disableActionbar;
        public bool disableTimeUI;
        public bool disableTaskbar;
        public bool disableStatbars;
        public bool disableNameLabels;

        public void DrawDisableUI()
        {
            GUILayout.Label(" [disables] ");
            disableStatbars = GUILayout.Toggle(disableStatbars, "Disable Statbars");
            disableNameLabels = GUILayout.Toggle(disableNameLabels, "Disable Name Labels");
            disableCrosshair = GUILayout.Toggle(disableCrosshair, "Disable Crosshair");
            disableMinimap = GUILayout.Toggle(disableMinimap, "Disable Minimap");
            disableQuestTracker = GUILayout.Toggle(disableQuestTracker, "Disable QuestTracker");
        }
    }
}