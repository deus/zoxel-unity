using Unity.Entities;
using System;

namespace Zoxel.UI
{
    [Serializable]
    public struct RenderTextDataEditor
    {
        //! The Size2D off each zigel.
        public float fontSize;
        //! The Color off each zigel.
        public UnityEngine.Color color;
        //! The OutlineColor off each zigel.
        public UnityEngine.Color outlineColor;
        //! Data used in generated the texture per zigel.
        public TextGenerationData textGenerationData;

        public RenderTextData GetData()
        {
            return new RenderTextData
            {
                fontSize = fontSize,
                color = new Color(color),
                outlineColor = new Color(outlineColor),
                textGenerationData = textGenerationData
            };
        }
    }
}