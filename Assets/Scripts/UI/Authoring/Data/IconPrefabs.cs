using Unity.Entities;

namespace Zoxel.UI
{
    //! Entities used for a single UI frame. Has an Icon and Label.
    public struct IconPrefabs
    {
        public Entity frame;
        public Entity icon;
        public Entity label;
    }
}