using Unity.Mathematics;

namespace Zoxel.UI
{
    //! Datam used for icons in game.
    public struct IconStyle
    {
        public float2 iconSize;

        public float2 soundVolume;
        public FrameGenerationData frameGenerationData;
        public Color frameColor;
        public Color selectedFrameColor;
        // Label
        public float fontSize;
        public float2 textPadding;
        public byte labelAlignment;
        public Color textColor;
        public Color textOutlineColor;
        public Color selectedTextColor;
        public Color selectedTextOutlineColor;
        public TextGenerationData textGenerationData;
    }
}