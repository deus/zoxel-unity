﻿using UnityEngine;
using System;

namespace Zoxel.UI
{
    [CreateAssetMenu(fileName = "UIMaterials", menuName = "ZoxelUI/UIMaterials")]
    public partial class UIMaterials : ScriptableObject
    {
        [Header("Panels")]
        public Material panelMaterial;
        public Material subPanelMaterial;
        public Material buttonMaterial;

        [Header("Text")]  // lower to higher queue
        public Material fontMaterial;

        [Header("Game")]
        public Material cooldownOverlayMaterial;
        public Material skillTreeLineMaterial;
        public Material frameMaterial;
        public Material iconMaterial;
        public Material crosshairMaterial;

        [Header("Map")]
        public Material mapPartMaterial;
        public Material miniMapPartMaterial;
        public Material mapIconMaterial;

        [Header("Statbars")]
        public Material frontBarMaterial;
        public Material backBarMaterial;

        [Header("Misc")]
        public Material voxelViewerMaterial;    // move into materials
        public Material screenFaderMaterial;
    }
}

        /*[Header("Mouse Follow")]
        public Material iconMaterialMouseFollow;
        public Material fontMaterialMouseFollow;*/