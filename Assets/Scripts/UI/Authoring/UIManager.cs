using UnityEngine;

namespace Zoxel.UI
{
    //! Manages ui things.
    public partial class UIManager : MonoBehaviour
    {
        public static UIManager instance;
        [Header("Settings")]
        [UnityEngine.SerializeField] public UISettingsDatam UISettings;
        [Header("UI Data")]
        public UIDatam uiDatam;
        public UIMaterials materials;
        [Header("Game")]
        public string realmName = "Zoxel";
        [UnityEngine.HideInInspector] public UISettings realUISettings;

        public UISettings uiSettings
        {
            get
            { 
                return realUISettings;
            }
        }

        public void Awake()
        {
            instance = this;
            Initialize();
        }

        public void Initialize()
        {
            realUISettings = UISettings.uiSettings;
        }
    }
}