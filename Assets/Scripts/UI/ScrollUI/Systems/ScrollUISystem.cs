using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! Scrolls through ui list.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class ScrollUISystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var iconSize = uiDatam.GetIconSize(uiScale) * 0.64f;
            var fontSize = iconSize.y;
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<SpawnButtonsList, DestroyChildren>()
                .WithAll<ScrollDown>()
                .ForEach((Entity e, int entityInQueryIndex, ref ScrollListUI scrollListUI) =>
            {
                PostUpdateCommands.RemoveComponent<ScrollDown>(entityInQueryIndex, e);
                var newIndex = scrollListUI.index + scrollListUI.visibleCount;
                if (newIndex < scrollListUI.saveNames.Length)
                {
                    scrollListUI.index = newIndex;
                    // can scroll down
                    var initialSaveLabels = new NativeList<Text>();
                    var endIndex = math.min(scrollListUI.saveNames.Length, scrollListUI.index + scrollListUI.visibleCount);
                    for (int i = scrollListUI.index; i < endIndex; i++)
                    {
                        initialSaveLabels.Add(scrollListUI.saveNames[i]);
                    }
                    PostUpdateCommands.AddComponent<DestroyChildren>(entityInQueryIndex, e);
                    // , fontSize, textPadding
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SpawnButtonsList(in initialSaveLabels));
                    initialSaveLabels.Dispose();
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<SpawnButtonsList, DestroyChildren>()
                .WithAll<ScrollUp>()
                .ForEach((Entity e, int entityInQueryIndex, ref ScrollListUI scrollListUI) =>
            {
                PostUpdateCommands.RemoveComponent<ScrollUp>(entityInQueryIndex, e);
                var newIndex = scrollListUI.index - scrollListUI.visibleCount;
                if (newIndex >= 0)
                {
                    scrollListUI.index = newIndex;
                    // can scroll down
                    var initialSaveLabels = new NativeList<Text>();
                    var endIndex = math.min(scrollListUI.saveNames.Length, scrollListUI.index + scrollListUI.visibleCount);
                    for (int i = scrollListUI.index; i < endIndex; i++)
                    {
                        initialSaveLabels.Add(scrollListUI.saveNames[i]);
                    }
                    PostUpdateCommands.AddComponent<DestroyChildren>(entityInQueryIndex, e);
                    // , fontSize, textPadding
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SpawnButtonsList(in initialSaveLabels));
                    initialSaveLabels.Dispose();
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}