using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! Scrolls through ui list.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class ScrollUIRefreshSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<SpawnButtonsList, DestroyChildren>()
                .WithAll<ScrollRefresh>()
                .ForEach((Entity e, int entityInQueryIndex, ref ScrollListUI scrollListUI) =>
            {
                PostUpdateCommands.RemoveComponent<ScrollRefresh>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<DestroyChildren>(entityInQueryIndex, e);
                if (scrollListUI.index >= scrollListUI.saveNames.Length)
                {
                    scrollListUI.index -= scrollListUI.visibleCount;
                }
                if (scrollListUI.saveNames.Length > 0)
                {
                    // can scroll down
                    var initialSaveLabels = new NativeList<Text>();
                    var endIndex = math.min(scrollListUI.saveNames.Length, scrollListUI.index + scrollListUI.visibleCount);
                    for (int i = scrollListUI.index; i < endIndex; i++)
                    {
                        initialSaveLabels.Add(scrollListUI.saveNames[i]);
                    }
                    // , fontSize, textPadding, fontSize, textPadding
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SpawnButtonsList(in initialSaveLabels));
                    // PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ListPrefabLink(scrollListUI.prefab));
                    initialSaveLabels.Dispose();
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}