using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Cleans up ScrollListUI components on DestroyEntity event.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ScrollListUIDestroySystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, ScrollListUI>()
                .ForEach((in ScrollListUI scrollListUI) =>
            {
                scrollListUI.DisposeFinal();
			}).ScheduleParallel();
        }
    }
}