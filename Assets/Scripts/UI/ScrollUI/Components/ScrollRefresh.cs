using Unity.Entities;

namespace Zoxel.UI
{
    //! Refreshes list after a item is removed or added.
    public struct ScrollRefresh : IComponentData { }
}