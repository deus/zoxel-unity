using Unity.Entities;
using Unity.Collections;

namespace Zoxel.UI
{
    //! Stores a list of save file texts to use for scrolling.
    public struct ScrollListUI : IComponentData
    {
        public int index;                       //! Which index are we displaying from
        public int visibleCount;
        // todo move lists into another component
        public BlitableArray<Text> saveNames;
        public BlitableArray<int> saveIDs;

        public void DisposeFinal()
        {
            for (int i = 0; i < saveNames.Length; i++)
            {
                saveNames[i].DisposeFinal();
            }
            saveNames.DisposeFinal();
            saveIDs.DisposeFinal();
        }

        public void Dispose()
        {
            for (int i = 0; i < saveNames.Length; i++)
            {
                saveNames[i].Dispose();
            }
            saveNames.Dispose();
            saveIDs.Dispose();
        }

        public ScrollListUI(in NativeList<Text> saveNames, in NativeList<int> saveIDs, int visibleCount, byte isClone = 1)
        {
            this.index = 0;
            this.visibleCount = visibleCount;
            this.saveNames = new BlitableArray<Text>(saveNames.Length, Allocator.Persistent);
            for (int i = 0; i < saveNames.Length; i++)
            {
                if (isClone == 1)
                {
                    this.saveNames[i] = saveNames[i].Clone();
                }
                else
                {
                    this.saveNames[i] = saveNames[i];
                }
            }
            this.saveIDs = new BlitableArray<int>(saveIDs.Length, Allocator.Persistent);
            for (int i = 0; i < saveIDs.Length; i++)
            {
                this.saveIDs[i] = saveIDs[i];
            }
        }

        public void RemoveAt(int index)
        {
            if (saveNames.Length == 0)
            {
                return;
            }
            saveNames[index].Dispose();
            var newSaveNames = new BlitableArray<Text>(saveNames.Length - 1, Allocator.Persistent);
            var newSaveIDs = new BlitableArray<int>(saveIDs.Length - 1, Allocator.Persistent);
            for (int i = 0; i < newSaveNames.Length; i++)
            {
                if (i < index)
                {
                    newSaveNames[i] = saveNames[i];
                    newSaveIDs[i] = saveIDs[i];
                }
                else
                {
                    newSaveNames[i] = saveNames[i + 1];
                    newSaveIDs[i] = saveIDs[i + 1];
                }
            }
            saveNames.Dispose();
            saveIDs.Dispose();
            saveNames = newSaveNames;
            saveIDs = newSaveIDs;
        }
    }
}