using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    //! Initializes Label's material.
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class LabelInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var buttonMaterial = UIManager.instance.materials.buttonMaterial;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<DestroyEntity, PanelUI>()
                .WithAll<InitializeEntity, RenderText>()    // Button, Label, Input elements
                .ForEach((Entity e, ZoxMesh zoxMesh) =>
            {
                zoxMesh.material = new UnityEngine.Material(buttonMaterial);
                zoxMesh.layer = worldUILayer;
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
        }
    }
}