using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Using the DestroyEntity event, also removes ViewerCameraLink camera.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
	public partial class ViewerCameraLinkDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyEntity, ViewerCameraLink>()
                .ForEach((int entityInQueryIndex, in ViewerCameraLink viewerCameraLink) =>
            {
                if (viewerCameraLink.camera.Index != 0)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, viewerCameraLink.camera);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}