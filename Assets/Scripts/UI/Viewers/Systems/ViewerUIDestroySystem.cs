using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Using the DestroyEntity event, also removes ViewerUI object.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
	public partial class ViewerUIDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DisableDestroyViewerUI>()
                .WithAll<DestroyEntity, ViewerUI>()
                .ForEach((int entityInQueryIndex, in ViewerUI viewerUI) =>
            {
                // UnityEngine.Debug.LogError("Destroying ViewerUI Target.");
                if (viewerUI.targetEntity.Index != 0 && HasComponent<ViewerObject>(viewerUI.targetEntity))
                {
                    if (HasComponent<DestroyEntityDirect>(viewerUI.targetEntity))
                    {
                        PostUpdateCommands.DestroyEntity(entityInQueryIndex, viewerUI.targetEntity);
                    }
                    else
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, viewerUI.targetEntity);
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}