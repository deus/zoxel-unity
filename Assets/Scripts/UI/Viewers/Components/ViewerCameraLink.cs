using Unity.Entities;

namespace Zoxel.UI
{
    //! A UIElement that pierces the veil into a new realm.
    public struct ViewerCameraLink : IComponentData
    {
        public Entity camera;

        public ViewerCameraLink(Entity camera)
        {
            this.camera = camera;
        }
    }
}
