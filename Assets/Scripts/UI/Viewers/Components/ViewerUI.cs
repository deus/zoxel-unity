using Unity.Entities;

namespace Zoxel.UI
{
    //! A UIElement that pierces the veil into a new realm.
    public struct ViewerUI : IComponentData
    {
        public Entity targetEntity;

        public ViewerUI(Entity targetEntity)
        {
            this.targetEntity = targetEntity;
        }
    }
}
