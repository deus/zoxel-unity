using Unity.Entities;

namespace Zoxel.UI
{
    //! A UIElement that pierces the veil into a new realm.
    public struct ViewerUILink : IComponentData
    {
        public Entity viewerUI;

        public ViewerUILink(Entity viewerUI)
        {
            this.viewerUI = viewerUI;
        }
    }
}
