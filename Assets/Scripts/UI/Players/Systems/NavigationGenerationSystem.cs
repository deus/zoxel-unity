﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Players;        
using Zoxel.Input;

namespace Zoxel.UI.Players
{
    //! Generates navigation data between UI for gamepad.
    /**
    *   At first it waits until all the UIElement's in the tree have positioned themselves properly.
    *
    *   Do this from navigator. Process all the ui that is navigational based on their ui links.
    *
    *   \todo Decouple this from Players and Input.
    *   \todo Move navigation data from navigator to just being put on the UI itself.
    *   \todo Move the SelectFirstUI event from this into a new system.
    *   \todo Convert button positions by adding parent positions. As currently this only works for child navigationalUIs of a panel.
    *   \todo Remove players from navigation generation
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class NavigationGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllersQuery;
        private EntityQuery charactersQuery;
        private EntityQuery uiQuery;
        private EntityQuery childrenQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllersQuery = GetEntityQuery(
                ComponentType.ReadWrite<Navigator>(),
                ComponentType.ReadOnly<Controller>());
            charactersQuery = GetEntityQuery(ComponentType.ReadOnly<ControllerLink>());
            uiQuery = GetEntityQuery(
                ComponentType.ReadOnly<LocalPosition>(),
                ComponentType.ReadOnly<UINavigational>());
            childrenQuery = GetEntityQuery(
                ComponentType.ReadOnly<Childrens>(),
                ComponentType.ReadOnly<UIElement>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // pass in controllerEntity navigator & navigational navigationalUIs
            var characterEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var controllerLinks = GetComponentLookup<ControllerLink>(true);
            characterEntities.Dispose();
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var navigators = GetComponentLookup<Navigator>(false);
            var controllers = GetComponentLookup<Controller>(true);
            var deviceTypeDatas = GetComponentLookup<DeviceTypeData>(true);
            controllerEntities.Dispose();
            var uiEntities = uiQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var localPositions = GetComponentLookup<LocalPosition>(true);
            var uiNavigationals = GetComponentLookup<UINavigational>(true);
            var navigationVisuals = GetComponentLookup<NavigationVisuals>(true);
            uiEntities.Dispose();
            var childrensEntities = childrenQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
			var childrens2 = GetComponentLookup<Childrens>(true);
            childrensEntities.Dispose();
            var panelEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleE);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleE);
            var footerLinks = GetComponentLookup<FooterLink>(true);
            var headerLinks = GetComponentLookup<HeaderLink>(true);
            panelEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, MeshUIDirty>()
                .WithNone<OnChildrenSpawned, DestroyChildren, GridUIDirty>()
                .ForEach((Entity e, int entityInQueryIndex, in Childrens childrens, in CharacterLink characterLink, in NavigationDirty navigationDirty) =>
            {
                var controllerEntity = characterLink.character;
                if (controllerLinks.HasComponent(characterLink.character))
                {
                    controllerEntity = controllerLinks[characterLink.character].controller;
                }
                if (!controllers.HasComponent(controllerEntity) || !navigators.HasComponent(controllerEntity))
                {
                    PostUpdateCommands.RemoveComponent<NavigationDirty>(entityInQueryIndex, e);
                    return;
                }
                if (HasComponent<UISelectEvent>(controllerEntity))
                {
                    return;
                }
                var controller = controllers[controllerEntity];
                var deviceTypeData = deviceTypeDatas[controllerEntity];
                var navigator = navigators[controllerEntity];
                var allChildrens = new NativeList<Entity>();
                var navigationalUIs = new NativeList<Entity>();
                GatherChildren(ref allChildrens, in childrens, in childrens2);
                if (headerLinks.HasComponent(e))
                {
                    var headerEntity = headerLinks[e].header;
                    if (childrens2.HasComponent(headerEntity))
                    {
                        var headerChildrens = childrens2[headerEntity];
                        GatherChildren(ref allChildrens, in headerChildrens, in childrens2);
                    }
                }
                if (footerLinks.HasComponent(e))
                {
                    var footerEntity = footerLinks[e].footer;
                    if (childrens2.HasComponent(footerEntity))
                    {
                        var footerChildrens = childrens2[footerEntity];
                        GatherChildren(ref allChildrens, in footerChildrens, in childrens2);
                    }
                }
                // wait for loading to finish
                for (int i = 0; i < allChildrens.Length; i++)
                {
                    var child = allChildrens[i];
                    if (HasComponent<GridUIDirty>(child) || HasComponent<OnChildrenSpawned>(child))
                    {
                        // UnityEngine.Debug.LogError("Still loading Children UI");
                        return;
                    }
                    else if (HasComponent<SetUIElementPosition>(child))
                    {
                        // UnityEngine.Debug.LogError("Still loading Children UI (v2)");
                        return;
                    }
                    else if (HasComponent<InitializeButtonColors>(child))
                    {
                        // UnityEngine.Debug.LogError("Still loading Children UI (v2.5)");
                        return;
                    }
                    else if (HasComponent<SetGridPosition>(child))
                    {
                        // UnityEngine.Debug.LogError("Still loading Children UI (v3)");
                        return;
                    }
                    else if (HasComponent<SpawnButtonsList>(child))
                    {
                        // UnityEngine.Debug.LogError("Still loading Children UI (v4)");
                        return;
                    }
                }
                PostUpdateCommands.RemoveComponent<NavigationDirty>(entityInQueryIndex, e);
                for (int i = 0; i < allChildrens.Length; i++)
                {
                    var childEntity = allChildrens[i];
                    if (uiNavigationals.HasComponent(childEntity))
                    {
                        navigationalUIs.Add(childEntity);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, childEntity, new NavigatorLink(controllerEntity));
                        // UnityEngine.Debug.LogError("Navigation Child Added: " + i);
                    }
                }
                allChildrens.Dispose();
                // check if any elements are still loading
                var isActionbar = HasComponent<SelectFirstNavigation>(e);
                var isGamepad = deviceTypeData.type == DeviceType.Gamepad;
                var isInGame = controller.mapping == ControllerMapping.InGame;
                // Deselect Old UI
                if (isGamepad && navigator.selected.Index != 0)
                {
                    // PostUpdateCommands.AddComponent<VisualDeselectEvent>(entityInQueryIndex, navigator.selected);
                    if (navigationVisuals.HasComponent(navigator.selected) && !HasComponent<DestroyEntity>(navigator.selected))
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, navigator.selected, NavigationAnimation.DeselectEvent(elapsedTime));
                    }
                }
                if (navigationalUIs.Length == 0)
                {
                    navigator.Dispose();
                    navigators[controllerEntity] = navigator;
                    navigationalUIs.Dispose();
                    return;
                }
                var positions = new NativeArray<float2>(navigationalUIs.Length, Allocator.Temp);
                var navigationParents = new NativeArray<Entity>(navigationalUIs.Length, Allocator.Temp);
                var newNavigationElements = new NativeList<NavigateData>();
                for (byte i = 0; i < navigationalUIs.Length; i++)
                {
                    var uiEntity = navigationalUIs[i];
                    var position = localPositions[uiEntity].position;
                    positions[i] = new float2(position.x, position.y);
                    // UnityEngine.Debug.LogError("Position at: " + i + " is " + position + " EntityIndex: " + uiEntity.Index);
                    navigationParents[i] = uiEntity;
                    if (isGamepad || isInGame || isActionbar)
                    {
                        //! Select first selected ui in group.
                        if ((navigationDirty.selected.Index == 0 && i == navigationDirty.selectedIndex) || (navigationDirty.selected.Index != 0 && navigationDirty.selected == uiEntity))
                        {
                            if (navigationVisuals.HasComponent(navigator.selected) && !HasComponent<DestroyEntity>(navigator.selected))
                            {
                                var navigationAnimation = NavigationAnimation.DeselectEvent(elapsedTime);
                                PostUpdateCommands.AddComponent(entityInQueryIndex, navigator.selected, navigationAnimation);
                                PostUpdateCommands.AddComponent(entityInQueryIndex, navigator.selected, new UIDeselectEvent(controllerEntity, uiEntity));
                            }
                            navigator.selected = uiEntity;
                            PostUpdateCommands.AddComponent(entityInQueryIndex, uiEntity, new UISelectEvent(controllerEntity));
                            var navigationAnimation2 = NavigationAnimation.SelectEvent(elapsedTime);
                            PostUpdateCommands.AddComponent(entityInQueryIndex, uiEntity, navigationAnimation2);
                            //UnityEngine.Debug.LogError("Selecting New UI: " + navigator.selected.Index);
                        }
                    }
                }
                // Set Selected Panel
                if (positions.Length == 1)
                {
                    var element = new NavigateData();
                    element.targetPosition = positions[0];
                    element.direction = 0;
                    element.panel = navigationParents[0];
                    element.button = navigationalUIs[0];
                    newNavigationElements.Add(element);
                }
                else
                {
                    for (int i = 0; i < positions.Length; i++)
                    {
                        var position = positions[i];
                        AddNavigationUI(NavigationDirection.Up, ref newNavigationElements, in navigationalUIs, navigationParents, positions, position, i);
                        AddNavigationUI(NavigationDirection.Down, ref newNavigationElements, in navigationalUIs, navigationParents, positions, position, i);
                        AddNavigationUI(NavigationDirection.Left, ref newNavigationElements, in navigationalUIs, navigationParents, positions, position, i);
                        AddNavigationUI(NavigationDirection.Right, ref newNavigationElements, in navigationalUIs, navigationParents, positions, position, i);
                    }
                }
                var combinedElementsCount = newNavigationElements.Length;
                navigator.Dispose();
                navigator.navigateData = new BlitableArray<NavigateData>(combinedElementsCount, Allocator.Persistent);
                for (int i = 0; i < combinedElementsCount; i++)
                {
                    navigator.navigateData[i] = newNavigationElements[i];
                }
                navigators[controllerEntity] = navigator;
                //UnityEngine.Debug.LogError("newNavigationData: " + newNavigationElements.Length);
                positions.Dispose();
                navigationParents.Dispose();
                newNavigationElements.Dispose();
                navigationalUIs.Dispose();
            })  .WithReadOnly(controllers)
                .WithReadOnly(deviceTypeDatas)
                .WithReadOnly(controllerLinks)
                .WithReadOnly(localPositions)
                .WithReadOnly(uiNavigationals)
                .WithReadOnly(navigationVisuals)
                .WithReadOnly(childrens2)
                .WithReadOnly(headerLinks)
                .WithReadOnly(footerLinks)
                .WithNativeDisableContainerSafetyRestriction(navigators)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static void GatherChildren(ref NativeList<Entity> allChildren, in Childrens childrens, in ComponentLookup<Childrens> childrensInjected)
        {
            for (int i = 0; i < childrens.children.Length; i++)
            {
                var child = childrens.children[i];
                allChildren.Add(child);
                if (childrensInjected.HasComponent(child))
                {
                    var childrensChildren = childrensInjected[child];
                    GatherChildren(ref allChildren, in childrensChildren, in childrensInjected);
                }
            }
        }

        // Add navigation elements for one ui, given a direction
       private static void AddNavigationUI(NavigationDirection newDirection, ref NativeList<NavigateData> newNavigationElements,
            in NativeList<Entity> navigationalUIs, NativeArray<Entity> parents, NativeArray<float2> positions, float2 position, int i)
        {
            var closest = float.MaxValue; // 10000
            var closestIndex = -1;
            for (int j = 0; j < positions.Length; j++)
            {
                if (i != j)
                {
                    var otherPosition = positions[j];
                    float distanceX = math.abs(otherPosition.x - position.x);
                    float distanceY = math.abs(otherPosition.y - position.y);
                    if (newDirection == NavigationDirection.Down)
                    {
                        if (otherPosition.y < position.y)
                        {
                            var closest2 = distanceX * 2 + distanceY;
                            if (closest2 < closest)
                            {
                                closest = closest2;
                                closestIndex = j;
                            }
                        }
                    }
                    else if (newDirection == NavigationDirection.Up)
                    {
                        if (otherPosition.y > position.y)
                        {
                            var closest2 = distanceX * 2 + distanceY;
                            if (closest2 < closest)
                            {
                                closest = closest2;
                                closestIndex = j;
                            }
                        }
                    }
                    else if (newDirection == NavigationDirection.Left)
                    {
                        if (otherPosition.x < position.x)
                        {
                            var closest2 = distanceX + distanceY * 2;
                            if (closest2 < closest)
                            {
                                closest = closest2;
                                closestIndex = j;
                            }
                        }
                    }
                    else if (newDirection == NavigationDirection.Right)
                    {
                        if (otherPosition.x > position.x)
                        {
                            var closest2 = distanceX + distanceY * 2;
                            if (closest2 <= closest)
                            {
                                closest = closest2;
                                closestIndex = j;
                            }
                        }
                    }
                }
            }
            // flipp navigation around screen border
            if (closestIndex == -1)
            {
                var originalPosition = position;
                if (newDirection == NavigationDirection.Left)
                {
                    position.x += 9999;
                }
                else if (newDirection == NavigationDirection.Right)
                {
                    position.x -= 9999;
                }
                else if (newDirection == NavigationDirection.Up)
                {
                    position.y -= 9999;
                }
                else if (newDirection == NavigationDirection.Down)
                {
                    position.y += 9999;
                }
                for (int j = 0; j < positions.Length; j++)
                {
                    if (i != j)
                    {
                        var otherPosition = positions[j];
                        float distanceX = math.abs(otherPosition.x - position.x);
                        float distanceY = math.abs(otherPosition.y - position.y);
                        if (newDirection == NavigationDirection.Down)
                        {
                            if (otherPosition.y != originalPosition.y && otherPosition.y < position.y)
                            {
                                var closest2 = distanceX * 2 + distanceY;
                                if (closest2 < closest)
                                {
                                    closest = closest2;
                                    closestIndex = j;
                                }
                            }
                        }
                        else if (newDirection == NavigationDirection.Up)
                        {
                            if (otherPosition.y != originalPosition.y && otherPosition.y > position.y)
                            {
                                var closest2 = distanceX * 2 + distanceY;
                                if (closest2 < closest)
                                {
                                    closest = closest2;
                                    closestIndex = j;
                                }
                            }
                        }
                        else if (newDirection == NavigationDirection.Left)
                        {
                            if (otherPosition.x != originalPosition.x && otherPosition.x != position.x)
                            {
                                var closest2 = distanceX + distanceY * 2;
                                if (closest2 < closest)
                                {
                                    closest = closest2;
                                    closestIndex = j;
                                }
                            }
                        }
                        else if (newDirection == NavigationDirection.Right)
                        {
                            if (otherPosition.x != originalPosition.x && otherPosition.x > position.x)
                            {
                                var closest2 = distanceX + distanceY * 2;
                                if (closest2 <= closest)
                                {
                                    closest = closest2;
                                    closestIndex = j;
                                }
                            }
                        }
                    }
                }
            }
            if (closestIndex != -1)
            {
                // UnityEngine.Debug.LogError("Adding Closest (" + newDirection + ") to UI at: " + positions[i] + " is " + positions[closestIndex]);
                var element = new NavigateData();
                element.button = navigationalUIs[closestIndex];
                element.panel = parents[closestIndex];
                element.targetPosition = positions[closestIndex];
                element.previousIndex = i;
                element.previousPosition = positions[i];
                element.targetIndex = closestIndex;
                element.direction = (byte) newDirection;
                newNavigationElements.Add(element);
            }
            /*else
            {
                UnityEngine.Debug.LogError("Closest Index not found for position: " + positions[i]);
            }*/
        }

    }
}
// todo:
//      Since i am now using two players, I need to separate navigation data from the Navigator
//      Put Navigation data in seperate entity
//      Generate navigation data when any player selects the UI Panel
//      Or maybe I just need separate selectors - frame overlays per player on the navigationalUIs
//      Or set to color depending on the player that selects it
// If update positions in UI, update this
// for all entities, add a navigationAnimation in it
//      navigationStarterSystem will handle links
//      calculate links here for closest positions
//  SelectedSystem - Lerps colour of UI when selected
// For all children add navigation components
//      NavigationStarter system will calculate the rest