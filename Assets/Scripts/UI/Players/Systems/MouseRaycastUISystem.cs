using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Cameras;
using Zoxel.Players;
using Zoxel.Input;

namespace Zoxel.UI.Players
{
    //! A mouse raycasts over the UI through a camera.
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class MouseRaycastUISystem : SystemBase
	{
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery processQuery;
		private EntityQuery uisQuery;
		private EntityQuery camerasQuery;
		private EntityQuery devicesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			uisQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<DisableUIRaycast>(),
                ComponentType.ReadOnly<RaycastUIOrigin>(),
                ComponentType.ReadOnly<UINavigational>());
        	camerasQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Camera>());
            devicesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Device>());
            RequireForUpdate(processQuery);
            RequireForUpdate(uisQuery);
            RequireForUpdate(camerasQuery);
            RequireForUpdate(devicesQuery);
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var deviceEntities = devicesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var mouses = GetComponentLookup<Mouse>(true);
            var touchpads = GetComponentLookup<Touchpad>(true);
            deviceEntities.Dispose();
            var uiEntities = uisQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA).AsArray();
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var navigatorLinks = GetComponentLookup<NavigatorLink>(true);
			var raycastUIOrigins = GetComponentLookup<RaycastUIOrigin>(true);
			var size2Ds = GetComponentLookup<Size2D>(true);
			var raycastUISizes = GetComponentLookup<RaycastUISize>(true);
			var navigationVisuals = GetComponentLookup<NavigationVisuals>(true);
			var materialBaseColors = GetComponentLookup<MaterialBaseColor>(false);
			var materialFrameColors = GetComponentLookup<MaterialFrameColor>(false);
            var cameraEntities = camerasQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var cameras = GetComponentLookup<Camera>(true);
			var cameraScreenRects = GetComponentLookup<CameraScreenRect>(true);
            cameraEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<SetControllerMapping>()
                .ForEach((Entity e, int entityInQueryIndex, ref Navigator navigator, ref NavigatorSelected navigatorSelected,
                    in Controller controller, in DeviceTypeData deviceTypeData,
                    in CameraLink cameraLink, in ConnectedDevices connectedDevices) =>
            {
                if (controller.lockState != (int) UnityEngine.CursorLockMode.None || controller.mapping == ControllerMapping.Input)
                {
                    navigatorSelected.mouseOverEntity = new Entity();
                    return;
                }
                var targetDeviceEntity = new Entity();
                var mainPointerPosition = float2.zero;
                for (byte i = 0; i < connectedDevices.devices.Length; i++)
                {
                    var deviceEntity = connectedDevices.devices[i];
                    if (mouses.HasComponent(deviceEntity))
                    {
                        targetDeviceEntity = deviceEntity;
                        mainPointerPosition = mouses[deviceEntity].position;
                        break;
                    }
                    else if (touchpads.HasComponent(deviceEntity))
                    {
                        var touchpad = touchpads[deviceEntity];
                        if (touchpad.touchesCount > 0)
                        {
                            targetDeviceEntity = deviceEntity;
                            mainPointerPosition = touchpad.fingers[0].position;
                            break;
                        }
                    }
                }
                if (targetDeviceEntity.Index == 0)
                {
                    return;
                }
                var cameraEntity = cameraLink.camera;
                if (!cameras.HasComponent(cameraEntity))
                {
                    return;
                }
                var camera = cameras[cameraEntity];
                var cameraScreenDimensions = camera.screenDimensions.ToFloat2();
                var cameraScreenRect = cameraScreenRects[cameraEntity];
                var frustrumSize = camera.GetFrustrumSize();
                var mainPointer = MouseUtil.GetPointer(mainPointerPosition, cameraScreenDimensions, cameraScreenRect.rect);
                var foundUIEntity = RaycastUIs(mainPointer, frustrumSize, in uiEntities, in raycastUIOrigins, in size2Ds, in raycastUISizes);
                navigatorSelected.mouseOverEntity = foundUIEntity;
                if (foundUIEntity.Index > 0)
                {
                    if (navigatorLinks.HasComponent(foundUIEntity))
                    {
                        var navigatorLink = navigatorLinks[foundUIEntity];
                        if (navigatorLink.navigator == e)
                        {
                            if (navigator.selected != foundUIEntity)
                            {
                                //! New UI was selected. Add events.
                                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SelectionEvent(SelectionEventType.MouseRaycast, e, foundUIEntity));
                                if (foundUIEntity.Index != 0 && navigationVisuals.HasComponent(foundUIEntity))
                                {
                                    var navigationVisual = navigationVisuals[foundUIEntity];
                                    materialBaseColors[foundUIEntity] = new MaterialBaseColor(navigationVisual.selectedFillColor);
                                    materialFrameColors[foundUIEntity] = new MaterialFrameColor(navigationVisual.selectedFrameColor);
                                }
                            }
                        }
                    }
                }
                //! Deselection of old entity.
                else if (foundUIEntity.Index == 0 && navigator.selected.Index != 0)
                {
                    if (navigator.selected.Index != 0 && navigationVisuals.HasComponent(navigator.selected))
                    {
                        var navigationVisual = navigationVisuals[navigator.selected];
                        materialBaseColors[navigator.selected] = new MaterialBaseColor(navigationVisual.fillColor);
                        materialFrameColors[navigator.selected] = new MaterialFrameColor(navigationVisual.frameColor);
                    }
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SelectionEvent(SelectionEventType.MouseRaycast, e, new Entity()));
                }
                if (touchpads.HasComponent(targetDeviceEntity))
                {
                    navigatorSelected.Dispose();
                    var touchpad = touchpads[targetDeviceEntity];
                    for (byte i = 0; i < touchpad.fingers.Length; i++)
                    {
                        var finger = touchpad.fingers[i];
                        if (finger.previousState == FingerState.Moved || finger.state == FingerState.Moved || finger.state == FingerState.Begin) // finger.active) // 
                        {
                            var pointerPosition = finger.position;
                            var pointer = MouseUtil.GetPointer(pointerPosition, cameraScreenDimensions, cameraScreenRect.rect);
                            var mouseOverEntity = RaycastUIs(pointer, frustrumSize, in uiEntities, in raycastUIOrigins, in size2Ds, in raycastUISizes);
                            //! \todo Support multiple mouseOverEntities
                            navigatorSelected.AddMouseOver(mouseOverEntity, i);
                        }
                    }
                }
            })  .WithReadOnly(uiEntities)
                .WithReadOnly(navigatorLinks)
                .WithReadOnly(raycastUIOrigins)
                .WithReadOnly(size2Ds)
                .WithReadOnly(raycastUISizes)
                .WithReadOnly(navigationVisuals)
                .WithReadOnly(cameras)
                .WithReadOnly(cameraScreenRects)
                .WithReadOnly(mouses)
                .WithReadOnly(touchpads)
                .WithDisposeOnCompletion(uiEntities)
                .WithNativeDisableContainerSafetyRestriction(materialBaseColors)
                .WithNativeDisableContainerSafetyRestriction(materialFrameColors)
			    .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}

        private static Entity RaycastUIs(float2 pointer, float2 frustrumSize,
            in NativeArray<Entity> uiEntities,
            in ComponentLookup<RaycastUIOrigin> raycastUIOrigins,
            in ComponentLookup<Size2D> size2Ds,
            in ComponentLookup<RaycastUISize> raycastUISizes)
        {
            for (int i = 0; i < uiEntities.Length; i++)
            {
                var uiEntity = uiEntities[i];
                if (!raycastUIOrigins.HasComponent(uiEntity))
                {
                    continue;
                }
                var uiElement = raycastUIOrigins[uiEntity];
                var size2D = size2Ds[uiEntity];
                var position = uiElement.position;
                var size = size2D.size;
                if (raycastUISizes.HasComponent(uiEntity))
                {
                    size = raycastUISizes[uiEntity].size;
                }
                size.x /= frustrumSize.x;
                size.y /= frustrumSize.y;
                position = position - size / 2f;
                //! Check basic bounds for uis!
                if (pointer.x >= position.x && pointer.x <= position.x + size.x &&
                    pointer.y >= position.y && pointer.y <= position.y + size.y)
                {
                    return uiEntity;
                }
            }
            return new Entity();
        }
    }
}

// controller.mapping != ControllerMapping.InGame && controller.mapping != ControllerMapping.None &&
                // UnityEngine.Debug.LogError("Camera Mouse: " + e.Index + " is at: " + cameraScreenRect.position);
                // Collect all uis that mouse interacts with
                // then get topmost layer
                //! \todo Sort UIEntities by distance to mouse pointer?