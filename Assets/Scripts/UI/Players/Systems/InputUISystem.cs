using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Audio;
using Zoxel.Players;
using Zoxel.Input;

namespace Zoxel.UI.Players
{
    //! This is for input into an input field.
    /**
    *   \todo Fix slowness of the key inputs
    *   \todo Add shift for capitals
    *   \todo Add gamepad selection for input - with a fullscreen mode / or half screen mode
    *   \todo Add a characters only for names
    *   \todo Add a numbers only input for seed inputs
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class InputUISystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery renderTextQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            renderTextQuery = GetEntityQuery(
                ComponentType.Exclude<CopyToClipboard>(),
                ComponentType.ReadOnly<RenderText>(),
                ComponentType.ReadOnly<InputField>(),
                ComponentType.ReadOnly<InputActivated>());
            RequireForUpdate(processQuery);
            RequireForUpdate(renderTextQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var firstTiming = 0.5f;
            var repeatTiming = 0.12f;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var renderTextEntities = renderTextQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var renderTexts = GetComponentLookup<RenderText>(true);
            renderTextEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<SetControllerMapping>()
                .ForEach((Entity e, int entityInQueryIndex, ref Navigator navigator, in Controller controller) =>
            {
                var selected = navigator.selected;
                var timePassed = elapsedTime - navigator.lastInputTime;
                var hasInputField = HasComponent<InputField>(selected);
                if (!hasInputField || !renderTexts.HasComponent(selected))
                {
                    return;
                }
                if (controller.keyboard.leftControlKey.isPressed == 1 && controller.keyboard.cKey.wasPressedThisFrame == 1)
                {
                    PostUpdateCommands.AddComponent<CopyToClipboard>(entityInQueryIndex, selected);
                    return;
                }
                else if (controller.keyboard.leftControlKey.isPressed == 1 && controller.keyboard.vKey.wasPressedThisFrame == 1)
                {
                    PostUpdateCommands.AddComponent<PasteFromClipboard>(entityInQueryIndex, selected);
                    return;
                }
                var numbersOnly = HasComponent<InputNumbersOnly>(selected);
                //if (navigator.lastInputKey != 255)
                {
                    byte overrideKeyPress = 255;
                    byte addCharacter = 255;
                    byte isUpperCase = 0;
                    if (!numbersOnly)
                    {
                        if (controller.keyboard.aKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('a');
                        }
                        else if (controller.keyboard.bKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('b');
                        }
                        else if (controller.keyboard.cKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('c');
                        }
                        else if (controller.keyboard.dKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('d');
                        }
                        else if (controller.keyboard.eKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('e');
                        }
                        else if (controller.keyboard.fKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('f');
                        }
                        else if (controller.keyboard.gKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('g');
                        }
                        else if (controller.keyboard.hKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('h');
                        }
                        else if (controller.keyboard.iKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('i');
                        }
                        else if (controller.keyboard.jKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('j');
                        }
                        else if (controller.keyboard.kKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('k');
                        }
                        else if (controller.keyboard.lKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('l');
                        }
                        else if (controller.keyboard.mKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('m');
                        }
                        else if (controller.keyboard.nKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('n');
                        }
                        else if (controller.keyboard.oKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('o');
                        }
                        else if (controller.keyboard.pKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('p');
                        }
                        else if (controller.keyboard.qKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('q');
                        }
                        else if (controller.keyboard.rKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('r');
                        }
                        else if (controller.keyboard.sKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('s');
                        }
                        else if (controller.keyboard.tKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('t');
                        }
                        else if (controller.keyboard.uKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('u');
                        }
                        else if (controller.keyboard.vKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('v');
                        }
                        else if (controller.keyboard.wKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('w');
                        }
                        else if (controller.keyboard.xKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('x');
                        }
                        else if (controller.keyboard.yKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('y');
                        }
                        else if (controller.keyboard.zKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte('z');
                        }
                        else if (controller.keyboard.spaceKey.wasPressedThisFrame == 1)
                        {
                            overrideKeyPress = Text.ConvertCharToByte(' ');
                        }
                        if (overrideKeyPress >= 10 && overrideKeyPress < 36)
                        {
                            if (controller.keyboard.leftShiftKey.isPressed == 1)
                            {
                                isUpperCase = 1;
                                // overrideKeyPress = (byte) (overrideKeyPress + 26);
                            }
                        }
                    }
                    if (!numbersOnly)
                    {
                        if (controller.keyboard.aKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('a'))
                        {
                            addCharacter = Text.ConvertCharToByte('a');
                        }
                        else if (controller.keyboard.bKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('b'))
                        {
                            addCharacter = Text.ConvertCharToByte('b');
                        }
                        else if (controller.keyboard.cKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('c'))
                        {
                            addCharacter = Text.ConvertCharToByte('c');
                        }
                        else if (controller.keyboard.dKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('d'))
                        {
                            addCharacter = Text.ConvertCharToByte('d');
                        }
                        else if (controller.keyboard.eKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('e'))
                        {
                            addCharacter = Text.ConvertCharToByte('e');
                        }
                        else if (controller.keyboard.fKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('f'))
                        {
                            addCharacter = Text.ConvertCharToByte('f');
                        }
                        else if (controller.keyboard.gKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('g'))
                        {
                            addCharacter = Text.ConvertCharToByte('g');
                        }
                        else if (controller.keyboard.hKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('h'))
                        {
                            addCharacter = Text.ConvertCharToByte('h');
                        }
                        else if (controller.keyboard.iKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('i'))
                        {
                            addCharacter = Text.ConvertCharToByte('i');
                        }
                        else if (controller.keyboard.jKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('j'))
                        {
                            addCharacter = Text.ConvertCharToByte('j');
                        }
                        else if (controller.keyboard.kKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('k'))
                        {
                            addCharacter = Text.ConvertCharToByte('k');
                        }
                        else if (controller.keyboard.lKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('l'))
                        {
                            addCharacter = Text.ConvertCharToByte('l');
                        }
                        else if (controller.keyboard.mKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('m'))
                        {
                            addCharacter = Text.ConvertCharToByte('m');
                        }
                        else if (controller.keyboard.nKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('n'))
                        {
                            addCharacter = Text.ConvertCharToByte('n');
                        }
                        else if (controller.keyboard.oKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('o'))
                        {
                            addCharacter = Text.ConvertCharToByte('o');
                        }
                        else if (controller.keyboard.pKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('p'))
                        {
                            addCharacter = Text.ConvertCharToByte('p');
                        }
                        else if (controller.keyboard.qKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('q'))
                        {
                            addCharacter = Text.ConvertCharToByte('q');
                        }
                        else if (controller.keyboard.rKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('r'))
                        {
                            addCharacter = Text.ConvertCharToByte('r');
                        }
                        else if (controller.keyboard.sKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('s'))
                        {
                            addCharacter = Text.ConvertCharToByte('s');
                        }
                        else if (controller.keyboard.tKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('t'))
                        {
                            addCharacter = Text.ConvertCharToByte('t');
                        }
                        else if (controller.keyboard.uKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('u'))
                        {
                            addCharacter = Text.ConvertCharToByte('u');
                        }
                        else if (controller.keyboard.vKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('v'))
                        {
                            addCharacter = Text.ConvertCharToByte('v');
                        }
                        else if (controller.keyboard.wKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('w'))
                        {
                            addCharacter = Text.ConvertCharToByte('w');
                        }
                        else if (controller.keyboard.xKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('x'))
                        {
                            addCharacter = Text.ConvertCharToByte('x');
                        }
                        else if (controller.keyboard.yKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('y'))
                        {
                            addCharacter = Text.ConvertCharToByte('y');
                        }
                        else if (controller.keyboard.zKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte('z'))
                        {
                            addCharacter = Text.ConvertCharToByte('z');
                        }
                        else if (controller.keyboard.spaceKey.isPressed == 1 && navigator.lastInputKey == Text.ConvertCharToByte(' '))
                        {
                            addCharacter = Text.ConvertCharToByte(' ');
                        }
                        if (addCharacter >= 10 && addCharacter < 36)
                        {
                            if (controller.keyboard.leftShiftKey.isPressed == 1)
                            {
                                isUpperCase = 1;
                                // addCharacter = (byte) (addCharacter + 26);
                            }
                        }
                    }
                    if (addCharacter == 255)
                    {
                        if (controller.keyboard.digit0Key.isPressed == 1)
                        {
                            addCharacter = Text.ConvertCharToByte('0');
                        }
                        else if (controller.keyboard.digit1Key.isPressed == 1)
                        {
                            addCharacter = Text.ConvertCharToByte('1');
                        }
                        else if (controller.keyboard.digit2Key.isPressed == 1)
                        {
                            addCharacter = Text.ConvertCharToByte('2');
                        }
                        else if (controller.keyboard.digit3Key.isPressed == 1)
                        {
                            addCharacter = Text.ConvertCharToByte('3');
                        }
                        else if (controller.keyboard.digit4Key.isPressed == 1)
                        {
                            addCharacter = Text.ConvertCharToByte('4');
                        }
                        else if (controller.keyboard.digit5Key.isPressed == 1)
                        {
                            addCharacter = Text.ConvertCharToByte('5');
                        }
                        else if (controller.keyboard.digit6Key.isPressed == 1)
                        {
                            addCharacter = Text.ConvertCharToByte('6');
                        }
                        else if (controller.keyboard.digit7Key.isPressed == 1)
                        {
                            addCharacter = Text.ConvertCharToByte('7');
                        }
                        else if (controller.keyboard.digit8Key.isPressed == 1)
                        {
                            addCharacter = Text.ConvertCharToByte('8');
                        }
                        else if (controller.keyboard.digit9Key.isPressed == 1)
                        {
                            addCharacter = Text.ConvertCharToByte('9');
                        }
                        else if (controller.keyboard.minusKey.isPressed == 1)
                        {
                            addCharacter = Text.ConvertCharToByte('-');
                        }
                        // special functions
                        else if (controller.keyboard.backspaceKey.isPressed == 1)
                        {
                            addCharacter = 254;
                        }
                        else if (controller.keyboard.enterKey.wasPressedThisFrame == 1 || controller.keyboard.escapeKey.isPressed == 1)
                        {
                            // Stop using InputActivated
                            PostUpdateCommands.RemoveComponent<InputActivated>(entityInQueryIndex, selected);
                            PostUpdateCommands.AddComponent<InputFinished>(entityInQueryIndex, selected);
                            PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetControllerMapping(ControllerMapping.Menu));
                        }
                    }
                    if (overrideKeyPress != 255)
                    {
                        addCharacter = overrideKeyPress;
                    }
                    if (addCharacter == 255)
                    {
                        navigator.lastInputKey = 255;
                        navigator.isFirstKey = 0;
                        return;
                    }
                    var timing = 0f;
                    // if new key press
                    // if (navigator.lastInputKey != 255 && navigator.lastInputKey != addCharacter)
                    if (overrideKeyPress != 255)
                    {
                        timing = 0f;
                    }
                    else if (navigator.isFirstKey == 1)
                    {
                        timing = firstTiming;
                    }
                    else if (navigator.lastInputKey != 255)
                    {
                        timing = repeatTiming;
                    }
                    if (timePassed >= timing)
                    {
                        if (navigator.isFirstKey == 1)
                        {
                            navigator.isFirstKey = 0;
                        }
                        if (navigator.lastInputKey != addCharacter)
                        {
                            navigator.isFirstKey = 1;
                        }
                        navigator.lastInputKey = addCharacter;
                        navigator.lastInputTime = elapsedTime;
                        if (addCharacter == 254)
                        {
                            PostUpdateCommands.AddComponent<BackspaceRenderText>(entityInQueryIndex, selected);
                        }
                        else
                        {
                            if (isUpperCase == 1)
                            {
                                addCharacter = (byte) (addCharacter + 26);
                            }
                            PostUpdateCommands.AddComponent(entityInQueryIndex, selected, new AddCharToRenderText { addCharacter = addCharacter });
                        }
                    }
                }
            })  .WithReadOnly(renderTexts)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}