using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI.Players
{
    //! Cleans up Navigator / NavigatorSelected data in memory.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class NavigatorDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, Navigator>()
                .ForEach((in Navigator navigator) =>
			{
                navigator.Dispose();
			}).ScheduleParallel();
			Entities
                .WithAll<DestroyEntity, NavigatorSelected>()
                .ForEach((in NavigatorSelected navigatorSelected) =>
			{
                navigatorSelected.Dispose();
			}).ScheduleParallel();
        }
    }
}