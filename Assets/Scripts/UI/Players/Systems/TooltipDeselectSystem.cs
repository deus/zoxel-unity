using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.UI
{
    //! Handles ButtonSelectEvents for ItemButton's.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class TooltipDeselectSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllerQuery;
        private EntityQuery panelQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllerQuery = GetEntityQuery(
                ComponentType.ReadOnly<Player>(),
                ComponentType.ReadOnly<PlayerTooltipLinks>());
            panelQuery = GetEntityQuery(
                ComponentType.ReadOnly<PanelUI>(),
                ComponentType.ReadOnly<PanelTooltipLink>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = controllerQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var playerTooltipLinks = GetComponentLookup<PlayerTooltipLinks>(true);
            controllerEntities.Dispose();
            var panelEntities = panelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var panelTooltipLinks = GetComponentLookup<PanelTooltipLink>(true);
            panelEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<UIDeselectEvent>()
                .ForEach((Entity e, int entityInQueryIndex, in PanelLink panelLink, in UIDeselectEvent uiDeselectEvent) =>
            {
                if (uiDeselectEvent.newSelected.Index != 0)
                {
                    // No need to clear text.
                    return;
                }
                // UnityEngine.Debug.LogError("[" + elapsedTime + "] ClearTooltips Old [" + e.Index + "] - New [" + uiDeselectEvent.newSelected.Index + "]");
                var controllerEntity = uiDeselectEvent.controller;
                var panelEntity = panelLink.panel;
                if (playerTooltipLinks.HasComponent(controllerEntity))
                {
                    var selectionTooltip = playerTooltipLinks[controllerEntity].selectionTooltip;
                    if (selectionTooltip.Index > 0)
                    {
                        PostUpdateCommands.AddComponent<ClearRenderText>(entityInQueryIndex, selectionTooltip);
                    }
                }
                if (panelTooltipLinks.HasComponent(panelEntity))
                {
                    var tooltip = panelTooltipLinks[panelEntity].tooltip;
                    if (tooltip.Index > 0)
                    {
                        PostUpdateCommands.AddComponent<ClearRenderText>(entityInQueryIndex, tooltip);
                    }
                }
            })  .WithReadOnly(playerTooltipLinks)
                .WithReadOnly(panelTooltipLinks)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}