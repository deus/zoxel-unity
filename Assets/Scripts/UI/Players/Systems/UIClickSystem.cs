﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Audio;
using Zoxel.Players;
using Zoxel.Input;

namespace Zoxel.UI.Players
{
    //! Used for player to navigate ui.
    /**
    *   Used for a Player to interact with the UI system by adding UIClickEvent events.
    *   Also plays a sound upon navigation.
    *   \todo With mobile touch, it lags a little bit.
    *   \todo Move this directly into raycast system? or something? Multiple selects at once?
    */
    [UpdateAfter(typeof(MouseRaycastUISystem))]
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class UIClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery uiQuery;
        private EntityQuery devicesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            uiQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<RaycastUIOrigin>());
            devicesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Device>());
            RequireForUpdate(processQuery);
            RequireForUpdate(uiQuery);
            RequireForUpdate(devicesQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uiEntities = uiQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var raycastUIOrigins = GetComponentLookup<RaycastUIOrigin>(true);
            var inputFields = GetComponentLookup<InputField>(true);
            //uiEntities.Dispose();
            var deviceEntities = devicesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var mouses = GetComponentLookup<Mouse>(true);
            var touchpads = GetComponentLookup<Touchpad>(true);
            var gamepads = GetComponentLookup<Gamepad>(true);
            //deviceEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<SetControllerMapping>()
                .ForEach((Entity e, int entityInQueryIndex, in Controller controller, in DeviceTypeData deviceTypeData,
                    in Navigator navigator, in NavigatorSelected navigatorSelected,
                    in ConnectedDevices connectedDevices, in CameraLink cameraLink) =>
            {
                var selectedUIEntity = navigator.selected;
                if (deviceTypeData.type != DeviceType.Gamepad)
                {
                    selectedUIEntity = navigatorSelected.mouseOverEntity;
                }
                if (!raycastUIOrigins.HasComponent(selectedUIEntity) || HasComponent<UIClickEvent>(selectedUIEntity))
                {
                    return;
                }
                //! If cancel button, don't add events?
                var targetDeviceEntity = new Entity();
                for (byte i = 0; i < connectedDevices.devices.Length; i++)
                {
                    var deviceEntity = connectedDevices.devices[i];
                    if (gamepads.HasComponent(deviceEntity) || mouses.HasComponent(deviceEntity) || touchpads.HasComponent(deviceEntity))
                    {
                        targetDeviceEntity = deviceEntity;
                        break;
                    }
                }
                if (targetDeviceEntity.Index == 0)
                {
                    return;
                }
                var buttonActionType = ButtonActionType.None;
                if (gamepads.HasComponent(targetDeviceEntity)) // deviceTypeData.type == DeviceType.Gamepad)
                {
                    var gamepad = gamepads[targetDeviceEntity];
                    if (gamepad.startButton.wasPressedThisFrame == 1 || gamepad.buttonA.wasPressedThisFrame == 1)
                    {
                        buttonActionType = ButtonActionType.Confirm;
                    }
                    else if (gamepad.buttonB.wasPressedThisFrame == 1)
                    {
                        buttonActionType = ButtonActionType.Cancel;
                    }
                    else if (gamepad.buttonX.wasPressedThisFrame == 1)
                    {
                        buttonActionType = ButtonActionType.Action1;
                    }
                    else if (gamepad.buttonY.wasPressedThisFrame == 1)
                    {
                        buttonActionType = ButtonActionType.Action2;
                    } 
                    else if (gamepad.buttonLT.wasPressedThisFrame == 1)
                    {
                        buttonActionType = ButtonActionType.Action3;
                    }
                    else if (gamepad.buttonRT.wasPressedThisFrame == 1)
                    {
                        buttonActionType = ButtonActionType.Action4;
                    } 
                    else if (gamepad.buttonLB.wasPressedThisFrame == 1)
                    {
                        buttonActionType = ButtonActionType.Left1;
                    }
                    else if (gamepad.buttonRB.wasPressedThisFrame == 1)
                    {
                        buttonActionType = ButtonActionType.Right1;
                    }
                }
                else if (mouses.HasComponent(targetDeviceEntity))
                {
                    var mouse = mouses[targetDeviceEntity];
                    if (mouse.leftButton.wasPressedThisFrame == 1)
                    {
                        buttonActionType = ButtonActionType.Confirm;
                        PostUpdateCommands.AddComponent(entityInQueryIndex, selectedUIEntity,
                            new MouseDragEvent(targetDeviceEntity, mouse.position, cameraLink.camera));
                        // UnityEngine.Debug.LogError("Added MouseDragEvent: " + selectedUIEntity.Index);
                    }
                    //! A normal right click without control.
                    else if (mouse.rightButton.wasPressedThisFrame == 1 && controller.keyboard.leftControlKey.isPressed == 0)
                    {
                        buttonActionType = ButtonActionType.Action3;
                    }
                    // or control right click (for equiping item in 2nd slot)
                    else if (mouse.rightButton.wasPressedThisFrame == 1 && controller.keyboard.leftControlKey.isPressed == 1)
                    {
                        buttonActionType = ButtonActionType.Action2;
                    }
                }
                else if (touchpads.HasComponent(targetDeviceEntity))
                {
                    var touchpad = touchpads[targetDeviceEntity];
                    if (touchpad.touchesCount == 0)
                    {
                        return;
                    }
                    for (byte i = 0; i < touchpad.fingers.Length; i++)
                    {
                        var finger = touchpad.fingers[i];
                        /*if (!finger.active)
                        {
                            continue;
                        }*/
                        var mouseOverEntity = navigatorSelected.GetMouseOver(i);
                        if (finger.state == FingerState.End)
                        {
                            PostUpdateCommands.AddComponent(entityInQueryIndex, mouseOverEntity,
                                new UIClickEvent(e, targetDeviceEntity, DeviceType.Touch, ButtonActionType.Confirm));
                        }
                        else if (finger.state == FingerState.Begin)
                        {
                            // UnityEngine.Debug.LogError("FingerState.Begin: " + i + ":: " + mouseOverEntity.Index);
                            PostUpdateCommands.AddComponent(entityInQueryIndex, mouseOverEntity,
                                new MouseDragEvent(targetDeviceEntity, finger.position, cameraLink.camera, finger.id));
                        }
                    }
                    return;
                }
                //! Only triggers click event with player input
                if (buttonActionType != ButtonActionType.None)
                {
                    //! \todo Make Sounds catch UIClickEvent and add PlaySound there instead.
                    if (inputFields.HasComponent(selectedUIEntity))
                    {
                        if (deviceTypeData.type == DeviceType.Keyboard)
                        {
                            //UnityEngine.Debug.LogError("Activating Input Mapping at " + elapsedTime + " time passed: " + timePassed + " lastLastTime: " + lastLastTime);
                            PostUpdateCommands.AddComponent<InputActivated>(entityInQueryIndex, selectedUIEntity);
                            // controller.SetMapping(ControllerMapping.Input);
                            PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetControllerMapping(ControllerMapping.Input));
                        }
                        //! \todo If gamepad or touch, open virtual keyboard!
                        // UnityEngine.Debug.LogError("Editing " + selectedUIEntity.Index + " text.");
                        return;
                    }
                    PostUpdateCommands.AddComponent(entityInQueryIndex, selectedUIEntity,
                        new UIClickEvent(e, targetDeviceEntity, deviceTypeData.type, buttonActionType));
                }
                if (uiEntities.Length > 0) { var e2 = uiEntities[0]; }
                if (deviceEntities.Length > 0) { var e2 = deviceEntities[0]; }
			})  .WithReadOnly(raycastUIOrigins)
                .WithReadOnly(inputFields)
                .WithReadOnly(mouses)
                .WithReadOnly(touchpads)
                .WithReadOnly(gamepads)
                .WithReadOnly(uiEntities)
                .WithDisposeOnCompletion(uiEntities)
                .WithReadOnly(deviceEntities)
                .WithDisposeOnCompletion(deviceEntities)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                //! \todo Make toggles use UIClickEvents
                /*if (toggleUIs.HasComponent(selectedUIEntity))
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, selectedUIEntity, new InitialToggleClickEvent(e, buttonActionType));
                }*/

                //! Only Menu, Dialogue, RealmUI, ChestUI mappings have button events.
                /*if (controller.mapping != ControllerMapping.Menu && controller.mapping != ControllerMapping.Dialogue
                    && controller.mapping != ControllerMapping.RealmUI && controller.mapping != ControllerMapping.ChestUI)
                {
                    return;
                }*/
                /*if (deviceTypeData.type == DeviceType.Keyboard)
                {
                    if (controller.keyboard.escapeKey.wasPressedThisFrame == 1)
                    {
                        return;
                    }
                }
                else if (deviceTypeData.type == DeviceType.Gamepad)
                {
                    if (controller.gamepad.buttonB.wasPressedThisFrame == 1)
                    {
                        return;
                    }
                }*/
                            //byte fingerBeginCount = 0;
                            //byte fingerEndCount = 0;
                                    /*if (fingerEndCount == 0)
                                    {
                                        buttonActionType = ButtonActionType.Confirm;
                                    }
                                    else // if (fingerEndCount == 1)
                                    {
                                    }*/
                                    //fingerEndCount++;
                                    //fingerBeginCount++;
                                    /*if (fingerBeginCount == 0)
                                    {
                                        if (!HasComponent<MouseDragEvent>(navigator.mouseOverEntity))
                                        {
                                            PostUpdateCommands.AddComponent(entityInQueryIndex, navigator.mouseOverEntity,
                                                new MouseDragEvent(targetDeviceEntity, finger.position, cameraLink.camera, finger.id));
                                        }
                                    }
                                    else // if (fingerBeginCount == 1)
                                    {*/
                                    //}