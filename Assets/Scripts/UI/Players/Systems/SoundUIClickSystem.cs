using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Audio;
using Zoxel.Players;
using Zoxel.Input;

namespace Zoxel.UI.Players
{
    //! Used for player to navigate ui.
    /**
    *   - Event System
    */
    [BurstCompile, UpdateInGroup(typeof(PlayerSystemGroup))]
    public partial class SoundUIClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UIClickEvent>(),
                ComponentType.ReadOnly<Sound>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AddComponent<PlaySound>(processQuery);
        }
    }
}