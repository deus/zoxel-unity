﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Stats;
using Zoxel.Races;
using Zoxel.Textures;

namespace Zoxel.UI.NameLabels
{
    //! Spawns name label above npcs heads.
    /**
    *   - UI Spawn System -
    *  Uses Race Name and ' - Lvl ' in between
    *  \todo Use UICore prefab for name label.
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class NameLabelSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery racesQuery;
        private EntityQuery userStatsQuery;
        private Entity nameLabelPrefab;
        private NativeArray<Text> texts;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            racesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Race>(),
                ComponentType.ReadOnly<ZoxName>(),
                ComponentType.Exclude<DestroyEntity>());
            userStatsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserStat>(),
                ComponentType.ReadOnly<LevelStat>(),
                ComponentType.Exclude<DestroyEntity>());
            texts = new NativeArray<Text>(1, Allocator.Persistent);
            texts[0] = new Text(" Lvl ");
            var nameLabelArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(FadeIn),
                typeof(RenderTextDirty),
                typeof(GenerateTextureFrame),
                typeof(TextureFrame),
                typeof(NameLabel),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(FaceCamera),
                typeof(DistanceFaderUI),
                typeof(TrailerUI),
                typeof(NewCharacterUI),
                typeof(CharacterUI),
                typeof(CharacterLink),
                typeof(RenderText),
                typeof(RenderTextData),
                typeof(RenderTextResizePanel),
                typeof(ZoxMesh),
                typeof(Texture),
                typeof(MaterialBaseColor),
                typeof(MaterialFrameColor),
                typeof(Fader),
                typeof(FaderInvisible),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale)
            );
            nameLabelPrefab = EntityManager.CreateEntity(nameLabelArchetype);
            EntityManager.SetComponentData(nameLabelPrefab, new NonUniformScale { Value = new float3(1f, 1f, 1f) });
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance.uiSettings.disableNameLabels)
            {
                return;
            }
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            // var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var uiDatam = UIManager.instance.uiDatam;
            var nameLabelStyle = uiDatam.nameLabel.GetStyle(1f);
            var color4 = nameLabelStyle.color.ToFloat4();
            var alpha = color4.w;
            color4.w = 0f;
            var frameGenerationData = nameLabelStyle.frameGenerationData;
            frameGenerationData.color.alpha = 0;
            var textColor = nameLabelStyle.textColor;
            textColor.alpha = 0;
            var textOutlineColor = nameLabelStyle.textOutlineColor;
            textOutlineColor.alpha = 0;
            var texts = this.texts;
            var nameLabelPrefab = this.nameLabelPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var raceEntities = racesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var raceNames = GetComponentLookup<ZoxName>(true);
            raceEntities.Dispose();
            var userStatEntities = racesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var levelStats = GetComponentLookup<LevelStat>(true);
            userStatEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, EntityBusy, LoadRace>()
                .WithNone<GenerateStats, LoadStats, OnUserStatsSpawned>()
                .WithAll<SpawnNameLabel>()
                .ForEach((Entity e, int entityInQueryIndex, in Body body, in Translation translation, in ZoxName zoxName, in UserStatLinks userStatLinks, in RaceLink raceLink) =>
            {
                var positionOffset = body.size.y;
                if (positionOffset == 0 || raceLink.race.Index == 0)
                {
                    return; // wait until userStatLinks are generated
                }
                PostUpdateCommands.RemoveComponent<SpawnNameLabel>(entityInQueryIndex, e);  // Complete event
                var level = 0; // userStatLinks.levels[0].value;
                for (int i = 0; i < userStatLinks.stats.Length; i++)
                {
                    var userStatEntity = userStatLinks.stats[i];
                    if (HasComponent<SoulStat>(userStatEntity))
                    {
                        if (levelStats.HasComponent(userStatEntity))
                        {
                            level = levelStats[userStatEntity].value;
                        }
                        break;
                    }
                }
                positionOffset += (nameLabelStyle.fontSize + nameLabelStyle.textPadding.y * 2f) * 1.4f;
                var spawnPosition = translation.Value + new float3(0, positionOffset, 0);
                var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, nameLabelPrefab);
                // Setters
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new TrailerUI(positionOffset));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new CharacterLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Fader(0, alpha, nameLabelStyle.fadeTime));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new MaterialBaseColor { Value = color4 });
                UICoreSystem.SetTextureFrame(PostUpdateCommands, entityInQueryIndex, e2, in frameGenerationData);
                var raceName = new Text();
                if (raceNames.HasComponent(raceLink.race))
                {
                    raceName = raceNames[raceLink.race].name.Clone();
                }
                var labelText = raceName; //zoxName.name.Clone();
                // use names for humans/orcs etc - 'Named NPCs'
                // labelText.AddChar(' ');
                var menuA = texts[0];
                labelText.AddText(in menuA);
                labelText.AddInteger(level);
                UICoreSystem.SetRenderText(PostUpdateCommands, entityInQueryIndex, e2, textColor, textOutlineColor, nameLabelStyle.textGenerationData,
                    labelText, nameLabelStyle.fontSize, nameLabelStyle.textPadding);
                // OnUISpawned Event - links to UIHolder
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));
            })  .WithReadOnly(raceNames)
                .WithReadOnly(texts)
                .WithReadOnly(levelStats)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}