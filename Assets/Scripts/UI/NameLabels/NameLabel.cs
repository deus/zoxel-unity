using Unity.Entities;

namespace Zoxel.UI.NameLabels
{
    //! Tag for name label above character heads.
    public struct NameLabel : IComponentData { }
}