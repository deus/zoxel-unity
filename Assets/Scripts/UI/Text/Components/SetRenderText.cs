using Unity.Entities;

namespace Zoxel.UI
{
    public struct SetRenderText : IComponentData
    {
        public Text text;
        
        public SetRenderText(Text text)
        {
            this.text = text;
        }
    }
}