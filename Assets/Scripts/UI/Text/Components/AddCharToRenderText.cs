using Unity.Entities;

namespace Zoxel.UI
{
    public struct AddCharToRenderText : IComponentData
    {
        public byte addCharacter;
    }
}