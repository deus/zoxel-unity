﻿using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! UI component for text rendering.
    public struct RenderText : IComponentData
    {
        //! Contains UI text data for spawning Zigel's.
        public Text text;
        //! Links to Zigell entities.
        public BlitableArray<Entity> letters;

        public int Length
        {
            get
            { 
                return letters.Length;
            }
        }

        public override string ToString()
        {
            return text.ToString();
        }

        public RenderText(Text text)
        {
            this.text = text;
            this.letters = new BlitableArray<Entity>();
        }

        public void DisposeFinal()
        {
            letters.DisposeFinal();
            text.DisposeFinal();
        }

        public void Dispose()
        {
            letters.Dispose();
            text.Dispose();
        }
        
        public float2 GetSize(in RenderTextData renderTextData)
        {
            var panelSize = new float2(
                2 * renderTextData.margins.x + renderTextData.fontSize * (text.GetSizeX() + 0), 
                2 * renderTextData.margins.y + renderTextData.fontSize * (text.GetTotalLines() ) );
            if (text.textData.Length == 0)
            {
                panelSize = float2.zero;
            }
            return panelSize;
        }

        public bool SetText(string newText2)
        {
            var comparisonText = new Text(newText2);
            if (comparisonText != text)
            {
                text.Dispose();
                text = comparisonText;
                return true;
            }
            else
            {
                comparisonText.Dispose();
            }
            return false;
        }

        public bool ClearText()
        {
            if (Length == 0)
            {
                return false;
            }
            text.Dispose();
            return true;
        }

        public bool SetText(Text newText)
        {
            if (text != newText)
            {
                text.Dispose();
                text = newText;
                return true;
            }
            return false;
        }

        public bool AddText(Text newText)
        {
            if (newText.textData.Length != 0)
            {
                var newTextData = new BlitableArray<byte>(text.textData.Length + newText.textData.Length, Allocator.Persistent);
                for (int i = 0; i < text.Length; i++)
                {
                    newTextData[i] = text.textData[i];
                }
                for (int i = 0; i < newText.Length; i++)
                {
                    newTextData[i + text.Length] = newText[i];
                }
                if (text.Length > 0)
                {
                    text.textData.Dispose();
                }
                text.textData = newTextData;
                return true;
            }
            return false;
        }

        public bool InsertText(Text newText)
        {
            if (newText.textData.Length != 0)
            {
                var newTextData = new BlitableArray<byte>(text.textData.Length + newText.textData.Length, Allocator.Persistent);
                for (int i = 0; i < newText.Length; i++)
                {
                    newTextData[i] = newText[i];
                }
                for (int i = 0; i < text.Length; i++)
                {
                    newTextData[i + newText.Length] = text.textData[i];
                }
                if (text.Length > 0)
                {
                    text.textData.Dispose();
                }
                text.textData = newTextData;
                return true;
            }
            return false;
        }

        public void Backspace()
        {
            if (text.Length == 0)
            {
                return;
            }
            var newText = new BlitableArray<byte>(text.Length - 1, Allocator.Persistent);
            for (int i = 0; i < newText.Length; i++)
            {
                newText[i] = text[i];
            }
            text.Dispose();
            text.textData = newText;
        }

        public void AddChar(byte newChar)
        {
            text.AddChar(newChar);
        }

        public void SetTextArray(BlitableArray<byte> textArray)
        {
            text.SetTextArray(textArray);
        }
    }
}
        // Remove the rest into data components.
        /*public Color color;
        public Color outlineColor;
        public TextGenerationData textGenerationData;
        //! Space around the text.
        public float2 margins;
        //! Space (Y) between the text lines.
        // public float padding;
        public float fontSize;
        public float offsetX;
        public byte horizontalAlignment;
        public byte verticalAlignment;
        // Perhaps... Remove maxWidth and maxHeight and use UIElementConstraints
        public float maxWidth;
        public float maxHeight;*/