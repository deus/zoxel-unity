using Unity.Entities;

namespace Zoxel.UI
{
    //! Used for relinking a Zigel entity to a RenderText.
    public struct NewZigel : IComponentData { }
}