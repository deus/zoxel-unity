using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    public struct SetZigelSize : IComponentData
    {
        public float2 size;

        public SetZigelSize(float2 size)
        {
            this.size = size;
        }
    }
}