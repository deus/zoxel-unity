using Unity.Entities;

namespace Zoxel.UI
{
    //! Resizes the UIElement to fit the RenderText.
    public struct RenderTextResizePanel : IComponentData { }
}