using Unity.Entities;

namespace Zoxel.UI
{
    public struct AddRenderText : IComponentData
    {
        public Text text;
        
        public AddRenderText(Text text)
        {
            this.text = text;
        }
    }
}