using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    public struct AnimatedText : IComponentData
    {
        public uint seed;
        public Text text;
        public byte endIndex;
        public byte maxIndex;
        public float timeLastLetterSpawned;
        public float timePerLetter;
        public float timePerLetterMin;
        public float timePerLetterMax;

        public AnimatedText(float2 timePerLetter, float time, int seed)
        {
            this.seed = (uint) seed;
            // Time
            this.timePerLetterMin = timePerLetter.x;
            this.timePerLetterMax = timePerLetter.y;
            var random = new Random();
            random.InitState(this.seed);
            // randomize cooldown
            this.timeLastLetterSpawned = time;
            this.timePerLetter = timePerLetterMin + random.NextFloat(timePerLetterMax - timePerLetterMin);

            // Text
            this.text = new Text();
            this.endIndex = 0;
            this.maxIndex = 0;
        }

        public void SetText(string text)
        {
            this.text.SetText(text);
            this.endIndex = 0;
            this.maxIndex = (byte) text.Length;
        }

        public void SetText(Text text)
        {
            this.text = text;
            this.endIndex = 0;
            this.maxIndex = (byte) text.Length;
        }

        public void OnLetterSpawned(ref Random random, float time, ref RenderText renderText)
        {
            if (this.endIndex >= this.maxIndex + 1 || (int)endIndex > this.text.textData.Length)
            {
                return;
            }
            this.timeLastLetterSpawned = time;
            this.timePerLetter = this.timePerLetterMin + random.NextFloat(this.timePerLetterMax - this.timePerLetterMin);
            renderText.SetTextArray(this.text.SubString(this.endIndex));
        }

        // return true if finished
        public bool IncrementText()
        {
            this.endIndex++;
            return this.endIndex == this.maxIndex + 1 + 1;    // have to wait for text to update after finished
        }

        public bool IsIncrementText(float elapsedTime)
        {
            if (this.maxIndex == 0)
            {
                return false;
            }
            if (this.endIndex == this.maxIndex + 1 + 1) // finished
            {
                return false;
            }
            return elapsedTime - this.timeLastLetterSpawned >= this.timePerLetter;
        }
    }
}