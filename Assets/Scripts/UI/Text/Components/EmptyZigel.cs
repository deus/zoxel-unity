using Unity.Entities;

namespace Zoxel.UI
{
    //! A Zigel tag for a space character.
    public struct EmptyZigel : IComponentData { }
}