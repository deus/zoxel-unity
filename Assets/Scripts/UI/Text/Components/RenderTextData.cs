using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! \todo Seperate render text data from the component.
    public struct RenderTextData : IComponentData
    {
        //! The Size2D off each zigel.
        public float fontSize;
        public float2 margins;
        //! The Color off each zigel.
        public Color color;
        //! The OutlineColor off each zigel.
        public Color outlineColor;
        //! Data used in generated the texture per zigel.
        public TextGenerationData textGenerationData;
        //! Space around the text.
        public float offsetX;
        public byte horizontalAlignment;
        public byte verticalAlignment;
        // Perhaps... Remove maxWidth and maxHeight and use UIElementConstraints
        public float maxWidth;
        public float maxHeight;

        public RenderTextData(Color color, Color outlineColor, float fontSize)
        {
            this.color = color;
            this.outlineColor = outlineColor;
            this.fontSize = fontSize;
            this.textGenerationData = new TextGenerationData();
            this.margins = float2.zero;
            this.offsetX = 0;
            this.horizontalAlignment = 0;
            this.verticalAlignment = 0;
            this.maxWidth = 0;
            this.maxHeight = 0;
        }

        public float2 GetPanelSize(int length)
        {
            return new float2(fontSize * length, fontSize);
        }

        public UnityEngine.Color GetColor()
        {
            return color.GetColor();
        }

        public void SetColor(UnityEngine.Color secondaryColor)
        {
            color.SetColor(secondaryColor);
        }

        public static float2 GetPanelSize(float fontSize, int lettersLength)
        {
            return new float2(fontSize * lettersLength, fontSize);
        }
    }
}
        //! Space (Y) between the text lines. Line Spacing.
        // public float padding;