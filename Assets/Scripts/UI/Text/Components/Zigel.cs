using Unity.Entities;

namespace Zoxel.UI
{
    //! Renders a Letter UI entity. A Child of RenderText.
    public struct Zigel : IComponentData
    {
        public int index;               // index in relation to all buttons in childrens group
        public byte fontIndex;
        
        public Zigel(int index, byte fontIndex)
        {
            this.index = index;
            this.fontIndex = fontIndex;
        }
    }
}