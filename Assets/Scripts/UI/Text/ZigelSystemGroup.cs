﻿using Unity.Entities;

namespace Zoxel.UI
{
    //! For Zigels, zigels.
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class ZigelSystemGroup : ComponentSystemGroup { }
}