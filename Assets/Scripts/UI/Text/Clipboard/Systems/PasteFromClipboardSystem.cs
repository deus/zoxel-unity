using Unity.Entities;

namespace Zoxel.UI
{
    //! Pastes RenderText text from clipboard.
    [ UpdateInGroup(typeof(UISystemGroup))]
    public partial class PasteFromClipboardSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands.RemoveComponent<PasteFromClipboard>(processQuery);
            PostUpdateCommands.AddComponent<RenderTextDirty>(processQuery);
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<PasteFromClipboard>()
                .ForEach((ref RenderText renderText) =>
            {
                renderText.SetText(UnityEngine.GUIUtility.systemCopyBuffer);
                UnityEngine.Debug.Log("Pasted [" + UnityEngine.GUIUtility.systemCopyBuffer + "] to clipboard.");
            }).WithoutBurst().Run();
        }
    }
}