using Unity.Entities;

namespace Zoxel.UI
{
    //! Copies RenderText text to clipboard.
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class CopyToClipboardSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands.RemoveComponent<CopyToClipboard>(processQuery);
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<CopyToClipboard>()
                .ForEach((in RenderText renderText) =>
            {
                UnityEngine.GUIUtility.systemCopyBuffer = renderText.text.ToString();
                UnityEngine.Debug.Log("Copied [" + UnityEngine.GUIUtility.systemCopyBuffer + "] to clipboard.");
            }).WithoutBurst().Run();
        }
    }
}