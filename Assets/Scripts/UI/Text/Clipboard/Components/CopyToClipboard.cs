using Unity.Entities;

namespace Zoxel.UI
{
    //! Copies RenderText text to clipboard.
    public struct CopyToClipboard : IComponentData { }
}