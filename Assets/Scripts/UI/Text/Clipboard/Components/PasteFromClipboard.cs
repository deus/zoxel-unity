using Unity.Entities;

namespace Zoxel.UI
{
    //! Pastes RenderText text from clipboard.
    public struct PasteFromClipboard : IComponentData { }
}