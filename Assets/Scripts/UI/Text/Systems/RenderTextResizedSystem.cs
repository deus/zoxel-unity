using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Textures;

namespace Zoxel.UI
{
    //! Resizes panel based on RenderText. It also regenerates the frame texture.
    [BurstCompile, UpdateInGroup(typeof(ZigelSystemGroup))]
    public partial class RenderTextResizedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity, OnRenderTextSpawned>()
                .WithAll<OnRenderTextResized>()
                .ForEach((Entity e, int entityInQueryIndex, ref UIMeshData uiMeshData, ref Size2D size2D, in RenderText renderText, in RenderTextData renderTextData) =>
            {
                PostUpdateCommands.RemoveComponent<OnRenderTextResized>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<GenerateMeshUI>(entityInQueryIndex, e);
                var panelSize = renderText.GetSize(in renderTextData);
                if (renderText.Length == 0 && HasComponent<InputField>(e))
                {
                    panelSize =  new float2(
                        2 * renderTextData.margins.x + renderTextData.fontSize * (1), 
                        2 * renderTextData.margins.y + renderTextData.fontSize * (1) );
                }
                size2D.size = panelSize;
                var totalLines = renderText.text.GetTotalLines();
                uiMeshData.offset = new float3();
                if (renderTextData.verticalAlignment == TextVerticalAlignment.Middle)
                {
                    uiMeshData.offset.y -= ((totalLines - 1) * renderTextData.fontSize) / 2f;
                }
                uiMeshData.verticalAlignment = renderTextData.verticalAlignment;
                uiMeshData.horizontalAlignment = renderTextData.horizontalAlignment;
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity, OnRenderTextSpawned>()
                .WithAll<OnRenderTextResized, TextureFrame>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.AddComponent<GenerateTextureFrame>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

// UICoreSystem.AddGenerateFrame(PostUpdateCommands, entityInQueryIndex, e, new Color(255, 255, 255, 255), new Color(0, 0, 0, 255), 32, 1);
//if (totalLines == 1 || renderText.verticalAlignment != TextVerticalAlignment.Middle)
//{
// if (renderText.verticalAlignment != TextVerticalAlignment.Middle)
/*}
else
{
    var panelHeight = 2 * renderText.margins.y + renderText.fontSize * totalLines; //  * (totalLines - 1);
    uiMeshData.offset = - new float3(0, panelHeight, 0) / 2f;
}*/