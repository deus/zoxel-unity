using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Sets new RenderText data from a Text.
    [BurstCompile, UpdateInGroup(typeof(ZigelSystemGroup))]
    public partial class RenderTextSetSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity, RenderTextDirty, OnRenderTextSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref RenderText renderText, in SetRenderText setRenderText) =>
            {
                PostUpdateCommands.RemoveComponent<SetRenderText>(entityInQueryIndex, e);
                if (renderText.SetText(setRenderText.text))
                {
                    // UnityEngine.Debug.LogError("Setting actionbar label text: " + setRenderText.text);
                    PostUpdateCommands.AddComponent<RenderTextDirty>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}