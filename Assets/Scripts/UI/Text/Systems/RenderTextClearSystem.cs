using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Clears RenderText!
    [UpdateBefore(typeof(RenderTextSpawnSystem))]
    [BurstCompile, UpdateInGroup(typeof(ZigelSystemGroup))]
    public partial class RenderTextClearSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime2 = UISystemGroup.elapsedTime;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var elapsedTime = World.Time.ElapsedTime;
            PostUpdateCommands2.RemoveComponent<ClearRenderText>(processQuery);
            PostUpdateCommands2.AddComponent<MeshUIDirty>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            Dependency = Entities
                .WithNone<SetTooltipText, SetRenderText>()
                .WithNone<RenderTextDirty, OnRenderTextSpawned>()
                .WithAll<ClearRenderText>()
                .ForEach((ref Size2D size2D) =>
            {
                size2D.size.x = 0;
                size2D.size.y = 0;
            }).ScheduleParallel(Dependency);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<SetTooltipText, SetRenderText>()
                .WithNone<RenderTextDirty, OnRenderTextSpawned>()
                .WithAll<ClearRenderText>()
                .ForEach((Entity e, int entityInQueryIndex, ref RenderText renderText) => // Entity e, 
            {
                // UnityEngine.Debug.LogError("[" + elapsedTime2 + "] ClearRenderText From [" + e.Index + "]");
                for (int i = 0; i < renderText.letters.Length; i++)
                {
                    var zigelEntity = renderText.letters[i];
                    if (HasComponent<Zigel>(zigelEntity))
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, zigelEntity);
                    }
                }
                renderText.Dispose();
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}