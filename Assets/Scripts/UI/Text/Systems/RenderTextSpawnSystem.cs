﻿using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Textures;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    //! Spawns new Zigel's for a RenderText component.
    [BurstCompile, UpdateInGroup(typeof(ZigelSystemGroup))]
    public partial class RenderTextSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var seed = IDUtil.GenerateUniqueID();
            var elapsedTime = World.Time.ElapsedTime;
            var letterPrefab = UICoreSystem.letterPrefab;
            var emptyLetterPrefab = UICoreSystem.emptyLetterPrefab;
            var fadedLetterPrefab = UICoreSystem.fadedLetterPrefab;
            var fadedEmptyLetterPrefab = UICoreSystem.fadedEmptyLetterPrefab;
            var uiDatam = UIManager.instance.uiDatam;
            var defaultOutlineColor = new Color(uiDatam.errorOutlineColor);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // Fit to maxWidth
            Dependency = Entities
                .WithNone<DestroyEntity, InitializeEntity>()
                .WithNone<OnRenderTextSpawned, MeshUIDirty>()
                .WithAll<RenderTextDirty>()
                .ForEach((ref RenderText renderText, ref RenderTextData renderTextData) =>
            {
                int desiredLength = renderText.text.Length;
                if (renderTextData.maxWidth != 0 && desiredLength != 0)
                {
                    var calculatedWidth = renderTextData.maxWidth / ((float)desiredLength);
                    if (renderTextData.maxHeight != 0)
                    {
                        renderTextData.fontSize = math.min(renderTextData.maxHeight, calculatedWidth);
                    }
                    else
                    {
                        renderTextData.fontSize = calculatedWidth;
                    }
                }
			}).ScheduleParallel(Dependency);
            // Fix to Panel Size2D
            Dependency = Entities
                .WithNone<DestroyEntity, InitializeEntity>()
                .WithNone<OnRenderTextSpawned, MeshUIDirty>()
                .WithAll<RenderTextDirty, RenderTextFit>()
                .ForEach((ref RenderText renderText, ref RenderTextData renderTextData, in Size2D size2D) =>
            {
                int desiredLength = renderText.text.Length;
                if (desiredLength != 0)
                {
                    // resize fontsize to fit
                    // check lines and resize more to fit Y constraints
                    renderTextData.fontSize = (size2D.size.x - renderTextData.margins.x * 2) / ((float) desiredLength);
                    // UnityEngine.Debug.LogError("Size: " + renderTextData.fontSize);
                }
			}).ScheduleParallel(Dependency);
            Dependency = Entities
                .WithNone<DestroyEntity, InitializeEntity>()
                .WithNone<OnRenderTextSpawned, MeshUIDirty>()
                .WithAll<RenderTextDirty, RenderTextResizePanel, UIElement>()
                .ForEach((Entity e, int entityInQueryIndex, in RenderText renderText) =>
            {
                for (int i = 0; i < renderText.letters.Length; i++)
                {
                    var letter = renderText.letters[i];
                    if (HasComponent<InitializeZigel>(letter) || HasComponent<GenerateFontTexture>(letter))
                    {
                        return;
                    }
                }
                PostUpdateCommands.AddComponent<OnRenderTextResized>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<DestroyEntity, InitializeEntity>()
                .WithNone<OnRenderTextSpawned, MeshUIDirty>()
                .WithAll<RenderTextDirty>()
                .ForEach((Entity e, int entityInQueryIndex, ref RenderText renderText, in RenderTextData renderTextData, in Size2D size2D) =>
            {
                for (int i = 0; i < renderText.letters.Length; i++)
                {
                    var letter = renderText.letters[i];
                    if (HasComponent<InitializeZigel>(letter) || HasComponent<GenerateFontTexture>(letter))
                    {
                        return;
                    }
                }
                PostUpdateCommands.RemoveComponent<RenderTextDirty>(entityInQueryIndex, e);
                var isResizePanel = HasComponent<RenderTextResizePanel>(e);
                var isFader = HasComponent<Fader>(e);
                int desiredLength = renderText.text.Length;
                //! Destroy old letters that are above the new size.
                for (int i = 0; i < renderText.letters.Length; i++)
                {
                    var letter = renderText.letters[i];
                    if (i >= desiredLength)
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, letter);
                    }
                }
                var previousEntitiesLength = renderText.letters.Length;
                var letters = new BlitableArray<Entity>(desiredLength, Allocator.Persistent);
                var spawnTextCount = math.max(desiredLength, renderText.Length);    // If old letters higher, takes that as max, otherwise takes new letters as max
                // spawn new
                var fontSize = renderTextData.fontSize;
                if (!isResizePanel)
                {
                    if (size2D.size.y == 0)
                    {
                        fontSize = renderTextData.fontSize - renderTextData.margins.y * 2f; // limits size to panel size - margins
                    }
                    else
                    {
                        fontSize = math.min(size2D.size.y - renderTextData.margins.y * 2f, renderTextData.fontSize); // limits size to panel size - margins
                    }
                }
                var zigelSize = new float2(fontSize, fontSize);
                var totalLines = renderText.text.GetTotalLines();
                // UnityEngine.Debug.LogError("Render Text Length set to: " + letters.Length);
                // todo: randomize this based on time and entity index
                for (int i = 0; i < letters.Length; i++)
                {
                    var color = renderTextData.color;
                    var outlineColor = renderTextData.outlineColor;
                    var fontIndex = renderText.text.textData[i];
                    var position = CalculateFontPosition(in renderText, in renderTextData, i, totalLines, fontSize);
                    Entity letter;
                    var isEmpty = fontIndex == 255 || fontIndex == 254;
                    if (isEmpty)
                    {
                        color.alpha = 0;
                        outlineColor.alpha = 0;
                    }
                    if (i < previousEntitiesLength)
                    {
                        // reuse old letters
                        letter = renderText.letters[i];
                        letters[i] = letter;
                        PostUpdateCommands.AddComponent<InitializeZigel>(entityInQueryIndex, letter);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, letter, new SetZigelSize(zigelSize));
                        if (!isEmpty)
                        {
                            if (HasComponent<EmptyZigel>(letter))
                            {
                                PostUpdateCommands.RemoveComponent<EmptyZigel>(entityInQueryIndex, letter);
                            }
                        }
                        else
                        {
                            if (!HasComponent<EmptyZigel>(letter))
                            {
                                PostUpdateCommands.AddComponent<EmptyZigel>(entityInQueryIndex, letter);
                            }
                        }
                    }
                    else
                    {
                        var usedLetterPrefab = new Entity();
                        if (!isFader)
                        {
                            if (!isEmpty)
                            {
                                usedLetterPrefab = letterPrefab;
                            }
                            else
                            {
                                usedLetterPrefab = emptyLetterPrefab; 
                                // UnityEngine.Debug.LogError("Added Empty Letter: " + i);
                            }
                        }
                        else
                        {
                            if (!isEmpty)
                            {
                                usedLetterPrefab = fadedLetterPrefab;
                            }
                            else
                            {
                                usedLetterPrefab = fadedEmptyLetterPrefab; 
                            }
                        }
                        letters[i] = new Entity();
                        letter = PostUpdateCommands.Instantiate(entityInQueryIndex, usedLetterPrefab);
                        var letterSeed = (int) (seed + 369 + elapsedTime + entityInQueryIndex * 369 + i * 3939);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, letter, new Seed(letterSeed));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, letter, new ParentLink(e));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, letter, new Size2D(zigelSize));
                    }
                    PostUpdateCommands.SetComponent(entityInQueryIndex, letter, new Zigel(i, fontIndex));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, letter, new LocalPosition(position));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, letter, new MaterialBaseColor(color));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, letter, new MaterialFrameColor(outlineColor));
                }
                // update letters
                renderText.letters.Dispose();
                renderText.letters = letters;
                if (desiredLength > 0 && desiredLength > previousEntitiesLength) //  != desiredLength)
                {
                    for (int i = 0; i < previousEntitiesLength; i++)
                    {
                        var letter = renderText.letters[i];
                        PostUpdateCommands.AddComponent<NewZigel>(entityInQueryIndex, letter);
                    }
                    PostUpdateCommands.AddComponent<OnRenderTextSpawned>(entityInQueryIndex, e);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        //! Calculates the position of a character in the RenderText.
        private static float3 CalculateFontPosition(in RenderText renderText, in RenderTextData renderTextData, int index, int totalLines, float fontSize)
        {
            // get any new lines before
            var newLineCount = 0;
            var lineStart = 0;
            for (int i = 0; i <= index; i++)
            {
                if (renderText.text.textData[i] == 254) //'\n')
                {
                    newLineCount++;
                    lineStart = i + 1;
                }
            }
            var offset = new float3();
            // first get total size
            // now we know the offset for centring
            if (renderTextData.horizontalAlignment == (byte) TextHorizontalAlignment.Middle)
            {
                offset.x = ((-renderText.text.GetSizeX(newLineCount) - 1f) / 2f) * fontSize; // should be offset by half total size
            }
            else if (renderTextData.horizontalAlignment == (byte) TextHorizontalAlignment.Left)
            {
                offset.x = (-0.5f) * fontSize + renderTextData.margins.x;
                //offset.x -= ((renderText.text.GetSizeX(newLineCount) + 0.5f) * renderTextData.fontSize + renderText.margins.x) / 2f;
            }
            else if (renderTextData.horizontalAlignment == (byte) TextHorizontalAlignment.Right)
            {
                offset.x = -((renderText.text.GetSizeX(newLineCount) + 0.5f) * fontSize + renderTextData.margins.x);
            }

            if (renderTextData.verticalAlignment == TextVerticalAlignment.Middle)
            {
                // UnityEngine.Debug.LogError("Font lines: " + newLineCount);
                var padding = 0f;    // renderTextData.padding;
                offset.y = - (fontSize + padding) * newLineCount;
            }
            else if (renderTextData.verticalAlignment == TextVerticalAlignment.Top)
            {
                offset.y = -(0.5f * fontSize + fontSize * (newLineCount) + renderTextData.margins.y);
            }
            else if (renderTextData.verticalAlignment == TextVerticalAlignment.Bottom)
            {
                offset.y = (0.5f * fontSize + fontSize * (newLineCount) + renderTextData.margins.y);
            }
            if (renderTextData.offsetX != 0)
            {
                offset.x = renderTextData.offsetX;
            }
            //offset.z = fontDepthBuffer;
            offset.x += fontSize * (index + 1 - lineStart);
            return offset;
        }
    }
}
                //var totalHeight = (fontSize + padding) * totalLines - padding;
                //offset.y -= totalHeight / 2f;
                //offset.y += fontSize / 2f;
                // offset.y += fontSize * (totalLines / 2f);
                /*if (totalLines == 2)
                {
                    offset.y = - fontSize * newLineCount - fontSize * 0.5f;  
                }
                else if (totalLines >= 2)
                {
                    newLineCount++;
                    offset.y = - fontSize * newLineCount;  
                }
                else
                {
                    offset.y = 0;
                }*/