using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! A link event system to link zigels after they spawn.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(ZigelSystemGroup))]
    public partial class RenderTextSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery renderTextQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            renderTextQuery = GetEntityQuery(
                ComponentType.ReadWrite<RenderText>(),
                ComponentType.ReadOnly<OnRenderTextSpawned>());
            RequireForUpdate(processQuery);
            RequireForUpdate(renderTextQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<NewZigel>(processQuery);
            PostUpdateCommands2.RemoveComponent<OnRenderTextSpawned>(renderTextQuery);
            //! Children initialized in spawn system.
            //! For each zigel, using the index, sets into parents letters that is passed in.
            var parentEntities = renderTextQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var renderTexts = GetComponentLookup<RenderText>(false);
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<NewZigel, Zigel>()
                .ForEach((Entity e, in Zigel zigel, in ParentLink parentLink) =>
            {
                if (parentEntities.Length > 0) { var e2 = parentEntities[0]; }
                var renderText = renderTexts[parentLink.parent];
                renderText.letters[zigel.index] = e;
                // UnityEngine.Debug.LogError(parentLink.parent.Index + " is setting zigel Index: " + zigel.index + " :: " + e.Index);
                // renderTexts[parentLink.parent] = renderText;
            })  .WithNativeDisableContainerSafetyRestriction(renderTexts)
                .WithReadOnly(parentEntities)
                .WithDisposeOnCompletion(parentEntities)
                #if RenderText_ChildChecks
                .WithoutBurst().Run();
                #else
                .ScheduleParallel();
                #endif
		}
    }
}