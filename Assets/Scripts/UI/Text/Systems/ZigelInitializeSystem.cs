using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Textures;
using Zoxel.Rendering;
 
namespace Zoxel.UI
{
    //! Initializes Zigel based on parent data.
    /**
    *  \todo When setting zigel, only regenerate texture if font index has changed.
    */
    [BurstCompile, UpdateInGroup(typeof(ZigelSystemGroup))]
    public partial class ZigelInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery parentQuery;
        private EntityQuery fontsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            parentQuery = GetEntityQuery(
                ComponentType.ReadOnly<RenderText>(),
                ComponentType.Exclude<DestroyEntity>());
            fontsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Font>(),
                ComponentType.Exclude<DestroyEntity>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (processQuery.IsEmpty || fontsQuery.IsEmpty)
            {
                return;
            }
            var outlineColor = new Color(0, 0, 0, 255);
            var fillColor =  new Color(255, 255, 255, 255);
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<InitializeZigel>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var parentEntities = parentQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var renderTextDatas = GetComponentLookup<RenderTextData>(true);
            var parentsFadeDelays = GetComponentLookup<FadeDelay>(true);
            var parentsFaders = GetComponentLookup<Fader>(true);
            var parentsFadingIn = GetComponentLookup<FadingIn>(true);
            var parentsFadingOut = GetComponentLookup<FadingIn>(true);
            var renderQueues = GetComponentLookup<RenderQueue>(true);
            // parentEntities.Dispose();
            var fontEntities = fontsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var fonts = GetComponentLookup<Font>(true);
            Dependency = Entities
                .WithNone<GenerateFontTexture>()
                .WithAll<InitializeZigel>()
                .ForEach((ref RenderQueue renderQueue, in ParentLink parentLink) =>
            {
                if (renderQueues.HasComponent(parentLink.parent))
                {
                    var parentLayer = renderQueues[parentLink.parent].layer;
                    if (parentLayer != 0)
                    {
                        renderQueue.layer = parentLayer + 1;
                    }
                }
                if (parentEntities.Length > 0) { var e2 = parentEntities[0]; }
			})  .WithReadOnly(renderQueues)
                .WithNativeDisableContainerSafetyRestriction(renderQueues)
                .WithReadOnly(parentEntities)
                .WithDisposeOnCompletion(parentEntities)
                .ScheduleParallel(Dependency);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<GenerateFontTexture>()
                .WithAll<InitializeZigel>()
                .ForEach((Entity e, int entityInQueryIndex, ref MaterialBaseColor materialBaseColor, ref MaterialFrameColor materialFrameColor, in Zigel zigel, in ParentLink parentLink,
                    in Seed seed) =>
            {
                if (!renderTextDatas.HasComponent(parentLink.parent))
                {
                    return;
                }
                var fontEntity = fontEntities[0];
                var isEmpty = HasComponent<EmptyZigel>(e);
                var isFader = HasComponent<Fader>(e);
                if (!isEmpty)
                {
                    var generateFontTexture = new GenerateFontTexture();
                    generateFontTexture.fontIndex = zigel.fontIndex;
                    generateFontTexture.outlineColor = outlineColor;
                    generateFontTexture.fillColor = fillColor;
                    var parentRenderText = renderTextDatas[parentLink.parent];
                    generateFontTexture.textGenerationData = parentRenderText.textGenerationData;
                    generateFontTexture.SetRandom(seed.seed);
                    var font = fonts[fontEntity];
                    var fontIndex = (int) zigel.fontIndex;
                    if (fontIndex < 0 || fontIndex >= font.zigels.Length)
                    {
                        return;
                    }
                    // UnityEngine.Debug.LogError("fontIndex: " + fontIndex + " out of " + font.zigels.Length);
                    var zigelPoints = font.zigels[fontIndex];
                    generateFontTexture.points = zigelPoints.points.Clone();
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, generateFontTexture);
                }
                else
                {
                    materialBaseColor.Value.w = 0;
                    materialFrameColor.Value.w = 0;
                }
                // Move this to InitializeFader system
                if (isFader)
                {
                    if (isEmpty)
                    {
                        if (!HasComponent<DisableFader>(e))
                        {
                            PostUpdateCommands.AddComponent<DisableFader>(entityInQueryIndex, e);
                        }
                        if (HasComponent<FadingIn>(e))
                        {
                            PostUpdateCommands.RemoveComponent<FadingIn>(entityInQueryIndex, e);
                        }
                        else if (HasComponent<FadingOut>(e))
                        {
                            PostUpdateCommands.RemoveComponent<FadingOut>(entityInQueryIndex, e);
                        }
                        // FadeIn/FadeOut events
                        if (HasComponent<FadeIn>(e))
                        {
                            PostUpdateCommands.RemoveComponent<FadeIn>(entityInQueryIndex, e);
                        }
                        else if (HasComponent<FadeOut>(e))
                        {
                            PostUpdateCommands.RemoveComponent<FadeOut>(entityInQueryIndex, e);
                        }
                        if (HasComponent<FaderVisible>(e))
                        {
                            PostUpdateCommands.RemoveComponent<FaderVisible>(entityInQueryIndex, e);
                        }
                        if (!HasComponent<FaderInvisible>(e))
                        {
                            PostUpdateCommands.AddComponent<FaderInvisible>(entityInQueryIndex, e);
                        }
                    }
                    else
                    {
                        if (HasComponent<DisableFader>(e))
                        {
                            PostUpdateCommands.RemoveComponent<DisableFader>(entityInQueryIndex, e);
                        }
                        // Add FadeIn/FadeOut events
                        if (parentsFadingIn.HasComponent(parentLink.parent))
                        {
                            PostUpdateCommands.AddComponent(entityInQueryIndex, e, parentsFadingIn[parentLink.parent]);
                        }
                        else if (parentsFadingOut.HasComponent(parentLink.parent))
                        {
                            PostUpdateCommands.AddComponent(entityInQueryIndex, e, parentsFadingOut[parentLink.parent]);
                            // UnityEngine.Debug.LogError("Entity Parent Fading Out: " + e.Index + " with font: " + fontIndex);
                        }
                        // FadeIn/FadeOut events
                        if (parentsFadeDelays.HasComponent(parentLink.parent))
                        {
                            PostUpdateCommands.AddComponent(entityInQueryIndex, e, parentsFadeDelays[parentLink.parent]);
                        }
                        if (HasComponent<FadeIn>(parentLink.parent))
                        {
                            PostUpdateCommands.AddComponent<FadeIn>(entityInQueryIndex, e);
                        }
                        else if (HasComponent<FadeOut>(parentLink.parent))
                        {
                            PostUpdateCommands.AddComponent<FadeOut>(entityInQueryIndex, e);
                        }
                        // FadeVisible / FadeInvisible components
                        if (HasComponent<FaderVisible>(parentLink.parent))
                        {
                            if (HasComponent<FaderInvisible>(e))
                            {
                                PostUpdateCommands.RemoveComponent<FaderInvisible>(entityInQueryIndex, e);
                            }
                            PostUpdateCommands.AddComponent<FaderVisible>(entityInQueryIndex, e);
                        }
                        else if (HasComponent<FaderInvisible>(parentLink.parent))
                        {
                            if (HasComponent<FaderVisible>(e))
                            {
                                PostUpdateCommands.RemoveComponent<FaderVisible>(entityInQueryIndex, e);
                            }
                            PostUpdateCommands.AddComponent<FaderInvisible>(entityInQueryIndex, e);
                        }
                        // Set to fader as well
                        if (parentsFaders.HasComponent(parentLink.parent))
                        {
                            var parentFader = parentsFaders[parentLink.parent];
                            materialBaseColor.Value.w = parentFader.alpha;
                            materialFrameColor.Value.w = parentFader.alpha;
                            PostUpdateCommands.SetComponent(entityInQueryIndex, e, parentFader);
                        }
                    }
                }
			})  .WithReadOnly(renderTextDatas)
                .WithReadOnly(parentsFaders)
                .WithReadOnly(parentsFadingIn)
                .WithReadOnly(parentsFadingOut)
                .WithReadOnly(parentsFadeDelays)
                .WithReadOnly(fontEntities)
                .WithReadOnly(fonts)
                .WithDisposeOnCompletion(fontEntities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}