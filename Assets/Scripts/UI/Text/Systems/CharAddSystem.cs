using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    [BurstCompile, UpdateInGroup(typeof(ZigelSystemGroup))]
    public partial class CharAddSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref RenderText renderText, in AddCharToRenderText addCharToRenderText) =>
            {
                renderText.AddChar(addCharToRenderText.addCharacter);
                PostUpdateCommands.RemoveComponent<AddCharToRenderText>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<RenderTextDirty>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<BackspaceRenderText>()
                .ForEach((Entity e, int entityInQueryIndex, ref RenderText renderText) =>
            {
                if (renderText.Length > 0)
                {
                    renderText.Backspace();
                }
                PostUpdateCommands.RemoveComponent<BackspaceRenderText>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<RenderTextDirty>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}