using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Handles destruction of RenderText data by destroying Zigel's.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
	public partial class RenderTextDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyEntity, RenderText>()
                .ForEach((int entityInQueryIndex, in RenderText renderText) =>
            {
                //UnityEngine.Debug.LogError("Destroying Button with Text: " + renderText.letters.Length);
                for (int i = 0; i < renderText.letters.Length; i++)
                {
                    if (HasComponent<Zigel>(renderText.letters[i]))
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, renderText.letters[i]);
                    }
                }
                renderText.DisposeFinal();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}