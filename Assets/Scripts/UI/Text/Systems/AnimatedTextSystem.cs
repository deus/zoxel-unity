﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;
// todo: base audio sounds on character
// TODO:
//      Add choices - generate textboxes for choices after each line of dialogue is given
//      Remove choices - remove navigation and choices from previous ones
//      After end of all dialogue, instead of looping, set players controller to planet back and close the entire dialogue panel

namespace Zoxel.UI
{
    //! Animates RenderText.
    [BurstCompile, UpdateInGroup(typeof(ZigelSystemGroup))]
    public partial class AnimatedTextSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<SoundSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var soundSettings = GetSingleton<SoundSettings>();
            var sampleRate = soundSettings.sampleRate;
            var dialogueSoundVolume = soundSettings.dialogueSoundVolume;
            var soundPrefab = SoundPlaySystem.soundPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var elapsedTime = (float) World.Time.ElapsedTime;
            Dependency = Entities
                .WithNone<InitializeEntity, AnimatedTextFinished, RenderTextDirty>()
                .ForEach((Entity e, int entityInQueryIndex, ref AnimatedText animatedText, ref RenderText renderText, in Translation translation) =>
            {
                if (animatedText.IsIncrementText(elapsedTime))
                {
                    var timeSeed = (uint) (1 + entityInQueryIndex * 256 + 1 * elapsedTime);
                    var timeRandom = new Random();
                    timeRandom.InitState(timeSeed);
                    if (animatedText.IncrementText())
                    {
                        PostUpdateCommands.AddComponent<AnimatedTextFinished>(entityInQueryIndex, e);
                    }
                    else
                    {
                        animatedText.OnLetterSpawned(ref timeRandom, elapsedTime, ref renderText);
                        PostUpdateCommands.AddComponent<RenderTextDirty>(entityInQueryIndex, e);
                    }
                    // play sound every now and then
                    var random = new Random();
                    random.InitState(animatedText.seed);
                    var soundEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, soundPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Seed((int) animatedText.seed));
                    var generateSound = new GenerateSound();
                    generateSound.CreateMusicSound((byte)(24 + random.NextInt(14) + timeRandom.NextInt(6)), 0, sampleRate);
                    // sound.Initialize((int)animatedText.seed, generateSound.timeLength, generateSound.sampleRate, 1, dialogueSoundVolume);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new SoundData(ref random, generateSound.timeLength, generateSound.sampleRate, 1, dialogueSoundVolume));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new DelayEvent(elapsedTime, 0.03));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Translation { Value = translation.Value });
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}