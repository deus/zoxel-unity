using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    //! Initializes mesh and material of a Zigel (text character render).
    /**
    *   - Material Initialize System -
    */
    [BurstCompile, UpdateInGroup(typeof(ZigelSystemGroup))]
    public partial class ZigelZenderInitializeSystem : SystemBase
    {
        private const string textureName = "_BaseMap";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var fontMaterial = UIManager.instance.materials.fontMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, Zigel>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in UIMeshData uiMeshData, in Size2D size2D) =>
            {
                var size = size2D.size;
                var material = new UnityEngine.Material(fontMaterial);
                material.SetTexture(textureName, TextureUtil.CreateBlankTexture());
                zoxMesh.material = material;
                zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size, uiMeshData.horizontalAlignment, uiMeshData.verticalAlignment, uiMeshData.offset);
                zoxMesh.layer = worldUILayer;
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
            Entities.ForEach((Entity e, ZoxMesh zoxMesh, ref Size2D size2D, in SetZigelSize setZigelSize, in UIMeshData uiMeshData) =>
            {
                PostUpdateCommands.RemoveComponent<SetZigelSize>(e);
                if (setZigelSize.size.x != size2D.size.x || setZigelSize.size.y != size2D.size.y)
                {
                    size2D.size = setZigelSize.size;
                    var size = size2D.size;
                    zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size2D.size, uiMeshData.horizontalAlignment, uiMeshData.verticalAlignment, uiMeshData.offset);
                    PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
                }
            }).WithoutBurst().Run();
        }
    }
}