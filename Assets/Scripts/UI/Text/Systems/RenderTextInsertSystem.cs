using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    [BurstCompile, UpdateInGroup(typeof(ZigelSystemGroup))]
    public partial class RenderTextInsertSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            const int maxLogUILines = 12;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity, RenderTextDirty>()
                .ForEach((Entity e, int entityInQueryIndex, ref RenderText renderText, in AddRenderText addRenderText) =>
            {
                if (renderText.InsertText(addRenderText.text))
                //if (renderText.AddText(addRenderText.text))
                {
                    var lineCount = renderText.text.GetLineCount();
                    // remove last line
                    if (lineCount > maxLogUILines)
                    {
                        renderText.text.RemoveLastLine();
                    }
                    // UnityEngine.Debug.LogError("Setting actionbar label text: " + setRenderText.text);
                    PostUpdateCommands.AddComponent<RenderTextDirty>(entityInQueryIndex, e);
                }
                PostUpdateCommands.RemoveComponent<AddRenderText>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}