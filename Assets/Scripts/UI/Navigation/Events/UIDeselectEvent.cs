using Unity.Entities;

namespace Zoxel.UI
{
    //! Event on a button for Deselecting an Entity.
    public struct UIDeselectEvent : IComponentData
    {
        public Entity controller;
        public Entity newSelected;

        public UIDeselectEvent(Entity controller, Entity newSelected)
        {
            this.controller = controller;
            this.newSelected = newSelected;
        }
    }
}