using Unity.Entities;

namespace Zoxel.UI
{
    public struct ClickPanelChild : IComponentData
    {
        public Entity controllerEntity;
        public byte index;
        public byte deviceType;
        public byte buttonType;

        public ClickPanelChild(Entity controllerEntity, byte index, byte deviceType, byte buttonType)
        {
            this.controllerEntity = controllerEntity;
            this.index = index;
            this.deviceType = deviceType;
            this.buttonType = buttonType;
        }
    }
}