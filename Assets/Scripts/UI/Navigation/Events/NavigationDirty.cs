using Unity.Entities;

namespace Zoxel.UI
{
    //! Added to a panel to rebuild navigational ui for it.
    /**
    *   \todo Rewrite navigation systems to store navigation data per ui.
    */
    public struct NavigationDirty : IComponentData
    { 
        public Entity selected;
        public byte selectedIndex;

        public NavigationDirty(byte selectedIndex)
        {
            this.selectedIndex = selectedIndex;
            this.selected = new Entity();
        }

        public NavigationDirty(Entity selected)
        {
            this.selectedIndex = 0;
            this.selected = selected;
        }
    }
}