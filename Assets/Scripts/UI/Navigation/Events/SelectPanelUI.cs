using Unity.Entities;

namespace Zoxel.UI
{
    // should probaly put this into UI system as a way to control multiple UI windows at once
    public struct SelectPanelChild : IComponentData
    {
        public Entity controllerEntity;
        public byte index;

        public SelectPanelChild(Entity controllerEntity, byte index)
        {
            this.controllerEntity = controllerEntity;
            this.index = index;
        }
    }
}