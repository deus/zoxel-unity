using Unity.Entities;

namespace Zoxel.UI
{
    //! Links to a Navigator Entity.
    public struct NavigatorLink : IComponentData
    {
        public Entity navigator;

        public NavigatorLink(Entity navigator)
        {
            this.navigator = navigator;
        }
    }
}