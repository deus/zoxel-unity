﻿using Unity.Entities;
using Unity.Collections;

namespace Zoxel.UI
{
    //! Handles UI navigation for the user.
    public struct Navigator : IComponentData
    {
        //! The last clicked element.
        public Entity selected;
        //! Used for gamepad navigation.
        public BlitableArray<NavigateData> navigateData;
        // this? what is it?
        public byte lastInputKey;       // 255 for nothing
        public byte isFirstKey;
        public double lastInputTime;    // used when scrolling through UI

        public void Dispose()
        {
            navigateData.Dispose();
        }
    }
}