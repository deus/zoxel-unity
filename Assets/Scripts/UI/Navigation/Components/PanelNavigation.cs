﻿using Unity.Entities;

namespace Zoxel.UI
{
    public struct PanelNavigation : IComponentData
    {
        public byte selectedChildIndex;
    }
}