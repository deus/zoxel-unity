using Unity.Entities;
using Unity.Collections;

namespace Zoxel.UI
{
    //! Handles Selected UIs for the Player.
    public struct NavigatorSelected : IComponentData
    {
        //! The element the mouse is over.
        public Entity mouseOverEntity;
        //! The mouse over entity, for the second finger.
        public BlitableArray<Entity> mouseOvers;
        public BlitableArray<byte> mouseOverFingerIndexes;
        //! The last clicked element.
        // public Entity selected;

        public void Dispose()
        {
            mouseOvers.Dispose();
            mouseOverFingerIndexes.Dispose();
        }

        public void AddMouseOver(Entity e, byte fingerIndex)
        {
            // mouseOvers = BlitableArray.Add(entity, mouseOvers);
            var mouseOvers2 = new BlitableArray<Entity>(mouseOvers.Length + 1, Allocator.Persistent, mouseOvers);
            mouseOvers2[mouseOvers.Length] = e;
            mouseOvers.Dispose();
            mouseOvers = mouseOvers2;
            var mouseOverFingerIndexes2 = new BlitableArray<byte>(mouseOverFingerIndexes.Length + 1, Allocator.Persistent, mouseOverFingerIndexes);
            mouseOverFingerIndexes2[mouseOverFingerIndexes.Length] = fingerIndex;
            mouseOverFingerIndexes.Dispose();
            mouseOverFingerIndexes = mouseOverFingerIndexes2;
        }

        public Entity GetMouseOver(byte fingerIndex)
        {
            for (byte i = 0; i < mouseOverFingerIndexes.Length; i++)
            {
                if (mouseOverFingerIndexes[i] == fingerIndex)
                {
                    return mouseOvers[i];
                }
            }
            return new Entity();
        }
    }
}