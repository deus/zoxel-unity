using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! An origin point for raycasting a 2D UIElement.
    /**
    *   Position is cached position within camera bounds.
    */
    public struct RaycastUIOrigin : IComponentData
    {
        public float2 position;

        public RaycastUIOrigin(float2 position)
        {
            this.position = position;
        }
    }
}