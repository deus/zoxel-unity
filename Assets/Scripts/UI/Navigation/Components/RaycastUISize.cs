using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! A size override for UIs and raycasting.
    public struct RaycastUISize : IComponentData
    {
        public float2 size;

        public RaycastUISize(float2 size)
        {
            this.size = size;
        }
    }
}