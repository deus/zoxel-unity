using Unity.Entities;

namespace Zoxel.UI
{
    //! Attached to a ui to animate it after selection state has changed.
    public struct NavigationAnimation : IComponentData
    {
        public double timeSelected;
        private byte selected;
        private byte hasFinalizedColor;

        public static NavigationAnimation SelectEvent(double elapsedTime)
        {
            var navigationAnimation = new NavigationAnimation();
            navigationAnimation.timeSelected = elapsedTime;
            navigationAnimation.selected = 1;
            navigationAnimation.hasFinalizedColor = 0;
            return navigationAnimation;
        }

        public static NavigationAnimation DeselectEvent(double elapsedTime)
        {
            var navigationAnimation = new NavigationAnimation();
            navigationAnimation.timeSelected = elapsedTime;
            navigationAnimation.selected = 0;
            navigationAnimation.hasFinalizedColor = 0;
            return navigationAnimation;
        }
        
        public void Select(double timeSelected_)
        {
            hasFinalizedColor = 0;
            selected = 1;
            timeSelected = timeSelected_;
        }

        public void Deselect(double timeSelected_)
        {
            if (selected == 1)
            {
                hasFinalizedColor = 0;
                selected = 0;
                timeSelected = timeSelected_;
            }
        }

        public bool IsSelectedAnimation(double currentTime, double navigationAnimationTime)
        {
            return selected == 1 && GetAnimationLerp(currentTime, navigationAnimationTime) <= navigationAnimationTime;
        }

        public bool IsSelectedFinalAnimation(double currentTime, double navigationAnimationTime)
        {
            if (selected == 1 && hasFinalizedColor == 0 && GetAnimationLerp(currentTime, navigationAnimationTime) >= navigationAnimationTime)
            {
                hasFinalizedColor = 1;
                return true;
            }
            return false;
        }

        public bool IsDeselectedAnimation(double currentTime, double navigationAnimationTime)
        {
            return selected == 0 && GetAnimationLerp(currentTime, navigationAnimationTime) <= navigationAnimationTime;
        }

        public bool IsDeselectedFinalAnimation(double currentTime, double navigationAnimationTime)
        {
            if (selected == 0 && hasFinalizedColor == 0 && GetAnimationLerp(currentTime, navigationAnimationTime) >= navigationAnimationTime)
            {
                hasFinalizedColor = 1;
                return true;
            }
            return false;
        }

        public float GetAnimationLerp(double currentTime, double navigationAnimationTime)
        {
            var timePassed = (currentTime - timeSelected);
            if (timePassed >= navigationAnimationTime)
            {
                return 1;   // finished
            }
            return (float)(timePassed / navigationAnimationTime);
        }
    }
}