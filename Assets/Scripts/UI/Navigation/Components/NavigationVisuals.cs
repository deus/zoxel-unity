using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! Holds data for animating a ui when navigated.
    public struct NavigationVisuals : IComponentData
    {
        public double navigationAnimationTime;
        public float growthScale;
        public Color fillColor;
        public Color selectedFillColor;
        public Color frameColor;
        public Color selectedFrameColor;

        public NavigationVisuals(Color fillColor, Color selectedFillColor, Color frameColor, Color selectedFrameColor)
        {
            this.navigationAnimationTime = 0;
            this.growthScale = 0;
            this.fillColor = fillColor;
            this.selectedFillColor = selectedFillColor;
            this.frameColor = frameColor;
            this.selectedFrameColor = selectedFrameColor;
        }
        
        public float4 GetFillColor()
        {
            return fillColor.ToFloat4();
        }

        public float4 GetSelectedFillColor()
        {
            return selectedFillColor.ToFloat4();
        }
        
        public float4 GetFrameColor()
        {
            return frameColor.ToFloat4();
        }

        public float4 GetSelectedFrameColor()
        {
            return selectedFrameColor.ToFloat4();
        }
    }
}