using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! Data used for navigation in the UI from one ui to another.
    public struct NavigateData
    {
        public Entity button;
        public Entity panel;
        public int previousIndex;          // UISpawnSystem index in the UI
        public byte direction;              // direction that goes to next position
        public float2 targetPosition;
        public float2 previousPosition;
        public int targetIndex;
    }
}