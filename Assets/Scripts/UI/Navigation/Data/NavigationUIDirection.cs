using Unity.Entities;

namespace Zoxel.UI
{
    public static class NavigationUIDirection
    {
        public const byte Right = 0;
        public const byte Left = 1;
        public const byte Up = 2;
        public const byte Down = 3;
    }
}