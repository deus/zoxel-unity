namespace Zoxel.UI
{
    public enum NavigationDirection : byte
    {
        Right,
        Left,
        Up,
        Down
    }
}