using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Zoxel.Transforms;
// todo: make this event based instead of per frame
//          when select, colour changes over .5 seconds, and then component is removed

namespace Zoxel.UI
{
    //! Animates InputFieldActivated entities using MaterialBaseColor and MaterialFrameColor.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class InputFieldActivatedColorSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var time = World.Time.ElapsedTime;
            var activatedFillColorU = UIManager.instance.uiDatam.inputActivatedFillColor;
            var inputActivatedFillColor = new float4(activatedFillColorU.r, activatedFillColorU.g, activatedFillColorU.b, activatedFillColorU.a);
            var inputActivatedFrameColorU = UIManager.instance.uiDatam.inputActivatedFrameColor;
            var inputActivatedFrameColor = new float4(inputActivatedFrameColorU.r, inputActivatedFrameColorU.g, inputActivatedFrameColorU.b, inputActivatedFrameColorU.a);
            Dependency = Entities
                .WithAll<InputActivated>()
                .ForEach((ref MaterialBaseColor materialBaseColor, ref MaterialFrameColor materialFrameColor) => // , in NavigationAnimation navigationAnimation, in NavigationVisuals navigationVisuals) =>
            {
                //var selectedFillColor = navigationVisuals.GetSelectedFillColor();
                //var lerpTime = navigationAnimation.GetAnimationLerp(time, navigationVisuals.navigationAnimationTime);
                var newFillColor = inputActivatedFillColor; // math.lerp(selectedFillColor, inputActivatedFillColor, lerpTime);
                if (!(newFillColor.x == materialBaseColor.Value.x && newFillColor.y == materialBaseColor.Value.y &&
                    newFillColor.z == materialBaseColor.Value.z && newFillColor.w == materialBaseColor.Value.w))
                {
                    materialBaseColor.Value = newFillColor;
                }
                if (!(inputActivatedFrameColor.x == materialFrameColor.Value.x && inputActivatedFrameColor.y == materialFrameColor.Value.y &&
                    inputActivatedFrameColor.z == materialFrameColor.Value.z && inputActivatedFrameColor.w == materialFrameColor.Value.w))
                {
                    materialFrameColor.Value = inputActivatedFrameColor;
                }
            }).ScheduleParallel(Dependency);
        }
    }
}