using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Removes tooltip on ui deselection.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class NavigationDeselectedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((int entityInQueryIndex, in PlayerTooltipLinks playerTooltipLinks, in SelectionEvent selectionEvent) =>
            {
                if (selectionEvent.ui.Index == 0)
                {
                    if (HasComponent<RenderText>(playerTooltipLinks.selectionTooltip))
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, playerTooltipLinks.selectionTooltip, new SetTooltipText(new Text()));
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities.ForEach((int entityInQueryIndex, in PlayerPanelTooltipLink playerPanelTooltipLink, in SelectionEvent selectionEvent) =>
            {
                if (selectionEvent.ui.Index == 0)
                {
                    if (HasComponent<RenderText>(playerPanelTooltipLink.tooltip))
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, playerPanelTooltipLink.tooltip, new SetTooltipText(new Text()));
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}