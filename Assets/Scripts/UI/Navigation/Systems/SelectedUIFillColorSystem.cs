using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! Sets the uis fill colour based on selected or not.
    /**
    *   Todo: make this event based instead of per frame
    *       when select, colour changes over .5 seconds, and then component is removed
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class SelectedUIFillColorSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            // Fill Color
            Dependency = Entities
                .WithNone<InitializeEntity, InitializeButtonColors>()
                .WithNone<InputActivated>()
                .ForEach((ref MaterialBaseColor materialBaseColor, in NavigationAnimation navigationAnimation, in NavigationVisuals navigationVisuals) =>
            {
                var fillColor = navigationVisuals.GetFillColor();
                var selectedFillColor = navigationVisuals.GetSelectedFillColor();
                var lerpTime = navigationAnimation.GetAnimationLerp(elapsedTime, navigationVisuals.navigationAnimationTime);
                var color = new float4();
                if (navigationAnimation.IsSelectedAnimation(elapsedTime, navigationVisuals.navigationAnimationTime))
                {
                    color = math.lerp(fillColor, selectedFillColor, lerpTime);
                }
                else if (navigationAnimation.IsDeselectedAnimation(elapsedTime, navigationVisuals.navigationAnimationTime))
                {
                    color = math.lerp(selectedFillColor, fillColor, lerpTime);
                }
                else if(navigationAnimation.IsSelectedFinalAnimation(elapsedTime, navigationVisuals.navigationAnimationTime))
                {
                    color = selectedFillColor;
                    // UnityEngine.Debug.LogError("Selected Final Animation: " + elapsedTime);
                }
                else if(navigationAnimation.IsDeselectedFinalAnimation(elapsedTime, navigationVisuals.navigationAnimationTime))
                {
                    color = fillColor;
                }
                else
                {
                    return;
                }
                if (!(color.x == materialBaseColor.Value.x && color.y == materialBaseColor.Value.y && color.z == materialBaseColor.Value.z && color.w == materialBaseColor.Value.w))
                {
                    materialBaseColor.Value = color;
                }
            }).ScheduleParallel(Dependency);
            
            // Scale
            var originalScale = new float3(1,1,1);
            Dependency = Entities
                //.WithNone<InputActivated>()
                .WithNone<InitializeEntity, InitializeButtonColors>()
                .ForEach((ref LocalScale localScale, in NavigationAnimation navigationAnimation, in NavigationVisuals navigationVisuals) =>
            {
                var selectedScale = originalScale * navigationVisuals.growthScale;
                var lerpTime = navigationAnimation.GetAnimationLerp(elapsedTime, navigationVisuals.navigationAnimationTime);
                if (navigationAnimation.IsSelectedAnimation(elapsedTime, navigationVisuals.navigationAnimationTime))
                {
                    localScale.scale = math.lerp(originalScale, selectedScale, lerpTime);
                }
                else if (navigationAnimation.IsDeselectedAnimation(elapsedTime, navigationVisuals.navigationAnimationTime))
                {
                    localScale.scale = math.lerp(selectedScale, originalScale, lerpTime);
                }
                else if(navigationAnimation.IsSelectedFinalAnimation(elapsedTime, navigationVisuals.navigationAnimationTime))
                {
                    localScale.scale = selectedScale;
                }
                else if(navigationAnimation.IsDeselectedFinalAnimation(elapsedTime, navigationVisuals.navigationAnimationTime))
                {
                    localScale.scale = originalScale;
                }
            }).ScheduleParallel(Dependency);
        }
    }
}