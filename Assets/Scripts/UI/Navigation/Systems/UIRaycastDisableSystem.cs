using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
// using Zoxel.Cameras;
using Zoxel.Transforms;
// todo: use local FrustrumSize rather then a global one
// todo:    have two entities, one is your selected, the other is mouseOver one
//          this way if you have selected something with a click, but the other is just what mouse is over
//          If you click on nothing, then deactivate input feild
// todo:    Use skewered screen input, for split screen mouse input
// for mouse position
// for all uis
// check if mouse between
// Divide this by screen size (x 20, y 50) / 1920 x 1080

namespace Zoxel.UI
{
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class UIRaycastDisableSystem : SystemBase
	{
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<EnablePanelRaycast, NavigationDirty, OnChildrenSpawned>()
                .WithAll<DisablePanelRaycast>()
                .ForEach((Entity e, int entityInQueryIndex, in Childrens childrens) =>
            {
                PostUpdateCommands.RemoveComponent<DisablePanelRaycast>(entityInQueryIndex, e);
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    var uiEntity = childrens.children[i];
                    if (HasComponent<NavigationVisuals>(uiEntity))
                    {
                        PostUpdateCommands.AddComponent<DisableUIRaycast>(entityInQueryIndex, uiEntity);
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<NavigationDirty, OnChildrenSpawned>()
                .WithAll<EnablePanelRaycast>()
                .ForEach((Entity e, int entityInQueryIndex, in Childrens childrens) =>
            {
                PostUpdateCommands.RemoveComponent<EnablePanelRaycast>(entityInQueryIndex, e);
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    var uiEntity = childrens.children[i];
                    if (HasComponent<DisableUIRaycast>(uiEntity))
                    {
                        PostUpdateCommands.RemoveComponent<DisableUIRaycast>(entityInQueryIndex, uiEntity);
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}