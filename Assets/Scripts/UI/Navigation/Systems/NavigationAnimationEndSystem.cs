using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Removes a ui animation event after it has finished animating.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class NavigationAnimationEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, InitializeButtonColors>()
                .ForEach((Entity e, int entityInQueryIndex, in NavigationAnimation navigationAnimation, in NavigationVisuals navigationVisuals) =>
            {
                if (navigationAnimation.IsSelectedFinalAnimation(elapsedTime, navigationVisuals.navigationAnimationTime) ||
                    navigationAnimation.IsDeselectedFinalAnimation(elapsedTime, navigationVisuals.navigationAnimationTime))
                {
                    // UnityEngine.Debug.LogError("Deselected Finalized Selection Event: " + e.Index);
                    PostUpdateCommands.RemoveComponent<NavigationAnimation>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}