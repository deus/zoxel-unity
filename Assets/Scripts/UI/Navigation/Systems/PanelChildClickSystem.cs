using Unity.Entities;
using Unity.Burst;
using Zoxel.UI;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! Selects a child of an entity using SelectionEvent events.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class PanelChildClickSystem : SystemBase
    {
        private const float gameUISwitchDelay = 0.4f;
        private const int maxUIs = 7;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, in Childrens childrens, in ClickPanelChild clickPanelChild) =>
            {
                PostUpdateCommands.RemoveComponent<ClickPanelChild>(entityInQueryIndex, e);
                if (clickPanelChild.index < childrens.children.Length)
                {
                    var selectedUI = childrens.children[clickPanelChild.index];
                    PostUpdateCommands.AddComponent(entityInQueryIndex, selectedUI,
                        new UIClickEvent(clickPanelChild.controllerEntity, new Entity(), clickPanelChild.deviceType, clickPanelChild.buttonType));
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
