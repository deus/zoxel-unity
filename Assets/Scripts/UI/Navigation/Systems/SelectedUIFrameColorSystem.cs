﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Zoxel.Transforms;
// todo: make this event based instead of per frame
//          when select, colour changes over .5 seconds, and then component is removed

namespace Zoxel.UI
{
    //! Animates a UIFrame when an element is selected.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class SelectedUIFrameColorSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            // frame only
            Dependency = Entities
                .WithNone<InitializeEntity, InitializeButtonColors>()
                .WithNone<InputActivated, ToggleActivated>()
                .ForEach((Entity e, ref MaterialFrameColor materialFrameColor, in NavigationAnimation navigationAnimation, in NavigationVisuals navigationVisuals) =>
            {
                var lerpTime = navigationAnimation.GetAnimationLerp(elapsedTime, navigationVisuals.navigationAnimationTime);
                var frameColor = navigationVisuals.GetFrameColor();
                var selectedFrameColor = navigationVisuals.GetSelectedFrameColor();
                var color = new float4();
                if (navigationAnimation.IsSelectedAnimation(elapsedTime, navigationVisuals.navigationAnimationTime))
                {
                    color = math.lerp(frameColor, selectedFrameColor, lerpTime);
                }
                else if (navigationAnimation.IsDeselectedAnimation(elapsedTime, navigationVisuals.navigationAnimationTime))
                {
                    color = math.lerp(selectedFrameColor, frameColor, lerpTime);
                }
                else if(navigationAnimation.IsSelectedFinalAnimation(elapsedTime, navigationVisuals.navigationAnimationTime))
                {
                    color = selectedFrameColor;
                }
                else if(navigationAnimation.IsDeselectedFinalAnimation(elapsedTime, navigationVisuals.navigationAnimationTime))
                {
                    color = frameColor;
                }
                else
                {
                    color = selectedFrameColor;
                    return;
                }
                if (!(color.x == materialFrameColor.Value.x && color.y == materialFrameColor.Value.y && color.z == materialFrameColor.Value.z && color.w == materialFrameColor.Value.w))
                {
                    materialFrameColor.Value = color;
                }
            }).ScheduleParallel(Dependency);
        }
    }
}