using Unity.Entities;
using Unity.Burst;
using Zoxel.UI;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! Selects a child of an entity using SelectionEvent events.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class PanelChildSelectSystem : SystemBase
    {
        private const float gameUISwitchDelay = 0.4f;
        private const int maxUIs = 7;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, in Childrens childrens, in SelectPanelChild selectPanelChild) =>
            {
                PostUpdateCommands.RemoveComponent<SelectPanelChild>(entityInQueryIndex, e);
                if (selectPanelChild.index < childrens.children.Length)
                {
                    var selectedUI = childrens.children[selectPanelChild.index];
                    PostUpdateCommands.AddComponent(entityInQueryIndex, selectPanelChild.controllerEntity, 
                        new SelectionEvent(SelectionEventType.Input, selectPanelChild.controllerEntity, selectedUI, selectPanelChild.index));
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
