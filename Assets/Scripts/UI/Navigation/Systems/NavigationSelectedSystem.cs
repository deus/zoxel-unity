﻿using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Audio;

namespace Zoxel.UI
{
    //! Selects a new UI (navigator.selected).
    /**
    *   This event comes from MouseRaycastSystem or NavigationSystem.
    *   One uses mouse rays while the other uses positional navigation (gamepad).
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class NavigationSelectedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var elapsedTime2 = UISystemGroup.elapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref Navigator navigator, in SelectionEvent selectionEvent) =>
            {
                var controllerEntity = selectionEvent.character;
                var newSelected = selectionEvent.ui;
                var oldSelected = navigator.selected;
                var isNewOld = oldSelected == newSelected;
                if (isNewOld)
                {
                    PostUpdateCommands.RemoveComponent<SelectionEvent>(entityInQueryIndex, e);
                    return;
                }
                PostUpdateCommands.RemoveComponent<SelectionEvent>(entityInQueryIndex, e);
                if (HasComponent<NavigationVisuals>(oldSelected))
                {
                    var navigationAnimation = NavigationAnimation.DeselectEvent(elapsedTime);
                    if (HasComponent<NavigationAnimation>(oldSelected))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, oldSelected, navigationAnimation);
                    }
                    else
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, oldSelected, navigationAnimation);
                    }
                    PostUpdateCommands.AddComponent(entityInQueryIndex, oldSelected, new UIDeselectEvent(controllerEntity, newSelected));
                }
                navigator.selected = newSelected;
                // UnityEngine.Debug.LogError("[" + elapsedTime2 + "] NavigationSelectedSystem - New [" + newSelected.Index + "] : Old [" + oldSelected.Index + "]");
                if (HasComponent<NavigationVisuals>(newSelected))
                {
                    if (HasComponent<Sound>(newSelected))
                    {
                        PostUpdateCommands.AddComponent<PlaySound>(entityInQueryIndex, newSelected);
                    }
                    if (selectionEvent.type == (byte) SelectionEventType.Input)
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, newSelected, NavigationAnimation.SelectEvent(elapsedTime));
                    }
                    if (HasComponent<Button>(newSelected))
                    {
                        if (!HasComponent<UISelectEvent>(newSelected))
                        {
                            PostUpdateCommands.AddComponent(entityInQueryIndex, newSelected, new UISelectEvent(controllerEntity));
                        }
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}