using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Removes events off uis after one frame.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class UISelectionEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery buttonSelectionQuery;
        private EntityQuery buttonDeselectionQuery;
        private EntityQuery buttonClickQuery;
        private EntityQuery inputFinishedQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            buttonSelectionQuery = GetEntityQuery(ComponentType.ReadOnly<UISelectEvent>());
            buttonDeselectionQuery = GetEntityQuery(ComponentType.ReadOnly<UIDeselectEvent>());
            buttonClickQuery = GetEntityQuery(ComponentType.ReadOnly<UIClickEvent>());
            inputFinishedQuery = GetEntityQuery(ComponentType.ReadOnly<InputFinished>());
            /*RequireForUpdate(buttonSelectionQuery);
            RequireForUpdate(buttonDeselectionQuery);
            RequireForUpdate(buttonClickQuery);
            RequireForUpdate(inputFinishedQuery);*/
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            if (!buttonSelectionQuery.IsEmpty)
            {
                PostUpdateCommands.RemoveComponent<UISelectEvent>(buttonSelectionQuery);
            }
            if (!buttonDeselectionQuery.IsEmpty)
            {
                PostUpdateCommands.RemoveComponent<UIDeselectEvent>(buttonDeselectionQuery);
            }
            if (!buttonClickQuery.IsEmpty)
            {
                PostUpdateCommands.RemoveComponent<UIClickEvent>(buttonClickQuery);
            }
            if (!inputFinishedQuery.IsEmpty)
            {
                PostUpdateCommands.RemoveComponent<InputFinished>(inputFinishedQuery);
            }
        }
    }
}
            /*var PostUpdateCommands = PostUpdateCommands.AsParallelWriter();
            Dependency = Entities
                .WithAll<UISelectEvent>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<UISelectEvent>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<UIClickEvent>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<UIClickEvent>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<InputFinished>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<InputFinished>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);*/