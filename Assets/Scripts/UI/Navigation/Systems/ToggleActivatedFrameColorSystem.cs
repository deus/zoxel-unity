using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
// todo: make this event based instead of per frame
//          when select, colour changes over .5 seconds, and then component is removed

namespace Zoxel.UI
{
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class ToggleActivatedFrameColorSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var time = World.Time.ElapsedTime;
            var activatedTabColorU = UIManager.instance.uiDatam.activatedTabColor;
            var activatedTabColor = new float4(activatedTabColorU.r, activatedTabColorU.g, activatedTabColorU.b, activatedTabColorU.a); // new float4(0.2f, 1, 1, 1f);
            Dependency = Entities
                .WithAll<Toggle, ToggleActivated>()
                .ForEach((ref MaterialFrameColor materialFrameColor, in NavigationAnimation navigationAnimation, in NavigationVisuals navigationVisuals) =>
            {
                var lerpTime = navigationAnimation.GetAnimationLerp(time, navigationVisuals.navigationAnimationTime);
                var color = math.lerp(materialFrameColor.Value, activatedTabColor, lerpTime);
                if (!(color.x == materialFrameColor.Value.x && color.y == materialFrameColor.Value.y && 
                    color.z == materialFrameColor.Value.z && color.w == materialFrameColor.Value.w))
                {
                    materialFrameColor.Value = color;
                }
            }).ScheduleParallel(Dependency);
        }
    }
}