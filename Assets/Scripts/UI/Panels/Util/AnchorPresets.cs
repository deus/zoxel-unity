﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! Utility functions used by UI module.
    public static class AnchorPresets
    {
        //! This just gets offset based off an anchor. \todo Make into a const.
        public static float3 TransposePosition(float3 panelPosition, byte anchor)
        {
            if (anchor == AnchorUIType.Middle)
            {
                panelPosition = new float3(0.5f, 0.5f, 0); //  + new float3(panelPosition.x, -panelPosition.y, 0);
            }
            else if (anchor == AnchorUIType.BottomLeft)
            {
                panelPosition = new float3(0f, 0f, 0);
            }
            else if (anchor == AnchorUIType.BottomMiddle)
            {
                panelPosition = new float3(0.5f, 0f, 0);
            }
            else if (anchor == AnchorUIType.BottomRight)
            {
                panelPosition = new float3(1f, 0f, 0);
            }
            else if (anchor == AnchorUIType.TopLeft)
            {
                // panelPosition = new float3(0.5f, 1f, 0);
                panelPosition = new float3(0f, 1f, 0);
            }
            else if (anchor == AnchorUIType.TopMiddle)
            {
                panelPosition = new float3(0.5f, 1f, 0);
            }
            else if (anchor == AnchorUIType.TopRight)
            {
                panelPosition = new float3(1f, 1f, 0);
            }
            else if (anchor == AnchorUIType.Left)
            {
                panelPosition = new float3(0f, 0.5f, 0);
            }
            else if (anchor == AnchorUIType.Right)
            {
                panelPosition = new float3(1f, 0.5f, 0);
            }
            else if (anchor == AnchorUIType.LeftMiddle)
            {
                panelPosition = new float3(0.25f, 0.5f, 0);
            }
            else if (anchor == AnchorUIType.RightMiddle)
            {
                panelPosition = new float3(0.75f, 0.5f, 0);
            }
            return panelPosition;
        }

        public static float3 GetOrbitAnchors(byte anchor, float3 orbitPosition)
        {
            var originalOrbitValue = new float2(orbitPosition.x, orbitPosition.y);
            if (anchor == AnchorUIType.BottomLeft)
            {
                orbitPosition.x = -0.5f;
                orbitPosition.y = 0.5f;
            }
            // Testing Actionbar UI!
            else if (anchor == AnchorUIType.BottomMiddle)
            {
                orbitPosition.x = 0;
                orbitPosition.y = 0.5f;
            }
            else if (anchor == AnchorUIType.BottomRight)
            {
                orbitPosition.x = 0.5f;
                orbitPosition.y = 0.5f;
            }
            else if (anchor == AnchorUIType.TopRight)
            {
                orbitPosition.x = 0.5f;
                orbitPosition.y = -0.5f;
            }
            else if (anchor == AnchorUIType.TopMiddle)
            {
                orbitPosition.x = 0;
                orbitPosition.y = -0.5f;
            }
            else if (anchor == AnchorUIType.TopLeft)
            {
                orbitPosition.x = -0.5f;
                orbitPosition.y = -0.5f;
            }
            else if (anchor == AnchorUIType.Left)
            {
                orbitPosition.x = -0.5f;
                orbitPosition.y = 0;
            }
            else if (anchor == AnchorUIType.Right)
            {
                orbitPosition.x = 0.5f;
                orbitPosition.y = 0;
            }
            else if (anchor == AnchorUIType.LeftMiddle)
            {
                orbitPosition.x = -0.25f;
                orbitPosition.y = 0;
            }
            else if (anchor == AnchorUIType.RightMiddle)
            {
                orbitPosition.x = 0.25f;
                orbitPosition.y = 0;
            }
            orbitPosition.x += originalOrbitValue.x;
            orbitPosition.y += originalOrbitValue.y;
            return orbitPosition;
        }

        public static float3 GetAnchorOffset(byte anchor, float2 panelSize)
        {
            if (anchor == AnchorUIType.BottomLeft)
            {
                return new float3(panelSize.x / 2f, panelSize.y / 2f, 0);
            }
            // Testing Actionbar UI!
            else if (anchor == AnchorUIType.BottomMiddle)
            {
                return new float3(0, + panelSize.y / 2f, 0);
            }
            else if (anchor == AnchorUIType.BottomRight)
            {
                return new float3(- panelSize.x / 2f, panelSize.y / 2f, 0);
            }
            else if (anchor == AnchorUIType.TopRight)
            {
                return new float3(- panelSize.x / 2f, - panelSize.y / 2f, 0);
            }
            else if (anchor == AnchorUIType.TopMiddle)
            {
                return new float3(0, - panelSize.y / 2f, 0);
            }
            else if (anchor == AnchorUIType.TopLeft)
            {
                return new float3(panelSize.x / 2f, - panelSize.y / 2f, 0);
            }
            else if (anchor == AnchorUIType.Left)
            {
                return new float3(panelSize.x / 2f, 0, 0);
            }
            else if (anchor == AnchorUIType.Right)
            {
                return new float3(-panelSize.x / 2f, 0, 0);
            }
            else if (anchor == AnchorUIType.LeftMiddle)
            {
                return new float3(panelSize.x / 4f, 0, 0);
            }
            else if (anchor == AnchorUIType.RightMiddle)
            {
                return new float3(-panelSize.x / 4f, 0, 0);
            }
            return new float3();
        }
    }
}