using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Textures;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! Spawns a headerEntity ui for PanelUI's.
    /**
    *   \todo Spawn directly in UI Spawn Systems.
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class HeaderSpawnSystem : SystemBase
    {        
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity headerPrefab;
        private NativeArray<Text> texts;

        /*protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var texts2 = new NativeList<Text>();
            texts2.Add(new Text("Paused"));
            texts2.Add(new Text("Status"));
            texts2.Add(new Text("Inventory"));
            texts2.Add(new Text("Skills"));
            texts2.Add(new Text("Stats"));

            texts2.Add(new Text("Equipment"));
            texts2.Add(new Text("Map"));
            texts2.Add(new Text("Questlog"));
            texts2.Add(new Text("Skilltree"));
            texts2.Add(new Text("Craft"));

            texts2.Add(new Text(""));
            texts2.Add(new Text("Logs"));
            texts2.Add(new Text(""));
            texts2.Add(new Text(""));
            texts2.Add(new Text(""));

            texts2.Add(new Text(""));
            texts2.Add(new Text(""));
            texts2.Add(new Text(""));
            texts2.Add(new Text("Shop"));
            texts2.Add(new Text("Zoxel"));

            texts2.Add(new Text("Which realm"));
            texts2.Add(new Text("Who are you"));
            texts2.Add(new Text("Make Realm"));
            texts2.Add(new Text("Make Body"));
            texts2.Add(new Text("Options"));
            texts2.Add(new Text("Chest"));
            texts2.Add(new Text("Music"));
            
            texts = texts2.ToArray(Allocator.Persistent);
            texts2.Dispose();
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }*/

        [BurstCompile]
        protected override void OnUpdate()
        {
            /*var texts = this.texts;
            HeaderSpawnSystem.InitializePrefabs(EntityManager);
            var headerPrefab = HeaderSpawnSystem.headerPrefab;
            var headerStyle = UIManager.instance.uiDatam.headerStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, MeshUIDirty, GridUIDirty>()
                .WithAll<SpawnHeaderUI>()
                .ForEach((Entity e, int entityInQueryIndex, in PanelUI panelUI, in Size2D size2D) =>
            {
                if (size2D.size.x == 0 || size2D.size.y == 0)
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<SpawnHeaderUI>(entityInQueryIndex, e);
                var headerText = texts[panelUI.panelType].Clone();
                HeaderSpawnSystem.SpawnHeaderUI(PostUpdateCommands, entityInQueryIndex, headerPrefab, e, size2D.size, headerStyle.fontSize, headerStyle.textPadding,
                    in headerText);
			})  .WithReadOnly(texts)
                .ScheduleParallel(Dependency);*/
        }

        public static Entity SpawnHeaderUI(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity headerPrefab,
            Entity panelEntity, float2 panelSize, float fontSize, float2 textPadding, in Text headerText)
        {
            var headerHeight = fontSize + textPadding.y * 2f;
            var size = new float2(panelSize.x, headerHeight);
            var position = new float3(0, 0.5f * panelSize.y + size.y / 2f, 0);
            var e2 = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, headerPrefab, panelEntity, position, size);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new RenderText(headerText));
            PostUpdateCommands.SetComponent(entityInQueryIndex, panelEntity, new HeaderLink(e2));
            return e2;
        }

        public static Entity SpawnHeaderUI(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity headerPrefab,
            Entity panelEntity, in Text headerText)
        {
            var e2 = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, headerPrefab, panelEntity);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new RenderText(headerText));
            PostUpdateCommands.SetComponent(entityInQueryIndex, panelEntity, new HeaderLink(e2));
            return e2;
        }

        public static void InitializePrefabs(EntityManager EntityManager)
        {
            if (HeaderSpawnSystem.headerPrefab.Index != 0)
            {
                return;
            }
            var uiDatam = UIManager.instance.uiDatam;
            var headerStyle = uiDatam.headerStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var headerPrefab = EntityManager.Instantiate(UICoreSystem.labelFixedSizePrefab);
            HeaderSpawnSystem.headerPrefab = headerPrefab;
            EntityManager.AddComponent<Prefab>(headerPrefab);
            EntityManager.AddComponent<HeaderUI>(headerPrefab);
            UICoreSystem.RemoveChildComponents(EntityManager, headerPrefab);
            // Set new Style
            EntityManager.SetComponentData(headerPrefab, new MaterialBaseColor(headerStyle.color));
            EntityManager.SetComponentData(headerPrefab, new MaterialFrameColor(headerStyle.frameGenerationData.color));
            EntityManager.SetComponentData(headerPrefab, new TextureFrame(in headerStyle.frameGenerationData));
            UICoreSystem.SetRenderTextData(EntityManager, headerPrefab, headerStyle.textGenerationData,
                headerStyle.textColor, headerStyle.textOutlineColor, headerStyle.fontSize, headerStyle.textPadding);
            // Set a header text
            // texts[19] = new Text(UIManager.instance.realmName); // "Zoxel";
        }
    }
}