using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Using DestroyEntity event, also destroys a HeaderUI connected to a PanelUI.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
	public partial class HeaderDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyEntity, HeaderLink>()
                .ForEach((Entity e, int entityInQueryIndex, in HeaderLink headerLink) =>
            {
                if (HasComponent<UIElement>(headerLink.header))
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, headerLink.header);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}