using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! Repositions the HeaderUI after the PanelUI updates.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class HeaderUpdateSystem : SystemBase
    {        
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var gamePanelMaterial = UIManager.instance.materials.panelMaterial;
            var uiDatam = UIManager.instance.uiDatam;
            var headerStyle = uiDatam.headerStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<HeaderUIDirty>()
                .ForEach((Entity e, int entityInQueryIndex, in HeaderLink headerLink, in Size2D size2D) =>
            {
                PostUpdateCommands.RemoveComponent<HeaderUIDirty>(entityInQueryIndex, e);
                if (HasComponent<LocalPosition>(headerLink.header))
                {
                    var panelSizeX = size2D.size.x;
                    var panelSizeY = size2D.size.y;
                    var headerSize = new float2(
                        panelSizeX,
                        headerStyle.fontSize + headerStyle.textPadding.y * 2f);   //   + gridUI.margins.y * 4
                    var headerPosition = new float3(0,
                        0.5f * panelSizeY + headerSize.y / 2f, 0);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, headerLink.header, new LocalPosition(headerPosition));
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}