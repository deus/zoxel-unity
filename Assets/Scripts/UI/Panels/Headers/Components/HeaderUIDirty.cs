using Unity.Entities;

namespace Zoxel.UI
{
    //! An event for resizing header to match the PanelUI.
    public struct HeaderUIDirty : IComponentData { }
}