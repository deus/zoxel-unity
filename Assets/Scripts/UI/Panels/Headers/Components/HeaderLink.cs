using Unity.Entities;

namespace Zoxel.UI
{
    //! Links to a HeaderUI.
    public struct HeaderLink : IComponentData
    {
        public Entity header;

        public HeaderLink(Entity header)
        {
            this.header = header;
        }
    }
}