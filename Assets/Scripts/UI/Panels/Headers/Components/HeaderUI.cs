using Unity.Entities;

namespace Zoxel.UI
{
    //! A Label that sits on top of the PanelUI.
    public struct HeaderUI : IComponentData { }
}