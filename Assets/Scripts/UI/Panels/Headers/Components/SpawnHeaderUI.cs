using Unity.Entities;

namespace Zoxel.UI
{
    //! Spawns a header UI on the PanelUI.
    public struct SpawnHeaderUI : IComponentData { }
}