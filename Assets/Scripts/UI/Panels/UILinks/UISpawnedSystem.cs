using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.UI
{
    //! Finishes linking new uis after they spawn.
    /**
    *   - Linking System -
    *   \todo Replace CharacterLink, use with UIHolderLink instead.
    *   Handles indexless new character uis.
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class UISpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;
        private EntityQuery uisQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<OnUISpawned>());
            uiHoldersQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadWrite<UILink>());
            uisQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<NewCharacterUI>(),
                ComponentType.ReadOnly<CharacterUI>(),
                ComponentType.ReadOnly<CharacterLink>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
			// PostUpdateCommands2.DestroyEntitiesForEntityQuery(processQuery);
			PostUpdateCommands2.RemoveComponent<NewCharacterUI>(uisQuery);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			//! Next initializes children.
			var spawnedEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
			Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var onUISpawneds = GetComponentLookup<OnUISpawned>(true);
			var newUIEntities = uisQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
			Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var characterLinks = GetComponentLookup<CharacterLink>(true);
			Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref UILink uiLink) =>
			{
                byte addedCount = 0;
				for (int i = 0; i < spawnedEntities.Length; i++)
				{
					var e2 = spawnedEntities[i];
					var onUISpawned = onUISpawneds[e2];
					if (e == onUISpawned.uiHolder)
					{
						// uiLinks.Add(onMinivoxesSpawned2.added.Length);
                        addedCount += onUISpawned.spawned;
					}
				}
                if (addedCount > 0)
                {
                    var beforeCount = (byte) uiLink.uis.Length;
                    uiLink.AddNew(addedCount);
                    if (HasComponent<RealmUILink>(e))
                    {
                        PostUpdateCommands.AddComponent<OnUILinkUpdated>(entityInQueryIndex, e);
                    }
                    byte count = 0;
                    for (int i = 0; i < newUIEntities.Length; i++)
                    {
                        var e2 = newUIEntities[i];
                        var characterLink = characterLinks[e2];
                        if (characterLink.character == e)
                        {
                            uiLink.uis[beforeCount + count] = e2;
                            count++;
                        }
                    }
                }
				// UnityEngine.Debug.LogError("Added " + onUISpawned.spawned + " to chunk.");
			})  .WithReadOnly(spawnedEntities)
				.WithReadOnly(onUISpawneds)
                .WithReadOnly(newUIEntities)
				.WithReadOnly(characterLinks)
				.WithDisposeOnCompletion(spawnedEntities)
                .WithDisposeOnCompletion(newUIEntities)
				.ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
			//! Finally, For each child, using the index, sets into parents children that is passed in.
			/*var parentEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
			Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var uiLinks = GetComponentLookup<UILink>(false);
			parentEntities.Dispose();
			Dependency = Entities
				.WithAll<NewCharacterUI>()
				.ForEach((Entity e, in CharacterUIIndex characterUIIndex, in CharacterLink characterLink) =>
			{
				var uiLinks2 = uiLinks[characterLink.character];
				uiLinks2.uis[characterUIIndex.index] = e;
				// uiLinks[chunkLink.chunk] = minivoxLinks2;
			})  .WithNativeDisableContainerSafetyRestriction(uiLinks)
				.ScheduleParallel(Dependency);*/
			// UnityEngine.Debug.LogError("Updated Minivoxes: " + processQuery.CalculateEntityCount() + " : " + removedMinivoxesQuery.CalculateEntityCount());

            /*var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uiEntities = uisQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var characterLinks = GetComponentLookup<CharacterLink>(true);
            var onUISpawnedEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var onUISpawneds = GetComponentLookup<OnUISpawned>(true);
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref UILink uiLink) =>
            {
                var didUpdate = false;
                for (int i = 0; i < onUISpawnedEntities.Length; i++)
                {
                    var e2 = onUISpawnedEntities[i];
                    var onUISpawned = onUISpawneds[e2];
                    if (onUISpawned.uiHolder == e)
                    {
                        // do things
                        if (onUISpawned.spawned != 0)
                        {
                            uiLink.AddNew(onUISpawned.spawned);
                            onUISpawned.spawned = 0;
                        }
                        PostUpdateCommands.DestroyEntity(entityInQueryIndex, e2);
                        didUpdate = true;
                        // UnityEngine.Debug.LogError("On Spawned UIs: " + uiLink.uis.Length);
                    }
                }
                if (!didUpdate)
                {
                    return;
                }
                var count = 0;
                for (int i = 0; i < uiEntities.Length; i++)
                {
                    var e2 = uiEntities[i];
                    var characterLink = characterLinks[e2];
                    if (characterLink.character == e)
                    {
                        uiLink.uis[count] = e2;
                        count++;
                        if (count >= uiLink.uis.Length)
                        {
                            // UnityEngine.Debug.LogError("Uis all found at: " + uiLink.uis.Length);
                            PostUpdateCommands.AddComponent<OnUILinkUpdated>(entityInQueryIndex, e);
                            break;
                        }
                    }
                }
            })  .WithReadOnly(uiEntities)
                .WithDisposeOnCompletion(uiEntities)
                .WithReadOnly(characterLinks)
                .WithReadOnly(onUISpawnedEntities)
                .WithDisposeOnCompletion(onUISpawnedEntities)
                .WithReadOnly(onUISpawneds)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);*/