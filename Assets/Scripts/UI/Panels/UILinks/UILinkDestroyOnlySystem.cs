using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Destroy's the UILink entities and clears links.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class UILinkDestroyOnlySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyUIs>()
                .ForEach((Entity e, int entityInQueryIndex, ref UILink uiLink) =>
            {
                for (int i = 0; i < uiLink.uis.Length; i++)
                 {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, uiLink.uis[i]);
                }
                uiLink.Dispose();
                PostUpdateCommands.RemoveComponent<DestroyUIs>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}