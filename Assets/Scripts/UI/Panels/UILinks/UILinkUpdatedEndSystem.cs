using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Ends event OnUILinkUpdated by removing it.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class UILinkUpdatedEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<OnUILinkUpdated>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<OnUILinkUpdated>(processQuery);
        }
    }
}