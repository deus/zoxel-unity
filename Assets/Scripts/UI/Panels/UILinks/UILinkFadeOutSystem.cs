using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Uses the DyingEntity event to fade out UIs.
    /**
    *   \todo Actually fade out uis instead of destroying them.
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class UILinkFadeOutSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DyingEntity, UILink>()
                .ForEach((Entity e, int entityInQueryIndex, ref UILink uiLink) =>
            {
                for (int i = 0; i < uiLink.uis.Length; i++)
                {
                    var ui = uiLink.uis[i];
                    if (HasComponent<UIElement>(ui))
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, ui);
                    }
                }
                uiLink.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}