using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.UI
{
    //! Removes a UI from UIHolder.
    /**
    *   \todo Refact this to be nicer.
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class UILinkRemoveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery removeCharacterUIQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			removeCharacterUIQuery = GetEntityQuery(
                ComponentType.ReadOnly<RemoveUI>(),
                ComponentType.Exclude<DelayEvent>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (removeCharacterUIQuery.IsEmpty)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var destroyUIEntities = removeCharacterUIQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
			var removeCharacterUIs = GetComponentLookup<RemoveUI>(true);
            Dependency = Entities
                .ForEach((Entity e, int entityInQueryIndex, ref UILink uiLink) =>
            {
                // native list? set uis at the end of all removals?
                var removedIndexes = new NativeList<byte>();
                for (int i = 0; i < destroyUIEntities.Length; i++)
                {
                    var e2 = destroyUIEntities[i];
                    var removeUI = removeCharacterUIs[e2];
                    if (removeUI.character == e)
                    {
                        PostUpdateCommands.DestroyEntity(entityInQueryIndex, e2);
                        // if time is up
                        // for all uis
                        for (int j = uiLink.uis.Length - 1; j >= 0; j--)
                        {
                            var ui = uiLink.uis[j];
                            if (!HasComponent<CharacterUI>(ui) || HasComponent<DestroyEntity>(ui))
                            {
                                continue;
                            }
                            if (removeUI.ui == ui)
                            {
                                // UnityEngine.Debug.LogError("Removing UI at: " + j + ", ui.Index: " + ui.Index);
                                PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, ui);
                                removedIndexes.Add((byte) j);
                                break;
                            }
                        }
                        // destroys if found a belonging character
                        // todo: just destroy them all after?
                    }
                }
                // decrease indexes / reset them
                if (removedIndexes.Length > 0)
                {
                    var uis = new NativeList<Entity>();
                    for (byte i = 0; i < uiLink.uis.Length; i++)
                    {
                        if (!removedIndexes.Contains(i))
                        {
                            uis.Add(uiLink.uis[i]);
                        }
                    }
                    uiLink.Dispose();
                    uiLink.uis = new BlitableArray<Entity>(uis.Length, Allocator.Persistent);
                    for (byte i = 0; i < uiLink.uis.Length; i++)
                    {
                        uiLink.uis[i] = uis[i];
                        // PostUpdateCommands.SetComponent(entityInQueryIndex, uis[i], new CharacterUIIndex(i));
                    }
                    uis.Dispose();
                }
                removedIndexes.Dispose();
            })  .WithReadOnly(destroyUIEntities)
                .WithDisposeOnCompletion(destroyUIEntities)
                .WithReadOnly(removeCharacterUIs)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}