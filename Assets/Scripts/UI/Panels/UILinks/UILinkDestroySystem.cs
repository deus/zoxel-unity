using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Clean up UIs connected to an entity with DestroyEntity event.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class UILinkDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyEntity, UILink>()
                .ForEach((int entityInQueryIndex, in UILink uiLink) =>
            {
                for (int i = 0; i < uiLink.uis.Length; i++)
                {
                    var e2 = uiLink.uis[i];
                    if (HasComponent<UIElement>(e2))
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e2);
                    }
                }
                uiLink.DisposeFinal();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}