﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Textures;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    //! Resizes UI Mesh after the size updates.
    /**
    *   \todo Make Panels use UIElement size instead.
    */
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class MeshUIDirtySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<MeshUIDirty>());
            RequireForUpdate(processQuery);
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var panelEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var panelMinSizes = GetComponentLookup<PanelMinSize>(true);
            panelEntities.Dispose();
            Entities
                .WithNone<ZoxMesh>()
                .WithAll<MeshUIDirty>()
                .ForEach((Entity e) =>
            {
                PostUpdateCommands.RemoveComponent<MeshUIDirty>(e);
            }).WithoutBurst().Run();
            Entities
                .WithAll<MeshUIDirty>()
                .ForEach((Entity e, ZoxMesh zoxMesh, ref Size2D size2D) =>
            {
                PostUpdateCommands.RemoveComponent<MeshUIDirty>(e);
                if (panelMinSizes.HasComponent(e))
                {
                    var panelMinSize = panelMinSizes[e];
                    if (panelMinSize.minSize.x != 0)
                    {
                        if (size2D.size.x < panelMinSize.minSize.x)
                        {
                            size2D.size.x = panelMinSize.minSize.x;
                        }
                    }
                    if (panelMinSize.minSize.y != 0)
                    {
                        if (size2D.size.y < panelMinSize.minSize.y)
                        {
                            size2D.size.y = panelMinSize.minSize.y;
                        }
                    }
                }
                var panelSize = size2D.size;
                // clean up previous
                if (zoxMesh.mesh)
                {
                    ObjectUtil.Destroy(zoxMesh.mesh);
                }
                if (!HasComponent<Crosshair>(e))
                {
                    if (zoxMesh.material)
                    {
                        var unityTexture = zoxMesh.material.GetTexture("_BaseMap");
                        if (unityTexture)
                        {
                            ObjectUtil.Destroy(unityTexture);
                        }
                    }
                }
                if (size2D.size.x != 0 && size2D.size.y != 0)
                {
                    zoxMesh.mesh = MeshUtilities.CreateQuadMesh(panelSize);
                }
                else
                {
                    zoxMesh.mesh = null;
                }
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
                if (HasComponent<HeaderLink>(e))
                {
                    PostUpdateCommands.AddComponent<HeaderUIDirty>(e);
                }
                if (HasComponent<TextureFrame>(e))
                {
                    PostUpdateCommands.AddComponent<GenerateTextureFrame>(e);
                }
            }).WithoutBurst().Run();
        }
    }
}