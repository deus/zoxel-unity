using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    //! Initializes a SubPanel materials.
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class SubPanelInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var subPanelMaterial = UIManager.instance.materials.subPanelMaterial;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<DestroyEntity>()
                .WithAll<InitializeEntity, SubPanel>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in Size2D size2D, in UIMeshData uiMeshData) =>
            {
                zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size2D.size, uiMeshData.horizontalAlignment, uiMeshData.verticalAlignment, uiMeshData.offset);
                zoxMesh.material = new UnityEngine.Material(subPanelMaterial);
                zoxMesh.layer = worldUILayer;
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
        }
    }
}