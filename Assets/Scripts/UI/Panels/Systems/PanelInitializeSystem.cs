using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    //! Initializes a PanelUI materials.
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class PanelInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var materials = UIManager.instance.materials;
            var panelMaterial = materials.panelMaterial;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<DestroyEntity, ScreenFader, Crosshair>()
                .WithAll<InitializeEntity, PanelUI>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in Size2D size2D) =>
            {
                if (HasComponent<RenderText>(e))
                {
                    zoxMesh.layer = worldUILayer;
                    zoxMesh.material = new UnityEngine.Material(panelMaterial);
                    PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
                    return;
                }
                zoxMesh.layer = worldUILayer;
                if (size2D.size.x != 0 && size2D.size.y != 0)
                {
                    zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size2D.size, 0, 0);
                }
                /*else
                {
                    UnityEngine.Debug.LogError("Size is 0 for panel UI.");
                }*/
                zoxMesh.material = new UnityEngine.Material(panelMaterial);
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
        }
    }
}