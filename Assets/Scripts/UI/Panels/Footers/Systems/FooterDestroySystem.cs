using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Using DestroyEntity event, also destroys a HeaderUI connected to a PanelUI.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
	public partial class FooterDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyEntity, FooterLink>()
                .ForEach((Entity e, int entityInQueryIndex, in FooterLink footerLink) =>
            {
                if (footerLink.footer.Index > 0) // HasComponent<UIElement>(footerLink.footer))
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, footerLink.footer);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}