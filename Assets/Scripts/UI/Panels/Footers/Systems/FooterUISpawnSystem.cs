using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Textures;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! Spawns a headerEntity ui for PanelUI's.
    /**
    *   \todo Spawn directly in UI Spawn Systems.
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class FooterUISpawnSystem : SystemBase
    {        
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity footerPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        public static void InitializePrefabs(EntityManager EntityManager)
        {
            if (FooterUISpawnSystem.footerPrefab.Index != 0 || UICoreSystem.labelFixedSizePrefab.Index == 0) //  || !EntityManager.Exists(UICoreSystem.labelFixedSizePrefab)
            {
                return;
            }
            var uiDatam = UIManager.instance.uiDatam;
            var headerStyle = uiDatam.headerStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var footerPrefab = EntityManager.Instantiate(UICoreSystem.labelFixedSizePrefab);
            FooterUISpawnSystem.footerPrefab = footerPrefab;
            EntityManager.AddComponent<Prefab>(footerPrefab);
            EntityManager.AddComponent<HeaderUI>(footerPrefab);
            UICoreSystem.RemoveChildComponents(EntityManager, footerPrefab);
            // Set new Style
            EntityManager.SetComponentData(footerPrefab, new MaterialBaseColor(headerStyle.color));
            EntityManager.SetComponentData(footerPrefab, new MaterialFrameColor(headerStyle.frameGenerationData.color));
            EntityManager.SetComponentData(footerPrefab, new TextureFrame(in headerStyle.frameGenerationData));
            UICoreSystem.SetRenderTextData(EntityManager, footerPrefab, headerStyle.textGenerationData,
                headerStyle.textColor, headerStyle.textOutlineColor, headerStyle.fontSize, headerStyle.textPadding);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            FooterUISpawnSystem.InitializePrefabs(EntityManager);
            var footerPrefab = FooterUISpawnSystem.footerPrefab;
            var headerStyle = UIManager.instance.uiDatam.headerStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, MeshUIDirty, GridUIDirty>()
                .WithAll<SpawnFooterUI>()
                .ForEach((Entity e, int entityInQueryIndex, in PanelUI panelUI, in Size2D size2D) =>
            {
                if (size2D.size.x == 0 || size2D.size.y == 0)
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<SpawnFooterUI>(entityInQueryIndex, e);
                FooterUISpawnSystem.SpawnFooterUI(PostUpdateCommands, entityInQueryIndex, footerPrefab, e, size2D.size, headerStyle.fontSize, headerStyle.textPadding);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static Entity SpawnFooterUI(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab,
            Entity panelEntity, float2 panelSize, float fontSize, float2 textPadding)
        {
            var size = new float2(panelSize.x, fontSize + textPadding.y * 2f);
            var position = new float3(0, -0.5f * panelSize.y - size.y / 2f, 0);
            var e2 = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, prefab, panelEntity, position, size);
            PostUpdateCommands.SetComponent(entityInQueryIndex, panelEntity, new FooterLink(e2));
            return e2;
        }

        public static Entity SpawnFooterUI(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab,
            Entity panelEntity, float2 panelSize, float footerHeight)
        {
            var size = new float2(panelSize.x, footerHeight);
            var position = new float3(0, -0.5f * panelSize.y - size.y / 2f, 0);
            var e2 = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, prefab, panelEntity, position, size);
            PostUpdateCommands.SetComponent(entityInQueryIndex, panelEntity, new FooterLink(e2));
            return e2;
        }

        public static Entity SpawnFooterUI(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab, Entity panelEntity)
        {
            var e2 = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, prefab, panelEntity);
            PostUpdateCommands.SetComponent(entityInQueryIndex, panelEntity, new FooterLink(e2));
            return e2;
        }
    }
}