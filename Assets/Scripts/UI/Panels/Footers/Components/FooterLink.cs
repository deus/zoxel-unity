using Unity.Entities;

namespace Zoxel.UI
{
    //! Links to a FooterUI.
    public struct FooterLink : IComponentData
    {
        public Entity footer;

        public FooterLink(Entity footer)
        {
            this.footer = footer;
        }
    }
}