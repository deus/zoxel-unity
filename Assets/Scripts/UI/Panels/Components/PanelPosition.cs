using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! Offsets a Panel based on anchor and size.
    public struct PanelPosition : IComponentData
    {
        public float3 position;

        public PanelPosition(float3 position)
        {
            this.position = position;
        }

        public float3 GetPositionOffset(float2 size, byte anchor)
        {
            return position + AnchorPresets.GetAnchorOffset(anchor, size);
        }

        public static float3 GetPosition(float2 size, byte anchor)
        {
            return AnchorPresets.GetAnchorOffset(anchor, size);
        }
    }
}