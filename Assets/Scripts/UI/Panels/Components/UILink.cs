using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.UI
{
    //! Links a player or character to ui entities.
    public struct UILink : IComponentData
    {
        public BlitableArray<Entity> uis;

        public void DisposeFinal()
        {
            uis.DisposeFinal();
        }

        public void Dispose()
        {
            uis.Dispose();
        }

        public void AddNew(byte newCount)
        {
            var newOnes = new BlitableArray<Entity>(uis.Length + newCount, Allocator.Persistent);
            for (int i = 0; i < uis.Length; i++)
            {
                newOnes[i] = uis[i];
            }
            for (int i = uis.Length; i < newOnes.Length; i++)
            {
                newOnes[i] = new Entity();
            }
            Dispose();
            uis = newOnes;
        }

        public void AddUI(Entity ui)
        {
            var uis2 = new BlitableArray<Entity>(uis.Length + 1, Allocator.Persistent);
            for (int i = 0; i < uis.Length; i++)
            {
                uis2[i] = uis[i];
            }
            uis2[uis.Length] = ui;
            Dispose();
            uis = uis2;
        }

        public Entity GetUI<T>(EntityManager EntityManager) where T : struct, IComponentData //  Nullable<T>
        {
            for (int i = 0; i < uis.Length; i++)
            {
                var ui = uis[i];
                if (EntityManager.Exists(ui) && EntityManager.HasComponent<T>(ui))
                {
                    return ui;
                }
            }
            return new Entity();
        }
    }
}