using Unity.Entities;

namespace Zoxel.UI // .Voxels.Interaction
{
    //! A tag for a SpawnChestUI event.
    public struct SpawnChestUI : IComponentData { }
}