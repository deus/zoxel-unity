using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! Position of a ui panel. The Z Value is used for depth.
    public struct UIPosition : IComponentData
    {
        public float3 position;

        public UIPosition(float depth)
        {
            this.position = new float3(0, 0, depth);
        }

        public UIPosition(float3 position)
        {
            this.position = position;
        }

        public float GetDepth()
        {
            return position.z;
        }
    }
}