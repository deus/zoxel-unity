using Unity.Entities;
// Used for orbiting, trailing and being attached to an object
// Attached to a UI Entity
// todo: replace with CharacterLink Uses for all UI

namespace Zoxel.UI
{
    public struct PanelAnchorLink : IComponentData
    {
        public Entity anchor;

        public PanelAnchorLink(Entity anchor)
        {
            this.anchor = anchor;
        }
    }
}