using Unity.Entities;

namespace Zoxel.UI
{
    //! An event that signals a ui spawned for an entity.
    public struct OnUISpawned : IComponentData
    {
        public Entity uiHolder;
        public byte spawned;

        public OnUISpawned(Entity uiHolder, byte spawned)
        {
            this.uiHolder = uiHolder;
            this.spawned = spawned;
        }
    }
}