using Unity.Entities;

namespace Zoxel.UI
{
    // offsets a ui position based on parents
    public struct UIAnchor : IComponentData
    {
        public byte anchor;

        public UIAnchor(byte anchor)
        {
            this.anchor = anchor;
        }
    }
}