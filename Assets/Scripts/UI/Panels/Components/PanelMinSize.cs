using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! Adds a minimum size for a UI.
    public struct PanelMinSize : IComponentData
    {
        public float2 minSize;

        public PanelMinSize(float2 minSize)
        {
            this.minSize = minSize;
        }
    }
}