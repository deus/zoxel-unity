using Unity.Entities;

namespace Zoxel.UI
{
    //! Links to a PanelUI.
    public struct PanelLink : IComponentData
    {
        public Entity panel;          // a pointer to the tooltip object

        public PanelLink(Entity panel)
        {
            this.panel = panel;
        }
    }
}