﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! Added to all UI groups that orbit a character
    /**
    *   \todo Move other properties out of here besides type. Type should be optional too.
    */
    public struct PanelUI : IComponentData
    {
        public byte panelType; // type, like what ui it is

        public PanelUI(PanelType panelType)
        {
            this.panelType = (byte) panelType;
        }
    }
}