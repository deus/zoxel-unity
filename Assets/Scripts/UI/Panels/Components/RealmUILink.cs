using Unity.Entities;

namespace Zoxel.UI
{
    //! Links to spawwned uis and index of Realm UI.
	public struct RealmUILink : IComponentData
    {
        public byte index;
        public Entity previousUI;
        public Entity previousUI2;
    }
}