using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Textures;

namespace Zoxel.UI
{
    //! Spawns a crosshair ui!
    /**
    *   - UI Spawn System -
    *   Use a base prefab from UICoreSystem and add crosshair components to it.
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class CrosshairSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private Entity crosshairPrefab;
        public static Entity spawnCrosshairPrefab;
        public static Entity spawnCrosshairInstantPrefab;
        public static float4 crosshairDefaultColor = new float4(0.6f, 0.6f, 0.6f, 1);
        public static float4 crosshairTargetColor = new float4(0.6f, 0.1f, 0.1f, 1);

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.ReadOnly<SpawnCrosshair>(),
                ComponentType.ReadOnly<CharacterLink>(),
                ComponentType.Exclude<DelayEvent>());
            var crosshairArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(GenerateCrosshairUI),
                typeof(Crosshair),
                typeof(CrosshairState),
                typeof(GameOnlyUI),
                typeof(NewCharacterUI),
                typeof(CharacterUI),
                // typeof(CharacterUIIndex),
                typeof(UIPosition),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(OrbitTransform),
                typeof(OrbitPosition),
                typeof(Size2D),
                typeof(CameraLink),
                typeof(CharacterLink),
                typeof(Texture),
                typeof(MaterialFrameColor),
                typeof(MaterialBaseColor),
                typeof(ZoxMesh),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld));
            crosshairPrefab = EntityManager.CreateEntity(crosshairArchetype);
            EntityManager.SetComponentData(crosshairPrefab, new MaterialBaseColor { Value = crosshairDefaultColor });
            EntityManager.SetComponentData(crosshairPrefab, new MaterialFrameColor { Value = new float4(0, 0, 0, 1) });
            EntityManager.SetComponentData(crosshairPrefab, new NonUniformScale { Value = new float3(1, 1, 1) } );
            EntityManager.SetComponentData(crosshairPrefab, new Rotation { Value = quaternion.identity } );
            var spawnCrosshairArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnCrosshair),
                typeof(CharacterLink),
                typeof(DelayEvent));
            spawnCrosshairPrefab = EntityManager.CreateEntity(spawnCrosshairArchetype);
            var spawnCrosshairInstantArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnCrosshair),
                typeof(CharacterLink));
            spawnCrosshairInstantPrefab = EntityManager.CreateEntity(spawnCrosshairInstantArchetype);
        }

        private void InitiliazePrefab()
        {
            if (this.crosshairPrefab.Index == 0)
            {
                // expand from panel ui basic - UICoreSystem
            }
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (processQuery.IsEmpty || UIManager.instance.uiSettings.disableCrosshair)
            {
                return;
            }
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            InitiliazePrefab();
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            // var panelDepth = CameraManager.instance.cameraSettings.uiDepth;
            var panelDepth = UIManager.instance.uiSettings.panelDepth;
            var crosshairSize = uiDatam.GetIconSize(uiScale) / 2f;
            var genericPadding = uiDatam.GetIconPadding(uiScale); // new float2(0.0006f, 0.0006f);
            var genericMargins = uiDatam.GetIconMargins(uiScale); // new float2(0.001f, 0.001f);
            var actionbarPanelColor = new Color(uiDatam.actionbarPanelColor);
            var elapsedTime = World.Time.ElapsedTime;
            var crosshairPrefab = this.crosshairPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var spawnCrosshairEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var characterLinks = GetComponentLookup<CharacterLink>(true);
            Dependency = Entities
                .ForEach((Entity e, int entityInQueryIndex, in UILink uiLink, in CameraLink cameraLink) =>
            {
                if (cameraLink.camera.Index == 0)
                {
                    return;
                }
                for (int i = 0; i < spawnCrosshairEntities.Length; i++)
                {
                    var e2 = spawnCrosshairEntities[i];
                    var characterLink = characterLinks[e2];
                    if (characterLink.character != e)
                    {
                        continue;
                    }
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e2);   // destroy spawn event
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));
                    // Spawn Crosshair
                    var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, crosshairPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CharacterLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CameraLink(cameraLink.camera));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new Size2D(crosshairSize));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new UIPosition(panelDepth));
                    break;  // only one crosshair per character atm
                }
            })  .WithReadOnly(spawnCrosshairEntities)
                .WithDisposeOnCompletion(spawnCrosshairEntities)
                .WithReadOnly(characterLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}