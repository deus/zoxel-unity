using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    //! Initializes a Crosshair materials.
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class CrosshairInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var materials = UIManager.instance.materials;
            var crosshairMaterial = materials.crosshairMaterial;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, Crosshair>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in Size2D size2D) =>
            {
                zoxMesh.layer = worldUILayer;
                if (size2D.size.x != 0 && size2D.size.y != 0)
                {
                    zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size2D.size, 0, 0);
                }
                zoxMesh.material = new UnityEngine.Material(crosshairMaterial);
                zoxMesh.material.SetTexture("_BaseMap", TextureUtil.CreateBlankTexture());
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
                #if UNITY_EDITOR
                if (!UnityEditor.EditorApplication.isPlaying && ZoxelEditorSettings.isEditorMeshes)
                {
                    PostUpdateCommands.AddComponent<InitializeEditorMesh>(e);
                    PostUpdateCommands.AddComponent<EditorMesh>(e);
                }
                #endif
            }).WithoutBurst().Run();
        }
    }
}