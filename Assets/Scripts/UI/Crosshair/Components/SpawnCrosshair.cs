using Unity.Entities;

namespace Zoxel.UI
{
    //! Spawns a crosshair ui on UIHolder entity.
    public struct SpawnCrosshair : IComponentData { }
}