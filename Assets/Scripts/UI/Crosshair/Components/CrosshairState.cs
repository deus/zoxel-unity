using Unity.Entities;

namespace Zoxel.UI
{
    public struct CrosshairState : IComponentData
    {
        public byte state;
    }
}