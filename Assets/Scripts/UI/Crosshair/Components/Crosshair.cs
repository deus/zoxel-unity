using Unity.Entities;

namespace Zoxel.UI
{
	//! A cross UI  to show where we are aiming.
	public struct Crosshair : IComponentData { }
}