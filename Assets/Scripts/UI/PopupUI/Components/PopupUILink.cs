using Unity.Entities;

namespace Zoxel.UI
{
    public struct PopupUILink : IComponentData
    {
        public Entity ui;

        public PopupUILink(Entity ui)
        {
            this.ui = ui;
        }
    }
}