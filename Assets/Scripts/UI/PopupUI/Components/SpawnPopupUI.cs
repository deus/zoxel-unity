using Unity.Entities;

namespace Zoxel.UI
{
    //! Spawns a generic PopupUI.
    public struct SpawnPopupUI : IComponentData
    {
        public Text header;
        public Text core;
        public Entity closeButtonPrefab;

        public SpawnPopupUI(Text header, Text core, Entity closeButtonPrefab)
        {
            this.header = header;
            this.core = core;
            this.closeButtonPrefab = closeButtonPrefab;
        }

        public void Dispose()
        {
            header.Dispose();
            core.Dispose();
        }
    }
}