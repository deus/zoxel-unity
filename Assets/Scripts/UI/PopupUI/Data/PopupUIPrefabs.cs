using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.UI
{
    public struct PopupUIPrefabs
    {
        public Entity panelPrefab;
        public Entity headerPrefab;
        public Entity buttonPrefab;
        public float3 closeButtonPosition;
        [ReadOnly] public Text closeButtonText;
    }
}