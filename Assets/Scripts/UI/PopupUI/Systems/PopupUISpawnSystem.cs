using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    //! Spawns a generic PopupUI.
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class PopupUISpawnSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;
        public static Entity spawnPanelPrefab;
        public static PopupUIPrefabs popupUIPrefabs;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            uiHoldersQuery = GetEntityQuery(
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<UILink>());
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnPopupUI),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnPanelPrefab = EntityManager.CreateEntity(spawnArchetype);
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            PopupUISpawnSystem.InitializePrefabs(EntityManager);
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;   // move to ui system group
            var popupUIPrefabs = PopupUISpawnSystem.popupUIPrefabs;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uiHolderEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
            uiHolderEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<SpawnPopupUI>()
                .ForEach((int entityInQueryIndex, in CharacterLink characterLink, in SpawnPopupUI spawnPopupUI) =>
            {
                PopupUISpawnSystem.SpawnPopupUI(PostUpdateCommands, entityInQueryIndex, in popupUIPrefabs, spawnPopupUI.closeButtonPrefab,
                    characterLink.character, in cameraLinks, uiSpawnedEventPrefab, spawnPopupUI.header, spawnPopupUI.core);
			})  .WithReadOnly(cameraLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static Entity SpawnPopupUI(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, in PopupUIPrefabs popupUIPrefabs,
            Entity closeButtonPrefab, Entity uiHolderEntity, in ComponentLookup<CameraLink> cameraLinks, Entity uiSpawnedEventPrefab,
            Text headerText, Text coreText)
        {
            var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, popupUIPrefabs.panelPrefab);
            var headerEntity = HeaderSpawnSystem.SpawnHeaderUI(PostUpdateCommands, entityInQueryIndex, popupUIPrefabs.headerPrefab, e3, in headerText);
            PostUpdateCommands.AddComponent(entityInQueryIndex, headerEntity, new PanelLink(e3));
            // create close button
            if (closeButtonPrefab.Index > 0)
            {
                var closeButtonEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, closeButtonPrefab, headerEntity);
                PostUpdateCommands.SetComponent(entityInQueryIndex, closeButtonEntity, new RenderText(popupUIPrefabs.closeButtonText.Clone()));
                PostUpdateCommands.SetComponent(entityInQueryIndex, closeButtonEntity, new PanelLink(e3));
                PostUpdateCommands.SetComponent(entityInQueryIndex, closeButtonEntity, new LocalPosition(popupUIPrefabs.closeButtonPosition));
                PostUpdateCommands.AddComponent<PopupButton>(entityInQueryIndex, closeButtonEntity);
            }
            // header children
            PostUpdateCommands.AddComponent<Childrens>(entityInQueryIndex, headerEntity);
            PostUpdateCommands.AddComponent(entityInQueryIndex, headerEntity, new OnChildrenSpawned((byte) 1));
            // Spawn texts
            var buttonEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, popupUIPrefabs.buttonPrefab, e3);
            PostUpdateCommands.SetComponent(entityInQueryIndex, buttonEntity, new RenderText(coreText));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawned((byte) 1));
            if (uiHolderEntity.Index > 0)
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CharacterLink(uiHolderEntity));
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab),
                    new OnUISpawned(uiHolderEntity, 1));
                if (cameraLinks.HasComponent(uiHolderEntity))
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLinks[uiHolderEntity]);
                }
            }
            return e3;
        }

        public static void InitializePrefabs(EntityManager EntityManager)
        {
            if (popupUIPrefabs.panelPrefab.Index != 0)
            {
                return;
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var buttonStyle = uiDatam.buttonStyle.GetStyle(uiScale);
            var headerStyle = uiDatam.headerStyle.GetStyle(uiScale);
            var buttonFontSize = buttonStyle.fontSize * 0.3f;
            var buttonTextPadding = buttonStyle.textPadding * 0.6f;
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            var padding = uiDatam.GetIconPadding(uiScale);
            var gridUI = new GridUI();
            gridUI.gridSize = new int2(1, 8); // texts.Length - 1);
            gridUI.margins = 0.2f * new float2(buttonStyle.fontSize, buttonStyle.fontSize);
            gridUI.padding = padding;
            var gridUISize = new GridUISize(new float2(buttonFontSize * 24 + buttonTextPadding.x * 2f, buttonFontSize + buttonTextPadding.y * 2f));
            var panelSize = gridUI.GetSize(gridUISize.size) * 1.6f;
            gridUI.gridSize.y = 1;
            panelSize.y *= 0.3f;
            // panel
            var panelPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(panelPrefab);
            EntityManager.RemoveComponent<SpawnHeaderUI>(panelPrefab);
            EntityManager.AddComponent<PopupUI>(panelPrefab);
            EntityManager.SetComponentData(panelPrefab, new PanelUI(PanelType.ChatUI));
            EntityManager.SetComponentData(panelPrefab, gridUI);
            EntityManager.SetComponentData(panelPrefab, gridUISize);
            EntityManager.SetComponentData(panelPrefab, new Size2D(panelSize));
            EntityManager.AddComponent<DisableGridResize>(panelPrefab);
            // header
            var headerHeight = headerStyle.fontSize + headerStyle.textPadding.y * 2f;
            var headerSize = new float2(panelSize.x, headerHeight);
            var headerPosition = new float3(0, 0.5f * panelSize.y + headerSize.y / 2f, 0);
            HeaderSpawnSystem.InitializePrefabs(EntityManager);
            var headerPrefab = EntityManager.Instantiate(HeaderSpawnSystem.headerPrefab);
            EntityManager.AddComponent<Prefab>(headerPrefab);
            EntityManager.SetComponentData(headerPrefab, new LocalPosition(headerPosition));
            EntityManager.SetComponentData(headerPrefab, new Size2D(headerSize));
            // close button
            // button
            var buttonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(buttonPrefab);
            UICoreSystem.SetRenderTextData(EntityManager, buttonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, buttonStyle.fontSize * 0.3f, buttonStyle.textPadding * 0.6f);
            popupUIPrefabs.panelPrefab = panelPrefab;
            popupUIPrefabs.headerPrefab = headerPrefab;
            popupUIPrefabs.buttonPrefab = buttonPrefab;
            popupUIPrefabs.closeButtonPosition = new float3(panelSize.x * 0.44f, 0, 0);
            popupUIPrefabs.closeButtonText = new Text("X");
        }

        protected override void OnDestroy()
        {
            popupUIPrefabs.closeButtonText.Dispose();
        }
    }
}