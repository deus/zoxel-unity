using Unity.Entities;
using Unity.Burst;
using Zoxel.Rendering;
using Zoxel.Audio;
using Zoxel.Textures;
// this is added to any UI with pre known size
//      For render texts that are sized based on text, this will be added after text is spawned

namespace Zoxel.UI
{
    //! Generate's a UIElement's mesh.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class UIMeshGenerateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var layermask = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<InitializeEntity>()
                .WithAll<GenerateMeshUI>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in Size2D size2D, in UIMeshData uiMeshData) =>
            {
                PostUpdateCommands.RemoveComponent<GenerateMeshUI>(e);
                // Destroy Previous
                if (zoxMesh.mesh != null)
                {
                    //! \todo Destroy Texture too
                    ObjectUtil.Destroy(zoxMesh.mesh);
                }
                zoxMesh.layer = layermask;
                zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size2D.size, uiMeshData.horizontalAlignment, uiMeshData.verticalAlignment, uiMeshData.offset);
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
        }   
    }
}