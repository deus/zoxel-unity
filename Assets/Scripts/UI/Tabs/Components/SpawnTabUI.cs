using Unity.Entities;

namespace Zoxel.UI
{
    public struct SpawnTabUI : IComponentData
    {
        public byte typeType;
        public SpawnTabUI(byte typeType)
        {
            this.typeType = typeType;
        }
    }
}