using Unity.Mathematics;

namespace Zoxel.UI
{
    public struct PanelStyle
    {
        // Frame data?
        // public int frameThickness;  // 1
        // public int resolution;  // 128

        // Default
        public Color color;
        public FrameGenerationData frameGenerationData;
        // public Color frameColor;
        // Selected - for later?
        public Color selectedColor;
        public Color selectedFrameColor;
    }
}