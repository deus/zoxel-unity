using UnityEngine;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! Holds some ui datam, colours, paddings, font sizes, etc.
    [CreateAssetMenu(fileName = "UIElementStyle", menuName = "ZoxelUI/UIElementStyle")]
    public partial class UIElementStyleDatam : ScriptableObject
    {
        [Header("Panel")]
        public UnityEngine.Color color;
        public float fadeTime;

        [Header("Panel Frame")]
        public UnityEngine.Color frameColor;
        public FrameGenerationData frameGenerationData;
        
        [Header("Text")]
        public float fontSize;
        public float2 textPadding;
        public UnityEngine.Color textColor;
        public UnityEngine.Color textOutlineColor;
        public TextGenerationData textGenerationData;

        [Header("Selected")]
        public UnityEngine.Color selectedColor;
        public UnityEngine.Color selectedFrameColor;
        public UnityEngine.Color selectedTextColor;
        public UnityEngine.Color selectedTextOutlineColor;

        public UIElementStyle GetStyle(float uiScale)
        {
            return new UIElementStyle
            {
                color = new Color(color),
                fadeTime = fadeTime,
                frameGenerationData = frameGenerationData.SetColor(frameColor),
                // font
                fontSize = fontSize * uiScale,
                textPadding = textPadding * uiScale,
                textGenerationData = textGenerationData,
                textColor = new Color(textColor),
                textOutlineColor = new Color(textOutlineColor),
                // Selected
                selectedColor = new Color(selectedColor),
                selectedFrameColor = new Color(selectedFrameColor),
                selectedTextColor = new Color(selectedTextColor),
                selectedTextOutlineColor = new Color(selectedTextOutlineColor)
            };
        }
    }
}