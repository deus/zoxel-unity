using UnityEngine;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! Holds some ui datam, colours, paddings, font sizes, etc.
    [CreateAssetMenu(fileName = "PanelStyle", menuName = "ZoxelUI/PanelStyle")]
    public partial class PanelStyleDatam : ScriptableObject
    {
        [Header("Panel")]
        public UnityEngine.Color color;

        [Header("Panel Frame")]
        public UnityEngine.Color frameColor;
        public FrameGenerationData frameGenerationData;

        [Header("Selected")]
        public UnityEngine.Color selectedColor;
        public UnityEngine.Color selectedFrameColor;

        public PanelStyle GetStyle() // float uiScale)
        {
            return new PanelStyle
            {
                color = new Color(color),
                frameGenerationData = frameGenerationData.SetColor(frameColor),
                // frameColor = new Color(frameColor),
                selectedColor = new Color(selectedColor),
                selectedFrameColor = new Color(selectedFrameColor)
            };
        }
    }
}