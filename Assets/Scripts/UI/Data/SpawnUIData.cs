using Unity.Mathematics;

namespace Zoxel.UI
{
    public struct SpawnUIData
    {
        public byte type;   // 0 button, 1 label, 2 input, 3 viewer
        public float size;
        public float3 position;
        public Text label;
    }
}