﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! Holds some ui datam, colours, paddings, font sizes, etc.
    /**
    *   \todo Store these properties in files based on UIElement. Link to RenderTextSettingsDatam's.
    */
    [CreateAssetMenu(fileName = "UIData", menuName = "ZoxelUI/UIData")]
    public partial class UIDatam : ScriptableObject
    {
        [Header("Generic")]
        public float2 genericPadding;
        public float2 genericMargins;

        [Header("Panels")]
        public PanelStyleDatam panelStyle;
        public PanelStyleDatam actionbarStyle;
        public UnityEngine.Color actionbarPanelColor;

        [Header("Headers")]
        public UIElementStyleDatam headerStyle;

        [Header("Button UI")]
        public UIElementStyleDatam mainMenuButtonStyle;
        public UIElementStyleDatam buttonStyle;
        public float2 textPadding;
        public UnityEngine.Color activatedTabColor;
        public UnityEngine.Color mapFrameColor;

        [Header("InputFields")]
        public UnityEngine.Color inputActivatedFillColor;
        public UnityEngine.Color inputActivatedFrameColor;

        [Header("SubPanel")]
        public PanelStyleDatam subPanelStyle;
        public PanelStyleDatam controlPanelStyle;

        [Header("Skilltree")]
        public UnityEngine.Color nodeBaseColor;
        public UnityEngine.Color unlockedNodeColor;
        public UnityEngine.Color canUnlockNodeColor;
        public UnityEngine.Color lockedNodeColor;
        
        [Header("Dialogue")]
        public UIElementStyleDatam dialogueSpeechStyle;
        public UIElementStyleDatam dialogueAnswerStyle;

        [Header("Quest Tracker")]
        public UIElementStyleDatam questTrackerStyle;
        public UnityEngine.Color questTrackerTextColor;
        public UnityEngine.Color questTrackerTextOutlineColor;
        public TextGenerationData questTrackerGenerationData;

        [Header("Name Bar")]
        public UIElementStyleDatam nameLabel;

        [Header("Weather UI")]
        public UIElementStyleDatam weatherUI;

        [Header("Realm Selection")]
        public Texture2D[] uiTabTextures;
        public UnityEngine.Color actionbarSelectedColor;

        [Header("MapUI")]
        public UnityEngine.Color arrowColor;
        public float playerIconMultiplier;

        [Header("Icons")]
        public int actionsCount = 10;
        public IconStyleDatam iconStyle;
        public float2 gridSize = new float2(3,3);
        public UIElementStyleDatam frameStyle;

        [Header("Obsolete Icons")]
        public UnityEngine.Color iconFillColor;
        public UnityEngine.Color iconFrameColor;
        public int iconFrameThickness;
        public float2 iconTextPadding;
        public FrameGenerationData iconFrameGenerationData;

        [Header("Sizes")]
        public float iconSize = 0.01f;  // 0.06
        public float crosshairSize =  0.006f;
        public float damagePopupSize = 0.24f;

        [Header("Popups")]
        public TextGenerationData popupTextData;
        public float2 popupLifetime; // 2, 3
        public float2 popupVariationX;// = new float2(-0.05f, 0.05f);
        public float2 popupVariationY;// = new float2(3, 4);
        public float2 popupVariationZ;// = new float2(-0.05f, 0.05f);

        [Header("Colors")]
        public UnityEngine.Color overlayTextColor;
        public UnityEngine.Color overlayTextOutlineColor;
        public UnityEngine.Color defaultFrameColor;
        public UnityEngine.Color selectedFrameColor;
        public UnityEngine.Color errorOutlineColor;

        [Header("Tooltips")]
        public float2 tooltipTextPadding;
        public UnityEngine.Color tooltipTextColor;
        public UnityEngine.Color tooltipTextOutlineColor;

        [Header("Actionbar")]
        public UnityEngine.Color actionOverlayColor;
        public UnityEngine.Color actionbarLabelColor;
        
        [Header("Statbars")]
        public float statbarFadeTime; // = 0.8f;   // 1
        public float actionLabelFadeTime; // = 0.8f;   // 1
        public float playerStatbarDivision; // 1.4 - 2.2 = Scale compared to icon size
        public float playerStatbarPadding;  // percentage of icon size
        public UnityEngine.Color healthColor;
        public UnityEngine.Color energyColor;
        public UnityEngine.Color manaColor;

        [Header("Statbar Panel")]
        public UnityEngine.Color healthPanelColor;
        public UnityEngine.Color energyPanelColor;
        public UnityEngine.Color manaPanelColor;
        public TextGenerationData statbarGenerationData;

        [Header("Statbar Text")]
        public UnityEngine.Color healthTextColor;
        public UnityEngine.Color energyTextColor;
        public UnityEngine.Color manaTextColor;
        public float2 statbarTextPadding;

        [Header("Experience Bar")]
        public UnityEngine.Color experienceColor;
        public UnityEngine.Color experiencePanelColor;
        public UnityEngine.Color experienceTextColor;
        public UnityEngine.Color experienceTextOutlineColor;
        public TextGenerationData experienceGenerationData;

        [Header("Titlescreen")]
        public float3 titlePosition = new float3(0, 0.05f, 0.1f);
        public float3 subTitlePosition = new float3(0, -0.1f,0);

        [Header("Player Devices")]
        public Texture2D[] playersConnected;
        public Texture2D[] playersDisconnected;
        public Texture2D[] devices;

        public float2 GetIconSize(float uiScale)
        {
            #if UNITY_ANDROID
            return new float2(iconSize, iconSize) * uiScale * 1.4f;
            #endif
            return new float2(iconSize, iconSize) * uiScale;
        }

        public float2 GetIconMargins(float uiScale)
        {
            return genericMargins * uiScale;
        }

        public float2 GetIconPadding(float uiScale)
        {
            return genericPadding * uiScale;
        }

        public float2 GetNewGameMapCellSize(float uiScale)
        {
            return uiScale * (new float2(iconSize, iconSize)) * 0.8f;
        }

        public float2 GetMenuPaddings(float uiScale)
        {
            return textPadding * uiScale;
        }

        public float2 GetMenuMargins(float uiScale)
        {
            return textPadding * uiScale;
        }

        public float GetPlayerStatbarPadding(float uiScale)
        {
            return iconSize * uiScale * playerStatbarPadding;
        }

        /*public float2 GetHeaderPadding()
        {
            return headerTextPadding * uiScale;
        }*/

        /*public float GetHeaderSize()
        {
            return uiScale * headerFontSize;
        }*/
    }
}