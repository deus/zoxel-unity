using Unity.Mathematics;

namespace Zoxel.UI
{
    //! UI styles!
    public struct UIElementStyle
    {
        // Panel
        public Color color;
        public float fadeTime;

        // Panel Frame
        public Color frameColor
        {
            get
            {
                return frameGenerationData.color;
            }
        }
        public FrameGenerationData frameGenerationData;

        // Text
        public float fontSize;
        public float2 textPadding;
        public Color textColor;
        public Color textOutlineColor;
        public TextGenerationData textGenerationData;

        // Selected
        public Color selectedColor;
        public Color selectedFrameColor;
        public Color selectedTextColor;
        public Color selectedTextOutlineColor;
    }
}