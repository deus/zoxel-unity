using Unity.Entities;

namespace Zoxel.UI
{
    //! Links to a ScreenFader UI.
    public struct ScreenFaderLink : IComponentData
    {
        public Entity screenFader;

        public ScreenFaderLink(Entity screenFader) 
        {
            this.screenFader = screenFader;
        }
    }
}
