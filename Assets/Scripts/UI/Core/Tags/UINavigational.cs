using Unity.Entities;

namespace Zoxel.UI
{
    //! A tag for if mouse can navigate the UI.
    public struct UINavigational : IComponentData { }
}