using Unity.Entities;
namespace Zoxel.UI
{
    //! A tag ofr a UIElement that is invisible.
    public struct InvisibleUI : IComponentData { }
}