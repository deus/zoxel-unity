using Unity.Entities;

namespace Zoxel.UI
{
    //! This is a tag for ui labels.
    public struct Label : IComponentData { }
}