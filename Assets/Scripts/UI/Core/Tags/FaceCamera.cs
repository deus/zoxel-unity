using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
	//! A ui entity will face the camera.
	public struct FaceCamera : IComponentData { }
}

//! Offset the UI while facing the camera.
// public float3 position;