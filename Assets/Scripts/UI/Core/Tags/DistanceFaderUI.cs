using Unity.Entities;

namespace Zoxel.UI
{
    //! Makes a UI fade away based on the distance.
    public struct DistanceFaderUI : IComponentData { }
}