﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! Used for statbars atm.
    public struct PercentageUI : IComponentData
    {
        public float percentage;
        public float targetPercentage;
        public float originalPercentage;
        public float lerpSpeed;
        public double timePassed;
        public double animationTime;   
        public float2 size;
        public float edgeSpace;

        public void SetTargetPercentage(float newPercentage)
        {
            if (targetPercentage != newPercentage)
            {
                targetPercentage = newPercentage;
                timePassed = 0;
                originalPercentage = percentage;
                lerpSpeed = math.abs(targetPercentage - percentage);
            }
        }

        public void UpdatePercentage(double delta)
        {
            //var timePassed = time - setTime;
            timePassed += delta;
            if (lerpSpeed == 0)
            {
                percentage = targetPercentage;
            }
            else
            {
                var lerpValue = (float)(timePassed) / lerpSpeed;
                if (lerpValue > 1)
                {
                    lerpValue = 1;
                }
                percentage = math.lerp(originalPercentage, targetPercentage, lerpValue);
            }
        }
    }
}