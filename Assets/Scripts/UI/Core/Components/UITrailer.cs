﻿using Unity.Entities;

namespace Zoxel.UI
{
    //! A ui will follow on top of a character.
    public struct TrailerUI : IComponentData
    {
        public float heightAddition;

        public TrailerUI(float heightAddition)
        {
            this.heightAddition = heightAddition;
        }
    }
}