using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! UIMesh information.
    public struct UIMeshData : IComponentData
    {
        //! mesh offset? shouldn't i just adjust position
        public float3 offset;
        //! Vertical Alignment used for Mesh creation.
        public byte verticalAlignment;
        //! Horizontal Alignment used for Mesh creation.
        public byte horizontalAlignment;
    }
}