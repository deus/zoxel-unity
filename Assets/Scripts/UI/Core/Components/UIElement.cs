using Unity.Entities;

namespace Zoxel.UI
{
    //! A tag for UI elements.
    public struct UIElement : IComponentData { }
}