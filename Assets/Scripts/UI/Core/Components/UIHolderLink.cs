using Unity.Entities;

namespace Zoxel.UI
{
    //! A link for a holder of UIs.
    public struct UIHolderLink : IComponentData
    {
        public Entity holder;

        public UIHolderLink(Entity holder)
        {
            this.holder = holder;
        }
    }
}