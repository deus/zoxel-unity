using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;
using Zoxel.Textures;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.UI
{
    //! Holds UI Prefabs used by others.
    /**
    *   \todo Consolidate prefabs. Move functions into a Utility script. Move Prefabs to UISystemGroup.
    */
    [AlwaysUpdateSystem]
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class UICoreSystem : SystemBase
    {
        private static bool hasInitializedIconPrefabs;
        public const int panelQueue = 3001;
        public const int subPanelQueue = 3002;
        public const int buttonQueue = 3003;
        public const int iconQueue = 3004;
        public const int iconLabelQueue = 3005;
        public const int zigelQueue = 3008;
        public static NativeList<Text> controlsLabels;
        public static Entity realmPanelPrefab;
        public static Entity letterPrefab;
        public static Entity emptyLetterPrefab;
        public static Entity fadedLetterPrefab;
        public static Entity fadedEmptyLetterPrefab;
        public static Entity textlessButtonPrefab;
        public static Entity buttonPrefab;
        public static Entity togglePrefab;
        public static Entity inputPrefab;
        public static Entity labelPrefab;
        public static Entity labelNoBackgroundPrefab;
        public static Entity labelFixedSizePrefab;
        public static Entity viewerPrefab;
        public static Entity viewerGroupPrefab;
        public static Entity panelPrefab;
        public static Entity closeButtonPrefab;
        public static Entity subPanelPrefab;
        public static Entity subPanelGridPrefab;
        public static Entity toggleGroupPrefab;
        public static IconPrefabs iconPrefabs;
        public static Entity iconOverlayPrefab;
        public static Entity invisibleIconFramePrefab;
        public static Entity spawnCharacterUIPrefab;
        public static Entity removeCharacterUIPrefab;
        public static Entity playerTooltipPrefab;
        public static Entity controlPanelPrefab;
        public static Entity toastUIPrefab;
        public static Entity controlPanelFooterPrefab;
        public static Entity controlButtonPrefab;

        protected override void OnDestroy()
        {
            for (int i = 0; i < controlsLabels.Length; i++)
            {
                controlsLabels[i].Dispose();
            }
            controlsLabels.Dispose();
        }

        /*public static void SpawnCharacterUI(EntityCommandBuffer PostUpdateCommands, Entity e, PanelType panelType, double timeStarted = 0, double timeDelay = 0)
        {
            var spawnCharacter = PostUpdateCommands.Instantiate(UICoreSystem.spawnCharacterUIPrefab);
            PostUpdateCommands.SetComponent(spawnCharacter, new SpawnCharacterUI(e, panelType));
            PostUpdateCommands.SetComponent(spawnCharacter, new DelayEvent(timeStarted, timeDelay));
        }

        public static void SpawnCharacterUI(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity spawnCharacterUIPrefab,
            Entity e, PanelType panelType, double timeStarted = 0, double timeDelay = 0)
        {
            var spawnCharacter = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnCharacterUIPrefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, spawnCharacter, new SpawnCharacterUI(e, panelType));
            PostUpdateCommands.SetComponent(entityInQueryIndex, spawnCharacter, new DelayEvent(timeStarted, timeDelay));
        }*/

        public static void RemoveUI(EntityCommandBuffer PostUpdateCommands, Entity character, Entity ui,
            double timeDelay = 0, double timeStarted = 0)
        {
            var removeEntity = PostUpdateCommands.Instantiate(UICoreSystem.removeCharacterUIPrefab);
            PostUpdateCommands.SetComponent(removeEntity, new RemoveUI(character, ui));
            PostUpdateCommands.SetComponent(removeEntity, new DelayEvent(timeStarted, timeDelay));
        }

        public static void RemoveUI(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity removeCharacterUIPrefab,
            Entity character, Entity ui,
            double timeDelay = 0, double timeStarted = 0)
        {
            var removeEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, removeCharacterUIPrefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, removeEntity, new RemoveUI(character, ui));
            PostUpdateCommands.SetComponent(entityInQueryIndex, removeEntity, new DelayEvent(timeStarted, timeDelay));
        }

        protected override void OnCreate()
        {
            controlsLabels = new NativeList<Text>(Allocator.Persistent);
            controlsLabels.Add(new Text("<"));
            controlsLabels.Add(new Text("-"));
            controlsLabels.Add(new Text("+"));
            controlsLabels.Add(new Text(">"));
            controlsLabels.Add(new Text("↓"));
            controlsLabels.Add(new Text("↑"));
            // UICoreSystem.controlsLabels = controlsLabelsList.ToArray(Allocator.Persistent);
            // variables
            var textureFrame = new TextureFrame
            {
                frameThickness = 1,
                fillColor = new Color(255, 255, 255, 255),
                frameColor = new Color(0, 0, 0, 255),
                resolution = 64
            };
            var oneScale = new NonUniformScale { Value = new float3(1, 1, 1) };
            var identityRotation = new Rotation { Value = quaternion.identity };
            var whiteMaterialColor = new MaterialBaseColor(new float4(1, 1, 1, 1));
            var blackMaterialFrameColor = new MaterialFrameColor(new float4(0, 0, 0, 1));
            var removeCharacterUIArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(RemoveUI),
                typeof(DelayEvent));
            removeCharacterUIPrefab = EntityManager.CreateEntity(removeCharacterUIArchetype);
            var letterArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(InitializeZigel),
                typeof(NewZigel),
                typeof(Zigel),
                typeof(Texture),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(Seed),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(LocalScale),
                typeof(ZoxMesh),
                typeof(RenderQueue),
                typeof(MaterialBaseColor),
                typeof(MaterialFrameColor),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            letterPrefab = EntityManager.CreateEntity(letterArchetype);
            EntityManager.SetComponentData(letterPrefab, new RenderQueue(zigelQueue));    // 3006
            EntityManager.SetComponentData(letterPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(letterPrefab, LocalScale.One);
            #if UNITY_EDITOR
            // EntityManager.AddComponent<EditorName>(letterPrefab);
            // EntityManager.AddComponentData(letterPrefab, new EditorName("[letter]"));
            #endif
            emptyLetterPrefab = EntityManager.Instantiate(letterPrefab);
            EntityManager.AddComponent<Prefab>(emptyLetterPrefab);
            EntityManager.AddComponent<EmptyZigel>(emptyLetterPrefab);
            fadedLetterPrefab = EntityManager.Instantiate(letterPrefab);
            EntityManager.AddComponent<Prefab>(fadedLetterPrefab);
            EntityManager.AddComponent<Fader>(fadedLetterPrefab);
            EntityManager.AddComponent<DisableFader>(fadedLetterPrefab);
            fadedEmptyLetterPrefab = EntityManager.Instantiate(letterPrefab);
            EntityManager.AddComponent<Prefab>(fadedEmptyLetterPrefab);
            EntityManager.AddComponent<EmptyZigel>(fadedEmptyLetterPrefab);
            EntityManager.AddComponent<Fader>(fadedEmptyLetterPrefab);
            EntityManager.AddComponent<FaderInvisible>(fadedEmptyLetterPrefab);
            EntityManager.AddComponent<DisableFader>(fadedEmptyLetterPrefab);
            var panelArchetype = EntityManager.CreateArchetype(
                typeof(CharacterUI),
                typeof(Prefab),
                typeof(NewCharacterUI),
                typeof(InitializeEntity),
                typeof(GenerateTextureFrame),
                typeof(CharacterLink),
                typeof(CameraLink),
                typeof(Childrens), 
                typeof(PanelUI),
                typeof(UIAnchor),
                typeof(UIPosition),
                typeof(PanelTooltipLink),
                typeof(HeaderLink),
                typeof(FooterLink),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(OrbitTransform),
                typeof(OrbitPosition),
                typeof(GridUI),
                typeof(GridUISize),
                typeof(ZoxMesh),
                typeof(RenderQueue),
                typeof(MaterialBaseColor),
                typeof(MaterialFrameColor),
                typeof(Texture),
                typeof(TextureFrame),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld));
            panelPrefab = EntityManager.CreateEntity(panelArchetype);
            EntityManager.SetComponentData(panelPrefab, whiteMaterialColor);
            EntityManager.SetComponentData(panelPrefab, blackMaterialFrameColor);
            EntityManager.SetComponentData(panelPrefab, new RenderQueue(panelQueue));
            EntityManager.SetComponentData(panelPrefab, oneScale);
            EntityManager.SetComponentData(panelPrefab, identityRotation);
            // todo: generate interesting texture for this UI rect
            var subPanelArchetype = EntityManager.CreateArchetype(
                typeof(SubPanel),
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(GenerateTextureFrame),
                typeof(TextureFrame),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),      // size, position?
                typeof(PanelLink),
                typeof(Texture),
                typeof(ZoxMesh),
                typeof(RenderQueue),
                typeof(MaterialBaseColor),
                typeof(MaterialFrameColor),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(Childrens),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            subPanelPrefab = EntityManager.CreateEntity(subPanelArchetype);
            EntityManager.SetComponentData(subPanelPrefab, whiteMaterialColor);
            EntityManager.SetComponentData(subPanelPrefab, new RenderQueue(subPanelQueue));
            EntityManager.SetComponentData(subPanelPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(subPanelPrefab, oneScale);
            subPanelGridPrefab = EntityManager.CreateEntity(subPanelArchetype);
            EntityManager.SetComponentData(subPanelGridPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(subPanelGridPrefab, oneScale);
            EntityManager.AddComponent<GridUI>(subPanelGridPrefab);
            EntityManager.AddComponent<GridUISize>(subPanelGridPrefab);
            var textlessButtonArchetype = EntityManager.CreateArchetype(
                typeof(Button),
                typeof(Prefab),
                typeof(InitializeEntity),
                // typeof(InitializeButtonColors),
                typeof(GenerateTextureFrame),
                typeof(GenerateSound),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(UINavigational),
                typeof(NavigationVisuals),
                typeof(Texture),
                typeof(TextureFrame),
                typeof(ZoxMesh),
                typeof(RenderQueue),
                typeof(MaterialBaseColor),
                typeof(MaterialFrameColor),
                typeof(Seed),
                typeof(Sound),
                typeof(SoundData),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(PanelLink),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            textlessButtonPrefab = EntityManager.CreateEntity(textlessButtonArchetype);
            EntityManager.SetComponentData(textlessButtonPrefab, textureFrame);
            EntityManager.SetComponentData(textlessButtonPrefab, new RenderQueue(buttonQueue));
            EntityManager.SetComponentData(textlessButtonPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(textlessButtonPrefab, identityRotation);
            EntityManager.SetComponentData(textlessButtonPrefab, oneScale);
            var buttonArchetype = EntityManager.CreateArchetype(
                typeof(Button),
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(InitializeButtonColors),
                typeof(RenderTextDirty),
                typeof(GenerateTextureFrame),
                typeof(GenerateSound),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(RenderText),
                typeof(RenderTextData),
                typeof(RenderTextResizePanel),
                typeof(UINavigational),
                typeof(NavigationVisuals),
                typeof(Texture),
                typeof(TextureFrame),
                typeof(ZoxMesh),
                typeof(RenderQueue),
                typeof(MaterialBaseColor),
                typeof(MaterialFrameColor),
                typeof(Seed),
                typeof(Sound),
                typeof(SoundData),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(PanelLink),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            buttonPrefab = EntityManager.CreateEntity(buttonArchetype);
            EntityManager.SetComponentData(buttonPrefab, textureFrame);
            EntityManager.SetComponentData(buttonPrefab, new RenderQueue(buttonQueue));
            EntityManager.SetComponentData(buttonPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(buttonPrefab, identityRotation);
            EntityManager.SetComponentData(buttonPrefab, oneScale);
            var inputArchetype = EntityManager.CreateArchetype(
                typeof(InputField),
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(InitializeButtonColors),
                typeof(RenderTextDirty),
                typeof(GenerateTextureFrame),
                typeof(GenerateSound),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(RenderText),
                typeof(RenderTextData),
                typeof(RenderTextResizePanel),
                typeof(UINavigational),
                typeof(NavigationVisuals),
                typeof(ZoxMesh),
                typeof(RenderQueue),
                typeof(MaterialBaseColor),
                typeof(MaterialFrameColor),
                typeof(Texture),
                typeof(TextureFrame),
                typeof(Seed),
                typeof(Sound),
                typeof(SoundData),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(PanelLink),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            inputPrefab = EntityManager.CreateEntity(inputArchetype);
            EntityManager.SetComponentData(inputPrefab, textureFrame);
            EntityManager.SetComponentData(inputPrefab, new RenderQueue(buttonQueue));
            EntityManager.SetComponentData(inputPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(inputPrefab, oneScale);
            EntityManager.SetComponentData(inputPrefab, identityRotation);
            var labelArchetype = EntityManager.CreateArchetype(
                typeof(Label),
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(InitializeButtonColors),
                typeof(GenerateTextureFrame),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(Seed),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(RenderTextDirty),
                typeof(RenderText),
                typeof(RenderTextData),
                typeof(RenderTextResizePanel),
                typeof(UINavigational),
                typeof(NavigationVisuals),
                typeof(ZoxMesh),
                typeof(RenderQueue),
                typeof(MaterialBaseColor),
                typeof(MaterialFrameColor),
                typeof(Texture),
                typeof(TextureFrame),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(ParentLink),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            labelPrefab = EntityManager.CreateEntity(labelArchetype);
            EntityManager.SetComponentData(labelPrefab, textureFrame);
            EntityManager.SetComponentData(labelPrefab, new RenderQueue(buttonQueue));
            EntityManager.SetComponentData(labelPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(labelPrefab, oneScale);
            EntityManager.SetComponentData(labelPrefab, identityRotation);
            var labelNoBackgroundArchetype = EntityManager.CreateArchetype(
                typeof(Label),
                typeof(Prefab),
                typeof(RenderTextDirty),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(RenderText),
                typeof(RenderTextData),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(ParentLink),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            labelNoBackgroundPrefab = EntityManager.CreateEntity(labelNoBackgroundArchetype);
            EntityManager.SetComponentData(labelNoBackgroundPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(labelNoBackgroundPrefab, oneScale);
            EntityManager.SetComponentData(labelNoBackgroundPrefab, identityRotation);
            var viewerUIArchetype = EntityManager.CreateArchetype(
                typeof(ViewerUI),
                typeof(Prefab),
                typeof(GenerateMeshUI), // do i need this
                typeof(InitializeEntity),
                typeof(ViewerCameraLink),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(ZoxMesh),
                typeof(RenderQueue),
                typeof(MaterialBaseColor),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            viewerPrefab = EntityManager.CreateEntity(viewerUIArchetype);
            EntityManager.SetComponentData(viewerPrefab, new RenderQueue(buttonQueue));
            EntityManager.SetComponentData(viewerPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(viewerPrefab, oneScale);
            EntityManager.SetComponentData(viewerPrefab, identityRotation);
            // viewer UIs
            var viewerGroupArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(Childrens),      // for all planet pieces
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(GridUI),             // adding grid to non panel might cause issues
                typeof(GridUISize),
                typeof(ZoxMesh),
                typeof(MaterialBaseColor),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            viewerGroupPrefab = EntityManager.CreateEntity(viewerGroupArchetype);
            EntityManager.SetComponentData(viewerGroupPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(viewerGroupPrefab, oneScale);
            var playerTooltipArchetype = EntityManager.CreateArchetype(
                typeof(PlayerTooltip),
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(PanelUI),
                typeof(UIAnchor),
                typeof(UIPosition),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(OrbitTransform),
                typeof(OrbitPosition),
                typeof(RenderText),
                typeof(RenderTextData),
                typeof(RenderTextResizePanel),
                typeof(CharacterLink),
                typeof(CameraLink),
                typeof(ZoxMesh),
                typeof(RenderQueue),
                typeof(MaterialBaseColor),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            playerTooltipPrefab = EntityManager.CreateEntity(playerTooltipArchetype);
            EntityManager.SetComponentData(playerTooltipPrefab, new RenderQueue(buttonQueue));
            EntityManager.SetComponentData(playerTooltipPrefab, identityRotation);
            EntityManager.SetComponentData(playerTooltipPrefab, oneScale);
        }

        protected override void OnUpdate()
        {
            //base.OnUpdate();
            InitializeIconPrefabs();
        }

        private void InitializeIconPrefabs()
        {
            if (iconPrefabs.frame.Index != 0 || UIManager.instance == null)
            {
                return;
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var oneScale = new NonUniformScale { Value = new float3(1, 1, 1) };
            var identityRotation = new Rotation { Value = quaternion.identity };
            var whiteMaterialColor = new MaterialBaseColor(new float4(1, 1, 1, 1));
            var blackMaterialFrameColor = new MaterialFrameColor(new float4(0, 0, 0, 1));
            var iconFrameArchetype = EntityManager.CreateArchetype(
                typeof(IconFrame),
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(InitializeButtonColors), //! \todo Set this in prefab instead
                typeof(GenerateSound),
                typeof(GenerateTextureFrame),
                typeof(TextureFrame),
                typeof(OnChildrenSpawned),
                typeof(IconData),
                typeof(PanelLink),
                typeof(Button),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(UINavigational),
                typeof(NavigationVisuals),
                typeof(Seed),
                typeof(Sound),
                typeof(SoundData),
                typeof(ZoxMesh),
                typeof(RenderQueue),
                typeof(Texture),
                typeof(MaterialBaseColor),
                typeof(MaterialFrameColor),
                typeof(Childrens),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            iconPrefabs.frame = EntityManager.CreateEntity(iconFrameArchetype);
            EntityManager.SetComponentData(iconPrefabs.frame, new RenderQueue(buttonQueue));
            EntityManager.SetComponentData(iconPrefabs.frame, LocalRotation.Identity);
            EntityManager.SetComponentData(iconPrefabs.frame, oneScale);
            EntityManager.SetComponentData(iconPrefabs.frame, identityRotation);
            var invisibleIconFrameArchetype = EntityManager.CreateArchetype(
                typeof(IconFrame),
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(OnChildrenSpawned),
                typeof(IconData),
                typeof(PanelLink),
                typeof(Button),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(Childrens),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            invisibleIconFramePrefab = EntityManager.CreateEntity(invisibleIconFrameArchetype);
            EntityManager.SetComponentData(invisibleIconFramePrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(invisibleIconFramePrefab, oneScale);
            EntityManager.SetComponentData(invisibleIconFramePrefab, identityRotation);
            var iconArchetype = EntityManager.CreateArchetype(
                typeof(Icon),
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(Texture),
                typeof(ZoxMesh),
                typeof(RenderQueue),
                typeof(MaterialBaseColor),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            iconPrefabs.icon = EntityManager.CreateEntity(iconArchetype);
            EntityManager.SetComponentData(iconPrefabs.icon, new RenderQueue(iconQueue));
            EntityManager.SetComponentData(iconPrefabs.icon, LocalRotation.Identity);
            EntityManager.SetComponentData(iconPrefabs.icon, oneScale);
            EntityManager.SetComponentData(iconPrefabs.icon, identityRotation);
            EntityManager.SetComponentData(iconPrefabs.icon, whiteMaterialColor);
            // this doesn't use background
            var iconTextArchetype = EntityManager.CreateArchetype(
                // typeof(IconLabel),
                typeof(Prefab),
                typeof(RenderTextDirty),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(RenderQueue),
                typeof(Size2D),
                typeof(RenderText),
                typeof(RenderTextData),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            iconPrefabs.label = EntityManager.CreateEntity(iconTextArchetype);
            EntityManager.SetComponentData(iconPrefabs.label, LocalRotation.Identity);
            EntityManager.SetComponentData(iconPrefabs.label, oneScale);
            EntityManager.SetComponentData(iconPrefabs.label, new RenderQueue(UICoreSystem.iconLabelQueue));

            // Set data
            var iconStyle = uiDatam.iconStyle.GetStyle(uiScale);
            EntityManager.SetComponentData(iconPrefabs.frame, new TextureFrame
            {
                resolution = iconStyle.frameGenerationData.resolution,
                frameThickness = iconStyle.frameGenerationData.thickness,
                fillColor = new Color(255, 255, 255, 255),
                frameColor = new Color(0, 0, 0, 255)
            });
            EntityManager.SetComponentData(iconPrefabs.frame, new Size2D(iconStyle.iconSize));
            EntityManager.SetComponentData(iconPrefabs.frame, new MaterialFrameColor { Value = iconStyle.frameColor.ToFloat4() });
            // EntityManager.AddComponent<ActionUI>(iconPrefabs.frame);
            EntityManager.AddComponent<SetIconData>(iconPrefabs.frame);
            EntityManager.SetComponentData(iconPrefabs.frame, new OnChildrenSpawned(2));
            // invisibleIconFramePrefab
            /*EntityManager.SetComponentData(invisibleIconFramePrefab, new TextureFrame
            {
                resolution = iconStyle.frameGenerationData.resolution,
                frameThickness = iconStyle.frameGenerationData.thickness,
                fillColor = new Color(255, 255, 255, 255),
                frameColor = new Color(0, 0, 0, 255)
            });*/
            EntityManager.SetComponentData(invisibleIconFramePrefab, new Size2D(iconStyle.iconSize));
            // EntityManager.SetComponentData(iconPrefabs.frame, new MaterialFrameColor { Value = iconStyle.frameColor.ToFloat4() });
            // EntityManager.AddComponent<ActionUI>(iconPrefabs.frame);
            // EntityManager.AddComponent<SetIconData>(iconPrefabs.frame);
            EntityManager.SetComponentData(invisibleIconFramePrefab, new OnChildrenSpawned(2));
            
            // EntityManager.AddComponent<DisableUIRaycast>(iconPrefabs.frame);
            // Icon
            // EntityManager.SetComponentData(iconPrefabs.icon, new Size2D(iconStyle.iconSize * 0.84f));
            // EntityManager.AddComponent<ActionIcon>(iconPrefabs.icon);
            // Label
            UICoreSystem.SetRenderTextData(EntityManager, iconPrefabs.label, iconStyle.textGenerationData,
                iconStyle.textColor, iconStyle.textOutlineColor, iconStyle.fontSize, iconStyle.textPadding,
                iconStyle.labelAlignment, iconStyle.iconSize.x / 2f, iconStyle.fontSize);
            // EntityManager.AddComponent<Seed>(iconPrefabs.frame);
            EntityManager.SetComponentData(iconPrefabs.frame, new TextureFrame
            {
                resolution = iconStyle.frameGenerationData.resolution,
                frameThickness = iconStyle.frameGenerationData.thickness,
                fillColor = new Color(255, 255, 255, 255),
                frameColor = new Color(0, 0, 0, 255)
            });
            EntityManager.SetComponentData(iconPrefabs.frame, new Size2D(iconStyle.iconSize));
            EntityManager.SetComponentData(iconPrefabs.frame, new MaterialFrameColor(iconStyle.frameColor));
            // EntityManager.AddComponent<SetIconData>(iconPrefabs.frame);
            // EntityManager.SetComponentData(iconPrefabs.frame, new OnChildrenSpawned(2));
            // Icon
            EntityManager.SetComponentData(iconPrefabs.icon, new Size2D(iconStyle.iconSize * 0.84f));
            EntityManager.SetComponentData(iconPrefabs.icon, new Zoxel.Transforms.Child(0));
            // Label
            var labelPosition = new float3(iconStyle.fontSize / 4f, -(iconStyle.iconSize.y / 2) + (iconStyle.fontSize / 2f), 0);
            UICoreSystem.SetRenderTextData(EntityManager, iconPrefabs.label, iconStyle.textGenerationData,
                iconStyle.textColor, iconStyle.textOutlineColor, iconStyle.fontSize, iconStyle.textPadding,
                iconStyle.labelAlignment, iconStyle.iconSize.x / 2f, iconStyle.fontSize);
            EntityManager.SetComponentData(iconPrefabs.label, new Zoxel.Transforms.Child(1));
            EntityManager.SetComponentData(iconPrefabs.label, new LocalPosition(labelPosition));
            // realmPanelPrefab expand from panel ui basic - UICoreSystem
            var uiDepth = UIManager.instance.uiSettings.uiDepth;
            var panelStyle = uiDatam.panelStyle.GetStyle();
            var gridSize = new int2(6, 6);
            var iconSize = uiDatam.GetIconSize(uiScale);
            var padding = uiDatam.GetIconPadding(uiScale); // iconSize / 6f;
            var margins = uiDatam.GetIconMargins(uiScale); // iconSize / 4f;
            realmPanelPrefab = EntityManager.Instantiate(panelPrefab);
            EntityManager.AddComponent<Prefab>(realmPanelPrefab);
            EntityManager.AddComponent<SpawnPanelTooltip>(realmPanelPrefab);
            EntityManager.AddComponent<OnChildrenSpawned>(realmPanelPrefab);
            EntityManager.AddComponent<SpawnHeaderUI>(realmPanelPrefab);
            EntityManager.AddComponent<GridUIDirty>(realmPanelPrefab);
            EntityManager.AddComponent<NavigationDirty>(realmPanelPrefab);
            // Set data too
            EntityManager.SetComponentData(realmPanelPrefab, new UIPosition(uiDepth));
            EntityManager.SetComponentData(realmPanelPrefab, new UIAnchor(AnchorUIType.Middle));
            EntityManager.SetComponentData(realmPanelPrefab, new GridUI(gridSize, padding, margins));
            EntityManager.SetComponentData(realmPanelPrefab, new GridUISize(iconSize));
            EntityManager.SetComponentData(realmPanelPrefab, new MaterialBaseColor(panelStyle.color));
            EntityManager.SetComponentData(realmPanelPrefab, new MaterialFrameColor(panelStyle.frameGenerationData.color));
            EntityManager.SetComponentData(realmPanelPrefab, new TextureFrame(in panelStyle.frameGenerationData));

            // Set buttonPrefab things
            var buttonStyle = uiDatam.buttonStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            UICoreSystem.SetRenderTextData(EntityManager, buttonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, buttonStyle.fontSize, buttonStyle.textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, labelPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, buttonStyle.fontSize, buttonStyle.textPadding);

            togglePrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(togglePrefab);
            EntityManager.AddComponent<Toggle>(togglePrefab);
            
            labelFixedSizePrefab = EntityManager.Instantiate(labelPrefab);
            EntityManager.AddComponent<Prefab>(labelFixedSizePrefab);
            EntityManager.RemoveComponent<NavigationVisuals>(labelFixedSizePrefab);
            EntityManager.RemoveComponent<InitializeButtonColors>(labelFixedSizePrefab);
            EntityManager.RemoveComponent<RenderTextResizePanel>(labelFixedSizePrefab);
            EntityManager.AddComponent<TextureFrame>(labelFixedSizePrefab);
            EntityManager.AddComponent<GenerateTextureFrame>(labelFixedSizePrefab);
            EntityManager.SetComponentData(labelFixedSizePrefab, new TextureFrame(in panelStyle.frameGenerationData));
            EntityManager.AddComponent<GenerateMeshUI>(labelFixedSizePrefab);
            var subPanelStyle = uiDatam.subPanelStyle.GetStyle();
            toggleGroupPrefab = EntityManager.Instantiate(subPanelPrefab);
            EntityManager.AddComponent<Prefab>(toggleGroupPrefab);
            EntityManager.SetComponentData(toggleGroupPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(toggleGroupPrefab, oneScale);
            EntityManager.AddComponent<GridUI>(toggleGroupPrefab);
            EntityManager.AddComponent<GridUISize>(toggleGroupPrefab);
            EntityManager.AddComponentData(toggleGroupPrefab, new ToggleGroup(255));
            EntityManager.AddComponent<SetToggleGroup>(toggleGroupPrefab);
            EntityManager.SetComponentData(toggleGroupPrefab, new MaterialBaseColor(subPanelStyle.color));
            EntityManager.SetComponentData(toggleGroupPrefab, new MaterialFrameColor(subPanelStyle.frameGenerationData.color));
            EntityManager.SetComponentData(toggleGroupPrefab, new TextureFrame(in subPanelStyle.frameGenerationData));
            InitializeControlPanelPrefabs();
            // setser
            iconOverlayPrefab = EntityManager.Instantiate(UICoreSystem.iconPrefabs.icon);
            EntityManager.AddComponent<Prefab>(iconOverlayPrefab);
            EntityManager.RemoveComponent<Icon>(iconOverlayPrefab);
            EntityManager.RemoveComponent<Texture>(iconOverlayPrefab);
            EntityManager.SetComponentData(iconOverlayPrefab, new Size2D(iconStyle.iconSize));
            EntityManager.SetComponentData(iconOverlayPrefab, new RenderQueue(UICoreSystem.iconQueue + 20));
            // toast ui
            toastUIPrefab = EntityManager.Instantiate(UICoreSystem.labelNoBackgroundPrefab);
            EntityManager.AddComponent<Prefab>(toastUIPrefab);
            EntityManager.AddComponent<Fader>(toastUIPrefab);
            EntityManager.AddComponent<ReverseFader>(toastUIPrefab);
            EntityManager.AddComponent<IgnoreGrid>(toastUIPrefab);
            EntityManager.AddComponent<Fader>(toastUIPrefab);
            EntityManager.AddComponent<MaterialBaseColor>(toastUIPrefab);
            // EntityManager.SetComponentData(actionbarLabelPrefab, new Fader(0, actionbarLabelColor.alpha / 255f, actionbarLabelFadeTime));
            // EntityManager.SetComponentData(actionbarLabelPrefab, new ReverseFader(actionbarLabelFadeTime));
        }

        private void InitializeControlPanelPrefabs()
        {
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var buttonStyle = uiDatam.buttonStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var buttonFontSize = buttonStyle.fontSize * 0.3f;
            var buttonTextPadding = buttonStyle.textPadding * 0.6f;
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            var iconSize = uiDatam.GetIconSize(uiScale) * 0.64f;
            var margins = uiDatam.GetIconMargins(uiScale);
            var padding = uiDatam.GetIconPadding(uiScale);
            var controlFontSize = iconSize.y * 0.9f;
            var controlTextPadding = margins * 0.4f;
            var headerStyle = uiDatam.headerStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var controlPanelStyle = uiDatam.controlPanelStyle.GetStyle();
            // control panel
            controlPanelPrefab = EntityManager.Instantiate(subPanelGridPrefab);
            EntityManager.AddComponent<Prefab>(controlPanelPrefab);
            // EntityManager.RemoveComponent<SubPanel>(controlPanelPrefab); // keep for now for material setting
            EntityManager.SetComponentData(controlPanelPrefab, new MaterialBaseColor(controlPanelStyle.color));
            EntityManager.SetComponentData(controlPanelPrefab, new MaterialFrameColor(controlPanelStyle.frameGenerationData.color));
            EntityManager.SetComponentData(controlPanelPrefab, new TextureFrame(in controlPanelStyle.frameGenerationData));
            EntityManager.AddComponent<DisableGridResize>(controlPanelPrefab);
            EntityManager.AddComponent<AutoGridX>(controlPanelPrefab);
            EntityManager.AddComponent<SpawnButtonsList>(controlPanelPrefab);
            EntityManager.AddComponent<ListPrefabLink>(controlPanelPrefab);
            EntityManager.SetComponentData(controlPanelPrefab, new GridUI(padding, margins * 0.7f));
            EntityManager.SetComponentData(controlPanelPrefab, new GridUISize(new float2(controlFontSize, controlFontSize) + controlTextPadding * 2f));
            // control button
            controlButtonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(controlButtonPrefab);
            UICoreSystem.SetRenderTextData(EntityManager, controlButtonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, controlFontSize, controlTextPadding);

            var gridUI = new GridUI();
            gridUI.gridSize = new int2(1, 8); // texts.Length - 1);
            gridUI.margins = 0.2f * new float2(buttonStyle.fontSize, buttonStyle.fontSize);
            gridUI.padding = padding;
            var gridIconSize = new float2(buttonFontSize * 24 + buttonTextPadding.x * 2f, buttonFontSize + buttonTextPadding.y * 2f);
            var panelSize = gridUI.GetSize(gridIconSize);
            controlPanelFooterPrefab = EntityManager.Instantiate(UICoreSystem.controlPanelPrefab);
            EntityManager.AddComponent<Prefab>(controlPanelFooterPrefab);
            EntityManager.RemoveComponent<Child>(controlPanelFooterPrefab);
            EntityManager.RemoveComponent<NewChild>(controlPanelFooterPrefab);
            var footerHeight = headerStyle.fontSize + headerStyle.textPadding.y * 2f;
            var controlPanelSize = new float2(panelSize.x, footerHeight);
            EntityManager.SetComponentData(controlPanelFooterPrefab, new Size2D(controlPanelSize));
            var controlPanelPosition = new float3(0, -0.5f * panelSize.y - controlPanelSize.y / 2f, 0);
            EntityManager.SetComponentData(controlPanelFooterPrefab, new LocalPosition(controlPanelPosition));

            // Close button
            closeButtonPrefab = EntityManager.Instantiate(UICoreSystem.controlButtonPrefab);
            EntityManager.AddComponent<Prefab>(closeButtonPrefab);
            EntityManager.AddComponent<CloseButton>(closeButtonPrefab);
            EntityManager.AddComponent<PanelLink>(closeButtonPrefab);
            EntityManager.SetComponentData(closeButtonPrefab, new RenderQueue(UICoreSystem.iconQueue + 20));
            EntityManager.SetComponentData(closeButtonPrefab, new LocalPosition(new float3(panelSize.x * 0.44f, 0, 0)));
            EntityManager.AddComponent<SetUIElementPosition>(closeButtonPrefab);
        }

        public static void RemoveChildComponents(EntityManager EntityManager, Entity prefab)
        {
            EntityManager.RemoveComponent<Child>(prefab);
            EntityManager.RemoveComponent<NewChild>(prefab);
        }

        private void AddUIComponents(EntityManager EntityManager, Entity prefab)
        {
            EntityManager.AddComponent<UIElement>(prefab);
            EntityManager.AddComponent<Size2D>(prefab);
        }

        private void AddRenderComponents(EntityManager EntityManager, Entity prefab)
        {
            EntityManager.AddSharedComponentManaged(prefab, new ZoxMesh());
            EntityManager.AddComponentData(prefab, new MaterialBaseColor(new float4(1, 1, 1, 1)));
        }

        private void AddLocalTransformComponents(EntityManager EntityManager, Entity prefab)
        {
            EntityManager.AddComponent<ParentLink>(prefab);
            EntityManager.AddComponent<LocalPosition>(prefab);
            EntityManager.AddComponent<LocalRotation>(prefab);
            EntityManager.AddComponent<Zoxel.Transforms.Child>(prefab); // does it need child?
        }

        private void AddUnityTransformComponents(EntityManager EntityManager, Entity prefab)
        {
            EntityManager.AddComponent<LocalToWorld>(prefab);
            EntityManager.AddComponent<Translation>(prefab);
            EntityManager.AddComponentData(prefab, new Rotation { Value = quaternion.identity });
            EntityManager.AddComponentData(prefab, new NonUniformScale { Value = new float3(1, 1, 1) });
        }
        
        public static void SpawnLabel(EntityCommandBuffer PostUpdateCommands, Entity parent, Entity prefab,
            int childIndex, float3 position, Text text, float fontSize, float2 margins,
            Color baseColor, Color textColor)
        {
            var e = UICoreSystem.SpawnElement(PostUpdateCommands, prefab, parent, position, float2.zero, baseColor);
            UICoreSystem.SetChild(PostUpdateCommands, e, childIndex);
            UICoreSystem.SetRenderText(PostUpdateCommands, e, textColor, textColor, text, fontSize, margins);
        }

        public static Entity SpawnButton(EntityCommandBuffer PostUpdateCommands, Entity parent, Entity prefab,
            int childIndex, float3 position, Text text, float fontSize, float2 margins,
            Color baseColor, Color textColor, in SoundSettings soundSettings)
        {
            var e = UICoreSystem.SpawnElement(PostUpdateCommands, prefab, parent, position, float2.zero, baseColor);
            SetChild(PostUpdateCommands, e, childIndex);
            SetRenderText(PostUpdateCommands, e, textColor, textColor, text, fontSize, margins);
            SetPanelLink(PostUpdateCommands, e, parent);
            SetSound(PostUpdateCommands, e, in soundSettings);
            return e;
        }

        public static Entity SpawnButton(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex,
            Entity prefab, Entity parent, int childIndex,
            float3 position, Text text, float fontSize, float2 margins,
            Color baseColor, Color frameColor, Color textColor, Color textOutlineColor,
            int seed, float2 soundVolume)
        {
            var e = SpawnElement(PostUpdateCommands, entityInQueryIndex, prefab, parent, position, float2.zero, baseColor);
            SetTextureFrame(PostUpdateCommands, entityInQueryIndex, e, frameColor);
            SetChild(PostUpdateCommands, entityInQueryIndex, e, childIndex);
            SetRenderText(PostUpdateCommands, entityInQueryIndex, e, textColor, textOutlineColor, text, fontSize, margins);
            SetPanelLink(PostUpdateCommands, entityInQueryIndex, e, parent);
            SetSound(PostUpdateCommands, entityInQueryIndex, e, seed, soundVolume);
            return e;
        }

        public static void SetTextureFrame(EntityManager EntityManager, Entity e, in FrameGenerationData frameGenerationData)
        {
            EntityManager.SetComponentData(e, new TextureFrame(in frameGenerationData));
            EntityManager.SetComponentData(e, new MaterialFrameColor(frameGenerationData.color));
        }

        public static void SetTextureFrame(EntityCommandBuffer PostUpdateCommands, Entity e, Color fillColor, Color frameColor, int resolution = 128, int frameThickness = 1)
        {
            PostUpdateCommands.SetComponent(e, new TextureFrame
            {
                frameThickness = frameThickness,
                fillColor = fillColor,
                frameColor = frameColor,
                resolution = resolution
            });
        }

        public static void SetTextureFrame(EntityCommandBuffer PostUpdateCommands, Entity e, Color frameColor, int resolution = 128, int frameThickness = 1)
        {
            PostUpdateCommands.SetComponent(e, new TextureFrame
            {
                resolution = resolution,
                frameThickness = frameThickness,
                fillColor = new Color(255, 255, 255, 255),
                frameColor = new Color(0, 0, 0, 255)
            });
            PostUpdateCommands.SetComponent(e, new MaterialFrameColor(frameColor));
        }

        public static void SetTextureFrame(EntityCommandBuffer PostUpdateCommands, Entity e, in FrameGenerationData frameGenerationData)
        {
            PostUpdateCommands.SetComponent(e, new TextureFrame(in frameGenerationData));
            PostUpdateCommands.SetComponent(e, new MaterialFrameColor(frameGenerationData.color));
        }

        public static void SetTextureFrame(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e, Color frameColor, int resolution = 128, int frameThickness = 1)
        {
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new TextureFrame
            {
                resolution = resolution,
                frameThickness = frameThickness,
                fillColor = new Color(255, 255, 255, 255),
                frameColor = new Color(0, 0, 0, 255)
            });
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new MaterialFrameColor(frameColor));
        }

        public static void SetTextureFrame(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e, in FrameGenerationData frameGenerationData)
        {
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new TextureFrame(in frameGenerationData));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new MaterialFrameColor(frameGenerationData.color));
        }

        public static void AddGenerateFrame(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex,
            Entity e, Color fillColor, Color frameColor, int resolution = 128, int frameThickness = 1)
        {
            PostUpdateCommands.AddComponent(entityInQueryIndex, e, new TextureFrame
            {
                frameThickness = frameThickness,
                fillColor = fillColor,
                frameColor = frameColor,
                resolution = resolution
            });
        }

        public static Entity SpawnElement(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab)
        {
            return PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
        }

        public static Entity SpawnElement(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex,
            Entity prefab, Entity parent)
        {
            var e = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new ParentLink(parent));
            return e;
        }

        public static Entity SpawnElement(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex,
            Entity prefab, Entity parent, float3 position)
        {
            var e = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new ParentLink(parent));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new LocalPosition(position));
            return e;
        }

        public static Entity SpawnElement(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex,
            Entity prefab, Entity parent, float3 position, float2 size)
        {
            var e = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new ParentLink(parent));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new LocalPosition(position));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new Size2D(size));
            return e;
        }

        public static Entity SpawnElement(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab, Entity parent, float2 size)
        {
            var e = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new ParentLink(parent));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new Size2D(size));
            return e;
        }

        public static Entity SpawnElement(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab, float2 size)
        {
            var e = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new Size2D(size));
            return e;
        }

        public static Entity SpawnElement(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity prefab, float2 size, Color color)
        {
            var e = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new Size2D(size));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new MaterialBaseColor { Value = color.ToFloat4() });
            return e;
        }

        public static Entity SpawnElement(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex,
            Entity prefab, Entity parent, float3 position, float2 size, Color color)
        {
            var e = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new Size2D(size));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new ParentLink(parent));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new LocalPosition(position));
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new MaterialBaseColor { Value = color.ToFloat4() });
            return e;
        }

        public static Entity SpawnElement(EntityCommandBuffer PostUpdateCommands, Entity prefab, Entity parent, float3 position, float2 size,
            Color color)
        {
            var e = PostUpdateCommands.Instantiate(prefab);
            PostUpdateCommands.SetComponent(e, new Size2D(size));
            PostUpdateCommands.SetComponent(e, new MaterialBaseColor { Value = color.ToFloat4() });
            if (parent.Index != 0)
            {
                PostUpdateCommands.SetComponent(e, new ParentLink(parent));
                PostUpdateCommands.SetComponent(e, new LocalPosition(position));
            }
            return e;
        }


        // Invisible
        public static Entity SpawnElement(EntityCommandBuffer PostUpdateCommands, Entity prefab, Entity parent, float3 position, float2 size)
        {
            var e = PostUpdateCommands.Instantiate(prefab);
            PostUpdateCommands.SetComponent(e, new Size2D(size));
            // Zoxel.Transform
            if (parent.Index != 0)
            {
                PostUpdateCommands.SetComponent(e, new ParentLink(parent));
                PostUpdateCommands.SetComponent(e, new LocalPosition(position));
            }
            return e;
        }

        public static void SetSound(EntityCommandBuffer PostUpdateCommands, Entity e, in SoundSettings soundSettings)
        {
            // var soundSettings = GetSingleton<SoundSettings>();
            var soundVolume = soundSettings.uiSoundVolume;
            var seed = IDUtil.GenerateUniqueID();
            var random = new Random();
            random.InitState((uint) seed);
            var generateSound = new GenerateSound();
            generateSound.InitializeUISound();
            PostUpdateCommands.SetComponent(e, generateSound);
            PostUpdateCommands.SetComponent(e, new SoundData(ref random, generateSound.timeLength, generateSound.sampleRate, 1, soundVolume));
            PostUpdateCommands.SetComponent(e, new Seed(seed));
        }

        public static void SetSeed(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e, int seed)
        {
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new Seed(seed));
        }

        public static void SetSound(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e, int seed, float2 soundVolume)
        {
            var random = new Random();
            random.InitState((uint) seed);
            var generateSound = new GenerateSound();
            generateSound.InitializeUISound();
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, generateSound);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new SoundData(ref random, generateSound.timeLength, generateSound.sampleRate, 1, soundVolume));
        }

        public static void SetPanelLink(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e, Entity panel)
        {
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new PanelLink(panel));
        }

        public static void SetPanelLink(EntityCommandBuffer PostUpdateCommands, Entity e, Entity panel)
        {
            PostUpdateCommands.SetComponent(e, new PanelLink(panel));
        }
        
        public static void SetChild(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e, int childIndex)
        {
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new Zoxel.Transforms.Child(childIndex));
        }
        
        public static void SetChild(EntityCommandBuffer PostUpdateCommands, Entity e, int childIndex)
        {
            PostUpdateCommands.SetComponent(e, new Zoxel.Transforms.Child(childIndex));
        }

        public static void SetRenderText(EntityCommandBuffer PostUpdateCommands, Entity e, Color color, Color outlineColor, Text text, float fontSize, float2 margins,
            byte horizontalAlignment = 0, float maxWidth = 0, float maxHeight = 0)
        {
            var renderTextData = new RenderTextData
            {
                fontSize = fontSize,
                horizontalAlignment = horizontalAlignment,
                maxWidth = maxWidth,
                maxHeight = maxHeight,
                margins = margins,
                color = color,
                outlineColor = outlineColor
            };
            PostUpdateCommands.SetComponent(e, renderTextData);
            PostUpdateCommands.SetComponent(e, new RenderText(text));
        }

        public static void SetRenderText(EntityCommandBuffer PostUpdateCommands, Entity e, Color color, Color outlineColor, TextGenerationData textGenerationData,
            Text text, float fontSize, float2 margins, byte horizontalAlignment = 0, float maxWidth = 0, float maxHeight = 0)
        {
            var renderTextData = new RenderTextData
            {
                textGenerationData = textGenerationData,
                color = color,
                outlineColor = outlineColor,
                fontSize = fontSize,
                horizontalAlignment = horizontalAlignment,
                maxWidth = maxWidth,
                maxHeight = maxHeight,
                margins = margins
            };
            PostUpdateCommands.SetComponent(e, renderTextData);
            PostUpdateCommands.SetComponent(e, new RenderText(text));
        }

        public static void SetRenderText(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e,
            Color color, Color outlineColor, Text text, float fontSize, float2 margins, byte horizontalAlignment = 0, float maxWidth = 0, float maxHeight = 0)
        {
            var renderTextData = new RenderTextData
            {
                fontSize = fontSize,
                horizontalAlignment = horizontalAlignment,
                maxWidth = maxWidth,
                maxHeight = maxHeight,
                margins = margins,
                color = color,
                outlineColor = outlineColor
            };
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, renderTextData);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new RenderText(text));
        }

        public static void SetRenderText(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e,
            Color color, Color outlineColor, TextGenerationData textGenerationData, Text text, float fontSize, float2 margins,
            byte horizontalAlignment = 0, float maxWidth = 0, float maxHeight = 0)
        {
            var renderTextData = new RenderTextData
            {
                textGenerationData = textGenerationData,
                color = color,
                outlineColor = outlineColor,
                fontSize = fontSize,
                horizontalAlignment = horizontalAlignment,
                maxWidth = maxWidth,
                maxHeight = maxHeight,
                margins = margins
            };
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, renderTextData);
            PostUpdateCommands.SetComponent(entityInQueryIndex, e, new RenderText(text));
        }

        public static void SetRenderTextData(EntityManager EntityManager, Entity e,
            TextGenerationData textGenerationData, Color color, Color outlineColor,
            float fontSize, float2 margins, byte horizontalAlignment = 0, float maxWidth = 0, float maxHeight = 0)
        {
            EntityManager.SetComponentData(e, new RenderTextData
            {
                textGenerationData = textGenerationData,
                color = color,
                outlineColor = outlineColor,
                fontSize = fontSize,
                horizontalAlignment = horizontalAlignment,
                maxWidth = maxWidth,
                maxHeight = maxHeight,
                margins = margins
            });
        }

        //! Depreciated. Still used by 2 systems.
        public static Entity SpawnElement(EntityCommandBuffer PostUpdateCommands, Entity prefab,
            Entity parent, float3 position, float2 size, Color color,
            UnityEngine.Material baseMaterial, UnityEngine.Texture2D texture, bool isDestroyMaterial = true)
        {
            var e = PostUpdateCommands.Instantiate(prefab);
            // Zoxel.UI
            UnityEngine.Material material = null;
            var renderMesh = new ZoxMesh
            {
                //hashCode = IDUtil.GenerateUniqueID()
            };
            if (baseMaterial != null)
            {
                if (!isDestroyMaterial)
                {
                    material = baseMaterial;
                }
                else
                {
                    material = new UnityEngine.Material(baseMaterial);
                }
                if (texture != null)
                {
                    material.SetTexture("_BaseMap", texture);
                }
            }
            renderMesh.layer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            renderMesh.material = material;
            PostUpdateCommands.SetComponent(e, new Size2D(size));
            PostUpdateCommands.SetComponent(e, new MaterialBaseColor { Value = color.ToFloat4() });
            // Zoxel.Transform
            if (parent.Index != 0)
            {
                PostUpdateCommands.SetComponent(e, new ParentLink(parent));
                PostUpdateCommands.SetComponent(e, new LocalPosition(position));
            }
            // Unity
            PostUpdateCommands.SetSharedComponentManaged(e, renderMesh);
            return e;
        }
    }
}