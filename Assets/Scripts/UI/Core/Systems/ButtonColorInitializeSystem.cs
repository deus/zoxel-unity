using Unity.Entities;
using Unity.Burst;

namespace Zoxel.UI
{
    //! Sets button colours \todo Set button colours when spawning.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class ButtonColorInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery  processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var selectedEnlargeSize = UIManager.instance.uiSettings.selectedEnlargeSize;
            var buttonAnimationTime = UIManager.instance.uiSettings.buttonAnimationTime;
            Dependency = Entities
                .WithAll<InitializeEntity, NavigationVisuals>()
                .ForEach((ref NavigationVisuals navigationVisuals) =>
            {
                navigationVisuals.navigationAnimationTime = buttonAnimationTime;
                navigationVisuals.growthScale = selectedEnlargeSize;
			}).ScheduleParallel(Dependency);
            if (processQuery.IsEmpty)
            {
                return;
            }
            var uiDatam = UIManager.instance.uiDatam;
            var buttonStyle = uiDatam.buttonStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var activatedTabColor = new Color(UIManager.instance.uiDatam.activatedTabColor).ToFloat4();
            var labelFrameColor = (new Color(25, 15, 12));
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<InitializeButtonColors>(processQuery);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref MaterialBaseColor materialBaseColor, ref MaterialFrameColor materialFrameColor,
                    ref NavigationVisuals navigationVisuals, in InitializeButtonColors initializeButtonColors) =>
            {
                navigationVisuals.fillColor = buttonStyle.color;
                navigationVisuals.frameColor = buttonStyle.frameGenerationData.color;
                if (initializeButtonColors.selectedFrameColor.red == 0 && initializeButtonColors.selectedFrameColor.green == 0 && 
                    initializeButtonColors.selectedFrameColor.blue == 0 && initializeButtonColors.selectedFrameColor.alpha == 0)
                {
                    navigationVisuals.selectedFillColor = buttonStyle.selectedColor;
                }
                else
                {
                    navigationVisuals.selectedFillColor = initializeButtonColors.selectedColor;
                }
                if (initializeButtonColors.selectedFrameColor.red == 0 && initializeButtonColors.selectedFrameColor.green == 0 && 
                    initializeButtonColors.selectedFrameColor.blue == 0 && initializeButtonColors.selectedFrameColor.alpha == 0)
                {
                    navigationVisuals.selectedFrameColor = buttonStyle.selectedFrameColor;
                }
                else
                {
                    navigationVisuals.selectedFrameColor = initializeButtonColors.selectedFrameColor;
                }
                if (HasComponent<Label>(e))
                {
                    navigationVisuals.frameColor = labelFrameColor;
                    navigationVisuals.selectedFrameColor = labelFrameColor;
                }
                materialBaseColor.Value = navigationVisuals.fillColor.ToFloat4();
                if (HasComponent<ToggleActivated>(e))
                {
                    // UnityEngine.Debug.LogError("Toggle Activated on start!");
                    materialFrameColor.Value = activatedTabColor;
                }
                else
                {
                    materialFrameColor.Value = navigationVisuals.frameColor.ToFloat4();
                }
			}).ScheduleParallel(Dependency);
        }
    }
}