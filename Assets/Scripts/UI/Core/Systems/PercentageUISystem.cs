using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;

using Unity.Burst;
using Zoxel.Transforms;
// Updates the scale of the UI bar
// Updates Translation based on scale

namespace Zoxel.UI
{
    [UpdateAfter(typeof(TrailerUISystem))]
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class PercentageUISystem : SystemBase
	{
		[BurstCompile]
        protected override void OnUpdate()
        {
            var time = World.Time.ElapsedTime;
            //var edgeSpace = 0.008f; // 0.004f; 0.033f
            Entities.ForEach((ref PercentageUI percentageUI, ref LocalPosition localPosition, ref LocalScale localScale) =>
            {
                var ratio = percentageUI.size.x / percentageUI.size.y;
                var edgeSpaceX = percentageUI.edgeSpace; 
                var edgeSpaceY = percentageUI.edgeSpace * ratio; 
                percentageUI.UpdatePercentage(time);
				var newScale = localScale.scale;
                newScale.x = percentageUI.percentage - edgeSpaceX * 2;
                newScale.y = 1 - edgeSpaceY * 2;
                localScale.scale = newScale;
                var invertedScaleX = 1 - localScale.scale.x - edgeSpaceX * 2;   //  - edgeSpaceX
                localPosition.position = new float3(-(invertedScaleX * (percentageUI.size.x / 2f)), 0, 0);
            }).ScheduleParallel();
		}
	}
}