﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.UI
{
    //! Keeps UIs floating above character heads.
    /**
    *   \todo Should I use character's Body data instead of 'heightAddition' of TrailerUI.
    */
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class TrailerUISystem : SystemBase
    {
		private EntityQuery processQuery;
		private EntityQuery characterQuery;

        protected override void OnCreate()
        {
			characterQuery = GetEntityQuery(
                ComponentType.ReadOnly<UILink>(),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<Character>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<DeadEntity>());
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var characterEntities = characterQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
			var translations = GetComponentLookup<Translation>(true);
			var rotations = GetComponentLookup<Rotation>(true);
            characterEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .ForEach((ref Translation position, in TrailerUI trailerUI, in CharacterLink characterLink) =>
            {
                if (translations.HasComponent(characterLink.character))
                {
                    var position2 = translations[characterLink.character].Value;
                    if (rotations.HasComponent(characterLink.character))
                    {
                        var rotation = rotations[characterLink.character].Value;
                        position.Value = position2 + math.mul(rotation, new float3(0, trailerUI.heightAddition + 0.1f, 0));
                    }
                    else
                    {
                        position.Value = position2 + new float3(0, trailerUI.heightAddition + 0.1f, 0);
                    }
                }
            })  .WithReadOnly(translations)
                .WithNativeDisableContainerSafetyRestriction(translations)
                .WithReadOnly(rotations)
                .ScheduleParallel(Dependency);
        }
    }
}