using Unity.Entities;

namespace Zoxel.UI
{
    //! Initializes some ui colors.
    public struct InitializeButtonColors : IComponentData
    {
        public Color selectedColor;
        public Color selectedFrameColor;
        public float growthScale;

        public InitializeButtonColors(Color selectedColor, Color selectedFrameColor, float growthScale = 0)
        {
            this.selectedColor = selectedColor;
            this.selectedFrameColor = selectedFrameColor;
            this.growthScale = growthScale;
        }
    }
}