using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! A mouse drag event per frame, add when initially click, remove when released.
    public struct MouseDragEvent : IComponentData
    {
        //! The device that dragged.
        public Entity mouse;
        //! The camera of the player.
        public Entity camera;
        //! The mouse position.
        public float2 position;
        //! The delta distance moved since last.
        public float2 delta;
        //! The index for which finger of touchpad it uses.
        public int fingerID;

        public MouseDragEvent(Entity mouse, float2 position, Entity camera, int fingerID = 0)
        {
            this.mouse = mouse;
            this.position = position;
            this.delta = float2.zero;
            this.camera = camera;
            this.fingerID = fingerID;
        }
    }
}