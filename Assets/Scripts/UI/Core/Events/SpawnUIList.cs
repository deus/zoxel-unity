using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.UI
{
    //! A spawn event for a list.
    public struct SpawnUIList : IComponentData
    {
        public Entity buttonPrefab;
        public Entity labelPrefab;
        public Entity inputPrefab;
        public float fontSize;
        public float2 textPadding;
        public BlitableArray<Text> labels;

        // todo: pass in a list of ui types
        public SpawnUIList(Entity buttonPrefab, Entity labelPrefab, Entity inputPrefab, NativeArray<Text> labels, float fontSize, float2 textPadding, byte isClone = 1)
        {
            this.buttonPrefab = buttonPrefab;
            this.labelPrefab = labelPrefab;
            this.inputPrefab = inputPrefab;
            this.fontSize = fontSize;
            this.textPadding = textPadding;
            this.labels = new BlitableArray<Text>(labels.Length, Allocator.Persistent);
            for (int i = 0; i < labels.Length; i++)
            {
                if (isClone == 1)
                {
                    this.labels[i] = labels[i].Clone();
                }
                else
                {
                    this.labels[i] = labels[i];
                }
            }
        }

        public void Dispose()
        {
            if (this.labels.Length > 0)
            {
                this.labels.Dispose();
            }
        }
    }
}