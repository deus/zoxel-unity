using Unity.Entities;

namespace Zoxel.UI
{
    public enum SelectionEventType : byte
    {
        MouseRaycast,
        Input
    }
    public struct SelectionEvent : IComponentData
    {
        public byte type;
        public Entity character;
        public Entity ui;
        public int buttonIndex;

        public SelectionEvent(SelectionEventType type, Entity character, Entity ui, int buttonIndex = 0)
        {
            this.type = (byte) type;
            this.character = character;
            this.ui = ui;
            this.buttonIndex = buttonIndex;
        }
    }

}