using Unity.Entities;

namespace Zoxel.UI
{
    public struct FadeOutScreen : IComponentData
    {
        public double fadeTime;

        public FadeOutScreen(double fadeTime)
        {
            this.fadeTime = fadeTime;
        }
    }
}