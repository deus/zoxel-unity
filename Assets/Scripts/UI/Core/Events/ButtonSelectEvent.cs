using Unity.Entities;

namespace Zoxel.UI
{
    //! Event on a button for Selecting an Entity.
    public struct UISelectEvent : IComponentData
    {
        public Entity character;

        public UISelectEvent(Entity character)
        {
            this.character = character;
        }
    }
}
