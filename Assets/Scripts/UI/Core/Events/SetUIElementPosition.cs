using Unity.Entities;

namespace Zoxel.UI
{
    //! Fixes raycasting UI position and sizing based on camera.
    public struct SetUIElementPosition : IComponentData { }
}