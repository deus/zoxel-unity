using Unity.Entities;
using Unity.Collections;
// This component carries out a command of destroying character UIs
//  > Supports many commands in one frame

namespace Zoxel.UI
{
    public struct RemoveUI : IComponentData
    {
        public Entity character;
        public Entity ui;

        public RemoveUI(Entity character, Entity ui)
        {
            this.character = character;
            this.ui = ui;
        }
    }
}