using Unity.Entities;

namespace Zoxel.UI
{
    //! Event signifying a Button is Clicked by a player.
    public struct UIClickEvent : IComponentData
    {
        //! The controller that triggered the button.
        public Entity controller;
        //! The device that triggered the button.
        public Entity device;
        //! The button type that triggered the button. A, X, B etc
        public byte buttonType;
        //! \todo Remove this and just inject using controller data.
        //! The device type that triggered the button.
        public byte deviceType;

        public UIClickEvent(Entity controller, Entity device, byte deviceType, byte buttonType)
        {
            this.controller = controller;
            this.buttonType = buttonType;
            this.device = device;
            this.deviceType = deviceType;
        }
    }
}
