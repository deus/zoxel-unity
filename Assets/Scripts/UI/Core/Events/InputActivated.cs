using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.UI
{
    public struct InputNumbersOnly : IComponentData { }
    public struct InputFinished : IComponentData { }
    public struct InputActivated : IComponentData { }
}
