﻿// Note: order linked to HeaderUISpawnSystem
namespace Zoxel
{
    public enum PanelType : byte
    {
        // RealmUI
        PauseMenu,
        StatusUI,
        InventoryUI,
        SkillsUI,
        StatsUI,
        EquipmentUI,
        Map,
        QuestlogUI,
        SkilltreeUI,
        CraftUI,
        MaxRealmUI,
        LogUI,

        // While Playing UIs
        DialogueUI, // 9
        Actionbar,
        Crosshair,
        Minimap,
        QuestTracker,
        WeatherUI,
        TradeUI,

        // Menus
        MainMenu,
        RealmLoadUI,
        CharacterLoadUI,
        NewGameMenu,
        NewCharacterMenu,
        Options,

        ChestUI,
        MusicUI,
        ChatUI,
        ServerUI,

        PlayerTooltip,
        Taskbar,
        JoystickUI,

        None
    }
}
