namespace Zoxel
{
    public static class AnchorUIType
    {
        public const byte Middle = 0;
        public const byte Left = 1;
        public const byte Right = 2;
        public const byte BottomLeft = 3;
        public const byte BottomMiddle = 4;
        public const byte BottomRight = 5;
        public const byte TopLeft = 6;
        public const byte TopMiddle = 7;
        public const byte TopRight = 8;
        public const byte RightMiddle = 9;
        public const byte LeftMiddle = 10;
        public const byte None = 255;
    }
}