using UnityEngine;
using Unity.Entities;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Players;
using Zoxel.Worlds;

namespace Zoxel.Movement.Debug
{
    //! Debugs the Gravity Quadrants with the UI
    public partial class GravityDebugger : MonoBehaviour
    {
        public static GravityDebugger instance;
        private int fontSize = 26;
        private UnityEngine.Color fontColor = UnityEngine.Color.green;

        void Awake()
        {
            instance = this;
        }

        public void OnGUI()
        {
            GUI.skin.label.fontSize = fontSize;
            GUI.color = fontColor;
            DebugGravity();
        }

        public EntityManager EntityManager
        {
            get
            { 
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        public void DebugGravity()
        {
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            if (playerLinks.players.Length == 0)
            {
                GUILayout.Label("Debugging Gravity. No Players.");
            }
            for (int i = 0; i < playerLinks.players.Length; i++)
            {
                var player = playerLinks.players[i];
                GUILayout.Label("Debugging Gravity of Player [" + i + ":" + player.Index + "]");
                var characterEntity = EntityManager.GetComponentData<CharacterLink>(player).character;
                if (!EntityManager.Exists(characterEntity))
                {
                    GUILayout.Label("   Player has no character connected.");
                    continue;
                }
                if (EntityManager.HasComponent<GravityQuadrant>(characterEntity))
                {
                    var quadrant = EntityManager.GetComponentData<GravityQuadrant>(characterEntity).quadrant;
                    GUILayout.Label("   Planet Side: " + (quadrant) + " : " + quadrant);
                }
                if (EntityManager.HasComponent<Rotation>(characterEntity))
                {
                    var rotation2 = EntityManager.GetComponentData<Rotation>(characterEntity).Value;
					var euler = new UnityEngine.Quaternion(
						rotation2.value.x, rotation2.value.y,
						rotation2.value.z, rotation2.value.w).eulerAngles;
                    GUILayout.Label("   Euler: " + euler);
                }
                if (EntityManager.HasComponent<IsOnGround>(characterEntity))
                {
                    var isOnGround = EntityManager.GetComponentData<IsOnGround>(characterEntity);
                    GUILayout.Label("   On Ground: " + isOnGround.isOnGround);
                }
                if (EntityManager.HasComponent<VoxLink>(characterEntity))
                {
                    var planet = EntityManager.GetComponentData<VoxLink>(characterEntity).vox;
                    if (EntityManager.HasComponent<PlanetChunk2DLinks>(planet))
                    {
                        var planetChunk2DLinks = EntityManager.GetComponentData<PlanetChunk2DLinks>(planet);
                        GUILayout.Label("   Planet Chunks: " + planetChunk2DLinks.chunks.Length);
                    }
                    if (EntityManager.HasComponent<ChunkGravityQuadrant>(planet))
                    {
                        var chunkGravityQuadrant = EntityManager.GetComponentData<ChunkGravityQuadrant>(planet);
                        GUILayout.Label("   Chunk Gravity Quadrant - "
                            + ", A1: " + chunkGravityQuadrant.quadrantA.GetBoolA()
                            + ", B1: " + chunkGravityQuadrant.quadrantA.GetBoolB() + ", C1: " + chunkGravityQuadrant.quadrantA.GetBoolC()
                            + ", A2: " + chunkGravityQuadrant.quadrantB.GetBoolA()
                            + ", B2: " + chunkGravityQuadrant.quadrantB.GetBoolB() + ", C2: " + chunkGravityQuadrant.quadrantB.GetBoolC());
                    }
                }
            }
        }
    }
}