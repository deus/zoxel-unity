﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Lines;
using Zoxel.Movement.Authoring;

namespace Zoxel.Movement
{
    [BurstCompile, UpdateInGroup(typeof(PhysicsSystemGroup))]
    public partial class DebugColliderSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<PhysicsSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var physicsSettings = GetSingleton<PhysicsSettings>();
            if (!physicsSettings.isDrawCharacterColliders)
            {
                return;
            }
            var elapsedTime = World.Time.ElapsedTime;
            var linePrefab = RenderLineGroup.linePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((in Body body, in Translation position, in Rotation rotation) =>
            {
                //DebugLines.DrawCubeLines(position.Value, rotation.Value, body.size, Color.gray);
                //var size = body.size;
                //size.x = size.z = math.max(size.x, size.z);
                //DebugLines.DrawCubeLines(position.Value, quaternion.identity, size, Color.white);
                RenderLineGroup.CreateCubeLines(PostUpdateCommands, linePrefab, elapsedTime, position.Value, rotation.Value, body.size, 1); 
            }).Run();
        }

    }
}