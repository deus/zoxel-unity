using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;

namespace Zoxel.Movement
{
    //! Updates an entities GravityQuadrant based on EntityVoxelPosition.
    [BurstCompile, UpdateInGroup(typeof(PhysicsSystemGroup))]
    public partial class GravityEntityUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<ChunkDimensions>());
            chunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<PlanetChunk>(),
                ComponentType.ReadOnly<ChunkGravity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(voxesQuery);
            RequireForUpdate(chunksQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunkDimensions = GetComponentLookup<ChunkDimensions>(true);
            voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunkGravitys = GetComponentLookup<ChunkGravity>(true);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            chunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DeadEntity, DestroyEntity>()
                .WithNone<DisableForce, FlyMode>()
                .WithChangeFilter<EntityVoxelPosition>()
                .ForEach((Entity e, int entityInQueryIndex, ref GravityQuadrant gravityQuadrant, in EntityVoxelPosition entityVoxelPosition, in ChunkLink chunkLink, in Rotation rotation) =>
            {
                // UnityEngine.Debug.LogError("    gravityQuadrant.quadrant: " + gravityQuadrant.quadrant);
                if (!voxLinks.HasComponent(chunkLink.chunk))
                {
                    return;
                }
                var voxLink = voxLinks[chunkLink.chunk];
                if (!chunkDimensions.HasComponent(voxLink.vox))
                {
                    return;
                }
                var chunkDimensions2 = chunkDimensions[voxLink.vox];
                var chunkGravity = chunkGravitys[chunkLink.chunk];
                var chunkPosition = chunkPositions[chunkLink.chunk];
                var localPosition = VoxelUtilities.GetLocalPosition(entityVoxelPosition.position, chunkPosition.position, chunkDimensions2.voxelDimensions);
                // var gravityIn = chunkGravity.gravity.value;
                var gravityIn = chunkGravity.gravity.GetValueDepthDifference(localPosition.ToByte3(), 4, chunkGravity.depth);
                // get value based on depth
                if (gravityIn != gravityQuadrant.quadrant && gravityIn != 0)
                {
                    // UnityEngine.Debug.LogError("Gravity updated to: " + localPosition2 + " :: " + gravityIn);
                    var oldGravity = gravityQuadrant.quadrant;
                    gravityQuadrant.quadrant = gravityIn;
                    if (!HasComponent<DisableGravityRotation>(e))
                    {
                        var beginPitch = math.mul(math.inverse(gravityQuadrant.rotation), rotation.Value).GetAxisRotationY();
                        var targetPitch = beginPitch;
                        var targetRotation = gravityQuadrant.GetBaseRotation(oldGravity, ref targetPitch);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e,
                            new LerpGravityQuadrant(gravityQuadrant.rotation, targetRotation, beginPitch, targetPitch, elapsedTime));
                    }
                }
            })  .WithReadOnly(voxLinks)
                .WithReadOnly(chunkGravitys)
                .WithReadOnly(chunkPositions)
                .WithReadOnly(chunkDimensions)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}