using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;

namespace Zoxel.Movement
{
    //! Limits movement of a character to a map chunk.
    [UpdateAfter(typeof(VelocitySystem))]
    [BurstCompile, UpdateInGroup(typeof(PhysicsSystemGroup))]
    public partial class ChunkBoundsSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
			voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxScale>());
            chunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<PlanetChunk>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
		}

        //! \todo Debug this, character spawning / despawning being weird
        [BurstCompile]
        protected override void OnUpdate()
        {
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxScales = GetComponentLookup<VoxScale>(true);
            voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            var chunks = GetComponentLookup<Chunk>(true);
            chunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithChangeFilter<Translation>()
                .WithNone<DisableForce>()
                .WithNone<InitializeEntity, DeadEntity, DestroyEntity>()
                .WithAll<ChunkBounded>()
                .ForEach((ref Translation translation, in ChunkBoundLink chunkBoundLink, in VoxLink voxLink) =>
            {
                if (!chunks.HasComponent(chunkBoundLink.chunk) || !voxScales.HasComponent(voxLink.vox))
                {
                    return;
                }
                var voxScale = voxScales[voxLink.vox].scale;
                var chunkPosition = chunkPositions[chunkBoundLink.chunk];
                var voxelDimensions = chunks[chunkBoundLink.chunk].voxelDimensions; // new int3(16, 16, 16);
                var buffer = - new float3(0.5f, 0.5f, 0.5f); // float3.zero; // (voxelDimensions / 2).ToFloat3() + offset;
                var chunkLowerBounds = chunkPosition.GetVoxelPosition(voxelDimensions).ToFloat3() - buffer;
                var chunkUpperBounds = chunkLowerBounds + voxelDimensions.ToFloat3() + buffer;
                chunkLowerBounds.x *= voxScale.x;
                chunkLowerBounds.y *= voxScale.y;
                chunkLowerBounds.z *= voxScale.z;
                chunkUpperBounds.x *= voxScale.x;
                chunkUpperBounds.y *= voxScale.y;
                chunkUpperBounds.z *= voxScale.z;
                translation.Value = new float3(
                    math.clamp(translation.Value.x, chunkLowerBounds.x, chunkUpperBounds.x),
                    math.clamp(translation.Value.y, chunkLowerBounds.y, chunkUpperBounds.y),
                    math.clamp(translation.Value.z, chunkLowerBounds.z, chunkUpperBounds.z));
                // UnityEngine.Debug.DrawLine(translation.Value, chunkLowerBounds + (chunkUpperBounds - chunkLowerBounds) / 2f, UnityEngine.Color.red, 10);
            })  .WithReadOnly(chunkPositions)
                .WithReadOnly(chunks)
                .WithReadOnly(voxScales)
                .ScheduleParallel(Dependency);
		}
    }
}