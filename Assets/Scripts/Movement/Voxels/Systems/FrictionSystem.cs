using Unity.Entities;
using Unity.Burst;
using Zoxel.Movement.Authoring;

namespace Zoxel.Movement.Voxels
{
    //! This system responsed to the intersections found in the collision system.
    [UpdateAfter(typeof(VoxelCollisionResponseSystem))]
    [BurstCompile, UpdateInGroup(typeof(CollisionSystemGroup))]
    public partial class FrictionSystem : SystemBase
    {
        protected override void OnCreate()
        {
            RequireForUpdate<PhysicsSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var physicsSettings = GetSingleton<PhysicsSettings>();
            if (physicsSettings.disablePhysics)
            {
                return;
            }
            var groundFriction = physicsSettings.groundFriction;
            var airFriction = physicsSettings.airFriction;
            var bounceMultiplier = physicsSettings.bounceMultiplier;
            var delta = (float) World.Time.DeltaTime;
            Entities
                .WithNone<InitializeEntity>()
                .WithNone<DisableForce, NoClip>()
                // .WithNone<OnTeleported>()
                //.WithChangeFilter<Translation>()
                .ForEach((ref BodyForce bodyForce, in Body body, in GravityQuadrant gravityQuadrant, in VoxelCollider voxelCollider, in IsOnGround isOnGround) =>
            {
                if (voxelCollider.points.Length == 0)
                {
                    return;
                }
                if (isOnGround.isOnGround)
                {
                    for (int i = 0; i < voxelCollider.points.Length; i++)
                    {
                        var point = voxelCollider.points[i];
                        if ((point.hitY.didHit == 1 && gravityQuadrant.quadrant == PlanetSide.Up) || (point.hitY.didHit == 2 && gravityQuadrant.quadrant == PlanetSide.Down))
                        {
                            bodyForce.acceleration.x -= bodyForce.velocity.x * groundFriction;
                            bodyForce.acceleration.z -= bodyForce.velocity.z * groundFriction;
                            break;
                        }
                        else if ((point.hitZ.didHit == 1 && gravityQuadrant.quadrant == PlanetSide.Forward) || (point.hitZ.didHit == 2 && gravityQuadrant.quadrant == PlanetSide.Backward))
                        {
                            bodyForce.acceleration.x -= bodyForce.velocity.x * groundFriction;
                            bodyForce.acceleration.y -= bodyForce.velocity.y * groundFriction;
                            break;
                        }
                        else if ((point.hitX.didHit == 1 && gravityQuadrant.quadrant == PlanetSide.Right) || (point.hitX.didHit == 2 && gravityQuadrant.quadrant == PlanetSide.Left))
                        {
                            bodyForce.acceleration.y -= bodyForce.velocity.y * groundFriction;
                            bodyForce.acceleration.z -= bodyForce.velocity.z * groundFriction;
                            break;
                        }
                    }
                }
                else
                {
                    if (gravityQuadrant.quadrant == PlanetSide.Up || gravityQuadrant.quadrant == PlanetSide.Down)
                    {
                        bodyForce.acceleration.x -= bodyForce.velocity.x * airFriction;
                        bodyForce.acceleration.z -= bodyForce.velocity.z * airFriction;
                    }
                    else if (gravityQuadrant.quadrant == PlanetSide.Left || gravityQuadrant.quadrant == PlanetSide.Right)
                    {
                        bodyForce.acceleration.y -= bodyForce.velocity.y * airFriction;
                        bodyForce.acceleration.z -= bodyForce.velocity.z * airFriction;
                    }
                    else if (gravityQuadrant.quadrant == PlanetSide.Backward || gravityQuadrant.quadrant == PlanetSide.Forward)
                    {
                        bodyForce.acceleration.x -= bodyForce.velocity.x * airFriction;
                        bodyForce.acceleration.y -= bodyForce.velocity.y * airFriction;
                    }
                }
            }).ScheduleParallel();
		}
    }
}