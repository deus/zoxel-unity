using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Movement.Voxels
{
    //! Removes the BasicVoxelCollided event from an entity.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(CollisionSystemGroup))]
    public partial class BasicVoxelColliderEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<BasicVoxelCollided>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<BasicVoxelCollided>(processQuery);
        }
    }
}