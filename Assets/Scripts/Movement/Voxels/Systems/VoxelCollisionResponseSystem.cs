﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Movement.Authoring;

namespace Zoxel.Movement.Voxels
{
    //! Responses to collisions by displacing the position to keep it aligned.
    /**
    *   \todo Play different sounds based on what voxel I am standing on.
    */
    [UpdateAfter(typeof(VoxelCollisionDetectSystem))]
    [BurstCompile, UpdateInGroup(typeof(CollisionSystemGroup))]
    public partial class VoxelCollisionResponseSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;

        protected override void OnCreate()
        {
            voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<Planet>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate<PhysicsSettings>();
            RequireForUpdate(processQuery);
            RequireForUpdate(voxesQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var physicsSettings = GetSingleton<PhysicsSettings>();
            if (physicsSettings.disablePhysics)
            {
                return;
            }
            var collisionFailureShift = physicsSettings.collisionFailureShift; //  1.0f;
            var groundFriction = physicsSettings.groundFriction;
            var airFriction = physicsSettings.airFriction;
            var bounceMultiplier = physicsSettings.bounceMultiplier;
            var delta = (float) World.Time.DeltaTime;
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var voxScales = GetComponentLookup<VoxScale>(true);
            voxEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithChangeFilter<Translation>()
                .WithNone<InitializeEntity, InitializeVoxelCollider>()
                .WithNone<DisableForce, NoClip>()
                .ForEach((ref IsOnGround isOnGround, ref Translation translation, ref BodyForce bodyForce, in VoxelCollider voxelCollider, in Body body,
                    in GravityQuadrant gravityQuadrant, in VoxLink voxLink) =>  // , ref Rotation rotation
            {
                if (!voxScales.HasComponent(voxLink.vox))
                {
                    return;
                }
                var isAllSolid = true;
                for (int i = 0; i < voxelCollider.points.Length; i++)
                {
                    var point = voxelCollider.points[i];
                    if (point.hitX.isSolidWithin == 0 && point.hitY.isSolidWithin == 0 && point.hitZ.isSolidWithin == 0)
                    {
                        isAllSolid = false;
                        break;
                    }
                }
                if (isAllSolid)
                {
                    // UnityEngine.Debug.LogError("All Points are Solid!");
                    var amount = new float3();
                    if (gravityQuadrant.quadrant == PlanetSide.Left)
                    {
                        amount.x = collisionFailureShift;
                        bodyForce.velocity.x = 0;
                    }
                    else if (gravityQuadrant.quadrant == PlanetSide.Right)
                    {
                        amount.x = -collisionFailureShift;
                        bodyForce.velocity.x = 0;
                    }
                    else if (gravityQuadrant.quadrant == PlanetSide.Down)
                    {
                        amount.y = -collisionFailureShift;
                        bodyForce.velocity.y = 0;
                    }
                    else if (gravityQuadrant.quadrant == PlanetSide.Up)
                    {
                        amount.y = collisionFailureShift;
                        bodyForce.velocity.y = 0;
                    }
                    else if (gravityQuadrant.quadrant == PlanetSide.Backward)
                    {
                        amount.z = -collisionFailureShift;
                        bodyForce.velocity.z = 0;
                    }
                    else if (gravityQuadrant.quadrant == PlanetSide.Forward)
                    {
                        amount.z = collisionFailureShift;
                        bodyForce.velocity.z = 0;
                    }
                    translation.Value += amount;
                    return;
                }
                var voxScale = voxScales[voxLink.vox].scale;
                isOnGround.isOnGround = false;
                var displacementPosition = float3.zero;
                var displacements = 0f;
                for (int i = 0; i < voxelCollider.points.Length; i++)
                {
                    var point = voxelCollider.points[i];
                    if (point.hitY.didHit == 1)
                    {
                        if (bodyForce.velocity.y < 0)
                        {
                            bodyForce.velocity.y = -bodyForce.velocity.y * bounceMultiplier;
                        }
                        if (gravityQuadrant.quadrant == PlanetSide.Up) // && voxelCollider.isOnGround == 0)
                        {
                            isOnGround.isOnGround = true;
                        }
                        // UnityEngine.Debug.LogError("Hit Down!");
                        //! Gets displacement between voxel face and collider position!
                        var voxelFaceUp = voxScale.y * (point.voxelPosition.y + 1f);
                        var differenceY = voxelFaceUp - voxelCollider.updatedPoints[i].y; // voxelCollider.updatedPoints[i].y;
                        displacementPosition += new float3(0, differenceY, 0);
                        displacements++;
                    }
                    else if (point.hitY.didHit == 2)
                    {
                        if (bodyForce.velocity.y > 0)
                        {
                            bodyForce.velocity.y = -bodyForce.velocity.y * bounceMultiplier;
                        }
                        // apply friction
                        if (gravityQuadrant.quadrant == PlanetSide.Down) // && voxelCollider.isOnGround == 0)
                        {
                            isOnGround.isOnGround = true;
                        }
                        var voxelFaceDown = voxScale.y * point.voxelPosition.y;
                        var differenceY = voxelFaceDown - voxelCollider.updatedPoints[i].y;
                        displacementPosition += new float3(0, differenceY, 0);
                        displacements++;
                    }
                }
                if (displacements != 0)
                {
                    translation.Value += displacementPosition / displacements;
                }
                // === Respond X ===
                displacementPosition = float3.zero;
                displacements = 0f;
                for (int i = 0; i < voxelCollider.points.Length; i++)
                {
                    var point = voxelCollider.points[i];
                    var oldPoint = voxelCollider.previousPoints[i];
                    if (point.hitX.didHit == 1)
                    {
                        // i need to just displace this value
                        // by the 
                        if (bodyForce.velocity.x < 0)
                        {
                            bodyForce.velocity.x = -bodyForce.velocity.x * bounceMultiplier;
                        }
                        if (gravityQuadrant.quadrant == PlanetSide.Right) //  && voxelCollider.isOnGround == 0)
                        {
                            isOnGround.isOnGround = true;
                        }
                        var voxelFaceRight = voxScale.x * (point.voxelPosition.x + 1);
                        var differenceX = voxelFaceRight - voxelCollider.updatedPoints[i].x;
                        displacementPosition += new float3(differenceX, 0, 0);
                        displacements++;
                    }
                    else if (point.hitX.didHit == 2)
                    {
                        if (bodyForce.velocity.x > 0)
                        {
                            bodyForce.velocity.x = -bodyForce.velocity.x * bounceMultiplier;
                        }
                        // apply friction
                        if (gravityQuadrant.quadrant == PlanetSide.Left) //  && voxelCollider.isOnGround == 0)
                        {
                            isOnGround.isOnGround = true;
                        }
                        var voxelFaceLeft = voxScale.x * point.voxelPosition.x;
                        var differenceX = voxelFaceLeft - voxelCollider.updatedPoints[i].x;
                        displacementPosition += new float3(differenceX, 0, 0);
                        displacements++;
                    }
                }
                if (displacements != 0)
                {
                    translation.Value += displacementPosition / displacements;
                }
                // === Respond Z ===
                displacementPosition = float3.zero;
                displacements = 0f;
                for (int i = 0; i < voxelCollider.points.Length; i++)
                {
                    var point = voxelCollider.points[i];
                    if (point.hitZ.didHit == 1)
                    {
                        if (bodyForce.velocity.z < 0)
                        {
                            bodyForce.velocity.z = -bodyForce.velocity.z * bounceMultiplier;
                        }
                        // apply friction
                        if (gravityQuadrant.quadrant == PlanetSide.Forward) //  && voxelCollider.isOnGround == 0)
                        {
                            isOnGround.isOnGround = true;
                        }
                        var voxelFaceBack = voxScale.z * (point.voxelPosition.z + 1);
                        var differenceZ = voxelFaceBack - voxelCollider.updatedPoints[i].z;
                        displacementPosition += new float3(0, 0, differenceZ);
                        displacements++;
                    }
                    else if (point.hitZ.didHit == 2)
                    {
                        if (bodyForce.velocity.z > 0)
                        {
                            bodyForce.velocity.z = -bodyForce.velocity.z * bounceMultiplier;
                        }
                        // apply friction
                        if (gravityQuadrant.quadrant == PlanetSide.Backward) //  && voxelCollider.isOnGround == 0)
                        {
                            isOnGround.isOnGround = true;
                        }
                        var voxelFaceForward = voxScale.z * point.voxelPosition.z;
                        var differenceZ = voxelFaceForward - voxelCollider.updatedPoints[i].z;
                        displacementPosition += new float3(0, 0, differenceZ);
                        displacements++;
                    }
                }
                if (displacements != 0)
                {
                    translation.Value += displacementPosition / displacements;
                }
            })  .WithReadOnly(voxScales)
                .ScheduleParallel();
		}
    }
}