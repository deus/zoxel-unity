﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Voxels.Minivoxes;
using Zoxel.Movement.Authoring;

namespace Zoxel.Movement.Voxels
{
    //! Detects collisions with a voxel per point around their collider.
    /**
    *   Contains debug line cod for debugging.
    *   \todo Make each collision point a separate entity! Then in response system, pass in points.
    *   \todo Make it one function, pass in axis.
    */
    [BurstCompile, UpdateInGroup(typeof(CollisionSystemGroup))]
    public partial class VoxelCollisionDetectSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;
        private EntityQuery chunksQuery;
        private EntityQuery chunkRenders;
        private EntityQuery voxelQuery;
        private EntityQuery characterQuery;
        private EntityQuery minivoxQuery;

        protected override void OnCreate()
        {
            voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<Planet>());
            chunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<ChunkPosition>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<PlanetChunk>());
            chunkRenders = GetEntityQuery(
                ComponentType.ReadOnly<ChunkPosition>(),
                ComponentType.ReadOnly<ChunkSides>(),
                ComponentType.ReadOnly<VoxLink>(),
                ComponentType.ReadOnly<PlanetRender>());
            voxelQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Voxel>());
            characterQuery = GetEntityQuery(
                ComponentType.ReadOnly<Body>(),
                ComponentType.ReadOnly<VoxelCollider>(),
                ComponentType.Exclude<VoxLink>(),
                ComponentType.Exclude<Translation>());
            minivoxQuery = GetEntityQuery(
                ComponentType.ReadOnly<Minivox>(),
                ComponentType.Exclude<DestroyEntity>());
            minivoxQuery = GetEntityQuery(
                ComponentType.ReadOnly<Voxel>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate<PhysicsSettings>();
            RequireForUpdate(processQuery);
            RequireForUpdate(voxesQuery);
            RequireForUpdate(chunksQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var physicsSettings = GetSingleton<PhysicsSettings>();
            if (physicsSettings.disablePhysics)
            {
                return;
            }
            var oneFloat3 = new float3(1f, 1f, 1f);
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var voxScales = GetComponentLookup<VoxScale>(true);
            var voxelLinks2 = GetComponentLookup<VoxelLinks>(true);
            voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            var chunks = GetComponentLookup<Chunk>(true);
            var chunkNeighbors = GetComponentLookup<ChunkNeighbors>(true);
            var chunkRenderLinks = GetComponentLookup<ChunkRenderLinks>(true);
            var minivoxLinks = GetComponentLookup<MinivoxLinks>(true);
            chunkEntities.Dispose();
            var chunkRenderEntities = chunkRenders.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunkSides = GetComponentLookup<ChunkSides>(true);
            var waterChunkRenders = GetComponentLookup<WaterRender>(true);
            chunkRenderEntities.Dispose();
            var minivoxEntities = minivoxQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleE);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleE);
            var ignoreMinivoxCollisions = GetComponentLookup<IgnoreMinivoxCollision>(true);
            minivoxEntities.Dispose();
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleF);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleF);
            var voxelSolidWithins = GetComponentLookup<VoxelSolidWithin>(true);
            var voxelWillCollides = GetComponentLookup<VoxelWillCollide>(true);
            voxelEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, InitializeVoxelCollider>()
                .WithNone<DisableForce, NoClip>()
                .WithChangeFilter<Translation>()
                .ForEach((Entity e, int entityInQueryIndex, ref VoxelCollider voxelCollider, in Body body, in VoxLink voxLink, in Translation translation, in Rotation rotation, in ChunkLink chunkLink) =>
            {
                if (voxelCollider.points.Length == 0 || body.size.x == 0)
                {
                    return;
                }
                var chunkEntity = chunkLink.chunk;
                if (!chunkPositions.HasComponent(chunkEntity) || !voxelLinks2.HasComponent(voxLink.vox))
                {
                    return;
                }
                // UnityEngine.Profiling.Profiler.BeginSample("Detection Colliders");
                var voxScale = voxScales[voxLink.vox].scale;
                var voxelLinks = voxelLinks2[voxLink.vox];
                var size = body.size;
                var maxXZ = math.max(size.x, size.z);
                var position = translation.Value;
                // Reduce all point positions to last y position if hit ground
                for (int i = 0; i < voxelCollider.previousPoints.Length; i++)
                {
                    voxelCollider.previousPoints[i] = voxelCollider.points[i].globalPosition;
                }
                var inChunkPosition = chunkPositions[chunkEntity].position;
                var chunkPosition = inChunkPosition;
                var voxelDimensions = chunks[chunkEntity].voxelDimensions;
                var chunkNeighbors2 = chunkNeighbors[chunkEntity];
                //! For each point around the Entity
                var didCollideX = false;
                var didCollideY = false;
                var didCollideZ = false;
                for (int i = 0; i < voxelCollider.points.Length; i++)
                {
                    var point = voxelCollider.points[i];
                    var scaledPosition = new float3(point.localPosition.x * maxXZ, point.localPosition.y * size.y, point.localPosition.z * maxXZ);
                    var pointGlobalPosition = position + math.rotate(rotation.Value, scaledPosition);
                    voxelCollider.updatedPoints[i] = pointGlobalPosition;
                    var pointVoxelPositionX = VoxelUtilities.GetVoxelPosition(new float3(pointGlobalPosition.x, point.globalPosition.y, point.globalPosition.z), voxScale);
                    var pointVoxelPositionY = VoxelUtilities.GetVoxelPosition(new float3(point.globalPosition.x, pointGlobalPosition.y, point.globalPosition.z), voxScale);
                    var pointVoxelPositionZ = VoxelUtilities.GetVoxelPosition(new float3(point.globalPosition.x, point.globalPosition.y, pointGlobalPosition.z), voxScale);
                    var newVoxelPosition = new int3(pointVoxelPositionX.x, pointVoxelPositionY.y, pointVoxelPositionZ.z);
                    point.Reset();
                    var voxelLowerBounds = newVoxelPosition.ToFloat3();
                    // vary this for beds
                    var voxelUpperBounds = voxelLowerBounds + oneFloat3;
                    voxelLowerBounds.x *= voxScale.x;
                    voxelLowerBounds.y *= voxScale.y;
                    voxelLowerBounds.z *= voxScale.z;
                    voxelUpperBounds.x *= voxScale.x;
                    voxelUpperBounds.y *= voxScale.y;
                    voxelUpperBounds.z *= voxScale.z;
                    didCollideX = didCollideX || ProcessPoint(position, pointGlobalPosition, ref point, PlanetAxis.Horizontal,
                        chunkLink.chunk, inChunkPosition, pointVoxelPositionX,
                        voxelDimensions, voxelLowerBounds, voxelUpperBounds,
                        in chunkNeighbors2, in voxelLinks, in chunks, in chunkSides,
                        in chunkRenderLinks, in voxelSolidWithins, in voxelWillCollides,
                        in minivoxLinks, in ignoreMinivoxCollisions, in waterChunkRenders);
                    didCollideY = didCollideY || ProcessPoint(position, pointGlobalPosition, ref point, PlanetAxis.Vertical,
                        chunkLink.chunk, inChunkPosition, pointVoxelPositionY,
                        voxelDimensions, voxelLowerBounds, voxelUpperBounds,
                        in chunkNeighbors2, in voxelLinks, in chunks, in chunkSides,
                        in chunkRenderLinks, in voxelSolidWithins, in voxelWillCollides,
                        in minivoxLinks, in ignoreMinivoxCollisions, in waterChunkRenders);
                    didCollideZ = didCollideZ || ProcessPoint(position, pointGlobalPosition, ref point, PlanetAxis.Depth,
                        chunkLink.chunk, inChunkPosition, pointVoxelPositionZ,
                        voxelDimensions, voxelLowerBounds, voxelUpperBounds,
                        in chunkNeighbors2, in voxelLinks, in chunks, in chunkSides,
                        in chunkRenderLinks, in voxelSolidWithins, in voxelWillCollides,
                        in minivoxLinks, in ignoreMinivoxCollisions, in waterChunkRenders);
                    // set point back
                    point.voxelPosition = newVoxelPosition;
                    point.globalPosition = pointGlobalPosition;
                    voxelCollider.points[i] = point;
                    #if DEBUG_VOXEL_COLLISIONS
                        if (point.hitX.didHit == 1 || point.hitY.didHit == 1 || point.hitZ.didHit == 1)
                        {
                            DebugLines.DrawCubeLines(pointGlobalPosition, quaternion.identity, 0.08f, UnityEngine.Color.red);
                        }
                        else
                        {
                            DebugLines.DrawCubeLines(pointGlobalPosition, quaternion.identity, 0.08f, UnityEngine.Color.green);
                        }
                    #endif
                }
                //! First needs to set previousPoints if has not updated yet.
                /*if (voxelCollider.initialized < VoxelCollisionDetectSystem.maxUpdateChecks)
                {
                    voxelCollider.initialized++;
                }
                //! Resets collider points if collided per axis to previousPoints.
                else */
                if ((didCollideX || didCollideY || didCollideZ))
                {
                    for (int i = 0; i < voxelCollider.points.Length; i++)
                    {
                        var point = voxelCollider.points[i];
                        if (didCollideX)
                        {
                            point.globalPosition.x = voxelCollider.previousPoints[i].x;
                        }
                        if (didCollideY)
                        {
                            point.globalPosition.y = voxelCollider.previousPoints[i].y;
                        }
                        if (didCollideZ)
                        {
                            point.globalPosition.z = voxelCollider.previousPoints[i].z;
                        }
                        voxelCollider.points[i] = point;
                    }
                }
			    // UnityEngine.Profiling.Profiler.EndSample();
            })  .WithReadOnly(voxScales)
                .WithReadOnly(voxelLinks2)
                .WithReadOnly(chunks)
                .WithReadOnly(chunkPositions)
                .WithReadOnly(chunkNeighbors)
                .WithReadOnly(chunkRenderLinks)
                .WithReadOnly(minivoxLinks)
                .WithReadOnly(chunkSides)
                .WithReadOnly(waterChunkRenders)
                .WithReadOnly(ignoreMinivoxCollisions)
                .WithReadOnly(voxelSolidWithins)
                .WithReadOnly(voxelWillCollides)
                .ScheduleParallel(Dependency);
		}

        private static bool ProcessPoint(float3 position, float3 pointGlobalPosition, ref VoxelCollisionPointXYZ point, byte side,
            Entity chunkEntity, int3 chunkPosition, int3 pointVoxelPosition,
            int3 voxelDimensions, float3 voxelLowerBounds, float3 voxelUpperBounds,
            in ChunkNeighbors chunkNeighbors2, in VoxelLinks voxelLinks,
            in ComponentLookup<Chunk> chunks,
            in ComponentLookup<ChunkSides> chunkSides,
            in ComponentLookup<ChunkRenderLinks> chunkRenderLinks,
            in ComponentLookup<VoxelSolidWithin> voxelSolidWithins,
            in ComponentLookup<VoxelWillCollide> voxelWillCollides,
            in ComponentLookup<MinivoxLinks> minivoxLinks,
            in ComponentLookup<IgnoreMinivoxCollision> ignoreMinivoxCollisions,
            in ComponentLookup<WaterRender> waterChunkRenders)
        {
            var localPosition = VoxelUtilities.GetLocalPosition(pointVoxelPosition, chunkPosition, voxelDimensions);
            if (!VoxelUtilities.InBounds(localPosition, voxelDimensions))
            {
                chunkEntity = chunkNeighbors2.GetChunk(ref localPosition, ref chunkPosition, voxelDimensions);
            }
            if (!chunks.HasComponent(chunkEntity))
            {
                return false;
            }
            var voxelIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
            var voxels2 = chunks[chunkEntity].voxels;
            if (!(voxelIndex >= 0 && voxelIndex < voxels2.Length))
            {
                return false;
            }
            var sides = new VoxelCollisionPointSides();
            var voxelType = voxels2[voxelIndex];
            if (voxelType == 0)
            {
                return false;
            }
            //! Check if voxel meta data has collision or not.
            // check if isCollide
            var voxelIndexMinusAir = voxelType - 1;
            if (voxelIndexMinusAir < voxelLinks.voxels.Length)
            {
                var voxelEntity = voxelLinks.voxels[voxelIndexMinusAir];
                if (voxelSolidWithins.HasComponent(voxelEntity))
                {
                    point.SetIsSolidWithin(1, side); // hitY.isSolidWithin = 1;
                }
                if (!voxelWillCollides.HasComponent(voxelEntity))
                {
                    return false;
                }
            }
            //! Check if minivox has collision or not
            var minivoxLinks2 = minivoxLinks[chunkEntity];
            var hitMinivox = minivoxLinks2.GetMinivox(pointVoxelPosition);
            if (ignoreMinivoxCollisions.HasComponent(hitMinivox))
            {
                point.SetIsSolidWithin(0, side); // hitY.isSolidWithin = 0;
                return false;
            }
            //! Check if Chunk Render has collision sides.
            var chunkRenderFound = false;
            var chunkRenderLinks2 = chunkRenderLinks[chunkEntity];
            for (byte j = 0; j < chunkRenderLinks2.chunkRenders.Length; j++)
            {
                var chunkRenderEntity = chunkRenderLinks2.chunkRenders[j];
                if (waterChunkRenders.HasComponent(chunkRenderEntity))
                {
                    continue;
                }
                var chunkSides2 = chunkSides[chunkRenderEntity];
                if (!(voxelIndex >= 0 && voxelIndex < chunkSides2.sides.Length))
                {
                    continue;
                }
                chunkRenderFound = true;
                if (side == PlanetAxis.Horizontal)
                {
                    sides.SetSide((byte) (sides.GetSide(VoxelSide.Left) | chunkSides2.GetSideIndex(voxelIndex, VoxelSide.Left)), VoxelSide.Left);
                    sides.SetSide((byte) (sides.GetSide(VoxelSide.Right) | chunkSides2.GetSideIndex(voxelIndex, VoxelSide.Right)), VoxelSide.Right);
                }
                else if (side == PlanetAxis.Vertical)
                {
                    sides.SetSide((byte) (sides.GetSide(VoxelSide.Down) | chunkSides2.GetSideIndex(voxelIndex, VoxelSide.Down)), VoxelSide.Down);
                    sides.SetSide((byte) (sides.GetSide(VoxelSide.Up) | chunkSides2.GetSideIndex(voxelIndex, VoxelSide.Up)), VoxelSide.Up);
                }
                else if (side == PlanetAxis.Depth)
                {
                    sides.SetSide((byte) (sides.GetSide(VoxelSide.Backward) | chunkSides2.GetSideIndex(voxelIndex, VoxelSide.Backward)), VoxelSide.Backward);
                    sides.SetSide((byte) (sides.GetSide(VoxelSide.Forward) | chunkSides2.GetSideIndex(voxelIndex, VoxelSide.Forward)), VoxelSide.Forward);
                }
            }
            if (!chunkRenderFound)
            {
                return false;
            }
            // Collision Y
            // Up
            if ((side == PlanetAxis.Horizontal && pointGlobalPosition.x < position.x)
                || (side == PlanetAxis.Vertical && pointGlobalPosition.y < position.y)
                || (side == PlanetAxis.Depth && pointGlobalPosition.z < position.z))
            {
                //! Check has y moved pass voxel upper bounds.
                if ((side == PlanetAxis.Horizontal && sides.GetSide(VoxelSide.Right) == 1 && pointGlobalPosition.x < voxelUpperBounds.x && point.globalPosition.x >= voxelUpperBounds.x)
                    || (side == PlanetAxis.Vertical && sides.GetSide(VoxelSide.Up) == 1 && pointGlobalPosition.y < voxelUpperBounds.y && point.globalPosition.y >= voxelUpperBounds.y)
                    || (side == PlanetAxis.Depth && sides.GetSide(VoxelSide.Forward) == 1 && pointGlobalPosition.z < voxelUpperBounds.z && point.globalPosition.z >= voxelUpperBounds.z))
                {
                    point.SetDidHit(1, side); // hitY.didHit = 1;
                    return true;
                }
            }
            // Down
            else // if (pointGlobalPosition.y > position.y)
            {
                //! Check has y moved pass voxel lower bounds.
                if ((side == PlanetAxis.Horizontal && sides.GetSide(VoxelSide.Left) == 1 && pointGlobalPosition.x > voxelLowerBounds.x && point.globalPosition.x <= voxelLowerBounds.x)
                    || (side == PlanetAxis.Vertical && sides.GetSide(VoxelSide.Down) == 1 && pointGlobalPosition.y > voxelLowerBounds.y && point.globalPosition.y <= voxelLowerBounds.y)
                    || (side == PlanetAxis.Depth && sides.GetSide(VoxelSide.Backward) == 1 && pointGlobalPosition.z > voxelLowerBounds.z && point.globalPosition.z <= voxelLowerBounds.z))
                {
                    point.SetDidHit(2, side); // hitY.didHit = 2;
                    return true;
                }
            }
            return false;
        }
    }
}