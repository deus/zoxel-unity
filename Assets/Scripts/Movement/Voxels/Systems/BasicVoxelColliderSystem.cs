using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Voxels;

namespace Zoxel.Movement.Voxels
{
    //! Handles basic collisions, used for bullets.
    [BurstCompile, UpdateInGroup(typeof(CollisionSystemGroup))]
    public partial class BasicVoxelColliderSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<ChunkDimensions>(),
                ComponentType.ReadOnly<VoxelLinks>());
            chunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkPosition>(),
                ComponentType.ReadOnly<PlanetChunk>());
            RequireForUpdate(processQuery);
            RequireForUpdate(voxesQuery);
            RequireForUpdate(chunksQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunkLinks2 = GetComponentLookup<ChunkLinks>(true);
            var chunkDimensions = GetComponentLookup<ChunkDimensions>(true);
            var voxelLinks2 = GetComponentLookup<VoxelLinks>(true);
            var voxScales = GetComponentLookup<VoxScale>(true);
            voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunks = GetComponentLookup<Chunk>(true);
            chunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity, BasicVoxelCollided>()
                // .SetChangedVersionFilter(ComponentType.ReadOnly<Translation>())
                .WithChangeFilter<Translation>()
                .ForEach((Entity e, int entityInQueryIndex, ref BasicVoxelCollider basicVoxelCollider, in Translation translation, in VoxLink voxLink) =>
            {
                if (voxLink.vox.Index == 0 || !voxScales.HasComponent(voxLink.vox))
                {
                    return;
                }
                var voxScale = voxScales[voxLink.vox].scale;
                var voxelPosition = VoxelUtilities.GetVoxelPosition(translation.Value, voxScale);
                // when it enters a new position
                if (basicVoxelCollider.hitPosition == voxelPosition)
                {
                    return;
                }
                basicVoxelCollider.hitPosition = voxelPosition;
                var hitInfo = basicVoxelCollider.hit;
                var worldEntity = voxLink.vox;
                var voxelDimensions = chunkDimensions[worldEntity].voxelDimensions;
                var voxelLinks = voxelLinks2[worldEntity];
                var chunkPosition = VoxelUtilities.GetChunkPosition(voxelPosition, voxelDimensions);
                // using chunk positions, if chunk position changes, get new chunk entity
                if (basicVoxelCollider.init == 0 || basicVoxelCollider.hitChunkPosition != chunkPosition)
                {
                    Entity chunkEntity;
                    if (chunkLinks2[worldEntity].chunks.TryGetValue(chunkPosition, out chunkEntity))
                    {
                        basicVoxelCollider.hitChunkPosition = chunkPosition;
                        basicVoxelCollider.chunk = chunkEntity;
                        basicVoxelCollider.init = 1;
                    }
                    else
                    {
                        basicVoxelCollider.chunk = new Entity();
                        return;
                    }
                }
                if (basicVoxelCollider.chunk.Index == 0 || !chunks.HasComponent(basicVoxelCollider.chunk))
                {
                    return;
                }
                var chunk = chunks[basicVoxelCollider.chunk];
                if (chunk.voxels.Length > 0)
                {
                    var localVoxelPosition = voxelPosition - ChunkPosition.GetChunkVoxelPosition(chunkPosition, voxelDimensions);
                    int voxelIndex = VoxelUtilities.GetVoxelArrayIndex(localVoxelPosition, voxelDimensions);
                    if (voxelIndex < chunk.voxels.Length)
                    {
                        basicVoxelCollider.hitVoxelType = chunk.voxels[voxelIndex];
                        if (basicVoxelCollider.hitVoxelType != 0)
                        {
                            // if grass model, then make it not hit
                            var voxelTypeMinusAir = basicVoxelCollider.hitVoxelType - 1;
                            var voxelEntity = voxelLinks.voxels[voxelTypeMinusAir];
                            if (HasComponent<VoxelWillCollide>(voxelEntity))
                            {
                                PostUpdateCommands.AddComponent<BasicVoxelCollided>(entityInQueryIndex, e);
                            }
                            else
                            {
                                basicVoxelCollider.hitVoxelType = 0;
                            }
                        }
                    }
                    else
                    {
                        basicVoxelCollider.hitVoxelType = 0;
                    }
                }
                else
                {
                    basicVoxelCollider.hitVoxelType = 0;
                }
                basicVoxelCollider.hit = hitInfo;
            })  .WithReadOnly(chunkLinks2)
                .WithReadOnly(voxelLinks2)
                .WithReadOnly(chunkDimensions)
                .WithReadOnly(voxScales)
                .WithReadOnly(chunks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}