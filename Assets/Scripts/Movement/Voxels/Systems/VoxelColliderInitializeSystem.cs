using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Movement.Authoring;

namespace Zoxel.Movement.Voxels
{
    //! Initializes collision points on the voxel colliders.
    [BurstCompile, UpdateInGroup(typeof(CollisionSystemGroup))]
    public partial class VoxelColliderInitializeSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;

        protected override void OnCreate()
        {
            voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<Planet>());
            RequireForUpdate(processQuery);
            RequireForUpdate(voxesQuery);
            RequireForUpdate<PhysicsSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var physicsSettings = GetSingleton<PhysicsSettings>();
            if (physicsSettings.disablePhysics)
            {
                return;
            }
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var voxScales = GetComponentLookup<VoxScale>(true);
            voxEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity>()
                .WithAll<InitializeVoxelCollider>()
                .ForEach((Entity e, int entityInQueryIndex, ref VoxelCollider voxelCollider, in Body body, in VoxLink voxLink, in Translation translation, in Rotation rotation) =>
            {
                if (voxelCollider.points.Length == 0 || body.size.x == 0 || !voxScales.HasComponent(voxLink.vox))
                {
                    return;
                }
                // UnityEngine.Profiling.Profiler.BeginSample("Detection Colliders");
                var voxScale = voxScales[voxLink.vox].scale;
                var size = body.size;
                var maxXZ = math.max(size.x, size.z);
                var position = translation.Value;
                // Reduce all point positions to last y position if hit ground
                for (int i = 0; i < voxelCollider.points.Length; i++)
                {
                    var point = voxelCollider.points[i];
                    var scaledPosition = new float3(point.localPosition.x * maxXZ, point.localPosition.y * size.y, point.localPosition.z * maxXZ);
                    var pointGlobalPosition = position + math.rotate(rotation.Value, scaledPosition);
                    voxelCollider.updatedPoints[i] = pointGlobalPosition;
                    var pointVoxelPositionX = VoxelUtilities.GetVoxelPosition(new float3(pointGlobalPosition.x, point.globalPosition.y, point.globalPosition.z), voxScale);
                    var pointVoxelPositionY = VoxelUtilities.GetVoxelPosition(new float3(point.globalPosition.x, pointGlobalPosition.y, point.globalPosition.z), voxScale);
                    var pointVoxelPositionZ = VoxelUtilities.GetVoxelPosition(new float3(point.globalPosition.x, point.globalPosition.y, pointGlobalPosition.z), voxScale);
                    var newVoxelPosition = new int3(pointVoxelPositionX.x, pointVoxelPositionY.y, pointVoxelPositionZ.z);
                    point.voxelPosition = newVoxelPosition;
                    point.globalPosition = pointGlobalPosition;
                    voxelCollider.points[i] = point;
                }
                for (int i = 0; i < voxelCollider.previousPoints.Length; i++)
                {
                    voxelCollider.previousPoints[i] = voxelCollider.points[i].globalPosition;
                }
			    // UnityEngine.Profiling.Profiler.EndSample();
            })  .WithReadOnly(voxScales)
                .ScheduleParallel(Dependency);
        }
    }
}