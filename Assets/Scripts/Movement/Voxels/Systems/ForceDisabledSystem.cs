using Unity.Entities;
using Unity.Burst;
using Zoxel.Voxels;
// when you spawn, the collision data isn't set properly, it uses displacement of positoins
// so because that displacement isn't set initially, we end up clipping through the ground
// a quick work around is positioning 

namespace Zoxel.Movement.Voxels
{
    //! Keeps Entity force disabled when in loading chunk.
    [BurstCompile, UpdateInGroup(typeof(CollisionSystemGroup))]
    public partial class ForceDisabledSystem : SystemBase
    {
        private const byte disableForceBuffer = 1; // 1
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // really need to know when chunks change!
            Dependency = Entities
                .WithNone<InitializeEntity, EntityBusy, EntityLoading>()
                .WithAll<DisableForce>()
                .ForEach((Entity e, int entityInQueryIndex, ref DisableForce disableForce, in ChunkLink chunkLink) =>
            {
                if (chunkLink.chunk.Index == 0 || HasComponent<EntityBusy>(chunkLink.chunk))
                {
                    return;
                }
                /*if (disableForce.count < disableForceBuffer)
                {
                    disableForce.count++;
                    return;
                }*/
                PostUpdateCommands.RemoveComponent<DisableForce>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<InitializeVoxelCollider>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<InitializeVoxelCollider>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<InitializeVoxelCollider>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}