﻿using Unity.Entities;

namespace Zoxel.Movement.Voxels
{
    //! Handles Physics Collisions.
    [UpdateAfter(typeof(VelocitySystem))]
    [UpdateInGroup(typeof(PhysicsSystemGroup))]
    public partial class CollisionSystemGroup : ComponentSystemGroup { }
}