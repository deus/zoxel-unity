using Unity.Entities;
using UnityEngine;

namespace Zoxel.Movement.Authoring
{
    // [GenerateAuthoringComponent]
    public struct PhysicsSettings : IComponentData
    {
        public int characterVoxelIntersectSystemDivision;   // 6
        public float characterMovementSpeed;        // 18
        public float characterRotationSpeed;        // 32
        public float bounceMultiplier;              // 0.05
        public float airFriction;                   // 2
        public float groundFriction;                // 8
        public float maxChaseDistance;              // 18
        public float airSlowMultiplier;             // 0.2
        public float jumpForce;                     // 340
        public float fallSpeed;                     // 10
        public float flyForce;                      // 24
        public float collisionFailureShift;         // 1.6
        public float sprintSpeedMultiplier;         // 1.6
        public float playerVoxelColliderResolution; // 0
        public float gravityLerpSpeed;              // 0.64;
        public bool disablePhysics;
        public bool isDrawCharacterColliders;
        public bool isAutoWalk;
    }

    public class PhysicsSettingsAuthoring : MonoBehaviour
    {
        public int characterVoxelIntersectSystemDivision;   // 6
        public float characterMovementSpeed;        // 18
        public float characterRotationSpeed;        // 32
        public float bounceMultiplier;              // 0.05
        public float airFriction;                   // 2
        public float groundFriction;                // 8
        public float maxChaseDistance;              // 18
        public float airSlowMultiplier;             // 0.2
        public float jumpForce;                     // 340
        public float fallSpeed;                     // 10
        public float flyForce;                      // 24
        public float collisionFailureShift;         // 1.6
        public float sprintSpeedMultiplier;         // 1.6
        public float playerVoxelColliderResolution; // 0
        public float gravityLerpSpeed;              // 0.64;
        public bool disablePhysics;
        public bool isDrawCharacterColliders;
        public bool isAutoWalk;
    }

    public class PhysicsSettingsAuthoringBaker : Baker<PhysicsSettingsAuthoring>
    {
        public override void Bake(PhysicsSettingsAuthoring authoring)
        {
            AddComponent(new PhysicsSettings
            {
                characterVoxelIntersectSystemDivision = authoring.characterVoxelIntersectSystemDivision,
                characterMovementSpeed = authoring.characterMovementSpeed,
                characterRotationSpeed = authoring.characterRotationSpeed,
                bounceMultiplier = authoring.bounceMultiplier,
                airFriction = authoring.airFriction,
                groundFriction = authoring.groundFriction,
                maxChaseDistance = authoring.maxChaseDistance,
                airSlowMultiplier = authoring.airSlowMultiplier,
                jumpForce = authoring.jumpForce,
                fallSpeed = authoring.fallSpeed,
                flyForce = authoring.flyForce,
                collisionFailureShift = authoring.collisionFailureShift,
                sprintSpeedMultiplier = authoring.sprintSpeedMultiplier,
                playerVoxelColliderResolution = authoring.playerVoxelColliderResolution,
                gravityLerpSpeed = authoring.gravityLerpSpeed,
                disablePhysics = authoring.disablePhysics,
                isDrawCharacterColliders = authoring.isDrawCharacterColliders,
                isAutoWalk = authoring.isAutoWalk
            });       
        }
    }
}