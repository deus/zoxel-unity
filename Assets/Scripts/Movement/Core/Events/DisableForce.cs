using Unity.Entities;

namespace Zoxel.Movement
{    
    //! Disables force until the chunk linked finishes updating.
    public struct DisableForce : IComponentData
    {
        public byte count;
    }
}