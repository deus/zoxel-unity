using Unity.Entities;

namespace Zoxel.Movement
{
    //! A link to the chunk that entity is bound to.
    public struct ChunkBoundLink : IComponentData
    {
        public Entity chunk;
        public ChunkBoundLink(Entity chunk)
        {
            this.chunk = chunk;
        }
    }
}