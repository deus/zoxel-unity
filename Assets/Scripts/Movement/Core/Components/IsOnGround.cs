using Unity.Entities;

namespace Zoxel.Movement
{
    //! Is the entityy on the ground?
    public struct IsOnGround : IComponentData
    {
        public bool isOnGround;
    }
}