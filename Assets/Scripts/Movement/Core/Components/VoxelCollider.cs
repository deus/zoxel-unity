using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Movement
{
    //! Uses points around a cube to handle collisions.
    public struct VoxelCollider : IComponentData
    {
        public BlitableArray<VoxelCollisionPointXYZ> points;   // 8 points per side - for middle points and corners
        public BlitableArray<float3> previousPoints;   // 8 points per side - for middle points and corners
        public BlitableArray<float3> updatedPoints;   // what points updated too, before they were reset
        
        public void DisposeFinal()
        {
            points.DisposeFinal();
            previousPoints.DisposeFinal();
            updatedPoints.DisposeFinal();
        }

        public void Dispose()
        {
            points.Dispose();
            previousPoints.Dispose();
            updatedPoints.Dispose();
        }
        
        public void InitializeBasic()
        {
            points = new BlitableArray<VoxelCollisionPointXYZ>(8, Allocator.Persistent);
            previousPoints = new BlitableArray<float3>(8, Allocator.Persistent);
            updatedPoints = new BlitableArray<float3>(8, Allocator.Persistent);
            points[0] = new VoxelCollisionPointXYZ(new float3(-1, -1, -1));
            points[1] = new VoxelCollisionPointXYZ(new float3(-1, -1, 1));
            points[2] = new VoxelCollisionPointXYZ(new float3(1, -1, 1));
            points[3] = new VoxelCollisionPointXYZ(new float3(1, -1, -1));
            points[4] = new VoxelCollisionPointXYZ(new float3(-1, 1, -1));
            points[5] = new VoxelCollisionPointXYZ(new float3(-1, 1, 1));
            points[6] = new VoxelCollisionPointXYZ(new float3(1, 1, 1));
            points[7] = new VoxelCollisionPointXYZ(new float3(1, 1, -1));
        }
        
        public void Initialize(float resolution)
        {
            // var resolution = 0.1f;
            var points2 = new NativeList<float3>();
            for (float i = -1f; i <= 1f; i += resolution)
            {
                for (float j = -1f; j <= 1f; j += resolution)
                {
                    for (float k = -1f; k <= 1f; k += resolution)
                    {
                        // check if on edge of collider
                        if (i == -1f || i == 1f || j == -1f || j == 1f || k == -1f || k == 1f)
                        {
                            points2.Add(new float3(i, j, k));
                        }
                    }
                }
            }
            points = new BlitableArray<VoxelCollisionPointXYZ>(points2.Length, Allocator.Persistent);
            previousPoints = new BlitableArray<float3>(points2.Length, Allocator.Persistent);
            updatedPoints = new BlitableArray<float3>(points2.Length, Allocator.Persistent);
            for (var i = 0; i < points2.Length; i++)
            {
                points[i] = new VoxelCollisionPointXYZ(points2[i]);
            }
            points2.Dispose();
        }

        public void DisplacePoints(float3 offset)
        {
            for (int i = 0; i < updatedPoints.Length; i++)
            {
                var point2 = points[i];
                point2.globalPosition += offset;
                points[i] = point2;
                var point = updatedPoints[i];
                point += offset;
                updatedPoints[i] = point;
                var point3 = previousPoints[i];
                point3 += offset;
                previousPoints[i] = point3;
            }
        }
    }
}
