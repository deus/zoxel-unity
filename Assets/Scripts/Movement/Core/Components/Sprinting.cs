using Unity.Entities;

namespace Zoxel.Movement
{
    //! Uses energy but causes the character to move faster
    public struct Sprinting : IComponentData { }
}