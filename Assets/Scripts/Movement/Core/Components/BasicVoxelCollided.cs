using Unity.Entities;

namespace Zoxel.Movement
{
    //! A collision event for BasicVoxelCollider with new voxel.
    public struct BasicVoxelCollided : IComponentData { }
}