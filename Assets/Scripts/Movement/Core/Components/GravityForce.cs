using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Movement
{
    //! A force that pulls an entity towards a very heavy object (planet).
    public struct GravityForce : IComponentData
    {
        public float3 acceleration;

        public GravityForce(float3 acceleration)
        {
            this.acceleration = acceleration;
        }
    }
}
