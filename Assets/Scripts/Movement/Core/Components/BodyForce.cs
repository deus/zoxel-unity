﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Movement
{
    public struct InitializeVoxelCollider : IComponentData { }
    //! Force that gets added to the body over a frame. It gets reset after applied.
    public struct BodyForce : IComponentData
    {
        //! The displacement per second of position.
        public float3 velocity;
        //! Adds to velocity every frame, based on forces acting upon the entity.
        public float3 acceleration;

        public BodyForce(float3 velocity)
        {
            this.velocity = velocity;
            this.acceleration = float3.zero;
        }

        public void Clear()
        {
            ResetVelocity();
            ResetAcceleration();
        }

        public void ResetVelocity()
        {
            velocity.x = 0;
            velocity.y = 0;
            velocity.z = 0;
        }

        public void ResetAcceleration()
        {
            acceleration.x = 0;
            acceleration.y = 0;
            acceleration.z = 0;
        }
    }
}
