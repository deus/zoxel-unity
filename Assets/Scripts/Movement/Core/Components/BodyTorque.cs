﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Movement
{
    public struct BodyTorque : IComponentData
    {
        public float3 angle;
        public float3 velocity;
        public float3 torque;
    }
}
