using Unity.Entities;

namespace Zoxel.Movement
{
    public struct Jumping : IComponentData
    {
        public byte isJumping;
    }
}