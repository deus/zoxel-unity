using Unity.Entities;

namespace Zoxel.Movement
{
    //! A tag for an entity that is bound to a chunk.
    public struct ChunkBounded : IComponentData { }
}