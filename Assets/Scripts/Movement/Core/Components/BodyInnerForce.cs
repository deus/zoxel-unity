﻿using Unity.Entities;

namespace Zoxel.Movement
{
    public struct BodyInnerForce : IComponentData
    {
        public float maxVelocity;
        public float movementForce;
        public float movementTorque;
    }
}
