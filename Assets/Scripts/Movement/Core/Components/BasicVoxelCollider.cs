using Unity.Entities;

namespace Zoxel.Movement
{
    //! A basic collider that does point collision detection.
    public struct BasicVoxelCollider : IComponentData
    {
        public byte init;
        public int3 hitPosition;
        public int3 hitChunkPosition;
        public byte hitVoxelType;
        public Entity chunk;
        public VoxelCollisionPoint hit;
    }
}