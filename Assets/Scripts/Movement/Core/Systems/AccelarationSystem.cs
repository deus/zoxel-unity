using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;

namespace Zoxel.Movement
{
    //! Adds acceleration to velocity and clears acceleration.
    [BurstCompile, UpdateInGroup(typeof(PhysicsSystemGroup))]
    public partial class AccelarationSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            float deltaTime = World.Time.DeltaTime;
            if (deltaTime >= (1 / 30f))
            {
                deltaTime = (1 / 30f);
            }
            Entities
                .WithChangeFilter<BodyForce>()
                .WithNone<InitializeEntity, DeadEntity, DestroyEntity>()
                .WithNone<DisableForce>()
                .ForEach((ref BodyForce bodyForce) =>
            {
                bodyForce.velocity += bodyForce.acceleration * deltaTime;
            }).ScheduleParallel();
            Entities
                .WithChangeFilter<BodyForce>()
                .WithNone<InitializeEntity, DeadEntity, DestroyEntity>()
                .ForEach((ref BodyForce bodyForce) =>
            {
                bodyForce.ResetAcceleration();
            }).ScheduleParallel();
		}
    }
}