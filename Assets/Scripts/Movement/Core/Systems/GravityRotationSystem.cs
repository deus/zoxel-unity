using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Movement
{
    //! Move the first person camera around with player input.
    /**
    *   Sets camera input to characters one, sets characters angle to camera one?
    *   \todo This has issues during lerping due to boundary issues. And until fully rotated it will have issues rotating around Y axis.
    *   \todo Usses gravityQuadrant.rotation in another system that works on npcs too.
    */
    [BurstCompile, UpdateInGroup(typeof(PhysicsSystemGroup))]
    public partial class GravityRotationSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Dependency = Entities
                .WithNone<DestroyEntity, InitializeEntity, LerpGravityQuadrant>()
                .WithNone<DisableGravityRotation>()
                .WithChangeFilter<GravityQuadrant>()
                .ForEach((ref Rotation rotation, in GravityQuadrant gravityQuadrant) =>
            {
                var planetSideCharacterRotation = math.mul(math.inverse(gravityQuadrant.rotation), rotation.Value);
                var pitchRotation = planetSideCharacterRotation.GetAxisRotationY();
                rotation.Value = math.mul(gravityQuadrant.rotation, pitchRotation);
            }).ScheduleParallel(Dependency);
        }
    }
}