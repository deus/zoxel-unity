using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Movement
{
    //! Displaces Translation by Velocity and clears velocity.
    [UpdateAfter(typeof(AccelarationSystem))]
    [BurstCompile, UpdateInGroup(typeof(PhysicsSystemGroup))]
    public partial class VelocitySystem : SystemBase
    {
        public static float DeltaTime;

        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DisableForce>()
                .ForEach((ref BodyForce bodyForce) =>
            {
                bodyForce.ResetVelocity();
            }).ScheduleParallel();
            Entities
                .WithAll<InitializeVoxelCollider>()
                .ForEach((ref BodyForce bodyForce) =>
            {
                bodyForce.ResetVelocity();
            }).ScheduleParallel();
            float deltaTime = World.Time.DeltaTime;
            if (deltaTime >= (1 / 30f))
            {
                //UnityEngine.Debug.LogError("FPS Less then 30.");
                deltaTime = (1 / 30f);
            }
            VelocitySystem.DeltaTime = deltaTime;
            // remove dead entity, and make this depend on type of entity with dyingEntity events
            Dependency = Entities
                .WithNone<DisableForce, InitializeVoxelCollider>()
                .WithNone<InitializeEntity, DeadEntity, DestroyEntity>()
                .ForEach((ref Translation translation, in BodyForce bodyForce) =>
            {
                translation.Value += bodyForce.velocity * deltaTime;
            }).ScheduleParallel(Dependency);
		}
    }
}