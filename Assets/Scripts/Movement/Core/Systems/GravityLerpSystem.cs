using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement.Authoring;

namespace Zoxel.Movement
{
    //! Lerps rotation over time.
    [BurstCompile, UpdateInGroup(typeof(PhysicsSystemGroup))]
    public partial class GravityLerpSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<PhysicsSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var physicsSettings = GetSingleton<PhysicsSettings>();
            var elapsedTime = World.Time.ElapsedTime;
            var gravityLerpSpeed = physicsSettings.gravityLerpSpeed;    // 0.3f
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, DeadEntity, DestroyEntity>()
                .WithNone<DisableForce, FlyMode>()
                .ForEach((Entity e, int entityInQueryIndex, ref GravityQuadrant gravityQuadrant, in LerpGravityQuadrant lerpGravityQuadrant) =>
            {
                var lerpTime = math.min(1, (float)(elapsedTime - lerpGravityQuadrant.timeStarted) / gravityLerpSpeed);
                gravityQuadrant.rotation = QuaternionHelpers.nlerp(lerpGravityQuadrant.beginRotation, lerpGravityQuadrant.targetRotation, lerpTime);
                if (elapsedTime - lerpGravityQuadrant.timeStarted >= gravityLerpSpeed)
                {
                    gravityQuadrant.rotation = lerpGravityQuadrant.targetRotation;
                    PostUpdateCommands.RemoveComponent<LerpGravityQuadrant>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<DestroyEntity, InitializeEntity, DisableForce>()
                .ForEach((ref Rotation rotation, in GravityQuadrant gravityQuadrant, in LerpGravityQuadrant lerpGravityQuadrant) =>
            {
                var lerpTime = math.min(1, (float)(elapsedTime - lerpGravityQuadrant.timeStarted) / gravityLerpSpeed);
                var pitch = QuaternionHelpers.slerp(lerpGravityQuadrant.beginPitch, lerpGravityQuadrant.targetPitch, lerpTime);
                rotation.Value = math.mul(gravityQuadrant.rotation, pitch);
            }).ScheduleParallel(Dependency);
            /*Entities
                .WithNone<InitializeEntity, DeadEntity, DestroyEntity>()
                .WithNone<DisableForce, FlyMode>()
                .ForEach((in Translation translation, in GravityQuadrant gravityQuadrant, in LerpGravityQuadrant lerpGravityQuadrant) =>
            {
                //UnityEngine.Debug.DrawLine(translation.Value, translation.Value + math.mul(lerpGravityQuadrant.beginRotation, new float3(0, 1f, 0)), UnityEngine.Color.red);
                //UnityEngine.Debug.DrawLine(translation.Value, translation.Value + math.mul(lerpGravityQuadrant.targetRotation, new float3(0, 1f, 0)), UnityEngine.Color.cyan);
                var offset = new float3(0.1f, 0.1f, 0.1f);
                UnityEngine.Debug.DrawLine(translation.Value + offset, translation.Value + offset + math.mul(lerpGravityQuadrant.beginRotation, new float3(0, 0, 1f)), UnityEngine.Color.black, 6f);
                UnityEngine.Debug.DrawLine(translation.Value + offset, translation.Value + offset + math.mul(lerpGravityQuadrant.beginRotation, new float3(0, 1f, 0f)), UnityEngine.Color.blue, 6f);
                // new
                UnityEngine.Debug.DrawLine(translation.Value, translation.Value + math.mul(lerpGravityQuadrant.targetRotation, new float3(0, 0, 1f)), UnityEngine.Color.white, 6f);
                UnityEngine.Debug.DrawLine(translation.Value, translation.Value + math.mul(lerpGravityQuadrant.targetRotation, new float3(0, 1f, 0f)), UnityEngine.Color.cyan, 6f);
            }).WithoutBurst().Run();*/
		}
    }
}