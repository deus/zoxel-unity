﻿/*using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;

using Unity.Burst;

namespace Zoxel.Movement
{
    [UpdateAfter(typeof(PositionalForceSystem))]
    [BurstCompile, UpdateInGroup(typeof(PhysicsSystemGroup))]
    public partial class RotationalForceSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            // float deltaTime;
            Entities.ForEach((ref BodyTorque body, ref Translation position, ref Rotation rotation) =>
            {
                body.velocity += body.torque / (4 * 360);
                body.angle += body.velocity;
                body.torque = new float3();
                body.velocity *= 0.8f;
                rotation.Value = quaternion.EulerXYZ(body.angle);
            }).ScheduleParallel();
		}
    }
}*/