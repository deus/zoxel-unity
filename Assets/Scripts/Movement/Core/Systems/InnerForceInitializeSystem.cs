using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Movement.Authoring;

namespace Zoxel.Movement
{
    //! Initializes some basic data in characters, should be done in prefab instead.
    [BurstCompile, UpdateInGroup(typeof(PhysicsSystemGroup))]
    public partial class InnerForceInitializeSystem : SystemBase
    {
        protected override void OnCreate()
        {
            RequireForUpdate<PhysicsSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var physicsSettings = GetSingleton<PhysicsSettings>();
            var characterMovementSpeed = physicsSettings.characterMovementSpeed;
            var characterRotationSpeed = physicsSettings.characterRotationSpeed;
            var fallSpeed = new float3(0, -physicsSettings.fallSpeed, 0);
            Dependency = Entities
                .WithAll<InitializeEntity, Character, GravityForce>()
                .ForEach((ref GravityForce gravityForce) =>
            {
                gravityForce.acceleration = fallSpeed;
            }).ScheduleParallel(Dependency);
            Dependency = Entities
                .WithAll<InitializeEntity, Character, BodyInnerForce>()
                .ForEach((ref BodyInnerForce bodyInnerForce) =>
            {
                bodyInnerForce.movementForce = characterMovementSpeed;
                bodyInnerForce.maxVelocity = characterMovementSpeed;
                bodyInnerForce.movementTorque = characterRotationSpeed;
            }).ScheduleParallel(Dependency);
            // Physics
            Dependency = Entities
                .WithNone<PlayerCharacter>()
                .WithAll<InitializeEntity, VoxelCollider>()
                .ForEach((ref VoxelCollider voxelCollider) =>
            {
                voxelCollider.InitializeBasic();
            }).ScheduleParallel(Dependency);
            var playerVoxelColliderResolution = physicsSettings.playerVoxelColliderResolution;
            Dependency = Entities
                .WithAll<InitializeEntity, VoxelCollider, PlayerCharacter>()
                .ForEach((ref VoxelCollider voxelCollider) =>
            {
                if (playerVoxelColliderResolution == 0)
                {
                    voxelCollider.InitializeBasic();
                }
                else
                {
                    voxelCollider.Initialize(playerVoxelColliderResolution);
                }
            }).ScheduleParallel(Dependency);
        }
    }
}