using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement.Authoring;

namespace Zoxel.Movement
{
    //! Handles jump ending and adds force during it.
    [BurstCompile, UpdateInGroup(typeof(PhysicsSystemGroup))]
    public partial class JumpSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<PhysicsSettings>();
        }
        
		[BurstCompile]
        protected override void OnUpdate()
        {
            var physicsSettings = GetSingleton<PhysicsSettings>();
            var delta = (float) World.Time.DeltaTime;
            var jumpForce = physicsSettings.jumpForce;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref Jumping jumping, ref BodyForce bodyForce, in IsOnGround isOnGround,
                in Rotation rotation) =>    // , in GravityQuadrant gravityQuadrant
            {
                if (isOnGround.isOnGround) //  && math.abs(bodyForce.velocity.y) < innerBody.maxVelocity * 0.1f)
                {
                    if (jumping.isJumping == 0)
                    {
                        jumping.isJumping = 1;
                        bodyForce.acceleration += math.mul(rotation.Value, new float3(0, jumpForce / (delta * 60), 0));
                    }
                    else
                    {
                        // remove the second time we reach the ground
                        PostUpdateCommands.RemoveComponent<Jumping>(entityInQueryIndex, e);
                    }
                }
                else
                {
                    if (jumping.isJumping == 0)
                    {
                        PostUpdateCommands.RemoveComponent<Jumping>(entityInQueryIndex, e);
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}