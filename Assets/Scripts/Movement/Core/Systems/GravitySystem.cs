using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Movement
{
    //! Adds gravity based on quadrant the entity is in.
    [BurstCompile, UpdateInGroup(typeof(PhysicsSystemGroup))]
    public partial class GravitySystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<InitializeEntity, DeadEntity, DestroyEntity>()
                .WithNone<DisableForce, FlyMode>()
                .ForEach((ref BodyForce bodyForce, in GravityForce gravityForce, in GravityQuadrant gravityQuadrant) =>
            {
                // base gravity on quadrant
                if (gravityQuadrant.quadrant == PlanetSide.Up)
                {
                    bodyForce.acceleration += gravityForce.acceleration;
                }
                else if (gravityQuadrant.quadrant == PlanetSide.Down)
                {
                    bodyForce.acceleration -= gravityForce.acceleration;
                }
                else if (gravityQuadrant.quadrant == PlanetSide.Left)
                {
                    bodyForce.acceleration -= new float3(gravityForce.acceleration.y, 0, 0);
                }
                else if (gravityQuadrant.quadrant == PlanetSide.Right)
                {
                    bodyForce.acceleration += new float3(gravityForce.acceleration.y, 0, 0);
                }
                else if (gravityQuadrant.quadrant == PlanetSide.Backward)
                {
                    bodyForce.acceleration -= new float3(0, 0, gravityForce.acceleration.y);
                }
                else if (gravityQuadrant.quadrant == PlanetSide.Forward)
                {
                    bodyForce.acceleration += new float3(0, 0, gravityForce.acceleration.y);
                }
            }).ScheduleParallel();
            Entities
                .WithNone<InitializeEntity, DeadEntity, DestroyEntity>()
                .WithNone<DisableForce, FlyMode, GravityQuadrant>()  // , OnTeleported
                .ForEach((ref BodyForce bodyForce, in GravityForce gravityForce, in Translation translation) =>
            {
                // base gravity on quadrant
                bodyForce.acceleration += gravityForce.acceleration;
            }).ScheduleParallel();
		}
    }
}