using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Movement
{
    // Cleans up VoxelColliders.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class VoxelColliderDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, VoxelCollider>()
                .ForEach((in VoxelCollider voxelCollider) =>
			{
                voxelCollider.DisposeFinal();
			}).ScheduleParallel();
        }
    }
}