using Unity.Entities;

namespace Zoxel.Movement
{
    public struct VoxelCollisionPointSides
    {
        public byte sides;
        
        //! Input should be 0 or 1.
        public void SetSide(byte input, byte sideIndex)
        {
			sides |= (byte)(input << sideIndex);
        }

        //! Ouput is 0 or 1.
		public byte GetSide(byte sideIndex)
		{
			if ((sides & (1 << sideIndex)) > 0) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
    }
}