using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Movement
{
    //! A point in space that is used to intersect with voxel colliders.
    public struct VoxelCollisionPointXYZ
    {
        public float3 localPosition;
        public float3 globalPosition;
        public int3 voxelPosition;
        public VoxelCollisionPoint hitX;
        public VoxelCollisionPoint hitY;
        public VoxelCollisionPoint hitZ; 

        public VoxelCollisionPointXYZ(float3 localPosition)
        {
            this.localPosition = localPosition;
            this.globalPosition = new float3();
            this.voxelPosition = new int3();
            this.hitX = new VoxelCollisionPoint();
            this.hitY = new VoxelCollisionPoint();
            this.hitZ = new VoxelCollisionPoint();
        }
        
        public void Reset()
        {
            hitX.Reset();
            hitY.Reset();
            hitZ.Reset();
        }

        public void SetDidHit(byte value, byte side)
        {
            if (side == PlanetAxis.Horizontal)
            {
                hitX.didHit = value;
            }
            else if (side == PlanetAxis.Vertical)
            {
                hitY.didHit = value;
            }
            else if (side == PlanetAxis.Depth)
            {
                hitZ.didHit = value;
            }
        }

        public void SetIsSolidWithin(byte value, byte side)
        {
            if (side == PlanetAxis.Horizontal)
            {
                hitX.isSolidWithin = value;
            }
            else if (side == PlanetAxis.Vertical)
            {
                hitY.isSolidWithin = value;
            }
            else if (side == PlanetAxis.Depth)
            {
                hitZ.isSolidWithin = value;
            }
        }
    }
}