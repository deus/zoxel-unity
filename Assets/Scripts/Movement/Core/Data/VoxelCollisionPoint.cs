using Unity.Entities;

namespace Zoxel.Movement
{
    //! Sine data for a collision into a voxel.
    public struct VoxelCollisionPoint
    {
        public byte didHit;
        public byte isSolidWithin;

        public void Reset()
        {
            didHit = 0;
            isSolidWithin = 0;
        }
    }
}