using Unity.Entities;
using Unity.Burst;
using Zoxel.Movement;
using Zoxel.Cameras;

namespace Zoxel.Transforms.IO
{
    //! Saves NPCCharacter's Transform data into a file.
    [UpdateBefore(typeof(TransformSaveWriteSystem))]
    [BurstCompile, UpdateInGroup(typeof(TransformIOSystemGroup))]
    public partial class TransformSaveSystem : SystemBase
    {
        const double saveRate = 3;

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            
            Entities
                .WithNone<InitializeEntity, DeadEntity, DestroyEntity>()
                .WithNone<DisableForce, LoadTransform, CameraLink>()
                .WithNone<InitializeSaver, CharacterMakerCharacter>()
                .WithAll<SaveTransform>()
                .ForEach((ref TransformSaverData transformSaverData, in Translation translation, in Rotation rotation, in GravityQuadrant gravityQuadrant) =>
            {
                var roundedPosition = new int3((int) translation.Value.x, (int) translation.Value.y, (int) translation.Value.z);
                SaveTransform(ref transformSaverData, elapsedTime, roundedPosition, in translation, in rotation, in gravityQuadrant);
            }).ScheduleParallel();
            Entities
                .WithNone<InitializeEntity, DeadEntity, DestroyEntity>()
                .WithNone<DisableForce, LoadTransform, CameraLink>()
                .WithNone<InitializeSaver, CharacterMakerCharacter>()
                .WithChangeFilter<Translation>()
                .ForEach((ref TransformSaverData transformSaverData, in Translation translation, in Rotation rotation, in GravityQuadrant gravityQuadrant) =>
            {
                if (transformSaverData.didUpdate == 0 && elapsedTime - transformSaverData.lastSavedTime >= saveRate)
                {
                    var roundedPosition = new int3((int) translation.Value.x, (int) translation.Value.y, (int) translation.Value.z);
                    if (transformSaverData.lastSavedPosition == roundedPosition)
                    {
                        return;
                    }
                    SaveTransform(ref transformSaverData, elapsedTime, roundedPosition, in translation, in rotation, in gravityQuadrant);
                }
            }).ScheduleParallel();
        }

        private static void SaveTransform(ref TransformSaverData transformSaverData, double elapsedTime, int3 roundedPosition,
            in Translation translation, in Rotation rotation, in GravityQuadrant gravityQuadrant)
        {
            transformSaverData.didUpdate = 1;
            transformSaverData.lastSavedPosition = roundedPosition;
            transformSaverData.lastSavedTime = elapsedTime;
            ByteUtil.SetFloat3(ref transformSaverData.bytes, 0, translation.Value);
            ByteUtil.SetFloat4(ref transformSaverData.bytes, 12, rotation.Value.value);
            transformSaverData.bytes[28] = gravityQuadrant.quadrant;
        }
    }
}