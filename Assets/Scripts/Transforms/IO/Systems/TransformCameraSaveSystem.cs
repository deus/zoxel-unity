using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Cameras;

namespace Zoxel.Transforms.IO
{
    //! Saves a character with the additional camera rotation data.
    /**
    *   Note: Saving has to happen after movement systems.
    */
    [UpdateBefore(typeof(TransformSaveWriteSystem))]
    [BurstCompile, UpdateInGroup(typeof(TransformIOSystemGroup))]
    public partial class TransformCameraSaveSystem : SystemBase
    {
        const double saveRate = 1.0;
        private EntityQuery processQuery;
        private EntityQuery processQuery2;
        private EntityQuery camerasQuery;

        protected override void OnCreate()
        {
            camerasQuery = GetEntityQuery(
                ComponentType.ReadOnly<Camera>(),
                ComponentType.ReadWrite<LocalRotation>(),
                ComponentType.Exclude<DestroyEntity>());
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (processQuery.IsEmpty && processQuery2.IsEmpty) return;
            var elapsedTime = World.Time.ElapsedTime;
            var cameraEntities = camerasQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var localRotations = GetComponentLookup<LocalRotation>(false);
            cameraEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery2)
                .WithNone<DestroyEntity, DeadEntity, InitializeEntity>()
                .WithNone<InitializeSaver, CharacterMakerCharacter>()
                .WithNone<LoadTransform, DisableSaving>()
                .WithAll<SaveTransform>()
                .ForEach((ref TransformSaverData transformSaverData, in Translation translation, in Rotation rotation, in GravityQuadrant gravityQuadrant, in CameraLink cameraLink) =>
            {
                var roundedPosition = new int3((int) translation.Value.x, (int) translation.Value.y, (int) translation.Value.z);
                SaveTransform(ref transformSaverData, elapsedTime, roundedPosition, in translation, in rotation, in gravityQuadrant, in cameraLink, in localRotations);
            })  .WithReadOnly(localRotations)
                .ScheduleParallel();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, DeadEntity, InitializeEntity>()
                .WithNone<InitializeSaver, CharacterMakerCharacter>()
                .WithNone<LoadTransform, DisableSaving>()
                .WithChangeFilter<Translation>()
                .ForEach((ref TransformSaverData transformSaverData, in Translation translation, in Rotation rotation, in GravityQuadrant gravityQuadrant, in CameraLink cameraLink) =>
            {
                if (transformSaverData.didUpdate == 0 && elapsedTime - transformSaverData.lastSavedTime >= saveRate)
                {
                    var roundedPosition = new int3((int) translation.Value.x, (int) translation.Value.y, (int) translation.Value.z);
                    if (!localRotations.HasComponent(cameraLink.camera))    // transformSaverData.lastSavedPosition == roundedPosition || 
                    {
                        return;
                    }
                    SaveTransform(ref transformSaverData, elapsedTime, roundedPosition, in translation, in rotation, in gravityQuadrant, in cameraLink, in localRotations);
                }
            })  .WithReadOnly(localRotations)
                .ScheduleParallel();
        }

        private static void SaveTransform(ref TransformSaverData transformSaverData, double elapsedTime, int3 roundedPosition,
            in Translation translation, in Rotation rotation, in GravityQuadrant gravityQuadrant, in CameraLink cameraLink,
            in ComponentLookup<LocalRotation> localRotations)
        {
                var cameraLocalRotation = localRotations[cameraLink.camera];
                transformSaverData.lastSavedPosition = roundedPosition;
                transformSaverData.didUpdate = 1;
                transformSaverData.lastSavedTime = elapsedTime;
                ByteUtil.SetFloat3(ref transformSaverData.bytes, 0, translation.Value);
                ByteUtil.SetFloat4(ref transformSaverData.bytes, 12, rotation.Value.value);
                transformSaverData.bytes[28] = gravityQuadrant.quadrant;
                ByteUtil.SetFloat4(ref transformSaverData.bytes, 29, cameraLocalRotation.rotation.value);
        }
    }
}