﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;

using Zoxel.Transforms;
using Zoxel.Movement;

namespace Zoxel.Transforms.IO
{
    //! Loads the transform into a file.
    /**
    *   - Load System -
    */
    [BurstCompile, UpdateInGroup(typeof(TransformIOSystemGroup))]
    public partial class TransformLoadSystem : SystemBase
    {
        private const string filename = "Translation.zox";

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithNone<CameraLink>()
                .ForEach((ref LoadTransform loadTransform, ref Translation translation, ref Rotation rotation, ref GravityQuadrant gravityQuadrant, in EntitySaver entitySaver) =>
            {
                if (loadTransform.loadPath.Length == 0)
                {
                    loadTransform.loadPath = new Text(entitySaver.GetPath() + filename);
                }
                loadTransform.reader.UpdateOnMainThread(in loadTransform.loadPath);
            }).WithoutBurst().Run();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity, InitializeSaver>()
                .WithNone<CameraLink>()
                .WithAll<EntitySaver>()
                .ForEach((Entity e, int entityInQueryIndex, ref LoadTransform loadTransform, ref Translation translation, ref Rotation rotation, ref GravityQuadrant gravityQuadrant) =>
            {
                if (loadTransform.reader.IsReadFileInfoComplete())
                {
                    if (loadTransform.reader.DoesFileExist())
                    {
                        loadTransform.reader.state = AsyncReadState.StartOpenFile;
                    }
                    else
                    {
                        loadTransform.Dispose();
                        PostUpdateCommands.RemoveComponent<LoadTransform>(entityInQueryIndex, e);
                    }
                }
                else if (loadTransform.reader.IsReadFileComplete())
                {
                    if (loadTransform.reader.DidFileReadSuccessfully())
                    {
                        var bytes = loadTransform.reader.GetBytes();
                        if (bytes.Length == TransformSaverDataInitializeSystem.fileSizeWithoutCamera)
                        {
                            translation.Value = ByteUtil.GetFloat3(in bytes, 0);
                            rotation.Value = new quaternion(ByteUtil.GetFloat4(in bytes, 12));
                            gravityQuadrant.SetQuadrant(bytes[28]);
                            // check if valid!
                            if (!MathUtil.IsValid(translation.Value))
                            {
                                // UnityEngine.Debug.LogError("Loaded Invalid Transform.");
                                translation.Value = new float3();
                            }
                            if (!MathUtil.IsValid(rotation.Value.value))
                            {
                                // UnityEngine.Debug.LogError("Loaded Invalid Rotation.");
                                rotation.Value = quaternion.identity;
                            }
                        }
                    }
                    // Dispose
                    loadTransform.Dispose();
                    PostUpdateCommands.RemoveComponent<LoadTransform>(entityInQueryIndex, e);
                }
                // UnityEngine.Debug.LogError("Loaded Unique Items [" + count + "] at: " + filepath);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}