using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Transforms.IO
{ 
    //! Removes SaveTransform event after use.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(TransformIOSystemGroup))]
    public partial class TransformSaveEndSystem : SystemBase
    {
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            processQuery = GetEntityQuery(ComponentType.ReadOnly<SaveTransform>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<SaveTransform>(processQuery);
        }
    }
}