using Unity.Entities;
using System.IO;

namespace Zoxel.Transforms.IO
{
    //! Saves Transform data to a file.
    [UpdateInGroup(typeof(TransformIOSystemGroup))]
    public partial class TransformSaveWriteSystem : SystemBase
    {
        const string fileName = "Translation.zox";

        protected override void OnUpdate() {
            Entities
                .WithNone<DestroyEntity, DeadEntity, DisableSaving>()
                .ForEach((ref TransformSaverData transformSaverData, in EntitySaver entitySaver) => {
                if (transformSaverData.didUpdate == 1) {
                    transformSaverData.didUpdate = 0;
                    var filepath = entitySaver.GetPath() + fileName;
                    var bytesArray = transformSaverData.bytes.ToArray();
                    #if WRITE_ASYNC
                    File.WriteAllBytesAsync(filepath, bytesArray);
                    #else
                    File.WriteAllBytes(filepath, bytesArray);
                    #endif
                }
            }).WithoutBurst().Run();
        }
    }
}