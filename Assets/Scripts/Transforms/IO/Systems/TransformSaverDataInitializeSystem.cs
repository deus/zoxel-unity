using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Transforms.IO
{
    //! Initializes TransformSaverData.
    [BurstCompile, UpdateInGroup(typeof(TransformIOSystemGroup))]
    public partial class TransformSaverDataInitializeSystem : SystemBase
    {
        public const int fileSizeWithoutCamera = 29;    // 12 * 4;
        public const int fileSizeWithCamera = 45;       // 16 * 4;

        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithAll<InitializeEntity, TransformSaverData, CameraLink>()
                .ForEach((ref TransformSaverData transformSaverData) =>
            {
                transformSaverData.Initialize(fileSizeWithCamera);
            }).ScheduleParallel();
            Entities
                .WithNone<CameraLink>()
                .WithAll<InitializeEntity, TransformSaverData>()
                .ForEach((ref TransformSaverData transformSaverData) =>
            {
                transformSaverData.Initialize(fileSizeWithoutCamera);
            }).ScheduleParallel();
        }
    }
}