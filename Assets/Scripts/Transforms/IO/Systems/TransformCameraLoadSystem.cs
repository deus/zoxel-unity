using System;
using System.IO;
using System.Text;
using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Cameras;

namespace Zoxel.Transforms.IO
{
    //! Loads the transform into a file.
    /**
    *   - Load System -
    */
    [UpdateInGroup(typeof(TransformIOSystemGroup))]
    public partial class TransformCameraLoadSystem : SystemBase
    {
        private const string filename = "Translation.zox";

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<DeadEntity, DestroyEntity, InitializeSaver>()
                .WithAll<LoadTransform>()
                .ForEach((Entity e, ref Translation translation, ref Rotation rotation, ref GravityQuadrant gravityQuadrant, in EntitySaver entitySaver, in CameraLink cameraLink) =>
            {
                PostUpdateCommands.RemoveComponent<LoadTransform>(e);
                var directoryPath = entitySaver.GetPath();
                if (!Directory.Exists(directoryPath))
                {
                    UnityEngine.Debug.LogError("TransformCameraLoadSystem: Directory Doez not exizt: " + directoryPath);
                    return;
                }
                var filepath = directoryPath + filename;
                if (!File.Exists(filepath))
                {
                    UnityEngine.Debug.LogError("TransformCameraLoadSystem: File Doez not exizt: " + filepath);
                    return;
                }
                var bytes = File.ReadAllBytes(filepath);
                if (bytes.Length != TransformSaverDataInitializeSystem.fileSizeWithCamera)
                {
                    UnityEngine.Debug.LogError("TransformCameraLoadSystem: File Wrong Zize: " + bytes.Length);
                    translation.Value = new float3(0, 150, 0);
                    return;
                }
                translation.Value = new float3(ByteUtil.GetFloat3(in bytes, 0));
                rotation.Value = new quaternion(ByteUtil.GetFloat4(in bytes, 12));
                gravityQuadrant.SetQuadrant(bytes[28]);
                PostUpdateCommands.SetComponent(cameraLink.camera, new LocalRotation(ByteUtil.GetFloat4(in bytes, 29)));
                // UnityEngine.Debug.LogError("TransformCameraLoadSystem: Loaded position at: " + translation.Value);
            }).WithoutBurst().Run();
        }
    }
}
                /*var reader = new BinaryReader(File.OpenRead(filepath));
                reader.BaseStream.Position = 0;
                var bytes = reader.ReadBytes(TransformSaverDataInitializeSystem.fileSizeWithCamera);
                reader.Close();
                reader.Dispose();*/

                // gravityQuadrant.quadrant = (byte)((int) ByteUtil.GetFloat(in bytes, 44));
                // gravityQuadrant.rotation = new quaternion(ByteUtil.GetFloat4(in bytes, 48));
                // gravityQuadrant.OnLoadedRotation();
                // UnityEngine.Debug.LogError("-> Character loaded to: " +  translation.Value + " with bytes Length: " + bytes.Length);

// var floats = new float[16];
/*
Buffer.BlockCopy(bytes, 0, floats, 0, bytes.Length);
var positionX = floats[0]; // float.Parse(stream.ReadLine());   
var positionY = floats[1];
var positionZ = floats[2];
var rotationX = floats[3];
var rotationY = floats[4];
var rotationZ = floats[5];
var rotationW = floats[6];
rotation.Value = new quaternion(rotationX, rotationY, rotationZ, rotationW);
translation.Value = new float3(positionX, positionY, positionZ) + math.rotate(rotation.Value, new float3(0, 0.02f, 0));
if (HasComponent<LocalRotation>(cameraLink.camera))
{
    PostUpdateCommands.SetComponent(cameraLink.camera, new LocalRotation(floats[7], floats[8], floats[9], floats[10]));
}
gravityQuadrant.quadrant = (byte)((int)floats[11]);
gravityQuadrant.rotation = new quaternion(floats[12], floats[13], floats[14], floats[15]);*/

/// Add Components to things
///      they are updated when things update
/// Players are auto saved every 5 seconds
///     - they are marked as dirty on more important things like inventory updates etc
/// Uses Json and IOStream to load and save files
/// Load Components added onto new things for loading
/// Once loaded a player it will tell that planet to load chunks
/// Players
/// NPCs
/// Chunks - based on worlds (maps)
/// ChunkCharacters (character maps, like monsterspawner)
/*var firstPersonCamera = new FirstPersonCamera(new float3(cameraX, cameraY, 0));
if (HasComponent<FirstPersonCamera>(camera))
{
PostUpdateCommands.SetComponent(camera, firstPersonCamera);
}
if (HasComponent<LocalRotation>(camera))
{
var cameraParentSynch = EntityManager.GetComponentData<LocalRotation>(camera);
cameraParentSynch.rotation = quaternion.EulerXYZ(firstPersonCamera.rotation * degreesToRadians);
PostUpdateCommands.SetComponent(camera, cameraParentSynch);
}*/