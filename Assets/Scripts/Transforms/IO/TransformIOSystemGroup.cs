﻿using Unity.Entities;
// [BurstCompile, UpdateInGroup(typeof(TransformSystemGroup))]
// [BurstCompile, UpdateInGroup(typeof(TransformSynchSystemGroup))]

namespace Zoxel.Transforms.IO
{
    //! Save the tranforms!
    [UpdateAfter(typeof(TransformSynchSystemGroup))]
    [UpdateInGroup(typeof(TransformSystemGroup))]
    public partial class TransformIOSystemGroup : ComponentSystemGroup { }
}
