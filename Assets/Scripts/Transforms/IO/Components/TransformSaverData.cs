using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Transforms.IO
{
    //! Timed saves when moving.
    public struct TransformSaverData : IComponentData
    {
        public byte didUpdate;
        public double lastSavedTime;
        public int3 lastSavedPosition;
        public BlitableArray<byte> bytes;

        public void Initialize(int count)
        {
            this.bytes.Dispose();
            this.bytes = new BlitableArray<byte>(count, Allocator.Persistent);
        }

        public void DisposeFinal()
        {
            bytes.Dispose();
        }
    }
}