using Unity.Entities;

namespace Zoxel.Transforms.IO
{
    //! An event to save an entity's transform data.
    public struct SaveTransform : IComponentData { }
}