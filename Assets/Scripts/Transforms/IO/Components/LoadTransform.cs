using Unity.Entities;

namespace Zoxel.Transforms.IO
{
    //! An event to load an entity's transform data.
    public struct LoadTransform : IComponentData
    {
        public Text loadPath;
        public AsyncFileReader reader;

        public void Dispose()
        {
            loadPath.Dispose();
            reader.Dispose();
        }
    }
}