//! Because if you are here, you are also there..
/**
*   This covers all the information relating to spacial positioning.
*       - Position
*       - Rotation
*       - Scale
*       - Matrix
*       - Parents and Childrens
*
*   I stopped using unitys early on when it had bugs, crashes and errors with some of my builds.
*   
*   It also lacked a good Childrens system. The flexibility it brought was worth the effort.
*   
*   \todo Seperate PositionSynch into LocalPosition, LocalRotation, LocalScale.
*   \todo Add in a Matrix struct to replace unitys.
*   \todo Handle repositioning after removing from parent properly.
*/
namespace Zoxel.Transforms { }