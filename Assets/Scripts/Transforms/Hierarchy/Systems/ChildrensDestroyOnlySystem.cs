using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Transforms
{
    //! Destroys the Childrens of an entity.
    [UpdateBefore(typeof(ChildrensLinkSystem))]
    [BurstCompile, UpdateInGroup(typeof(TransformSystemGroup))]
    public partial class ChildrensDestroyOnlySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<DestroyChildren>(processQuery);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<DestroyChildren>()
                .ForEach((int entityInQueryIndex, ref Childrens childrens) =>
            {
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    var e2 = childrens.children[i];
                    if (e2.Index > 0)
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e2);
                    }
                }
                childrens.Dispose();
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}