using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Transforms
{
    //! Handles setting [integer] children.
    [UpdateBefore(typeof(ChildrensLinkSystem))]
    [BurstCompile, UpdateInGroup(typeof(TransformSystemGroup))]
    public partial class ChildrenRemoveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyChildren>()
                .ForEach((Entity e, int entityInQueryIndex, ref Childrens childrens, in RemoveChildren removeChildren) =>
            {
                PostUpdateCommands.RemoveComponent<RemoveChildren>(entityInQueryIndex, e);
                if (removeChildren.removedChildren != 0)
                {
                    childrens.RemoveChildren(removeChildren.removedChildren);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}