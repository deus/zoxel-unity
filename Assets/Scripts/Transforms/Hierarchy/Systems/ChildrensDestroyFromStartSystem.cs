using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Transforms
{
    //! Destroys the Childrens of an entity.
    [UpdateBefore(typeof(ChildrensLinkSystem))]
    [BurstCompile, UpdateInGroup(typeof(TransformSystemGroup))]
    public partial class ChildrensDestroyFromStartSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<DestroyChildrenFromStart>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<DestroyChildrenFromStart>()
                .ForEach((int entityInQueryIndex, ref Childrens childrens, in DestroyChildrenFromStart destroyChildrenFromStart) =>
            {
                if (childrens.children.Length == 0)
                {
                    return;
                }
                var count = destroyChildrenFromStart.count;
                if (count >= childrens.children.Length)
                {
                    count = (byte) childrens.children.Length;
                }
                for (int i = 0; i < count; i++)
                {
                    var e2 = childrens.children[i];
                    if (e2.Index > 0)
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e2);
                    }
                }
                childrens.RemoveChildrenFromStart(count, PostUpdateCommands, entityInQueryIndex);
            }).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}