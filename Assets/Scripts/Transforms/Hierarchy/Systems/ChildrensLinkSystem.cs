using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Transforms
{
    //! A link event system to link children after they spawn.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(TransformSystemGroup))]
    public partial class ChildrensLinkSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery childrenSpawnedIntQuery;
        private EntityQuery childrensQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            childrensQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<NewChild>(),
                ComponentType.ReadOnly<Child>(),
                ComponentType.ReadOnly<ParentLink>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            // integer based childrens
            if (!childrenSpawnedIntQuery.IsEmpty)
            {
                Entities
                    .WithStoreEntityQueryInField(ref childrenSpawnedIntQuery)
                    .WithAll<OnChildrenSpawned>()
                    .ForEach((ref Childrens childrens, in OnChildrenSpawnedInt onChildrenSpawned) =>
                {
                    if (onChildrenSpawned.spawned != 0)
                    {
                        childrens.AddNew(onChildrenSpawned.spawned);
                    }
                }).ScheduleParallel();
                PostUpdateCommands2.RemoveComponent<OnChildrenSpawnedInt>(childrenSpawnedIntQuery);
            }
            if (processQuery.IsEmpty)
            {
                return;
            }
            PostUpdateCommands2.RemoveComponent<OnChildrenSpawned>(processQuery);
            PostUpdateCommands2.RemoveComponent<NewChild>(childrensQuery);
            //! First initializes children.
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((ref Childrens childrens, in OnChildrenSpawned onChildrenSpawned) =>
            {
                if (onChildrenSpawned.spawned != 0)
                {
                    childrens.AddNew(onChildrenSpawned.spawned);
                }
            }).ScheduleParallel();
            //! For each child, using the index, sets into parents children that is passed in.
            var parentEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var parentChildrens = GetComponentLookup<Childrens>(false);
            // parentEntities.Dispose();
            Entities
                .WithNone<DestroyEntity>()
                .WithAll<NewChild>()
                .ForEach((Entity e, in Child child, in ParentLink parentLink) =>
            {
                /*if (!parentChildrens.HasComponent(parentLink.parent))
                {
                    UnityEngine.Debug.LogError("Parent not found: " + parentLink.parent.Index);
                    return;
                }
                if (parentLink.parent.Index <= 0)
                {
                    UnityEngine.Debug.LogError("2 Parent not found: " + parentLink.parent.Index);
                    return;
                }*/
                var childrens = parentChildrens[parentLink.parent];
                #if DATA_COUNT_CHECKS
                if (child.index >= childrens.children.Length)
                {
                    UnityEngine.Debug.LogError("Index out of bounds: " + child.index + " to parent " + parentLink.parent.Index);
                    return;
                }
                #endif
                childrens.children[child.index] = e;
                if (parentEntities.Length > 0) { var e2 = parentEntities[0]; }
            })  .WithNativeDisableContainerSafetyRestriction(parentChildrens)
                .WithReadOnly(parentEntities)
                .WithDisposeOnCompletion(parentEntities)
                .ScheduleParallel();
        }
    }
}

/*if (!parentChildrens.HasComponent(parentLink.parent))
{
    UnityEngine.Debug.LogError("Parent does not contain Childrens: " + e.Index + " to " + parentLink.parent.Index);
    return;
}*/
// UnityEngine.Debug.LogError(parentLink.parent.Index + " is setting child Index: " + child.index + " :: " + e.Index);
// parentChildrens[parentLink.parent] = childrens;