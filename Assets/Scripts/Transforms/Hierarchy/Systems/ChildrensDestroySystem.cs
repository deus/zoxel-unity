using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Transforms
{
    //! Destroys all children and disposes of the links using DestroyEntity event.
    /**
    *   - Destroy System -
    *   Recursively destroys all children at once.
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
	public partial class ChildrensDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery childQuery;
        private EntityQuery parentsQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            childQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Child>());
            parentsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadWrite<Childrens>());
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var childEntities = childQuery.ToEntityArray(World.UpdateAllocator.ToAllocator); // , out var jobHandleA);
            // Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var childs = GetComponentLookup<Child>(true);
			var destroyEntityDirects = GetComponentLookup<DestroyEntityDirect>(true);
            var parentEntities = parentsQuery.ToEntityArray(World.UpdateAllocator.ToAllocator); // , out var jobHandleB);
            // Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var childrens2 = GetComponentLookup<Childrens>(false);
            childEntities.Dispose();
            parentEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<DestroyEntity, Childrens>()
                .ForEach((Entity e, int entityInQueryIndex, ref Childrens childrens) =>
            {
                if (childrens.children.Length == 0)
                {
                    return;
                }
                var destroyChildren = new NativeList<Entity>();
                var destroyChildrenDirect = new NativeList<Entity>();
                DestroyChildren(e, ref childrens, ref childrens2, in childs, in destroyEntityDirects, ref destroyChildren, ref destroyChildrenDirect);
                if (destroyChildren.Length > 0)
                {
                    // var destroyChildren2 = destroyChildren.AsArray().ToArray();
                    // PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, destroyChildren2);
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, destroyChildren);
                    //destroyChildren2.Dispose();
                }
                if (destroyChildrenDirect.Length > 0)
                {
                    //var destroyChildrenDirect2 = destroyChildrenDirect.AsArray().ToArray();
                    //PostUpdateCommands.DestroyEntity(entityInQueryIndex, destroyChildrenDirect2);
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, destroyChildrenDirect);
                    //destroyChildrenDirect2.Dispose();
                }
                destroyChildren.Dispose();
                destroyChildrenDirect.Dispose();
                if (childEntities.Length > 0) { var e2 = childEntities[0]; }
                if (parentEntities.Length > 0) { var e2 = parentEntities[0]; }
			})  .WithReadOnly(childs)
                .WithReadOnly(destroyEntityDirects)
                .WithNativeDisableContainerSafetyRestriction(childrens2) // .WithReadOnly(childrens2)
                .WithReadOnly(childEntities)
                .WithDisposeOnCompletion(childEntities)
                .WithReadOnly(parentEntities)
                .WithDisposeOnCompletion(parentEntities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static void DestroyChildren(Entity e, ref Childrens childrens, ref ComponentLookup<Childrens> childrens2,
            in ComponentLookup<Child> childs, in ComponentLookup<DestroyEntityDirect> destroyEntityDirects,
            ref NativeList<Entity> destroyChildren, ref NativeList<Entity> destroyChildrenDirect)
        {
            for (int i = 0; i < childrens.children.Length; i++)
            {
                var childEntity = childrens.children[i];
                if (childEntity.Index <= 0)
                {
                    continue;
                }
                else if (destroyEntityDirects.HasComponent(childEntity))
                {
                    destroyChildrenDirect.Add(childEntity);
                }
                else if (childs.HasComponent(childEntity))
                {
                    destroyChildren.Add(childEntity);
                    // UnityEngine.Debug.LogError(e.Index + " is Destroying childEntity Index: " + i + " :: " + childEntity.Index);
                }
                if (childrens2.HasComponent(childEntity))
                {
                    var childrens3 = childrens2[childEntity];
                    if (childrens3.children.Length != 0)
                    {
                        DestroyChildren(childEntity, ref childrens3, ref childrens2, in childs, in destroyEntityDirects, ref destroyChildren, ref destroyChildrenDirect);
                    }
                }
            }
            childrens.Dispose();
            childrens2[e] = childrens;
        }
    }
}