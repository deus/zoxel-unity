using Unity.Entities;

namespace Zoxel.Transforms
{
    //! A special case for things with more children, bless those parent.
    public struct OnChildrenSpawnedInt : IComponentData
    {
        public int spawned;

        public OnChildrenSpawnedInt(int spawned)
        {
            this.spawned = spawned;
        }
    }
}