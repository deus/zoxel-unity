using Unity.Entities;

namespace Zoxel.Transforms
{
    //! Destroy X amount of children from start of list.
    public struct DestroyChildrenFromStart : IComponentData
    {
        public byte count;

        public DestroyChildrenFromStart(byte count)
        {
            this.count = count;
        }
    }
}