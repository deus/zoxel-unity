using Unity.Entities;

namespace Zoxel.Transforms
{
    //! Removes children from the top of the stack.
    public struct RemoveChildren : IComponentData
    {
        public byte removedChildren;

        public RemoveChildren(byte removedChildren)
        {
            this.removedChildren = removedChildren;
        }
    }
}