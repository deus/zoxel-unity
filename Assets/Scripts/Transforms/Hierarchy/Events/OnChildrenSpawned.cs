using Unity.Entities;

namespace Zoxel.Transforms
{
    //! When children spawns, this adds some new children and collects them using a EntityQuery.
    public struct OnChildrenSpawned : IComponentData
    {
        public byte spawned;

        public OnChildrenSpawned(byte spawned)
        {
            this.spawned = spawned;
        }
    }
}