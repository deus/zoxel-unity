using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    //! Local transform data.
    /**
    *   Uses these to calculate positions based on parents
    */
    public struct LocalPosition : IComponentData
    {
        public float3 position;

        public LocalPosition(float3 position)
        {
            this.position = position;
        }

        // doesn't apply scales
        public float4x4 GetMatrix(quaternion localRotation, float3 localScale)
        {
            return math.mul(float4x4.Scale(localScale), new float4x4(localRotation, position));
        }
    }
}

        // put this in a seperate component
        /*public float4x4 GetLocalMatrix(quaternion localRotation, float3 localScale)
        {
            return math.mul(float4x4.Scale(localScale), new float4x4(localRotation, position));
        }
        public float4x4 GetLocalMatrix(quaternion rotation2)    // float3 scale
        {
            return math.mul(float4x4.Scale(scale), new float4x4(rotation2, position));
        }*/