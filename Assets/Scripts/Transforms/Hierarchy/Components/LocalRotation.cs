using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    //! Local rotation data.
    /**
    *   Uses these to calculate rotations based on parents
    */
    public struct LocalRotation : IComponentData
    {
        public quaternion rotation;

        public LocalRotation(float x, float y, float z, float w)
        {
            this.rotation = new quaternion(x, y, z, w);
        }

        public LocalRotation(quaternion rotation)
        {
            this.rotation = rotation;
        }

        public static LocalRotation Identity
        {
            get
            { 
                return new LocalRotation(quaternion.identity);
            }
        }
    }
}