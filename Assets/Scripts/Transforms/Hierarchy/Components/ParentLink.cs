using Unity.Entities;

namespace Zoxel.Transforms
{
    //! Added to a child entity. Contains a link to it's parent.
    public struct ParentLink : IComponentData
    {
        public Entity parent;

        public ParentLink(Entity parent)
        {
            this.parent = parent;
        }
    }
}