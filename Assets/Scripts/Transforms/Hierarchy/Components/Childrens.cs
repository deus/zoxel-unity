﻿using Unity.Entities;
using Unity.Collections;
//! \todo Rename namespace to Hierarchy!

namespace Zoxel.Transforms
{
    //! Holds links to children entities.
    public struct Childrens : IComponentData
    {
        //! A simple list of children of an Entity.
        public BlitableArray<Entity> children;

        public void DisposeFinal()
        {
            children.DisposeFinal();
        }

        public void Dispose()
        {
            children.Dispose();
        }

        public void AddNew(byte count)
        {
            var children2 = new BlitableArray<Entity>(children.Length + count, Allocator.Persistent, children);
            Dispose();
            children = children2;
        }

        public void AddNew(int count)
        {
            var children2 = new BlitableArray<Entity>(children.Length + count, Allocator.Persistent, children);
            Dispose();
            children = children2;
        }

        public void Add(Entity newChild)
        {
            var childrens2 = new BlitableArray<Entity>(children.Length + 1, Allocator.Persistent, children);
            childrens2[children.Length] = newChild;
            Dispose();
            children = childrens2;
        }

        public void RemoveChild(int index, EntityCommandBuffer PostUpdateCommands)
        {
            if (children.Length == 0)
            {
                return;
            }
            PostUpdateCommands.AddComponent<DestroyEntity>(children[index]);
            var children2 = new BlitableArray<Entity>(children.Length - 1, Allocator.Persistent, children);
            for (int i = index; i < children2.Length; i++)
            {
                var e2 = children[i + 1];
                children2[i] = e2;
                PostUpdateCommands.SetComponent(e2, new Child(i));
            }
            Dispose();
            children = children2;
        }

        public void RemoveChildren(byte childrenRemoved)
        {
            var children2 = new BlitableArray<Entity>(children.Length - ((int) childrenRemoved), Allocator.Persistent, children);
            Dispose();
            children = children2;
        }

        public void RemoveChildrenFromStart(byte childrenRemoved, EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex)
        {
            var children2 = new BlitableArray<Entity>(children.Length - ((int) childrenRemoved), Allocator.Persistent, children, childrenRemoved);
            // set new indexes
            for (int i = 0; i < children2.Length; i++)
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, children2[i], new Child(i));
            }
            Dispose();
            children = children2;
        }

        public NativeList<Entity> GetChildren<T>(EntityManager EntityManager) where T : IComponentData
        {
            return GetChildrenCore<T>(EntityManager, this);
        }

        private NativeList<Entity> GetChildrenCore<T>(EntityManager EntityManager, Childrens moreChildren) where T : IComponentData
        {
            var entities = new NativeList<Entity>();
            for (int i = 0; i < moreChildren.children.Length; i++)
            {
                if (moreChildren.children[i].Index > 0 && EntityManager.Exists(moreChildren.children[i]))
                {
                    if (EntityManager.HasComponent<T>(moreChildren.children[i]))
                    {
                        entities.Add(moreChildren.children[i]);
                    }
                    if (EntityManager.HasComponent<Childrens>(moreChildren.children[i]))
                    {
                        var childrensChildren = EntityManager.GetComponentData<Childrens>(moreChildren.children[i]);
                        entities.AddRange(GetChildrenCore<T>(EntityManager, childrensChildren));
                    }
                }
            }
            return entities;
        }
    }
}