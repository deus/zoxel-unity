
using Unity.Entities;

namespace Zoxel.Transforms
{
    //! Added to a child entity. Contains a child index.
    public struct Child : IComponentData
    {
        public int index;               // index in relation to all buttons in childrens group
        
        public Child(int index)
        {
            this.index = index;
        }
    }
}