using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    //! Local scale data.
    /**
    *   Uses these to calculate rotations based on parents
    */
    public struct LocalScale : IComponentData
    {
        public float3 scale;

        public LocalScale(float3 scale)
        {
            this.scale = scale;
        }

        public static LocalScale One
        {
            get
            { 
                return new LocalScale { scale = new float3(1,1,1)};
            }
        }
    }
}