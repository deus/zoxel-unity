using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Transforms
{
    //! Attaches to entity and sets the local position.
    [BurstCompile, UpdateInGroup(typeof(TransformSystemGroup))]
    public partial class EntityAttachSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<AttachToEntity>(processQuery);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((ref LocalPosition localPosition, ref ParentLink parentLink, in AttachToEntity attachToEntity) =>
            {
                localPosition.position = attachToEntity.localPosition;
                parentLink.parent = attachToEntity.parent;
            }).ScheduleParallel(Dependency);
        }
    }
}