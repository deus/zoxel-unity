using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    //! A local scale system.
    [UpdateAfter(typeof(OrbitParentRotationSystem))]
    [BurstCompile, UpdateInGroup(typeof(TransformSystemGroup))]
	public partial class ParentScaleSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery parentQuery;
        private EntityQuery unityParentQuery;

        protected override void OnCreate()
        {
            parentQuery = GetEntityQuery(
                ComponentType.ReadOnly<LocalScale>(),
                ComponentType.ReadOnly<ParentLink>());
            unityParentQuery = GetEntityQuery(
                ComponentType.Exclude<LocalScale>(),
                ComponentType.ReadOnly<NonUniformScale>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var parentEntities = parentQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var parentLocalScales = GetComponentLookup<LocalScale>(true);
            var parentLinks = GetComponentLookup<ParentLink>(true);
            //parentEntities.Dispose();
            var unityParentEntities = unityParentQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var parentScales = GetComponentLookup<NonUniformScale>(true);
            //unityParentEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((ref NonUniformScale scale, in ParentLink parentLink, in LocalScale localScale) =>
            {
                var newScale = localScale.scale;
                var parent = parentLink.parent;
                var count = 0;
                while (parent.Index > 0)
                {
                    if (parentLocalScales.HasComponent(parent)) 
                    {
                        var parentSynchScale = parentLocalScales[parent].scale;
                        parent = parentLinks[parent].parent;
                        newScale.x *= parentSynchScale.x;
                        newScale.y *= parentSynchScale.y;
                        newScale.z *= parentSynchScale.z;
                    }
                    else if (parentScales.HasComponent(parent))
                    {
                        var parentScale = parentScales[parent].Value;
                        newScale.x *= parentScale.x;
                        newScale.y *= parentScale.y;
                        newScale.z *= parentScale.z;
                        parent = new Entity();
                    }
                    else
                    {
                        parent = new Entity();
                    }
                    count++;
                    if (count >= Zoxel.Transforms.TransformSystemGroup.maxParents)
                    {
                        break;
                    }
                }
                scale.Value = newScale;
                if (unityParentEntities.Length > 0) { var e2 = unityParentEntities[0]; }
                if (parentEntities.Length > 0) { var e2 = parentEntities[0]; }
            })  .WithReadOnly(parentLocalScales)
                .WithReadOnly(parentLinks)
                .WithReadOnly(parentScales)
                .WithNativeDisableContainerSafetyRestriction(parentScales)
                .WithReadOnly(unityParentEntities)
				.WithDisposeOnCompletion(unityParentEntities)
                .WithReadOnly(parentEntities)
				.WithDisposeOnCompletion(parentEntities)
                .ScheduleParallel(Dependency);
		}
    }
}