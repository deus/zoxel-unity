using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    //! A local rotation system.
    [UpdateAfter(typeof(OrbitParentPositionSystem))]
    [BurstCompile, UpdateInGroup(typeof(TransformSystemGroup))]
	public partial class ParentRotationSystem : SystemBase
    {
        private EntityQuery childQuery;
        private EntityQuery orbitQuery;
        private EntityQuery childrenQuery;
        private EntityQuery unityParentQuery;

        protected override void OnCreate()
        {
            orbitQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<ParentLink>(),
                ComponentType.ReadWrite<Rotation>(),
                ComponentType.ReadOnly<OrbitPosition>(),
                ComponentType.ReadOnly<CameraLink>());
            childrenQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<PositionSynchOnly>(),
                ComponentType.ReadWrite<Rotation>(),
                ComponentType.ReadOnly<ParentLink>(),
                ComponentType.ReadOnly<LocalRotation>()); 
            unityParentQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<LocalRotation>(),
                ComponentType.ReadOnly<Rotation>());
            RequireForUpdate(childQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var twoPiQuaternion = quaternion.EulerXYZ(new float3(0, math.PI * 2, 0)).value;
            var parentEntities = childQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var parentLocalRotations = GetComponentLookup<LocalRotation>(true);
            var parentLinks = GetComponentLookup<ParentLink>(true);
            //parentEntities.Dispose();
            var unityParentEntities = unityParentQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var parentRotations = GetComponentLookup<Rotation>(true);
            //unityParentEntities.Dispose();
            var orbitTransformEntities = orbitQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var orbitPositions = GetComponentLookup<OrbitPosition>(true);
            var cameraLinks = GetComponentLookup<CameraLink>(true);
            //orbitTransformEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref childQuery)
                .WithNone<DestroyEntity, ParentLink>()
                .WithAll<OrbitPosition>()
                .ForEach((Entity e, ref Rotation rotation, in CameraLink cameraLink) =>
            {
                var parent = cameraLink.camera;
                if (parent.Index == 0)
                {
                    return;
                }
                var newRotation = quaternion.identity;
                if (parentLocalRotations.HasComponent(parent) && parentLinks[parent].parent.Index != 0)
                {
                    var cameraRotation = math.mul(twoPiQuaternion, parentLocalRotations[parent].rotation);
                    newRotation = math.mul(cameraRotation, newRotation);
                    parent = parentLinks[parent].parent;
                }
                else if (parentRotations.HasComponent(parent))
                {
                    var cameraRotation = math.mul(twoPiQuaternion, parentRotations[parent].Value);
                    newRotation = math.mul(cameraRotation, newRotation);
                    parent = new Entity();
                }
                else
                {
                    return;
                    // parent = new Entity();
                }
                rotation.Value = GetRotation(newRotation, parent, in parentLinks, in parentLocalRotations, in parentRotations, in orbitPositions, in cameraLinks);
                if (unityParentEntities.Length > 0) { var e2 = unityParentEntities[0]; }
                if (parentEntities.Length > 0) { var e2 = parentEntities[0]; }
                if (orbitTransformEntities.Length > 0) { var e2 = orbitTransformEntities[0]; }
            })  .WithReadOnly(cameraLinks)
                .WithReadOnly(orbitPositions)
                .WithReadOnly(parentLocalRotations)
                .WithReadOnly(parentLinks)
                .WithReadOnly(parentRotations)
                .WithNativeDisableContainerSafetyRestriction(parentRotations)
                .WithReadOnly(unityParentEntities)
				.WithDisposeOnCompletion(unityParentEntities)
                .WithReadOnly(parentEntities)
				.WithDisposeOnCompletion(parentEntities)
                .WithReadOnly(orbitTransformEntities)
				.WithDisposeOnCompletion(orbitTransformEntities)
                .ScheduleParallel(Dependency);
            Dependency = Entities
                .WithNone<DestroyEntity, PositionSynchOnly>()
                .ForEach((ref Rotation rotation, in ParentLink parentLink, in LocalRotation localRotation) =>
            {
                var parent = parentLink.parent;
                if (parent.Index > 0 && !HasComponent<DestroyEntity>(parent))
                {
                    var newRotation = localRotation.rotation;
                    if (newRotation.value.w == 0 && newRotation.value.z == 0 && newRotation.value.y == 0 && newRotation.value.x == 0)
                    {
                        // UnityEngine.Debug.LogError("Local Rotation was null!");
                        newRotation = quaternion.identity;
                    }
                    rotation.Value = GetRotation(newRotation, parent, in parentLinks, in parentLocalRotations, in parentRotations, in orbitPositions, in cameraLinks);
                }
            })  .WithReadOnly(cameraLinks)
                .WithReadOnly(orbitPositions)
                .WithReadOnly(parentLocalRotations)
                .WithReadOnly(parentLinks)
                .WithReadOnly(parentRotations)
                .WithNativeDisableContainerSafetyRestriction(parentRotations)
                .ScheduleParallel(Dependency);
		}

        public static quaternion GetRotation(quaternion newRotation, Entity parent,
            in ComponentLookup<ParentLink> parentLinks, in ComponentLookup<LocalRotation> parentLocalRotations, 
            in ComponentLookup<Rotation> parentRotations, in ComponentLookup<OrbitPosition> orbitPositions, in ComponentLookup<CameraLink> cameraLinks)
        {
            var twoPiQuaternion = quaternion.EulerXYZ(new float3(0, math.PI * 2, 0)).value;
            var count = 0;
            while (parent.Index != 0)
            {
                if (parentLocalRotations.HasComponent(parent)) 
                {
                    newRotation = math.mul(parentLocalRotations[parent].rotation, newRotation);
                    parent = parentLinks[parent].parent;
                }
                else if (orbitPositions.HasComponent(parent))
                {
                    var orbitPosition = orbitPositions[parent].position;
                    parent = cameraLinks[parent].camera;
                    if (parentLinks.HasComponent(parent) && parentLinks[parent].parent.Index != 0)
                    {
                        var cameraRotation = math.mul(twoPiQuaternion, parentLocalRotations[parent].rotation);
                        newRotation = math.mul(cameraRotation, newRotation);
                        parent = parentLinks[parent].parent;
                    }
                    else if (parentRotations.HasComponent(parent))
                    {
                        var cameraRotation = math.mul(twoPiQuaternion, parentRotations[parent].Value);
                        newRotation = math.mul(cameraRotation, newRotation);
                        parent = new Entity();
                    }
                    else
                    {
                        parent = new Entity();
                    }
                }
                else if (parentRotations.HasComponent(parent))
                {
                    newRotation = math.mul(parentRotations[parent].Value, newRotation);
                    parent = new Entity();
                }
                else
                {
                    parent = new Entity();
                }
                count++;
                if (count >= Zoxel.Transforms.TransformSystemGroup.maxParents)
                {
                    break;
                }
            }
            return newRotation;
        }
    }
}