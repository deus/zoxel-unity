using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Transforms
{
    //! Sets position of an entity! This avoids camera issues by updating in TransformSystemGroup.
    [UpdateAfter(typeof(ParentScaleSystem))]
    [BurstCompile, UpdateInGroup(typeof(Zoxel.Transforms.TransformSystemGroup))]
    public partial class TransformSetPositionSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<SetTransformPosition>(processQuery);
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((ref Translation translation, in SetTransformPosition setTransformPosition) =>
            {
                translation.Value = setTransformPosition.position;
            }).ScheduleParallel();
        }
    }
}