using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    //! A local rotation system.
    [UpdateAfter(typeof(ParentRotationSystem))]
    [BurstCompile, UpdateInGroup(typeof(TransformSystemGroup))]
	public partial class OrbitParentRotationSystem : SystemBase
    {
        private EntityQuery orbitQuery;
        private EntityQuery childQuery;
        private EntityQuery childrenQuery;
        private EntityQuery unityParentQuery;

        protected override void OnCreate()
        {
            childQuery = GetEntityQuery(
                ComponentType.ReadOnly<LocalRotation>(),
                ComponentType.ReadOnly<ParentLink>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            childrenQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<PositionSynchOnly>(),
                ComponentType.ReadWrite<Rotation>(),
                ComponentType.ReadOnly<ParentLink>(),
                ComponentType.ReadOnly<LocalRotation>()); 
            unityParentQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<LocalRotation>(),
                ComponentType.ReadOnly<Rotation>());
            RequireForUpdate(orbitQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var twoPiQuaternion = quaternion.EulerXYZ(new float3(0, math.PI * 2, 0)).value;
            var parentEntities = childQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var parentLocalRotations = GetComponentLookup<LocalRotation>(true);
            var parentLinks = GetComponentLookup<ParentLink>(true);
            //parentEntities.Dispose();
            var unityParentEntities = unityParentQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var parentRotations = GetComponentLookup<Rotation>(true);
            //unityParentEntities.Dispose();
            var orbitTransformEntities = orbitQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var orbitPositions = GetComponentLookup<OrbitPosition>(true);
            var cameraLinks = GetComponentLookup<CameraLink>(true);
            //orbitTransformEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref orbitQuery)
                .WithNone<DestroyEntity, ParentLink>()
                .WithAll<OrbitPosition>()
                .ForEach((Entity e, ref Rotation rotation, in CameraLink cameraLink) =>
            {
                var parent = cameraLink.camera;
                if (parent.Index == 0)
                {
                    return;
                }
                var newRotation = quaternion.identity;
                if (parentLocalRotations.HasComponent(parent) && parentLinks[parent].parent.Index != 0)
                {
                    var cameraRotation = math.mul(twoPiQuaternion, parentLocalRotations[parent].rotation);
                    newRotation = math.mul(cameraRotation, newRotation);
                    parent = parentLinks[parent].parent;
                }
                else if (parentRotations.HasComponent(parent))
                {
                    var cameraRotation = math.mul(twoPiQuaternion, parentRotations[parent].Value);
                    newRotation = math.mul(cameraRotation, newRotation);
                    parent = new Entity();
                }
                else
                {
                    return;
                    // parent = new Entity();
                }
                rotation.Value = ParentRotationSystem.GetRotation(newRotation, parent, in parentLinks, in parentLocalRotations, in parentRotations, in orbitPositions, in cameraLinks);
                if (unityParentEntities.Length > 0) { var e2 = unityParentEntities[0]; }
                if (parentEntities.Length > 0) { var e2 = parentEntities[0]; }
                if (orbitTransformEntities.Length > 0) { var e2 = orbitTransformEntities[0]; }
            })  .WithReadOnly(cameraLinks)
                .WithReadOnly(orbitPositions)
                .WithReadOnly(parentLocalRotations)
                .WithReadOnly(parentLinks)
                .WithReadOnly(parentRotations)
                .WithNativeDisableContainerSafetyRestriction(parentRotations)
                .WithReadOnly(unityParentEntities)
				.WithDisposeOnCompletion(unityParentEntities)
                .WithReadOnly(parentEntities)
				.WithDisposeOnCompletion(parentEntities)
                .WithReadOnly(orbitTransformEntities)
				.WithDisposeOnCompletion(orbitTransformEntities)
                .ScheduleParallel(Dependency);
		}
    }
}