using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    //! A local position system.
    //! A local position system.
    [UpdateAfter(typeof(ParentPositionSystem))]
    [BurstCompile, UpdateInGroup(typeof(TransformSystemGroup))]
	public partial class OrbitParentPositionSystem : SystemBase
    {
        private EntityQuery orbitQuery;
        private EntityQuery childQuery;
        private EntityQuery childrenQuery;
        private EntityQuery unityParentQuery;

        protected override void OnCreate()
        {
            childrenQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadWrite<Translation>(),
                ComponentType.ReadOnly<ParentLink>(),
                ComponentType.ReadOnly<LocalPosition>()); 
            childQuery = GetEntityQuery(
                ComponentType.ReadOnly<LocalPosition>(),
                ComponentType.ReadOnly<ParentLink>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>()); 
            unityParentQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<Rotation>());
            RequireForUpdate(orbitQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var parentEntities = childQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var localPositions = GetComponentLookup<LocalPosition>(true);
            var localRotations = GetComponentLookup<LocalRotation>(true);
            var parentLinks = GetComponentLookup<ParentLink>(true);
            //parentEntities.Dispose();
            var unityParentEntities = unityParentQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var parentTranslations = GetComponentLookup<Translation>(true);
            var parentRotations = GetComponentLookup<Rotation>(true);
            //unityParentEntities.Dispose();
            var orbitTransformEntities = orbitQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var orbitPositions = GetComponentLookup<OrbitPosition>(true);
            var cameraLinks = GetComponentLookup<CameraLink>(true);
            //orbitTransformEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref orbitQuery)
                .WithNone<DestroyEntity, ParentLink>()
                .ForEach((Entity e, ref Translation position, in OrbitPosition orbitPosition, in CameraLink cameraLink) =>
            {
                var parent = cameraLink.camera;
                if (parent.Index == 0)
                {
                    return;
                }
                var deltaPosition = float3.zero;
                var orbitPosition2 = orbitPosition.position;
                if (localPositions.HasComponent(parent) && parentLinks[parent].parent.Index != 0)
                {
                    var cameraRotation = localRotations[parent].rotation;
                    var cameraPosition = localPositions[parent].position;
                    // UnityEngine.Debug.LogError("01 Adding from Camera: " + cameraPosition);
                    cameraPosition += math.rotate(cameraRotation, orbitPosition2);
                    deltaPosition = cameraPosition + math.rotate(cameraRotation, deltaPosition);
                    parent = parentLinks[parent].parent;
                }
                else if (parentTranslations.HasComponent(parent))
                {
                    // i kind of need current camera position, and not old one.
                    var cameraRotation = parentRotations[parent].Value;
                    var cameraPosition = parentTranslations[parent].Value;
                    // UnityEngine.Debug.LogError("02 Adding from Camera: " + cameraPosition);
                    cameraPosition += math.rotate(cameraRotation, orbitPosition2);
                    deltaPosition = cameraPosition + math.rotate(cameraRotation, deltaPosition);
                    parent = new Entity();
                }
                else
                {
                    return;
                }
                position.Value = ParentPositionSystem.GetPosition(deltaPosition, parent, in parentLinks, in localPositions, in localRotations, in parentTranslations, in parentRotations, in orbitPositions, in cameraLinks);
                if (unityParentEntities.Length > 0) { var e2 = unityParentEntities[0]; }
                if (parentEntities.Length > 0) { var e2 = parentEntities[0]; }
                if (orbitTransformEntities.Length > 0) { var e2 = orbitTransformEntities[0]; }
            })  .WithReadOnly(cameraLinks)
                .WithReadOnly(orbitPositions)
                .WithReadOnly(localPositions)
                .WithReadOnly(localRotations)
                .WithReadOnly(parentLinks)
                .WithReadOnly(parentTranslations)
                .WithReadOnly(parentRotations)
                .WithNativeDisableContainerSafetyRestriction(parentTranslations)
                .WithNativeDisableContainerSafetyRestriction(parentRotations)
                .WithReadOnly(unityParentEntities)
				.WithDisposeOnCompletion(unityParentEntities)
                .WithReadOnly(parentEntities)
				.WithDisposeOnCompletion(parentEntities)
                .WithReadOnly(orbitTransformEntities)
				.WithDisposeOnCompletion(orbitTransformEntities)
                .ScheduleParallel(Dependency);
		}
    }
}