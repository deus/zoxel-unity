using Unity.Burst;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Burst.Intrinsics;

namespace Zoxel.Transforms
{
    // LocalToWorld = Translation * Rotation * NonUniformScale
    // (or) LocalToWorld = Translation * CompositeRotation * NonUniformScale
    // (or) LocalToWorld = Translation * Rotation * Scale
    // (or) LocalToWorld = Translation * CompositeRotation * Scale
    // (or) LocalToWorld = Translation * Rotation * CompositeScale
    // (or) LocalToWorld = Translation * CompositeRotation * CompositeScale
    [UpdateAfter(typeof(ParentScaleSystem))]
    [BurstCompile, UpdateInGroup(typeof(TransformSystemGroup))]
    public partial class TransformSystem : SystemBase
    {
        private EntityQuery m_Query;

        //burst disabled pending burstable entityquerydesc
        //[BurstCompile]
        protected override void OnCreate()
        {
            m_Query = GetEntityQuery(new EntityQueryDesc()
            {
                All = new ComponentType[]
                {
                    typeof(LocalToWorld)
                },
                Any = new ComponentType[]
                {
                    ComponentType.ReadOnly<NonUniformScale>(),
                    ComponentType.ReadOnly<Scale>(),
                    ComponentType.ReadOnly<Rotation>(),
                    // ComponentType.ReadOnly<CompositeRotation>(),
                    // ComponentType.ReadOnly<CompositeScale>(),
                    ComponentType.ReadOnly<Translation>()
                },
                Options = EntityQueryOptions.FilterWriteGroup
            });
        }
        
        //disabling burst in dotsrt until burstable scheduling works
#if !UNITY_DOTSRUNTIME
        [BurstCompile]
#endif
        protected override void OnUpdate()
        {
            var rotationType = GetComponentTypeHandle<Rotation>(true);
            // var compositeRotationType = GetComponentTypeHandle<CompositeRotation>(true);
            var translationType = GetComponentTypeHandle<Translation>(true);
            var nonUniformScaleType = GetComponentTypeHandle<NonUniformScale>(true);
            var scaleType = GetComponentTypeHandle<Scale>(true);
            // var compositeScaleType = GetComponentTypeHandle<CompositeScale>(true);
            var localToWorldType = GetComponentTypeHandle<LocalToWorld>(false);
            var trsToLocalToWorldJob = new TRSToLocalToWorld()
            {
                RotationTypeHandle = rotationType,
                // CompositeRotationTypeHandle = compositeRotationType,
                TranslationTypeHandle = translationType,
                ScaleTypeHandle = scaleType,
                NonUniformScaleTypeHandle = nonUniformScaleType,
                // CompositeScaleTypeHandle = compositeScaleType,
                LocalToWorldTypeHandle = localToWorldType,
                LastSystemVersion = LastSystemVersion
            };
            Dependency = trsToLocalToWorldJob.ScheduleParallel(m_Query, Dependency);
        }
    }

    [BurstCompile]
    struct TRSToLocalToWorld : IJobChunk
    {
        [ReadOnly] public ComponentTypeHandle<Rotation> RotationTypeHandle;
        //  [ReadOnly] public ComponentTypeHandle<CompositeRotation> CompositeRotationTypeHandle;
        [ReadOnly] public ComponentTypeHandle<Translation> TranslationTypeHandle;
        [ReadOnly] public ComponentTypeHandle<NonUniformScale> NonUniformScaleTypeHandle;
        [ReadOnly] public ComponentTypeHandle<Scale> ScaleTypeHandle;
        // [ReadOnly] public ComponentTypeHandle<CompositeScale> CompositeScaleTypeHandle;
        public ComponentTypeHandle<LocalToWorld> LocalToWorldTypeHandle;
        public uint LastSystemVersion;

        public void Execute(in ArchetypeChunk chunk, int unfilteredChunkIndex, bool useEnabledMask, in v128 chunkEnabledMask)
        // public void Execute(ArchetypeChunk chunk, int batchIndex)
        {
            bool changed =
                chunk.DidOrderChange(LastSystemVersion) ||
                chunk.DidChange(TranslationTypeHandle, LastSystemVersion) ||
                chunk.DidChange(NonUniformScaleTypeHandle, LastSystemVersion) ||
                chunk.DidChange(ScaleTypeHandle, LastSystemVersion) ||
                // chunk.DidChange(CompositeScaleTypeHandle, LastSystemVersion) ||
                chunk.DidChange(RotationTypeHandle, LastSystemVersion);// ||
                //chunk.DidChange(CompositeRotationTypeHandle, LastSystemVersion);
            if (!changed)
            {
                return;
            }

            var chunkTranslations = chunk.GetNativeArray(TranslationTypeHandle);
            var chunkNonUniformScales = chunk.GetNativeArray(NonUniformScaleTypeHandle);
            var chunkScales = chunk.GetNativeArray(ScaleTypeHandle);
            // var chunkCompositeScales = chunk.GetNativeArray(CompositeScaleTypeHandle);
            var chunkRotations = chunk.GetNativeArray(RotationTypeHandle);
            // var chunkCompositeRotations = chunk.GetNativeArray(CompositeRotationTypeHandle);
            var chunkLocalToWorld = chunk.GetNativeArray(LocalToWorldTypeHandle);
            var hasTranslation = chunk.Has(TranslationTypeHandle);
            // var hasCompositeRotation = chunk.Has(CompositeRotationTypeHandle);
            var hasRotation = chunk.Has(RotationTypeHandle);
            var hasAnyRotation = hasRotation;   // hasCompositeRotation || 
            var hasNonUniformScale = chunk.Has(NonUniformScaleTypeHandle);
            var hasScale = chunk.Has(ScaleTypeHandle);
            // var hasCompositeScale = chunk.Has(CompositeScaleTypeHandle);
            var hasAnyScale = hasScale || hasNonUniformScale; //  || hasCompositeScale;
            var count = chunk.Count;

            // #todo jump table when burst supports function pointers

            if (!hasAnyRotation)
            {
                // 00 = invalid (must have at least one)
                // 01
                if (!hasTranslation && hasAnyScale)
                {
                    for (int i = 0; i < count; i++)
                    {
                        var scale = hasNonUniformScale ? float4x4.Scale(chunkNonUniformScales[i].Value) : float4x4.Scale(new float3(chunkScales[i].Value)); // (hasScale ?  : chunkCompositeScales[i].Value);

                        chunkLocalToWorld[i] = new LocalToWorld
                        {
                            Value = scale
                        };
                    }
                }
                // 10
                else if (hasTranslation && !hasAnyScale)
                {
                    for (int i = 0; i < count; i++)
                    {
                        var translation = chunkTranslations[i].Value;

                        chunkLocalToWorld[i] = new LocalToWorld
                        {
                            Value = float4x4.Translate(translation)
                        };
                    }
                }
                // 11
                else if (hasTranslation && hasAnyScale)
                {
                    for (int i = 0; i < count; i++)
                    {
                        //var scale = hasNonUniformScale ? float4x4.Scale(chunkNonUniformScales[i].Value) : (hasScale ? float4x4.Scale(new float3(chunkScales[i].Value)) : chunkCompositeScales[i].Value);
                        var scale = hasNonUniformScale ? float4x4.Scale(chunkNonUniformScales[i].Value) : float4x4.Scale(new float3(chunkScales[i].Value)); // (hasScale ?  : chunkCompositeScales[i].Value);
                        var translation = chunkTranslations[i].Value;

                        chunkLocalToWorld[i] = new LocalToWorld
                        {
                            Value = math.mul(float4x4.Translate(translation), scale)
                        };
                    }
                }
            }
            else // if (hasRotation) -- Only in same WriteGroup if !hasCompositeRotation
            {
                // 00
                if (!hasTranslation && !hasAnyScale)
                {
                    for (int i = 0; i < count; i++)
                    {
                        var rotation = chunkRotations[i].Value;

                        chunkLocalToWorld[i] = new LocalToWorld
                        {
                            Value = new float4x4(rotation, float3.zero)
                        };
                    }
                }
                // 01
                else if (!hasTranslation && hasAnyScale)
                {
                    for (int i = 0; i < count; i++)
                    {
                        var rotation = chunkRotations[i].Value;
                        // var scale = hasNonUniformScale ? float4x4.Scale(chunkNonUniformScales[i].Value) : (hasScale ? float4x4.Scale(new float3(chunkScales[i].Value)) : chunkCompositeScales[i].Value);
                        var scale = hasNonUniformScale ? float4x4.Scale(chunkNonUniformScales[i].Value) : float4x4.Scale(new float3(chunkScales[i].Value)); // (hasScale ?  : chunkCompositeScales[i].Value);

                        chunkLocalToWorld[i] = new LocalToWorld
                        {
                            Value = math.mul(new float4x4(rotation, float3.zero), scale)
                        };
                    }
                }
                // 10
                else if (hasTranslation && !hasAnyScale)
                {
                    for (int i = 0; i < count; i++)
                    {
                        var rotation = chunkRotations[i].Value;
                        var translation = chunkTranslations[i].Value;

                        chunkLocalToWorld[i] = new LocalToWorld
                        {
                            Value = new float4x4(rotation, translation)
                        };
                    }
                }
                // 11
                else if (hasTranslation && hasAnyScale)
                {
                    for (int i = 0; i < count; i++)
                    {
                        var rotation = chunkRotations[i].Value;
                        var translation = chunkTranslations[i].Value;
                        var scale = hasNonUniformScale ? float4x4.Scale(chunkNonUniformScales[i].Value) : float4x4.Scale(new float3(chunkScales[i].Value)); // (hasScale ?  : chunkCompositeScales[i].Value);
                        // var scale = hasNonUniformScale ? float4x4.Scale(chunkNonUniformScales[i].Value) : (hasScale ? float4x4.Scale(new float3(chunkScales[i].Value)) : chunkCompositeScales[i].Value);

                        chunkLocalToWorld[i] = new LocalToWorld
                        {
                            Value = math.mul(new float4x4(rotation, translation), scale)
                        };
                    }
                }
            }
        }
    }
}

            /*else if (hasCompositeRotation)
            {
                // 00
                if (!hasTranslation && !hasAnyScale)
                {
                    for (int i = 0; i < count; i++)
                    {
                        var rotation = chunkCompositeRotations[i].Value;

                        chunkLocalToWorld[i] = new LocalToWorld
                        {
                            Value = rotation
                        };
                    }
                }
                // 01
                else if (!hasTranslation && hasAnyScale)
                {
                    for (int i = 0; i < count; i++)
                    {
                        var rotation = chunkCompositeRotations[i].Value;
                        var scale = hasNonUniformScale ? float4x4.Scale(chunkNonUniformScales[i].Value) : (hasScale ? float4x4.Scale(new float3(chunkScales[i].Value)) : chunkCompositeScales[i].Value);

                        chunkLocalToWorld[i] = new LocalToWorld
                        {
                            Value = math.mul(rotation, scale)
                        };
                    }
                }
                // 10
                else if (hasTranslation && !hasAnyScale)
                {
                    for (int i = 0; i < count; i++)
                    {
                        var rotation = chunkCompositeRotations[i].Value;
                        var translation = chunkTranslations[i].Value;

                        chunkLocalToWorld[i] = new LocalToWorld
                        {
                            Value = math.mul(float4x4.Translate(translation), rotation)
                        };
                    }
                }
                // 11
                else if (hasTranslation && hasAnyScale)
                {
                    for (int i = 0; i < count; i++)
                    {
                        var rotation = chunkCompositeRotations[i].Value;
                        var translation = chunkTranslations[i].Value;
                        var scale = hasNonUniformScale ? float4x4.Scale(chunkNonUniformScales[i].Value) : (hasScale ? float4x4.Scale(new float3(chunkScales[i].Value)) : chunkCompositeScales[i].Value);

                        chunkLocalToWorld[i] = new LocalToWorld
                        {
                            Value = math.mul(math.mul(float4x4.Translate(translation), rotation) , scale)
                        };
                    }
                }
            }*/