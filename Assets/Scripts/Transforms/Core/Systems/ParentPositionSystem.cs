using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    //! A local position system.
    [BurstCompile, UpdateInGroup(typeof(TransformSystemGroup))]
	public partial class ParentPositionSystem : SystemBase
    {
        private EntityQuery childQuery;
        private EntityQuery orbitQuery;
        private EntityQuery childrenQuery;
        private EntityQuery unityParentQuery;

        protected override void OnCreate()
        {
            orbitQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<ParentLink>(),
                ComponentType.ReadWrite<Translation>(),
                ComponentType.ReadOnly<OrbitPosition>(),
                ComponentType.ReadOnly<CameraLink>()); 
            childrenQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadWrite<Translation>(),
                ComponentType.ReadOnly<ParentLink>(),
                ComponentType.ReadOnly<LocalPosition>()); 
            unityParentQuery = GetEntityQuery(
                // ComponentType.Exclude<LocalPosition>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<Rotation>());
            RequireForUpdate(childQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var parentEntities = childQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var localPositions = GetComponentLookup<LocalPosition>(true);
            var localRotations = GetComponentLookup<LocalRotation>(true);
            var parentLinks = GetComponentLookup<ParentLink>(true);
            //parentEntities.Dispose();
            var unityParentEntities = unityParentQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var parentTranslations = GetComponentLookup<Translation>(true);
            var parentRotations = GetComponentLookup<Rotation>(true);
            //unityParentEntities.Dispose();
            var orbitTransformEntities = orbitQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var orbitPositions = GetComponentLookup<OrbitPosition>(true);
            var cameraLinks = GetComponentLookup<CameraLink>(true);
            //orbitTransformEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref childQuery)
                .WithNone<DestroyEntity>()
                .ForEach((Entity e, ref Translation position, in ParentLink parentLink, in LocalPosition localPosition) =>
            {
                var deltaPosition = localPosition.position;
                var parent = parentLink.parent;
                //! Gets parent position - Checks if parent has parent, add those parents until parent doesnt exist.
                position.Value = GetPosition(deltaPosition, parent, in parentLinks, in localPositions, in localRotations, in parentTranslations, in parentRotations, in orbitPositions, in cameraLinks);
                if (unityParentEntities.Length > 0) { var e2 = unityParentEntities[0]; }
                if (parentEntities.Length > 0) { var e2 = parentEntities[0]; }
                if (orbitTransformEntities.Length > 0) { var e2 = orbitTransformEntities[0]; }
            })  .WithReadOnly(cameraLinks)
                .WithReadOnly(orbitPositions)
                .WithReadOnly(localPositions)
                .WithReadOnly(localRotations)
                .WithReadOnly(parentLinks)
                .WithReadOnly(parentTranslations)
                .WithReadOnly(parentRotations)
                .WithNativeDisableContainerSafetyRestriction(parentTranslations)
                .WithNativeDisableContainerSafetyRestriction(parentRotations)
                .WithReadOnly(unityParentEntities)
				.WithDisposeOnCompletion(unityParentEntities)
                .WithReadOnly(parentEntities)
				.WithDisposeOnCompletion(parentEntities)
                .WithReadOnly(orbitTransformEntities)
				.WithDisposeOnCompletion(orbitTransformEntities)
                .ScheduleParallel(Dependency);
		}

        public static float3 GetPosition(float3 deltaPosition, Entity parent, in ComponentLookup<ParentLink> parentLinks,
            in ComponentLookup<LocalPosition> localPositions, in ComponentLookup<LocalRotation> localRotations, 
            in ComponentLookup<Translation> parentTranslations, in ComponentLookup<Rotation> parentRotations, 
            in ComponentLookup<OrbitPosition> orbitPositions, in ComponentLookup<CameraLink> cameraLinks)
        {
            if (parent.Index == 0)
            {
                return deltaPosition;
            }
            var count = 0;
            while (parent.Index > 0)
            {
                if (localPositions.HasComponent(parent))
                {
                    deltaPosition = localPositions[parent].position + math.rotate(localRotations[parent].rotation, deltaPosition);
                    parent = parentLinks[parent].parent;
                }
                else if (orbitPositions.HasComponent(parent))
                {
                    var orbitPosition = orbitPositions[parent].position;
                    parent = cameraLinks[parent].camera;
                    if (parentLinks.HasComponent(parent) && parentLinks[parent].parent.Index != 0)
                    {
                        var cameraRotation = localRotations[parent].rotation;
                        var cameraPosition = localPositions[parent].position;
                        // UnityEngine.Debug.LogError("1 Adding from Camera: " + cameraPosition);
                        cameraPosition += math.rotate(cameraRotation, orbitPosition);
                        deltaPosition = cameraPosition + math.rotate(cameraRotation, deltaPosition);
                        parent = parentLinks[parent].parent;
                    }
                    else if (parentTranslations.HasComponent(parent))
                    {
                        // i kind of need current camera position, and not old one.
                        var cameraRotation = parentRotations[parent].Value;
                        var cameraPosition = parentTranslations[parent].Value;
                        // UnityEngine.Debug.LogError("2 Adding from Camera: " + cameraPosition);
                        cameraPosition += math.rotate(cameraRotation, orbitPosition);
                        deltaPosition = cameraPosition + math.rotate(cameraRotation, deltaPosition);
                        parent = new Entity();
                    }
                    else
                    {
                        parent = new Entity();
                    }
                }
                else if (parentTranslations.HasComponent(parent))
                {
                    if (parentRotations.HasComponent(parent))
                    {
                        var parentRotation = parentRotations[parent].Value;
                        deltaPosition = parentTranslations[parent].Value + math.rotate(parentRotation, deltaPosition);
                    }
                    else
                    {
                        deltaPosition = parentTranslations[parent].Value + deltaPosition;
                    }
                    parent = new Entity();
                }
                else
                {
                    parent = new Entity();
                }
                count++;
                if (count >= TransformSystemGroup.maxParents)
                {
                    break;
                }
            }
            return deltaPosition;
        }
    }
}