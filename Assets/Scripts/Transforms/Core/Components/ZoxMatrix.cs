using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    //! Holds a matrix (Matrix4x4) data.
    public struct ZoxMatrix : IComponentData
    {
		public UnityEngine.Matrix4x4 matrix;

        public ZoxMatrix(float3 realPosition)
        {
			this.matrix = float4x4.TRS(realPosition, quaternion.identity, new float3(1,1,1));
        }
        
        public ZoxMatrix(float3 realPosition, quaternion rotation)
        {
			this.matrix = float4x4.TRS(realPosition, rotation, new float3(1,1,1));
        }
        
        public ZoxMatrix(int3 chunkPosition, int3 voxelDimensions)
        {
            var voxelPosition = VoxelUtilities.GetChunkVoxelPosition(chunkPosition, voxelDimensions).ToFloat3();
			var scale = new float3(1,1,1);
        	var identityRotation = quaternion.identity;
			this.matrix = float4x4.TRS(voxelPosition, identityRotation, scale);
        }

        public void Set(int3 chunkPosition, int3 voxelDimensions, float3 scale)
        {
            var voxelPosition = VoxelUtilities.GetChunkVoxelPosition(chunkPosition, voxelDimensions).ToFloat3();
            voxelPosition.x *= scale.x;
            voxelPosition.y *= scale.y;
            voxelPosition.z *= scale.z;
        	var identityRotation = quaternion.identity;
			this.matrix = float4x4.TRS(voxelPosition, identityRotation, new float3(1, 1, 1));
        }
    }
}