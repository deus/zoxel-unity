using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    public struct BoneUniqueRotation : IComponentData
    {
        public quaternion rotation;

        public BoneUniqueRotation(quaternion rotation)
        {
            this.rotation = rotation;
        }
    }
}