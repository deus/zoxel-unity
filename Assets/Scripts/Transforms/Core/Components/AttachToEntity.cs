using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    //! Attaches to an entity with a local position.
    public struct AttachToEntity : IComponentData
    {
        public Entity parent;
        public float3 localPosition;

        public AttachToEntity(Entity parent, float3 localPosition)
        {
            this.parent = parent;
            this.localPosition = localPosition;
        }
    }
}