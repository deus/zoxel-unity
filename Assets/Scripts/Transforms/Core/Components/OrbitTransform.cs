﻿using Unity.Entities;

namespace Zoxel.Transforms
{
    //! This is used for an object to orbit another.
    public struct OrbitTransform : IComponentData { }
}

// This is used for UI to orbit the camera entity