using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    //! Sets an entity's transform position.
    public struct SetTransformPosition : IComponentData
    {
        public float3 position;

        public SetTransformPosition(float3 position)
        {
            this.position = position;
        }
    }
}