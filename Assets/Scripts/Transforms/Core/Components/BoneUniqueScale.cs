using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    public struct BoneUniqueScale : IComponentData
    {
        public float3 scale;

        public BoneUniqueScale(float3 scale)
        {
            this.scale = scale;
        }
    }
}