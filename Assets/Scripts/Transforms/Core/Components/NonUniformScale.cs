// using System;
using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    public struct NonUniformScale : IComponentData
    {
        public float3 Value;
    }
}

    /*[Serializable]
    [WriteGroup(typeof(LocalToWorld))]
    [WriteGroup(typeof(LocalToParent))]
    [WriteGroup(typeof(CompositeScale))]
    [WriteGroup(typeof(ParentScaleInverse))]*/