using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    //! This contains the offset of a transform orbit.
    public struct OrbitPosition : IComponentData
    {
        public float3 position;
    }
}