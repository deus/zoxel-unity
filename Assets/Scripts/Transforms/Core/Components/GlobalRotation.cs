using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    public struct GlobalRotation : IComponentData
    {
        public quaternion rotation;

        public GlobalRotation(float3 position)
        {
            this.rotation = quaternion.identity;
        }
    }
}