using Unity.Entities;

namespace Zoxel.Transforms
{
    public struct Scale : IComponentData
    {
        public float Value;
    }
}

    /*[Serializable]
    [WriteGroup(typeof(LocalToWorld))]
    [WriteGroup(typeof(LocalToParent))]
    [WriteGroup(typeof(CompositeScale))]
    [WriteGroup(typeof(ParentScaleInverse))]*/