using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    public struct Translation : IComponentData
    {
        public float3 Value;
    }
}

    /*[Serializable]
    [WriteGroup(typeof(LocalToWorld))]
    [WriteGroup(typeof(LocalToParent))]*/