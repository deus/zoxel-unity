using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Transforms
{
    public struct Rotation : IComponentData
    {
        public quaternion Value;
    }
}

    /*[Serializable]
    [WriteGroup(typeof(LocalToWorld))]
    [WriteGroup(typeof(LocalToParent))]
    [WriteGroup(typeof(CompositeRotation))]*/