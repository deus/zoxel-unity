using Unity.Entities;
using Zoxel.Transforms;

namespace Zoxel.Transforms
{
    //! Synchs gameobjects from entities.
    // [UpdateAfter(typeof(TransformSystem))]
    [UpdateInGroup(typeof(TransformSystemGroup))]
    public partial class TransformSynchSystemGroup : ComponentSystemGroup { }
}