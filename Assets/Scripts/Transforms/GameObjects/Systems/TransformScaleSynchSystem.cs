/*using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using UnityEngine.Jobs;

namespace Zoxel.Transforms
{
    //! Synchs gameobjects to their corresponding entity.
    [BurstCompile, UpdateInGroup(typeof(TransformSynchSystemGroup))]
    public partial class TransformScaleSynchSystem : SystemBase
    {
        private TransformAccessArray transformAccessArray;
        private EntityQuery transformQuery;

        protected override void OnDestroy()
        {
            transformAccessArray.Dispose();
        }

        protected override void OnCreate()
        {
            transformAccessArray = new TransformAccessArray(0);
            transformQuery = GetEntityQuery(
                ComponentType.ReadOnly<GameObjectLink>(),
                ComponentType.ReadOnly<NonUniformScale>(),
                ComponentType.Exclude<GameObjectReverseSync>(),
                ComponentType.Exclude<DestroyEntity>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var entities = transformQuery.ToEntityArray(Allocator.TempJob);
            var transforms = new UnityEngine.Transform[entities.Length];
            for (int i = 0; i < entities.Length; i++)
            {
                var e = entities[i];
                var gameObject = EntityManager.GetSharedComponentData<GameObjectLink>(e).gameObject;
                if (gameObject)
                {
                    transforms[i] = gameObject.transform;
                }
            }
            transformAccessArray.SetTransforms(transforms);
            var job = new CopyScaleJob
            {
                entities = entities,
                scales = GetComponentLookup<NonUniformScale>()
            };
            // Dependency = job.ScheduleReadOnly(transformAccessArray, 32, Dependency);
            Dependency = job.Schedule(transformAccessArray, Dependency);
        }

        [BurstCompile]
        struct CopyScaleJob : IJobParallelForTransform
        {
            [DeallocateOnJobCompletion, ReadOnly] public NativeArray<Entity> entities;
            [ReadOnly] public ComponentLookup<NonUniformScale> scales;

            public void Execute(int index, TransformAccess transform)   // unsafe
            {
                transform.localScale = scales[entities[index]].Value;
            }
        }
    }
}*/