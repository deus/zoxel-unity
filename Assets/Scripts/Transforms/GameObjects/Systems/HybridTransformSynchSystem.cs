using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine.Jobs;

namespace Zoxel.Transforms
{
    //! Synchs gameobject Transforms to their corresponding entity.
    /*[BurstCompile, UpdateInGroup(typeof(TransformSynchSystemGroup))]
    public partial class HybridTransformSynchSystem : SystemBase
    {
        private TransformAccessArray transformAccessArray;
        private EntityQuery transformQuery;
        private UnityEngine.Transform[] transforms;

        protected override void OnCreate()
        {
            transforms = new UnityEngine.Transform[0];
            transformQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<GameObjectReverseSync>(),
                ComponentType.Exclude<DisableGameObjectSync>(),
                ComponentType.ReadOnly<GameObjectLink>(),
                ComponentType.ReadOnly<LocalToWorld>());
        }

        protected override void OnDestroy()
        {
            if (transformAccessArray.isCreated)
            {
                transformAccessArray.Dispose();
            }
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (transformQuery.IsEmpty)
            {
                return;
            }
            var entities = transformQuery.ToEntityArray(Allocator.TempJob);
            if (transforms.Length != entities.Length)
            {
                transforms = new UnityEngine.Transform[entities.Length];
            }
            for (int i = 0; i < transforms.Length; i++)
            {
                var e = entities[i];
                var gameObject = EntityManager.GetSharedComponentData<GameObjectLink>(e).gameObject;
                if (gameObject)
                {
                    transforms[i] = gameObject.transform;
                }
                else
                {
                    transforms[i] = null;
                }
            }
            if (!transformAccessArray.isCreated)
            {
                transformAccessArray = new TransformAccessArray(transforms);
            }
            else
            {
                transformAccessArray.SetTransforms(transforms);
            }
            entities.Dispose();
            var localToWorlds = transformQuery.ToComponentDataArrayAsync<LocalToWorld>(Allocator.TempJob, out var dependencyB);
            // Dependency = JobHandle.CombineDependencies(Dependency, dependencyB);
            Dependency = new CopyTransforms
            {
                localToWorlds = localToWorlds
            }.Schedule(transformAccessArray, dependencyB);
        }

        [BurstCompile]
        struct CopyTransforms : IJobParallelForTransform
        {
            [DeallocateOnJobCompletion, ReadOnly] public NativeArray<LocalToWorld> localToWorlds;

            public void Execute(int index, TransformAccess transform)
            {
                var value = localToWorlds[index];
                transform.position = value.Position;
                transform.rotation = new quaternion(value.Value);
            }
        }
    }*/
}

            // var translations = GetComponentLookup<Translation>();
            // var gameObjectSynchs = group.ToComponentArray<GameObjectLink>();
            // var transforms = transformQuery.GetTransformAccessArray(ref transformAccessArray);


    /*class TransformAccessArrayData : IDisposable
    {
        public TransformAccessArray Data;
        public int OrderVersion;

        public void Dispose()
        {
            if (Data.isCreated)
                Data.Dispose();
        }
    }*/
            /*var state = (TransformAccessArrayData) group._CachedState;*/
            //if (transformAccessArrayData == null)
            //    transformAccessArrayData = new TransformAccessArray();
            // var orderVersion = group._GetImpl()->_Access->EntityComponentStore->GetComponentTypeOrderVersion(TypeManager.GetTypeIndex<UnityEngine.Transform>());

            /*if (state.Data.isCreated && orderVersion == state.OrderVersion)
                return state.Data;
            state.OrderVersion = orderVersion;*/


        /*private EntityQuery transformQuery;
        private TransformAccessArray transformAccessArray;

        protected override void OnDestroy()
        {
            transformAccessArray.Dispose();
        }

        protected override void OnCreate()
        {
            transformAccessArray = new TransformAccessArray(1024);
            transformQuery = GetEntityQuery(
                ComponentType.Exclude<Prefab>(),
                ComponentType.Exclude<GameObjectReverseSync>(),
                ComponentType.Exclude<EditorCamera>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<GameObjectLink>(),
                ComponentType.ReadOnly<Translation>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            //var entities = transformQuery.ToEntityArray(Allocator.TempJob);
            var entities = transformQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var translations = GetComponentLookup<Translation>();
            var gameObjectSynchs = new System.Collections.Generic.List<GameObjectLink>(transformQuery.CalculateEntityCount());
            // EntityManager.GetAllUniqueSharedComponentData<GameObjectLink>(gameObjectSynchs);
            var gameObjectSynchs2 = GetSharedComponentDataFromEntity<GameObjectLink>();
            if (transformQuery.CalculateEntityCount() != gameObjectSynchs.Count)
            {
                UnityEngine.Debug.LogError("Game Object Synchs count is off: " + transformQuery.CalculateEntityCount() + " : " + gameObjectSynchs.Count);
                entities.Dispose();
                return;
            }
            var transforms = new UnityEngine.Transform[entities.Length];
            for (int i = 0; i < gameObjectSynchs.Count; i++)
            {
                var gameObjectLink = gameObjectSynchs[i];
                if (gameObjectLink.gameObject)
                {
                    transforms[i] = gameObjectLink.gameObject.transform;
                }
            }
            transformAccessArray.SetTransforms(transforms);
            var job = new CopyPositionJob
            {
                entities = entities,
                translations = translations
            };
            Dependency = job.Schedule(transformAccessArray, Dependency);
        }

        [BurstCompile]
        struct CopyPositionJob : IJobParallelForTransform
        {
            [DeallocateOnJobCompletion, ReadOnly] public NativeArray<Entity> entities;
            [ReadOnly] public ComponentLookup<Translation> translations;

            public void Execute(int index, TransformAccess transform)
            {
                var e = entities[index];
                transform.position = translations[e].Value;
            }
        }*/