using Unity.Entities;

namespace Zoxel.Transforms
{
    //! Cleans up game objects connected to an Entity.
    [UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class GameObjectDestroySystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, GameObjectLink>()
                .ForEach((in GameObjectLink gameObjectLink) =>
            {
                ObjectUtil.Destroy(gameObjectLink.gameObject);
            }).WithoutBurst().Run();
        }
    }
}