/*using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using UnityEngine.Jobs;

namespace Zoxel.Transforms
{
    //! Synchs gameobjects to their corresponding entity.
    [BurstCompile, UpdateInGroup(typeof(TransformSynchSystemGroup))]
    public partial class TransformRotationSynchSystem : SystemBase
    {
        private EntityQuery transformQuery;
        private TransformAccessArray transformAccessArray;

        protected override void OnDestroy()
        {
            transformAccessArray.Dispose();
        }

        protected override void OnCreate()
        {
            transformAccessArray = new TransformAccessArray(0);
            transformQuery = GetEntityQuery(
                ComponentType.ReadOnly<GameObjectLink>(),
                ComponentType.ReadOnly<Rotation>(),
                ComponentType.Exclude<EditorCamera>(),
                ComponentType.Exclude<GameObjectReverseSync>(),
                ComponentType.Exclude<DestroyEntity>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var entities = transformQuery.ToEntityArray(Allocator.TempJob);
            //var entities = transformQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            //Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var transforms = new UnityEngine.Transform[entities.Length];
            for (int i = 0; i < entities.Length; i++)
            {
                var e = entities[i];
                var gameObject = EntityManager.GetSharedComponentData<GameObjectLink>(e).gameObject;
                if (gameObject)
                {
                    transforms[i] = gameObject.transform;
                }
                // UnityEditor.EditorUtility.SetDirty(gameObject);
            }
            transformAccessArray.SetTransforms(transforms);
            var job = new CopyRotationJob
            {
                entities = entities,
                rotations = GetComponentLookup<Rotation>()
            };
            // Dependency = job.ScheduleReadOnly(transformAccessArray, 32, Dependency);
            Dependency = job.Schedule(transformAccessArray, Dependency);
        }

        [BurstCompile]
        struct CopyRotationJob : IJobParallelForTransform
        {
            [DeallocateOnJobCompletion, ReadOnly] public NativeArray<Entity> entities;
            [ReadOnly] public ComponentLookup<Rotation> rotations;

            public unsafe void Execute(int index, TransformAccess transform)
            {
                transform.rotation = rotations[entities[index]].Value;
            }
        }
    }
}*/