using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using UnityEngine.Jobs;

namespace Zoxel.Transforms
{
    //! Synchs gameobjects to their corresponding entity.
    /*[BurstCompile, UpdateInGroup(typeof(TransformSynchSystemGroup))]
    public partial class GameObjectReverseSyncSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithAll<GameObjectReverseSync>()
                .ForEach((ref LocalToWorld localToWorld, in GameObjectLink gameObjectLink) =>
            {
                if (gameObjectLink.gameObject)
                {
                    localToWorld.Value = gameObjectLink.gameObject.transform.localToWorldMatrix;
                }
            }).WithoutBurst().Run();
		}
    }*/
}
        
        /*private TransformAccessArray transforms;
        private EntityQuery transformQuery;

        protected override void OnDestroy()
        {
            transforms.Dispose();
        }

        protected override void OnCreate()
        {
            transforms = new TransformAccessArray(32);
            transformQuery = GetEntityQuery(
                ComponentType.ReadOnly<GameObjectLink>(),
                ComponentType.ReadWrite<LocalToWorld>(),
                ComponentType.ReadOnly<GameObjectReverseSync>(),
                ComponentType.Exclude<DestroyEntity>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var entities = transformQuery.ToEntityArray(Allocator.TempJob);
            // var entities = transformQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            // Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var localToWorlds = GetComponentLookup<LocalToWorld>();
            var transforms2 = new UnityEngine.Transform[entities.Length];
            for (int i = 0; i < entities.Length; i++)
            {
                var gameObject = EntityManager.GetSharedComponentData<GameObjectLink>(entities[i]).gameObject;
                if (gameObject)
                {
                    transforms2[i] = gameObject.transform;
                }
                else
                {
                    entities.Dispose();
                    return;
                }
            }
            transforms.SetTransforms(transforms2);
            var job = new RetrieveTransformJob
            {
                entities = entities,
                localToWorlds = localToWorlds
            };
            Dependency = job.ScheduleReadOnly(transforms, 32, Dependency);
            // Dependency = job.Schedule(transforms, Dependency);
        }

        [BurstCompile]
        struct RetrieveTransformJob : IJobParallelForTransform
        {
            [DeallocateOnJobCompletion, ReadOnly] public NativeArray<Entity> entities;
            // [ReadOnly]
            [NativeDisableParallelForRestriction] public ComponentLookup<LocalToWorld> localToWorlds;

            public unsafe void Execute(int index, TransformAccess transform)
            {
                var e = entities[index];
                var localToWorld = localToWorlds[e];
                localToWorld.Value = transform.localToWorldMatrix;
                localToWorlds[e] = localToWorld;
                // UnityEngine.Debug.LogError("Setting LocalToWorlds.");
            }
        }*/