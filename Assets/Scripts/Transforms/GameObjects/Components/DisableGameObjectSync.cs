using Unity.Entities;
using System;

namespace Zoxel.Transforms
{
    //! Syncs GameObject transform to entity one.
    public struct DisableGameObjectSync : IComponentData { }
}