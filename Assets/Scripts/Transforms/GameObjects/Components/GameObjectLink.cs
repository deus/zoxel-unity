using Unity.Entities;
using System;

namespace Zoxel.Transforms
{
    //! Syncs GameObject to a Entity's transform.
    /**
    *   If there is a gameobject for Camera, Light or AudioListener, it will need this component to synch transforms.
    */
    public struct GameObjectLink : ISharedComponentData, IEquatable<GameObjectLink>
    {
        public UnityEngine.GameObject gameObject;

        public GameObjectLink(UnityEngine.GameObject gameObject)
        {
            this.gameObject = gameObject;
        }

        public bool Equals(GameObjectLink other)
        {
            return gameObject == other.gameObject;
        }
        
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                if (gameObject == null)
                {
                    return 0;
                }
                return gameObject.GetHashCode();
            }
        }
    }
}