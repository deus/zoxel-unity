#if UNITY_EDITOR
using Unity.Entities;
using Unity.Mathematics;
using UnityEditor;

namespace Zoxel.Transforms
{
    //! Synchs SceneView Cameras.
    /**
    *   \todo Support multiple sceneviews by linking a sceneview camera to a scene.
    */
    /*[BurstCompile, UpdateInGroup(typeof(TransformSynchSystemGroup))]
    public partial class EditorCameraSynchSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            if (EditorApplication.isPlaying)
            {
                return;
            }
            Entities
                .WithChangeFilter<Translation>()
                .WithAll<EditorCamera>()
                .ForEach((in SceneViewLink sceneViewLink, in Translation translation) =>
            {
                var sceneView = sceneViewLink.sceneView;
                if (sceneView != null)
                {
                    sceneView.pivot = translation.Value + new float3(0, 0, sceneView.cameraDistance);
                }
            }).WithoutBurst().Run();
            Entities
                .WithChangeFilter<Rotation>()
                .WithAll<EditorCamera>()
                .ForEach((in SceneViewLink sceneViewLink, in Rotation rotation) =>
            {
                var sceneView = sceneViewLink.sceneView;
                if (sceneView != null)
                {
                    sceneView.rotation = rotation.Value;
                }
            }).WithoutBurst().Run();
        }
    }*/
}
#endif