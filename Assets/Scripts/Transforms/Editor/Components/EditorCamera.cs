using Unity.Entities;

namespace Zoxel
{
    //! Uses SceneCamera data.
    public struct EditorCamera : IComponentData { }
}