#if UNITY_EDITOR
using Unity.Entities;
using System;
using UnityEditor;

namespace Zoxel.Transforms
{
    //! Links to SceneView!
    public struct SceneViewLink : ISharedComponentData, IEquatable<SceneViewLink>
    {
        public SceneView sceneView;

        public SceneViewLink(SceneView sceneView)
        {
            this.sceneView = sceneView;
        }

        public bool Equals(SceneViewLink other)
        {
            return sceneView == other.sceneView;
        }
        
        public override int GetHashCode()
        {
            return sceneView.GetHashCode();
        }
    }
}
#endif