using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Lines;
using Zoxel.Items.Authoring;

namespace Zoxel.Items
{
    [UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class DebugHeldItemSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<ItemsSettings>();
        }

        protected override void OnUpdate()
        {
            var itemsSettings = GetSingleton<ItemsSettings>();
            if (!itemsSettings.isDebugHeldItem)
            {
                return;
            }
            var elapsedTime = World.Time.ElapsedTime;
            var linePrefab = RenderLineGroup.linePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<WorldVoxel>()
                .ForEach((Entity e, in Translation translation, in Rotation rotation) =>
            {
                var worldPosition = translation.Value;
                var worldRotation = rotation.Value;
                // var boneSize =  1.2f * (new float3(boneDebugSize, boneDebugSize, boneDebugSize));
                // RenderLineGroup.CreateCubeLines(PostUpdateCommands, linePrefab, elapsedTime, worldPosition, worldRotation, boneSize); 
                RenderLineGroup.SpawnLine(PostUpdateCommands, linePrefab, elapsedTime, worldPosition,
                    worldPosition + math.rotate(worldRotation, new float3(0, 0, 0.3f)), 1);
            }).Run();
        }

    }
}