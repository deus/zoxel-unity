using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Items
{
    //! Links user items to inventory user after they spawn.
    /**
    *   - Linking System -
    *   \todo Use InitializeEntity component to gather UserEquipments, instead of going through all the previous ones. Only go through new ones spawned.
    */
    [BurstCompile, UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class UserEquipmentSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userEquipmentQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userEquipmentQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserEquipment>(),
                ComponentType.ReadOnly<CreatorLink>(),
                ComponentType.ReadOnly<UserDataIndex>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userEquipmentEntities = userEquipmentQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var creatorLinks = GetComponentLookup<CreatorLink>(true);
            var userItemIndexes = GetComponentLookup<UserDataIndex>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref Equipment equipment, ref OnUserEquipmentSpawned onUserEquipmentSpawned) =>
            {
                if (onUserEquipmentSpawned.spawned != 255)
                {
                    equipment.Initialize(onUserEquipmentSpawned.spawned);
                    onUserEquipmentSpawned.spawned = 255;
                }
                if (equipment.body.Length == 0)
                {
                    PostUpdateCommands.RemoveComponent<OnUserEquipmentSpawned>(entityInQueryIndex, e);
                    return;
                }
                var count = 0;
                for (int i = 0; i < userEquipmentEntities.Length; i++)
                {
                    var e2 = userEquipmentEntities[i];
                    var inventoryEntity = creatorLinks[e2].creator;
                    if (inventoryEntity == e)
                    {
                        var index = userItemIndexes[e2].index;
                        equipment.body[index] = e2;
                        count++;
                        if (equipment.body.Length == count)
                        {
                            PostUpdateCommands.RemoveComponent<OnUserEquipmentSpawned>(entityInQueryIndex, e);
                            break;
                        }
                    }
                }
                // UnityEngine.Debug.LogError("Spawned " + count + " user equipment items.");
            })  .WithReadOnly(userEquipmentEntities)
                .WithDisposeOnCompletion(userEquipmentEntities)
                .WithReadOnly(creatorLinks)
                .WithReadOnly(userItemIndexes)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}