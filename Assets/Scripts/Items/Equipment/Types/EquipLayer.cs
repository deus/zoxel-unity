using System;

namespace Zoxel.Items
{
    [Serializable]
    public enum EquipLayer : byte
    {
        Body,
        Gear
    }
}