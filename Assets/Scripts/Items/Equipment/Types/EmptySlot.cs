namespace Zoxel.Items
{
    public struct EmptySlot
    { 
        public int bodyIndex;
        public int slotIndex;   // slot plugged into - chest has 2 arm slots, 1 head slot and one hips slot
        public int slotID;
    }
}