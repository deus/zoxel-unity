using Unity.Entities;
// rewrite this later and call them equipment parts - for body and gear, just using layers
// need a way to store positions of voxes here, offset from their parents - only recalculate if they are dirty (changed) or if their parents changed

namespace Zoxel.Items
{
    //! User equipment item!
    public struct EquipmentParent : IComponentData
    {
        public Entity parent;
        public byte parentSlotIndex;

        public EquipmentParent(Entity parent, byte parentSlotIndex)
        {
            this.parent = parent;
            this.parentSlotIndex = parentSlotIndex;
        }
    }
}

// relating to the slot used to equip the item
// 0, to 4, 0 is head, 1,2 shoulder, 3 is hips etc
// public float durability;    // todo: this

/*
public byte bodyIndex;      // 0 if is core - Equipment Index
public byte slotIndex;      // 0 or 1, depending on what side of body, index of slot with the same input slot
public byte layer;          // SlotLayer
*/