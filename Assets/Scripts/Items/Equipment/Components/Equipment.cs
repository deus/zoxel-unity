﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Items
{
    //! Holds EquipmentParent's for body creation.
    public struct Equipment : IComponentData
    {
        //! Links to UserEquipment Entities.
        public BlitableArray<Entity> body;

        public void DisposeFinal()
        {
            body.DisposeFinal();
        }

        public void Dispose()
        {
            body.Dispose();
        }

        public void Initialize(byte count)
        {
			if (body.Length != count)
			{
				Dispose();
				body = new BlitableArray<Entity>(count, Allocator.Persistent);
				for (int i = 0; i < body.Length; i++)
				{
					body[i] = new Entity();
				}
			}
        }
    }
}