using Unity.Entities;

namespace Zoxel.Items
{
    //! The users equipment reference and data.
    public struct ItemDurability : IComponentData
    {
        public byte durability;

        public ItemDurability(byte durability)
        {
            this.durability = durability;
        }
    }
}