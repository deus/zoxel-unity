namespace Zoxel.Items
{
    public struct RemovedPart
    { 
        public EquipmentParent item;
        public EmptySlot emptySlot;
    }
}