using Unity.Entities;

namespace Zoxel.Items
{
    //! An event for linking up UserEquipment entities after spawning.
    public struct OnUserEquipmentSpawned : IComponentData
    {
        public byte spawned;

        public OnUserEquipmentSpawned(byte spawned)
        {
            this.spawned = spawned;
        }
    }
}