using Unity.Entities;

namespace Zoxel.Items
{
    //! The users equipment reference and data.
    public struct UserEquipment : IComponentData { }
}