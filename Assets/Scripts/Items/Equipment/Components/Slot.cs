using Unity.Entities;

namespace Zoxel.Items
{
    //! Data used to equip items.
    public struct Slot : IComponentData
    {
        //! This is equal to 1 if it is the Core slot.
        public byte core;
        public byte layer;      // 0 for body, 1 for gear
        public byte anchor;
        public byte modifier;
        public BlitableArray<Entity> femaleSlots;
        public BlitableArray<byte> femaleModifiers;

        public void Dispose()
        {
            if (femaleSlots.Length > 0)
            {
                femaleSlots.Dispose();
            }
            if (femaleModifiers.Length > 0)
            {
                femaleModifiers.Dispose();
            }
        }
    }
}