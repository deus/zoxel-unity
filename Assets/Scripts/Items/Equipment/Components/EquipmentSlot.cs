using Unity.Entities;
using Zoxel.Voxels;

namespace Zoxel.Items
{
    // data for equipment slots
    public struct EquipmentSlot : IComponentData
    {
        public int id;        // replace with ZoxID
        public byte anchor;   // anchor for voxel item to connect with other - 0 for Core
        public byte layer;    // 0 for body, 1 for gear
        public BlitableArray<int> femaleSlots;
        // shouldn't i put these directly in child slot?
        // public BlitableArray<int3> femaleOffsets;
        // public BlitableArray<byte> femaleModifiers;
        // For example, if a chest model is made, i would have two arm slots from it
        // So each Slot contains information on how it connects with other models
        
        // a chest has these female slots:
        // 2 shoulder slots
        // 1 head slot
        // 1 hip slot
        // possible 2 wing slots

        // the chest model it self determines what connects in it..!
        // perhaps slots are outdated and that information needs to go into the model itself

        public void Dispose()
        {
            if (femaleSlots.Length > 0)
            {
                femaleSlots.Dispose();
            }
        }
    }

}