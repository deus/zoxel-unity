using Unity.Entities;
// EquipmentParent has SlotLink and UserStatLinks

namespace Zoxel.Items
{
    public struct SlotLink : IComponentData
    {
        public Entity slot;

        public SlotLink(Entity slot)
        {
            this.slot = slot;
        }
    }
}