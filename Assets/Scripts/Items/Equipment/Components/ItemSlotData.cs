using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Items
{
    //! Offsets an item in the skeleton system. Also offsets for attached voxes.
    public struct ItemSlotData : IComponentData
    {
		const int dataSize = 12;
		const int alignment = 4;
        //! Offset from spawn point.
        public int3 offset;
        //! Child offset positions.
        public BlitableArray<int3> femaleOffsets;

        public ItemSlotData(int3 offset)
        {
            this.offset = offset;
            this.femaleOffsets = new BlitableArray<int3>();
        }

        public ItemSlotData(int3 offset, in NativeList<int3> femaleOffsets)
        {
            this.offset = offset;
            this.femaleOffsets = new BlitableArray<int3>(femaleOffsets.Length, Allocator.Persistent, dataSize, alignment);
            for (int i = 0; i < femaleOffsets.Length; i++)
            {
                this.femaleOffsets[i] = femaleOffsets[i];
            }
        }

        public void Dispose()
        {
            if (femaleOffsets.Length > 0)
            {
                femaleOffsets.Dispose();
            }
        }
    }
}