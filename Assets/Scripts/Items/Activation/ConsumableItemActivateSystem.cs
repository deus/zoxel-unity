using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Actions;
using Zoxel.Skills;
using Zoxel.Stats;
using Zoxel.UI;
using Zoxel.Voxels;
using Zoxel.VoxelInteraction;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;
using Zoxel.Items.Authoring;

namespace Zoxel.Items
{
    //! Activate an item with userActionLinks!
    /**
    *   \todo Refactor ActivateItem into a generic system that preceeds this and voxel items.
    *   \todo add drinking animation
    *   \todo particles for healing
    *   \todo add cooldown for other userActionLinks until its done, how should i do this?
    *   \todo play failure sound
    *   \todo UI effects for failed to use item
    */
    [BurstCompile, UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class ConsumableItemActivateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userItemsQuery;
        private EntityQuery itemsQuery;
        private EntityQuery voxelsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			userItemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserItem>(),
                ComponentType.Exclude<DestroyEntity>());
			itemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Item>(),
                ComponentType.ReadOnly<ConsumableItem>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(userItemsQuery);
            RequireForUpdate(itemsQuery);
            RequireForUpdate<ItemsSettings>();
            RequireForUpdate<SoundSettings>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var itemsSettings = GetSingleton<ItemsSettings>();
            var soundSettings = GetSingleton<SoundSettings>();
            var sampleRate = soundSettings.sampleRate;
            var activateItemSoundVolume = soundSettings.activateItemSoundVolume;
            var isUnlimitedItems = itemsSettings.isUnlimitedItems;
            var voxelSpawnPrefab = VoxelPlaceSystem.voxelSpawnPrefab;
            var soundPrefab = SoundPlaySystem.soundPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var healingEventPrefab = HealSystem.healingEventPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var consumableItems = GetComponentLookup<ConsumableItem>(true);
            itemEntities.Dispose();
            var inventoryItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var userItems = GetComponentLookup<UserItemActivated>(false);
			var userItemQuantitys = GetComponentLookup<UserItemQuantity>(false);
			var userMetaDatas = GetComponentLookup<MetaData>(false);
            inventoryItemEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<ActivateAction>()
                .ForEach((Entity e, int entityInQueryIndex, ref UserActionLinks userActionLinks, in UserStatLinks userStatLinks, in Translation translation, in Inventory inventory) =>
            {
                var userItemEntity = userActionLinks.GetSelectedTarget();
                if (!HasComponent<UserItem>(userItemEntity))
                {
                    return;
                }
                var userItem = userItems[userItemEntity];
                var userItemQuantity = userItemQuantitys[userItemEntity];
                if (!((isUnlimitedItems || userItemQuantity.quantity > 0) && elapsedTime - userItem.lastActivated >= 1f))
                {
                    return;
                }
                var usedItem = false;
                var selectedItemEntity = userMetaDatas[userItemEntity].data;
                if (HasComponent<ConsumableItem>(selectedItemEntity))
                {
                    var consumableItem = consumableItems[selectedItemEntity];
                    // consumableItem -> stat, value
                    // Add Healing Effect
                    // todo: grab healing amount from item userStatLinks
                    var healthUserStat = new Entity();
                    for (int i = 0; i < userStatLinks.stats.Length; i++)
                    {
                        if (HasComponent<HealthStat>(userStatLinks.stats[i]))
                        {
                            healthUserStat = userStatLinks.stats[i];
                            break;
                        }
                    }
                    if (healthUserStat.Index > 0)
                    {
                        usedItem = true;
                        var healingEventEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, healingEventPrefab);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, healingEventEntity, new Healing(3, healthUserStat));
                    }
                    // PostUpdateCommands.AddComponent(entityInQueryIndex, e, new Healing { value = 3, targetIndex = 0 });
                }
                if (!usedItem)
                {
                    return;
                }
                // todo: create another event for this
                //  that will generate the sound for what type of event it is
                //  character - places - block = generate sound based on voxel and character and what it is placed on
                var soundSeed = (int)(256 * elapsedTime);   // zoxID.id + 
                var random = new Random();
                random.InitState((uint)soundSeed);
                var soundEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, soundPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Seed(soundSeed));
                var generateSound = new GenerateSound();
                generateSound.CreateMusicSound((byte)(30 + random.NextInt(2)), 1, sampleRate);
                generateSound.timeLength = 0.6f;
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new SoundData(ref random, generateSound.timeLength, generateSound.sampleRate, 1, activateItemSoundVolume));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new DelayEvent(elapsedTime, 0.05));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, translation); // new Translation { Value = translation.Value });
                userItem.lastActivated = (float) elapsedTime;
                if (!isUnlimitedItems)
                {
                    userItemQuantity.quantity--;
                }
                var zeroMetaEntity = new Entity();
                if (userItemQuantity.quantity == 0)
                {
                    zeroMetaEntity = selectedItemEntity;
                    userItem.lastActivated = 0;
                    userMetaDatas[userItemEntity] = new MetaData();
                }
                userItems[userItemEntity] = userItem;
                userItemQuantitys[userItemEntity] = userItemQuantity;
                // UI effects for global cooldown
                var totalQuantity = userItemQuantity.quantity;
                for (int i = 0; i < inventory.items.Length; i++)
                {
                    var userItemEntity2 = inventory.items[i];
                    var itemEntity = userMetaDatas[userItemEntity2].data;
                    if (itemEntity.Index != 0 && selectedItemEntity == itemEntity)
                    {
                        totalQuantity += userItemQuantitys[userItemEntity2].quantity;
                    }
                }
                if (totalQuantity == 0)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ActionTargetUpdated(userItemEntity, zeroMetaEntity, 1));
                }
                else // if (quantity != 0) //  && voxelType != 0)
                {
                    // if quantity total is not 0, and the UserItem quantity is zero, relink aciton to new ItemUI.
                    var targetMeta = selectedItemEntity;
                    if (zeroMetaEntity.Index != 0)
                    {
                        var newItemTarget = new Entity();
                        for (int i = 0; i < inventory.items.Length; i++)
                        {
                            var userItemEntity2 = inventory.items[i];
                            var itemEntity = userMetaDatas[userItemEntity2].data;
                            if (zeroMetaEntity == itemEntity)
                            {
                                newItemTarget = userItemEntity2;
                                var userItem2 = userItems[userItemEntity2];
                                userItem2.lastActivated = (float) elapsedTime;
                                userItems[userItemEntity2] = userItem2;
                                targetMeta = selectedItemEntity;
                                break;
                            }
                        }
                        var action = userActionLinks.actions[userActionLinks.selectedAction];
                        action.target = newItemTarget;
                        userActionLinks.actions[userActionLinks.selectedAction] = action;
                    }
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ActionTargetQuantityUpdated(userItemEntity, targetMeta));
                }
                PostUpdateCommands.AddComponent<SaveInventory>(entityInQueryIndex, e);
                // need an animation for this
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ActivatedAction(userActionLinks.selectedAction, elapsedTime, 0.5f, 0.5f));
            })  .WithReadOnly(consumableItems)
                .WithNativeDisableContainerSafetyRestriction(userItems)
                .WithNativeDisableContainerSafetyRestriction(userItemQuantitys)
                .WithNativeDisableContainerSafetyRestriction(userMetaDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}