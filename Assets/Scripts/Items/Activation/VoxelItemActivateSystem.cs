using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Actions;
using Zoxel.Skills;
using Zoxel.Stats;
using Zoxel.UI;
using Zoxel.Voxels;
using Zoxel.VoxelInteraction;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;
using Zoxel.Items.Authoring;

namespace Zoxel.Items
{
    //! Activate an item with userActionLinks!
    /**
    *   BigMinivox - Checks if can spawn by checking all are air.
    *   \todo For BigMinivox, spawn a group of voxels here instead of just one.
    *   \todo Place voxel animation
    */
    [BurstCompile, UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class VoxelItemActivateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userItemsQuery;
        private EntityQuery voxelItemsQuery;
        private EntityQuery voxelsQuery;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			userItemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserItem>(),
                ComponentType.Exclude<DestroyEntity>());
			voxelItemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Item>(),
                ComponentType.ReadOnly<VoxelItem>(),
                ComponentType.Exclude<DestroyEntity>());
			voxelsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Voxel>(),
                ComponentType.Exclude<DestroyEntity>());
            chunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<PlanetChunk>(),  //! \todo Replace with EditableChunk
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkNeighbors>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(userItemsQuery);
            RequireForUpdate(voxelItemsQuery);
            RequireForUpdate(voxelsQuery);
            RequireForUpdate(chunksQuery);
            RequireForUpdate<ItemsSettings>();
            RequireForUpdate<SoundSettings>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var itemsSettings = GetSingleton<ItemsSettings>();
            var isUnlimitedItems = itemsSettings.isUnlimitedItems;
            var voxelSpawnPrefab = VoxelPlaceSystem.voxelSpawnPrefab;
            var soundPrefab = SoundPlaySystem.soundPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var soundSettings = GetSingleton<SoundSettings>();
            var sampleRate = soundSettings.sampleRate;
            var activateItemSoundVolume = soundSettings.activateItemSoundVolume;
            var healingEventPrefab = HealSystem.healingEventPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var itemEntities = voxelItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var voxelItems = GetComponentLookup<VoxelItem>(true);
            itemEntities.Dispose();
            var inventoryItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var userItems = GetComponentLookup<UserItemActivated>(false);
			var userItemQuantitys = GetComponentLookup<UserItemQuantity>(false);
			var userMetaDatas = GetComponentLookup<MetaData>(false);
            inventoryItemEntities.Dispose();
            var voxelEntities = voxelsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var blockDimensions = GetComponentLookup<BlockDimensions>(true);
            voxelEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
			var chunks = GetComponentLookup<Chunk>(true);
			var chunkNeighbors = GetComponentLookup<ChunkNeighbors>(true);
            chunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<ActivateAction>()
                .ForEach((Entity e, int entityInQueryIndex, ref UserActionLinks userActionLinks, in RaycastVoxel raycastVoxel, in VoxLink voxLink, in Translation translation, in GravityQuadrant gravityQuadrant, in Inventory inventory) =>
            {
                var userItemEntity = userActionLinks.GetSelectedTarget();
                if (!userItems.HasComponent(userItemEntity))
                {
                    return;
                }
                var userItem = userItems[userItemEntity];
                var userItemQuantity = userItemQuantitys[userItemEntity];
                if (!((isUnlimitedItems || userItemQuantity.quantity > 0) && elapsedTime - userItem.lastActivated >= 1f))
                {
                    return;
                }
                var usedItem = false;
                var selectedItemEntity = userMetaDatas[userItemEntity].data;
                if (voxelItems.HasComponent(selectedItemEntity))
                {
                    // todo: only place item on top of solid block (not on top of minivox, or water)
                    var voxelItem = voxelItems[selectedItemEntity];
                    var voxelRotation = BlockRotation.GetBlockRotation(gravityQuadrant.quadrant, raycastVoxel.blockRotationRegion);
                    var canPlaceVoxel = raycastVoxel.rayHitType == (byte) RayHitType.Voxel && raycastVoxel.hitVoxel.meshIndex == MeshType.Block;
                    var voxEntityHit = raycastVoxel.hitVoxel.vox;
                    if (blockDimensions.HasComponent(voxelItem.voxel))  // BigMinivoxVoxel
                    {
                        //! Checks if all are air. Passes in chunk and voxel data of placement.
                        var positionOffsets = blockDimensions[voxelItem.voxel].GetPositionOffsets(voxelRotation);
                        var chunk = chunks[raycastVoxel.normalVoxel.chunk];
                        var chunkNeighbors2 = chunkNeighbors[raycastVoxel.normalVoxel.chunk];
                        var voxelDimensions = chunk.voxelDimensions;
                        // UnityEngine.Debug.LogError("Checking Positions: " + positionOffsets.Length);
                        for (var j = 0; j < positionOffsets.Length; j++)
                        {
                            var voxelPosition = raycastVoxel.normalVoxel.position + positionOffsets[j];
                            // get chunk position
                            // get chunk
                            // check voxels in chunk
                            var localPosition = VoxelUtilities.GetLocalPosition(voxelPosition, voxelDimensions);
                            var voxelType = GetVoxelValue(localPosition, voxelDimensions, in chunk, in chunkNeighbors2, in chunks);
                            if (voxelType != 0)
                            {
                                //UnityEngine.Debug.LogError("Cannot place at " + voxelPosition);
                                canPlaceVoxel = false;
                                break;
                            }
                            /*else
                            {
                                UnityEngine.Debug.LogError("Place availale at: " + voxelPosition + " :: " + voxelType);
                            }*/
                        }
                        positionOffsets.Dispose();
                    }
                    /*else
                    {
                        UnityEngine.Debug.LogError("Placement Voxel is not BigMinivox at: " + voxelItem.voxel.Index);
                    }*/
                    if (canPlaceVoxel)
                    {
                        usedItem = true;
                        //! \todo If door, check two spots, place in both of them!
                        //UnityEngine.Debug.LogError("Placing Block Direction: " + voxelRotation + " with Region: " + raycastVoxel.blockRotationRegion + " and Quadrant: " + gravityQuadrant.quadrant);
                        // get place type
                        var voxelType = voxelItem.voxelType;
                        if (blockDimensions.HasComponent(voxelItem.voxel)) // <BigMinivoxVoxel>
                        {
                            var positionOffsets = blockDimensions[voxelItem.voxel].GetPositionOffsets(voxelRotation);
                            for (var j = 0; j < positionOffsets.Length; j++)
                            {
                                var voxelPosition = raycastVoxel.normalVoxel.position + positionOffsets[j];
                                // UnityEngine.Debug.LogError("voxelPosition: " + j + ": " + voxelPosition);
                                VoxelPlaceSystem.SpawnVoxel(PostUpdateCommands, entityInQueryIndex, voxelSpawnPrefab, voxLink.vox,
                                    voxelType, voxelPosition, voxelRotation);
                            }
                            positionOffsets.Dispose();
                        }
                        else
                        {
                            VoxelPlaceSystem.SpawnVoxel(PostUpdateCommands, entityInQueryIndex, voxelSpawnPrefab, voxLink.vox, voxelType, raycastVoxel.normalVoxel.position, voxelRotation);
                        }
                        // UnityEngine.Debug.LogError("Placed Voxel at: " + elapsedTime);
                        #if DEBUG_VOXEL_PLACEMENT
                        UnityEngine.Debug.LogError(raycastVoxel.normalVoxel.chunk.Index + " has Started Voxel Placement at: " + elapsedTime);
                        #endif
                    }
                }
                if (!usedItem)
                {
                    return;
                }
                // todo: create another event for this
                //  that will generate the sound for what type of event it is
                //  character - places - block = generate sound based on voxel and character and what it is placed on
                var soundSeed = (int)(256 * elapsedTime);   // zoxID.id + 
                var random = new Random();
                random.InitState((uint)soundSeed);
                var soundEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, soundPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Seed(soundSeed));
                var generateSound = new GenerateSound();
                generateSound.CreateMusicSound((byte)(30 + random.NextInt(2)), 1, sampleRate);
                generateSound.timeLength = 0.6f;
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new SoundData(ref random, generateSound.timeLength, generateSound.sampleRate, 1, activateItemSoundVolume));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new DelayEvent(elapsedTime, 0.05));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, translation); // new Translation { Value = translation.Value });
                userItem.lastActivated = (float) elapsedTime;
                if (!isUnlimitedItems)
                {
                    userItemQuantity.quantity--;
                }
                var zeroMetaEntity = new Entity();
                if (userItemQuantity.quantity == 0)
                {
                    zeroMetaEntity = selectedItemEntity;
                    userItem.lastActivated = 0;
                    userMetaDatas[userItemEntity] = new MetaData();
                }
                userItems[userItemEntity] = userItem;
                userItemQuantitys[userItemEntity] = userItemQuantity;
                // UI effects for global cooldown
                var totalQuantity = userItemQuantity.quantity;
                for (int i = 0; i < inventory.items.Length; i++)
                {
                    var userItemEntity2 = inventory.items[i];
                    var itemEntity = userMetaDatas[userItemEntity2].data;
                    if (itemEntity.Index != 0 && selectedItemEntity == itemEntity)
                    {
                        totalQuantity += userItemQuantitys[userItemEntity2].quantity;
                    }
                }
                if (totalQuantity == 0)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ActionTargetUpdated(userItemEntity, zeroMetaEntity, 1));
                }
                else // if (quantity != 0) //  && voxelType != 0)
                {
                    // if quantity total is not 0, and the UserItem quantity is zero, relink aciton to new ItemUI.
                    var targetMeta = selectedItemEntity;
                    if (zeroMetaEntity.Index != 0)
                    {
                        var newItemTarget = new Entity();
                        for (int i = 0; i < inventory.items.Length; i++)
                        {
                            var userItemEntity2 = inventory.items[i];
                            var itemEntity = userMetaDatas[userItemEntity2].data;
                            if (zeroMetaEntity == itemEntity)
                            {
                                newItemTarget = userItemEntity2;
                                var userItem2 = userItems[userItemEntity2];
                                userItem2.lastActivated = (float) elapsedTime;
                                userItems[userItemEntity2] = userItem2;
                                targetMeta = selectedItemEntity;
                                break;
                            }
                        }
                        var action = userActionLinks.actions[userActionLinks.selectedAction];
                        action.target = newItemTarget;
                        userActionLinks.actions[userActionLinks.selectedAction] = action;
                    }
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ActionTargetQuantityUpdated(userItemEntity, targetMeta));
                }
                PostUpdateCommands.AddComponent<SaveInventory>(entityInQueryIndex, e);
                // need an animation for this
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ActivatedAction(userActionLinks.selectedAction, elapsedTime, 0.5f, 0.5f));
            })  .WithReadOnly(voxelItems)
                .WithReadOnly(blockDimensions)
                .WithReadOnly(chunks)
                .WithReadOnly(chunkNeighbors)
                .WithNativeDisableContainerSafetyRestriction(userItems)
                .WithNativeDisableContainerSafetyRestriction(userItemQuantitys)
                .WithNativeDisableContainerSafetyRestriction(userMetaDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static byte GetVoxelValue(int3 localPosition, int3 voxelDimensions, in Chunk chunk, in ChunkNeighbors chunkNeighbors,
            in ComponentLookup<Chunk> chunks)
        {
            if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
            {
                return chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)];
            }
            else
            {
                var chunkEntity = chunkNeighbors.GetChunk(ref localPosition, voxelDimensions);
                var chunk2 = chunks[chunkEntity];
                if (chunk2.voxels.Length == 0)
                {
                    return 1;
                }
                return chunk2.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)];
            }
        }
    }
}