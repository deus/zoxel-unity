﻿using UnityEngine;
using Zoxel.Voxels;

namespace Zoxel.Items
{
    //! Stores an item data for editor editing!
    [CreateAssetMenu(fileName = "Item", menuName = "Zoxel/Item")]
    public partial class ItemDatam : ScriptableObject // , ISerializationCallbackReceiver
    {
        public ZoxID zoxID;
        public ItemTypeEditor type;
        public byte itemValue;
        public VoxDatam model;
        public SlotDatam slot;
        
        [ContextMenu("Generate ID")]
        public void GenerateID()
        {
            zoxID.id = IDUtil.GenerateUniqueID();
        }
    }
}