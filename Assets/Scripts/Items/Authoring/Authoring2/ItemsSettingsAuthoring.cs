using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Zoxel.Items.Authoring
{
    //! Settings for Items, including cheats.
    // [GenerateAuthoringComponent]
    public struct ItemsSettings : IComponentData
    {
        public float itemLifeTime;      // 90
        public float itemPickupRadius;  // 1
        public float3 heldVoxOffset;    // -0.086, 0.06, 0
        public float3 heldVoxelOffset;  // -0.14, 0.06, 0
        public byte playerItemsCount;    // 12
        public bool isDebugHeldItem;
        public bool isUnlimitedItems;
        public bool disableWorldItems;
    }

    public class ItemsSettingsAuthoring : MonoBehaviour
    {
        public float itemLifeTime;      // 90
        public float itemPickupRadius;  // 1
        public float3 heldVoxOffset;    // -0.086, 0.06, 0
        public float3 heldVoxelOffset;  // -0.14, 0.06, 0
        public byte playerItemsCount;    // 12
        public bool isDebugHeldItem;
        public bool isUnlimitedItems;
        public bool disableWorldItems;
    }

    public class ItemsSettingsAuthoringBaker : Baker<ItemsSettingsAuthoring>
    {
        public override void Bake(ItemsSettingsAuthoring authoring)
        {
            AddComponent(new ItemsSettings
            {
                itemLifeTime = authoring.itemLifeTime,
                itemPickupRadius = authoring.itemPickupRadius,
                heldVoxOffset = authoring.heldVoxOffset,
                heldVoxelOffset = authoring.heldVoxelOffset,
                playerItemsCount = authoring.playerItemsCount,
                isDebugHeldItem = authoring.isDebugHeldItem,
                isUnlimitedItems = authoring.isUnlimitedItems,
                disableWorldItems = authoring.disableWorldItems,
            });       
        }
    }
}

        /*public void DrawUI()
        {
            GUILayout.Label(" [items] ");

            GUILayout.Label("Item Life Time");
            itemLifeTime = float.Parse(GUILayout.TextField(itemLifeTime.ToString()));

            GUILayout.Label("Item Pickup Radius");
            itemPickupRadius = float.Parse(GUILayout.TextField(itemPickupRadius.ToString()));

            //GUILayout.Label("   Render Distance Y");
            //renderDistanceY = int.Parse(GUILayout.TextField(renderDistanceY.ToString()));
            
            
            //GUILayout.Label("Player Starting Health:");
            //defaultPlayerHealth = float.Parse(GUILayout.TextField(defaultPlayerHealth.ToString()));
            
            //GUILayout.Label("Default Voxel Health:");
            //defaultVoxelHealth = int.Parse(GUILayout.TextField(defaultVoxelHealth.ToString()));

            //GUILayout.Label("Debug:");
            //disableStatbars = GUILayout.Toggle(disableStatbars, "Disable Statbars");
        }

        public void DrawDisableUI()
        {
            GUILayout.Label(" [debug visuals] ");
            isDebugHeldItem = GUILayout.Toggle(isDebugHeldItem, "Debug Held Item");

            GUILayout.Label(" [cheats] ");
            isUnlimitedItems = GUILayout.Toggle(isUnlimitedItems, "Unlimited Items");

            GUILayout.Label(" [disables] ");
            disableWorldItems = GUILayout.Toggle(disableWorldItems, "Disable World Items");

            // GUILayout.Label(" [debug planet] ");
        }*/