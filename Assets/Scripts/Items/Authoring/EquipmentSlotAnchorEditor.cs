using System;

namespace Zoxel.Items
{
    //! Equipment Slot Anchor for data.
    [Serializable]
    public enum EquipmentSlotAnchorEditor : byte
    {
        Center,
        Left,
        Right,
        Bottom,
        Top,
        Back,
        Front
    }
}