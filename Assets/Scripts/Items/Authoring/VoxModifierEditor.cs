namespace Zoxel.Items
{
    //! Used to rotate Vox based on EquipmentSlot.
    public enum VoxModifierEditor
    {
        None,
        RotateXY,
        RotateX,
        RotateY
    }
}