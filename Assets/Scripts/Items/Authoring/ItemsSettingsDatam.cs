using UnityEngine;

namespace Zoxel.Items
{
    [CreateAssetMenu(fileName = "ItemsSettings", menuName = "ZoxelSettings/ItemsSettings")]
    public partial class ItemsSettingsDatam : ScriptableObject
    {
        public ItemDatam[] items;
        public SlotDatam[] slots;
    }
}