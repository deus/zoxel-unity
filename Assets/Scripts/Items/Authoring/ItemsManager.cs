using Unity.Mathematics;
using UnityEngine;
using System.Collections.Generic;

namespace Zoxel.Items
{
    public partial class ItemsManager : MonoBehaviour
    {
        public static ItemsManager instance;
        [UnityEngine.SerializeField] public ItemsSettingsDatam ItemsSettings;

        public void Awake()
        {
            instance = this;
        }

        public Dictionary<int, string> GetSlotNames()
        {
            var slotsMeta = new Dictionary<int, string>();
            foreach (var data in ItemsSettings.slots)
            {
                if (data.data.id == 0)
                {
                    data.data.id = IDUtil.GenerateUniqueID();
                }
                slotsMeta.Add(data.data.id, data.name);
            }
            return slotsMeta;
        }
    }
}

/*public void OnGUI()
{
    GUI.skin.label.fontSize = fontSize;
    GUI.color = fontColor;
    DrawUI();
}

public void DrawUI()
{
    GUILayout.Label("   > World Items [" + ItemSystemGroup.worldItemsCount + "]");
    realItemsSettings.DrawUI();
    realItemsSettings.DrawDisableUI();
}*/