using System;

namespace Zoxel.Items
{
    //! Used for EquipmentSlotAnchor and EquipLayer types to be used as dropdowns.
    [Serializable]
    public struct EquipSlotEditor
    {
        public int id;
        public EquipLayer layer;  // 0 for body, 1 for gear
        public byte core;         // 1 is core - for chest?
        public EquipmentSlotAnchorEditor anchor;     // Anchor for slot

        public void GenerateID()
        {
            id = IDUtil.GenerateUniqueID();
        }
    }
}