namespace Zoxel.Items
{    
    //! Performs a vox operation on the model attached to the skeleton.
    public enum VoxOperationEditor : byte
    {
        None,
        FlipX
    }
}