using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zoxel.Voxels;
using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Items
{
    //! Used to author equipment slot data in the editor.
    [CreateAssetMenu(fileName = "Slot", menuName = "Zoxel/Slot")]
    public partial class SlotDatam : ScriptableObject
    {
        [Header("Data")]
        public EquipSlotEditor data;
        public VoxModifierEditor modifier;
        
        [Header("Slots")]
        public List<SlotDatam> femaleSlots;
        public List<VoxOperationEditor> femaleModifiers;
        
        [ContextMenu("Generate ID")]
        public void GenerateID()
        {
            data.id = IDUtil.GenerateUniqueID();
        }

        public Slot GenerateData()
        {
            var slot = new Slot();
            slot.modifier = (byte) modifier;
            slot.core = (byte) data.core;
            slot.anchor = (byte) data.anchor;
            slot.layer = (byte) data.layer;
            slot.core = (byte) data.core;
            slot.femaleSlots = new BlitableArray<Entity>(femaleSlots.Count, Allocator.Persistent);
            slot.femaleModifiers = new BlitableArray<byte>(femaleModifiers.Count, Allocator.Persistent);
            for (int i = 0; i < slot.femaleModifiers.Length; i++)
            {
                slot.femaleModifiers[i] = (byte) femaleModifiers[i];
            }
            return slot;
        }
    }
}