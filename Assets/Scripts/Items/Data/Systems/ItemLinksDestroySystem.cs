using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Items
{
    //! Disposes of ItemLinks made up of Unique items.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ItemLinksDestroySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithAll<DestroyEntity, ItemLinks>()
                .ForEach((int entityInQueryIndex, in ItemLinks itemLinks) =>
			{
                for (int i = 0; i < itemLinks.items.Length; i++)
                {
                    var itemEntity = itemLinks.items[i];
                    if (HasComponent<Item>(itemEntity))
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, itemEntity);
                    }
                }
                itemLinks.DisposeFinal();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}