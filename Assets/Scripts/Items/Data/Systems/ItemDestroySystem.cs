using Unity.Entities;
using Unity.Burst;
// todo: Generate consumeable items

namespace Zoxel.Items
{
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ItemSlotDestroySystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, ItemSlotData>()
                .ForEach((in ItemSlotData itemSlotData) =>
            {
                itemSlotData.Dispose();
			}).ScheduleParallel();
        }
    }
}