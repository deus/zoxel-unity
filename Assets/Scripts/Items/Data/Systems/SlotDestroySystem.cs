using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;

namespace Zoxel.Items
{
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class SlotDestroySystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, Slot>()
                .ForEach((in Slot slot) =>
            {
                slot.Dispose();
			}).ScheduleParallel();
        }
    }
}