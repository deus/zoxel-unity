using Unity.Entities;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Zoxel.Items
{
    //! Links to slot entities.
    public struct SlotLinks : IComponentData
    {
        public BlitableArray<Entity> slots;
        public UnsafeParallelHashMap<int, Entity> slotsHash;

        public void Dispose()
        {
            slots.Dispose();
			if (slotsHash.IsCreated)
			{
				slotsHash.Dispose();
			}
        }

        public void Initialize(int count)
        {
            Dispose();
            this.slots = new BlitableArray<Entity>(count, Allocator.Persistent);
            this.slotsHash = new UnsafeParallelHashMap<int, Entity>(count, Allocator.Persistent);
        }

        public Entity GetSlot(int id)
        {
            if (slotsHash.IsCreated)
            {
                Entity outputEntity;
                if (slotsHash.TryGetValue(id, out outputEntity))
                {
                    return outputEntity;
                }
            }
            return new Entity();
        }
    }
}