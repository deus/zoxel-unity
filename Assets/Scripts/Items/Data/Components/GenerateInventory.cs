using Unity.Entities;

namespace Zoxel.Items
{
    public struct GenerateInventory : IComponentData
    {
        public byte spawnType;
        
        public GenerateInventory(byte spawnType)
        {
            this.spawnType = spawnType;
        }
    }
}