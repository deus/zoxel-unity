using Unity.Entities;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Zoxel.Items
{
    //! Links to Item's data!
    public struct ItemLinks : IComponentData
    {
        public BlitableArray<Entity> items;
        public UnsafeParallelHashMap<int, Entity> itemsHash;

        public void DisposeFinal()
        {
            items.DisposeFinal();
			if (itemsHash.IsCreated)
			{
				itemsHash.Dispose();
			}
        }

        public void Dispose()
        {
            items.Dispose();
			if (itemsHash.IsCreated)
			{
				itemsHash.Dispose();
			}
        }

        public void Initialize(int count)
        {
            this.Dispose();
            this.items = new BlitableArray<Entity>(count, Allocator.Persistent);
            this.itemsHash = new UnsafeParallelHashMap<int, Entity>(count, Allocator.Persistent);
        }

        public Entity GetItem(int itemID)
        {
            if (itemsHash.IsCreated)
            {
                Entity outputEntity;
                if (itemsHash.TryGetValue(itemID, out outputEntity))
                {
                    return outputEntity;
                }
            }
            return new Entity();
        }

        public void AddNew(int addition)
        {
            if (items.Length == 0)
            {
                Initialize(addition);
                return;
            }
            var items2 = new BlitableArray<Entity>(items.Length + addition, Allocator.Persistent);
            for (int i = 0; i < items.Length; i++)
            {
                items2[i] = items[i];
            }
            for (int i = items.Length; i < items2.Length; i++)
            {
                items2[i] = new Entity();
            }
            if (items.Length > 0)
            {
                items.Dispose();
            }
            items = items2;
        }
    }
}