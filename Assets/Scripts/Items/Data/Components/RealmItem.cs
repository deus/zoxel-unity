using Unity.Entities;

namespace Zoxel.Items
{
    //! A non unique item that is held by the Realm.
    public struct RealmItem : IComponentData { }
}