﻿using Unity.Entities;

namespace Zoxel.Items
{
    //! Item systems involve inventory, equipment, trading, etc.
    [AlwaysUpdateSystem]
    public partial class ItemSystemGroup : ComponentSystemGroup
    {
        public static int worldItemsCount;
		private EntityQuery worldItemsQuery;
        public static Entity itemPrefab;
        public static Entity userItemPrefab;
        public static Entity userEquipmentPrefab;

        protected override void OnCreate()
        {
            base.OnCreate();
			worldItemsQuery = GetEntityQuery(ComponentType.ReadOnly<WorldItem>());
            var itemArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(Item),
                typeof(ZoxName),
                typeof(ZoxID)
            );
            itemPrefab = EntityManager.CreateEntity(itemArchetype);
            var userItemArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(NewUserItem),
                typeof(UserItem),
                typeof(UserItemActivated),
                typeof(UserItemQuantity),
                typeof(MetaData),
                typeof(UserDataIndex),
                typeof(InventoryLink)
            );
            userItemPrefab = EntityManager.CreateEntity(userItemArchetype);
            var userEquipmentArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(UserEquipment),
                typeof(EquipmentParent),
                typeof(MetaData),
                typeof(UserDataIndex),
                typeof(CreatorLink)
            );
            userEquipmentPrefab = EntityManager.CreateEntity(userEquipmentArchetype);
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();
            worldItemsCount = worldItemsQuery.CalculateEntityCount();
        }
    }
}
