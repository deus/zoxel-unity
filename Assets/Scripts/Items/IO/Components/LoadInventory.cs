using Unity.Entities;

namespace Zoxel.Items
{
    //! Loads UserItem's of an entity.
    public struct LoadInventory : IComponentData
    {
        public Text loadPath;
        public AsyncFileReader reader;

        public void Dispose()
        {
            loadPath.Dispose();
            reader.Dispose();
        }
    }
}