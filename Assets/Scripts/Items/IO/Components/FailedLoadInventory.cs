using Unity.Entities;

namespace Zoxel.Items
{
    //! Failed to load Inventory.
    public struct FailedLoadInventory : IComponentData { }
}