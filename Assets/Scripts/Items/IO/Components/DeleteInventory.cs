using Unity.Entities;

namespace Zoxel.Items
{
    //! Deletes the inventory from file.
    public struct DeleteInventory : IComponentData { }
}