using Unity.Entities;

namespace Zoxel.Items
{
    //! Saves an inventory on an entity.
    public struct SaveInventory : IComponentData { }
}