using Unity.Entities;

namespace Zoxel.Items
{
    public struct LoadEquipment : IComponentData
    {
        public Text loadPath;
        public AsyncFileReader reader;

        public void Dispose()
        {
            loadPath.Dispose();
            reader.Dispose();
        }
    }
}