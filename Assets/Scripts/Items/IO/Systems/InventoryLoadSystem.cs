using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Items
{
    //! Loads inventory for character.
    /**
    *   - Load System -
    */
    [BurstCompile, UpdateInGroup(typeof(ItemIOSystemGroup))]
    public partial class InventoryLoadSystem : SystemBase
    {
        const int bytesPerData = 5;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<ItemLinks>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var userItemPrefab = ItemSystemGroup.userItemPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var realmItemLinks = GetComponentLookup<ItemLinks>(true);
            var characterEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var characterItemLinks = GetComponentLookup<ItemLinks>(true);
            realmEntities.Dispose();
            characterEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithNone<InitializeEntity, LoadUniqueItems, OnUniqueItemsSpawned>()
                .WithNone<LoadStats, GenerateBody>()
                .ForEach((Entity e, int entityInQueryIndex, ref LoadInventory loadInventory, in RealmLink realmLink) =>
            {
                if (loadInventory.reader.IsReadFileInfoComplete())
                {
                    if (loadInventory.reader.DoesFileExist())
                    {
                        loadInventory.reader.state = AsyncReadState.StartOpenFile;
                    }
                    else
                    {
                        // UnityEngine.Debug.LogError("File Did not exist.");
                        PostUpdateCommands.RemoveComponent<LoadInventory>(entityInQueryIndex, e);
                        PostUpdateCommands.AddComponent<FailedLoadInventory>(entityInQueryIndex, e);
                        loadInventory.Dispose();
                    }
                }
                else if (loadInventory.reader.IsReadFileComplete())
                {
                    if (loadInventory.reader.DidFileReadSuccessfully())
                    {
                        var bytes = loadInventory.reader.GetBytes();
                        var itemLinks = new ItemLinks();
                        if (characterItemLinks.HasComponent(e))
                        {
                            itemLinks = characterItemLinks[e];
                        }
                        InventoryLoadSystem.LoadInventory(in bytes, PostUpdateCommands, entityInQueryIndex, e, userItemPrefab, in realmLink, in realmItemLinks, in itemLinks);
                        // var count = (byte)(bytes.Length / bytesPerData);
                        // UnityEngine.Debug.LogError("Chest path: " + loadInventory.loadPath.ToString() + " : " + count);
                    }
                    else
                    {
                        PostUpdateCommands.AddComponent<FailedLoadInventory>(entityInQueryIndex, e);
                    }
                    // Dispose
                    loadInventory.Dispose();
                    PostUpdateCommands.RemoveComponent<LoadInventory>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(realmItemLinks)
                .WithReadOnly(characterItemLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static bool LoadInventory(in BlitableArray<byte> bytes, EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e,
            Entity userPrefab, in RealmLink realmLink, in ComponentLookup<ItemLinks> realmItemLinks, in ItemLinks itemLinks)
        {
            var realmItemLinks2 = realmItemLinks[realmLink.realm];
            var byteIndex = 0;
            var count = (byte) (bytes.Length / bytesPerData);
            // UnityEngine.Debug.LogError("Loaded User Items: " + inventory.items.Length);
            var userEntities = new NativeArray<Entity>(count, Allocator.Temp);
            PostUpdateCommands.Instantiate(entityInQueryIndex, userPrefab, userEntities);
            PostUpdateCommands.AddComponent(entityInQueryIndex, userEntities, new InventoryLink(e));
            for (int i = 0; i < count; i++)
            {
                var itemID = ByteUtil.GetInt(in bytes, byteIndex);
                var itemQuantity = bytes[byteIndex + 4];
                var itemEntity = new Entity();
                if (itemID != 0)
                {
                    Entity outputEntity;
                    //! Uses Hashmap for inventory for fast access.
                    if (realmItemLinks2.itemsHash.TryGetValue(itemID, out outputEntity))
                    {
                        itemEntity = outputEntity;
                    }
                    else if (itemLinks.itemsHash.IsCreated && itemLinks.itemsHash.TryGetValue(itemID, out outputEntity))
                    {
                        itemEntity = outputEntity;
                    }
                    /*if (itemEntity.Index == 0)
                    {
                        UnityEngine.Debug.LogWarning("InventoryLoadSystem: Could not find itemID: " + itemID);
                    }*/
                }
                var userItemEntity = userEntities[i]; // PostUpdateCommands.Instantiate(entityInQueryIndex, userItemPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, new UserDataIndex(i));
                PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, new MetaData(itemEntity));
                PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, new UserItemQuantity(itemQuantity));
                byteIndex += bytesPerData;
            }
            userEntities.Dispose();
            PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUserItemsSpawned(count));
            // UnityEngine.Debug.LogError("InventoryLoadSystem: e [" + e.Index + "] Loaded UserItems: " + count + " bytez length: " + bytes.Length);
            return true;
        }
    }
}