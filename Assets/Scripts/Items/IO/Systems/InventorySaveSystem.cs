using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using System;
using System.IO;

namespace Zoxel.Items
{
    //! Saves an entities Inventory data.
    /**
    *   - Save System -
    */
    [UpdateInGroup(typeof(ItemIOSystemGroup))]
    public partial class InventorySaveSystem : SystemBase
    {
        const string filename = "Inventory.zox";
        const int bytesPerData = 5;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userItemsQuery;
        private EntityQuery itemsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userItemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserItem>(),
                ComponentType.Exclude<DestroyEntity>());
            itemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Item>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }
        
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands.RemoveComponent<SaveInventory>(processQuery);
            var userItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var metaDatas = GetComponentLookup<MetaData>(true);
            var userItemQuantitys = GetComponentLookup<UserItemQuantity>(true);
            userItemEntities.Dispose();
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var itemIDs = GetComponentLookup<ZoxID>(true);
            itemEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<OnUserItemsSpawned>()
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<LoadInventory, GenerateInventory>()
                .WithNone<InitializeSaver, DisableSaving>()
                .WithAll<SaveInventory>()
                .ForEach((in EntitySaver entitySaver, in Inventory inventory) =>
            {
                var filepath = entitySaver.GetPath() + filename;
                InventorySaveSystem.SaveInventory(filepath, in inventory, in metaDatas, in userItemQuantitys, in itemIDs);
            })  .WithReadOnly(metaDatas)
                .WithReadOnly(userItemQuantitys)
                .WithReadOnly(itemIDs)
                .WithoutBurst().Run();
        }

        public static void SaveInventory(string filepath, in Inventory inventory, in ComponentLookup<MetaData> metaDatas,
            in ComponentLookup<UserItemQuantity> userItemQuantitys, in ComponentLookup<ZoxID> itemIDs)
        {
            var bytes = new BlitableArray<byte>(inventory.items.Length * bytesPerData, Allocator.Temp);
            var byteIndex = 0;
            for (int i = 0; i < inventory.items.Length; i++)
            {
                var userItemEntity = inventory.items[i];
                if (userItemEntity.Index == 0 || !metaDatas.HasComponent(userItemEntity))
                {
                    ByteUtil.SetInt(ref bytes, byteIndex, 0);
                    bytes[byteIndex + 4] = 0;
                    byteIndex += bytesPerData;
                    continue;
                }
                var itemEntity = metaDatas[userItemEntity].data;
                if (itemEntity.Index == 0 || !itemIDs.HasComponent(itemEntity))
                {
                    ByteUtil.SetInt(ref bytes, byteIndex, 0);
                    bytes[byteIndex + 4] = 0;
                    byteIndex += bytesPerData;
                    continue;
                }
                ByteUtil.SetInt(ref bytes, byteIndex, itemIDs[itemEntity].id);
                bytes[byteIndex + 4] = userItemQuantitys[userItemEntity].quantity;
                byteIndex += bytesPerData;
            }
            #if WRITE_ASYNC
            File.WriteAllBytesAsync(filepath, bytes.ToArray());
            #else
            File.WriteAllBytes(filepath, bytes.ToArray());
            #endif
            bytes.Dispose();
            // UnityEngine.Debug.LogError("InventorySaveSystem: e [" + filepath + "] Saved UserItems: " + inventory.items.Length);
        }
    }
}