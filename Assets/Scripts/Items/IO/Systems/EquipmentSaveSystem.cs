using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using System;
using System.IO;

namespace Zoxel.Items
{
    //! Saves user equipment to a file.
    /**
    *   - Save System -
    */
    [UpdateInGroup(typeof(ItemIOSystemGroup))]
    public partial class EquipmentSaveSystem : SystemBase
    {
        const string filename = "Equipment.zox";
        const int bytesPerData = 6;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userEquipmentsQuery;
        private EntityQuery itemsQuery;
        private EntityQuery slotsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userEquipmentsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserEquipment>(),
                ComponentType.Exclude<DestroyEntity>());
            itemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Item>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<DestroyEntity>());
            slotsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Slot>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands.RemoveComponent<SaveEquipment>(processQuery);
            var userItemEntities = userEquipmentsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var metaDatas = GetComponentLookup<MetaData>(true);
            var equipmentItems = GetComponentLookup<EquipmentParent>(true);
            userItemEntities.Dispose();
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var itemIDs = GetComponentLookup<ZoxID>(true);
            var slotLinks = GetComponentLookup<SlotLink>(true);
            itemEntities.Dispose();
            var slotEntities = slotsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var slots = GetComponentLookup<Slot>(true);
            slotEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<LoadEquipment, OnUserEquipmentSpawned, GenerateBody>()
                .WithNone<InitializeSaver, DisableSaving>()
                .WithAll<SaveEquipment>()
                .ForEach((Entity e, in EntitySaver entitySaver, in Equipment equipment) =>
            {
                var filepath = entitySaver.GetPath() + filename;
                var bytes = new BlitableArray<byte>(equipment.body.Length * bytesPerData, Allocator.Temp);
                var byteIndex = 0;
                for (int i = 0; i < equipment.body.Length; i++)
                {
                    var userEquipmentEntity = equipment.body[i];
                    if (userEquipmentEntity.Index == 0 || !metaDatas.HasComponent(userEquipmentEntity))
                    {
                        for (var j = 0; j < bytesPerData; j++)
                        {
                            bytes[byteIndex + j] = 0;
                        }
                        byteIndex += bytesPerData;
                        continue;
                    }
                    var itemEntity = metaDatas[userEquipmentEntity].data;
                    if (itemEntity.Index == 0 || !itemIDs.HasComponent(itemEntity))
                    {
                        for (var j = 0; j < bytesPerData; j++)
                        {
                            bytes[byteIndex + j] = 0;
                        }
                        byteIndex += bytesPerData;
                        continue;
                    }
                    ByteUtil.SetInt(ref bytes, byteIndex, itemIDs[itemEntity].id);
                    var equipmentItem = equipmentItems[userEquipmentEntity];
                    bytes[byteIndex + 4] = GetParentIndex(in equipment, equipmentItem.parent);
                    var parentItemEntity = metaDatas[equipmentItem.parent].data;
                    var parentSlotEntity = slotLinks[parentItemEntity].slot;
                    var parentSlot = slots[parentSlotEntity];
                    bytes[byteIndex + 5] = equipmentItem.parentSlotIndex; // GetSlotIndex(in parentSlot, equipmentItem.parentSlot);
                    /*bytes[byteIndex + 5] = equipmentItem.slotIndex;
                    bytes[byteIndex + 6] = equipmentItem.layer;*/
                    byteIndex += bytesPerData;
                }
                #if WRITE_ASYNC
                File.WriteAllBytesAsync(filepath, bytes.ToArray());
                #else
                File.WriteAllBytes(filepath, bytes.ToArray());
                #endif
                bytes.Dispose();
            })  .WithReadOnly(metaDatas)
                .WithReadOnly(equipmentItems)
                .WithReadOnly(itemIDs)
                .WithReadOnly(slotLinks)
                .WithReadOnly(slots)
                .WithoutBurst().Run();
        }

        private static byte GetSlotIndex(in Slot parentSlot, Entity parentSlotEntity)
        {
            for (byte i = 0; i < parentSlot.femaleSlots.Length; i++)
            {
                if (parentSlot.femaleSlots[i] == parentSlotEntity)
                {
                    return i;
                }
            }
            return 255;
        }

        private static byte GetParentIndex(in Equipment equipment, Entity parentEntity)
        {
            for (byte i = 0; i < equipment.body.Length; i++)
            {
                if (equipment.body[i] == parentEntity)
                {
                    return i;
                }
            }
            return 255;
        }
    }
}