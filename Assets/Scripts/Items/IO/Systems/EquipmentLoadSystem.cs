using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Items
{
    //! Loads Equipment from file.
    /**
    *   Loads after GenerateBody event.
    *   - Load System -
    */
    [BurstCompile, UpdateInGroup(typeof(ItemIOSystemGroup))]
    public partial class EquipmentLoadSystem : SystemBase
    {
        const string filename = "Equipment.zox";
        const int bytesPerData = 6;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;
        private EntityQuery itemsQuery;
        private EntityQuery slotsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<ItemLinks>());
            itemsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Item>(),
                ComponentType.ReadOnly<SlotLink>());
            slotsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Slot>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithNone<OnUserEquipmentSpawned, LoadUniqueItems, OnUniqueItemsSpawned>()
                .WithAll<Equipment, RealmLink>()
                .ForEach((ref LoadEquipment loadEquipment, in EntitySaver entitySaver) =>
            {
                if (loadEquipment.loadPath.Length == 0)
                {
                    loadEquipment.loadPath = new Text(entitySaver.GetPath() + filename);
                    // UnityEngine.Debug.LogError("Equipment loaded: " + loadEquipment.loadPath);
                }
                loadEquipment.reader.UpdateOnMainThread(in loadEquipment.loadPath);
            }).WithoutBurst().Run();
            var userEquipmentPrefab = ItemSystemGroup.userEquipmentPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var realmItemLinks = GetComponentLookup<ItemLinks>(true);
            realmEntities.Dispose();
            var characterEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var characterItemLinks = GetComponentLookup<ItemLinks>(true);
            characterEntities.Dispose();
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var slotLinks = GetComponentLookup<SlotLink>(true);
            itemEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithNone<OnUserEquipmentSpawned, LoadUniqueItems, OnUniqueItemsSpawned>()
                .WithAll<Equipment, EntitySaver>()
                .ForEach((Entity e, int entityInQueryIndex, ref LoadEquipment loadEquipment, in RealmLink realmLink) =>
            {
                if (loadEquipment.reader.IsReadFileInfoComplete())
                {
                    if (loadEquipment.reader.DoesFileExist())
                    {
                        loadEquipment.reader.state = AsyncReadState.StartOpenFile;
                    }
                    else
                    {
                        // UnityEngine.Debug.LogError("File Did not exist.");
                        loadEquipment.Dispose();
                        PostUpdateCommands.RemoveComponent<LoadEquipment>(entityInQueryIndex, e);
                        PostUpdateCommands.AddComponent<SkeletonDirty>(entityInQueryIndex, e);
                    }
                }
                else if (loadEquipment.reader.IsReadFileComplete())
                {
                    if (loadEquipment.reader.DidFileReadSuccessfully())
                    {
                        var bytes = loadEquipment.reader.GetBytes();
                        var realmItemLinks2 = realmItemLinks[realmLink.realm];
                        var itemLinks = new ItemLinks();
                        if (characterItemLinks.HasComponent(e))
                        {
                            itemLinks = characterItemLinks[e];
                        }
                        var count = (byte)(bytes.Length / bytesPerData);
                        // UnityEngine.Debug.LogError("Loaded EquipmentItems: " + count);
                        // var userEquipmentEntities = new NativeArray<Entity>(count, Allocator.Temp);
                        var userEquipmentParents = new NativeArray<byte>(count, Allocator.Temp);
                        var slotIndexes = new NativeArray<byte>(count, Allocator.Temp);
                        var itemEntities = new NativeArray<Entity>(count, Allocator.Temp);
                        var userEquipmentEntities = new NativeArray<Entity>(count, Allocator.Temp);
                        PostUpdateCommands.Instantiate(entityInQueryIndex, userEquipmentPrefab, userEquipmentEntities);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, userEquipmentEntities, new CreatorLink(e));
                        var byteIndex = 0;
                        for (int i = 0; i < count; i++)
                        {
                            var itemEntity = new Entity();
                            var dataID = ByteUtil.GetInt(in bytes, byteIndex);
                            if (dataID != 0)
                            {
                                Entity outputEntity;
                                //! Uses Hashmap for inventory for fast access.
                                if (realmItemLinks2.itemsHash.TryGetValue(dataID, out outputEntity))
                                {
                                    itemEntity = outputEntity;
                                }
                                else if (itemLinks.itemsHash.IsCreated && itemLinks.itemsHash.TryGetValue(dataID, out outputEntity))
                                {
                                    itemEntity = outputEntity;
                                }
                            }
                            /*if (itemEntity.Index == 0)
                            {
                                UnityEngine.Debug.LogError("EquipmentLoadSystem: Could not find dataID: " + dataID);
                            }*/
                            var userEquipmentEntity = userEquipmentEntities[i]; // PostUpdateCommands.Instantiate(entityInQueryIndex, userEquipmentPrefab);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, userEquipmentEntity, new UserDataIndex(i));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, userEquipmentEntity, new MetaData(itemEntity));
                            // userEquipmentEntities[i] = userEquipmentEntity;
                            userEquipmentParents[i] = bytes[byteIndex + 4];
                            slotIndexes[i] = bytes[byteIndex + 5];
                            itemEntities[i] = itemEntity;
                            byteIndex += bytesPerData;
                        }
                        for (int i = 0; i < count; i++)
                        {
                            var parentIndex = userEquipmentParents[i];
                            if (parentIndex == 255 || parentIndex >= userEquipmentEntities.Length)
                            {
                                continue;
                            }
                            var parentEntity = userEquipmentEntities[parentIndex];
                            var itemEntity = itemEntities[parentIndex];
                            byte slotParentIndex = 0;
                            if (slotLinks.HasComponent(itemEntity))
                            {
                                var slotEntity = slotLinks[itemEntity].slot;
                                slotParentIndex = slotIndexes[i];
                            }
                            /*else if (itemEntity.Index > 0)
                            {
                                UnityEngine.Debug.LogError("Item [" + EntityManager.GetComponentData<ZoxName>(itemEntity).name.ToString() + "] does not have slot link.");
                            }*/
                            PostUpdateCommands.SetComponent(entityInQueryIndex, userEquipmentEntities[i], new EquipmentParent(parentEntity, slotParentIndex));
                        }
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUserEquipmentSpawned(count));
                        userEquipmentEntities.Dispose();
                        userEquipmentParents.Dispose();
                        slotIndexes.Dispose();
                        itemEntities.Dispose();
                    }
                    // Dispose
                    loadEquipment.Dispose();
                    PostUpdateCommands.RemoveComponent<LoadEquipment>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<SkeletonDirty>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(realmItemLinks)
                .WithReadOnly(characterItemLinks)
                .WithReadOnly(slotLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}