using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Items
{
    //! Removes FailedLoadInventory Event.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(ItemIOSystemGroup))]
    public partial class InventoryLoadFailedEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<FailedLoadInventory>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<FailedLoadInventory>(processQuery);
        }
    }
}