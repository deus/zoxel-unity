using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Items
{
    //! Removes SaveInventory component from entity when disabled.
    /**
    *   - Save System -
    */
    [BurstCompile, UpdateInGroup(typeof(ItemIOSystemGroup))]
    public partial class InventorySaveDisabledSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<DisableSaving>(),
                ComponentType.ReadOnly<SaveInventory>(),
                ComponentType.ReadOnly<Inventory>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<SaveInventory>(processQuery);
        }
    }
}