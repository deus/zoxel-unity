using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Items
{
    //! Loads inventory for character.
    /**
    *   - Load Filename System -
    */
    [BurstCompile, UpdateInGroup(typeof(ItemIOSystemGroup))]
    public partial class CharacterInventoryLoadSystem : SystemBase
    {
        const string filename = "Inventory.zox";

        protected override void OnUpdate()
        {
            Entities
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithNone<InitializeEntity, LoadUniqueItems, OnUniqueItemsSpawned>()
                .WithNone<LoadStats, GenerateBody>()
                .WithAll<Character>()
                .ForEach((ref LoadInventory loadInventory, in EntitySaver entitySaver) =>
            {
                if (loadInventory.loadPath.Length == 0)
                {
                    loadInventory.loadPath = new Text(entitySaver.GetPath() + filename);
                    // UnityEngine.Debug.LogError("CharacterInventoryLoadSystem Load Path: " + loadInventory.loadPath.ToString());
                }
                loadInventory.reader.UpdateOnMainThread(in loadInventory.loadPath);
            }).WithoutBurst().Run();
        }
    }
}