﻿using Unity.Entities;

namespace Zoxel.Items
{
    //! Item systems involve inventory, equipment, trading, etc.
    [UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class ItemIOSystemGroup : ComponentSystemGroup { }
}
