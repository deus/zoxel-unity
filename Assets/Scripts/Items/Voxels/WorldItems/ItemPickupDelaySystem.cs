using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Items
{
    //! Delay event entities with a timed component. Removed after time runs out.
    [BurstCompile, UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class ItemPickupDelaySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in DelayItemPickup delayItemPickup) =>
            {
                if (delayItemPickup.HasFinished(elapsedTime)) //  - delayItemPickup.timeStarted >= delayItemPickup.timeDelay)
                {
                    PostUpdateCommands.RemoveComponent<DelayItemPickup>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}