using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Rendering;
using Zoxel.Voxels;
using Zoxel.Textures.Tilemaps;

namespace Zoxel.Items
{
    //! Initializes WorldVoxelItem's.
    /**
    *   - Initialize System -
    */
    [BurstCompile, UpdateInGroup(typeof(WorldItemSystemGroup))]
    public partial class WorldVoxelItemInitSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;
        private EntityQuery itemsQuery;
        private EntityQuery voxelsQuery;

        protected override void OnCreate()
        {
			voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Vox>());
			itemsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<Item>());
			voxelsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Voxel>());
            RequireForUpdate(processQuery);
            RequireForUpdate(voxesQuery);
            RequireForUpdate(itemsQuery);
            RequireForUpdate(voxelsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate() 
        {
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var voxelLinks = GetComponentLookup<VoxelLinks>(true);
			var voxItemMaterialTilemaps = GetComponentLookup<VoxItemMaterialTilemap>(true);
            voxEntities.Dispose();
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var voxelItems = GetComponentLookup<VoxelItem>(true);
            itemEntities.Dispose();
            var voxelEntities = voxelsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var voxelUVMaps = GetComponentLookup<VoxelUVMap>(true);
            voxelEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<InitializeEntity, WorldVoxelItem>()
                .ForEach((Entity e, ref VoxelTileIndex voxelTileIndex, in WorldItem worldItem, in VoxLink voxLink) =>
            {
                if (!voxelLinks.HasComponent(voxLink.vox) || !voxelItems.HasComponent(worldItem.item))
                {
                    return;
                }
                var voxelLinks2 = voxelLinks[voxLink.vox];
                var voxelType = voxelItems[worldItem.item].voxelType - 1;
                if (voxelType < 0 || voxelType >= voxelLinks2.voxels.Length)
                {
                    //UnityEngine.Debug.LogError("Voxel Type outside of voxels array: " + voxelType + "::" + voxelLinks.voxels.Length);
                    return;
                }
                var gridSize = (float) ((int) voxItemMaterialTilemaps[voxLink.vox].gridSize);
                var voxelEntity = voxelLinks2.voxels[voxelType];
                if (!voxelUVMaps.HasComponent(voxelEntity))
                {
                    //UnityEngine.Debug.LogError("Voxel Entity: " + voxelType + " does not exist.");
                    return;
                }
                // if doesn't, its a minivoxVoxel
                var voxelUVs = voxelUVMaps[voxelEntity];
                if (voxelUVs.uvs.Length > 0)
                {
                    var uvLength = 1 / gridSize;
                    var uver = voxelUVs.uvs[0] + uvLength / 2f;
                    voxelTileIndex.index = (int) ((uver.x / uvLength) + ((int) (uver.y / uvLength)) * ((int) gridSize)) - 1;
                }
            })  .WithReadOnly(voxelLinks)
                .WithReadOnly(voxItemMaterialTilemaps)
                .WithReadOnly(voxelItems)
                .WithReadOnly(voxelUVMaps)
                .ScheduleParallel();
        }
    }
}