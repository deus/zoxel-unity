﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Animations;
using Zoxel.Voxels;
using Zoxel.Voxels.Lighting;
using Zoxel.Voxels.Authoring;
using Zoxel.Items.Authoring;

namespace Zoxel.Items.Voxels
{
    //! Spawns items (voxel or vox) in the world.
    /**
    *   - Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(WorldItemSystemGroup))]
    public partial class WorldItemSpawnSystem : SystemBase
    {
        const float baseVoxItemScale = 0.48f;  // 0.32f
        const float additionalItemScale = 0.04f;  // 0.32f
        const float baseVoxelItemScale = 0.26f;  // 0.32f
        const float spawnPositionVariance = 0.08f; // 0.25f
        const float spawnPositionHeightAddition = 0.1f; // 0.5f
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery itemsQuery;
        private EntityQuery voxelsQuery;
        private EntityQuery planetsQuery;
        private EntityQuery chunksQuery;
        public static Entity spawnItemPrefab;
        private Entity voxelItemPrefab;
        private Entity voxItemPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnItemArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnWorldItem));
            spawnItemPrefab = EntityManager.CreateEntity(spawnItemArchetype);
			itemsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Item>());
			planetsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxScale>());
			chunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<PlanetChunk>(),
                ComponentType.ReadOnly<Chunk>());
			voxelsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Voxel>());
            RequireForUpdate<ModelSettings>();
            RequireForUpdate(processQuery);
            RequireForUpdate(itemsQuery);
        }

        private void InitializePrefabs()
        {
            if (this.voxItemPrefab.Index != 0)
            {
                return;
            }
            var modelSettings = GetSingleton<ModelSettings>();
            var voxScale = modelSettings.GetVoxelScale() * 1.3f;
            // Vox Item
            this.voxItemPrefab = EntityManager.Instantiate(VoxelSystemGroup.worldVoxPrefab);
            EntityManager.AddComponent<Prefab>(this.voxItemPrefab);
            EntityManager.SetComponentData(this.voxItemPrefab, new VoxScale(voxScale));
            EntityManager.AddComponentData(this.voxItemPrefab, new EternalScaling(baseVoxItemScale, additionalItemScale ));
            EntityManager.AddComponentData(this.voxItemPrefab, new EternalRotation(1f, new float3(0, 45, 0)));
            EntityManager.AddComponent<ItemBob>(this.voxItemPrefab);
            EntityManager.AddComponent<DestroyEntityInTime>(this.voxItemPrefab);
            EntityManager.AddComponent<WorldItem>(this.voxItemPrefab);
            EntityManager.AddComponent<WorldVoxItem>(this.voxItemPrefab);
            EntityManager.AddComponent<VoxLink>(this.voxItemPrefab);
            EntityManager.AddComponent<EntityLighting>(this.voxItemPrefab);
            EntityManager.AddComponent<ChunkLightingLink>(this.voxItemPrefab);
            // Voxel Item
            this.voxelItemPrefab = EntityManager.Instantiate(VoxelSystemGroup.worldVoxelPrefab);
            EntityManager.AddComponent<Prefab>(this.voxelItemPrefab);
            EntityManager.AddComponentData(this.voxelItemPrefab, new EternalScaling(baseVoxelItemScale, additionalItemScale ));
            EntityManager.AddComponentData(this.voxelItemPrefab, new EternalRotation(1f, new float3(0, 45, 0)));
            EntityManager.AddComponent<ItemBob>(this.voxelItemPrefab);
            EntityManager.AddComponent<DestroyEntityInTime>(this.voxelItemPrefab);
            EntityManager.AddComponent<WorldItem>(this.voxelItemPrefab);
            EntityManager.AddComponent<WorldVoxelItem>(this.voxelItemPrefab);
            EntityManager.AddComponent<EntityLighting>(this.voxelItemPrefab);
            EntityManager.AddComponent<ChunkLightingLink>(this.voxelItemPrefab);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var itemsSettings = GetSingleton<ItemsSettings>();
            if (itemsSettings.disableWorldItems)
            {
                PostUpdateCommands2.DestroyEntity(processQuery);
                return;
            }
            InitializePrefabs();
            var itemLifeTime = itemsSettings.itemLifeTime;
            var voxelItemPrefab = this.voxelItemPrefab;
            var voxItemPrefab = this.voxItemPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var voxelItems = GetComponentLookup<VoxelItem>(true);
			var itemVoxDatas = GetComponentLookup<Chunk>(true);
			var itemVoxColors = GetComponentLookup<VoxColors>(true);
            itemEntities.Dispose();
            var voxelEntities = voxelsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var voxelGenerateVoxDatas = GetComponentLookup<GenerateVoxData>(true);
			var voxelChunkDimensions = GetComponentLookup<ChunkDimensions>(true);
			var voxelVoxDatas = GetComponentLookup<Chunk>(true);
            voxelEntities.Dispose();
            var voxEntities = planetsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var chunkLinks = GetComponentLookup<ChunkLinks>(true);
			var chunkDimensions = GetComponentLookup<ChunkDimensions>(true);
			var voxScales = GetComponentLookup<VoxScale>(true);
            voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
			var chunkLights = GetComponentLookup<ChunkLights>(true);
            chunkEntities.Dispose();
            //! \todo Generate Unique ID in jobs for world items
            Entities
                .WithNone<DelayEvent>()
                .ForEach((ref SpawnWorldItem spawnWorldItem) =>
            {
                if (spawnWorldItem.id == 0)
                {
                    spawnWorldItem.id = IDUtil.GenerateUniqueID();
                }
            }).WithoutBurst().Run();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .ForEach((Entity e, int entityInQueryIndex, in SpawnWorldItem spawnWorldItem) =>
            {
                var voxEntity = spawnWorldItem.vox;
                var voxScale = voxScales[voxEntity].scale;
                var realPosition = spawnWorldItem.spawnPosition;
                var globalPosition = VoxelUtilities.GetVoxelPosition(realPosition, voxScale);
                var chunkPosition = int3.zero;
                var voxelDimensions = int3.zero;
                var chunkEntity = new Entity();
                if (chunkLinks.HasComponent(voxEntity))
                {
                    var chunkLinks2 = chunkLinks[voxEntity];
                    voxelDimensions = chunkDimensions[voxEntity].voxelDimensions;
                    chunkPosition = VoxelUtilities.GetChunkPosition(globalPosition, voxelDimensions);
                    Entity chunkEntity2;
                    if (chunkLinks2.chunks.TryGetValue(chunkPosition, out chunkEntity2))
                    {
                        if (HasComponent<EntityBusy>(chunkEntity2))
                        {
                            return;
                        }
                        chunkEntity = chunkEntity2;
                    }
                }
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);    // Destroy Spawn Event
                var isVoxelItem = HasComponent<VoxelItem>(spawnWorldItem.item);
                var random = new Random();
                random.InitState((uint) spawnWorldItem.id);
                // todo: use two different prefabs depending on item type - so i dont need to add components
                var isVoxelMinivox = false;
                var voxelItem = new VoxelItem();
                var itemPrefab = voxItemPrefab;
                if (isVoxelItem)
                {
                    voxelItem = voxelItems[spawnWorldItem.item];
                    isVoxelMinivox = HasComponent<MinivoxVoxel>(voxelItem.voxel);
                    if (!isVoxelMinivox)
                    {
                        itemPrefab = voxelItemPrefab;
                    }
                }
                var itemEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, itemPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new Seed(spawnWorldItem.id));
                PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new ZoxID(spawnWorldItem.id));
                byte lightValue = 0;
                if (chunkEntity.Index > 0)
                {
                    var localPosition = VoxelUtilities.GetLocalPosition(globalPosition, chunkPosition, voxelDimensions);
                    var voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
                    var chunkLights2 = chunkLights[chunkEntity];
                    lightValue = chunkLights2.lights[voxelArrayIndex];
                    PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new ChunkLightingLink(chunkEntity, chunkPosition));
                }
                var entityLighting = new EntityLighting(lightValue);
                PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, entityLighting);

                if (isVoxelItem)
                {
                    // check if minivox?
                    if (!isVoxelMinivox)
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new VoxelTileIndex((byte)(voxelItem.voxelType)));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new VoxelLink(voxelItem.voxel));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new MaterialBaseColor { Value = entityLighting.GetBrightnessFloat4() });
                        PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new EternalScaling(baseVoxelItemScale * voxScale.x, additionalItemScale * voxScale.x ));
                    }
                    else
                    {
                        var voxelEntity = voxelItem.voxel;
                        // Set generated vox data
                        // UnityEngine.Debug.LogError("Todo: Voxel Minivox world items.");
                        if (HasComponent<GenerateVoxData>(voxelEntity))
                        {
                            // Torch for example
                            PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, voxelChunkDimensions[voxelEntity]);
                            PostUpdateCommands.AddComponent(entityInQueryIndex, itemEntity, voxelGenerateVoxDatas[voxelEntity]);
                            PostUpdateCommands.AddComponent<GenerateVox>(entityInQueryIndex, itemEntity);
                            PostUpdateCommands.AddComponent<GenerateVoxColors>(entityInQueryIndex, itemEntity);
                            PostUpdateCommands.RemoveComponent<Chunk>(entityInQueryIndex, itemEntity);
                        }
                        else if (HasComponent<Chunk>(voxelEntity))
                        {
                            PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, voxelVoxDatas[voxelEntity]);
                            // PostUpdateCommands.AddComponent<DontDestroyVoxData>(entityInQueryIndex, itemEntity);
                        }
                    }
                    // else check GenerateVox data
                }
                else
                {
                    if (itemVoxDatas.HasComponent(spawnWorldItem.item))
                    {
                        var voxData = itemVoxDatas[spawnWorldItem.item];
                        PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new Chunk(voxData.voxelDimensions, voxData.voxels.Clone()));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new ChunkDimensions(voxData.voxelDimensions));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new VoxColors(itemVoxColors[spawnWorldItem.item].colors.Clone()));
                    }
                    // else check GenerateVox data
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new Translation { Value = spawnWorldItem.spawnPosition });
                PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new Rotation { Value = spawnWorldItem.spawnRotation });
                PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new VoxLink(spawnWorldItem.vox));
                PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new ItemBob
                {
                    originalPosition = spawnWorldItem.spawnPosition + new float3(
                        random.NextFloat(-spawnPositionVariance, spawnPositionVariance),
                        random.NextFloat(-spawnPositionVariance, spawnPositionVariance),
                        random.NextFloat(-spawnPositionVariance, spawnPositionVariance))
                        + math.mul(spawnWorldItem.spawnRotation, new float3(0, spawnPositionHeightAddition, 0))
                });
                PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new WorldItem(spawnWorldItem.item, spawnWorldItem.quantity, elapsedTime));
                // Death
                PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new DestroyEntityInTime(elapsedTime, itemLifeTime));
                if (spawnWorldItem.character.Index > 0)
                {
                    // add delay to pickup
                    PostUpdateCommands.AddComponent(entityInQueryIndex, itemEntity, new DelayItemPickup(elapsedTime, 10));
                }
			})  .WithReadOnly(voxelItems)
                .WithReadOnly(itemVoxDatas)
                .WithReadOnly(itemVoxColors)
                .WithReadOnly(voxelVoxDatas)
                .WithReadOnly(voxelGenerateVoxDatas)
                .WithReadOnly(voxelChunkDimensions)
                .WithReadOnly(chunkLinks)
                .WithReadOnly(chunkDimensions)
                .WithReadOnly(voxScales)
                .WithReadOnly(chunkLights)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}