using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;

namespace Zoxel.Items.Voxels
{
    [BurstCompile, UpdateInGroup(typeof(WorldItemSystemGroup))]
	public partial class DropItemsSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userItemsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			userItemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserItem>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var spawnItemPrefab = WorldItemSpawnSystem.spawnItemPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var userItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var userItems = GetComponentLookup<UserItemQuantity>(true);
			var userMetaDatas = GetComponentLookup<MetaData>(true);
            userItemEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<DropItems>()
                .ForEach((Entity e, int entityInQueryIndex, ref Inventory inventory, in VoxLink voxLink, in Translation translation, in Rotation rotation) =>
            {
                PostUpdateCommands.RemoveComponent<DropItems>(entityInQueryIndex, e);
                for (int i = 0; i < inventory.items.Length; i++)
                {
                    var userItemEntity = inventory.items[i];
                    var userMetaEntity = userMetaDatas[userItemEntity].data;
                    if (userMetaEntity.Index != 0)
                    {
                        var userItem = userItems[userItemEntity];
                        PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, spawnItemPrefab), 
                            new SpawnWorldItem
                            {
                                item = userMetaEntity,
                                quantity = userItem.quantity,
                                spawnPosition = translation.Value,
                                spawnRotation = rotation.Value,
                                vox = voxLink.vox
                            });
                        PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, new MetaData());
                        PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, new UserItemQuantity());
                    }
                }
			})  .WithReadOnly(userItems)
                .WithReadOnly(userMetaDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}