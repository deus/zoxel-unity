using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Textures;
using Zoxel.Voxels;
using Zoxel.Skeletons;
using System.Collections.Generic;

namespace Zoxel.Items.Voxels
{
    //! Generates Items for the Realm.
    /**
    *   - Data Generation System -
    *   \todo Generate consumeable items from scratch
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class ItemsGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity itemPrefab;
        public static Entity voxelItemPrefab;
        public static Entity slotPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var itemArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(ZoxID),
                typeof(ZoxName),
                typeof(RealmItem),
                typeof(Item),
                typeof(ItemValue),
                typeof(Texture)
            );
            itemPrefab = EntityManager.CreateEntity(itemArchetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(itemPrefab); // EntityManager.AddComponentData(itemPrefab, new EditorName("[item]"));
            #endif
            EntityManager.AddComponent<Chunk>(itemPrefab);  //! Rename chunk to VoxelData after
            EntityManager.AddComponent<ChunkDimensions>(itemPrefab);  //! Rename chunk to VoxelData after
            EntityManager.AddComponent<VoxColors>(itemPrefab);
            EntityManager.AddComponent<BoneType>(itemPrefab);
            EntityManager.AddComponent<DontDestroyVoxData>(itemPrefab);
            voxelItemPrefab = EntityManager.CreateEntity(itemArchetype);
            EntityManager.AddComponent<VoxelItem>(voxelItemPrefab);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(voxelItemPrefab); // EntityManager.AddComponentData(voxelItemPrefab, new EditorName("[voxelitem]"));
            #endif
            var slotArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(ZoxID),
                typeof(ZoxName),
                typeof(Slot)
            );
            slotPrefab = EntityManager.CreateEntity(slotArchetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(slotPrefab); // EntityManager.AddComponentData(slotPrefab, new EditorName("[slot]"));
            #endif
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (ItemsManager.instance == null) return;
            var itemDatams = ItemsManager.instance.ItemsSettings.items;
            var slotDatams = new List<SlotDatam>(ItemsManager.instance.ItemsSettings.slots);
            var itemPrefab = ItemsGenerationSystem.itemPrefab;
            var slotPrefab = ItemsGenerationSystem.slotPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); //.AsParallelWriter();
            Entities
                .WithNone<GenerateRealmVoxels>()
                .ForEach((Entity e, ref ItemLinks itemLinks, ref SlotLinks slotLinks, ref GenerateRealm generateRealm, in ZoxID zoxID, in VoxelLinks voxelLinks) =>
            {
                if (generateRealm.state != (byte) GenerateRealmState.GenerateItems)
                {
                    return; // wait for thingo
                }
                var realmSeed = zoxID.id;
                if (realmSeed == 0)
                {
                    return;
                }
                for (int i = 0; i < voxelLinks.voxels.Length; i++)
                {
                    var voxelEntity = voxelLinks.voxels[i];
                    if (!EntityManager.HasComponent<Voxel>(voxelEntity))
                    {
                        UnityEngine.Debug.LogWarning(i + " has null voxel.");
                        return;
                    }
                }
                generateRealm.state = (byte) GenerateRealmState.GenerateRaces;
                // Destroy UserSkillLinks and SkillTrees
                for (int i = 0; i < itemLinks.items.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(itemLinks.items[i]);
                }
                itemLinks.Dispose();
                for (int i = 0; i < slotLinks.slots.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(slotLinks.slots[i]);
                }
                slotLinks.Dispose();

                // Slots first
                // UnityEngine.Debug.LogError("Initializing Realm Items: " + itemDatams.Count);
                var slotNames = new NativeList<Text>();
                var slotEntities = new NativeList<Entity>();
                var slotIDs = new NativeList<int>();
                for (int i = 0; i < slotDatams.Count; i++)
                {
                    var slotDatam = slotDatams[i];
                    if (slotDatam != null)
                    {
                        var slotID = slotDatam.data.id;
                        var slotEntity = EntityManager.Instantiate(slotPrefab);
                        PostUpdateCommands.SetComponent(slotEntity, new ZoxID(slotID));
                        var slotName = new Text(slotDatam.name);
                        PostUpdateCommands.SetComponent(slotEntity, new ZoxName(slotName));
                        slotEntities.Add(slotEntity);
                        slotNames.Add(slotName);
                        slotIDs.Add(slotID);
                    }
                    else
                    {
                        UnityEngine.Debug.LogError("Problem with Slot at: " + i);
                    }
                }
                // now set up links between slotDatams
                for (int i = 0; i < slotDatams.Count; i++)
                {
                    var slotDatam = slotDatams[i];
                    if (slotDatam != null)
                    {
                        var slotEntity = slotEntities[i];
                        var slot = slotDatam.GenerateData();
                        for (int j = 0; j < slotDatam.femaleSlots.Count; j++)
                        {
                            var femaleSlotDatam = slotDatam.femaleSlots[j];
                            var slotIndex = slotDatams.IndexOf(femaleSlotDatam);
                            slot.femaleSlots[j] = slotEntities[slotIndex];
                        }
                        PostUpdateCommands.SetComponent(slotEntity, slot);
                    }
                }
                // link to slot Links
                slotLinks.Initialize(slotEntities.Length);
                for (int i = 0; i < slotEntities.Length; i++)
                {
                    slotLinks.slots[i] = slotEntities[i];
                    var slotID = slotIDs[i];
                    slotLinks.slotsHash[slotID] = slotEntities[i];
                }
                // since some of this data comes from authored datams, we should not dispose of that data
                // later on, generate some of the itemDatams too
                // UnityEngine.Debug.LogError("Initializing Realm Items: " + itemDatams.Count);
                var itemIDs = new NativeList<int>();
                var newItems = new NativeList<Entity>();
                for (int i = 0; i < itemDatams.Length; i++)
                {
                    //var itemID = realmSeed + i * 500;
                    var itemDatam = itemDatams[i];
                    if (itemDatam != null)
                    {
                        var itemName = itemDatam.name.Replace(" Item", "").Trim();
                        // UnityEngine.Debug.LogError(i + " New Realm Item: " + itemName);
                        var itemID = itemDatam.zoxID.id;
                        if (itemID != 0)
                        {
                            var itemEntity = EntityManager.Instantiate(itemPrefab);
                            PostUpdateCommands.SetComponent(itemEntity, new ZoxID(itemID));
                            PostUpdateCommands.SetComponent(itemEntity, new ItemValue(itemDatam.itemValue));
                            PostUpdateCommands.SetComponent(itemEntity, new ZoxName(itemName));
                            #if UNITY_EDITOR
                            PostUpdateCommands.SetComponent(itemEntity, new EditorName("[item] " + itemName));
                            #endif
                            if ((byte) itemDatam.type == ItemType.Currency) // itemName.Contains("Coin"))
                            {
                                PostUpdateCommands.AddComponent<ItemCurrency>(itemEntity);
                            }
                            else if ((byte) itemDatam.type == ItemType.Consumable)
                            {
                                PostUpdateCommands.AddComponent<ConsumableItem>(itemEntity);
                            }
                            else if ((byte) itemDatam.type == ItemType.Equipable)
                            {
                                PostUpdateCommands.AddComponent<ItemEquipable>(itemEntity);
                                var slotEntity = new Entity();
                                if (itemDatam.slot)
                                {
                                    var slotName = itemDatam.slot.name;
                                    for (int j = 0; j < slotLinks.slots.Length; j++)
                                    {
                                        if (slotNames[j].ToString() == slotName) // "Hold")
                                        {
                                            slotEntity = slotEntities[j];
                                            break;
                                        }
                                    }
                                }
                                PostUpdateCommands.AddComponent(itemEntity, new SlotLink(slotEntity));
                                PostUpdateCommands.AddComponent(itemEntity, new ItemSlotData(itemDatam.model.offset));
                            }
                            // generate VoxItem for those itemDatams as well
                            itemDatam.model.data.OptimizeColors();
                            var modelData = itemDatam.model.data;
                            PostUpdateCommands.SetComponent(itemEntity, new BoneType(InitializeBoneType(itemName)));
                            PostUpdateCommands.SetComponent(itemEntity, new Chunk(modelData.size, modelData.data.Clone()));
                            PostUpdateCommands.SetComponent(itemEntity, new ChunkDimensions(modelData.size));
                            PostUpdateCommands.SetComponent(itemEntity, new VoxColors(in modelData.colors));
                            newItems.Add(itemEntity);
                            itemIDs.Add(itemID);
                        }
                    }
                    else
                    {
                        UnityEngine.Debug.LogError("Problem with item at: " + i);
                    }
                }
                for (int i = 0; i < voxelLinks.voxels.Length; i++)
                {
                    var voxelEntity = voxelLinks.voxels[i];
                    var itemID = EntityManager.GetComponentData<ZoxID>(voxelEntity);
                    var itemName = EntityManager.GetComponentData<ZoxName>(voxelEntity).name.Clone();
                    var itemEntity = EntityManager.Instantiate(voxelItemPrefab);
                    PostUpdateCommands.SetComponent(itemEntity, itemID);
                    PostUpdateCommands.SetComponent(itemEntity, new ZoxName(itemName));
                    #if UNITY_EDITOR
                    PostUpdateCommands.SetComponent(itemEntity, new EditorName("[voxelitem] " + itemName.ToString()));
                    #endif
                    PostUpdateCommands.SetComponent(itemEntity, new VoxelItem(voxelEntity, (byte)(i + 1)));
                    // for grass, spawn dirt item
                    var voxel = EntityManager.GetComponentData<Voxel>(voxelEntity);
                    if (voxel.type == (byte) VoxelType.Grass)
                    {
                        PostUpdateCommands.SetComponent(voxelEntity, new ItemLink(newItems[newItems.Length - 1]));
                    }
                    else
                    {
                        PostUpdateCommands.SetComponent(voxelEntity, new ItemLink(itemEntity));
                    }
                    // it's bugged out atm
                    // PostUpdateCommands.SetComponent(voxelEntity, new ItemLink(itemEntity));
                    newItems.Add(itemEntity);
                    itemIDs.Add(itemID.id);
                }
                itemLinks.Initialize(newItems.Length);
                for (int i = 0; i < newItems.Length; i++)
                {
                    itemLinks.items[i] = newItems[i];
                    var itemID = itemIDs[i];
                    itemLinks.itemsHash[itemID] = newItems[i];
                }
                slotNames.Dispose();
                slotEntities.Dispose();
                slotIDs.Dispose();
                itemIDs.Dispose();
                newItems.Dispose();
            }).WithStructuralChanges().Run();   // spawns skill tree and skill data
            
        }

        public byte InitializeBoneType(string attachName)
        {
            // var attachName = name.ToString();
            if (attachName.Contains("Shoulder") && !attachName.Contains("gaurd"))
            {
                return HumanoidBoneType.Shoulder;
            }
            else if (attachName.Contains("Pickaxe"))
            {
                return HumanoidBoneType.Held;  // pickaxe type
            }
            else if (attachName.Contains("Head"))
            {
                return HumanoidBoneType.Head;  // pickaxe type
            }
            else if (attachName.Contains("Chest"))
            {
                return HumanoidBoneType.Chest;  // pickaxe type
            }
            return 0;
        }
    }
}