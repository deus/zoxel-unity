using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;
using Zoxel.Items.Authoring;

namespace Zoxel.Items
{
    //! Generates an Entity's Inventory data.
    /**
    *   Spawns based on some types ive manually set atm.
    *   \todo Base items on types, consumable, currency, hat.
    *   \todo Items given out based on npc level, and context.
    */
    [BurstCompile, UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class InventoryGenerateSystem : SystemBase
    {
        private const byte slimeItems = 1;
        private const byte mintmanItems = 4;
        // private const byte playerItems = 32;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;
        private EntityQuery itemQuery;
        private NativeArray<Text> texts;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.ReadOnly<GenerateInventory>(),
                ComponentType.ReadOnly<RealmLink>());
            realmQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<ItemLinks>());
            itemQuery = GetEntityQuery(
                ComponentType.ReadOnly<RealmItem>(),
                ComponentType.ReadOnly<ZoxName>());
            texts = new NativeArray<Text>(5, Allocator.Persistent);
            texts[0] = new Text("Mushroom");
            texts[1] = new Text("Cookie");
            texts[2] = new Text("Coin");
            texts[3] = new Text("Pickaxe");
            texts[4] = new Text("TopHat");
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity, DeadEntity>()
                .WithAll<GenerateInventory, EntitySaver>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.AddComponent<SaveInventory>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
			if (processQuery.IsEmpty)
			{
				return;
			}
            var itemsSettings = GetSingleton<ItemsSettings>();
            var playerItemsCount = itemsSettings.playerItemsCount;
            var userItemPrefab = ItemSystemGroup.userItemPrefab;
            var texts = this.texts;
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var itemLinks = GetComponentLookup<ItemLinks>(true);
            var itemEntities = itemQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var itemNames = GetComponentLookup<ZoxName>(true);
            realmEntities.Dispose();
            itemEntities.Dispose();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity, DeadEntity>()
                .ForEach((Entity e, int entityInQueryIndex, in GenerateInventory generateInventory, in RealmLink realmLink) =>
            {
                PostUpdateCommands.RemoveComponent<GenerateInventory>(entityInQueryIndex, e);
                var inventorySlots = slimeItems;
                if (generateInventory.spawnType == NPCType.jona || generateInventory.spawnType == NPCType.minty || generateInventory.spawnType == NPCType.dave)
                {
                    inventorySlots = mintmanItems;
                }
                else if (generateInventory.spawnType == NPCType.player)
                {
                    inventorySlots = playerItemsCount;
                }
                // add mushroom to inventory
                var realmItems = itemLinks[realmLink.realm].items;
                var addItems = new NativeList<Entity>();
                var addItemQuantities = new NativeList<byte>();
                for (int j = 0; j < realmItems.Length; j++)
                {
                    var itemEntity = realmItems[j];
                    var itemName = itemNames[itemEntity].name; // .ToString();
                    // UnityEngine.Debug.LogError(j + " Realm Item: " + itemName);
                    if (
                        // Tiny Melees
                        generateInventory.spawnType == NPCType.slem && itemName == texts[0] // "Mushroom"
                        // Range
                        || generateInventory.spawnType == NPCType.glem && itemName == texts[1] // "Cookie"
                        // Big Enemy
                        || generateInventory.spawnType == NPCType.chen && itemName == texts[2] // "Coin"
                        // Regular NPC
                        || generateInventory.spawnType == NPCType.dave && itemName == texts[1] // "Cookie"
                        // Regular NPC
                        || generateInventory.spawnType == NPCType.jona && itemName == texts[2] // "Cookie"
                        // shop
                        || generateInventory.spawnType == NPCType.minty && itemName == texts[3] // "Pickaxe"
                        || generateInventory.spawnType == NPCType.minty && itemName == texts[4] // "TopHat")
                        || generateInventory.spawnType == NPCType.minty && itemName == texts[1] // "Cookie"
                        || generateInventory.spawnType == NPCType.minty && itemName == texts[0] // "Cookie"

                        // Mintman
                        //|| (generateInventory.spawnType == 4 &&
                        //    (itemDatam.name.Contains("Torch") || itemDatam.name.Contains("Treasure"))))
                        //|| generateInventory.spawnType == 5 && itemDatam.name.Contains("Mushroom"))
                        )
                    {
                        byte quantity = 1;
                        if (generateInventory.spawnType == NPCType.dave)
                        {
                            quantity = 2;
                        }
                        else if (generateInventory.spawnType == NPCType.minty)
                        {
                            quantity = 3;
                        }
                        else if (generateInventory.spawnType == NPCType.jona)
                        {
                            quantity = 3;
                        }
                        addItems.Add(itemEntity);
                        addItemQuantities.Add(quantity);
                        // UnityEngine.Debug.LogError("Generated Item: " + j + " is " + itemEntity.Index);
                    }
                }
                for (byte j = 0; j < inventorySlots; j++)
                {
                    var userItemEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, userItemPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, new InventoryLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, new UserDataIndex(j));
                    if (j >= addItems.Length)
                    {
                        continue;
                    }
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, new MetaData(addItems[j]));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, new UserItemQuantity(addItemQuantities[j]));
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUserItemsSpawned(inventorySlots));
                addItems.Dispose();
                addItemQuantities.Dispose();
            })  .WithReadOnly(texts)
                .WithReadOnly(itemLinks)
                .WithReadOnly(itemNames)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

// Add 3 chests
/*if (generateInventory.spawnType == 4)
{
    var voxelLinks = EntityManager.GetComponentData<VoxelLinks>(voxLink.vox);
    var nonUniqueVoxelsCount = (9 * 12) + 1;    // + 1 for air
    //UnityEngine.Debug.LogError("nonUniqueVoxelsCount: " + nonUniqueVoxelsCount + " out of " + voxelLinks.voxels.Length);
    // Torch
    var torchIndex = nonUniqueVoxelsCount + 0;
    var torchVoxelEntity = voxelLinks.voxels[torchIndex];
    inventory.AddItem(new Item
    {
        id = torchIndex,    // chest index
        voxelType = (byte) torchIndex, // VoxelType.Torch,
        type = (byte) ItemTypeEditor.Voxel,
        name = EntityManager.GetComponentData<ZoxName>(torchVoxelEntity).name.Clone(),
        value = 2
    }, 10);
    // Chest
    var chestIndex = nonUniqueVoxelsCount + 2;
    var chestVoxelEntity = voxelLinks.voxels[chestIndex];
    AddItem(ref inventory, new Item
    {
        id = chestIndex,    // chest index
        voxelType = (byte) chestIndex, // VoxelType.Chest,
        type = (byte) ItemTypeEditor.Voxel,
        name = EntityManager.GetComponentData<ZoxName>(chestVoxelEntity).name.Clone(),
        value = 8
    }, 1);
    // Hat
}*/