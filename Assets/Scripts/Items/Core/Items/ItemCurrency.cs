using Unity.Entities;

namespace Zoxel.Items
{
    //! Used for gold coin items.
    /**
    *   Perhaps contain the value per type of coin on this - so i can trade 10 silvers into a gold
    */
    public struct ItemCurrency : IComponentData { }
}