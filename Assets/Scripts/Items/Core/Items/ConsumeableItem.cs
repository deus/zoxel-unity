using Unity.Entities;

namespace Zoxel.Items
{
    //! Increases a stat instantly by an ammount or add a buff/debuff on consuming.
    public struct ConsumableItem : IComponentData
    {
        public Entity stat;
        public float value;

        public ConsumableItem(Entity stat, float value)
        {
            this.stat = stat;
            this.value = value;
        }
    }
}