using Unity.Entities;

namespace Zoxel.Items
{
    //! A tag for equipment kind of item.
    public struct ItemEquipable : IComponentData { }
}