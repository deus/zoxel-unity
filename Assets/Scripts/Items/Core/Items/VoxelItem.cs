using Unity.Entities;

namespace Zoxel.Items
{
    public struct VoxelItem : IComponentData
    {
        public Entity voxel;
        public byte voxelType;  // can get name by finding game voxel from id

        public VoxelItem(Entity voxel, byte voxelType)
        {
            this.voxel = voxel;
            this.voxelType = voxelType;
        }
    }
}