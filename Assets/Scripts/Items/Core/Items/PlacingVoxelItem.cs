using Unity.Entities;

namespace Zoxel.Items
{
    public struct BeginPlacingVoxelItem : IComponentData { }
    
    public struct EndPlacingVoxelItem : IComponentData { }

    public struct PlacingVoxelItem : IComponentData
    {
        public Entity itemPlacing;

        public PlacingVoxelItem(Entity itemPlacing)
        {
            this.itemPlacing = itemPlacing;
        }
    }
}