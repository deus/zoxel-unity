using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Items
{
    //! Handles when a character dies using the DyingEntity event.
    [BurstCompile, UpdateInGroup(typeof(ItemSystemGroup))]
	public partial class ItemHitTakerDyingSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DyingEntity, ItemHitTaker>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            { 
                PostUpdateCommands.RemoveComponent<ItemHitTaker>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}