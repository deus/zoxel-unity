using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Items
{
    // If character dies mid way, remove these events
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ItemPickupDestroySystem : SystemBase
    {        
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DelayEvent>()
                .ForEach((Entity e, int entityInQueryIndex, in PickupItem pickupItem) =>
            {
                if (!HasComponent<Inventory>(pickupItem.character) || HasComponent<DestroyEntity>(pickupItem.character)
                    || HasComponent<DeadEntity>(pickupItem.character))
                {
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}