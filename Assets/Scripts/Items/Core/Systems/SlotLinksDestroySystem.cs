using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Items
{
    //! Disposes of SlotLinks.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class SlotLinksDestroySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // Unique Items
			Dependency = Entities
                .WithAll<DestroyEntity, SlotLinks>()
                .ForEach((int entityInQueryIndex, in SlotLinks slotLinks) =>
			{
                for (int i = 0; i < slotLinks.slots.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, slotLinks.slots[i]);
                }
                slotLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}