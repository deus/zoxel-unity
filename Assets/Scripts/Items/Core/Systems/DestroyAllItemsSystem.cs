using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Jobs;

namespace Zoxel.Items
{
    [BurstCompile, UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class DestroyAllItemsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery items;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            items = GetEntityQuery(
                ComponentType.ReadOnly<WorldItem>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var entities = items.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<DestroyAllItems>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                for (int i = 0; i < entities.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, entities[i]);
                }
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
            })  .WithReadOnly(entities)
                .WithDisposeOnCompletion(entities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}