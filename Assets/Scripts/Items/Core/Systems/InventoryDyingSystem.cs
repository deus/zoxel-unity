using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Items
{
    //! Handles when a Inventory dies using the DyingEntity event by adding DropItems event.
    [BurstCompile, UpdateInGroup(typeof(ItemSystemGroup))]
	public partial class InventoryDyingSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DyingEntity, Inventory>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            { 
                PostUpdateCommands.AddComponent<DropItems>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}