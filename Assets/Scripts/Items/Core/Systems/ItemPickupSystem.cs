﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Items
{
    //! A Character picks up WorldItem's by injecting PickupItem event entities.
    /**
    *   \todo Make Pickup Item - only if can pickup in the first place - reserves space?? sounds complicated
    *   \todo Kill item only if picked up - otherwise it will just stay - so i dont run out of places
    */
    [BurstCompile, UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class ItemPickupSystem : SystemBase
    {        
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery pickupItemQuery;
        private EntityQuery itemsQuery;
        private EntityQuery userItemsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			pickupItemQuery = GetEntityQuery(
                ComponentType.ReadOnly<PickupItem>(),
                ComponentType.Exclude<DelayEvent>());
			itemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Item>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<DestroyEntity>());
			userItemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserItem>(),
                ComponentType.Exclude<DestroyEntity>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (pickupItemQuery.IsEmpty)
            {
                return;
            }
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var pickupEntities = pickupItemQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var pickupItems = GetComponentLookup<PickupItem>(true);
            var inventoryItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var userItems = GetComponentLookup<UserItemQuantity>(false);
			var userItemMetaDatas = GetComponentLookup<MetaData>(false);
            inventoryItemEntities.Dispose();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<SaveInventory, OnInventoryUpdated>()
                .ForEach((Entity e, int entityInQueryIndex, ref Inventory inventory) =>
            {
                var entityPickedUpItem = false;
                for (int a = 0; a < pickupEntities.Length; a++)
                {
                    var e2 = pickupEntities[a];
                    var pickupItem = pickupItems[e2];
                    if (e == pickupItem.character)
                    {
                        entityPickedUpItem = true;
                        break;
                    }
                }
                if (!entityPickedUpItem)
                {
                    return;
                }
                var inventoryDirty = false;
                var itemsUpdated = new NativeList<Entity>();
                var itemQuantitiesUpdated = new NativeList<Entity>();
                // UnityEngine.Debug.LogError("Pickup Entities: " + pickupEntities.Length);
                for (int a = 0; a < pickupEntities.Length; a++)
                {
                    var e2 = pickupEntities[a];
                    var pickupItem = pickupItems[e2];
                    if (e != pickupItem.character)
                    {
                        continue;
                    }
                    // PostUpdateCommands.DestroyEntity(entityInQueryIndex, e2);
                    var worldItem = pickupItem.item;
                    var addInventoryItemEntity = new Entity();
                    var stackInventoryItemEntity = new Entity();
                    for (int i = 0; i < inventory.items.Length; i++)
                    {
                        var userItemEntity = inventory.items[i];
                        // var userItem = userItems[userItemEntity];
                        var metaData = userItemMetaDatas[userItemEntity];
                        var itemEntity = metaData.data;
                        if (itemEntity == worldItem.item)
                        {
                            stackInventoryItemEntity = userItemEntity;
                            break;
                        }
                    }
                    // Next check for new place
                    if (stackInventoryItemEntity.Index == 0)
                    {
                        for (int i = 0; i < inventory.items.Length; i++)
                        {
                            var userItemEntity = inventory.items[i];
                            var metaData = userItemMetaDatas[userItemEntity];
                            var itemEntity = metaData.data;
                            if (itemEntity.Index == 0)
                            {
                                addInventoryItemEntity = userItemEntity;
                                break;
                            }
                        }
                    }
                    if (stackInventoryItemEntity.Index != 0)
                    {
                        inventoryDirty = true;
                        var userItem = userItems[stackInventoryItemEntity];
                        userItem.quantity += worldItem.quantity;
                        userItems[stackInventoryItemEntity] = userItem;
                        // UnityEngine.Debug.LogError("Stacked Item to: " + item.quantity);
                        if (!itemQuantitiesUpdated.Contains(stackInventoryItemEntity))
                        {
                            itemQuantitiesUpdated.Add(stackInventoryItemEntity);
                        }
                        //Debug.Log("At " + i + " - Stacking item: " + metaID + " to " + item.quantity + " with added quantity of: " + worldItem.quantity);
                    }
                    if (addInventoryItemEntity.Index != 0)
                    {
                        inventoryDirty = true;
                        var metaData = userItemMetaDatas[addInventoryItemEntity];
                        metaData.data = worldItem.item;
                        userItemMetaDatas[addInventoryItemEntity] = metaData;
                        var addInventoryItem = userItems[addInventoryItemEntity];
                        addInventoryItem.quantity = worldItem.quantity;
                        userItems[addInventoryItemEntity] = addInventoryItem;
                        if (!itemsUpdated.Contains(addInventoryItemEntity) && 
                            (HasComponent<ConsumableItem>(worldItem.item) || HasComponent<VoxelItem>(worldItem.item)))
                        {
                            itemsUpdated.Add(addInventoryItemEntity);
                        }
                        //Debug.Log("At " + i + " - Adding item to inventory: " + metaID + " of quantity: " + worldItem.quantity);
                    }
                }
                var onInventoryUpdated = new OnInventoryUpdated();
                onInventoryUpdated.itemsUpdated = new BlitableArray<Entity>(itemsUpdated.Length, Allocator.Persistent);
                for (int i = 0; i < itemsUpdated.Length; i++)
                {
                    onInventoryUpdated.itemsUpdated[i] = itemsUpdated[i];
                }
                onInventoryUpdated.itemQuantitiesUpdated = new BlitableArray<Entity>(itemQuantitiesUpdated.Length, Allocator.Persistent);
                for (int i = 0; i < itemQuantitiesUpdated.Length; i++)
                {
                    onInventoryUpdated.itemQuantitiesUpdated[i] = itemQuantitiesUpdated[i];
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, onInventoryUpdated);
                if (inventoryDirty)
                {
                    PostUpdateCommands.AddComponent<SaveInventory>(entityInQueryIndex, e);
                }
                itemsUpdated.Dispose();
                itemQuantitiesUpdated.Dispose();
            })  .WithNativeDisableContainerSafetyRestriction(userItems)
                .WithNativeDisableContainerSafetyRestriction(userItemMetaDatas)
                .WithReadOnly(pickupEntities)
                .WithDisposeOnCompletion(pickupEntities)
                .WithReadOnly(pickupItems)
                .WithReadOnly(userItemMetaDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
            //var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            //Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			//var itemZoxIDs = GetComponentLookup<ZoxID>(true);
            //itemEntities.Dispose();