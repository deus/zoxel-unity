using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Items
{
    //! Disposes of UserItem's UniqueItem's.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class UserItemDestroySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // make generic and use UniqueMetaData component
            Dependency = Entities
                .WithAll<DestroyEntity, UserItem>()
                .ForEach((int entityInQueryIndex, in MetaData metaData) =>
            {
                var itemEntity = metaData.data;
                if (itemEntity.Index != 0 && HasComponent<Item>(itemEntity) && !HasComponent<RealmItem>(itemEntity))
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, itemEntity);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}