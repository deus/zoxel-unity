using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Items
{
    //! Links user items to inventory user after they spawn.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class UserItemLinkSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userItemsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
            RequireForUpdate(userItemsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<OnUserItemsSpawned>(processQuery);
            PostUpdateCommands2.RemoveComponent<NewUserItem>(userItemsQuery);
            //! First initializes Inventory.
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((ref Inventory inventory, in OnUserItemsSpawned onUserItemsSpawned) =>
            {
                if (onUserItemsSpawned.spawned != 0)
                {
                    inventory.Initialize(onUserItemsSpawned.spawned);
                }
            }).ScheduleParallel();
            //! Next, for each UserItem, link inventory to it with the pre created array.
            var parentEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var inventory = GetComponentLookup<Inventory>(false);
            // parentEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref userItemsQuery)
                .WithAll<NewUserItem>()
                .ForEach((Entity e, in UserDataIndex userDataIndex, in InventoryLink inventoryLink) =>
            {
                var inventory2 = inventory[inventoryLink.inventory];
                #if DATA_COUNT_CHECKS
                if (userDataIndex.index >= inventory2.items.Length)
                {
                    // UnityEngine.Debug.LogError("Index out of bounds: " + child.index + " to parent " + parentLink.parent.Index);
                    return;
                }
                #endif
                inventory2.items[userDataIndex.index] = e;
                if (parentEntities.Length > 0) { var e2 = parentEntities[0]; }
            })  .WithNativeDisableContainerSafetyRestriction(inventory)
                .WithReadOnly(parentEntities)
                .WithDisposeOnCompletion(parentEntities)
                .ScheduleParallel();
        }
    }
}