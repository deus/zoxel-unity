using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Items
{
    //! Disposes of Inventory's UserItem's.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class InventoryDestroySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyEntity, Inventory>()
                .ForEach((int entityInQueryIndex, in Inventory inventory) =>
            {
                for (var i = 0; i < inventory.items.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, inventory.items[i]);
                }
                inventory.DisposeFinal();
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}