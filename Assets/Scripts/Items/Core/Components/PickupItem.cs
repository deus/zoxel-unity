using Unity.Entities;

namespace Zoxel.Items
{
    //! An event entity of a Character picking up a WorldItem.
    public struct PickupItem : IComponentData
    {
        public Entity character;
        public WorldItem item;

        public PickupItem(Entity character, WorldItem item)
        {
            this.character = character;
            this.item = item;
        }
    }
}