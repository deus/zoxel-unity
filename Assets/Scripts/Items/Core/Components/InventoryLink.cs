using Unity.Entities;

namespace Zoxel.Items
{
    //! Links to an Entity with an Inventory component.
    public struct InventoryLink : IComponentData
    {
        public Entity inventory;

        public InventoryLink(Entity inventory)
        {
            this.inventory = inventory;
        }
    }
}