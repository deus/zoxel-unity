﻿using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Items
{
    //! Contains UserItem's. These link to item meta entities.
    /**
    *   They link to either Realm or Character's ItemLinks
    */
    public struct Inventory : IComponentData
    {
        //! Links to UserItem Entities.
        public BlitableArray<Entity> items;

        public void DisposeFinal()
        {
            items.DisposeFinal();
        }

        public void Dispose()
        {
            items.Dispose();
        }

        public void Initialize(byte count)
        {
			if (items.Length != count)
			{
				Dispose();
				items = new BlitableArray<Entity>(count, Allocator.Persistent);
			}
        }

        public int GetItemIndex(EntityManager EntityManager, int itemID)
        {
            for (int i = 0; i < items.Length; i++)
            {
                var userItemEntity = items[i];
                var metaData = EntityManager.GetComponentData<MetaData>(userItemEntity).data;
                if (metaData.Index != 0 && EntityManager.GetComponentData<ZoxID>(metaData).id == itemID)
                {
                    return i;
                }
            }
            return -1;
        }

        public int GetItemQuantity(EntityManager EntityManager, Entity item)
        {
            var quantity = 0;
            for (int i = 0; i < items.Length; i++)
            {
                var userItemEntity = items[i];
                var metaData = EntityManager.GetComponentData<MetaData>(userItemEntity).data;
                if (metaData == item)
                {
                    var userItem = EntityManager.GetComponentData<UserItemQuantity>(userItemEntity);
                    quantity += userItem.quantity;
                    // UnityEngine.Debug.LogError("Item at " + i + " quantity: " + item.quantity);
                }
            }
            return quantity;
        }
    }
}