using Unity.Entities;

namespace Zoxel.Items
{
    //! The item's quantity in a inventory.
    public struct UserItemQuantity : IComponentData
    {
        public byte quantity;

        public UserItemQuantity(byte quantity)
        {
            this.quantity = quantity;
        }
    }
}