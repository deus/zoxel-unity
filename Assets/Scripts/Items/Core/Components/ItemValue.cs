using Unity.Entities;

namespace Zoxel.Items
{
    //! Contains sales information for an Item.
    public struct ItemValue : IComponentData
    {
        public byte value;  //! When selling or buying it, trading it for gold pieces

        public ItemValue(byte value)
        {
            this.value = value;
        }
    }
}