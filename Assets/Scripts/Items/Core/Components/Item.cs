using Unity.Entities;

namespace Zoxel.Items
{
    //! An item tag for entities.
    /**
    *   Each item entity has various components added.
    */
    public struct Item : IComponentData { }
}