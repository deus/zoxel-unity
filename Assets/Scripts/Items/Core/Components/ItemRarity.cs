using Unity.Entities;

namespace Zoxel.Items
{
    //! A level of item rarity.
    /**
    *   Purple for Epic
    *   Orange for Rare
    *   Green for Good
    *   Gray for Common
    */
    public struct ItemRarity : IComponentData
    {
        public byte level;
    }
}