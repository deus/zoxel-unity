using Unity.Entities;

namespace Zoxel.Items
{
    //! The users item reference and data.
    public struct UserItem : IComponentData { }
}