using Unity.Entities;

namespace Zoxel.Items
{
    //! The users item reference and data.
    public struct UserItemActivated : IComponentData
    {
        public float lastActivated;
    }
}

//  public float cooldown;  // add this to item