using Unity.Entities;

namespace Zoxel.Items
{
    //! When user moves an item in inventory, this event gets added. Handled by action systems.

    public struct OnUserItemPickedUp : IComponentData
    {
        public Entity userItemPickedUp;
        public Entity itemPickedUp;

        public OnUserItemPickedUp(Entity userItemPickedUp, Entity itemPickedUp)
        {
            this.userItemPickedUp = userItemPickedUp;
            this.itemPickedUp = itemPickedUp;
        }
    }
    public struct OnUserItemPlaced : IComponentData
    {
        public Entity userItemPlaced;
        public Entity itemPlaced;

        public OnUserItemPlaced(Entity userItemPlaced, Entity itemPlaced)
        {
            this.userItemPlaced = userItemPlaced;
            this.itemPlaced = itemPlaced;
        }
    }

    public struct OnUserItemSwapped : IComponentData
    {
        public Entity lastUserItemPickedUp;
        public Entity lastItemPickedUp;
        public Entity userItemPickedUp;
        public Entity itemPickedUp;

        public OnUserItemSwapped(Entity lastUserItemPickedUp, Entity lastItemPickedUp, Entity userItemPickedUp, Entity itemPickedUp)
        {
            this.lastUserItemPickedUp = lastUserItemPickedUp;
            this.lastItemPickedUp = lastItemPickedUp;
            this.userItemPickedUp = userItemPickedUp;
            this.itemPickedUp = itemPickedUp;
        }
    }
}