using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Items
{
    //! Added to character when it's inventory is updated, used to update action uis.
    public struct OnInventoryUpdated : IComponentData
    {
        public BlitableArray<Entity> itemsUpdated;
        public BlitableArray<Entity> itemQuantitiesUpdated;

        /*public OnInventoryUpdated(in NativeList<Entity> itemsUpdated, in NativeList<Entity> itemQuantitiesUpdated)
        {
            this.itemsUpdated = new BlitableArray<Entity>(in itemsUpdated, Allocator.Persistent);
            this.itemQuantitiesUpdated = new BlitableArray<Entity>(in itemQuantitiesUpdated, Allocator.Persistent);
        }*/

        public void Dispose()
        {
            this.itemsUpdated.Dispose();
            this.itemQuantitiesUpdated.Dispose();
        }
    }
}