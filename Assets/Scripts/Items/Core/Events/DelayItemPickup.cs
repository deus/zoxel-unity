using Unity.Entities;

namespace Zoxel.Items
{
    //! Destroys an Entity after a set amount of time.
    public struct DelayItemPickup : IComponentData
    {
        public double started;
        public float life;

        public DelayItemPickup(double started, float life)
        {
            this.started = started;
            this.life = life;
        }

        public bool HasFinished(double elapsedTime)
        {
            return (elapsedTime - started >= life);
        }
    }
}