using Unity.Entities;

namespace Zoxel.Items
{
    //! An event for linking up user items after spawning.
    public struct OnUserItemsSpawned : IComponentData
    {
        public byte spawned;

        public OnUserItemsSpawned(byte spawned)
        {
            this.spawned = spawned;
        }
    }
}