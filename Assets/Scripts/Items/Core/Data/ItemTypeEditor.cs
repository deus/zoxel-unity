using System;

namespace Zoxel.Items
{
    [Serializable]
    public enum ItemTypeEditor : byte
    {
        None,
        Equipable,
        Consumable,
        Reagant,
        Currency,
        Voxel,
        PlaceableVox
    }
}