using System;

namespace Zoxel.Items
{
    public static class ItemType
    {
        public const byte None = 0;
        public const byte Equipable = 1;
        public const byte Consumable = 2;
        public const byte Reagant = 3;
        public const byte Currency = 4;
        public const byte Voxel = 5;
        public const byte PlaceableVox = 6;
    }
}