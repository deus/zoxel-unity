using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Audio;

namespace Zoxel.Items.UI
{
    //! Spawns Equipment UI Icons.
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(ItemUISystemGroup))]
    public partial class EquipmentUISpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity equipmentUIPrefab;
        public static IconPrefabs iconPrefabs;
        public static Entity spawnEquipmentUIPrefab;
        private EntityQuery processQuery;
        private EntityQuery equipmentsQuery;
        private EntityQuery userEquipmentsQuery;
        private EntityQuery itemsQuery;
        private EntityQuery slotsQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnEquipmentUI),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnEquipmentUIPrefab = EntityManager.CreateEntity(spawnArchetype);
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DelayEvent>(),
                ComponentType.ReadOnly<SpawnEquipmentUI>(),
                ComponentType.ReadOnly<CharacterLink>());
            userEquipmentsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserEquipment>());
            itemsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Item>());
            slotsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Slot>());
            RequireForUpdate(processQuery);
        }

        private void InitializePrefabs()
        {
            if (EquipmentUISpawnSystem.equipmentUIPrefab.Index != 0)
            {
                return;
            }
            var equipmentUIPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EquipmentUISpawnSystem.equipmentUIPrefab = equipmentUIPrefab;
            EntityManager.AddComponent<Prefab>(equipmentUIPrefab);
            EntityManager.AddComponent<EquipmentUI>(equipmentUIPrefab);
            EntityManager.SetComponentData(equipmentUIPrefab, new PanelUI(PanelType.EquipmentUI));
            iconPrefabs.frame = EntityManager.Instantiate(UICoreSystem.iconPrefabs.frame);
            EntityManager.AddComponent<Prefab>(iconPrefabs.frame);
            EntityManager.AddComponent<EquipmentButton>(iconPrefabs.frame);
            EntityManager.SetComponentData(iconPrefabs.frame, new OnChildrenSpawned(1));
            iconPrefabs.icon = EntityManager.Instantiate(UICoreSystem.iconPrefabs.icon);
            EntityManager.AddComponent<Prefab>(iconPrefabs.icon);
            EntityManager.AddComponent<ItemIcon>(iconPrefabs.icon);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var slotNames = ItemsManager.instance.GetSlotNames();
            var uiDatam = UIManager.instance.uiDatam;
            var iconStyle = uiDatam.iconStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var equipmentUIPrefab = EquipmentUISpawnSystem.equipmentUIPrefab;
            var iconPrefabs = EquipmentUISpawnSystem.iconPrefabs;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var spawnEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var characterLinks = GetComponentLookup<CharacterLink>(true);
            var prefabLinks = GetComponentLookup<PrefabLink>(true);
            var uiAnchors = GetComponentLookup<UIAnchor>(true);
            var userItemEntities = userEquipmentsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var userEquipmentMetaDatas = GetComponentLookup<MetaData>(true);
            userItemEntities.Dispose();
            var itemEntities = slotsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var slotLinks = GetComponentLookup<SlotLink>(true);
            itemEntities.Dispose();
            var equipmentParents = GetComponentLookup<EquipmentParent>(true);
            var slotEntities = slotsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var slots = GetComponentLookup<Slot>(true);
            slotEntities.Dispose();
            Dependency = Entities
                .WithAll<UILink>()
                .ForEach((Entity e, int entityInQueryIndex, in Equipment equipment, in CameraLink cameraLink) =>
            {
                // Does spawn a invenntory ui?
                var spawnEntity = new Entity();
                for (int i = 0; i < spawnEntities.Length; i++)
                {
                    var e2 = spawnEntities[i];
                    var characterLink = characterLinks[e2];
                    if (characterLink.character == e)
                    {
                        spawnEntity = e2;
                        break;  // only one UI per character atm
                    }
                }
                if (spawnEntity.Index == 0)
                {
                    return;
                }
                var prefab = equipmentUIPrefab;
                if (prefabLinks.HasComponent(spawnEntity))
                {
                    prefab = prefabLinks[spawnEntity].prefab;
                }
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CharacterLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLink);
                if (uiAnchors.HasComponent(spawnEntity))
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, uiAnchors[spawnEntity]);
                }
                // spawnedUI event
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));

                // also get all possible slots from equipment
                var childrenCount = (byte) 0;
                for (byte i = 0; i < equipment.body.Length; i++)
                {
                    var userEquipmentEntity = equipment.body[i];
                    if (!userEquipmentMetaDatas.HasComponent(userEquipmentEntity))
                    {
                        return;
                    }
                    var itemEntity = userEquipmentMetaDatas[userEquipmentEntity].data;
                    if (!slotLinks.HasComponent(itemEntity))
                    {
                        continue;
                    }
                    SpawnEquipmentIcon(PostUpdateCommands, entityInQueryIndex, elapsedTime, iconPrefabs, childrenCount, e3, e, userEquipmentEntity, in iconStyle);
                    childrenCount++;
                }
                var openSlots = new NativeList<Entity>();
                FindOpenSlots(in equipment, ref openSlots, in userEquipmentMetaDatas, in equipmentParents, in slotLinks, in slots);
                for (byte i = 0; i < openSlots.Length; i++)
                {
                    var slotEntity = openSlots[i];
                    var frameEntity = SpawnEquipmentIcon(PostUpdateCommands, entityInQueryIndex, elapsedTime, iconPrefabs, childrenCount, e3, e, new Entity(), in iconStyle);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, frameEntity, new EquipmentButtonOpenSlot(slotEntity));
                    childrenCount++;
                }
                openSlots.Dispose();
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawned(childrenCount));
            })  .WithReadOnly(spawnEntities)
                .WithDisposeOnCompletion(spawnEntities)
                .WithReadOnly(characterLinks)
                .WithReadOnly(prefabLinks)
                .WithReadOnly(uiAnchors)
                .WithReadOnly(userEquipmentMetaDatas)
                .WithReadOnly(slotLinks)
                .WithReadOnly(equipmentParents)
                .WithReadOnly(slots)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static Entity SpawnEquipmentIcon(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, double elapsedTime, IconPrefabs iconPrefabs, byte index,
            Entity panelEntity, Entity character, Entity userEquipmentEntity, in IconStyle iconStyle)
        {
            var seed = (int) (128 + elapsedTime + index * 2048 + 4196 * entityInQueryIndex);
            var frameEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.frame, panelEntity);
            UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, frameEntity, panelEntity);
            UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, frameEntity, index);
            UICoreSystem.SetSound(PostUpdateCommands, entityInQueryIndex, frameEntity, seed, iconStyle.soundVolume);
            UICoreSystem.SetSeed(PostUpdateCommands, entityInQueryIndex, frameEntity, seed);
            UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.icon, frameEntity);
            if (userEquipmentEntity.Index != 0)
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, frameEntity, new SetIconData(userEquipmentEntity));
            }
            return frameEntity;
        }

        public static void FindOpenSlots(in Equipment equipment, ref NativeList<Entity> openSlots,
            in ComponentLookup<MetaData> userEquipmentMetaDatas, in ComponentLookup<EquipmentParent> equipmentParents,
            in ComponentLookup<SlotLink> slotLinks, in ComponentLookup<Slot> slots)
        {
            for (int i = equipment.body.Length - 1; i >= 0; i--)
            {
                var userEquipmentEntity = equipment.body[i];
                if (!userEquipmentMetaDatas.HasComponent(userEquipmentEntity))
                {
                    continue;
                }
                var itemEntity = userEquipmentMetaDatas[userEquipmentEntity].data;
                if (!slotLinks.HasComponent(itemEntity))
                {
                    continue;
                }
                var slotEntity = slotLinks[itemEntity].slot;
                var slot = slots[slotEntity];
                // for all female slots, find an empty one if it is the slot of our male one
                for (int j = 0; j < slot.femaleSlots.Length; j++)
                {
                    var femaleSlotEntity = slot.femaleSlots[j];
                    // check if anything connected to it
                    bool isConnected = false;
                    for (int k = 0; k < equipment.body.Length; k++)
                    {
                        if (i == k)
                        {
                            continue;
                        }
                        var userEquipmentEntity2 = equipment.body[k];
                        var parent = equipmentParents[userEquipmentEntity2].parent;
                        if (parent == userEquipmentEntity)
                        {
                            isConnected = true;
                            break;
                        }
                    }
                    if (!isConnected)
                    {
                        openSlots.Add(femaleSlotEntity);
                    }
                }
            }
        }
    }
}