using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Textures;

namespace Zoxel.Items.UI
{
    //! Handles UIClickEvent's in the EquipmentUI.
    /**
    *   - UIClickEvent System -
    *   \todo Replace Equipment body with links to UserParts - similar to UserItems.
    *   \todo Fix bug; Cannot remove two body parts at once due to stacking.
    */
    [BurstCompile, UpdateInGroup(typeof(ItemUISystemGroup))]
    public partial class EquipmentUIClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllerQuery;
        private EntityQuery charactersQuery;
        private EntityQuery userItemsQuery;
        private EntityQuery userEquipmentsQuery;
        private EntityQuery itemsQuery;
        private EntityQuery slotsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllerQuery = GetEntityQuery(
                ComponentType.ReadOnly<Player>(),
                ComponentType.ReadOnly<CharacterLink>());
            charactersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Character>(),
                ComponentType.ReadOnly<Inventory>(),
                ComponentType.ReadOnly<Equipment>());
            userItemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserItem>(),
                ComponentType.ReadOnly<DestroyEntity>());
            userEquipmentsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserEquipment>(),
                ComponentType.ReadOnly<DestroyEntity>());
            itemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Item>(),
                ComponentType.ReadOnly<DestroyEntity>());
            slotsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Slot>(),
                ComponentType.ReadOnly<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            // var doesIncludeBodyParts = true;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = controllerQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var characterLinks = GetComponentLookup<CharacterLink>(true);
            controllerEntities.Dispose();
            var characterEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var inventorys = GetComponentLookup<Inventory>(true);
            var equipments = GetComponentLookup<Equipment>(true);
            characterEntities.Dispose();
            var userItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var userItemQuantities = GetComponentLookup<UserItemQuantity>(false);
            var itemMetaDatas = GetComponentLookup<MetaData>(false);
            userItemEntities.Dispose();
            var userEquipmentEntities = userEquipmentsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var equipmentMetaDatas = GetComponentLookup<MetaData>(true);
            userEquipmentEntities.Dispose();
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleE);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleE);
            var slotLinks = GetComponentLookup<SlotLink>(true);
            itemEntities.Dispose();
            var slotEntities = slotsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleF);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleF);
            var slots = GetComponentLookup<Slot>(true);
            slotEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<EquipmentButton>()
                .ForEach((Entity e, int entityInQueryIndex, in Child child, in Childrens childrens, in IconData iconData, in UIClickEvent uiClickEvent) =>
            {
                var arrayIndex = child.index;
                var controllerEntity = uiClickEvent.controller;
                var characterEntity = characterLinks[controllerEntity].character;
                if (!HasComponent<Equipment>(characterEntity))
                {
                    return;
                }
                var userEquipmentEntity = iconData.data;
                if (userEquipmentEntity.Index == 0)
                {
                    return;
                }
                var itemEntity = equipmentMetaDatas[userEquipmentEntity].data;
                if (itemEntity.Index == 0)
                {
                    // UnityEngine.Debug.LogError("Cannot Remove gear at index: " + arrayIndex + ", layer: " + equipmentItem.layer + ", itemEntity.Index: " + itemEntity.Index);
                    return;
                }
                var slotEntity = slotLinks[itemEntity].slot;
                var layer = slots[slotEntity].layer;
                if (layer == SlotLayer.Body)
                {
                    return;
                }
                // UnityEngine.Debug.LogError("Removing gear at index: " + arrayIndex);
                var inventory = inventorys[characterEntity];
                var equipment = equipments[characterEntity];
                if (!CanRemoveBodyItem(in equipment, userEquipmentEntity))
                {
                    // UnityEngine.Debug.LogError("Cannot remove body part.");
                    return;
                }
                //! Adds removed Body Part to inventory, by finding the empty UserItem.
                var emptyInventorySpot = false;
                for (int i = 0; i < inventory.items.Length; i++)
                {
                    var userItemEntity = inventory.items[i];
                    var metaData = itemMetaDatas[userItemEntity];
                    if (metaData.data.Index == 0)
                    {
                        metaData.data = itemEntity;
                        itemMetaDatas[userItemEntity] = metaData;
                        var userItem = userItemQuantities[userItemEntity];
                        userItem.quantity = 1;
                        userItemQuantities[userItemEntity] = userItem;
                        emptyInventorySpot = true;
                        break;
                    }
                }
                if (!emptyInventorySpot)
                {
                    // UnityEngine.Debug.LogError("Cannot Unequip as inventory is full.");
                    return;
                }
                RemoveBodyItem(PostUpdateCommands, entityInQueryIndex, ref equipment, userEquipmentEntity);
                // remove UI of children
                PostUpdateCommands.AddComponent(entityInQueryIndex, childrens.children[0], new GenerateEmptyIcon(32, 1));
                // now effect character
                PostUpdateCommands.SetComponent(entityInQueryIndex, characterEntity, equipment);
                PostUpdateCommands.AddComponent<SkeletonDirty>(entityInQueryIndex, characterEntity);
                PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, characterEntity);
                PostUpdateCommands.AddComponent<SaveEquipment>(entityInQueryIndex, characterEntity);
                if (HasComponent<EntitySaver>(characterEntity))
                {
                    PostUpdateCommands.AddComponent<SaveInventory>(entityInQueryIndex, characterEntity);
                }
                // now set equipment item
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetIconData(new Entity()));
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new UISelectEvent(controllerEntity));
            })  .WithReadOnly(characterLinks)
                .WithReadOnly(inventorys)
                .WithReadOnly(equipments)
                .WithReadOnly(equipmentMetaDatas)
                .WithReadOnly(slotLinks)
                .WithReadOnly(slots)
                .WithNativeDisableContainerSafetyRestriction(userItemQuantities)
                .WithNativeDisableContainerSafetyRestriction(itemMetaDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        // bool doesIncludeBodyParts, 
        public static bool CanRemoveBodyItem(in Equipment equipment, Entity userEquipmentEntity)
        {
            for (int i = 0; i < equipment.body.Length; i++)
            {
                var userEquipmentEntity2 = equipment.body[i];
                if (userEquipmentEntity == userEquipmentEntity2)
                {
                    return true;
                }
            }
            return false;
        }

        public static void RemoveBodyItem(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, ref Equipment equipment, Entity userEquipmentEntity)
        {
            byte bodyIndex = 255;
            for (byte i = 0; i < equipment.body.Length; i++)
            {
                var userEquipmentEntity2 = equipment.body[i];
                if (userEquipmentEntity == userEquipmentEntity2)
                {
                    bodyIndex = i;
                    break;
                }
            }
            if (bodyIndex == 255)
            {
                return;
            }
            PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, userEquipmentEntity);
            var newParts = new BlitableArray<Entity>(equipment.body.Length - 1, Allocator.Persistent);
            for (int i = 0; i < equipment.body.Length; i++)
            {
                if (i < bodyIndex)
                {
                    newParts[i] = equipment.body[i];
                }
                else if (i > bodyIndex)
                {
                    newParts[i - 1] = equipment.body[i];
                }
            }
            for (int i = bodyIndex; i < newParts.Length; i++)
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, newParts[i], new UserDataIndex(i));
            }
            equipment.Dispose();
            equipment.body = newParts;
        }
    }
}