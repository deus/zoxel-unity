using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.UI;

namespace Zoxel.Items.UI
{ 
    //! Handls Slection of EquipmentButtons.
    [BurstCompile, UpdateInGroup(typeof(ItemUISystemGroup))]
    public partial class EquipmentUISelectSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = UISystemGroup.elapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<UISelectEvent, EquipmentButton, EquipmentButtonOpenSlot>()
                .ForEach((Entity e, in Child child, in EquipmentButtonOpenSlot equipmentButtonOpenSlot, in PanelLink panelLink, in UISelectEvent uiSelectEvent) =>
            {
                // UnityEngine.Debug.LogError("(1) Selected EquipmentButton: " + e.Index + " at " + elapsedTime);
                var panelEntity = panelLink.panel;
                var slotName = EntityManager.GetComponentData<ZoxName>(equipmentButtonOpenSlot.slot).name.Clone();
                PostUpdateCommands.AddComponent(panelEntity, new SetPanelTooltip(slotName));
            }).WithoutBurst().Run();
            Entities
                .WithNone<EquipmentButtonOpenSlot>()
                .WithAll<UISelectEvent, EquipmentButton>()
                .ForEach((Entity e, in Child child, in IconData iconData, in UISelectEvent uiSelectEvent, in PanelLink panelLink) =>
            {
                // UnityEngine.Debug.LogError("(2) Selected EquipmentButton: " + e.Index + " at " + elapsedTime);
                var controllerEntity = uiSelectEvent.character;
                var userEquipmentEntity = iconData.data;
                if (userEquipmentEntity.Index == 0)
                {
                    return;
                }
                var itemEntity = EntityManager.GetComponentData<MetaData>(userEquipmentEntity).data;
                var equipmentSelectedName = new Text();
                if (itemEntity.Index != 0)
                {
                    equipmentSelectedName = EntityManager.GetComponentData<ZoxName>(itemEntity).name;
                }
                var tooltipText2 = "";
                if (itemEntity.Index != 0)
                {
                    tooltipText2 += "" + equipmentSelectedName.ToString(); //  + "\n";
                }
                else
                {
                    tooltipText2 = "Empty Slot";
                }
                // UnityEngine.Debug.LogError("[" + elapsedTime + "] SetTooltips New [" + e.Index + "]");
                var slotEntity = EntityManager.GetComponentData<SlotLink>(itemEntity).slot;
                var slot = EntityManager.GetComponentData<Slot>(slotEntity);
                var tooltipText = "";
                if (slot.layer == SlotLayer.Body)
                {
                    tooltipText = "Cannot Unequip " + equipmentSelectedName.ToString();
                }
                else
                {
                    tooltipText = "[action1] to Unequip " + equipmentSelectedName.ToString();
                }
                PostUpdateCommands.AddComponent(panelLink.panel, new SetPanelTooltip(new Text(tooltipText2)));
                PostUpdateCommands.AddComponent(controllerEntity, new SetPlayerTooltip(new Text(tooltipText)));
            }).WithoutBurst().Run();
        }
    }
}

// var e3 = PostUpdateCommands.CreateEntity();
// PostUpdateCommands.AddComponent(e3, new SetPlayerTooltip(new Text(tooltipText)));
// var playerTooltipLinks = EntityManager.GetComponentData<PlayerTooltipLinks>(controllerEntity);
// playerTooltipLinks.SetTooltipText(PostUpdateCommands, controllerEntity, tooltipText);

/*if (HasComponent<PanelTooltipLink>(panelEntity))
{
    var tooltip = EntityManager.GetComponentData<PanelTooltipLink>(panelEntity).tooltip;
    if (HasComponent<RenderText>(tooltip))
    {
        PostUpdateCommands.AddComponent(tooltip, new SetTooltipText(slotName));
    }
}*/


// Get Realm Slots and use that
/*if (slots.ContainsKey(item.maleSlot.id))
{
    tooltipText += " e " + slots[item.maleSlot.id].name;
}*/

/*if (HasComponent<PanelTooltipLink>(panelEntity))
{
    var tooltip = EntityManager.GetComponentData<PanelTooltipLink>(panelEntity).tooltip;
    if (HasComponent<RenderText>(tooltip))
    {
        PostUpdateCommands.AddComponent(tooltip, new SetTooltipText(tooltipText));
    }
}*/
// var characterEntity = EntityManager.GetComponentData<CharacterLink>(controllerEntity).character;