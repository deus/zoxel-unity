using Unity.Entities;

namespace Zoxel.Items.UI
{
    //! A button on the equipment UI.
    public struct EquipmentButton : IComponentData { }
}