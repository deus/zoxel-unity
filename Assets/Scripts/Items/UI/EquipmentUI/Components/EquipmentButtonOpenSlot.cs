using Unity.Entities;

namespace Zoxel.Items.UI
{
    public struct EquipmentButtonOpenSlot : IComponentData
    {
        public Entity slot;

        public EquipmentButtonOpenSlot(Entity slot)
        {
            this.slot = slot;
        }
    }
}