using Unity.Entities;

namespace Zoxel.Items.UI
{
    //! ItemUI for the mouse following.
    public struct MouseUserItem : IComponentData
    {
        public Entity inventoryEntity;
        public Entity userItemEntity;   // last clicked user data
        // public UserItem userItem;

        public MouseUserItem(Entity inventoryEntity, Entity userItemEntity) // , UserItem userItem)
        {
            this.inventoryEntity = inventoryEntity;
            this.userItemEntity = userItemEntity;
            // this.userItem = userItem;
        }
    }
}