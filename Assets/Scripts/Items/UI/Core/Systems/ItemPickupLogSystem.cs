using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Logs;

namespace Zoxel.Items.UI
{
    //! Updates LogUI with the item you picked up.
    /**
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(ItemUISystemGroup))]
    public partial class ItemPickupLogSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Text text;
        private Text text2;
        private EntityQuery processQuery;
        private EntityQuery itemsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            text = new Text("You Got ");
            text2 = new Text(" x");
            itemsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Item>(),
                ComponentType.ReadOnly<ZoxID>());
            RequireForUpdate(processQuery);
            RequireForUpdate(itemsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var text = this.text;
            var text2 = this.text;
            var logPrefab = LogSystemGroup.logPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var itemNames = GetComponentLookup<ZoxName>(true);
            itemEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity, DelayEvent>()
                .ForEach((Entity e, int entityInQueryIndex, in PickupItem pickupItem) =>
            {
                var worldItem = pickupItem.item;
                var worldItemName = itemNames[worldItem.item].name;
                var logText = text.Clone();
                logText.AddText(in worldItemName);
                // var tooltip = "You Got " + worldItemName.ToString(); // worldItem.data.name;
                if (worldItem.quantity >= 2)
                {
                    logText.AddText(in text2);
                    logText.AddInteger(worldItem.quantity);
                    // tooltip += " x" + worldItem.quantity;
                }
                PostUpdateCommands.AddComponent<OnLogsSpawned>(entityInQueryIndex, pickupItem.character);
                var logEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, logPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, logEntity, new LogText(logText));
                PostUpdateCommands.SetComponent(entityInQueryIndex, logEntity, new LogHolderLink(pickupItem.character));
            })  .WithReadOnly(itemNames)
                .ScheduleParallel();
        }

        protected override void OnDestroy()
        {
            text.Dispose();
            text2.Dispose();
        }
    }
}