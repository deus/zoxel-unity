using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.UI;
using Zoxel.Voxels;
using Zoxel.Textures;
using Zoxel.Textures.Voxels;

namespace Zoxel.Items.UI
{
    //! Sets the ItemUI's icon and texture.
    /**
    *   - UI Update System -
    */
    [BurstCompile, UpdateInGroup(typeof(ItemUISystemGroup))]
    public partial class ItemUISetSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            // Set icon to item
            Entities
                .WithNone<DelayEventFrames, InitializeEntity>()
                .WithAll<SetIconData>()
                .ForEach((Entity e, ref IconData iconData, in SetIconData setIconData, in Childrens childrens) =>
            {
                if (!HasComponent<UserItem>(setIconData.data) && !HasComponent<UserEquipment>(setIconData.data))
                {
                    return;
                }
                /*if (!HasComponent<MetaData>(setIconData.data))
                {
                    return;
                }*/
                // UnityEngine.Debug.LogError("    > SetIconData");
                iconData.data = setIconData.data;
                if (childrens.children.Length >= 1)
                {
                    var metaEntity = EntityManager.GetComponentData<MetaData>(iconData.data).data;
                    var iconEntity = childrens.children[0];
                    SetIconTextureEvent(EntityManager, PostUpdateCommands, iconEntity, metaEntity);
                }
                if (childrens.children.Length >= 2)
                {
                    var quantity = 1;
                    if (HasComponent<UserItemQuantity>(iconData.data))
                    {
                        quantity = EntityManager.GetComponentData<UserItemQuantity>(iconData.data).quantity;
                    }
                    var textEntity = childrens.children[1];
                    var subText = quantity.ToString();
                    if (subText == "0" || subText == "1")
                    {
                        subText = "";
                    }
                    PostUpdateCommands.AddComponent(textEntity, new SetRenderText(new Text(subText)));
                }
                // UnityEngine.Debug.LogError("Set to new Item: " + e.Index + " :: " + setIconData.data.Index + " with item meta: " + metaEntity.Index);
            }).WithoutBurst().Run();
        }
        
        public static void SetIconTextureEvent(EntityManager EntityManager, EntityCommandBuffer PostUpdateCommands, Entity iconEntity, Entity itemEntity)
        {
            // UnityEngine.Debug.LogError("Set Icon Texture: " + userItem.item.Index);
            var voxDataEntity = new Entity();
            var textureEntity = new Entity();
            var isDebugIcon = false;
            // For Voxel Items
            if (EntityManager.HasComponent<VoxelItem>(itemEntity))
            {
                // set item texture to voxel
                var voxelItem = EntityManager.GetComponentData<VoxelItem>(itemEntity);
                var voxelType = voxelItem.voxelType;
                var voxelEntity = voxelItem.voxel;
                if (EntityManager.HasComponent<Chunk>(voxelEntity))
                {
                    voxDataEntity = voxelEntity;
                }
                else if (EntityManager.HasComponent<GenerateVoxData>(voxelEntity))
                {
                    UnityEngine.Debug.Log("Todo: Spawn generated Voxel Vox Textures.");
                    isDebugIcon = true;
                }
                else
                {
                    // Set as voxel texture
                    if (EntityManager.HasComponent<Texture>(voxelEntity))
                    {
                        textureEntity = voxelEntity;
                    }
                    else if (EntityManager.HasComponent<VoxelTextureTypes>(voxelEntity))
                    {
                        var voxelChildren = EntityManager.GetComponentData<Childrens>(voxelEntity);
                        if (voxelChildren.children.Length > 0)
                        {
                            textureEntity = voxelChildren.children[0];
                        }
                    }
                }
            }
            // For all other items, having VoxDatas
            else
            {
                // Realm Items
                if (itemEntity.Index != 0 && EntityManager.HasComponent<Chunk>(itemEntity))
                {
                    voxDataEntity = itemEntity;
                }
            }
            if (voxDataEntity.Index > 0)
            {
                // UnityEngine.Debug.LogError("Generate Texture Vox!");
                PostUpdateCommands.AddComponent(iconEntity, new GenerateTextureVox(voxDataEntity, 32, 1));
            }
            else if (textureEntity.Index > 0)
            {
                var oldTexture = EntityManager.GetComponentData<Texture>(iconEntity);
                oldTexture.Dispose();
                if (textureEntity.Index > 0)
                {
                    var newTexture = EntityManager.GetComponentData<Texture>(textureEntity).Clone();
                    PostUpdateCommands.SetComponent(iconEntity, newTexture);
                    PostUpdateCommands.AddComponent<TextureDirty>(iconEntity);
                    PostUpdateCommands.AddComponent(iconEntity, new TextureDirtyDelayed(2));
                }
                
            }
            else if (isDebugIcon)
            {
                PostUpdateCommands.AddComponent(iconEntity, new GenerateSquareIcon(32));
            }
            else
            {
                PostUpdateCommands.AddComponent(iconEntity, new GenerateEmptyIcon(32, 1));
            }
        }
    }
}