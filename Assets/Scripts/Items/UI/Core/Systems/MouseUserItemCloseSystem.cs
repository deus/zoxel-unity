using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.UI;
using Zoxel.UI.MouseFollowing;

namespace Zoxel.Items.UI
{
    //! Closes the MouseUserItem entity.
    /**
    *   \todo Convert to Parallel System.
    *   \todo Add the item back to the one it was picked from! Do this w ithout the 'pickup' event, use an AddItemToInventory event! similar to pickup..
    */
    [BurstCompile, UpdateInGroup(typeof(ItemUISystemGroup))]
    public partial class MouseUserItemCloseSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var elapsedTime = World.Time.ElapsedTime;
            var pickupItemPrefab = ItemHitSystem.pickupItemPrefab;
            //! When closing MouseUserItem Mouse UIs!
            Entities
                .WithNone<DelayEventFrames>()
                .WithAll<HideMouseUI>()
                .ForEach((Entity e, ref MouseUserItem mouseUserItem, ref UserItemQuantity userItemQuantity, ref MetaData metaData, in MouseUI mouseUI, in CharacterLink characterLink, in Childrens childrens) =>
            {
                PostUpdateCommands.RemoveComponent<MouseUserItem>(e);
                PostUpdateCommands.RemoveComponent<FollowingMouse>(e);
                PostUpdateCommands.RemoveComponent<HideMouseUI>(e);
                PostUpdateCommands.RemoveComponent<UserItem>(e);
                DestroyItemUI(PostUpdateCommands, childrens.children[0]);
                PostUpdateCommands.AddComponent<ClearRenderText>(childrens.children[1]);
                var inventoryEntity = mouseUserItem.inventoryEntity;
                var mouseItemEntity = metaData.data;
                var mouseItemQuantity = userItemQuantity.quantity;
                if (mouseItemEntity.Index != 0 && mouseItemQuantity != 0)
                {
                    var userEntity = mouseUI.userEntity;
                    // if Clicked Mouse in Inventory is Empty
                    var didAdd = false;
                    if (userEntity.Index > 0 && HasComponent<UserItem>(userEntity))
                    {
                        var userMetaEntity = EntityManager.GetComponentData<MetaData>(userEntity).data;
                        if (userMetaEntity.Index == 0)
                        {
                            PostUpdateCommands.SetComponent(userEntity, userItemQuantity);
                            PostUpdateCommands.SetComponent(userEntity, metaData);
                            PostUpdateCommands.AddComponent<SaveInventory>(inventoryEntity);
                            // todo: update inventory ui at that index
                            if (HasComponent<UIElement>(mouseUI.button))
                            {
                                PostUpdateCommands.AddComponent(mouseUI.button, new SetIconData(userEntity));
                            }
                            didAdd = true;
                        }
                    }
                    if (!didAdd)
                    {
                        var controllerEntity = characterLink.character;
                        var characterEntity = EntityManager.GetComponentData<CharacterLink>(controllerEntity).character;
                        // todo: add to new spot in inventory
                        // refresh the index added to, using children lookup of inventoryUI
                        // add to new spot in inventory
                        var worldItem = new WorldItem(mouseItemEntity, mouseItemQuantity, elapsedTime);
                        var pickupEntity = PostUpdateCommands.Instantiate(pickupItemPrefab);
                        PostUpdateCommands.SetComponent(pickupEntity, new PickupItem(characterEntity, worldItem));
                        PostUpdateCommands.SetComponent(pickupEntity, new DelayEvent(elapsedTime, 0.01));
                    }
                    userItemQuantity.quantity = 0;
                    metaData.data = new Entity();
                }
            }).WithoutBurst().Run();
        }

        private void DestroyItemUI(EntityCommandBuffer PostUpdateCommands, Entity iconEntity)
        {
            Janitor.DestroyTexture(EntityManager, iconEntity);
            var renderer = EntityManager.GetSharedComponentManaged<ZoxMesh>(iconEntity);
            if (renderer.material)
            {
                renderer.material.SetTexture("_BaseMap", TextureUtil.CreateBlankTexture());
                PostUpdateCommands.SetSharedComponentManaged(iconEntity, renderer);
            }
        }
    }
}

        /*private void DestroyText(EntityCommandBuffer PostUpdateCommands, Entity textEntity)
        {
            if (HasComponent<RenderText>(textEntity))
            {
                // event for clearing?
                var renderText = EntityManager.GetComponentData<RenderText>(textEntity);
                if (renderText.ClearText())
                {
                    PostUpdateCommands.SetComponent(textEntity, renderText);
                    PostUpdateCommands.AddComponent<RenderTextDirty>(textEntity);
                }
            }
        }*/