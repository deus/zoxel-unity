﻿using Unity.Entities;

namespace Zoxel.Items.UI
{
    //! Item UI systems involve inventory, equipment, trading, etc.
    [UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class ItemUISystemGroup : ComponentSystemGroup { }
}
