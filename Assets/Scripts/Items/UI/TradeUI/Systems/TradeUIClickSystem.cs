using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.UI;

namespace Zoxel.Items.UI
{
    //! Handles UIClickEvent for ItemButton's.
    /**
    *   - UIClickEvent System -
    *   \todo When switching items or anything, create events for that.
    *   \todo When switching items, switch cooldowns too or any other user item data.
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(ItemUISystemGroup))]
    public partial class TradeUIClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var pickupItemPrefab = ItemHitSystem.pickupItemPrefab;
            Entities
                .WithAll<TradeUIButton>()
                .ForEach((Entity e, in IconData iconData, in Child child, in ParentLink parentLink, in UIClickEvent uiClickEvent) =>
            {
                var controllerEntity = uiClickEvent.controller;
                var characterEntity = EntityManager.GetComponentData<CharacterLink>(controllerEntity).character;
                var arrayIndex = child.index;
                var buttonType = uiClickEvent.buttonType;
                var panelUI = parentLink.parent;
                var isShop = HasComponent<TradeUI>(panelUI);
                Entity inventoryEntity;
                if (HasComponent<InventoryLink>(panelUI))
                {
                    inventoryEntity = EntityManager.GetComponentData<InventoryLink>(panelUI).inventory;
                }
                else
                {
                    inventoryEntity = characterEntity;
                }
                var userItemEntity = iconData.data;
                var userItem = EntityManager.GetComponentData<UserItemQuantity>(userItemEntity);
                var userItemMetaData = EntityManager.GetComponentData<MetaData>(userItemEntity);
                var itemEntity = userItemMetaData.data;
                if (itemEntity.Index == 0)
                {
                    return;
                }
                // if gold coins exist, trade gold coins to player characterEntity for the item
                // add toast ui to Log for purchase
                //UnityEngine.Debug.LogError("Purchasing Item " + itemClicked.name);
                if (userItem.quantity != 0)
                {
                    // get currency of characterEntity player
                    var playerInventory = EntityManager.GetComponentData<Inventory>(characterEntity);
                    // later make an item property for currency
                    var coinValue = 0;
                    for (int i = 0; i < playerInventory.items.Length; i++)
                    {
                        var coinInventoryItemEntity = playerInventory.items[i];
                        var coinMetaData = EntityManager.GetComponentData<MetaData>(coinInventoryItemEntity);
                        var coinItemEntity = coinMetaData.data;
                        if (coinItemEntity.Index == 0)
                        {
                            continue;
                        }
                        if (HasComponent<ItemCurrency>(coinItemEntity))
                        {
                            // add value
                            var coinUserItem = EntityManager.GetComponentData<UserItemQuantity>(coinInventoryItemEntity);
                            coinValue += coinUserItem.quantity;
                        }
                    }
                    // var playerTooltipLinks = EntityManager.GetComponentData<PlayerTooltipLinks>(controllerEntity);
                    //var logUILink = EntityManager.GetComponentData<LogUILink>(controllerEntity);
                    var itemName = EntityManager.GetComponentData<ZoxName>(itemEntity).name;
                    var itemClickedValue = EntityManager.GetComponentData<ItemValue>(itemEntity).value;
                    if (coinValue < itemClickedValue)
                    {
                        //! \todo Different click sound for failed
                        //! \todo Overlay animation for cannot purchase item
                        //logUILink.AddToLogUI(PostUpdateCommands, "You Cannot Purchase a " + itemName.ToString() +
                        //    ". You need " + (itemClickedValue - coinValue) + " more coins.", elapsedTime);
                        return;
                    }
                    // reduce characters currency
                    for (int i = 0; i < playerInventory.items.Length; i++)
                    {
                        var coinInventoryItemEntity = playerInventory.items[i];
                        var coinMetaData = EntityManager.GetComponentData<MetaData>(coinInventoryItemEntity);
                        var coinItemEntity = coinMetaData.data;
                        if (coinItemEntity.Index == 0)
                        {
                            continue;
                        }
                        if (HasComponent<ItemCurrency>(coinItemEntity))
                        {
                            // Decrease coin value
                            var coinUserItem = EntityManager.GetComponentData<UserItemQuantity>(coinInventoryItemEntity);
                            while (coinUserItem.quantity != 0 && itemClickedValue != 0)
                            {
                                coinUserItem.quantity--;
                                itemClickedValue--;
                                if (coinUserItem.quantity == 0)
                                {
                                    coinUserItem.quantity = 0;
                                }
                            }
                            PostUpdateCommands.SetComponent(coinInventoryItemEntity, coinUserItem);
                            if (itemClickedValue == 0)
                            {
                                break;
                            }
                        }
                    }
                    // Give item to player
                    var pickupEntity = PostUpdateCommands.Instantiate(pickupItemPrefab);
                    PostUpdateCommands.SetComponent(pickupEntity, new PickupItem(characterEntity, new WorldItem
                    {
                        item = itemEntity,
                        quantity = 1
                    }));
                    PostUpdateCommands.SetComponent(pickupEntity, new DelayEvent(elapsedTime, 0.3));
                    // Reduce Sold Item for this
                    userItem.quantity--;
                    if (userItem.quantity == 0)
                    {
                        // Reset meta data
                        PostUpdateCommands.SetComponent(userItemEntity, new MetaData());
                    }
                    PostUpdateCommands.SetComponent(userItemEntity, userItem);
                    // Update UI visuals of Sold Item
                    PostUpdateCommands.AddComponent(e, new SetIconData(userItemEntity));
                    // Finalize Components
                    // PostUpdateCommands.SetComponent(inventoryEntity, inventory);
                    // PostUpdateCommands.SetComponent(characterEntity, playerInventory);
                    if (HasComponent<EntitySaver>(characterEntity))
                    {
                        PostUpdateCommands.AddComponent<SaveInventory>(characterEntity);
                    }
                    if (inventoryEntity != characterEntity && HasComponent<EntitySaver>(inventoryEntity))
                    {
                        PostUpdateCommands.AddComponent<SaveInventory>(inventoryEntity);
                    }
                    // todo: Save NPC
                    // todo: Restock Items NPC weekly based on trade and gathering of town npcs
                    //! \todo Only add Purchased event to log. Don't do 'You got' event. Make that for picking up items from world only.
                    // logUILink.AddToLogUI(PostUpdateCommands, "You Purchased Item " + itemName.ToString(), elapsedTime);
                }
            }).WithoutBurst().Run();
        }
    }
}