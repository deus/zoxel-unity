using Unity.Entities;

namespace Zoxel.Items.UI
{
    public struct SpawnTradeUI : IComponentData
    {
        public Entity characterSelling;

        public SpawnTradeUI(Entity characterSelling)
        {
            this.characterSelling = characterSelling;
        }
    }
}