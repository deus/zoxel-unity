using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.UI;

namespace Zoxel.Items.UI
{
    //! Handles UIClickEvent for Equipping ItemButton's.
    /**
    *   - UIClickEvent System -
    */
    [BurstCompile, UpdateInGroup(typeof(ItemUISystemGroup))]
    public partial class ItemUIEquipSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllersQuery;
        private EntityQuery inventorysQuery;
        private EntityQuery panelUIsQuery;
        private EntityQuery userItemsQuery;
        private EntityQuery itemsQuery;
        private EntityQuery slotsQuery;
        private EntityQuery userEquipmentsQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Player>(),
                ComponentType.ReadOnly<CharacterLink>());
            panelUIsQuery = GetEntityQuery(ComponentType.ReadOnly<InventoryLink>());
            inventorysQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Inventory>());
            userItemsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserItem>());
            itemsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Item>(),
                ComponentType.ReadOnly<SlotLink>());
            slotsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Slot>());
            userEquipmentsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserEquipment>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var userEquipmentPrefab = ItemSystemGroup.userEquipmentPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = inventorysQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var characterLinks = GetComponentLookup<CharacterLink>(true);
            controllerEntities.Dispose();
            var panelEntities = panelUIsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var inventoryLinks = GetComponentLookup<InventoryLink>(true);
            panelEntities.Dispose();
            var inventoryEntities = inventorysQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            // var inventorys = GetComponentLookup<Inventory>(true);
            var equipments = GetComponentLookup<Equipment>(true);
            inventoryEntities.Dispose();
            var userItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var userItemMetaDatas = GetComponentLookup<MetaData>(true);
            var userItemQuantitys = GetComponentLookup<UserItemQuantity>(true);
            userItemEntities.Dispose();
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleE);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleE);
            var itemEquipables = GetComponentLookup<ItemEquipable>(true);
            var slotLinks = GetComponentLookup<SlotLink>(true);
            itemEntities.Dispose();
            var slotEntities = slotsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleF);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleF);
            var slots = GetComponentLookup<Slot>(true);
            slotEntities.Dispose();
            var userEquipmentEntities = userEquipmentsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleH);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleH);
            var userEquipmentMetaDatas = GetComponentLookup<MetaData>(true);
            var userEquipmentEquipmentParents = GetComponentLookup<EquipmentParent>(true);
            userEquipmentEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<TradeUIButton>()
                .WithAll<ItemButton>()
                .ForEach((Entity e, int entityInQueryIndex, in IconData iconData, in ParentLink parentLink, in UIClickEvent uiClickEvent) =>
            {
                var controllerEntity = uiClickEvent.controller;
                if (!characterLinks.HasComponent(controllerEntity))
                {
                    return;
                }
                var characterEntity = characterLinks[controllerEntity].character;
                Entity inventoryEntity;
                if (inventoryLinks.HasComponent(parentLink.parent))
                {
                    inventoryEntity = inventoryLinks[parentLink.parent].inventory;
                }
                else
                {
                    inventoryEntity = characterEntity;
                }
                if (!equipments.HasComponent(inventoryEntity))
                {
                    return;
                }
                var equipment = equipments[inventoryEntity];
                var userItemEntity = iconData.data;
                if (!userItemMetaDatas.HasComponent(userItemEntity))
                {
                    return;
                }
                var itemEntity = userItemMetaDatas[userItemEntity].data;
                if (!itemEquipables.HasComponent(itemEntity))
                {
                    return;
                }
                //! Equip into slot 0
                if (uiClickEvent.buttonType == ButtonActionType.Action3)
                {
                    OnEquipItem(PostUpdateCommands, entityInQueryIndex, userEquipmentPrefab, userItemEntity, e, controllerEntity, characterEntity, 0,
                        in equipment, in userItemQuantitys, in userItemMetaDatas, in slotLinks, slots, in userEquipmentMetaDatas, in userEquipmentEquipmentParents);
                }
                //! Equip into slot 1
                else if (uiClickEvent.buttonType == ButtonActionType.Action2)
                {
                    OnEquipItem(PostUpdateCommands, entityInQueryIndex, userEquipmentPrefab, userItemEntity, e, controllerEntity, characterEntity, 1,
                        in equipment, in userItemQuantitys, in userItemMetaDatas, in slotLinks, slots, in userEquipmentMetaDatas, in userEquipmentEquipmentParents);
                }
            })  .WithReadOnly(characterLinks)
                .WithReadOnly(inventoryLinks)
                .WithReadOnly(equipments)
                .WithReadOnly(userItemMetaDatas)
                .WithReadOnly(userItemQuantitys)
                .WithReadOnly(itemEquipables)
                .WithReadOnly(slotLinks)
                .WithReadOnly(slots)
                .WithReadOnly(userEquipmentMetaDatas)
                .WithReadOnly(userEquipmentEquipmentParents)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static void OnEquipItem(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity userEquipmentPrefab,
            Entity userItemEntity, Entity itemButtonEntity, Entity controllerEntity,
            Entity characterEntity, byte equipingSide, in Equipment equipment,
            in ComponentLookup<UserItemQuantity> userItemQuantitys, in ComponentLookup<MetaData> userItemMetaDatas,
            in ComponentLookup<SlotLink> slotLinks, in ComponentLookup<Slot> slots,
            in ComponentLookup<MetaData> userEquipmentMetaDatas, in ComponentLookup<EquipmentParent> userEquipmentEquipmentParents)
        {
            var userItemQuantity = userItemQuantitys[userItemEntity];
            var userItemMetaData = userItemMetaDatas[userItemEntity];
            var userMetaEntity = userItemMetaData.data;
            // new item to equip, check if any
            var didEquip = false;
            var equipmentParent = FindNewBodyIndex(in equipment, userMetaEntity, equipingSide, in slotLinks, in slots, in userEquipmentMetaDatas, in userEquipmentEquipmentParents);
            if (equipmentParent.parent.Index > 0)
            {
                didEquip = true;
                AddNewBodyPart(PostUpdateCommands, entityInQueryIndex, userEquipmentPrefab, characterEntity, in equipment, userItemEntity,
                    ref userItemMetaData, ref userItemQuantity, equipmentParent.parent, equipmentParent.parentSlotIndex);
            }
            else
            {
                var userEquipmentEntity = FindReplaceBodyIndex(in equipment, userMetaEntity, equipingSide, in slotLinks, in userEquipmentMetaDatas);
                if (userEquipmentEntity.Index > 0)
                {
                    didEquip = true;
                    SwapBodyParts(PostUpdateCommands, entityInQueryIndex, userEquipmentEntity, userItemEntity, in userItemMetaDatas, in userEquipmentMetaDatas);
                }
            }
            if (didEquip)
            {
                PostUpdateCommands.AddComponent(entityInQueryIndex, itemButtonEntity, new SetIconData(userItemEntity));
                PostUpdateCommands.AddComponent(entityInQueryIndex, itemButtonEntity, new UISelectEvent(controllerEntity));
                PostUpdateCommands.AddComponent<SkeletonDirty>(entityInQueryIndex, characterEntity);
                PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, characterEntity);
                PostUpdateCommands.AddComponent<SaveEquipment>(entityInQueryIndex, characterEntity);
                PostUpdateCommands.AddComponent<SaveInventory>(entityInQueryIndex, characterEntity);
            }
        }

        public static void SwapBodyParts(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity userEquipmentEntity, Entity userItemEntity,
            in ComponentLookup<MetaData> userItemMetaDatas, in ComponentLookup<MetaData> userEquipmentMetaDatas)
        {
            var itemMetaData = userItemMetaDatas[userItemEntity];
            var equipmentMetaData = userEquipmentMetaDatas[userEquipmentEntity];
            PostUpdateCommands.SetComponent(entityInQueryIndex, userEquipmentEntity, itemMetaData);
            PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, equipmentMetaData);
        }

        public static void AddNewBodyPart(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity userEquipmentPrefab, Entity equipmentEntity, in Equipment equipment,
            Entity userItemEntity, ref MetaData userItemMetaData, ref UserItemQuantity userItemQuantity, Entity parentUserEquipmentEntity, byte parentSlotIndex)
        {
            var itemEntity = userItemMetaData.data;
            userItemQuantity.quantity--;
            PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, userItemQuantity);
            if (userItemQuantity.quantity == 0)
            {
                userItemMetaData.data = new Entity();
                PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, userItemMetaData);
            }
            //! Spawns a new UserEquipment entity for our character.
            var userEquipmentEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, userEquipmentPrefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, userEquipmentEntity, new CreatorLink(equipmentEntity));
            PostUpdateCommands.SetComponent(entityInQueryIndex, userEquipmentEntity, new UserDataIndex((byte) equipment.body.Length));
            PostUpdateCommands.SetComponent(entityInQueryIndex, userEquipmentEntity, new MetaData(itemEntity));
            PostUpdateCommands.SetComponent(entityInQueryIndex, userEquipmentEntity, new EquipmentParent(parentUserEquipmentEntity, parentSlotIndex));
            PostUpdateCommands.AddComponent(entityInQueryIndex, equipmentEntity, new OnUserEquipmentSpawned((byte) (equipment.body.Length + 1)));
        }

        // i want an item i can replace
        public static Entity FindReplaceBodyIndex(in Equipment equipment, Entity item, byte slotIndex, in ComponentLookup<SlotLink> slotLinks, in ComponentLookup<MetaData> userEquipmentMetaDatas)
        {
            var baseSlotEntity = slotLinks[item].slot;
            if (baseSlotEntity.Index <= 0)
            {
                // UnityEngine.Debug.LogWarning("SlotEntity not found from item: " + EntityManager.GetComponentData<ZoxName>(item).name.ToString());
                return new Entity();
            }
            var slotIndexCount = 0;
            for (int i = 0; i < equipment.body.Length; i++)
            {
                var userEquipmentEntity = equipment.body[i];
                var itemEntity = userEquipmentMetaDatas[userEquipmentEntity].data;
                if (!slotLinks.HasComponent(itemEntity))
                {
                    continue;
                }
                var slotEntity = slotLinks[itemEntity].slot;
                if (slotEntity == baseSlotEntity)
                {
                    //Debug.LogError("Finding ReplaceBodyIndex: " + thisItem.data.maleSlot.id + ":" + i);
                    if (slotIndex == 255 || slotIndexCount == slotIndex)
                    {
                        return userEquipmentEntity;
                    }
                    slotIndexCount++;
                }
            }
            return new Entity();
        }

        //! Finds a spot to attach new item to body.
        public static EquipmentParent FindNewBodyIndex(in Equipment equipment, Entity itemEntity, byte equipingSide, in ComponentLookup<SlotLink> slotLinks,
            in ComponentLookup<Slot> slots, in ComponentLookup<MetaData> userEquipmentMetaDatas, in ComponentLookup<EquipmentParent> userEquipmentEquipmentParents)
        {
            var attachSlotEntity = slotLinks[itemEntity].slot;
            byte slotConnectCount = 0;
            for (int bodyIndex = equipment.body.Length - 1; bodyIndex >= 0; bodyIndex--)
            {
                var userEquipmentEntity = equipment.body[bodyIndex];
                if (userEquipmentEntity.Index <= 0 || !userEquipmentMetaDatas.HasComponent(userEquipmentEntity))
                {
                    continue;
                }
                var itemEntity2 = userEquipmentMetaDatas[userEquipmentEntity].data;
                if (!slotLinks.HasComponent(itemEntity2))
                {
                    continue;
                }
                var slotEntity = slotLinks[itemEntity2].slot;
                var slot = slots[slotEntity];
                // for all female slots, find an empty one if it is the slot of our male one
                if (slot.femaleSlots.Length > 0)
                {
                    for (byte slotIndex = (byte) (slot.femaleSlots.Length - 1); slotIndex != 255; slotIndex--)
                    {
                        // UnityEngine.Debug.LogError("SlotIndex: " + slotIndex + " : " + bodyIndex);
                        var femaleSlotEntity = slot.femaleSlots[slotIndex];
                        if (attachSlotEntity == femaleSlotEntity)
                        {
                            if (equipingSide != slotConnectCount)
                            {
                                //UnityEngine.Debug.LogError("Skipping Slot Index: " + slotIndex + ", bodyIndex: " + bodyIndex);
                                slotConnectCount++;
                                continue;
                            }
                            //UnityEngine.Debug.LogError("Checking Slot Index: " + slotIndex + ", bodyIndex: " + bodyIndex);
                            slotConnectCount++;
                            // check if slot is the male we need
                            var anythingConnectedToIt = false;
                            // check if anything connected to it
                            for (byte k = 0; k < equipment.body.Length; k++)
                            {
                                if (bodyIndex != k)
                                {
                                    var userEquipmentEntity2 = equipment.body[k];
                                    var equipmentItem2 = userEquipmentEquipmentParents[userEquipmentEntity2];
                                    if (equipmentItem2.parent == userEquipmentEntity && equipmentItem2.parentSlotIndex == slotIndex) //bodyIndex == i && equipmentItem2.slotIndex == j)
                                    {
                                        anythingConnectedToIt = true;
                                        break;
                                    }
                                }
                            }
                            if (!anythingConnectedToIt)
                            {
                                // return the index of body part to connect to, and the slot index (female) it will connect to, say Chest, and Head slot
                                // UnityEngine.Debug.LogError("Found Equipment Parent: " + userEquipmentEntity.Index + " :: " + slotIndex);
                                return new EquipmentParent(userEquipmentEntity, slotIndex);
                            }
                        }
                    }
                }
            }
            return new EquipmentParent();
        }
    }
}