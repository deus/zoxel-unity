using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.UI.MouseFollowing;

namespace Zoxel.Items.UI
{
    //! Handles UIClickEvent for ItemButton's.
    /**
    *   - UIClickEvent System -
    *   \todo When switching items or anything, create events for that.
    *   \todo When switching items, switch cooldowns too or any other user item data.
    */
    [BurstCompile, UpdateInGroup(typeof(ItemUISystemGroup))]
    public partial class ItemUIClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery controllersQuery;
        private EntityQuery panelUIsQuery;
        private EntityQuery userItemsQuery;
        private EntityQuery mouseUIsQuery;
        private EntityQuery saverQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Player>(),
                ComponentType.ReadOnly<CharacterLink>());
            panelUIsQuery = GetEntityQuery(ComponentType.ReadOnly<InventoryLink>());
            userItemsQuery = GetEntityQuery(ComponentType.ReadOnly<UserItem>());
            mouseUIsQuery = GetEntityQuery(ComponentType.ReadOnly<MouseUI>());
            saverQuery = GetEntityQuery(
                ComponentType.ReadOnly<Inventory>(),
                ComponentType.ReadOnly<Saver>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var userEquipmentPrefab = ItemSystemGroup.userEquipmentPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var characterLinks = GetComponentLookup<CharacterLink>(true);
            var mouseUILinks = GetComponentLookup<MouseUILink>(true);
            var cameraLinks = GetComponentLookup<CameraLink>(true);
            controllerEntities.Dispose();
            var panelEntities = panelUIsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var inventoryLinks = GetComponentLookup<InventoryLink>(true);
            var uiPositions = GetComponentLookup<UIPosition>(true);
            panelEntities.Dispose();
            var userItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var userItemMetaDatas = GetComponentLookup<MetaData>(true);
            var userItemQuantitys = GetComponentLookup<UserItemQuantity>(true);
            userItemEntities.Dispose();
            var mouseUIEntities = mouseUIsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleE);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleE);
            var mouseUserItems = GetComponentLookup<MouseUserItem>(true);
            mouseUIEntities.Dispose();
            var saverEntities = mouseUIsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleG);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleG);
            var savers = GetComponentLookup<Saver>(true);
            saverEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<TradeUIButton>()
                .WithAll<ItemButton>()
                .ForEach((Entity e, int entityInQueryIndex, in IconData iconData, in ParentLink parentLink, in UIClickEvent uiClickEvent) =>
            {
                var controllerEntity = uiClickEvent.controller;
                var characterEntity = characterLinks[controllerEntity].character;
                var panelEntity = parentLink.parent;
                var uiDepth = 0f;
                if (uiPositions.HasComponent(panelEntity))
                {
                    uiDepth = uiPositions[panelEntity].GetDepth();
                }
                Entity inventoryEntity;
                if (inventoryLinks.HasComponent(panelEntity))
                {
                    inventoryEntity = inventoryLinks[panelEntity].inventory;
                }
                else
                {
                    inventoryEntity = characterEntity;
                }
                var userItemEntity = iconData.data;
                if (!userItemMetaDatas.HasComponent(userItemEntity))
                {
                    return;
                }
                var userItemMetaData = userItemMetaDatas[userItemEntity];
                var itemEntity = userItemMetaData.data;
                var isClickEquipment = HasComponent<ItemEquipable>(itemEntity);
                var mouseUIEntity = mouseUILinks[controllerEntity].mouseUI;
                var isFollowingMouse = HasComponent<MouseUserItem>(mouseUIEntity);
                var cameraEntity = cameraLinks[controllerEntity].camera;
                if (uiClickEvent.buttonType == ButtonActionType.Confirm)
                {
                    if (!isFollowingMouse)
                    {
                        PickupUserItem(PostUpdateCommands, entityInQueryIndex, panelEntity, e, controllerEntity, characterEntity, userItemEntity, inventoryEntity,
                            mouseUIEntity, 0, cameraEntity, uiDepth, in userItemQuantitys, in userItemMetaDatas);
                    }
                    else
                    {
                        if (HasComponent<DisableItemUIPlacement>(e))
                        {
                            return;
                        }
                        var mouseMetaData = userItemMetaDatas[mouseUIEntity];
                        var mouseMetaEntity = mouseMetaData.data;
                        var isMouseEquipment = HasComponent<ItemEquipable>(mouseMetaEntity);
                        PlaceUserItem(PostUpdateCommands, entityInQueryIndex, panelEntity, e, controllerEntity, characterEntity, userItemEntity, inventoryEntity, mouseUIEntity,
                            in userItemQuantitys, in userItemMetaDatas, in mouseUserItems, isMouseEquipment, in savers);
                    }
                    return;
                }
                //! Other buttons only work for non equipment
                if (isClickEquipment)
                {
                    return;
                }
                if (uiClickEvent.buttonType == ButtonActionType.Action3)
                {
                    // UnityEngine.Debug.LogError("ButtonLT on ItemUI!");
                    if (!isFollowingMouse)
                    {
                        PickupUserItem(PostUpdateCommands, entityInQueryIndex, panelEntity, e, controllerEntity, characterEntity, userItemEntity, inventoryEntity,
                            mouseUIEntity, 1, cameraEntity, uiDepth, in userItemQuantitys, in userItemMetaDatas);
                    }
                    else
                    {
                        if (HasComponent<DisableItemUIPlacement>(e))
                        {
                            return;
                        }
                        PlaceSingleUserItem(PostUpdateCommands, entityInQueryIndex, panelEntity, e, controllerEntity, characterEntity, userItemEntity, inventoryEntity, mouseUIEntity,
                            in userItemQuantitys, in userItemMetaDatas, in mouseUserItems, in savers);
                    }
                }
                else if (uiClickEvent.buttonType == ButtonActionType.Action2)
                {
                    AssignUserItem(PostUpdateCommands, entityInQueryIndex, panelEntity, e, controllerEntity, characterEntity, mouseUIEntity, userItemEntity, uiClickEvent.deviceType);
                }
                /*else if (uiClickEvent.buttonTypeuiClickEvent.buttonType == ButtonActionType.Cancel)
                {
                    // cancel whatever action was doing
                }*/
            })  .WithReadOnly(characterLinks)
                .WithReadOnly(mouseUILinks)
                .WithReadOnly(cameraLinks)
                .WithReadOnly(uiPositions)
                .WithReadOnly(inventoryLinks)
                .WithReadOnly(userItemMetaDatas)
                .WithReadOnly(userItemQuantitys)
                .WithReadOnly(mouseUserItems)
                .WithReadOnly(savers)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        //! \todo Fix gamepad assigning, including tooltip.
        private static void AssignUserItem(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex,
            Entity panelEntity, Entity buttonEntity, Entity controllerEntity, Entity characterEntity, Entity mouseUIEntity, Entity userItemEntity, byte deviceType)
        {
            PostUpdateCommands.SetComponent(entityInQueryIndex, mouseUIEntity, new MouseUI(characterEntity, panelEntity, buttonEntity, userItemEntity));
            if (deviceType == DeviceType.Gamepad)
            {
                /*var uiLink = EntityManager.GetComponentData<UILink>(characterEntity);
                var actionbar = uiLink.GetUI<Actionbar>(EntityManager);
                if (actionbar.Index != 0)
                {
                    PostUpdateCommands.AddComponent<NavigationDirty>(entityInQueryIndex, actionbar);
                }*/
            }
        }

        //! \todo When pickup and put down - update action connected to that UserItemQuantity.
        private static void PickupUserItem(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex,
            Entity panelEntity, Entity itemButtonEntity, Entity controllerEntity, Entity characterEntity, Entity userItemEntity,
            Entity inventoryEntity, Entity mouseUIEntity, byte clickType, Entity cameraEntity, float uiDepth,
            in ComponentLookup<UserItemQuantity> userItemQuantitys, in ComponentLookup<MetaData> userItemMetaDatas)
        {
            var userItemQuantity = userItemQuantitys[userItemEntity];
            var userItemMetaData = userItemMetaDatas[userItemEntity];
            var itemEntity = userItemMetaData.data;
            if (itemEntity.Index == 0 || userItemQuantity.quantity == 0)
            {
                return;
            }
            var mouseUserItem = new UserItemQuantity();
            var mouseUserMetaData = new MetaData();
            // if right clicks - grab half item stack
            if (clickType == 1 && userItemQuantity.quantity > 1)
            {
                var quantityToPickup = (byte) (userItemQuantity.quantity / 2);
                userItemQuantity.quantity -= quantityToPickup;
                mouseUserMetaData.data = itemEntity;
                mouseUserItem.quantity = quantityToPickup;
            }
            // if grab whole item
            else
            {
                mouseUserMetaData.data = userItemMetaData.data;
                mouseUserItem = userItemQuantity;
                userItemQuantity.quantity = 0;
                userItemMetaData.data = new Entity();
                // UnityEngine.Debug.LogError("PickupUserItem quantity: " + userItemQuantity.quantity + ", " + inventory.items[selectedIndex].quantity);
            }
            // UnityEngine.Debug.LogError("mouseUserItem voxelType: " + ((int)mouseUserItem.data.voxelType));
            // Mouse UI
            PostUpdateCommands.AddComponent(entityInQueryIndex, mouseUIEntity, new MouseUserItem(inventoryEntity, userItemEntity));
            PostUpdateCommands.AddComponent(entityInQueryIndex, mouseUIEntity, mouseUserItem);
            PostUpdateCommands.AddComponent<UserItem>(entityInQueryIndex, mouseUIEntity);
            PostUpdateCommands.SetComponent(entityInQueryIndex, mouseUIEntity, mouseUserMetaData);
            PostUpdateCommands.AddComponent(entityInQueryIndex, mouseUIEntity, new SetIconData(mouseUIEntity));
            PostUpdateCommands.SetComponent(entityInQueryIndex, mouseUIEntity, new CharacterLink(controllerEntity));
            PostUpdateCommands.SetComponent(entityInQueryIndex, mouseUIEntity, new MouseUI(inventoryEntity, panelEntity, itemButtonEntity, userItemEntity));
            PostUpdateCommands.AddComponent(entityInQueryIndex, mouseUIEntity, new FollowingMouse(cameraEntity, uiDepth));
            // User Item
            PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, userItemQuantity);
            PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, userItemMetaData);
            // Item UI
            PostUpdateCommands.AddComponent(entityInQueryIndex, itemButtonEntity, new SetIconData(userItemEntity));
            PostUpdateCommands.AddComponent(entityInQueryIndex, itemButtonEntity, new DelayEventFrames(2));
            PostUpdateCommands.AddComponent(entityInQueryIndex, itemButtonEntity, new UISelectEvent(controllerEntity));
            // If picked up entire user item
            if (userItemMetaData.data.Index == 0)
            {
                PostUpdateCommands.AddComponent(entityInQueryIndex, characterEntity, new OnUserItemPickedUp(userItemEntity, itemEntity));
            }
        }

        private static void PlaceSingleUserItem(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex,
            Entity panelEntity, Entity itemButtonEntity, Entity controllerEntity, Entity characterEntity, Entity userItemEntity, Entity inventoryEntity,
            Entity mouseUIEntity,
            in ComponentLookup<UserItemQuantity> userItemQuantitys, in ComponentLookup<MetaData> userItemMetaDatas,
            in ComponentLookup<MouseUserItem> mouseUserItems, in ComponentLookup<Saver> savers)
        {
            var userItemQuantity = userItemQuantitys[userItemEntity];
            var userItemMetaData = userItemMetaDatas[userItemEntity];
            var itemEntity = userItemMetaData.data;
            var mouseUserItem2 = mouseUserItems[mouseUIEntity];
            var mouseUserItem = userItemQuantitys[mouseUIEntity];
            var mouseMetaData = userItemMetaDatas[mouseUIEntity];
            var mouseItemEntity = mouseMetaData.data;
            if (userItemQuantity.quantity != 0)
            {
                // if not same item, return
                if (itemEntity != mouseItemEntity)
                {
                    return;
                }
                // Stack item
                else
                {
                    userItemQuantity.quantity++;
                }
            }
            else
            {
                userItemMetaData.data = mouseItemEntity;
                userItemQuantity.quantity = 1;
            }
            mouseUserItem.quantity--;
            PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, userItemQuantity);
            PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, userItemMetaData);
            PostUpdateCommands.SetComponent(entityInQueryIndex, mouseUIEntity, new MouseUserItem(inventoryEntity, mouseUserItem2.userItemEntity));
            PostUpdateCommands.SetComponent(entityInQueryIndex, mouseUIEntity, mouseUserItem);
            PostUpdateCommands.AddComponent(entityInQueryIndex, itemButtonEntity, new SetIconData(userItemEntity));
            // remove follow ui
            if (mouseUserItem.quantity == 0)
            {
                PostUpdateCommands.AddComponent<HideMouseUI>(entityInQueryIndex, mouseUIEntity);
            }
            // Set Mouse UI Icon
            else
            {
                PostUpdateCommands.AddComponent(entityInQueryIndex, mouseUIEntity, new SetIconData(mouseUIEntity));
            }
            PostUpdateCommands.AddComponent(entityInQueryIndex, itemButtonEntity, new UISelectEvent(controllerEntity));
            PostUpdateCommands.AddComponent(entityInQueryIndex, characterEntity, new OnUserItemPlaced(userItemEntity, mouseItemEntity));
            if (savers.HasComponent(inventoryEntity))
            {
                PostUpdateCommands.AddComponent<SaveInventory>(entityInQueryIndex, inventoryEntity);
            }
        }

        private static void PlaceUserItem(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex,
            Entity panelEntity, Entity itemButtonEntity, Entity controllerEntity, Entity characterEntity,
            Entity userItemEntity, Entity inventoryEntity, Entity mouseUIEntity,
            in ComponentLookup<UserItemQuantity> userItemQuantitys, in ComponentLookup<MetaData> userItemMetaDatas,
            in ComponentLookup<MouseUserItem> mouseUserItems, bool isMouseEquipment, in ComponentLookup<Saver> savers)
        {
            var userItemQuantity = userItemQuantitys[userItemEntity];
            var userItemMetaData = userItemMetaDatas[userItemEntity];
            var itemEntity = userItemMetaData.data;
            var mouseUserItem2 = mouseUserItems[mouseUIEntity];
            var mouseUserItem = userItemQuantitys[mouseUIEntity];
            var mouseMetaData = userItemMetaDatas[mouseUIEntity];
            var mouseMetaEntity = mouseMetaData.data;
            // actually, for this item, grab it from the mouse, drop into an empty slot
            var didDropOffItem = false;
            if (itemEntity.Index == 0)
            {
                userItemQuantity = mouseUserItem;
                userItemMetaData.data = mouseMetaEntity;
                mouseUserItem = new UserItemQuantity();
                mouseMetaData.data = new Entity();
                PostUpdateCommands.AddComponent<HideMouseUI>(entityInQueryIndex, mouseUIEntity);
                didDropOffItem = true;
            }
            else
            {
                // Can Stack?
                var maxQuantity = 0;
                if (isMouseEquipment)
                {
                    maxQuantity = 1;
                }
                var canStack = itemEntity == mouseMetaEntity && (maxQuantity == 0 || userItemQuantity.quantity < maxQuantity);
                //! Stacks the UserItemQuantity using Quantity property.
                if (canStack)
                {
                    userItemQuantity.quantity += mouseUserItem.quantity;
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, userItemQuantity);
                    mouseUserItem = new UserItemQuantity();
                    PostUpdateCommands.AddComponent(entityInQueryIndex, mouseUIEntity, new DelayEventFrames(1));
                    PostUpdateCommands.AddComponent<HideMouseUI>(entityInQueryIndex, mouseUIEntity);
                    didDropOffItem = true;
                }
                //! Swaps the two user items, from mouseUI to the itemUI.
                else
                {
                    var tempItem = mouseUserItem;
                    mouseUserItem = userItemQuantity;
                    userItemQuantity = tempItem;
                    var tempMetaData = mouseMetaData;
                    mouseMetaData = userItemMetaData;
                    userItemMetaData = tempMetaData;
                    PostUpdateCommands.AddComponent(entityInQueryIndex, mouseUIEntity, new SetIconData(mouseUIEntity));
                }
            }
            // Inventory ItemButton
            PostUpdateCommands.AddComponent(entityInQueryIndex, itemButtonEntity, new SetIconData(userItemEntity));
            PostUpdateCommands.AddComponent(entityInQueryIndex, itemButtonEntity, new UISelectEvent(controllerEntity));
            // UserItemQuantity
            PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, userItemQuantity);
            PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, userItemMetaData);
            // Mouse UI
            PostUpdateCommands.SetComponent(entityInQueryIndex, mouseUIEntity, new MouseUserItem(inventoryEntity, userItemEntity));
            PostUpdateCommands.SetComponent(entityInQueryIndex, mouseUIEntity, mouseUserItem);
            PostUpdateCommands.SetComponent(entityInQueryIndex, mouseUIEntity, mouseMetaData);
            if (didDropOffItem)
            {
                PostUpdateCommands.AddComponent(entityInQueryIndex, characterEntity, new OnUserItemPlaced(userItemEntity, mouseMetaEntity));
            }
            else
            {
                PostUpdateCommands.AddComponent(entityInQueryIndex, characterEntity, new OnUserItemSwapped(mouseUserItem2.userItemEntity, mouseMetaEntity, userItemEntity, itemEntity));
            }
            // save inventory
            // Could also be chest, need generic Saver component, crafting ui won't save items but spit them out
            /*if (HasComponent<Saver>(inventoryEntity))
            {
                
            }*/
            if (savers.HasComponent(inventoryEntity))
            {
                PostUpdateCommands.AddComponent<SaveInventory>(entityInQueryIndex, inventoryEntity);
            }
            // Save first one
            if (inventoryEntity != mouseUserItem2.inventoryEntity)
            {
                if (savers.HasComponent(mouseUserItem2.inventoryEntity))
                {
                    PostUpdateCommands.AddComponent<SaveInventory>(entityInQueryIndex, mouseUserItem2.inventoryEntity);
                }
                /*if (HasComponent<Saver>(inventoryEntity))
                {

                }*/
            }
        }
    }
}