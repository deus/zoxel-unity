﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Audio;
using Zoxel.Transforms;

namespace Zoxel.Items.UI
{
    //! Spawns a ItemUI connected to a user.
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(ItemUISystemGroup))]
    public partial class ItemUISpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity inventoryUIPrefab;
        public static IconPrefabs iconPrefabs;
        public static Entity spawnInventoryUIPrefab;
        public static Entity spawnTradeUIPrefab;
        public static Entity spawnChestUIPrefab;
        private EntityQuery processQuery;
        private EntityQuery inventoriesQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnInventoryUIArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnInventoryUI),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnInventoryUIPrefab = EntityManager.CreateEntity(spawnInventoryUIArchetype);
            spawnTradeUIPrefab = EntityManager.CreateEntity(spawnInventoryUIArchetype);
            EntityManager.AddComponent<SpawnTradeUI>(spawnTradeUIPrefab);
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DelayEvent>(),
                ComponentType.ReadOnly<SpawnInventoryUI>(),
                ComponentType.ReadOnly<CharacterLink>());
            inventoriesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Inventory>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        public static void InitializePrefabs(EntityManager EntityManager)
        {
            if (ItemUISpawnSystem.inventoryUIPrefab.Index != 0)
            {
                return;
            }
            var inventoryUIPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            ItemUISpawnSystem.inventoryUIPrefab = inventoryUIPrefab;
            EntityManager.AddComponent<Prefab>(inventoryUIPrefab);
            EntityManager.AddComponent<InventoryUI>(inventoryUIPrefab);
            EntityManager.SetComponentData(inventoryUIPrefab, new PanelUI(PanelType.InventoryUI));
            iconPrefabs.frame = EntityManager.Instantiate(UICoreSystem.iconPrefabs.frame);
            EntityManager.AddComponent<Prefab>(iconPrefabs.frame);
            EntityManager.AddComponent<ItemButton>(iconPrefabs.frame);
            iconPrefabs.icon = EntityManager.Instantiate(UICoreSystem.iconPrefabs.icon);
            EntityManager.AddComponent<Prefab>(iconPrefabs.icon);
            EntityManager.AddComponent<ItemIcon>(iconPrefabs.icon);
            iconPrefabs.label = UICoreSystem.iconPrefabs.label;
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs(EntityManager);
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var iconStyle = uiDatam.iconStyle.GetStyle(uiScale);
            var iconSize = uiDatam.GetIconSize(uiScale);
            var padding = uiDatam.GetIconPadding(uiScale); // iconSize / 8f;
            var margins = uiDatam.GetIconMargins(uiScale); // iconSize / 6f;
            var elapsedTime = World.Time.ElapsedTime;
            var inventoryUIPrefab = ItemUISpawnSystem.inventoryUIPrefab;
            var iconPrefabs = ItemUISpawnSystem.iconPrefabs;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var spawnEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var characterLinks = GetComponentLookup<CharacterLink>(true);
			var spawnTradeUIs = GetComponentLookup<SpawnTradeUI>(true);
			var inventoryLinks = GetComponentLookup<InventoryLink>(true);
            var prefabLinks = GetComponentLookup<PrefabLink>(true);
            var uiAnchors = GetComponentLookup<UIAnchor>(true);
            var inventoryEntities = inventoriesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var inventories = GetComponentLookup<Inventory>(true);
            inventoryEntities.Dispose();
            Dependency = Entities
                .WithAll<UILink>()
                .ForEach((Entity e, int entityInQueryIndex, in CameraLink cameraLink) =>
            {
                var spawnEntity = new Entity();
                for (int i = 0; i < spawnEntities.Length; i++)
                {
                    var e2 = spawnEntities[i];
                    var characterLink = characterLinks[e2];
                    if (characterLink.character == e)
                    {
                        spawnEntity = e2;
                        break;  // only one UI per character atm
                    }
                }
                if (spawnEntity.Index == 0)
                {
                    return;
                }
                var prefab = inventoryUIPrefab;
                if (prefabLinks.HasComponent(spawnEntity))
                {
                    prefab = prefabLinks[spawnEntity].prefab;
                }
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CharacterLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLink);
                if (uiAnchors.HasComponent(spawnEntity))
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, uiAnchors[spawnEntity]);
                }
                // spawnedUI event
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));
                // set up rest
                // var itemUICount = (byte) 0;
                var maxUIs = (byte) 0;
                Inventory inventory;
                var isShop = spawnTradeUIs.HasComponent(spawnEntity);
                if (isShop)
                {
                    var spawnTradeUI = spawnTradeUIs[spawnEntity];
                    PostUpdateCommands.AddComponent<TradeUI>(entityInQueryIndex, e3);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new PanelUI(PanelType.TradeUI));
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e3, new InventoryLink(spawnTradeUI.characterSelling));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new GridUI(new int2(4, 1), padding, margins));
                    inventory = inventories[spawnTradeUI.characterSelling];
                    maxUIs = (byte) math.min(inventory.items.Length, 4);
                }
                else if (inventoryLinks.HasComponent(spawnEntity))
                {
                    var inventoryEntity = inventoryLinks[spawnEntity].inventory;
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e3, new InventoryLink(inventoryEntity));
                    inventory = inventories[inventoryEntity];
                    maxUIs = (byte) math.min(inventory.items.Length, 16);
                }
                else
                {
                    inventory = inventories[e];
                    maxUIs = (byte) math.min(inventory.items.Length, 32);
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawned(maxUIs));
                // spawn children
                for (byte i = 0; i < maxUIs; i++)
                {
                    var userItemEntity = inventory.items[i];
                    var frameEntity = SpawnItemUI(PostUpdateCommands, entityInQueryIndex, elapsedTime, i, iconPrefabs, e3, userItemEntity, in iconStyle);
                    if (isShop)
                    {
                        PostUpdateCommands.AddComponent<TradeUIButton>(entityInQueryIndex, frameEntity);
                        // UnityEngine.Debug.LogError("User Item at " + i + " is " + itemEntity.Index);
                    }
                }
            })  .WithReadOnly(spawnEntities)
                .WithDisposeOnCompletion(spawnEntities)
                .WithReadOnly(characterLinks)
                .WithReadOnly(spawnTradeUIs)
                .WithReadOnly(inventoryLinks)
                .WithReadOnly(prefabLinks)
                .WithReadOnly(uiAnchors)
                .WithReadOnly(inventories)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static Entity SpawnItemUI(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, double elapsedTime, byte index, IconPrefabs iconPrefabs,
            Entity panelEntity, Entity userItemEntity, in IconStyle iconStyle)
        {
            var seed = (int) (128 + elapsedTime + index * 2048 + 4196 * entityInQueryIndex);
            var frameEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.frame, panelEntity);
            UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, frameEntity, panelEntity);
            UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, frameEntity, index);
            UICoreSystem.SetSound(PostUpdateCommands, entityInQueryIndex, frameEntity, seed, iconStyle.soundVolume);
            UICoreSystem.SetSeed(PostUpdateCommands, entityInQueryIndex, frameEntity, seed);
            UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.icon, frameEntity);
            UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, iconPrefabs.label, frameEntity);
            if (userItemEntity.Index != 0)
            {
                PostUpdateCommands.SetComponent(entityInQueryIndex, frameEntity, new SetIconData(userItemEntity));
            }
            return frameEntity;
        }
    }
}