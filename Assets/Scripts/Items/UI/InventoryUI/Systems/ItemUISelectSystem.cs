using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Voxels;

namespace Zoxel.Items.UI
{
    //! Handles ButtonSelectEvents for ItemButton's.
    /**
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(ItemUISystemGroup))]
    public partial class ItemUISelectSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<ItemButton>()
                .ForEach((Entity e, in IconData iconData, in Child child, in PanelLink panelLink, in UISelectEvent uiSelectEvent) =>
            {
                var panelEntity = panelLink.panel;
                if (!HasComponent<UIElement>(panelEntity))
                {
                    return;
                }
                var arrayIndex = child.index;
                var controllerEntity = uiSelectEvent.character;
                var isShop = HasComponent<TradeUI>(panelEntity);
                var userItemEntity = iconData.data;
                if (!HasComponent<UserItem>(userItemEntity))
                {
                    // UnityEngine.Debug.LogError("Selected null UserItem.");
                    return;
                }
                var userItem = EntityManager.GetComponentData<UserItemQuantity>(userItemEntity);
                var metaData = EntityManager.GetComponentData<MetaData>(userItemEntity);
                var itemEntity = metaData.data;
                if (!HasComponent<Item>(itemEntity))
                {
                    // UnityEngine.Debug.LogError("Selected null Item.");
                    PostUpdateCommands.AddComponent(panelEntity, new SetPanelTooltip(new Text()));
                    PostUpdateCommands.AddComponent(controllerEntity, new SetPlayerTooltip(new Text()));
                    return;
                }
                var itemValue = 0;
                var itemName = new Text();
                if (itemEntity.Index != 0)
                {
                    itemName = EntityManager.GetComponentData<ZoxName>(itemEntity).name;
                    itemValue = EntityManager.GetComponentData<ItemValue>(itemEntity).value;
                }
                var panelTooltip = itemName.ToString(); // .Replace(" Item", "");
                var tooltipText = "";
                if (itemEntity.Index == 0)
                {
                    // nothing
                    // panelTooltip += "Nothing";
                }
                else if (!isShop)
                {
                    var isEquipItem = HasComponent<ItemEquipable>(itemEntity);
                    panelTooltip += " x" + userItem.quantity;
                    if (isEquipItem)
                    {
                        // tooltipText = "[action1] to Pickup Item\n[action2] to Equip Item to Slot 1\n[action3] to Equip Item to Slot 2";
                        //tooltipText = "[action1] to Pickup " + itemName.ToString() + "\n";
                        //tooltipText += "[action2] to Equip Item to Slot 1\n[action3] to Equip Item to Slot 2";
                        // panelTooltip += "\nEquipment";
                        var slotEntity = EntityManager.GetComponentData<SlotLink>(itemEntity).slot;
                        if (HasComponent<ZoxName>(slotEntity))
                        {
                            panelTooltip += "\nEquip to " + EntityManager.GetComponentData<ZoxName>(slotEntity).name.ToString();
                        }
                        else
                        {
                            panelTooltip += "\nEquip to Error";
                        }
                    }
                    else if (itemEntity.Index != 0)
                    {
                        // tooltipText = "[action1] to Pickup " + itemName.ToString();
                        if (HasComponent<ConsumableItem>(itemEntity))
                        {
                            panelTooltip += "\nConsumable";
                            //! \todo Add known effects to items. If you eat it, it reveals the effect. Some mushrooms can be poisin.
                            panelTooltip += "\nHeals 3 Health.";
                        }
                        else if (HasComponent<VoxelItem>(itemEntity))
                        {
                            var voxelEntity = EntityManager.GetComponentData<VoxelItem>(itemEntity).voxel;
                            var voxel = EntityManager.GetComponentData<Voxel>(voxelEntity);
                            panelTooltip += "\nVoxel " + ((VoxelType)(voxel.type));
                        }
                        else if (HasComponent<ItemCurrency>(itemEntity))
                        {
                            panelTooltip += "\nCurrency";
                        }
                    }
                }
                else
                {
                    if (itemName.Length != 0)
                    {
                        panelTooltip += " x" + userItem.quantity;
                        if (itemValue != 0)
                        {
                            panelTooltip += "\nWorth " + itemValue + " Coin";
                            if (itemValue > 1)
                            {
                                panelTooltip += "s";
                            }
                            panelTooltip += ".";
                            tooltipText = "[action1] to Purchase Item."; //  for " + itemValue + " Coin"; //  + ((int)itemSelected.voxelType);
                        }
                        else
                        {
                            panelTooltip += "\nFree Stuff";
                        }
                    }
                }
                PostUpdateCommands.AddComponent(panelEntity, new SetPanelTooltip(new Text(panelTooltip)));
                PostUpdateCommands.AddComponent(controllerEntity, new SetPlayerTooltip(new Text(tooltipText)));
            }).WithoutBurst().Run();
        }
    }
}

/* Entity inventoryEntity;
if (HasComponent<InventoryLink>(panelEntity))
{
    inventoryEntity = EntityManager.GetComponentData<InventoryLink>(panelEntity).inventoryEntity;
}
else
{
    inventoryEntity = EntityManager.GetComponentData<CharacterLink>(controllerEntity).character;
}
var inventory = EntityManager.GetComponentData<Inventory>(inventoryEntity);
var inventoryItemSelectedEntity = inventory.items[arrayIndex];*/
//var tooltipTextLinks = EntityManager.GetComponentData<PlayerTooltipLinks>(controllerEntity);
//tooltipTextLinks.SetTooltipText(PostUpdateCommands, controllerEntity, tooltipText);
/*if (HasComponent<PanelTooltipLink>(panelEntity))
{
    var tooltip = EntityManager.GetComponentData<PanelTooltipLink>(panelEntity).tooltip;
    if (HasComponent<RenderText>(tooltip))
    {
        PostUpdateCommands.AddComponent(tooltip, new SetTooltipText(new Text(panelTooltip)));
    }
}*/