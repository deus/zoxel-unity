using Unity.Entities;

namespace Zoxel.Items.Input
{
    //! Using userActionLinks, drops the item selected.
    public struct DropHeldItem : IComponentData { }
}