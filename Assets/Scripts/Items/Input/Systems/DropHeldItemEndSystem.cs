using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Items.Input
{
	//! Removes DropHeldItem from Entity after use.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class DropHeldItemEndSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<DropHeldItem>());
            RequireForUpdate(processQuery);
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
			SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<DropHeldItem>(processQuery);
		}
	}
}