using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Actions;
using Zoxel.Items.Voxels;

namespace Zoxel.Items.Input
{
    //! Drops an item that user has selected.
    /**
    *   Drops sick beats too.
    */
    [BurstCompile, UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class DropHeldItemSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userItemsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			userItemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserItem>(),
                ComponentType.ReadWrite<UserItemQuantity>(),
                ComponentType.ReadWrite<MetaData>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var isDropAll = false;
            var spawnItemPrefab = WorldItemSpawnSystem.spawnItemPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var inventoryItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var userItemQuantities = GetComponentLookup<UserItemQuantity>(false);
			var userMetaDatas = GetComponentLookup<MetaData>(true);
            inventoryItemEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<DropHeldItem>()
                .ForEach((Entity e, int entityInQueryIndex, ref UserActionLinks userActionLinks, in Inventory inventory, in VoxLink voxLink, in Translation translation, in Rotation rotation) =>
            {
                // for held item, remove from inventory
                // create pickup item for it
                // add cooldown there for pickup to item
                // Play sound for dropping item
                // Animate item that gets thrown
                // UnityEngine.Debug.LogError("Todo. Drop an item.");
                var selectedTarget = userActionLinks.GetSelectedTarget();
                if (selectedTarget.Index == 0 || !userItemQuantities.HasComponent(selectedTarget))
                {
                    return;
                }
                // var userItemEntity = inventory.items[i];
                var userMetaEntity = userMetaDatas[selectedTarget].data;
                if (userMetaEntity.Index == 0)
                {
                    return;
                }
                var dropCount = (byte) 0;
                if (isDropAll)
                {
                    for (int i = 0; i < inventory.items.Length; i++)
                    {
                        var userItemEntity = inventory.items[i];
                        if (userItemEntity.Index == 0)
                        {
                            continue;
                        }
                        var itemEntity = userMetaDatas[userItemEntity].data;
                        if (itemEntity == userMetaEntity)
                        {
                            var userItemQuantity = userItemQuantities[userItemEntity];
                            dropCount += userItemQuantity.quantity;
                        }
                    }
                }
                else
                {
                    dropCount = 1;
                }
                var spawnPosition = translation.Value + math.mul(rotation.Value.value, new float3(0, 0, 1f));
                PostUpdateCommands.SetComponent(entityInQueryIndex,
                    PostUpdateCommands.Instantiate(entityInQueryIndex, spawnItemPrefab),
                    new SpawnWorldItem
                    {
                        item = userMetaEntity,
                        quantity = dropCount,
                        spawnPosition = spawnPosition,
                        spawnRotation = rotation.Value,
                        vox = voxLink.vox,
                        character = e
                    });
                var isZeroQuantity = (byte) 0;
                if (isDropAll)
                {
                    isZeroQuantity = 1;
                    for (int i = 0; i < inventory.items.Length; i++)
                    {
                        var userItemEntity = inventory.items[i];
                        if (userItemEntity.Index == 0)
                        {
                            continue;
                        }
                        var itemEntity = userMetaDatas[userItemEntity].data;
                        if (itemEntity == userMetaEntity)
                        {
                            // set to null
                            PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, new MetaData());
                            PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, new UserItemQuantity());
                        }
                    }
                }
                else
                {
                    var userItemQuantity = userItemQuantities[selectedTarget];
                    userItemQuantity.quantity--;
                    if (userItemQuantity.quantity == 0)
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, selectedTarget, new MetaData());
                        PostUpdateCommands.SetComponent(entityInQueryIndex, selectedTarget, new UserItemQuantity());
                        // is any left?
                        var newUserItemEntity = new Entity();
                        for (int i = 0; i < inventory.items.Length; i++)
                        {
                            var userItemEntity = inventory.items[i];
                            if (userItemEntity.Index == 0 || userItemEntity == selectedTarget)
                            {
                                continue;
                            }
                            var itemEntity = userMetaDatas[userItemEntity].data;
                            if (itemEntity == userMetaEntity)
                            {
                                var userItemQuantity2 = userItemQuantities[userItemEntity];
                                if (userItemQuantity2.quantity > 0)
                                {
                                    newUserItemEntity = userItemEntity;
                                    break;
                                }
                            }
                        }
                        if (newUserItemEntity.Index != 0)
                        {
                            // Set action meta to new one
                            for (int i = 0; i < userActionLinks.actions.Length; i++)
                            {
                                var action = userActionLinks.actions[i];
                                if (action.targetMeta == userMetaEntity)
                                {
                                    action.target = newUserItemEntity;
                                    userActionLinks.actions[i] = action;
                                }
                            }
                        }
                        else
                        {
                            // zerod action, refreshes
                            isZeroQuantity = (byte) 1;
                        }
                    }
                    else
                    {
                        userItemQuantities[selectedTarget] = userItemQuantity;
                    }
                }
                var actionUpdated = new Action();
                for (int i = 0; i < userActionLinks.actions.Length; i++)
                {
                    var action = userActionLinks.actions[i];
                    if (action.targetMeta == userMetaEntity)
                    {
                        actionUpdated = action;
                        break;
                        // userActionLinks.actions[i] = new Action();
                    }
                }
                if (actionUpdated.target.Index > 0)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ActionTargetUpdated(actionUpdated.target, actionUpdated.targetMeta, isZeroQuantity));
                    PostUpdateCommands.AddComponent<SaveActions>(entityInQueryIndex, e);
                }
                PostUpdateCommands.AddComponent<SaveInventory>(entityInQueryIndex, e);
			})  .WithNativeDisableContainerSafetyRestriction(userItemQuantities)
                .WithReadOnly(userMetaDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}