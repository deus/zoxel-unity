using Unity.Entities;
using Unity.Burst;
using Zoxel.Input;
using Zoxel.Players;

namespace Zoxel.Items.Input
{
    //! Drops an item when pressing the z key.
    /**
    *   - Input Trigger System -
    */
    [BurstCompile, UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class ItemDropInputSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<ControllerDisabled>()
                .ForEach((int entityInQueryIndex, in Controller controller, in CharacterLink characterLink) =>
            {
                if (controller.mapping != (ControllerMapping.InGame))
                {
                    return;
                }
                if (controller.keyboard.zKey.wasPressedThisFrame == 1 && characterLink.character.Index > 0)
                {
                    PostUpdateCommands.AddComponent<DropHeldItem>(entityInQueryIndex, characterLink.character);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}