﻿using Unity.Entities;

namespace Zoxel.Items
{
    //! Handles all the items in the world.
    [UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class WorldItemSystemGroup : ComponentSystemGroup { }
}
