﻿using Unity.Entities;

namespace Zoxel.Items
{
    //! Tag for world item. 
    /**
    *   \todo Use BornTime, ItemQuantity and MetaData components instead.
    */
    public struct WorldItem : IComponentData
    {
        public Entity item;
        public byte quantity;
        public double timeBorn;

        public WorldItem(Entity item, byte quantity, double timeBorn)
        {
            this.item = item;
            this.quantity = quantity;
            this.timeBorn = timeBorn;
        }
    }
}