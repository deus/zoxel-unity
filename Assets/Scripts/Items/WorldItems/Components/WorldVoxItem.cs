using Unity.Entities;

namespace Zoxel.Items
{
    //! An item that has a vox model.
    public struct WorldVoxItem : IComponentData { }
}