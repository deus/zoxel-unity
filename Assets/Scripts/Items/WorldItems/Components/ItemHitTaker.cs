using Unity.Entities;

namespace Zoxel.Items
{
    public struct ItemHitTaker : IComponentData
    {
        public float radius;

        public ItemHitTaker(float radius)
        {
            this.radius = radius;
        }
    }
}