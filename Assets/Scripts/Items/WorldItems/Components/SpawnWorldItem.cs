using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Items
{
    //! An event to spawn a new world item!
    public struct SpawnWorldItem : IComponentData
    {
        public int id;
        public Entity item;
        public byte quantity;
        public Entity vox;
        public float3 spawnPosition;
        public quaternion spawnRotation;
        public byte voxelID;
        public Entity character;
    }
}