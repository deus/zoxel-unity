using Unity.Entities;
using Zoxel.Voxels;
using Zoxel.Rendering;

namespace Zoxel.Items
{
    //! Initializes WorldVoxItem's EntityMaterials.
    [UpdateInGroup(typeof(WorldItemSystemGroup))]
    public partial class WorldVoxModelInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null) return;
            var itemMaterial = MaterialsManager.instance.materials.itemMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, WorldVoxItem>()
                .ForEach((Entity e) =>
            {
                PostUpdateCommands.SetSharedComponentManaged(e, new EntityMaterials(itemMaterial));
            }).WithoutBurst().Run();
        }
    }
}