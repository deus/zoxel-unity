﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Animations;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;

namespace Zoxel.Items
{
    //! For interacting with items, they will play a sound upon hit.
    /**
    *   \todo Don't pickup items if your inventory is full.
    *   \todo Let NPCs pick up items as well.
    *   \todo Make system use a generic collision event; Character vs Item Collider.
    */
    [BurstCompile, UpdateInGroup(typeof(WorldItemSystemGroup))]
    public partial class ItemHitSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery worldItemQuery;
        public static Entity pickupItemPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            worldItemQuery = GetEntityQuery(
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DelayItemPickup>(),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<WorldItem>());
            var pickupItemArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(PickupItem),
                typeof(GenericEvent),
                typeof(DelayEvent));
            pickupItemPrefab = EntityManager.CreateEntity(pickupItemArchetype);
            RequireForUpdate(worldItemQuery);
            RequireForUpdate<SoundSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var soundSettings = GetSingleton<SoundSettings>();
            var itemPickupSoundVolume = soundSettings.itemPickupSoundVolume;
            var itemPickupSpeeds = new float2(0.7f, 1.3f);
            var itemSpawnSpeed = 0.25;  // todo: replace this by checking if it's chunk is busy
            var elapsedTime = World.Time.ElapsedTime;
            var pickupItemPrefab = ItemHitSystem.pickupItemPrefab;
            var soundPrefab = SoundPlaySystem.soundPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var itemEntities = worldItemQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var translations = GetComponentLookup<Translation>(true);
            var worldItems = GetComponentLookup<WorldItem>(true);
            var worldItemIDs = GetComponentLookup<ZoxID>(true);
            Entities
                .WithNone<EntityBusy, DeadEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref ItemHitTaker itemHitTaker, in Body body, in Translation translation) =>
            {
                for (var i = 0; i < itemEntities.Length; i++)
                {
                    var itemEntity = itemEntities[i];
                    var worldItem = worldItems[itemEntity];
                    if (elapsedTime - worldItem.timeBorn >= itemSpawnSpeed)
                    {
                        var itemPosition = translations[itemEntity].Value;
                        var difference = translation.Value - itemPosition;
                        if (math.abs(difference.x) <= body.size.x + itemHitTaker.radius
                            && math.abs(difference.y) <= body.size.y + itemHitTaker.radius
                            && math.abs(difference.z) <= body.size.z + itemHitTaker.radius)
                        {
                            // if can, start moving towards player, do a second check when item reaches them
                            var itemID = worldItemIDs[itemEntity].id;
                            // Animate the planet Item after it hits character
                            // Play Swoosh Sound
                            var soundSeed = itemID + (int)(8 * elapsedTime);
                            var random = new Random();
                            random.InitState((uint)soundSeed);
                            var soundEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, soundPrefab);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Seed(soundSeed));
                            var generateSound = new GenerateSound();
                            generateSound.InitializeItemPickupSound();
                            PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new SoundData(ref random, generateSound.timeLength, generateSound.sampleRate, 1, itemPickupSoundVolume));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new DelayEvent(elapsedTime, 0.05));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Translation { Value = itemPosition });
                            var itemPickupSpeed = random.NextFloat(itemPickupSpeeds.x, itemPickupSpeeds.y);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new DestroyEntityInTime(elapsedTime, itemPickupSpeed));
                            PostUpdateCommands.AddComponent<DeadEntity>(entityInQueryIndex, itemEntity);
                            PostUpdateCommands.AddComponent(entityInQueryIndex, itemEntity, new PositionEntityLerper(elapsedTime, itemPickupSpeed, itemPosition, e));
                            // Delays this for a second, and make it a seperate entity for picking up items
                            var pickupEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, pickupItemPrefab);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, pickupEntity, new PickupItem(e, worldItem));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, pickupEntity, new DelayEvent(elapsedTime, itemPickupSpeed));
                            // todo: play a second sound when item picked up?
                        }
                    }
                }
            })  .WithReadOnly(itemEntities)
                .WithDisposeOnCompletion(itemEntities)
                .WithReadOnly(worldItems)
                .WithReadOnly(translations)
                .WithReadOnly(worldItemIDs)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}