using Unity.Entities;

namespace Zoxel.Items
{
    //! An event for linking up user items after spawning.
    public struct OnUniqueItemsSpawned : IComponentData
    {
        public byte spawned;

        public OnUniqueItemsSpawned(byte spawned)
        {
            this.spawned = spawned;
        }
    }
}