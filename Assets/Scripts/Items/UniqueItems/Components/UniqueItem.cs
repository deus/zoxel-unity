using Unity.Entities;

namespace Zoxel.Items
{
    //! A tag for a unique item attached to an entity.
    public struct UniqueItem : IComponentData { }
}