using Unity.Entities;

namespace Zoxel.Items
{
    //! Used to identify a UniqueItem that needs to link to it's Entity.
    public struct LinkUniqueItem : IComponentData { }
}