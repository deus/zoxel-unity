using Unity.Entities;

namespace Zoxel.Items
{
    //! Saves any unique items on the entity.
    public struct SaveUniqueItems : IComponentData { }
}