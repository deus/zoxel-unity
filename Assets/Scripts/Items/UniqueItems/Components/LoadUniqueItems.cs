using Unity.Entities;

namespace Zoxel.Items
{
    //! Load any unique items on the entity.
    public struct LoadUniqueItems : IComponentData
    {
        public Text loadPath;
        public AsyncFileReader reader;

        public void Dispose()
        {
            loadPath.Dispose();
            reader.Dispose();
        }
    }
}