using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Items
{
    //! Links user items to inventory user after they spawn.
    /**
    *   \todo Link like UserItemLinkSystem.
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(ItemSystemGroup))]
    public partial class UniqueItemsSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery uniqueItemsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            uniqueItemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<LinkUniqueItem>(),
                ComponentType.ReadOnly<UniqueItem>(),
                ComponentType.ReadOnly<CreatorLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uniqueItemsEntities = uniqueItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var itemIDs = GetComponentLookup<ZoxID>(true);
            var creatorLinks = GetComponentLookup<CreatorLink>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref ItemLinks itemLinks, ref OnUniqueItemsSpawned onUniqueItemsSpawned) =>
            {
                var previousItemsCount = itemLinks.items.Length;
                if (onUniqueItemsSpawned.spawned != 255)
                {
                    // itemLinks.Initialize(onUniqueItemsSpawned.spawned);
                    itemLinks.AddNew((byte) (onUniqueItemsSpawned.spawned));
                    onUniqueItemsSpawned.spawned = 255;
                }
                if (itemLinks.items.Length == 0)
                {
                    PostUpdateCommands.RemoveComponent<OnUniqueItemsSpawned>(entityInQueryIndex, e);
                    return;
                }
                var count = previousItemsCount;
                for (int i = 0; i < uniqueItemsEntities.Length; i++)
                {
                    var e2 = uniqueItemsEntities[i];
                    var creator = creatorLinks[e2].creator;
                    if (creator == e)
                    {
                        itemLinks.items[count] = e2;
                        itemLinks.itemsHash[itemIDs[e2].id] = e2;
                        count++;
                        if (itemLinks.items.Length == count)
                        {
                            break;
                        }
                    }
                }
                if (count == itemLinks.items.Length)
                {
                    PostUpdateCommands.RemoveComponent<OnUniqueItemsSpawned>(entityInQueryIndex, e);
                    for (int i = previousItemsCount; i < itemLinks.items.Length; i++)
                    {
                        PostUpdateCommands.RemoveComponent<LinkUniqueItem>(entityInQueryIndex, itemLinks.items[i]);
                    }
                }
                // UnityEngine.Debug.LogError("Spawned " + count + " Unique Items.");
            })  .WithReadOnly(uniqueItemsEntities)
                .WithDisposeOnCompletion(uniqueItemsEntities)
                .WithReadOnly(itemIDs)
                .WithReadOnly(creatorLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}