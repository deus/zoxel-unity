using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Cameras;
using Zoxel.Rendering;
using Zoxel.Rendering.Authoring;

namespace Zoxel.Items
{
    //! Renders WorldItem's as cubes in a single draw call.
    /**
    *   - Render System -
    *   \todo Render call for the player cameras, portal cameras, but not the viewer cameras!
    *   \todo Look into replacing with DrawMeshInstancedIndirect using a compute buffer for arguments
    *   \todo Add these buffers to a shared component that i can add to planet
    *   Note: tried to make this when update but failed?
    */
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public partial class WorldVoxelRenderSystem : SystemBase
    {
        private const UnityEngine.ComputeBufferType bufferType = UnityEngine.ComputeBufferType.IndirectArguments;
        private const UnityEngine.ComputeBufferMode bufferMode = UnityEngine.ComputeBufferMode.SubUpdates;
        private EntityQuery renderQuery;
        public static bool hasBoundsUpdated;
        private static float3 newBoundsPosition;
        public static UnityEngine.Bounds bounds;
        public static UnityEngine.Mesh voxelCube;
        public static int isZeroLastFrame;

        protected override void OnCreate()
        {
            renderQuery = GetEntityQuery(
                ComponentType.ReadOnly<VoxelTileIndex>(),
                ComponentType.Exclude<DestroyEntity>());
            voxelCube = MeshUtilities.GenerateCube(new float3(1,1,1), true);    // 0.42f * 
            bounds = new UnityEngine.Bounds(float3.zero, new float3(512, 512, 512));
            RequireForUpdate(renderQuery);
            RequireForUpdate<RenderSettings>();
        }

        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null)
            {
                return;
            }
            var renderCount = WorldItemBeginSystem.renderCount;
            if (renderCount == 0)
            {
                isZeroLastFrame = 0;
                return;
            }
            else if (isZeroLastFrame <= 0)
            {
                isZeroLastFrame++;
                return;
            }
            // var layer = (byte) 0; //  (byte) UnityEngine.LayerMask.NameToLayer("WorldUI"); // 0;
            var layer = (byte) UnityEngine.LayerMask.NameToLayer("Default"); // 0;
            var material = MaterialsManager.instance.materials.voxelItemMaterial;
            var renderSettings = GetSingleton<RenderSettings>();
            var isShadows = renderSettings.isShadows;
            var shadowMode = renderSettings.shadowMode;
            var isRenderToAllCameras = renderSettings.isRenderToAllCameras;
            UnityEngine.Camera renderCamera = null;
            if (!isRenderToAllCameras)
            {
                renderCamera = CameraReferences.GetMainCamera(EntityManager);
            }
            UnityEngine.Graphics.DrawMeshInstancedProcedural(voxelCube, 0, material, bounds, renderCount,
                null, shadowMode, isShadows, layer, renderCamera);
        }

        public static void SetBuffers(int renderCount)
        {
            WorldItemPositionSystem.renderPositionBuffer = new UnityEngine.ComputeBuffer(renderCount, 3 * 4, bufferType, bufferMode);
            WorldItemRotationSystem.renderRotationBuffer = new UnityEngine.ComputeBuffer(renderCount, 4 * 4, bufferType, bufferMode);
            WorldItemScaleSystem.renderScaleBuffer = new UnityEngine.ComputeBuffer(renderCount, 1 * 4, bufferType, bufferMode);
            WorldItemColorSystem.renderColorBuffer = new UnityEngine.ComputeBuffer(renderCount, 3 * 4, bufferType, bufferMode);
            WorldItemTileIndexSystem.renderTileIndexBuffer = new UnityEngine.ComputeBuffer(renderCount, 1 * 4, bufferType, bufferMode);
        }

        public static void SetBoundsPosition(float3 newPosition)
        {
            if (bounds.center.x != newPosition.x || bounds.center.y != newPosition.y || bounds.center.z != newPosition.z)
            {
                newBoundsPosition = newPosition;
                hasBoundsUpdated = true;
            }
        }

        public static void SetNewBoundsPosition()
        {
            bounds.center = newBoundsPosition;
            hasBoundsUpdated = false;
        }

        public static void ReleaseBuffers()
        {
            WorldItemPositionSystem.ReleaseBuffers();
            WorldItemRotationSystem.ReleaseBuffers();
            WorldItemScaleSystem.ReleaseBuffers();
            WorldItemColorSystem.ReleaseBuffers();
            WorldItemTileIndexSystem.ReleaseBuffers();
        }

        protected override void OnDestroy()
        {
            if (voxelCube)
            {
                ObjectUtil.Destroy(voxelCube);
            }
        }
    }
}