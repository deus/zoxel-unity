using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Items
{
    [UpdateAfter(typeof(WorldItemBeginSystem))]
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class WorldItemScaleSystem : SystemBase
    {
        public static int renderCount;
        private EntityQuery changeQuery;
        public static UnityEngine.ComputeBuffer renderScaleBuffer;

        protected override void OnCreate()
        {
            changeQuery = GetEntityQuery(
                ComponentType.ReadOnly<VoxelTileIndex>(),
                ComponentType.ReadOnly<Scale>(),
                ComponentType.Exclude<DestroyEntity>());
            changeQuery.SetChangedVersionFilter(typeof(Scale));
        }

        protected override void OnDestroy()
        {
            ReleaseBuffers();
        }

        public static void ReleaseBuffers()
        {
            if (renderScaleBuffer != null)
            {
                renderScaleBuffer.Release();
            }
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            #if DISABLE_COMPUTER_BUFFERS
            return;
            #endif
            var didQueryNotChange = changeQuery.CalculateEntityCount() == 0;
            if (!WorldItemBeginSystem.didChangeLastFrame && didQueryNotChange)
            {
                return;
            }
            var renderCount = WorldItemBeginSystem.renderCount;
            var scales =  renderScaleBuffer.BeginWrite<float>(0, renderCount);
            Dependency = Entities
                // .WithChangeFilter<Scale>()
                .WithNone<DestroyEntity>()
                .WithAll<WorldVoxel>()
                .ForEach((int entityInQueryIndex, in Scale scale) =>
            {
                scales[entityInQueryIndex] = scale.Value;
            }).ScheduleParallel(Dependency);
            renderScaleBuffer.EndWrite<float>(renderCount);
            MaterialsManager.instance.materials.voxelItemMaterial.SetBuffer("scales",  renderScaleBuffer);
        }
    }
}