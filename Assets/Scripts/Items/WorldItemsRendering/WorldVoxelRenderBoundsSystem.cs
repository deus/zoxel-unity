using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.Items
{
    //! Updates render systems origin positions when ChunkStreamPoint position changes.
    /**
    *   Note: Make sure this takes effect before those positions upload their positions to the GPU.
    *   \todo Make this a generic event instead of static function calls
    */
    [UpdateBefore(typeof(RenderBoundsPostUpdateSystem))]
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class WorldVoxelRenderBoundsSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((in OnChunkStreamPointUpdated onChunkStreamPointUpdated) =>
            {
                WorldVoxelRenderSystem.SetBoundsPosition(onChunkStreamPointUpdated.worldPosition);
            }).WithoutBurst().Run();
        }
    }
}