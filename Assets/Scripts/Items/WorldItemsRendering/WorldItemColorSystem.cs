using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Rendering;

namespace Zoxel.Items
{
    [UpdateAfter(typeof(WorldItemBeginSystem))]
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class WorldItemColorSystem : SystemBase
    {
        public static int renderCount;
        private EntityQuery changeQuery;
        public static UnityEngine.ComputeBuffer renderColorBuffer;

        protected override void OnCreate()
        {
            changeQuery = GetEntityQuery(
                ComponentType.ReadOnly<VoxelTileIndex>(),
                ComponentType.ReadOnly<MaterialBaseColor>(),
                ComponentType.Exclude<DestroyEntity>());
            changeQuery.SetChangedVersionFilter(typeof(MaterialBaseColor));
        }

        protected override void OnDestroy()
        {
            ReleaseBuffers();
        }

        public static void ReleaseBuffers()
        {
            if (renderColorBuffer != null)
            {
                renderColorBuffer.Release();
            }
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            #if DISABLE_COMPUTER_BUFFERS
            return;
            #endif
            var didQueryNotChange = changeQuery.CalculateEntityCount() == 0; // changeQuery.IsEmpty;
            if (!WorldItemBeginSystem.didChangeLastFrame && didQueryNotChange)
            {
                return;
            }
            var renderCount = WorldItemBeginSystem.renderCount;
            var colors = renderColorBuffer.BeginWrite<float3>(0, renderCount);
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<WorldVoxel>()
                .ForEach((int entityInQueryIndex, in MaterialBaseColor materialBaseColor) =>
            {
                colors[entityInQueryIndex] = new float3(materialBaseColor.Value.x, materialBaseColor.Value.y, materialBaseColor.Value.z);
            }).ScheduleParallel(Dependency);
            renderColorBuffer.EndWrite<float3>(renderCount);
            MaterialsManager.instance.materials.voxelItemMaterial.SetBuffer("colors", renderColorBuffer);
        }
    }
}