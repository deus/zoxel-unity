using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Items
{
    [UpdateAfter(typeof(WorldItemBeginSystem))]
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class WorldItemRotationSystem : SystemBase
    {
        public static int renderCount;
        private EntityQuery changeQuery;
        public static UnityEngine.ComputeBuffer renderRotationBuffer;

        protected override void OnCreate()
        {
            changeQuery = GetEntityQuery(
                ComponentType.ReadOnly<VoxelTileIndex>(),
                ComponentType.ReadOnly<Rotation>(),
                ComponentType.Exclude<DestroyEntity>());
            changeQuery.SetChangedVersionFilter(typeof(Rotation));
        }

        protected override void OnDestroy()
        {
            ReleaseBuffers();
        }

        public static void ReleaseBuffers()
        {
            if (renderRotationBuffer != null)
            {
                renderRotationBuffer.Release();
            }
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            #if DISABLE_COMPUTER_BUFFERS
            return;
            #endif
            var didQueryNotChange = changeQuery.CalculateEntityCount() == 0; // changeQuery.IsEmpty;
            if (!WorldItemBeginSystem.didChangeLastFrame && didQueryNotChange)
            {
                return;
            }
            var renderCount = WorldItemBeginSystem.renderCount;
            var rotations =  renderRotationBuffer.BeginWrite<float4>(0, renderCount);
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<WorldVoxel>()
                .ForEach((int entityInQueryIndex, in Rotation rotation) =>
            {
                rotations[entityInQueryIndex] = rotation.Value.value;
            }).ScheduleParallel(Dependency);
            renderRotationBuffer.EndWrite<float4>(renderCount);
            MaterialsManager.instance.materials.voxelItemMaterial.SetBuffer("rotations",  renderRotationBuffer);
        }
    }
}