using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Items
{
    [UpdateAfter(typeof(WorldItemBeginSystem))]
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class WorldItemPositionSystem : SystemBase
    {
        public static UnityEngine.ComputeBuffer renderPositionBuffer;
        public static int renderCount;
        private EntityQuery changeQuery;

        protected override void OnCreate()
        {
            changeQuery = GetEntityQuery(
                ComponentType.ReadOnly<VoxelTileIndex>(),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.Exclude<DestroyEntity>());
            changeQuery.SetChangedVersionFilter(typeof(Translation));
        }

        protected override void OnDestroy()
        {
            ReleaseBuffers();
        }

        public static void ReleaseBuffers()
        {
            if (renderPositionBuffer != null)
            {
                renderPositionBuffer.Release();
            }
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            #if DISABLE_COMPUTER_BUFFERS
            return;
            #endif
            var didQueryNotChange = changeQuery.CalculateEntityCount() == 0; // changeQuery.IsEmpty;
            if (!WorldItemBeginSystem.didChangeLastFrame && didQueryNotChange && !WorldVoxelRenderSystem.hasBoundsUpdated)
            {
                return;
            }
            if (WorldVoxelRenderSystem.hasBoundsUpdated)
            {
                WorldVoxelRenderSystem.SetNewBoundsPosition();
            }
            var boundsPosition = new float3(WorldVoxelRenderSystem.bounds.center.x, WorldVoxelRenderSystem.bounds.center.y, WorldVoxelRenderSystem.bounds.center.z);
            var renderCount = WorldItemBeginSystem.renderCount;
            var positions = renderPositionBuffer.BeginWrite<float3>(0, renderCount);
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<WorldVoxel>()
                .ForEach((int entityInQueryIndex, in Translation translation) =>
            {
                positions[entityInQueryIndex] = translation.Value - boundsPosition;
            }).ScheduleParallel(Dependency);
            renderPositionBuffer.EndWrite<float3>(renderCount);
            MaterialsManager.instance.materials.voxelItemMaterial.SetBuffer("positions",  renderPositionBuffer);
        }
    }
}