using Unity.Entities;
using Unity.Burst;
using Zoxel.Rendering;

namespace Zoxel.Items
{
    //! Uploads TileIndex to the GPU for WorldVoxel's.
    [UpdateAfter(typeof(WorldItemBeginSystem))]
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class WorldItemTileIndexSystem : SystemBase
    {
        public static int renderCount;
        private EntityQuery changeQuery;
        public static UnityEngine.ComputeBuffer renderTileIndexBuffer;

        protected override void OnCreate()
        {
            changeQuery = GetEntityQuery(
                ComponentType.ReadOnly<VoxelTileIndex>(),
                ComponentType.ReadOnly<MaterialBaseColor>(),
                ComponentType.Exclude<DestroyEntity>());
            changeQuery.SetChangedVersionFilter(typeof(VoxelTileIndex));
        }

        protected override void OnDestroy()
        {
            ReleaseBuffers();
        }

        public static void ReleaseBuffers()
        {
            if (renderTileIndexBuffer != null)
            {
                renderTileIndexBuffer.Release();
            }
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            #if DISABLE_COMPUTER_BUFFERS
            return;
            #endif
            var didQueryNotChange = changeQuery.CalculateEntityCount() == 0; // changeQuery.IsEmpty;
            if (!WorldItemBeginSystem.didChangeLastFrame && didQueryNotChange)
            {
                return;
            }
            var renderCount = WorldItemBeginSystem.renderCount;
            var voxelTileIndexes =  renderTileIndexBuffer.BeginWrite<float>(0, renderCount);
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<WorldVoxel>()
                .ForEach((int entityInQueryIndex, in VoxelTileIndex voxelTileIndex) =>
            {
                voxelTileIndexes[entityInQueryIndex] = voxelTileIndex.index;
            }).ScheduleParallel(Dependency);
            renderTileIndexBuffer.EndWrite<float>(renderCount);
            MaterialsManager.instance.materials.voxelItemMaterial.SetBuffer("tileIndexes",  renderTileIndexBuffer);
        }
    }
}