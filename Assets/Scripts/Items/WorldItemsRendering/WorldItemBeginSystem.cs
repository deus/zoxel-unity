using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Rendering;

namespace Zoxel.Items
{
    //! Set's the buffers and size for WorldItem render systems.
    [AlwaysUpdateSystem]
    [UpdateAfter(typeof(RenderBoundsPostUpdateSystem))]
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class WorldItemBeginSystem : SystemBase
    {
        private EntityQuery renderQuery;
        public static int renderCount;
        public static bool didChangeLastFrame;

        protected override void OnCreate()
        {
            renderQuery = GetEntityQuery(
                ComponentType.ReadOnly<VoxelTileIndex>(),
                ComponentType.Exclude<DestroyEntity>());
        }
        
        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null)
            {
                return;
            }
            var material = MaterialsManager.instance.materials.voxelItemMaterial;
            var newRenderCount = renderQuery.CalculateEntityCount();
            if (renderCount != newRenderCount)
            {
                renderCount = newRenderCount;
                if (renderCount != 0)
                {
                    WorldVoxelRenderSystem.ReleaseBuffers();
                    WorldVoxelRenderSystem.SetBuffers(renderCount);
                }
                didChangeLastFrame = true;
            }
            if (renderCount == 0)
            {
                WorldVoxelRenderSystem.isZeroLastFrame = 0;
            }
        }
    }
}