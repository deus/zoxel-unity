using Unity.Entities;

namespace Zoxel.Characters
{
    public struct GiveGameSkill : IComponentData
    {
        public int skillIndex;

        public GiveGameSkill(int skillIndex)
        {
            this.skillIndex = skillIndex;
        }
    }
}