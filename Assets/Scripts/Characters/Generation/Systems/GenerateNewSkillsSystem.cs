using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Skills;
using Zoxel.Actions;
using Zoxel.Classes;

namespace Zoxel.Characters
{
    //! Generates new skills for a character.
    /**
    *   Generates skills based off class or off npc type.
    */
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class GenerateNewSkillsSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<SkillLinks>());
            RequireForUpdate(processQuery);
            RequireForUpdate(realmsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<GenerateSkills>(processQuery);
            var userSkillPrefab = SkillSystemGroup.userSkillPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var realmsEntities = realmsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var skillLinks = GetComponentLookup<SkillLinks>(true);
            realmsEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .WithAll<UserSkillLinks>()
                .ForEach((Entity e, int entityInQueryIndex, in RealmLink realmLink, in ClassLink classLink, in GenerateSkills generateSkills) =>
            {
                var addedSkills = (byte) 0;
                var realmEntity = realmLink.realm;
                if (!skillLinks.HasComponent(realmEntity))
                {
                    return;
                }
                var skillLinks2 = skillLinks[realmEntity];
                if (classLink.classs.Index == 0)
                {
                    var skillEntity = skillLinks2.skills[0];
                    if (generateSkills.spawnType == 1)
                    {
                        skillEntity = skillLinks2.skills[1];
                    }
                    if (generateSkills.spawnType == 2)
                    {
                        skillEntity = skillLinks2.skills[1];
                    }
                    var userSkillEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, userSkillPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new MetaData(skillEntity));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new CharacterLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new UserDataIndex(0));
                    addedSkills++;
                }
                if (addedSkills > 0)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUserSkillSpawned(addedSkills));
                }
                PostUpdateCommands.AddComponent<SaveSkills>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<GenerateUserActions>(entityInQueryIndex, e);
			})  .WithReadOnly(skillLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                /*else
                {
                    var classData = classes[classLink.classs];
                    // Adds startingSkills
                    for (int i = 0; i < classData.startingSkills.Length; i++)
                    {
                        var skillEntity = classData.startingSkills[i];
                        var userSkillEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, userSkillPrefab);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new MetaData(skillEntity));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new UserDataIndex(i));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new CharacterLink(e));
                        addedSkills++;
                    }
                }*/