using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Zoxel.Skills;
using Zoxel.Actions;

namespace Zoxel.Characters
{
    //! Gives an entity a new skill. Then adds GenerateUserActions.
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class GiveGameSkillSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<SkillLinks>());
            RequireForUpdate(processQuery);
            RequireForUpdate(realmQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<GiveGameSkill>(processQuery);
            var userSkillPrefab = SkillSystemGroup.userSkillPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var skillLinks = GetComponentLookup<SkillLinks>(true);
            realmEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, GenerateBody>()
                .ForEach((Entity e, int entityInQueryIndex, in UserSkillLinks userSkillLinks, in GiveGameSkill giveGameSkill, in RealmLink realmLink) =>
            {
                var realmSkills = skillLinks[realmLink.realm].skills;
                var skillEntity = realmSkills[giveGameSkill.skillIndex];
                var userSkillEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, userSkillPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new MetaData(skillEntity));
                PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new CharacterLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new UserDataIndex((byte) userSkillLinks.skills.Length));
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUserSkillSpawned(1));
                PostUpdateCommands.AddComponent<GenerateUserActions>(entityInQueryIndex, e);
			})  .WithReadOnly(skillLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}