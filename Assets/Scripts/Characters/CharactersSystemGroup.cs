﻿using Unity.Entities;
using Zoxel.Clans;

namespace Zoxel.Characters
{
    //! Now, this is a Character! Ay!
    [AlwaysUpdateSystem]
    public partial class CharactersSystemGroup : ComponentSystemGroup
    {
        public static int charactersCount;
		private EntityQuery charactersQuery;
        public static int busyCharactersCount;
		private EntityQuery busyCharactersQuery;
        public static Entity spawnCharacterPrefab;
        public static Entity spawnPlayerCharacterPrefab;

        protected override void OnCreate()
        {
            base.OnCreate();
			charactersQuery = GetEntityQuery(ComponentType.ReadOnly<Character>());
			busyCharactersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Character>(),
                ComponentType.ReadOnly<EntityBusy>());
            var spawnCharacterArchetype = EntityManager.CreateArchetype(
                typeof(DelayEvent),
                typeof(SpawnCharacter),
                typeof(ClanLink),
                typeof(Prefab));
            spawnCharacterPrefab = EntityManager.CreateEntity(spawnCharacterArchetype);
            var spawnPlayerCharacterArchetype = EntityManager.CreateArchetype(
                typeof(DelayEvent),
                typeof(SpawnCharacter),
                typeof(SpawnPlayerCharacter),
                typeof(ClanLink),
                typeof(Prefab));
            spawnPlayerCharacterPrefab = EntityManager.CreateEntity(spawnPlayerCharacterArchetype);
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();
            charactersCount = charactersQuery.CalculateEntityCount();
            busyCharactersCount = busyCharactersQuery.CalculateEntityCount();
        }
    }
}
