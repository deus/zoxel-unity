using Unity.Entities;

namespace Zoxel.Characters.World
{
    //! Spawns characters in a chunk
    public struct SpawnChunkCharacters : IComponentData { }
}