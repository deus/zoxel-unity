using Unity.Entities;

namespace Zoxel.Characters.World
{
	public struct ChunkHasCharacters : IComponentData { }
}