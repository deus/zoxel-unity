using Unity.Entities;

namespace Zoxel.Characters.World
{
    //! Saves Characters in Chunk
    public struct SpawnedChunkCharacters : IComponentData { }
}