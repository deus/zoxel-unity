using Unity.Entities;

namespace Zoxel.Characters.World
{
    public struct ChunkCharacterDied : IComponentData
    {
        public Entity chunk;
        public Entity character;

        public ChunkCharacterDied(Entity chunk, Entity character)
        {
            this.chunk = chunk;
            this.character = character;
        }
    }
}