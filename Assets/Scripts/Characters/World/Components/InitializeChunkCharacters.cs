using Unity.Entities;

namespace Zoxel.Characters.World
{
    //! Loads Characters in Chunk
    public struct InitializeChunkCharacters : IComponentData { }
}