﻿using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Characters.World
{
    //! Chunk's character links.
    /**
    *   \todo when character moves into a new chunk - create reemove and add events for both old and new chunks
    *   \todo Debug why its not destroying characters in chunks atm, probaly not linked properly.
    */
    public struct ChunkCharacters : IComponentData
    {
        //! An array of links to the characters currently in the chunk.
        public BlitableArray<Entity> characters;
        
        public void DisposeFinal()
        {
            characters.DisposeFinal();
        }
        
        public void Dispose()
        {
            characters.Dispose();
        }

        public void Remove(int index)
        {
            if (characters.Length == 0)
            {
                return;
            }
            var newChildren = new BlitableArray<Entity>(characters.Length - 1, Allocator.Persistent);
            for (int i = 0; i < newChildren.Length; i++)
            {
                if (i < index)
                {
                    newChildren[i] = characters[i];
                }
                else
                {
                    newChildren[i] = characters[i + 1];
                }
            }
            // can also adjust indexes if needed
            characters.Dispose();
            characters = newChildren;
        }

        //! Adds an amount of new character entity links to array.
        public void AddNew(byte addCount)
        {
            var newOnes = new BlitableArray<Entity>(characters.Length + addCount, Allocator.Persistent);
            for (int i = 0; i < characters.Length; i++)
            {
                newOnes[i] = characters[i];
            }
            for (int i = characters.Length; i < newOnes.Length; i++)
            {
                newOnes[i] = new Entity();
            }
            Dispose();
            characters = newOnes;
        }
    }
}