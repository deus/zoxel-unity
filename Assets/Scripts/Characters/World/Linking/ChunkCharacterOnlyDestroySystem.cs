using Unity.Entities;
using Unity.Burst;
using Zoxel.Characters;

namespace Zoxel.Characters.World
{
    //! Destroys all characters inside a chunk. Done during streaming map.
    /**
    *   - Destroy System -
    *    \todo What happens if DestroyEntity with chunk, while it's still linking? Make sure to pass in characters when destroying ChunkCharacters
    */
    [BurstCompile, UpdateInGroup(typeof(WorldCharactersSystemGroup))]
    public partial class ChunkCharacterOnlyDestroySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyChunkCharacters>()
                .ForEach((Entity e, int entityInQueryIndex, ref ChunkCharacters chunkCharacters) =>
            {
                /*if (disableDestroyCharacters)
                {
                    chunkCharacters.Dispose();
                    PostUpdateCommands.RemoveComponent<DestroyChunkCharacters>(entityInQueryIndex, e);
                    return;
                }*/
                PostUpdateCommands.RemoveComponent<DestroyChunkCharacters>(entityInQueryIndex, e);
                for (int i = 0; i < chunkCharacters.characters.Length; i++)
                {
                    var characterEntity = chunkCharacters.characters[i];
                    if (HasComponent<Character>(characterEntity))
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, characterEntity);
                    }
                }
                chunkCharacters.Dispose();
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}