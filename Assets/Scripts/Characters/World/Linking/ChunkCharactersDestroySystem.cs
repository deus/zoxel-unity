using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Characters.World
{
    //! Using DestroyEntity, cleans up the chunk's characters.
    /**
    *   \todo What happens if DestroyEntity with chunk, while it's still linking? Make sure to pass in characters when destroying ChunkCharacters
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ChunkCharactersDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyEntity, ChunkCharacters>()
                .ForEach((Entity e, int entityInQueryIndex, in ChunkCharacters chunkCharacters) =>
			{
                if (chunkCharacters.characters.Length == 0)
                {
                    return;
                }
                // UnityEngine.Debug.LogError("Chunk " + e.Index + " Destroying Characters: " + chunkCharacters.characters.Length);
                for (int i = 0; i < chunkCharacters.characters.Length; i++)
                {
                    var spawn = chunkCharacters.characters[i];
                    if (HasComponent<Character>(spawn)) 
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, spawn);
                    }
                }
                chunkCharacters.DisposeFinal();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}