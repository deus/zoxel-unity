using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;
using Zoxel.Characters;

namespace Zoxel.Characters.World
{
    //! Links characters to chunk after they spawn.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(WorldCharactersSystemGroup))]
    public partial class ChunkCharacterSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery characterQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<ChunkCharacterSpawned>());
            characterQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<DeadEntity>(),
                // ComponentType.Exclude<SummonedEntity>(),
                ComponentType.ReadOnly<NewCharacter>(),
                ComponentType.ReadOnly<Character>(),
                ComponentType.ReadOnly<ChunkLink>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
			PostUpdateCommands2.RemoveComponent<NewCharacter>(characterQuery);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			//! Next initializes children.
			var spawnedEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
			Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var chunkCharacterSpawned = GetComponentLookup<ChunkCharacterSpawned>(true);
			var newCharacterEntities = characterQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
			Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunkLinks = GetComponentLookup<ChunkLink>(true);
			Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref ChunkCharacters chunkCharacters) =>
			{
                byte addedCount = 0;
				for (int i = 0; i < spawnedEntities.Length; i++)
				{
					var e2 = spawnedEntities[i];
					var chunkCharacterSpawned2 = chunkCharacterSpawned[e2];
					if (e == chunkCharacterSpawned2.chunk)
					{
                        addedCount += chunkCharacterSpawned2.spawned;
					}
				}
                if (addedCount == 0)
                {
                    return;
                }
                PostUpdateCommands.AddComponent<SaveChunkCharacters>(entityInQueryIndex, e);
                var beforeCount = (byte) chunkCharacters.characters.Length;
                chunkCharacters.AddNew(addedCount);
                byte count = 0;
                for (int i = 0; i < newCharacterEntities.Length; i++)
                {
                    var e2 = newCharacterEntities[i];
                    var chunkLink = chunkLinks[e2];
                    if (chunkLink.chunk == e)
                    {
                        chunkCharacters.characters[beforeCount + count] = e2;
                        count++;
                        if (beforeCount + count >= chunkCharacters.characters.Length)
                        {
                            break;
                        }
                    }
                }
                /*if (count != addedCount)
                {
                    UnityEngine.Debug.LogError("ChunkCharacterSpawnedSystem issue! beforeCount was: " + beforeCount + ", Added [" + addedCount + ", but found only " + count
                        + "] Total newCharacterEntities.Length: " + newCharacterEntities.Length);
                    for (int i = 0; i < newCharacterEntities.Length; i++)
                    {
                        var e2 = newCharacterEntities[i];
                        UnityEngine.Debug.LogError(i + "    e2: " + e2.Index);
                    }
                }*/
			})  .WithReadOnly(spawnedEntities)
				.WithDisposeOnCompletion(spawnedEntities)
				.WithReadOnly(chunkCharacterSpawned)
                .WithReadOnly(newCharacterEntities)
                .WithDisposeOnCompletion(newCharacterEntities)
				.WithReadOnly(chunkLinks)
				.ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}