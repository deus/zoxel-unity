using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;
using Zoxel.Characters;
using Zoxel.Rendering;

namespace Zoxel.Characters.World
{
    //! Links characters to chunk after they spawn.
    /**
    *   - Update Linking System -
    */
    [UpdateAfter(typeof(ChunkCharacterSpawnedSystem))]
    [BurstCompile, UpdateInGroup(typeof(WorldCharactersSystemGroup))]
    public partial class ChunkCharacterMovedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery planetChunksQuery;
        private EntityQuery charactersQuery;
        private EntityQuery characterChunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<ChunkCharacterMoved>());
            planetChunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<PlanetChunk>());
            charactersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Character>(),
                ComponentType.ReadOnly<Vox>());
            characterChunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<ModelChunk>(),
                ComponentType.ReadOnly<ChunkRenderLinks>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			var movedEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
			Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var chunkCharacterMoved = GetComponentLookup<ChunkCharacterMoved>(true);
            var characterEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleE);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleE);
            var characterVoxes = GetComponentLookup<Vox>(true);
            // var characterDisableRenders = GetComponentLookup<DisableRender>(true);
            characterEntities.Dispose();
            var characterChunkEntities = characterChunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleF);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleF);
            var characterChunkRenderLinks = GetComponentLookup<ChunkRenderLinks>(true);
            characterChunkEntities.Dispose();
			Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref ChunkCharacters chunkCharacters) =>
			{
                var isUpdated = false;
				for (int i = 0; i < movedEntities.Length; i++)
				{
					var e2 = movedEntities[i];
					var chunkCharacterMoved2 = chunkCharacterMoved[e2];
					if (e == chunkCharacterMoved2.oldChunk || e == chunkCharacterMoved2.newChunk)
					{
                        isUpdated = true;
                        break;
					}
				}
                if (!isUpdated)
                {
                    return;
                }
                PostUpdateCommands.AddComponent<SaveChunkCharacters>(entityInQueryIndex, e);
                var addCharacters = new NativeList<Entity>();
                var removeCharacters = new NativeList<Entity>();
                // for Renders
                var movedCharacters = new NativeList<Entity>();
                // var movedCharacterToChunks = new NativeList<Entity>();
                var movedCharacterFromChunks = new NativeList<Entity>();
				for (int i = 0; i < movedEntities.Length; i++)
				{
					var e2 = movedEntities[i];
					var chunkCharacterMoved2 = chunkCharacterMoved[e2];
					if (e == chunkCharacterMoved2.oldChunk)
					{
                        removeCharacters.Add(chunkCharacterMoved2.character);
                        /*movedCharacters.Add(chunkCharacterMoved2.character);
                        movedCharacterToChunks.Add(chunkCharacterMoved2.newChunk);
                        movedCharacterFromChunks.Add(e);*/
					}
					else if (e == chunkCharacterMoved2.newChunk)
					{
                        addCharacters.Add(chunkCharacterMoved2.character);
                        movedCharacters.Add(chunkCharacterMoved2.character);
                        movedCharacterFromChunks.Add(chunkCharacterMoved2.oldChunk);
                        // movedCharacterToChunks.Add(e);
					}
				}
                var oldCharactersLength = chunkCharacters.characters.Length;
                var newCount = oldCharactersLength + addCharacters.Length - removeCharacters.Length;
                if (newCount < 0)
                {
                    removeCharacters.Dispose();
                    addCharacters.Dispose();
                    //UnityEngine.Debug.LogError("newCount wrong: " + newCount
                    //            + " oldCharactersLength: " + oldCharactersLength + " addCharacters: " + addCharacters.Length + " removeCharacters: " + removeCharacters.Length);
                    return;
                }
                var count = 0;
                var newChildren = new BlitableArray<Entity>(newCount, Allocator.Persistent);
                for (int i = 0; i < oldCharactersLength; i++)
                {
                    var oldCharacter = chunkCharacters.characters[i];
                    if (!removeCharacters.Contains(oldCharacter))
                    {
                        if (count >= newCount)
                        {
                            //UnityEngine.Debug.LogError("1 count wrong: " + count + " / " + newCount + "[" + oldCharacter.Index + "]"
                            //    + " --- i: " + i + " oldCharactersLength: " + oldCharactersLength + " addCharacters: " + addCharacters.Length + " removeCharacters: " + removeCharacters.Length);
                            /*for (int j = 0; j < removeCharacters.Length; j++)
                            {
                                UnityEngine.Debug.LogError("1 count removeCharacters: " + removeCharacters[j].Index);
                            }
                            for (int j = 0; j < oldCharactersLength; j++)
                            {
                                UnityEngine.Debug.LogError("1 count chunkCharacters.characters: " + chunkCharacters.characters[j].Index);
                            }*/
                            break;
                        }
                        newChildren[count] = oldCharacter;
                        count++;
                    }
                }
                for (int i = 0; i < addCharacters.Length; i++)
                {
                    var newCharacter = addCharacters[i];
                    if (count >= newCount)
                    {
                        //UnityEngine.Debug.LogError("2 count wrong: " + count + " / " + newCount + "[" + newCharacter.Index + "]"
                        //        + " --- i: " + i + " oldCharactersLength: " + oldCharactersLength + " addCharacters: " + addCharacters.Length + " removeCharacters: " + removeCharacters.Length);
                        /*for (int j = 0; j < removeCharacters.Length; j++)
                        {
                            UnityEngine.Debug.LogError("2 count removeCharacters: " + removeCharacters[j].Index);
                        }
                        for (int j = 0; j < oldCharactersLength; j++)
                        {
                            UnityEngine.Debug.LogError("2 count chunkCharacters.characters: " + chunkCharacters.characters[j].Index);
                        }*/
                        break;
                    }
                    newChildren[count] = newCharacter;
                    count++;
                }
                chunkCharacters.Dispose();
                chunkCharacters.characters = newChildren;
                removeCharacters.Dispose();
                addCharacters.Dispose();
                // Render changes
                var disableRenderEntities = new NativeList<Entity>();
                var enableRenderEntities = new NativeList<Entity>();
                var isNewChunkDisabled = HasComponent<DisableRender>(e);
                for (int i = 0; i < movedCharacters.Length; i++)
                {
                    var oldChunk = movedCharacterFromChunks[i];
                    // var newChunk = e; // movedCharacterToChunks[i];
                    var isOldChunkDisabled = HasComponent<DisableRender>(oldChunk);
                    // if disable status doesn't change
                    if (isOldChunkDisabled == isNewChunkDisabled)
                    {
                        continue;
                    }
                    var characterEntity = movedCharacters[i];
                    if (characterVoxes.HasComponent(characterEntity))
                    {
                        if (isNewChunkDisabled)
                        {
                            disableRenderEntities.Add(characterEntity);
                        }
                        else
                        {
                            enableRenderEntities.Add(characterEntity);
                        }
                        var characterVox = characterVoxes[characterEntity];
                        for (int j = 0; j < characterVox.chunks.Length; j++)
                        {
                            var chunkEntity = characterVox.chunks[j];
                            if (characterChunkRenderLinks.HasComponent(chunkEntity))
                            {
                                var characterChunkRenderLinks2 = characterChunkRenderLinks[chunkEntity];
                                for (int k = 0; k < characterChunkRenderLinks2.chunkRenders.Length; k++)
                                {
                                    var renderEntity = characterChunkRenderLinks2.chunkRenders[k];
                                    //disableEntities.Add(renderEntity);
                                    if (isNewChunkDisabled)
                                    {
                                        disableRenderEntities.Add(renderEntity);
                                    }
                                    else
                                    {
                                        enableRenderEntities.Add(renderEntity);
                                    }
                                }
                            }
                        }
                    }
                }
                if (disableRenderEntities.Length > 0)
                {
                    // UnityEngine.Debug.LogError("Disabling Renders: " + disableRenderEntities.Length);
                    var disableEntities2 = disableRenderEntities.AsArray();
                    PostUpdateCommands.AddComponent<DisableRender>(entityInQueryIndex, disableEntities2);
                    disableEntities2.Dispose();
                }
                if (enableRenderEntities.Length > 0)
                {
                    // UnityEngine.Debug.LogError("Enabling Renders: " + enableRenderEntities.Length);
                    var enableEntities2 = enableRenderEntities.AsArray();
                    PostUpdateCommands.RemoveComponent<DisableRender>(entityInQueryIndex, enableEntities2);
                    enableEntities2.Dispose();
                }
                disableRenderEntities.Dispose();
                enableRenderEntities.Dispose();
                movedCharacters.Dispose();
                // movedCharacterToChunks.Dispose();
                movedCharacterFromChunks.Dispose();
			})  .WithReadOnly(movedEntities)
				.WithDisposeOnCompletion(movedEntities)
				.WithReadOnly(chunkCharacterMoved)
                .WithReadOnly(characterVoxes)
                // .WithReadOnly(characterDisableRenders)
                .WithReadOnly(characterChunkRenderLinks)
				.ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}