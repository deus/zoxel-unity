using Unity.Entities;
using Unity.Burst;
using Zoxel.Characters;
using Zoxel.Voxels;

namespace Zoxel.Characters.World
{
    //! NPC tells the chunk that it has died through the event.
    [BurstCompile, UpdateInGroup(typeof(WorldCharactersSystemGroup))]
	public partial class ChunkCharacterDyingSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity chunkCharacterDiedPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(ChunkCharacterDied),
                typeof(GenericEvent)
            );
            chunkCharacterDiedPrefab = EntityManager.CreateEntity(archetype);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var chunkCharacterDiedPrefab = this.chunkCharacterDiedPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities
                .WithNone<PlayerCharacter>() // , SummonedEntity>()
                .WithAll<DyingEntity, Character>()
                .ForEach((Entity e, int entityInQueryIndex, in ChunkLink chunkLink) =>
            {
                var chunk = chunkLink.chunk;
                if (HasComponent<Chunk>(chunk))
                {
                    var diedEventEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, chunkCharacterDiedPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, diedEventEntity, new ChunkCharacterDied(chunk, e));
                }
            }).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}