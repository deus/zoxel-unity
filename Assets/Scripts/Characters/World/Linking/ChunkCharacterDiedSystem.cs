using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Characters.World
{
    //! Removes a character linked to a chunk when the character dies.
    /**
    *   - Unlinking System -
    *   This system handles the event of a character dying by removving it from a chunk.
    */
    [BurstCompile, UpdateInGroup(typeof(WorldCharactersSystemGroup))]
    public partial class ChunkCharacterDiedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<ChunkCharacterDied>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var eventEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunkCharacterDieds = GetComponentLookup<ChunkCharacterDied>(true);
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref ChunkCharacters chunkCharacters) =>
            {
                var updated = false;
                for (int i = 0; i < eventEntities.Length; i++)
                {
                    var chunkCharacterDied = chunkCharacterDieds[eventEntities[i]];
                    if (e == chunkCharacterDied.chunk)
                    {
                        updated = true;
                        for (int j = 0; j < chunkCharacters.characters.Length; j++)
                        {
                            var spawn = chunkCharacters.characters[j];
                            if (spawn == chunkCharacterDied.character)
                            {
                                chunkCharacters.Remove(j);
                                break;
                            }
                        }
                    }
                }
                if (updated)
                {
                    PostUpdateCommands.AddComponent<SaveChunkCharacters>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(eventEntities)
                .WithDisposeOnCompletion(eventEntities)
                .WithReadOnly(chunkCharacterDieds)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}