using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Characters;
using Zoxel.Characters.Authoring;
using Zoxel.Voxels;
using System.IO;

namespace Zoxel.Characters.World
{
    //! Loads characters inside a chunk or adds spawn event.
    /**
    *   - IO System -
    *   \todo Optimize this took 6ms (once). Can I Directory.GetDirectories?
    *   Now spawns character once per frame using EventDelays on the spawn entities.
    *   Actually whn a charactr moves in here, it will load again, need to check if any ids are loaded
    */
	[BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class ChunkCharactersLoadSystem : SystemBase
    {
        private const string worldsLabel = "/Worlds/";
        private const string chunkCharactersLabel = "/ChunkCharacters/";
        private const string chunkLabel = "ChunkCharacters_";
        private const string underScore = "_";
        private const string fileExtension = ".zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;
        private EntityQuery worldQuery;
        private EntityQuery charactersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.ReadOnly<Realm>());
            worldQuery = GetEntityQuery(
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<ZoxID>());
			charactersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Character>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var characterSettings = GetSingleton<CharacterSettings>();
            var disableNPCLoading = characterSettings.disableNPCLoading;
            var loadCharacterEventPrefab = CharacterLoadSystem.loadCharacterEventPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var timePerLoad = math.max(0.03, World.Time.DeltaTime * 2); // 0.03
            // UnityEngine.Debug.LogError("Time per load: " + timePerLoad);
            var realmsPath = SaveUtilities.GetRealmsPath();
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<LoadChunkCharacters>(processQuery);
            if (disableNPCLoading)
            {
                PostUpdateCommands2.AddComponent<SpawnChunkCharacters>(processQuery);
                return;
            }
            var planetEntities = worldQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var planetZoxIDs = GetComponentLookup<ZoxID>(true);
            var realmLinks = GetComponentLookup<RealmLink>(true);
            planetEntities.Dispose();
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var realmIDs = GetComponentLookup<ZoxID>(true);
            realmEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithAll<LoadChunkCharacters>()
                .ForEach((Entity e, ref LoadChunkCharacters loadChunkCharacters, in ChunkPosition chunkPosition, in VoxLink voxLink) =>
            {
                if (!realmLinks.HasComponent(voxLink.vox))
                {
                    UnityEngine.Debug.LogError("Cannot Load ChunkCharacters. Vox issue.");
                    return;
                }
                // Get ChunkCharacters save folder
                // If folder exists, then load from there
                var realmEntity = realmLinks[voxLink.vox].realm;
                var worldID = planetZoxIDs[voxLink.vox].id;
                var realmID = realmIDs[realmEntity].id;
                var directory = realmsPath + realmID + worldsLabel + worldID + chunkCharactersLabel;
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                var filepath = directory + chunkLabel + chunkPosition.position.x + underScore + chunkPosition.position.y + underScore + chunkPosition.position.z + fileExtension;
                if (!File.Exists(filepath))
                {
                    PostUpdateCommands2.AddComponent<SpawnChunkCharacters>(e);
                    return;
                }
                var bytes = File.ReadAllBytes(filepath);
                if (bytes[0] == 1)
                {
                    PostUpdateCommands2.AddComponent<SpawnedChunkCharacters>(e);
                }
                else
                {
                    PostUpdateCommands2.AddComponent<SpawnChunkCharacters>(e);
                }
                // load IDs and give to CharacterSpawnSystem.
                // count characters
                var charactersCount = (bytes.Length - 1) / 4;
                loadChunkCharacters.ids = new BlitableArray<int>(charactersCount, Allocator.Persistent);
                var byteIndex = 1;
                for (int i = 0; i < charactersCount; i++)
                {
                    loadChunkCharacters.ids[i] = ByteUtil.GetInt(in bytes, byteIndex);
                    byteIndex += 4;
                }
                // UnityEngine.Debug.LogError("Loaded characters: " + charactersCount);
                #if DEBUG_CHARACTER_SPAWN
                UnityEngine.Debug.LogError("Loaded characters: " + e.Index + ": " + charactersCount);
                #endif
            })  .WithReadOnly(realmIDs)
                .WithReadOnly(planetZoxIDs)
                .WithReadOnly(realmLinks)
                .WithoutBurst().Run();
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var charactersEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var characterZoxIDs = GetComponentLookup<ZoxID>(true);
            charactersEntities.Dispose();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithAll<LoadChunkCharacters>()
                .ForEach((Entity e, int entityInQueryIndex, in LoadChunkCharacters loadChunkCharacters, in ChunkPosition chunkPosition, in ChunkCharacters chunkCharacters, in VoxLink voxLink) =>
            {
                if (!realmLinks.HasComponent(voxLink.vox))
                {
                    return;
                }
                var realmEntity = realmLinks[voxLink.vox].realm;
                for (int i = 0; i < loadChunkCharacters.ids.Length; i++)
                {
                    var characterID = loadChunkCharacters.ids[i];
                    if (characterID == 0)
                    {
                        continue;
                    }
                    var didHave = false;
                    for (int j = 0; j < chunkCharacters.characters.Length; j++)
                    {
                        var characterEntity = chunkCharacters.characters[j];
                        if (characterZoxIDs.HasComponent(characterEntity))
                        {
                            continue;
                        }
                        var otherID = characterZoxIDs[characterEntity].id;
                        if (otherID == characterID)
                        {
                            didHave = true;
                            break;
                        }
                    }
                    if (didHave)
                    {
                        // UnityEngine.Debug.LogError("Not Loading Charact3r: " + characterID);
                        continue;
                    }
                    // UnityEngine.Debug.LogError("characterID: " + characterID);
                    var loadCharacter = new LoadCharacter
                    {
                        characterID = characterID,
                        realm = realmEntity,
                        chunk = e,
                        chunkPosition = chunkPosition.position
                    };
                    var loadEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, loadCharacterEventPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, loadEntity, loadCharacter);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, loadEntity, new DelayEvent(elapsedTime, (1 + i) * timePerLoad));
                }
                loadChunkCharacters.Dispose();
            })  .WithReadOnly(realmLinks)
                .WithReadOnly(characterZoxIDs)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                    // UnityEngine.Debug.LogError("Loading character: " + characterID);
                    // add to component to spawn in parallel
                /*for (int j = 0; j < chunkCharacters.characters.Length; j++)
                {
                    var characterEntity = chunkCharacters.characters[j];
                    if (!characterZoxIDs.HasComponent(characterEntity))
                    {
                        return;
                    }
                }*/
                /*for (int j = 0; j < chunkCharacters.characters.Length; j++)
                {
                    var characterEntity = chunkCharacters.characters[j];
                    if (!characterZoxIDs.HasComponent(characterEntity))
                    {
                        UnityEngine.Debug.LogError("Cannot Load ChunkCharacters. CharacterEntity issue.");
                        return;
                    }
                }*/
                /*PostUpdateCommands.RemoveComponent<LoadChunkCharacters>(e);
                if (disableNPCLoading)
                {
                    PostUpdateCommands.AddComponent<SpawnChunkCharacters>(e);
                    return;
                }*/
                // var characterDirectories = Directory.GetDirectories(chunkCharactersDirectory);
                    //loadChunkCharacters.ids[i] = 0;
                    //var idString = Path.GetFileName(characterDirectories[i]);
                    // loadChunkCharacters.ids[i] = int.Parse(idString);
                //  var chunkCharactersDirectory = worldCharacterDirectory + chunkLabel + chunkPosition.position.x + underScore + chunkPosition.position.y + underScore + chunkPosition.position.z;
                /*var directoryExists = Directory.Exists(chunkCharactersDirectory);
                // Spawn New Chunk Characters
                if (!directoryExists)
                {
                    Directory.CreateDirectory(chunkCharactersDirectory);
                    PostUpdateCommands2.AddComponent<SpawnChunkCharacters>(e);
                }
                else
                {*/
                //}