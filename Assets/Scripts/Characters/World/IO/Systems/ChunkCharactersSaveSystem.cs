using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Characters;
using Zoxel.Characters.Authoring;
using Zoxel.Voxels;
using System.IO;

namespace Zoxel.Characters.World
{
    //! Loads characters inside a chunk or adds spawn event.
	[BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class ChunkCharactersSaveSystem : SystemBase
    {
        private const string worldsLabel = "/Worlds/";
        private const string chunkCharactersLabel = "/ChunkCharacters/";
        private const string chunkLabel = "ChunkCharacters_";
        private const string underScore = "_";
        private const string fileExtension = ".zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity loadCharacterPrefab;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;
        private EntityQuery worldQuery;
        private EntityQuery charactersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var loadCharacterArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(DelayEvent),
                typeof(LoadCharacter));
            loadCharacterPrefab = EntityManager.CreateEntity(loadCharacterArchetype);
            realmQuery = GetEntityQuery(
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.ReadOnly<Realm>());
            worldQuery = GetEntityQuery(
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<ZoxID>());
			charactersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Character>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var characterSettings = GetSingleton<CharacterSettings>();
            var disableNPCLoading = characterSettings.disableNPCLoading;
            var elapsedTime = World.Time.ElapsedTime;
            var timePerLoad = math.max(0.03, World.Time.DeltaTime * 2); // 0.03
            // UnityEngine.Debug.LogError("Time per load: " + timePerLoad);
            var loadCharacterPrefab = this.loadCharacterPrefab;
            var realmsPath = SaveUtilities.GetRealmsPath();
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<SaveChunkCharacters>(processQuery);
            if (disableNPCLoading)
            {
                return;
            }
            var planetEntities = worldQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var planetZoxIDs = GetComponentLookup<ZoxID>(true);
            var realmLinks = GetComponentLookup<RealmLink>(true);
            planetEntities.Dispose();
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var realmIDs = GetComponentLookup<ZoxID>(true);
            realmEntities.Dispose();
            var charactersEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var characterZoxIDs = GetComponentLookup<ZoxID>(true);
            var disableSavings = GetComponentLookup<DisableSaving>(true);
            var playerCharacters = GetComponentLookup<PlayerCharacter>(true);
            charactersEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithAll<SaveChunkCharacters>()
                .ForEach((Entity e, in ChunkCharacters chunkCharacters, in ChunkPosition chunkPosition, in VoxLink voxLink) =>
            {
                if (!realmLinks.HasComponent(voxLink.vox))
                {
                    UnityEngine.Debug.LogError("Cannot SaveChunkCharacters. Vox issue.");
                    return;
                }
                // Get ChunkCharacters save folder
                // If folder exists, then load from there
                var realmEntity = realmLinks[voxLink.vox].realm;
                var worldID = planetZoxIDs[voxLink.vox].id;
                if (!realmIDs.HasComponent(realmEntity))
                {
                    return;
                }
                var realmID = realmIDs[realmEntity].id;
                var directory = realmsPath + realmID + worldsLabel + worldID + chunkCharactersLabel;
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                var filepath = directory + chunkLabel + chunkPosition.position.x + underScore + chunkPosition.position.y + underScore + chunkPosition.position.z + fileExtension;
                var filesize = 1 + chunkCharacters.characters.Length * 4;
                var bytes = new NativeArray<byte>(filesize, Allocator.Temp);
                if (HasComponent<SpawnedChunkCharacters>(e))
                {
                    bytes[0] = 1;
                }
                else
                {
                    bytes[0] = 0;
                }
                var byteIndex = 1;
                // UnityEngine.Debug.LogError("Saved characters: " + chunkCharacters.characters.Length);
                for (int i = 0; i < chunkCharacters.characters.Length; i++)
                {
                    var characterEntity = chunkCharacters.characters[i];
                    if (disableSavings.HasComponent(characterEntity) || playerCharacters.HasComponent(characterEntity))
                    {
                        continue;
                    }
                    var characterID = 0;
                    if (characterZoxIDs.HasComponent(characterEntity))
                    {
                        characterID = characterZoxIDs[characterEntity].id;
                    }
                    ByteUtil.SetInt(ref bytes, byteIndex, characterID);
                    byteIndex += 4;
                }
                // UnityEngine.Debug.LogError("SaveChunkCharacters [" + filepath + "]");
                var bytesArray = bytes.ToArray();
                #if WRITE_ASYNC
                File.WriteAllBytesAsync(filepath, bytesArray);
                #else
                File.WriteAllBytes(filepath, bytesArray);
                #endif
                bytes.Dispose();
                #if DEBUG_CHARACTER_SPAWN
                UnityEngine.Debug.LogError("Loaded characters: " + e.Index + ": " + characterDirectories.Length);
                #endif
            })  .WithReadOnly(realmIDs)
                .WithReadOnly(planetZoxIDs)
                .WithReadOnly(realmLinks)
                .WithReadOnly(disableSavings)
                .WithReadOnly(playerCharacters)
                .WithoutBurst().Run();
        }
    }
}
                // var characterDirectories = Directory.GetDirectories(chunkCharactersDirectory);
                    //loadChunkCharacters.ids[i] = 0;
                    //var idString = Path.GetFileName(characterDirectories[i]);
                    // loadChunkCharacters.ids[i] = int.Parse(idString);
                //  var chunkCharactersDirectory = worldCharacterDirectory + chunkLabel + chunkPosition.position.x + underScore + chunkPosition.position.y + underScore + chunkPosition.position.z;
                /*var directoryExists = Directory.Exists(chunkCharactersDirectory);
                // Spawn New Chunk Characters
                if (!directoryExists)
                {
                    Directory.CreateDirectory(chunkCharactersDirectory);
                    PostUpdateCommands2.AddComponent<SpawnChunkCharacters>(e);
                }
                else
                {*/
                //}