using Unity.Entities;

namespace Zoxel.Characters.World
{
    //! Loads Characters in Chunk
    public struct LoadChunkCharacters : IComponentData
    {
        public BlitableArray<int> ids;

        public void Dispose()
        {
            ids.Dispose();
        }
    }
}