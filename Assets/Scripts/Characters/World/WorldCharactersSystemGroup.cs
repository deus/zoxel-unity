﻿using Unity.Entities;

namespace Zoxel.Characters.World
{
    //! Holds ChunkCharacter things
    [UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class WorldCharactersSystemGroup : ComponentSystemGroup { }
}