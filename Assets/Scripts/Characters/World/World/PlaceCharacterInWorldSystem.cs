using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms; // SetTransformPosition
using Zoxel.Movement;   // Body
using Zoxel.Voxels;     // Chunks
using Zoxel.Worlds;     // WorldsManager planet radius
using Zoxel.Characters; // CharacterMakerCharacter

namespace Zoxel.Characters.World
{
    [BurstCompile, UpdateInGroup(typeof(WorldCharactersSystemGroup))]
    public partial class PlaceCharacterInWorld2System : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DisableForce>(),
                ComponentType.ReadOnly<PlaceCharacterInWorld>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AddComponent<DisableForce>(processQuery);
        }
    }
    [BurstCompile, UpdateInGroup(typeof(WorldCharactersSystemGroup))]
    public partial class PlaceCharacterInWorld3System : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmsQuery;

        protected override void OnCreate()
        {
            realmsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<WorldLinks>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var realmEntities = realmsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var worldLinks = GetComponentLookup<WorldLinks>(true);
            realmEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<SetTransformPosition>()
                .ForEach((ref VoxLink voxLink, in PlaceCharacterInWorld placeCharacterInWorld, in RealmLink realmLink) =>
            {
                if (voxLink.vox.Index == 0 && worldLinks.HasComponent(realmLink.realm))
                {
                    var worldLinks2 = worldLinks[realmLink.realm];
                    if (worldLinks2.worlds.Length > 0)
                    {
                        voxLink.vox = worldLinks2.worlds[0];
                    }
                }
            })  .WithReadOnly(worldLinks)
                .ScheduleParallel(Dependency);
        }
    }
    //! Positions character in world.
    /**
    *   Optimized System; uses ChunkLinks for getting chunk.
    *   \todo Place on different place on planet surface.
    */
    [BurstCompile, UpdateInGroup(typeof(WorldCharactersSystemGroup))]
    public partial class PlaceCharacterInWorldSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.Exclude<DestroyEntity>());
            chunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<EntityBusy>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkPosition>(),
                ComponentType.ReadOnly<ChunkNeighbors>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var seed = (uint) IDUtil.GenerateUniqueID();
            // position height where we check from
            var heightAddition = WorldsManager.instance.worldsSettings.planetHeightRadius + 4 + 16; // 18; // 8
            var planetRadius = WorldsManager.instance.worldsSettings.planetRadius;
            var chunkHeight = VoxelManager.instance.voxelSettings.voxelDimensions.y;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunkLinks2 = GetComponentLookup<ChunkLinks>(true);
            var voxScales = GetComponentLookup<VoxScale>(true);
            //voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunks = GetComponentLookup<Chunk>(true);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            var chunkNeighbors2 = GetComponentLookup<ChunkNeighbors>(true);
            var chunksBusy = GetComponentLookup<EntityBusy>(true);
            //chunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<SetTransformPosition>()
                .ForEach((Entity e, int entityInQueryIndex, ref PlaceCharacterInWorld placeCharacterInWorld, in Body body, in VoxLink voxLink, in Translation translation) =>
            {
                int3 position;
                if (!voxScales.HasComponent(voxLink.vox))
                {
                    return;
                }
                var voxScale = voxScales[voxLink.vox].scale;
                // UnityEngine.Debug.LogError("~PlaceCharacterInWorld~");
                if (!HasComponent<RevivingEntity>(e))
                {
                    var streamPosition = new float3(0.5f, planetRadius + heightAddition, 0.5f);
                    streamPosition.y *= voxScale.y;
                    streamPosition.y += body.size.y;
                    if (translation.Value.x != streamPosition.x || translation.Value.y != streamPosition.y || translation.Value.z != streamPosition.z)
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetTransformPosition(streamPosition));
                        // UnityEngine.Debug.LogError("New Stream Position: " + streamPosition + ":" + VoxelUtilities.GetChunkPositionInt((int) streamPosition.y, 16));
                        if (HasComponent<DisabledChunkStreamPoint>(e))
                        {
                            PostUpdateCommands.RemoveComponent<DisabledChunkStreamPoint>(entityInQueryIndex, e);
                        }
                        return;
                    }
                }
                var chunkLinks = chunkLinks2[voxLink.vox];
                // get placement chunk position
                var bodySizeIncrease = (int) math.ceil(body.size.y);
                var targetChunkPositionY = (planetRadius + heightAddition + bodySizeIncrease) / chunkHeight;
                placeCharacterInWorld.chunkPosition = new int3(0, targetChunkPositionY, 0);  // y = 1
                var targetChunkPosition = placeCharacterInWorld.chunkPosition;
                // Find chunk using targetChunkPosition!
                if (!HasComponent<Chunk>(placeCharacterInWorld.chunk))
                {
                    Entity chunkEntity;
                    if (chunkLinks.chunks.TryGetValue(targetChunkPosition, out chunkEntity))
                    {
                        placeCharacterInWorld.chunk = chunkEntity;
                    }
                }
                var chunkEntity2 = placeCharacterInWorld.chunk;
                if (!chunks.HasComponent(chunkEntity2) || chunksBusy.HasComponent(chunkEntity2))
                    // HasComponent<Chunk>(placeCharacterInWorld.chunk) || HasComponent<InitializeEntity>(placeCharacterInWorld.chunk) || HasComponent<EntityBusy>(placeCharacterInWorld.chunk))
                {
                    // UnityEngine.Debug.LogError("Chunk not found at all: " + placeCharacterInWorld.chunkPosition + " out of " + vox.chunks.Length + " chunks.");
                    // UnityEngine.Debug.LogError("Chunk still loading.");
                    return;
                }
                var random = new Random();
                var seed2 = (uint) (seed + entityInQueryIndex * 20);
                random.InitState(seed2);
                var chunksTestCount = 0;
                while (chunksTestCount <= 12)
                {
                    var chunk = chunks[chunkEntity2];
                    var chunkPosition = chunkPositions[chunkEntity2];
                    bool success;
                    position = WorldUtilities.FindNewPosition(in chunk, ref random, out success);
                    if (success)
                    {
                        PostUpdateCommands.RemoveComponent<PlaceCharacterInWorld>(entityInQueryIndex, e);
                        position += chunkPosition.GetVoxelPosition(chunk.voxelDimensions);
                        var spawnPosition = position.ToFloat3();
                        spawnPosition.x *= voxScale.x;
                        spawnPosition.y *= voxScale.y;
                        spawnPosition.z *= voxScale.z;
                        // spawnPosition += bodySizeIncrease; // body.size;
                        spawnPosition += 2f * body.size;
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetTransformPosition(spawnPosition));
                        if (HasComponent<CharacterMakerCharacter>(e))
                        {
                            PostUpdateCommands.AddComponent<SaveNewCharacter>(entityInQueryIndex, e);
                        }
                        // UnityEngine.Debug.LogError("Placing Character at: " + position);
                        break;
                    }
                    else
                    {
                        // move chunk position down
                        // UnityEngine.Debug.LogError("Could not find standing position in chunk: " + placeCharacterInWorld.chunkPosition);
                        var chunkNeighbors = chunkNeighbors2[chunkEntity2];
                        placeCharacterInWorld.chunk = chunkNeighbors.chunkDown;
                        placeCharacterInWorld.chunkPosition = placeCharacterInWorld.chunkPosition.Down();
                        chunkEntity2 = placeCharacterInWorld.chunk;
                        if (!chunks.HasComponent(chunkEntity2))
                        {
                            return;
                        }
                    }
                    chunksTestCount++;
                }
                if (chunksTestCount == 13)
                {
                    UnityEngine.Debug.LogError("Failed to place character in world.");
                }
                /*else
                {
                    UnityEngine.Debug.LogError("Chunk still loading 2.");
                }*/
                if (voxEntities.Length > 0) { var e2 = voxEntities[0]; }
                if (chunkEntities.Length > 0) { var e2 = chunkEntities[0]; }
            })  .WithReadOnly(chunkLinks2)
                .WithReadOnly(voxScales)
                .WithReadOnly(chunks)
                .WithReadOnly(chunkPositions)
                .WithReadOnly(chunkNeighbors2)
                .WithReadOnly(chunksBusy)
                .WithReadOnly(voxEntities)
                .WithDisposeOnCompletion(voxEntities)
                .WithReadOnly(chunkEntities)
                .WithDisposeOnCompletion(chunkEntities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}