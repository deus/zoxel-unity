using Unity.Mathematics;
using Zoxel.Voxels;

namespace Zoxel.Characters.World
{
    public static class WorldUtilities
    {
        // used for spawning player atm
        //		Need to replace this, keeping ChunkBuilderComponent on chunk until chunk renders have finished loading
        //		Simply remove it from them when all chunk renders are done
        // Clear list once I spawn minivoxes
        // if spawning a rotation that's not none, then add to this list

        // idea: get list of possible positions in the y direction
        //			If another one, chose the one where it's on tile rather then on top of the roof
        public static int3 FindNewPosition(in Chunk chunk, ref Random random, out bool success, byte raySide)
        {
            var voxelDimensions = chunk.voxelDimensions;
			var isPastAir = false;
            if (raySide == PlanetSide.Down)
            {
                var localPosition = new int3((int)math.floor(random.NextInt(0, voxelDimensions.x - 1)), 0,
                    (int)math.floor(random.NextInt(0, voxelDimensions.z - 1)));
                for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                {
                    var isAir = chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] == 0;
                    if (isAir)
                    {
                        isPastAir = true;
                    }
                    else if (isPastAir)
                    {
                        var newPosition = localPosition.Down();
                        success = true;
                        return newPosition;
                    }
                }
            }
            else if (raySide == PlanetSide.Up)
            {
                var localPosition = new int3((int)math.floor(random.NextInt(0, voxelDimensions.x - 1)), 0,
                    (int)math.floor(random.NextInt(0, voxelDimensions.z - 1)));
                for (localPosition.y = voxelDimensions.y - 1; localPosition.y >= 0; localPosition.y--)
                {
                    var isAir = chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] == 0;
                    if (isAir)
                    {
                        isPastAir = true;
                    }
                    else if (isPastAir)
                    {
                        var newPosition = localPosition.Up();
                        success = true;
                        return newPosition;
                    }
                }
            }
            else if (raySide == PlanetSide.Left)
            {
                var localPosition = new int3(0, (int)math.floor(random.NextInt(0, voxelDimensions.y - 1)),
                    (int)math.floor(random.NextInt(0, voxelDimensions.z - 1)));
                for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                {
                    var isAir = chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] == 0;
                    if (isAir)
                    {
                        isPastAir = true;
                    }
                    else if (isPastAir)
                    {
                        var newPosition = localPosition.Left();
                        success = true;
                        return newPosition;
                    }
                }
            }
            else if (raySide == PlanetSide.Right)
            {
                var localPosition = new int3(0, (int)math.floor(random.NextInt(0, voxelDimensions.y - 1)),
                    (int)math.floor(random.NextInt(0, voxelDimensions.z - 1)));
                for (localPosition.x = voxelDimensions.x - 1; localPosition.x >= 0; localPosition.x--)
                {
                    var isAir = chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] == 0;
                    if (isAir)
                    {
                        isPastAir = true;
                    }
                    else if (isPastAir)
                    {
                        var newPosition = localPosition.Right();
                        success = true;
                        return newPosition;
                    }
                }
            }
            else if (raySide == PlanetSide.Backward)
            {
                var localPosition = new int3((int)math.floor(random.NextInt(0, voxelDimensions.x - 1)),
                    (int)math.floor(random.NextInt(0, voxelDimensions.y - 1)), 0);
                for (localPosition.z = 0; localPosition.z <  voxelDimensions.z; localPosition.z++)
                {
                    var isAir = chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] == 0;
                    if (isAir)
                    {
                        isPastAir = true;
                    }
                    else if (isPastAir)
                    {
                        var newPosition = localPosition.Backward();
                        success = true;
                        return newPosition;
                    }
                }
            }
            else if (raySide == PlanetSide.Forward)
            {
                var localPosition = new int3((int)math.floor(random.NextInt(0, voxelDimensions.x - 1)),
                    (int)math.floor(random.NextInt(0, voxelDimensions.y - 1)), 0);
                for (localPosition.z = voxelDimensions.z - 1; localPosition.z >= 0; localPosition.z--)
                {
                    var isAir = chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)] == 0;
                    if (isAir)
                    {
                        isPastAir = true;
                    }
                    else if (isPastAir)
                    {
                        var newPosition = localPosition.Forward();
                        success = true;
                        return newPosition;
                    }
                }
            }
            success = false;
            return int3.zero;
        }

        public static int3 FindNewPosition(in Chunk chunk, ref Random random, out bool success)
        {
			// idea: get list of possible positions in the y direction
			//			If another one, chose the one where it's on tile rather then on top of the roof
            int voxelIndex;
            int3 checkVoxelPosition;
            //var randomPositionX = (int)math.floor(random.NextInt(0, voxelDimensions.x - 1));
            //var randomPositionZ = (int)math.floor(random.NextInt(0, voxelDimensions.z - 1));
            var voxelDimensions = chunk.voxelDimensions;
            var randomPositionX = (int)math.floor(random.NextInt(1, voxelDimensions.x - 2));
            var randomPositionZ = (int)math.floor(random.NextInt(1, voxelDimensions.z - 2));
            var maxHeight = voxelDimensions.y - 1;
			var isPastAir = false;
            for (var j = voxelDimensions.y - 1; j >= 0; j--)
            {
                // if not air, pick j + 1
                checkVoxelPosition = new int3(randomPositionX, j, randomPositionZ);
                voxelIndex = VoxelUtilities.GetVoxelArrayIndex(checkVoxelPosition, voxelDimensions);
				var isSolid = chunk.voxels[voxelIndex] != 0;
				if (!isSolid)
				{
					isPastAir = true;
				}
				else if (isSolid && isPastAir)
				{
                    success = true;
                    var newPosition = new int3(checkVoxelPosition.x, j + 1, checkVoxelPosition.z);
                    return newPosition;
				}
            }
            success = false;
            return int3.zero;
        }

        public static int3 FindNewPosition(in Chunk chunk, int3 positionXZ, out bool success)
        {
            var voxelDimensions = chunk.voxelDimensions;
			if (positionXZ.x < 0 || positionXZ.z < 0 || positionXZ.x >= voxelDimensions.x || positionXZ.z >= voxelDimensions.z)
			{
				success = false;
				return positionXZ;
			}
            int voxelIndex;
            int3 checkVoxelPosition;
            var randomPositionX = positionXZ.x;
            var randomPositionZ =  positionXZ.z;
			var isPastAir = false;
            for (var j = (int)voxelDimensions.y - 1; j >= 0; j--)
            {
                // if not air, pick j + 1
                checkVoxelPosition = new int3(randomPositionX, j, randomPositionZ);
                voxelIndex = VoxelUtilities.GetVoxelArrayIndex(checkVoxelPosition, voxelDimensions);
				var isSolid = chunk.voxels[voxelIndex] != 0;
				if (!isSolid)
				{
					isPastAir = true;
				}
				else if (isSolid && isPastAir)
				{
                    var newPosition = new int3(checkVoxelPosition.x, j + 1, checkVoxelPosition.z);
					//UnityEngine.Debug.LogError("New Standing Position found at: " + newPosition);
					success = true;
                    return newPosition;
				}
            }
			success = false;
            return int3.zero;
        }
    }
}