using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Characters;
using Zoxel.Characters.Authoring;
using Zoxel.Voxels;

namespace Zoxel.Characters.World
{
    //! Spawns characters inside a chunk.
    /**
    *   Spawns characters from a chunk when characters folder doesn't exist in directory.
    *   \todo Use CharacterSpawnMap2D data instead of choosing what spawns here.
    *   \todo Spawn characters directly here, in bulk too.
    */
    [BurstCompile, UpdateInGroup(typeof(WorldCharactersSystemGroup))]
    public partial class ChunkCharacterSpawnSystem : SystemBase
    {
        private int mintmanID = 666;
        private int slimeID;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxScale>());
            RequireForUpdate(processQuery);
            RequireForUpdate(voxesQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var characterSettings = GetSingleton<CharacterSettings>();
            var spawnOnlySkeletons = characterSettings.spawnOnlySkeletons;
            var disableSpawningHumanoids = characterSettings.disableSpawningHumanoids;
            var characterDensity = characterSettings.characterDensity;
            var mintmanSpawnChance = characterSettings.mintmanSpawnChance;
            var mintmanID = this.mintmanID;
            var slimeID2 = slimeID;
            var positionShift = new float3(0.5f, 1.5f, 0.5f);   // this should beb ased on size of npc
            var timePerLoad = math.max(0.03, World.Time.DeltaTime * 2); // 0.03
            var elapsedTime = World.Time.ElapsedTime;
            var spawnCharacterPrefab = CharactersSystemGroup.spawnCharacterPrefab;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<SpawnChunkCharacters>(processQuery);
            PostUpdateCommands2.AddComponent<SpawnedChunkCharacters>(processQuery);
            // PostUpdateCommands2.AddComponentForEntityQuery<SaveChunkCharacters>(processQuery);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxScales = GetComponentLookup<VoxScale>(true);
            var realmLinks = GetComponentLookup<RealmLink>(true);
            voxEntities.Dispose();
            var chunkEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var chunkGravityQuadrants = GetComponentLookup<ChunkGravityQuadrant>(true);
            chunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, EntityBusy>()
                .WithAll<SpawnChunkCharacters>()
                .ForEach((Entity e, int entityInQueryIndex, in ChunkCharacters chunkCharacters, in ChunkPosition chunkPosition, in Chunk chunk,
                    in VoxLink voxLink, in ChunkGravity chunkGravity, in ZoxID zoxID) =>
            {
                if (!voxScales.HasComponent(voxLink.vox))
                {
                    return;
                }
                var chunkGravityQuadrant = chunkGravityQuadrants[e];
                var realmEntity = realmLinks[voxLink.vox].realm;
                var voxScale = voxScales[voxLink.vox].scale;
                var hasHeightsDown = chunkGravityQuadrant.quadrantA.GetBoolA();
                var hasHeightsUp = chunkGravityQuadrant.quadrantB.GetBoolA();
                var hasHeightsLeft = chunkGravityQuadrant.quadrantA.GetBoolB();
                var hasHeightsRight = chunkGravityQuadrant.quadrantB.GetBoolB();
                var hasHeightsBackward = chunkGravityQuadrant.quadrantA.GetBoolC();
                var hasHeightsForward = chunkGravityQuadrant.quadrantB.GetBoolC();
                if (!(hasHeightsDown || hasHeightsUp || hasHeightsLeft || hasHeightsRight || hasHeightsBackward || hasHeightsForward))
                {
                    return;
                }
                var planetSide = PlanetSide.None;
                if (hasHeightsDown)
                {
                    planetSide = PlanetSide.Down;
                }
                else if (hasHeightsUp)
                {
                    planetSide = PlanetSide.Up;
                }
                else if (hasHeightsLeft)
                {
                    planetSide = PlanetSide.Left;
                }
                else if (hasHeightsRight)
                {
                    planetSide = PlanetSide.Right;
                }
                else if (hasHeightsBackward)
                {
                    planetSide = PlanetSide.Backward;
                }
                else if (hasHeightsForward)
                {
                    planetSide = PlanetSide.Forward;
                }
                // var voxelDimensions = chunkDimensions[voxLink.vox].voxelDimensions;
                var planet = voxLink.vox;
                var voxelDimensions = chunk.voxelDimensions;
                var chunkPositionInt3 = chunkPosition.GetVoxelPosition(voxelDimensions);
                var chunkPosition2 = chunkPositionInt3.ToFloat3();
                var realChunkPosition = chunkPosition2 + new float3(voxelDimensions.x / 2f, 0, voxelDimensions.z / 2f);
                var random = new Random();
                random.InitState((uint)(zoxID.id + (elapsedTime * 16)));
                var characterDensity2 = random.NextInt(characterDensity.x, characterDensity.y);
                for (int i = chunkCharacters.characters.Length; i < characterDensity2; i++)
                {
                    realChunkPosition.y = 0;
                    bool success;
                    var spawnPosition2 = WorldUtilities.FindNewPosition(in chunk, ref random, out success, planetSide);
                    if (!success)
                    {
                        // PostUpdateCommands.RemoveComponent<SpawnChunkCharacters>(entityInQueryIndex, e);
                        return;
                    }
                    var spawnPosition = chunkPosition2 + spawnPosition2.ToFloat3() + positionShift;
                    spawnPosition.x *= voxScale.x;
                    spawnPosition.y *= voxScale.y;
                    spawnPosition.z *= voxScale.z;
                    var gravityIn = chunkGravity.gravity.GetValueDepthDifference(spawnPosition2.ToByte3(), 4, chunkGravity.depth);
                    var spawnRotation = BlockRotation.GetRotation((byte)(gravityIn - 1));
                    // UnityEngine.Debug.LogError("Spawning Characterwith gravityIn: " + gravityIn);
                    // UnityEngine.Debug.DrawLine(spawnPosition, spawnPosition + math.mul(spawnRotation, new float3(0, 1f, 0)), UnityEngine.Color.red, 20f);
                    // UnityEngine.Debug.DrawLine(spawnPosition - math.mul(spawnRotation, new float3(0.5f, 0, 0)), spawnPosition + math.mul(spawnRotation, new float3(0.5f, 0, 0)), UnityEngine.Color.cyan, 20f);
                    var spawnCharacter = new SpawnCharacter
                    {
                        chunk = e,
                        planet = planet,
                        realm = realmEntity,
                        position = spawnPosition,
                        rotation = spawnRotation,
                        quadrant = gravityIn
                    };
                    if (!disableSpawningHumanoids && (spawnOnlySkeletons || random.NextInt(100) <= mintmanSpawnChance))
                    {
                        spawnCharacter.prefabType = CharacterPrefabType.npcSkeletonNew;
                        // UnityEngine.Debug.LogError("Spawned Mintman");
                    }
                    else
                    {
                        spawnCharacter.prefabType = CharacterPrefabType.blobNew;
                    }
                    var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnCharacterPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, spawnCharacter);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new DelayEvent(elapsedTime, (1 + i) * timePerLoad));
                    // PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new ClanLink(new Entity(), 0));
                    // if multiple of same type - add SpawnMultipleCharacters on top
                }
            })  .WithReadOnly(voxScales)
                .WithReadOnly(realmLinks)
                .WithReadOnly(chunkGravityQuadrants)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                // PostUpdateCommands.RemoveComponent<SpawnChunkCharacters>(entityInQueryIndex, e);
                    // var spawnRotation = quaternion.identity;
                    /*if (planetSide == PlanetSide.Up)
                    {
                        spawnRotation = quaternion.identity;
                    }
                    else if (planetSide == PlanetSide.Down)
                    {
                        spawnRotation = flippedCharacter;
                    }
                    else if (planetSide == PlanetSide.Left)
                    {
                        spawnRotation = rotation2;
                    }
                    else if (planetSide == PlanetSide.Right)
                    {
                        spawnRotation = rotation1;
                    }
                    else if (planetSide == PlanetSide.Backward)
                    {
                        spawnRotation = rotation3;
                    }
                    else if (planetSide == PlanetSide.Forward)
                    {
                        spawnRotation = rotation4;
                    }*/
            /*var degreesToRadians = ((math.PI * 2) / 360f);
            var flippedCharacter = quaternion.EulerXYZ(new float3(180, 0, 0) * degreesToRadians);
            var rotation1 = quaternion.EulerXYZ(new float3(0, 0, -90) * degreesToRadians);
            var rotation2 = quaternion.EulerXYZ(new float3(0, 0, 90) * degreesToRadians);
            var rotation3 = quaternion.EulerXYZ(new float3(-90, 0, 0) * degreesToRadians);
            var rotation4 = quaternion.EulerXYZ(new float3(90, 0, 0) * degreesToRadians);  */