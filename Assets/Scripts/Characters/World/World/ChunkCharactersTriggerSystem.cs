using Unity.Entities;
using Unity.Burst;
using Zoxel.Voxels;
using Zoxel.Characters;
using Zoxel.Voxels.Authoring;
using Zoxel.Characters.Authoring;

namespace Zoxel.Characters.World
{
	//! An event system for when a chunk changes view distance from camera.
	[BurstCompile, UpdateInGroup(typeof(WorldCharactersSystemGroup))]
    public partial class ChunkCharactersTriggerSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<ModelSettings>();
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
            var characterSettings = GetSingleton<CharacterSettings>();
			if (characterSettings.disableNPCCharacters)
			{
				return;
			}
            var modelSettings = GetSingleton<ModelSettings>();
            var characterRenderDistance = modelSettings.characterRenderDistance;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
			Dependency = Entities
				.WithNone<DestroyEntity>()
				.WithNone<LoadChunkCharacters, SpawnChunkCharacters, DestroyChunkCharacters>()
                .WithChangeFilter<ChunkDivision>()
				.WithAll<PlanetChunk, ChunkHasCharacters>()
				.ForEach((Entity e, int entityInQueryIndex, in ChunkDivision chunkDivision) =>
			{
				var newViewDistance = chunkDivision.viewDistance;
				if (newViewDistance > characterRenderDistance)
				{
					PostUpdateCommands.RemoveComponent<ChunkHasCharacters>(entityInQueryIndex, e);
					PostUpdateCommands.AddComponent<DestroyChunkCharacters>(entityInQueryIndex, e);
				}
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
			Dependency = Entities
				.WithNone<ChunkHasCharacters>()
				.WithNone<DestroyEntity>()
				.WithNone<LoadChunkCharacters, SpawnChunkCharacters, DestroyChunkCharacters>()
                .WithChangeFilter<ChunkDivision>()
				.WithAll<PlanetChunk>()
				.ForEach((Entity e, int entityInQueryIndex, in ChunkDivision chunkDivision) =>
			{
				var newViewDistance = chunkDivision.viewDistance;
				if (newViewDistance <= characterRenderDistance)
				{
					PostUpdateCommands.AddComponent<ChunkHasCharacters>(entityInQueryIndex, e);
					PostUpdateCommands.AddComponent<LoadChunkCharacters>(entityInQueryIndex, e);
				}
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}