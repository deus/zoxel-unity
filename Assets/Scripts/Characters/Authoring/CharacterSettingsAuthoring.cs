using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Zoxel.Characters.Authoring
{
    //! Manages character settings.
    /**
    *   \todo limit characters within their chunk spawned in
    */
    // [GenerateAuthoringComponent]
    public struct CharacterSettings : IComponentData
    {
        public int2 characterDensity;   // 1, 4
        public int mintmanSpawnChance;  // 2
        public int maxCharacters;       // 512
        public bool chaosMode;
        public bool disableNPCCharacters;
        public bool spawnOnlySkeletons;
        public bool spawnOnlyTraders;
        public bool disableSpawningHumanoids;
        public bool disableDestroyCharacters;
        public bool disableNPCLoading;
        public bool disableNPCSaving;
    }

    public class CharacterSettingsAuthoring : MonoBehaviour
    {
        public int2 characterDensity;   // 1, 4
        public int mintmanSpawnChance;  // 2
        public int maxCharacters;       // 512
        public bool chaosMode;
        public bool disableNPCCharacters;
        public bool spawnOnlySkeletons;
        public bool spawnOnlyTraders;
        public bool disableSpawningHumanoids;
        public bool disableDestroyCharacters;
        public bool disableNPCLoading;
        public bool disableNPCSaving;
    }

    public class CharacterSettingsAuthoringBaker : Baker<CharacterSettingsAuthoring>
    {
        public override void Bake(CharacterSettingsAuthoring authoring)
        {
            AddComponent(new CharacterSettings
            {
                characterDensity = authoring.characterDensity,
                mintmanSpawnChance = authoring.mintmanSpawnChance,
                maxCharacters = authoring.maxCharacters,
                chaosMode = authoring.chaosMode,
                disableNPCCharacters = authoring.disableNPCCharacters,
                spawnOnlySkeletons = authoring.spawnOnlySkeletons,
                spawnOnlyTraders = authoring.spawnOnlyTraders,
                disableSpawningHumanoids = authoring.disableSpawningHumanoids,
                disableDestroyCharacters = authoring.disableDestroyCharacters,
                disableNPCLoading = authoring.disableNPCLoading,
                disableNPCSaving = authoring.disableNPCSaving
            });       
        }
    }
}

        /*public void DrawUI()
        {
            GUILayout.Label(" [characters] ");

            GUILayout.Label("   Max Characters");
            maxCharacters = int.Parse(GUILayout.TextField(maxCharacters.ToString()));
            GUILayout.Label("   Characters Per Chunk (min/max)");
            characterDensity.x = int.Parse(GUILayout.TextField(characterDensity.x.ToString()));
            characterDensity.y = int.Parse(GUILayout.TextField(characterDensity.y.ToString()));
            GUILayout.Label("   Humanoid Spawn Chance");
            mintmanSpawnChance = int.Parse(GUILayout.TextField(mintmanSpawnChance.ToString()));
        }

        public void DrawDisableUI()
        {
            GUILayout.Label(" [cheats] ");
            chaosMode = GUILayout.Toggle(chaosMode, "Chaos Mode");
            GUILayout.Label(" [disables] ");
            disableNPCCharacters = GUILayout.Toggle(disableNPCCharacters, "Disable NPCs");
            spawnOnlySkeletons = GUILayout.Toggle(spawnOnlySkeletons, "Spawn Only Humanoids");
            disableSpawningHumanoids = GUILayout.Toggle(disableSpawningHumanoids, "Disable Spawning Humanoids");
            disableDestroyCharacters = GUILayout.Toggle(disableDestroyCharacters, "Disable Destroy Characters");
        }*/