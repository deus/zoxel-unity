using Unity.Entities;

namespace Zoxel.Characters
{
    //! An event from the ui for loading a character into the world.
    public struct LoadCharacter : IComponentData
    {
        public int characterID;
        public Entity realm;
        // player
        public Entity player;
        // npc
        public Entity chunk;
        public int3 chunkPosition;
    }
}