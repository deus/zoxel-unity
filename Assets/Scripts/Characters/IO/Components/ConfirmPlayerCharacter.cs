using Unity.Entities;

namespace Zoxel.Characters
{
    //! An event from the ui for loading a character into the world.
    public struct ConfirmPlayerCharacter : IComponentData
    {
        public byte state;
        public Entity player;
        public Entity character;

        public ConfirmPlayerCharacter(Entity player, Entity character)
        {
            this.state = 0;
            this.player = player;
            this.character = character;
        }
    }
}