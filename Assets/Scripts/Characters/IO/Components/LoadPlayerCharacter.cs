using Unity.Entities;

namespace Zoxel.Characters
{
    //! An event from the ui for loading a character into the world.
    public struct LoadPlayerCharacter : IComponentData
    {
        public byte state;
        public Entity realm;
        public Entity player;
        public int characterID;
        public int worldID;

        public LoadPlayerCharacter(Entity realm, Entity player, int characterID)
        {
            this.state = 0;
            this.realm = realm;
            this.player = player;
            this.characterID = characterID;
            this.worldID = 0;
        }
    }
}