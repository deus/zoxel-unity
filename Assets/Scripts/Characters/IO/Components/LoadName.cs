

using Unity.Entities;

namespace Zoxel.Characters
{
    public struct LoadName : IComponentData
    {
        public Text loadPath;
        public AsyncFileReader reader;

        public void Dispose()
        {
            loadPath.Dispose();
            reader.Dispose();
        }
    }
}