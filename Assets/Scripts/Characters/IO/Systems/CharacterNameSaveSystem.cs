

using Unity.Entities;
using System;
using System.IO;

namespace Zoxel.Characters
{
    //! Saves the name into a file.
    /**
    *   \todo Compile Bytes in parallel/burst.
    *   - Save System -
    */
    [UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class CharacterNameSaveSystem : SystemBase
    {
        private const string filename = "CharacterName.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands.RemoveComponent<SaveName>(processQuery);
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity, InitializeSaver>()
                .WithAll<SaveName>()
                .ForEach((in ZoxName zoxName, in EntitySaver entitySaver) =>
            {
                var directoryPath = entitySaver.GetPath();
                if (!Directory.Exists(directoryPath))
                {
                    return;
                }
                var filepath = directoryPath + filename;
                try
                {
                    var bytes = zoxName.name.textData.ToArray();
                    #if WRITE_ASYNC
                    File.WriteAllBytesAsync(filepath, bytes);
                    #else
                    File.WriteAllBytes(filepath, bytes);
                    #endif
                }
                catch (Exception exception)
                {
                    UnityEngine.Debug.LogError("Error Saving Name: " + exception);
                }
            }).WithoutBurst().Run();
        }
    }
}