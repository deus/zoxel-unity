using Unity.Entities;
using Unity.Jobs;
using System.IO;

namespace Zoxel.Characters
{
    //! Saves an entities PrefabType data.
    /**
    *   - Save System -
    */
    [UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class CharacterPrefabTypeSaveSystem : SystemBase
    {
        const string filename = "PrefabType.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }
        
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<SavePrefabType>(processQuery);
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DeadEntity, DestroyEntity>()
                .WithNone<InitializeSaver, DisableSaving>()
                .WithAll<SavePrefabType>()
                .ForEach((in EntitySaver entitySaver, in PrefabType prefabType) =>
            {
                var filepath = entitySaver.GetPath() + filename;
                var saveType = prefabType.type;
                if (saveType == CharacterPrefabType.playerNew || saveType == CharacterPrefabType.playerRespawn)
                {
                    saveType = CharacterPrefabType.playerLoading;
                }
                else if (saveType == CharacterPrefabType.blobNew)
                {
                    saveType = CharacterPrefabType.blobLoading;
                }
                else if (saveType == CharacterPrefabType.blobAuthoredNew)
                {
                    saveType = CharacterPrefabType.blobAuthoredLoading;
                }
                else if (saveType == CharacterPrefabType.npcSkeletonNew)
                {
                    saveType = CharacterPrefabType.npcSkeletonLoading;
                }
                var bytes = new byte[] { saveType };
                #if WRITE_ASYNC
                File.WriteAllBytesAsync(filepath, bytes);
                #else
                File.WriteAllBytes(filepath, bytes);
                #endif
            }).WithoutBurst().Run();
        }
    }
}