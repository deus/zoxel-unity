using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;
using Zoxel.Races;
using System;
using System.IO;

namespace Zoxel.Characters
{
    //! Starts the loading of a character by spawning it into the world.
    /**
    *   \todo Optimize. This took 10ms.
    *   - Load System -
    */
    [UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class CharacterLoadSystem : SystemBase
    {
        private const string translationFilename = "Translation.zox";
        private const string prefabTypeFilename = "PrefabType.zox";
        const int floatsLength = 8;
        const int bytesLength = floatsLength * 4;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;
        private EntityQuery worldsQuery;
        private EntityQuery controllerQuery;
        public static Entity loadCharacterEventPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.Exclude<DestroyEntity>());
            worldsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Planet>(),
                ComponentType.Exclude<DestroyEntity>());
            controllerQuery = GetEntityQuery(
                ComponentType.ReadOnly<Player>(),
                ComponentType.ReadOnly<CameraLink>());
            var loadCharacterEventArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(LoadCharacter),
                typeof(DelayEvent),
                typeof(GenericEvent));
            loadCharacterEventPrefab = EntityManager.CreateEntity(loadCharacterEventArchetype);
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var spawnPlayerCharacterPrefab = CharactersSystemGroup.spawnPlayerCharacterPrefab;
            var spawnCharacterPrefab = CharactersSystemGroup.spawnCharacterPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var realmIDs = GetComponentLookup<ZoxID>(true);
            var worldLinks = GetComponentLookup<WorldLinks>(true);
            realmEntities.Dispose();
            var worldEntities = worldsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var worldIDs = GetComponentLookup<ZoxID>(true);
            worldEntities.Dispose();
            var controllerEntities = controllerQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var cameraLinks = GetComponentLookup<CameraLink>(true);
            controllerEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .ForEach((Entity e, in LoadCharacter loadCharacter) =>
            {
                var controllerEntity = loadCharacter.player;
                var characterID = loadCharacter.characterID;
                var realmEntity = loadCharacter.realm;
                var realmID = realmIDs[realmEntity].id;
                var worlds = worldLinks[realmEntity].worlds;
                var worldEntity = new Entity();
                if (worlds.Length > 0)
                {
                    worldEntity = worlds[0];
                }
                var isPlayerCharacter = controllerEntity.Index != 0;
                var directory = SaveUtilities.GetRealmsPath();
                if (isPlayerCharacter)
                {
                    directory += EntitySaver.GetPlayerPath(realmID, characterID);
                }
                else
                {
                    if (!worldIDs.HasComponent(worldEntity))
                    {
                        return;
                    }
                    var worldID = worldIDs[worldEntity].id;
                    directory += EntitySaver.GetCharacterPath(realmID, worldID, characterID);
                }
                var position = new float3();
                var rotation = new quaternion();
                var quadrant = (byte) 0;
                if (isPlayerCharacter)
                {
                    var translationFilepath = directory + translationFilename;
                    // UnityEngine.Debug.LogError("filePath: " + filePath);
                    if (!File.Exists(translationFilepath))
                    {
                        UnityEngine.Debug.LogError("File not found at: " + translationFilepath);
                        return;
                    }
                    var bytes = new byte[0];
                    try
                    {
                        var reader = new BinaryReader(File.OpenRead(translationFilepath)); // File.Open(filePath, FileMode.Create);
                        reader.BaseStream.Position = 0;
                        bytes = reader.ReadBytes(bytesLength);
                        reader.Close();
                        reader.Dispose();
                    }
                    catch (Exception exception)
                    {
                        UnityEngine.Debug.LogError("Exception with File Access (CharacterLoadSystem) at: " + translationFilepath + "\nException is: " + exception);
                        return;
                    }
                    // From bytes get position and rotation of character to load
                    var floats = new float[floatsLength];
                    Buffer.BlockCopy(bytes, 0, floats, 0, bytes.Length);
                    var positionX = floats[0];
                    var positionY = floats[1];
                    var positionZ = floats[2];
                    var rotationX = floats[3];
                    var rotationY = floats[4];
                    var rotationZ = floats[5];
                    var rotationW = floats[6];
                    quadrant = (byte) ((int) floats[7]);
                    position = new int3((int) positionX, (int) positionY, (int) positionZ).ToFloat3();
                    rotation = new quaternion(rotationX, rotationY, rotationZ, rotationW);
                }
                var prefab = spawnCharacterPrefab;
                if (isPlayerCharacter)
                {
                    prefab = spawnPlayerCharacterPrefab;
                }
                var spawnEntity = PostUpdateCommands.Instantiate(prefab);
                PostUpdateCommands.AddComponent<SpawnLoadingCharacter>(spawnEntity);
                var spawnCharacter = new SpawnCharacter
                {
                    quadrant = quadrant,        // set spawn character quadrant
                    spawnID = characterID,
                    planet = worldEntity,
                    position = position,
                    rotation = rotation,
                    realm = realmEntity,
                    chunk = loadCharacter.chunk
                };
                var prefabTypeFilepath = directory + prefabTypeFilename;
                if (File.Exists(prefabTypeFilepath))
                {
                    var bytes2 = File.ReadAllBytes(prefabTypeFilepath);
                    spawnCharacter.prefabType = bytes2[0];
                }
                else
                {
                    spawnCharacter.prefabType = CharacterPrefabType.blobNew;
                }
                PostUpdateCommands.SetComponent(spawnEntity, spawnCharacter);
                if (isPlayerCharacter)
                {
                    var spawnPlayerCharacter = new SpawnPlayerCharacter
                    {
                        controllerEntity = controllerEntity,
                        cameraEntity = cameraLinks[controllerEntity].camera,
                        isConnectCamera = 1
                    };
                    PostUpdateCommands.SetComponent(spawnEntity, spawnPlayerCharacter);
                }
                //UnityEngine.Debug.DrawLine(position, position + math.rotate(rotation, new float3(0, 2, 0)), UnityEngine.Color.red, 10f);
            })  .WithReadOnly(realmIDs)
                .WithReadOnly(worldLinks)
                .WithReadOnly(worldIDs)
                .WithReadOnly(cameraLinks)
                .WithoutBurst().Run();
        }
    }
}
                /*#if DEBUG_CHARACTER_SPAWN
                UnityEngine.Debug.LogError("Loading Character: " + directory);
                #endif
                if (!Directory.Exists(directory))
                {
                    return;
                }*/