using Unity.Entities;
using System.IO;

namespace Zoxel.Characters
{
    //! Creates a single character save folder.
    [UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class CreateCharacterSaveFolderSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeSaver, DisableSaving>()
                .ForEach((Entity e) =>
            {
                PostUpdateCommands.RemoveComponent<InitializeSaver>(e);
            }).WithoutBurst().Run();
            Entities
                .WithNone<CharacterMakerCharacter, PlayerCharacter, DisableSaving>()
                .WithAll<InitializeSaver>()
                .ForEach((Entity e, ref EntitySaver entitySaver, in ZoxID zoxID, in ZoxName zoxName) =>
            {
                PostUpdateCommands.RemoveComponent<InitializeSaver>(e);
                entitySaver.SetPath(zoxID.id, false);
                var directoryPath = entitySaver.GetPath();
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }
            }).WithoutBurst().Run();
            Entities
                .WithNone<CharacterMakerCharacter, DisableSaving>()
                .WithAll<InitializeSaver, PlayerCharacter>()
                .ForEach((Entity e, ref EntitySaver entitySaver, in ZoxID zoxID, in ZoxName zoxName) =>
            {
                PostUpdateCommands.RemoveComponent<InitializeSaver>(e);
                entitySaver.SetPath(zoxID.id, true);
                var directoryPath = entitySaver.GetPath();
                if (!Directory.Exists(directoryPath))
                {
                    Directory.CreateDirectory(directoryPath);
                }
            }).WithoutBurst().Run();
        }
    }
}
                // SaveUtilities.CreateNewCharacterFolder(zoxName.name.ToString(), entitySaver.realmID, zoxID.id, isPlayer);
                // var directory = SaveUtilities.CreateCharacterSaveFile(entitySaver.realmID, zoxID.id, isPlayer);
                // UnityEngine.Debug.LogError("Saved character to: " + directory);