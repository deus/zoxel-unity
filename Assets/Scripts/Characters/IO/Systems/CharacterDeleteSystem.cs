using Unity.Entities;
using System.IO;

namespace Zoxel.Characters
{
    //! Deletes characters when they die.
    [UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class CharacterDeleteSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithNone<PlayerCharacter, SummonedEntity>()
                .WithAll<DyingEntity, EntitySaver>()
                .ForEach((in EntitySaver entitySaver) =>
            {
                var directoryPath = entitySaver.GetPath();
                if (!Directory.Exists(directoryPath))
                {
                    return;
                }
                Directory.Delete(directoryPath, true);
                // UnityEngine.Debug.LogError("Deleted Character Save: " + directoryPath);
            }).WithoutBurst().Run();
        }
    }
}