/*using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using System;
using System.IO;
using Zoxel.Voxels;
using Zoxel.Stats;
// mayb just use IDs in a foldre for characters

namespace Zoxel.Characters
{
	//! Updates charactr lighting when moving around.
	[BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class CharacterMoveFolderSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<EntityBusy>(),
                ComponentType.Exclude<InitializeSaver>(),
                ComponentType.Exclude<PlayerCharacter>(),
                ComponentType.ReadOnly<MoveChunkFolder>(),
                ComponentType.ReadWrite<EntitySaver>(),
                ComponentType.ReadOnly<ChunkLink>(),
                ComponentType.ReadOnly<ZoxID>());
            //processQuery.SetChangedVersionFilter(typeof(ChunkLink));
            chunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkPosition>());
		}

		protected override void OnUpdate()
		{
            if (processQuery.IsEmpty)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands.RemoveComponentForEntityQuery<MoveChunkFolder>(processQuery);
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
			var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            chunkEntities.Dispose();
            // it should have a cooldown for moving directories of 5 seconds
			Entities
                .WithNone<DestroyEntity, DeadEntity, EntityLoading>()
                .WithNone<InitializeSaver, PlayerCharacter>()
                .WithAll<MoveChunkFolder>()
				.ForEach((Entity e, ref EntitySaver entitySaver, in ChunkLink chunkLink, in ZoxID zoxID) =>
			{
                var beforePath = entitySaver.GetPath();
                // beforePath = beforePath.Substring(0, beforePath.Length - 1);
                if (!Directory.Exists(beforePath))
                {
				    UnityEngine.Debug.LogError("entitySaver Directory does not exist [" + e.Index + "]: " + beforePath);
                    return;
                }
                if (!chunkPositions.HasComponent(chunkLink.chunk))
                {
                    return;
                }
                var chunkPosition = chunkPositions[chunkLink.chunk];
                if (!entitySaver.SetPath(zoxID.id, chunkPosition.position))
                {
                    return;
                }
                var afterPath = entitySaver.GetPath();
                //afterPath = afterPath.Substring(0, afterPath.Length - 1);
                if (beforePath == afterPath)
                {
				    // UnityEngine.Debug.LogWarning("CharacterMoveFolderSystem: WTF Directory [" + beforePath + "] has not changed.");
                    return;
                }
                var afterPathChunkPath = Directory.GetParent(afterPath.Substring(0, afterPath.Length - 1)).FullName;   // 
                if (!Directory.Exists(afterPathChunkPath))
                {
				    // UnityEngine.Debug.Log("Creating new Chunk Path Directory [" + afterPathChunkPath + "]");
                    Directory.CreateDirectory(afterPathChunkPath);
                }
                else if (Directory.Exists(afterPath))
                {
				    UnityEngine.Debug.LogError("Move location exists [" + e.Index + "]: [" + afterPath + "] - beforePath: " + beforePath);
				    // UnityEngine.Debug.LogError("Deleting [" + beforePath + "] for newer folder [" + afterPath + "]");
                    // Directory.Delete(afterPath, true);
                    return;
                }
                try
                {
                    Directory.Move(beforePath, afterPath);
                }
                catch (Exception exception)
                {
                    UnityEngine.Debug.LogError("Exception in CharacterMoveFolderSystem: " + exception);
                    return;
                }
            })  .WithReadOnly(chunkPositions)
                .WithoutBurst().Run();
		}
	}
}*/
    /**
    *   \todo When moving chunks, disable/enable rendrs!
    *   \todo Chunks use folder creation to know if they've spawned characters, make it use a file.
    */
    
				//.WithChangeFilter<ChunkLink>()

                //beforePath = beforePath.Substring(0, beforePath.Length - 1);
                //afterPath = afterPath.Substring(0, afterPath.Length - 1);
				/// UnityEngine.Debug.LogError("ChunkLink WithChangeFilter: " + e.Index + "\nbeforePath: [" + beforePath + "]\nafterPath: [" + afterPath + "]\nchunkPosition: " + chunkPosition.position);
                //#if WRITE_ASYNC
                //Directory.MoveAsync(beforePath, afterPath);
                //#else
    /*
    //! Initializes our TransformSaver by opening the BinaryWriter IO stream. 2
    [UpdateAfter(typeof(CharacterMoveFolderSystem))]
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class StatsSaverMovedSystem : SystemBase
    {
        private const string filename = "Translation.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithNone<CharacterMakerCharacter, LoadTransform>()
                .WithAll<MoveChunkFolder>()
                .ForEach((Entity e, TransformSaver transformSaver, in EntitySaver entitySaver) =>
            {
                transformSaver.Dispose();
                // var transformSaver = new TransformSaver();
                var filepath = entitySaver.GetPath() + filename;
                if (transformSaver.CreateStream(filepath))
                {
                }
                PostUpdateCommands.SetSharedComponentManaged(e, transformSaver);
                // UnityEngine.Debug.LogError("Save Location: " + e.Index + " is: " + filePath);
            }).WithoutBurst().Run();
        }
    }

    //! Initializes our TransformSaver by opening the BinaryWriter IO stream. 2
    [UpdateAfter(typeof(CharacterMoveFolderSystem))]
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class TransformSaverMovedSystem : SystemBase
    {
        private const string filename = "Stats.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<DestroyEntity, LoadStats>()
                .WithNone<InitializeSaver, CharacterMakerCharacter, EntityBusy>()
                .WithAll<MoveChunkFolder>()
                .ForEach((Entity e, StatsSaver statsSaver, in EntitySaver entitySaver) =>
            {
                statsSaver.Dispose();
                var filepath = entitySaver.GetPath() + filename;
                if (statsSaver.CreateStream(filepath))
                {
                }
                PostUpdateCommands.SetSharedComponentManaged(e, statsSaver);
            }).WithoutBurst().Run();
        }
    }
    */