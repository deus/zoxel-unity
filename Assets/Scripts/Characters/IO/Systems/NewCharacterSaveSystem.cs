using Unity.Entities;
using Unity.Burst;
using Zoxel.Stats;
using Zoxel.Quests;
using Zoxel.Skills;
using Zoxel.Actions;
using Zoxel.Races;
using Zoxel.Classes;
using Zoxel.Items;
using Zoxel.Transforms.IO;

namespace Zoxel.Characters
{
    //! Adds all the characters save events on.
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class NewCharacterSaveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<GenerateStats, GenerateSkills, GenerateInventory>()
                .WithNone<DisableSaving>()
                .WithAll<SaveNewCharacter>() //, DisableSaving>()
                .ForEach((Entity e, int entityInQueryIndex, ref UserActionLinks userActionLinks) =>
            {
                userActionLinks.selectedAction = 0;
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SelectedActionUpdated(userActionLinks.selectedAction));  // this will save userActionLinks
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<GenerateStats, GenerateSkills, GenerateInventory>()
                .WithNone<DisableSaving>()
                .WithAll<SaveNewCharacter>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<SaveNewCharacter>(entityInQueryIndex, e);
                //! \todo This should be removed somewhere else, where EntityGenerating is pending
                if (HasComponent<EntityBusy>(e))
                {
                    PostUpdateCommands.RemoveComponent<EntityBusy>(entityInQueryIndex, e);
                }
                PostUpdateCommands.AddComponent<InitializeSaver>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<SaveTransform>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<SaveName>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<SaveInventory>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<SaveEquipment>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<SaveUniqueItems>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<SaveStats>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<SaveSkills>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<SaveActions>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<SaveQuestlog>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<SaveRace>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<SaveClass>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<SavePrefabType>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

/*PostUpdateCommands.RemoveComponent<DisableSaving>(entityInQueryIndex, e);
PostUpdateCommands.RemoveComponent<DisableRaycaster>(entityInQueryIndex, e);*/
// PostUpdateCommands.RemoveComponent<CharacterMakerCharacter>(entityInQueryIndex, e);
// PostUpdateCommands.RemoveComponent<ViewerObject>(entityInQueryIndex, e);