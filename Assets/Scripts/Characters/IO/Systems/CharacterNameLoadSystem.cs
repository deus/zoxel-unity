using Unity.Entities;
using Unity.Burst;
using Unity.Collections;

namespace Zoxel.Characters
{
    //! Loads the name into a file.
    /**
    *   - Load System -
    */
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class CharacterNameLoadSystem : SystemBase
    {
        private const string filename = "CharacterName.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .ForEach((ref LoadName loadName, ref ZoxName zoxName, in EntitySaver entitySaver) =>
            {
                if (loadName.loadPath.Length == 0)
                {
                    loadName.loadPath = new Text(entitySaver.GetPath() + filename);
                }
                loadName.reader.UpdateOnMainThread(in loadName.loadPath);
            }).WithoutBurst().Run();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity, InitializeSaver>()
                .WithAll<EntitySaver>()
                .ForEach((Entity e, int entityInQueryIndex, ref LoadName loadName, ref ZoxName zoxName) =>
            {
                if (loadName.reader.IsReadFileInfoComplete())
                {
                    if (loadName.reader.DoesFileExist())
                    {
                        loadName.reader.state = AsyncReadState.StartOpenFile;
                    }
                    else
                    {
                        // UnityEngine.Debug.LogError("File Did not exist.");
                        loadName.Dispose();
                        PostUpdateCommands.RemoveComponent<LoadName>(entityInQueryIndex, e);
                    }
                }
                else if (loadName.reader.IsReadFileComplete())
                {
                    if (loadName.reader.DidFileReadSuccessfully())
                    {
                        var bytes = loadName.reader.GetBytes();
                        zoxName.name.textData.Dispose();
                        zoxName.name.textData = bytes.Clone();
                    }
                    // Dispose
                    loadName.Dispose();
                    PostUpdateCommands.RemoveComponent<LoadName>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

/* Entities
    .WithNone<DeadEntity, DestroyEntity, InitializeSaver>()
    .WithAll<LoadName>()
    .ForEach((Entity e, ref ZoxName zoxName, in EntitySaver entitySaver) =>
{
    var directoryPath = entitySaver.GetPath();
    if (!Directory.Exists(directoryPath))
    {
        zoxName.name.SetText("Null");
        return;
    }
    var filePath = directoryPath + filename;
    if (!File.Exists(filePath))
    {
        zoxName.name.SetText("Null");
        return;
    }
    try
    {
        var bytes = File.ReadAllBytes(filePath);
        if (bytes.Length > 16)
        {
            zoxName.name.SetText("Null");
            return;
        }
        zoxName.name.textData.Dispose();
        zoxName.name.textData = new BlitableArray<byte>(bytes, Allocator.Persistent);
        // var name = File.ReadAllText(filePath);
        // zoxName.name.SetText(name);
    }
    catch (Exception exception)
    {
        UnityEngine.Debug.LogError("Error Loading Name: " + exception);
        zoxName.name.SetText("Error");
    }
}).WithoutBurst().Run();*/
// new BlitableArray<byte>(bytes, Allocator.Persistent);