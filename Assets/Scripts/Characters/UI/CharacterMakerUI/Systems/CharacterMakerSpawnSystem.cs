using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Cameras;
using Zoxel.UI;
using Zoxel.Audio;
using Zoxel.Transforms;
using Zoxel.Classes;
using Zoxel.Races;
using Zoxel.Voxels;

namespace Zoxel.Characters.UI
{
    //! Spawns CharacterMaker UIs.
    /**
    *   - UI Spawn System -
    *   Spawns a Editable Character. With a UI Viewer and options.
    *   \todo Use ZoxDescriptions as tooltips for classes
    *   \todo Fix gamepad not working here!
    */
    [BurstCompile, UpdateInGroup(typeof(CharacterUISystemGroup))]
    public partial class CharacterMakerSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private NativeArray<Text> texts;
        private float2 panelSize;
        public static Entity spawnPanelPrefab;
        private Entity panelPrefab;
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            texts = new NativeArray<Text>(1, Allocator.Persistent);
            texts[0] = new Text("New Character");
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnCharacterMaker),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnPanelPrefab = EntityManager.CreateEntity(spawnArchetype);
            uiHoldersQuery = GetEntityQuery(
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<UILink>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            HeaderSpawnSystem.InitializePrefabs(EntityManager);
            var headerPrefab = HeaderSpawnSystem.headerPrefab;
            var uiDatam = UIManager.instance.uiDatam;
            var headerStyle = uiDatam.headerStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var panelPrefab = this.panelPrefab;
            var texts = this.texts;
            var panelSize = this.panelSize;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uiHolderEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
            uiHolderEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<SpawnCharacterMaker>()
                .ForEach((int entityInQueryIndex, in CharacterLink characterLink) =>
            {
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, panelPrefab);
                var headerText = texts[0].Clone();
                HeaderSpawnSystem.SpawnHeaderUI(PostUpdateCommands, entityInQueryIndex, headerPrefab, e3, panelSize, headerStyle.fontSize, headerStyle.textPadding,
                    in headerText);
                if (characterLink.character.Index > 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, characterLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab),
                        new OnUISpawned(characterLink.character, 1));
                    if (cameraLinks.HasComponent(characterLink.character))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLinks[characterLink.character]);
                    }
                }
            })  .WithReadOnly(texts)
                .WithReadOnly(cameraLinks)
                .ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private void InitializePrefabs()
        {
            if (panelPrefab.Index != 0)
            {
                return;
            }
            var uiDatam = UIManager.instance.uiDatam;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var iconSize = uiDatam.GetIconSize(uiScale);
            var totalSize = new float2(18, 12) * iconSize.y;
            this.panelSize = totalSize;
            panelPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(panelPrefab);
            EntityManager.RemoveComponent<SpawnHeaderUI>(panelPrefab);
            EntityManager.RemoveComponent<GridUIDirty>(panelPrefab);
            EntityManager.RemoveComponent<GridUI>(panelPrefab);
            EntityManager.SetComponentData(panelPrefab, new PanelUI(PanelType.NewCharacterMenu));
            EntityManager.SetComponentData(panelPrefab, new Size2D(totalSize));
            EntityManager.AddComponent<CharacterMakerUI>(panelPrefab);
            EntityManager.AddComponent<ViewerUILink>(panelPrefab);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }
    }
}