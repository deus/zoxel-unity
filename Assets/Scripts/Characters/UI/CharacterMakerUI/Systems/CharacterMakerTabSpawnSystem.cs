using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Cameras;
using Zoxel.Voxels;
using Zoxel.Classes;
using Zoxel.Races;
using Zoxel.Clans;
// Spawns a Editable Character
// Spawns a UI to edit it too
// System spawns CharacterMakerUI
//      Also Spawns a character we can Edit
// todo: fix gamepad not working here!
// structural changes for spawned children
//  todo: use main menus buttonComplete to set children

namespace Zoxel.Characters.UI
{
    //! Spawns tab ui for CharacterMaker.
    /**
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(CharacterUISystemGroup))]
    public partial class CharacterMakerTabSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity nameButtonPrefab;
        private Entity nameInputPrefab;
        private Entity labelPrefab;
        private Entity classButtonPrefab;
        private Entity raceButtonPrefab;
        private Entity jobButtonPrefab;
        private Entity clanButtonPrefab;
        private Entity animatorButtonPrefab;
        private NativeArray<Text> jobOptions;
        private NativeArray<Text> nameLabels;
        private NativeArray<Text> animatorOptions;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            nameLabels = new NativeArray<Text>(3, Allocator.Persistent);
            nameLabels[0] = new Text("Name");
            nameLabels[1] = new Text("84912937");
            nameLabels[2] = new Text("New Name");
            jobOptions = new NativeArray<Text>(3, Allocator.Persistent);
            jobOptions[0] = new Text("Lumberjack");
            jobOptions[1] = new Text("Miner");
            jobOptions[2] = new Text("Herbologist");
            animatorOptions = new NativeArray<Text>(3, Allocator.Persistent);
            animatorOptions[0] = new Text("Idle");
            animatorOptions[1] = new Text("Walk");
            animatorOptions[2] = new Text("Attack");

        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < nameLabels.Length; i++)
            {
                nameLabels[i].Dispose();
            }
            nameLabels.Dispose();
            for (int i = 0; i < jobOptions.Length; i++)
            {
                jobOptions[i].Dispose();
            }
            jobOptions.Dispose();
            for (int i = 0; i < animatorOptions.Length; i++)
            {
                animatorOptions[i].Dispose();
            }
            animatorOptions.Dispose();
        }

        private void InitializePrefabs()
        {
            if (labelPrefab.Index != 0)
            {
                return;
            }
            labelPrefab = EntityManager.Instantiate(UICoreSystem.labelPrefab);
            EntityManager.AddComponent<Prefab>(labelPrefab);
            EntityManager.AddComponent<SetUIElementPosition>(labelPrefab);
            classButtonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(classButtonPrefab);
            EntityManager.AddComponent<Toggle>(classButtonPrefab);
            raceButtonPrefab = EntityManager.Instantiate(classButtonPrefab);
            EntityManager.AddComponent<Prefab>(raceButtonPrefab);
            EntityManager.AddComponentData(raceButtonPrefab, new CharacterMakerButton(CharacterMakerButtonType.Race));
            jobButtonPrefab = EntityManager.Instantiate(classButtonPrefab);
            EntityManager.AddComponent<Prefab>(jobButtonPrefab);
            EntityManager.AddComponentData(jobButtonPrefab, new CharacterMakerButton(CharacterMakerButtonType.Job));
            clanButtonPrefab = EntityManager.Instantiate(classButtonPrefab);
            EntityManager.AddComponent<Prefab>(clanButtonPrefab);
            EntityManager.AddComponentData(clanButtonPrefab, new CharacterMakerButton(CharacterMakerButtonType.Clan));
            EntityManager.AddComponentData(classButtonPrefab, new CharacterMakerButton(CharacterMakerButtonType.Class));
            animatorButtonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(animatorButtonPrefab);
            EntityManager.AddComponentData(animatorButtonPrefab, new CharacterMakerButton(CharacterMakerButtonType.Dance));
            nameButtonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(nameButtonPrefab);
            EntityManager.AddComponentData(nameButtonPrefab, new CharacterMakerButton(CharacterMakerButtonType.Name));
            nameInputPrefab = EntityManager.Instantiate(UICoreSystem.inputPrefab);
            EntityManager.AddComponent<Prefab>(nameInputPrefab);
            EntityManager.AddComponentData(nameInputPrefab, new CharacterMakerButton(CharacterMakerButtonType.Name));

            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var headerStyle = uiDatam.headerStyle.GetStyle(uiScale);
            var buttonStyle = uiDatam.buttonStyle.GetStyle(uiScale);
            var iconSize = uiDatam.GetIconSize(uiScale) * 0.4f;
            var fontSize = iconSize.y;
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            var textMargins = uiDatam.GetMenuMargins(uiScale);
            UICoreSystem.SetRenderTextData(EntityManager, labelPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, classButtonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, raceButtonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, jobButtonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, clanButtonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, animatorButtonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, nameButtonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, nameInputPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            InitializePrefabs();
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var panelStyle = uiDatam.panelStyle.GetStyle();
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            var iconSize = uiDatam.GetIconSize(uiScale);
            var totalSize = new float2(18, 12) * iconSize.y;
            var viewerSize = new float2(iconSize.x * 6.1f, iconSize.x * 6.1f);
            var viewerPosition = new float3(0, iconSize.y * 2, 0);
            var smallButtonSize = iconSize.y * 0.6f;
            var smallestButtonSize = iconSize.y * 0.4f;
            var lowestButtonsPosition = new float2(-(totalSize.x / 2f - 4 * (smallButtonSize / 2f)), -(totalSize.y / 2f));
            var spawnCharacterPrefab2 = CharactersSystemGroup.spawnCharacterPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var leftSidePanel = new float2(-(totalSize.x / 2f), 0);
            var rightPanelIndex = 4;
            var clanButtonPrefab = this.clanButtonPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<InitializeEntity, MeshUIDirty>()
                .ForEach((Entity e, in Childrens childrens, in CharacterMakerUI characterMakerUI, in CharacterLink characterLink, in SpawnTabUI spawnTabUI) =>
            {
                var character = characterMakerUI.character;
                if (HasComponent<EntityBusy>(character) || !HasComponent<ZoxName>(character))
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<SpawnTabUI>(e);
                var rightPanel = childrens.children[rightPanelIndex];
                var player = characterLink.character;
                PostUpdateCommands.AddComponent<DestroyChildren>(rightPanel);
                if (spawnTabUI.typeType == CharacterMakerButtonType.Name)
                {
                    var characterName = EntityManager.GetComponentData<ZoxName>(character).name;
                    nameLabels[1].Dispose();
                    nameLabels[1] = characterName.Clone();
                    PostUpdateCommands.AddComponent(rightPanel, new SpawnUIList(nameButtonPrefab, labelPrefab, nameInputPrefab, nameLabels, iconSize.y / 2f, textPadding));
                }
                else if (spawnTabUI.typeType == CharacterMakerButtonType.Class)
                {
                    var selectedToggleIndex = (byte) 0;
                    var classLink = EntityManager.GetComponentData<ClassLink>(character);
                    var selectedData = classLink.classs;
                    // spawn class choices
                    var game = EntityManager.GetComponentData<RealmLink>(player).realm;
                    var data = EntityManager.GetComponentData<ClassLinks>(game).data;
                    var names = new NativeArray<Text>(data.Length, Allocator.Temp);
                    for (int i = 0; i < names.Length; i++)
                    {
                        names[i] = EntityManager.GetComponentData<ZoxName>(data[i]).name;
                        if (selectedData == data[i])
                        {
                            selectedToggleIndex = (byte) i;
                        }
                    }
                    // , iconSize.y / 2f, textPadding
                    PostUpdateCommands.AddComponent(rightPanel, new ListPrefabLink(classButtonPrefab));
                    PostUpdateCommands.AddComponent(rightPanel, new SpawnButtonsList(in names));
                    // todo: base on class of character, and labels
                    PostUpdateCommands.SetComponent(rightPanel, new ToggleGroup(255));
                    PostUpdateCommands.AddComponent(rightPanel, new SetToggleGroup(selectedToggleIndex));
                    names.Dispose();
                }
                else if (spawnTabUI.typeType == CharacterMakerButtonType.Race)
                {
                    var selectedToggleIndex = (byte) 0;
                    var selectedData = EntityManager.GetComponentData<RaceLink>(character).race;
                    var game = EntityManager.GetComponentData<RealmLink>(player).realm;
                    var races = EntityManager.GetComponentData<RaceLinks>(game).races;
                    var names = new NativeList<Text>();
                    for (int i = 0; i < races.Length; i++)
                    {
                        var race = races[i];
                        if (HasComponent<PlayerChoice>(race))
                        {
                            names.Add(EntityManager.GetComponentData<ZoxName>(race).name);
                            if (selectedData == race)
                            {
                                selectedToggleIndex = (byte) (names.Length - 1);
                            }
                        }
                    }
                    PostUpdateCommands.AddComponent(rightPanel, new ListPrefabLink(raceButtonPrefab));
                    PostUpdateCommands.AddComponent(rightPanel, new SpawnButtonsList(in names));
                    // todo: base on class of character, and labels
                    PostUpdateCommands.SetComponent(rightPanel, new ToggleGroup(255));
                    PostUpdateCommands.AddComponent(rightPanel, new SetToggleGroup(selectedToggleIndex));
                    names.Dispose();
                }
                else if (spawnTabUI.typeType == CharacterMakerButtonType.Clan)
                {
                    var selectedToggleIndex = (byte) 0;
                    var selectedData = EntityManager.GetComponentData<ClanLink>(character).clan;
                    var game = EntityManager.GetComponentData<RealmLink>(player).realm;
                    var clans = EntityManager.GetComponentData<ClanLinks>(game).clans;
                    var names = new NativeList<Text>();
                    for (int i = 0; i < clans.Length; i++)
                    {
                        var clanEntity = clans[i];
                        if (HasComponent<PlayerChoice>(clanEntity))
                        {
                            names.Add(EntityManager.GetComponentData<ZoxName>(clanEntity).name);
                            if (selectedData == clanEntity)
                            {
                                selectedToggleIndex = (byte) (names.Length - 1);
                            }
                        }
                    }
                    PostUpdateCommands.AddComponent(rightPanel, new ListPrefabLink(clanButtonPrefab));
                    PostUpdateCommands.AddComponent(rightPanel, new SpawnButtonsList(in names));
                    // todo: base on class of character, and labels
                    PostUpdateCommands.SetComponent(rightPanel, new ToggleGroup(255));
                    PostUpdateCommands.AddComponent(rightPanel, new SetToggleGroup(selectedToggleIndex));
                    names.Dispose();
                }
                else if (spawnTabUI.typeType == CharacterMakerButtonType.Job)
                {
                    PostUpdateCommands.AddComponent(rightPanel, new ListPrefabLink(jobButtonPrefab));
                    PostUpdateCommands.AddComponent(rightPanel, new SpawnButtonsList(in jobOptions));
                    // todo: base on class of character, and labels
                    PostUpdateCommands.SetComponent(rightPanel, new ToggleGroup(255));
                    PostUpdateCommands.AddComponent(rightPanel, new SetToggleGroup(0));
                }
                else if (spawnTabUI.typeType == CharacterMakerButtonType.Dance)
                {
                    PostUpdateCommands.AddComponent(rightPanel, new ListPrefabLink(animatorButtonPrefab));
                    PostUpdateCommands.AddComponent(rightPanel, new SpawnButtonsList(in animatorOptions));
                }
            }).WithoutBurst().Run();
        }
    }
}