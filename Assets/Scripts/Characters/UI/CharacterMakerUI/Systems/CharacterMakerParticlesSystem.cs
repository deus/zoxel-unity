using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Particles;                  // When updating character race/class

namespace Zoxel.Characters.UI
{
    //! Adds particles to character when transformed.
    [BurstCompile, UpdateInGroup(typeof(CharacterUISystemGroup))]
    public partial class CharacterMakerParticlesSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var particleSystemPrefab = ParticleSystemGroup.particleSystemPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<CharacterMakerParticles>()
                .ForEach((Entity e, int entityInQueryIndex, in ZoxID zoxID, in Body body, in Translation translation, in VoxLink voxLink) =>
            {
                PostUpdateCommands.RemoveComponent<CharacterMakerParticles>(entityInQueryIndex, e);
                // spawn particles
                var random = new Random();
                random.InitState((uint)zoxID.id);
                var particlesSystemEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, particleSystemPrefab);
                // color should depend on voxel
                var sizeBuff = random.NextFloat(0.1f);
                PostUpdateCommands.SetComponent(entityInQueryIndex, particlesSystemEntity,
                    new ParticleSystem
                    {
                        timeSpawn = 0, // 0.001,
                        timeBegun = elapsedTime,
                        lifeTime = 0.3 + random.NextFloat(0.1f),
                        random = random,
                        spawnSize = body.size + new float3(sizeBuff, sizeBuff, sizeBuff),
                        // new float3(0.26f + sizeBuff, 1.5f + sizeBuff, 0.26f + sizeBuff),
                        baseColor = new Color(12, 4, 4),
                        colorVariance = new float3(0.15f, 0.15f, 0.15f), 
                        spawnRate = 26,
                        particleLife = 4,
                        positionAdd = new float3(0, 1, 0)
                    });
                PostUpdateCommands.SetComponent(entityInQueryIndex, particlesSystemEntity, new Translation { Value = translation.Value });
                PostUpdateCommands.SetComponent(entityInQueryIndex, particlesSystemEntity, voxLink);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        } 
    }
}