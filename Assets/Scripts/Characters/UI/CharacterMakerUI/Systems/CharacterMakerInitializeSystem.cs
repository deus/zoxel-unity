using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;
using Zoxel.Transforms;
using Zoxel.Classes;
using Zoxel.Races;
using Zoxel.Voxels;
using Zoxel.Cameras;
using Zoxel.Worlds;

namespace Zoxel.Characters.UI
{
    //! Initializes CharacterMaker things.
    [UpdateInGroup(typeof(CharacterUISystemGroup))]
    public partial class CharacterMakerInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity backNextButtonPrefab;
        private Entity tabButtonPrefab;
        private Entity seedButtonPrefab;
        private Entity seedInputPrefab;
        private Entity labelPrefab;
        private Entity viewerPrefab;
        private NativeArray<Text> tabLabels;
        private NativeArray<Text> seedLabels;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            tabLabels = new NativeArray<Text>(7, Allocator.Persistent);
            tabLabels[CharacterMakerButtonType.Name] = new Text("Name");
            tabLabels[CharacterMakerButtonType.Class] = new Text("Class");
            tabLabels[CharacterMakerButtonType.Race] = new Text("Race");
            tabLabels[CharacterMakerButtonType.Job] = new Text("Job");
            tabLabels[CharacterMakerButtonType.Clan] = new Text("Clan");
            tabLabels[CharacterMakerButtonType.Dance] = new Text("Dance");
            tabLabels[CharacterMakerButtonType.DNA] = new Text("DNA");
            //tabLabels[5] = new Text("Stats");
            //tabLabels[7] = new Text("Location");
            seedLabels = new NativeArray<Text>(3, Allocator.Persistent);
            seedLabels[0] = new Text("Seed");
            seedLabels[1] = new Text("84912937");
            seedLabels[2] = new Text("New Body");
            RequireForUpdate<SoundSettings>();
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < tabLabels.Length; i++)
            {
                tabLabels[i].Dispose();
            }
            tabLabels.Dispose();
            for (int i = 0; i < seedLabels.Length; i++)
            {
                seedLabels[i].Dispose();
            }
            seedLabels.Dispose();
        }

        protected override void OnUpdate()
        {
            InitializePrefabs();
            var soundSettings = GetSingleton<SoundSettings>();
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var panelStyle = uiDatam.panelStyle.GetStyle();
            var subPanelStyle = uiDatam.subPanelStyle.GetStyle();
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            var iconSize = uiDatam.GetIconSize(uiScale);
            var totalSize = new float2(18, 12) * iconSize.y;
            var viewerSize = new float2(iconSize.x, iconSize.x) * 6f; // 6.1
            var viewerPosition = new float3(0, iconSize.y * 2, 0);
            var smallButtonSize = iconSize.y * 0.6f;
            var smallestButtonSize = iconSize.y * 0.4f;
            var lowestButtonsPosition = new float2(-(totalSize.x / 2f - 4 * (smallButtonSize / 2f)), -(totalSize.y / 2f));
            var leftSidePanel = new float2(-(totalSize.x / 2f), 0);
            var tabLabels = this.tabLabels;
            var characterPosition = UIManager.instance.uiSettings.characterMakerCharacterPosition; // new float3(-256, -256, -256);
            // spawns buttons, viewer and sub panels
            var spawnPlayerCharacterPrefab = CharactersSystemGroup.spawnPlayerCharacterPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<MeshUIDirty>()
                .WithAll<InitializeEntity, CharacterMakerUI>()
                .ForEach((Entity e, ref CharacterMakerUI characterMakerUI, in Size2D size2D, in CharacterLink characterLink) =>
            {
                var controllerEntity = characterLink.character;
                var characterID = IDUtil.GenerateUniqueID();
                var realmEntity = EntityManager.GetComponentData<RealmLink>(controllerEntity).realm;
                var random = new Random();
                random.InitState((uint) characterID);
                var characterName = NameGenerator.GenerateName2(ref random);
                // spawn children
                var children = new NativeList<SpawnUIData>();
                children.Add(new SpawnUIData
                {
                    label = new Text("Done"),
                    position = new float3(-lowestButtonsPosition.x - textPadding.x, lowestButtonsPosition.y + smallButtonSize / 2f + textPadding.y, 0),
                    size = smallButtonSize
                });
                children.Add(new SpawnUIData
                {
                    label = new Text("Back"),
                    position = new float3(lowestButtonsPosition.x + textPadding.x, lowestButtonsPosition.y + smallButtonSize / 2f + textPadding.y, 0),
                    size = smallButtonSize
                });
                // viewer UI
                children.Add(new SpawnUIData
                {
                    type = 3,
                    position = viewerPosition
                });
                var viewerUI = new Entity();
                for (int i = 0; i < children.Length; i++)
                {
                    var spawnData = children[i];
                    if (spawnData.type == 0)
                    {
                        // spawn button
                        SpawnButton(PostUpdateCommands, e, spawnData.label, i, spawnData.size, spawnData.position, in soundSettings);
                    }
                    else if (spawnData.type == 3)
                    {
                        // spawn button
                        viewerUI = SpawnViewer(PostUpdateCommands, e, i, viewerSize, spawnData.position, characterPosition);
                    }
                }
                PostUpdateCommands.SetComponent(e, new ViewerUILink(viewerUI));

                // spawn invinsible UI - child - w ith size of a third of panel
                var panelPositionX = leftSidePanel.x + (size2D.size.x / 3f) / 2f;

                var leftPanel = UICoreSystem.SpawnElement(PostUpdateCommands, UICoreSystem.toggleGroupPrefab, e, 
                    new float3(panelPositionX, 0, 0), new float2(size2D.size.x / 3f, size2D.size.y), subPanelStyle.color);
                UICoreSystem.SetChild(PostUpdateCommands, leftPanel, children.Length);
                UICoreSystem.SetPanelLink(PostUpdateCommands, leftPanel, e);
                UICoreSystem.SetTextureFrame(PostUpdateCommands, leftPanel, in subPanelStyle.frameGenerationData);
                PostUpdateCommands.SetComponent(leftPanel, new GridUISize(iconSize));
                PostUpdateCommands.AddComponent<AutoGridY>(leftPanel);
                PostUpdateCommands.AddComponent<DisableGridResize>(leftPanel);
                // , iconSize.y / 2f, textPadding
                PostUpdateCommands.AddComponent(leftPanel, new ListPrefabLink(tabButtonPrefab));
                PostUpdateCommands.AddComponent(leftPanel, new SpawnButtonsList(in tabLabels));

                var rightPanel = UICoreSystem.SpawnElement(PostUpdateCommands, UICoreSystem.toggleGroupPrefab, e, 
                    new float3(-panelPositionX, 0, 0), new float2(size2D.size.x / 3f, size2D.size.y), subPanelStyle.color);
                UICoreSystem.SetChild(PostUpdateCommands, rightPanel, children.Length + 1);
                UICoreSystem.SetPanelLink(PostUpdateCommands, rightPanel, e);
                UICoreSystem.SetTextureFrame(PostUpdateCommands, rightPanel, in subPanelStyle.frameGenerationData);
                PostUpdateCommands.SetComponent(rightPanel, new GridUISize(iconSize));
                PostUpdateCommands.AddComponent<AutoGridY>(rightPanel);
                PostUpdateCommands.AddComponent<DisableGridResize>(rightPanel);

                // just put seed here
                var seedLabel = seedLabels[1];
                seedLabel.SetText(characterID.ToString());
                seedLabels[1] = seedLabel;
                var middlePanel = UICoreSystem.SpawnElement(PostUpdateCommands, UICoreSystem.subPanelGridPrefab, e, 
                    new float3(0, - totalSize.y / 2f + (size2D.size.y / 3f) / 2f, 0), new float2(size2D.size.x / 3f, size2D.size.y / 3f), subPanelStyle.color);
                UICoreSystem.SetChild(PostUpdateCommands, middlePanel, children.Length + 2);
                UICoreSystem.SetPanelLink(PostUpdateCommands, middlePanel, e);
                UICoreSystem.SetTextureFrame(PostUpdateCommands, middlePanel, in subPanelStyle.frameGenerationData);
                PostUpdateCommands.SetComponent(middlePanel, new GridUISize(iconSize));
                PostUpdateCommands.AddComponent<AutoGridY>(middlePanel);
                PostUpdateCommands.AddComponent<DisableGridResize>(middlePanel);
                // SpawnUIList with Label, Input and Button
                PostUpdateCommands.AddComponent(middlePanel, new SpawnUIList(seedButtonPrefab, labelPrefab, seedInputPrefab,
                    seedLabels, iconSize.y / 2f, textPadding));

                // Finished Spawning UI
                PostUpdateCommands.AddComponent(e, new OnChildrenSpawned((byte)(children.Length + 3)));
                PostUpdateCommands.AddComponent<MeshUIDirty>(e);
                PostUpdateCommands.AddComponent<NavigationDirty>(e);
                PostUpdateCommands.AddComponent(e, new SpawnTabUI(CharacterMakerButtonType.Name));

                // Spawn Character Here
                var spawnCharacter = new SpawnCharacter
                {
                    quadrant = PlanetSide.Up,
                    prefabType = CharacterPrefabType.playerNew,
                    spawnID = characterID,
                    realm = realmEntity,
                    position = characterPosition,
                    rotation = quaternion.identity
                };
                var spawnPlayerCharacter = new SpawnPlayerCharacter
                {
                    controllerEntity = controllerEntity,
                    cameraEntity = EntityManager.GetComponentData<CameraLink>(controllerEntity).camera,
                    characterMaker = e,
                    viewerUI = viewerUI,
                    name = characterName
                };
                var spawnEntity = PostUpdateCommands.Instantiate(spawnPlayerCharacterPrefab);
                PostUpdateCommands.SetComponent(spawnEntity, spawnCharacter);
                PostUpdateCommands.SetComponent(spawnEntity, spawnPlayerCharacter);
                // OnSpawnedCharacter - Add it to viewer - instead of linking in CharacterMaker into the spawn component..
                // .. actually linking is more efficient.. nvm
                children.Dispose();
            }).WithoutBurst().Run();
        }

        private Entity SpawnViewer(EntityCommandBuffer PostUpdateCommands, Entity e, int childIndex, float2 viewerSize, float3 position, float3 characterPosition)
        {
            var characterMakerCameraPosition = UIManager.instance.uiSettings.characterMakerCameraPosition;
            var characterMakerCameraRotation = UIManager.instance.uiSettings.characterMakerCameraRotation;
            var uiDatam = UIManager.instance.uiDatam;
            var voxelViewerMaterial = UIManager.instance.materials.voxelViewerMaterial;
            var viewerEntity = UICoreSystem.SpawnElement(PostUpdateCommands, viewerPrefab, e, position, viewerSize, new Color(UnityEngine.Color.white), voxelViewerMaterial, TextureUtil.CreateBlankTexture());
            var degreesToRadians = ((math.PI * 2) / 360f);
            SpawnViewerCamera(PostUpdateCommands, viewerEntity, new Entity(), characterPosition + characterMakerCameraPosition,
                quaternion.EulerXYZ(characterMakerCameraRotation * degreesToRadians), 1);
            UICoreSystem.SetChild(PostUpdateCommands, viewerEntity, childIndex);
            return viewerEntity;
        }

        public void SpawnButton(EntityCommandBuffer PostUpdateCommands, Entity e, Text text, int childIndex, float fontSize, float3 position,
            in SoundSettings soundSettings)
        {
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var buttonStyle = uiDatam.buttonStyle.GetStyle(uiScale);
            UICoreSystem.SpawnButton(PostUpdateCommands, e, backNextButtonPrefab, childIndex, position, text, fontSize, uiDatam.GetMenuPaddings(uiScale), 
                buttonStyle.color, buttonStyle.textColor, in soundSettings);
        }

        public static int SpawnViewerCamera(EntityCommandBuffer PostUpdateCommands, Entity ui, Entity targetEntity, float3 position, quaternion rotation, byte cameraType)
        {
            var id = IDUtil.GenerateUniqueID();
            PostUpdateCommands.AddComponent(ui, new SpawnViewerCamera
            {
                id = id,
                targetEntity = targetEntity,
                position = position,
                rotation = rotation,
                cameraType = cameraType
            });
            return id;
        }

        private void InitializePrefabs()
        {
            if (backNextButtonPrefab.Index != 0)
            {
                return;
            }
            backNextButtonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(backNextButtonPrefab);
            EntityManager.AddComponent<SetUIElementPosition>(backNextButtonPrefab);
            EntityManager.AddComponentData(backNextButtonPrefab, new CharacterMakerButton(CharacterMakerButtonType.BackDone));
            labelPrefab = EntityManager.Instantiate(UICoreSystem.labelPrefab);
            EntityManager.AddComponent<Prefab>(labelPrefab);
            EntityManager.AddComponent<SetUIElementPosition>(labelPrefab);
            viewerPrefab = EntityManager.Instantiate(UICoreSystem.viewerPrefab);
            EntityManager.AddComponent<Prefab>(viewerPrefab);
            EntityManager.AddComponent<SetUIElementPosition>(viewerPrefab);
            tabButtonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(tabButtonPrefab);
            EntityManager.AddComponentData(tabButtonPrefab, new CharacterMakerButton(CharacterMakerButtonType.TabButtons));
            EntityManager.AddComponent<Toggle>(tabButtonPrefab);
            seedButtonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(seedButtonPrefab);
            EntityManager.AddComponentData(seedButtonPrefab, new CharacterMakerButton(CharacterMakerButtonType.Seed));
            seedInputPrefab = EntityManager.Instantiate(UICoreSystem.inputPrefab);
            EntityManager.AddComponent<Prefab>(seedInputPrefab);
            EntityManager.AddComponent<InputNumbersOnly>(seedInputPrefab);
            EntityManager.AddComponentData(seedInputPrefab, new CharacterMakerButton(CharacterMakerButtonType.Seed));

            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var headerStyle = uiDatam.headerStyle.GetStyle(uiScale);
            var buttonStyle = uiDatam.buttonStyle.GetStyle(uiScale);
            var iconSize = uiDatam.GetIconSize(uiScale) * 0.4f;
            var fontSize = iconSize.y;
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            var textMargins = uiDatam.GetMenuMargins(uiScale);
            UICoreSystem.SetRenderTextData(EntityManager, backNextButtonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, labelPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, tabButtonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, seedButtonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
            UICoreSystem.SetRenderTextData(EntityManager, seedInputPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, fontSize, textPadding);
        }
    }
}