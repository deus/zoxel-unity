using Unity.Entities;
using Unity.Mathematics;
using Zoxel.UI;         // button click
using Zoxel.Transforms; // button parent
using Zoxel.Animations; // setting animator
using Zoxel.Clans;
using Zoxel.Classes;    // setting class
using Zoxel.Races;      // setting race

namespace Zoxel.Characters.UI
{
    [UpdateInGroup(typeof(CharacterUISystemGroup))]
    public partial class CharacterMakerSelectSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((Entity e, in Child child, in ParentLink parentLink,
                in PanelLink panelLink, in CharacterMakerButton characterMakerButton, in UISelectEvent uiSelectEvent) =>
            {
                var arrayIndex = child.index;
                var panel = panelLink.panel;
                var controllerEntity = uiSelectEvent.character;
                var playerTooltipLinks = EntityManager.GetComponentData<PlayerTooltipLinks>(controllerEntity);
                var realmEntity = EntityManager.GetComponentData<RealmLink>(controllerEntity).realm;
                if (characterMakerButton.buttonType == CharacterMakerButtonType.BackDone)
                {
                    if (child.index == 1)
                    {
                        playerTooltipLinks.SetTooltipText(PostUpdateCommands, controllerEntity, "Cancel Making Body");
                    }
                    else
                    {
                        playerTooltipLinks.SetTooltipText(PostUpdateCommands, controllerEntity, "Enter Your Body");
                    }
                }
                else if (characterMakerButton.buttonType == CharacterMakerButtonType.Class)
                {
                    //! \todo Fix indexing logic for class. Store direct ClassLink on button.
                    var data = EntityManager.GetComponentData<ClassLinks>(realmEntity).data;
                    var description = EntityManager.GetComponentData<ZoxDescription>(data[arrayIndex]).description;
                    playerTooltipLinks.SetTooltipText(PostUpdateCommands, controllerEntity, description.ToString()); 
                }
                else if (characterMakerButton.buttonType == CharacterMakerButtonType.Race)
                {
                    //! \todo Fix indexing logic for race. Store direct RaceLink on button.
                    var races = EntityManager.GetComponentData<RaceLinks>(realmEntity).races;
                    var description = EntityManager.GetComponentData<ZoxDescription>(races[arrayIndex + 4]).description;
                    playerTooltipLinks.SetTooltipText(PostUpdateCommands, controllerEntity, description.ToString()); 
                }
                else if (characterMakerButton.buttonType == CharacterMakerButtonType.Clan)
                {
                    //! \todo Fix indexing logic for clan. Store direct RaceLink on button.
                    var clans = EntityManager.GetComponentData<ClanLinks>(realmEntity).clans;
                    var description = EntityManager.GetComponentData<ZoxDescription>(clans[arrayIndex + 1]).description;
                    playerTooltipLinks.SetTooltipText(PostUpdateCommands, controllerEntity, description.ToString()); 
                }
            }).WithoutBurst().Run();
        }
    }
}