using Unity.Entities;
using Unity.Mathematics;
using Zoxel.UI;         // button click
using Zoxel.Transforms; // button parent
using Zoxel.Classes;    // setting class
using Zoxel.Races;      // setting race
using Zoxel.Animations; // setting animator
using Zoxel.Players;    // SetControllerMapping when entering world
using Zoxel.Rendering;
using Zoxel.Weather;    // setting SetSkyAsCamera
using Zoxel.Input;
using Zoxel.Cameras;
using Zoxel.Cameras.Spawning;   //! \todo Put prefab into CameraSystemGroup, and just edit it from the spawning systems.

namespace Zoxel.Characters.UI
{
    //! Handles UISelectEvent for CharacterMakerButton.
    /**
    *   - UIClickEvent System -
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(CharacterUISystemGroup))]
    public partial class CharacterMakerClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((Entity e, in Zoxel.Transforms.Child child, in ParentLink parentLink, in PanelLink panelLink, in CharacterMakerButton characterMakerButton,
                in UIClickEvent uiClickEvent) =>
            {
                var arrayIndex = child.index;
                var playerEntity = uiClickEvent.controller;
                var panel = panelLink.panel;
                var character = EntityManager.GetComponentData<CharacterMakerUI>(panelLink.panel).character;
                if (characterMakerButton.buttonType == CharacterMakerButtonType.BackDone)
                {
                    if (arrayIndex == 0) //3 + 3)
                    {
                        var playerTooltipLinks = EntityManager.GetComponentData<PlayerTooltipLinks>(playerEntity);
                        playerTooltipLinks.SetTooltipText(PostUpdateCommands, playerEntity, ""); 
                        ConfirmNewCharacter(PostUpdateCommands, playerEntity, panel, character);
                    }
                    else if (arrayIndex == 1) //4 + 3)
                    {
                        CancelNewCharacter(PostUpdateCommands, playerEntity, panel);
                    }
                }
                else if (characterMakerButton.buttonType == CharacterMakerButtonType.Name)
                {
                    if (arrayIndex == 2)
                    {
                        GenerateName(PostUpdateCommands, character, parentLink.parent, arrayIndex);
                    }
                }
                else if (characterMakerButton.buttonType == CharacterMakerButtonType.Seed)
                {
                    if (arrayIndex == 2)
                    {
                        GenerateSeed(PostUpdateCommands, character, parentLink.parent, arrayIndex);
                    }
                }
                else if (characterMakerButton.buttonType == CharacterMakerButtonType.Dance)
                {
                    var animator = EntityManager.GetComponentData<Animator>(character);
                    animator.animationSpeed = 2f;
                    if (arrayIndex == 0)
                    {
                        // var playerCharacter = EntityManager.GetComponentData<CharacterMakerUI>(uiEntity).character;
                        animator.setCoreState = AnimationState.Idle;
                        animator.setRightArmState = AnimationState.Idle;
                        animator.setLeftArmState = AnimationState.Idle;
                        animator.setLegsState = AnimationState.Idle;
                    }
                    else if (arrayIndex == 1)
                    {
                        animator.setCoreState = AnimationState.Walking;
                        animator.setRightArmState = AnimationState.Walking;
                        animator.setLeftArmState = AnimationState.Walking;
                        animator.setLegsState = AnimationState.Walking;
                    }
                    else if (arrayIndex == 2)
                    {
                        animator.setRightArmState = AnimationState.Attacking;
                    }
                    PostUpdateCommands.SetComponent(character, animator);
                }
            }).WithoutBurst().Run();
            // toggles
            Entities.ForEach((Entity e, in Zoxel.Transforms.Child child, in ParentLink parentLink, in PanelLink panelLink, in CharacterMakerButton characterMakerButton,
                in ToggleClickEvent toggleClickEvent) =>
            {
                var arrayIndex = child.index;
                var playerEntity = toggleClickEvent.controller;
                var panel = panelLink.panel;
                var character = EntityManager.GetComponentData<CharacterMakerUI>(panelLink.panel).character;
                if (characterMakerButton.buttonType == CharacterMakerButtonType.TabButtons)
                {
                    PostUpdateCommands.AddComponent(panel, new SpawnTabUI((byte) arrayIndex));
                }
                else if (characterMakerButton.buttonType == CharacterMakerButtonType.Class)
                {
                    // todo: Update CLothes, UserStatLinks & UserSkillLinks based on Class
                    // Set Class based on clicked one
                    var game = EntityManager.GetComponentData<RealmLink>(playerEntity).realm;
                    var data = EntityManager.GetComponentData<ClassLinks>(game).data;
                    PostUpdateCommands.AddComponent(character, new SetClass(data[arrayIndex]));
                    PostUpdateCommands.AddComponent<CharacterMakerParticles>(character);
                    // generate userSkillLinks again
                }
                else if (characterMakerButton.buttonType == CharacterMakerButtonType.Race)
                {
                    // todo: Give Buffs for Races
                    var game = EntityManager.GetComponentData<RealmLink>(playerEntity).realm;
                    var races = EntityManager.GetComponentData<RaceLinks>(game).races;
                    PostUpdateCommands.SetComponent(character, new RaceLink(races[arrayIndex + 4]));
                    PostUpdateCommands.AddComponent<GenerateBody>(character);
                    PostUpdateCommands.AddComponent<CharacterMakerParticles>(character);
                }
                else if (characterMakerButton.buttonType == CharacterMakerButtonType.Job)
                {
                    // todo: Save Job Changes
                    var game = EntityManager.GetComponentData<RealmLink>(playerEntity).realm;
                    // var data = EntityManager.GetComponentData<JobLinks>(game).data;
                    // PostUpdateCommands.SetComponent(character, new JobLink(data[arrayIndex + 4]));
                    PostUpdateCommands.AddComponent<CharacterMakerParticles>(character);
                }
            }).WithoutBurst().Run();
        }

        void GenerateName(EntityCommandBuffer PostUpdateCommands, Entity character, Entity parentUI, int arrayIndex)
        {
            var characterName = NameGenerator.GenerateName();
            var input = EntityManager.GetComponentData<Childrens>(parentUI).children[arrayIndex - 1];
            var renderText = EntityManager.GetComponentData<RenderText>(input);
            if (renderText.SetText(characterName.ToString()))
            {
                PostUpdateCommands.AddComponent<RenderTextDirty>(input);
                PostUpdateCommands.SetComponent(input, renderText);
            }
            PostUpdateCommands.SetComponent(character, new ZoxName(new Text(characterName)));
        }

        // Generate New Seed & refresh Character!
        private void GenerateSeed(EntityCommandBuffer PostUpdateCommands, Entity character, Entity parentUI, int arrayIndex)
        {
            if (HasComponent<GenerateBody>(character) || !HasComponent<Character>(character))
            {
                return;
            }
            var seed = IDUtil.GenerateUniqueID();
            var seedString = seed.ToString();           // NameGenerator.GenerateName();
            var input = EntityManager.GetComponentData<Childrens>(parentUI).children[arrayIndex - 1];
            var renderText = EntityManager.GetComponentData<RenderText>(input);
            if (renderText.SetText(seedString))
            {
                PostUpdateCommands.AddComponent<RenderTextDirty>(input);
                PostUpdateCommands.SetComponent(input, renderText);
            }
            var zoxID = EntityManager.GetComponentData<ZoxID>(character);
            zoxID.id = seed;
            PostUpdateCommands.SetComponent(character, zoxID);
            PostUpdateCommands.AddComponent<GenerateBody>(character);
            PostUpdateCommands.AddComponent<CharacterMakerParticles>(character);
        }

        //! Starts playing as the new character.
        public void ConfirmNewCharacter(EntityCommandBuffer PostUpdateCommands, Entity playerEntity, Entity uiEntity, Entity characterEntity)
        {
            PostUpdateCommands.RemoveComponent<ViewerObject>(characterEntity);    // so it doesn't get destroyed
            PostUpdateCommands.RemoveComponent<CharacterMakerCharacter>(characterEntity);
            // remove viewerObject from it
            var fadeinScreenTime = UIManager.instance.uiSettings.fadeinScreenTime; // 1f;
            var fadeScreenWaitTime = UIManager.instance.uiSettings.fadeinScreenWaitTime; // 1f;
            var elapsedTime = World.Time.ElapsedTime;
            // Load Player Character
            var loadEntity = PostUpdateCommands.CreateEntity();
            PostUpdateCommands.AddComponent(loadEntity, new ConfirmPlayerCharacter(playerEntity, characterEntity));
            PostUpdateCommands.AddComponent(loadEntity, new DelayEvent(elapsedTime, fadeinScreenTime));
            // var characterEntity = EntityManager.GetComponentData<CharacterMakerUI>(uiEntity).character;
            // Remove UI
            var viewerUI = EntityManager.GetComponentData<ViewerUILink>(uiEntity).viewerUI;
            PostUpdateCommands.AddComponent<DisableDestroyViewerUI>(viewerUI);
            UICoreSystem.RemoveUI(PostUpdateCommands, playerEntity, uiEntity);
            // Disable Controls
            PostUpdateCommands.AddComponent(playerEntity, new SetControllerMapping(ControllerMapping.None));
            // spawn new camera
            var spawnFirstPersonCamera = new SpawnFirstPersonCamera(playerEntity, new float3());
            var firstPersonCameraEntity = FirstPersonCameraSpawnSystem.SpawnFirstPersonCamera(PostUpdateCommands, in spawnFirstPersonCamera);
            // Fade out
            var screenFaderLink = EntityManager.GetComponentData<ScreenFaderLink>(playerEntity);
            PostUpdateCommands.SetComponent(screenFaderLink.screenFader, new Fader(0, 1, fadeinScreenTime));
            PostUpdateCommands.AddComponent(screenFaderLink.screenFader, new ReverseFader(fadeinScreenTime + fadeScreenWaitTime));
            PostUpdateCommands.AddComponent<FadeIn>(screenFaderLink.screenFader);
            PostUpdateCommands.AddComponent(screenFaderLink.screenFader, new TransitionCameraLink(firstPersonCameraEntity, elapsedTime, fadeinScreenTime));
            // Turn into event for Confirming new character.
            // spawn first peron camera
            // TransitionCamera
            var beforeCameraEntity = EntityManager.GetComponentData<CameraLink>(playerEntity).camera;
            var transitionEvent = PostUpdateCommands.CreateEntity();
            PostUpdateCommands.AddComponent(transitionEvent, new TransitionCamera(beforeCameraEntity, firstPersonCameraEntity));
            PostUpdateCommands.AddComponent(transitionEvent, new DelayEvent(elapsedTime, fadeinScreenTime));
            PostUpdateCommands.AddComponent<GenericEvent>(transitionEvent);
            // Spawn other players
            var playerHomeEntity = EntityManager.GetComponentData<PlayerHomeLink>(playerEntity).playerHome;
            PostUpdateCommands.AddComponent(playerHomeEntity, new SpawnPlayerCharacters(playerEntity));
        }

        //! Removes CharacterMakerUI and goes back to RealmSelectionUI
        public void CancelNewCharacter(EntityCommandBuffer PostUpdateCommands, Entity playerEntity, Entity uiEntity)
        {
            // Remove Character Maker UI
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var removeUI = PostUpdateCommands.Instantiate(removeCharacterUIPrefab);
            PostUpdateCommands.SetComponent(removeUI, new RemoveUI(playerEntity, uiEntity));
            // Spawn load character UI
            var spawnEntity = PostUpdateCommands.Instantiate(CharacterLoadUISpawnSystem.spawnPanelPrefab);
            PostUpdateCommands.SetComponent(spawnEntity, new CharacterLink(playerEntity));
        }
    }
}

// set class
/*else if (arrayIndex == 12 + 0)
{
    UnityEngine.Debug.LogError("Setting class to Warrior");
    // give punch skill
    // give armor
    // give metal helm
    var playerCharacter = EntityManager.GetComponentData<CharacterMakerUI>(uiEntity).character;
    // PostUpdateCommands.AddComponent<RemoveAllClasses>(playerCharacter);
    // PostUpdateCommands.AddComponent(playerCharacter, new GiveGameClass("Warrior"));
    // give game class will grab the class from game data, and store in on character
    // not all characters have a class, only humanoid characters are given one, and some stronger monsters

}
// set class
else if (arrayIndex == 12 + 1)
{
    UnityEngine.Debug.LogError("Setting class to Mage");
    // give fireball skill
    // give robes
    // give magic hat
    // give staff
    var playerCharacter = EntityManager.GetComponentData<CharacterMakerUI>(uiEntity).character;
}
// set class
else if (arrayIndex == 12 + 2)
{
    UnityEngine.Debug.LogError("Setting class to Rogue");
    // give stab skill
    // give leather
    // give ninja mask
    // give dagger
    var playerCharacter = EntityManager.GetComponentData<CharacterMakerUI>(uiEntity).character;
}*/

/*else if (arrayIndex == 4 + 8)
{
    // UnityEngine.Debug.LogError("Toggle Clothes");
    var playerCharacter = EntityManager.GetComponentData<CharacterMakerUI>(uiEntity).character;
    var equipment = EntityManager.GetComponentData<Equipment>(playerCharacter);
    // toggle gear?
    var partRemoved = equipment.RemoveGearPart(0, false);
    PostUpdateCommands.SetComponent(playerCharacter, equipment);
    PostUpdateCommands.AddComponent<SkeletonDirty>(playerCharacter);
}*/