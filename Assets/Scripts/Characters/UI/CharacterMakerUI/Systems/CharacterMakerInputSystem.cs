using Unity.Entities;
using Unity.Mathematics;
using Zoxel.UI;         // button click
using Zoxel.Transforms; // button parent
using Zoxel.Classes;    // setting class
using Zoxel.Races;      // setting race
using Zoxel.Animations; // setting animator
using Zoxel.Players;    /// SetControllerMapping when entering world
using Zoxel.Rendering;

namespace Zoxel.Characters.UI
{
    [UpdateInGroup(typeof(CharacterUISystemGroup))]
    public partial class CharacterMakerInputSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InputFinished>()
                .ForEach((Entity e, in Zoxel.Transforms.Child child, in PanelLink panelLink, in RenderText renderText, in ParentLink parentLink,
                    in CharacterMakerButton characterMakerButton) =>
            {
                var arrayIndex = child.index;
                var character = EntityManager.GetComponentData<CharacterMakerUI>(panelLink.panel).character;

                if (characterMakerButton.buttonType == CharacterMakerButtonType.Name)
                {
                    if (arrayIndex == 1)
                    {
                        OnNameUpdated(PostUpdateCommands, character, in renderText, parentLink.parent, arrayIndex);
                    }
                }
                else if (characterMakerButton.buttonType == CharacterMakerButtonType.Seed)
                {
                    if (arrayIndex == 1)
                    {
                        OnSeedUpdated(PostUpdateCommands, character, in renderText, parentLink.parent, arrayIndex);
                    }
                }
            }).WithoutBurst().Run();
        }        
        
        public void OnNameUpdated(EntityCommandBuffer PostUpdateCommands, Entity character, in RenderText renderText, Entity parentUI, int arrayIndex)
        {
            var zoxName = EntityManager.GetComponentData<ZoxName>(character);
            var name = renderText.text.Clone();
            if (name.Length == 0)
            {
                var random = new Random();
                random.InitState((uint) IDUtil.GenerateUniqueID());
                name = NameGenerator.GenerateName2(ref random);
                var input = EntityManager.GetComponentData<Childrens>(parentUI).children[arrayIndex];
                var renderText2 = EntityManager.GetComponentData<RenderText>(input);
                if (renderText2.SetText(name.ToString()))
                {
                    PostUpdateCommands.AddComponent<RenderTextDirty>(input);
                    PostUpdateCommands.SetComponent(input, renderText2);
                }
            }
            zoxName.SetName(name);
            PostUpdateCommands.SetComponent(character, zoxName);
        }

        public void OnSeedUpdated(EntityCommandBuffer PostUpdateCommands, Entity playerCharacter, in RenderText renderText, Entity parentUI, int arrayIndex)
        {
            var seed = 0;
            try
            {
                seed = int.Parse(renderText.text.ToString());
            }
            catch (System.Exception e)
            {
                UnityEngine.Debug.LogError("OnSeedUpdated: Wrong input: " + e);
            }
            if (seed == 0)
            {
                seed = IDUtil.GenerateUniqueID();
                var input = EntityManager.GetComponentData<Childrens>(parentUI).children[arrayIndex];
                var renderText2 = EntityManager.GetComponentData<RenderText>(input);
                if (renderText2.SetText(seed.ToString()))
                {
                    PostUpdateCommands.AddComponent<RenderTextDirty>(input);
                    PostUpdateCommands.SetComponent(input, renderText2);
                }
            }
            //var input = EntityManager.GetComponentData<Childrens>(ui).children[arrayIndex - 1];
            //var renderText = EntityManager.GetComponentData<RenderText>(input);
            PostUpdateCommands.SetComponent(playerCharacter, new ZoxID(seed));
            PostUpdateCommands.AddComponent<GenerateBody>(playerCharacter);
            PostUpdateCommands.AddComponent<CharacterMakerParticles>(playerCharacter);
        }
    }
}