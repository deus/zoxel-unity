using Unity.Entities;

namespace Zoxel.Characters
{
    //! A component for a CharacterMaker ui Button.
    public struct CharacterMakerButton : IComponentData
    { 
        public byte buttonType;

        public CharacterMakerButton(byte buttonType)
        {
            this.buttonType = buttonType;
        }
    }
}