namespace Zoxel.Characters
{
    //! Types of buttons in the CharacterMaker ui.
    public static class CharacterMakerButtonType
    {
        // Tabs
        public const byte Name = 0;
        public const byte Race = 1;
        public const byte Class = 2;
        public const byte Job = 3;
        public const byte Clan = 4;
        public const byte Dance = 5;
        public const byte DNA = 6;
        // other buttons
        public const byte BackDone = 7;
        public const byte Seed = 8;
        public const byte TabButtons = 9;
    }
}