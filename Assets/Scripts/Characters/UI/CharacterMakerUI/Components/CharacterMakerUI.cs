using Unity.Entities;

namespace Zoxel.Characters.UI
{
    public struct CharacterMakerUI : IComponentData
    {
        public Entity character;

        public CharacterMakerUI(Entity character)
        {
            this.character = character;
        }
    }
}