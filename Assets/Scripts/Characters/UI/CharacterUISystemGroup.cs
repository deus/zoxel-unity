﻿using Unity.Entities;

namespace Zoxel.Characters.UI
{
    //! Now, this is a CharacterUI! Ay!\
    [UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class CharacterUISystemGroup : ComponentSystemGroup { }
}
