using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Zoxel.UI;

namespace Zoxel.Characters.UI
{
    //! Spawns characters ui list based on realm player characters.
    /**
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(CharacterUISystemGroup))]
    public partial class CharacterLoadUIListSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity togglePrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        private void InitializePrefabs()
        {
            if (togglePrefab.Index != 0)
            {
                return;
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var buttonStyle = uiDatam.buttonStyle.GetStyle(uiScale);
            var panelStyle = uiDatam.panelStyle.GetStyle();
            var iconSize = uiDatam.GetIconSize(uiScale) * 0.64f;
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            togglePrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(togglePrefab);
            EntityManager.AddComponent<Toggle>(togglePrefab);
            EntityManager.AddComponent<CharacterLoadButton>(togglePrefab);
            UICoreSystem.SetRenderTextData(EntityManager, togglePrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, iconSize.y, textPadding);
        }
        
        //! \todo Spawn a generic control panel UI [ < - + > ] for both this and CharacterLoadUI
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            InitializePrefabs();
            var togglePrefab = this.togglePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((Entity e, in SpawnCharacterList spawnCharacterList) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnCharacterList>(e);
                var realmID = EntityManager.GetComponentData<ZoxID>(spawnCharacterList.realm).id; // get game from player object
                var saveLabels = new NativeList<Text>();
                var saveSlots = SaveUtilities.GetCharacterNames(realmID, true);
                foreach (var saveSlot in saveSlots)
                {
                    saveLabels.Add(saveSlot);
                }
                PostUpdateCommands.AddComponent(e, new SpawnButtonsList(in saveLabels));
                saveLabels.Dispose();
            }).WithoutBurst().Run();
        }
    }
}