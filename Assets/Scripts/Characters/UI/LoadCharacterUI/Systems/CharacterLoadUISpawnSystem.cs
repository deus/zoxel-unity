using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Cameras;
using Zoxel.Characters.UI;

namespace Zoxel.Characters.UI
{
    //! Spawn CharacterLoadUI.
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(CharacterUISystemGroup))]
    public partial class CharacterLoadUISpawnSystem : SystemBase
    {
        public static int maxSaveSlots = 5;
        public static Entity spawnPanelPrefab;
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;
        private Entity panelPrefab;
        private Entity toggleGroupPrefab;
        private Entity controlPanelPrefab;
        private Entity togglePrefab;
        private Entity controlButtonPrefab;
        private NativeArray<Text> texts;
        private float2 panelSize;

        /*protected override void OnCreate()
        {
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnCharacterLoadUI),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnPanelPrefab = EntityManager.CreateEntity(spawnArchetype);
            uiHoldersQuery = GetEntityQuery(
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<UILink>());
            var texts2 = new NativeList<Text>();
            texts2.Add(new Text("Who are you"));
            texts = texts2.ToArray(Allocator.Persistent);
            texts2.Dispose();
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }*/
        
        //! \todo Spawn a generic control panel UI [ < - + > ] for both this and CharacterLoadUI
        [BurstCompile]
        protected override void OnUpdate()
        {
            /*InitializePrefabs();
            var texts = this.texts;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var headerStyle = uiDatam.headerStyle.GetStyle(uiScale);
            var panelSize = this.panelSize;
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            HeaderSpawnSystem.InitializePrefabs(EntityManager);
            var headerPrefab = HeaderSpawnSystem.headerPrefab;
            var panelPrefab = this.panelPrefab;
            var toggleGroupPrefab = this.toggleGroupPrefab;
            var togglePrefab = this.togglePrefab;
            var controlPanelPrefab = this.controlPanelPrefab;
            var controlButtonPrefab = this.controlButtonPrefab;
            var controlsLabels = UICoreSystem.controlsLabels;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uiHolderEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
            var realmLinks = GetComponentLookup<RealmLink>(true);
            uiHolderEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<SpawnCharacterLoadUI>()
                .ForEach((int entityInQueryIndex, in CharacterLink characterLink) =>
            {
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, panelPrefab);
                // Header UI
                var headerText = texts[0].Clone();
                HeaderSpawnSystem.SpawnHeaderUI(PostUpdateCommands, entityInQueryIndex, headerPrefab, e3, panelSize,
                    headerStyle.fontSize, headerStyle.textPadding, in headerText);
                // Toggle Group
                var toggleGroupEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, toggleGroupPrefab, e3);
                UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, toggleGroupEntity, e3);
                // Spawn ControlPanel
                var controlPanelEntity = FooterUISpawnSystem.SpawnFooterUI(PostUpdateCommands, entityInQueryIndex, controlPanelPrefab, e3);
                UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, controlPanelEntity, e3);
                PostUpdateCommands.SetComponent(entityInQueryIndex, controlPanelEntity, new SpawnButtonsList(in controlsLabels));
                // Link to Character
                if (characterLink.character.Index > 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, characterLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab),
                        new OnUISpawned(characterLink.character, 1));
                    if (cameraLinks.HasComponent(characterLink.character))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLinks[characterLink.character]);
                    }
                    if (realmLinks.HasComponent(characterLink.character))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, toggleGroupEntity, new SpawnCharacterList(realmLinks[characterLink.character].realm));
                    }
                }
			})  .WithReadOnly(texts)
                .WithReadOnly(cameraLinks)
                .WithReadOnly(realmLinks)
                .WithReadOnly(controlsLabels)
                .ScheduleParallel(Dependency);*/
        }

        private void InitializePrefabs()
        {
            if (panelPrefab.Index != 0)
            {
                return;
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var buttonStyle = uiDatam.buttonStyle.GetStyle(uiScale);
            var headerStyle = uiDatam.headerStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var panelStyle = uiDatam.panelStyle.GetStyle();
            var iconSize = uiDatam.GetIconSize(uiScale) * 0.64f;
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            var textMargins = uiDatam.GetMenuMargins(uiScale);
            var margins = uiDatam.GetIconMargins(uiScale);
            var padding = uiDatam.GetIconPadding(uiScale);
            var subPanelStyle = uiDatam.subPanelStyle.GetStyle();
            var controlPanelStyle = uiDatam.controlPanelStyle.GetStyle();
            var minSize = new float2(10 * iconSize.x, 0);
            panelSize = new float2(iconSize.x * 14 + textPadding.x * 2f, 8f * (iconSize.y + textPadding.y * 2f));
            var footerHeight = headerStyle.fontSize + headerStyle.textPadding.y * 2f;
            var controlPanelSize = new float2(panelSize.x, footerHeight); // 1f * (iconSize.y + textPadding.y * 2f + padding.y * 2f));
            var toggleGroupPosition = float3.zero; // new float3(0, 0.5f * (iconSize.y + textPadding.y * 2f), 0);
            // panel
            panelPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(panelPrefab);
            EntityManager.RemoveComponent<SpawnHeaderUI>(panelPrefab);
            EntityManager.AddComponent<CharacterLoadUI>(panelPrefab);
            EntityManager.RemoveComponent<GridUI>(panelPrefab);
            EntityManager.RemoveComponent<GridUIDirty>(panelPrefab);
            EntityManager.RemoveComponent<NavigationDirty>(panelPrefab);
            EntityManager.SetComponentData(panelPrefab, new PanelUI(PanelType.CharacterLoadUI));
            EntityManager.AddComponentData(panelPrefab, new PanelMinSize(minSize));
            EntityManager.SetComponentData(panelPrefab, new Size2D(panelSize));
            EntityManager.SetComponentData(panelPrefab, new OnChildrenSpawned(1));
            // toggle group
            toggleGroupPrefab = EntityManager.Instantiate(UICoreSystem.toggleGroupPrefab);
            EntityManager.AddComponent<Prefab>(toggleGroupPrefab);
            EntityManager.SetComponentData(toggleGroupPrefab, new Size2D(panelSize));
            EntityManager.AddComponent<SpawnCharacterList>(toggleGroupPrefab);
            EntityManager.AddComponent<DisableGridResize>(toggleGroupPrefab);
            EntityManager.AddComponent<AutoGridY>(toggleGroupPrefab);
            EntityManager.SetComponentData(toggleGroupPrefab, new LocalPosition(toggleGroupPosition));
            EntityManager.SetComponentData(toggleGroupPrefab, new GridUI(padding, margins));
            EntityManager.SetComponentData(toggleGroupPrefab, new GridUISize(iconSize + textPadding * 2f));
            // control panel
            controlPanelPrefab = EntityManager.Instantiate(UICoreSystem.controlPanelFooterPrefab);
            EntityManager.AddComponent<Prefab>(controlPanelPrefab);
            EntityManager.SetComponentData(controlPanelPrefab, new Size2D(controlPanelSize));
            var controlPanelPosition = new float3(0, -0.5f * panelSize.y - controlPanelSize.y / 2f, 0);
            EntityManager.SetComponentData(controlPanelPrefab, new LocalPosition(controlPanelPosition));
            // toggle
            togglePrefab = EntityManager.Instantiate(UICoreSystem.togglePrefab);
            EntityManager.AddComponent<Prefab>(togglePrefab);
            EntityManager.AddComponent<CharacterLoadButton>(togglePrefab);
            UICoreSystem.SetRenderTextData(EntityManager, togglePrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, iconSize.y, textPadding);
            // control button
            controlButtonPrefab = EntityManager.Instantiate(UICoreSystem.controlButtonPrefab);
            EntityManager.AddComponent<Prefab>(controlButtonPrefab);
            EntityManager.AddComponent<CharacterLoadButton>(controlButtonPrefab);
            // link list to element prefab
            EntityManager.AddComponentData(toggleGroupPrefab, new ListPrefabLink(togglePrefab));
            EntityManager.SetComponentData(controlPanelPrefab, new ListPrefabLink(controlButtonPrefab));
        }
    }
}