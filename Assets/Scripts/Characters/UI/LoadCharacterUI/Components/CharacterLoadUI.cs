using Unity.Entities;

namespace Zoxel.Characters.UI
{
    //! A ui for loading Player Characters.
    public struct CharacterLoadUI : IComponentData { }
}