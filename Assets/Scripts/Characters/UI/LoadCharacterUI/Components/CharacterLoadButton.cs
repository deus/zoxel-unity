using Unity.Entities;

namespace Zoxel.Characters.UI
{
    //! A button element of the CharacterLoadUI.
    public struct CharacterLoadButton : IComponentData { }
}