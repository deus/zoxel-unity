using Unity.Entities;

namespace Zoxel.Characters.UI
{
    //! Spawns characters ui list based on realm player characters.
    public struct SpawnCharacterList : IComponentData
    {
        public Entity realm;

        public SpawnCharacterList(Entity realm)
        {
            this.realm = realm;
        }
    }
}