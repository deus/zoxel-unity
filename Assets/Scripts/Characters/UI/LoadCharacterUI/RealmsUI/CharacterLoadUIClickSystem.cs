using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Cameras;
using Zoxel.Cameras.Spawning;
using Zoxel.Transforms;
using Zoxel.Players;
using Zoxel.Rendering;
using Zoxel.Input;
using Zoxel.Voxels;
using Zoxel.Characters;
using Zoxel.Worlds;
using Zoxel.Weather;    // setting SetSkyAsCamera
using Zoxel.Realms.UI;

namespace Zoxel.Characters.UI
{
    //! When CharacterLoadButton is clicked!
    /**
    *   - UIClickEvent System -
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(CharacterUISystemGroup))]
    public partial class CharacterLoadUIClickSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        protected override void OnUpdate()
        {
            var spawnCharacterMakerPrefab = CharacterMakerSpawnSystem.spawnPanelPrefab;
            var spawnRealmLoadUIPrefab = RealmLoadUISpawnSystem.spawnPanelPrefab;
            var maxSaveSlots = CharacterLoadUISpawnSystem.maxSaveSlots;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<Toggle>()
                .WithAll<CharacterLoadButton>()
                .ForEach((Entity e, in Zoxel.Transforms.Child child, in PanelLink panelLink, in UIClickEvent uiClickEvent) =>
            {
                var buttonType = uiClickEvent.buttonType;
                if (buttonType != ButtonActionType.Confirm)
                {
                    return;
                }
                var player = uiClickEvent.controller;
                var panelEntity = panelLink.panel;
                var panelChildren = EntityManager.GetComponentData<Childrens>(panelEntity);
                var loadCharactersParent = panelChildren.children[0];
                if (loadCharactersParent.Index <= 0 || !HasComponent<ToggleGroup>(loadCharactersParent))
                {
                    return;
                }
                var saveSlotIndex = EntityManager.GetComponentData<ToggleGroup>(loadCharactersParent).selectedIndex;
                var realmEntity = EntityManager.GetComponentData<RealmLink>(player).realm; // get realmEntity from player object
                var realmID = EntityManager.GetComponentData<ZoxID>(realmEntity).id; // get realmEntity from player object
                var characterIDs = SaveUtilities.GetCharacterIDs(realmID, true);
                var arrayIndex = child.index;
                // Return to LoadRealm
                if (arrayIndex == 0)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(realmEntity);
                    UICoreSystem.RemoveUI(PostUpdateCommands, player, panelEntity);
                    var spawnEntity = PostUpdateCommands.Instantiate(spawnRealmLoadUIPrefab);
                    PostUpdateCommands.SetComponent(spawnEntity, new CharacterLink(player));
                }
                // Delete Character
                else if (arrayIndex == 1)
                {
                    if (saveSlotIndex >= 0 && saveSlotIndex < characterIDs.Length)
                    {
                        var characterID = int.Parse(characterIDs[saveSlotIndex]);
                        if (SaveUtilities.DeletePlayerCharacter(realmID, characterID))
                        {
                            // Deletes the button if deleted character
                            // UnityEngine.Debug.LogError("Removing Child: " + saveSlotIndex);
                            var children = EntityManager.GetComponentData<Childrens>(loadCharactersParent);
                            children.RemoveChild(saveSlotIndex, PostUpdateCommands);
                            PostUpdateCommands.SetComponent(loadCharactersParent, children);
                            PostUpdateCommands.AddComponent(loadCharactersParent, new SetToggleGroup((byte) math.max(0, saveSlotIndex - 1)));
                            PostUpdateCommands.AddComponent<GridUIDirty>(loadCharactersParent);
                            PostUpdateCommands.AddComponent<NavigationDirty>(panelEntity);
                        }
                    }
                }
                // new character
                else if (arrayIndex == 2)
                {
                    var canMakeNewCharacter = (characterIDs.Length < maxSaveSlots);
                    if (canMakeNewCharacter)
                    {
                        UICoreSystem.RemoveUI(PostUpdateCommands, player, panelEntity);
                        var newCharacterEntity = PostUpdateCommands.Instantiate(spawnCharacterMakerPrefab);
                        PostUpdateCommands.SetComponent(newCharacterEntity, new CharacterLink(player));
                        PostUpdateCommands.SetComponent(newCharacterEntity, new DelayEvent(elapsedTime, 0.2));
                    }
                }
                // confirm character
                else if (arrayIndex == 3)
                {
                    if (saveSlotIndex >= 0 && saveSlotIndex < characterIDs.Length)
                    {
                        var characterID = int.Parse(characterIDs[saveSlotIndex]);
                        LoadCharacter(EntityManager, PostUpdateCommands, realmEntity, player, panelEntity, characterID, elapsedTime);
                    }
                }
            }).WithoutBurst().Run();
        }

        private static void LoadCharacter(EntityManager EntityManager, EntityCommandBuffer PostUpdateCommands, Entity realmEntity, Entity playerEntity,
            Entity panelEntity, int characterID, double elapsedTime)
        {
            var fadeinScreenTime = UIManager.instance.uiSettings.fadeinScreenTime; // 1f;
            var fadeScreenWaitTime = UIManager.instance.uiSettings.fadeinScreenWaitTime; // 1f;
            // Remove UI
            UICoreSystem.RemoveUI(PostUpdateCommands, playerEntity, panelEntity);
            // Disable Controls
            PostUpdateCommands.AddComponent(playerEntity, new SetControllerMapping(ControllerMapping.None));
            // spawn new camera
            var spawnFirstPersonCamera = new SpawnFirstPersonCamera(playerEntity, new float3());
            var newCameraEntity = FirstPersonCameraSpawnSystem.SpawnFirstPersonCamera(PostUpdateCommands, in spawnFirstPersonCamera);
            // Fade out
            var screenFaderLink = EntityManager.GetComponentData<ScreenFaderLink>(playerEntity);
            PostUpdateCommands.SetComponent(screenFaderLink.screenFader, new Fader(0, 1, fadeinScreenTime));
            PostUpdateCommands.AddComponent(screenFaderLink.screenFader, new ReverseFader(fadeinScreenTime + fadeScreenWaitTime));
            PostUpdateCommands.AddComponent<FadeIn>(screenFaderLink.screenFader);
            PostUpdateCommands.AddComponent(screenFaderLink.screenFader, new TransitionCameraLink(newCameraEntity, elapsedTime, fadeinScreenTime));
            // Load Player Character
            var loadEntity = PostUpdateCommands.CreateEntity();
            PostUpdateCommands.AddComponent(loadEntity, new LoadPlayerCharacter(realmEntity, playerEntity, characterID));
            PostUpdateCommands.AddComponent(loadEntity, new DelayEvent(elapsedTime, fadeinScreenTime));
            // TransitionCamera
            var beforeCameraEntity = EntityManager.GetComponentData<CameraLink>(playerEntity).camera;
            var transitionEvent = PostUpdateCommands.CreateEntity();
            PostUpdateCommands.AddComponent(transitionEvent, new TransitionCamera(beforeCameraEntity, newCameraEntity));
            PostUpdateCommands.AddComponent(transitionEvent, new DelayEvent(elapsedTime, fadeinScreenTime));
            PostUpdateCommands.AddComponent<GenericEvent>(transitionEvent);
            // spawn other player cameras
            var playerHomeEntity = EntityManager.GetComponentData<PlayerHomeLink>(playerEntity).playerHome;
            PostUpdateCommands.AddComponent(playerHomeEntity, new SpawnPlayerCharacters(playerEntity));
        }
    }
}