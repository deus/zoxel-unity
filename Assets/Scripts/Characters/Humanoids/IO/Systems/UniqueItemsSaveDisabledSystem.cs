using Unity.Entities;
using Unity.Burst;
using Zoxel.Items;

namespace Zoxel.Characters
{
    //! Removes SaveUniqueItems component from entity when disabled.
    /**
    *   - Save System -
    */
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class UniqueItemsSaveDisabledSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<DisableSaving>(),
                ComponentType.ReadOnly<SaveUniqueItems>(),
                ComponentType.ReadOnly<ItemLinks>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<SaveUniqueItems>(processQuery);
        }
    }
}