using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Items;
using Zoxel.Skeletons;
using Zoxel.Voxels;

namespace Zoxel.Characters
{
    //! Loads inventory for character.
    /**
    *   - Load System -
    */
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class UniqueItemsLoadSystem : SystemBase
    {
        private const string filename = "UniqueItems.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<SlotLinks>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            BodyGenerationSystem.InitializePrefabs(EntityManager);
            Entities
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithAll<ItemLinks, RealmLink>()
                .ForEach((ref LoadUniqueItems loadUniqueItems, in EntitySaver entitySaver) =>
            {
                if (loadUniqueItems.loadPath.Length == 0)
                {
                    loadUniqueItems.loadPath = new Text(entitySaver.GetPath() + filename);
                }
                loadUniqueItems.reader.UpdateOnMainThread(in loadUniqueItems.loadPath);
            }).WithoutBurst().Run();
            var uniqueItemPrefab = BodyGenerationSystem.uniqueItemPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var slotLinks = GetComponentLookup<SlotLinks>(true);
            realmEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity, InitializeSaver>()
                .WithAll<ItemLinks, EntitySaver>()
                .ForEach((Entity e, int entityInQueryIndex, ref LoadUniqueItems loadUniqueItems, in RealmLink realmLink) =>
            {
                if (loadUniqueItems.reader.IsReadFileInfoComplete())
                {
                    if (loadUniqueItems.reader.DoesFileExist())
                    {
                        loadUniqueItems.reader.state = AsyncReadState.StartOpenFile;
                    }
                    else
                    {
                        // UnityEngine.Debug.LogError("File Did not exist.");
                        loadUniqueItems.Dispose();
                        PostUpdateCommands.RemoveComponent<LoadUniqueItems>(entityInQueryIndex, e);
                    }
                }
                else if (loadUniqueItems.reader.IsReadFileComplete())
                {
                    if (loadUniqueItems.reader.DidFileReadSuccessfully())
                    {
                        var bytes = loadUniqueItems.reader.GetBytes();
                        // var creatorLink = new CreatorLink(e);
                        var slotLinks2 = slotLinks[realmLink.realm];
                        byte itemsCount = 0;
                        var byteIndex = 0;
                        var userItemEntities = new NativeList<Entity>();
                        while (byteIndex < bytes.Length)
                        {
                            // Spawn Item
                            var itemEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, uniqueItemPrefab);
                            userItemEntities.Add(itemEntity);
                            // PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, creatorLink);
                            // Identification data
                            var itemID = ByteUtil.GetInt(in bytes, byteIndex);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new ZoxID(itemID));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new Seed(itemID));
                            byteIndex += 4;
                            // get length of string
                            var nameLength = bytes[byteIndex];
                            byteIndex += 1;
                            var name = new Text();
                            name.Initialize(nameLength);
                            for (byte i = 0; i < nameLength; i++)
                            {
                                name[i] = bytes[byteIndex + i];
                            }
                            byteIndex += nameLength;
                            PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new ZoxName(name));
                            // Vox Data
                            var size = ByteUtil.GetInt3(in bytes, byteIndex);   // X + 12 = 22
                            byteIndex += 12;
                            PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new ChunkDimensions(size));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new Chunk(size, 1));
                            var voxShapes = new VoxShapes();
                            var voxShapeslength = bytes[byteIndex];
                            byteIndex += 1;
                            voxShapes.shapes = new BlitableArray<VoxShape>(voxShapeslength, Allocator.Persistent);
                            for (byte i = 0; i < voxShapeslength; i++)
                            {
                                voxShapes.shapes[i] = new VoxShape(in bytes, byteIndex);
                                byteIndex += VoxShape.byteSize;
                            }
                            PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, voxShapes);
                            // Bone Data
                            var boneType = bytes[byteIndex];
                            byteIndex += 1;
                            PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new BoneType(boneType));
                            var slotID = ByteUtil.GetInt(in bytes, byteIndex);
                            byteIndex += 4;
                            var slotEntity = slotLinks2.GetSlot(slotID);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new SlotLink(slotEntity));
                            var positionOffset = ByteUtil.GetInt3(in bytes, byteIndex);
                            byteIndex += 12;
                            var femaleOffsetsLength = bytes[byteIndex];
                            byteIndex += 1;
                            var femaleOffsets = new NativeList<int3>(femaleOffsetsLength, Allocator.Temp);
                            for (byte i = 0; i < femaleOffsetsLength; i++)
                            {
                                femaleOffsets.Add(ByteUtil.GetInt3(in bytes, byteIndex));
                                byteIndex += 12;
                            }
                            PostUpdateCommands.SetComponent(entityInQueryIndex, itemEntity, new ItemSlotData(positionOffset, in femaleOffsets));
                            femaleOffsets.Dispose();
                            // finally increase count
                            itemsCount++;
                        }
                        if (userItemEntities.Length > 0)
                        {
                            var userItemEntitiesArray = userItemEntities.AsArray();
                            PostUpdateCommands.AddComponent(entityInQueryIndex, userItemEntitiesArray, new CreatorLink(e));
                            userItemEntitiesArray.Dispose();
                        }
                        userItemEntities.Dispose();
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUniqueItemsSpawned(itemsCount));
                    }
                    // Dispose
                    loadUniqueItems.Dispose();
                    PostUpdateCommands.RemoveComponent<LoadUniqueItems>(entityInQueryIndex, e);
                }
                // UnityEngine.Debug.LogError("Loaded Unique Items [" + itemsCount + "] at: " + filepath);
            })  .WithReadOnly(slotLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}