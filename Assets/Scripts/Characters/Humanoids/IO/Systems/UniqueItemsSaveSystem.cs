using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using System;
using System.IO;
using Zoxel.Items;
using Zoxel.Skeletons;
using Zoxel.Voxels;

namespace Zoxel.Characters
{
    //! Saves an entities Inventory data.
    /**
    *   - Save System -
    *   \todo Compile Bytes in parallel/burst.
    *   \todo Fix, sometimes UniqueItems.zox saves in root directory of project.
    */
    [UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class UniqueItemsSaveSystem : SystemBase
    {
        const string filename = "UniqueItems.zox";
        public const int bytesPerData = 34;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery itemsQuery;
        private EntityQuery slotsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            itemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UniqueItem>(),
                ComponentType.ReadOnly<Item>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<DestroyEntity>());
            slotsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Slot>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }
        
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands.RemoveComponent<SaveUniqueItems>(processQuery);
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var itemIDs = GetComponentLookup<ZoxID>(true);
            var itemNames = GetComponentLookup<ZoxName>(true);
            var boneTypes = GetComponentLookup<BoneType>(true);
            var itemSlotDatas = GetComponentLookup<ItemSlotData>(true);
            var slotLinks = GetComponentLookup<SlotLink>(true);
            var chunkDimensions = GetComponentLookup<ChunkDimensions>(true);
            var voxShapes = GetComponentLookup<VoxShapes>(true);
            itemEntities.Dispose();
            var slotEntities = slotsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var slotIDs = GetComponentLookup<ZoxID>(true);
            slotEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<LoadUniqueItems, OnUniqueItemsSpawned, GenerateBody>()
                .WithNone<InitializeSaver, DisableSaving>()
                .WithAll<SaveUniqueItems>()
                .ForEach((in EntitySaver entitySaver, in ItemLinks itemLinks) =>
            {
                var filepath = entitySaver.GetPath() + filename;
                var bytes = new NativeList<byte>();
                for (int i = 0; i < itemLinks.items.Length; i++)
                {
                    var itemEntity = itemLinks.items[i];
                    if (itemEntity.Index == 0 || !itemIDs.HasComponent(itemEntity))
                    {
                        for (var j = 0; j < bytesPerData; j++)
                        {
                            bytes.Add(0);    // 4
                        }
                        continue;
                    }
                    var itemID = itemIDs[itemEntity].id;
                    var itemName = itemNames[itemEntity].name;
                    var boneType = boneTypes[itemEntity].boneType;
                    var slotEntity = slotLinks[itemEntity].slot;
                    var slotID = slotIDs[slotEntity].id;
                    var itemSlotData = itemSlotDatas[itemEntity];
                    var size = chunkDimensions[itemEntity].voxelDimensions;
                    var voxShapes2 = voxShapes[itemEntity];
                    // ID, Name
                    ByteUtil.AddInt(ref bytes, itemID);
                    var nameSize = (byte) (itemName.Length);
                    bytes.Add(nameSize);
                    for (var j = 0; j < nameSize; j++)
                    {
                        bytes.Add(itemName[j]);
                    }
                    // Size, VoxData
                    ByteUtil.AddInt3(ref bytes, size);
                    var voxShapesLength = (byte) (voxShapes2.shapes.Length);
                    bytes.Add(voxShapesLength);
                    for (var j = 0; j < voxShapesLength; j++)
                    {
                        var voxShape = voxShapes2.shapes[j];
                        voxShape.AddBytes(ref bytes);
                    }
                    // BoneType, SlotID, ItemSlotData
                    bytes.Add(boneType);    // 4
                    ByteUtil.AddInt(ref bytes, slotID);
                    ByteUtil.AddInt3(ref bytes, itemSlotData.offset);
                    var femaleOffsetsLength = (byte) (itemSlotData.femaleOffsets.Length);
                    bytes.Add(femaleOffsetsLength);
                    for (var j = 0; j < femaleOffsetsLength; j++)
                    {
                        ByteUtil.AddInt3(ref bytes, itemSlotData.femaleOffsets[j]);
                    }
                }
                #if WRITE_ASYNC
                File.WriteAllBytesAsync(filepath, bytes.AsArray().ToArray());
                #else
                File.WriteAllBytes(filepath, bytes.AsArray().ToArray());
                #endif
                bytes.Dispose();
                // UnityEngine.Debug.LogError("Saved Unique items [" + itemLinks.items.Length + "] at: " + filepath);
            })  .WithReadOnly(itemIDs)
                .WithReadOnly(boneTypes)
                .WithReadOnly(slotLinks)
                .WithReadOnly(itemSlotDatas)
                .WithReadOnly(chunkDimensions)
                .WithReadOnly(voxShapes)
                .WithReadOnly(slotIDs)
                .WithoutBurst().Run();
        }
    }
}