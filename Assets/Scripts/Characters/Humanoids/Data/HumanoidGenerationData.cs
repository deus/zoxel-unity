using Unity.Mathematics;

namespace Zoxel.Characters
{
    //! Humanoid Generation data is used in GenerateBodySystem.
    public struct HumanoidGenerationData
    {
        public float height;
        public float3 voxScale;
        public int shoulderScale;
        public int shoulderLength;
        public int bicepScale;
        public int bicepLength;
        public int forearmLength;
        public int chestScale;
        public int chestLength;
        public int headScale;
        public int hipsScale;
        public int hipsLength;
        public int thighScale;
        public int thighLength;
        public int calfScale;
        public int calfLength;
        public ColorRGB skinColor;
        public ColorRGB hairColor;
        public ColorRGB eyeColor;

        public void SetColors(ColorRGB skinColor)
        {
            this.skinColor = skinColor;
            var hairHSV = skinColor.ToColor().GetHSV();
            hairHSV.x += 90;
            if (hairHSV.x > 360)
            {
                hairHSV.x -= 360;
            }
            hairHSV.z += 30;
            if (hairHSV.z > 100)
            {
                hairHSV.z -= 100;
            }
            this.hairColor = Color.FromHSV(hairHSV).ToColorRGB();
            var eyeHSV = skinColor.ToColor().GetHSV();
            eyeHSV.x += 180;
            if (eyeHSV.x > 360)
            {
                eyeHSV.x -= 360;
            }
            eyeHSV.y += 15;
            if (eyeHSV.y > 100)
            {
                eyeHSV.y -= 30;
                // eyeHSV.y = 100;
            }
            eyeHSV.z += 15;
            if (eyeHSV.z > 100)
            {
                //eyeHSV.z -= 100;
                eyeHSV.z -= 30;
            }
            this.eyeColor = Color.FromHSV(eyeHSV).ToColorRGB();
            // this.eyeColor = new ColorRGB(0, 255, 0);
        }
    }
}