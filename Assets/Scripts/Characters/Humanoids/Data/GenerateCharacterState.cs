namespace Zoxel.Characters
{
    public enum GenerateCharacterState : byte
    {
        SpawnParts,
        SetParts,
        SetParts2,
        SkeletonDirty
    }
}