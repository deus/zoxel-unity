using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;             // using VoxDatas and VoxShapes
using Zoxel.Skeletons;          // HumanoidBoneType enum
using Zoxel.Items;              // Spawning items dah
using Zoxel.Races;              // RaceLink and Race
using Zoxel.Items.Voxels;       // GenerateGameItemsSystem Prefab
// Save these VoxShapes as blueprints in the realm data - Blueprint Bicep - Blueprint Head - etc
//  Each blueprint has an amount of inputs to them
//  Saving a Hand for example will save the blueprint ID - and seed data
// Use DNA data to input for the blueprints
// This System Spawns body's items
// 2nd system - sets as equipment items

namespace Zoxel.Characters
{
    //! Generates a humanoid body.
    /**
    *   \todo Seperate out VoxShape data from functions, into a dataset - Vox Blueprints.
    *   \todo Add Generated Shirt, Pants, Boots
    *   \todo Undead dissapearss sometimes.
    *   \todo Blood Color - mark the entire body red - then have enough function paint the outside parts as skin
    *   \todo Scale all body parts seperately based on Race - Then Generate DNA - Use DNA as inputs to blueprint.
    *   \todo Move to Zoxel.Races.Voxels namespace.
    */
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class BodyGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity uniqueItemPrefab;
        private NativeArray<Text> humanoidSlotNames;
        private EntityQuery processQuery;
        private EntityQuery raceQuery;
        private EntityQuery realmsQuery;
        private EntityQuery slotsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            humanoidSlotNames = new NativeArray<Text>(11, Allocator.Persistent);
            humanoidSlotNames[0] = new Text("Chest");
            humanoidSlotNames[1] = new Text("Head");
            humanoidSlotNames[2] = new Text("Shoulder");
            humanoidSlotNames[3] = new Text("Bicep");
            humanoidSlotNames[4] = new Text("Forearm");
            humanoidSlotNames[5] = new Text("Hand");
            humanoidSlotNames[6] = new Text("Hips");
            humanoidSlotNames[7] = new Text("Thigh");
            humanoidSlotNames[8] = new Text("Calf");
            humanoidSlotNames[9] = new Text("Foot");
            humanoidSlotNames[10] = new Text("Hat");
            raceQuery = GetEntityQuery(
                ComponentType.ReadOnly<Race>(),
                ComponentType.Exclude<DestroyEntity>());
            realmsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<SlotLinks>(),
                ComponentType.Exclude<DestroyEntity>());
            slotsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Slot>(),
                ComponentType.ReadOnly<ZoxName>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < humanoidSlotNames.Length; i++)
            {
                humanoidSlotNames[i].Dispose();
            }
            humanoidSlotNames.Dispose();
        }

        public static void InitializePrefabs(EntityManager EntityManager)
        {
            if (BodyGenerationSystem.uniqueItemPrefab.Index != 0)
            {
                return;
            }
            var itemPrefab = ItemsGenerationSystem.itemPrefab;
            BodyGenerationSystem.uniqueItemPrefab = EntityManager.Instantiate(itemPrefab);
            EntityManager.AddComponent<Prefab>(BodyGenerationSystem.uniqueItemPrefab);
            EntityManager.AddComponent<UniqueItem>(BodyGenerationSystem.uniqueItemPrefab);
            EntityManager.AddComponent<LinkUniqueItem>(BodyGenerationSystem.uniqueItemPrefab);
            EntityManager.AddComponent<CreatorLink>(BodyGenerationSystem.uniqueItemPrefab);
            EntityManager.AddComponent<UniqueCharacterItem>(BodyGenerationSystem.uniqueItemPrefab);
            EntityManager.AddComponent<ItemEquipable>(BodyGenerationSystem.uniqueItemPrefab);
            EntityManager.AddComponent<SlotLink>(BodyGenerationSystem.uniqueItemPrefab);
            EntityManager.AddComponent<ItemSlotData>(BodyGenerationSystem.uniqueItemPrefab);
            EntityManager.AddComponent<VoxShapes>(BodyGenerationSystem.uniqueItemPrefab);
            EntityManager.AddComponent<GenerateVoxData>(BodyGenerationSystem.uniqueItemPrefab);
            EntityManager.AddComponent<Seed>(BodyGenerationSystem.uniqueItemPrefab);
            EntityManager.AddComponent<GenerateVox>(BodyGenerationSystem.uniqueItemPrefab);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs(EntityManager);
            var uniqueItemPrefab = BodyGenerationSystem.uniqueItemPrefab;
            var humanoidSlotNames = this.humanoidSlotNames;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var raceEntities = raceQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var races = GetComponentLookup<Race>(true);
            raceEntities.Dispose();
            var realmEntities = realmsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var slotLinks = GetComponentLookup<SlotLinks>(true);
            realmEntities.Dispose();
            var slotEntities2 = slotsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var slotNames2 = GetComponentLookup<ZoxName>(true);
            slotEntities2.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity, LoadRace>()
                .WithNone<OnUserEquipmentSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref GenerateBody generateBody, ref ItemLinks itemLinks, in ZoxID zoxID, in RaceLink raceLink,
                    in RealmLink realmLink, in VoxScale voxScale) =>
            {
                if (generateBody.state != (byte) GenerateCharacterState.SpawnParts || zoxID.id == 0 || raceLink.race.Index == 0)
                {
                    return;
                }
                generateBody.state = (byte) GenerateCharacterState.SetParts;
                // Clear previous parts
                for (int i = 0; i < itemLinks.items.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, itemLinks.items[i]);
                }
                itemLinks.Dispose();
                // get slot data
                var slotEntities = slotLinks[realmLink.realm].slots;
                var slots = new NativeArray<Entity>(slotEntities.Length, Allocator.Temp);
                var slotNames = new NativeArray<Text>(slotEntities.Length, Allocator.Temp);
                for (int i = 0; i < slotEntities.Length; i++)
                {
                    var slotEntity = slotEntities[i];
                    slots[i] = slotEntity;
                    slotNames[i] = slotNames2[slotEntity].name;
                }
                var random = new Random();
                random.InitState((uint) zoxID.id);
                var race = races[raceLink.race];
                // new float3(random.NextInt(360), 20 + random.NextInt(36), 8 + random.NextInt(30));
                var generationData = new HumanoidGenerationData();
                generationData.voxScale = voxScale.scale; // generateBody.voxScale; //
                generationData.shoulderScale = math.max(5, random.NextInt(race.shoulderDimensions.x, race.shoulderDimensions.y));
                generationData.shoulderLength = random.NextInt(race.shoulderLength.x, race.shoulderLength.y);
                // between bicep and shoulder
                generationData.bicepScale = random.NextInt(race.bicepDimensions.x, race.bicepDimensions.y);
                generationData.bicepLength = random.NextInt(race.bicepLength.x, race.bicepLength.y);
                generationData.forearmLength = random.NextInt(race.forearmLength.x, race.forearmLength.y);
                generationData.hipsScale = random.NextInt(race.hipsDimensions.x, race.hipsDimensions.y);
                generationData.hipsLength = random.NextInt(race.hipsLength.x, race.hipsLength.y);
                generationData.thighScale = random.NextInt(race.thighDimensions.x, race.thighDimensions.y);
                generationData.thighLength = random.NextInt(race.thighLength.x, race.thighLength.y);
                generationData.calfScale = random.NextInt(race.calfDimensions.x, race.calfDimensions.y);
                generationData.calfLength = random.NextInt(race.calfLength.x, race.calfLength.y);

                generationData.chestScale = random.NextInt(race.chestDimensions.x, race.chestDimensions.y);
                generationData.chestLength = random.NextInt(race.chestLength.x, race.chestLength.y);
                // generationData.hipsScale = random.NextInt(race.hipsDimensions.x, race.hipsDimensions.y);
                generationData.height = random.NextFloat(race.heightBase - race.heightVariance, race.heightBase + race.heightVariance);
                var skinHSV = race.hsvBase;
                skinHSV.x += random.NextFloat(-race.hsvVariance.x, race.hsvVariance.x);
                skinHSV.x %= 360;
                skinHSV.y += random.NextFloat(-race.hsvVariance.y, race.hsvVariance.y);
                skinHSV.y = math.clamp(skinHSV.y, 0, 100);
                skinHSV.z += random.NextFloat(-race.hsvVariance.z, race.hsvVariance.z);
                skinHSV.z = math.clamp(skinHSV.z, 0, 100);
                var skinColor = Color.GetColorFromHSV(skinHSV);
                generationData.SetColors(skinColor.ToColorRGB());
                //UnityEngine.Debug.LogError("generationData Color, SkinColorA: " + skinColor + " skinColorB: " + generationData.skinColor
                //    + ", ToFloat3: " + generationData.skinColor.ToFloat3() + ", HSV: " +  skinHSV);
                var replaceItemsCount = humanoidSlotNames.Length - 1; // 10;
                // === Has Hat? ===
                if (random.NextInt(100) >= 70)
                {
                    replaceItemsCount++;
                    // UnityEngine.Debug.LogError("Spawning Hat.");
                }
                for (byte i = 0; i < replaceItemsCount; i++)
                {
                    SpawnBodyPart(PostUpdateCommands, entityInQueryIndex, uniqueItemPrefab, e, zoxID.id, i, (byte) (i + 1), in slots, in slotNames, in humanoidSlotNames, in generationData);
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUniqueItemsSpawned((byte) replaceItemsCount));
                PostUpdateCommands.AddComponent<SaveUniqueItems>(entityInQueryIndex, e);
                slots.Dispose();
                slotNames.Dispose();
			})  .WithReadOnly(races)
                .WithReadOnly(slotLinks)
                .WithReadOnly(slotNames2)
                .WithReadOnly(humanoidSlotNames)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        //! Spawns a body part entity as a unique item.
        public static Entity SpawnBodyPart(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity uniqueItemPrefab, Entity e, int zoxID, byte index, byte boneType, 
            in NativeArray<Entity> slots, in NativeArray<Text> slotNames,  in NativeArray<Text> humanoidSlotNames, in HumanoidGenerationData generationData)
        {
            var partName = new Text();
            var itemID = zoxID + 1 + index;
            // todo: for all sub items, do this - call individual functions merely for shape information
            int3 size;
            VoxShapes voxShapes;
            var offset = int3.zero;
            var femaleOffsets = new NativeList<int3>();
            if (boneType == HumanoidBoneType.Chest)
            {
                partName = humanoidSlotNames[0];
                SpawnChest(itemID, in generationData, out size, out voxShapes, ref femaleOffsets);
            }
            else if (boneType == HumanoidBoneType.Head)
            {
                partName = humanoidSlotNames[1];
                SpawnHead(itemID, in generationData, out size, out voxShapes);
            }
            else if (boneType == HumanoidBoneType.Shoulder)
            {
                partName = humanoidSlotNames[2];
                SpawnShoulder(itemID, in generationData, out size, out voxShapes, ref femaleOffsets);
                // Add parent body index - based on input
            }
            else if (boneType == HumanoidBoneType.Bicep)
            {
                partName = humanoidSlotNames[3];
                SpawnBicep(itemID, in generationData, out size, out voxShapes);
            }
            else if (boneType == HumanoidBoneType.Forearm)
            {
                partName = humanoidSlotNames[4];
                SpawnForearm(itemID, in generationData, out size, out voxShapes);
            }
            else if (boneType == HumanoidBoneType.Hand)
            {
                partName = humanoidSlotNames[5];
                SpawnHand(itemID, in generationData, out size, out voxShapes);
            }
            else if (boneType == HumanoidBoneType.Hips)
            {
                partName = humanoidSlotNames[6];
                SpawnHips(itemID, in generationData, out size, out voxShapes, ref femaleOffsets);
            }
            else if (boneType == HumanoidBoneType.Thigh)
            {
                partName = humanoidSlotNames[7];
                SpawnThigh(itemID, in generationData, out size, out voxShapes);
            }
            else if (boneType == HumanoidBoneType.Calf)
            {
                partName = humanoidSlotNames[8];
                SpawnCalf(itemID, in generationData, out size, out voxShapes);
            }
            else if (boneType == HumanoidBoneType.Foot)
            {
                partName = humanoidSlotNames[9];
                offset = SpawnFoot(itemID, in generationData, out size, out voxShapes);
            }
            else if (boneType == HumanoidBoneType.Hat)
            {
                partName = humanoidSlotNames[10];
                SpawnHat(itemID, out size, out voxShapes);
                offset.y = -2;
            }
            else
            {
                return new Entity();
            }
            var newItemEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, uniqueItemPrefab);
            PostUpdateCommands.SetComponent(entityInQueryIndex, newItemEntity, new ZoxID(itemID));
            PostUpdateCommands.SetComponent(entityInQueryIndex, newItemEntity, new ZoxName(partName.Clone()));
            PostUpdateCommands.SetComponent(entityInQueryIndex, newItemEntity, new BoneType(boneType));
            PostUpdateCommands.SetComponent(entityInQueryIndex, newItemEntity, new SlotLink(GetItemSlot(in slots, in slotNames, in partName)));
            PostUpdateCommands.SetComponent(entityInQueryIndex, newItemEntity, new ItemSlotData(offset, femaleOffsets));
            PostUpdateCommands.SetComponent(entityInQueryIndex, newItemEntity, new ChunkDimensions(size));
            PostUpdateCommands.SetComponent(entityInQueryIndex, newItemEntity, new Chunk(size, 1));
            PostUpdateCommands.SetComponent(entityInQueryIndex, newItemEntity, new CreatorLink(e));
            PostUpdateCommands.SetComponent(entityInQueryIndex, newItemEntity, voxShapes);
            PostUpdateCommands.SetComponent(entityInQueryIndex, newItemEntity, new Seed(itemID));
            femaleOffsets.Dispose();
            return newItemEntity;
        }

        private static Entity GetItemSlot(in NativeArray<Entity> slots, in NativeArray<Text> slotNames, in Text slotName)
        {
            for (int i = 0; i < slotNames.Length; i++)
            {
                if (slotNames[i] == slotName)
                {
                    return slots[i];
                }
            }
            return new Entity();
        }

        // move this into a new VoxGeneration System
        private static void SpawnHead(int partID, in HumanoidGenerationData generationData, out int3 size, out VoxShapes voxShapes)
        {
            var skinColor = generationData.skinColor.MultiplyBrightness(1.2f);
            voxShapes = new VoxShapes();
            var random = new Random();
            random.InitState((uint) partID);
            size = new int3(10 + 2 * random.NextInt(3), 0, 10 + 2 * random.NextInt(3)); // 12 + 2 * random.NextInt(3)
            size.y = (int)((0.9f * generationData.height / 6.0f) / generationData.voxScale.y); // (1 / 48f));
            // UnityEngine.Debug.LogError("Head Height: " + size.y);
            // UnityEngine.Debug.LogError("Skin Color: " + skinColor + " and eye: " + eyeColor);
            // Add VoxShapes to this
            voxShapes.shapes = new BlitableArray<VoxShape>(5, Allocator.Persistent);
            var neckSize = 2;
            var headspherePosition = new int3(0, 0, 0);
            var headsphereRadius = math.min(math.min((size.x / 2), (size.y / 2)), (size.z / 2));
            var headsphereMaxRadius = math.max(math.max((size.x / 2), (size.y / 2)), (size.z / 2));
            if (headsphereRadius >= (size.y / 2) - neckSize / 2)
            {
                // UnityEngine.Debug.LogError("headsphereRadius: " + headsphereRadius + ", headsphereMaxRadius: " + headsphereMaxRadius);
                headsphereRadius = (size.y / 2) - neckSize / 2;
            }
            // headspherePosition.y = math.abs((size.y / 2) - neckSize - headsphereRadius);
            // anchor to top
            headspherePosition.y = (size.y / 2) - headsphereRadius;
            var headsphereSize = new int3(headsphereRadius, headsphereRadius, headsphereRadius);
            // UnityEngine.Debug.LogError("Size: " + size + ", Head Sphere: " + headspherePosition + " : " + headsphereSize);
            // Head
            voxShapes.shapes[0] = new VoxShape(skinColor, VoxShapeType.Sphere, headspherePosition, headsphereSize);
            // Neck
            voxShapes.shapes[1] = new VoxShape(skinColor,
                VoxShapeType.Cylinder,
                new int3(0, - (size.y / 4) - 1, 0),
                new int3((size.x / 4), size.y / 4, (size.z / 4)));
            // Hair
            voxShapes.shapes[2] = new VoxShape(generationData.hairColor,
                VoxShapeType.Cube, VoxBrushType.Paint,
                new int3(0, (size.y / 2), 0),
                new int3((size.x / 2), (headsphereRadius / 3), (size.z / 2)));
            // Eyes
            var eyeDistanceApart = 2;
            var eyeSize = 0;
            var eyeDepth = 2;
            var eyePosition = new int3(-eyeDistanceApart, headspherePosition.y, -(size.z / 2) + (headsphereMaxRadius - headsphereRadius));
            voxShapes.shapes[3] = new VoxShape(generationData.eyeColor, VoxShapeType.Cube, VoxBrushType.Paint, eyePosition, new int3(eyeSize, eyeSize, eyeDepth));
            eyePosition.x += 2 * eyeDistanceApart - 1;
            voxShapes.shapes[4] = new VoxShape(generationData.eyeColor, VoxShapeType.Cube, VoxBrushType.Paint, eyePosition, new int3(eyeSize, eyeSize, eyeDepth));
        }

        // move this into a new VoxGeneration System
        private static void SpawnChest(int partID, in HumanoidGenerationData generationData, out int3 size, out VoxShapes voxShapes, ref NativeList<int3> femaleOffsets)
        {
            voxShapes = new VoxShapes();
            var random = new Random();
            random.InitState((uint) partID);
            var scale = generationData.chestScale; // 14-20 originally
            // 12 to 18
            size = new int3(generationData.chestScale, generationData.chestLength, (int) (generationData.chestScale * (2f / 3f)));        //14 + random.NextInt(6), 0, 10 + random.NextInt(6));
            // size.y = (int) math.ceil((1.4f * generationData.height / 6f) / generationData.voxScale.y); // (1 / 48f));
            //UnityEngine.Debug.LogError("Chest Height: " + size.y);
            var neckSize = new int3(size.x / 4, size.y / 8, size.x / 4);
            var stomacheSize = new int3(size.x / 3, size.y / 3, size.z / 3);
            var chestSize = new int3(size.x / 2, (size.y / 5), (size.z / 2));
            voxShapes.shapes = new BlitableArray<VoxShape>(3, Allocator.Persistent);
            // neck
            voxShapes.shapes[0] = new VoxShape(generationData.skinColor, VoxShapeType.Cube, new int3(0, (size.y / 2), 0), neckSize);
            // chest
            voxShapes.shapes[1] = new VoxShape(generationData.skinColor, VoxShapeType.Cube, new int3(0, (size.y / 4) - 1, 0), chestSize); //  + 1
            // stomache
            stomacheSize.y += 1;
            voxShapes.shapes[2] = new VoxShape(generationData.skinColor, VoxShapeType.Cube, new int3(0, - (size.y / 4) + 1, 0), stomacheSize);
            // add shoulder offsets here
            // head
            femaleOffsets.Add(int3.zero);
            // hips
            femaleOffsets.Add(new int3(0, 0, -1));
            // shoulders
            var shoulderSize = generationData.shoulderScale; // 2;
            femaleOffsets.Add(new int3(0, (size.y / 2) - shoulderSize / 2, 1));
            femaleOffsets.Add(new int3(0, (size.y / 2) - shoulderSize / 2, 1));
            // shirt
            // backpack
        }

        private static void SpawnHips(int partID, in HumanoidGenerationData generationData, out int3 size, out VoxShapes voxShapes, ref NativeList<int3> femaleOffsets)
        {
            var random = new Random();
            random.InitState((uint) partID);
            size = new int3(generationData.hipsLength, generationData.hipsScale, generationData.hipsScale);
            var shapes = new NativeList<VoxShape>();
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.Cube, new int3(0, 0, 0), size / 2));
            voxShapes = new VoxShapes(in shapes);
            // add shoulder offsets here
            // thigh 1 and 2
            // og is 4, 3, 1
            // (size.x / 2) + 1
            var offsetX = generationData.hipsLength / 2 + generationData.thighScale / 2;    // 4
            if (offsetX > 2)
            {
                offsetX --;
            }
            var thighOffset = new int3(offsetX, size.y / 2, 0); // z=1
            femaleOffsets.Add(thighOffset);
            femaleOffsets.Add(thighOffset);
            // Pants - shouldn't this depend on how tall the pants are?
            femaleOffsets.Add(new int3(0, size.y, 0));  // og is 0, 7, 0
            shapes.Dispose();
        }

        private static void SpawnThigh(int partID, in HumanoidGenerationData generationData, out int3 size, out VoxShapes voxShapes)
        {
            var random = new Random();
            random.InitState((uint) partID);
            var bumpHeight = random.NextInt(1, 3);
            size = new int3(generationData.thighScale, generationData.thighLength, generationData.thighScale);
            var innerSize = size.x - random.NextInt(2);
            var shapes = new NativeList<VoxShape>();
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.Cube, new int3(0, 0, 0), new int3(innerSize, size.y / 2, innerSize)));
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.Cube, new int3(0, size.y / 2, 0), new int3(size.x, bumpHeight, size.z)));
            voxShapes = new VoxShapes(in shapes);
            shapes.Dispose();
        }

        private static void SpawnCalf(int partID, in HumanoidGenerationData generationData, out int3 size, out VoxShapes voxShapes)
        {
            var random = new Random();
            random.InitState((uint) partID);
            var bumpHeight = random.NextInt(1, 3);
            size = new int3(generationData.calfScale, generationData.calfLength, generationData.calfScale);
            var innerSize = size.x - random.NextInt(2);
            var shapes = new NativeList<VoxShape>();
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.Cube, new int3(0, 0, 0), new int3(innerSize, size.y / 2, innerSize)));
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.Cube, new int3(0, size.y / 2, 0), new int3(size.x, bumpHeight, size.z)));
            voxShapes = new VoxShapes(in shapes);
            shapes.Dispose();
        }

        private static int3 SpawnFoot(int partID, in HumanoidGenerationData generationData, out int3 size, out VoxShapes voxShapes)
        {
            var random = new Random();
            random.InitState((uint) partID);
            var heightBump = 2;
            var footLength = random.NextInt(8, 14);
            size = new int3(generationData.calfScale, generationData.calfScale + heightBump, footLength);
            var shapes = new NativeList<VoxShape>();
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.CubeNonCentred, new int3(0, 0, 0), new int3(size.x, size.y / 2, size.z)));
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.CubeNonCentred, new int3(0, size.y / 2, 3 * size.z / 4), new int3(size.x, 1 + size.y / 2, size.z / 4)));
            voxShapes = new VoxShapes(in shapes);
            shapes.Dispose();
            return new int3(0, 0, 1 - size.z / 2);
        }

        private static void SpawnShoulder(int partID, in HumanoidGenerationData generationData, out int3 size, out VoxShapes voxShapes, ref NativeList<int3> femaleOffsets)
        {
            var shapes = new NativeList<VoxShape>();
            var random = new Random();
            random.InitState((uint) partID);
            size = new int3(generationData.shoulderLength, generationData.shoulderScale, generationData.shoulderScale);
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.CurveCubeZ, new int3(0, 0, 0), size / 2));
            voxShapes = new VoxShapes(in shapes);
            femaleOffsets.Add(new int3((generationData.shoulderLength / 2) - 2, 0, 0));
            shapes.Dispose();
        }

        private static void SpawnBicep(int partID, in HumanoidGenerationData generationData, out int3 size, out VoxShapes voxShapes)
        {
            var shapes = new NativeList<VoxShape>();
            var random = new Random();
            random.InitState((uint) partID);
            size = new int3(generationData.bicepScale, generationData.bicepLength, generationData.bicepScale);
            var outerBicepSize = (size.x / 2);
            var innerBicepSize = math.max(outerBicepSize - 1, 0);
            var outerBicepHeight = math.max(size.y / 4, 2);
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.Cube, new int3(0, 0, 0), new int3(innerBicepSize, size.y / 2, innerBicepSize)));
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.Cube, new int3(0, -size.y / 2, 0), new int3(outerBicepSize, outerBicepHeight, outerBicepSize)));
            voxShapes = new VoxShapes(in shapes);
            shapes.Dispose();
        }

        private static void SpawnForearm(int partID, in HumanoidGenerationData generationData, out int3 size, out VoxShapes voxShapes)
        {
            var shapes = new NativeList<VoxShape>();
            var random = new Random();
            random.InitState((uint) partID);
            var scale = generationData.bicepScale; // 4 + random.NextInt(4);
            size = new int3(scale, generationData.forearmLength, scale);
            var outerBicepSize = (size.x / 2);
            var innerBicepSize = math.max(outerBicepSize - 1, 1);
            var outerBicepHeight = math.max(size.y / 4, 2);
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.Cube, new int3(0, 0, 0), new int3(innerBicepSize, size.y / 2, innerBicepSize)));
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.Cube, new int3(0, size.y / 2, 0), new int3(outerBicepSize, outerBicepHeight, outerBicepSize)));
            voxShapes = new VoxShapes(in shapes);
            shapes.Dispose();
        }

        private static void SpawnHand(int partID, in HumanoidGenerationData generationData, out int3 size, out VoxShapes voxShapes)
        {
            var shapes = new NativeList<VoxShape>();
            var random = new Random();
            random.InitState((uint) partID);
            size = new int3(4, 8, 6);
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.CubeNonCentred, new int3(1, size.y / 2, 0), new int3(size.x - 1, (size.y / 2), size.z)));
            // palm
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.CubeNonCentred, new int3(0, (size.y / 2) + 1, 1), new int3(0, (size.y / 2) - 1, size.z - 3)));
            // fingers
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.CubeNonCentred, new int3(3, 0, 1), new int3(0, size.y / 2, 0)));
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.CubeNonCentred, new int3(3, 0, 3), new int3(0, size.y / 2, 0)));
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.CubeNonCentred, new int3(3, 0, 5), new int3(0, size.y / 2, 0)));
            shapes.Add(new VoxShape(generationData.skinColor, VoxShapeType.CubeNonCentred, new int3(1, 1, 0), new int3(0, (size.y / 2) - 1, 0)));    // thumb
            voxShapes = new VoxShapes(in shapes);
            shapes.Dispose();
        }

        private static void SpawnHat(int partID, out int3 size, out VoxShapes voxShapes)
        {
            var shapes = new NativeList<VoxShape>();
            var random = new Random();
            random.InitState((uint) partID);
            var hatColor = new ColorRGB(122, 55, 55);
            var hatColor2 = new ColorRGB(88, 33, 33);
            var hatHeight = random.NextInt(6, 14);
            var bottomHatHeight = random.NextInt(1, 3);
            var hatRadius = 6 + random.NextInt(0, 3) * 2;
            var topHatRadius = hatRadius - random.NextInt(1, 3) * 2;
            size = new int3(hatRadius, hatHeight, hatRadius);
            // bottom part
            shapes.Add(new VoxShape(hatColor, VoxShapeType.CubeNonCentred, new int3(0, 0, 0), new int3(size.x, bottomHatHeight - 1, size.z)));
            // top part
            shapes.Add(new VoxShape(hatColor2, VoxShapeType.CubeNonCentred,
                new int3((hatRadius - topHatRadius) / 2, bottomHatHeight, (hatRadius - topHatRadius) / 2),
                new int3(topHatRadius - 1, hatHeight - bottomHatHeight, topHatRadius - 1)));
            voxShapes = new VoxShapes(in shapes);
            shapes.Dispose();
        }
    }
}

/*for (int i = 0; i < equipment.body.Length; i++)
{
    UnityEngine.Debug.LogError(i + ": bodyIndex: " + equipment.body[i].bodyIndex +
        ", slotIndex: " + equipment.body[i].slotIndex);
}*/
/*for (int i = 0; i < equipment.body.Length; i++)
{
    UnityEngine.Debug.LogError("After: " + i + ": bodyIndex: " + equipment.body[i].bodyIndex +
        ", slotIndex: " + equipment.body[i].slotIndex);
}*/