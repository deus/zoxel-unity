using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Items;
using Zoxel.Skeletons;

namespace Zoxel.Characters
{
    //! OnBonesSpawnedSystem: Waits for bones to spawn, then links them to boneLinks
    /**
    *   \todo Make more dynamic, remove use of static parent datas for bones.
    */
    [UpdateBefore(typeof(BodyGenerationSystem))]
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class BodyPartsAttachSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery itemsQuery;
        private EntityQuery slotsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            itemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UniqueItem>(),
                ComponentType.ReadOnly<SlotLink>());
            slotsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Slot>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var userEquipmentPrefab = ItemSystemGroup.userEquipmentPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var partEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var slotLinks = GetComponentLookup<SlotLink>(true);
            var slotEntities = slotsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var slots = GetComponentLookup<Slot>(true);
            partEntities.Dispose();
            slotEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<OnUniqueItemsSpawned>()
                .ForEach((Entity e, int entityInQueryIndex, ref GenerateBody generateBody, ref Equipment equipment, in ItemLinks itemLinks) =>
            {
                if (generateBody.state == (byte) GenerateCharacterState.SetParts)
                {
                    generateBody.state = (byte) GenerateCharacterState.SetParts2;
                    return;
                }
                else if (generateBody.state != (byte) GenerateCharacterState.SetParts2)
                {
                    return;
                }
                generateBody.state = (byte) GenerateCharacterState.SkeletonDirty;
                if (HasComponent<LoadEquipment>(e))
                {
                    return;
                }
                // Spawn New UserEquipment Here
                var bodyPartMetaDatas = new NativeList<Entity>();
                var equipmentItems = new NativeList<EquipmentParent>();
                for (byte i = 0; i < itemLinks.items.Length; i++)
                {
                    var itemEntity = itemLinks.items[i];
                    SpawnEquipmentItems(ref bodyPartMetaDatas, (byte) (i + 1), itemEntity);
                }
                var userEquipmentEntities = new NativeArray<Entity>(bodyPartMetaDatas.Length, Allocator.Temp);
                var creatorLink = new CreatorLink(e);
                for (int i = 0; i < userEquipmentEntities.Length; i++)
                {
                    var userEquipmentEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, userEquipmentPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userEquipmentEntity, new UserDataIndex(i));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userEquipmentEntity, creatorLink);
                    userEquipmentEntities[i] = (userEquipmentEntity);
                }
                for (byte i = 0; i < itemLinks.items.Length; i++)
                {
                    var itemEntity = itemLinks.items[i];
                    GetParentItems(in userEquipmentEntities, in bodyPartMetaDatas, ref equipmentItems, (byte) (i + 1), itemEntity, in slotLinks, in slots);
                }
                for (byte i = 0; i < userEquipmentEntities.Length; i++)
                {
                    var userEquipmentEntity = userEquipmentEntities[i];
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userEquipmentEntity, new MetaData(bodyPartMetaDatas[i]));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userEquipmentEntity, equipmentItems[i]);
                }
                // destroy previous
                for (int i = 0; i < equipment.body.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, equipment.body[i]);
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUserEquipmentSpawned((byte) userEquipmentEntities.Length));
                equipment.Dispose();
                bodyPartMetaDatas.Dispose();
                equipmentItems.Dispose();
                userEquipmentEntities.Dispose();
            })  .WithReadOnly(slotLinks)
                .WithReadOnly(slots)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        // in NativeArray<Entity> userEquipmentEntities, ref NativeList<EquipmentParent> equipmentItems, 

        private static void SpawnEquipmentItems(ref NativeList<Entity> bodyPartMetaDatas, byte partType, Entity itemEntity)
        {
            var limbAttachCount = 1;
            //var limbParentCount = 1;
            if (partType == HumanoidBoneType.Shoulder ||
                partType == HumanoidBoneType.Bicep || 
                partType == HumanoidBoneType.Forearm || 
                partType == HumanoidBoneType.Hand || 
                partType == HumanoidBoneType.Thigh || 
                partType == HumanoidBoneType.Calf || 
                partType == HumanoidBoneType.Foot)
            {
                limbAttachCount = 2;
                /*if (partType == HumanoidBoneType.Bicep ||
                    partType == HumanoidBoneType.Forearm ||
                    partType == HumanoidBoneType.Hand ||
                    partType == HumanoidBoneType.Calf ||
                    partType == HumanoidBoneType.Foot)
                {
                    limbParentCount = 2;
                }*/
            }
            else if (partType == HumanoidBoneType.Hips)
            {
            }
            else if (partType == HumanoidBoneType.Hat)
            {
                //parentBodyIndex = (byte) (HumanoidBoneType.Head);
                // layer = SlotLayer.Gear;
                // UnityEngine.Debug.LogError("Adding UserEntity for hat.");
            }
            else if (partType == HumanoidBoneType.Held)
            {
                return;
            }
            if (limbAttachCount == 2)
            {
                bodyPartMetaDatas.Add(itemEntity);
            }
            bodyPartMetaDatas.Add(itemEntity);
        }
        
        private static void GetParentItems(in NativeArray<Entity> userEquipmentEntities, in NativeList<Entity> itemEntities,
            ref NativeList<EquipmentParent> equipmentItems, byte partType, Entity itemEntity,
            in ComponentLookup<SlotLink> slotLinks, in ComponentLookup<Slot> slots)
        {
            var limbAttachCount = 1;
            var parentBodyIndex = (byte) 0;     // chest - part body array index
            var limbParentCount = 1;
            var parentSlotIndex = (byte) 0;     // chest has 5 sslots you can plug into
            var parentSlotIndex2 = (byte) 0;
            if (partType == HumanoidBoneType.Shoulder ||
                partType == HumanoidBoneType.Bicep || 
                partType == HumanoidBoneType.Forearm || 
                partType == HumanoidBoneType.Hand || 
                partType == HumanoidBoneType.Thigh || 
                partType == HumanoidBoneType.Calf || 
                partType == HumanoidBoneType.Foot)
            {
                limbAttachCount = 2;
                if (partType == HumanoidBoneType.Bicep ||
                    partType == HumanoidBoneType.Forearm ||
                    partType == HumanoidBoneType.Hand ||
                    partType == HumanoidBoneType.Calf ||
                    partType == HumanoidBoneType.Foot)
                {
                    limbParentCount = 2;
                }
                if (partType == HumanoidBoneType.Thigh)
                {
                    parentBodyIndex = 10;
                    parentSlotIndex2 = 1;
                }
                else if (partType == HumanoidBoneType.Shoulder)
                {
                    parentSlotIndex = 2;
                    parentSlotIndex2 = 3;
                }
                else if (partType == HumanoidBoneType.Bicep)
                {
                    parentBodyIndex = 2;
                }
                else if (partType == HumanoidBoneType.Forearm)
                {
                    parentBodyIndex = 4;
                }
                else if (partType == HumanoidBoneType.Hand)
                {
                    parentBodyIndex = 6;
                }
                else if (partType == HumanoidBoneType.Calf)
                {
                    parentBodyIndex = 11;
                }
                else if (partType == HumanoidBoneType.Foot)
                {
                    parentBodyIndex = 13;
                }
            }
            else if (partType == HumanoidBoneType.Hips)
            {
                parentSlotIndex = (byte) (HumanoidBoneType.Chest); // 1;
            }
            else if (partType == HumanoidBoneType.Hat)
            {
                parentBodyIndex = (byte) (HumanoidBoneType.Head - 1);
            }
            else if (partType == HumanoidBoneType.Held)
            {
                return;
            }
            var parentItemEntity = itemEntities[parentBodyIndex];
            var equipmentItem = new EquipmentParent(userEquipmentEntities[parentBodyIndex], parentSlotIndex);
            if (limbAttachCount == 2)
            {
                var equipmentItem2 = new EquipmentParent();
                if (limbParentCount == 1)
                {
                    equipmentItem2.parent = equipmentItem.parent;
                }
                else
                {
                    var bodyIndex = (byte) (parentBodyIndex + 1);
                    equipmentItem2.parent = userEquipmentEntities[bodyIndex];
                }
                equipmentItem2.parentSlotIndex = parentSlotIndex2;
                equipmentItems.Add(equipmentItem2);
            }
            equipmentItems.Add(equipmentItem);
        }
    }
}