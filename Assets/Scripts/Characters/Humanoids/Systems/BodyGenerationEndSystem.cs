using Unity.Entities;
using Unity.Burst;
using Zoxel.Skeletons;
using Zoxel.Items;
using Zoxel.Voxels;

namespace Zoxel.Characters
{
    //! Ends GenerateBody event.
    /**
    *   - End System -
    *   Adds a SkeletonDirty component if not loading LoadEquipment.
    *   Note: Have to wait for items to spawn, so this has to update the frame after the other.
    *   \todo Move to Race System eventually.
    *   \todo Blood Color - mark the entire body red - then have enough function paint the outside parts as skin
    */
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class BodyGenerationEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity, SkeletonDirty>()
                .ForEach((Entity e, int entityInQueryIndex, in GenerateBody generateBody, in ItemLinks itemLinks) =>
            {
                if (generateBody.state != (byte) GenerateCharacterState.SkeletonDirty)
                {
                    return;
                }
                for (int i = 0; i < itemLinks.items.Length; i++)
                {
                    var itemEntity = itemLinks.items[i];
                    if (HasComponent<GenerateVox>(itemEntity))
                    {
                        // UnityEngine.Debug.LogError("Body part still growing.");
                        return;
                    }
                }
                // UnityEngine.Debug.LogError("Finished Generating Body: " + e.Index);
                PostUpdateCommands.RemoveComponent<GenerateBody>(entityInQueryIndex, e);
                if (!HasComponent<LoadEquipment>(e))
                {
                    PostUpdateCommands.AddComponent<SkeletonDirty>(entityInQueryIndex, e);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}