using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Characters
{
    public struct SpawnMultipleCharacters : IComponentData
    {
        public int amount;
        public BlitableArray<int> ids;

        public void InitializeIDs()
        {
            Dispose();
            ids = new BlitableArray<int>(amount, Allocator.Persistent);
            for (int i = 0; i < ids.Length; i++)
            {
                ids[i] = IDUtil.GenerateUniqueID();
            }
        }
        
        public void Dispose()
        {
            if (ids.Length > 0)
            {
                ids.Dispose();
            }
        }
    }
}