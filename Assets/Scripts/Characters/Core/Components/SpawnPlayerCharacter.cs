using Unity.Entities;

namespace Zoxel.Characters
{
    //! Spawns a PlayerCharacter entity.
    public struct SpawnPlayerCharacter : IComponentData
    {
        // Player Only
        public byte isRespawning;
        // public byte isLoadingPlayer;
        public byte isGenerateGameData;
        public Text name;

        // Camera Connecting
        public byte isConnectCamera;
        public Entity cameraEntity;
        public Entity controllerEntity;

        // New Player
        public Entity characterMaker;
        public Entity viewerUI;
    }
}