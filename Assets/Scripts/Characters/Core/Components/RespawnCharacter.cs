using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Characters
{
    public struct RespawnCharacter : IComponentData
    {
        public Entity deadCharacter;

        public RespawnCharacter(Entity deadCharacter)
        {
            this.deadCharacter = deadCharacter;
        }
    }
}