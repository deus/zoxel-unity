using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Characters
{
    //! Spawn a new character.
    public struct SpawnCharacter : IComponentData
    {
        public int spawnID;
        public byte prefabType;  // prefab type
        // where to spawn
        public float3 position;
        public quaternion rotation;
        public byte quadrant;
        public Entity chunk;
        public Entity planet;
        public Entity realm;
        // body
        public int voxDataID;

        // Summoned
        public Entity creator;
        // public byte isSummonedOne;
    }
}

        // NPCs
        //public int characterChunkIndex;  // for chunk?