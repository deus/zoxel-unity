using Unity.Entities;

namespace Zoxel.Characters
{
    public struct PlaceCharacterInWorld : IComponentData
    {
        public byte state;
        public int3 chunkPosition;
        public Entity chunk;
        public int targetTown;
        public int3 targetMegaChunk;
    }
}