using Unity.Entities;

namespace Zoxel.Characters
{
    public static class CharacterPrefabType
    {
        public const byte playerNew = 0;
        public const byte playerLoading = 1;
        public const byte playerRespawn = 2;
        public const byte blobNew = 3;
        public const byte blobLoading = 4;
        public const byte blobAuthoredNew = 5;
        public const byte blobAuthoredLoading = 6;
        public const byte npcSkeletonNew = 7;
        public const byte npcSkeletonLoading = 8;
    }
    ///! The type of prefab used to SpawnCharacter.
    public struct PrefabType : IComponentData
    {
        public byte type;

        public PrefabType(byte type)
        {
            this.type = type;
        }
    }
}