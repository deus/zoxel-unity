using Unity.Entities;

namespace Zoxel.Characters
{
    public struct AttachToCharacter : IComponentData
    {
        public Entity controller;
        public Entity camera;
        public Entity character;

        public AttachToCharacter(Entity controller, Entity camera, Entity character)
        {
            this.controller = controller;
            this.camera = camera;
            this.character = character;
        }
    }
}