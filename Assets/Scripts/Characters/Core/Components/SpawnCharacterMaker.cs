using Unity.Entities;

namespace Zoxel.Characters
{
    public struct SpawnCharacterMaker : IComponentData
    { 
        public Entity player;

        public SpawnCharacterMaker(Entity player)
        {
            this.player = player;
        }
    }
}