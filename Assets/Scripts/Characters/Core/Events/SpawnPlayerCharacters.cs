using Unity.Entities;

namespace Zoxel.Characters
{
    //! An event for spawning Player Characters.
    public struct SpawnPlayerCharacters : IComponentData
    {
        public Entity player;

        public SpawnPlayerCharacters(Entity player)
        {
            this.player = player;
        }
    }
}