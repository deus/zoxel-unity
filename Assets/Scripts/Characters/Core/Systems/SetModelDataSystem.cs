using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Zoxel.Voxels;
using Zoxel.Movement;

namespace Zoxel.Characters
{
    //! Sets a charactrs model based on an authored model (Mr Penguin).
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class SetModelDataSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (VoxelManager.instance == null) return;
            var minionModels = VoxelManager.instance.minionModels;
            var minionModels2 = new NativeParallelHashMap<int, VoxDataEditor>(minionModels.Count, Allocator.TempJob);
            for (int i = 0; i < minionModels.Count; i++)
            {
                minionModels2[minionModels[i].data.id] = minionModels[i].data;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity, DeadEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref Body body, in VoxScale voxScale, in SetModelData setModelData) =>
            {
                PostUpdateCommands.RemoveComponent<SetModelData>(entityInQueryIndex, e);
                var voxID = setModelData.voxID;
                // UnityEngine.Debug.LogError("Attempting to Set model: " + voxID);
                if (minionModels2.IsCreated)
                {
                    VoxDataEditor voxData;
                    if (minionModels2.TryGetValue(voxID, out voxData))
                    {
                        // UnityEngine.Debug.LogError("Setting model: " + voxID);
                        PostUpdateCommands.AddComponent<SpawnVoxChunk>(entityInQueryIndex, e);
                        body.size = voxData.GetSize(voxScale.scale);    // set this when building chunk! When 'voxUpdated'
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e, new Chunk(voxData.size, voxData.data.Clone()));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e, new ChunkDimensions(voxData.size));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e, new VoxColors(in voxData.colors));
                    }
                }
                /*for (int i = 0; i < minionModels.Count; i++)
                {
                    var voxData = minionModels[i].data;
                    if (voxData.id == voxID)
                    {
                        // UnityEngine.Debug.LogError("Mr Penguin Vox Found: " + i + " vox: " + voxData.size);
                        break;
                    }
                }*/
                // UnityEngine.Debug.LogError("Vox Not Found: " + voxID);
			})  .WithReadOnly(minionModels2)
                .WithDisposeOnCompletion(minionModels2)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}