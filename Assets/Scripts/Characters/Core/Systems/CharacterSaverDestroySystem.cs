using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Characters
{
    //! Disposes of EntitySaver.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class CharacterSaverDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
            Entities
                .WithAll<DestroyEntity, EntitySaver>()
                .ForEach((in EntitySaver entitySaver) =>
            {
                entitySaver.Dispose();
            }).ScheduleParallel();
        }
    }
}