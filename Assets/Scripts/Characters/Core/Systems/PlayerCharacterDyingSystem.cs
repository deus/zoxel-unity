using Unity.Entities;
using Unity.Burst;
using Zoxel.Movement;

namespace Zoxel.Characters
{
    //! Handles when a PlayerCharacter dies using the DyingEntity event.
    /**
    *   \todo Add post processing overlay - darkness - grayscale
    */
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
	public partial class PlayerCharacterDyingSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var respawnPrefab = CharacterRespawnSystem.respawnPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DyingEntity, PlayerCharacter>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.AddComponent<RevivingEntity>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<DisableForce>(entityInQueryIndex, e);
                PostUpdateCommands.RemoveComponent<DestroyEntityInTime>(entityInQueryIndex, e);
                var respawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, respawnPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, respawnEntity, new DelayEvent(elapsedTime, 12));
                PostUpdateCommands.SetComponent(entityInQueryIndex, respawnEntity, new RespawnCharacter(e));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}