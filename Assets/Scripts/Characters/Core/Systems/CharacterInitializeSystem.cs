using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.AI;

namespace Zoxel.Characters
{
    //! Initializes some basic data in characters, should be done in prefab instead.
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class AIInitializeSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            var seekData = new SeekData
            {
                seekRange = 4,
                attackRange = 8,
                maintainRange = 12,
                seekCooldown = 4
            };
            Dependency = Entities
                .WithAll<InitializeEntity, Character, Targeter>()
                .ForEach((ref Targeter targeter) =>
            {
                targeter.data = seekData;
            }).ScheduleParallel(Dependency);
            Dependency = Entities
                .WithAll<InitializeEntity, NearbyEntities>()
                .ForEach((ref NearbyEntities nearbyEntities) =>
            {
                nearbyEntities.Initialize();
            }).ScheduleParallel(Dependency);
        }
    }
}