using Unity.Entities;
using Unity.Burst;
using Zoxel.Players;    // ControllerLink, SetControllerMapping
using Zoxel.Skeletons;  // SetCameraBone
using Zoxel.Input;
using Zoxel.Cameras;    // CameraLink
using Zoxel.Cameras.PostProcessing; // UpdatePostProcessing

namespace Zoxel.Characters
{
    //! Used by CharacterMaker to start playing as character.
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
	public partial class CharacterControllerAttachSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DelayEvent>()
                .ForEach((Entity e, int entityInQueryIndex, in AttachToCharacter attachToCharacter) =>
            {
                // = Camera =
                PostUpdateCommands.AddComponent(entityInQueryIndex, attachToCharacter.character, new CameraLink(attachToCharacter.camera));
                PostUpdateCommands.AddComponent(entityInQueryIndex, attachToCharacter.camera, new UpdatePostProcessing(PostProcessingProfileType.Game));
                PostUpdateCommands.AddComponent<SetCameraBone>(entityInQueryIndex, attachToCharacter.character);
                // = Controller =
                // \todo Redo controller things as player things
                PostUpdateCommands.AddComponent(entityInQueryIndex, attachToCharacter.controller, new CharacterLink(attachToCharacter.character));
                PostUpdateCommands.AddComponent(entityInQueryIndex, attachToCharacter.character, new ControllerLink(attachToCharacter.controller));
                PostUpdateCommands.AddComponent(entityInQueryIndex, attachToCharacter.controller, new SetControllerMapping(ControllerMapping.InGame));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}