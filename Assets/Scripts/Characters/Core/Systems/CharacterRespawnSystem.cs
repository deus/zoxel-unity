using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Burst;
using Zoxel.Voxels;
using Zoxel.AI;
using Zoxel.UI;
using Zoxel.Animations;
using Zoxel.Cameras;
using Zoxel.Audio;
using Zoxel.Quests;
using Zoxel.Actions;
using Zoxel.Stats;
using Zoxel.Skeletons;
using Zoxel.Items;
using Zoxel.Items.Authoring;
using Zoxel.Bullets;
//      do this by adding a DelayDestroy Component on the DestroyEntity
//      then use data that's still there on it, none of the systems should process character stuff due to WithNone<DestroyEntity>()

namespace Zoxel.Characters
{
    //! Respawns characters after death.
    /**
    *   Uses the DyingEvent from Stats to do things after death.
    *   \todo Add some delay to player deaths.
    */
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class CharacterRespawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity respawnPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var respawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(RespawnCharacter),
                typeof(DelayEvent));
            respawnPrefab = EntityManager.CreateEntity(respawnArchetype);
            RequireForUpdate<ItemsSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var itemsSettings = GetSingleton<ItemsSettings>();
            var itemPickupRadius = itemsSettings.itemPickupRadius;
            var elapsedTime = World.Time.ElapsedTime;
            var spawnCharacterUIPrefab = UICoreSystem.spawnCharacterUIPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DelayEvent>()
                .ForEach((Entity e, int entityInQueryIndex, in RespawnCharacter respawnCharacter) =>
            {
                // spawn new character
                var deadCharacter = respawnCharacter.deadCharacter;
                if (HasComponent<UserStatLinks>(deadCharacter))
                {
                    // now reposition it
                    // fix it's vox?
                    // readd colliders?
                    // remove deadEntity
                    PostUpdateCommands.RemoveComponent<RevivingEntity>(entityInQueryIndex, deadCharacter);
                    PostUpdateCommands.RemoveComponent<DeadEntity>(entityInQueryIndex, deadCharacter);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, deadCharacter, new ItemHitTaker(itemPickupRadius));
                    PostUpdateCommands.AddComponent<BulletHitTaker>(entityInQueryIndex, deadCharacter);
                    // restore position and userStatLinks
                    PostUpdateCommands.AddComponent<RestoreStateStats>(entityInQueryIndex, deadCharacter);
                    PostUpdateCommands.AddComponent<PlaceCharacterInWorld>(entityInQueryIndex, deadCharacter);
                    PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, deadCharacter);
                    // respawn uis, re create vox mesh
                    PostUpdateCommands.AddComponent<SkeletonDirty>(entityInQueryIndex, deadCharacter);
                    PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, deadCharacter);
                    // has not worked
                    PostUpdateCommands.AddComponent<SelectedActionUpdated>(entityInQueryIndex, deadCharacter);
                    PostUpdateCommands.AddComponent<SaveInventory>(entityInQueryIndex, deadCharacter);
                    // uis
                    PostUpdateCommands.AddComponent<SpawnRealmUI>(entityInQueryIndex, deadCharacter);
                }
                // end event
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}