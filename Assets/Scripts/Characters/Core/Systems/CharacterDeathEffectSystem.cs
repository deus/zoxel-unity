﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Animations;
using Zoxel.Particles;

namespace Zoxel.Characters
{
    //! Handles when a character dies using the DyingEntity event.
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
	public partial class CharacterDeathEffectSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var deathEffectTime = 2f;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DyingEntity, Character>()
                .ForEach((Entity e, int entityInQueryIndex, in DyingEntity dyingOne, in NonUniformScale scale, in BodyForce bodyForce) =>
            { 
                if (dyingOne.deathType == 0)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ParticleExplosion
                    {
                        createdTime = elapsedTime,
                        lifeTime = deathEffectTime,
                        delay = 0.05,  // 0.1
                        aliveTime = 1,
                        hitForce = (bodyForce.velocity) * 1f // + new float3(0, 0.3f, 0)
                    });
                }
                else
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new LerpScale(elapsedTime, deathEffectTime, scale.Value, new float3()));
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}