using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Voxels;
using Zoxel.Worlds;
using Zoxel.Cameras;
using Zoxel.Transforms;
using Zoxel.Characters;
using Zoxel.Players;
using Zoxel.Rendering;
using Zoxel.Weather;    // setting SetSkyAsCamera
using Zoxel.Input;

namespace Zoxel.Characters
{
    //! Loads a player Character and the World that it will spawn in.
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class PlayerCharacterLoadSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;
        private EntityQuery controllersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.ReadOnly<WorldLinks>());
            controllersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Controller>(),
                ComponentType.ReadOnly<CameraLink>());
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var loadCharacterEventPrefab = CharacterLoadSystem.loadCharacterEventPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var spawnPlanetPrefab = WorldSystemGroup.spawnPlanetPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var realmIDs = GetComponentLookup<ZoxID>(true);
            var worldLinks = GetComponentLookup<WorldLinks>(true);
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var cameraLinks = GetComponentLookup<CameraLink>(true);
            realmEntities.Dispose();
            controllerEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .ForEach((ref LoadPlayerCharacter loadPlayerCharacter) =>
            {
                if (loadPlayerCharacter.state == 0)
                {
                    var realmID = realmIDs[loadPlayerCharacter.realm].id;
                    var realmID2 = SaveUtilities.LoadWorldID(realmID);
                    if (realmID2 != "")
                    {
                        loadPlayerCharacter.worldID = int.Parse(realmID2);
                    }
                }
            })  .WithReadOnly(realmIDs)
                .WithoutBurst().Run();
            Dependency = Entities
                .WithNone<DelayEvent>()
                .ForEach((Entity e, int entityInQueryIndex, ref LoadPlayerCharacter loadPlayerCharacter) =>
            {
                if (loadPlayerCharacter.state == 0)
                {
                    // load world
                    loadPlayerCharacter.state = 1;
                    var spawnPlanetEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnPlanetPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnPlanetEntity, new SpawnPlanet(loadPlayerCharacter.realm, loadPlayerCharacter.player, loadPlayerCharacter.worldID));
                }
                else
                {
                    var worlds = worldLinks[loadPlayerCharacter.realm].worlds;
                    //! Waits for world to load
                    if (worlds.Length == 0)
                    {
                        return;
                    }
                    var worldEntity = worlds[0];
                    if (HasComponent<EntityBusy>(worldEntity))
                    {
                        return;
                    }
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                    // clear menu, open player menu, Disable UI now!
                    var cameraEntity = cameraLinks[loadPlayerCharacter.player].camera;
                    PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new VoxLink(worldEntity));
                    PostUpdateCommands.AddComponent(entityInQueryIndex, cameraEntity, new SetCameraAsSky(elapsedTime, 0f));
                    var loadEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, loadCharacterEventPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, loadEntity, new LoadCharacter
                    {
                        characterID = loadPlayerCharacter.characterID,
                        realm = loadPlayerCharacter.realm,
                        player = loadPlayerCharacter.player
                    });
                
                }
            })  .WithReadOnly(worldLinks)
                .WithReadOnly(cameraLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
