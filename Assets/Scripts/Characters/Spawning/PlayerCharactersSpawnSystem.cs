using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using Zoxel.Players;
using Zoxel.Cameras;
using Zoxel.Cameras.Spawning;
using Zoxel.UI;

namespace Zoxel.Characters
{
    //! Spawns local coop player characters (the non saveable kind) when playyer one enters a character.
    /**
    *   - Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class PlayerCharactersSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var fadeinScreenTime = UIManager.instance.uiSettings.fadeinScreenTime; // 1f;
            var fadeScreenWaitTime = UIManager.instance.uiSettings.fadeinScreenWaitTime; // 1f;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<SpawnPlayerCharacters>()
                .ForEach((Entity e, in PlayerLinks playerLinks, in SpawnPlayerCharacters spawnPlayerCharacters, in RealmLink realmLink) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnPlayerCharacters>(e);
                var worldEntity = new Entity(); // EntityManager.GetComponentData<WorldLinks>(realmEntity).worlds[0];
                for (int i = 0; i < playerLinks.players.Length; i++)
                {
                    var playerEntity2 = playerLinks.players[i];
                    if (playerEntity2 != spawnPlayerCharacters.player)
                    {
                        // camera
                        var spawnFirstPersonCamera = new SpawnFirstPersonCamera(playerEntity2, new float3());
                        var newCameraEntity = FirstPersonCameraSpawnSystem.SpawnFirstPersonCamera(PostUpdateCommands, in spawnFirstPersonCamera);
                        var beforeCameraEntity = EntityManager.GetComponentData<CameraLink>(playerEntity2).camera;
                        // PostUpdateCommands.AddComponent(screenFaderLink.screenFader, new TransitionCameraLink(newCameraEntity, elapsedTime, 2.1f));
                        // PostUpdateCommands.AddComponent(playerEntity2, new TransitionCameraLink(newCameraEntity, elapsedTime, 2.1f));
                        var transitionEvent = PostUpdateCommands.CreateEntity();
                        PostUpdateCommands.AddComponent(transitionEvent, new TransitionCamera(beforeCameraEntity, newCameraEntity));
                        PostUpdateCommands.AddComponent(transitionEvent, new DelayEvent(elapsedTime, fadeinScreenTime));
                        PostUpdateCommands.AddComponent<GenericEvent>(transitionEvent);
                        // character
                        var characterID = IDUtil.GenerateUniqueID();
                        var random = new Random();
                        random.InitState((uint) characterID);
                        var characterName = NameGenerator.GenerateName2(ref random);
                        var spawnEntity = PostUpdateCommands.Instantiate(CharactersSystemGroup.spawnPlayerCharacterPrefab);
                        var spawnCharacter = new SpawnCharacter
                        {
                            quadrant = PlanetSide.Up,
                            prefabType = CharacterPrefabType.playerNew,
                            spawnID = characterID,
                            realm = realmLink.realm,
                            planet = worldEntity,
                            position = new float3(0, 120f, 0), // characterPosition,
                            rotation = quaternion.identity
                        };
                        var spawnPlayerCharacter = new SpawnPlayerCharacter
                        {
                            isConnectCamera = 1,
                            controllerEntity = playerEntity2,
                            cameraEntity = newCameraEntity,
                            name = characterName
                        };
                        PostUpdateCommands.SetComponent(spawnEntity, spawnCharacter);
                        PostUpdateCommands.SetComponent(spawnEntity, spawnPlayerCharacter);
                        PostUpdateCommands.AddComponent(spawnEntity, new DelayEvent(elapsedTime, fadeinScreenTime));
                    }
                }
            }).WithoutBurst().Run();
        }
    }
}