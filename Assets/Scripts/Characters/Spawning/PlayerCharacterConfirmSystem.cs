using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Voxels;
using Zoxel.Worlds;
using Zoxel.Cameras;
using Zoxel.Transforms;
using Zoxel.Characters;
using Zoxel.Players;
using Zoxel.Rendering;
using Zoxel.Weather;    // setting SetSkyAsCamera
using Zoxel.Input;
using Zoxel.Cameras.PostProcessing;

namespace Zoxel.Characters
{
    //! Loads a player Characcter.
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class PlayerCharacterConfirmSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;
        private EntityQuery controllersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.ReadOnly<WorldLinks>());
            controllersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Controller>(),
                ComponentType.ReadOnly<CameraLink>());
            RequireForUpdate(processQuery);
            RequireForUpdate(realmQuery);
            RequireForUpdate(controllersQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var spawnPlanetPrefab = WorldSystemGroup.spawnPlanetPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var worldLinks = GetComponentLookup<WorldLinks>(true);
            realmEntities.Dispose();
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var cameraLinks = GetComponentLookup<CameraLink>(true);
            var realmLinks = GetComponentLookup<RealmLink>(true);
            controllerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .ForEach((Entity e, int entityInQueryIndex, ref ConfirmPlayerCharacter confirmPlayerCharacter) =>
            {
                var realmEntity = realmLinks[confirmPlayerCharacter.player].realm;
                if (confirmPlayerCharacter.state == 0)
                {
                    // load world
                    confirmPlayerCharacter.state = 1;
                    var spawnPlanetEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnPlanetPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnPlanetEntity, new SpawnPlanet(realmEntity, confirmPlayerCharacter.player, 0));
                    PostUpdateCommands.AddComponent<SpawnNewPlanet>(entityInQueryIndex, spawnPlanetEntity);
                }
                else
                {
                    var worlds = worldLinks[realmEntity].worlds;
                    //! Waits for world to load
                    if (worlds.Length == 0)
                    {
                        return;
                    }
                    var worldEntity = worlds[0];
                    if (HasComponent<EntityBusy>(worldEntity))
                    {
                        return;
                    }
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                    // set character things
                    var characterEntity = confirmPlayerCharacter.character;
                    PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, characterEntity);
                    PostUpdateCommands.AddComponent<SpawnRealmUI>(entityInQueryIndex, characterEntity);
                    // PostUpdateCommands.RemoveComponent<ViewerObject>(entityInQueryIndex, characterEntity);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, characterEntity, new VoxLink(worldEntity));
                    PostUpdateCommands.AddComponent<PlaceCharacterInWorld>(entityInQueryIndex, characterEntity);
                    var cameraEntity = cameraLinks[confirmPlayerCharacter.player].camera;
                    PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new VoxLink(worldEntity));
                    PostUpdateCommands.AddComponent(entityInQueryIndex, cameraEntity, new SetCameraAsSky(elapsedTime, 0f));
                    // connect event
                    var connectEvent = PostUpdateCommands.CreateEntity(entityInQueryIndex);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, connectEvent, new AttachToCharacter(confirmPlayerCharacter.player, cameraEntity, characterEntity));
                    PostUpdateCommands.AddComponent(entityInQueryIndex, connectEvent, new DelayEvent(elapsedTime, 0f));
                    PostUpdateCommands.AddComponent<GenericEvent>(entityInQueryIndex, connectEvent);
                    // save too!
                    PostUpdateCommands.AddComponent<SaveNewCharacter>(entityInQueryIndex, characterEntity);
                    PostUpdateCommands.RemoveComponent<DisableSaving>(entityInQueryIndex, characterEntity);
                    PostUpdateCommands.RemoveComponent<DisableRaycaster>(entityInQueryIndex, characterEntity);
                }
            })  .WithReadOnly(worldLinks)
                .WithReadOnly(cameraLinks)
                .WithReadOnly(realmLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}