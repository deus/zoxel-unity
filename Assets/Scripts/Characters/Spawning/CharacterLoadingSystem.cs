using Unity.Entities;
using Unity.Burst;
using Zoxel.AI;
using Zoxel.Quests;
using Zoxel.Movement;
using Zoxel.Dialogue;
using Zoxel.Items;
using Zoxel.Stats;
using Zoxel.Actions;            // Has UserActionLinks
using Zoxel.Skills;             // Has UserSkillLinks
using Zoxel.Skeletons;          // Has BoneLinks
using Zoxel.Clans;              // Has Clan Link
using Zoxel.Classes;            // Has Class Link
using Zoxel.Races;              // Has RaceLink
using Zoxel.Voxels;             // Has Vox
using Zoxel.Transforms.IO;

namespace Zoxel.Characters
{ 
    //! Removes SelectedActionUpdated event after use.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class CharacterLoadingSystem : SystemBase
    {
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            processQuery = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<LoadTransform>(),
                ComponentType.Exclude<LoadStats>(),
                ComponentType.Exclude<LoadInventory>(),
                ComponentType.Exclude<LoadName>(),
                ComponentType.Exclude<LoadRace>(),
                ComponentType.Exclude<LoadSkills>(),
                ComponentType.Exclude<LoadActions>(),
                ComponentType.Exclude<LoadSpawnPoint>(),
                ComponentType.Exclude<LoadUniqueItems>(),
                ComponentType.Exclude<LoadEquipment>(),
                ComponentType.Exclude<LoadQuestlog>(),
                ComponentType.Exclude<LoadClass>(),
                ComponentType.ReadOnly<EntityLoading>(),
                ComponentType.ReadOnly<Character>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<EntityLoading>(processQuery);
        }
    }
}