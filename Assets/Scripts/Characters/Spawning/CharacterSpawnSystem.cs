using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Rendering.Authoring;
using Zoxel.AI;
using Zoxel.UI;
using Zoxel.Animations;
using Zoxel.Cameras;
using Zoxel.Audio;
using Zoxel.Quests;
using Zoxel.Movement;
using Zoxel.Dialogue;
using Zoxel.Items;
using Zoxel.Items.Authoring;
using Zoxel.Stats;
using Zoxel.Actions;            // Has UserActionLinks
using Zoxel.Skills;             // Has UserSkillLinks
using Zoxel.Skeletons;          // Has BoneLinks
using Zoxel.Clans;              // Has Clan Link
using Zoxel.Classes;            // Has Class Link
using Zoxel.Races;              // Has RaceLink
using Zoxel.Voxels;             // Has Vox
using Zoxel.Voxels.Lighting;    // Has Character lighting
using Zoxel.Voxels.Authoring;
using Zoxel.Bullets;            // Has Bullet Hit Taker
using Zoxel.Characters.World;
using Zoxel.Characters.Authoring;
using Zoxel.Players;    // Players only
using Zoxel.Characters.UI;
using Zoxel.Portals;
using Zoxel.Input;
using Zoxel.Transforms.IO;
using Zoxel.VoxelInteraction;
using Zoxel.Logs;

namespace Zoxel.Characters
{
    //! Spawns Characters!
    /**
    *   - Spawn System -
    *   Spawns both player and npc characters.
    *   Best way to spawn lots of npcs? Can we do a batch spawning. Then for each character, initialize data in the next lot.
    *   \todo Spawn PlayerCharacter separately and reuse functions between systems.
    *
    *   \todo Clean up prefab useage a bit.
    *   \todo Initialize some stuff in a second system to remove some logic out of here.
    *   \todo Generic character IDs within jobs
    *   \todo Support for authored character content?
    */
    [BurstCompile, UpdateInGroup(typeof(CharactersSystemGroup))]
    public partial class CharacterSpawnSystem : SystemBase
    {
        private bool hasSetVoxScale;
        private Entity chunkCharacterSpawnedPrefab;
        private EntityQuery processQuery;
        private EntityQuery planetQuery;
        private EntityQuery realmQuery;
        private CharacterPrefabs npcPrefab;
        private CharacterPrefabs npcSkeletonPrefab;
        private Entity npcAuthoredPrefab;
        private CharacterPrefabs playerPrefab;
        private Entity respawnPlayerPrefab;

        public struct CharacterPrefabs
        {
            public Entity newPrefab;
            public Entity loadPrefab;
        }

        protected override void OnCreate()
        {
            planetQuery = GetEntityQuery(
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<RealmLink>(),
                ComponentType.ReadOnly<ZoxID>());
            realmQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<RaceLinks>());
            var chunkCharacterSpawnedArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(ChunkCharacterSpawned),
                typeof(GenericEvent));
            chunkCharacterSpawnedPrefab = EntityManager.CreateEntity(chunkCharacterSpawnedArchetype);
            SpawnPrefabs();
            RequireForUpdate<ModelSettings>();
            RequireForUpdate<RenderSettings>();
            RequireForUpdate(processQuery);
        }

        //! Spawn character prefabs.
        private void SpawnPrefabs()
        {
            var archtype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(DisableForce),
                // typeof(InitializeSaver),
                typeof(ZoxID),
                typeof(Seed),
                typeof(PrefabType),
                typeof(ZoxName),
                typeof(Character),
                typeof(ChunkLightingLink),
                typeof(EntityLighting),
                typeof(RealmLink),
                typeof(UILink),
                typeof(RaceLink),
                typeof(ClassLink),
                typeof(ClanLink),
                typeof(VoxLink),
                typeof(DynamicChunkLink),
                typeof(ChunkLink),
                typeof(NearbyEntities),
                typeof(UserActionLinks),
                typeof(Inventory),
                typeof(Equipment),
                typeof(UserStatLinks),
                typeof(UserSkillLinks),
                typeof(Questlog),
                typeof(VoxelCollider),
                typeof(IsOnGround),
                typeof(Body),
                typeof(BodyInnerForce),
                typeof(BodyForce),
                typeof(BodyTorque),
                typeof(BulletHitTaker),
                typeof(Target),
                typeof(Targeter),
                typeof(EntityVoxelPosition),
                typeof(GravityQuadrant),
                typeof(GravityForce),
                typeof(Animator),
                typeof(Vox),
                typeof(ChunkDimensions),
                typeof(VoxColors),
                typeof(VoxScale),
                typeof(EntityMaterials),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld)
            );
            // New NPC
            npcPrefab.newPrefab = EntityManager.CreateEntity(archtype);
            SetTransformComponents(npcPrefab.newPrefab);
            AddNPCComponents(npcPrefab.newPrefab);
            AddAIComponents(npcPrefab.newPrefab);
            AddGenerateComponents(npcPrefab.newPrefab);
            //EntityManager.AddComponent<BeginGenerateVox>(npcPrefab.newPrefab);
            EntityManager.AddComponent<GenerateVox>(npcPrefab.newPrefab);
            EntityManager.AddComponent<SpawnVoxChunk>(npcPrefab.newPrefab);
            EntityManager.AddComponentData(npcPrefab.newPrefab, new GenerateVoxData(GenerateVoxType.Head));
            AddSaveComponents(npcPrefab.newPrefab, false);
            EntityManager.AddComponent<SaveNewCharacter>(npcPrefab.newPrefab);
            //EntityManager.AddComponent<ChunkCharacterSpawnedLink>(npcPrefab.newPrefab);
            // Load NPC
            npcPrefab.loadPrefab = EntityManager.CreateEntity(archtype);
            SetTransformComponents(npcPrefab.loadPrefab);
            AddNPCComponents(npcPrefab.loadPrefab);
            AddAIComponents(npcPrefab.loadPrefab);
            AddLoadComponents(npcPrefab.loadPrefab, false, false);
            // EntityManager.AddComponent<BeginGenerateVox>(npcPrefab.loadPrefab);
            EntityManager.AddComponent<GenerateVox>(npcPrefab.loadPrefab);
            EntityManager.AddComponent<SpawnVoxChunk>(npcPrefab.loadPrefab);
            EntityManager.AddComponentData(npcPrefab.loadPrefab, new GenerateVoxData(GenerateVoxType.Head));
            AddSaveComponents(npcPrefab.loadPrefab, false);
            //EntityManager.AddComponent<ChunkCharacterSpawnedLink>(npcPrefab.loadPrefab);
            // New BoneLinks NPC
            npcSkeletonPrefab.newPrefab = EntityManager.CreateEntity(archtype);
            SetTransformComponents(npcSkeletonPrefab.newPrefab);
            AddNPCComponents(npcSkeletonPrefab.newPrefab);
            AddAIComponents(npcSkeletonPrefab.newPrefab);
            AddGenerateComponents(npcSkeletonPrefab.newPrefab);
            AddSaveComponents(npcSkeletonPrefab.newPrefab, false);
            EntityManager.AddComponent<SaveNewCharacter>(npcSkeletonPrefab.newPrefab);
            AddSkeletonComponents(npcSkeletonPrefab.newPrefab);
            AddNPCDialogueComponents(npcSkeletonPrefab.newPrefab, true);
            //EntityManager.AddComponent<ChunkCharacterSpawnedLink>(npcSkeletonPrefab.newPrefab);
            EntityManager.AddComponent<GenerateBody>(npcSkeletonPrefab.newPrefab);
            EntityManager.AddComponent<EntityBusy>(npcSkeletonPrefab.newPrefab);
            // Load NPC
            npcSkeletonPrefab.loadPrefab = EntityManager.CreateEntity(archtype);
            SetTransformComponents(npcSkeletonPrefab.loadPrefab);
            AddNPCComponents(npcSkeletonPrefab.loadPrefab);
            AddAIComponents(npcSkeletonPrefab.loadPrefab);
            AddLoadComponents(npcSkeletonPrefab.loadPrefab, true, false);
            AddSaveComponents(npcSkeletonPrefab.loadPrefab, false);
            AddSkeletonComponents(npcSkeletonPrefab.loadPrefab);
            // EntityManager.AddComponent<DialogueTree>(npcSkeletonPrefab.loadPrefab);
            AddNPCDialogueComponents(npcSkeletonPrefab.loadPrefab, false);
            //EntityManager.AddComponent<ChunkCharacterSpawnedLink>(npcSkeletonPrefab.loadPrefab);
            EntityManager.AddComponent<EntityBusy>(npcSkeletonPrefab.loadPrefab);
            // Mr Penguin!
            //! \todo Replace with VoxDataLink to the vox data.
            npcAuthoredPrefab = EntityManager.CreateEntity(archtype);
            SetTransformComponents(npcAuthoredPrefab);
            AddNPCComponents(npcAuthoredPrefab);
            AddAIComponents(npcAuthoredPrefab, false);
            AddGenerateComponents(npcAuthoredPrefab);
            // EntityManager.AddComponent<VoxDataEditor>(npcAuthoredPrefab);       // todo: Pass in through a realm data e2
            EntityManager.AddComponent<SetModelData>(npcAuthoredPrefab);
            EntityManager.AddComponent<Chunk>(npcAuthoredPrefab);
            // New Character
            EntityManager.AddComponent<NewCharacter>(npcPrefab.newPrefab);
            EntityManager.AddComponent<NewCharacter>(npcPrefab.loadPrefab);
            EntityManager.AddComponent<NewCharacter>(npcSkeletonPrefab.newPrefab);
            EntityManager.AddComponent<NewCharacter>(npcSkeletonPrefab.loadPrefab);
            EntityManager.AddComponent<NewCharacter>(npcAuthoredPrefab);
            // New Player
            playerPrefab.newPrefab = EntityManager.CreateEntity(archtype);
            SetTransformComponents(playerPrefab.newPrefab);
            AddSkeletonComponents(playerPrefab.newPrefab);
            AddPlayerComponents(playerPrefab.newPrefab);
            AddRaycastComponents(playerPrefab.newPrefab);
            AddSaveComponents(playerPrefab.newPrefab, true);
            EntityManager.SetComponentData(playerPrefab.newPrefab, new UserActionLinks { selectedAction = 255 });
            // Extras
            // EntityManager.AddComponent<GiveGameSkill>(playerPrefab.newPrefab);
            EntityManager.AddComponent<GenerateStats>(playerPrefab.newPrefab);
            EntityManager.AddComponent<GenerateInventory>(playerPrefab.newPrefab);
            EntityManager.AddComponent<EntityBusy>(playerPrefab.newPrefab);
            EntityManager.AddComponent<GenerateBody>(playerPrefab.newPrefab);
            EntityManager.AddComponent<SetClass>(playerPrefab.newPrefab);
            // Load Player
            playerPrefab.loadPrefab = EntityManager.CreateEntity(archtype);
            SetTransformComponents(playerPrefab.loadPrefab);
            AddSkeletonComponents(playerPrefab.loadPrefab);
            AddPlayerComponents(playerPrefab.loadPrefab);
            AddRaycastComponents(playerPrefab.loadPrefab);
            AddSaveComponents(playerPrefab.loadPrefab, true);
            EntityManager.SetComponentData(playerPrefab.loadPrefab, new UserActionLinks { selectedAction = 255 });
            AddLoadComponents(playerPrefab.loadPrefab, true, true);
            EntityManager.AddComponent<SpawnRealmUI>(playerPrefab.loadPrefab);
            EntityManager.AddComponent<EntityBusy>(playerPrefab.loadPrefab);
        }

        private void AddSaveComponents(Entity prefab)
        {
            
        }

        private void AddNPCDialogueComponents(Entity prefab, bool isNew)
        {
            EntityManager.AddComponent<DialogueTree>(prefab);
            EntityManager.AddComponent<GenerateDialogue>(prefab);
            /*if (isNew)
            {
                EntityManager.AddComponent<GiveRealmQuest>(prefab);
            }*/
        }

        private void AddPlayerComponents(Entity prefab)
        {
            EntityManager.AddComponent<ChunkStreamPoint>(prefab);
            EntityManager.AddComponent<PlayerCharacter>(prefab);
            EntityManager.AddComponent<CameraLink>(prefab);
            EntityManager.AddComponent<RealmUILink>(prefab);
            EntityManager.AddComponent<ItemHitTaker>(prefab);
            EntityManager.AddComponent<Traveler>(prefab);
            EntityManager.AddComponent<DialogueTree>(prefab);
            EntityManager.AddComponent<LogLinks>(prefab);
        }

        private void AddRaycastComponents(Entity prefab)
        {
            EntityManager.AddComponent<RaycasterOrigin>(prefab);
            EntityManager.AddComponent<RaycastVoxel>(prefab);
            EntityManager.AddComponent<RaycastCharacter>(prefab);
            EntityManager.AddComponent<SpawnPreviewMesh>(prefab);
            EntityManager.AddComponent<GizmoLinks>(prefab);
        }

        private void AddNPCComponents(Entity prefab)
        {
            EntityManager.AddComponent<NPCCharacter>(prefab);
            EntityManager.AddComponent<SpawnNameLabel>(prefab);
            // EntityManager.AddComponent<ChunkBounded>(prefab);
            // EntityManager.AddComponent<ChunkBoundLink>(prefab);
        }

        private void AddGenerateComponents(Entity prefab)
        {
            EntityManager.AddComponent<GenerateStats>(prefab);
            EntityManager.AddComponent<GenerateSkills>(prefab);
            EntityManager.AddComponent<GenerateInventory>(prefab);
        }

        private void AddAIComponents(Entity prefab, bool hasSpawnPoint = true)
        {
            EntityManager.AddComponentData(prefab, new Brain(AIStateType.Wander));
            EntityManager.AddComponent<Wander>(prefab);
            // if (hasSpawnPoint)
            {
                EntityManager.AddComponent<SpawnPoint>(prefab);
            }
        }

        private void SetTransformComponents(Entity prefab)
        {
            EntityManager.SetComponentData(prefab, new Rotation { Value = quaternion.identity });
            EntityManager.SetComponentData(prefab, new NonUniformScale { Value = new float3(1, 1, 1) });
        }

        private void AddSkeletonComponents(Entity prefab)
        {
            EntityManager.AddComponent<Skeleton>(prefab);
            EntityManager.AddComponent<BoneLinks>(prefab);
            EntityManager.AddComponent<ItemLinks>(prefab);
            EntityManager.AddComponent<HeldWeaponLinks>(prefab);
            EntityManager.AddComponent<HeldActionItemLink>(prefab);
        }

        private void AddLoadComponents(Entity prefab, bool isSkeleton, bool isPlayer)
        {
            EntityManager.AddComponent<InitializeSaver>(prefab);
            EntityManager.AddComponent<EntityLoading>(prefab);
            EntityManager.AddComponent<LoadTransform>(prefab);
            EntityManager.AddComponent<LoadStats>(prefab);
            EntityManager.AddComponent<LoadInventory>(prefab);
            EntityManager.AddComponent<LoadName>(prefab);
            EntityManager.AddComponent<LoadRace>(prefab);
            EntityManager.AddComponent<LoadSkills>(prefab);
            EntityManager.AddComponent<LoadActions>(prefab);
            if (!isPlayer)
            {
                EntityManager.AddComponent<LoadSpawnPoint>(prefab);
            }
            if (!isSkeleton)
            {
                return;
            }
            EntityManager.AddComponent<LoadUniqueItems>(prefab);
            EntityManager.AddComponent<LoadEquipment>(prefab);
            EntityManager.AddComponent<LoadQuestlog>(prefab);
            EntityManager.AddComponent<LoadClass>(prefab);
        }

        private void AddSaveComponents(Entity prefab, bool isPlayer)
        {
            EntityManager.AddComponent<InitializeSaver>(prefab);
            EntityManager.AddComponent<Saver>(prefab);
            EntityManager.AddComponent<EntitySaver>(prefab);
            /// EntityManager.AddComponent<InitializeTransformSaver>(prefab);
            // EntityManager.AddComponent<TransformSaver>(prefab);
            EntityManager.AddComponent<TransformSaverData>(prefab);
            // EntityManager.AddComponent<InitializeStatsSaver>(prefab);
            // EntityManager.AddComponent<StatsSaver>(prefab);
            EntityManager.AddComponent<StatsSaverData>(prefab);
            EntityManager.AddComponent<StatsSaverTimer>(prefab);
            if (!isPlayer)
            {
                EntityManager.AddComponent<SaveSpawnPoint>(prefab);
            }
        }

        private void InitializePrefabSettings()
        {
            if (hasSetVoxScale)
            {
                return;
            }
            hasSetVoxScale = true;
            var characterSettings = GetSingleton<CharacterSettings>();
            var modelSettings = GetSingleton<ModelSettings>();
            var renderSettings = GetSingleton<RenderSettings>();
            var voxelScale = modelSettings.GetVoxelScale();
            EntityManager.SetComponentData(npcPrefab.loadPrefab, new VoxScale(voxelScale)); //  * 2f
            EntityManager.SetComponentData(npcPrefab.newPrefab, new VoxScale(voxelScale));
            EntityManager.SetComponentData(npcSkeletonPrefab.newPrefab, new VoxScale(voxelScale));
            EntityManager.SetComponentData(npcSkeletonPrefab.loadPrefab, new VoxScale(voxelScale));
            EntityManager.SetComponentData(npcAuthoredPrefab, new VoxScale(voxelScale));
            var renderDistance = (byte) renderSettings.renderDistance;
            EntityManager.SetComponentData(playerPrefab.newPrefab, new ChunkStreamPoint(renderDistance));
            EntityManager.SetComponentData(playerPrefab.newPrefab, new VoxScale(voxelScale));
            EntityManager.SetComponentData(playerPrefab.loadPrefab, new ChunkStreamPoint(renderDistance));
            EntityManager.SetComponentData(playerPrefab.loadPrefab, new VoxScale(voxelScale));
            // EntityManager.SetComponentData(respawnPlayerPrefab, new ChunkStreamPoint(renderDistance));
            // EntityManager.SetComponentData(respawnPlayerPrefab, new VoxScale(voxelScale));
            var disableNPCSaving = characterSettings.disableNPCSaving;
            if (disableNPCSaving)
            {
                RemoveSaverComponents(npcPrefab.newPrefab);
                RemoveSaverComponents(npcPrefab.loadPrefab);
                RemoveSaverComponents(npcSkeletonPrefab.newPrefab);
                RemoveSaverComponents(npcSkeletonPrefab.loadPrefab);
            }
        }

        private void RemoveSaverComponents(Entity prefab)
        {
            EntityManager.RemoveComponent<Saver>(prefab);
            EntityManager.RemoveComponent<EntitySaver>(prefab);
            EntityManager.RemoveComponent<TransformSaverData>(prefab);
            EntityManager.RemoveComponent<StatsSaverData>(prefab);
            EntityManager.RemoveComponent<StatsSaverTimer>(prefab);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabSettings();
            var itemsSettings = GetSingleton<ItemsSettings>();
            var modelSettings = GetSingleton<ModelSettings>();
            var characterSettings = GetSingleton<CharacterSettings>();
            var fadeinScreenTime = UIManager.instance.uiSettings.fadeinScreenTime; // 1f;
            var fadeinScreenWaitTime = UIManager.instance.uiSettings.fadeinScreenWaitTime; // 1f;
            var disableCharacterLighting = VoxelManager.instance.voxelSettings.disableCharacterLighting;
            var disableNPCSaving = characterSettings.disableNPCSaving;
            var spawnOnlyTraders = characterSettings.spawnOnlyTraders;
            var chaosMode = characterSettings.chaosMode;
            var maxCharacters = characterSettings.maxCharacters;
            var charactersCount = CharactersSystemGroup.charactersCount;
            var itemPickupRadius = itemsSettings.itemPickupRadius;
            var voxelScale = modelSettings.GetVoxelScale();
            var elapsedTime = World.Time.ElapsedTime;
            var chunkCharacterSpawnedPrefab = this.chunkCharacterSpawnedPrefab;
            var npcPrefab = this.npcPrefab;
            var npcSkeletonPrefab = this.npcSkeletonPrefab;
            var npcAuthoredPrefab = this.npcAuthoredPrefab;
            var playerPrefab = this.playerPrefab;
            var respawnPlayerPrefab = this.respawnPlayerPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // todo: GenerateID myself instead of GUID
            Entities
                .WithNone<DelayEvent, SpawnPlayerCharacter, SpawnLoadingCharacter>()
                .ForEach((Entity e, ref SpawnCharacter spawnCharacter) =>
            {
                spawnCharacter.spawnID = IDUtil.GenerateUniqueID();
            }).WithoutBurst().Run();
            // this spawns npc characters
            // planet and games
            var planetEntities = planetQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var planetIDs = GetComponentLookup<ZoxID>(true);
            planetEntities.Dispose();
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var raceLinks = GetComponentLookup<RaceLinks>(true);
            var realmIDs = GetComponentLookup<ZoxID>(true);
            var classLinks = GetComponentLookup<ClassLinks>(true);
            realmEntities.Dispose();
            var spawnCharacterEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var spawnPlayerCharacters = GetComponentLookup<SpawnPlayerCharacter>(true);
            spawnCharacterEntities.Dispose();
            var chunkEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            chunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .ForEach((Entity e, int entityInQueryIndex, in SpawnCharacter spawnCharacter, in ClanLink clanLink) =>
            {
                var isSpawnNPC = !spawnPlayerCharacters.HasComponent(e);
                var hasChunk = chunkPositions.HasComponent(spawnCharacter.chunk) && !HasComponent<DisabledChunkSpawning>(spawnCharacter.chunk) && !HasComponent<DestroyEntity>(spawnCharacter.chunk);
                if (isSpawnNPC && !hasChunk)
                {
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                    return;
                }
                if (isSpawnNPC && charactersCount + 1 >= maxCharacters)
                {
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                    return;
                }
                // Destroy event entity.
                /*if (!planetRealmLinks.HasComponent(spawnCharacter.planet))
                {
                    return;
                }*/
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                var spawnID = spawnCharacter.spawnID;
                if (spawnID == 0)
                {
                    return;
                }
                var isLoadingCharacter = HasComponent<SpawnLoadingCharacter>(e);
                var realmEntity = spawnCharacter.realm;
                var realmRaceLinks = raceLinks[realmEntity];
                var voxDataID = spawnCharacter.voxDataID;
                var prefab = new Entity();
                var prefabType = spawnCharacter.prefabType;
                if (prefabType == CharacterPrefabType.playerNew)
                {
                    prefab = playerPrefab.newPrefab;
                }
                else if (prefabType == CharacterPrefabType.playerLoading)
                {
                    prefab = playerPrefab.loadPrefab;
                }
                else if (prefabType == CharacterPrefabType.playerRespawn)
                {
                    prefab = respawnPlayerPrefab;
                }
                else if (prefabType == CharacterPrefabType.blobNew)
                {
                    prefab = npcPrefab.newPrefab;
                }
                else if (prefabType == CharacterPrefabType.blobLoading)
                {
                    prefab = npcPrefab.loadPrefab;
                }
                else if (prefabType == CharacterPrefabType.blobAuthoredNew)
                {
                    prefab = npcAuthoredPrefab;
                }
                else if (prefabType == CharacterPrefabType.npcSkeletonNew)
                {
                    prefab = npcSkeletonPrefab.newPrefab;
                }
                else if (prefabType == CharacterPrefabType.npcSkeletonLoading)
                {
                    prefab = npcSkeletonPrefab.loadPrefab;
                }
                var isHumanoid = (prefabType == CharacterPrefabType.npcSkeletonNew || prefabType == CharacterPrefabType.npcSkeletonLoading);
                var isSlime = (prefabType == CharacterPrefabType.blobNew || prefabType == CharacterPrefabType.blobLoading);
                var isMrPenguin = (prefabType == CharacterPrefabType.blobAuthoredNew || prefabType == CharacterPrefabType.blobAuthoredLoading);
                // UnityEngine.Debug.LogError("Spawned NPC? " + isSpawnNPC + " prefab Type: " + prefabType);
                var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new PrefabType(prefabType));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ZoxID(spawnID));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Seed(spawnID));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Translation { Value = spawnCharacter.position });
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Rotation { Value = spawnCharacter.rotation });
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new RealmLink(realmEntity));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new VoxLink(spawnCharacter.planet));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, clanLink);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new GravityQuadrant(spawnCharacter.quadrant));
                var chunkPosition = new int3(-6666, -6666, -6666);
                if (chunkPositions.HasComponent(spawnCharacter.chunk))
                {
                    chunkPosition = chunkPositions[spawnCharacter.chunk].position;
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ChunkLink(spawnCharacter.chunk));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new DynamicChunkLink(chunkPosition));
                
                // Renderer
                // PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new MaterialFader(0.3f, 4));
                if (spawnCharacter.creator.Index == 0 && !(isSpawnNPC && disableNPCSaving)) // !isMrPenguin)
                {
                    var realmID = realmIDs[realmEntity].id;
                    var worldID = 0;
                    if (planetIDs.HasComponent(spawnCharacter.planet))
                    {
                        worldID = planetIDs[spawnCharacter.planet].id;
                    }
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new EntitySaver(realmID, worldID));
                }
                var raceEntity = new Entity();
                var characterName = new Text();
                if (isSpawnNPC)
                {
                    var isDisabled = HasComponent<DisableRender>(spawnCharacter.chunk);
                    if (isDisabled)
                    {
                        PostUpdateCommands.AddComponent<DisableRender>(entityInQueryIndex, e2);
                    }
                    // PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ChunkBoundLink(spawnCharacter.chunk));
                    // PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new SetBrainState(AIStateType.Wander));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Wander(spawnID, spawnCharacter.rotation));
                    
                    var spawnType = (byte) 0;
                    var random = new Random();
                    random.InitState((uint) spawnID);
                    //! \todo Generate Prefab type in systme before this one, save/load prefab type as well.
                    var spawnTypeChance = random.NextFloat(100);
                    if (isSlime)
                    {
                        if (spawnTypeChance <= 45)
                        {
                            spawnType = NPCType.slem;
                        }
                        else if (spawnTypeChance <= 80)
                        {
                            spawnType = NPCType.glem;
                        }
                        else if (spawnTypeChance > 94)
                        {
                            spawnType = NPCType.chen;
                        }
                    }
                    else if (isHumanoid)
                    {
                        if (spawnTypeChance < 50)
                        {
                            spawnType = NPCType.dave;
                        }
                        else if (spawnTypeChance < 80)
                        {
                            spawnType = NPCType.jona;
                        }
                        else
                        {
                            spawnType = NPCType.minty;  // salesman
                        }
                        if (spawnOnlyTraders)
                        {
                            spawnType = NPCType.minty;
                        }
                    }
                    // UnityEngine.Debug.LogError("Spawning npc: " + spawnType);
                    // For Slime
                    if (isSlime)
                    {
                        // PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new BeginGenerateVox(spawnType, worldID));
                        var voxScaleMultiplier = 6;
                        if (spawnType == NPCType.slem)
                        {
                            voxScaleMultiplier = 16;
                        }
                        else if (spawnType == NPCType.glem)
                        {
                            voxScaleMultiplier = 26;
                        }
                        else if (spawnType == NPCType.chen)
                        {
                            voxScaleMultiplier = 42;
                        }
                        var voxelDimensions = new int3(voxScaleMultiplier, voxScaleMultiplier, voxScaleMultiplier);
                        var randomVariation = voxelDimensions / 5;
                        voxelDimensions.x = math.max(1, voxelDimensions.x - random.NextInt(math.max(2, randomVariation.x)));
                        voxelDimensions.y = math.max(1, voxelDimensions.y - random.NextInt(math.max(2, randomVariation.y)));
                        voxelDimensions.z = math.max(1, voxelDimensions.z - random.NextInt(math.max(2, randomVariation.z)));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Body(
                            0.5f * new float3(voxelScale.x * voxelDimensions.x, voxelScale.y * voxelDimensions.y, voxelScale.z * voxelDimensions.z)));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ChunkDimensions(voxelDimensions));
                        var voxColors = new VoxColors();
                        VoxColorsGenerationSystem.GenerateRandomColors(ref voxColors, spawnType, (uint) spawnID, (uint) spawnID);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, voxColors);
                    }
                    // UnityEngine.Debug.LogError("Character spawned: " + spawnID + ":: " + spawnType);
                    // Loading Characters!
                    if (isLoadingCharacter)
                    {
                        //!? Loaded race? idk
                        if (isSlime)
                        {
                            raceEntity = realmRaceLinks.races[1 + spawnType];
                            // UnityEngine.Debug.LogError("Spawned Loading Character with spawn type: " + spawnType);
                        }
                        else if (isHumanoid)
                        {
                            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new GenerateDialogue(spawnType));
                        }
                    }
                    // New Characters!
                    else 
                    {
                        characterName = NameGenerator.GenerateName2(ref random);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new GenerateStats(spawnType));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new GenerateSkills(spawnType));
                        // Generated Vox Race
                        if (isSlime)
                        {
                            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new GenerateInventory(spawnType));
                            raceEntity = realmRaceLinks.races[1 + spawnType];    // 1, 2 or 3 race
                            // UnityEngine.Debug.LogError("Spawned New Character with spawn type: " + spawnType);
                        }
                        else if (isHumanoid)
                        {
                            // if Salesman - set type differently
                            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new GenerateInventory(spawnType));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new GenerateDialogue(spawnType));
                            //! \todo Cycle through races and find humanoid races, and pick one. PickHumanoidRace component?
                            raceEntity = realmRaceLinks.races[random.NextInt(4, realmRaceLinks.races.Length)]; // randomize as humanoid race
                            if (spawnType == NPCType.jona)
                            {
                                PostUpdateCommands.AddComponent<GiveRealmQuest>(entityInQueryIndex, e2);
                            }
                        }
                        // Mr Penguin / Minions
                        else
                        {
                            // PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new GenerateInventory(spawnType));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new SetModelData(voxDataID));
                            raceEntity = realmRaceLinks.races[0];    // custom race?
                            // UnityEngine.Debug.LogError("Mr Penguin Vox Spawned: " + voxDataID);
                        }
                    }
                    // for summoned beings
                    if (spawnCharacter.creator.Index != 0)
                    {
                        // UnityEngine.Debug.LogError("Summoned One Summoned!");
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new CreatorLink(spawnCharacter.creator));
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new SummonedEntity(elapsedTime, 32 + random.NextFloat(16)));
                        PostUpdateCommands.AddComponent<DisableSaving>(entityInQueryIndex, e2);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new SetSpawnPoint(spawnCharacter.position));
                        PostUpdateCommands.AddComponent<Aggressive>(entityInQueryIndex, e2);
                        // todo: Add Completed event for character summoner too
                        PostUpdateCommands.RemoveComponent<Wander>(entityInQueryIndex, e2);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new Idle(3));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Brain(AIStateType.Idle));
                    }
                    // for aggressive npcs
                    if (chaosMode)
                    {
                        PostUpdateCommands.AddComponent<Aggressive>(entityInQueryIndex, e2);
                    }
                    // Completed event for chunk
                    if (hasChunk) //  && spawnCharacter.isSummonedOne == 0)
                    {
                        // if (spawnCharacter.characterChunkIndex != -1)
                        {
                            var spawnedEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, chunkCharacterSpawnedPrefab);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, spawnedEntity, new ChunkCharacterSpawned(spawnCharacter.chunk, (byte) 1));
                            // PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ChunkCharacterSpawnedLink(spawnCharacter.chunk));
				            #if DEBUG_CHARACTER_SPAWN
                            UnityEngine.Debug.LogError("Chunk " + spawnCharacter.chunk.Index + " - Spawned new character.");
                            #endif
                        }
                    }
                }
                // Player Characters
                else
                {
                    var spawnPlayerCharacter = spawnPlayerCharacters[e];
                    var isNewPlayer = !isLoadingCharacter;
                    var characterMaker = spawnPlayerCharacter.characterMaker;
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ItemHitTaker(itemPickupRadius));
                    // For new players
                    if (isNewPlayer)
                    {
                        // PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new GiveGameSkill(0));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new GenerateStats(NPCType.player));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new GenerateInventory(NPCType.player));
                        var classes = classLinks[realmEntity].data;
                        var classEntity = new Entity();
                        if (classes.Length > 0)
                        {
                            classEntity = classes[0];
                        }
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new SetClass(classEntity));
                        for (int i = 0; i < realmRaceLinks.races.Length; i++)
                        {
                            if (HasComponent<PlayerChoice>(realmRaceLinks.races[i]))
                            {
                                raceEntity = realmRaceLinks.races[i];
                                break;
                            }
                        }
                        characterName = spawnPlayerCharacter.name;
                    }
                    // Connect camera to character
                    if (spawnPlayerCharacter.isConnectCamera == 1)
                    {
                        // Attach Controller to Characcter
                        var controllerEntity = spawnPlayerCharacter.controllerEntity;
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new ControllerLink(controllerEntity));
                        PostUpdateCommands.AddComponent(entityInQueryIndex, controllerEntity, new CharacterLink(e2));
                        PostUpdateCommands.AddComponent(entityInQueryIndex, controllerEntity,
                            new SetControllerMapping(ControllerMapping.InGame, elapsedTime, fadeinScreenTime * 2f + fadeinScreenWaitTime));
                        // Attach camera to character
                        var cameraEntity = spawnPlayerCharacter.cameraEntity;
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new CameraLink(cameraEntity));
                        // Attach character to camera
                        PostUpdateCommands.SetComponent(entityInQueryIndex, cameraEntity, new CharacterLink(e2));
                    }
                    // For CharacterMaker Characters
                    if (characterMaker.Index != 0)
                    {
                        //! \todo Make a prefab for CharacterMakerCharacter.
                        PostUpdateCommands.SetComponent(entityInQueryIndex, characterMaker, new CharacterMakerUI(e2));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, spawnPlayerCharacter.viewerUI, new ViewerUI(e2));
                        PostUpdateCommands.AddComponent<CharacterMakerCharacter>(entityInQueryIndex, e2);
                        PostUpdateCommands.AddComponent<ViewerObject>(entityInQueryIndex, e2);
                        PostUpdateCommands.AddComponent<DisableSaving>(entityInQueryIndex, e2);
                        PostUpdateCommands.RemoveComponent<SaveNewCharacter>(entityInQueryIndex, e2);
                        PostUpdateCommands.AddComponent<DisableRaycaster>(entityInQueryIndex, e2);
                        PostUpdateCommands.AddComponent<DisabledChunkStreamPoint>(entityInQueryIndex, e2);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new EntityLighting(16));
                    }
                    else if (isNewPlayer)
                    {
                        PostUpdateCommands.AddComponent<PlaceCharacterInWorld>(entityInQueryIndex, e2);
                        PostUpdateCommands.AddComponent<DisableSaving>(entityInQueryIndex, e2);
                        PostUpdateCommands.AddComponent<SpawnRealmUI>(entityInQueryIndex, e2);
                        PostUpdateCommands.RemoveComponent<SaveNewCharacter>(entityInQueryIndex, e2);
                        PostUpdateCommands.RemoveComponent<ChunkStreamPoint>(entityInQueryIndex, e2);
                    }
                }
                if (characterName.Length != 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ZoxName(characterName));
                }
                // Set Race
                if (raceEntity.Index != 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new RaceLink(raceEntity));
                }
                if (disableCharacterLighting)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new EntityLighting(16));
                }
			})  .WithReadOnly(spawnPlayerCharacters)
                .WithReadOnly(raceLinks)
                .WithReadOnly(classLinks)
                .WithReadOnly(realmIDs)
                .WithReadOnly(planetIDs)
                .WithReadOnly(chunkPositions)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}