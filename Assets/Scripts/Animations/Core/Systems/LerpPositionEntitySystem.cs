using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Animations
{
    [BurstCompile, UpdateInGroup(typeof(AnimationSystemGroup))]
    public partial class LerpPositionEntitySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processingQuery;
        private EntityQuery lerpingQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processingQuery = GetEntityQuery(ComponentType.ReadOnly<PositionEntityLerper>());
            lerpingQuery = GetEntityQuery(ComponentType.ReadOnly<Translation>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (processingQuery.IsEmpty)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();

            Dependency = Entities
                .WithAll<InitializePositionEntityLerper>()
                .ForEach((Entity e, int entityInQueryIndex, ref PositionEntityLerper positionEntityLerper, in Translation translation) =>
            {
                positionEntityLerper.positionBegin = translation.Value;
                PostUpdateCommands.RemoveComponent<InitializePositionEntityLerper>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

            var elapsedTime = World.Time.ElapsedTime;
            var entities = lerpingQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var translations = GetComponentLookup<Translation>(true);
            entities.Dispose();
            Dependency = Entities.ForEach((ref Translation position, in PositionEntityLerper lerper) =>
            {
                if (translations.HasComponent(lerper.positionEnd))
                {
                    var characterTranslation = translations[lerper.positionEnd];
                    var lerpValue = (float)((elapsedTime - lerper.createdTime) / lerper.lifeTime);
                    position.Value = math.lerp(lerper.positionBegin, characterTranslation.Value, lerpValue);
                }
            })  .WithReadOnly(translations)
                .WithNativeDisableContainerSafetyRestriction(translations)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}