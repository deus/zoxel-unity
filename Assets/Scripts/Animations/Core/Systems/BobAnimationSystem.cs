﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Animations
{
    //! Animates something by bobbing up and down.
    [BurstCompile, UpdateInGroup(typeof(AnimationSystemGroup))]
    public partial class BobAnimationSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
		{
            var time = World.Time.ElapsedTime;
            float bobAmount = 0.06f;
            double bobSpeed = 1;
            Entities.ForEach((ref Translation position, in Rotation rotation, in ItemBob item) =>
            {
                float sinAmount = ((float)math.sin(time * bobSpeed));
                position.Value = item.originalPosition + math.mul(rotation.Value, new float3(0, item.additionalY + sinAmount * bobAmount - bobAmount * 2f, 0));
            }).ScheduleParallel();
        }
    }
}