﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Zoxel.Transforms;

namespace Zoxel.Animations
{
    [BurstCompile, UpdateInGroup(typeof(AnimationSystemGroup))]
    public partial class ShrinkSystem2 : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
		{
            var deltaTime = World.Time.DeltaTime;
            Entities.WithAll<Shrink, Scale>().ForEach((ref Scale scale) =>    // ref Shrink component, 
            {
                scale.Value = math.lerp(scale.Value, 0, deltaTime);
            }).ScheduleParallel();
        }
    }

    [BurstCompile, UpdateInGroup(typeof(AnimationSystemGroup))]
    public partial class ShrinkSystem3 : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
		{
            var deltaTime = World.Time.DeltaTime;
            Entities.WithAll<Shrink, NonUniformScale>().ForEach((ref NonUniformScale scale) =>    // ref Shrink component, 
            {
                scale.Value = math.lerp(scale.Value, 0, deltaTime);
            }).ScheduleParallel();
        }
    }
}