﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

using Zoxel.Transforms;

namespace Zoxel.Animations
{
    //! Lerps position between two points.
    /**
    *   \todo Make lerp event based instead of constant.
    */
    [BurstCompile, UpdateInGroup(typeof(AnimationSystemGroup))]
    public partial class PositionLerpSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            var time = World.Time.ElapsedTime;
            // and keep positions up to date
            Entities
                .WithAll<Translation>()
                .ForEach((ref LerpPosition lerpPosition) =>
            {
                // just do this for looping animations
                if (lerpPosition.loop == AnimationLoopType.Reverse || lerpPosition.loop == AnimationLoopType.Loop)
                {
                    var timePassed = time - (lerpPosition.createdTime + lerpPosition.delay);
                    if (timePassed > lerpPosition.lifeTime)
                    {
                        if (lerpPosition.loop == AnimationLoopType.Reverse)
                        {
                            lerpPosition.loop = AnimationLoopType.None;
                        }
                        var tempEnd = lerpPosition.positionEnd;
                        lerpPosition.positionEnd = lerpPosition.positionBegin;
                        lerpPosition.positionBegin = tempEnd;
                        lerpPosition.createdTime = time;
                    }
                }
            }).ScheduleParallel();
            Entities
                .WithNone<LocalPosition>()
                .ForEach((ref Translation position, in LerpPosition lerpPosition) =>
            {
                var timePassed = time - (lerpPosition.createdTime + lerpPosition.delay);
                if (timePassed >= 0)
                {
                    var lerpValue = timePassed / lerpPosition.lifeTime;
                    if (lerpValue > 1)
                    {
                        lerpValue = 1;
                    }
                    position.Value = math.lerp(lerpPosition.positionBegin, lerpPosition.positionEnd, (float) lerpValue);
                }
            }).ScheduleParallel();
            Entities.ForEach((ref LocalPosition localPosition, in LerpPosition lerpPosition) =>
            {
                var timePassed = time - (lerpPosition.createdTime + lerpPosition.delay);
                if (timePassed >= 0)
                {
                    var lerpValue = timePassed / lerpPosition.lifeTime;
                    if (lerpValue > 1)
                    {
                        lerpValue = 1;
                    }
                    localPosition.position = math.lerp(lerpPosition.positionBegin, lerpPosition.positionEnd, (float) lerpValue);
                }
            }).ScheduleParallel();
        }
    }
}