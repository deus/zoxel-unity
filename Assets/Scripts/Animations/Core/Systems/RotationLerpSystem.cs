using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Zoxel.Transforms;

namespace Zoxel.Animations
{
    [BurstCompile, UpdateInGroup(typeof(AnimationSystemGroup))]
    public partial class RotationLerpSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            var time = World.Time.ElapsedTime;
            // and keep positions up to date
            Entities.ForEach((ref LerpRotation lerpRotation) => // , ref Rotation rotation
            {
                // just do this for looping animations
                if (lerpRotation.loop == AnimationLoopType.Reverse || lerpRotation.loop == AnimationLoopType.Loop)
                {
                    var timePassed = time - (lerpRotation.createdTime + lerpRotation.delay);
                    if (timePassed > lerpRotation.lifeTime)
                    {
                        if (lerpRotation.loop == AnimationLoopType.Reverse)
                        {
                            lerpRotation.loop = AnimationLoopType.None;
                        }
                        var tempEnd = lerpRotation.rotationEnd;
                        lerpRotation.rotationEnd = lerpRotation.rotationBegin;
                        lerpRotation.rotationBegin = tempEnd;
                        lerpRotation.createdTime = time;
                    }
                }
            }).ScheduleParallel();
            var halfPI = math.PI / 2f;
            Entities
                .WithNone<LocalRotation>()
                .ForEach((ref Rotation rotation, in LerpRotation lerpRotation) =>
            {
                var timePassed = time - (lerpRotation.createdTime + lerpRotation.delay);
                if (timePassed >= 0)
                {
                    var lerpValue = (float)(timePassed / lerpRotation.lifeTime);
                    if (lerpValue > 1)
                    {
                        lerpValue = 1;
                    }
                    if (lerpRotation.type == 1)
                    {
                        lerpValue = (float) (math.sin(halfPI * lerpValue));
                    }
                    rotation.Value = QuaternionHelpers.slerp(lerpRotation.rotationBegin, lerpRotation.rotationEnd, lerpValue);
                }
            }).ScheduleParallel();
            Entities.ForEach((ref LocalRotation localRotation, in LerpRotation lerpRotation) =>
            {
                var timePassed = time - (lerpRotation.createdTime + lerpRotation.delay);
                if (timePassed >= 0)
                {
                    var lerpValue = (float)(timePassed / lerpRotation.lifeTime);
                    if (lerpValue > 1)
                    {
                        lerpValue = 1;
                    }
                    if (lerpRotation.type == 1)
                    {
                        lerpValue = (float) (math.sin(halfPI * lerpValue));
                    }
                    localRotation.rotation = QuaternionHelpers.slerp(lerpRotation.rotationBegin, lerpRotation.rotationEnd, lerpValue);
                }
            }).ScheduleParallel();
        }
    }

}