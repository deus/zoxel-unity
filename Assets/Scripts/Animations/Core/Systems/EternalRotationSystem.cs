using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Animations
{
    //! Animates something by bobbing up and down.
    [BurstCompile, UpdateInGroup(typeof(AnimationSystemGroup))]
    public partial class EternalRotationSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
		{
            var elapsedTime = World.Time.ElapsedTime;
            var deltaTime = World.Time.DeltaTime * 0.6f;
            Entities.ForEach((ref Rotation rotation, in EternalRotation eternalRotation) =>
            {
                // var sinAmount = (float) math.sin(elapsedTime * eternalRotation.speed);
                rotation.Value = QuaternionHelpers.slerp(
                    rotation.Value,
                    math.mul(rotation.Value, eternalRotation.euler), 
                    deltaTime);
            }).ScheduleParallel();
        }
    }
}