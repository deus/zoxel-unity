using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Animations
{
    //! Animates something by bobbing up and down.
    [BurstCompile, UpdateInGroup(typeof(AnimationSystemGroup))]
    public partial class EternalScalingSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
		{
            double bobSpeed = 1;
            var elapsedTime = World.Time.ElapsedTime;
            Entities.ForEach((ref Scale scale, in EternalScaling eternalScaling) =>
            {
                // UnityEngine.Debug.LogError("elapsedTime: " + elapsedTime);
                var sinAmount = (float) (eternalScaling.additionScale * math.sin(elapsedTime * bobSpeed));
                scale.Value = eternalScaling.baseScale + sinAmount;
            }).ScheduleParallel();
        }
    }
}