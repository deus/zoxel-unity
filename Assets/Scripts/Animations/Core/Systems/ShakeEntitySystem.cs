using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Zoxel.Transforms;

namespace Zoxel.Animations
{
    [BurstCompile, UpdateInGroup(typeof(AnimationSystemGroup))]
    public partial class ShakeEntitySystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            var deltaTime = World.Time.DeltaTime;
            var elapsedTime = World.Time.ElapsedTime;
            elapsedTime += 1;
            var rotationVariance = 0.1f;
            Entities.ForEach((int entityInQueryIndex, ref LocalRotation localRotation, ref ShakeEntity shakeEntity) =>
            {
                if (shakeEntity.init == 0)
                {
                    shakeEntity.init = 1;
                    shakeEntity.initialRotation = localRotation.rotation;
                }
                var random = new Random();
                random.InitState((uint)((int)(elapsedTime + entityInQueryIndex)));
                var newRotation = new quaternion(
                    shakeEntity.initialRotation.value.x + random.NextFloat(-rotationVariance, rotationVariance),
                    shakeEntity.initialRotation.value.y + random.NextFloat(-rotationVariance, rotationVariance),
                    shakeEntity.initialRotation.value.z + random.NextFloat(-rotationVariance, rotationVariance),
                    shakeEntity.initialRotation.value.w + random.NextFloat(-rotationVariance, rotationVariance)
                );
                localRotation.rotation = QuaternionHelpers.slerp(localRotation.rotation, newRotation, deltaTime);
            }).ScheduleParallel();
            /*Entities.ForEach((int entityInQueryIndex, ref Rotation rotation, ref ShakeEntity shakeEntity) =>
            {
                if (shakeEntity.init == 0)
                {
                    shakeEntity.init = 1;
                    shakeEntity.initialRotation = rotation.Value;
                }
                var random = new Random();
                random.InitState((uint)((int)(elapsedTime + entityInQueryIndex)));
                var newRotation = new quaternion(
                    shakeEntity.initialRotation.value.x + random.NextFloat(-rotationVariance, rotationVariance),
                    shakeEntity.initialRotation.value.y + random.NextFloat(-rotationVariance, rotationVariance),
                    shakeEntity.initialRotation.value.z + random.NextFloat(-rotationVariance, rotationVariance),
                    shakeEntity.initialRotation.value.w + random.NextFloat(-rotationVariance, rotationVariance)
                );
                rotation.Value = QuaternionHelpers.slerp(rotation.Value, newRotation, deltaTime);
            }).ScheduleParallel();
            Entities.WithAll<ShakeEntity>().ForEach((ref Translation translation) =>
            {
                translation.Value += new float3(1, 0, 0) * deltaTime;
            }).ScheduleParallel();*/
        }
    }
}