﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Zoxel.Transforms;

namespace Zoxel.Animations
{
    [BurstCompile, UpdateInGroup(typeof(AnimationSystemGroup))]
    public partial class SinRotatorSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
		{
            var time = (float)World.Time.ElapsedTime;
            var degreesToRadians = ((math.PI * 2) / 360f);
            Entities.ForEach((ref Rotation rotation, in SinRotator sinRotator) =>    // ref Shrink component, 
            {
                rotation.Value = quaternion.EulerXYZ(new float3(0, 0, math.sin(sinRotator.multiplier * time)) * degreesToRadians);
            }).ScheduleParallel();
        }
    }
}
