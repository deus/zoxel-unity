﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Zoxel.Transforms;

namespace Zoxel.Animations
{
    [BurstCompile, UpdateInGroup(typeof(AnimationSystemGroup))]
    public partial class ScaleLerpSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // and keep positions up to date
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref NonUniformScale scale, ref LerpScale lerpScale) =>
            {
                var timePassed = elapsedTime - (lerpScale.createdTime + lerpScale.delay);
                if (lerpScale.loop == AnimationLoopType.Reverse)
                {
                    if (timePassed > lerpScale.lifeTime)
                    { 
                        lerpScale.loop = AnimationLoopType.None;
                        lerpScale.createdTime = elapsedTime;
                        timePassed = timePassed - lerpScale.lifeTime;
                        // Swap scales
                        var tempEnd = lerpScale.scaleEnd;
                        lerpScale.scaleEnd = lerpScale.scaleBegin;
                        lerpScale.scaleBegin = tempEnd;
                    }
                }
                //var timePassed = time - lerpScale.createdTime;
                if (timePassed >= 0) //  lerpScale.delay && timePassed - lerpScale.delay <= lerpScale.lifeTime)
                {
                    if (timePassed > lerpScale.lifeTime)
                    {
                        timePassed = lerpScale.lifeTime;
                        if (HasComponent<ScaleLerpOnce>(e))
                        {
                            PostUpdateCommands.RemoveComponent<LerpScale>(entityInQueryIndex, e);
                            PostUpdateCommands.RemoveComponent<ScaleLerpOnce>(entityInQueryIndex, e);
                        }
                    }
                    var lerpValue = (float)(timePassed / lerpScale.lifeTime);
                    scale.Value = math.lerp(lerpScale.scaleBegin, lerpScale.scaleEnd, lerpValue);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}