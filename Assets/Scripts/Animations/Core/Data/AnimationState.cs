namespace Zoxel.Animations
{
    //! Different states for our character to animate with.
    public static class AnimationState
    {
        public const byte None = 0;
        public const byte Idle = 1;
        public const byte Walking = 2;
        public const byte PreAttack = 3;
        public const byte Attacking = 4;
    }
}