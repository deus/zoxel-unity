namespace Zoxel.Animations
{
    public static class AnimationLoopType
    {
        public const byte None = 0;
        public const byte Reverse = 1;
        public const byte Loop = 2;
    }
}