﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Animations
{
    public struct LerpPosition : IComponentData
    {
        public double createdTime;
        public double lifeTime;
        public float3 positionBegin;
        public float3 positionEnd;
        public byte loop;
        public double delay;

        public LerpPosition(double createdTime, double lifeTime, float3 positionBegin, float3 positionEnd, byte loop = 0)
        {
            this.createdTime = createdTime;
            this.lifeTime = lifeTime;
            this.positionBegin = positionBegin;
            this.positionEnd = positionEnd;
            this.loop = loop;
            this.delay = 0;
        }
    }
}