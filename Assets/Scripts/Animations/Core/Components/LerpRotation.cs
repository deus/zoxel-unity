using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Animations
{
    public struct LerpRotation : IComponentData
    {
        public byte loop;
        public byte type;   // 0 for normal, 1 for sine
        public double createdTime;
        public double lifeTime;
        public double delay;
        public quaternion rotationBegin;
        public quaternion rotationEnd;
    }
}