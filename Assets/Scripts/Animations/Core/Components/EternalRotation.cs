using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Animations
{
    //! Rotates along an axis using a rotation and speed.
    public struct EternalRotation : IComponentData
    {
        public float speed;
        public quaternion euler;
        
        public EternalRotation(float speed, float3 rotation)
        {
            this.speed = speed;
            var degreesToRadians = ((math.PI * 2) / 360f);
            this.euler = quaternion.EulerXYZ(rotation * degreesToRadians);
        }
    }
}