﻿using Unity.Entities;

namespace Zoxel.Animations
{
    public struct SinRotator : IComponentData
    {
        public float timeBegun;
        public float multiplier;
    }
}