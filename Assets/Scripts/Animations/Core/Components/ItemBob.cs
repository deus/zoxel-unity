using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Animations
{
    public struct ItemBob : IComponentData
    {
        public float3 originalPosition;
        public float additionalY;
        public float sinMultiplier;
        public float timeScale;
    }
}