using Unity.Entities;

namespace Zoxel.Animations
{
    //! Animates scale eternally using sin and a additional scale value.
    public struct EternalScaling : IComponentData
    {
        public float baseScale;
        public float additionScale;
        
        public EternalScaling(float baseScale, float additionScale)
        {
            this.baseScale = baseScale;
            this.additionScale = additionScale;
        }
    }
}