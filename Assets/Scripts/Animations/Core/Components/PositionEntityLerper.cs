using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Animations
{
    public struct PositionEntityLerper : IComponentData
    {
        public double createdTime;
        public double lifeTime;
        public float3 positionBegin;
        public Entity positionEnd;

        public PositionEntityLerper(double createdTime, double lifeTime, float3 positionBegin, Entity positionEnd)
        {
            this.createdTime = createdTime;
            this.lifeTime = lifeTime;
            this.positionBegin = positionBegin;
            this.positionEnd = positionEnd;
        }
    }
}