using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Animations
{
    public struct LerpScale : IComponentData
    {
        public byte loop;
        public double createdTime;
        public double lifeTime;
        public double delay;
        public float3 scaleBegin;
        public float3 scaleEnd;

        public LerpScale(double createdTime, double lifeTime, float3 scaleBegin, float3 scaleEnd, byte loop = 0)
        {
            this.createdTime = createdTime;
            this.lifeTime = lifeTime;
            this.scaleBegin = scaleBegin;
            this.scaleEnd = scaleEnd;
            this.loop = loop;
            this.delay = 0;
        }
    }
}