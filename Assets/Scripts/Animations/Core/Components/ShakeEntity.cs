using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Animations
{
    public struct ShakeEntity : IComponentData
    {
        public byte init;
        public quaternion initialRotation;
    }
}