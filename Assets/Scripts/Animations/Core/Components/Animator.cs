﻿using Unity.Entities;
using Unity.Mathematics;
// lol old comment (poor timmys dead): finally the animator component i need to add to any monsters with animator data (timmy)
// planet spawned in!
// create debug feet position system to show feet positions
// if walking then find new feet position in front of character
// for animateOnce animations

namespace Zoxel.Animations
{
    //! Holds links to animation states
    /**
    *   \todo Transfer each animation group as a layer, seperate data.
    */
    public struct Animator : IComponentData
    {
        // should this be per limb?
        public float animationSpeed;
        // Blobs
        public double coreAnimationStartTime;
        public byte coreState;
        public byte setCoreState;
        // Legs
        public double legAnimationStartTime;
        public byte legsState;
        public byte setLegsState;
        // Left Arm
        public double leftArmAnimationStartTime;
        public byte leftArmState;     
        public byte setLeftArmState;
        // Right Arm
        public double rightArmAnimationStartTime;
        public byte rightArmState;     
        public byte setRightArmState;
        // used for IK arms?
        public float3 cameraPosition;
        public quaternion cameraRotation;
    }
}
