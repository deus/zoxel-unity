using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Zoxel.Transforms;
using Zoxel.Cameras;

namespace Zoxel.Animations.Cameras
{
    [BurstCompile, UpdateInGroup(typeof(AnimationSystemGroup))]
    public partial class AnimatorCameraSystem : SystemBase
    {
        private EntityQuery cameraQuery;

        protected override void OnCreate()
        {
            cameraQuery = GetEntityQuery(
                ComponentType.ReadOnly<Camera>(),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<Rotation>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var cameraEntities = cameraQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var cameraPositions = GetComponentLookup<Translation>(true);
            var cameraRotations = GetComponentLookup<Rotation>(true);
            //  cameraEntities.Dispose();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .ForEach((ref Animator animator, in CameraLink cameraLink) =>
            {
                if (cameraPositions.HasComponent(cameraLink.camera))
                {
                    animator.cameraPosition = cameraPositions[cameraLink.camera].Value;
                    animator.cameraRotation = cameraRotations[cameraLink.camera].Value;
                }
                if (cameraEntities.Length > 0) { var e2 = cameraEntities[0]; }
            })  .WithReadOnly(cameraPositions)
                .WithReadOnly(cameraRotations)
                .WithReadOnly(cameraEntities)
                .WithDisposeOnCompletion(cameraEntities)
                .ScheduleParallel(Dependency);
        }
    }
}