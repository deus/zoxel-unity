using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    public struct SetGizmoMesh : IComponentData
    {
        public byte type;

        public SetGizmoMesh(byte type)
        {
            this.type = type;
        }
    }
}