using Unity.Entities;
using Unity.Burst;

namespace Zoxel.VoxelInteraction
{
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class VoxelGizmoEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<OnRaycastVoxel>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<OnRaycastVoxel>(processQuery);
        }
    }
}