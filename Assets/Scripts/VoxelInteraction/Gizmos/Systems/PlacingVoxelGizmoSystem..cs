using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;
using Zoxel.Items;
using Zoxel.Voxels;

namespace Zoxel.VoxelInteraction
{
    //! Updates gizmo after selecting new Voxel.
    /**
    *   \todo Show gizmo for voxel only if - Physical Attack / Summoner Skill or item of type Voxel!
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class PlacingVoxelGizmoSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<BeginPlacingVoxelItem>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<BeginPlacingVoxelItem>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<EndPlacingVoxelItem>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<EndPlacingVoxelItem>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<BeginDisableVoxelInteraction>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<BeginDisableVoxelInteraction>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<EndDisableVoxelInteraction>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<EndDisableVoxelInteraction>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<BeginPlacingVoxelItem>()
                .ForEach((Entity e, int entityInQueryIndex, in GizmoLinks gizmoLinks) =>
            {
                PostUpdateCommands.AddComponent<OnRaycastVoxel>(entityInQueryIndex, e);
                // PostUpdateCommands.AddComponent(entityInQueryIndex, gizmoLinks.voxelGizmo, new SetGizmoMesh(1));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<EndPlacingVoxelItem>()
                .ForEach((Entity e, int entityInQueryIndex, in GizmoLinks gizmoLinks) =>
            {
                PostUpdateCommands.AddComponent<OnRaycastVoxel>(entityInQueryIndex, e);
                // PostUpdateCommands.AddComponent(entityInQueryIndex, gizmoLinks.voxelGizmo, new SetGizmoMesh(0));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<BeginDisableVoxelInteraction>()
                .ForEach((Entity e, int entityInQueryIndex, in GizmoLinks gizmoLinks) =>
            {
                PostUpdateCommands.AddComponent<OnRaycastVoxel>(entityInQueryIndex, e);
                // PostUpdateCommands.AddComponent(entityInQueryIndex, gizmoLinks.voxelGizmo, new SetGizmoMesh(0));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<EndDisableVoxelInteraction>()
                .ForEach((Entity e, int entityInQueryIndex, in GizmoLinks gizmoLinks) =>
            {
                PostUpdateCommands.AddComponent<OnRaycastVoxel>(entityInQueryIndex, e);
                // PostUpdateCommands.AddComponent(entityInQueryIndex, gizmoLinks.voxelGizmo, new SetGizmoMesh(0));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}