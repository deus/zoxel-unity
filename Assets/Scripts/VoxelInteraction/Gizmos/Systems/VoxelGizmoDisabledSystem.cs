using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;

namespace Zoxel.VoxelInteraction
{
    //! Hides gizmo.
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class VoxelGizmoDisabledSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities
                .WithNone<EntityBusy, InitializeEntity, DisableRaycaster>()
                .WithNone<OnRaycastGrass, OnRaycastMinivox>()
                .WithAll<OnRaycastVoxel, DisableVoxelInteraction>()
                .ForEach((int entityInQueryIndex, in GizmoLinks gizmoLinks) =>
            {
                var voxelGizmo = gizmoLinks.voxelGizmo;
                if (HasComponent<LocalToWorld>(voxelGizmo))
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, voxelGizmo, new Translation { Value = new float3(0, -1000, 0) });
                }
            }).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}