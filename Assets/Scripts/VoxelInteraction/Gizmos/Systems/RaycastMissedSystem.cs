using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.UI;

namespace Zoxel.VoxelInteraction
{
    //! Handles gizmo for when raycasting misses.
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class RaycastMissedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery controllersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            controllersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Player>(),
                ComponentType.ReadOnly<PlayerTooltipLinks>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var disableRaycasting = false;
            if (disableRaycasting)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var playerTooltipLinks2 = GetComponentLookup<PlayerTooltipLinks>(true);
            // controllerEntities.Dispose();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .WithAll<OnRaycastMissed>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<OnRaycastMissed>(entityInQueryIndex, e);
                if (controllerEntities.Length > 0) { var e2 = controllerEntities[0]; }
            })  .WithReadOnly(controllerEntities)
                .WithDisposeOnCompletion(controllerEntities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity, DisableRaycaster>()
                .WithAll<OnRaycastMissed>()
                .ForEach((int entityInQueryIndex, in ControllerLink controllerLink) =>
            {
                var playerTooltipLinks = playerTooltipLinks2[controllerLink.controller];
                playerTooltipLinks.SetPlayerSelectionTooltip(PostUpdateCommands, entityInQueryIndex, new Text());
            })  .WithReadOnly(playerTooltipLinks2)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity, DisableRaycaster>()
                .WithAll<OnRaycastMissed>()
                .ForEach((int entityInQueryIndex, in GizmoLinks gizmoLinks) =>
            {
                var voxelGizmo = gizmoLinks.voxelGizmo;
                PostUpdateCommands.SetComponent(entityInQueryIndex, voxelGizmo, new Translation { Value = new float3(0, -1000, 0) });
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}