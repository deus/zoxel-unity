using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.VoxelInteraction
{
    //! Spawns a Voxel Gizmo for our player.
    /**
    *   - Spawn System -
    *   \todo When selected a voxel - creates a selection gizmo overlaying that voxel. If not selected, starts fading out. When selected again fades back or spawns. Only works for solid voxels.
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class GizmoSpawnSystem : SystemBase
    {
        public static UnityEngine.Mesh quadMesh;
        public static UnityEngine.Mesh cubeMesh;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity previewMeshPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var previewMeshArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(Gizmo),
                typeof(BasicRender),
                typeof(ZoxMesh),
                typeof(DontDestroyMesh),
                typeof(DontDestroyMaterial),
                typeof(DontDestroyTexture),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale)
            );
            previewMeshPrefab = EntityManager.CreateEntity(previewMeshArchetype);
            EntityManager.SetComponentData(previewMeshPrefab, new Rotation { Value = UnityEngine.Quaternion.Euler(new float3(90, 0, 0)) });
            EntityManager.SetComponentData(previewMeshPrefab, new NonUniformScale { Value = new float3(1, 1, 1) });
        }

        protected override void OnDestroy()
        {
            if (quadMesh != null)
            {
                ObjectUtil.Destroy(quadMesh);
                ObjectUtil.Destroy(cubeMesh);
            }
        }

        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null) return;
            var voxelGizmoMaterial = MaterialsManager.instance.materials.voxelGizmoMaterial;
            if (quadMesh == null)
            {
                quadMesh = MeshUtilities.CreateQuadMeshXZ(new float2(1f, 1f) * 1f);
                cubeMesh = MeshUtilities.GenerateCube(new float3(1,1,1), true); // 0.16f * 
                EntityManager.SetSharedComponentManaged(previewMeshPrefab, new ZoxMesh
                {
                    mesh = quadMesh,
                    material = voxelGizmoMaterial
                });
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); //.AsParallelWriter();
            Entities
                .WithAll<SpawnPreviewMesh>()
                .ForEach((Entity e, ref GizmoLinks gizmoLinks) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnPreviewMesh>(e);
                // store a different previewMesh for every type of voxel combination
                var previewMesh = EntityManager.Instantiate(previewMeshPrefab);
                gizmoLinks.voxelGizmo = previewMesh;
            }).WithStructuralChanges().Run();
        }
    }
}