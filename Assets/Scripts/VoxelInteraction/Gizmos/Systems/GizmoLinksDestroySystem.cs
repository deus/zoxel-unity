using Unity.Entities;
using Unity.Burst;

namespace Zoxel.VoxelInteraction
{
    //! Disposes of GizmoLinks.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class GizmoLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyEntity, GizmoLinks>()
                .ForEach((Entity e, int entityInQueryIndex, in GizmoLinks gizmoLinks) =>
            {
                if (HasComponent<Gizmo>(gizmoLinks.voxelGizmo))
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, gizmoLinks.voxelGizmo);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            
        }
    }
}