using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;
using Zoxel.Transforms;
using Zoxel.Items;
using Zoxel.Voxels;

namespace Zoxel.VoxelInteraction
{
    //! Updates gizmo after selecting new Voxel.
    /**
    *   \todo Show gizmo for voxel only if - Physical Attack / Summoner Skill or item of type Voxel!
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class VoxelGizmoSystem : SystemBase
    {
        const float offset = 0.1f; // 0.01f
        const float linesSize2 = 0.5f; // 0.49
        const float previewMeshOffset = 0.01f; // 0.001f; // 0.01f
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;
        private EntityQuery itemsQuery;
        private EntityQuery voxelsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxScale>());
            itemsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Item>(),
                ComponentType.ReadOnly<VoxelItem>());
            voxelsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Voxel>(),
                ComponentType.ReadOnly<BlockDimensions>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxScales = GetComponentLookup<VoxScale>(true);
            voxEntities.Dispose();
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxelItems = GetComponentLookup<VoxelItem>(true);
            itemEntities.Dispose();
            var voxelEntities = voxelsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var blockDimensions = GetComponentLookup<BlockDimensions>(true);
            voxelEntities.Dispose();
            var characterEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var placingVoxelItems = GetComponentLookup<PlacingVoxelItem>(true);
            characterEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<OnRaycastGrass, OnRaycastMinivox, DisableVoxelInteraction>()
                .WithNone<EntityBusy, InitializeEntity, DisableRaycaster>()
                .WithAll<OnRaycastVoxel>()
                .ForEach((Entity e, int entityInQueryIndex, in RaycastVoxel raycastVoxel, in GizmoLinks gizmoLinks, in GravityQuadrant gravityQuadrant) =>
            {
                var voxelGizmo = gizmoLinks.voxelGizmo;
                var voxEntity = raycastVoxel.hitVoxel.vox;
                if (!HasComponent<LocalToWorld>(voxelGizmo) || !voxScales.HasComponent(voxEntity))
                {
                    return;
                }
                var hasPlacingVoxelItem = placingVoxelItems.HasComponent(e);
                var voxelRotation = BlockRotation.GetBlockRotation(gravityQuadrant.quadrant, raycastVoxel.blockRotationRegion);
                var voxelRotationQuaternion = BlockRotation.GetRotation(voxelRotation);
                var blockDimensions3 = new int3(1, 1, 1);
                var blockOffset = float3.zero;
                if (hasPlacingVoxelItem)
                {
                    var itemEntity = placingVoxelItems[e].itemPlacing;
                    var voxelEntity = voxelItems[itemEntity].voxel;
                    if (blockDimensions.HasComponent(voxelEntity))
                    {
                        var blockDimensions2 = blockDimensions[voxelEntity];
                        blockDimensions3 = blockDimensions2.size;
                        blockOffset = blockDimensions2.GetPositionOffset(voxelRotation);
                    }
                }
                // UnityEngine.Debug.LogError("Place offset: " + blockOffset + " dimensions: " + blockDimensions3);
                var voxScale = voxScales[voxEntity].scale;
                // var hasBlockDimensions = HasComponent<BlockDimensions>(raycastVoxel.hitVoxel.voxel);
                UpdateGizmoTransform(PostUpdateCommands, entityInQueryIndex, in raycastVoxel, voxelGizmo, voxScale,
                    voxelRotationQuaternion, hasPlacingVoxelItem, blockDimensions3, blockOffset);
                if (!hasPlacingVoxelItem)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, voxelGizmo, new SetGizmoMesh(0));
                }
                else
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, voxelGizmo, new SetGizmoMesh(1));
                }
            })  .WithReadOnly(voxScales)
                .WithReadOnly(voxelItems)
                .WithReadOnly(blockDimensions)
                .WithReadOnly(placingVoxelItems)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            
        }
        
        private static void UpdateGizmoTransform(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, in RaycastVoxel raycastVoxel,
            Entity voxelGizmo, float3 voxScale, quaternion voxelRotationQuaternion, bool hasPlacingVoxelItem, int3 blockDimensions, float3 blockOffset)
        {
            var hitNormal = raycastVoxel.hitNormal;
            var newPosition = raycastVoxel.hitVoxel.position.ToFloat3();    //  + new float3(0.5f, 0.5f, 0.5f)
            newPosition.x *= voxScale.x;
            newPosition.y *= voxScale.y;
            newPosition.z *= voxScale.z;
            newPosition += voxScale / 2f;
            // var degreesToRadians = ((math.PI * 2) / 360f);
            var newRotation = quaternion.identity;
            newPosition += new float3(hitNormal.x * (voxScale.x / 2f + previewMeshOffset), hitNormal.y * (voxScale.y / 2f + previewMeshOffset), hitNormal.z * (voxScale.z / 2f + previewMeshOffset));
            // change angle based on normal
            if (hitNormal == int3.left)
            {
                newRotation = quaternion.LookRotation(new float3(0, -1, 0), new float3(-1, 0, 0));
                // newRotation = quaternion.EulerXYZ(new float3(0, 0, 90) * degreesToRadians);
            }
            else if (hitNormal == int3.right)
            {
                newRotation = quaternion.LookRotation(new float3(0, -1, 0), new float3(1, 0, 0));
                // newRotation = quaternion.EulerXYZ(new float3(0, 0, -90) * degreesToRadians);
            }
            else if (hitNormal == int3.down)
            {
                newRotation = quaternion.LookRotation(new float3(0, 0, -1), new float3(0, -1, 0));
                //newRotation = quaternion.EulerXYZ(new float3(180, 0, 0) * degreesToRadians);
            }
            else if (hitNormal == int3.up)
            {
                newRotation = quaternion.LookRotation(new float3(0, 0, 1), new float3(0, 1, 0));
                //newRotation = quaternion.EulerXYZ(new float3(0, 0, 0) * degreesToRadians);
            }
            else if (hitNormal == int3.back)
            {
                newRotation = quaternion.LookRotation(new float3(0, -1, 0), new float3(0, 0, -1));
                //newRotation = quaternion.EulerXYZ(new float3(-90, 0, 0) * degreesToRadians);
            }
            else if (hitNormal == int3.forward)
            {
                newRotation = quaternion.LookRotation(new float3(0, -1, 0), new float3(0, 0, 1));
                //newRotation = quaternion.EulerXYZ(new float3(90, 0, 0) * degreesToRadians);
            }
            // Gizmo Scale
            var gizmoScale = voxScale;
            if (hasPlacingVoxelItem)
            {
                newPosition += new float3(hitNormal.x * (voxScale.x / 2f - previewMeshOffset), hitNormal.y * (voxScale.y / 2f - previewMeshOffset), hitNormal.z * (voxScale.z / 2f - previewMeshOffset));
                newPosition += new float3(voxScale.x * blockOffset.x, voxScale.y * blockOffset.y, voxScale.z * blockOffset.z);
                gizmoScale = blockDimensions.ToFloat3();
                gizmoScale.x *= voxScale.x;
                gizmoScale.y *= voxScale.y;
                gizmoScale.z *= voxScale.z;
                // newRotation = math.mul(voxelRotationQuaternion, newRotation);
                newRotation = voxelRotationQuaternion;
            }
            // Rotation and Position
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelGizmo, new Translation { Value = newPosition });
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelGizmo, new Rotation { Value = newRotation });
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelGizmo, new NonUniformScale { Value = gizmoScale });
        }
    }
}