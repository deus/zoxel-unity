using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.UI;
using Zoxel.Voxels;

namespace Zoxel.VoxelInteraction
{
    //! Handles raycasting against grass.
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class RaycastMinivoxSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var disableRaycasting = false;
            if (disableRaycasting)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DeadEntity, DestroyEntity, SpawnPreviewMesh>()
                .WithAll<OnRaycastMinivox>()
                .ForEach((Entity e, int entityInQueryIndex, in GizmoLinks gizmoLinks) =>
            {
                PostUpdateCommands.RemoveComponent<OnRaycastMinivox>(entityInQueryIndex, e);
                var voxelGizmo = gizmoLinks.voxelGizmo;
                PostUpdateCommands.SetComponent(entityInQueryIndex, voxelGizmo, new Translation { Value = new float3(0, -1000, 0) });
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}