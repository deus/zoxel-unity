using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Rendering;
// todo: Every Time selecting a new Voxel - Spawn new Lines at that location
// todo: its 2-3ms atm - make parllel

namespace Zoxel.VoxelInteraction
{
    [UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class GizmoSetMeshSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((Entity e, ZoxMesh renderMesh, in SetGizmoMesh setGizmoMesh) =>    // ref NonUniformScale scale, 
            {
                PostUpdateCommands.RemoveComponent<SetGizmoMesh>(e);
                if (setGizmoMesh.type == 0 && renderMesh.mesh != GizmoSpawnSystem.quadMesh)
                {
                    renderMesh.mesh = GizmoSpawnSystem.quadMesh;
                    PostUpdateCommands.SetSharedComponentManaged(e, renderMesh);
                }
                else if (setGizmoMesh.type == 1 && renderMesh.mesh != GizmoSpawnSystem.cubeMesh)
                {
                    renderMesh.mesh = GizmoSpawnSystem.cubeMesh;
                    PostUpdateCommands.SetSharedComponentManaged(e, renderMesh);
                    // scale.Value = new float3(1, 1, 1);
                    // PostUpdateCommands.SetComponent(e, new NonUniformScale { Value = new float3(1, 1, 1) });
                }
            }).WithoutBurst().Run();
        }
    }
}