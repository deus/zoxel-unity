using Unity.Entities;
using Zoxel.Voxels;

namespace Zoxel.VoxelInteraction
{
    //[UpdateBefore(typeof(VoxelSystemGroup))]
    [UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxelInteractSystemGroup : ComponentSystemGroup { }
}