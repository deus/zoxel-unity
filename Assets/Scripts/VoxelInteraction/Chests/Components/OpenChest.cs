using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    public struct OpenChest : IComponentData
    {
        public byte state;
        public Entity chunk;
        public int3 voxelPosition;

        public OpenChest(Entity chunk, int3 voxelPosition)
        {
            this.state = 0;
            this.chunk = chunk;
            this.voxelPosition = voxelPosition;
        }
    }
}