using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    //! A tag for a chest Minivox.
    public struct Chest : IComponentData { }
}