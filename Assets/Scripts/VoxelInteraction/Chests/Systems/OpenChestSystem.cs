using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Voxels;
using Zoxel.Voxels.Minivoxes;
using Zoxel.Items;
using Zoxel.Items.UI;

namespace Zoxel.VoxelInteraction
{
    //! Opens a Voxel Chest.
    /**
    *   Removes game UIs. Opens Chest and Inventory uis.
    *   \todo Play sound when opening chest.
    *   \todo Animation when opening chest
    *   \todo sound when opening chest
    *   \todo slight post processing
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class OpenChestSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private NativeArray<Text> texts;
        public Entity chestUIPrefab;
        public Entity spawnChestUIPrefab;
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;
        private EntityQuery controllersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            texts = new NativeArray<Text>(1, Allocator.Persistent);
            texts[0] = new Text("Hit [cancelAction] to Close Chest.");
            var spawnInventoryUIArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnInventoryUI),
                typeof(CharacterLink),
                typeof(SpawnChestUI),
                typeof(InventoryLink),
                typeof(PrefabLink),
                typeof(UIAnchor),
                typeof(GenericEvent),
                typeof(DelayEvent));
            spawnChestUIPrefab = EntityManager.CreateEntity(spawnInventoryUIArchetype);
            EntityManager.SetComponentData(spawnChestUIPrefab, new UIAnchor(AnchorUIType.LeftMiddle));
            chunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<MinivoxLinks>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.Exclude<DestroyEntity>());
            controllersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Player>(),
                ComponentType.ReadOnly<PlayerTooltipLinks>());
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        public void InitializePrefabs()
        {
            ItemUISpawnSystem.InitializePrefabs(EntityManager);
            if (chestUIPrefab.Index == 0 && ItemUISpawnSystem.inventoryUIPrefab.Index != 0)
            {
                var uiDatam = UIManager.instance.uiDatam;
                var uiScale = UIManager.instance.uiSettings.uiScale;
                var iconStyle = uiDatam.iconStyle.GetStyle(uiScale);
                var padding = uiDatam.GetIconPadding(uiScale); // iconSize / 8f;
                var margins = uiDatam.GetIconMargins(uiScale); // iconSize / 6f;
                chestUIPrefab = EntityManager.Instantiate(ItemUISpawnSystem.inventoryUIPrefab);
                EntityManager.AddComponent<Prefab>(chestUIPrefab);
                EntityManager.AddComponent<ChestUI>(chestUIPrefab);
                EntityManager.RemoveComponent<InventoryUI>(chestUIPrefab);
                EntityManager.SetComponentData(chestUIPrefab, new GridUI(new int2(4, 4), padding, margins));
                EntityManager.SetComponentData(chestUIPrefab, new PanelUI(PanelType.ChestUI));
                EntityManager.SetComponentData(chestUIPrefab, new UIAnchor(AnchorUIType.LeftMiddle));
                EntityManager.SetComponentData(spawnChestUIPrefab, new PrefabLink(chestUIPrefab));
            }
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var spawnChestUIPrefab = this.spawnChestUIPrefab;
            var spawnInventoryUIPrefab = ItemUISpawnSystem.spawnInventoryUIPrefab;
            var spawnCharacterUIPrefab = UICoreSystem.spawnCharacterUIPrefab;
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            // todo: move this into class data
            var texts = this.texts;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var minivoxLinks2 = GetComponentLookup<MinivoxLinks>(true);
            chunkEntities.Dispose();
            var controllerEntities = controllersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var playerTooltipLinks2 = GetComponentLookup<PlayerTooltipLinks>(true);
            controllerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref OpenChest openChest, in UILink uiLink, in ControllerLink controllerLink) =>
            {
                if (openChest.state == 6)
                {
                    var minivoxLinks = minivoxLinks2[openChest.chunk];
                    var minivoxEntity = minivoxLinks.GetMinivox(openChest.voxelPosition);
                    // Open Inventory UI
                    var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnInventoryUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new CharacterLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new DelayEvent(elapsedTime, 0.05));
                    PostUpdateCommands.AddComponent(entityInQueryIndex, spawnEntity, new UIAnchor(AnchorUIType.RightMiddle));

                    var spawnCharacterUI2 = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnChestUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnCharacterUI2, new CharacterLink(e));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnCharacterUI2, new DelayEvent(elapsedTime, 0.12));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnCharacterUI2, new InventoryLink(minivoxEntity));
                }
                else if (openChest.state == 32)
                {
                    var playerTooltipLinks = playerTooltipLinks2[controllerLink.controller];
                    PostUpdateCommands.AddComponent(entityInQueryIndex, playerTooltipLinks.actionsTooltip, new SetTooltipText(texts[0].Clone()));
                    PostUpdateCommands.RemoveComponent<OpenChest>(entityInQueryIndex, e);
                }
                openChest.state++;
            })  .WithReadOnly(minivoxLinks2)
                .WithReadOnly(playerTooltipLinks2)
                .WithReadOnly(texts)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}