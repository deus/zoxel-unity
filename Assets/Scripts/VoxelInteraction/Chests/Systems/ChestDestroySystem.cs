/*using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
// using Zoxel.Voxels;
using Zoxel.Items;

namespace Zoxel.VoxelInteraction
{
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class ChestDestroySystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
			var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // when clearing chunk, wipe out minivox, as opposed to manually removing chest
            Dependency = Entities
                .WithNone<DeadEntity>()
                .WithAll<DestroyEntity, Chest>()
                .ForEach((Entity e, int entityInQueryIndex, in Inventory inventory) =>
			{
                inventory.Dispose();
                // PostUpdateCommands.RemoveComponent<Inventory>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}*/