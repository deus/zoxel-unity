using Unity.Entities;
using Unity.Burst;
using Zoxel.Items;

namespace Zoxel.VoxelInteraction
{
    //! Spawns a brand new chest inventory if the file does not exist.
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class ChestLoadFailedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            //! If loading fails, spawn a new inventory for the chest.
            var userItemPrefab = ItemSystemGroup.userItemPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<FailedLoadInventory, Chest>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                var inventoryLink = new InventoryLink(e);
                for (int i = 0; i < 16; i++)
                {
                    var userItemEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, userItemPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, new UserDataIndex(i));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, userItemEntity, inventoryLink);
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUserItemsSpawned((byte) 16));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}