using Unity.Entities;
using System.IO;
using Zoxel.Voxels;
using Zoxel.Items;

namespace Zoxel.VoxelInteraction
{
    //! Deletes chest inventory when it is destroyed.
    [UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class ChestDeleteSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            Entities
                .WithNone<LoadInventory, SaveInventory>()
                .WithAll<Chest, DeleteInventory>()
                .ForEach((Entity e, in Minivox minivox, in RealmLink realmLink) =>
            {
                PostUpdateCommands.RemoveComponent<DeleteInventory>(e);
                var realmID = EntityManager.GetComponentData<ZoxID>(realmLink.realm).id;
                var folderPath = SaveUtilities.GetRealmPath(realmID) + "Minivoxes/";
                if (Directory.Exists(folderPath))
                {
                    var filePath = folderPath + minivox.position.x + "_" + minivox.position.y + "_" + minivox.position.z + ".zox";
                    if (File.Exists(filePath))
                    {
                        // UnityEngine.Debug.LogError("Deleting inventory chest: " + filePath);
                        File.Delete(filePath);
                    }
                }
            }).WithoutBurst().Run();
        }
    }
}