using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using System;
using System.IO;
using Zoxel.Voxels;
using Zoxel.Items;

namespace Zoxel.VoxelInteraction
{
    //! Saves Chest Inventory to file.
    [UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class ChestSaveSystem : SystemBase
    {
        const string minivoxesFolderName = "Minivoxes/";
        const string filename = "_Inventory.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery userItemsQuery;
        private EntityQuery itemsQuery;


        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            userItemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<UserItem>(),
                ComponentType.Exclude<DestroyEntity>());
            itemsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Item>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }
        
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands.RemoveComponent<SaveInventory>(processQuery);
            var userItemEntities = userItemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var metaDatas = GetComponentLookup<MetaData>(true);
            var userItemQuantitys = GetComponentLookup<UserItemQuantity>(true);
            userItemEntities.Dispose();
            var itemEntities = itemsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var itemIDs = GetComponentLookup<ZoxID>(true);
            itemEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<LoadInventory, OnUserItemsSpawned>()
                .WithAll<SaveInventory, Chest>()
                .ForEach((in VoxelPosition voxelPosition, in RealmLink realmLink, in Inventory inventory) =>
            {
                var realmID = EntityManager.GetComponentData<ZoxID>(realmLink.realm).id;
                var folderPath = SaveUtilities.GetRealmPath(realmID) + minivoxesFolderName;
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                var filepath = folderPath + voxelPosition.position.x + "_" + voxelPosition.position.y + "_" + voxelPosition.position.z + filename;
                InventorySaveSystem.SaveInventory(filepath, in inventory, in metaDatas, in userItemQuantitys, in itemIDs);
            })  .WithReadOnly(metaDatas)
                .WithReadOnly(userItemQuantitys)
                .WithReadOnly(itemIDs)
                .WithoutBurst().Run();
        }
    }
}