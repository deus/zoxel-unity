using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;
using Zoxel.Items;

namespace Zoxel.VoxelInteraction
{
    //! Loads Chest Inventory from file.
    /**
    *   - Load Filename System -
    */
    [UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class ChestLoadSystem : SystemBase
    {
        const string minivoxesFolderName = "Minivoxes/";
        const string filename = "_Inventory.zox";
        const string underscore = "_";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
                ComponentType.ReadOnly<Realm>(),
                ComponentType.ReadOnly<ItemLinks>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        protected override void OnUpdate()
        {
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var realmIDs = GetComponentLookup<ZoxID>(true);
            realmEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, DeadEntity, InitializeEntity>()
                .WithNone<LoadUniqueItems, OnUniqueItemsSpawned>()
                .WithAll<Chest>()
                .ForEach((ref LoadInventory loadInventory, in VoxelPosition voxelPosition, in RealmLink realmLink) =>
            {
                if (loadInventory.loadPath.Length == 0)
                {
                    var realmID = realmIDs[realmLink.realm].id;
                    var filepath = SaveUtilities.GetRealmPath(realmID) + minivoxesFolderName + 
                        + voxelPosition.position.x + underscore + voxelPosition.position.y + underscore + voxelPosition.position.z + filename;
                    loadInventory.loadPath = new Text(filepath);
                    // UnityEngine.Debug.LogError("Chest path: " + filepath);
                }
                loadInventory.reader.UpdateOnMainThread(in loadInventory.loadPath);
            })  .WithReadOnly(realmIDs)
                .WithoutBurst().Run();
        }
    }
}