using Unity.Entities;
using Unity.Burst;
using Zoxel.Items;

namespace Zoxel.VoxelInteraction
{
    //! Handles when a Chest dies using the DyingEntity event by adding DropItems event.
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
	public partial class ChestDyingSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DyingEntity, Chest>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            { 
                PostUpdateCommands.AddComponent<DeleteInventory>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}