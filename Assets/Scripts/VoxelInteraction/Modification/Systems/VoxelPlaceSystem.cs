﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

using Zoxel.Voxels;
using Zoxel.Voxels.Minivoxes;

namespace Zoxel.VoxelInteraction
{
    //! Places a voxel
    /**
    *   Uses DEBUG_VOXEL_LIGHTS_ONLY to debug lights.
    *   Note: Leaf destroying needs more testing. Might have issues with floating leaves.
    *
    *   \todo Grass (Weeds) destruction to check which way it's rotated. When destroying solid - check if weeds is in surrounding.
    *
    *   Remove alot of fluff from here.
    *   Voxel Destruction Rules though - For Attached Voxels. If Destroyed is solid, check surrounding for AttachVoxels. Do this in another system with same events.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class VoxelPlaceSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity voxelSpawnPrefab;
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;
        private EntityQuery voxelQuery;
        private EntityQuery chunksQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(VoxelModification),
                typeof(SpawnVoxel));
            voxelSpawnPrefab = EntityManager.CreateEntity(archetype);
            processQuery = GetEntityQuery(ComponentType.ReadOnly<SpawnVoxel>());
            voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxelLinks>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<InitializeEntity>());
            voxelQuery = GetEntityQuery(
                ComponentType.ReadOnly<Voxel>(),
                ComponentType.Exclude<DestroyEntity>());
            chunksQuery = GetEntityQuery(
                ComponentType.ReadWrite<Chunk>(),
                ComponentType.ReadOnly<ChunkNeighbors>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (VoxelManager.instance.voxelSettings.disableVoxelPlacement)
            {
                return;
            }
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxelDestroyedPrefab = VoxelDestroyedSystem.voxelDestroyedPrefab;
            var spawnVoxelEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var spawnVoxels = GetComponentLookup<SpawnVoxel>(true);
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxelLinks2 = GetComponentLookup<VoxelLinks>(true);
            voxEntities.Dispose();
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var voxels = GetComponentLookup<Voxel>(true);
            var voxelMeshTypes = GetComponentLookup<VoxelMeshType>(true);
            voxelEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var chunks = GetComponentLookup<Chunk>(false);
            chunkEntities.Dispose();
            Dependency = Entities
                .WithNone<EntityBusy>()
                .ForEach((Entity e, int entityInQueryIndex, ref ChunkVoxelRotations chunkVoxelRotations, in VoxLink voxLink, in ChunkNeighbors chunkNeighbors, in ChunkPosition chunkPosition2, in MinivoxLinks minivoxLinks) =>
            {
                if (voxLink.vox.Index <= 0 || !voxelLinks2.HasComponent(voxLink.vox))
                {
                    return;
                }
                var chunk = chunks[e];
                var voxelDimensions = chunk.voxelDimensions;
                var voxelLinks = voxelLinks2[voxLink.vox];
                var didUpdate = false;
                var updatedChunkEntities = new NativeList<Entity>();
                var updatedPositions = new NativeList<int3>();
                for (int i = 0; i < spawnVoxelEntities.Length; i++)
                {
                    var e2 = spawnVoxelEntities[i];
                    var spawnVoxel = spawnVoxels[e2];
                    if (spawnVoxel.voxelIndex == 0)
                    {
                        continue;
                    }
                    // for spawning when I don't have Chunk Entity Reference
                    if (chunkPosition2.position != VoxelUtilities.GetChunkPosition(spawnVoxel.position, voxelDimensions))
                    {
                        continue;
                    }
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e2);
                    // get index out of globalVoxelPosition
                    var voxelIndex = spawnVoxel.voxelIndex;
                    var globalVoxelPosition = spawnVoxel.position;
                    var chunkPosition = VoxelUtilities.GetChunkPosition(globalVoxelPosition, voxelDimensions);
                    var localPosition = VoxelUtilities.GetLocalPosition(globalVoxelPosition, chunkPosition, voxelDimensions);
                    int voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
                    chunk = chunks[e];
                    var previousVoxel = chunk.voxels[voxelArrayIndex];
                    if (previousVoxel == voxelIndex)
                    {
                        continue;
                    }
                    chunk.voxels[voxelArrayIndex] = voxelIndex;
                    chunks[e] = chunk;
                    didUpdate = true;
                    updatedPositions.Add(localPosition);
                    // Placing Voxel
                    var voxelEntityPlaced = voxelLinks.voxels[(int)(voxelIndex) - 1];
                    var voxelPlaced = voxels[voxelEntityPlaced];
                    var meshType = voxelMeshTypes[voxelEntityPlaced].meshType;
                    if (meshType == MeshType.Minivox && spawnVoxel.voxelDirection != BlockRotation.Up)
                    {
                        #if DEBUG_VOXEL_ROTATION
                        UnityEngine.Debug.LogError("Placing with direction: " + spawnVoxel.voxelDirection + " at " + localPosition);
                        #endif
                        chunkVoxelRotations.AddRotation(localPosition, spawnVoxel.voxelDirection);
                    }
                }
                if (!didUpdate)
                {
                    updatedChunkEntities.Dispose();
                    updatedPositions.Dispose();
                    return;
                }
                var didUpdateChunkDown = false;
                var didUpdateChunkUp = false;
                var didUpdateChunkLeft = false;
                var didUpdateChunkRight = false;
                var didUpdateChunkBack = false;
                var didUpdateChunkForward = false;
                for (int i = 0; i < updatedPositions.Length; i++)
                {
                    var localPosition = updatedPositions[i];
                    didUpdateChunkDown = didUpdateChunkDown || localPosition.y == 0;
                    didUpdateChunkUp = didUpdateChunkUp || localPosition.y == voxelDimensions.y - 1;
                    didUpdateChunkLeft = didUpdateChunkLeft || localPosition.x == 0;
                    didUpdateChunkRight = didUpdateChunkRight || localPosition.x == voxelDimensions.x - 1;
                    didUpdateChunkBack = didUpdateChunkBack || localPosition.z == 0;
                    didUpdateChunkForward = didUpdateChunkForward || localPosition.z == voxelDimensions.z - 1;
                }
                chunks[e] = chunk;
                if (!updatedChunkEntities.Contains(e))
                {
                    updatedChunkEntities.Add(e);
                }
                // Get total chunks updated
                // get total chunks effected (using check if voxel position is on edge)
                // get total chunks light effected - only add if not in above groups
                // then push build events
                // can we check if any surrounding chunk lights are updated here? To make it faster.
                // Mark surrounding dirty only if floodfill of lights get updated.
                // also update surrounding chunks, if multiple voxels update, what happens? Do they get told to update several times AddComponent?
                // check if within 8 length of the chunks surrounding
                for (int i = 0; i < updatedChunkEntities.Length; i++)
                {
                    var e2 = updatedChunkEntities[i];
                    if (!HasComponent<SaveChunk>(e2))
                    {
                        PostUpdateCommands.AddComponent<SaveChunk>(entityInQueryIndex, e2);
                    }
                    if (!HasComponent<EntityBusy>(e2))
                    {
                        AddChunkBuilder(PostUpdateCommands, entityInQueryIndex, e2);
                        /*if (HasComponent<VoxelBuilderLightsOnly>(e2))
                        {
                            #if DEBUG_VOXEL_LIGHTS_ONLY
                            UnityEngine.Debug.LogError("Chunk still has VoxelBuilderLightsOnly: " + e2.Index);
                            #endif
                            PostUpdateCommands.RemoveComponent<VoxelBuilderLightsOnly>(entityInQueryIndex, e2);
                        }*/
                    }
                }
                var chunksUpdated = new NativeList<Entity>();
                if (didUpdateChunkDown)
                {
                    chunksUpdated.Add(chunkNeighbors.chunkDown);
                    AddChunkBuilder(PostUpdateCommands, entityInQueryIndex, chunkNeighbors.chunkDown);
                }
                if (didUpdateChunkUp)
                {
                    chunksUpdated.Add(chunkNeighbors.chunkUp);
                    AddChunkBuilder(PostUpdateCommands, entityInQueryIndex, chunkNeighbors.chunkUp);
                }
                if (didUpdateChunkLeft)
                {
                    chunksUpdated.Add(chunkNeighbors.chunkLeft);
                    AddChunkBuilder(PostUpdateCommands, entityInQueryIndex, chunkNeighbors.chunkLeft);
                }
                if (didUpdateChunkRight)
                {
                    chunksUpdated.Add(chunkNeighbors.chunkRight);
                    AddChunkBuilder(PostUpdateCommands, entityInQueryIndex, chunkNeighbors.chunkRight);
                }
                if (didUpdateChunkBack)
                {
                    chunksUpdated.Add(chunkNeighbors.chunkBack);
                    AddChunkBuilder(PostUpdateCommands, entityInQueryIndex, chunkNeighbors.chunkBack);
                }
                if (didUpdateChunkForward)
                {
                    chunksUpdated.Add(chunkNeighbors.chunkForward);
                    AddChunkBuilder(PostUpdateCommands, entityInQueryIndex, chunkNeighbors.chunkForward);
                }
                var neighbors = chunkNeighbors.GetNeighbors();
                for (int i = 0; i < neighbors.Length; i++)
                {
                    var chunkEntity = neighbors[i];
                    if (!chunksUpdated.Contains(chunkEntity) && !updatedChunkEntities.Contains(chunkEntity))
                    {
                        AddLightBuilder(PostUpdateCommands, entityInQueryIndex, chunkEntity);
                    }
                }
                neighbors.Dispose();
                updatedChunkEntities.Dispose();
                updatedPositions.Dispose();
                chunksUpdated.Dispose();
            })  .WithReadOnly(spawnVoxelEntities)
                .WithDisposeOnCompletion(spawnVoxelEntities)
                .WithReadOnly(spawnVoxels)
                .WithReadOnly(voxelLinks2)
                .WithReadOnly(voxels)
                .WithReadOnly(voxelMeshTypes)
                .WithReadOnly(chunks)
                .WithNativeDisableContainerSafetyRestriction(chunks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static void AddChunkBuilder(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity chunk)
        {
            PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, chunk);
            PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, chunk);
        }

        private static void AddLightBuilder(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity chunk)
        {
            PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, chunk);
            PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, chunk);
            PostUpdateCommands.AddComponent<VoxelBuilderLightsOnly>(entityInQueryIndex, chunk);
        }

        // For Tree, if a surrounding voxel is leaf, go through all leaves as well, and c heck there
        public static bool HasVoxelNeighbor(in Chunk chunk, int3 chunkPosition, byte voxelID, int3 position, int maxLength)
        {
            var leafID = (byte)(voxelID + 1);
            var positionsChecked = new NativeList<int3>();
            var distancesChecked = new NativeList<int>();
            if (HasVoxelNeighbor(ref positionsChecked, ref distancesChecked, maxLength, in chunk, chunkPosition, voxelID, leafID, position.Up()))
            {
                return true;
            }
            else if (HasVoxelNeighbor(ref positionsChecked, ref distancesChecked, maxLength, in chunk, chunkPosition, voxelID, leafID, position.Down()))
            {
                return true;
            }
            else if (HasVoxelNeighbor(ref positionsChecked, ref distancesChecked, maxLength, in chunk, chunkPosition, voxelID, leafID, position.Left()))
            {
                return true;
            }
            else if (HasVoxelNeighbor(ref positionsChecked, ref distancesChecked, maxLength, in chunk, chunkPosition, voxelID, leafID, position.Right()))
            {
                return true;
            }
            else if (HasVoxelNeighbor(ref positionsChecked, ref distancesChecked, maxLength, in chunk, chunkPosition, voxelID, leafID, position.Backward()))
            {
                return true;
            }
            else if (HasVoxelNeighbor(ref positionsChecked, ref distancesChecked, maxLength, in chunk, chunkPosition, voxelID, leafID, position.Forward()))
            {
                return true;
            }
            return false;
        }

        // Check if any wood is connected still on tree
        public static bool HasVoxelNeighbor(ref NativeList<int3> positionsChecked, ref NativeList<int> distancesChecked, int maxLength, in Chunk chunk, int3 chunkPosition,
            byte voxelID, byte leafID, int3 position, int count = 0)
        {
            var localPosition = VoxelUtilities.GetLocalPosition(position, chunkPosition, chunk.voxelDimensions);
			if (VoxelUtilities.InBounds(localPosition, chunk.voxelDimensions))
			{
                //positionsChecked.Add(position);
                if (!positionsChecked.Contains(position))
                {
                    positionsChecked.Add(position);
                    distancesChecked.Add(count);
                }
                else // if (positionsChecked.Contains(position))
                {
                    var indexo = positionsChecked.IndexOf(position);
                    if (count <= distancesChecked[indexo])
                    {
                        distancesChecked[indexo] = count;
                    }
                    else
                    {
                        return false;
                    }
                }
                count++;
				var voxelIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, chunk.voxelDimensions);
                var foundVoxelID = chunk.voxels[voxelIndex];
				if (foundVoxelID == voxelID)    // found wood
				{
                    return true;
                }
                else if (count > maxLength)
                {
                    return false;
                }
                else if (foundVoxelID == leafID)
                {
                    if (HasVoxelNeighbor(ref positionsChecked, ref distancesChecked, maxLength, in chunk, chunkPosition, voxelID, leafID, position.Up(), count))
                    {
                        return true;
                    }
                    else if (HasVoxelNeighbor(ref positionsChecked, ref distancesChecked, maxLength, in chunk, chunkPosition, voxelID, leafID, position.Down(), count))
                    {
                        return true;
                    }
                    else if (HasVoxelNeighbor(ref positionsChecked, ref distancesChecked, maxLength, in chunk, chunkPosition, voxelID, leafID, position.Left(), count))
                    {
                        return true;
                    }
                    else if (HasVoxelNeighbor(ref positionsChecked, ref distancesChecked, maxLength, in chunk, chunkPosition, voxelID, leafID, position.Right(), count))
                    {
                        return true;
                    }
                    else if (HasVoxelNeighbor(ref positionsChecked, ref distancesChecked, maxLength, in chunk, chunkPosition, voxelID, leafID, position.Backward(), count))
                    {
                        return true;
                    }
                    else if (HasVoxelNeighbor(ref positionsChecked, ref distancesChecked, maxLength, in chunk, chunkPosition, voxelID, leafID, position.Forward(), count))
                    {
                        return true;
                    }
                }
			}
            return false;
        }

        public static void SpawnVoxel(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity voxelSpawnPrefab,
            Entity planet, int voxelID2, float3 position2, byte voxelDirection = 0)
        {
            var voxelID = (byte)voxelID2;
            var position = new int3((int)math.floor(position2.x), (int)math.floor(position2.y), (int)math.floor(position2.z));
            PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, voxelSpawnPrefab),
                new SpawnVoxel(planet, position, voxelID, voxelDirection));
        }

        public static void SpawnVoxel(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity voxelSpawnPrefab,
            Entity planet, int voxelID2, int3 position, byte voxelDirection = 0)
        {
            var voxelID = (byte)voxelID2;
            PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, voxelSpawnPrefab),
                new SpawnVoxel(planet, position, voxelID, voxelDirection));
        }
    }
}

// Weeds
// todo: First get weeds rotation
// todo: Is this voxel solid?
// check  voxel above it
// if voxels[previousVoxel].type == VoxelType.Weeds) - Destroy voxel above this one


//! \todo Add support for N sized BigMinivoxVoxel's. First checks if all spots are air before placing.
/*if (voxelPlaced.blockDimensions.y == 2)
{
    var chunkEntity = e;
    var placePosition = localPosition.Up();
    if (!VoxelUtilities.InBounds(placePosition, voxelDimensions))
    {
        int3 otherVoxelPosition;
        chunkEntity = chunkNeighbors.GetChunkAdjacent(placePosition, voxelDimensions, out otherVoxelPosition);
        if (chunkEntity.Index == 0 || !chunks.HasComponent(chunkEntity))
        {
            continue;
        }
        placePosition = otherVoxelPosition;
    }
    // place one above too, this is for door models
    if (VoxelUtilities.InBounds(placePosition, voxelDimensions))
    {
        var chunkUp = chunks[chunkEntity];
        int voxelArrayIndexUp = VoxelUtilities.GetVoxelArrayIndex(placePosition, voxelDimensions);
        chunkUp.voxels[voxelArrayIndexUp] = voxelIndex;
        chunks[chunkEntity] = chunkUp;
        if (!updatedChunkEntities.Contains(chunkEntity))
        {
            updatedChunkEntities.Add(chunkEntity);
        }
    }
}*/
// place one above too
/*var spawnLocalPositionUp = VoxelUtilities.GetLocalPosition(globalVoxelPosition.Up(), chunkPosition, voxelDimensions);
int voxelIndexUp = VoxelUtilities.GetVoxelArrayIndex(spawnLocalPositionUp, voxelDimensions);
var voxelTypeUp = chunk.voxels[voxelIndexUp];
if (voxelTypeUp == previousVoxel)
{
    // chunk.voxels[voxelIndexUp] = 0;
    VoxelDestroyedSystem.OnVoxelDestroyed(PostUpdateCommands, entityInQueryIndex, voxelDestroyedPrefab,
        voxLink.vox, e, previousVoxel, globalVoxelPosition);
}
else
{
    var spawnLocalPositionDown = VoxelUtilities.GetLocalPosition(globalVoxelPosition.Down(), chunkPosition, voxelDimensions);
    int voxelIndexDown = VoxelUtilities.GetVoxelArrayIndex(spawnLocalPositionDown, voxelDimensions);
    var voxelTypeDown = chunk.voxels[voxelIndexDown];
    if (voxelTypeDown == previousVoxel)
    {
        chunk.voxels[voxelIndexDown] = 0;
        VoxelDestroyedSystem.OnVoxelDestroyed(PostUpdateCommands, entityInQueryIndex, voxelDestroyedPrefab,
            voxLink.vox, e, previousVoxel, globalVoxelPosition.Down());
    }
}*/

// if BigMinivoxVoxel
/*if (voxelRemoved.blockDimensions.y == 2)
{
    VoxelDestroyedSystem.OnVoxelDestroyed(PostUpdateCommands, entityInQueryIndex, voxelDestroyedPrefab,
        voxLink.vox, e, previousVoxel, globalVoxelPosition);
    var chunkEntity = e;
    var bigMinivoxLink = minivoxLinks.GetBigMinivoxLink(globalVoxelPosition);
    if (bigMinivoxLink.positions.Length == 0)
    {
        //UnityEngine.Debug.LogError("Failed to find BigMinivox at: " + globalVoxelPosition
        //    + " - " + minivoxLinks.bigMinivoxes.Length);
        continue;
    }
    var placePosition = localPosition.Up();
    if (bigMinivoxLink.positions[1] == globalVoxelPosition)
    {
        placePosition = localPosition.Down();
    }
    if (!VoxelUtilities.InBounds(placePosition, voxelDimensions))
    {
        int3 otherVoxelPosition;
        chunkEntity = chunkNeighbors.GetChunkAdjacent(placePosition, voxelDimensions, out otherVoxelPosition);
        if (chunkEntity.Index == 0 || !chunks.HasComponent(chunkEntity))
        {
            continue;
        }
        placePosition = otherVoxelPosition;
    }
    // place one above too, this is for door models
    if (VoxelUtilities.InBounds(placePosition, voxelDimensions))
    {
        //VoxelDestroyedSystem.OnVoxelDestroyed(PostUpdateCommands, entityInQueryIndex, voxelDestroyedPrefab,
        //    voxLink.vox, e, previousVoxel, globalVoxelPosition);
        var chunkUp = chunks[chunkEntity];
        int voxelArrayIndexUp = VoxelUtilities.GetVoxelArrayIndex(placePosition, voxelDimensions);
        chunkUp.voxels[voxelArrayIndexUp] = 0;
        chunks[chunkEntity] = chunkUp;
        if (!updatedChunkEntities.Contains(chunkEntity))
        {
            updatedChunkEntities.Add(chunkEntity);
        }
    }
}
else
{
}*/
                /*for (int i = 0; i < chunksUpdated.Length; i++)
                {
                    if (HasComponent<VoxelBuilderLightsOnly>(chunksUpdated[i]))
                    {
                        #if DEBUG_VOXEL_LIGHTS_ONLY
                        UnityEngine.Debug.LogError("Chunk still has VoxelBuilderLightsOnly: " + chunksUpdated[i].Index);
                        #endif
                        PostUpdateCommands.RemoveComponent<VoxelBuilderLightsOnly>(entityInQueryIndex, chunksUpdated[i]);
                    }
                }*/
        
        /*
        // For Tree, if a surrounding voxel is leaf, go through all leaves as well, and c heck there
        // oh i get it..
        // so take all the possible expanding points
        // add them to the list
        // then take them all again
        // and add them
        // until all new points cannot grow anymore
        public static NativeList<int3> GetSurroundingVoxels(in Chunk chunk, int3 chunkPosition, byte leafID, int3 position, int maxLength)
        {
            var closedPoints = new NativeList<int3>();
            var openPoints = new NativeList<int3>();
            // get all new points
            // check  if new points are leaves
            // if they are leaves, get all new points again
            // otherwise dont add them
            AddSurroundingPoints(ref closedPoints, ref openPoints, position);
            // while open points exist]
            int count = 0;
            while (openPoints.Length > 0)
            {
                // ad any open nodes to found ones, if they are leaf ones
                for (int i = 0; i < openPoints.Length; i++)
                {
                    var openPosition = openPoints[i];
                    var localPosition = VoxelUtilities.GetLocalPosition(openPosition, chunkPosition, chunk.voxelDimensions);
                    if (VoxelUtilities.InBounds(localPosition, chunk.voxelDimensions))
                    {
                        var voxelIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, chunk.voxelDimensions);
                        var foundVoxelID = chunk.voxels[voxelIndex];
                        if (foundVoxelID == leafID)    // found wood
                        {
                            closedPoints.Add(openPosition);
                        }
                    }
                }
                for (int i = openPoints.Length - 1; i >= 0; i--)
                {
                    var newPoint = openPoints[i];
                    openPoints.RemoveAt(i);
                    AddSurroundingPoints(ref closedPoints, ref openPoints, newPoint);
                }
                count++;
                if (count >= maxLength)
                {
                    break;
                }
            }
            openPoints.Dispose();
            return closedPoints;
        }

        public static void AddSurroundingPoints(ref NativeList<int3> closedPoints, ref NativeList<int3> openPoints, int3 position)
        {
            AddPoint(ref closedPoints, ref openPoints, position.Up());
            AddPoint(ref closedPoints, ref openPoints, position.Down());
            AddPoint(ref closedPoints, ref openPoints, position.Left());
            AddPoint(ref closedPoints, ref openPoints, position.Right());
            AddPoint(ref closedPoints, ref openPoints, position.Backward());
            AddPoint(ref closedPoints, ref openPoints, position.Forward());
        }

        public static void AddPoint(ref NativeList<int3> closedPoints, ref NativeList<int3> openPoints, int3 newPosition)
        {
            if (!closedPoints.Contains(newPosition) && !openPoints.Contains(newPosition))
            {
                openPoints.Add(newPosition);
            }
        }

        // Check if any wood is connected still on tree
        // can still mess up in getting neighbors... ef NativeList<int> distancesChecked, 
        public static void GetSurroundingVoxels(ref NativeList<int3> leafBlocks, in Chunk chunk, int3 chunkPosition, byte leafID, int3 position, int maxLength, int count = 0)
        {
            if (leafBlocks.Contains(position))
            {
                return;
            }
            var localPosition = VoxelUtilities.GetLocalPosition(position, chunkPosition, chunk.voxelDimensions);
			if (VoxelUtilities.InBounds(localPosition, chunk.voxelDimensions))
			{
				var voxelIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, chunk.voxelDimensions);
                var foundVoxelID = chunk.voxels[voxelIndex];
				if (foundVoxelID == leafID)    // found wood
				{
                    leafBlocks.Add(position);
                    count++;
                    if (count > maxLength)
                    {
                        return;
                    }
                    GetSurroundingVoxels(ref leafBlocks, in chunk, chunkPosition, leafID, position.Up(), maxLength, count);
                    GetSurroundingVoxels(ref leafBlocks, in chunk, chunkPosition, leafID, position.Down(), maxLength, count);
                    GetSurroundingVoxels(ref leafBlocks, in chunk, chunkPosition, leafID, position.Left(), maxLength, count);
                    GetSurroundingVoxels(ref leafBlocks, in chunk, chunkPosition, leafID, position.Right(), maxLength, count);
                    GetSurroundingVoxels(ref leafBlocks, in chunk, chunkPosition, leafID, position.Backward(), maxLength, count);
                    GetSurroundingVoxels(ref leafBlocks, in chunk, chunkPosition, leafID, position.Forward(), maxLength, count);
                }
			}
        }*/