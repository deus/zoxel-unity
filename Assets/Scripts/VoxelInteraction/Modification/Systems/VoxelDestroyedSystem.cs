using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;
using Zoxel.Audio;
using Zoxel.Items;
using Zoxel.Items.Voxels;

namespace Zoxel.VoxelInteraction
{
    //! Handles destruction of a voxel events
    /**
    *   VoxelDamage triggers VoxelPlace to set to air, then VoxelPlaceSytem triggers this system to drop the items from a voxel.
    *   Spawn item once chunk has finished updating.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class VoxelDestroyedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery voxesQuery;
        private EntityQuery voxelQuery;
        private EntityQuery voxelDestroyedQuery;
        private EntityQuery voxelHealthsQuery;
        private EntityQuery chunksQuery;
        public static Entity voxelDestroyedPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var voxelDestroyedArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(OnVoxelDestroyed));
            voxelDestroyedPrefab = EntityManager.CreateEntity(voxelDestroyedArchetype);
            // for now, can only happen with maps
            voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<Vox>());
            voxelQuery = GetEntityQuery(
                ComponentType.ReadOnly<Voxel>(),
                ComponentType.ReadOnly<ItemLink>());
            voxelDestroyedQuery = GetEntityQuery(ComponentType.ReadOnly<OnVoxelDestroyed>());
            voxelHealthsQuery = GetEntityQuery(
                ComponentType.ReadOnly<VoxelHealth>(),
                ComponentType.Exclude<VoxelHealthDying>());
            chunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkGravity>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (voxelDestroyedQuery.IsEmpty)
            {
                return;
            }
            var elapsedTime = World.Time.ElapsedTime;
            var spawnItemPrefab = WorldItemSpawnSystem.spawnItemPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxelLinks2 = GetComponentLookup<VoxelLinks>(true);
            var voxScales = GetComponentLookup<VoxScale>(true);
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxels = GetComponentLookup<Voxel>(true);
            var itemLinks = GetComponentLookup<ItemLink>(true);
            var voxelHealthEntities = voxelHealthsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var voxelHealths = GetComponentLookup<VoxelHealth>(true);
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var chunkGravityQuadrants = GetComponentLookup<ChunkGravity>(true);
            voxEntities.Dispose();
            voxelEntities.Dispose();
            chunkEntities.Dispose();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in OnVoxelDestroyed onVoxelDestroyed) =>
            {
                var voxelLinks = voxelLinks2[onVoxelDestroyed.vox];
                if (onVoxelDestroyed.voxelID == 0)
                {
                    return;
                }
                var voxelTypeMinusAir = onVoxelDestroyed.voxelID - 1;
                if (voxelTypeMinusAir >= voxelLinks.voxels.Length)
                {
                    //UnityEngine.Debug.LogError("Voxel Type outside of voxels array: " + voxelType + "::" + voxelLinks.voxels.Length);
                    return;
                }
                var voxelEntity = voxelLinks.voxels[voxelTypeMinusAir];
                if (!HasComponent<Voxel>(voxelEntity))
                {
                    //UnityEngine.Debug.LogError("Voxel Entity: " + voxelType + " does not exist.");
                    return;
                }
                // just incase - this is for things like half destroyed grass that gets destroyed by dirt being destroyed
                for (int i = 0; i < voxelHealthEntities.Length; i++)
                {
                    var e2 = voxelHealthEntities[i];
                    var voxelHealth = voxelHealths[e2];
                    if (voxelHealth.HasPosition(onVoxelDestroyed.position))
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e2);
                    }
                }
                // Destroys Voxel Health - no need to destroy mesh and material - it's all instanced
                PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                //UnityEngine.Debug.LogError("VOXEL WAS DESTROYED");
                var voxel = voxels[voxelEntity];
                var voxelID = onVoxelDestroyed.voxelID;
                // grass to drop dirt!
                if (voxel.type == (byte) VoxelType.Grass)
                {
                    voxelID--;
                }
                else if (voxel.type == (byte) VoxelType.Weeds || voxel.type == (byte) VoxelType.Water)
                {
                    return;
                }
                else if (voxel.type == (byte) VoxelType.Leaf)
                {
                    var random = new Random();
                    random.InitState((uint)(1 + 100 * elapsedTime * onVoxelDestroyed.position.GetHashCode()));  // 10 * elapsedTime + 
                    if (random.NextInt(100) < 88)
                    {
                        return;
                    }
                }
                /*else if (voxel.type == (byte) VoxelType.Wood)
                {
                    // find all leaf blocks - that are within the tree (traveling accross wood and leaf blocks)
                    // remove them
                    // create fading leaf blocks in there position
                    // todo after: make these events set on a timer instead
                }*/
                var chunkGravity = chunkGravityQuadrants[onVoxelDestroyed.chunk];
                // var spawnRotation = chunkGravity.GetRotation();
                var gravityIn = chunkGravity.gravity.GetValueDepthDifference(onVoxelDestroyed.position.ToByte3(), 4, chunkGravity.depth);
                var spawnRotation = BlockRotation.GetRotation((byte) (gravityIn - 1));  // PlanetSide -> BlockRotation - removes None type
                // Drop Item!
                var voxScale = voxScales[onVoxelDestroyed.vox].scale;
                var spawnPosition = onVoxelDestroyed.position.ToFloat3();
                spawnPosition.x *= voxScale.x;
                spawnPosition.y *= voxScale.y;
                spawnPosition.z *= voxScale.z;
                spawnPosition += voxScale / 2f;
                // UnityEngine.Debug.LogError("DROPPED ITEM");
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, spawnItemPrefab),
                    new SpawnWorldItem
                    {
                        quantity = 1,
                        item = itemLinks[voxelEntity].item,
                        spawnPosition = spawnPosition,
                        spawnRotation = spawnRotation,
                        vox = onVoxelDestroyed.vox
                    });
            })  .WithReadOnly(voxelLinks2)
                .WithReadOnly(voxScales)
                .WithReadOnly(voxels)
                .WithReadOnly(itemLinks)
                .WithReadOnly(voxelHealthEntities)
                .WithDisposeOnCompletion(voxelHealthEntities)
                .WithReadOnly(voxelHealths)
                .WithReadOnly(chunkGravityQuadrants)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static void OnVoxelDestroyed(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity spawnItemPrefab,
            Entity worldEntity, Entity chunkEntity, byte voxelID, int3 position)
        {
            PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, spawnItemPrefab),
                new OnVoxelDestroyed(voxelID, position, worldEntity, chunkEntity));
        }
    }
}