using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;
using Zoxel.Voxels.Minivoxes;

// Group all erase events
//      Perform all voxel data changes
//      Including Erasing for any attached voxels (grass).
//      Leaves are attached to the closest wood, if no wood, it gets removed.
//      Create ChunkUpdated Event Entities? Perhaps I cannot keep adding updated to chunk events.
//      And Adds EntityBusy onto chunks so it can start updating.

namespace Zoxel.VoxelInteraction
{
    //! Removes a Voxel.
    /**
    *   ForEach RemoveVoxel - 
    *   \todo The leaf destruction in another system
    *   \todo The torch or grass destruction in another system
    *   \todo When dirt removed on planet side - check rotation of grass on side blocks - to see if its attached to the block removed
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class VoxelEraseSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity voxelErasePrefab;
        private EntityQuery eraseVoxelQuery;
        private EntityQuery voxesQuery;
        private EntityQuery voxelQuery;
        private EntityQuery chunksQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(VoxelModification),
                typeof(EraseVoxel));
            voxelErasePrefab = EntityManager.CreateEntity(archetype);
            eraseVoxelQuery = GetEntityQuery(ComponentType.ReadOnly<EraseVoxel>());
            voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxelLinks>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<InitializeEntity>());
            voxelQuery = GetEntityQuery(
                ComponentType.ReadOnly<Voxel>(),
                ComponentType.Exclude<DestroyEntity>());
            chunksQuery = GetEntityQuery(
                ComponentType.ReadWrite<Chunk>(),
                ComponentType.ReadOnly<ChunkNeighbors>(),
                ComponentType.Exclude<DestroyEntity>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (VoxelManager.instance == null) return;
            if (VoxelManager.instance.voxelSettings.disableVoxelPlacement || eraseVoxelQuery.IsEmpty)
            {
                return;
            }
            var voxelErasePrefab = VoxelEraseSystem.voxelErasePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxelDestroyedPrefab = VoxelDestroyedSystem.voxelDestroyedPrefab;
            var eraseVoxelEntities = eraseVoxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var eraseVoxels = GetComponentLookup<EraseVoxel>(true);
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxelLinks2 = GetComponentLookup<VoxelLinks>(true);
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var voxels = GetComponentLookup<Voxel>(true);
            var voxelMeshTypes = GetComponentLookup<VoxelMeshType>(true);
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var chunks = GetComponentLookup<Chunk>(true);
            var chunkVoxelRotations2 = GetComponentLookup<ChunkVoxelRotations>(true);
            voxEntities.Dispose();
            voxelEntities.Dispose();
            chunkEntities.Dispose();
            Dependency = Entities
                .WithNone<EntityBusy>()
                .ForEach((Entity e, int entityInQueryIndex, ref ChunkVoxelRotations chunkVoxelRotations, ref Chunk chunk, in VoxLink voxLink, in ChunkNeighbors chunkNeighbors, in ChunkPosition chunkPosition2, in MinivoxLinks minivoxLinks) =>
            {
                if (voxLink.vox.Index <= 0 || !voxelLinks2.HasComponent(voxLink.vox))
                {
                    return;
                }
                // var chunk = chunks[e];
                var voxelDimensions = chunk.voxelDimensions;
                var voxelLinks = voxelLinks2[voxLink.vox];
                var updateEntities = new NativeList<Entity>();
                var updatePositions = new NativeList<int3>();
                for (int i = 0; i < eraseVoxelEntities.Length; i++)
                {
                    var e2 = eraseVoxelEntities[i];
                    var eraseVoxel = eraseVoxels[e2];
                    if (chunkPosition2.position == VoxelUtilities.GetChunkPosition(eraseVoxel.position, voxelDimensions))
                    {
                        updateEntities.Add(e2);
                        updatePositions.Add(eraseVoxel.position);
                    }
                }
                if (updateEntities.Length == 0)
                {
                    updateEntities.Dispose();
                    updatePositions.Dispose();
                    return;
                }
                var updatedChunkEntities = new NativeList<Entity>();
                var updatedPositions = new NativeList<int3>();
				var chunkAdjacentNeighborsData = new ChunkAdjacentNeighborsData(in chunkNeighbors, in chunks);
				var chunkAdjacentVoxelRotations = new ChunkAdjacentVoxelRotations(in chunkNeighbors, in chunkVoxelRotations2);
                var didUpdate = false;
                for (int i = 0; i < updateEntities.Length; i++)
                {
                    var e2 = updateEntities[i];
                    var globalVoxelPosition = updatePositions[i];
                    // for spawning when I don't have Chunk Entity Reference
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e2);
                    // erase voxel at position:
                    var chunkPosition = VoxelUtilities.GetChunkPosition(globalVoxelPosition, voxelDimensions);
                    var localPosition = VoxelUtilities.GetLocalPosition(globalVoxelPosition, chunkPosition, voxelDimensions);
                    int voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
                    // chunk = chunks[e];
                    var previousVoxel = chunk.voxels[voxelArrayIndex];
                    if (previousVoxel == 0)
                    {
                        continue;
                    }
                    // for this kind of item spawning, soilGrass voxel must drop soil,
                    // todo: store item to drop inside voxels with chance etc
                    var erasedIndex = (int)(previousVoxel) - 1;
                    var voxelEntity = voxelLinks.voxels[erasedIndex];
                    var voxelRemoved = voxels[voxelEntity];
                    var meshType = voxelMeshTypes[voxelEntity].meshType;
                    if (meshType == MeshType.Block)
                    {
                        // get surrounding voxel entities
                        // check if any have VoxelAttachToSolid
                        // if they do, check rotation, and see if it's facing the solid
                        var voxelTypeDown = chunkAdjacentNeighborsData.GetVoxelValue(in chunk, localPosition.Down(), voxelDimensions);
                        var voxelTypeUp = chunkAdjacentNeighborsData.GetVoxelValue(in chunk, localPosition.Up(), voxelDimensions);
                        var voxelTypeLeft = chunkAdjacentNeighborsData.GetVoxelValue(in chunk, localPosition.Left(), voxelDimensions);
                        var voxelTypeRight = chunkAdjacentNeighborsData.GetVoxelValue(in chunk, localPosition.Right(), voxelDimensions);
                        var voxelTypeBackward = chunkAdjacentNeighborsData.GetVoxelValue(in chunk, localPosition.Backward(), voxelDimensions);
                        var voxelTypeForward = chunkAdjacentNeighborsData.GetVoxelValue(in chunk, localPosition.Forward(), voxelDimensions);

                        var voxelEntityDown = voxelLinks.voxels[voxelTypeDown - 1];
                        if (HasComponent<VoxelAttachToSolid>(voxelEntityDown))
                        {
                            var voxelRotationDown = chunkAdjacentVoxelRotations.GetRotation(in chunkVoxelRotations, localPosition.Down(), voxelDimensions);
                            if (BlockRotation.GetAxis(voxelRotationDown) == PlanetSide.Down)
                            {
                                EraseVoxel(PostUpdateCommands, entityInQueryIndex, voxelErasePrefab, voxLink.vox, globalVoxelPosition.Down(), int3.zero);
                            }
                        }
                        var voxelEntityUp = voxelLinks.voxels[voxelTypeUp - 1];
                        if (HasComponent<VoxelAttachToSolid>(voxelEntityUp))
                        {
                            var voxelRotationUp = chunkAdjacentVoxelRotations.GetRotation(in chunkVoxelRotations, localPosition.Up(), voxelDimensions);
                            if (BlockRotation.GetAxis(voxelRotationUp) == PlanetSide.Up)
                            {
                                EraseVoxel(PostUpdateCommands, entityInQueryIndex, voxelErasePrefab, voxLink.vox, globalVoxelPosition.Up(), int3.zero);
                            }
                        }
                        var voxelEntityLeft = voxelLinks.voxels[voxelTypeLeft - 1];
                        if (HasComponent<VoxelAttachToSolid>(voxelEntityLeft))
                        {
                            var voxelRotationLeft = chunkAdjacentVoxelRotations.GetRotation(in chunkVoxelRotations, localPosition.Left(), voxelDimensions);
                            if (BlockRotation.GetAxis(voxelRotationLeft) == PlanetSide.Left)
                            {
                                EraseVoxel(PostUpdateCommands, entityInQueryIndex, voxelErasePrefab, voxLink.vox, globalVoxelPosition.Left(), int3.zero);
                            }
                        }
                        var voxelEntityRight = voxelLinks.voxels[voxelTypeRight - 1];
                        if (HasComponent<VoxelAttachToSolid>(voxelEntityRight))
                        {
                            var voxelRotationRight = chunkAdjacentVoxelRotations.GetRotation(in chunkVoxelRotations, localPosition.Right(), voxelDimensions);
                            if (BlockRotation.GetAxis(voxelRotationRight) == PlanetSide.Right)
                            {
                                EraseVoxel(PostUpdateCommands, entityInQueryIndex, voxelErasePrefab, voxLink.vox, globalVoxelPosition.Right(), int3.zero);
                            }
                        }
                        var voxelEntityBackward = voxelLinks.voxels[voxelTypeBackward - 1];
                        if (HasComponent<VoxelAttachToSolid>(voxelEntityBackward))
                        {
                            var voxelRotationBackward = chunkAdjacentVoxelRotations.GetRotation(in chunkVoxelRotations, localPosition.Backward(), voxelDimensions);
                            if (BlockRotation.GetAxis(voxelRotationBackward) == PlanetSide.Backward)
                            {
                                EraseVoxel(PostUpdateCommands, entityInQueryIndex, voxelErasePrefab, voxLink.vox, globalVoxelPosition.Backward(), int3.zero);
                            }
                        }
                        var voxelEntityForward = voxelLinks.voxels[voxelTypeForward - 1];
                        if (HasComponent<VoxelAttachToSolid>(voxelEntityForward))
                        {
                            var voxelRotationForward = chunkAdjacentVoxelRotations.GetRotation(in chunkVoxelRotations, localPosition.Forward(), voxelDimensions);
                            if (BlockRotation.GetAxis(voxelRotationForward) == PlanetSide.Forward)
                            {
                                EraseVoxel(PostUpdateCommands, entityInQueryIndex, voxelErasePrefab, voxLink.vox, globalVoxelPosition.Forward(), int3.zero);
                            }
                        }
                    }
                    // remove here
                    chunk.voxels[voxelArrayIndex] = 0;
                    // chunks[e] = chunk;
                    didUpdate = true;
                    updatedPositions.Add(localPosition);
                    if (meshType == MeshType.Minivox)
                    {
                        chunkVoxelRotations.Remove(localPosition);
                    }
                    // Passes in disable if BigMinivox!
                    if (!HasComponent<DisableItemDrop>(e2))
                    {
                        VoxelDestroyedSystem.OnVoxelDestroyed(PostUpdateCommands, entityInQueryIndex, voxelDestroyedPrefab, voxLink.vox, e, previousVoxel, globalVoxelPosition);
                    }
                    // find any leaf surrounding
                    // then remove it if it has no wood near it
                }
                if (!didUpdate)
                {
                    updateEntities.Dispose();
                    updatePositions.Dispose();
                    updatedChunkEntities.Dispose();
                    updatedPositions.Dispose();
                    return;
                }
                var didUpdateChunkDown = false;
                var didUpdateChunkUp = false;
                var didUpdateChunkLeft = false;
                var didUpdateChunkRight = false;
                var didUpdateChunkBack = false;
                var didUpdateChunkForward = false;
                for (int i = 0; i < updatedPositions.Length; i++)
                {
                    var localPosition = updatedPositions[i];
                    didUpdateChunkDown = didUpdateChunkDown || localPosition.y == 0;
                    didUpdateChunkUp = didUpdateChunkUp || localPosition.y == voxelDimensions.y - 1;
                    didUpdateChunkLeft = didUpdateChunkLeft || localPosition.x == 0;
                    didUpdateChunkRight = didUpdateChunkRight || localPosition.x == voxelDimensions.x - 1;
                    didUpdateChunkBack = didUpdateChunkBack || localPosition.z == 0;
                    didUpdateChunkForward = didUpdateChunkForward || localPosition.z == voxelDimensions.z - 1;
                }
                chunks[e] = chunk;
                if (!updatedChunkEntities.Contains(e))
                {
                    updatedChunkEntities.Add(e);
                }
                for (int i = 0; i < updatedChunkEntities.Length; i++)
                {
                    var e2 = updatedChunkEntities[i];
                    if (!HasComponent<SaveChunk>(e2))
                    {
                        PostUpdateCommands.AddComponent<SaveChunk>(entityInQueryIndex, e2);
                    }
                    if (!HasComponent<EntityBusy>(e2))
                    {
                        AddChunkBuilder(PostUpdateCommands, entityInQueryIndex, e2);
                        if (HasComponent<VoxelBuilderLightsOnly>(e2))
                        {
                            #if DEBUG_VOXEL_LIGHTS_ONLY
                            UnityEngine.Debug.LogError("Chunk still has VoxelBuilderLightsOnly: " + e2.Index);
                            #endif
                            PostUpdateCommands.RemoveComponent<VoxelBuilderLightsOnly>(entityInQueryIndex, e2);
                        }
                    }
                }
                var chunksUpdated = new NativeList<Entity>();
                if (didUpdateChunkDown)
                {
                    chunksUpdated.Add(chunkNeighbors.chunkDown);
                    AddChunkBuilder(PostUpdateCommands, entityInQueryIndex, chunkNeighbors.chunkDown);
                }
                if (didUpdateChunkUp)
                {
                    chunksUpdated.Add(chunkNeighbors.chunkUp);
                    AddChunkBuilder(PostUpdateCommands, entityInQueryIndex, chunkNeighbors.chunkUp);
                }
                if (didUpdateChunkLeft)
                {
                    chunksUpdated.Add(chunkNeighbors.chunkLeft);
                    AddChunkBuilder(PostUpdateCommands, entityInQueryIndex, chunkNeighbors.chunkLeft);
                }
                if (didUpdateChunkRight)
                {
                    chunksUpdated.Add(chunkNeighbors.chunkRight);
                    AddChunkBuilder(PostUpdateCommands, entityInQueryIndex, chunkNeighbors.chunkRight);
                }
                if (didUpdateChunkBack)
                {
                    chunksUpdated.Add(chunkNeighbors.chunkBack);
                    AddChunkBuilder(PostUpdateCommands, entityInQueryIndex, chunkNeighbors.chunkBack);
                }
                if (didUpdateChunkForward)
                {
                    chunksUpdated.Add(chunkNeighbors.chunkForward);
                    AddChunkBuilder(PostUpdateCommands, entityInQueryIndex, chunkNeighbors.chunkForward);
                }
                /*for (int i = 0; i < chunksUpdated.Length; i++)
                {
                    if (HasComponent<VoxelBuilderLightsOnly>(chunksUpdated[i]))
                    {
                        #if DEBUG_VOXEL_LIGHTS_ONLY
                        UnityEngine.Debug.LogError("Chunk still has VoxelBuilderLightsOnly: " + chunksUpdated[i].Index);
                        #endif
                        PostUpdateCommands.RemoveComponent<VoxelBuilderLightsOnly>(entityInQueryIndex, chunksUpdated[i]);
                    }
                }*/
                var neighbors = chunkNeighbors.GetNeighbors();
                for (int i = 0; i < neighbors.Length; i++)
                {
                    var chunkEntity = neighbors[i];
                    if (!chunksUpdated.Contains(chunkEntity) && !updatedChunkEntities.Contains(chunkEntity))
                    {
                        AddLightBuilder(PostUpdateCommands, entityInQueryIndex, chunkEntity);
                    }
                }
                neighbors.Dispose();
                updateEntities.Dispose();
                updatePositions.Dispose();
                updatedChunkEntities.Dispose();
                updatedPositions.Dispose();
                chunksUpdated.Dispose();
            })  .WithReadOnly(eraseVoxelEntities)
                .WithDisposeOnCompletion(eraseVoxelEntities)
                .WithReadOnly(eraseVoxels)
                .WithReadOnly(voxelLinks2)
                .WithReadOnly(voxels)
                .WithReadOnly(voxelMeshTypes)
                .WithReadOnly(chunks)
                .WithReadOnly(chunkVoxelRotations2)
                .WithNativeDisableContainerSafetyRestriction(chunks)
                .WithNativeDisableContainerSafetyRestriction(chunkVoxelRotations2)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static void AddChunkBuilder(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity chunk)
        {
            PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, chunk);
            PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, chunk);
        }

        private static void AddLightBuilder(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity chunk)
        {
            PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, chunk);
            PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, chunk);
            PostUpdateCommands.AddComponent<VoxelBuilderLightsOnly>(entityInQueryIndex, chunk);
        }
        
        public static Entity EraseVoxel(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity voxelErasePrefab,
            Entity planet, int3 position, int3 brushSize)
        {
            if (brushSize.x == 0 && brushSize.y == 0 && brushSize.z == 0)
            {
                var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, voxelErasePrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new EraseVoxel(planet, position));
                return spawnEntity;
            }
            else
            {
                for (int i = -brushSize.x; i <= brushSize.x; i++)
                {
                    for (int j = -brushSize.y; j <= brushSize.y; j++)
                    {
                        for (int k = -brushSize.z; k <= brushSize.z; k++)
                        {
                            var erasePosition = position + new int3(i, j, k);
                            var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, voxelErasePrefab);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new EraseVoxel(planet, erasePosition));
                        }
                    }
                }
                return new Entity();
            }
        }
    }
}


/*byte voxelAboveID = 0;
var voxelArrayIndex2 = 0;
var spawnPositionAbove = globalVoxelPosition.Up();
var localPosition2 = VoxelUtilities.GetLocalPosition(spawnPositionAbove, chunkPosition, voxelDimensions);
if (VoxelUtilities.InBounds(localPosition2, voxelDimensions))
{
    voxelArrayIndex2 = VoxelUtilities.GetVoxelArrayIndex(localPosition2, voxelDimensions);
    voxelAboveID = chunk.voxels[voxelArrayIndex2];
}
else
{
    var chunkEntity = chunkNeighbors.GetChunk(ref localPosition2, voxelDimensions);
    voxelArrayIndex2 = VoxelUtilities.GetVoxelArrayIndex(localPosition2, voxelDimensions);
    var neighborChunk = chunks[chunkEntity];
    voxelAboveID = neighborChunk.voxels[voxelArrayIndex2];
}
if (voxelAboveID != 0)
{
    var voxelEntity = voxelLinks.voxels[(int)(voxelAboveID) - 1];
    var voxelAbove = voxels[voxelEntity];
    if (voxelAbove.needsSolidGround == 1)
    {
        chunk.voxels[voxelArrayIndex2] = 0;
        // drops seeds occasionally
        VoxelDestroyedSystem.OnVoxelDestroyed(PostUpdateCommands, entityInQueryIndex, voxelDestroyedPrefab,
            voxLink.vox, e, voxelAboveID, spawnPositionAbove);
    }
}*/

// Get total chunks updated
// get total chunks effected (using check if voxel position is on edge)
// get total chunks light effected - only add if not in above groups
// then push build events
// can we check if any surrounding chunk lights are updated here? To make it faster.
// Mark surrounding dirty only if floodfill of lights get updated.
// also update surrounding chunks, if multiple voxels update, what happens? Do they get told to update several times AddComponent?
// check if within 8 length of the chunks surrounding