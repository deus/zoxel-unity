using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    //! An event for erasing a voxel on a vox.
    public struct EraseVoxel : IComponentData
    {
        public Entity vox;
        public int3 position;

        public EraseVoxel(Entity vox, int3 position)
        {
            this.vox = vox;
            this.position = position;
        }
    }
}