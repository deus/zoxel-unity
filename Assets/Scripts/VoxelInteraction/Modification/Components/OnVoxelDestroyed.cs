using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    public struct OnVoxelDestroyed : IComponentData
    {
        public byte voxelID;
        public int3 position;
        public Entity vox;
        public Entity chunk;

        public OnVoxelDestroyed(byte voxelID, int3 position, Entity vox, Entity chunk)
        {
            this.voxelID = voxelID;
            this.position = position;
            this.vox = vox;
            this.chunk = chunk;
        }
    }
}