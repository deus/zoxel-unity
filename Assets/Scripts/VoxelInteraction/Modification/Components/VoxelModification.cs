using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    //! An event tag for a entity that's modification voxel data.
    public struct VoxelModification : IComponentData { }
}