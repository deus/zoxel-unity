using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    //! An event for spawning a voxel on the planet.
    public struct SpawnVoxel : IComponentData
    {
        public Entity planet;
        public int3 position;
        public byte voxelIndex;
        public byte voxelDirection;

        public SpawnVoxel(Entity planet, int3 position, byte voxelIndex, byte voxelDirection)
        {
            this.planet = planet;
            this.position = position;
            this.voxelIndex = voxelIndex;
            this.voxelDirection = voxelDirection;
        }
    }
}