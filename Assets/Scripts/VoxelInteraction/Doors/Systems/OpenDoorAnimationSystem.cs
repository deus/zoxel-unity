using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;
using Zoxel.Transforms;
using Zoxel.Audio;
using Zoxel.Particles;
using Zoxel.Particles.VoxelParticles;
using Zoxel.Rendering;
using Zoxel.Voxels;
using Zoxel.Voxels.Authoring;
using Zoxel.Rendering.Authoring;

namespace Zoxel.VoxelInteraction
{
    //! Creates the open door animation with a Vox Chunk.
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class OpenDoorAnimationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
		private EntityQuery minivoxesQuery;
		private EntityQuery chunkRendersQuery;
		private EntityQuery planetsQuery;
        private Entity particlePrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			minivoxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Minivox>(),
                ComponentType.ReadOnly<VoxLink>(),
                ComponentType.ReadOnly<VoxScale>());
			chunkRendersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<ChunkRender>(),
                ComponentType.ReadOnly<ModelMesh>(),
                ComponentType.ReadOnly<ModelRender>());
			planetsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<VoxScale>());
            RequireForUpdate<MinivoxSettings>();
            RequireForUpdate<ModelSettings>();
            RequireForUpdate(processQuery);
            RequireForUpdate<RenderSettings>();
        }

        private void InitializePrefabs()
        {
            if (particlePrefab.Index == 0)
            {
                particlePrefab = EntityManager.Instantiate(ParticleSystemSystem.particlePrefab);
                EntityManager.AddComponent<Prefab>(particlePrefab);
                EntityManager.RemoveComponent<ParticleLerpScale>(particlePrefab);
                EntityManager.RemoveComponent<ParticleLerpColor>(particlePrefab);
            }
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var renderSettings = GetSingleton<RenderSettings>();
            if (renderSettings.disableParticles)
            {
                return;
            }
            InitializePrefabs();
            var minivoxSettings = GetSingleton<MinivoxSettings>();
            var debugDoorAnimations = minivoxSettings.debugDoorAnimations;
            var modelSettings = GetSingleton<ModelSettings>();
            var disableVoxelNoise = modelSettings.disableVoxelNoise;
            var voxelNoiseColorVariation = modelSettings.voxelNoiseColorVariation;
            var maxAnimationTime = 1.6f;
            // var maxDelay = 0.6f;
            var buggedColor = new float4(1, 0, 0, 1);
            var halfOne = new float3(0.5f, 0.5f, 0.5f);
            var particlePrefab = this.particlePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var elapsedTime = World.Time.ElapsedTime;
            var minivoxEntities = minivoxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var minivoxes = GetComponentLookup<Minivox>(true);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            var seeds = GetComponentLookup<Seed>(true);
            var voxScales = GetComponentLookup<VoxScale>(true);
            var voxColors = GetComponentLookup<VoxColors>(true);
            minivoxEntities.Dispose();
            var chunkRenderEntities = chunkRendersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunkMeshes = GetComponentLookup<ModelMesh>(true);
            var materialBaseColors = GetComponentLookup<MaterialBaseColor>(true);
            chunkRenderEntities.Dispose();
            var planetEntities = planetsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var planetVoxScales = GetComponentLookup<VoxScale>(true);
            planetEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, EntityBusy, DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref Chunk chunk, in OpenDoorAnimation openDoorAnimation, in ChunkRenderLinks chunkRenderLinks, in ZoxID zoxID,
                    in VoxLink voxLink) =>
            {
                if (zoxID.id == 0)
                {
                    // UnityEngine.Debug.LogError("openDoorAnimation zoxID.id is 0.");
                    return;
                }
                var minivoxEntity = voxLink.vox;
                var seed = seeds[minivoxEntity].seed;
                if (seed == 0)
                {
                    // UnityEngine.Debug.LogError("openDoorAnimation seed is 0.");
                    return;
                }
                var colors = voxColors[minivoxEntity].colors;
                var minivox = minivoxes[minivoxEntity];
                var voxelScale = voxScales[minivoxEntity].scale;
                var planetEntity = voxLinks[minivoxEntity].vox;
                var planetScale = planetVoxScales[planetEntity].scale;
                var voxelPositions = new NativeList<int3>();
                var voxelColors = new NativeList<float4>();
                // UnityEngine.Debug.LogError("Opening door with ID: " + zoxID.id);
                var chunkState = new ChunkState(in chunk);
                var random = new Random();
                var pigmentationRandom = new Random();
                random.InitState((uint) zoxID.id);
                pigmentationRandom.InitState((uint) seed);
                var voxelDimensions = chunk.voxelDimensions;
                var frameSize = voxelDimensions.x / 16;
                var renderEntity = chunkRenderLinks.chunkRenders[0];
                var terrainMesh = chunkMeshes[renderEntity];
                var renderColor = materialBaseColors[renderEntity].Value;
                var meshOffset = terrainMesh.position;
                // unique times?
                var animationDelay = 0f; // maxDelay / 2f; // random.NextFloat(0, maxDelay);
                var animationTime = maxAnimationTime; // (maxAnimationTime * force.y) - animationDelay;
                if (debugDoorAnimations)
                {
                    animationTime = 3f;
                }
                var position = new int3();
                var colorAddition = new float3();
                int voxelIndex;
                byte voxelType;
                for (position.x = 0; position.x < voxelDimensions.x; position.x++)
                {
                    for (position.y = 0; position.y < voxelDimensions.y; position.y++)
                    {
                        for (position.z = 0; position.z < voxelDimensions.z; position.z++)
                        {
                            if (!disableVoxelNoise)
                            {
                                colorAddition.x = pigmentationRandom.NextFloat(-voxelNoiseColorVariation, voxelNoiseColorVariation);
                                colorAddition.y = pigmentationRandom.NextFloat(-voxelNoiseColorVariation, voxelNoiseColorVariation);
                                colorAddition.z = pigmentationRandom.NextFloat(-voxelNoiseColorVariation, voxelNoiseColorVariation);
                            }
                            if (!(position.x >= frameSize + 1 && position.x <= voxelDimensions.x - 1 - frameSize - 1 && position.y <= voxelDimensions.y - 1 - frameSize - 1))
                            {
                                continue;
                            }
                            voxelIndex = VoxelUtilities.GetVoxelArrayIndex(position, voxelDimensions);
                            if (!(voxelIndex >= 0 && voxelIndex < chunk.voxels.Length))
                            {
                                continue;
                            }
                            voxelType = chunk.voxels[voxelIndex];
                            if (voxelType == 0)
                            {
                                continue;
                            }
                            chunk.voxels[voxelIndex] = 0;
                            voxelPositions.Add(position);
                            //var animationDelay = random.NextFloat(0, maxDelay);
                            //var animationTime = (maxAnimationTime * force.y) - animationDelay;
                            if (voxelType - 1 < colors.Length)
                            {
                                var color = colors[voxelType - 1];
                                // make color match minivox colors
                                var voxelColor = new float4(color.x, color.y, color.z, 1);
                                voxelColor += new float4(colorAddition.x, colorAddition.y, colorAddition.z, 0);
                                voxelColor.x = math.clamp(voxelColor.x, 0, 1f);
                                voxelColor.y = math.clamp(voxelColor.y, 0, 1f);
                                voxelColor.z = math.clamp(voxelColor.z, 0, 1f);
                                voxelColor.x *= renderColor.x;
                                voxelColor.y *= renderColor.y;
                                voxelColor.z *= renderColor.z;
                                voxelColor.w = 1;
                                voxelColors.Add(voxelColor);
                            }
                            else
                            {
                                voxelColors.Add(buggedColor);
                                // PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new MaterialBaseColor { Value = new float4(1, 0, 0, 1) });
                            }
                        }
                    }
                }
                if (voxelPositions.Length > 0)
                {
                    // UnityEngine.Debug.LogError("voxelScale: " + voxelScale);
                    var particleEntities = new NativeArray<Entity>(voxelPositions.Length, Allocator.Temp);
                    PostUpdateCommands.Instantiate(entityInQueryIndex, particlePrefab, particleEntities);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, particleEntities, new NonUniformScale { Value = voxelScale });
                    PostUpdateCommands.AddComponent(entityInQueryIndex, particleEntities, new ParticleLifeTime(elapsedTime, animationTime));
                    for (int i = 0; i < particleEntities.Length; i++)
                    {
                        var e2 = particleEntities[i];
                        var voxelPosition = voxelPositions[i];
                        var spawnPosition = voxelPosition.ToFloat3() + halfOne;
                        spawnPosition.x *= voxelScale.x;
                        spawnPosition.y *= voxelScale.y;
                        spawnPosition.z *= voxelScale.z;
                        spawnPosition -= meshOffset;
                        spawnPosition.y += planetScale.y / 2f;
                        spawnPosition.x *= -1;
                        spawnPosition.z *= -1;
                        spawnPosition = minivox.position + math.rotate(minivox.rotation, spawnPosition);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Translation { Value = spawnPosition });
                        var force = new float3(0, (voxelDimensions.y - voxelPosition.y) * voxelScale.y, 0);
                        var targetPosition = spawnPosition + force;
                        if (debugDoorAnimations)
                        {
                            targetPosition = spawnPosition;
                        }
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ParticleLerpPosition(spawnPosition, targetPosition, animationDelay));
                        // set color
                        var voxelColor = voxelColors[i];
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new MaterialBaseColor(voxelColor));
                        var targetAlpha = 0f;
                        if (debugDoorAnimations)
                        {
                            targetAlpha = 1f;
                        }
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new ParticleLerpAlpha(voxelColor.w, targetAlpha, animationTime, animationDelay));
                    }
                    PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, chunkState);
                    particleEntities.Dispose();
                }
                else
                {
                    chunkState.Dispose();
                }
                voxelPositions.Dispose();
                voxelColors.Dispose();
            })	.WithReadOnly(minivoxes).WithReadOnly(voxLinks).WithReadOnly(seeds).WithReadOnly(voxScales).WithReadOnly(voxColors)
                .WithReadOnly(chunkMeshes).WithReadOnly(materialBaseColors)
                .WithReadOnly(planetVoxScales)
				.ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}

// var force = new float3(0, (voxelDimensions.y - position.y) * characterScale.y, 0);
/*var voxelPosition = (voxelScale * (position.ToFloat3() + new float3(0.5f, 0.5f, 0.5f))) - meshOffset;
voxelPosition.z *= -1;
voxelPosition.x *= -1;
voxelPosition *= planetScale;
voxelPosition.y += 0.5f;
// grab individual offset of chunk render - create a chunk offset and use that on any chunkRenders to offset the mesh (centre it)
voxelPosition = openDoorAnimation.position + math.rotate(openDoorAnimation.rotation, voxelPosition);*/

/*var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, particlePrefab);
PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new NonUniformScale { Value = characterScale });
PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Translation { Value = voxelPosition });
PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ParticleLifeTime(elapsedTime, animationTime));
PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ParticleLerpPosition(voxelPosition, voxelPosition + force, animationDelay));
PostUpdateCommands.RemoveComponent<ParticleLerpScale>(entityInQueryIndex, e2);
*/