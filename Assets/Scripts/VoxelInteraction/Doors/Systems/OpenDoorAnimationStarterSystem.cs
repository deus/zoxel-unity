
using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;
using Zoxel.Voxels;
using Zoxel.Voxels.Minivoxes;
// todo: make pick a few random ones p er frame
//          count the amount of available voxels
//          save the first chunk state
//          restore to the first chunk state over time when reversing

namespace Zoxel.VoxelInteraction
{
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class OpenDoorAnimationStarterSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<SoundSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var soundSettings = GetSingleton<SoundSettings>();
            var sampleRate = soundSettings.sampleRate;
            var soundVolume = soundSettings.doorSoundVolume;
            var soundPrefab = SoundPlaySystem.soundPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, EntityBusy, DestroyEntity>()
                .WithAll<OpenDoorAnimation>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<OpenDoorAnimation>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // second stage sends the data to the chunks
            Dependency = Entities
                .WithNone<InitializeEntity, EntityBusy, DestroyEntity>()
                .WithAll<OpenDoorAnimation, Minivox>()
                .ForEach((Entity e, int entityInQueryIndex, in Seed seed, in Minivox minivox) =>
            {
                var soundSeed = seed.seed + (int)(2048 * elapsedTime);
                var random = new Random();
                random.InitState((uint) soundSeed);
                var soundEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, soundPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Seed(soundSeed));
                var generateSound = new GenerateSound();
                generateSound.CreateMusicSound((byte)(26 + random.NextInt(8)), 1, sampleRate);
                generateSound.timeLength = 0.8f;
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new SoundData(ref random, generateSound.timeLength, generateSound.sampleRate, 1, soundVolume));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new DelayEvent(elapsedTime, 0.05));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Translation { Value = minivox.position });
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<InitializeEntity, EntityBusy, DestroyEntity>()
                .WithAll<OpenDoorAnimation>()
                .ForEach((Entity e, int entityInQueryIndex, in Vox vox, in OpenDoorAnimation openDoorAnimation) =>
            {
                // UnityEngine.Debug.LogError("openDoorAnimation Vox!");
                for (int i = 0; i < vox.chunks.Length; i++)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, vox.chunks[i], openDoorAnimation);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in DoorTransitioning doorTransitioning) =>
            {
                if (elapsedTime - doorTransitioning.timeStarted >= doorTransitioning.timeDelay)
                {
                    PostUpdateCommands.RemoveComponent<DoorTransitioning>(entityInQueryIndex, e);
                    if (doorTransitioning.removeIgnoreCollision == 1)
                    {
                        PostUpdateCommands.RemoveComponent<IgnoreMinivoxCollision>(entityInQueryIndex, e);
                    }
                    else
                    {
                        PostUpdateCommands.AddComponent<IgnoreMinivoxCollision>(entityInQueryIndex, e);
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}


//  in Rotation rotation, in NonUniformScale scale,
// first stage just passes data into the component
/*if (!processQuery.IsEmpty)
{
    var planetEntities = planetsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
    Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
    var voxScales = GetComponentLookup<VoxScale>(true);
    planetEntities.Dispose();
    Dependency = Entities.ForEach((int entityInQueryIndex, ref OpenDoorAnimation openDoorAnimation, in Minivox minivox, in VoxColors voxColors,
        in VoxScale voxScale, in Seed seed, in VoxLink voxLink) =>
    {
        if (!voxScales.HasComponent(voxLink.vox))
        {
            return;
        }
        // UnityEngine.Debug.LogError("Vox Opening door with ID: " + seed.seed);
        openDoorAnimation.position = minivox.position;
        openDoorAnimation.rotation = quaternion.identity;
        openDoorAnimation.colors = voxColors.colors;
        openDoorAnimation.seed = seed.seed;
        openDoorAnimation.voxelScale = voxScale.scale;
        var voxScale2 = voxScales[voxLink.vox];
        openDoorAnimation.planetScale = voxScale2.scale; // new float3(1, 1, 1); // scale.Value;
    })  .WithReadOnly(voxScales)
        .ScheduleParallel(Dependency);
    // commandBufferSystem.AddJobHandleForProducer(Dependency);
}*/