using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Audio;
using Zoxel.Audio.Authoring;

namespace Zoxel.VoxelInteraction
{
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class CloseDoorAnimationStarterSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<SoundSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var soundSettings = GetSingleton<SoundSettings>();
            var sampleRate = soundSettings.sampleRate;
            var soundVolume = soundSettings.doorSoundVolume;
            var soundPrefab = SoundPlaySystem.soundPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, EntityBusy, DestroyEntity>()
                .WithAll<CloseDoorAnimation>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<CloseDoorAnimation>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // second stage sends the data to the chunks
            Dependency = Entities
                .WithNone<InitializeEntity, EntityBusy, DestroyEntity>()
                .WithAll<CloseDoorAnimation, Minivox>()
                .ForEach((Entity e, int entityInQueryIndex, in Seed seed, in Minivox minivox) =>
            {
                var soundSeed = seed.seed + (int)(2048 * elapsedTime);
                var random = new Random();
                random.InitState((uint) soundSeed);
                var soundEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, soundPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Seed(soundSeed));
                var generateSound = new GenerateSound();
                generateSound.CreateMusicSound((byte)(26 + random.NextInt(8)), 1, sampleRate);
                generateSound.timeLength = 0.8f;
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new SoundData(ref random, generateSound.timeLength, generateSound.sampleRate, 1, soundVolume));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new DelayEvent(elapsedTime, 0.05));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Translation { Value = minivox.position });
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<InitializeEntity, EntityBusy, DestroyEntity>()
                .WithAll<CloseDoorAnimation>()
                .ForEach((Entity e, int entityInQueryIndex, in Vox vox, in CloseDoorAnimation closeDoorAnimation) =>
            {
                // UnityEngine.Debug.LogError("CloseDoorAnimation! Vox.");
                for (int i = 0; i < vox.chunks.Length; i++)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, vox.chunks[i], closeDoorAnimation);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
            // second stage sends the data to the chunks
            /*Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in CloseDoorAnimation closeDoorAnimation, in Vox vox) =>
            {
                for (int i = 0; i < vox.chunks.Length; i++)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, vox.chunks[i], closeDoorAnimation);
                }
                PostUpdateCommands.RemoveComponent<CloseDoorAnimation>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);*/
            
            /*Dependency = Entities.ForEach((int entityInQueryIndex, ref CloseDoorAnimation closeDoorAnimation, in Minivox minivox, in VoxColors voxColors,
                in VoxScale voxScale, in Seed seed) =>
            {
                //UnityEngine.Debug.LogError("Vox Closing door with ID: " + zoxID.id);
                closeDoorAnimation.position = minivox.position;
                closeDoorAnimation.rotation = quaternion.identity;
                closeDoorAnimation.colors = voxColors.colors;
                closeDoorAnimation.voxelScale = voxScale.scale;
                closeDoorAnimation.voxID = seed.seed;
                closeDoorAnimation.globalScale = new float3(1, 1, 1); // scale.Value;
                var soundSeed = seed.seed + (int)(2048 * elapsedTime);
                var random = new Random();
                random.InitState((uint)soundSeed);
                var soundEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, soundPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Seed(soundSeed));
                var generateSound = new GenerateSound();
                generateSound.CreateMusicSound((byte)(26 + random.NextInt(8)), 1, sampleRate);
                generateSound.timeLength = 0.8f;
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new SoundData(ref random, generateSound.timeLength, generateSound.sampleRate, 1, soundVolume));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new DelayEvent(elapsedTime, 0.05));
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Translation { Value = minivox.position });
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities.ForEach((ref CloseDoorAnimation closeDoorAnimation, in Minivox minivox, in Rotation rotation) =>
            {
                closeDoorAnimation.rotation = rotation.Value;
			}).ScheduleParallel(Dependency);*/



                /*var generateSound = new GenerateSound();
                var sound = new Sound();
                generateSound.CreateMusicSound(soundSeed, (byte)(26 + random.NextInt(8)), 1, sampleRate);
                generateSound.timeLength = 0.8f;
                sound.Initialize(soundSeed, generateSound.timeLength, generateSound.sampleRate, 1, soundVolume);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, sound);*/