
using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.VoxelInteraction
{
    public struct DoorTransitioning : IComponentData
    {
        public double timeStarted;
        public double timeDelay;
        public byte removeIgnoreCollision;

        public DoorTransitioning(double timeStarted, double timeDelay, byte removeIgnoreCollision)
        {
            this.timeDelay = timeDelay;
            this.timeStarted = timeStarted;
            this.removeIgnoreCollision = removeIgnoreCollision;
        }
    }
}