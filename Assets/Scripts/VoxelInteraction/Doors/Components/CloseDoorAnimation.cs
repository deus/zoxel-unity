
using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.VoxelInteraction
{
    //! An event to close the door by animating voxels.
    public struct CloseDoorAnimation : IComponentData
    {
        public double createdTime;
    }
}