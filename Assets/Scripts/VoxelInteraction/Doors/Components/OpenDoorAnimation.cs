
using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.VoxelInteraction
{
    //! An event to open the door by animating voxels.
    public struct OpenDoorAnimation : IComponentData
    {
        public double createdTime;
    }
}