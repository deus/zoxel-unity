﻿/*using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

using Zoxel.Voxels;
using Zoxel.Characters;

namespace Zoxel.VoxelInteraction
{
    [UpdateAfter(typeof(VoxelGizmoSystem))]
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class CharacterGizmoUpdateSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery characterQuery;

        protected override void OnCreate()
        {
            processQuery = GetEntityQuery(ComponentType.ReadOnly<CharacterGizmo>());
            characterQuery = GetEntityQuery(ComponentType.ReadOnly<Character>(), ComponentType.ReadOnly<Body>(), ComponentType.Exclude<DeadEntity>(), ComponentType.Exclude<DestroyEntity>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var disableRaycasting = false;  // umm.. RaycastManager?
            if (disableRaycasting || processQuery.IsEmpty)
            {
                return;
            }
            var characterEntities = characterQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var bodys = GetComponentLookup<Body>(true);
            var translations = GetComponentLookup<Translation>(true);
            var rotations = GetComponentLookup<Rotation>(true);
            Dependency = Entities
                .WithAll<CharacterGizmo>()
                .ForEach((Entity e, ref Translation translation, in CharacterGizmo characterGizmo) =>
            {
                if (characterEntities.Length > 0 && translations.HasComponent(characterGizmo.character) && bodys.HasComponent(characterGizmo.character))
                {
                    var characterBody = bodys[characterGizmo.character].size;
                    var characterPosition = translations[characterGizmo.character].Value;
                    var characterRotation = rotations[characterGizmo.character].Value;
                    translation.Value = characterPosition + math.mul(characterRotation, new float3(0, 0.12f, 0) + new float3(0, characterBody.y, 0));
                }
            })  .WithReadOnly(characterEntities)
                .WithDisposeOnCompletion(characterEntities)
                .WithReadOnly(bodys)
                .WithReadOnly(translations)
                .WithReadOnly(rotations)
                .WithNativeDisableContainerSafetyRestriction(translations)
                .ScheduleParallel(Dependency);
        }
    }
}

*/