using Unity.Entities;
using Unity.Mathematics;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.AI;
using Zoxel.Dialogue;
using Zoxel.Stats;
using Zoxel.UI;
using Zoxel.Races;

namespace Zoxel.VoxelInteraction
{
    //! Raycasts a Character
    /**
    *   \todo Convert to Parallel.
    *   \todo Use event when new character or voxel is selected. Then update Gizmo > Tooltip with this event.
    */
    [UpdateAfter(typeof(VoxelGizmoSystem))]
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class RaycastCharacterUISystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<DeadEntity, DestroyEntity, DisableRaycaster>()
                .WithAll<OnRaycastCharacter>()
                .ForEach((Entity e, in RaycastCharacter raycastCharacter, in ControllerLink controllerLink) =>
            {
                var playerTooltipLinks = EntityManager.GetComponentData<PlayerTooltipLinks>(controllerLink.controller);
                var hitCharacter = raycastCharacter.character;
                if (!HasComponent<ZoxName>(hitCharacter))
                {
                    UnityEngine.Debug.LogError("Target Character does not have name: " + hitCharacter.Index);
                    return;
                }
                var name = EntityManager.GetComponentData<ZoxName>(hitCharacter).name.ToString(); // Mr Penguin
                var raceEntity = EntityManager.GetComponentData<RaceLink>(hitCharacter).race;
                string raceName = "";
                if (raceEntity.Index > 0)
                {
                    // todo: use race name in description
                    raceName = EntityManager.GetComponentData<ZoxName>(raceEntity).name.ToString();
                }
                else
                {
                    raceName = "Uknown";
                }
                string label = raceName + " ";
                if (!HasComponent<DeadEntity>(hitCharacter))
                {
                    /*var userStatLinks = EntityManager.GetComponentData<UserStatLinks>(hitCharacter);
                    if (userStatLinks.stats.Length > 0)
                    {
                        label += "Lvl " + 1; // userStatLinks.levels[0].value;
                    }*/
                    // Tooltip for character
                    if (HasComponent<DialogueTree>(hitCharacter))
                    {
                        if (HasComponent<Attack>(hitCharacter))
                        {
                            label += "\n" + name + " is in Combat";
                        }
                        else if (HasComponent<Speaking>(hitCharacter))
                        {
                            label += "\n" + name + " is Speaking to someone else";
                        }
                        else
                        {
                            label += "\n[action2] to Talk to " + name; // + " : " + EntityManager.GetComponentData<ZoxID>(hitCharacter).id;
                        }
                    }
                    /*else
                    {
                        label += "\nGoes by " + name;
                    }*/
                }
                else
                {
                    label += "\nWent by " + name;
                }
                playerTooltipLinks.SetPlayerSelectionTooltip(PostUpdateCommands2, e, label);
            }).WithoutBurst().Run();
        }
    }
}

