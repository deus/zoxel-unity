﻿using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.AI;

namespace Zoxel.VoxelInteraction
{
    //! Handles OnRaycastCharacter events.
    [UpdateAfter(typeof(VoxelGizmoSystem))]
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class RaycastCharacterSystem : SystemBase
    {
        const float offset = 0.001f;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var disableRaycasting = false;  // todo: RaycastManager
            if (disableRaycasting)
            {
                return;
            }
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // On Raycast Character
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<OnRaycastCharacter>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<OnRaycastCharacter>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<DestroyEntity, DeadEntity, DisableRaycaster>()
                .WithAll<OnRaycastCharacter>()
                .ForEach((ref Target target, in RaycastCharacter raycastCharacter) =>
            {
                target.target = raycastCharacter.character;
            }).ScheduleParallel(Dependency);
            Dependency = Entities
                .WithNone<DestroyEntity, DeadEntity, DisableRaycaster>()
                .WithAll<OnRaycastCharacter>()
                .ForEach((Entity e, int entityInQueryIndex, in RaycastCharacter raycastCharacter, in GizmoLinks gizmoLinks) =>
            {
                var voxelGizmo = gizmoLinks.voxelGizmo;
                if (!HasComponent<Translation>(voxelGizmo))
                {
                    return;
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, voxelGizmo, new Translation { Value = new float3(0, -1000, 0) });
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}