using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;
using Zoxel.Cameras;
using Zoxel.Actions;
using Zoxel.Players;
using Zoxel.Voxels.Minivoxes;
using Zoxel.Input;

namespace Zoxel.VoxelInteraction
{
    //! For a PlayerCharacter that triggers world voxels.
    /**
    *   Player can activate or switch UserSkillLinks using the Controller component.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class PlayerTriggerVoxelSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxelsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			voxelsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Voxel>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxelEntities = voxelsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var voxels = GetComponentLookup<Voxel>(true);
            voxelEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DisableRaycaster>()
                .WithAll<ActivateWorldAction>()
                .ForEach((Entity e, int entityInQueryIndex, in RaycastVoxel raycastVoxel, in VoxLink voxLink, in CameraLink cameraLink, in ControllerLink controllerLink) =>
            {
                if (raycastVoxel.rayHitType != (byte) RayHitType.Voxel || !voxels.HasComponent(raycastVoxel.hitVoxel.voxel))
                {
                    return;
                }
                var hitChunk = raycastVoxel.hitVoxel.chunk;
                var hitVoxel = voxels[raycastVoxel.hitVoxel.voxel];
                // if chest
                if (hitVoxel.functionType == (byte) VoxelFunctionType.Chest)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, controllerLink.controller, new SetControllerMapping(ControllerMapping.ChestUI));
                    PostUpdateCommands.AddComponent<DisableRaycaster>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<DisableFirstPersonCamera>(entityInQueryIndex, cameraLink.camera);
                    PostUpdateCommands.AddComponent<CloseRealmUI>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OpenChest(hitChunk, raycastVoxel.hitVoxel.position));
                }
                else if (hitVoxel.functionType == (byte) VoxelFunctionType.Door)
                {
                    // open door!
                    // UnityEngine.Debug.LogError("Opening Door: " + raycastVoxel.hitMinivox.Index);
                    var hitMinivox = raycastVoxel.hitVoxel.hitMinivox;
                    if (!HasComponent<DoorTransitioning>(hitMinivox))
                    {
                        if (HasComponent<Minivox>(hitMinivox))
                        {
                            var removeIgnoreCollision = (byte) 0;
                            if (!HasComponent<IgnoreMinivoxCollision>(hitMinivox))
                            {
                                PostUpdateCommands.AddComponent(entityInQueryIndex, hitMinivox, new OpenDoorAnimation { createdTime = elapsedTime });
                            }
                            else
                            {
                                PostUpdateCommands.AddComponent(entityInQueryIndex, hitMinivox, new CloseDoorAnimation { createdTime = elapsedTime });
                                removeIgnoreCollision = 1;
                            }
                            PostUpdateCommands.AddComponent(entityInQueryIndex, hitMinivox, new DoorTransitioning(elapsedTime, 2, removeIgnoreCollision));
                        }
                    }
                    // animate the voxels of the door to open
                    // basically if x >= 2 && x <= 13 then fade out voxels randomly over 2 seconds
                    // after this animation, add or remove ignore collision
                    // a ChunkState will be added, to save the original door model before animating it, containing the first data of the model
                    // PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ToggleDoor(hitChunk, raycastVoxel.hitVoxel.localPosition));
                }
                else if (hitVoxel.functionType == (byte) VoxelFunctionType.Bed)
                {
                    //! \todo Fade out and fade in screen
                    //! \todo Set time to morning 5am - Event to skip time ahead for 6-10 (random) hours (base on stamina later)
                }
            })  .WithReadOnly(voxels)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}