﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Input;
using Zoxel.Transforms;
using Zoxel.Cameras;

namespace Zoxel.VoxelInteraction
{
    //! Sets the origin point of a mouse raycast
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class MouseRaycasterOriginSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery playersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            playersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Controller>(),
                ComponentType.ReadOnly<Player>());
            RequireForUpdate(processQuery);
            RequireForUpdate(playersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var playerEntities = playersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var controllers = GetComponentLookup<Controller>(true);
            var deviceTypeDatas = GetComponentLookup<DeviceTypeData>(true);
            playerEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DisableRaycaster>()
                .WithAll<MouseRaycaster>()
                .ForEach((Entity e, ref RaycasterOrigin raycasterOrigin, in Camera camera, in LocalToWorld localToWorld, in Rotation rotation,
                    in CameraProjectionMatrix cameraProjectionMatrix, in ControllerLink controllerLink) =>
            {
                var controller = controllers[controllerLink.controller];
                var deviceTypeData = deviceTypeDatas[controllerLink.controller];
                // var mousePosition = controller.mouse.GetPointer(screenDimensions, cameraScreenRect.rect.position, cameraScreenRect.rect.size); 
                var mousePosition = controller.mouse.GetPointer(camera.screenDimensions.ToFloat2());    // Gives mouse position between 0 and 1 for camera.
                if (deviceTypeData.type == DeviceType.Gamepad)
                {
                    mousePosition = new float2(0.06f, 0.5f);    // gamepad will just show mouse on left side of screen
                }
                var screenPosition = new float3(mousePosition.x, mousePosition.y, 0f); // uiDepth);
                raycasterOrigin.rayOrigin = localToWorld.Position;
                raycasterOrigin.rayDirection = CameraUtil.GetCameraRayDirection(screenPosition, localToWorld.Value, cameraProjectionMatrix.projectionMatrix, localToWorld.Position);
                raycasterOrigin.rayQuaternion = quaternion.LookRotation(raycasterOrigin.rayDirection, new float3(0, 1, 0));
            })  .WithReadOnly(controllers)
                .WithReadOnly(deviceTypeDatas)
                .ScheduleParallel();
        }
    }
}

// Flip z for camera matrix
/*var mainCamera = CameraReferences.GetMainCamera(EntityManager);
if (mainCamera == null)
{
return;
}
float4x4 projectionMatrix = mainCamera.projectionMatrix;*/ /// rotation.Value;
// CameraUtil.ScreenToWorldPoint(screenPosition, cameraLocalToWorld, cameraProjectionMatrix.projectionMatrix, localToWorld.Position);
//var direction = math.normalize(raycasterOrigin.rayOrigin - localToWorld.Position);
//raycasterOrigin.rayQuaternion = lookDirection; /// rotation.Value;
// raycasterOrigin.rayOrigin = localToWorld.Position;