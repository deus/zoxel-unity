﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Burst;
using Unity.Jobs;
using Zoxel.Transforms;
using Zoxel.Cameras;
// using Zoxel.Players;
// When switching to placing voxel or whatever
//      Add this component onto character
//      else remove it

namespace Zoxel.VoxelInteraction
{
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class CharacterCameraRaycastSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery cameraQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            cameraQuery = GetEntityQuery(
                ComponentType.ReadOnly<Camera>(),
                ComponentType.ReadOnly<Rotation>());
            cameraQuery.SetChangedVersionFilter(typeof(Rotation));
            RequireForUpdate(processQuery);
            RequireForUpdate(cameraQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var screenWidth = UnityEngine.Screen.width;
            var screenHeight = UnityEngine.Screen.height;
            var cameraEntities = cameraQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var cameras = GetComponentLookup<LocalToWorld>(true);
            var cameraRotations = GetComponentLookup<Rotation>(true);
            cameraEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DisableRaycaster, MouseRaycaster>()
                .ForEach((Entity e, ref RaycasterOrigin raycasterOrigin, in CameraLink cameraLink) =>
            {
                var camera = cameraLink.camera;
                if (cameras.HasComponent(camera))
                {
                    var localToWorld = cameras[camera];
                    raycasterOrigin.rayDirection = localToWorld.Forward;
                    raycasterOrigin.rayOrigin = localToWorld.Position;
                    raycasterOrigin.rayQuaternion = cameraRotations[camera].Value;
                }
            })  .WithReadOnly(cameras)
                .WithReadOnly(cameraRotations)
                .ScheduleParallel(Dependency);
        }
    }
}