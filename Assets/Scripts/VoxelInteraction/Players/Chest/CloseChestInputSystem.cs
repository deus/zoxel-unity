using Unity.Entities;
using Unity.Burst;
using Zoxel.UI;
using Zoxel.Players;
using Zoxel.Input;

namespace Zoxel.VoxelInteraction
{
    //! Input system for closing chest using a cancel action.
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class CloseChestInputSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<ControllerDisabled>()
                .WithNone<DeadEntity, DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, in Controller controller, in UILink uiLink, in CharacterLink characterLink) =>
            {
                if (HasComponent<CloseChest>(characterLink.character) || HasComponent<OpenChest>(characterLink.character) || HasComponent<DeadEntity>(characterLink.character))
                {
                    return;
                }
                if (controller.mapping == ControllerMapping.ChestUI && controller.gamepad.buttonB.wasPressedThisFrame == 1)
                {
                    PostUpdateCommands.AddComponent<CloseChest>(entityInQueryIndex, characterLink.character);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}