using Unity.Entities;
using Unity.Burst;
using Zoxel.Input;
using Zoxel.UI;
using Zoxel.Items.UI;
using Zoxel.Cameras;

namespace Zoxel.VoxelInteraction
{
    //! Closes Chest Voxel.
    /**
    *   Closes RealmUI and Opens Chest UI and Inventory UI.
    *   \todo Animate post processing back to normal.
    *   \todo Play sound when closing chest.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class CloseChestSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<OpenChest>()
                .WithAll<CloseChest>()
                .ForEach((Entity e, int entityInQueryIndex, in UILink uiLink, in CameraLink cameraLink, in ControllerLink controllerLink) =>
            {
                PostUpdateCommands.RemoveComponent<CloseChest>(entityInQueryIndex, e);
                PostUpdateCommands.RemoveComponent<DisableRaycaster>(entityInQueryIndex, e);
                PostUpdateCommands.RemoveComponent<DisableFirstPersonCamera>(entityInQueryIndex, cameraLink.camera);
                PostUpdateCommands.AddComponent(entityInQueryIndex, controllerLink.controller, new SetControllerMapping(ControllerMapping.InGame));
                for (int i = 0; i < uiLink.uis.Length; i++)
                {
                    var ui = uiLink.uis[i];
                    if (HasComponent<ChestUI>(ui))
                    {
                        UICoreSystem.RemoveUI(PostUpdateCommands, entityInQueryIndex, removeCharacterUIPrefab, e, ui);
                    }
                    else if (HasComponent<InventoryUI>(ui))
                    {
                        UICoreSystem.RemoveUI(PostUpdateCommands, entityInQueryIndex, removeCharacterUIPrefab, e, ui);
                    }
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SpawnRealmUI(SpawnRealmUIType.ChestUI));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}