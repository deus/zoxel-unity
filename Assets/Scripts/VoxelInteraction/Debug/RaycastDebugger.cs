using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Voxels.Lighting;
using Zoxel.Voxels.Minivoxes;
using Zoxel.Movement;
using Zoxel.Rendering;
using Zoxel.Players;
using Zoxel.Worlds;

namespace Zoxel.VoxelInteraction.Debug
{
    //! Debugs the raycasting with the UI
    public partial class RaycastDebugger : MonoBehaviour
    {
        public static RaycastDebugger instance;
        public bool isDebugDirect;
        private int fontSize = 26;
        private UnityEngine.Color fontColor = UnityEngine.Color.green;

        [Header("Debug Options")]
        public bool isDebugHitNormal;
        public bool isDebugMegaChunkPosition;
        public bool isDebugQuadrants;
        public bool isDebugRotations;
        public bool isDebugChunkRenders;
        public bool isDebugChunkLights;
        public bool isDebugStructures;
        public bool isDebugStructuresAll;
        public bool isDebugMinivoxes;
        public bool isDebugBigMinivoxes;
        public bool isDebugGravity;
        public bool isDebugLights;

        void Awake()
        {
            instance = this;
        }

        public void OnGUI()
        {
            if (isDebugDirect)
            {
                GUI.skin.label.fontSize = fontSize;
                GUI.color = fontColor;
                DebugRaycasting();
            }
        }

        public EntityManager EntityManager
        {
            get
            { 
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        public void DebugRaycasting()
        {
            var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
            if (playerLinks.players.Length > 0)
            {
                var player = playerLinks.players[0];
                VoxelSelectionUI(player);
            }
        }


        private void VoxelSelectionUI(Entity playerEntity)
        {
            var characterEntity = EntityManager.GetComponentData<CharacterLink>(playerEntity).character;
            GUILayout.Label("Debugging Raycasting of Player [" + characterEntity.Index + "]");
            if (EntityManager.HasComponent<RaycastVoxel>(characterEntity) == false)
            {
                GUILayout.Label("Player Has no RaycastVoxel Component.");
            }
            else
            {
                var raycastVoxel = EntityManager.GetComponentData<RaycastVoxel>(characterEntity);
                GUILayout.Label("Hit Type: " + (RayHitType) raycastVoxel.rayHitType + ". Hit Angle: " + raycastVoxel.hitAngle);
                var gravityQuadrant = EntityManager.GetComponentData<GravityQuadrant>(characterEntity);
                var voxelRotation = BlockRotation.GetBlockRotation(gravityQuadrant.quadrant, raycastVoxel.blockRotationRegion);
                GUILayout.Label("PlanetSide: " + gravityQuadrant.quadrant + ". Region: " + raycastVoxel.blockRotationRegion + ". VoxelRotation: " + voxelRotation);
                if (raycastVoxel.rayHitType == (byte) RayHitType.Voxel)
                {
                    GUILayout.Label("Hit Voxel");
                    DrawVoxelHitInfoUI(in raycastVoxel.hitVoxel, EntityManager);
                    if (isDebugHitNormal)
                    {
                        GUILayout.Label("Hit Normal: " + raycastVoxel.hitNormal);
                        GUILayout.Label("Hit Normal Voxel");
                        DrawVoxelHitInfoUI(in raycastVoxel.normalVoxel, EntityManager);
                    }
                }
                else if (raycastVoxel.rayHitType == (byte) RayHitType.Character)
                {
                    var hitCharacterEntity = EntityManager.GetComponentData<Target>(characterEntity).target;
                    GUILayout.Label("Hit Character [" + hitCharacterEntity.Index + "]");
                }

                /*if (raycastVoxel.hitVoxel.chunk.Index != 0)
                {
                    var chunkEntity = raycastVoxel.hitVoxel.chunk;
                    var chunkPosition = EntityManager.GetComponentData<ChunkPosition>(chunkEntity);
                    var chunk = EntityManager.GetComponentData<Zoxel.Voxels.Chunk>(chunkEntity);
                    //var localPosition = VoxelUtilities.GetLocalPosition(raycastVoxel.hitPosition, chunkPosition.position, chunk.voxelDimensions);
                    GUILayout.Label("Local Voxel Position: " + raycastVoxel.hitVoxel.localPosition);
                    var selectedChunkPosition = VoxelUtilities.GetChunkPosition(raycastVoxel.hitPosition, chunk.voxelDimensions);
                    GUILayout.Label("Chunk Position: " + selectedChunkPosition);
                    var selectedMegaChunkPosition = VoxelUtilities.GetMegaChunkPosition(selectedChunkPosition, MaterialsManager.instance.renderSettings.megaChunkDivision);
                    GUILayout.Label("Mega Chunk Position: " + selectedMegaChunkPosition);
                    GUILayout.Label("Selected Chunk: " + chunkPosition.position + ", Chunk Index: " + chunkEntity.Index);
                    // lights
                    var chunkLights = EntityManager.GetComponentData<ChunkLights>(chunkEntity);
                    var index = VoxelUtilities.GetVoxelArrayIndex(localPosition, chunk.voxelDimensions);
                    GUILayout.Label("Voxel Value: " + chunk.voxels[index]);
                    GUILayout.Label("Light Value: " + chunkLights.lights[index]);
                }*/
            }
        }

        public void DrawVoxelHitInfoUI(in VoxelHitInfo voxelHitInfo, EntityManager EntityManager)
        {
            UnityEngine.GUILayout.Label("Chunk [" + voxelHitInfo.chunk.Index + "]");
            UnityEngine.GUILayout.Label(" - Voxel Index [" + voxelHitInfo.voxelIndex + "]");
            UnityEngine.GUILayout.Label(" - Globally [" + voxelHitInfo.position + "]");
            UnityEngine.GUILayout.Label(" - Locally [" + voxelHitInfo.localPosition + "]");
            UnityEngine.GUILayout.Label(" - Chunk Position [" + voxelHitInfo.chunkPosition + "]");
            // UnityEngine.GUILayout.Label("   Chunk: " + voxelHitInfo.chunk.Index);
            // var selectedChunkPosition = VoxelUtilities.GetChunkPosition(raycastVoxel.hitPosition, chunk.voxelDimensions);
            if (isDebugMegaChunkPosition)
            {
                //var selectedMegaChunkPosition = VoxelUtilities.GetMegaChunkPosition(voxelHitInfo.chunkPosition,
                //    MaterialsManager.instance.renderSettings.megaChunkDivision);
                //UnityEngine.GUILayout.Label("   Mega Chunk Position: " + selectedMegaChunkPosition);
            }
            // UnityEngine.GUILayout.Label("Selected Chunk: " + chunkPosition + ", Chunk Index: " + chunk.Index);
            // lights
            if (EntityManager.Exists(voxelHitInfo.chunk))
            {
                var chunkComponent = EntityManager.GetComponentData<Chunk>(voxelHitInfo.chunk);
                if (isDebugChunkLights)
                {
                    var index = VoxelUtilities.GetVoxelArrayIndex(voxelHitInfo.localPosition, chunkComponent.voxelDimensions);
                    var chunkLights = EntityManager.GetComponentData<ChunkLights>(voxelHitInfo.chunk);
                    if (index < chunkLights.lights.Length)
                    {
                        UnityEngine.GUILayout.Label("   Light Value: " + chunkLights.lights[index]);
                    }
                    else
                    {
                        UnityEngine.GUILayout.Label("   Lights Length: " + chunkLights.lights.Length);
                    }
                }

                if (isDebugRotations)
                {
                    var chunkVoxelRotations = EntityManager.GetComponentData<ChunkVoxelRotations>(voxelHitInfo.chunk);
                    UnityEngine.GUILayout.Label("   Rotations: " + chunkVoxelRotations.Count());
                }
                if (isDebugChunkRenders)
                {
                    var chunkRenderLinks = EntityManager.GetComponentData<ChunkRenderLinks>(voxelHitInfo.chunk);
                    UnityEngine.GUILayout.Label("   chunkRenderLinks: " + chunkRenderLinks.chunkRenders.Length);
                    for (int i = 0; i < chunkRenderLinks.chunkRenders.Length; i++)
                    {
                        UnityEngine.GUILayout.Label("       Chunk Render [" + i + "]: " + chunkRenderLinks.chunkRenders[i].Index);
                    }
                }

                if (isDebugStructures && EntityManager.HasComponent<StructureLinks>(voxelHitInfo.chunk))
                {
                    var structureLinks = EntityManager.GetComponentData<StructureLinks>(voxelHitInfo.chunk);
                    UnityEngine.GUILayout.Label("   StructureLinks: " + structureLinks.structures.Length);
                    // var buffer = 3;
                    for (int i = 0; i < structureLinks.structures.Length; i++)
                    {
                        var structureEntity = structureLinks.structures[i];
                        var treeTranslation = EntityManager.GetComponentData<Translation>(structureEntity).Value;
					    var globalPosition = new int3((int) math.floor(treeTranslation.x), (int) math.floor(treeTranslation.y), (int) math.floor(treeTranslation.z));
                        var structure = EntityManager.GetComponentData<Structure>(structureEntity);
                        if (isDebugStructuresAll)
                        {
                            UnityEngine.GUILayout.Label("   Tree [" + i + "]: " + structure.direction + " :: " + globalPosition);
                        }
                        /*if (globalPosition.x - buffer >= voxelHitInfo.position.x && globalPosition.x + buffer <= voxelHitInfo.position.x
                            && globalPosition.y - buffer >= voxelHitInfo.position.y && globalPosition.y + buffer <= voxelHitInfo.position.y
                            && globalPosition.z - buffer >= voxelHitInfo.position.z && globalPosition.z + buffer <= voxelHitInfo.position.z)*/
                        if (globalPosition == voxelHitInfo.position)
                        {
                            UnityEngine.GUILayout.Label("       Tree Selected [" + i + "]: " + structure.direction);
                            break;
                        }
                    }
                }
                if ((isDebugMinivoxes || isDebugBigMinivoxes) && EntityManager.HasComponent<MinivoxLinks>(voxelHitInfo.chunk))
                {
                    var minivoxLinks = EntityManager.GetComponentData<MinivoxLinks>(voxelHitInfo.chunk);
                    if (minivoxLinks.minivoxes.IsCreated)
                    {
                        UnityEngine.GUILayout.Label("       Chunk has Minivoxes [" + minivoxLinks.minivoxes.Count() + "]");
                        UnityEngine.GUILayout.Label("           - Capacity [" + minivoxLinks.minivoxes.Capacity + "]");
                        UnityEngine.GUILayout.Label("           - Removed [" + minivoxLinks.removedCount + "]");
                        UnityEngine.GUILayout.Label("           - Pool [" + minivoxLinks.GetPoolCount() + "]");
                        UnityEngine.GUILayout.Label(" --- --- --- --- --- --- --- --- --- ---");
                        foreach (var KVP in minivoxLinks.minivoxes)
                        {
                            var minivoxPosition = KVP.Key;
                            var minivoxEntity = KVP.Value;
                            if (minivoxEntity.Index <= 0)
                            {
                                continue;
                            }
                            if (isDebugMinivoxes && !EntityManager.HasComponent<BigMinivox>(minivoxEntity)) //  EntityManager.HasComponent<BasicMinivox>(minivoxEntity))
                            {
                                var multiVoxelPosition = EntityManager.GetComponentData<VoxelPosition>(minivoxEntity);
                                UnityEngine.GUILayout.Label("           - Minivox [" + minivoxPosition + "]: At " + multiVoxelPosition.position + ".");
                            }
                            if (isDebugBigMinivoxes && EntityManager.HasComponent<BigMinivox>(minivoxEntity))
                            {
                                var multiVoxelPosition = EntityManager.GetComponentData<MultiVoxelPosition>(minivoxEntity);
                                UnityEngine.GUILayout.Label("           - BigMinivox [" + minivoxPosition + "]: " + multiVoxelPosition.positions.Length + " positions.");
                            }
                        }
                    }
                    else
                    {
                        UnityEngine.GUILayout.Label("       Chunk has no Minivoxes.");
                    }
                }
                if (isDebugQuadrants)
                {
                    var chunkGravityQuadrant = EntityManager.GetComponentData<ChunkGravityQuadrant>(voxelHitInfo.chunk);
                        var hasHeightsDown = chunkGravityQuadrant.quadrantA.GetBoolA();
                        var hasHeightsUp = chunkGravityQuadrant.quadrantB.GetBoolA();
                        var hasHeightsLeft = chunkGravityQuadrant.quadrantA.GetBoolB();
                        var hasHeightsRight = chunkGravityQuadrant.quadrantB.GetBoolB();
                        var hasHeightsBackward = chunkGravityQuadrant.quadrantA.GetBoolC();
                        var hasHeightsForward = chunkGravityQuadrant.quadrantB.GetBoolC();
                    UnityEngine.GUILayout.Label("   Gravity Quadrant: " 
                        + " Down: " + hasHeightsDown
                        + ", Up: " + hasHeightsUp
                        + ", Left: " + hasHeightsLeft
                        + ", Right: " + hasHeightsRight
                        + ", Backward: " + hasHeightsBackward
                        + ", Forward: " + hasHeightsForward);
                }
                if (isDebugGravity)
                {
                    var chunkGravity = EntityManager.GetComponentData<ChunkGravity>(voxelHitInfo.chunk);
                    if (!chunkGravity.gravity.IsOpen)
                    {
                        UnityEngine.GUILayout.Label("   Gravity: " + chunkGravity.gravity.value);
                    }
                    else
                    {
                        UnityEngine.GUILayout.Label("   Gravity: " 
                            + " 1: " + chunkGravity.gravity[0].value
                            + ", 2: " + chunkGravity.gravity[1].value
                            + ", 3: " + chunkGravity.gravity[2].value
                            + ", 4: " + chunkGravity.gravity[3].value
                            + ", 5: " + chunkGravity.gravity[4].value
                            + ", 6: " + chunkGravity.gravity[5].value
                            + ", 7: " + chunkGravity.gravity[6].value
                            + ", 8: " + chunkGravity.gravity[7].value);
                    }
                }
                if (isDebugLights)
                {
                    var index = VoxelUtilities.GetVoxelArrayIndex(voxelHitInfo.localPosition, chunkComponent.voxelDimensions);
                    var chunkLights = EntityManager.GetComponentData<ChunkLights>(voxelHitInfo.chunk);
                    var lightValue = chunkLights.lights[index];
                    UnityEngine.GUILayout.Label("   Light: " + lightValue);
                }
            }
            else
            {
                UnityEngine.GUILayout.Label("   Chunk does not exist.");
            }
        }

        /*private void DrawRaycaster(Entity player)
        {
            if (EntityManager.HasComponent<RaycastVoxel>(player))
            {
                var raycastVoxel = booty.GetSystems().space.EntityManager.GetComponentData<RaycastVoxel>(player);
                GUILayout.Label("Voxel");
                GUILayout.Label("   Hit Position: " + (raycastVoxel.voxelPosition - raycastVoxel.hitNormal).ToString());
                GUILayout.Label("   Type: " + raycastVoxel.hitVoxelIndex.ToString());
                GUILayout.Label("   Place Position: " + raycastVoxel.voxelPosition.ToString());
                GUILayout.Label("   Light: " + raycastVoxel.hitLightValue.ToString());
                GUILayout.Label("   HitNormal: " + raycastVoxel.hitNormal.ToString());
                if (booty.EntityManager.Exists(raycastVoxel.chunk))
                {
                    var chunkPosition = booty.GetSystems().space.EntityManager.GetComponentData<ChunkPosition>(raycastVoxel.chunk);
                    GUILayout.Label("Chunk Index [" + raycastVoxel.chunk.Index.ToString() + "] at Position: " + chunkPosition.position.ToString());
                }
            }
        }*/
    }
}
                /*for (int i = 0; i < chunkVoxelRotations.rotations.Length; i++)
                {
                    UnityEngine.GUILayout.Label("       rot [" + i + "]: " + chunkVoxelRotations.rotations[i].position);
                }*/