using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    //! Tag for a vox emoticon.
    /**
    *   Expresses emotions for a character. Or displays their status.
    */
    public struct VoxEmoticon : IComponentData { }
}