using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;

namespace Zoxel.VoxelInteraction
{
    //! Sets block place rotation after RaycasterOrigin changes.
    [UpdateAfter(typeof(RaycastSystem))]
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class RaycastOrientationSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            var radianToDegrees = 360f / (math.PI * 2);
            //! Bases hit direction on quadrant. Uses GravityQuadrant unlike previous function.
            Entities
                .WithChangeFilter<RaycasterOrigin>()
                .WithNone<InitializeEntity, EntityLoading, EntityBusy>()
                .WithNone<DisableRaycaster>()
                .WithNone<OnRaycastMissed, OnRaycastVoxel, OnRaycastCharacter>()
                .ForEach((ref RaycastVoxel raycastVoxel, in RaycasterOrigin raycasterOrigin, in GravityQuadrant gravityQuadrant) =>
            {
                var direction = raycasterOrigin.rayQuaternion;
                direction = math.mul(math.inverse(gravityQuadrant.rotation), direction);
                raycastVoxel.hitAngle = direction.ToEulerSlow();
                ///! \todo Test hit angle using ui.
                // raycastVoxel.hitAngle = direction.ToEuler();
                raycastVoxel.blockRotationRegion = GetHitRotation(gravityQuadrant.quadrant, raycastVoxel.hitAngle.y);
                // UnityEngine.Debug.LogError("Block Region: " + raycastVoxel.blockRotationRegion + " : " + angleYDegrees);
            }).ScheduleParallel();
        }

        public static byte GetHitRotation(byte quadrant, float hitAngleY)
        {
            // var hitAngleY = hitAngle.y;
            // todo: fix this for planet sides
            // blockRotationRegion = (byte) 0;
            // if (hitAngleY >= -0.25f && hitAngleY <= 0.25f)
            if (hitAngleY >= 315 || hitAngleY <= 45)
            {
                return BlockRotationRegion.Forward; // 2;
            }
            // else if (hitAngleY > 0.25f && hitAngleY <= 0.75f)
            else if (hitAngleY >= 45 && hitAngleY <= 135)
            {
                return BlockRotationRegion.Left; // 4;
            }
            else if (hitAngleY >= 135 && hitAngleY <= 225)
            {
                return BlockRotationRegion.Backward; // 1;
            }
            // else if (hitAngleY >= -0.75f && hitAngleY < -0.25f)
            else //  if (hitAngleY >= 225 && hitAngleY <= 315)
            {
                return BlockRotationRegion.Right; // 3;
            }
        }
    }
}
            // else // if (hitAngle < -0.25f && hitAngle >= -0.75f)
            /*if (quadrant == PlanetSide.Left)
            {
                // hitAngleY = hitAngle.x;
                hitAngleY = hitAngle.z;
                if (hitAngleY >= -0.25f && hitAngleY <= 0.25f)
                {
                    blockRotationRegion = BlockRotationRegion.Left;
                }
                else if (hitAngleY > 0.25f && hitAngleY <= 0.75f)
                {
                    blockRotationRegion = BlockRotationRegion.Forward;
                }
                else if (hitAngleY >= -0.75f && hitAngleY < -0.25f)
                {
                    blockRotationRegion = BlockRotationRegion.Backward;
                }
                else // if (hitAngle < -0.25f && hitAngle >= -0.75f)
                {
                    blockRotationRegion = BlockRotationRegion.Right;
                }
                return;
            }
            else if (quadrant == PlanetSide.Right)
            {
                // hitAngleY = hitAngle.x;
                hitAngleY = hitAngle.z;
                if (hitAngleY >= -0.25f && hitAngleY <= 0.25f)
                {
                    blockRotationRegion = BlockRotationRegion.Right;
                }
                else if (hitAngleY > 0.25f && hitAngleY <= 0.75f)
                {
                    blockRotationRegion = BlockRotationRegion.Backward;
                }
                else if (hitAngleY >= -0.75f && hitAngleY < -0.25f)
                {
                    blockRotationRegion = BlockRotationRegion.Forward;
                }
                else // if (hitAngle < -0.25f && hitAngle >= -0.75f)
                {
                    blockRotationRegion = BlockRotationRegion.Left;
                }
                return;
            }
            else if (quadrant == PlanetSide.Backward)
            {
                hitAngleY = hitAngle.z;
                if (hitAngleY >= -0.25f && hitAngleY <= 0.25f)
                {
                    blockRotationRegion = BlockRotationRegion.Forward;
                }
                else if (hitAngleY > 0.25f && hitAngleY <= 0.75f)
                {
                    blockRotationRegion = BlockRotationRegion.Left;
                }
                else if (hitAngleY >= -0.75f && hitAngleY < -0.25f)
                {
                    blockRotationRegion = BlockRotationRegion.Right;
                }
                else // if (hitAngle < -0.25f && hitAngle >= -0.75f)
                {
                    blockRotationRegion = BlockRotationRegion.Backward;
                }
                return;
            }
            else if (quadrant == PlanetSide.Forward)
            {
                hitAngleY = hitAngle.z;
                if (hitAngleY >= -0.25f && hitAngleY <= 0.25f)
                {
                    blockRotationRegion = BlockRotationRegion.Backward;
                }
                else if (hitAngleY > 0.25f && hitAngleY <= 0.75f)
                {
                    blockRotationRegion = BlockRotationRegion.Left;
                }
                else if (hitAngleY >= -0.75f && hitAngleY < -0.25f)
                {
                    blockRotationRegion = BlockRotationRegion.Right;
                }
                else // if (hitAngle < -0.25f && hitAngle >= -0.75f)
                {
                    blockRotationRegion = BlockRotationRegion.Forward;
                }
                return;
            }
            else if (quadrant == PlanetSide.Down)
            {
                if (hitAngleY >= -0.25f && hitAngleY <= 0.25f)
                {
                    blockRotationRegion = BlockRotationRegion.Backward;
                }
                else if (hitAngleY > 0.25f && hitAngleY <= 0.75f)
                {
                    blockRotationRegion = BlockRotationRegion.Right;
                }
                else if (hitAngleY >= -0.75f && hitAngleY < -0.25f)
                {
                    blockRotationRegion = BlockRotationRegion.Left;
                }
                else // if (hitAngle < -0.25f && hitAngle >= -0.75f)
                {
                    blockRotationRegion = BlockRotationRegion.Forward;
                }
                return;
            }
            else*/