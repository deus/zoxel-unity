using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Cameras;

namespace Zoxel.VoxelInteraction
{
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class RaycastOriginSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithNone<DisableRaycaster, CameraLink, MouseRaycaster>()
                .WithChangeFilter<Rotation>()
                .ForEach((Entity e, ref RaycasterOrigin raycasterOrigin, in Translation translation, in Rotation rotation) =>
            {
                raycasterOrigin.rayOrigin = translation.Value;
                var rotation2 = rotation.Value.value;
                var quat = new UnityEngine.Quaternion(rotation2.x, rotation2.y, rotation2.z, rotation2.w);
                var euler = quat.eulerAngles; // .ToEulerAngles();
                euler.Normalize();
                raycasterOrigin.rayDirection = euler;
                raycasterOrigin.rayQuaternion = rotation.Value;
            }).ScheduleParallel();
        }
    }
}