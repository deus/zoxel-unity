﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Cameras;
using Zoxel.Voxels;
using Zoxel.Voxels.Minivoxes;

namespace Zoxel.VoxelInteraction
{
    //! Handles raycasting through the world.
    /**
    *   Uses DDA now instead of intervals.
    *   Injects all chunks and characters with their bounding boxes.
    *   \todo Fix issue when loading game next to door, it doesn't select it properly accross chunks. (hard to test)
    *   \todo Use Chunk Characters instead of nearby entities for raycasting per chunk.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class RaycastSystem : SystemBase
    {
        const float characterRaycastInterval = 0.2f;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;
        private EntityQuery chunkRenderQuery;
        private EntityQuery characterQuery;
        private EntityQuery voxesQuery;
        private EntityQuery voxelsQuery;
        private EntityQuery grassesQuery;
        private EntityQuery destroyEntitiesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            chunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<EntityBusy>(),
                ComponentType.ReadOnly<PlanetChunk>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkPosition>(),
                ComponentType.ReadOnly<ChunkRenderLinks>());
            chunkRenderQuery = GetEntityQuery(
                ComponentType.ReadOnly<PlanetRender>(),
                ComponentType.ReadOnly<ChunkSides>(),
                ComponentType.Exclude<DestroyEntity>());
            characterQuery = GetEntityQuery(
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<Rotation>(),
                ComponentType.ReadOnly<Body>(),
                ComponentType.ReadOnly<Character>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            voxelsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Voxel>(),
                ComponentType.Exclude<DestroyEntity>());
            voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<StreamVox>(),
                ComponentType.Exclude<OnChunksSpawned>(),
                ComponentType.ReadOnly<VoxelLinks>());
            grassesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Grass>());
            destroyEntitiesQuery = GetEntityQuery(ComponentType.ReadOnly<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (VoxelManager.instance.voxelSettings.disableRaycasting)
            {
                return;
            }
            // replacing intervals atm with DDA
            var raycastLength = VoxelManager.instance.voxelSettings.raycastLength; // 5f; // 4f;
            var failedPosition = new int3(0, -10000, 0);
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxScales = GetComponentLookup<VoxScale>(true);
            var voxelLinks = GetComponentLookup<VoxelLinks>(true);
            var chunkLinks2 = GetComponentLookup<ChunkLinks>(true);
            voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunks2 = GetComponentLookup<Chunk>(true);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            var chunkRenderLinksArray = GetComponentLookup<ChunkRenderLinks>(true);
            var chunkNeighbors = GetComponentLookup<ChunkNeighbors>(true);
            var minivoxLinks = GetComponentLookup<MinivoxLinks>(true);
            chunkEntities.Dispose();
            var chunkRenderEntities = chunkRenderQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var chunkSides = GetComponentLookup<ChunkSides>(true);
            chunkRenderEntities.Dispose();
            var voxelEntities = voxelsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var voxels = GetComponentLookup<Voxel>(true);
            var voxelMeshTypes = GetComponentLookup<VoxelMeshType>(true);
            voxelEntities.Dispose();
            var characterEntities = characterQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleE);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleE);
            var characterPositions = GetComponentLookup<Translation>(true);
            var characterRotations = GetComponentLookup<Rotation>(true);
            var characterBodies = GetComponentLookup<Body>(true);
            characterEntities.Dispose();
            var grassEntities = grassesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleF);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleF);
            var grasses = GetComponentLookup<Grass>(true);
            grassEntities.Dispose();
            var destroyEntitiesEntities = destroyEntitiesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleG);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleG);
            var destroyEntitys = GetComponentLookup<DestroyEntity>(true);
            destroyEntitiesEntities.Dispose();
            // Add Disables
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DisableRaycaster>()
                .WithNone<OnRaycastMissed, OnRaycastVoxel, OnRaycastCharacter>()
                .WithChangeFilter<RaycasterOrigin>()
                .ForEach((Entity e, int entityInQueryIndex, ref RaycastVoxel raycastVoxel, ref RaycastCharacter raycastCharacter, in RaycasterOrigin raycasterOrigin,
                    in ChunkLink chunkLink, in VoxLink voxLink, in NearbyEntities nearbyEntities) =>
            {
                var planetEntity = voxLink.vox;
                if (!voxelLinks.HasComponent(planetEntity) || !chunks2.HasComponent(chunkLink.chunk))
                {
                    // UnityEngine.Debug.LogError("One is null: " + chunkLink.chunk.Index);
                    return;
                }
                // data
                var voxVoxels = voxelLinks[planetEntity].voxels;
                var voxScale = voxScales[planetEntity].scale;
                var chunkLinks = chunkLinks2[planetEntity];
                // starting chunk
                var newHitVoxel = new VoxelHitInfo();
                newHitVoxel.vox = planetEntity;
                var rayHitData = new RayHitData();
                // rayHitData.chunkEntity = chunkLink.chunk;
                var readOnlyChunk = new ReadOnlyChunk(chunkLink.chunk, chunks2[chunkLink.chunk], chunkPositions[chunkLink.chunk].position);
                // more data
                var voxelDimensions = readOnlyChunk.chunk.voxelDimensions;
                // rayHitData.voxelDimensions = voxelDimensions;
                // grass - put this into RayHitInfo?
                var hitGrassInfo = new VoxelHitInfo();
                hitGrassInfo.vox = planetEntity;
                var hitGrassNormal = new int3();
                // character
                var oldHitCharacter = raycastCharacter.character;
                var hitCharacter = new Entity();
                // ray itself
                var isRaycastingGrass = false;
                // Old Minivvox
                var oldHitVoxel = raycastVoxel.hitVoxel;
                var oldHitMinivox = raycastVoxel.hitVoxel.hitMinivox;
                var wasOldMinivoxSelected = oldHitMinivox.Index != 0 && HasComponent<EntitySelected>(oldHitMinivox);
                // for all points in the ray
                // our ray
                var rayOrigin = raycasterOrigin.rayOrigin;
                var rayDirection = raycasterOrigin.rayDirection;
                // step through position
                var rayGlobalVoxelPosition = VoxelUtilities.GetVoxelPosition(rayOrigin, voxScale);
                #if DEBUG_VOXEL_RAYCAST
                // Put spike lines along raycast axis
                UnityEngine.Debug.DrawLine(rayGlobalVoxelPosition.ToFloat3() - new float3(0, 0.5f, 0), rayGlobalVoxelPosition.ToFloat3() + new float3(0, 0.5f, 0), UnityEngine.Color.red);
                #endif
                // UnityEngine.Debug.LogError("Raycasting from: " + rayGlobalVoxelPosition); // rayHitData.chunkPosition);
                var rayHitEndPosition = VoxelUtilities.GetVoxelPosition(rayOrigin + rayDirection * raycastLength, voxScale);
	            var stepDirection = new int3(math.sign(rayDirection));
                // var rayDirectionMagnitude = rayDirection.x * rayDirection.x + rayDirection.y * rayDirection.y + rayDirection.z * rayDirection.z;
	            var rayUnitSize = math.abs(new float3(math.length(rayDirection) / rayDirection));
                var rayLength = new float3(); // (math.sign(rayDirection) * (rayGlobalVoxelPosition.ToFloat3() - rayOrigin) + (math.sign(rayDirection) * 0.5f) + 0.05f) * rayUnitSize;
                // This gets the distance between origin and origin voxel position to check direction of our ray
                var rayOriginScaled = new float3(rayOrigin.x / voxScale.x, rayOrigin.y / voxScale.y, rayOrigin.z / voxScale.z);
                if (rayDirection.x < 0)
                {
                    rayLength.x = (rayOriginScaled.x - rayGlobalVoxelPosition.x) * rayUnitSize.x;   // (float) 
                }
                else
                {
                    rayLength.x = ((float) rayGlobalVoxelPosition.x + 1 - rayOriginScaled.x) * rayUnitSize.x;  //  + 1
                }
                if (rayDirection.y < 0)
                {
                    rayLength.y = (rayOriginScaled.y - (float) rayGlobalVoxelPosition.y) * rayUnitSize.y;
                }
                else
                {
                    rayLength.y = ((float) rayGlobalVoxelPosition.y + 1 - rayOriginScaled.y) * rayUnitSize.y;  //  + 1
                }
                if (rayDirection.z < 0)
                {
                    rayLength.z = (rayOriginScaled.z - (float) rayGlobalVoxelPosition.z) * rayUnitSize.z;
                }
                else
                {
                    rayLength.z = ((float) rayGlobalVoxelPosition.z + 1 - rayOriginScaled.z) * rayUnitSize.z;  //  + 1
                }
                var foundCharacter = false;
                var rayDistance = 0f;
                while (rayGlobalVoxelPosition != rayHitEndPosition && !foundCharacter && rayDistance <= raycastLength)
                {
                    var localPosition = VoxelUtilities.GetLocalPosition(rayGlobalVoxelPosition, readOnlyChunk.position, voxelDimensions);
                    if (!VoxelUtilities.InBounds(localPosition, voxelDimensions))
                    {
                        ReadOnlyChunk outputChunk;
                        if (!UpdateChunk(out outputChunk, in readOnlyChunk, ref localPosition, voxelDimensions, in chunkNeighbors, in chunks2))
                        {
                            // UnityEngine.Debug.LogError("Chunk is null!");
                            #if DEBUG_VOXEL_RAYCAST
                            UnityEngine.Debug.DrawLine(rayOrigin, rayOrigin + rayDistance * rayDirection, UnityEngine.Color.red);
                            #endif
                            break;
                        }
                        readOnlyChunk = outputChunk;
                    }
                    if (IsVoxelSolid(in readOnlyChunk, ref rayHitData, voxelDimensions, localPosition))
                    {
                        // UnityEngine.Debug.LogError("Hit Voxel Solid!");
                        var voxelArrayIndex = rayHitData.voxelIndex - 1;
                        if (voxelArrayIndex >= voxVoxels.Length)
                        {
                            break;
                        }
                        newHitVoxel.voxel = voxVoxels[voxelArrayIndex];
                        var voxelData = voxels[newHitVoxel.voxel];
                        newHitVoxel.meshIndex = voxelMeshTypes[newHitVoxel.voxel].meshType;
                        // hit grass
                        if (voxelData.raycastMode == 2)
                        {
                            if (!isRaycastingGrass)
                            {
                                isRaycastingGrass = true;
                                newHitVoxel.position = rayGlobalVoxelPosition;
                                newHitVoxel.chunkPosition = readOnlyChunk.position;
                                newHitVoxel.chunk = readOnlyChunk.chunkEntity;
                                newHitVoxel.voxelIndex = rayHitData.voxelIndex;
                                newHitVoxel.localPosition = VoxelUtilities.GetLocalPosition(rayGlobalVoxelPosition, readOnlyChunk.position, voxelDimensions);
                                newHitVoxel.chunkVoxelPosition = VoxelUtilities.GetChunkVoxelPosition(readOnlyChunk.position, voxelDimensions);
                                hitGrassInfo = newHitVoxel;
                                hitGrassNormal = rayHitData.hitNormal;
                            }
                            // only raycasting characters now, for when in grass
                        }
                        // hit a solid block
                        else if (voxelData.raycastMode == 1) 
                        {
                            rayHitData.rayHitType = (byte) RayHitType.Voxel;
                            if (isRaycastingGrass)
                            {
                                newHitVoxel = hitGrassInfo;
                                rayHitData.hitNormal = hitGrassNormal;
                            }
                            #if DEBUG_VOXEL_RAYCAST
                            UnityEngine.Debug.DrawLine(rayOrigin, rayOrigin + rayDistance * rayDirection, UnityEngine.Color.green);
                            #endif
                            break;
                        }
                    }
                    //! \todo Check position we are in, if that matches nearby - a character can be within multiple voxel grid positions tho
                    // Raycast Characters
                    // I should check based along rayDistance to rayDistance + 1
                    for (var k = 0f; k <= 1f; k += characterRaycastInterval)
                    {
                        var checkPosition = rayOrigin + (rayDistance + k) * rayDirection;
                        for (int j = 0; j < nearbyEntities.count; j++)
                        {
                            var characterEntity = nearbyEntities.characters[j].character;
                            if (characterEntity.Index == 0)
                            {
                                continue;
                            }
                            var position = characterPositions[characterEntity].Value;
                            var rotation = characterRotations[characterEntity].Value;
                            var size = characterBodies[characterEntity].size;
                            //size = math.rotate(rotation, size);
                            size.x = size.z = math.max(size.x, size.z);
                            var characterLowerBounds = position - size;
                            var characterUpperBounds = position + size;
                            if (    checkPosition.x >= characterLowerBounds.x && checkPosition.x <= characterUpperBounds.x
                                &&  checkPosition.y >= characterLowerBounds.y && checkPosition.y <= characterUpperBounds.y
                                &&  checkPosition.z >= characterLowerBounds.z && checkPosition.z <= characterUpperBounds.z)
                            {
                                // UnityEngine.Debug.LogError("Hit Character!");
                                rayHitData.rayHitType = (byte) RayHitType.Character;
                                hitCharacter = characterEntity;
                                foundCharacter = true;
                                k = 1f;
                                break;
                            }
                        }
                    }
                    // Increment Ray Position - RayLength X is smallest
                    if (rayLength.x < rayLength.y)  // rayLength.x < rayLength.y && 
                    {
                        if (rayLength.x < rayLength.z)
                        {
                            rayGlobalVoxelPosition.x += stepDirection.x;  // moves position to right
                            rayDistance = rayLength.x;
                            rayLength.x += rayUnitSize.x;
                            if (stepDirection.x >= 0)
                            {
                                rayHitData.hitNormal = int3.left;
                            }
                            else
                            {
                                rayHitData.hitNormal = int3.right;
                            }
                            #if DEBUG_VOXEL_RAYCAST
                            // Put spike lines along raycast axis
                            UnityEngine.Debug.DrawLine(rayGlobalVoxelPosition.ToFloat3() - new float3(0, 0.3f, 0), rayGlobalVoxelPosition.ToFloat3() + new float3(0, 0.3f, 0), UnityEngine.Color.red);
                            // Debug, moved X
                            var rayStepPosition = (rayGlobalVoxelPosition - new int3(stepDirection.x, 0, 0)).ToFloat3() + new float3(0.5f, 0.5f, 0.5f);
                            UnityEngine.Debug.DrawLine(rayStepPosition, rayGlobalVoxelPosition.ToFloat3() + new float3(0.5f, 0.5f, 0.5f), UnityEngine.Color.cyan);
                            #endif
                        }
                        else
                        {
                            rayGlobalVoxelPosition.z += stepDirection.z;
                            rayDistance = rayLength.z;
                            rayLength.z += rayUnitSize.z;
                            if (stepDirection.z >= 0)
                            {
                                rayHitData.hitNormal = int3.back;
                            }
                            else
                            {
                                rayHitData.hitNormal = int3.forward;
                            }
                            
                            #if DEBUG_VOXEL_RAYCAST
                            var rayStepPosition = (rayGlobalVoxelPosition - new int3(0, 0, stepDirection.z)).ToFloat3() + new float3(0.5f, 0.5f, 0.5f);
                            UnityEngine.Debug.DrawLine(rayStepPosition, rayGlobalVoxelPosition.ToFloat3() + new float3(0.5f, 0.5f, 0.5f), UnityEngine.Color.cyan);
                            #endif
                        }
                    }
                    // RayLength Z is smallest
                    else // if (rayLength.z < rayLength.x && rayLength.z < rayLength.y)
                    {
                        if (rayLength.y < rayLength.z)
                        {
                            rayGlobalVoxelPosition.y += stepDirection.y;
                            rayDistance = rayLength.y;
                            rayLength.y += rayUnitSize.y;
                            if (stepDirection.y >= 0)
                            {
                                rayHitData.hitNormal = int3.down;
                            }
                            else
                            {
                                rayHitData.hitNormal = int3.up;
                            }
                            #if DEBUG_VOXEL_RAYCAST
                            var rayStepPosition = (rayGlobalVoxelPosition - new int3(0, stepDirection.y, 0)).ToFloat3() + new float3(0.5f, 0.5f, 0.5f);
                            UnityEngine.Debug.DrawLine(rayStepPosition, rayGlobalVoxelPosition.ToFloat3() + new float3(0.5f, 0.5f, 0.5f), UnityEngine.Color.cyan);
                            #endif
                        }
                        else
                        {
                            rayGlobalVoxelPosition.z += stepDirection.z;
                            rayDistance = rayLength.z;
                            rayLength.z += rayUnitSize.z;
                            if (stepDirection.z >= 0)
                            {
                                rayHitData.hitNormal = int3.back;
                            }
                            else
                            {
                                rayHitData.hitNormal = int3.forward;
                            }
                            #if DEBUG_VOXEL_RAYCAST
                            var lineStepPosition = (rayGlobalVoxelPosition - new int3(0, 0, stepDirection.z)).ToFloat3() + new float3(0.5f, 0.5f, 0.5f);
                            UnityEngine.Debug.DrawLine(lineStepPosition, rayGlobalVoxelPosition.ToFloat3() + new float3(0.5f, 0.5f, 0.5f), UnityEngine.Color.cyan);
                            #endif
                        }
                    }
                }

                //! Now handle new position raycast has hit
                //      This section shouldn't be so fat?
                if (rayHitData.rayHitType == (byte) RayHitType.None)
                {
                    if (isRaycastingGrass)
                    {
                        rayHitData.rayHitType = (byte) RayHitType.Voxel;
                        newHitVoxel = hitGrassInfo;
                        rayHitData.hitNormal = hitGrassNormal;
                    }
                }
                else if (rayHitData.rayHitType == (byte) RayHitType.Voxel)
                {
                    if (!isRaycastingGrass)
                    {
                        // UnityEngine.Debug.LogError("Hit Voxel at: " + rayGlobalVoxelPosition);
                        newHitVoxel.position = rayGlobalVoxelPosition;
                        newHitVoxel.voxelIndex = rayHitData.voxelIndex;
                        newHitVoxel.chunkPosition = readOnlyChunk.position;
                        newHitVoxel.chunk = readOnlyChunk.chunkEntity;
                        newHitVoxel.localPosition =  VoxelUtilities.GetLocalPosition(rayGlobalVoxelPosition, readOnlyChunk.position, voxelDimensions);
                        newHitVoxel.chunkVoxelPosition = VoxelUtilities.GetChunkVoxelPosition(readOnlyChunk.position, voxelDimensions);
                    }
                }
                newHitVoxel.hitMinivox = new Entity();
                var lastRayHitType = raycastVoxel.rayHitType;
                var lastHitNormal = raycastVoxel.hitNormal;
                raycastVoxel.rayHitType = rayHitData.rayHitType;
                raycastVoxel.hitNormal = rayHitData.hitNormal;
                if (rayHitData.rayHitType == (byte) RayHitType.None || (rayHitData.rayHitType == (byte) RayHitType.Voxel && newHitVoxel.chunk.Index == 0))
                {
                    if (lastRayHitType != rayHitData.rayHitType)
                    {
                        PostUpdateCommands.AddComponent<OnRaycastMissed>(entityInQueryIndex, e);
                        raycastVoxel.OnHitFailed();
                        raycastCharacter.OnHitFailed();
                    }
                }
                else if (rayHitData.rayHitType == (byte) RayHitType.Voxel)
                {
                    var chunkRenderLinks = chunkRenderLinksArray[newHitVoxel.chunk];
                    if (chunkRenderLinks.chunkRenders.Length > 0)
                    {
                        var chunkSide = chunkSides[chunkRenderLinks.chunkRenders[0]];
                        if (rayHitData.voxelArrayIndex < chunkSide.sides.Length)
                        {
                            newHitVoxel.sides = chunkSide.sides[rayHitData.voxelArrayIndex];
                        }
                    }
                    if (!(oldHitVoxel.voxelIndex != newHitVoxel.voxelIndex || newHitVoxel.position != oldHitVoxel.position
                        || lastHitNormal != rayHitData.hitNormal || oldHitVoxel.sides != newHitVoxel.sides))
                    {
                        if (minivoxLinks.HasComponent(newHitVoxel.chunk))
                        {
                            var minivoxLinks2 = minivoxLinks[newHitVoxel.chunk];
                            newHitVoxel.hitMinivox = minivoxLinks2.GetMinivox(newHitVoxel.position);
                        }
                        if (SetSelectedMinivox(PostUpdateCommands, entityInQueryIndex, ref raycastVoxel,
                            oldHitMinivox, newHitVoxel, wasOldMinivoxSelected, elapsedTime))
                        {
                            if (newHitVoxel.hitMinivox.Index != 0)
                            {
                                PostUpdateCommands.AddComponent<OnRaycastVoxel>(entityInQueryIndex, e);
                                if (grasses.HasComponent(newHitVoxel.hitMinivox))
                                {
                                    PostUpdateCommands.AddComponent<OnRaycastGrass>(entityInQueryIndex, e);
                                }
                                else
                                {
                                    PostUpdateCommands.AddComponent<OnRaycastMinivox>(entityInQueryIndex, e);
                                }
                            }
                        }
                        return;
                    }
                    if (minivoxLinks.HasComponent(newHitVoxel.chunk))
                    {
                        newHitVoxel.hitMinivox = minivoxLinks[newHitVoxel.chunk].GetMinivox(newHitVoxel.position);
                    }
                    // newHitVoxel.hitMinivox = newMinivoxHit;
                    // raycastVoxel.hitVoxel = newHitVoxel;
                    // newMinivoxHit = newHitVoxel;
                    // calculate normal position from hit position now, update chunk info if its outside hit voxel chunk
                    var normalVoxel = raycastVoxel.normalVoxel;
                    normalVoxel.vox = planetEntity;
                    normalVoxel.position = newHitVoxel.position + rayHitData.hitNormal;
                    // UnityEngine.Debug.LogError("Raycast - Raycast Hit: " + rayHitData.hitNormal);
                    normalVoxel.chunkPosition = VoxelUtilities.GetChunkPosition(normalVoxel.position, voxelDimensions);
                    normalVoxel.localPosition = VoxelUtilities.GetLocalPosition(normalVoxel.position, normalVoxel.chunkPosition, voxelDimensions);
                    if (newHitVoxel.chunkPosition == normalVoxel.chunkPosition)
                    {
                        normalVoxel.chunk = newHitVoxel.chunk;
                    }
                    else
                    {
                        Entity chunkEntity;
                        if (chunkLinks.chunks.TryGetValue(normalVoxel.chunkPosition, out chunkEntity))
                        {
                            normalVoxel.chunk = chunkEntity;
                        }
                    }
                    // set type of normal
                    if (chunks2.HasComponent(normalVoxel.chunk))
                    {
                        var hitNormalChunk = chunks2[normalVoxel.chunk];
                        var voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(normalVoxel.localPosition, voxelDimensions);
                        if (voxelArrayIndex >= 0 && voxelArrayIndex < hitNormalChunk.voxels.Length)
                        {
                            normalVoxel.voxelIndex = hitNormalChunk.voxels[voxelArrayIndex];
                        }
                    }
                    raycastVoxel.normalVoxel = normalVoxel;
                    PostUpdateCommands.AddComponent<OnRaycastVoxel>(entityInQueryIndex, e);
                    if (newHitVoxel.hitMinivox.Index != 0)
                    {
                        if (grasses.HasComponent(newHitVoxel.hitMinivox))
                        {
                            PostUpdateCommands.AddComponent<OnRaycastGrass>(entityInQueryIndex, e);
                        }
                        else
                        {
                            PostUpdateCommands.AddComponent<OnRaycastMinivox>(entityInQueryIndex, e);
                        }
                    }
                }
                else if (rayHitData.rayHitType == (byte) RayHitType.Character)
                {
                    if (raycastCharacter.character != hitCharacter || newHitVoxel.voxelIndex != 0)
                    {
                        PostUpdateCommands.AddComponent<OnRaycastCharacter>(entityInQueryIndex, e);
                    }
                    raycastVoxel.OnHitCharacter();
                    raycastCharacter.hitCharacterHeight = characterBodies[hitCharacter].size.y;
                    raycastCharacter.hitCharacterPosition = characterPositions[hitCharacter].Value;
                }
                if (oldHitCharacter != hitCharacter)
                {
                    if (oldHitCharacter.Index != 0 && HasComponent<EntitySelected>(oldHitCharacter) && !destroyEntitys.HasComponent(oldHitCharacter))
                    {
                        PostUpdateCommands.RemoveComponent<EntitySelected>(entityInQueryIndex, oldHitCharacter);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, oldHitCharacter, new OnEntityDeselected(elapsedTime));
                        if (HasComponent<OnEntitySelected>(oldHitCharacter))
                        {
                            PostUpdateCommands.RemoveComponent<OnEntitySelected>(entityInQueryIndex, oldHitCharacter);
                        }
                    }
                    raycastCharacter.character = hitCharacter;
                    if (hitCharacter.Index != 0 && !destroyEntitys.HasComponent(hitCharacter))
                    {
                        PostUpdateCommands.AddComponent<EntitySelected>(entityInQueryIndex, hitCharacter);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, hitCharacter, new OnEntitySelected(elapsedTime));
                        if (HasComponent<OnEntityDeselected>(hitCharacter))
                        {
                            PostUpdateCommands.RemoveComponent<OnEntityDeselected>(entityInQueryIndex, hitCharacter);
                        }
                    }
                }
                SetSelectedMinivox(PostUpdateCommands, entityInQueryIndex, ref raycastVoxel,
                    oldHitMinivox, newHitVoxel, wasOldMinivoxSelected, elapsedTime);
            })  .WithReadOnly(voxScales)
                .WithReadOnly(voxelLinks)
                .WithReadOnly(chunkLinks2)
                .WithReadOnly(chunkPositions)
                .WithReadOnly(chunks2)
                .WithReadOnly(chunkRenderLinksArray)
                .WithReadOnly(chunkNeighbors)
                .WithReadOnly(minivoxLinks)
                .WithReadOnly(chunkSides)
                .WithReadOnly(voxels)
                .WithReadOnly(voxelMeshTypes)
                .WithReadOnly(characterPositions)
                .WithReadOnly(characterRotations)
                .WithReadOnly(characterBodies)
                .WithReadOnly(grasses)
                .WithReadOnly(destroyEntitys)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static bool SetSelectedMinivox(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, ref RaycastVoxel raycastVoxel,
            Entity oldHitMinivox, VoxelHitInfo newHitVoxel, bool wasOldMinivoxSelected, double elapsedTime)
        {
            if (oldHitMinivox == newHitVoxel.hitMinivox)
            {
                raycastVoxel.hitVoxel = newHitVoxel;
                return false;
            }
            if (wasOldMinivoxSelected)
            {
                PostUpdateCommands.RemoveComponent<EntitySelected>(entityInQueryIndex, oldHitMinivox);
                PostUpdateCommands.AddComponent(entityInQueryIndex, oldHitMinivox, new OnEntityDeselected(elapsedTime));
                //if (HasComponent<OnEntitySelected>(oldHitMinivox))
                {
                    PostUpdateCommands.RemoveComponent<OnEntitySelected>(entityInQueryIndex, oldHitMinivox);
                }
                // UnityEngine.Debug.LogError("OnEntityDeselected! aweaweawe");
            }
            raycastVoxel.hitVoxel = newHitVoxel;
            // var newHitVoxel = raycastVoxel.hitVoxel;
            // newHitVoxel.hitMinivox = newMinivoxHit;
            // raycastVoxel.hitVoxel = newHitVoxel;
            if (newHitVoxel.hitMinivox.Index != 0)
            {
                PostUpdateCommands.AddComponent<EntitySelected>(entityInQueryIndex, newHitVoxel.hitMinivox);
                PostUpdateCommands.AddComponent(entityInQueryIndex, newHitVoxel.hitMinivox, new OnEntitySelected(elapsedTime));
                //if (HasComponent<OnEntityDeselected>(newMinivoxHit))
                {
                    PostUpdateCommands.RemoveComponent<OnEntityDeselected>(entityInQueryIndex, newHitVoxel.hitMinivox);
                }
            }
            return true;
        }

        private static bool UpdateChunk(out ReadOnlyChunk outOnlyChunk, in ReadOnlyChunk lastChunk, ref int3 localPosition, int3 voxelDimensions,
            in ComponentLookup<ChunkNeighbors> chunkNeighbors, in ComponentLookup<Chunk> chunks)
        {
            // UnityEngine.Debug.LogError("Old Hit Chunk: " + rayHitData.chunkEntity.Index);
            var chunkNeighbors2 = chunkNeighbors[lastChunk.chunkEntity];
            var chunkPosition = lastChunk.position;
            var chunkEntity = chunkNeighbors2.GetChunk(ref localPosition, ref chunkPosition, voxelDimensions);
            if (chunkEntity.Index == 0 || !chunks.HasComponent(chunkEntity))
            {
                // UnityEngine.Debug.LogError("Hit Chunk Entity is Empty");
                outOnlyChunk = new ReadOnlyChunk();
                return false;
            }
            outOnlyChunk = new ReadOnlyChunk(chunkEntity, chunks[chunkEntity], chunkPosition);
            //UnityEngine.Debug.LogError("New Hit Chunk: " + rayHitData.chunkEntity.Index);
            return true;
        }

        //! Returns true if hits solid.. Returns false if chunk is invalid, if index out of bounds, finally if hits air.
        private static bool IsVoxelSolid(in ReadOnlyChunk readOnlyChunk, ref RayHitData rayHitData, int3 voxelDimensions, int3 localPosition)
        {
            // var localPosition = VoxelUtilities.GetLocalPosition(globalPosition, rayHitData.chunkPosition, voxelDimensions);
            var voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
            if (!(voxelArrayIndex <= readOnlyChunk.chunk.voxels.Length))
            {
                return false;
            }
            rayHitData.voxelIndex = readOnlyChunk.chunk.voxels[voxelArrayIndex];
            rayHitData.voxelArrayIndex = voxelArrayIndex;
            return rayHitData.voxelIndex != 0;
        }

        private struct RayHitData
        {
            public byte rayHitType;
            public int3 hitNormal;
            public byte voxelIndex;
            public int voxelArrayIndex; // used for voxel sides
        }

        private struct ReadOnlyChunk
        {
            public readonly Entity chunkEntity;
            public readonly Chunk chunk;
            public readonly int3 position;

            public ReadOnlyChunk(in Entity chunkEntity, in Chunk chunk, in int3 position)
            {
                this.chunkEntity = chunkEntity;
                this.chunk = chunk;
                this.position = position;
            }
        }
    }
}
            // public int3 globalPosition;
            // public int3 chunkPosition;
            // public Entity chunkEntity;
            // public int3 localPosition;
            // public int3 voxelDimensions;
            
            // var readChunk = readOnlyChunk.chunk;
            /*if (!VoxelUtilities.InBounds(localPosition, voxelDimensions))
            {
                // UnityEngine.Debug.LogError("Old Hit Chunk: " + rayHitData.chunkEntity.Index);
                var chunkNeighbors2 = chunkNeighbors[rayHitData.chunkEntity];
                rayHitData.chunkEntity = chunkNeighbors2.GetChunk(ref localPosition, ref rayHitData.chunkPosition, voxelDimensions);
                if (rayHitData.chunkEntity.Index == 0)
                {
                    // UnityEngine.Debug.LogError("Hit Chunk Entity is Empty");
                    outOnlyChunk = new ReadOnlyChunk(readChunk);
                    return false;
                }
                readChunk = chunks[rayHitData.chunkEntity];
                //UnityEngine.Debug.LogError("New Hit Chunk: " + rayHitData.chunkEntity.Index);
            }*/
            // outOnlyChunk = new ReadOnlyChunk(readChunk);
            /*else
            {
                UnityEngine.Debug.LogError("Position In Bounds " + rayHitData.globalPosition 
                    + " - " + rayHitData.localPosition);
            }*/
            // rayHitData.globalPosition = newGlobalVoxelPosition;

/*if (newMinivoxHit != raycastVoxel.hitMinivox)
{
    if (raycastVoxel.hitMinivox.Index != 0 && HasComponent<EntitySelected>(raycastVoxel.hitMinivox))
    {
        PostUpdateCommands.RemoveComponent<EntitySelected>(entityInQueryIndex, raycastVoxel.hitMinivox);
        PostUpdateCommands.AddComponent(entityInQueryIndex, raycastVoxel.hitMinivox, new OnEntityDeselected(elapsedTime));
        if (HasComponent<OnEntitySelected>(raycastVoxel.hitMinivox))
        {
            PostUpdateCommands.RemoveComponent<OnEntitySelected>(entityInQueryIndex, raycastVoxel.hitMinivox);
        }
        // UnityEngine.Debug.LogError("OnEntityDeselected! aweaweawe");
    }
    raycastVoxel.hitMinivox = newMinivoxHit;
    if (newMinivoxHit.Index != 0)
    {
        PostUpdateCommands.AddComponent<EntitySelected>(entityInQueryIndex, newMinivoxHit);
        PostUpdateCommands.AddComponent(entityInQueryIndex, newMinivoxHit, new OnEntitySelected(elapsedTime));
        if (HasComponent<OnEntityDeselected>(raycastVoxel.hitMinivox))
        {
            PostUpdateCommands.RemoveComponent<OnEntityDeselected>(entityInQueryIndex, raycastVoxel.hitMinivox);
        }
    }
}*/
/*if (HasComponent<BlockDimensions>(newHitVoxel.voxel))
{
    newHitVoxel.bigMinivoxChunk = newHitVoxel.chunk;
    var positionIndex = minivoxLinks2.GetBigMinivoxPositionIndex(newHitVoxel.position);
    if (positionIndex != -1)
    {
        newMinivoxHit = minivoxLinks2.GetBigMinivox(newHitVoxel.position);
        if (positionIndex == 1)
        {
            newHitVoxel.minivoxOffset = int3.up;
        }
        newHitVoxel.bigMinivoxLink = minivoxLinks2.GetBigMinivoxLink(newHitVoxel.position).Clone();
    }
    if (newHitVoxel.bigMinivoxLink.positions.Length == 0)
    {
        #if DEBUG_BIGMINIVOXES_HEALTH
        UnityEngine.Debug.LogError("bigMinivoxLink.positions.Length is 0 at: " + newHitVoxel.position);
        #endif
        #if !DEBUG_KILL_BROKEN_BIGMINIVOX
        return; // Assuming minivox is still spawning.
        #endif
    }
}
else
{
    newMinivoxHit = minivoxLinks2.GetMinivox(newHitVoxel.position);
}*/
/*if (rayDirection.x < 0)
{
    rayLength.x = (rayOrigin.x - ((float)rayGlobalVoxelPosition.x)) * rayUnitSize.x;
}
else
{
    rayLength.x = (((float)rayGlobalVoxelPosition.x) + 1 - rayOrigin.x) * rayUnitSize.x;  //  + 1
}
if (rayDirection.y < 0)
{
    rayLength.y = (rayOrigin.y - ((float)rayGlobalVoxelPosition.y)) * rayUnitSize.y;
}
else
{
    rayLength.y = (((float)rayGlobalVoxelPosition.y) + 1 - rayOrigin.y) * rayUnitSize.y;  //  + 1
}
if (rayDirection.z < 0)
{
    rayLength.z = (rayOrigin.z - ((float)rayGlobalVoxelPosition.z)) * rayUnitSize.z;
}
else
{
    rayLength.z = (((float)rayGlobalVoxelPosition.z) + 1 - rayOrigin.z) * rayUnitSize.z;  //  + 1
}*/