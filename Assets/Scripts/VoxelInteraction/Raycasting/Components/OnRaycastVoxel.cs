using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    //! An event for user raycasting a voxel.
    public struct OnRaycastVoxel : IComponentData { }
}