using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    //! An event for user raycasting a grass.
    public struct OnRaycastGrass : IComponentData { }
}