using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.VoxelInteraction
{
    public struct RaycasterOrigin : IComponentData
    {
        public float3 rayOrigin;
        public float3 rayDirection;
        public quaternion rayQuaternion;
    }
}