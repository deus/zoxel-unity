using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    public struct OnRaycastCharacter : IComponentData { public byte state; }
}