﻿using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.VoxelInteraction
{
    //! Raycasting Voxel data. Stores what is hit.
    /**
    *   \todo Add raycast pass data: whether it passes through certain voxels or types of voxels!
    */
    public struct RaycastVoxel : IComponentData
    {
        // Hit Information
        public byte rayHitType;
        public float lastCasted;
        public int3 hitNormal;
        // type is 0 - hit voxel
        public VoxelHitInfo hitVoxel;
        public VoxelHitInfo normalVoxel;
        // type is 2 - hit item
        // Ray Cached before casting
        public float3 hitAngle;
        public byte blockRotationRegion;

        public void OnHitFailed()
        {
            Reset();
        }

        public void OnHitCharacter()
        {
            Reset();
        }

        public void Reset()
        {
            this.hitVoxel = new VoxelHitInfo();
            this.normalVoxel = new VoxelHitInfo();
        }
    }
}