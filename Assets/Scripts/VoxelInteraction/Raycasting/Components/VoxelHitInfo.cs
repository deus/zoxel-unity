using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    //! Data used for raycasting!
    public struct VoxelHitInfo // : IComponentData
    {
        //! Position Hit
        public int3 position;
        //! Entities Hit
        public Entity vox;
        public Entity chunk;
        public Entity voxel;
        public Entity hitMinivox;
        //! Cached data of voxel
        public byte voxelIndex;
        public byte lightValue;
        public byte sides;
        public byte meshIndex;
        public int3 minivoxOffset;
        public int3 localPosition;
        public int3 chunkPosition;
        public int3 chunkVoxelPosition;
    }
}