using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    public struct OnRaycastMissed : IComponentData { }
}