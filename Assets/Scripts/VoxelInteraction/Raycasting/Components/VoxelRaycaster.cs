using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.VoxelInteraction
{
    public struct VoxelRaycaster : IComponentData
    {
        public byte hasRaycast;
        public byte didHit;
        public float3 screenPosition;
        public int3 voxelPosition;
        public Entity planet;
        public Entity camera;
    }
}