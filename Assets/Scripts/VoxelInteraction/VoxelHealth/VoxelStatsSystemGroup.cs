using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Textures;
using Zoxel.Rendering;
using Zoxel.Voxels;
using Zoxel.Stats;

namespace Zoxel.VoxelInteraction
{
    //! A seperate entity spawned to manage VoxelStats.
    /**
    *   \todo Save/Load Voxel userStatLinks.
    */
    [UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxelStatsSystemGroup : ComponentSystemGroup
    {
        public static Entity voxelStatsPrefab;

        protected override void OnCreate()
        {
            base.OnCreate();
            var voxelHealthArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(GenerateVoxelHealthTexture),
                typeof(VoxelHealth),
                typeof(UpdateVoxelHealth),
                typeof(VoxLink),
                typeof(ChunkLink),
                typeof(BasicRender),
                typeof(ZoxMesh),
                typeof(VoxelHealthBuffer),
                typeof(VoxelHealthTileIndex),
                typeof(DontDestroyMesh),        // uses cube mesh atm
                typeof(Texture),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale)
            );
            voxelStatsPrefab = EntityManager.CreateEntity(voxelHealthArchetype);
            EntityManager.SetComponentData(voxelStatsPrefab, new Rotation { Value = quaternion.identity });
            EntityManager.SetComponentData(voxelStatsPrefab, new NonUniformScale { Value = new float3(1f, 1f, 1f) });
        }
    }
}