using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Zoxel.Animations;
using Zoxel.Voxels;
using Zoxel.Stats;
using Zoxel.Rendering;

// Todo: Create a Mesh/Animation over the top of voxels being destroyed
//          Set The index with compute buffers
//          In the shader, get the tile position (float2) for the texture
// Create render mesh at the location!
// one per each side of the voxel
// what is side index of the voxel? Can we pass this in voxel damage struct
// Create 6 side meshes anyway for now since we don't know

namespace Zoxel.VoxelInteraction
{
    //! Applies damage to existing VoxelHealth.
    [BurstCompile, UpdateInGroup(typeof(VoxelStatsSystemGroup))]
    public partial class VoxelHealthDamageSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity voxelDamagePrefab;
        private EntityQuery processQuery;
        private EntityQuery voxelDamagesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            voxelDamagesQuery = GetEntityQuery(ComponentType.ReadOnly<VoxelDamage>());
            var voxelDamageArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(VoxelDamage),
                typeof(GenericEvent));
            voxelDamagePrefab = EntityManager.CreateEntity(voxelDamageArchetype);
			RequireForUpdate(processQuery);
			RequireForUpdate(voxelDamagesQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // pass in voxel Healths
            // for all damages, add a component to increase or decrease health
            // if no VoxelHealth Exists, create one
            var voxelDamageEntities = voxelDamagesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var voxelDamages = GetComponentLookup<VoxelDamage>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<UpdateVoxelHealth>()
                .ForEach((Entity e, int entityInQueryIndex, in VoxelHealth voxelHealth) =>
            {
                var damageApplied = 0f;
                for (int i = 0; i < voxelDamageEntities.Length; i++)
                {
                    var e2 = voxelDamageEntities[i];
                    var voxelDamage = voxelDamages[e2];
                    // need to update this, so damage and health can take in a list of positions, like minivoxCombinedLink
                    if (voxelHealth.HasPosition(voxelDamage.position))
                    {
                        damageApplied += voxelDamage.damage;
                    }
                }
                if (damageApplied != 0)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new UpdateVoxelHealth(-damageApplied));
                }
            })  .WithReadOnly(voxelDamageEntities)
                .WithDisposeOnCompletion(voxelDamageEntities)
                .WithReadOnly(voxelDamages)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}