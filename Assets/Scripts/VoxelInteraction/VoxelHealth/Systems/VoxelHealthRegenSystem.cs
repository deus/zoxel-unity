using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;

namespace Zoxel.VoxelInteraction
{
    //! Regens a damaged voxel.
    /**
    *   \todo Regen each voxel uniquely based on a Stats component.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelStatsSystemGroup))]
    public partial class VoxelHealthRegenSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var elapsedTime = World.Time.ElapsedTime;
            // another for voxel health regen, destroy when its finished regening
            Dependency = Entities
                .WithNone<UpdateVoxelHealth, DestroyEntity, VoxelHealthDying>()
                .ForEach((Entity e, int entityInQueryIndex, ref VoxelHealth voxelHealth, ref VoxelHealthTileIndex voxelHealthTileIndex) =>
            {
                // last regen
                if (elapsedTime - voxelHealth.lastRegened >= 1)
                {
                    voxelHealth.lastRegened = elapsedTime;
                    if (voxelHealth.health == voxelHealth.maxHealth)
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e);
                    }
                    else
                    {
                        voxelHealth.health += 0.1f;
                        if (voxelHealth.health > voxelHealth.maxHealth)
                        {
                            voxelHealth.health = voxelHealth.maxHealth;
                        }
                        // max tiles
                        voxelHealthTileIndex.healthPercentage = voxelHealth.health / voxelHealth.maxHealth;
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}