using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Stats;
using Zoxel.Stats.Authoring;

namespace Zoxel.VoxelInteraction
{
    //! Spawns a new VoxelHealth Entity when VoxelDamage event happens. Or applies damage to the existing one.
    /**
    *   \todo Link VoxelHealth entities to Chunks like I do with minivoxes so it scales better.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelStatsSystemGroup))]
    public partial class VoxelHealthSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxelsQuery;
        private EntityQuery voxelHealthsQuery;
        private EntityQuery bigMinivoxQuery;
        private EntityQuery voxesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            voxelHealthsQuery = GetEntityQuery(ComponentType.ReadOnly<VoxelHealth>());
            voxelsQuery = GetEntityQuery(ComponentType.ReadOnly<Voxel>());
            bigMinivoxQuery = GetEntityQuery(ComponentType.ReadOnly<BigMinivox>());
            voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxScale>());
			RequireForUpdate(processQuery);
            RequireForUpdate<StatSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var statSettings = GetSingleton<StatSettings>();
            var voxelHealthStart = statSettings.defaultVoxelHealth;
            var elapsedTime = World.Time.ElapsedTime;
            var voxelStatsPrefab = VoxelStatsSystemGroup.voxelStatsPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxelHealthEntities = voxelHealthsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle2);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle2);
            var voxelHealths = GetComponentLookup<VoxelHealth>(true);
            var voxelEntities = voxelsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle3);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle3);
            var voxels = GetComponentLookup<Voxel>(true);
            voxelEntities.Dispose();
            var bigMinivoxEntities = bigMinivoxQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var multiVoxelPositions = GetComponentLookup<MultiVoxelPosition>(true);
            bigMinivoxEntities.Dispose();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var voxScales = GetComponentLookup<VoxScale>(true);
            voxEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, in VoxelDamage voxelDamage) =>
            {
                // PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                /*var multiVoxelPosition = new MultiVoxelPosition();
                if (HasComponent<BigMinivox>(voxelDamage.vox))
                {
                    multiVoxelPosition = multiVoxelPositions[voxelDamage.vox];
                }*/
                // did have a voxel health associated with it
                var wasFound = false;
                for (int i = 0; i < voxelHealthEntities.Length; i++)
                {
                    var e2 = voxelHealthEntities[i];
                    var voxelHealth = voxelHealths[e2];
                    wasFound = voxelHealth.HasPosition(voxelDamage.position);
                    if (wasFound)
                    {
                        break;
                    }
                }
                if (wasFound || !voxels.HasComponent(voxelDamage.voxelMeta))
                {
                    return;
                }
                // we have position: voxelDamage.position and type: hitVoxelIndex
                var voxelData = voxels[voxelDamage.voxelMeta];
                var startingHealth = voxelData.startingHealth;
                var voxScale = voxScales[voxelDamage.vox].scale;
                var voxelHealthPosition = voxelDamage.position.ToFloat3();
                voxelHealthPosition.x *= voxScale.x;
                voxelHealthPosition.y *= voxScale.y;
                voxelHealthPosition.z *= voxScale.z;
                voxelHealthPosition += voxScale / 2f;
                VoxelHealth voxelHealth2;
                // var isBigMinivox = HasComponent<BigMinivoxVoxel>(voxelDamage.voxelMeta);
                if (HasComponent<BigMinivox>(voxelDamage.minivox))
                {
                    var multiVoxelPosition = multiVoxelPositions[voxelDamage.minivox];
                    voxelHealth2 = new VoxelHealth(in voxelDamage, in multiVoxelPosition, startingHealth, elapsedTime, voxelDamage.direction);
                    //UnityEngine.Debug.LogError("multiVoxelPosition: " + multiVoxelPosition.positions.Length);
                }
                else
                {
                    voxelHealth2 = new VoxelHealth(in voxelDamage, startingHealth, elapsedTime, voxelDamage.direction);
                    //UnityEngine.Debug.LogError("multiVoxelPosition: " + voxelDamage.position);
                }
                // Create a new voxel health, pass in voxel userStatLinks as well!
                var voxelHealthEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, voxelStatsPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, voxelHealthEntity, voxelHealth2);
                PostUpdateCommands.SetComponent(entityInQueryIndex, voxelHealthEntity, new ChunkLink(voxelDamage.chunk));
                PostUpdateCommands.SetComponent(entityInQueryIndex, voxelHealthEntity, new VoxLink(voxelDamage.vox));
                PostUpdateCommands.SetComponent(entityInQueryIndex, voxelHealthEntity, new UpdateVoxelHealth(-voxelDamage.damage));
                PostUpdateCommands.SetComponent(entityInQueryIndex, voxelHealthEntity, new Translation { Value = voxelHealthPosition });
                PostUpdateCommands.SetComponent(entityInQueryIndex, voxelHealthEntity, new NonUniformScale { Value = voxScale });
            })  .WithReadOnly(voxelHealthEntities)
                .WithDisposeOnCompletion(voxelHealthEntities)
                .WithReadOnly(voxelHealths)
                .WithReadOnly(voxels)
                .WithReadOnly(multiVoxelPositions)
                .WithReadOnly(voxScales)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                    /*#if DEBUG_BIGMINIVOXES_HEALTH
                    if (voxelDamage.bigMinivoxLink.positions.Length == 0)
                    {
                        UnityEngine.Debug.LogError("Cannot Spawn BigMinivox VoxelHealth, no positions from Raycasting.");
                    }
                    #endif
                    #if !DEBUG_KILL_BROKEN_BIGMINIVOX
                    return;
                    #else
                    isBigMinivox = false;
                    #endif*/