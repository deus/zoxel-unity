using Unity.Entities;
using Unity.Burst;
using Zoxel.Voxels;

namespace Zoxel.VoxelInteraction
{
    //! Destroys the VoxelStats Entity.
    /**
    *   Using a SpawnVoxelLink, it checks to see if the event is still occuring. Or if chunk is still updating.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelStatsSystemGroup))]
    public partial class VoxelHealthDyingSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // delay removing voxel damage
            Dependency = Entities
                .WithAll<VoxelHealth>()
                .ForEach((Entity e, int entityInQueryIndex, in VoxelHealthDying voxelHealthDying, in ChunkLink chunkLink, in SpawnVoxelLink spawnVoxelLink) =>
            {
                if (HasComponent<VoxelModification>(spawnVoxelLink.spawnVoxel) || HasComponent<EntityBusy>(chunkLink.chunk))
                {
                    return;
                }
                // timing added to get past the VoxelDying Initial Stage
                // UnityEngine.Debug.LogError("Voxel Health Dying: " + voxelHealth.positions[0]);
                PostUpdateCommands.RemoveComponent<VoxelHealthDying>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}