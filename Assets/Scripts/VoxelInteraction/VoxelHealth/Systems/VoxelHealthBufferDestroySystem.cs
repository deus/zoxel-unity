using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    //! Uses DestroyEntity event to dispose of VoxelHealthBuffer.
    [UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class VoxelHealthBufferDestroySystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, VoxelHealthBuffer>()
                .ForEach((in VoxelHealthBuffer voxelHealthBuffer) =>
            {
                voxelHealthBuffer.Dispose();
            }).WithoutBurst().Run();
        }
    }
}