using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Textures;

namespace Zoxel.VoxelInteraction
{
    //! Initializes voxel health materials.
    [UpdateInGroup(typeof(VoxelStatsSystemGroup))]
    public partial class VoxelHealthInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        //! One mesh per voxel side possibility 2 * 2 * 2 * 2 * 2 * 2 combinations.
        private UnityEngine.Mesh cubeMesh;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            cubeMesh = MeshUtilities.GenerateCube(new float3(1,1,1) * 1.01f, true);
        }

        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null) return;
            var voxelDamageMaterial = MaterialsManager.instance.materials.voxelDamageMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity>()
                .ForEach((Entity e, in VoxelHealth voxelHealth) =>
            {
                var voxelHealthMaterial = new UnityEngine.Material(voxelDamageMaterial);
                voxelHealthMaterial.SetTexture("_BaseMap", TextureUtil.CreateBlankTexture());
                PostUpdateCommands.SetSharedComponentManaged(e, new ZoxMesh
                {
                    material = voxelHealthMaterial,
                    mesh = cubeMesh
                });
            }).WithoutBurst().Run();
        }
    }
}