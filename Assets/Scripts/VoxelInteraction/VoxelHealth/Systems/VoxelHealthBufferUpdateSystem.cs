using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.VoxelInteraction
{
    //! Sets the health percentages in the VoxelHealthBuffer data.
    [UpdateInGroup(typeof(VoxelStatsSystemGroup))]
    public partial class VoxelHealthBufferUpdateSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            //! \todo Readd change filter
            //      atm it - breaks for voxels that die straight away - make sure to initially update it so it updates here
            //      was testing this with high damage and destroying the voxels - many at once
            Entities
                .WithChangeFilter<VoxelHealthTileIndex>()
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithAll<VoxelHealth>()
                .ForEach((in VoxelHealthTileIndex voxelHealthTileIndex, in ZoxMesh zoxMesh) => //, in VoxelHealthBuffer voxelHealthBuffer) =>
            {
                if (zoxMesh.material != null)
                {
                    zoxMesh.material.SetFloat("healthPercentage", voxelHealthTileIndex.healthPercentage);
                }
            }).WithoutBurst().Run();
        }
    }
}