using Unity.Entities;
using Unity.Burst;

namespace Zoxel.VoxelInteraction
{
    //! Uses DestroyEntity event to dispose of VoxelHealth.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class VoxelHealthDestroySystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, VoxelHealth>()
                .ForEach((in VoxelHealth voxelHealth) =>
            {
                voxelHealth.Dispose();
            }).ScheduleParallel();
        }
    }
}