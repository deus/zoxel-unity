using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Animations;
using Zoxel.Audio;
using Zoxel.Voxels;
using Zoxel.UI;
using Zoxel.Stats;
using Zoxel.UI.Animations;
using Zoxel.Stats.Authoring;    

namespace Zoxel.VoxelInteraction
{
    //! VoxelHealth gets set into percentage. Triggers VoxelHealthDying.
    /**
    *   Creates a Mesh/Animation over the top of voxels being destroyed. Sets the index with compute buffers.
    *   In the shader, gets the tile position (float2) for the texture.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelStatsSystemGroup))]
    public partial class VoxelHealthSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
		private EntityQuery planetsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			planetsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<VoxScale>());
			RequireForUpdate(processQuery);
			RequireForUpdate(planetsQuery);
            RequireForUpdate<StatSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var statSettings = GetSingleton<StatSettings>();
            var spawnPopupPrefab = TextPopupSystem.spawnPopupPrefab;
            var voxelErasePrefab = VoxelEraseSystem.voxelErasePrefab;
            var eraseBrushSize = VoxelManager.instance.voxelSettings.eraseBrushSize;
            var voxelHealthStart = statSettings.defaultVoxelHealth;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var planetEntities = planetsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var planetVoxScales = GetComponentLookup<VoxScale>(true);
            planetEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, VoxelHealthDying>()
                .ForEach((Entity e, int entityInQueryIndex, ref VoxelHealth voxelHealth, ref VoxelHealthTileIndex voxelHealthTileIndex,
                    in UpdateVoxelHealth updateVoxelHealth, in ChunkLink chunkLink, in VoxLink voxLink) =>
            {
                PostUpdateCommands.RemoveComponent<UpdateVoxelHealth>(entityInQueryIndex, e);
                if (!planetVoxScales.HasComponent(voxLink.vox))
                {
                    return;
                }
                var planetVoxScale = planetVoxScales[voxLink.vox].scale;
                voxelHealth.health += updateVoxelHealth.health;
                if (voxelHealth.positions.Length == 0)
                {
                    return;
                }
                // UnityEngine.Debug.LogError("voxelHealth.position: " + voxelHealth.position + " damage done: " + updateVoxelHealth.health);
                var voxelPopupPosition = new float3();
                for (int i = 0; i < voxelHealth.positions.Length; i++)
                {
                    voxelPopupPosition += voxelHealth.positions[i].ToFloat3();
                }
                voxelPopupPosition /= voxelHealth.positions.Length;
                voxelPopupPosition += new float3(0.5f, 0.5f, 0.5f);
                voxelPopupPosition.x *= planetVoxScale.x;
                voxelPopupPosition.y *= planetVoxScale.y;
                voxelPopupPosition.z *= planetVoxScale.z;
                var spawnPopupEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnPopupPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnPopupEntity, 
                    new SpawnTextPopup(-updateVoxelHealth.health, voxelPopupPosition, voxelHealth.direction, TextPopupType.VoxelDamage));
                if (voxelHealth.health < 0)
                {
                    voxelHealth.health = 0;
                }
                // max tiles
                voxelHealthTileIndex.healthPercentage = voxelHealth.health / voxelHealth.maxHealth;
                //UnityEngine.Debug.LogError("Voxel Health updated to: " + voxelHealth.health);
                // When Voxel dies
                if (voxelHealth.health == 0)
                {
                    // UnityEngine.Debug.LogError("Voxel has died.");
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new VoxelHealthDying(elapsedTime));
                    for (int i = 0; i < voxelHealth.positions.Length; i++)
                    {
                        var eraseEntity = VoxelEraseSystem.EraseVoxel(PostUpdateCommands, entityInQueryIndex, voxelErasePrefab, voxLink.vox,
                            voxelHealth.positions[i], eraseBrushSize);
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SpawnVoxelLink(eraseEntity));
                        if (i != 0)
                        {
                            PostUpdateCommands.AddComponent<DisableItemDrop>(entityInQueryIndex, eraseEntity);
                        }
                    }
                }
            })  .WithReadOnly(planetVoxScales)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}