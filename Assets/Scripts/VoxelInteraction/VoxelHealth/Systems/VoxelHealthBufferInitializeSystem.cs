using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    //! Initializes voxel health render buffers (VoxelHealthBuffer).
    [UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class VoxelHealthBufferInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, VoxelHealth>()
                .ForEach((Entity e) =>
            {
                PostUpdateCommands.SetSharedComponentManaged(e, new VoxelHealthBuffer(new UnityEngine.ComputeBuffer(1, 4,
                    UnityEngine.ComputeBufferType.Default, UnityEngine.ComputeBufferMode.SubUpdates)));
            }).WithoutBurst().Run();
        }
    }
}