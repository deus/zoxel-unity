using Unity.Entities;
using Zoxel.Voxels;
using Zoxel.Voxels.Minivoxes;

namespace Zoxel.VoxelInteraction
{
    //! Used to apply damage to voxels.
    /**
    *   \todo Make this component Simpler. Break it apart.
    */
    public struct VoxelDamage : IComponentData
    {
        public float damage;
        public byte hitType;
        public Entity vox;
        public Entity chunk;
        public int3 position;
        public int3 chunkVoxelPosition;
        public int3 chunkPosition;
        public Entity minivox;
        public Entity voxelMeta;
        public byte direction;
    }
}