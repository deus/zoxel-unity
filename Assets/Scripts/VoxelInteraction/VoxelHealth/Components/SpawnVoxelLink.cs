using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    public struct SpawnVoxelLink : IComponentData
    {
        public Entity spawnVoxel;

        public SpawnVoxelLink(Entity spawnVoxel)
        {
            this.spawnVoxel = spawnVoxel;
        }
    }

}