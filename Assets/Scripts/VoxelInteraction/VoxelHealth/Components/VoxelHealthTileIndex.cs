using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    public struct VoxelHealthTileIndex : IComponentData
    { 
        public float healthPercentage;

        public VoxelHealthTileIndex(float healthPercentage)
        {
            this.healthPercentage = healthPercentage;
        }
    }
}