using Unity.Entities;
using Unity.Collections;
using Zoxel.Voxels;

namespace Zoxel.VoxelInteraction
{
    //! Added to an entity used for Voxel's health.
    /**
    *   \todo Replace positions with VoxelPosition and MultiVoxelPosition
    *   \todo Replace with StateStat and RegenStat. Add Stats struct to VoxelStats Entities. Use positions as VoxelPositionData.
    */
    public struct VoxelHealth : IComponentData
    {
        //! The health of the voxel.
        public float health;
        //! The max health of the voxel.
        public float maxHealth;
        //! The last time the voxel regened health.
        public double lastRegened;
        //! Direction is used for particles. \todo Use hit direction instead.
        public byte direction;
        //! Global Voxel Positions
		public BlitableArray<int3> positions;

        public VoxelHealth(in VoxelDamage voxelDamage, in MultiVoxelPosition multiVoxelPosition, float health, double elapsedTime, byte direction)
        {
            this.direction = direction;
            this.health = health;               // default health
            this.maxHealth = health;            // default health
            this.lastRegened = elapsedTime;
            /*#if DEBUG_BIGMINIVOXES_HEALTH
            UnityEngine.Debug.LogError("    Creating BigMinivox Voxel Health: " + voxelDamage.position
                + " with " + voxelDamage.bigMinivoxLink.positions.Length + " positions."
                + " [" + voxelDamage.bigMinivoxLink.positions[0] + "], [" + voxelDamage.bigMinivoxLink.positions[1] + "]");
            #endif*/
            /*this.positions = new BlitableArray<int3>(voxelDamage.bigMinivoxLink.positions.Length, Allocator.Persistent);
            for (int i = 0; i < this.positions.Length; i++)
            {
                this.positions[i] = voxelDamage.bigMinivoxLink.positions[i];
            }*/
            this.positions = multiVoxelPosition.positions.Clone();  // Allocator.Persistent
        }

        public VoxelHealth(in VoxelDamage voxelDamage, float health, double elapsedTime, byte direction)
        {
            this.direction = direction;
            this.health = health;               // default health
            this.maxHealth = health;            // default health
            this.lastRegened = elapsedTime;
            this.positions = new BlitableArray<int3>(1, Allocator.Persistent);
            this.positions[0] = voxelDamage.position;
            #if DEBUG_BIGMINIVOXES_HEALTH
            UnityEngine.Debug.LogError("    Creating Normal Voxel Health: " + voxelDamage.position);
            #endif
        }

        public void Dispose()
        {
            if (positions.Length > 0)
            {
                positions.Dispose();
            }
        }

        public bool HasPosition(int3 position)
        {
            for (int i = 0; i < positions.Length; i++)
            {
                var position2 = positions[i];
                #if DEBUG_BIGMINIVOXES_HEALTH
                UnityEngine.Debug.LogError("    VoxelHealth.HasPosition - checking against [" + i + "]: " + position2);
                #endif
                if (position2 == position)
                {
                    #if DEBUG_BIGMINIVOXES_HEALTH
                    UnityEngine.Debug.LogError("    Found Voxel Position: " + position);
                    #endif
                    return true;
                }
            }
            #if DEBUG_BIGMINIVOXES_HEALTH
            UnityEngine.Debug.LogError("    Failed to find Voxel Position: " + position);
            #endif
            return false;
        }
    }
}