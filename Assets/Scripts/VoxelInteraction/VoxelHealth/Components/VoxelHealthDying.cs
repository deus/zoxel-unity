using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    public struct VoxelHealthDying : IComponentData
    {
        public double timeStarted;

        public VoxelHealthDying(double elapsedTime)
        {
            this.timeStarted = elapsedTime;
        }
    }

}