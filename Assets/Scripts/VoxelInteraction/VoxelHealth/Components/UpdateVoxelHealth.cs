using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    public struct UpdateVoxelHealth : IComponentData
    {
        public float health;

        public UpdateVoxelHealth(float health)
        {
            this.health = health;   // default health
        }
    }
}