using Unity.Entities;
using System;

namespace Zoxel.VoxelInteraction
{
    public struct VoxelHealthBuffer : ISharedComponentData, IEquatable<VoxelHealthBuffer>
    {
        public UnityEngine.ComputeBuffer buffer;

        public VoxelHealthBuffer(UnityEngine.ComputeBuffer buffer)
        {
            this.buffer = buffer;
        }

        public bool Equals(VoxelHealthBuffer other)
        {
            return buffer == other.buffer;
        }
        
        public override int GetHashCode()
        {
            return buffer.GetHashCode();
        }

        public void Dispose()
        {
            buffer.Release();
        }
    }
}