using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;
using Zoxel.Items;
using Zoxel.Voxels;
using Zoxel.UI;

namespace Zoxel.VoxelInteraction
{
    //! Sets tooltip text when selected a Voxel.
    /**
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class VoxelSelectedUISystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<EntityBusy, InitializeEntity, DisableRaycaster>()
                .WithAll<OnRaycastVoxel>()
                .ForEach((Entity e, in RaycastVoxel raycastVoxel, in ControllerLink controllerLink) =>
            {  
                var playerTooltipLinks = EntityManager.GetComponentData<PlayerTooltipLinks>(controllerLink.controller);
                var isEmptyText = true;
                if (HasComponent<Chunk>(raycastVoxel.hitVoxel.chunk))
                {
                    var voxLink = EntityManager.GetComponentData<VoxLink>(raycastVoxel.hitVoxel.chunk);
                    var worldEntity = voxLink.vox;
                    if (!HasComponent<VoxelLinks>(worldEntity))
                    {
                        return;
                    }
                    var voxelLinks = EntityManager.GetComponentData<VoxelLinks>(worldEntity);
                    var voxelIndex = raycastVoxel.hitVoxel.voxelIndex - 1;
                    if (voxelIndex >= 0 && voxelIndex < voxelLinks.voxels.Length)
                    {
                        var voxelEntity = voxelLinks.voxels[voxelIndex];
                        if (HasComponent<ZoxName>(voxelEntity) && HasComponent<Voxel>(voxelEntity))
                        {
                            var voxelName = EntityManager.GetComponentData<ZoxName>(voxelEntity).name.ToString();
                            var voxel = EntityManager.GetComponentData<Voxel>(voxelEntity);
                            var voxelType = (VoxelType) (voxel.type);
                            var label = voxelName + " " + voxelType.ToString();
                            if (voxel.functionType == (byte) VoxelFunctionType.Chest)
                            {
                                label = "[action2] to Open [" + label + "]";
                            }
                            else if (voxel.functionType == (byte) VoxelFunctionType.Door)
                            {
                                label = "[action2] to Open [" + label + "]";
                            }
                            else if (voxel.functionType == (byte) VoxelFunctionType.Bed)
                            {
                                //label += "\n[action2] to Sleep on  Bed";
                                label = "[action2] to Sleep on [" + label + "]";
                            }
                            isEmptyText = false;
                            playerTooltipLinks.SetPlayerSelectionTooltip(PostUpdateCommands, e, label);  // originally just voxelName
                        }
                    }
                }
                if (isEmptyText)
                {
                    playerTooltipLinks.SetPlayerSelectionTooltip(PostUpdateCommands, e, "");
                }
            }).WithoutBurst().Run();
        }
    }
}