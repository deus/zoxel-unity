using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Voxels;
using Zoxel.Voxels.Authoring;

namespace Zoxel.VoxelInteraction
{
    //! Handles raycasting against grass.
    [BurstCompile, UpdateInGroup(typeof(VoxelInteractSystemGroup))]
    public partial class EntityOutlineSelectedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<ModelSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var modelSettings = GetSingleton<ModelSettings>();
            var entitySelectedColor = modelSettings.entitySelectedColor.ToFloat4();
            var entitySelectionSpeed = modelSettings.entitySelecionSpeed;
            // var selectedColor = new float4(entitySelectedColor.r, entitySelectedColor.g, entitySelectedColor.b, entitySelectedColor.a);
            var selectionSpeed = World.Time.DeltaTime * (1f / entitySelectionSpeed);
            Entities
				.WithAll<ChunkRender>()
				.ForEach((ref OutlineColor outlineColor, in VoxLink voxLink) =>
			{
                if (!HasComponent<EntitySelected>(voxLink.vox))
                {
                    if (HasComponent<OnEntityDeselected>(voxLink.vox))
                    {
                        var outlineColor2 = new float4(0, 0, 0, outlineColor.color.w);
                        outlineColor.color = math.lerp(outlineColor.color, outlineColor2, selectionSpeed);
                        // UnityEngine.Debug.LogError("OnEntityDeselected!");
                    }
                }
                else
                {
                    if (HasComponent<OnEntitySelected>(voxLink.vox))
                    {
                        outlineColor.color = math.lerp(outlineColor.color, entitySelectedColor, selectionSpeed);
                    }
                    // UnityEngine.Debug.LogError("GrassHighlightSystem: GrassStrand Outline Colors Updated!");
                }
			}).ScheduleParallel();
        }
    }
}