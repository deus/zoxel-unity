using Unity.Entities;

namespace Zoxel.VoxelInteraction
{
    public struct BeginDisableVoxelInteraction : IComponentData { }
    public struct EndDisableVoxelInteraction : IComponentData { }
    public struct DisableVoxelInteraction : IComponentData { }
}