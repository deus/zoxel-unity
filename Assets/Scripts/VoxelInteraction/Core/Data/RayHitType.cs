namespace Zoxel.VoxelInteraction
{
    public enum RayHitType : byte
    {
        None,
        Voxel,
        Character
    }
}