using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Audio
{
    //! Initializes sound data.
    [BurstCompile, UpdateInGroup(typeof(AudioSystemGroup))]
    public partial class SoundInitializeSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithAll<InitializeEntity, Sound>()
                .ForEach((ref Sound sound, in SoundData soundData) =>
            {
                sound.Initialize(in soundData);
            }).ScheduleParallel();
        }
    }
}