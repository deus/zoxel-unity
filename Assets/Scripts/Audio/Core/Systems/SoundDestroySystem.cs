using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Audio
{
    //! Clears Sound on DestroyEntity.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class SoundDestroySystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, Sound>()
                .ForEach((in Sound sound) =>
            {
                sound.Dispose();
            }).ScheduleParallel();
        }
    }
}