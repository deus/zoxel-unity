using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Audio
{
    //! Using DestroyAfterEvent, Adds a DestroyEntity to the Entity upon the event PlaySound.
    [UpdateAfter(typeof(SoundPlaySystem))]
    [BurstCompile, UpdateInGroup(typeof(AudioSystemGroup))]
    public partial class SoundDestroyAfterPlaySystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity, GenerateSound, DelayEvent>()
                .WithAll<PlaySound, DestroyAfterEvent>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}