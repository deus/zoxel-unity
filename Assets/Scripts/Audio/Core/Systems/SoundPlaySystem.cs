using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Audio.Authoring;
using Zoxel.Transforms;

namespace Zoxel.Audio
{
    //! Uploads our Sound data to AudioClip for Unity to play.
    /**
    *   \todo AudioListener is from UI camera... have to move this onto player cameras
    *   \todo Use a pool of AudioSource gameobjects to grab and use.
    *       Set link GO with an entity and return to pool when time is up on playing the sound.
    */
    [UpdateInGroup(typeof(AudioSystemGroup))]
    public partial class SoundPlaySystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity soundPrefab;
        private EntityQuery processQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var soundArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(GenerateSound),
                typeof(DelayEvent),
                typeof(DestroyAfterEvent),
                typeof(PlaySound),
                typeof(Seed),
                typeof(Sound),
                typeof(SoundData),
                typeof(Translation));
            soundPrefab = EntityManager.CreateEntity(soundArchetype);
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<GenerateSound>(),
                ComponentType.Exclude<DelayEvent>(),
                ComponentType.Exclude<DestroyAfterEvent>(),
                ComponentType.ReadOnly<PlaySound>());
            RequireForUpdate<SoundSettings>();
		}
        
        protected override void OnUpdate()
        {
            if (!processQuery.IsEmpty)
            {
                SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<PlaySound>(processQuery);
            }
            if (NetworkUtil.IsHeadless())
            {
                return;
            }
            var soundSettings = GetSingleton<SoundSettings>();
            var musicMixerGroup = AudioManager.instance.music;
            var soundMixerGroup = AudioManager.instance.sounds;
            var musicLevel = AudioManager.instance.musicLevel / 10f;
            var soundLevel = soundSettings.soundLevel / 10f;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities
                .WithNone<DestroyEntity, GenerateSound, DelayEvent>()
                .WithAll<PlaySound, MusicSound>()
                .ForEach((in Translation translation, in Sound sound, in SoundData soundData) =>
            {
                PlaySound(sound.GetAudioClip(in soundData), musicMixerGroup, soundData.volume * musicLevel, translation.Value);
            }).WithoutBurst().Run();
            Entities
                .WithNone<DestroyEntity, GenerateSound, DelayEvent>()
                .WithNone<MusicSound>()
                .WithAll<PlaySound>()
                .ForEach((in Translation translation, in Sound sound, in SoundData soundData) =>
            {
                PlaySound(sound.GetAudioClip(in soundData), soundMixerGroup, soundData.volume * soundLevel, translation.Value);
            }).WithoutBurst().Run();
            Entities
                .WithNone<DestroyEntity, GenerateSound, DelayEvent>()
                .WithNone<Translation>()
                .WithAll<PlaySound, MusicSound>()
                .ForEach((in Sound sound, in SoundData soundData) =>
            {
                PlaySound(sound.GetAudioClip(in soundData), musicMixerGroup, soundData.volume * musicLevel);
            }).WithoutBurst().Run();
            Entities
                .WithNone<DestroyEntity, GenerateSound, DelayEvent>()
                .WithNone<Translation, MusicSound>()
                .WithAll<PlaySound>()
                .ForEach((in Sound sound, in SoundData soundData) =>
            {
                PlaySound(sound.GetAudioClip(in soundData), soundMixerGroup, soundData.volume * soundLevel);
            }).WithoutBurst().Run();
        }

        public void PlaySound(UnityEngine.AudioClip audioClip, UnityEngine.Audio.AudioMixerGroup mixerGroup, float volume)
        {
            //UnityEngine.Debug.LogError("Playing sound note with: " + volume);
            PlaySound(audioClip, new float3(), volume, mixerGroup, true); // AudioManager.instance.music, true);
        }

        public void PlaySound(UnityEngine.AudioClip audioClip, UnityEngine.Audio.AudioMixerGroup mixerGroup, float volume, float3 position)
        {
            PlaySound(audioClip, position, volume, mixerGroup, false); // AudioManager.instance.sounds, false);
        }

        void PlaySound(UnityEngine.AudioClip audioClip, float3 position, float volume, UnityEngine.Audio.AudioMixerGroup mixerGroup, bool is2D)
        {
            var audioPrefab = AudioManager.instance.audioPrefab;
            var blendLevel = AudioManager.instance.defaultBlendLevel;
            var newSound = UnityEngine.GameObject.Instantiate(audioPrefab);
            #if UNITY_EDITOR
            if (!AudioManager.instance.showAudioInEditor)
            {
                newSound.hideFlags = UnityEngine.HideFlags.HideInHierarchy;
            }
            #endif
            newSound.SetActive(true);
            newSound.transform.position = position;
            var audioSource = newSound.GetComponent<UnityEngine.AudioSource>();
            if (AudioManager.instance.disable3DAudio || is2D)
            {
                blendLevel = 0;
            }
            else
            {
                audioSource.minDistance = 1.5f;
                audioSource.maxDistance = 32f;
            }
            audioSource.spatialBlend = blendLevel;
            audioSource.volume = volume;
            audioSource.PlayOneShot(audioClip);
            audioSource.outputAudioMixerGroup = mixerGroup;
            // destroy GameObject and AudioClip after it plays
            AudioManager.instance.DestroyThing(newSound, audioClip.length);
            AudioManager.instance.DestroyThing(audioClip, audioClip.length);
        }
    }
}