using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;

namespace Zoxel.Audio
{
    //! Generates a Sound!
    [UpdateBefore(typeof(SoundPlaySystem))]
    [BurstCompile, UpdateInGroup(typeof(AudioSystemGroup))]
    public partial class SoundGenerationSystem : SystemBase
    {
        const float pi = 3.14159265f;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var processSpeed = AudioManager.instance.processSpeed;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref Sound sound, ref GenerateSound generateSound, in SoundData soundData, in Seed seed) =>
            {
                if (sound.data.Length == 0 || seed.seed == 0)
                {
                    return;
                }
                generateSound.generationState++;
                if (generateSound.generationState == processSpeed + 1)
                {
                    PostUpdateCommands.RemoveComponent<GenerateSound>(entityInQueryIndex, e);
                }
                var random = new Random();
                random.InitState((uint) seed.seed);
                var timeLength = soundData.timeLength;
                var frequency = random.NextFloat(generateSound.frequency.x, generateSound.frequency.y); // * timeLength;
                var noise = random.NextFloat(generateSound.noise.x, generateSound.noise.y);
                var sampleRate = (float) soundData.sampleRate;
                float maxPosition = sound.data.Length;
                //var decay = generateSound.decay;
                var attack = generateSound.attack;
                var volume = 1f; // sound.volume;
                var processCount = (int) (math.ceil(sound.data.Length / ((float)processSpeed)));
                //var startCount = ((int) (generateSound.generationState)) * processCount;
                var soundEndCount = math.min(sound.data.Length, generateSound.startCount + processCount);
                //for (int i = 0; i < sound.data.Length; i++)
                for (int i = generateSound.startCount; i < soundEndCount; i++)
                {
                    var percentage = ((float) i / sampleRate); //((float) timeLength) * ((float) i / maxPosition);
                    // another for i / sampleRate
                    // time value is?
                    var soundValue = 0f; //  generateSound.random.NextFloat(-generateSound.noise, generateSound.noise);
                    // piano
                    if (generateSound.instrumentType == 0)
                    {
                        soundValue += Modulate1(percentage, frequency,
                            math.pow(Modulate0(percentage, frequency, 0f), 2) + 
                                (0.75f * Modulate0(percentage, frequency, 0.25f)) + 
                                (0.5f * Modulate0(percentage, frequency, 0.5f)) );
                    }
                    // organ
                    else if (generateSound.instrumentType == 1)
                    {
                        soundValue += Modulate1(percentage, frequency,
                            (Modulate0(percentage, frequency, 0f)) + 
                            (0.5f * Modulate0(percentage, frequency, 0.25f)) + 
                            (0.25f * Modulate0(percentage, frequency, 0.5f)));
                    }
                    // edm
                    else if (generateSound.instrumentType == 2)
                    {
                        soundValue += Modulate0(percentage, frequency, 
                            (Modulate9(percentage, frequency, 
                                Modulate2(percentage, frequency, 
                                    math.pow(Modulate0(percentage, frequency, 0f),   3) + 
                                    math.pow(Modulate0(percentage, frequency, 0.5f), 5) + 
                                    math.pow(Modulate0(percentage, frequency, 1f),   7)     )))) +
                            Modulate8(percentage, frequency, Modulate1(percentage, frequency, 1.75f));
                    }
                    soundValue += random.NextFloat(-noise, noise);
                    // Linear build-up, fast.
                    if(i <= sampleRate * attack)
                    {
                        soundValue = soundValue * (i / (sampleRate * attack));
                    }
                    else //if (i >= 1 - sampleRate * decay)
                    {
                        //var dampen2 = Math.log((frequency*volume)/sampleRate);
                        //var dampen3 = 1 + (0.01 * frequency);   
                        var dampen = 0f;
                        if (generateSound.instrumentType == 0)
                        {
                            dampen = math.pow(0.5f * math.log((frequency * volume) / sampleRate), 2f);
                        }
                        else if (generateSound.instrumentType == 1)
                        {
                            dampen = 1 + (frequency * 0.01f);
                        }
                        else if (generateSound.instrumentType == 2)
                        {
                            dampen = 1;
                        }
                        soundValue = soundValue * math.pow((1f - ((i - (sampleRate * attack)) / (sampleRate * (timeLength - attack)))), dampen);
                    }
                    soundValue = math.clamp(soundValue, -1, 1);
                    sound.data[i] = soundValue;
                }
                generateSound.startCount += processCount;
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static float Modulate0(float percentage, float frequency, float x)
        {
            return 1 * math.sin(2 * pi * ((percentage) * frequency) + x);
        }

        public static float Modulate1(float percentage, float frequency, float x)
        {	
            return 1 * math.sin(4 * pi * ((percentage) * frequency) + x);
        }

        public static float Modulate2(float percentage, float frequency, float x)
        {	
            return 1 * math.sin(8f * pi * ((percentage) * frequency) + x);
        }

        public static float Modulate3(float percentage, float frequency, float x)
        {	
            return 1 * math.sin(0.5f * pi * ((percentage) * frequency) + x);
        }

        public static float Modulate4(float percentage, float frequency, float x)
        {	
            return 1 * math.sin(0.25f * pi * ((percentage) * frequency) + x);
        }

        public static float Modulate5(float percentage, float frequency, float x)
        {	
            return 0.5f * math.sin(2f * pi * ((percentage) * frequency) + x);
        }

        public static float Modulate6(float percentage, float frequency, float x)
        {	
            return 0.5f * math.sin(4f * pi * ((percentage) * frequency) + x);
        }

        public static float Modulate7(float percentage, float frequency, float x)
        {	
            return 0.5f * math.sin(8f * pi * ((percentage) * frequency) + x);
        }

        public static float Modulate8(float percentage, float frequency, float x)
        {	
            return 0.5f * math.sin(0.5f * pi * ((percentage) * frequency) + x);
        }

        public static float Modulate9(float percentage, float frequency, float x)
        {	
            return 0.5f * math.sin(0.25f * pi * ((percentage) * frequency) + x);
        }
    }
}

/*var sinValueA = 0.5f * generateSound.sinAmplitude * math.sin(timeLength * pi * 2 * frequency * percentage + soundValue); // * pi);
var sinValueB = 0.25f * generateSound.sinAmplitude * math.sin(timeLength * pi * 2 * frequency * percentage + sinValueA);
var sinValueC = 0.125f * generateSound.sinAmplitude * math.sin(timeLength * pi * 2 * frequency * percentage + sinValueB);
soundValue += sinValueA;
soundValue += sinValueB;
soundValue += sinValueC;*/


//soundValue /= 3;
// if (generateSound.isFadeOut == 1)
// {
//     soundValue *= 0.01f + (sound.data.Length - i) / maxPosition;
// }
/*soundValue += ModuleX(0, percentage, frequency, 
    (ModuleX(10, percentage, frequency, 
        ModuleX(3, percentage, frequency, 
            math.pow(ModuleX(0, percentage, frequency, 0f),   3) + 
            math.pow(ModuleX(0, percentage, frequency, 0.5f), 5) + 
            math.pow(ModuleX(0, percentage, frequency, 1f),   7)     )))) +
    ModuleX(9, percentage, frequency, ModuleX(1, percentage, frequency, 1.75f));*/
/*var secondPart = 
    (Modulate9(percentage, frequency, 
        Modulate3(percentage, frequency, 
            math.pow(Modulate1(percentage, frequency, 0f),   3) + 
            math.pow(Modulate1(percentage, frequency, 0.5f), 5) + 
            math.pow(Modulate1(percentage, frequency, 1f),   7)
    )));
soundValue += Modulate2(percentage, frequency, secondPart) +
    Modulate8(percentage, frequency, Modulate1(percentage, frequency, 1.75f));*/