using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Audio
{
    //! Holds a byte of floats for audio data, between -1 and 1.
    public struct Sound : IComponentData
    {
        const string soundName = "Zound";   // Sin
        public BlitableArray<float> data;   // audio data - convert to audio clip

        public void Dispose()
        {
            data.Dispose();
        }

        public void Initialize(in SoundData soundData) // float timeLength, int sampleRate)
        {
            this.data = new BlitableArray<float>((int)(soundData.timeLength * soundData.sampleRate), Allocator.Persistent);
        }

        public UnityEngine.AudioClip GetAudioClip(in SoundData soundData)
        {
            var length = (int) (soundData.timeLength * soundData.sampleRate * soundData.channels);
            if (length == 0)
            {
                return null;
            }
            var audioClip = UnityEngine.AudioClip.Create(soundName, length, soundData.channels, soundData.sampleRate, false);
            // audioClip.SetData(data.ToNativeArray(Allocator.None), 0);
            audioClip.SetData(data.ToArray(), 0);
            return audioClip;
        }
    }
}

// name
// samples count
// channels
// frequency
// is stream
/*if (timeLength == 0 || sampleRate == 0 || channels == 0)
{
    return null;
}*/