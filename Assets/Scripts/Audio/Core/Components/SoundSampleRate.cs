using Unity.Entities;

namespace Zoxel.Audio
{
    //! How many bytes per second plays in a sound.
    public struct SoundSampleRate : IComponentData
    {
        //! Default sampleRate is 44100.
        public int sampleRate;

        public SoundSampleRate(int sampleRate)
        {
            this.sampleRate = sampleRate;
        }
    }
}