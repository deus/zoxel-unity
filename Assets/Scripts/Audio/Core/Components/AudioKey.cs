using Unity.Entities;

namespace Zoxel.Audio
{
    public struct AudioKey : IComponentData
    {
        public float frequency;
        public float volume;
    }
}