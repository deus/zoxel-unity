using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Audio
{
    //! Holds a byte of floats for audio data, between -1 and 1.
    public struct SoundData : IComponentData
    {
        public int sampleRate;
        public int channels;
        public float timeLength;
        public float volume;

        public SoundData(float timeLength, int sampleRate, int channels, float volume)
        {
            this.sampleRate = sampleRate;
            this.timeLength = timeLength;
            this.channels = channels;
            this.volume =  volume;
        }

        public SoundData(ref Random random, float2 timeLength, int sampleRate, int channels, float2 volume)
        {
            this.timeLength = random.NextFloat(timeLength.x, timeLength.y);
            this.volume = random.NextFloat(volume.x, volume.y);
            this.sampleRate = sampleRate;
            this.channels = channels;
        }
    }
}
        //  = 1  = 44100

        /*public void Initialize(float timeLength, int sampleRate, int channels, float2 volume)
        {
            //var random = new Random();
            //random.InitState((uint) seed);
            this.sampleRate = sampleRate;
            this.timeLength = timeLength;
            this.channels = channels;
            this.volume = volume;
        }*/