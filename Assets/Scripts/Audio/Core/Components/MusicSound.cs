using Unity.Entities;

namespace Zoxel.Audio // .Music
{
    //! A tag of a sound entity that spawns from music.
    /**
    *   \todo Make a AudioMixerIndex for sounds, instead of using this tag here.
    */
    public struct MusicSound : IComponentData { }
}