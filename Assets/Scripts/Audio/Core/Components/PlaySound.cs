using Unity.Entities;

namespace Zoxel.Audio
{
    //! Plays a Sound.
    public struct PlaySound : IComponentData { }
}