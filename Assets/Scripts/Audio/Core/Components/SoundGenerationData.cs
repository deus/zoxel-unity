using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Audio
{
    //! Data to generate a sound.
    public struct SoundGenerationData : IComponentData
    {
        public byte generationState;
        public int startCount;
        public float noise;
        public float frequency;
        public float timeLength;
        public int sampleRate;
        public float attack;
        public Random random;
        public byte instrumentType;
    }
}