using Unity.Entities;
using Unity.Mathematics;
// Generate multiple wave layers
// Need to generate multiple sounds together - music or Jingles
//  Add a jingle at the start of the game
//  Add sounds to play during talking

namespace Zoxel.Audio
{
    //! A command to generate sounds.
    public struct GenerateSound : IComponentData
    {
        public byte generationState;
        public int startCount;
        public byte instrumentType;
        public int sampleRate;
        public float2 noise;
        public float2 frequency;
        public float2 timeLength;
        public float attack;
        // public Random random;

        // wave type int seed, 
        public void CreateMusicSound(int songNote, byte instrumentType, int sampleRate) // , float volume)
        {
            this.generationState = 0;
            this.sampleRate = 44100;
            var frequency2 = GenerateSound.GetNoteFrequency(songNote);
            this.frequency = new float2(frequency2, frequency2);
            this.noise = new float2(0.01f, 0.05f); // 0.01f + random.NextFloat(0.04f);
            this.attack = 0.002f;   // piano
            if (instrumentType == (byte) InstrumentType.Piano)
            {
                // piano
                this.attack = 0.002f;
                // this.timeLength = 0.8f + random.NextFloat(0.4f);
                this.timeLength = new float2(0.8f, 1.2f);
            }
            // organ
            else if (instrumentType == (byte) InstrumentType.Organ)
            {
                this.attack = 0.3f;
                // this.timeLength = 1.8f + random.NextFloat(0.4f);
                this.timeLength = new float2(1.8f, 2.2f);
            }
            else if (instrumentType == (byte)InstrumentType.EDM) 
            {
                this.attack = 0.002f;
                // this.timeLength = 3.8f + random.NextFloat(0.4f);
                this.timeLength = new float2(3.8f, 4.2f);
            }
            // this.volume = volume;
            // this.random = new Random();
            // this.random.InitState((uint)seed);
            // this.sinAmplitude = 0.4f + random.NextFloat(0.1f);
            // this.noise = 0.01f + random.NextFloat(0.04f);
            //this.decay = timeLength * 0.5f;  // 0.7f
        }

        // wave type
        public void InitializeUISound()
        {
            this.generationState = 0;
            this.sampleRate = 44100;
            this.timeLength = new float2((1.87f / 2f) - 0.1f,  (1.87f / 2f) + 0.1f);
            this.attack = 0.002f;   // piano
            this.instrumentType = 0; // 2;
            var frequency2 = GenerateSound.GetNoteFrequency(40);
            this.frequency = new float2(frequency2, frequency2);
            this.noise = new float2(0f, 0.06f); 
            //this.random = new Random();
            //this.random.InitState((uint) seed);
            //this.timeLength = (1.87f / 2f) + random.NextFloat(-0.1f, 0.1f); // + random.NextFloat(0.2f); // 0.4f + random.NextFloat(0.4f);
            // this.volume = this.random.NextFloat(volume.x, volume.y);   // 0.34f; // + random.NextFloat(0.16f);
            // this.frequency = GenerateSound.GetNoteFrequency(36 + random.NextInt(-4, 4));
            // this.noise = random.NextFloat(0.06f);
        }

        public void InitializeShootSound()
        {
            this.generationState = 0;
            this.sampleRate = 44100;
            this.timeLength = new float2(0.8f, 1.2f);
            this.attack = 0.002f;   // piano
            this.noise = new float2(0.1f, 0.22f); 
            this.frequency = new float2(120, 160);
            // this.random = new Random();
            // this.random.InitState((uint)seed);
            // this.noise = 0.10f + random.NextFloat(0.12f); //  + UnityEngine.Random.Range(-0.02f, 0.04f);
            // this.frequency = 120 + random.NextFloat(40f); //  + UnityEngine.Random.Range(-40, 40); //420;
            //this.sinAmplitude = 0.7f + random.NextFloat(0.2f); //  + UnityEngine.Random.Range(-0.1f, 0.1f);
            // this.timeLength = 0.8f + random.NextFloat(0.4f); // UnityEngine.Random.Range(-0.3f, 0.3f);
            //this.decay = timeLength * 0.7f;
        }

        public void InitializeItemPickupSound()
        {
            this.generationState = 0;
            this.sampleRate = 44100;
            this.instrumentType = 1; // 2;
            this.timeLength = new float2(2.4f, 2.8f);
            this.noise = new float2(0.3f, 0.6f);
            this.frequency = new float2(220, 260);
            this.attack = 0.25f;
            //this.random = new Random();
            //this.random.InitState((uint)seed);
            // this.noise = 0.3f + random.NextFloat(0.3f);
            // this.frequency = 220 + random.NextInt(40); //  UnityEngine.Random.Range(-40, 40);
            // var frequency2 = GenerateSound.GetNoteFrequency(40);
            //this.sinAmplitude = 0.4f + random.NextFloat(0.2f);
            // this.timeLength = 0.5f + random.NextFloat(0.3f);
            //this.attack = 0.1f;   // piano
            //this.decay = timeLength * 0.7f;
            //this.instrumentType = 1;    // organ
            // this.timeLength = 2.4f + random.NextFloat(0.4f);
            //this.decay = timeLength * 0.5f;
            // this.volume = 0.08f; // + random.NextFloat(0.16f);
        }

        /*public void InitializeLevelUpSound()
        {
            generationState = 0;
            timeLength = 0.4f + UnityEngine.Random.Range(-0.2f, 0.2f);
            noise = 0.02f + UnityEngine.Random.Range(-0.02f, 0.04f);
            frequency = 260 + UnityEngine.Random.Range(-60, 60); //420;
            sinAmplitude = 0.8f + UnityEngine.Random.Range(-0.1f, 0.1f);
            sampleRate = 44100;
        }*/

        public static float GetNoteFrequency(int note)
        {
            return NoteFrequencies.noteFrequencies[note];
        }
    }
}
            /*var noteType = 48;
            var randomNote = random.NextInt(noteType, noteType + 11);
            // empathesis
            if (count == 0)
            {
                randomNote = music.notes[0];
            }
            else
            {
                randomNote = music.notes[random.NextInt(1, music.notes.Length - 1)];
            }*/
            /*float frequency = 0;
            if (randomNote == 0)
            {
                frequency = 16.35f; // C
            }
            else if (randomNote == 1)
            {
                frequency = 17.32f; // C#
            }
            else if (randomNote == 3)
            {
                frequency = 18.35f; // D
            }
            else if (randomNote == 4)
            {
                frequency = 19.45f; // D#
            }
            else if (randomNote == 4)
            {
                frequency = 20.60f; // E
            }
            else if (randomNote == 5)
            {
                frequency = 21.83f; // F
            }
            else if (randomNote == 6)
            {
                frequency = 23.12f; // F #
            }
            else if (randomNote == 7)
            {
                frequency = 24.50f; // G
            }
            else if (randomNote == 8)
            {
                frequency = 25.96f; // G #
            }
            else if (randomNote == 9)
            {
                frequency = 27.50f; // A
            }
            else if (randomNote == 10)
            {
                frequency = 29.14f; // A #
            }
            else if (randomNote == 11)
            {
                frequency = 30.87f; // B
            }

            // first ones
            else if (randomNote == 12)
            {
                frequency = 32.70f; // C1
            }
            else if (randomNote == 13)
            {
                frequency = 34.65f; // C1#
            }
            else if (randomNote == 14)
            {
                frequency = 36.71f; // D1
            }
            else if (randomNote == 15)
            {
                frequency = 38.89f; // D1#
            }
            else if (randomNote == 16)
            {
                frequency = 41.20f; // E1
            }
            else if (randomNote == 17)
            {
                frequency = 43.65f; // F1
            }
            else if (randomNote == 18)
            {
                frequency = 46.25f; // F1#
            }
            else if (randomNote == 19)
            {
                frequency = 49.00f; // G1
            }
            else if (randomNote == 20)
            {
                frequency = 51.91f; // G1#
            }
            else if (randomNote == 21)
            {
                frequency = 55.00f; // A1
            }
            else if (randomNote == 22)
            {
                frequency = 58.27f; // A1#
            }
            else if (randomNote == 23)
            {
                frequency = 61.74f; // B1
            }

            // level 2
            else if (randomNote == 24)
            {
                frequency = 69.30f; // C2
            }
            else if (randomNote == 25)
            {
                frequency = 69.30f; // C2#
            }
            else if (randomNote == 26)
            {
                frequency = 73.42f; // D2
            }
            else if (randomNote == 27)
            {
                frequency = 77.78f; // D2#
            }
            else if (randomNote == 28)
            {
                frequency = 82.41f; // E2
            }
            else if (randomNote == 29)
            {
                frequency = 87.31f; // F2
            }
            else if (randomNote == 30)
            {
                frequency = 92.50f; // F2#
            }
            else if (randomNote == 31)
            {
                frequency = 98.00f; // G2
            }
            else if (randomNote == 32)
            {
                frequency = 103.83f; // G2#
            }
            else if (randomNote == 33)
            {
                frequency = 110.00f; // A2
            }
            else if (randomNote == 34)
            {
                frequency = 116.54f; // A2#
            }
            else if (randomNote == 35)
            {
                frequency = 123.47f; // B2
            }

            // level 3
            else if (randomNote == 36)
            {
                frequency = 130.81f; // C3
            }
            else if (randomNote == 37)
            {
                frequency = 138.59f; // C3#
            }
            else if (randomNote == 38)
            {
                frequency = 146.83f; // D3
            }
            else if (randomNote == 39)
            {
                frequency = 155.56f; // D3#
            }
            else if (randomNote == 40)
            {
                frequency = 164.81f; // E3
            }
            else if (randomNote == 41)
            {
                frequency = 174.61f; // F3
            }
            else if (randomNote == 42)
            {
                frequency = 185.00f; // F3#
            }
            else if (randomNote == 43)
            {
                frequency = 196.00f; // G3
            }
            else if (randomNote == 44)
            {
                frequency = 207.65f; // G3#
            }
            else if (randomNote == 45)
            {
                frequency = 220.00f; // A3
            }
            else if (randomNote == 46)
            {
                frequency = 233.08f; // A3#
            }
            else if (randomNote == 47)
            {
                frequency = 246.94f; // B3
            }

            // level 4
            else if (randomNote == 48)
            {
                frequency = 261.63f; // C4
            }
            else if (randomNote == 49)
            {
                frequency = 277.18f; // C4#
            }
            else if (randomNote == 50)
            {
                frequency = 293.66f; // D4
            }
            else if (randomNote == 51)
            {
                frequency = 311.13f; // D4#
            }
            else if (randomNote == 52)
            {
                frequency = 329.63f; // E4
            }
            else if (randomNote == 53)
            {
                frequency = 349.23f; // F4
            }
            else if (randomNote == 54)
            {
                frequency = 369.99f; // F4#
            }
            else if (randomNote == 55)
            {
                frequency = 392.00f; // G4
            }
            else if (randomNote == 56)
            {
                frequency = 415.30f; // G4 #
            }
            else if (randomNote == 57)
            {
                frequency = 440.00f; // A4
            }
            else if (randomNote == 58)
            {
                frequency = 466.16f; // A4 #
            }
            else if (randomNote == 59)
            {
                frequency = 493.88f; // B4
            }

            // level 5
            else if (randomNote == 60)
            {
                frequency = 523.25f; // C5
            }
            else if (randomNote == 61)
            {
                frequency = 554.37f; // C5#
            }
            else if (randomNote == 62)
            {
                frequency = 587.33f; // D5
            }
            else if (randomNote == 63)
            {
                frequency = 622.25f; // D5#
            }
            else if (randomNote == 64)
            {
                frequency = 659.25f; // E5
            }
            else if (randomNote == 65)
            {
                frequency = 698.46f; // F5
            }
            else if (randomNote == 66)
            {
                frequency = 739.99f; // F5#
            }
            else if (randomNote == 67)
            {
                frequency = 783.99f; // G5
            }
            else if (randomNote == 68)
            {
                frequency = 830.61f; // G5 #
            }
            else if (randomNote == 69)
            {
                frequency = 880.00f; // A5
            }
            else if (randomNote == 70)
            {
                frequency = 932.33f; // A5#
            }
            else if (randomNote == 71)
            {
                frequency = 987.77f; // B5
            }
            return frequency;*/