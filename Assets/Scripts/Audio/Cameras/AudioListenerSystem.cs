using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Cameras;

namespace Zoxel.Audio
{
    //! Positions the audio listener as an avrage of camra positions.
    [UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class AudioListenerSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            if (AudioManager.instance == null) return;
            var audioListener = AudioManager.instance.audioListener;
            if (audioListener == null)
            {
                return;
            }
            var camerasCount = 0;
            var cameraPosition = new float3();
            Entities
                .WithAll<Camera>()
                .ForEach((in Translation translation, in CameraEnabledState cameraEnabledState) =>
            {
                if (float.IsNaN(translation.Value.x)) return;
                if (cameraEnabledState.state == 1)
                {
                    cameraPosition += translation.Value;
                    camerasCount++;
                }
            }).WithoutBurst().Run();
            if (camerasCount != 0) audioListener.transform.position = cameraPosition / (float) camerasCount;
        }
    }
}