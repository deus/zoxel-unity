using Unity.Entities;

namespace Zoxel.Audio.Music
{
    //! Handles all musicy things.
    [UpdateInGroup(typeof(AudioSystemGroup))]
    public partial class MusicSystemGroup : ComponentSystemGroup { }
}
