using Unity.Entities;

namespace Zoxel.Audio.Music.UI
{
    //! Handles all musicy things.
    [UpdateInGroup(typeof(MusicSystemGroup))]
    public partial class MusicUISystemGroup : ComponentSystemGroup { }
}
