using Unity.Entities;

namespace Zoxel.Audio.Music.UI
{
    //! A tag for a music button ui.
    public struct MusicButton : IComponentData { }
}