using Unity.Entities;

namespace Zoxel.Audio.Music.UI
{
    //! A tag for a music ui.
    public struct MusicUI : IComponentData { }
}