using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.UI;

namespace Zoxel.Audio.Music.UI
{
    //! Spawns a MusicUI.
    /**
    *   - UI Spawn System -
    *   \todo Refactor all UI Spawn Systems to be like this one.
    */
    [BurstCompile, UpdateInGroup(typeof(MusicUISystemGroup))]
    public partial class MusicUISpawnSystem : SystemBase
    {
        private static bool isTesting;
        private NativeArray<Text> texts;
        public static Entity spawnSpawnMusicUIPrefab;
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;
        private Entity panelPrefab;
        private Entity buttonPrefab;

        /*protected override void OnCreate()
        {
            uiHoldersQuery = GetEntityQuery(
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<UILink>());
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnMusicUI),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnSpawnMusicUIPrefab = EntityManager.CreateEntity(spawnArchetype);
            var texts2 = new NativeList<Text>();
            texts2.Add(new Text("Music"));
            texts2.Add(new Text("42"));
            texts = texts2.ToArray(Allocator.Persistent);
            texts2.Dispose();
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }*/

        private void InitializePrefabs()
        {
            if (panelPrefab.Index != 0)
            {
                return;
            }
            panelPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(panelPrefab);
            EntityManager.AddComponent<MusicUI>(panelPrefab);
            EntityManager.AddComponent<MusicLink>(panelPrefab);
            EntityManager.SetComponentData(panelPrefab, new PanelUI(PanelType.MusicUI));
            EntityManager.RemoveComponent<SpawnHeaderUI>(panelPrefab);
            buttonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(buttonPrefab);
            EntityManager.AddComponent<MusicButton>(buttonPrefab);
            EntityManager.AddComponent<IgnoreGrid>(buttonPrefab);
            // testing, keep here for now until I finish testing it
            //      - this spawns it at origin
            if (MusicUISpawnSystem.isTesting)
            {
                EntityManager.SetComponentData(panelPrefab, new Translation { Value = new float3(0, 0, 0.06f) });
                EntityManager.SetComponentData(panelPrefab, new Rotation { Value = new quaternion(new float4(0, 0, 0, -1f)) });
                EntityManager.RemoveComponent<OrbitTransform>(panelPrefab);
                EntityManager.RemoveComponent<OrbitPosition>(panelPrefab);
                EntityManager.AddComponent<MusicLink>(panelPrefab);
                EntityManager.AddComponent<SpawnMusic>(panelPrefab);
            }
        }

        public static void TestUI(EntityManager EntityManager)
        {
            // test spawn music
            EntityManager.SetComponentData(spawnSpawnMusicUIPrefab, new DelayEvent(0, 3));
            EntityManager.Instantiate(spawnSpawnMusicUIPrefab);
            MusicUISpawnSystem.isTesting = true;
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            /*InitializePrefabs();
            HeaderSpawnSystem.InitializePrefabs(EntityManager);
            var headerPrefab = HeaderSpawnSystem.headerPrefab;
            var uiDatam = UIManager.instance.uiDatam;
            var headerStyle = uiDatam.headerStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;   // move to ui system group
            var buttonPrefab = this.buttonPrefab;
            var panelPrefab = this.panelPrefab;
            var texts = this.texts;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uiHolderEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
            uiHolderEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<SpawnMusicUI>()
                .ForEach((int entityInQueryIndex, in CharacterLink characterLink) =>
            {
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, panelPrefab);
                if (characterLink.character.Index > 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, characterLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab),
                        new OnUISpawned(characterLink.character, 1));
                    if (cameraLinks.HasComponent(characterLink.character))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLinks[characterLink.character]);
                    }
                }
                // link to music
                // spawn notes from music
                var buttonEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, buttonPrefab, e3);
                // UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, buttonEntity, i);
                var noteText = texts[1].Clone();
                PostUpdateCommands.SetComponent(entityInQueryIndex, buttonEntity, new RenderText(noteText));

                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawned((byte) 1));

                var panelSize = new float2(0.094f, 0.094f);
                var headerText = texts[0].Clone();
                HeaderSpawnSystem.SpawnHeaderUI(PostUpdateCommands, entityInQueryIndex, headerPrefab, e3, panelSize, headerStyle.fontSize, headerStyle.textPadding,
                    in headerText);
			})  .WithReadOnly(texts)
                .WithReadOnly(cameraLinks)
                .ScheduleParallel(Dependency);*/
        }
    }
}