using Unity.Entities;

namespace Zoxel.Audio.Music.UI
{
    //! An event to spawn MusicUI.
    public struct SpawnMusicUI : IComponentData { }
}