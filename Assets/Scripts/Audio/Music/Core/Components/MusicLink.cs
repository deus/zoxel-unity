using Unity.Entities;

namespace Zoxel.Audio.Music
{
    //! Links to a music entity that plays musics.
    public struct MusicLink : IComponentData
    {
        public Entity music;

        public MusicLink(Entity music)
        {
            this.music = music;
        }
    }
}