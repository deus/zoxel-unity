using Unity.Entities;

namespace Zoxel.Audio.Music
{
    public struct MusicEnabledState : IComponentData
    {
        public bool state;

        public MusicEnabledState(bool state)
        {
            this.state = state;
        }
    }
}