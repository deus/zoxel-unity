using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Audio.Music
{
    //! Attached to an entity that plays music beeps in patterns.
    public struct Music : IComponentData
    {
        public float volume;
        public double lastPlayedTime;
        public byte hasInitialized;
        public int noteCount;
        public int bleepCount;
        private int bleepToPlay;
        public byte instrumentType;
        // every 8 bleeps, regenerate them
        public BlitableArray<Bleep> bleeps;
        public MusicPallete pallete;
        public double nextNoteSoundTime;
        private int seed;
        public Random random;
        public float3 lastPosition;

        public void SetSeed(int seed)
        {
            if (this.seed != seed && seed != 0)
            {
                this.seed = seed;
                this.random = new Random();
                this.random.InitState((uint)(seed * 10));
            }
        }

        public void RandomizeNextSoundTime(int seed, float2 beepTiming)
        {
            nextNoteSoundTime = (double) (beepTiming.x + random.NextFloat(beepTiming.y - beepTiming.x));
            if (instrumentType == 1)
            {
                nextNoteSoundTime *= 2;
            }
        }

        private void RandomizeNextBleep(int seed)
        {
            var lastBleepToPlay = bleepToPlay;
            var count = 0;
            while (bleepToPlay == lastBleepToPlay)
            {
                bleepToPlay = this.random.NextInt(0, bleeps.Length);
                count++;
                if (count >= 32)
                {
                    break;
                }
            }
            //UnityEngine.Debug.LogError("New Beep to play: " + bleepToPlay);
        }

        public void IncrementNote(int seed, int bleepsPerMusic)
        {
            var bleep = bleeps[this.bleepToPlay];
            noteCount++;
            noteCount %= bleep.palleteIndexes.Length;
            if (noteCount == 0)
            {
                this.RandomizeNextBleep(seed);
                bleepCount++;
                bleepCount %= bleepsPerMusic; // (bleeps.Length - 1);
            }
        }

        public void Dispose()
        {
            if (this.bleeps.Length > 0)
            {
                for (int i = 0; i < this.bleeps.Length; i++)
                {
                    this.bleeps[i].Dispose();
                }
                this.bleeps.Dispose();
            }
            pallete.Dispose();
        }

        public void GenerateMusic(int seed, float2 volume, float2 notesRange, int bleepPalleteCount = 8, int notesPerBleep = 8)
        {
            this.volume = random.NextFloat(volume.x, volume.y);
            this.bleepCount = 0;
            Dispose();
            this.pallete.GenerateNotes(ref this.random, notesRange);
            this.bleeps = new BlitableArray<Bleep>(bleepPalleteCount, Allocator.Persistent);
            var notesTotal = this.pallete.notes.Length;
            for (int i = 0; i < this.bleeps.Length; i++)
            {
                var bleep = new Bleep();
                bleep.GenerateBleep(ref random, notesTotal, notesPerBleep); //  seed + i * 2048,
                this.bleeps[i] = bleep;
            }
            this.bleepToPlay = random.NextInt(0, bleeps.Length - 1);
            this.instrumentType = (byte) random.NextInt(0, 2);
        }

        public bool GetBass()
        {
            var bleep = this.bleeps[this.bleepToPlay];
            return (bleep.isBass == 1);
        }

        public byte GetInstrumentType()
        {
            return instrumentType;
            //var bleep = this.bleeps[this.bleepToPlay];
            //return bleep.instrumentType;
        }

        public int GetNote()
        {
            var bleep = this.bleeps[this.bleepToPlay];
            if (this.noteCount < 0 || this.noteCount >= bleep.palleteIndexes.Length)
            {
                return -1;
            }
            var palleteIndex = bleep.palleteIndexes[this.noteCount];
            if (palleteIndex < 0 || palleteIndex >= this.pallete.notes.Length)
            {
                return -1;
            }
            return this.pallete.notes[palleteIndex];
        }
    }
}