using Unity.Entities;

namespace Zoxel.Audio.Music
{
    //! Disables music playing.
    public struct DisableMusic : IComponentData { }
}