using Unity.Entities;

namespace Zoxel.Audio.Music
{
    //! Spawns music from and links to an entity.
    public struct SpawnMusic : IComponentData { }
}