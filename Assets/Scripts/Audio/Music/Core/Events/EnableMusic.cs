using Unity.Entities;

namespace Zoxel.Audio.Music
{
    //! Enables music playing.
    public struct EnableMusic : IComponentData { }
}