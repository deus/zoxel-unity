using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Audio.Music
{
    //! Initializes music seed.
    /**
    *   \todo Convert to Parallel. (ID Generation)
    */
    [BurstCompile, UpdateInGroup(typeof(MusicSystemGroup))]
    public partial class MusicInitializeSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithAll<InitializeEntity, Music>()
                .ForEach((ref Seed seed) =>
            {
                seed.seed = IDUtil.GenerateUniqueID();
            }).WithoutBurst().Run();
        }
    }
}