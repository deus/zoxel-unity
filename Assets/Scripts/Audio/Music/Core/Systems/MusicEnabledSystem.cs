using Unity.Entities;
using Zoxel.Transforms;

namespace Zoxel.Audio.Music
{
    //! Pushes CameraEnabledState data to unity camera component.
    [UpdateInGroup(typeof(MusicSystemGroup))]
    public partial class MusicEnabledSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithChangeFilter<MusicEnabledState>()
                .WithNone<InitializeEntity>()
                .ForEach((in MusicEnabledState musicEnabledState, in GameObjectLink gameObjectLink) =>
            {
                UpdateEnabledState(gameObjectLink.gameObject, in musicEnabledState);
            }).WithoutBurst().Run();
        }

        public static void UpdateEnabledState(UnityEngine.GameObject gameObject, in MusicEnabledState musicEnabledState)
        {
            if (gameObject == null)
            {
                return;
            }
            var audioSource = gameObject.GetComponent<UnityEngine.AudioSource>();
            if (audioSource == null)
            {
                return;
            }
            if (musicEnabledState.state)
            {
                audioSource.Play();
            }
            else
            {
                audioSource.Stop();
            }
        }
    }
}