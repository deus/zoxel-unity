using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;

namespace Zoxel.Audio.Music
{
    //! Enables and disables music
    [BurstCompile, UpdateInGroup(typeof(MusicSystemGroup))]
    public partial class MusicEnableSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<EnableMusic>()
                .ForEach((Entity e, int entityInQueryIndex, in MusicLink musicLink) =>
            {
                PostUpdateCommands.RemoveComponent<EnableMusic>(entityInQueryIndex, e);
                PostUpdateCommands.RemoveComponent<DisableMusic>(entityInQueryIndex, musicLink.music);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<DisableMusic>()
                .ForEach((Entity e, int entityInQueryIndex, in MusicLink musicLink) =>
            {
                PostUpdateCommands.RemoveComponent<DisableMusic>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<DisableMusic>(entityInQueryIndex, musicLink.music);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}