using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;

namespace Zoxel.Audio.Music
{
    //! Spawns music entity as a child.
    /**
    *   - Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(MusicSystemGroup))]
    public partial class MusicSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (AudioManager.instance == null) return;
            if (AudioManager.instance.disableMusic || AudioManager.instance.musicLevel == 0)
            {
                return;
            }
            var musicPrefab = MusicPlaySystem.musicPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity>()
                .WithAll<SpawnMusic, MusicLink>()
                .ForEach((Entity e, int entityInQueryIndex, in Translation translation) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnMusic>(entityInQueryIndex, e);
                var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, musicPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ParentLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, translation);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e, new MusicLink(e2));
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}