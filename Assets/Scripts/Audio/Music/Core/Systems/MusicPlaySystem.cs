using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Unity.Jobs;
using Zoxel.Transforms;
// if x time has passed
// create a new soundData
// with generate on it
// and play it
// destroy it after play too

namespace Zoxel.Audio.Music
{
    //! Plays music by spawning sounds.
    /**
    *   \todo Seperate note generation into verses (for ui) music data.
    */
    [BurstCompile, UpdateInGroup(typeof(MusicSystemGroup))]
    public partial class MusicPlaySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity musicPrefab;
        private Entity soundPrefab;
        // public static float3 musicPosition;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var musicArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(Music),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(Translation),
                typeof(Seed));
            musicPrefab = EntityManager.CreateEntity(musicArchetype);
        }

        private void InitializePrefabs()
        {
            if (soundPrefab.Index == 0)
            {
                soundPrefab = EntityManager.Instantiate(SoundPlaySystem.soundPrefab);
                EntityManager.AddComponent<Prefab>(soundPrefab);
                EntityManager.AddComponent<MusicSound>(soundPrefab);
            }
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (AudioManager.instance == null) return;
            if (AudioManager.instance.disableMusic || AudioManager.instance.musicLevel == 0)
            {
                return;
            }
            InitializePrefabs();
            var randomID = IDUtil.GenerateUniqueID();
            var soundPrefab = this.soundPrefab;
            // var audioListenerPosition = musicPosition;
            var beepTiming = AudioManager.instance.beepTiming;
            var disableBass = AudioManager.instance.disableBass;
            var forceBass = AudioManager.instance.forceBass;
            byte instrumentType = AudioManager.instance.instrumentType;
            var sampleRate = AudioManager.instance.sampleRate;
            int bleepsPerMusic = 12; // 8
            int bleepPalleteCount = 6; // 8
            int notesPerBleep = 8;
            var elapsedTime = World.Time.ElapsedTime;
            var musicVolume = AudioManager.instance.musicVolume;
            var musicVolumeAddition = AudioManager.instance.musicVolumeAddition;
            var musicNotesRange = AudioManager.instance.musicNotesRange;
            var musicDistance = AudioManager.instance.musicDistance;    // 5f
            var musicDistanceAdd = AudioManager.instance.musicDistanceAdd;    // 5f
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, DisableMusic>()
                .ForEach((Entity e, int entityInQueryIndex, ref Music music, in Seed seed, in Translation translation) =>
            {
                if (elapsedTime - music.lastPlayedTime >= music.nextNoteSoundTime)    // 0.25
                {
                    var soundSeed = seed.seed + (int)(256 * elapsedTime);
                    music.SetSeed(soundSeed);
                    music.lastPlayedTime = elapsedTime;
                    music.RandomizeNextSoundTime(soundSeed, beepTiming);
                    if (music.noteCount == 0 && music.bleepCount == 0)
                    {
                        music.GenerateMusic(soundSeed, musicVolume, musicNotesRange, bleepPalleteCount, notesPerBleep); // regenerate new bleeps
                    }
                    music.IncrementNote(soundSeed, bleepsPerMusic);
                    var note = music.GetNote();
                    var random = new Random();
                    random.InitState((uint) soundSeed);
                    var noteVolume = new float2(music.volume, music.volume + musicVolumeAddition);
                    // var noteVolume = music.volume + random.NextFloat(musicVolumeAddition);
                    var isBass = music.GetBass();
                    music.lastPosition += new float3(
                        random.NextFloat(musicDistanceAdd.x, musicDistanceAdd.y),
                        random.NextFloat(musicDistanceAdd.x, musicDistanceAdd.y),
                        random.NextFloat(musicDistanceAdd.x, musicDistanceAdd.y));
                    music.lastPosition = new float3(
                        math.clamp(music.lastPosition.x, musicDistance.x, musicDistance.y),
                        math.clamp(music.lastPosition.y, musicDistance.x, musicDistance.y),
                        math.clamp(music.lastPosition.z, musicDistance.x, musicDistance.y)
                    );
                    var musicPosition = translation.Value;
                    var position = new float3(musicPosition.x, musicPosition.y, musicPosition.z) + music.lastPosition;
                    var noteInstrument = instrumentType;
                    if (note != -1)
                    {
                        var soundEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, soundPrefab);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new DelayEvent(elapsedTime, 0.1));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Translation { Value = position});
                        PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Seed(soundSeed));
                        var generateSound = new GenerateSound();
                        if (instrumentType == 255)
                        {
                            noteInstrument = music.GetInstrumentType();
                        }
                        generateSound.CreateMusicSound(note, noteInstrument, sampleRate);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                        var audioLength = random.NextFloat(generateSound.timeLength.x, generateSound.timeLength.y);
                        var noteVolume2 = random.NextFloat(noteVolume.x, noteVolume.y);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new SoundData(audioLength, generateSound.sampleRate, 1, noteVolume2));
                    }
                    // with bass
                    if (forceBass || (!disableBass && isBass && note != -1))
                    {
                        var soundEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, soundPrefab);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new DelayEvent(elapsedTime, 0.1));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Translation { Value = position });
                        PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new Seed(soundSeed));
                        var generateSound = new GenerateSound();
                        generateSound.CreateMusicSound(note - 12, noteInstrument, sampleRate);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, generateSound);
                        var audioLength = random.NextFloat(generateSound.timeLength.x, generateSound.timeLength.y);
                        var noteVolume2 = random.NextFloat(noteVolume.x, noteVolume.y);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, soundEntity, new SoundData(audioLength, generateSound.sampleRate, 1, noteVolume2));
                    }
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}