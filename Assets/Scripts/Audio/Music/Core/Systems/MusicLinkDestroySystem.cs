using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Audio.Music
{
    //! Uses DestroyEntity to dispose of MapPart and destroy MapUI icons.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
	public partial class MusicLinkDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyEntity, MusicLink>()
                .ForEach((int entityInQueryIndex, in MusicLink musicLink) =>
            {
                if (HasComponent<Music>(musicLink.music))
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, musicLink.music);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}