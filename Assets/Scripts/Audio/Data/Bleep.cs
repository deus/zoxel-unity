using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Audio
{    
    public struct Bleep
    {
        public byte instrumentType;
        public byte isBass;
        public BlitableArray<int> palleteIndexes;

        public void Dispose()
        {
            if (this.palleteIndexes.Length > 0)
            {
                this.palleteIndexes.Dispose();
            }
        }

        //  int seed, 
        public void GenerateBleep(ref Random random, int maxPallete, int notesCount = 8)
        {
            //Dispose();
            this.isBass = (byte) random.NextInt(0, 1);
            this.instrumentType = (byte) random.NextInt(0, 2);
            var randomChance = random.NextInt(50, 70);
            var randomChance2 = random.NextInt(70, 90);
            var interval = random.NextInt(1, 6);
            this.palleteIndexes = new BlitableArray<int>(notesCount, Allocator.Persistent);
            this.palleteIndexes[0] = 0;
            // using intervals from last
            for (int i = 1; i < this.palleteIndexes.Length; i++)
            {
                var emptyNote = random.NextInt(100);
                if (emptyNote >= randomChance2)
                {
                    this.palleteIndexes[i] = -1;
                }
                else if ((i + 1) % 4 == 0 && emptyNote >= randomChance)
                {
                    this.palleteIndexes[i] = -1;
                }
                else
                {
                    var lastIndex = this.palleteIndexes[i - 1];
                    var newIndex = lastIndex;
                    var count = 0;
                    while (newIndex == lastIndex)
                    {
                        if (lastIndex <= 1)
                        {
                            newIndex = math.clamp(1 + random.NextInt(1, interval + 1),
                                1, maxPallete - 1);
                        }
                        else if (lastIndex == maxPallete - 1)
                        {
                            newIndex = math.clamp(lastIndex + random.NextInt(-interval - 1, -1),
                                1, maxPallete - 1);
                        }
                        else
                        {
                            newIndex = math.clamp(lastIndex + random.NextInt(-interval, interval),
                                1, maxPallete - 1);
                        }
                        count++;
                        if (count >= 32)
                        {
                            break;
                        }
                    }
                    this.palleteIndexes[i] = newIndex;
                }
                //UnityEngine.Debug.LogError("Set " + i + " to " + this.palleteIndexes[i]);
            }
        }
    }
}