using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Audio
{    
    public struct MusicPallete
    {
        public BlitableArray<int> notes;

        public void Dispose()
        {
            if (this.notes.Length > 0)
            {
                this.notes.Dispose();
            }
        }

        public void GenerateNotes(ref Random random, float2 notesRange) //int startNote = 40)
        {
            Dispose();
            var startNote = random.NextInt((int)notesRange.x, (int)notesRange.y);
            var palleteType = random.NextInt(1, 5);
            //UnityEngine.Debug.LogError("Pallete Type set to: " + palleteType);
            // using scaleHarmonicMinor {2,1,2,2,1,3,1}; // (haunting, creepy)
            //startNote = 40;
            // haunting/creepy harmonics
            if (palleteType == 1)
            {
                this.notes = new BlitableArray<int>(7, Allocator.Persistent);
                this.notes[0] = startNote;
                this.notes[1] = startNote + 1;
                this.notes[2] = startNote + 3;
                this.notes[3] = startNote + 5;
                this.notes[4] = startNote + 6;
                this.notes[5] = startNote + 9;
                this.notes[6] = startNote + 10;
            }
            // happy
            else if (palleteType == 2)
            {
                this.notes = new BlitableArray<int>(7, Allocator.Persistent);
                this.notes[0] = startNote;
                this.notes[1] = startNote + 2;
                this.notes[2] = startNote + 3;
                this.notes[3] = startNote + 5;
                this.notes[4] = startNote + 7;
                this.notes[5] = startNote + 9;
                this.notes[6] = startNote + 10;
            }
            // blues
            else if (palleteType == 3)
            {
                this.notes = new BlitableArray<int>(5, Allocator.Persistent);
                this.notes[0] = startNote;
                this.notes[1] = startNote + 2;
                this.notes[2] = startNote + 4;
                this.notes[3] = startNote + 7;
                this.notes[4] = startNote + 9;
            }
            // blues
            else if (palleteType == 4)
            {
                this.notes = new BlitableArray<int>(7, Allocator.Persistent);
                this.notes[0] = startNote;
                this.notes[1] = startNote + 1;
                this.notes[2] = startNote + 3;
                this.notes[3] = startNote + 5;
                this.notes[4] = startNote + 6;
                this.notes[5] = startNote + 8;
                this.notes[6] = startNote + 10;
            }
            // chaos
            else
            {
                this.notes = new BlitableArray<int>(12, Allocator.Persistent);
                this.notes[0] = startNote + random.NextInt(16);
                for (int i = 1; i < this.notes.Length; i++)
                {
                    var oldNote = this.notes[i - 1];
                    var newNote = oldNote;
                    var count = 0;
                    while (newNote == oldNote)
                    {
                        newNote = startNote + random.NextInt(16);
                        count++;
                        if (count >= 32)
                        {
                            break;
                        }
                    }
                    this.notes[i] = newNote;
                }
            }
        }
    }
}

/*
    - musical scales in semitone intervals:
scaleChromatic = {1,1,1,1,1,1,1,1,1,1,1}; // (random, atonal: all twelve notes)
scaleMajor = {2,2,1,2,2,2,1}; // (classic, happy)
scaleHarmonicMinor = {2,1,2,2,1,3,1}; // (haunting, creepy)
scaleMinorPentatonic = {3,2,2,3,2}; // (blues, rock)
scaleNaturalMinor = {2,1,2,2,1,2,2}; // (scary, epic)
scaleMelodicMinorUp = {2,1,2,2,2,2,1}; // (wistful, mysterious)
scaleMelodicMinorDown = {2,2,1,2,2,1,2};  // (sombre, soulful)
scaleDorian = {2,1,2,2,2,1,2}; // (cool, jazzy)
scaleMixolydian = {2,2,1,2,2,1,2}; // (progressive, complex)
scaleAhavaRaba = {1,3,1,2,1,2,2}; // (exotic, unfamiliar)
scaleMajorPentatonic = {2,2,3,2,3}; // (country, gleeful)
scaleDiatonic = {2,2,2,2,2,2}; // (bizarre, symmetrical)
*/