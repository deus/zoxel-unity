namespace Zoxel.Audio
{
    public enum InstrumentType
    {
        Piano,
        Organ,
        EDM
    }
}