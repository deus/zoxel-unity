using System;       // for BitConverter and Byte Type
using System.IO;    // for FileStream

namespace Zoxel.Audio.Recording
{
    //! Test reecord some audio!
    public static class AudioSaveUtil
    {
        private const int headerSize = 44; //default for uncompressed wav

        public static void SaveAudioClip(string filename, in UnityEngine.AudioClip audioClip)
        {
            var fileStream = new FileStream(filename, FileMode.Create);
            AudioSaveUtil.EmptyWaveHeader(in fileStream);
            var samples = new float[audioClip.samples * audioClip.channels];
            audioClip.GetData(samples, 0);
            AppendToFilestream(in fileStream, samples);
            // Buffer.BlockCopy(receivedBytes, 0, samples, 0, receivedBytes.Length);
            WriteWaveHeader(in fileStream, audioClip.frequency, audioClip.channels);
            // fileStream.Dispose();
            fileStream.Close();
            fileStream.Dispose();
        }
        
        public static void AppendToFilestream(in FileStream fileStream, float[] dataSource)
        {
            // UnityEngine.Debug.LogError("Reading in audio: " + dataSource.Length);
            //converting in 2 steps : float[] to Int16[], //then Int16[] to Byte[]
            //bytesData array is twice the size of
            //dataSource array because a float converted in Int16 is 2 bytes
            //! Converts our incoming float array to Int16.
            // var intData = new Int16[dataSource.Length];
            int rescaleFactor = 32767; // to convert float to Int16
            var bytesData = new Byte[dataSource.Length * 2];
            var intData = new short[dataSource.Length];
            for (var i = 0; i < dataSource.Length;i++)
            {
                // intData[i] = (Int16) (dataSource[i] * rescaleFactor);
                // var intData = (Int16) (dataSource[i] * rescaleFactor);
                intData[i] = (short) (dataSource[i] * rescaleFactor);
                var byteArr = new Byte[2];
                byteArr = BitConverter.GetBytes(intData[i]);
                byteArr.CopyTo(bytesData, i * 2);
            }
            // okay this does append on
            fileStream.Write(bytesData, 0, bytesData.Length);
        }

        public static void EmptyWaveHeader(in FileStream fileStream)
        {
            byte emptyByte = 0;
            for(var i = 0; i < headerSize; i++) //preparing the header
            {
                fileStream.WriteByte(emptyByte);
            }
        }
        
        public static void WriteWaveHeader(in FileStream fileStream, int frequency, int channels)
        {
            fileStream.Seek(0, SeekOrigin.Begin);

            // Chunk ID
            var riff = System.Text.Encoding.UTF8.GetBytes("RIFF");
            fileStream.Write(riff, 0, 4);

            // ChunkSize
            var chunkSize = BitConverter.GetBytes(fileStream.Length - 8);
            fileStream.Write(chunkSize, 0, 4);

            // Format
            var wave = System.Text.Encoding.UTF8.GetBytes("WAVE");
            fileStream.Write(wave, 0, 4);

            // Subchunk1ID
            var fmt = System.Text.Encoding.UTF8.GetBytes("fmt ");
            fileStream.Write(fmt, 0, 4);

            // Subchunk1Size
            var subChunk1 = BitConverter.GetBytes(16);
            fileStream.Write(subChunk1, 0, 4);

            // AudioFormat
            var audioFormat = BitConverter.GetBytes(1);
            fileStream.Write(audioFormat, 0, 2);

            // NumChannels
            var numChannels = BitConverter.GetBytes(channels); //two);
            fileStream.Write(numChannels, 0, 2);

            // SampleRate
            var sampleRate = BitConverter.GetBytes(frequency);
            fileStream.Write(sampleRate, 0, 4);

            // ByteRate
            // var byteRate = BitConverter.GetBytes(frequency * 4);
            var byteRate = BitConverter.GetBytes(frequency * channels * 2);
            fileStream.Write(byteRate, 0, 4);

            // BlockAlign
            //var blockAlign = BitConverter.GetBytes(four);
            var blockAlign = (ushort)(channels * 2);
            fileStream.Write(BitConverter.GetBytes(blockAlign), 0, 2);

            // BitsPerSample
            ushort bps = 16;
            var bitsPerSample = BitConverter.GetBytes(bps);
            fileStream.Write(bitsPerSample, 0, 2);

            // Subchunk2ID
            var dataString = System.Text.Encoding.UTF8.GetBytes("data");
            fileStream.Write(dataString, 0, 4);

            // Subchunk2Size
            var subChunk2 = BitConverter.GetBytes(fileStream.Length - headerSize);
            fileStream.Write(subChunk2, 0, 4);
        }
    }
}
            // sampleRate * bytesPerSample*number of channels, here 44100*2*2
            // 
            // UInt16 two = 2;
            // UInt16 channels2 = (UInt16) channels;
            // UInt16 four = 4;
            // UInt16 sixteen = 16;
            // UInt16 one = 1;
