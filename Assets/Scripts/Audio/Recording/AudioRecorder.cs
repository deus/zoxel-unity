using UnityEngine;
using System.IO;    // for FileStream
using System;       // for BitConverter and Byte Type
using UnityEngine.InputSystem;

namespace Zoxel.Audio.Recording
{
    //! Test reecord some audio!
    /**
    *   How To:
    *       f10Key is Audio Recording.
    */
    public class AudioRecorder : MonoBehaviour
    {
        public static AudioRecorder instance;
        public string inputFilename = "audio";
        private int bufferSize;
        private int numBuffers;
        private int outputRate = 44100;
        private bool recOutput;
        private FileStream fileStream;
        
        void Awake()
        {
            instance = this;
            UnityEngine.AudioSettings.outputSampleRate = outputRate;
        }
        
        void Start()
        {
            UnityEngine.AudioSettings.GetDSPBufferSize(out bufferSize, out numBuffers);
        }
        
        void Update()
        {
            var keyboard = Keyboard.current;
            if(keyboard.f10Key.wasPressedThisFrame)
            {
                ToggleRecording();
            }  
        }

        public void ToggleRecording()
        {
            if(!recOutput)
            {
                StartRecording();
            }
            else
            {
                StopRecording();
            }
        }
        
        public static string GetAudioFolder()
        {
            var gameName = BuildOptions.gameName;
            return GetGameFolderPath() + "/audios/";
        }

        public static string GetGameFolderPath()
        {
            var gameName = BuildOptions.gameName;
            return GetPicturesFolder() + "/" + gameName;
        }

        public static string GetPicturesFolder()
        {
            // return System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyPictures) 
            return System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/zoxel-capture";
        }

        public static string GetDateString()
        {
            var dateTime = DateTime.Now;
            return dateTime.ToString("yyyy-MM-dd_") + dateTime.Hour + "h-" + dateTime.Minute + "m" + dateTime.Second + "s";
        }

        public void StartRecording()
        {
            var audioDirectory = GetAudioFolder();
            if (!System.IO.Directory.Exists(audioDirectory))
            {
                System.IO.Directory.CreateDirectory(audioDirectory);
            }
            StartRecording(audioDirectory + inputFilename + "-" + GetDateString() + ".wav");
        }

        public void StartRecording(string fileName)
        {
            if(!recOutput)
            {
                Debug.Log("Recording Audio.");
                StartWriting(fileName);
                recOutput = true;
            }
        }

        public void StopRecording()
        {
            if(recOutput)
            {
                Debug.Log("Finished Recording Audio.");
                recOutput = false;
                if (fileStream != null)
                {
                    AudioSaveUtil.WriteWaveHeader(in fileStream, outputRate, 2);
                    fileStream.Close();
                    fileStream.Dispose();
                    fileStream = null;
                }
            }
        }
        
        void StartWriting(string filename)
        {
            fileStream = new FileStream(filename, FileMode.Create);
            AudioSaveUtil.EmptyWaveHeader(in fileStream);
        }
        
        void OnAudioFilterRead(float[] data, int channels)
        {
            if(recOutput)
            {
                if (fileStream == null)
                {
                    StopRecording();
                    return;
                }
                AudioSaveUtil.AppendToFilestream(in fileStream, data);
                // WriteToBuffer(data); //audio data is interlaced
            }
        }
    }
}
        // public string inputFilename = "/home/deus/Desktop/audio-x.wav";