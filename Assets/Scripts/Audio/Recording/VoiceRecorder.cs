using UnityEngine;
using System.IO;    // for FileStream
using System;       // for BitConverter and Byte Type
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;

namespace Zoxel.Audio.Recording
{
    //! Test reecord some audio!
    /**
    *   How To:
    *       f7Key to Record
    *       f8Key to Play Sound
    *       f9Key to Save Sound
    */
    public class VoiceRecorder : MonoBehaviour
    {
        private const string inputFilename = "voice";
        private const string shellDirectory = "/home/deus/projects/zoxel/automation/gifs/";
        public static VoiceRecorder instance;
        [Header("Recorder")]
        public int recordTime = 5;
        public AudioClip recording;
        [Header("Settings")]
        public bool isLoopMicrophone = false;
        public bool isConvertToMp3 = true;
        public bool isDebugShell = false;
        /// public string recorderName = "AT9934USB Analogue Stereo";
        [Header("Font")]
        public int fontSize = 26;
        public UnityEngine.Color fontColor = UnityEngine.Color.green;
        public UnityEngine.Font font;
        // private parts
        [HideInInspector] public bool isRecording = false;
        private bool isRecordingOnlyVoice = false;
        private float recordStartTime;
        private int samplerate = 44100;
        private int currentRecordTime;

        void Awake()
        {
            instance = this;
            foreach (var device in Microphone.devices)
            {
                Debug.Log(" - Record Device: " + device);
            }
        }
     
        void Update()
        {
            if (isRecording)
            {
                if(Time.time - recordStartTime >= currentRecordTime)
                {
                    Microphone.End(null);
                    isRecording = false;
                    isRecordingOnlyVoice = false;
                    Debug.Log("Ended Voice Recording.");
                }
                return;
            }
            //space key triggers recording to start...
            var keyboard = Keyboard.current;
            if (keyboard.f7Key.wasPressedThisFrame)
            {
                isRecordingOnlyVoice = true;
                StartRecording(recordTime);
            }
            else if (recording && keyboard.f8Key.wasPressedThisFrame)
            {
                GetComponent<AudioSource>().PlayOneShot(recording);
            }
            else if(keyboard.f9Key.wasPressedThisFrame)
            {
                // Save clip to desktop
                if (recording)
                {
                    var audioDirectory = GetAudioFolder();
                    if (!System.IO.Directory.Exists(audioDirectory))
                    {
                        System.IO.Directory.CreateDirectory(audioDirectory);
                    }
                    var filename = inputFilename + "-" + GetDateString();
                    AudioSaveUtil.SaveAudioClip(audioDirectory + filename + ".wav", in recording);
                    if (isConvertToMp3)
                    {
                        StartCoroutine(ExecuteTerminal(audioDirectory, filename));
                    }
                }
            }
        }

        public void StartRecording(int recordTime)
        {
            currentRecordTime = recordTime;
            isRecording = true;
            recordStartTime = Time.time;
            if (recording)
            {
                Destroy(recording);
            }
            recording = Microphone.Start(null, isLoopMicrophone, recordTime, samplerate);
            Debug.Log("Started recording voice.");
            // yield return new WaitForEndOfFrame();
            /*while (!(Microphone.GetPosition(null) > 0))
            {
                yield return new WaitForEndOfFrame();
            }*/
        }

        public void SaveRecording(string filename)
        {
            if (isRecording)
            {
                UnityEngine.Debug.LogError("Cannot Save Voice Audio Clip: " + filename);
                return;
            }
            UnityEngine.Debug.Log("Saving Voice Audio Clip: " + filename);
            AudioSaveUtil.SaveAudioClip(filename, in recording);
        }

        public void OnGUI()
        {
            if (isRecording && isRecordingOnlyVoice)
            {
                SetGUISettings();
                GUILayout.Label("Recording (" + recordTime + "s) [" + (Time.time - recordStartTime) + "]");
            }
        }
        
        // idea - show latest line of terminal as toast ui
        public IEnumerator ExecuteTerminal(string foldername, string filename)
        {
            var arguments = foldername + " " + filename;
            var startInfo = new System.Diagnostics.ProcessStartInfo()
            {
                UseShellExecute = false,
                CreateNoWindow = true,
                FileName = shellDirectory + "audio-compile.sh",
                Arguments = arguments
            };
            if (isDebugShell)
            {
                startInfo.RedirectStandardError = true;
                startInfo.RedirectStandardInput = true;
                startInfo.RedirectStandardOutput = true;
            }
            var process = new System.Diagnostics.Process { StartInfo = startInfo };
            process.Start();
            while (!process.HasExited)
            {
                yield return new WaitForEndOfFrame();
            }
            if (isDebugShell)
            {
                while (!process.StandardOutput.EndOfStream)
                {
                    UnityEngine.Debug.Log(process.StandardOutput.ReadLine());
                }
            }
        }

        private void SetGUISettings()
        {
            if (font)
            {
                GUI.skin.font = font;
            }
            GUI.skin.label.fontSize = fontSize;
            GUI.color = fontColor;
        }
        
        public static string GetAudioFolder()
        {
            var gameName = BuildOptions.gameName;
            return GetGameFolderPath() + "/voices/";
        }

        public static string GetGameFolderPath()
        {
            var gameName = BuildOptions.gameName;
            return GetPicturesFolder() + "/" + gameName;
        }

        public static string GetPicturesFolder()
        {
            // return System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyPictures) 
            return System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) + "/zoxel-capture";
        }

        public static string GetDateString()
        {
            var dateTime = DateTime.Now;
            return dateTime.ToString("yyyy-MM-dd_") + dateTime.Hour + "h-" + dateTime.Minute + "m" + dateTime.Second + "s";
        }
    }
}
                    //stop recording, get length, create a new array of samples
                    /*int length = Microphone.GetPosition(null);
                 
                    Microphone.End(null);
                    float[] clipData = new float[length];
                    audioSource.clip.GetData(clipData, 0);
     
                    //create a larger vector that will have enough space to hold our temporary
                    //recording, and the last section of the current recording
                    float[] fullClip = new float[clipData.Length + tempRecording.Count];
                    for (int i = 0; i < fullClip.Length; i++)
                    {
                        //write data all recorded data to fullCLip vector
                        if (i < tempRecording.Count)
                            fullClip[i] = tempRecording[i];
                        else
                            fullClip[i] = clipData[i - tempRecording.Count];
                    }
                    recordedClips.Add(fullClip);
                    audioSource.clip = AudioClip.Create("recorded samples", fullClip.Length, 1, 44100, false);
                    audioSource.clip.SetData(fullClip, 0);
                    audioSource.loop = true;    */
                    //stop audio playback and start new recording...
                    //audioSource.Stop();
                    //tempRecording.Clear();
                    //Microphone.End(null);
                    // audioSource.clip = Microphone.Start(null, true, 1, 44100);
                    //Invoke("ResizeRecording", 1);
     
            //use number keys to switch between recorded clips, start from 1!!
            /*for (int i = 0; i < 10; i++)
            {
                if (Input.GetKeyDown("" + i))
                {
                    SwitchClips(i-1);
                }
            }*/
     
        //chooose which clip to play based on number key..
        /*void SwitchClips(int index)
        {
            if (index < recordedClips.Count)
            {
                audioSource.Stop();
                int length = recordedClips[index].Length;
                audioSource.clip = AudioClip.Create("recorded samples", length, 1, 44100, false);
                audioSource.clip.SetData(recordedClips[index], 0);
                audioSource.loop = true;
                audioSource.Play();
            }
        }*/
     
        /*void ResizeRecording()
        {
            if (isRecording)
            {
                //add the next second of recorded audio to temp vector
                int length = 44100;
                float[] clipData = new float[length];
                audioSource.clip.GetData(clipData, 0);
                tempRecording.AddRange(clipData);
                Invoke("ResizeRecording", 1);
            }
        }*/
        //temporary audio vector we write to every second while recording is enabled..
        // List<float> tempRecording = new List<float>();
     
        //list of recorded clips...
        // List<float[]> recordedClips = new List<float[]>();
            /*audioSource = gameObject.AddComponent<AudioSource>();
            //set up recording to last a max of 1 seconds and loop over and over
            audioSource.Play();*/
            //resize our temporary vector every second
            // Invoke("ResizeRecording", 1);