﻿using UnityEngine;
using Unity.Mathematics;
using UnityEngine.Experimental.Audio;
using System.Collections;
using System.Collections.Generic;

namespace Zoxel.Audio
{
    // Had to set Doppler Factor to 0 in audio settings under project settings
    public partial class AudioManager : MonoBehaviour
    {
        public static AudioManager instance;

        [Header("Scene")]
        public UnityEngine.GameObject audioListener;

        [Header("Reference")]
        public UnityEngine.Audio.AudioMixerGroup music;
        public UnityEngine.Audio.AudioMixerGroup sounds;
        public GameObject musicPrefab;
        [HideInInspector] public GameObject audioPrefab;
        
        [Header("Music")]
        public float2 beepTiming;
        public byte instrumentType;
        public float2 musicNotesRange = new float2(32, 42);
        public float2 musicDistance = new float2(-6, 6);
        public float2 musicDistanceAdd = new float2(0.04f, 0.08f);
        
        [Header("Volumes")]
        public float musicLevel = 5;
        public float2 musicVolume = new float2(0.6f, 0.8f); // per song
        public float musicVolumeAddition = 0.1f;            // per note

        [Header("Audio Settings")]
        public float defaultBlendLevel = 0.8f;
        public int sampleRate = 44100;

        [Header("Debug")]
        public bool disableBass;
        public bool forceBass;
        public bool disableMusic;
        public bool disable3DAudio;
        public bool debugLog;
        public bool showAudioInEditor;
        public int processSpeed = 12;

        void Awake()
        {
            instance = this;
            audioPrefab = new GameObject();
            audioPrefab.SetActive(false);
            if (!showAudioInEditor)
            {
                audioPrefab.hideFlags = HideFlags.HideInHierarchy;
            }
            audioPrefab.AddComponent<AudioSource>();
        }

        public void DestroyThing(Object o, float length)
        {
            Destroy(o, length);
        }
    }
}