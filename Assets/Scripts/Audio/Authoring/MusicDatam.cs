// Authoring a music fileusing UnityEngine;
using UnityEngine;

namespace Zoxel.Audio
{
    [CreateAssetMenu(fileName = "Music", menuName = "Zoxel/Music")]
    public partial class MusicDatam : ScriptableObject
    {
        public UnityEngine.AudioClip music;
    }
}