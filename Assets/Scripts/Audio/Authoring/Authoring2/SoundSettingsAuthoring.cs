using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Zoxel.Audio.Authoring
{
    //! Contains voxel settings data.
    //! \todo Seperate materials from this struct.
    // [GenerateAuthoringComponent]
    public struct SoundSettings : IComponentData
    {
        // [Header("Volumes")]
        public float soundLevel;                // 6
        public float2 uiSoundVolume;            // .3, .44
        public float2 skillSoundVolume;         // .4, .6
        public float2 itemPickupSoundVolume;    // .1, .3
        public float2 dialogueSoundVolume;      // .4, .6
        public float2 levelUpSoundVolume;       // .4, .5
        public float2 activateItemSoundVolume;  // .3, .4
        public float2 doorSoundVolume;          // .4, .6
        public int sampleRate;                  // 44100
    }

    public class SoundSettingsAuthoring : MonoBehaviour
    {
        public float soundLevel;                // 6
        public float2 uiSoundVolume;            // .3, .44
        public float2 skillSoundVolume;         // .4, .6
        public float2 itemPickupSoundVolume;    // .1, .3
        public float2 dialogueSoundVolume;      // .4, .6
        public float2 levelUpSoundVolume;       // .4, .5
        public float2 activateItemSoundVolume;  // .3, .4
        public float2 doorSoundVolume;          // .4, .6
        public int sampleRate;                  // 44100
    }

    public class SoundSettingsAuthoringBaker : Baker<SoundSettingsAuthoring>
    {
        public override void Bake(SoundSettingsAuthoring authoring)
        {
            AddComponent(new SoundSettings
            {
                soundLevel = authoring.soundLevel,
                uiSoundVolume = authoring.uiSoundVolume,
                skillSoundVolume = authoring.skillSoundVolume,
                itemPickupSoundVolume = authoring.itemPickupSoundVolume,
                dialogueSoundVolume = authoring.dialogueSoundVolume,
                levelUpSoundVolume = authoring.levelUpSoundVolume,
                activateItemSoundVolume = authoring.activateItemSoundVolume,
                sampleRate = authoring.sampleRate
            });       
        }
    }
}