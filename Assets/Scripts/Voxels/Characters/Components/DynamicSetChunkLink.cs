using Unity.Entities;

namespace Zoxel.Voxels
{
    //! Used (only on Player atm) for dynamically setting chunk position.
    public struct DynamicChunkLink : IComponentData
    {
        //! This is incase position changes of npcs - moving outside chunk - need to set it to a new one
        public int3 chunkPosition;

        public DynamicChunkLink(int3 chunkPosition)
        {
            this.chunkPosition = chunkPosition;
        }
    }
}