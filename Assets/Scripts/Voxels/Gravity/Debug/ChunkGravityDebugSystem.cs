//! Renders a line for each gravity direction in the chunk the player is in.
using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Lines;

namespace Zoxel.Voxels
{
    //! Debugs ChunkGravity component.
    /**
    *   \todo Convert to Parallel.
    *   \todo Attach lines permanently instead of spawning constantly.
    */
	[UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ChunkGravityDebugSystem : SystemBase
    {
        private const float bufferRatio = 0.05f; // 0.15f
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (VoxelManager.instance == null) return;
            if (!VoxelManager.instance.voxelSettings.isDebugGravity)
            {
                return;
            }
            var debugGravityViewDistance = VoxelManager.instance.voxelSettings.debugGravityViewDistance;
            var debugGravityDepth = VoxelManager.instance.voxelSettings.debugGravityDepth;
            var debugGravityShowNone = VoxelManager.instance.voxelSettings.debugGravityShowNone;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithAll<PlanetChunk>()
                .ForEach((in ChunkGravity chunkGravity, in ChunkDivision chunkDivision, in ChunkPosition chunkPosition, in VoxLink voxLink) =>
            {
                if (chunkDivision.viewDistance <= debugGravityViewDistance)
                {
                    var voxelDimensions = EntityManager.GetComponentData<ChunkDimensions>(voxLink.vox).voxelDimensions;
                    var position = chunkPosition.GetVoxelPosition(voxelDimensions).ToFloat3(); // + buffer;
                    DrawOctreeLines(in chunkGravity.gravity, position, voxelDimensions.ToFloat3(), debugGravityShowNone, debugGravityDepth);
                }
                //DebugLines.DrawCubeLines(position, quaternion.identity, 0.02f + signal.value * 0.06f, UnityEngine.Color.white);
            }).WithoutBurst().Run();
        }

        private void DrawOctreeLines(in OctNode<byte> octNode, float3 position, float3 dimensions, bool debugGravityShowNone, byte maxDepth, byte depth = 0)
        {
            if (octNode.IsOpen)
            {
                depth++;
                if (depth >= maxDepth)
                {
                    DrawGravityLines(octNode.value, position, dimensions, debugGravityShowNone);
                    return;
                }
                // move to bottom ones
                for (byte i = 0; i < octNode.Length; i++)
                {
                    var positionOffset = OctNodeHelper.positions[i] / 2f;
                    positionOffset.x *= dimensions.x;
                    positionOffset.y *= dimensions.y;
                    positionOffset.z *= dimensions.z;
                    var childNode = octNode[i];
                    DrawOctreeLines(in childNode, position + positionOffset, dimensions / 2f, debugGravityShowNone, maxDepth, depth);
                }
            }
            else
            {
                DrawGravityLines(octNode.value, position, dimensions, debugGravityShowNone);
            }
        }

        private void DrawGravityLines(byte value, float3 position, float3 dimensions, bool debugGravityShowNone)
        {
            if (!debugGravityShowNone && value == PlanetSide.None)
            {
                return;
            }
            var buffer = dimensions * bufferRatio; // new float3(1f, 1f, 1f);
            position += buffer;
            var positionA = position;
            var positionB = position + new float3(dimensions.x - buffer.x * 2f, 0, 0);
            var positionC = position + new float3(dimensions.x - buffer.x * 2f, 0, dimensions.z - buffer.z * 2f);
            var positionD = position + new float3(0, 0, dimensions.z - buffer.z * 2f);
            var color1 = new UnityEngine.Color(0.8f, 0, 0);
            var arrowColor = new UnityEngine.Color(0.2f, 0.8f, 0.2f);
            if (value == PlanetSide.Up || value == PlanetSide.Down)
            {
                var lineAddition = new float3(0, dimensions.y - buffer.y * 2f, 0);
                var arrowBottom = position - buffer + (new float3(dimensions.x, 0, dimensions.z) / 2f) + new float3(0, buffer.y, 0);
                var arrowTop = arrowBottom + lineAddition + new float3(0,  - buffer.y * 2f, 0);
                var bufferAddition = new float3(0, buffer.y, 0);
                if (value == PlanetSide.Down)
                {
                    color1 = new UnityEngine.Color(0.8f, 0.8f, 0.8f);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowTop, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowTop, arrowTop + new float3(-buffer.x, 0, 0) - bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowTop, arrowTop + new float3(buffer.x, 0, 0) - bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowTop, arrowTop + new float3(0, 0, -buffer.z) - bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowTop, arrowTop + new float3(0, 0, buffer.z) - bufferAddition, arrowColor);
                }
                else if (value == PlanetSide.Up)
                {
                    color1 = new UnityEngine.Color(0, 0.8f, 0.8f);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowTop, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowBottom + new float3(-buffer.x, 0, 0) + bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowBottom + new float3(buffer.x, 0, 0) + bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowBottom + new float3(0, 0, -buffer.z) + bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowBottom + new float3(0, 0, buffer.z) + bufferAddition, arrowColor);
                }
            }
            else if (value == PlanetSide.Backward || value == PlanetSide.Forward)
            {
                var lineAddition = new float3(0, 0, dimensions.z - buffer.z * 2f);
                var arrowBottom = position - buffer + (new float3(dimensions.x, dimensions.y, 0) / 2f) + new float3(0, 0, buffer.z);
                var arrowTop = arrowBottom + lineAddition - new float3(0, 0, buffer.z * 2f);
                var bufferAddition = new float3(0, 0, buffer.z);
                if (value == PlanetSide.Backward)
                {
                    color1 = new UnityEngine.Color(0, 0.8f, 0);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowTop, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowTop, arrowTop + new float3(-buffer.x, 0, 0) - bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowTop, arrowTop + new float3(buffer.x, 0, 0) - bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowTop, arrowTop + new float3(0, -buffer.y, 0) - bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowTop, arrowTop +  new float3(0, buffer.y, 0) - bufferAddition, arrowColor);
                }
                else if (value == PlanetSide.Forward)
                {
                    color1 = new UnityEngine.Color(0, 0, 0.8f);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowTop, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowBottom + new float3(-buffer.x, 0, 0) + bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowBottom + new float3(buffer.x, 0, 0) + bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowBottom + new float3(0, -buffer.y, 0) + bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowBottom + new float3(0, buffer.y, 0) + bufferAddition, arrowColor);
                }
            }
            else if (value == PlanetSide.Left || value == PlanetSide.Right)
            {
                var lineAddition = new float3(dimensions.x - buffer.x * 2f, 0, 0);
                var arrowBottom = position - buffer + (new float3(0, dimensions.y, dimensions.z) / 2f) + new float3(buffer.x, 0, 0);
                var arrowTop = arrowBottom + lineAddition - new float3(buffer.x * 2f, 0, 0);
                var bufferAddition = new float3(buffer.x, 0, 0);
                if (value == PlanetSide.Left)
                {
                    color1 = new UnityEngine.Color(0, 0.8f, 0.8f);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowTop, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowTop, arrowTop + new float3(0, -buffer.y, 0) - bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowTop, arrowTop + new float3(0, buffer.y, 0) - bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowTop, arrowTop + new float3(0, 0, -buffer.z) - bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowTop, arrowTop +  new float3(0, 0, buffer.z) - bufferAddition, arrowColor);
                }
                else if (value == PlanetSide.Right)
                {
                    color1 = new UnityEngine.Color(0.8f, 0.8f, 0);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowTop, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowBottom + new float3(0, -buffer.y, 0) + bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowBottom + new float3(0, buffer.y, 0) + bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowBottom + new float3(0, 0, -buffer.z) + bufferAddition, arrowColor);
                    UnityEngine.Debug.DrawLine(arrowBottom, arrowBottom + new float3(0, 0, buffer.z) + bufferAddition, arrowColor);
                }
            }
            var color2 = color1 * 0.6f; // new UnityEngine.Color(0.6f, 0, 0);
            var color3 = color1 * 0.5f; // new UnityEngine.Color(0.4f, 0, 0);
            color1 *= 0.8f;
            var lineAddition2 = new float3(0, dimensions.y - buffer.y * 2f, 0);
            UnityEngine.Debug.DrawLine(positionA, positionA + lineAddition2, color1);
            UnityEngine.Debug.DrawLine(positionB, positionB + lineAddition2, color1);
            UnityEngine.Debug.DrawLine(positionC, positionC + lineAddition2, color1);
            UnityEngine.Debug.DrawLine(positionD, positionD + lineAddition2, color1);
            // top lines
            UnityEngine.Debug.DrawLine(positionA + lineAddition2, positionB + lineAddition2, color2);
            UnityEngine.Debug.DrawLine(positionC + lineAddition2, positionD + lineAddition2, color2);
            UnityEngine.Debug.DrawLine(positionA + lineAddition2, positionD + lineAddition2, color2);
            UnityEngine.Debug.DrawLine(positionB + lineAddition2, positionC + lineAddition2, color2);
            // bottom lines
            UnityEngine.Debug.DrawLine(positionA, positionB, color3);
            UnityEngine.Debug.DrawLine(positionC, positionD, color3);
            UnityEngine.Debug.DrawLine(positionA, positionD, color3);
            UnityEngine.Debug.DrawLine(positionB, positionC, color3);
        }
    }
}
        /*UnityEngine.Color lineColor = UnityEngine.Color.green;
        UnityEngine.Color lineColorEdge = new UnityEngine.Color(0.66f, 0.1f, 0.1f);
        UnityEngine.Color lineColorCorner = new UnityEngine.Color(0f, 0f, 0f);
        UnityEngine.Color lineColorMiddle = new UnityEngine.Color(0.8f, 0.8f, 0.8f);*/