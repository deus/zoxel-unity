using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
    //! Disposes ChunkGravity data on DestroyEntity.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ChunkGravityDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, ChunkGravity>()
                .ForEach((in ChunkGravity chunkGravity) =>
			{
                chunkGravity.DisposeFinal();
			}).ScheduleParallel();
        }
    }
}