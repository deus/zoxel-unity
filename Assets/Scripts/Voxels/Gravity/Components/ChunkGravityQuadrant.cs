using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Voxels
{
    //! Gravity data for each Chunk.
    public struct ChunkGravityQuadrant : IComponentData
    {
        public bool3 quadrantA;
        public bool3 quadrantB;
    }
}

        /*public quaternion GetRotation()
        {
            var hasHeightsDown = quadrantA.GetBoolA();
            // var hasHeightsUp = quadrantB.GetBoolA();
            var hasHeightsLeft = quadrantA.GetBoolB();
            var hasHeightsRight = quadrantB.GetBoolB();
            var hasHeightsBackward = quadrantA.GetBoolC();
            var hasHeightsForward = quadrantB.GetBoolC();
            var degreesToRadians = ((math.PI * 2) / 360f);
            var targetRotation = quaternion.identity;
            if (hasHeightsDown)
            {
                targetRotation = quaternion.EulerXYZ(new float3(180, 0, 0) * degreesToRadians);
            }
            else if (hasHeightsLeft)
            {
                targetRotation = quaternion.EulerXYZ(new float3(0, 0, 90) * degreesToRadians);
            }
            else if (hasHeightsRight)
            {
                targetRotation = quaternion.EulerXYZ(new float3(0, 0, -90) * degreesToRadians);
            }
            else if (hasHeightsBackward)
            {
                targetRotation = quaternion.EulerXYZ(new float3(-90, 0, 0) * degreesToRadians);
            }
            else if (hasHeightsForward)
            {
                targetRotation = quaternion.EulerXYZ(new float3(90, 0, 0) * degreesToRadians);
            }
            return targetRotation;
        }*/