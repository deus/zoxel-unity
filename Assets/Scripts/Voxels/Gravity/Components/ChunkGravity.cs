﻿using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels
{
    //! Holds voxel data in a single array.
	public struct ChunkGravity : IComponentData
	{
		public byte depth;
		public OctNode<byte> gravity;

		public ChunkGravity(byte value)
		{
			this.depth = 0;
			this.gravity = new OctNode<byte>(value, Allocator.Persistent);
		}

		public void Initialize(byte value)
		{
			this.depth = 0;
			gravity = new OctNode<byte>(value, Allocator.Persistent, 0);
		}
		public void Initialize(byte value, byte depth)
		{
			this.depth = depth;
			gravity = new OctNode<byte>(value, Allocator.Persistent, depth);
		}

		public void Dispose()
		{
			this.gravity.Dispose();
		}

		public void DisposeFinal()
		{
			this.gravity.DisposeFinal();
		}
	}
}

// UnityEngine.Debug.LogError("Depth: " + depth);
// var random = new Unity.Mathematics.Random();
// random.InitState((uint) seed);
// var depth = (byte) random.NextInt(0, 7);
// RandomizeGravity(ref random, ref gravity);
/*for (byte i = 0; i < this.gravity.Length; i++)
{
	var octNode = gravity[i];
	octNode.value = (byte) random.NextInt(7);
	gravity[i] = octNode;
}*/
/*private void RandomizeGravity(ref Unity.Mathematics.Random random, ref OctNode<byte> parentNode)
{
	parentNode.value = (byte) random.NextInt(7);
	if (parentNode.IsOpen)
	{
		for (byte i = 0; i < parentNode.Length; i++)
		{
			var octNode = parentNode[i];
			RandomizeGravity(ref random, ref octNode);
			parentNode[i] = octNode;
		}
	}
}*/