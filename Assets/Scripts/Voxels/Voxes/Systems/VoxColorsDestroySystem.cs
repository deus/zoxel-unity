using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
	//! Disposes of VoxColors.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class VoxColorsDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, VoxColors>()
                .ForEach((in VoxColors voxColors) =>
			{
                voxColors.DisposeFinal();
			}).ScheduleParallel();
		}
	}
}