using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;

namespace Zoxel.Voxels 
{
    //! Sets the size of a Vox Entity's voxels data.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ModelResizeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // resize vox
            Dependency = Entities
                .WithNone<InitializeEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref ChunkDimensions chunkDimensions, in Vox vox, in SetModelSize setModelSize) =>
            {
                PostUpdateCommands.RemoveComponent<SetModelSize>(entityInQueryIndex, e);
                chunkDimensions.voxelDimensions = setModelSize.size;
                // UnityEngine.Debug.LogError("setModelSize from BodyBuilder: " + setModelSize.size);
                if (vox.chunks.Length == 0)
                {
                    PostUpdateCommands.AddComponent<SpawnVoxChunk>(entityInQueryIndex, e);
                }
                else
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, vox.chunks[0], setModelSize);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // resize chunk weights
            Dependency = Entities
                .WithNone<InitializeEntity>()
                .ForEach((ref ChunkWeights chunkWeights, in SetModelSize setModelSize) =>
            {
                chunkWeights.Initialize(setModelSize.size);
            }).ScheduleParallel(Dependency);
            // resize chunk and chunk renders
            Dependency = Entities
                .WithNone<InitializeEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref Chunk chunk, in ChunkRenderLinks chunkRenderLinks, in SetModelSize setModelSize) =>
            {
                if (chunk.ReInitializeChunk(setModelSize.size))
                {
                    // resize chunk render if they have them
                    var resizeChunkRender = new ResizeChunkRender(in chunk, setModelSize.size);
                    for (int i = 0; i < chunkRenderLinks.chunkRenders.Length; i++)
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, chunkRenderLinks.chunkRenders[i], resizeChunkRender);
                    }
                }
                PostUpdateCommands.RemoveComponent<SetModelSize>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}