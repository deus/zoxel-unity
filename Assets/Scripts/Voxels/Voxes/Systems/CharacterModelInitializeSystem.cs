using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.Voxels
{
    //! Initializes character materials. Starts on transparent.
    /**
    *   - Initialize System -
    */
    [UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class CharacterModelInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null) return;
            var characterMaterial = MaterialsManager.instance.materials.characterMaterial;
            var characterTransparentMaterial = MaterialsManager.instance.materials.characterTransparentMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<Skeleton>()
                .WithAll<InitializeEntity, Character, EntityMaterials>()
                .ForEach((Entity e) =>
            {
                if (HasComponent<MaterialFader>(e))
                {
                    PostUpdateCommands.SetSharedComponentManaged(e, new EntityMaterials(characterTransparentMaterial));
                }
                else
                {
                    PostUpdateCommands.SetSharedComponentManaged(e, new EntityMaterials(characterMaterial));
                }
            }).WithoutBurst().Run();
            var characterAnimatedTransparentMaterial = MaterialsManager.instance.materials.characterAnimatedTransparentMaterial;
            var characterAnimatedMaterial = MaterialsManager.instance.materials.characterAnimatedMaterial;
            Entities
                .WithAll<EntityMaterials>()
                .WithAll<InitializeEntity, Character, Skeleton>()
                .ForEach((Entity e, EntityMaterials entityMaterials) =>
            {
                if (HasComponent<MaterialFader>(e))
                {
                    PostUpdateCommands.SetSharedComponentManaged(e, new EntityMaterials(characterAnimatedTransparentMaterial));
                }
                else
                {
                    PostUpdateCommands.SetSharedComponentManaged(e, new EntityMaterials(characterAnimatedMaterial));
                }
            }).WithoutBurst().Run();
        }
    }
}