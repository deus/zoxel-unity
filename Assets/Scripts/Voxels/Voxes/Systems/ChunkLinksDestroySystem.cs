using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
    //! Cleans up ChunkLinks's chunks.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ChunkLinksDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
				.WithAll<DestroyEntity, ChunkLinks>()
				.ForEach((in ChunkLinks chunkLinks) =>
			{
				chunkLinks.Dispose();
			}).ScheduleParallel();
		}
	}
}