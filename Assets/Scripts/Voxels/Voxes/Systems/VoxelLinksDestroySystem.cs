using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
	//! Cleans up voxels connected to a Realm. (and planet?)
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class VoxelLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Entities
				.WithNone<Planet>()
				.WithAll<DestroyEntity, VoxelLinks>()
				.ForEach((Entity e, int entityInQueryIndex, in VoxelLinks voxelLinks) =>
			{
				for (int i = 0; i < voxelLinks.voxels.Length; i++)
				{
					PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, voxelLinks.voxels[i]);
				}
			}).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
			Entities
				.WithAll<DestroyEntity, VoxelLinks>()
				.ForEach((in VoxelLinks voxelLinks) =>
			{
				voxelLinks.Dispose();
			}).ScheduleParallel();
		}
	}
}