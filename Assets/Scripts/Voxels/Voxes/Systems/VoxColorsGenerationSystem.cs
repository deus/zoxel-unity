using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Voxels
{
    //! Generates VoxColors for Vox.
	[BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxColorsGenerationSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery worldsQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            worldsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<ZoxID>());
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<GenerateVoxColors>(processQuery);
            var worldEntities = worldsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var worldIDs = GetComponentLookup<ZoxID>(true);
            worldEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<DestroyEntity>()
				.WithAll<GenerateVoxColors>()
				.ForEach((Entity e, int entityInQueryIndex, ref VoxColors voxColors, in VoxLink voxLink, in Seed seed) =>
			{
                voxColors.GenerateRandomColors((uint) worldIDs[voxLink.vox].id, (uint) seed.seed);
            })  .WithReadOnly(worldIDs)
                .ScheduleParallel(Dependency);
		}

        public static void GenerateRandomColors(ref VoxColors voxColors, byte spawnType, uint worldSeed, uint uniqueSeed)
        {
            voxColors.Dispose();
            voxColors.colors = new BlitableArray<float3>(16, Allocator.Persistent);
            var random = new Random();
            random.InitState(worldSeed);
            var randomer = new Random();
            var hsvMin = 180;
            var hsvMax = hsvMin + 60;
            var mult = 256;
            if (spawnType == 0)
            {
                mult = 128;
                hsvMin = 220;
                hsvMax = 260;
            }
            else if (spawnType == 1)
            {
                mult = 512;
                hsvMin = 0;
                hsvMax = 60;
            }
            else if (spawnType == 2)
            {
                mult = 1024;
                hsvMin = 300;
                hsvMax = 360;
            }
            else if (spawnType == 3)
            {
                mult = 1;
                hsvMin = 0;
                hsvMax = 360;
            }
            randomer.InitState((uint) (uniqueSeed * mult));
            var skinHSV = new float3(randomer.NextInt(hsvMin, hsvMax), randomer.NextInt(40, 60), randomer.NextInt(30, 60));
            voxColors.colors[0] = Color.GetColorFromHSV(skinHSV).ToFloat3();
            for (int i = 0; i < voxColors.colors.Length; i++)
            {
                voxColors.colors[i] = voxColors.colors[0];
            }
            voxColors.colors[1] = Color.GetColorFromHSV(new float3(randomer.NextInt(hsvMin, hsvMax), randomer.NextInt(40, 60), randomer.NextInt(20, 45))).ToFloat3();
            // contrasts from body
            var eyesHSV = new float3((skinHSV.x + randomer.NextInt(180) - 90) % 360, 22 + randomer.NextInt(22), 40 + randomer.NextInt(20));
            var eyeColor = Color.GetColorFromHSV(eyesHSV);
            //voxColors.colors[4] = new float3(0, 0, 0);
            voxColors.colors[3] = eyeColor.ToFloat3();
            // voxColors.colors[3] = new float3(1, 1, 1);
            voxColors.colors[4] = eyeColor.Saturate(3f).MultiplyBrightness(1.5f).ToFloat3();
        }
	}
}