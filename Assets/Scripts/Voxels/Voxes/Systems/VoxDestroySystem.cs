using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
    //! Cleans up Vox's chunks.
    /**
    *   - Destroy System -
	*	\todo Destroy this in batch, for Realm Ending.
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class VoxDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithAll<DestroyEntity, Vox>()
				.ForEach((int entityInQueryIndex, in Vox vox) =>
			{
				for (int i = 0; i < vox.chunks.Length; i++)
				{
					var chunkEntity = vox.chunks[i];
					if (HasComponent<Chunk>(chunkEntity))
					{
						PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, chunkEntity);
					}
				}
				vox.DisposeFinal();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}