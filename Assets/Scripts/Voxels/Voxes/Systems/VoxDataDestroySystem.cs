using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
    //! Disposes of any VoxDataEditor without the DontDestroyVoxData component using the DestroyEntity event.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class VoxDataDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
            Entities
                .WithNone<DontDestroyVoxData>()
                .WithAll<DestroyEntity, VoxDataEditor>()
                .ForEach((in VoxDataEditor voxDataEditor) =>
            {
                voxDataEditor.Dispose();
			}).ScheduleParallel();
		}
	}
}