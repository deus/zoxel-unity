namespace Zoxel.Voxels
{
    public enum VoxelDirection : byte
    {
        None,
        Forward,
        Back,
        Left,
        Right,
        Up,
        Down
    }
}