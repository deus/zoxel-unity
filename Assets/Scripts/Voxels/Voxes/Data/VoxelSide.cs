using Unity.Mathematics;

namespace Zoxel.Voxels
{
	//! Voxel Side Type
	public static class VoxelSide
	{
		public const byte Left = 0;
		public const byte Right = 1;
		public const byte Down = 2;	
		public const byte Up = 3;
		public const byte Backward = 4;
		public const byte Forward = 5;
	}
}
