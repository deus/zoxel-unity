namespace Zoxel.Voxels
{
    public enum TextureVoxelSide : byte
    {
        Default,
        Down,
        Up,
        Forward,
        Back,
        Left,
        Right,
        UpDown,
        Sides
    }
}