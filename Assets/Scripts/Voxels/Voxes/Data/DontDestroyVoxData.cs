using Unity.Entities;

namespace Zoxel.Voxels
{
    public struct DontDestroyVoxData : IComponentData { }
}