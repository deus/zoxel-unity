using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Voxels // .Minivoxes
{
	//! Dimensions for a BigMinivoxVoxel.
	public struct BlockDimensions : IComponentData
	{
		//! The size in block units of the voxel.
		public int3 size;

		public BlockDimensions(int3 size)
		{
			this.size = size;
		}

		public float3 GetPositionOffset(byte blockRotation)
		{
			var offset = float3.zero;
			var positions = GetPositionOffsets(blockRotation);
			for (int i = 0; i < positions.Length; i++)
			{
				// UnityEngine.Debug.LogError("Position " + i + ": " + positions[i]);
				offset += positions[i].ToFloat3();
			}
			offset /= (float) positions.Length;
			positions.Dispose();
			return offset;
		}

		public NativeList<int3> GetPositionOffsets(byte blockRotation)
		{
			// This isn't working for bed, wip, this was made for door rotations
			var positions = new NativeList<int3>();
			int3 position;
			for (position.x = 0; position.x < size.x; position.x++)
			{
				for (position.y = 0; position.y < size.y; position.y++)
				{
					for (position.z = 0; position.z < size.z; position.z++)
					{
						var position2 = position;
						position2.x = -position2.x;
						position2.z = -position2.z;
						positions.Add(position2);
					}
				}
			}
			var rotation = BlockRotation.GetRotation(blockRotation);
			for (int i = 0; i < positions.Length; i++)
			{
				var position2 = positions[i];
				position2 = int3.Round(math.mul(rotation, position2.ToFloat3()));
				//UnityEngine.Debug.LogError("PreRotated Position " + i + ": " + positions[i]
				//	+ " post: " + (math.mul(rotation, positions[i].ToFloat3())) + " -- " + position2);
				positions[i] = position2;
			}
			return positions;
		}
	}
}
			// blockRotation = BlockRotation.GetRawAxis(blockRotation);
						/*var position = position;
						if (blockRotation == BlockRotation.Left)
						{
							position.x = -position.y;
							position.y = position.x;
						}
						else if (blockRotation == BlockRotation.Right)
						{
							position.x = position.y;
							position.y = position.x;
						}
						else if (blockRotation == BlockRotation.Down)
						{
							position.y = -position.y;
						}
						else if (blockRotation == BlockRotation.Backward)
						{
							position.z = -position.y;
							position.y = position.z;
						}
						else if (blockRotation == BlockRotation.Right)
						{
							position.z = position.y;
							position.y = position.z;
						}
						positions.Add(position);*/