using Unity.Entities;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Zoxel.Voxels
{
    //! Holds Vox links to Chunk entities.
    public struct ChunkLinks : IComponentData
    {
        public UnsafeParallelHashMap<int3, Entity> chunks;

        public ChunkLinks(int count)
        {
            this.chunks = new UnsafeParallelHashMap<int3, Entity>(count, Allocator.Persistent);
        }
        
        public void Dispose()
        {
            chunks.Dispose();
        }
    }
}