using Unity.Entities;

namespace Zoxel.Voxels
{
	//! A global voxel position in relation to the world.
	public struct VoxelPosition : IComponentData
	{
		public int3 position;

		public VoxelPosition(int3 position)
		{
			this.position = position;
		}
	}
}