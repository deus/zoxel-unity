using Unity.Entities;

namespace Zoxel.Voxels
{
    //! Spawns a single Chunk on a Vox, used for models.
    public struct SpawnVoxChunk : IComponentData { }
}