using Unity.Entities;

namespace Zoxel.Voxels
{
    //! Removes the Chunk's of a Vox.
    public struct RemoveVoxChunks : IComponentData { }
}