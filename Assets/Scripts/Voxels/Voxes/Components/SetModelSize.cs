using Unity.Entities;

namespace Zoxel.Voxels 
{
    public struct SetModelSize : IComponentData
    {
        public int3 size;

        public SetModelSize(int3 size)
        {
            this.size = size;
        }
    }
}