using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels // .Minivoxes
{
	//! A BigMinivox that takes up multiple voxel spaces.
	public struct MultiChunkLink : IComponentData
	{
		//! Positions of a voxel (global).
		public BlitableArray<Entity> chunks;

        public MultiChunkLink(in NativeList<Entity> chunks)
        {
            this.chunks = new BlitableArray<Entity>(chunks.Length, Allocator.Persistent);
            for (int i = 0; i < chunks.Length; i++)
            {
                this.chunks[i] = chunks[i];
            }
        }

		public void DisposeFinal()
		{
			chunks.DisposeFinal();
		}

		public void Dispose()
		{
			chunks.Dispose();
		}
	}
}