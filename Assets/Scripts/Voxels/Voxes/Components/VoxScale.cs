using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Voxels
{
    //! A scale value for a vox model.
    public struct VoxScale : IComponentData
    {
        public float3 scale;

        public VoxScale(float3 scale)
        {
            this.scale = scale;
        }
    }
}