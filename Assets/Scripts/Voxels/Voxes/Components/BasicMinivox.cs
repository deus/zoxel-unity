using Unity.Entities;

namespace Zoxel.Voxels // .Minivoxes
{
	//! A tag for the most basic minivox.
	public struct BasicMinivoxVoxel : IComponentData { }
	//! A tag for the most basic minivox instanceed.
	public struct BasicMinivox : IComponentData { }
}