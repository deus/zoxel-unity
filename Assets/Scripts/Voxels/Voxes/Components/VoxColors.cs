
using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Voxels
{
    //! A list of colours, used for a Vox model.
    public struct VoxColors : IComponentData
    {
        public BlitableArray<float3> colors;

        public VoxColors(BlitableArray<float3> colors)
        {
            this.colors = colors;
        }

        public VoxColors(in BlitableArray<ColorRGB> colors)
        {
            this.colors = new BlitableArray<float3>(colors.Length, Allocator.Persistent);
            for (int j = 0; j < colors.Length; j++)
            {
                this.colors[j] = colors[j].ToFloat3();
            }
        }

        public void DisposeFinal()
        {
            colors.DisposeFinal();
        }

        public void Dispose()
        {
            colors.Dispose();
        }

        public void Inititalize(int length)
        {
            Dispose();
            colors = new BlitableArray<float3>(length, Allocator.Persistent);
        }

        public void SetAsColor(Color color)
        {
            Dispose();
            colors = new BlitableArray<float3>(1, Allocator.Persistent);
            colors[0] = color.ToFloat3();
        }

        public void GenerateRandomColors(uint worldSeed, uint uniqueSeed)
        {
            var random = new Random();
            random.InitState(uniqueSeed);
            const int maxColorChecksCount = 16 * 1024;
            var failCount = 0;
            var baseColor = new int3(0, 0, 0);
            var variance = new int3(255, 255, 255);
            var grassColor = new Color();
            while (true)
            {
                var red = (byte)(baseColor.x + (int)(variance.x * random.NextFloat(1)));
                var green = (byte)(baseColor.y + (int)(variance.y * random.NextFloat(1)));
                var blue = (byte)(baseColor.z + (int)(variance.z * random.NextFloat(1)));
                var newColor = new Color(red, green, blue);
                var HSV = newColor.GetHSV();
                if (HSV.x >= 120 && HSV.x <= 240 && HSV.y >= 20 && HSV.y <= 40 && HSV.z >= 10 && HSV.z <= 30) // value greater then 10 and less then 60 && if saturation >= 30 && saturation <= 60
                {
                    grassColor = newColor;
                    break;
                }
                else
                {
                    failCount++;
                    grassColor = newColor;
                    if (failCount >= maxColorChecksCount)
                    {
                        // UnityEngine.Debug.LogError("Failed to get Soil Color: " + HSV);
                        break;
                    }
                }
            }

            var secondaryColor = grassColor.GetColor();
            const float variance2 = 0.15f;
            Dispose();
            colors = new BlitableArray<float3>(16, Allocator.Persistent);
            for (int i = 0; i < colors.Length; i++)
            {
                var thisColor = new float3(
                    secondaryColor.r + random.NextFloat(-variance2, variance2),
                    secondaryColor.g + random.NextFloat(-variance2, variance2),
                    secondaryColor.b + random.NextFloat(-variance2, variance2)
                );
                thisColor.x = math.clamp(thisColor.x, 0, 1);
                thisColor.y = math.clamp(thisColor.y, 0, 1);
                thisColor.z = math.clamp(thisColor.z, 0, 1);
                colors[i] = thisColor;
            }
        }
    }
}