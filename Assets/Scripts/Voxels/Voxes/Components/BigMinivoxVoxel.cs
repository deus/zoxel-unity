using Unity.Entities;

namespace Zoxel.Voxels // .Minivoxes
{
	//! A Voxel Minivox that takes up more then one block size.
	public struct BigMinivoxVoxel : IComponentData { }
}