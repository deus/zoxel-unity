using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels// .Minivoxes
{
	//! A BigMinivox that takes up multiple voxel spaces.
	public struct MultiVoxelPosition : IComponentData
	{
		//! Positions of a voxel (global).
		public BlitableArray<int3> positions;

        public MultiVoxelPosition(in NativeList<int3> positions)
        {
            this.positions = new BlitableArray<int3>(positions.Length, Allocator.Persistent);
            for (int i = 0; i < positions.Length; i++)
            {
                this.positions[i] = positions[i];
            }
        }

		public void DisposeFinal()
		{
			positions.DisposeFinal();
		}

		public void Dispose()
		{
			positions.Dispose();
		}
	}
}