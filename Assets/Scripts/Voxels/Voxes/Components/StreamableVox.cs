﻿using Unity.Entities;

namespace Zoxel.Voxels
{
    //! Data for streamable voxes.
    public struct StreamableVox : IComponentData
    {
        public int3 position;
        public byte quadrant;
        public byte renderDistance;

        public StreamableVox(int3 position, byte quadrant, byte renderDistance)
        {
            this.position = position;
            this.quadrant = quadrant;
            this.renderDistance = renderDistance;
        }
    }
}