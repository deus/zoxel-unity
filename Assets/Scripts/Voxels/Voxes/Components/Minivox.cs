using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Voxels // .Minivoxes
{
	//! A tag for a Minivox Entity.
	/**
	*	\todo Use Transform data instead of this.
	*/
	public struct Minivox : IComponentData
	{
		public float3 position;
		public quaternion rotation;

		public Minivox(float3 position, quaternion rotation)
		{
			this.position = position;
			this.rotation = rotation;
		}
	}
}