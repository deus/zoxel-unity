﻿using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Voxels
{
    //! A tag for a Vox model entity.
    /**
    *   \todo Move array to ChunkLinks.
    */
    public struct Vox : IComponentData
    {
        public BlitableArray<Entity> chunks;

        public void DisposeFinal()
        {
            chunks.DisposeFinal();
        }
        
        public void Dispose()
        {
            chunks.Dispose();
        }

        public void SetAs(int newCount)
        {
			if (chunks.Length != newCount)
			{
                var newChunks = new BlitableArray<Entity>(newCount, Allocator.Persistent);
                var minCount = math.min(newCount, chunks.Length);
				for (int i = 0; i < minCount; i++)
				{
					newChunks[i] = chunks[i];
				}
				for (int i = chunks.Length; i < newCount; i++)
				{
					newChunks[i] = new Entity();
				}
				Dispose();
				chunks = newChunks;
			}
        }
    }
}