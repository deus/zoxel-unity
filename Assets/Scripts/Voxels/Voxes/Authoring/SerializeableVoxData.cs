using Unity.Mathematics;
using System;

namespace Zoxel.Voxels
{
    //! Used to save VoxDatas.
    [Serializable]
    public partial class SerializeableVoxData
    {
        public int id;
        public float3 scale;
        public int3 size;   // size will be 1,1,1 for single chunks
        public byte[] data;
        public ColorRGB[] colors;

        public SerializeableVoxData()
        {
            data = new byte[0];
            colors = new ColorRGB[0];
        }
        
        public VoxDataEditor GetRealOne()
        {
            var voxData = new VoxDataEditor();
            voxData.id = id;
            //voxData.scale = scale;
            voxData.size = size;
            voxData.InitializeColors(colors.Length);
            if (voxData.colors.Length != colors.Length)
            {
                return voxData;
            }
            for (int i = 0; i < colors.Length; i++)
            {
                voxData.colors[i] = colors[i];
            }
            voxData.InitializeVoxData(); // data.Length
            if (voxData.data.Length != data.Length)
            {
                return voxData;
            }
            for (int i = 0; i < data.Length; i++)
            {
                voxData.data[i] = data[i];
            }
            return voxData;
        }
    }
}