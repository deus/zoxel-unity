﻿using Unity.Mathematics;
using System;
using UnityEngine;

namespace Zoxel.Voxels
{
    //! Used for Mr Penguin and Pals!
    [CreateAssetMenu(fileName = "Vox", menuName = "ZoxelArt/Vox")]
    public partial class VoxDatam : ScriptableObject, ISerializationCallbackReceiver
    {
        public VoxDataEditor data;
        public int3 offset;
        [HideInInspector] public SerializeableVoxData clone;

        [ContextMenu("GenerateID")]
        public void GenerateID()
        {
            data.id = IDUtil.GenerateUniqueID();
        }

        public void OnBeforeSerialize()
        {
            clone = data.GetSerializeableClone();
        }

        public void OnAfterDeserialize()
        {
            data = clone.GetRealOne();
        }
    }
}