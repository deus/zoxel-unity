using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Used to load different levels of data from the camera to the far distance.
	public struct ChunkDivision : IComponentData
	{
		//! The chunk distance from a chunk stream point.
		public byte viewDistance;

		public ChunkDivision(byte viewDistance)
		{
			this.viewDistance = viewDistance;
		}
	}
}