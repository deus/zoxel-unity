using Unity.Entities;

namespace Zoxel.Voxels
{
	public struct ChunkIndex : IComponentData
    {
        public int index;

        public ChunkIndex(int index)
        {
            this.index = index;
        }

		public override int GetHashCode()
		{
            unchecked
            {
				return index.GetHashCode();
			}
		}
    }
}