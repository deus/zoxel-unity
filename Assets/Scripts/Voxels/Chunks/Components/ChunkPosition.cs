using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Voxels
{
	//! A position int3 for a Chunk of a StreamableVox.
	public struct ChunkPosition : IComponentData
	{
		//! A position based on chunks in world.
		public int3 position;

		public override int GetHashCode()
		{
            unchecked
            {
				return position.GetHashCode();
			}
		}

		public ChunkPosition(int3 position)
		{
			this.position = position;
		}

		public int3 GetVoxelPosition(int3 voxelDimensions)
        {
            return GetChunkVoxelPosition(position, voxelDimensions);
		}

		public float3 GetRealPosition(int3 voxelDimensions, float3 voxScale)
        {
            var chunkVoxelPosition = GetChunkVoxelPosition(position, voxelDimensions);
			return new float3(chunkVoxelPosition.x * voxScale.x, chunkVoxelPosition.y * voxScale.y, chunkVoxelPosition.z * voxScale.z);
		}

        public static int3 GetChunkVoxelPosition(int3 chunkPosition, int3 voxelDimensions)
        {
			return VoxelUtilities.GetChunkVoxelPosition(chunkPosition, voxelDimensions);
        }

		public void GetChunkBounds(int3 voxelDimensions, out int3 chunkLowerBounds, out int3 chunkUpperBounds)
		{
			var voxelPosition = GetVoxelPosition(voxelDimensions);
			chunkLowerBounds = voxelPosition;
			chunkUpperBounds = voxelPosition + voxelDimensions;
		}

		// max numbers for chunks, - or + 4096?
		const int maxChunkNumber = 4096;

		public static int ConvertToUniqueID(int3 position)
		{
			// Keep within max number boundaries
			var positionClamped = new int3(position.x % maxChunkNumber, position.y % maxChunkNumber, position.z % maxChunkNumber);
			return positionClamped.x
				+ (2 * maxChunkNumber) * positionClamped.y
				+ (2 * maxChunkNumber * 2 * maxChunkNumber) * positionClamped.z;
		}
	}
}

/*
GLM_INLINE void hash_combine(size_t &seed, size_t hash)
    {
        hash += 0x9e3779b9 + (seed << 6) + (seed >> 2);
        seed ^= hash;
    }
	*/