using Unity.Entities;

namespace Zoxel.Voxels
{
    //! When something is linked to a chunk
    public struct ChunkLink : IComponentData
    {
        public Entity chunk;

        public ChunkLink(Entity chunk)
        {
            this.chunk = chunk;
        }
    }
}