using Unity.Entities;

namespace Zoxel.Voxels
{
    //! Holds Chunk dimensions, stored on a Vox.
	public struct ChunkDimensions : IComponentData
	{
        //! Each Chunk's max dimension data.
		public int3 voxelDimensions;

        public ChunkDimensions(int3 voxelDimensions)
        {
            this.voxelDimensions = voxelDimensions;
        }
    }
}