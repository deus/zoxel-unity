using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels
{
	//! The render side information for each voxel.
	/**
	*	We only need one lot of sides as we will never render two voxel faces at once.
	*	\todo Move this to Chunk instead of ChunkRender. Use ChunkRenders though to process it.
	*/
	public struct ChunkSides : IComponentData
	{
		const int dataSize = 1;
		const int alignment = 4;
		//! Another storing of VoxelDimensions. Remove this and pass it in.
		public int3 voxelDimensions;
		//! Stores whether to render voxel sides or not.
		public BlitableArray<byte> sides;

		public ChunkSides(int3 voxelDimensions)
		{
			this.voxelDimensions = voxelDimensions;
			var xyzSize = voxelDimensions.x * voxelDimensions.y * voxelDimensions.z;
			this.sides = new BlitableArray<byte>(xyzSize, Allocator.Persistent, dataSize, alignment);
		}

        public void Initialize(int3 voxelDimensions)
        {
			if (this.voxelDimensions.x != voxelDimensions.x || this.voxelDimensions.y != voxelDimensions.y || this.voxelDimensions.z != voxelDimensions.z)
			{
				Dispose();
				this.voxelDimensions = voxelDimensions;
				var xyzSize = voxelDimensions.x * voxelDimensions.y * voxelDimensions.z;
				sides = new BlitableArray<byte>(xyzSize, Allocator.Persistent, dataSize, alignment);
			}
        }

        public void Dispose()
        {
			sides.Dispose();
        }

        public void DisposeFinal()
        {
			sides.DisposeFinal();
        }

		public byte GetSideIndex(int voxelIndex, byte sideIndex)
		{
			if ((sides[voxelIndex] & (1 << sideIndex)) > 0) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
    }
}

		/*public static byte GetSideIndex(byte totalBytes, int sideIndex)
		{
			if ((totalBytes & (1 << sideIndex)) > 0) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}*/

		/*public byte GetSideIndex2(int voxelIndex, byte sideIndex)
		{
			if (sides.Length == 0 || voxelIndex >= sides.Length)
			{
				return 0;
			}
			if ((sides[voxelIndex] & (1 << sideIndex)) > 0) 
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}*/
			/*if (sides.Length == 0 || voxelIndex >= sides.Length)
			{
				return 0;
			}*/