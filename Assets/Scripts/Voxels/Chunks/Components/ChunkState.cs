using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Keeps a copy of the chunk data for restoring later.
	public struct ChunkState : IComponentData
	{
		public Chunk data;		// voxel data

		public ChunkState(in Chunk chunk)
		{
			data = chunk.Clone();
		}

		public void Dispose()
		{
			data.Dispose();
		}
	}
}