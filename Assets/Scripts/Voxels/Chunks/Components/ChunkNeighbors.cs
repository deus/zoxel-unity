using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels
{
	//! Each chunk has referencs to it's neighbor chunks.
	public struct ChunkNeighbors : IComponentData
	{
		// todo: Make store hashmap based on local chunk positions - can use this to calculate, perhaps?
		public Entity chunkUp;
		public Entity chunkDown;
		public Entity chunkLeft;
		public Entity chunkRight;
		public Entity chunkForward;
		public Entity chunkBack;
		// normal diagnols
		public Entity chunkForwardLeft;
		public Entity chunkForwardRight;
		public Entity chunkBackwardLeft;
		public Entity chunkBackwardRight;
		// Vertical Directionals
		public Entity chunkUpLeft;
		public Entity chunkUpRight;
		public Entity chunkUpBackward;
		public Entity chunkUpForward;
		public Entity chunkDownLeft;
		public Entity chunkDownRight;
		public Entity chunkDownBackward;
		public Entity chunkDownForward;
		// top diagnols
		public Entity chunkForwardLeftUp;
		public Entity chunkForwardRightUp;
		public Entity chunkBackwardLeftUp;
		public Entity chunkBackwardRightUp;
		public Entity chunkForwardLeftDown;
		public Entity chunkForwardRightDown;
		public Entity chunkBackwardLeftDown;
		public Entity chunkBackwardRightDown;

		public Entity GetChunkAdjacent(ref int3 localPosition, int3 voxelDimensions)
		{
			if (localPosition.x < 0)
			{
				localPosition.x += voxelDimensions.x;
				return chunkLeft;
			}
			else if (localPosition.x >= voxelDimensions.x)
			{
				localPosition.x -= voxelDimensions.x; // = 0;
				return chunkRight;
			}
			else if (localPosition.z < 0)
			{
				localPosition.z += voxelDimensions.z;
				return chunkBack;
			}
			else if (localPosition.z >= voxelDimensions.z)
			{
				localPosition.z -= voxelDimensions.z;
				return chunkForward;
			}
			else if (localPosition.y < 0)
			{
				localPosition.y += voxelDimensions.y;
				return chunkDown;
			}
			else if (localPosition.y >= voxelDimensions.y)
			{
				localPosition.y -= voxelDimensions.y;
				return chunkUp;
			}
			return new Entity();
		}

		//! Gets a neighbor chunk based on the local position going out of bounds.
		public Entity GetChunk(ref int3 localPosition, int3 voxelDimensions)
		{
			var isWithinBoundsX = localPosition.x >= 0 && localPosition.x < voxelDimensions.x;
			var isWithinBoundsY = localPosition.y >= 0 && localPosition.y < voxelDimensions.y;
			var isWithinBoundsZ = localPosition.z >= 0 && localPosition.z < voxelDimensions.z;
			var isBelowBoundsX = localPosition.x < 0;
			var isAboveBoundsX = localPosition.x >= voxelDimensions.x;
			var isBelowBoundsY = localPosition.y < 0;
			var isAboveBoundsY = localPosition.y >= voxelDimensions.y;
			var isBelowBoundsZ = localPosition.z < 0;
			var isAboveBoundsZ = localPosition.z >= voxelDimensions.z;
			// modify localPosition
			if (isAboveBoundsX)
			{
				localPosition.x -= voxelDimensions.x; // = 0;
			}
			else if (isBelowBoundsX)
			{
				localPosition.x += voxelDimensions.x; // = voxelDimensions.x - 1;
			}
			if (isAboveBoundsY)
			{
				localPosition.y -= voxelDimensions.y; // = 0;
			}
			else if (isBelowBoundsY)
			{
				localPosition.y += voxelDimensions.y; // = voxelDimensions.y - 1;
			}
			if (isAboveBoundsZ)
			{
				localPosition.z -= voxelDimensions.z; // = 0;
			}
			else if (isBelowBoundsZ)
			{
				localPosition.z += voxelDimensions.z; // = voxelDimensions.z - 1;
			}
			if (isWithinBoundsX && isWithinBoundsY && isAboveBoundsZ)
			{
				return chunkForward;
			}
			else if (isWithinBoundsX && isWithinBoundsY && isBelowBoundsZ)
			{
				return chunkBack;
			}
			else if (isWithinBoundsX && isWithinBoundsZ && isBelowBoundsY)
			{
				return chunkDown;
			}
			else if (isWithinBoundsX && isWithinBoundsZ && isAboveBoundsY)
			{
				return chunkUp;
			}
			else if (isWithinBoundsY && isWithinBoundsZ && isBelowBoundsX)
			{
				return chunkLeft;
			}
			else if (isWithinBoundsY && isWithinBoundsZ && isAboveBoundsX)
			{
				return chunkRight;
			}
			// first in Y
			else if (isWithinBoundsY && isBelowBoundsX && isAboveBoundsZ)
			{
				return chunkForwardLeft;
			}
			else if (isWithinBoundsY && isAboveBoundsX && isAboveBoundsZ)
			{
				return chunkForwardRight;
			}
			else if (isWithinBoundsY && isBelowBoundsX && isBelowBoundsZ)
			{
				return chunkBackwardLeft;
			}
			else if (isWithinBoundsY && isAboveBoundsX && isBelowBoundsZ)
			{
				return chunkBackwardRight;
			}
			// Vertical Directionals
			else if (isWithinBoundsZ && isBelowBoundsX && isAboveBoundsY)
			{
				return chunkUpLeft;
			}
			else if (isWithinBoundsZ && isAboveBoundsX && isAboveBoundsY)
			{
				return chunkUpRight;
			}
			else if (isWithinBoundsX && isBelowBoundsZ && isAboveBoundsY)
			{
				return chunkUpBackward;
			}
			else if (isWithinBoundsX && isAboveBoundsZ && isAboveBoundsY)
			{
				return chunkUpForward;
			}

			else if (isWithinBoundsZ && isBelowBoundsX && isBelowBoundsY)
			{
				return chunkDownLeft;
			}
			else if (isWithinBoundsZ && isAboveBoundsX && isBelowBoundsY)
			{
				return chunkDownRight;
			}
			else if (isWithinBoundsX && isBelowBoundsZ && isBelowBoundsY)
			{
				return chunkDownBackward;
			}
			else if (isWithinBoundsX && isAboveBoundsZ && isBelowBoundsY)
			{
				return chunkDownForward;
			}
			// within X bounds
			// first in Y
			else if (isBelowBoundsX && isBelowBoundsY && isBelowBoundsZ)
			{
				return chunkBackwardLeftDown;
			}
			else if (isAboveBoundsX && isBelowBoundsY && localPosition.z < voxelDimensions.z)
			{
				return chunkBackwardRightDown;
			}
			else if (isBelowBoundsX && isBelowBoundsY && isAboveBoundsZ)
			{
				return chunkForwardLeftDown;
			}
			else if (isAboveBoundsX && isBelowBoundsY && isAboveBoundsZ)
			{
				return chunkForwardRightDown;
			}

			else if (isBelowBoundsX && isAboveBoundsY && isBelowBoundsZ)
			{
				return chunkBackwardLeftUp;
			}
			else if (isAboveBoundsX && isAboveBoundsY && localPosition.z < voxelDimensions.z)
			{
				return chunkBackwardRightUp;
			}
			else if (isBelowBoundsX && isAboveBoundsY && isAboveBoundsZ)
			{
				return chunkForwardLeftUp;
			}
			else if (isAboveBoundsX && isAboveBoundsY && isAboveBoundsZ)
			{
				return chunkForwardRightUp;
			}
			return new Entity();
		}

		public Entity GetChunkAdjacent(int3 localPosition, int3 voxelDimensions, out int3 otherVoxelPosition)
		{
			var isWithinBoundsX = localPosition.x >= 0 && localPosition.x < voxelDimensions.x;
			var isWithinBoundsY = localPosition.y >= 0 && localPosition.y < voxelDimensions.y;
			var isWithinBoundsZ = localPosition.z >= 0 && localPosition.z < voxelDimensions.z;
			if (isWithinBoundsX && isWithinBoundsZ && localPosition.y < 0)
			{
				otherVoxelPosition = new int3(localPosition.x, voxelDimensions.y - 1, localPosition.z);
				return chunkDown;
			}
			else if (isWithinBoundsX && isWithinBoundsZ && localPosition.y >= voxelDimensions.y)
			{
				otherVoxelPosition = new int3(localPosition.x, 0, localPosition.z);
				return chunkUp;
			}
			else if (isWithinBoundsX && isWithinBoundsY && localPosition.z < 0)
			{
				otherVoxelPosition = new int3(localPosition.x, localPosition.y, voxelDimensions.z - 1);
				return chunkBack;
			}
			else if (isWithinBoundsX && isWithinBoundsY && localPosition.z >= voxelDimensions.z)
			{
				otherVoxelPosition = new int3(localPosition.x, localPosition.y, 0);
				return chunkForward;
			}
			else if (isWithinBoundsY && isWithinBoundsZ && localPosition.x < 0)
			{
				otherVoxelPosition = new int3(voxelDimensions.x - 1, localPosition.y, localPosition.z);
				return chunkLeft;
			}
			else if (isWithinBoundsY && isWithinBoundsZ && localPosition.x >= voxelDimensions.x)
			{
				otherVoxelPosition = new int3(0, localPosition.y, localPosition.z);
				return chunkRight;
			}
			otherVoxelPosition = new int3(0, 0, 0);
			return new Entity();
		}

		// todo: only clear those whos position doesn't match, when starting neighbors search
		public void ClearChunkNeighbors()
		{
			chunkDown = new Entity();
			chunkUp = new Entity();
			chunkLeft = new Entity();
			chunkRight = new Entity();
			chunkBack = new Entity();
			chunkForward = new Entity();
			chunkForwardLeft = new Entity();
			chunkForwardRight = new Entity();
			chunkBackwardLeft = new Entity();
			chunkBackwardRight = new Entity();
			chunkUpLeft = new Entity();
			chunkUpRight = new Entity();
			chunkUpBackward = new Entity();
			chunkUpForward = new Entity();
			chunkDownLeft = new Entity();
			chunkDownRight = new Entity();
			chunkDownBackward = new Entity();
			chunkDownForward = new Entity();
			chunkForwardLeftUp = new Entity();
			chunkForwardRightUp = new Entity();
			chunkBackwardLeftUp = new Entity();
			chunkBackwardRightUp = new Entity();
			chunkForwardLeftDown = new Entity();
			chunkForwardRightDown = new Entity();
			chunkBackwardLeftDown = new Entity();
			chunkBackwardRightDown = new Entity();
		}

		public NativeArray<Entity> GetNeighborsAdjacent()
		{
			var neighbors = new NativeArray<Entity>(6, Allocator.Temp);
			neighbors[0] = chunkDown;
			neighbors[1] = chunkUp;
			neighbors[2] = chunkLeft;
			neighbors[3] = chunkRight;
			neighbors[4] = chunkBack;
			neighbors[5] = chunkForward;
			return neighbors;
		}

		public NativeArray<Entity> GetNeighbors()
		{
			var neighbors = new NativeArray<Entity>(26, Allocator.Temp);	// 6 + 4 + 8 + 8
			neighbors[0] = chunkDown;
			neighbors[1] = chunkUp;
			neighbors[2] = chunkLeft;
			neighbors[3] = chunkRight;
			neighbors[4] = chunkBack;
			neighbors[5] = chunkForward;

			neighbors[6] = chunkForwardLeft;
			neighbors[7] = chunkForwardRight;
			neighbors[8] = chunkBackwardLeft;
			neighbors[9] = chunkBackwardRight;

			neighbors[10] = chunkForwardLeftUp;
			neighbors[11] = chunkForwardRightUp;
			neighbors[12] = chunkBackwardLeftUp;
			neighbors[13] = chunkBackwardRightUp;
			neighbors[14] = chunkForwardLeftDown;
			neighbors[15] = chunkForwardRightDown;
			neighbors[16] = chunkBackwardLeftDown;
			neighbors[17] = chunkBackwardRightDown;
			
			neighbors[18] = chunkUpLeft;
			neighbors[19] = chunkUpRight;
			neighbors[20] = chunkUpBackward;
			neighbors[21] = chunkUpForward;
			neighbors[22] = chunkDownLeft;
			neighbors[23] = chunkDownRight;
			neighbors[24] = chunkDownBackward;
			neighbors[25] = chunkDownForward;

			return neighbors;
		}

		public bool HasFoundNeighbors(byte isStreamY = 1)
		{
			return chunkLeft.Index != 0 && chunkRight.Index != 0 
				&& chunkBack.Index != 0 && chunkForward.Index != 0
				&& chunkForwardLeft.Index != 0 && chunkForwardRight.Index != 0
				&& chunkBackwardLeft.Index != 0 && chunkBackwardRight.Index != 0 && 
				(isStreamY == 0 || (isStreamY == 1 && chunkUp.Index != 0 && chunkDown.Index != 0
					 && chunkForwardLeftUp.Index != 0 && chunkForwardRightUp.Index != 0 && chunkBackwardLeftUp.Index != 0 && chunkBackwardRightUp.Index != 0
					  && chunkForwardLeftDown.Index != 0 && chunkForwardRightDown.Index != 0 && chunkBackwardLeftDown.Index != 0 && chunkBackwardRightDown.Index != 0
					 && chunkUpLeft.Index != 0 && chunkUpRight.Index != 0 && chunkUpBackward.Index != 0 && chunkUpForward.Index != 0
					  && chunkDownLeft.Index != 0 && chunkDownRight.Index != 0 && chunkDownBackward.Index != 0 && chunkDownForward.Index != 0));
		}

		//! \todo Make ForwardLeft and ForwardLeftUp int3 functions
		public NativeArray<int3> GetNeighborPositions(int3 chunkPosition)
		{
			var neighbors = new NativeArray<int3>(6 + 4 + 8 + 8, Allocator.Temp);
			neighbors[0] = chunkPosition.Down();
			neighbors[1] = chunkPosition.Up();
			neighbors[2] = chunkPosition.Left();
			neighbors[3] = chunkPosition.Right();
			neighbors[4] = chunkPosition.Backward();
			neighbors[5] = chunkPosition.Forward();
			neighbors[6] = chunkPosition.Forward().Left();
			neighbors[7] = chunkPosition.Forward().Right();
			neighbors[8] = chunkPosition.Backward().Left();
			neighbors[9] = chunkPosition.Backward().Right();
			neighbors[10] = chunkPosition.Forward().Left().Up();
			neighbors[11] = chunkPosition.Forward().Right().Up();
			neighbors[12] = chunkPosition.Backward().Left().Up();
			neighbors[13] = chunkPosition.Backward().Right().Up();
			neighbors[14] = chunkPosition.Forward().Left().Down();
			neighbors[15] = chunkPosition.Forward().Right().Down();
			neighbors[16] = chunkPosition.Backward().Left().Down();
			neighbors[17] = chunkPosition.Backward().Right().Down();
			neighbors[18] = chunkPosition.Up().Left();
			neighbors[19] = chunkPosition.Up().Right();
			neighbors[20] = chunkPosition.Up().Backward();
			neighbors[21] = chunkPosition.Up().Forward();
			neighbors[22] = chunkPosition.Down().Left(); 
			neighbors[23] = chunkPosition.Down().Right(); 
			neighbors[24] = chunkPosition.Down().Backward(); 
			neighbors[25] = chunkPosition.Down().Forward(); 
			return neighbors;
		}

		public bool SetChunkNeighbor(Entity otherChunkEntity, int3 otherChunkPosition, int3 chunkPosition, byte isStreamY = 1)
		{
			if (otherChunkPosition == chunkPosition.Left())
			{
				chunkLeft = otherChunkEntity;
			}
			else if (otherChunkPosition == chunkPosition.Right())
			{
				chunkRight = otherChunkEntity;
			}
			else if (otherChunkPosition == chunkPosition.Forward())
			{
				chunkForward = otherChunkEntity;
			}
			else if (otherChunkPosition == chunkPosition.Backward())
			{
				chunkBack = otherChunkEntity;
			}
			else if (otherChunkPosition == chunkPosition.Forward().Left())
			{
				chunkForwardLeft = otherChunkEntity;
			}
			else if (otherChunkPosition == chunkPosition.Forward().Right())
			{
				chunkForwardRight = otherChunkEntity;
			}
			else if (otherChunkPosition == chunkPosition.Backward().Left())
			{
				chunkBackwardLeft = otherChunkEntity;
			}
			else if (otherChunkPosition == chunkPosition.Backward().Right())
			{
				chunkBackwardRight = otherChunkEntity;
			}
			else if (isStreamY == 1)
			{
				if (otherChunkPosition == chunkPosition.Up())
				{
					chunkUp = otherChunkEntity;
				}
				else if (otherChunkPosition == chunkPosition.Down())
				{
					chunkDown = otherChunkEntity;
				}
				else if (otherChunkPosition == chunkPosition.Forward().Left().Up())
				{
					chunkForwardLeftUp = otherChunkEntity;
				}
				else if (otherChunkPosition == chunkPosition.Forward().Right().Up())
				{
					chunkForwardRightUp = otherChunkEntity;
				}
				else if (otherChunkPosition == chunkPosition.Backward().Left().Up())
				{
					chunkBackwardLeftUp = otherChunkEntity;
				}
				else if (otherChunkPosition == chunkPosition.Backward().Right().Up())
				{
					chunkBackwardRightUp = otherChunkEntity;
				}

				else if (otherChunkPosition == chunkPosition.Forward().Left().Down())
				{
					chunkForwardLeftDown = otherChunkEntity;
				}
				else if (otherChunkPosition == chunkPosition.Forward().Right().Down())
				{
					chunkForwardRightDown = otherChunkEntity;
				}
				else if (otherChunkPosition == chunkPosition.Backward().Left().Down())
				{
					chunkBackwardLeftDown = otherChunkEntity;
				}
				else if (otherChunkPosition == chunkPosition.Backward().Right().Down())
				{
					chunkBackwardRightDown = otherChunkEntity;
				}

				else if (otherChunkPosition == chunkPosition.Up().Left())
				{
					chunkUpLeft = otherChunkEntity;
				}
				else if (otherChunkPosition == chunkPosition.Up().Right())
				{
					chunkUpRight = otherChunkEntity;
				}
				else if (otherChunkPosition == chunkPosition.Up().Backward())
				{
					chunkUpBackward = otherChunkEntity;
				}
				else if (otherChunkPosition == chunkPosition.Up().Forward())
				{
					chunkUpForward = otherChunkEntity;
				}

				else if (otherChunkPosition == chunkPosition.Down().Left())
				{
					chunkDownLeft = otherChunkEntity;
				}
				else if (otherChunkPosition == chunkPosition.Down().Right())
				{
					chunkDownRight = otherChunkEntity;
				}
				else if (otherChunkPosition == chunkPosition.Down().Backward())
				{
					chunkDownBackward = otherChunkEntity;
				}
				else if (otherChunkPosition == chunkPosition.Down().Forward())
				{
					chunkDownForward = otherChunkEntity;
				}

				else
				{
					return false;
				}
			}
			else
			{
				return false;
			}
			return true;
		}

		public Entity GetChunkAdjacent(ref int3 localPosition, ref int3 chunkPosition, int3 voxelDimensions) // , out int3 otherVoxelPosition)
		{
			var isWithinBoundsX = localPosition.x >= 0 && localPosition.x < voxelDimensions.x;
			var isWithinBoundsY = localPosition.y >= 0 && localPosition.y < voxelDimensions.y;
			var isWithinBoundsZ = localPosition.z >= 0 && localPosition.z < voxelDimensions.z;
			var isBelowBoundsX = localPosition.x < 0;
			var isAboveBoundsX = localPosition.x >= voxelDimensions.x;
			var isBelowBoundsY = localPosition.y < 0;
			var isAboveBoundsY = localPosition.y >= voxelDimensions.y;
			var isBelowBoundsZ = localPosition.z < 0;
			var isAboveBoundsZ = localPosition.z >= voxelDimensions.z;
			if (isWithinBoundsX && isWithinBoundsY && isBelowBoundsZ)
			{
				// otherVoxelPosition = new int3(localPosition.x, localPosition.y, voxelDimensions.z - 1);
				localPosition.z = voxelDimensions.z - 1;
				chunkPosition.z--;
				return chunkBack;
			}
			else if (isWithinBoundsX && isWithinBoundsY && isAboveBoundsZ)
			{
				// otherVoxelPosition = new int3(localPosition.x, localPosition.y, 0);
				localPosition.z = 0;
				chunkPosition.z++;
				return chunkForward;
			}
			else if (isWithinBoundsX && isWithinBoundsZ && isBelowBoundsY)
			{
				// otherVoxelPosition = new int3(localPosition.x, voxelDimensions.y - 1, localPosition.z);
				localPosition.y = voxelDimensions.y - 1;
				chunkPosition.y--;
				return chunkDown;
			}
			else if (isWithinBoundsX && isWithinBoundsZ && isAboveBoundsY)
			{
				//otherVoxelPosition = new int3(localPosition.x, 0, localPosition.z);
				localPosition.y = 0;
				chunkPosition.y++;
				return chunkUp;
			}
			else if (isWithinBoundsY && isWithinBoundsZ && isBelowBoundsX)
			{
				// otherVoxelPosition = new int3(voxelDimensions.x - 1, localPosition.y, localPosition.z);
				localPosition.x = voxelDimensions.x - 1;
				chunkPosition.x--;
				return chunkLeft;
			}
			else if (isWithinBoundsY && isWithinBoundsZ && isAboveBoundsX)
			{
				//otherVoxelPosition = new int3(0, localPosition.y, localPosition.z);
				localPosition.x = 0;
				chunkPosition.x++;
				return chunkRight;
			}
			return new Entity();
		}

		public Entity GetChunk(ref int3 localPosition, ref int3 chunkPosition, int3 voxelDimensions) // , out int3 otherVoxelPosition)
		{
			var isWithinBoundsX = localPosition.x >= 0 && localPosition.x < voxelDimensions.x;
			var isWithinBoundsY = localPosition.y >= 0 && localPosition.y < voxelDimensions.y;
			var isWithinBoundsZ = localPosition.z >= 0 && localPosition.z < voxelDimensions.z;
			var isBelowBoundsX = localPosition.x < 0;
			var isAboveBoundsX = localPosition.x >= voxelDimensions.x;
			var isBelowBoundsY = localPosition.y < 0;
			var isAboveBoundsY = localPosition.y >= voxelDimensions.y;
			var isBelowBoundsZ = localPosition.z < 0;
			var isAboveBoundsZ = localPosition.z >= voxelDimensions.z;
			if (isWithinBoundsX && isWithinBoundsY && isAboveBoundsZ)
			{
				// otherVoxelPosition = new int3(localPosition.x, localPosition.y, 0);
				localPosition.z = 0;
				chunkPosition.z++;
				return chunkForward;
			}
			else if (isWithinBoundsX && isWithinBoundsY && isBelowBoundsZ)
			{
				// otherVoxelPosition = new int3(localPosition.x, localPosition.y, voxelDimensions.z - 1);
				localPosition.z = voxelDimensions.z - 1;
				chunkPosition.z--;
				return chunkBack;
			}
			else if (isWithinBoundsX && isWithinBoundsZ && isBelowBoundsY)
			{
				// otherVoxelPosition = new int3(localPosition.x, voxelDimensions.y - 1, localPosition.z);
				localPosition.y = voxelDimensions.y - 1;
				chunkPosition.y--;
				return chunkDown;
			}
			else if (isWithinBoundsX && isWithinBoundsZ && isAboveBoundsY)
			{
				//otherVoxelPosition = new int3(localPosition.x, 0, localPosition.z);
				localPosition.y = 0;
				chunkPosition.y++;
				return chunkUp;
			}
			else if (isWithinBoundsY && isWithinBoundsZ && isBelowBoundsX)
			{
				// otherVoxelPosition = new int3(voxelDimensions.x - 1, localPosition.y, localPosition.z);
				localPosition.x = voxelDimensions.x - 1;
				chunkPosition.x--;
				return chunkLeft;
			}
			else if (isWithinBoundsY && isWithinBoundsZ && isAboveBoundsX)
			{
				//otherVoxelPosition = new int3(0, localPosition.y, localPosition.z);
				localPosition.x = 0;
				chunkPosition.x++;
				return chunkRight;
			}
			// first in Y
			else if (isWithinBoundsY && isBelowBoundsX && isAboveBoundsZ)
			{
				// otherVoxelPosition = new int3(voxelDimensions.x - 1, localPosition.y, 0);
				localPosition.x = voxelDimensions.x - 1;
				localPosition.z = 0;
				chunkPosition.x--;
				chunkPosition.z++;
				return chunkForwardLeft;
			}
			else if (isWithinBoundsY && isAboveBoundsX && isAboveBoundsZ)
			{
				// otherVoxelPosition = new int3(0, localPosition.y, 0);
				localPosition.x = 0;
				localPosition.z = 0;
				chunkPosition.x++;
				chunkPosition.z++;
				return chunkForwardRight;
			}
			else if (isWithinBoundsY && isBelowBoundsX && isBelowBoundsZ)
			{
				// otherVoxelPosition = new int3(voxelDimensions.x - 1, localPosition.y, voxelDimensions.z - 1);
				localPosition.x = voxelDimensions.x - 1;
				localPosition.z = voxelDimensions.z - 1;
				chunkPosition.x--;
				chunkPosition.z--;
				return chunkBackwardLeft;
			}
			else if (isWithinBoundsY && isAboveBoundsX && isBelowBoundsZ)
			{
				// otherVoxelPosition = new int3(0, localPosition.y, voxelDimensions.z - 1);
				localPosition.x = 0;
				localPosition.z = voxelDimensions.z - 1;
				chunkPosition.x++;
				chunkPosition.z--;
				return chunkBackwardRight;
			}

			// Vertical Directionals
			else if (isWithinBoundsZ && isBelowBoundsX && isAboveBoundsY)
			{
				// otherVoxelPosition = new int3(voxelDimensions.x - 1, 0, localPosition.z);
				localPosition.x = voxelDimensions.x - 1;
				localPosition.y = 0;
				chunkPosition.x--;
				chunkPosition.y++;
				return chunkUpLeft;
			}
			else if (isWithinBoundsZ && isAboveBoundsX && isAboveBoundsY)
			{
				// otherVoxelPosition = new int3(0, 0, localPosition.z);
				localPosition.x = 0;
				localPosition.y = 0;
				chunkPosition.x++;
				chunkPosition.y++;
				return chunkUpRight;
			}
			else if (isWithinBoundsX && isBelowBoundsZ && isAboveBoundsY)
			{
				// otherVoxelPosition = new int3(localPosition.x, 0, voxelDimensions.z - 1);
				localPosition.y = 0;
				localPosition.z = voxelDimensions.z - 1;
				chunkPosition.y++;
				chunkPosition.z--;
				return chunkUpBackward;
			}
			else if (isWithinBoundsX && isAboveBoundsZ && isAboveBoundsY)
			{
				//otherVoxelPosition = new int3(localPosition.x, 0, 0);
				localPosition.y = 0;
				localPosition.z = 0;
				chunkPosition.y++;
				chunkPosition.z++;
				return chunkUpForward;
			}

			else if (isWithinBoundsZ && isBelowBoundsX && isBelowBoundsY)
			{
				// otherVoxelPosition = new int3(voxelDimensions.x - 1, voxelDimensions.y - 1, localPosition.z);
				localPosition.x = voxelDimensions.x - 1;
				localPosition.y = voxelDimensions.y - 1;
				chunkPosition.x--;
				chunkPosition.y--;
				return chunkDownLeft;
			}
			else if (isWithinBoundsZ && isAboveBoundsX && isBelowBoundsY)
			{
				//otherVoxelPosition = new int3(0, voxelDimensions.y - 1, localPosition.z);
				localPosition.x = 0;
				localPosition.y = voxelDimensions.y - 1;
				chunkPosition.x++;
				chunkPosition.y--;
				return chunkDownRight;
			}
			else if (isWithinBoundsX && isBelowBoundsZ && isBelowBoundsY)
			{
				// otherVoxelPosition = new int3(localPosition.x, voxelDimensions.y - 1, voxelDimensions.z - 1);
				localPosition.y = voxelDimensions.y - 1;
				localPosition.z = voxelDimensions.z - 1;
				chunkPosition.y--;
				chunkPosition.z--;
				return chunkDownBackward;
			}
			else if (isWithinBoundsX && isAboveBoundsZ && isBelowBoundsY)
			{
				//otherVoxelPosition = new int3(localPosition.x, voxelDimensions.y - 1, 0);
				localPosition.y = voxelDimensions.y - 1;
				localPosition.z = 0;
				chunkPosition.y--;
				chunkPosition.z++;
				return chunkDownForward;
			}
			
			// within X bounds
			// first in Y
			else if (isBelowBoundsX && isBelowBoundsY && isBelowBoundsZ)
			{
				//otherVoxelPosition = new int3(voxelDimensions.x - 1, voxelDimensions.y - 1, voxelDimensions.z - 1);
				localPosition.x = voxelDimensions.x - 1;
				localPosition.y = voxelDimensions.y - 1;
				localPosition.z = voxelDimensions.z - 1;
				chunkPosition.x--;
				chunkPosition.y--;
				chunkPosition.z--;
				return chunkBackwardLeftDown;
			}
			else if (isAboveBoundsX && isBelowBoundsY && localPosition.z < voxelDimensions.z)
			{
				// otherVoxelPosition = new int3(0, voxelDimensions.y - 1, voxelDimensions.z - 1);
				localPosition.x = 0;
				localPosition.y = voxelDimensions.y - 1;
				localPosition.z = voxelDimensions.z - 1;
				chunkPosition.x++;
				chunkPosition.y--;
				chunkPosition.z--;
				return chunkBackwardRightDown;
			}
			else if (isBelowBoundsX && isBelowBoundsY && isAboveBoundsZ)
			{
				// otherVoxelPosition = new int3(voxelDimensions.x - 1, voxelDimensions.y - 1, 0);
				localPosition.x = voxelDimensions.x - 1;
				localPosition.y = voxelDimensions.y - 1;
				localPosition.z = 0;
				chunkPosition.x--;
				chunkPosition.y--;
				chunkPosition.z++;
				return chunkForwardLeftDown;
			}
			else if (isAboveBoundsX && isBelowBoundsY && isAboveBoundsZ)
			{
				// otherVoxelPosition = new int3(0, voxelDimensions.y - 1, 0);
				localPosition.x = 0;
				localPosition.y = voxelDimensions.y - 1;
				localPosition.z = 0;
				chunkPosition.x++;
				chunkPosition.y--;
				chunkPosition.z++;
				return chunkForwardRightDown;
			}

			else if (isBelowBoundsX && isAboveBoundsY && isBelowBoundsZ)
			{
				// otherVoxelPosition = new int3(voxelDimensions.x - 1, 0, voxelDimensions.z - 1);
				localPosition.x = voxelDimensions.x - 1;
				localPosition.y = 0;
				localPosition.z = voxelDimensions.z - 1;
				chunkPosition.x--;
				chunkPosition.y++;
				chunkPosition.z--;
				return chunkBackwardLeftUp;
			}
			else if (isAboveBoundsX && isAboveBoundsY && localPosition.z < voxelDimensions.z)
			{
				// otherVoxelPosition = new int3(0, 0, voxelDimensions.z - 1);
				localPosition.x = 0;
				localPosition.y = 0;
				localPosition.z = voxelDimensions.z - 1;
				chunkPosition.x++;
				chunkPosition.y++;
				chunkPosition.z--;
				return chunkBackwardRightUp;
			}
			else if (isBelowBoundsX && isAboveBoundsY && isAboveBoundsZ)
			{
				// otherVoxelPosition = new int3(voxelDimensions.x - 1, 0, 0);
				localPosition.x = voxelDimensions.x - 1;
				localPosition.y = 0;
				localPosition.z = 0;
				chunkPosition.x--;
				chunkPosition.y++;
				chunkPosition.z++;
				return chunkForwardLeftUp;
			}
			else if (isAboveBoundsX && isAboveBoundsY && isAboveBoundsZ)
			{
				localPosition.x = 0;
				localPosition.y = 0;
				localPosition.z = 0;
				chunkPosition.x++;
				chunkPosition.y++;
				chunkPosition.z++;
				return chunkForwardRightUp;
			}
			return new Entity();
		}
	}
}