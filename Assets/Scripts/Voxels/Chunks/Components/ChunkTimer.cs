using Unity.Entities;

namespace Zoxel.Voxels
{
    //! A timer on a chunk used to place grass or convert grass blocks to dirt again.
    public struct ChunkTimer : IComponentData
    {
        public double lastTicked;
    }
}