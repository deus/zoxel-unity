﻿using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels
{
    //! Holds voxel data in a single array.
	/**
	*	\todo Seperate as tag and use VoxData for voxel data.
	*	\todo Remove voxelDimensions out and use parent ChunkDimensions.
	*/
	public struct Chunk : IComponentData
	{
		const int dataSize = 1;
		const int alignment = 1;
		//! \todo Pass in from Vox?
		public int3 voxelDimensions;
		//! Each voxel relates to Realm's VoxelLinks.
		public BlitableArray<byte> voxels;

		public Chunk(int3 voxelDimensions, byte isAllocate)
		{
			this.voxelDimensions = voxelDimensions;
			// Model
			if (isAllocate == 1)
			{
				var xyzSize = voxelDimensions.x * voxelDimensions.y * voxelDimensions.z;
				this.voxels = new BlitableArray<byte>(xyzSize, Allocator.Persistent, dataSize, alignment);
			}
			// PlanetChunk2D
			else
			{
				this.voxels = new BlitableArray<byte>();
			}
		}

		public Chunk(int3 voxelDimensions, BlitableArray<byte> voxels)
		{
			this.voxelDimensions = voxelDimensions;
			this.voxels = voxels;
		}

		public void Initialize()
		{
			// Dispose();
			var xyzSize = voxelDimensions.x * voxelDimensions.y * voxelDimensions.z;
			voxels = new BlitableArray<byte>(xyzSize, Allocator.Persistent, dataSize, alignment);
		}

		public void DisposeFinal()
		{
			voxels.DisposeFinal();
		}

		public void Dispose()
		{
			voxels.Dispose();
		}

        public void SetAllToAir()
        {
            for (int i = 0; i < voxels.Length; i++)
			{
				voxels[i] = 0;
			}
        }

		public bool ReInitializeChunk(int3 voxelDimensions)
		{
			if (this.voxelDimensions != voxelDimensions)
			{
				Dispose();
				this.voxelDimensions = voxelDimensions;
				var xyzSize = voxelDimensions.x * voxelDimensions.y * voxelDimensions.z;
				this.voxels = new BlitableArray<byte>(xyzSize, Allocator.Persistent, dataSize, alignment);
				return true;
			}
			return false;
		}

		public Chunk Clone()
		{
			var chunk = new Chunk();
			chunk.voxelDimensions = voxelDimensions;
			chunk.voxels = new BlitableArray<byte>(voxels.Length, Allocator.Persistent, dataSize, alignment);
			for (int i = 0; i < voxels.Length; i++)
			{
				chunk.voxels[i] = voxels[i];
			}
			return chunk;
		}
	}
}