using Unity.Entities;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Zoxel.Voxels
{
	//! Holds HashMap Voxel Rotations. A position index and rotation byte.
	public struct ChunkVoxelRotations : IComponentData
	{
        public UnsafeParallelHashMap<byte3, byte> rotations;

		public int Count()
		{
			if (rotations.IsCreated)
			{
				return rotations.Count();
			}
			else
			{
				return 0;
			}
		}

		public void Dispose()
		{
			if (rotations.IsCreated)
			{
				rotations.Dispose();
			}
		}

		public byte GetRotation(byte3 position)
		{
			byte rotation;
			if (rotations.IsCreated && rotations.TryGetValue(position, out rotation))
			{
				return rotation;
			}
			return BlockRotation.Up;
		}

		public byte GetRotation(int3 position)
		{
			byte rotation;
			if (rotations.IsCreated && rotations.TryGetValue(position.ToByte3(), out rotation))
			{
				return rotation;
			}
			return BlockRotation.Up;
		}

		// only use this once per frame
		public void AddRotation(byte3 position, byte rotation)
		{
			if (rotations.IsCreated)
			{
				rotations.Add(position, rotation);
			}
			else
			{
				rotations = new UnsafeParallelHashMap<byte3, byte>(1, Allocator.Persistent);
				rotations[position] = rotation;
			}
		}
		public void AddRotation(int3 position, byte rotation)
		{
			if (rotations.IsCreated)
			{
				rotations.Add(position.ToByte3(), rotation);
			}
			else
			{
				rotations = new UnsafeParallelHashMap<byte3, byte>(1, Allocator.Persistent);
				rotations[position.ToByte3()] = rotation;
			}
		}

		public void SetOrAddRotation(int3 position, byte rotation)
		{
			var position2 = position.ToByte3();
			if (rotations.IsCreated && rotations.ContainsKey(position2))
			{
				rotations[position2] = rotation;
			}
			else
			{
				AddRotation(position, rotation);
			}
		}

		public bool SetRotation(int3 position, byte rotation)
		{
			var position2 = position.ToByte3();
			if (rotations.IsCreated && rotations.ContainsKey(position2))
			{
				rotations[position2] = rotation;
				return true;
			}
			return false;
		}

		public void AddRotations(in NativeList<VoxelRotation> addRotations)
		{
			if (addRotations.Length == 0)
			{
				return;
			}
			if (rotations.IsCreated)
			{
				for (int i = 0; i < addRotations.Length; i++)
				{
					rotations.Add(addRotations[i].position, addRotations[i].rotation);
				}
			}
			else
			{
				rotations = new UnsafeParallelHashMap<byte3, byte>(addRotations.Length, Allocator.Persistent);
				for (int i = 0; i < addRotations.Length; i++)
				{
					rotations[addRotations[i].position] = addRotations[i].rotation;
				}
			}
		}

		public void Remove(int3 position)
		{
			var position2 = position.ToByte3();
			if (rotations.IsCreated && rotations.ContainsKey(position2))
			{
				rotations.Remove(position2);
			}
		}
	}
}

/*for (int i = 0; i < rotations.Length; i++)
{
	if (rotations[i].position == position)
	{
		return rotations[i].rotation;
	}
}*/
/*for (int i = 0; i < rotations.Length; i++)
{
	if (rotations[i].position == position)
	{
		var voxelRotation = rotations[i];
		voxelRotation.rotation = rotation;
		rotations[i] = voxelRotation;
		return true;
	}
}*/

// if not existing, add to list
/*var newRotations = new BlitableArray<VoxelRotation>(rotations.Length + addRotations.Length, Allocator.Persistent);
for (int i = 0; i < rotations.Length; i++)
{
	newRotations[i] = rotations[i];
}
int j = 0;
for (int i = rotations.Length; i < newRotations.Length; i++)
{
	newRotations[i] = addRotations[j];
	j++;
}
if (rotations.Length > 0)
{
	rotations.Dispose();
}
rotations = newRotations;*/

/*ar rotationIndex = -1;
for (int i = 0; i < rotations.Length; i++)
{
	if (rotations[i].position == position)
	{
		rotationIndex = i;
		break;
	}
}
if (rotationIndex != -1)
{
	// if not existing, add to list
	var newRotations = new BlitableArray<VoxelRotation>(rotations.Length - 1, Allocator.Persistent);
	for (int i = 0; i < rotationIndex; i++)
	{
		newRotations[i] = rotations[i];
	}
	for (int i = rotationIndex; i < newRotations.Length; i++)
	{
		newRotations[i] = rotations[i + 1];
	}
	if (rotations.Length > 0)
	{
		rotations.Dispose();
	}
	rotations = newRotations;
}*/
/*for (int i = 0; i < rotations.Length; i++)
{
	if (rotations[i].position == position)
	{
		var voxelRotation = rotations[i];
		voxelRotation.rotation = rotation;
		rotations[i] = voxelRotation;
		return;
	}
}
// if not existing, add to list
var newRotations = new BlitableArray<VoxelRotation>(rotations.Length + 1, Allocator.Persistent);
for (int i = 0; i < rotations.Length; i++)
{
	newRotations[i] = rotations[i];
}
newRotations[rotations.Length] = new VoxelRotation(position, rotation);
if (rotations.Length > 0)
{
	rotations.Dispose();
}
rotations = newRotations;*/