﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Voxels
{
	//!  Holds weights per voxel. Each weight corresponds to a bone index in the boneLinks.
    /**
    *  Every Item places voxels in bodybuilder - just set the index for the item added.
	*	This will correspond to the bone index
	*	\todo Apply Blending between bone joints.
    */
	public struct ChunkWeights : IComponentData
	{
		public int3 voxelDimensions;
		public BlitableArray<byte> weights;

		public ChunkWeights(int3 voxelDimensions)
		{
			this.voxelDimensions = voxelDimensions;
			var xyzSize = (int)(voxelDimensions.x * voxelDimensions.y * voxelDimensions.z);
			this.weights = new BlitableArray<byte>(xyzSize, Allocator.Persistent);
		}

		public void Initialize(int3 voxelDimensions)
		{
			if (this.voxelDimensions != voxelDimensions)
			{
				this.voxelDimensions = voxelDimensions;
				Dispose();
				var xyzSize = (int)(voxelDimensions.x * voxelDimensions.y * voxelDimensions.z);
				this.weights = new BlitableArray<byte>(xyzSize, Allocator.Persistent);
			}
		}

		public void DisposeFinal()
		{
			weights.DisposeFinal();
		}

		public void Dispose()
		{
			weights.Dispose();
		}
	}
}