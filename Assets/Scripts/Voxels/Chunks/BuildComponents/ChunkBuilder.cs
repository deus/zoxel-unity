﻿using Unity.Entities;
// todo: Carry any information from VoxelPlacement so I can optimize the update

namespace Zoxel.Voxels
{
    //! Builds a Chunk's meshes after an update. Normally EntityBusy is added at the same time.
    public struct ChunkBuilder : IComponentData { }
}