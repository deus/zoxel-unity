using Unity.Entities;

namespace Zoxel.Voxels
{
    public struct DelayChunkBuilder : IComponentData
    {
        public double timeStarted;
        public double timeDelay;

        public DelayChunkBuilder(double timeStarted, double timeDelay)
        {
            this.timeDelay = timeDelay;
            this.timeStarted = timeStarted;
        }
    }
}