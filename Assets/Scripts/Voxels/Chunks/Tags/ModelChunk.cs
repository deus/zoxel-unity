using Unity.Entities;

namespace Zoxel.Voxels
{
    //! A tag for a Chunk of a Model Vox Entity.
    public struct ModelChunk : IComponentData { }
}