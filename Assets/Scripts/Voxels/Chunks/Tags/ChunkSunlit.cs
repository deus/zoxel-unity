using Unity.Entities;

namespace Zoxel.Voxels
{
    //! A tag for when chunk has sunlight.
    public struct ChunkSunlit : IComponentData { }
}