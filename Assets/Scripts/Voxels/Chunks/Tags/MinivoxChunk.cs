using Unity.Entities;

namespace Zoxel.Voxels // .Minivoxes
{
    //! A tag for a Chunk of a Minivox Vox Entity.
    public struct MinivoxChunk : IComponentData { }
}