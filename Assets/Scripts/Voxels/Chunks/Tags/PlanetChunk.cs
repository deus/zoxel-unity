using Unity.Entities;

namespace Zoxel.Voxels
{
    //! A tag for a Chunk of a Planet Vox Entity.
    public struct PlanetChunk : IComponentData { }
}