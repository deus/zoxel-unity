using Unity.Entities;

namespace Zoxel.Voxels
{
    //! Generates data but doesn't build chunk.
	public struct ChunkDataOnly : IComponentData { }
}