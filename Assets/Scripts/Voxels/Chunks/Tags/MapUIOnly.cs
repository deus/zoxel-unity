using Unity.Entities;

namespace Zoxel.Voxels
{
    //! A chunk that is created purely for map use.
    public struct MapUIOnly : IComponentData { }
}