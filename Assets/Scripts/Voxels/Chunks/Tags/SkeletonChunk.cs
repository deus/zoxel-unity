using Unity.Entities;

namespace Zoxel.Voxels
{
    //! A tag for a Chunk of a BoneLinks Vox.
    public struct SkeletonChunk : IComponentData { }
}