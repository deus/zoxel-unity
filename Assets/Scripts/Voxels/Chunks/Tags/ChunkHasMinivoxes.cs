using Unity.Entities;

namespace Zoxel.Voxels // .Minivoxes
{
	//! Tags a chunk that has minivoxes visible.
	public struct ChunkHasMinivoxes : IComponentData { }
}