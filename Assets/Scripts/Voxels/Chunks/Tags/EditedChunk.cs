
using Unity.Entities;

namespace Zoxel.Voxels
{
    //! A Chunk that has since been edited after the original generation.
    public struct EditedChunk : IComponentData { }
}