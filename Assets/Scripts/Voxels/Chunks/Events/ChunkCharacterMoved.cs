using Unity.Entities;

namespace Zoxel.Voxels // Characters.World
{
    //! Event for when characters spawn on chunk
    public struct ChunkCharacterMoved : IComponentData
    {
        public Entity character;
        public Entity oldChunk;
        public Entity newChunk;

        public ChunkCharacterMoved(Entity character, Entity oldChunk, Entity newChunk)
        {
            this.character = character;
            this.oldChunk = oldChunk;
            this.newChunk = newChunk;
        }
    }
}