using Unity.Entities;

namespace Zoxel.Voxels
{
    //! Causes Chunk to spawn some ChunkRender entities making it visible.
    public struct ChunkBecomeInvisible : IComponentData { }
}