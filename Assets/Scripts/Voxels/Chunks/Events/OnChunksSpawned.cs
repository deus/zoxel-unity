using Unity.Entities;

namespace Zoxel.Voxels
{
    //! Links new Chunk entities to a Vox.
    public struct OnChunksSpawned : IComponentData
    {
        public int spawned;

        public OnChunksSpawned(int spawned)
        {
            this.spawned = spawned;
        }
    }
}