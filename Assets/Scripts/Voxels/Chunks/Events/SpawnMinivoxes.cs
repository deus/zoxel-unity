using Unity.Entities;

namespace Zoxel.Voxels // .Minivoxes
{
	//! Spawns minivoxes onto chunk.
	public struct SpawnMinivoxes : IComponentData { }
}