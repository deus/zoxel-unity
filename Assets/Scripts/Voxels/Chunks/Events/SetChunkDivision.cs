using Unity.Entities;

namespace Zoxel.Voxels
{
	public struct SetChunkDivision : IComponentData
	{
		public byte newDistance;

		public SetChunkDivision(byte newDistance)
		{
			this.newDistance = newDistance;
		}
	}
}