using Unity.Entities;

namespace Zoxel.Voxels // Characters.World
{
    //! Event for when characters spawn on chunk
    public struct ChunkCharacterSpawned : IComponentData
    {
        public Entity chunk;
        public byte spawned;

        public ChunkCharacterSpawned(Entity chunk, byte spawned)
        {
            this.chunk = chunk;
            this.spawned = spawned;
        }
    }
}