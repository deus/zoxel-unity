using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
    //! Removes the Chunk's of a Vox.
    /**
    *   - Destroy Children System -
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxRemoveChunksSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<OnChunksSpawned>()
                .WithAll<RemoveVoxChunks>()
                .ForEach((Entity e, int entityInQueryIndex, ref Vox vox) =>
            {
                PostUpdateCommands.RemoveComponent<RemoveVoxChunks>(entityInQueryIndex, e);
                for (int i = 0; i < vox.chunks.Length; i++)
                {
                    var chunkEntity = vox.chunks[i];
                    if (chunkEntity.Index <= 0)
                    {
                        return;
                    }
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, chunkEntity);
                }
                vox.Dispose();
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}