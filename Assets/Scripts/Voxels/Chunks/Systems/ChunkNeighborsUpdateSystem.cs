using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Voxels
{
    //! Links a chunk up to neighbor chunks.
    /**
    *   Uses UnsafeHashmap for fast operations.
    *   Note: After Stream, OnChunkSpawned is still on thhe planet entity.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
    public partial class ChunkNeighborsUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            voxesQuery = GetEntityQuery(
                ComponentType.Exclude<OnChunksSpawned>(),
                ComponentType.ReadOnly<StreamableVox>(),
                ComponentType.ReadOnly<Vox>());
            RequireForUpdate(processQuery);
            RequireForUpdate(voxesQuery);
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<UpdateChunkNeighbors>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunkLinks2 = GetComponentLookup<ChunkLinks>(true);
            // voxEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity>()
                .WithAll<UpdateChunkNeighbors>()
                .ForEach((Entity e, int entityInQueryIndex, ref ChunkNeighbors chunkNeighbors, in ChunkPosition chunkPosition, in VoxLink voxLink) =>
            {
                chunkNeighbors.ClearChunkNeighbors();
                if (!chunkLinks2.HasComponent(voxLink.vox))
                {
                    // UnityEngine.Debug.LogError("! UpdateChunkNeighbors");
                    return;
                }
                // PostUpdateCommands.RemoveComponent<UpdateChunkNeighbors>(entityInQueryIndex, e);
                var chunkPosition2 = chunkPosition.position;
                var chunks = chunkLinks2[voxLink.vox].chunks;
                var chunkPositions = chunkNeighbors.GetNeighborPositions(chunkPosition2);
                for (int i = 0; i < chunkPositions.Length; i++)
                {
                    var neighborChunkPosition = chunkPositions[i];
                    Entity chunkEntity;
                    if (chunks.TryGetValue(neighborChunkPosition, out chunkEntity))
                    {
                        chunkNeighbors.SetChunkNeighbor(chunkEntity, neighborChunkPosition, chunkPosition2);
                    }
                }
                /*if (!HasComponent<EdgeChunk>(e) && !chunkNeighbors.HasFoundNeighbors())
                {
                    UnityEngine.Debug.LogError("!chunkNeighbors.HasFoundNeighbors()");
                }*/
                if (voxEntities.Length > 0) { var e2 = voxEntities[0]; }
            })  .WithReadOnly(chunkLinks2)
                .WithReadOnly(voxEntities)
                .WithDisposeOnCompletion(voxEntities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}