﻿using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
	//! Waits for Lights to finish building, then it triggers ChunkRenders to build.
	/**
	*	Triggers Minivoxes to build lights. Triggers ChunkRenders to update.
	*/
	[BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
    public partial class ChunkBuiltSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            if (VoxelManager.instance == null) return;
			var elapsedTime = World.Time.ElapsedTime;
            var disableSmoothLighting = VoxelManager.instance.voxelSettings.disableSmoothLighting;
			var disableVoxelLighting = VoxelManager.instance.voxelSettings.disableVoxelLighting;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			if (disableVoxelLighting)
			{
				Dependency = Entities
					.WithNone<VoxelBuilderLightsOnly>()
					.WithAll<PlanetChunk, ChunkBuilderComplete>()
					.WithAll<ChunkHasMinivoxes>()
					.ForEach((Entity e, int entityInQueryIndex) =>
				{
					PostUpdateCommands.AddComponent<SpawnMinivoxes>(entityInQueryIndex, e);
				}).ScheduleParallel(Dependency);
				// commandBufferSystem.AddJobHandleForProducer(Dependency);
				Dependency = Entities
					.WithAll<PlanetChunk, ChunkBuilderComplete>()
					.WithAll<ChunkHasMinivoxes, VoxelBuilderLightsOnly>()
					.ForEach((Entity e, int entityInQueryIndex) =>
				{
					PostUpdateCommands.AddComponent<ChunkBuilderMinivoxLights>(entityInQueryIndex, e);
				}).ScheduleParallel(Dependency);
				// commandBufferSystem.AddJobHandleForProducer(Dependency);
			}
			else
			{
				Dependency = Entities
					.WithAll<ChunkHasMinivoxes, ChunkBuilderComplete>()
					.ForEach((Entity e, int entityInQueryIndex) =>
				{
					PostUpdateCommands.AddComponent<ChunkBuilderMinivoxLights>(entityInQueryIndex, e);
				}).ScheduleParallel(Dependency);
				// commandBufferSystem.AddJobHandleForProducer(Dependency);
			}

			//! Completes ChunkBuilderComplete event by remoing it.
			Dependency = Entities
				.WithAll<ChunkBuilderComplete>()
				.ForEach((Entity e, int entityInQueryIndex) =>
			{
				PostUpdateCommands.RemoveComponent<ChunkBuilderComplete>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
			// commandBufferSystem.AddJobHandleForProducer(Dependency);

			//! Removes VoxelBuilderMeshOnly after use.
			Dependency = Entities
				.WithAll<ChunkBuilderComplete, VoxelBuilderMeshOnly>()
				.ForEach((Entity e, int entityInQueryIndex) =>
			{
				PostUpdateCommands.RemoveComponent<VoxelBuilderMeshOnly>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
			// commandBufferSystem.AddJobHandleForProducer(Dependency);

			//! Removes VoxelBuilderLightsOnly after use.
			Dependency = Entities
				.WithAll<ChunkBuilderComplete, VoxelBuilderLightsOnly>()
				.ForEach((Entity e, int entityInQueryIndex) =>
			{
				PostUpdateCommands.RemoveComponent<VoxelBuilderLightsOnly>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
			// commandBufferSystem.AddJobHandleForProducer(Dependency);

			//! Removes EntityBusy after use.
			Dependency = Entities
				.WithAll<ChunkBuilderComplete, EntityBusy>()
				.ForEach((Entity e, int entityInQueryIndex) =>
			{
				PostUpdateCommands.RemoveComponent<EntityBusy>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
			// commandBufferSystem.AddJobHandleForProducer(Dependency);

			//! Adds a ChunkBuilt tag on to chunks that have built.
			Dependency = Entities
				.WithNone<ChunkBuilt>()
				.WithAll<ChunkBuilderComplete>()
				.ForEach((Entity e, int entityInQueryIndex) =>
			{
				PostUpdateCommands.AddComponent<ChunkBuilt>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
			// commandBufferSystem.AddJobHandleForProducer(Dependency);

			//! Triggers ChunkRenders to rebuild.
			Dependency = Entities
				.WithAll<PlanetChunk, ChunkBuilderComplete>()
				.ForEach((Entity e, int entityInQueryIndex, in ChunkRenderLinks chunkRenderLinks) =>
			{
				// PostUpdateCommands.RemoveComponent<ChunkBuilderComplete>(entityInQueryIndex, e);
				// handle changes of water
				// Triggers minivox building
				// Else Refresh Minivoxes if lights diabled
				// resize as part of the steps
				// finallly trigger chunk renders to  do their thing
				if (chunkRenderLinks.chunkRenders.Length == 0)
				{
					//! \todo Use Tag Empty Chunk for a chunk without renders (completely air)
					return;
				}
				var isLightsOnly = HasComponent<VoxelBuilderLightsOnly>(e);
				for (int i = 0; i < chunkRenderLinks.chunkRenders.Length; i++)
				{
					var chunkRenderEntity = chunkRenderLinks.chunkRenders[i];
					if (!HasComponent<ChunkRender>(chunkRenderEntity))
					{
						return;
					}
					if (isLightsOnly)
					{
						if (disableSmoothLighting)
						{
							PostUpdateCommands.AddComponent(entityInQueryIndex, chunkRenderEntity, new ChunkRenderBuilder(ChunkRenderBuildState.GenerateBasicLights));
						}
						else
						{
							PostUpdateCommands.AddComponent(entityInQueryIndex, chunkRenderEntity, new ChunkRenderBuilder(ChunkRenderBuildState.GenerateSmoothLights));
						}
					}
					else
					{
						PostUpdateCommands.AddComponent<ChunkRenderBuilder>(entityInQueryIndex, chunkRenderEntity);
					}
				}
				#if DEBUG_VOXEL_PLACEMENT
				UnityEngine.Debug.LogError(e.Index + " - Voxel Placement - ChunkRenderBuilder added: " + elapsedTime);
				#endif
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}