using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
	//! Removes EntityBusy from EdgeChunk after lighting is built.
    /**
    *   - End System -
    */
	/*[BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
    public partial class EdgeChunkEndSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery edgeChunkQuery;
        private EntityQuery edgeChunkVoxelBuilderLightsOnlyQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            edgeChunkQuery = GetEntityQuery(
				ComponentType.ReadOnly<PlanetChunk>(),
				ComponentType.ReadOnly<EdgeChunk>(),
				ComponentType.ReadOnly<ChunkBuilderComplete>());
            edgeChunkVoxelBuilderLightsOnlyQuery = GetEntityQuery(
				ComponentType.ReadOnly<VoxelBuilderLightsOnly>(),
				ComponentType.ReadOnly<PlanetChunk>(),
				ComponentType.ReadOnly<EdgeChunk>(),
				ComponentType.ReadOnly<ChunkBuilderComplete>());
            //RequireForUpdate(edgeChunkQuery);
            //RequireForUpdate(edgeChunkVoxelBuilderLightsOnlyQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
			var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
			if (!edgeChunkQuery.IsEmpty)
			{
				PostUpdateCommands.RemoveComponentForEntityQuery<EntityBusy>(edgeChunkQuery);
			}
			if (!edgeChunkVoxelBuilderLightsOnlyQuery.IsEmpty)
			{
				PostUpdateCommands.RemoveComponentForEntityQuery<VoxelBuilderLightsOnly>(edgeChunkVoxelBuilderLightsOnlyQuery);
			}
		}
	}*/

	//! When ChunkBuilding starts.
    /**
    *   Proceeds chunk building into building lights.
	*	If Lights are disabled, goes straight to ChunkBuilderComplete step.
    */
	[BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
    public partial class ChunkBuilderSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery chunkBuilderQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			chunkBuilderQuery = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<UpdateChunkNeighbors>(),
                ComponentType.Exclude<ChunkSpawnRenders>(),
                ComponentType.Exclude<OnChunkRendersSpawned>(),
                ComponentType.Exclude<DelayChunkBuilder>(),
                ComponentType.Exclude<EntityGenerating>(),
                ComponentType.Exclude<GenerateTrees>(),	// sometimes added during streaming updates
                ComponentType.Exclude<LoadChunk>(),
                ComponentType.ReadOnly<ChunkBuilder>(),
                ComponentType.ReadOnly<PlanetChunk>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkRenderLinks>(),
                ComponentType.ReadOnly<VoxLink>());
            RequireForUpdate(chunkBuilderQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
			var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
			var disableVoxelLighting = VoxelManager.instance.voxelSettings.disableVoxelLighting;
			PostUpdateCommands.RemoveComponent<ChunkBuilder>(chunkBuilderQuery);
			if (!disableVoxelLighting)
			{
				PostUpdateCommands.AddComponent<ChunkDarknessBuilder>(chunkBuilderQuery);
				// UnityEngine.Debug.LogError(e.Index + " - Voxel Placement - ChunkBuildState.Begin: " + elapsedTime);
			}
			else
			{
				PostUpdateCommands.AddComponent<ChunkBuilderComplete>(chunkBuilderQuery);
			}
			PostUpdateCommands.RemoveComponent<ChunkSunlit>(chunkBuilderQuery);
			// PostUpdateCommands.RemoveComponentForEntityQuery<VoxelBuilderLightsOnly>(chunkBuilderQuery);
		}
	}
}