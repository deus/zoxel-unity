using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
	//! Using DestroyEntity, disposes of ChunkWeights data.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ChunkWeightsDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
				.WithAll<DestroyEntity, ChunkWeights>()
				.ForEach((in ChunkWeights chunkWeights) =>
			{
                chunkWeights.DisposeFinal();
			}).ScheduleParallel();
		}
	}
}