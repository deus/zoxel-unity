using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
	//! Disposes of ChunkVoxelRotations on DestroyEntity event.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ChunkVoxelRotationsDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, PlanetChunk>()
                .ForEach((in ChunkVoxelRotations chunkVoxelRotations) =>
			{
                chunkVoxelRotations.Dispose();
			}).ScheduleParallel();
		}
	}
}