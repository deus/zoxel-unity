using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Voxels
{
    //! Pushes Chunk ChunkDivision changes to ChunkRender's.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ChunkRenderDivisionSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery chunkRendersQuery;

        protected override void OnCreate()
        {
            chunkRendersQuery = GetEntityQuery(
                ComponentType.ReadOnly<ChunkRender>(),
                ComponentType.ReadWrite<ChunkDivision>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var chunkRenderEntities = chunkRendersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var chunkDivisions = GetComponentLookup<ChunkDivision>(false);
            chunkRenderEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, EntityBusy, DestroyEntity>()
                .WithChangeFilter<ChunkDivision>()
                .ForEach((in ChunkRenderLinks chunkRenderLinks, in ChunkDivision chunkDivision) =>
            {
                // UnityEngine.Debug.LogError("ChunkDivision WithChangeFilter: " + chunkDivision.viewDistance);
                for (int i = 0; i < chunkRenderLinks.chunkRenders.Length; i++)
                {
                    var chunkRenderEntity = chunkRenderLinks.chunkRenders[i];
                    if (chunkDivisions.HasComponent(chunkRenderEntity))
                    {
                        chunkDivisions[chunkRenderEntity] = chunkDivision;
                    }
                }
            })  .WithNativeDisableContainerSafetyRestriction(chunkDivisions)
                .ScheduleParallel(Dependency);
        }
    }
}