using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ChunkStateDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, ChunkState>()
                .ForEach((in ChunkState chunkState) =>
			{
                chunkState.data.Dispose();
			}).ScheduleParallel();
        }
    }
}