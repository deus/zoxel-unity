using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
	//! Disposes of Chunk on DestroyEntity event.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ChunkDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
				.WithAll<DestroyEntity, Chunk>()
				.ForEach((in Chunk chunk) =>
			{
                chunk.DisposeFinal();
			}).ScheduleParallel();
		}
	}
}