using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
	//! Delays ChunkBuilder Event.
    /**
    *   - Delay System -
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ChunkBuilderDelaySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in DelayChunkBuilder delayChunkBuilder) =>
            {
                if (elapsedTime - delayChunkBuilder.timeStarted >= delayChunkBuilder.timeDelay)
                {
                    PostUpdateCommands.RemoveComponent<DelayChunkBuilder>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}