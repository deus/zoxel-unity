/*using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
	//! An event system for when a chunk changes view distance from camera.
	[BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ChunkDivisionSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref ChunkDivision chunkDivision, in SetChunkDivision setChunkDivision) =>
			{
				PostUpdateCommands.RemoveComponent<SetChunkDivision>(entityInQueryIndex, e);
				if (setChunkDivision.newDistance != chunkDivision.viewDistance)
				{
					var oldViewDistance = chunkDivision.viewDistance;
					chunkDivision.viewDistance = setChunkDivision.newDistance;
					PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChunkDivisionUpdated(oldViewDistance));
				}
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}*/
// UnityEngine.Debug.LogError("At " + chunkPosition.position + " - From view distance: " + oldDistance + " to " + chunkDivision.viewDistance);
/*if (chunkDivision.viewDistance == 0)
{
	UnityEngine.Debug.LogError("New View Distance: " + chunkDivision.viewDistance + ".");
}*/