using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Voxels
{
    //! Links spawned chunks to Vox after.
    /**
    *   - Linking System -
    *   \todo Refactor like ChildrensLinkSystem.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ChunksSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
            RequireForUpdate(chunksQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<OnChunksSpawned>(processQuery);
            PostUpdateCommands2.RemoveComponent<NewChunk>(chunksQuery);
            //! First initializes children.
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .ForEach((ref Vox vox, in OnChunksSpawned onChunksSpawned) =>
            {
                if (onChunksSpawned.spawned != 0)
                {
                    vox.SetAs(onChunksSpawned.spawned);
                }
            }).ScheduleParallel(Dependency);
            //! For each child, using the index, sets into parents children that is passed in.
            var parentEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var voxes = GetComponentLookup<Vox>(false);
            var chunkLinks = GetComponentLookup<ChunkLinks>(false);
            parentEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref chunksQuery)
                .WithNone<DestroyEntity>()
                .WithAll<NewChunk>()
                .ForEach((Entity e, in ChunkIndex chunkIndex, in VoxLink voxLink) =>
            {
                if (!voxes.HasComponent(voxLink.vox))
                {
                    /*#if UNITY_EDITOR
                    UnityEngine.Debug.LogError("Voxes not containing: " + voxLink.vox.Index);
                    #endif*/
                    return;
                }
                var vox = voxes[voxLink.vox];
                #if DATA_COUNT_CHECKS
                if (chunkIndex.index >= vox.chunks.Length)
                {
                    // UnityEngine.Debug.LogError("Index out of bounds: " + child.index + " to parent " + parentLink.parent.Index);
                    return;
                }
                #endif
                vox.chunks[chunkIndex.index] = e;
            })  .WithNativeDisableContainerSafetyRestriction(voxes)
                .ScheduleParallel(Dependency);
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<NewChunk, PlanetChunk>()
                .ForEach((Entity e, in ChunkPosition chunkPosition, in VoxLink voxLink) =>
            {
                if (chunkLinks.HasComponent(voxLink.vox))
                {
                    var chunkLinks2 = chunkLinks[voxLink.vox];
                    chunkLinks2.chunks[chunkPosition.position] = e;
                }
            })  .WithNativeDisableContainerSafetyRestriction(chunkLinks)
                .ScheduleParallel(Dependency);
        }
    }
}
                    /*if (voxLink.vox.Index != 0)
                    {
                        PostUpdateCommands.AddComponent<NotCloud>(entityInQueryIndex, voxLink.vox);
                    }*/
                    /*else
                    {
                        UnityEngine.Debug.LogError("Voxes not containing: " + voxLink.vox.Index); //  + " cloud? " + HasComponent<Cloud>(voxLinkvox));
                    }*/
            
/*var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
var chunkPositions = GetComponentLookup<ChunkPosition>(true);
var voxLinks = GetComponentLookup<VoxLink>(true);
Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref Vox vox, ref ChunkLinks chunkLinks, ref OnChunksSpawned onChunksSpawned) =>
{
    if (onChunksSpawned.spawned != 0)
    {
        vox.SetAs(onChunksSpawned.spawned);
        onChunksSpawned.spawned = 0;
    }
    if (vox.chunks.Length == 0)
    {
        PostUpdateCommands.RemoveComponent<OnChunksSpawned>(entityInQueryIndex, e);
        return;
    }
    var count = 0;
    for (int i = 0; i < chunkEntities.Length; i++)
    {
        var e2 = chunkEntities[i];
        var voxLink = voxLinks[e2];
        if (voxLink.vox == e)
        {
            vox.chunks[count] = e2;
            var chunkPosition = chunkPositions[e2].position;
            chunkLinks.chunks[chunkPosition] = e2;
            // UnityEngine.Debug.LogError("chunkPosition: " + chunkPosition);
            count++;
            if (count >= vox.chunks.Length)
            {
                break;
            }
        }
    }
    if (count == vox.chunks.Length)
    {
        PostUpdateCommands.RemoveComponent<OnChunksSpawned>(entityInQueryIndex, e);
    }
})  .WithReadOnly(chunkEntities)
    .WithDisposeOnCompletion(chunkEntities)
    .WithReadOnly(voxLinks)
    .WithReadOnly(chunkPositions)
    .ScheduleParallel(Dependency);
// commandBufferSystem.AddJobHandleForProducer(Dependency);*/