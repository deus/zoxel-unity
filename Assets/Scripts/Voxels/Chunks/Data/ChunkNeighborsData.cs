using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Holds Chunk Neighbor Data. Used for quick access to surrounding voxels.
	public struct ChunkNeighborsData
	{
		public readonly Chunk chunkLeft;
		public readonly Chunk chunkRight;
		public readonly Chunk chunkDown;
		public readonly Chunk chunkUp;
		public readonly Chunk chunkBack;
		public readonly Chunk chunkForward;
		// normal diagnols
		public readonly Chunk chunkForwardLeft;
		public readonly Chunk chunkForwardRight;
		public readonly Chunk chunkBackwardLeft;
		public readonly Chunk chunkBackwardRight;
		// Vertical Directionals
		public readonly Chunk chunkUpLeft;
		public readonly Chunk chunkUpRight;
		public readonly Chunk chunkUpBackward;
		public readonly Chunk chunkUpForward;
		public readonly Chunk chunkDownLeft;
		public readonly Chunk chunkDownRight;
		public readonly Chunk chunkDownBackward;
		public readonly Chunk chunkDownForward;
		// top diagnols
		public readonly Chunk chunkForwardLeftUp;
		public readonly Chunk chunkForwardRightUp;
		public readonly Chunk chunkBackwardLeftUp;
		public readonly Chunk chunkBackwardRightUp;
		public readonly Chunk chunkForwardLeftDown;
		public readonly Chunk chunkForwardRightDown;
		public readonly Chunk chunkBackwardLeftDown;
		public readonly Chunk chunkBackwardRightDown;

		public ChunkNeighborsData(in ChunkNeighbors chunkNeighbors, in ComponentLookup<Chunk> chunks)
		{
			chunkLeft = chunks[chunkNeighbors.chunkLeft];
			chunkRight = chunks[chunkNeighbors.chunkRight];
			chunkDown = chunks[chunkNeighbors.chunkDown];
			chunkUp = chunks[chunkNeighbors.chunkUp];
			chunkBack = chunks[chunkNeighbors.chunkBack];
			chunkForward = chunks[chunkNeighbors.chunkForward];
			// Diagnols
			chunkForwardLeft = chunks[chunkNeighbors.chunkForwardLeft];
			chunkForwardRight = chunks[chunkNeighbors.chunkForwardRight];
			chunkBackwardLeft = chunks[chunkNeighbors.chunkBackwardLeft];
			chunkBackwardRight = chunks[chunkNeighbors.chunkBackwardRight];
			// D2
			chunkUpLeft = chunks[chunkNeighbors.chunkUpLeft];
			chunkUpRight = chunks[chunkNeighbors.chunkUpRight];
			chunkUpBackward = chunks[chunkNeighbors.chunkUpBackward];
			chunkUpForward = chunks[chunkNeighbors.chunkUpForward];
			chunkDownLeft = chunks[chunkNeighbors.chunkDownLeft];
			chunkDownRight = chunks[chunkNeighbors.chunkDownRight];
			chunkDownBackward = chunks[chunkNeighbors.chunkDownBackward];
			chunkDownForward = chunks[chunkNeighbors.chunkDownForward];
			// top diagnols
			chunkForwardLeftUp = chunks[chunkNeighbors.chunkForwardLeftUp];
			chunkForwardRightUp = chunks[chunkNeighbors.chunkForwardRightUp];
			chunkBackwardLeftUp = chunks[chunkNeighbors.chunkBackwardLeftUp];
			chunkBackwardRightUp = chunks[chunkNeighbors.chunkBackwardRightUp];
			chunkForwardLeftDown = chunks[chunkNeighbors.chunkForwardLeftDown];
			chunkForwardRightDown = chunks[chunkNeighbors.chunkForwardRightDown];
			chunkBackwardLeftDown = chunks[chunkNeighbors.chunkBackwardLeftDown];
			chunkBackwardRightDown = chunks[chunkNeighbors.chunkBackwardRightDown];
		}

		//! Returns the voxel value at a location
		public byte GetVoxelValue(in Chunk chunk, int3 localPosition, int3 voxelDimensions)
		{
			var chunk2 = GetChunkAdjacent(in chunk, ref localPosition, voxelDimensions);
			if (chunk2.voxels.Length == 0)
			{
				return 1;
			}
			return chunk2.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)];
		}

		public Chunk GetChunkAdjacent(in Chunk chunk, ref int3 localPosition, int3 voxelDimensions)
		{
			if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
			{
				return chunk;
			}
			else if (localPosition.x == -1)
			{
				localPosition.x += voxelDimensions.x;
				return chunkLeft;
			}
			else if (localPosition.x == voxelDimensions.x)
			{
				localPosition.x -= voxelDimensions.x;
				return chunkRight;
			}
			else if (localPosition.y == -1)
			{
				localPosition.y += voxelDimensions.y;
				return chunkDown;
			}
			else if (localPosition.y == voxelDimensions.y)
			{
				localPosition.y -= voxelDimensions.y;
				return chunkUp;
			}
			else if (localPosition.z == -1)
			{
				localPosition.z += voxelDimensions.z;
				return chunkBack;
			}
			else if (localPosition.z == voxelDimensions.z)
			{
				localPosition.z -= voxelDimensions.z;
				return chunkForward;
			}
			else
			{
				return new Chunk();
			}
		}
	}
}