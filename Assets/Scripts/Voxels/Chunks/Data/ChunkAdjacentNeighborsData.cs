using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Holds Chunk Neighbor Data. Used for quick access to surrounding voxels.
	public struct ChunkAdjacentNeighborsData
	{
		public readonly Chunk chunkLeft;
		public readonly Chunk chunkRight;
		public readonly Chunk chunkDown;
		public readonly Chunk chunkUp;
		public readonly Chunk chunkBack;
		public readonly Chunk chunkForward;

		public ChunkAdjacentNeighborsData(in ChunkNeighbors chunkNeighbors, in ComponentLookup<Chunk> chunks)
		{
			if (chunkNeighbors.chunkLeft.Index > 0)
			{
				chunkLeft = chunks[chunkNeighbors.chunkLeft];
			}
			else
			{
				chunkLeft = new Chunk();
			}
			if (chunkNeighbors.chunkRight.Index > 0)
			{
				chunkRight = chunks[chunkNeighbors.chunkRight];
			}
			else
			{
				chunkRight = new Chunk();
			}
			if (chunkNeighbors.chunkDown.Index > 0)
			{
				chunkDown = chunks[chunkNeighbors.chunkDown];
			}
			else
			{
				chunkDown = new Chunk();
			}
			if (chunkNeighbors.chunkUp.Index > 0)
			{
				chunkUp = chunks[chunkNeighbors.chunkUp];
			}
			else
			{
				chunkUp = new Chunk();
			}
			if (chunkNeighbors.chunkBack.Index > 0)
			{
				chunkBack = chunks[chunkNeighbors.chunkBack];
			}
			else
			{
				chunkBack = new Chunk();
			}
			if (chunkNeighbors.chunkForward.Index > 0)
			{
				chunkForward = chunks[chunkNeighbors.chunkForward];
			}
			else
			{
				chunkForward = new Chunk();
			}
		}

		public bool GetVoxelValue(in Chunk chunk, int3 localPosition, int3 voxelDimensions, out byte voxelIndex)
		{
			var chunk2 = GetChunkAdjacent(in chunk, ref localPosition, voxelDimensions);
			if (chunk2.voxels.Length == 0)
			{
				voxelIndex = 0;
				return false;
			}
			voxelIndex = chunk2.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)];
			return true;
		}

		//! Returns the voxel value at a location
		public byte GetVoxelValue(in Chunk chunk, int3 localPosition, int3 voxelDimensions)
		{
			var chunk2 = GetChunkAdjacent(in chunk, ref localPosition, voxelDimensions);
			if (chunk2.voxels.Length == 0)
			{
				return 1;
			}
			return chunk2.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)];
		}

		public Chunk GetChunkAdjacent(in Chunk chunk, ref int3 localPosition, int3 voxelDimensions)
		{
			if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
			{
				return chunk;
			}
			else if (localPosition.x == -1)
			{
				localPosition.x += voxelDimensions.x;
				return chunkLeft;
			}
			else if (localPosition.x == voxelDimensions.x)
			{
				localPosition.x -= voxelDimensions.x;
				return chunkRight;
			}
			else if (localPosition.y == -1)
			{
				localPosition.y += voxelDimensions.y;
				return chunkDown;
			}
			else if (localPosition.y == voxelDimensions.y)
			{
				localPosition.y -= voxelDimensions.y;
				return chunkUp;
			}
			else if (localPosition.z == -1)
			{
				localPosition.z += voxelDimensions.z;
				return chunkBack;
			}
			else if (localPosition.z == voxelDimensions.z)
			{
				localPosition.z -= voxelDimensions.z;
				return chunkForward;
			}
			else
			{
				return new Chunk();
			}
		}
	}
}