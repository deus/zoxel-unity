namespace Zoxel.Voxels
{
    // The state of ChunkRenderBuilder
    public enum ChunkRenderBuildState : byte
    {
        CullSides,
        GenerateMesh,
        CalculateBounds,    // Calculates Mesh Bounds by using Min Max
        GenerateSmoothLights,     // Generates mesh vertex lights
        GenerateBasicLights,     // Generates mesh vertex lights

        // BoneLinks
        BakeWeights,        // Only for Skeletons
        BakeColorWeights,   // Only for Debugging Skeletons
        CentreMesh,         // Only done for Models
        
        CompletedChunkRenderBuilder           // finishes it all up.
    }
}

// ChunkLightsBuilderState Systems are:
//      - MapChunkRenderSideCullingSystem
//      - MapChunkMeshBuilderSystem
//      - CalculateBoundsSystem
//      - ChunkMeshLightBuilderSystem
//      - ChunkRenderBuilderCompleteSystem