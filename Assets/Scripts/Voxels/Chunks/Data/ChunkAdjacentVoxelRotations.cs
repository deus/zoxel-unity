using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Holds ChunkVoxelRotations Neighbor Data. Used for quick access to surrounding voxels.
	public struct ChunkAdjacentVoxelRotations
	{
		public readonly ChunkVoxelRotations chunkLeft;
		public readonly ChunkVoxelRotations chunkRight;
		public readonly ChunkVoxelRotations chunkDown;
		public readonly ChunkVoxelRotations chunkUp;
		public readonly ChunkVoxelRotations chunkBack;
		public readonly ChunkVoxelRotations chunkForward;

		public ChunkAdjacentVoxelRotations(in ChunkNeighbors chunkNeighbors, in ComponentLookup<ChunkVoxelRotations> chunkVoxelRotations)
		{
			if (chunkNeighbors.chunkLeft.Index > 0)
			{
				chunkLeft = chunkVoxelRotations[chunkNeighbors.chunkLeft];
			}
			else
			{
				chunkLeft = new ChunkVoxelRotations();
			}
			if (chunkNeighbors.chunkRight.Index > 0)
			{
				chunkRight = chunkVoxelRotations[chunkNeighbors.chunkRight];
			}
			else
			{
				chunkRight = new ChunkVoxelRotations();
			}
			if (chunkNeighbors.chunkDown.Index > 0)
			{
				chunkDown = chunkVoxelRotations[chunkNeighbors.chunkDown];
			}
			else
			{
				chunkDown = new ChunkVoxelRotations();
			}
			if (chunkNeighbors.chunkUp.Index > 0)
			{
				chunkUp = chunkVoxelRotations[chunkNeighbors.chunkUp];
			}
			else
			{
				chunkUp = new ChunkVoxelRotations();
			}
			if (chunkNeighbors.chunkBack.Index > 0)
			{
				chunkBack = chunkVoxelRotations[chunkNeighbors.chunkBack];
			}
			else
			{
				chunkBack = new ChunkVoxelRotations();
			}
			if (chunkNeighbors.chunkForward.Index > 0)
			{
				chunkForward = chunkVoxelRotations[chunkNeighbors.chunkForward];
			}
			else
			{
				chunkForward = new ChunkVoxelRotations();
			}
		}

		/*public bool GetVoxelValue(in ChunkVoxelRotations chunk, int3 localPosition, int3 voxelDimensions, out byte voxelIndex)
		{
			var chunk2 = GetChunkAdjacent(in chunk, ref localPosition, voxelDimensions);
			if (chunk2.voxels.Length == 0)
			{
				voxelIndex = 0;
				return false;
			}
			voxelIndex = chunk2.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)];
			return true;
		}*/

		//! Returns the voxel value at a location
		public byte GetRotation(in ChunkVoxelRotations chunk, int3 localPosition, int3 voxelDimensions)
		{
			var chunk2 = GetChunkAdjacent(in chunk, ref localPosition, voxelDimensions);
			return chunk2.GetRotation(localPosition);
		}

		public ChunkVoxelRotations GetChunkAdjacent(in ChunkVoxelRotations chunk, ref int3 localPosition, int3 voxelDimensions)
		{
			if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
			{
				return chunk;
			}
			else if (localPosition.x == -1)
			{
				localPosition.x += voxelDimensions.x;
				return chunkLeft;
			}
			else if (localPosition.x == voxelDimensions.x)
			{
				localPosition.x -= voxelDimensions.x;
				return chunkRight;
			}
			else if (localPosition.y == -1)
			{
				localPosition.y += voxelDimensions.y;
				return chunkDown;
			}
			else if (localPosition.y == voxelDimensions.y)
			{
				localPosition.y -= voxelDimensions.y;
				return chunkUp;
			}
			else if (localPosition.z == -1)
			{
				localPosition.z += voxelDimensions.z;
				return chunkBack;
			}
			else if (localPosition.z == voxelDimensions.z)
			{
				localPosition.z -= voxelDimensions.z;
				return chunkForward;
			}
			else
			{
				return new ChunkVoxelRotations();
			}
		}
	}
}