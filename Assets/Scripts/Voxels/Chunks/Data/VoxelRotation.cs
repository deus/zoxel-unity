using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Stores a position int3 and rotation byte.
	public struct VoxelRotation // : IComponentData
	{
		public byte3 position;
		public byte rotation;

		public VoxelRotation(byte3 position, byte rotation)
		{
			this.position = position;
			this.rotation = rotation;
		}
	}
}