namespace Zoxel.Voxels
{
    public enum VoxelPlaceFlags
    {
        MapLoading,
        SolidAdded,
        MinivoxAdded,       // could also change light?
        LightOnly
    }
}