using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Textures;
using Zoxel.Textures.Tilemaps;

namespace Zoxel.Voxels
{
    //! Creates a mesh and sets materials for new PlanetRender's.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class PlanetChunkRenderInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;
        private EntityQuery voxesQuery;
        private EntityQuery materialsQuery;
        private NativeArray<int> textureLowerDistances;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			chunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<PlanetChunk>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkDivision>());
            voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxScale>());
			materialsQuery = GetEntityQuery(
                ComponentType.ReadOnly<MaterialData>(),
                ComponentType.ReadOnly<TilemapLinks>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializeLowerDistances();
            var textureLowerDistances = this.textureLowerDistances;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            var chunkDivisions = GetComponentLookup<ChunkDivision>(true);
            chunkEntities.Dispose();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunkDimensions = GetComponentLookup<ChunkDimensions>(true);
            var voxScales = GetComponentLookup<VoxScale>(true);
            voxEntities.Dispose();
			Dependency = Entities
                .WithAll<InitializeEntity, PlanetRender>()
                .ForEach((ref ZoxMatrix zoxMatrix, ref VoxLink voxLink, ref ChunkPosition chunkPosition, ref ChunkSides chunkSides, in ChunkLink chunkLink) =>
			{
                var voxLink2 = voxLinks[chunkLink.chunk];
                var voxScale = voxScales[voxLink2.vox].scale;
                var voxelDimensions = chunkDimensions[voxLink2.vox].voxelDimensions;
                chunkPosition.position = chunkPositions[chunkLink.chunk].position;
                zoxMatrix.Set(chunkPosition.position, voxelDimensions, voxScale);
                chunkSides.Initialize(voxelDimensions);
                voxLink.vox = voxLink2.vox;
            })  .WithReadOnly(chunkDimensions)
                .WithReadOnly(voxScales)
                .WithReadOnly(chunkPositions)
                .WithReadOnly(voxLinks)
                .WithNativeDisableContainerSafetyRestriction(chunkPositions)
                .WithNativeDisableContainerSafetyRestriction(voxLinks)
                .ScheduleParallel(Dependency);
            var materialEntities = materialsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var materialDatas = GetComponentLookup<MaterialData>(true);
            var tilemapLinks = GetComponentLookup<TilemapLinks>(true);
            materialEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<InitializeEntity, PlanetRender>()
                .ForEach((ref MaterialIndex materialIndex, ref ChunkDivision chunkDivision, in ChunkLink chunkLink, in MaterialLink materialLink) =>
            {
                if (materialLink.material.Index == 0)
                {
                    return;
                }
                var chunkDivision2 = chunkDivisions[chunkLink.chunk];
                chunkDivision.viewDistance = chunkDivision2.viewDistance;
                var materialData = materialDatas[materialLink.material];
                if (materialData.resolutions != 1)
                {
                    materialIndex.index = (byte) (materialData.resolutions - 1);
                    for (byte i = 0; i < textureLowerDistances.Length; i++)
                    {
                        if (chunkDivision.viewDistance <= textureLowerDistances[i])
                        {
                            materialIndex.index = i;
                            break;
                        }
                    }
                }
            })  .WithReadOnly(chunkDivisions)
                .WithReadOnly(materialDatas)
                .WithReadOnly(textureLowerDistances)
                .WithNativeDisableContainerSafetyRestriction(chunkDivisions)
                .ScheduleParallel();
            Entities
                .WithAll<InitializeEntity, ChunkRender, PlanetRender>()
                .ForEach((Entity e, in MaterialLink materialLink) =>
            {
                if (materialLink.material.Index == 0)
                {
                    // UnityEngine.Debug.LogWarning("entityMaterials.materials is null.");
                    PostUpdateCommands.SetSharedComponentManaged(e, new ZoxMesh
                    {
                        mesh = new UnityEngine.Mesh()
                    });
                    return;
                }
                // set mat erial by tilemap
                var tilemapLinks2 = tilemapLinks[materialLink.material];
                // UnityEngine.Debug.LogError("tilemapLinks2.tilemaps.Length: " + tilemapLinks2.tilemaps.Length);
                var materials = new UnityEngine.Material[tilemapLinks2.tilemaps.Length];
                //! \todo Store Tilemap Materials all in one shared component data
                for (int i = 0; i < materials.Length; i++)
                {
                    var tilemapEntity = tilemapLinks2.tilemaps[i];    // materialIndex.index
                    materials[i] = EntityManager.GetSharedComponentManaged<UnityMaterial>(tilemapEntity).material;
                }
                PostUpdateCommands.SetSharedComponentManaged(e, new ZoxMaterials(materials));
                PostUpdateCommands.SetSharedComponentManaged(e, new ZoxMeshOnly(new UnityEngine.Mesh()));
            })  .WithReadOnly(tilemapLinks)
                .WithoutBurst().Run();
        }

        private void InitializeLowerDistances()
        {
            if (textureLowerDistances.Length == 0)
            {
			    var textureLowerDistances2 = VoxelManager.instance.voxelSettings.textureLowerDistances;
                textureLowerDistances = new NativeArray<int>(textureLowerDistances2.Length, Allocator.Persistent);
                for (int i = 0; i < textureLowerDistances2.Length; i++)
                {
                    textureLowerDistances[i] = textureLowerDistances2[i];
                }
            }
        }

        protected override void OnDestroy()
        {
            if (textureLowerDistances.Length > 0)
            {
                textureLowerDistances.Dispose();
            }
        }
    }
}