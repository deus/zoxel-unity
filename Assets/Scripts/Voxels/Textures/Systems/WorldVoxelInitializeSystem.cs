using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Textures;
using Zoxel.Textures.Tilemaps;

namespace Zoxel.Voxels
{
    //! Initializes world voxels.
    /**
    *   - Initialize System -
    *   Uses tilemap data to set the voxel tile.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class WorldVoxelInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxelsQuery;
        private EntityQuery materialsQuery;
        private EntityQuery tilemapsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			voxelsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Voxel>(),
                ComponentType.ReadOnly<MaterialLink>());
			materialsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UnityMaterial>(),
                ComponentType.ReadOnly<TilemapLinks>());
			tilemapsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Tilemap>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var voxelEntities = voxelsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var voxelUVMaps = GetComponentLookup<VoxelUVMap>(true);
			var materialLinks = GetComponentLookup<MaterialLink>(true);
            voxelEntities.Dispose();
            var materialEntities = materialsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var tilemapLinks = GetComponentLookup<TilemapLinks>(true);
            materialEntities.Dispose();
            var tilemapEntities = tilemapsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var tilemaps = GetComponentLookup<Tilemap>(true);
            tilemapEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<InitializeEntity, VoxelTileIndex>()
                .ForEach((Entity e, ref VoxelTileIndex voxelTileIndex, in VoxelLink voxelLink) =>
            {
                if (!voxelUVMaps.HasComponent(voxelLink.voxel))
                {
                    // UnityEngine.Debug.LogError("VoxelUVMap Not found on voxel: " + voxelLink.voxel);
                    return;
                }
                var voxelUVs = voxelUVMaps[voxelLink.voxel];
                if (voxelUVs.uvs.Length == 0)
                {
                    return;
                }
                int tilemapSize = 1;
                var materialEntity = materialLinks[voxelLink.voxel].material;
                var tilemapEntity = tilemapLinks[materialEntity].tilemaps[0];
                var tilemap = tilemaps[tilemapEntity];
                tilemapSize = tilemap.gridSize;
                var uvLength = 1 / ((float) tilemapSize);
                var uver = voxelUVs.uvs[0] + uvLength / 2f;
                voxelTileIndex.index = (int)((uver.x / uvLength) + ((int)(uver.y / uvLength)) * (tilemapSize)) - 1;
           })   .WithReadOnly(voxelUVMaps)
                .WithReadOnly(materialLinks)
                .WithReadOnly(tilemapLinks)
                .WithReadOnly(tilemaps)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}