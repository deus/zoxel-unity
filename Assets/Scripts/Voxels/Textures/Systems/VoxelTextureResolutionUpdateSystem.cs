using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Rendering;
using Zoxel.Textures;

namespace Zoxel.Voxels
{
    //! Changes chunk texture resolution with MaterialIndex component.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxelTextureResolutionUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery materialsQuery;
        private NativeArray<int> textureLowerDistances;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            materialsQuery = GetEntityQuery(
                ComponentType.ReadOnly<MaterialData>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(materialsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializeLowerDistances();
            var textureLowerDistances = this.textureLowerDistances;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var materialEntities = materialsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var materialDatas = GetComponentLookup<MaterialData>(true);
            materialEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithChangeFilter<ChunkDivision>()
                .WithNone<DestroyEntity>()
				.WithAll<ChunkRender>()
                .ForEach((ref MaterialIndex materialIndex, in MaterialLink materialLink, in ChunkDivision chunkDivision) =>
            {
                if (!materialDatas.HasComponent(materialLink.material))
                {
                    return;
                }
                var materialData = materialDatas[materialLink.material];
                if (materialData.resolutions <= 1)
                {
                    return;
                }
                var newIndex = (byte) (materialData.resolutions - 1);
                for (byte i = 0; i < textureLowerDistances.Length; i++)
                {
                    if (chunkDivision.viewDistance <= textureLowerDistances[i])
                    {
                        newIndex = i;
                        break;
                    }
                }
                if (materialIndex.index == newIndex)
                {
                    return;
                }
                materialIndex.index = newIndex;
                // UnityEngine.Debug.LogError("TilemapIndex is: " + newIndex + " for " + chunkDivision.viewDistance);
            })  .WithReadOnly(textureLowerDistances)
                .WithReadOnly(materialDatas)
                .ScheduleParallel(Dependency);
        }

        private void InitializeLowerDistances()
        {
            if (textureLowerDistances.Length == 0)
            {
			    var textureLowerDistances2 = VoxelManager.instance.voxelSettings.textureLowerDistances;
                textureLowerDistances = new NativeArray<int>(textureLowerDistances2.Length, Allocator.Persistent);
                for (int i = 0; i < textureLowerDistances2.Length; i++)
                {
                    textureLowerDistances[i] = textureLowerDistances2[i];
                }
            }
        }

        protected override void OnDestroy()
        {
            if (textureLowerDistances.Length > 0)
            {
                textureLowerDistances.Dispose();
            }
        }
    }
}