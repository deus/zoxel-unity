using Unity.Entities;

namespace Zoxel.Voxels
{
    public struct OnChunkRendersSpawned : IComponentData
    {
        public byte spawned;

        public OnChunkRendersSpawned(byte spawned)
        {
            this.spawned = spawned;
        }
    }
}