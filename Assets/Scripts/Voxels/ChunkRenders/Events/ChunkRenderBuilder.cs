using Unity.Entities;

namespace Zoxel.Voxels
{
    public struct ChunkRenderBuilder : IComponentData
    {
        public byte chunkRenderBuildState;

        public ChunkRenderBuilder(ChunkRenderBuildState state)
        {
            this.chunkRenderBuildState = (byte) state;
        }
    }
}