using Unity.Entities;

namespace Zoxel.Voxels
{
    //! An event to spawn render entities from a chunk.
    public struct ChunkSpawnRenders : IComponentData { }
}