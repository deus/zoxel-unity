using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels
{
	//! Links a Chunk to the ChunkRender entities.
	public struct ChunkRenderLinks : IComponentData
	{
		public BlitableArray<Entity> chunkRenders;

		public void Dispose()
		{
			if (chunkRenders.Length > 0)
			{
				chunkRenders.Dispose();
			}
		}

        public void SetAs(byte newCount)
        {
			if (chunkRenders.Length != newCount)
			{
				Dispose();
				chunkRenders = new BlitableArray<Entity>(newCount, Allocator.Persistent);
				for (int i = 0; i < newCount; i++)
				{
					chunkRenders[i] = new Entity();
				}
			}
        }
	}
}