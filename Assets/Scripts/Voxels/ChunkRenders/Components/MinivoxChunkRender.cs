using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Voxels // .Minivoxes
{
    //! Used for a ChunkRender that is attached to a MinivoxChunk.
    /**
    *   \todo Seperate out lastColor into a new component.
    *   \todo Consolidate ChunkRenders prefabs. Atm they are a little too unique.
    */
    public struct MinivoxRender : IComponentData
    {
        public float4 lastColor;
    }
}