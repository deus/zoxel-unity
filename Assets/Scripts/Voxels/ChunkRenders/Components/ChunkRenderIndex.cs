using Unity.Entities;

namespace Zoxel.Voxels
{
	//! An array index for a chunk render.
	public struct ChunkRenderIndex : IComponentData
    {
        public byte index;

        public ChunkRenderIndex(byte index)
        {
            this.index = index;
        }
    }
}