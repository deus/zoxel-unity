using Unity.Entities;

namespace Zoxel.Voxels
{
    //! Added to a ChunkRender with Translucent materials.
    public struct TranslucentRender : IComponentData { }
}