﻿using Unity.Entities;
// each voxel entity must have:
//		- VoxelUVMap
//		- Mesh Index
//		- Textures
// Then for the Tilemap i need to:
//		- Bake textures of all worlds voxels into the tilemap
//		- set it to the material

namespace Zoxel.Voxels
{
	//! A tag for a ChunkRender entity.
	public struct ChunkRender : IComponentData { }
}