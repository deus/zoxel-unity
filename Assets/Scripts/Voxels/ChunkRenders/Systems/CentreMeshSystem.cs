using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;

namespace Zoxel.Voxels
{
    //! Centers a mesh by adding half the bounds to the vertex positions.
    [UpdateAfter(typeof(CalculateBoundsSystem))]
	[BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
	public partial class CentreMeshSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;

        protected override void OnCreate()
        {
			voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxScale>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
		protected override void OnUpdate()
		{
            var degreesToRadians = ((math.PI * 2) / 360f);
            var flipRotation = quaternion.EulerXYZ(new float3(180, 0, 180) * degreesToRadians);
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var voxScales = GetComponentLookup<VoxScale>(true);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            voxEntities.Dispose();
			Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<PlanetRender>()
                .ForEach((Entity e, ref ModelMesh modelMesh, ref ChunkRenderBuilder chunkRenderBuilder, in ChunkRender chunkRender, in VoxLink voxLink) =>
			{
				if (chunkRenderBuilder.chunkRenderBuildState != (byte) ChunkRenderBuildState.CentreMesh)
                {
                    return;
                }
                if (HasComponent<SkeletonWeights>(e))
                {
                    chunkRenderBuilder.chunkRenderBuildState = (byte) ChunkRenderBuildState.BakeWeights;
                }
                else
                {
                    chunkRenderBuilder.chunkRenderBuildState = (byte) ChunkRenderBuildState.CompletedChunkRenderBuilder;
                }
                var meshPosition = modelMesh.position;
                if (HasComponent<MinivoxBounds>(e))
                {
                    var planetVox = voxLinks[voxLink.vox].vox;
                    var voxScale = voxScales[planetVox].scale;
                    meshPosition.y = voxScale.y / 2f;
                }
                for (int i = 0; i < modelMesh.vertices.Length; i++)
                {
                    var vertex = modelMesh.vertices[i];
                    var position = vertex.position;
                    position -= meshPosition;
                    position = math.rotate(flipRotation, position);
                    vertex.position = position;
                    vertex.normal = math.rotate(flipRotation, vertex.normal);
                    modelMesh.vertices[i] = vertex;
                }
			})  .WithReadOnly(voxScales)
                .WithReadOnly(voxLinks)
                .ScheduleParallel();
        }
	}
}