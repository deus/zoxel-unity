using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Voxels
{
    //! Links chunk renders to chunk after they spawn.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ChunkRendersSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunkRenderQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<OnChunkRendersSpawned>(processQuery);
            PostUpdateCommands2.RemoveComponent<NewChunkRender>(chunkRenderQuery);
            //! First initializes children.
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((ref ChunkRenderLinks chunkRenderLinks, in OnChunkRendersSpawned onChunkRendersSpawned) =>
            {
                if (onChunkRendersSpawned.spawned != 0)
                {
                    chunkRenderLinks.SetAs(onChunkRendersSpawned.spawned);
                }
            }).ScheduleParallel(Dependency);
            //! For each child, using the index, sets into parents children that is passed in.
            var parentEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var chunkRenderLinks = GetComponentLookup<ChunkRenderLinks>(false);
            // parentEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref chunkRenderQuery)
                .WithNone<DestroyEntity>()
                .WithAll<NewChunkRender>()
                .ForEach((Entity e, in ChunkRenderIndex chunkRenderIndex, in ChunkLink chunkLink) =>
            {
                var chunkRenderLinks2 = chunkRenderLinks[chunkLink.chunk];
                #if DATA_COUNT_CHECKS
                if (chunkRenderIndex.index >= chunkRenderLinks2.chunkRenders.Length)
                {
                    // UnityEngine.Debug.LogError("Index out of bounds: " + child.index + " to parent " + parentLink.parent.Index);
                    return;
                }
                #endif
                chunkRenderLinks2.chunkRenders[chunkRenderIndex.index] = e;
                if (parentEntities.Length > 0) { var e2 = parentEntities[0]; }
            })  .WithNativeDisableContainerSafetyRestriction(chunkRenderLinks)
                .WithReadOnly(parentEntities)
                .WithDisposeOnCompletion(parentEntities)
                .ScheduleParallel(Dependency);
        }
    }
}