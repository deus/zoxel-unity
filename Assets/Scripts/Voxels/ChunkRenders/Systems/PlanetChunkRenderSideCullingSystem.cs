﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Rendering;

namespace Zoxel.Voxels
{
	//! Culls ChunkRender sides used in generating voxel meshes.
	/**
	*	\todo Test out second diffuse material. This material should have it's own mesh culling made.
	*		I think the logic atm, it will render diffuse voxels onto both meshes.
	*/
	[UpdateAfter(typeof(ChunkBuiltSystem))]
    [BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
    public partial class PlanetChunkRenderSideCullingSystem : SystemBase
	{
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;
        private EntityQuery voxesQuery;
        private EntityQuery voxelQuery;
        private EntityQuery materialsQuery;

        protected override void OnCreate()
        {
            voxesQuery = GetEntityQuery(
				ComponentType.ReadOnly<StreamableVox>(),
				ComponentType.ReadOnly<Vox>(),
				ComponentType.Exclude<DestroyEntity>());
            chunksQuery = GetEntityQuery(
				ComponentType.ReadOnly<Chunk>(),
				ComponentType.ReadOnly<ChunkNeighbors>(),
				ComponentType.Exclude<DestroyEntity>());
            voxelQuery = GetEntityQuery(
				ComponentType.ReadOnly<Voxel>(),
				ComponentType.Exclude<DestroyEntity>());
            materialsQuery = GetEntityQuery(
				ComponentType.ReadOnly<MaterialData>(),
				ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

		[BurstCompile]
        protected override void OnUpdate()
        {
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var voxelLinks = GetComponentLookup<VoxelLinks>(true);
			voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunks = GetComponentLookup<Chunk>(true);
            var chunkNeighbors2 = GetComponentLookup<ChunkNeighbors>(true);
			chunkEntities.Dispose();
            var voxelMaterialEntities2 = materialsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var materialDatas = GetComponentLookup<MaterialData>(true);
			voxelMaterialEntities2.Dispose();
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var voxelMeshTypes = GetComponentLookup<VoxelMeshType>(true);
            var materialLinks = GetComponentLookup<MaterialLink>(true);
			voxelEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<InitializeEntity, ResizeChunkRender>()
				.WithAll<PlanetRender>()
				.ForEach((Entity e, ref ChunkRenderBuilder chunkRenderBuilder, ref ChunkSides chunkSides, in MaterialLink materialLink, in VoxLink voxLink, in ChunkLink chunkLink) =>
			{
				if (chunkRenderBuilder.chunkRenderBuildState != (byte) ChunkRenderBuildState.CullSides
					|| !voxelLinks.HasComponent(voxLink.vox) || !chunks.HasComponent(chunkLink.chunk) || !chunkNeighbors2.HasComponent(chunkLink.chunk))
				{
					return;
				}
				var chunkNeighbors = chunkNeighbors2[chunkLink.chunk];
				if (!chunks.HasComponent(chunkNeighbors.chunkDown) || !chunks.HasComponent(chunkNeighbors.chunkUp)
					|| !chunks.HasComponent(chunkNeighbors.chunkBack) || !chunks.HasComponent(chunkNeighbors.chunkForward)
					|| !chunks.HasComponent(chunkNeighbors.chunkLeft) || !chunks.HasComponent(chunkNeighbors.chunkRight))
				{
					return;
				}
				var worldVoxelEntities = voxelLinks[voxLink.vox].voxels;
				for (int i = 0; i < worldVoxelEntities.Length; i++)
				{
					var voxelEntity = worldVoxelEntities[i];
					if (!voxelMeshTypes.HasComponent(voxelEntity))
					{
						return;
					}
				}
				chunkRenderBuilder.chunkRenderBuildState = (byte) ChunkRenderBuildState.GenerateMesh;
				if (!materialDatas.HasComponent(materialLink.material))
				{
					return;
				}
				var chunkMaterialData = materialDatas[materialLink.material].type;
				var localChunk = chunks[chunkLink.chunk];
				var localChunkVoxels = localChunk.voxels;
				if (localChunkVoxels.Length == 0)
				{
					return;
				}
				var voxelDimensions = localChunk.voxelDimensions;
				var chunkAdjacentNeighborsData = new ChunkAdjacentNeighborsData(in chunkNeighbors, in chunks);
				// check in bounds, sometimes it out of bounds? not sure why
				var meshTypes = new NativeArray<byte>(worldVoxelEntities.Length, Allocator.Temp);
				var voxelMaterialDatas = new NativeArray<byte>(worldVoxelEntities.Length, Allocator.Temp);
				for (int i = 0; i < worldVoxelEntities.Length; i++)
				{
					var voxelEntity = worldVoxelEntities[i];
					meshTypes[i] = voxelMeshTypes[voxelEntity].meshType;
					if (materialLinks.HasComponent(voxelEntity))
					{
						var materialEntity = materialLinks[voxelEntity].material;
						voxelMaterialDatas[i] = materialDatas[materialEntity].type;
					}
				}
				byte voxelTypeLeft;
				byte voxelTypeRight;
				byte voxelTypeDown;
				byte voxelTypeBackward;
				byte voxelTypeUp;
				byte voxelTypeForward;
				byte materialType;
				int voxelArrayIndex = 0;
				byte sideIndex = 0;
				byte voxelIndex = 0;
				int3 localPosition;
				for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
				{
					for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
					{
						for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
						{
							voxelIndex = localChunkVoxels[voxelArrayIndex];
							// if air don't draw anything!
							if (voxelIndex == 0)
							{
								chunkSides.sides[voxelArrayIndex] = 0;
								voxelArrayIndex++;
								continue;
							}
							sideIndex = 0;
							materialType = voxelMaterialDatas[voxelIndex - 1];
							// Get our Neighbor Voxels!
							voxelTypeDown = chunkAdjacentNeighborsData.GetVoxelValue(in localChunk, localPosition.Down(), voxelDimensions);
							voxelTypeUp = chunkAdjacentNeighborsData.GetVoxelValue(in localChunk, localPosition.Up(), voxelDimensions);
							voxelTypeLeft = chunkAdjacentNeighborsData.GetVoxelValue(in localChunk, localPosition.Left(), voxelDimensions);
							voxelTypeRight = chunkAdjacentNeighborsData.GetVoxelValue(in localChunk, localPosition.Right(), voxelDimensions);
							voxelTypeBackward = chunkAdjacentNeighborsData.GetVoxelValue(in localChunk, localPosition.Backward(), voxelDimensions);
							voxelTypeForward = chunkAdjacentNeighborsData.GetVoxelValue(in localChunk, localPosition.Forward(), voxelDimensions);
							// water material pass
							if (materialType == MaterialType.Transparent)
							{
								if (chunkMaterialData == MaterialType.Transparent)
								{
									if (voxelTypeUp == 0 || 
										(meshTypes[voxelTypeUp - 1] != MeshType.Block
										&& voxelMaterialDatas[voxelTypeUp - 1] != MaterialType.Transparent))
									{
										sideIndex |= (byte)(1 << VoxelSide.Up);
									}
									if (voxelTypeDown == 0 || 
										(meshTypes[voxelTypeDown - 1] != MeshType.Block
										&& voxelMaterialDatas[voxelTypeDown - 1] != MaterialType.Transparent))
									{
										sideIndex |= (byte)(1 << VoxelSide.Down);
									}
									if (voxelTypeLeft == 0 ||
										(meshTypes[voxelTypeLeft - 1] != MeshType.Block
										&& voxelMaterialDatas[voxelTypeLeft - 1] != MaterialType.Transparent))
									{
										sideIndex |= (byte)(1 << VoxelSide.Left);
									}
									if (voxelTypeRight == 0 || 
										(meshTypes[voxelTypeRight - 1] != MeshType.Block
										&& voxelMaterialDatas[voxelTypeRight - 1] != MaterialType.Transparent))
									{
										sideIndex |= (byte)(1 << VoxelSide.Right);
									}
									if (voxelTypeForward == 0 || 
										(meshTypes[voxelTypeForward - 1] != MeshType.Block
										&& voxelMaterialDatas[voxelTypeForward - 1] != MaterialType.Transparent))
									{
										sideIndex |= (byte)(1 << VoxelSide.Forward);
									}
									if (voxelTypeBackward == 0 || 
										(meshTypes[voxelTypeBackward - 1] != MeshType.Block
										&& voxelMaterialDatas[voxelTypeBackward - 1] != MaterialType.Transparent))
									{
										sideIndex |= (byte)(1 << VoxelSide.Backward);
									}
								}
							}
							// for Diffuse / Leaf Passes
							else if (materialType == MaterialType.Translucent)
							{
								if (chunkMaterialData == MaterialType.Translucent)
								{
									if (voxelTypeUp == 0 || 
										(meshTypes[voxelTypeUp - 1] != MeshType.Block
										&& voxelMaterialDatas[voxelTypeUp - 1] != MaterialType.Translucent))
									{
										sideIndex |= (byte)(1 << VoxelSide.Up);
									}
									if (voxelTypeDown == 0 || 
										(meshTypes[voxelTypeDown - 1] != MeshType.Block
										&& voxelMaterialDatas[voxelTypeDown - 1] != MaterialType.Translucent))
									{
										sideIndex |= (byte)(1 << VoxelSide.Down);
									}
									if (voxelTypeLeft == 0 ||
										(meshTypes[voxelTypeLeft - 1] != MeshType.Block
										&& voxelMaterialDatas[voxelTypeLeft - 1] != MaterialType.Translucent))
									{
										sideIndex |= (byte)(1 << VoxelSide.Left);
									}
									if (voxelTypeRight == 0 || 
										(meshTypes[voxelTypeRight - 1] != MeshType.Block
										&& voxelMaterialDatas[voxelTypeRight - 1] != MaterialType.Translucent))
									{
										sideIndex |= (byte)(1 << VoxelSide.Right);
									}
									if (voxelTypeForward == 0 || 
										(meshTypes[voxelTypeForward - 1] != MeshType.Block
										&& voxelMaterialDatas[voxelTypeForward - 1] != MaterialType.Translucent))
									{
										sideIndex |= (byte)(1 << VoxelSide.Forward);
									}
									if (voxelTypeBackward == 0 || 
										(meshTypes[voxelTypeBackward - 1] != MeshType.Block
										&& voxelMaterialDatas[voxelTypeBackward - 1] != MaterialType.Translucent))
									{
										sideIndex |= (byte)(1 << VoxelSide.Backward);
									}
								}
							}
							else if (materialType == MaterialType.Diffuse)
							{
								if (chunkMaterialData == MaterialType.Diffuse)
								{
									if (voxelTypeUp == 0 || meshTypes[voxelTypeUp - 1] != 0
										|| voxelMaterialDatas[voxelTypeUp - 1] != MaterialType.Diffuse)
									{
										sideIndex |= (byte)(1 << VoxelSide.Up);
									}
									if (voxelTypeDown == 0 || meshTypes[voxelTypeDown - 1] != 0
										|| voxelMaterialDatas[voxelTypeDown - 1] != MaterialType.Diffuse)
									{
										sideIndex |= (byte)(1 << VoxelSide.Down);
									}
									if (voxelTypeForward == 0 || meshTypes[voxelTypeForward - 1] != 0
										|| voxelMaterialDatas[voxelTypeForward - 1] != MaterialType.Diffuse)
									{
										sideIndex |= (byte)(1 << VoxelSide.Forward);
									}
									if (voxelTypeBackward == 0 || meshTypes[voxelTypeBackward - 1] != 0
										|| voxelMaterialDatas[voxelTypeBackward - 1] != MaterialType.Diffuse)
									{
										sideIndex |= (byte)(1 << VoxelSide.Backward);
									}
									if (voxelTypeLeft == 0 || meshTypes[voxelTypeLeft - 1] != 0
										|| voxelMaterialDatas[voxelTypeLeft - 1] != MaterialType.Diffuse)
									{
										sideIndex |= (byte)(1 << VoxelSide.Left);
									}
									if (voxelTypeRight == 0 || meshTypes[voxelTypeRight - 1] != 0
										|| voxelMaterialDatas[voxelTypeRight - 1] != MaterialType.Diffuse)
									{
										sideIndex |= (byte)(1 << VoxelSide.Right);
									}
								}
							}
							chunkSides.sides[voxelArrayIndex] = sideIndex;
							voxelArrayIndex++;
						}
					}
				}
			})	.WithReadOnly(voxelLinks)
                .WithReadOnly(chunkNeighbors2)
				.WithReadOnly(chunks)
                .WithReadOnly(voxelMeshTypes)
                .WithReadOnly(materialLinks)
                .WithReadOnly(materialDatas)
				.ScheduleParallel(Dependency);
        }
	}
}