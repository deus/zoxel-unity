using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
    //! Removes Chunk's ChunkRender entities.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ChunkRenderHideSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // become invisible
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithAll<ChunkBecomeInvisible>()
                .ForEach((Entity e, int entityInQueryIndex, ref ChunkRenderLinks chunkRenderLinks) =>
            {
                PostUpdateCommands.RemoveComponent<ChunkBecomeInvisible>(entityInQueryIndex, e);
                // UnityEngine.Debug.LogError("(Hiding) Destroying Chunk Renders: " + chunkRenderLinks.chunkRenders.Length);
                for (int j = 0; j < chunkRenderLinks.chunkRenders.Length; j++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, chunkRenderLinks.chunkRenders[j]);
                }
                chunkRenderLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}