using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Rendering;

namespace Zoxel.Voxels
{
    //! Calculates bounds of a TerrainMesh
    /**
    *   \todo Move this to Rendering namespace. Make separate event component for it.
    */
    [UpdateAfter(typeof(PlanetChunkMeshBuilderSystem))]
    [BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
    public partial class CalculateBoundsSystem : SystemBase
	{
        const float maxBounds = 2048;

        [BurstCompile]
		protected override void OnUpdate()
		{
            if (VoxelManager.instance == null) return;
            var disableSmoothLighting = VoxelManager.instance.voxelSettings.disableSmoothLighting;
            var disableVoxelLighting = VoxelManager.instance.voxelSettings.disableVoxelLighting;
			Entities.ForEach((Entity e, ref TerrainMesh terrainMesh, ref ChunkRenderBuilder chunkRenderBuilder) =>
			{
				if (chunkRenderBuilder.chunkRenderBuildState != (byte) ChunkRenderBuildState.CalculateBounds)
                {
                    return;
                }
                if (!disableVoxelLighting)
                {
                    if (disableSmoothLighting)
                    {
                        chunkRenderBuilder.chunkRenderBuildState = (byte) ChunkRenderBuildState.GenerateBasicLights;
                    }
                    else
                    {
                        chunkRenderBuilder.chunkRenderBuildState = (byte) ChunkRenderBuildState.GenerateSmoothLights;
                    }
                }
                else
                {
                    chunkRenderBuilder.chunkRenderBuildState = (byte) ChunkRenderBuildState.CompletedChunkRenderBuilder;
                }
                CalculateMeshBounds(ref terrainMesh);
			}).ScheduleParallel();
			Entities.ForEach((Entity e, ref ModelMesh modelMesh, ref ChunkRenderBuilder chunkRenderBuilder) =>
			{
				if (chunkRenderBuilder.chunkRenderBuildState != (byte) ChunkRenderBuildState.CalculateBounds)
                {
                    return;
                }
                chunkRenderBuilder.chunkRenderBuildState = (byte) ChunkRenderBuildState.CentreMesh;
                CalculateMeshBounds(ref modelMesh);
			}).ScheduleParallel();
		}

        private static void CalculateMeshBounds(ref TerrainMesh terrainMesh)
        {
            var min = new float3(maxBounds, maxBounds, maxBounds);
            var max = new float3(-maxBounds, -maxBounds, -maxBounds);
            for (int i = 0; i < terrainMesh.vertices.Length; i++)
            {
                var vertex = terrainMesh.vertices[i].position;
                if (float.IsNaN(vertex.x) || float.IsNaN(vertex.y) || float.IsNaN(vertex.z))
                {
                    continue;
                }
                if (vertex.x < min.x)
                {
                    min.x = vertex.x;
                }
                if (vertex.x > max.x)
                {
                    max.x = vertex.x;
                }
                if (vertex.y < min.y)
                {
                    min.y = vertex.y;
                }
                if (vertex.y > max.y)
                {
                    max.y = vertex.y;
                }
                if (vertex.z < min.z)
                {
                    min.z = vertex.z;
                }
                if (vertex.z > max.z)
                {
                    max.z = vertex.z;
                }
            }
            terrainMesh.extents = (max - min) / 2f;
            terrainMesh.position = min + terrainMesh.extents;
        }

        private static void CalculateMeshBounds(ref ModelMesh modelMesh)
        {
            var min = new float3(maxBounds, maxBounds, maxBounds);
            var max = new float3(-maxBounds, -maxBounds, -maxBounds);
            for (int i = 0; i < modelMesh.vertices.Length; i++)
            {
                var vertex = modelMesh.vertices[i].position;
                if (float.IsNaN(vertex.x) || float.IsNaN(vertex.y) || float.IsNaN(vertex.z))
                {
                    continue;
                }
                if (vertex.x < min.x)
                {
                    min.x = vertex.x;
                }
                if (vertex.x > max.x)
                {
                    max.x = vertex.x;
                }
                if (vertex.y < min.y)
                {
                    min.y = vertex.y;
                }
                if (vertex.y > max.y)
                {
                    max.y = vertex.y;
                }
                if (vertex.z < min.z)
                {
                    min.z = vertex.z;
                }
                if (vertex.z > max.z)
                {
                    max.z = vertex.z;
                }
            }
            modelMesh.extents = (max - min) / 2f;
            modelMesh.position = min + modelMesh.extents;
        }
	}
}