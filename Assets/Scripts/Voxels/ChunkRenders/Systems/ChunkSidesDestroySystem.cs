using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
	//! Cleans up ChunkSides
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ChunkSidesDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
				.WithAll<DestroyEntity, ChunkSides>()
				.ForEach((in ChunkSides chunkSides) =>
			{
                chunkSides.DisposeFinal();
			}).ScheduleParallel();
		}
	}
}