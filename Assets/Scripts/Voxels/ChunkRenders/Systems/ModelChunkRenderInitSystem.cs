using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.Voxels
{
    //! Initializes Model Chunk Material and Meshes.
    /**
    *   - Material Initialize System -
    */
    [UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ModelChunkRenderInitSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<PlanetRender>()
                .WithAll<InitializeEntity, ChunkRender>()
                .ForEach((Entity e, in VoxLink voxLink) =>
            {
                if (HasComponent<Vox>(voxLink.vox))
                {
                    var entityMaterials = EntityManager.GetSharedComponentManaged<EntityMaterials>(voxLink.vox);
                    if (entityMaterials.materials == null || entityMaterials.materials.Length == 0)
                    {
                        UnityEngine.Debug.LogWarning("Vox has no materials group: " + e.Index);
                        PostUpdateCommands.SetSharedComponentManaged(e, new ZoxMesh
                        {
                            mesh = new UnityEngine.Mesh()
                        });
                    }
                    else
                    {
                        PostUpdateCommands.SetSharedComponentManaged(e, new ZoxMesh
                        {
                            mesh = new UnityEngine.Mesh(),
                            material = new UnityEngine.Material(entityMaterials.materials[0])
                        });
                    }
                }
            }).WithoutBurst().Run();
        }
    }
}