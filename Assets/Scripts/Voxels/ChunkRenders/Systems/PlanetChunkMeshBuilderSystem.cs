﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;

namespace Zoxel.Voxels
{
    //! Builds the mesh for MapChunk's.
    /**
    *   \todo Lower division of mesh baking.
    *   \todo Update division when view distance changes like Minivox, etc.
    */
    [UpdateAfter(typeof(PlanetChunkRenderSideCullingSystem))]
	[BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
	public partial class PlanetChunkMeshBuilderSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;
        private EntityQuery chunksQuery;
        private EntityQuery voxelQuery;

        protected override void OnCreate()
        {
            voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxelLinks>(),
                ComponentType.Exclude<DestroyEntity>());
            chunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkNeighbors>(),
                ComponentType.Exclude<DestroyEntity>());
            voxelQuery = GetEntityQuery(
                ComponentType.ReadOnly<VoxelMeshType>(),
                ComponentType.ReadOnly<VoxelUVMap>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(voxesQuery);
            RequireForUpdate(chunksQuery);
            RequireForUpdate(voxelQuery);
        }

        [BurstCompile]
		protected override void OnUpdate()
		{
            // const bool isDownsize = true;
            var whiteColor = new float3(1, 1, 1);
            var degreesToRadians = ((math.PI * 2) / 360f);
            var flipRotation = quaternion.EulerXYZ(new float3(180, 0, 180) * degreesToRadians);
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxelLinks = GetComponentLookup<VoxelLinks>(true);
            var voxScales = GetComponentLookup<VoxScale>(true);
            voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunks = GetComponentLookup<Chunk>(true);
            var chunkDivisions = GetComponentLookup<ChunkDivision>(true);
            chunkEntities.Dispose();
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var voxelMeshTypes = GetComponentLookup<VoxelMeshType>(true);
            var voxelUVMaps = GetComponentLookup<VoxelUVMap>(true);
            voxelEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<PlanetRender>()  // uvs
                .ForEach((ref TerrainMesh terrainMesh, ref ChunkRenderBuilder chunkRenderBuilder, in ChunkLink chunkLink, in ChunkSides chunkSides, in ChunkPosition chunkPosition, in VoxLink voxLink) =>
			{
				if (chunkRenderBuilder.chunkRenderBuildState != (byte) ChunkRenderBuildState.GenerateMesh)
                {
                    return;
                }
                chunkRenderBuilder.chunkRenderBuildState = (byte) ChunkRenderBuildState.CalculateBounds;
                var localChunk = chunks[chunkLink.chunk];
                var localChunkVoxels = localChunk.voxels;
                var voxelDimensions = localChunk.voxelDimensions;
                var voxScale = voxScales[voxLink.vox].scale;
                var isSphericalMesh = HasComponent<SphericalVox>(voxLink.vox); // localChunk.isSphericalMesh;
                var worldVoxelEntities = voxelLinks[voxLink.vox].voxels;
                var voxelMeshTypes2 = new NativeArray<byte>(worldVoxelEntities.Length, Allocator.Temp);
                var uvMaps = new NativeArray<VoxelUVMap>(worldVoxelEntities.Length, Allocator.Temp);
                for (int i = 0; i < worldVoxelEntities.Length; i++)
                {
                    var voxelEntity = worldVoxelEntities[i];
                    if (voxelEntity.Index > 0 && voxelUVMaps.HasComponent(voxelEntity))
                    {
                        uvMaps[i] = voxelUVMaps[voxelEntity];
                    }
                    else
                    {
                        uvMaps[i] = new VoxelUVMap();   // minivox has no uvs
                    }
                    voxelMeshTypes2[i] = voxelMeshTypes[voxelEntity].meshType;
                }
                var vertices = new NativeList<ZoxVertex>();
                var triangles = new NativeList<uint>();
                var chunkVoxelPosition = chunkPosition.GetVoxelPosition(voxelDimensions).ToFloat3();
                byte voxelType = 0;
                int voxelArrayIndex = 0;
                byte meshIndex = 0;
                int triangleIndex;
                int triangleStartIndex;
                int3 localPosition;
                var vert = new ZoxVertex();
                uint vertsLength;
                int cubeIndex;
                int vertStartIndex;
                byte sideIndex;
                float3 positionFloat3;
                var chunkDivision = chunkDivisions[chunkLink.chunk];
                var addition = 1;
                /*if (chunkDivision.viewDistance >= 2)
                {
                    addition = 2;
                }*/
                for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x += addition)
                {
                    for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y += addition)
                    {
                        for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z += addition)
                        {
                            voxelType = localChunkVoxels[voxelArrayIndex];
                            // if air, no need to build mesh
                            if (voxelType == 0)
                            {
                                voxelArrayIndex += addition;
                                continue;
                            }
                            voxelType--;
                            meshIndex = 0;
                            if (voxelType >= 0 && voxelType < voxelMeshTypes2.Length)
                            {
                                meshIndex = voxelMeshTypes2[voxelType];
                            }
                            if (meshIndex == 0)
                            {
                                positionFloat3 = localPosition.ToFloat3();
                                for (sideIndex = 0; sideIndex < 6; sideIndex++)
                                {
                                    // Add Voxel Side
                                    if ((sideIndex == VoxelSide.Up && chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Up) != 0)
                                        || (sideIndex == VoxelSide.Down && chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Down) != 0)
                                        || (sideIndex == VoxelSide.Left && chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Left) != 0)
                                        || (sideIndex == VoxelSide.Right && chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Right) != 0)
                                        || (sideIndex == VoxelSide.Backward && chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Backward) != 0)
                                        || (sideIndex == VoxelSide.Forward && chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Forward) != 0))
                                    {
                                        vertsLength = (uint) vertices.Length;
                                        vertStartIndex = sideIndex * 4;
                                        for (cubeIndex = vertStartIndex; cubeIndex < vertStartIndex + 4; cubeIndex++)
                                        {
                                            // var vert = new ZoxVertex();
                                            vert.color = whiteColor;
                                            // var cubeIndex = sideIndex * 4 + arrayIndex;
                                            vert.position = positionFloat3 + MeshUtilities.cubeVertices[cubeIndex] * addition;
                                            vert.position.x *= voxScale.x;
                                            vert.position.y *= voxScale.y;
                                            vert.position.z *= voxScale.z;
                                            // bend this localPosition based on distance to middle of planet
                                            if (isSphericalMesh)
                                            {
                                                vert.position += chunkVoxelPosition;
                                                var maxDistance = math.max(math.max(math.abs(vert.position.x), math.abs(vert.position.y)), math.abs(vert.position.z));
                                                vert.position = maxDistance * math.normalize(vert.position);
                                                vert.position -= chunkVoxelPosition;
                                            }
                                            // if planet chunk, do this
                                            if (voxelType < uvMaps.Length)
                                            {
                                                var uvs = uvMaps[voxelType].uvs;
                                                if (cubeIndex < uvs.Length)
                                                {
                                                    vert.uv = uvs[cubeIndex];
                                                }
                                            }
                                            vertices.Add(vert);
                                        }
                                        triangleStartIndex = sideIndex * 6;
                                        for (triangleIndex = triangleStartIndex; triangleIndex < triangleStartIndex + 6; triangleIndex++)
                                        {
                                            triangles.Add(MeshUtilities.cubeTriangles[triangleIndex] + vertsLength);
                                        }
                                    }
                                }
                            }
                            voxelArrayIndex += addition;
                        }
                    }
                }
                // UnityEngine.Debug.LogError("Before Disposing, Vert Count: " + terrainMesh.vertices.Length);
                /*var vertices2 = vertices.ToArray(Allocator.Persistent);
                var triangles2 = triangles.ToArray(Allocator.Persistent);
                terrainMesh.vertices = new BlitableArray<ZoxVertex>(ref vertices2, Allocator.Persistent);
                terrainMesh.triangles = new BlitableArray<uint>(ref triangles2, Allocator.Persistent);*/
                terrainMesh.Dispose();
                terrainMesh.vertices = new BlitableArray<ZoxVertex>(vertices.Length, Allocator.Persistent);
                terrainMesh.triangles = new BlitableArray<uint>(triangles.Length, Allocator.Persistent);
                for (int i = 0; i < vertices.Length; i++)
                {
                    terrainMesh.vertices[i] = vertices[i];
                }
                for (int i = 0; i < triangles.Length; i++)
                {
                    terrainMesh.triangles[i] = triangles[i];
                }
			})  .WithReadOnly(chunks)
                .WithReadOnly(chunkDivisions)
                .WithReadOnly(voxelLinks)
                .WithReadOnly(voxScales)
                .WithReadOnly(voxelMeshTypes)
                .WithReadOnly(voxelUVMaps)
                .ScheduleParallel(Dependency);
        }
	}
}