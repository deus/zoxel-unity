using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
    //! Destroys Chunk's ChunkRender's on DestroyEntity event.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ChunkRenderLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithAll<DestroyEntity, ChunkRenderLinks>()
                .ForEach((Entity e, int entityInQueryIndex, in ChunkRenderLinks chunkRenderLinks) =>
			{
                // UnityEngine.Debug.LogError("Destroying Chunk Renders: " + chunkRenderLinks.chunkRenders.Length);
                for (int i = 0; i < chunkRenderLinks.chunkRenders.Length; i++)
                {
                    var chunkRenderEntity = chunkRenderLinks.chunkRenders[i];
                    if (HasComponent<ChunkRender>(chunkRenderEntity))
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, chunkRenderEntity);
                    }
                }
                chunkRenderLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}