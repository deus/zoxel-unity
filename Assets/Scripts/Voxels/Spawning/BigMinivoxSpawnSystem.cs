using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;			// Positioning
using Zoxel.Voxels.Lighting;	// Starting Light
using Zoxel.Rendering;			// MaterialsManager
using Zoxel.Voxels.Authoring;

namespace Zoxel.Voxels.Minivoxes
{
	//! Spawns BigMinivox entities from our chunk voxel data.
    /**
    *   - Spawn System -
	*	I can use DEBUG_BIGMINIVOXES_HEALTH to debug BigMinivox healths.
	*	\todo Save open/close state of minivoxes (Doors).
	*	\todo Maybe don't dispose of BigMinivoxLink positions every time reassigning.
	*/
	[BurstCompile, UpdateInGroup(typeof(MinivoxSystemGroup))]
    public partial class BigMinivoxSpawnSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private Entity bigMinivoxPrefab;
        private EntityQuery processQuery;
        private EntityQuery worldQuery;
        private EntityQuery chunksQuery;	// using neighbor chunks to spawn bigger voxes
        private EntityQuery voxelQuery;
        private EntityQuery bigMinivoxQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            worldQuery = GetEntityQuery(
				ComponentType.ReadOnly<Planet>(),
				ComponentType.ReadOnly<ZoxID>(),
				ComponentType.ReadOnly<VoxelLinks>(),
				ComponentType.ReadOnly<RealmLink>(),
				ComponentType.Exclude<DestroyEntity>());
            chunksQuery = GetEntityQuery(
				ComponentType.ReadOnly<Chunk>(),
				ComponentType.ReadOnly<PlanetChunk>(),
				ComponentType.Exclude<DestroyEntity>());
            voxelQuery = GetEntityQuery(ComponentType.ReadOnly<Voxel>());
            bigMinivoxQuery = GetEntityQuery(ComponentType.ReadOnly<BigMinivox>());
            RequireForUpdate<MinivoxSettings>();
            RequireForUpdate(processQuery);
		}

		private void InitializePrefabs()
		{
			if (this.bigMinivoxPrefab.Index != 0)
			{
				return;
			}
			var minivoxPrefab = MinivoxSpawnSystem.minivoxPrefab;
			this.bigMinivoxPrefab = EntityManager.Instantiate(minivoxPrefab);
			EntityManager.AddComponent<Prefab>(this.bigMinivoxPrefab);
			EntityManager.RemoveComponent<BasicMinivox>(this.bigMinivoxPrefab);
			EntityManager.RemoveComponent<NewMinivox>(this.bigMinivoxPrefab);
			EntityManager.AddComponent<BigMinivox>(this.bigMinivoxPrefab);
			EntityManager.AddComponent<NewBigMinivox>(this.bigMinivoxPrefab);
			EntityManager.AddComponent<MultiChunkLink>(this.bigMinivoxPrefab);
			EntityManager.AddComponent<MultiVoxelPosition>(this.bigMinivoxPrefab);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var minivoxSettings = GetSingleton<MinivoxSettings>();
			if (minivoxSettings.disableBigMinivoxes)
			{
				return;
			}
			InitializePrefabs();
            var minivoxResolution = minivoxSettings.minivoxResolution / 16f;
			var maxMinivoxesPerChunk = minivoxSettings.maxMinivoxesPerChunk;
			var minivoxOffset = new float3(0.5f, 0.5f, 0.5f);
            var oneSize = new int3(1, 1, 1);
			var degreesToRadians = ((math.PI * 2) / 360f);
			var flippedCharacter = quaternion.EulerXYZ(new float3(180, 0, 0) * degreesToRadians);
			var rotation1 = quaternion.EulerXYZ(new float3(0, 0, -90) * degreesToRadians);
			var rotation2 = quaternion.EulerXYZ(new float3(0, 0, 90) * degreesToRadians);
			var rotation3 = quaternion.EulerXYZ(new float3(-90, 0, 0) * degreesToRadians);
			var rotation4 = quaternion.EulerXYZ(new float3(90, 0, 0) * degreesToRadians);  
			var minivoxVoxelDimensions = new int3(16, 16, 16);
			var minivoxScale = (new float3(1f / 16f, 1f / 16f, 1f / 16f));
			var bigMinivoxPrefab = this.bigMinivoxPrefab;
			var bigMinivoxesSpawnedPrefab = MinivoxSystemGroup.bigMinivoxesSpawnedPrefab;
			var bigMinivoxesRemovedPrefab = MinivoxSystemGroup.bigMinivoxesRemovedPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var worldEntities = worldQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var worldVoxelLinks = GetComponentLookup<VoxelLinks>(true);
            var worldRealmLinks = GetComponentLookup<RealmLink>(true);
			var voxScales = GetComponentLookup<VoxScale>(true);
			worldEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunks = GetComponentLookup<Chunk>(true);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            var chunkLights2 = GetComponentLookup<ChunkLights>(true);
			var minivoxLinks2 = GetComponentLookup<MinivoxLinks>(true);
			var chunkVoxelRotations = GetComponentLookup<ChunkVoxelRotations>(true);
			var chunkHasMinivoxes = GetComponentLookup<ChunkHasMinivoxes>(true);
			chunkEntities.Dispose();
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var voxels2 = GetComponentLookup<Voxel>(true);
            var voxelsVoxData = GetComponentLookup<VoxDataEditor>(true);
            var generateVoxDatas = GetComponentLookup<GenerateVoxData>(true);
			var blockDimensions = GetComponentLookup<BlockDimensions>(true);
            var voxelChunkDimensions = GetComponentLookup<ChunkDimensions>(true);
            var minivoxPlaceOffsets = GetComponentLookup<MinivoxPlaceOffset>(true);
			voxelEntities.Dispose();
            var bigMinivoxEntities = bigMinivoxQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var multiVoxelPositions = GetComponentLookup<MultiVoxelPosition>(true);
            var multiChunkLinks = GetComponentLookup<MultiChunkLink>(true);
			bigMinivoxEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<DestroyEntity, DestroyChunkMinivoxes, EntityBusy>()
				.WithAll<SpawnMinivoxes>()
				.ForEach((Entity e, int entityInQueryIndex, in VoxLink voxLink, in ChunkNeighbors chunkNeighbors) =>
			{
				if (!HasComponent<VoxelLinks>(voxLink.vox))
				{
					return;
				}
				var chunk = chunks[e];
				if (chunk.voxels.Length == 0)
				{
					return;
				}
				var planetEntity = voxLink.vox;
				var voxelLinks = worldVoxelLinks[planetEntity];
				var voxScale = voxScales[planetEntity].scale;
				var thisMinivoxScale = minivoxScale;
				thisMinivoxScale.x *= voxScale.x;
				thisMinivoxScale.y *= voxScale.y;
				thisMinivoxScale.z *= voxScale.z;
				// Temp Lists
				var bigMinivoxVoxelTypes = new NativeList<byte>();
				var bigMinivoxPositions = new NativeList<int3>();
				var bigMinivoxRotations = new NativeList<byte>();
				var bigMinivoxTypes = new NativeList<byte>();
				var bigMinivoxLocalVoxelPositions = new NativeList<int3>();
				var bigMinivoxRotations2 = new NativeList<byte>();
				var voxels = new NativeArray<Voxel>(voxelLinks.voxels.Length, Allocator.Temp);
				var chunkNeighbors2 = chunkNeighbors.GetNeighbors();
				var grassesAdded = new NativeList<int3>();
				var grassesRemoved = new NativeList<int3>();
				var grassesChunksAdded = new NativeList<Entity>();
				var grassesChunksRemoved = new NativeList<Entity>();
				var spawnGlobalPositions = new NativeList<int3>();
				var destroyEntities = new NativeList<Entity>();
				// more
				var localChunkVoxelRotations = chunkVoxelRotations[e];
				var minivoxLinks = minivoxLinks2[e];
				var voxelDimensions = chunk.voxelDimensions;
				for (int i = 0; i < voxels.Length; i++)
				{
					voxels[i] = voxels2[voxelLinks.voxels[i]];
				}
				var chunkPosition = chunkPositions[e];
				var chunkVoxelPosition = chunkPosition.GetVoxelPosition(voxelDimensions);
				int3 position;
				var voxelIndex = 0;
				byte voxelMetaIndex = 0;
				byte voxelMetaIndexMinusAir = 0;
				var realmEntity = worldRealmLinks[planetEntity].realm;
				var realmLink = new RealmLink(realmEntity);
				var chunkLink = new ChunkLink(e);
				// Add Individual Positions of BigMinivox Types
				for (position.x = 0; position.x < voxelDimensions.x; position.x++)
				{
					for (position.y = 0; position.y < voxelDimensions.y; position.y++)
					{
						for (position.z = 0; position.z < voxelDimensions.z; position.z++)
						{
							voxelMetaIndex = chunk.voxels[voxelIndex];
							if (voxelMetaIndex == 0)
							{
								voxelIndex++;
								continue;
							}
							voxelMetaIndexMinusAir = (byte)(voxelMetaIndex - 1);
							if (voxelMetaIndexMinusAir >= voxels.Length)
							{
								voxelIndex++;
								continue;
							}
							var voxelEntity = voxelLinks.voxels[voxelMetaIndexMinusAir];
							if (!HasComponent<BigMinivoxVoxel>(voxelEntity))
							{
								voxelIndex++;
								continue;
							}
							bigMinivoxPositions.Add(position);
							bigMinivoxTypes.Add(voxelMetaIndexMinusAir);
							bigMinivoxRotations.Add(localChunkVoxelRotations.GetRotation(position));
							voxelIndex++;
						}
					}
				}
				//! Add neighbor chunk voxels.
				{
					// add bottom voxels
					if (chunkNeighbors.chunkDown.Index > 0 && chunks.HasComponent(chunkNeighbors.chunkDown))
					{
						var chunkDown = chunks[chunkNeighbors.chunkDown];
						if (chunkDown.voxels.Length > 0)
						{
							var chunkVoxelRotationsDown = chunkVoxelRotations[chunkNeighbors.chunkDown];
							position.y = voxelDimensions.y - 1;
							for (position.x = 0; position.x < voxelDimensions.x; position.x++)
							{
								for (position.z = 0; position.z < voxelDimensions.z; position.z++)
								{
									voxelIndex = VoxelUtilities.GetVoxelArrayIndex(position, voxelDimensions);
									voxelMetaIndex = chunkDown.voxels[voxelIndex];
									if (voxelMetaIndex == 0)
									{
										continue;
									}
									voxelMetaIndexMinusAir = (byte)(voxelMetaIndex - 1);
									if (voxelMetaIndexMinusAir >= voxels.Length)
									{
										continue;
									}
									var voxelEntity = voxelLinks.voxels[voxelMetaIndexMinusAir];
									if (!HasComponent<BigMinivoxVoxel>(voxelEntity))
									{
										voxelIndex++;
										continue;
									}
									bigMinivoxPositions.Add(position - new int3(0, voxelDimensions.y, 0));
									bigMinivoxTypes.Add(voxelMetaIndexMinusAir);
									bigMinivoxRotations.Add(chunkVoxelRotationsDown.GetRotation(position));
								}
							}
						}
					}
					// add top voxels
					if (chunkNeighbors.chunkUp.Index > 0 && chunks.HasComponent(chunkNeighbors.chunkUp))
					{
						var chunkUp = chunks[chunkNeighbors.chunkUp];
						if (chunkUp.voxels.Length > 0)
						{
							var chunkVoxelRotationsUp = chunkVoxelRotations[chunkNeighbors.chunkUp];
							position.y = 0;
							for (position.x = 0; position.x < voxelDimensions.x; position.x++)
							{
								for (position.z = 0; position.z < voxelDimensions.z; position.z++)
								{
									voxelIndex = VoxelUtilities.GetVoxelArrayIndex(position, voxelDimensions);
									voxelMetaIndex = chunkUp.voxels[voxelIndex];
									if (voxelMetaIndex == 0)
									{
										continue;
									}
									voxelMetaIndexMinusAir = (byte)(voxelMetaIndex - 1);
									if (voxelMetaIndexMinusAir >= voxels.Length)
									{
										continue;
									}
									var voxelEntity = voxelLinks.voxels[voxelMetaIndexMinusAir];
									if (!HasComponent<BigMinivoxVoxel>(voxelEntity))
									{
										voxelIndex++;
										continue;
									}
									bigMinivoxPositions.Add(position + new int3(0, voxelDimensions.y, 0));
									bigMinivoxTypes.Add(voxelMetaIndexMinusAir);
									bigMinivoxRotations.Add(chunkVoxelRotationsUp.GetRotation(position));
								}
							}
						}
					}
					// add backward voxels
					if (chunkNeighbors.chunkBack.Index > 0 && chunks.HasComponent(chunkNeighbors.chunkBack))
					{
						var chunkBack = chunks[chunkNeighbors.chunkBack];
						if (chunkBack.voxels.Length > 0)
						{
							var chunkVoxelRotationsBack = chunkVoxelRotations[chunkNeighbors.chunkBack];
							position.z = voxelDimensions.z - 1;
							for (position.x = 0; position.x < voxelDimensions.x; position.x++)
							{
								for (position.y = 0; position.y < voxelDimensions.y; position.y++)
								{
									voxelIndex = VoxelUtilities.GetVoxelArrayIndex(position, voxelDimensions);
									voxelMetaIndex = chunkBack.voxels[voxelIndex];
									if (voxelMetaIndex == 0)
									{
										continue;
									}
									voxelMetaIndexMinusAir = (byte)(voxelMetaIndex - 1);
									if (voxelMetaIndexMinusAir >= voxels.Length)
									{
										continue;
									}
									var voxelEntity = voxelLinks.voxels[voxelMetaIndexMinusAir];
									if (!HasComponent<BigMinivoxVoxel>(voxelEntity))
									{
										voxelIndex++;
										continue;
									}
									bigMinivoxPositions.Add(position - new int3(0, 0, voxelDimensions.z));
									bigMinivoxTypes.Add(voxelMetaIndexMinusAir);
									bigMinivoxRotations.Add(chunkVoxelRotationsBack.GetRotation(position));
								}
							}
						}
					}
					// add bottom voxels
					if (chunkNeighbors.chunkForward.Index > 0 && chunks.HasComponent(chunkNeighbors.chunkForward))
					{
						var chunkForward = chunks[chunkNeighbors.chunkForward];
						if (chunkForward.voxels.Length > 0)
						{
							var chunkVoxelRotationsForward = chunkVoxelRotations[chunkNeighbors.chunkForward];
							position.z = 0;
							for (position.x = 0; position.x < voxelDimensions.x; position.x++)
							{
								for (position.y = 0; position.y < voxelDimensions.y; position.y++)
								{
									voxelIndex = VoxelUtilities.GetVoxelArrayIndex(position, voxelDimensions);
									voxelMetaIndex = chunkForward.voxels[voxelIndex];
									if (voxelMetaIndex == 0)
									{
										continue;
									}
									voxelMetaIndexMinusAir = (byte)(voxelMetaIndex - 1);
									if (voxelMetaIndexMinusAir >= voxels.Length)
									{
										continue;
									}
									var voxelEntity = voxelLinks.voxels[voxelMetaIndexMinusAir];
									if (!HasComponent<BigMinivoxVoxel>(voxelEntity))
									{
										voxelIndex++;
										continue;
									}
									bigMinivoxPositions.Add(position + new int3(0, 0, voxelDimensions.z));
									bigMinivoxTypes.Add(voxelMetaIndexMinusAir);
									bigMinivoxRotations.Add(chunkVoxelRotationsForward.GetRotation(position));
								}
							}
						}
					}
					// add left voxels
					if (chunkNeighbors.chunkLeft.Index > 0 && chunks.HasComponent(chunkNeighbors.chunkLeft))
					{
						var chunkLeft = chunks[chunkNeighbors.chunkLeft];
						if (chunkLeft.voxels.Length > 0)
						{
							var chunkVoxelRotationsLeft = chunkVoxelRotations[chunkNeighbors.chunkLeft];
							position.x = voxelDimensions.x - 1;
							for (position.y = 0; position.y < voxelDimensions.y; position.y++)
							{
								for (position.z = 0; position.z < voxelDimensions.z; position.z++)
								{
									voxelIndex = VoxelUtilities.GetVoxelArrayIndex(position, voxelDimensions);
									voxelMetaIndex = chunkLeft.voxels[voxelIndex];
									if (voxelMetaIndex == 0)
									{
										continue;
									}
									voxelMetaIndexMinusAir = (byte)(voxelMetaIndex - 1);
									if (voxelMetaIndexMinusAir >= voxels.Length)
									{
										continue;
									}
									var voxelEntity = voxelLinks.voxels[voxelMetaIndexMinusAir];
									if (!HasComponent<BigMinivoxVoxel>(voxelEntity))
									{
										voxelIndex++;
										continue;
									}
									bigMinivoxPositions.Add(position - new int3(voxelDimensions.x, 0, 0));
									bigMinivoxTypes.Add(voxelMetaIndexMinusAir);
									bigMinivoxRotations.Add(chunkVoxelRotationsLeft.GetRotation(position));
								}
							}
						}
					}
					// add right voxels
					if (chunkNeighbors.chunkRight.Index > 0 && chunks.HasComponent(chunkNeighbors.chunkRight))
					{
						var chunkRight = chunks[chunkNeighbors.chunkRight];
						if (chunkRight.voxels.Length > 0)
						{
							var chunkVoxelRotationsRight = chunkVoxelRotations[chunkNeighbors.chunkRight];
							position.x = 0;
							for (position.y = 0; position.y < voxelDimensions.y; position.y++)
							{
								for (position.z = 0; position.z < voxelDimensions.z; position.z++)
								{
									voxelIndex = VoxelUtilities.GetVoxelArrayIndex(position, voxelDimensions);
									voxelMetaIndex = chunkRight.voxels[voxelIndex];
									if (voxelMetaIndex == 0)
									{
										continue;
									}
									voxelMetaIndexMinusAir = (byte)(voxelMetaIndex - 1);
									if (voxelMetaIndexMinusAir >= voxels.Length)
									{
										continue;
									}
									var voxelEntity = voxelLinks.voxels[voxelMetaIndexMinusAir];
									if (!HasComponent<BigMinivoxVoxel>(voxelEntity))
									{
										voxelIndex++;
										continue;
									}
									bigMinivoxPositions.Add(position + new int3(voxelDimensions.x, 0, 0));
									bigMinivoxTypes.Add(voxelMetaIndexMinusAir);
									bigMinivoxRotations.Add(chunkVoxelRotationsRight.GetRotation(position));
								}
							}
						}
					}
				}
				
				//! Consolidates added voxel positions into grouped position data.
				for (int i = bigMinivoxPositions.Length - 1; i >= 0; i--)
				{
					if (bigMinivoxLocalVoxelPositions.Length >= maxMinivoxesPerChunk)
					{
						continue;
					}
					var voxelPosition = bigMinivoxPositions[i];
					var voxelType = bigMinivoxTypes[i];
					var voxelEntity = voxelLinks.voxels[voxelType];
					var voxelRotation = bigMinivoxRotations[i];
					var positionOffsets = blockDimensions[voxelEntity].GetPositionOffsets(voxelRotation);
					// check if has all positions
					var hasAllPositions = true;
					for (int j = 0; j < positionOffsets.Length; j++)
					{
						var positionOffsetted = voxelPosition + positionOffsets[j];
						if (!bigMinivoxPositions.Contains(positionOffsetted))
						{
							hasAllPositions = false;
							break;
						}
					}
					if (!hasAllPositions)
					{
						positionOffsets.Dispose();
						continue;
					}
					// check for local positions
					var anyLocalPositions = false;
					for (int j = 0; j < positionOffsets.Length; j++)
					{
						var positionOffsetted = voxelPosition + positionOffsets[j];
						if (VoxelUtilities.InBounds(positionOffsetted, voxelDimensions))
						{
							anyLocalPositions = true;
							break;
						}
					}
					bigMinivoxPositions.RemoveAt(i);
					bigMinivoxTypes.RemoveAt(i);
					bigMinivoxRotations.RemoveAt(i);
					// Removes other indexes
					for (int j = 0; j < positionOffsets.Length; j++)
					{
						var positionOffsetted = voxelPosition + positionOffsets[j];
						if (voxelPosition == positionOffsetted)
						{
							continue;
						}
						var removeIndex = bigMinivoxPositions.IndexOf(positionOffsetted);
						if (removeIndex < i)
						{
							i--;
						}
						bigMinivoxPositions.RemoveAt(removeIndex);
						bigMinivoxTypes.RemoveAt(removeIndex);
						bigMinivoxRotations.RemoveAt(removeIndex);
					}
					if (!anyLocalPositions)
					{
						positionOffsets.Dispose();
						// UnityEngine.Debug.LogError("No local positions for this placement: " + voxelPosition);
						continue;
					}
					bigMinivoxLocalVoxelPositions.Add(voxelPosition);
					bigMinivoxVoxelTypes.Add(voxelType);
					bigMinivoxRotations2.Add(voxelRotation);
					positionOffsets.Dispose();
				}
				
				#if DEBUG_BIG_MINIVOX_MULTICHUNK
				if (bigMinivoxVoxelTypes.Length > 0)
				{
					UnityEngine.Debug.LogError("Big Minivox Spawning: " + bigMinivoxVoxelTypes.Length);
				}
				#endif
				
				//! Spawning BigMinivox Entities!
				for (int i = 0; i < bigMinivoxLocalVoxelPositions.Length; i++)
				{
					//! Global voxel positions for a BigMinivox.
					var multiVoxelPositions = new NativeList<int3>();
					//! Global chunk entities for a BigMinivox.
					var overlappingChunks = new NativeList<Entity>();
					voxelIndex = bigMinivoxVoxelTypes[i];
					var voxelEntity = voxelLinks.voxels[voxelIndex];
					var voxelRotation = bigMinivoxRotations2[i];
					var blockDimensions2 = blockDimensions[voxelEntity];
					var positionOffsets = blockDimensions2.GetPositionOffsets(voxelRotation);
					var minivoxLocalPosition = bigMinivoxLocalVoxelPositions[i];
					var minivoxGlobalPosition = chunkVoxelPosition + minivoxLocalPosition;
					var voxelMeta = voxels[voxelIndex];
					var chunkEntity = e;
					//! Gets positions accross chunks
					for (var j = 0; j < positionOffsets.Length; j++)
					{
						var voxelPosition = minivoxGlobalPosition + positionOffsets[j];
						multiVoxelPositions.Add(voxelPosition);
						spawnGlobalPositions.Add(voxelPosition);
						// var chunkPosition2 = VoxelUtilities.GetChunkPosition(voxelPosition, voxelDimensions);
						// if (chunkPosition2 != chunkPosition.position && !multiVoxelPositions.Contains(chunkPosition2))
						{
							// multiVoxelPositions.Add(voxelPosition);
						}
					}
					//! \todo If spawning a MultiChunk BigMinivox, then add a Spawned Event for that chunk here.
					//if (multiVoxelPositions.Length > 0)
					//{
					// UnityEngine.Debug.LogError("BigMinivox Spans accross chunk at: " + minivoxGlobalPosition);
					// multiVoxelPositions.Add(chunkPosition.position);
					multiVoxelPositions.Sort();
					minivoxGlobalPosition = multiVoxelPositions[0];
					for (int j = 0; j < multiVoxelPositions.Length; j++)
					{
						var offsetedPosition = multiVoxelPositions[j];
						var extraChunkPosition = VoxelUtilities.GetChunkPosition(offsetedPosition, voxelDimensions);
						if (extraChunkPosition == chunkPosition.position)
						{
							overlappingChunks.Add(e);
							continue;
						}
						// UnityEngine.Debug.LogError("Checking extraChunkPosition[" + j + "]: " + extraChunkPosition);
						//! \todo Use hashmap for ChunkNeighbors for faster lookup
						for (int k = 0; k < chunkNeighbors2.Length; k++)
						{
							var chunkEntity2 = chunkNeighbors2[k];
							if (chunkEntity2.Index > 0 && chunkPositions.HasComponent(chunkEntity2) && chunkPositions[chunkEntity2].position == extraChunkPosition)
							{
								// UnityEngine.Debug.LogError("Sucess extraChunkPosition[" + j + ", " + k + "]: " + extraChunkPosition);
								overlappingChunks.Add(chunkEntity2);
								break;
							}
						}
					}
					chunkEntity = overlappingChunks[0];
					/*for (int j = 0; j < multiVoxelPositions.Length; j++)
					{
						var extraChunkPosition = multiVoxelPositions[j];
						// UnityEngine.Debug.LogError(e.Index + " - extraChunkPosition [" + j + "]: " + extraChunkPosition);
					}*/

					//! We don't want to spawn it twice, so if multi chunk, spawn only if in main chunk
					// Questions to ask
					//		Has BigMinivox already spawned?
					//		Did it spawn on any overlapping chunks?
					//	If so, continue, no need to spawn again.

					// later check only unique overlapping chunks
					var doesAlreadyExist = false;
					for (int j = 0; j < overlappingChunks.Length; j++)
					{
						var chunkEntity2 = overlappingChunks[j];
						var overlappingPosition = multiVoxelPositions[j];
						var minivoxLinks3 = minivoxLinks2[chunkEntity2];
						if (minivoxLinks3.HasMinivox(overlappingPosition))
						{
							doesAlreadyExist = true;
							break;
						}
					}
					if (doesAlreadyExist)
					{
						#if DEBUG_BIG_MINIVOX_MULTICHUNK
						UnityEngine.Debug.LogError("Big Minivox Already Contains BigMinivox: " + minivoxGlobalPosition);
						#endif
						// bigMinivoxes.Add(minivoxLinks3.GetBigMinivoxLink(minivoxGlobalPosition).Clone());
						multiVoxelPositions.Dispose();
						overlappingChunks.Dispose();
						positionOffsets.Dispose();
						continue;
					}

					//bigMinivoxes.Add(new BigMinivoxLink(in multiVoxelPositions, in overlappingChunks));
					var bigMinivoxSpawnChunkPosition = chunkPosition.position;
					//! If MultiChunk voxel, will only spawn if not first visible chunk that can spawn it.
					// Only spawn BigMinivoxVoxel's if core position is in bounds.
					// Sort chunks by X, then by Z, then by Y.
					// Spawn at first one in list that Has
					var isThisFirstChunk = true;
					for (var j = 0; j < overlappingChunks.Length; j++)
					{
						var chunkEntity2 = overlappingChunks[j];
						if (chunkHasMinivoxes.HasComponent(chunkEntity2))
						{
							isThisFirstChunk = e == chunkEntity2;
							break;
						}
					}
					if (!isThisFirstChunk) // bigMinivoxSpawnChunkPosition != chunkPosition.position)
					{
						/*#if DEBUG_BIG_MINIVOX_MULTICHUNK
						UnityEngine.Debug.LogError("Not Spawning BigMinivox for Chunk: " + chunkPosition.position);
						#endif*/
						multiVoxelPositions.Dispose();
						overlappingChunks.Dispose();
						positionOffsets.Dispose();
						continue;
					}
					/*#if DEBUG_BIG_MINIVOX_MULTICHUNK
					else
					{
						UnityEngine.Debug.LogError("Spawning BigMinivox for Chunk: " + chunkPosition.position);
					}
					#endif*/

					// New Spawned Ones, add to lists
					for (int j = 0; j < multiVoxelPositions.Length; j++)
					{
						grassesAdded.Add(multiVoxelPositions[j]);
						grassesChunksAdded.Add(overlappingChunks[j]);
					}
					// UnityEngine.Debug.LogError("BigMinivox Position: " + minivoxGlobalPosition);
					// UnityEngine.Debug.LogError("Positions of BigMinivox: " + overlappingChunks.Length + " :: " + multiVoxelPositions.Length);
					// Spawn BigMinivox.
					var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, bigMinivoxPrefab);
					// Add all positions within the chunk
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, realmLink);
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, voxLink);
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new VoxelPosition(minivoxGlobalPosition));
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ChunkLink(chunkEntity));
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new MultiVoxelPosition(in multiVoxelPositions));
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new MultiChunkLink(in overlappingChunks));
					// set properties per minivox
					// for all Minivoxes
					// Transforms
					var spawnPosition = float3.zero; // minivoxGlobalPosition.ToFloat3() + minivoxOffset;
					for (int a = 0; a < multiVoxelPositions.Length; a++)
					{
						spawnPosition += multiVoxelPositions[a].ToFloat3();
					}
					spawnPosition /= (float) multiVoxelPositions.Length;
					spawnPosition += minivoxOffset;
					var placeOffset = new int3();
					if (minivoxPlaceOffsets.HasComponent(voxelEntity))
					{
						placeOffset = minivoxPlaceOffsets[voxelEntity].placeOffset;
					}
					var offsetPosition = new float3(minivoxScale.x * placeOffset.x, minivoxScale.y * placeOffset.y,	minivoxScale.z * placeOffset.z);
					var rotation = quaternion.identity;
					if (voxelMeta.canRotate == 1)
					{
						// var voxelRotation = chunkVoxelRotations.GetRotation(minivoxLocalPosition);
						rotation = BlockRotation.GetRotation(voxelRotation);
						// UnityEngine.Debug.LogError("Rotation: " + rotation + " offsetPosition: " + offsetPosition + " rotated offset position: " + math.rotate(rotation, offsetPosition));
						#if DEBUG_VOXEL_ROTATION
						UnityEngine.Debug.LogError("voxelRotation " + voxelRotation + " at minivoxLocalPosition: " + minivoxLocalPosition);
						#endif
					}
					// UnityEngine.Debug.LogError("Big Minivox offsetPosition: " + offsetPosition + " : " + voxelRotation);
					spawnPosition = spawnPosition + math.rotate(rotation, offsetPosition);
					spawnPosition.x *= voxScale.x;
					spawnPosition.y *= voxScale.y;
					spawnPosition.z *= voxScale.z;
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Rotation { Value = rotation });
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Translation { Value = spawnPosition });
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Minivox(spawnPosition, rotation));
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new MinivoxColor(voxelMeta.color, voxelMeta.secondaryColor));
					//! Calculates Lighting for all positions of BigMinivox
					var lightValue = (byte) 0;
					var lightsCount = 0;
					for (var j = 0; j < multiVoxelPositions.Length; j++)
					{
						var voxelPosition = multiVoxelPositions[j];
						var chunkPosition2 = VoxelUtilities.GetChunkPosition(voxelPosition, voxelDimensions);
						var voxelLocalPosition = VoxelUtilities.GetLocalPosition(voxelPosition, chunkPosition2, voxelDimensions);
						var chunkEntity3 = e;
						for (int k = 0; k < chunkNeighbors2.Length; k++)
						{
							var chunkEntity2 = chunkNeighbors2[k];
							if (chunkEntity2.Index > 0 && chunkPositions.HasComponent(chunkEntity2) && chunkPositions[chunkEntity2].position == chunkPosition2)
							{
								chunkEntity3 = chunkEntity2;
								break;
							}
						}
						var voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(voxelLocalPosition, voxelDimensions);
						var chunkLights = chunkLights2[chunkEntity3];
						lightValue += chunkLights.lights[voxelArrayIndex];
						lightsCount++;
					}
					if (lightsCount > 0)
					{
						lightValue = (byte) (lightValue / lightsCount);
						PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new EntityLighting(lightValue));
					}
					var minivoxVoxelDimensions2 = minivoxVoxelDimensions;
					if (voxelChunkDimensions.HasComponent(voxelEntity))
					{
						minivoxVoxelDimensions2 = voxelChunkDimensions[voxelEntity].voxelDimensions;
					}
					if (HasComponent<GenerateVoxData>(voxelEntity))
					{
						//! \todo Move the components into separate prefabs
						var generateVoxData = generateVoxDatas[voxelEntity];
						PostUpdateCommands.AddComponent<SpawnVoxChunk>(entityInQueryIndex, e2);
						PostUpdateCommands.AddComponent<GenerateVox>(entityInQueryIndex, e2);
						PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new GenerateVoxData(generateVoxData.generationType));
						PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new VoxScale(thisMinivoxScale / minivoxResolution));
						minivoxVoxelDimensions2 *= minivoxResolution;
						// UnityEngine.Debug.LogError("BigMinivox set to generate: " + generateVoxData.generationType);
					}
					else if (HasComponent<VoxDataEditor>(voxelEntity))
					{
						//! \todo Move the components into separate prefabs
						var voxData = voxelsVoxData[voxelEntity];
						PostUpdateCommands.AddComponent(entityInQueryIndex, e2, voxData);
						PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new VoxScale(thisMinivoxScale));
						PostUpdateCommands.AddComponent<SpawnVoxChunk>(entityInQueryIndex, e2);
						minivoxVoxelDimensions2 = voxData.size;
					}
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ChunkDimensions(minivoxVoxelDimensions2));
					positionOffsets.Dispose();
					multiVoxelPositions.Dispose();
					overlappingChunks.Dispose();
				}
				//! Remove Removed BigMinivoxes.
				if (minivoxLinks.minivoxes.IsCreated)
				{
					foreach (var KVP in minivoxLinks.minivoxes)
					{
						var minivoxPosition = KVP.Key;
						var minivoxEntity = KVP.Value;
						if (minivoxEntity.Index <= 0 || !HasComponent<BigMinivox>(minivoxEntity) || spawnGlobalPositions.Contains(minivoxPosition))
						{
							continue;
						}
						// PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, minivoxEntity);
						if (!destroyEntities.Contains(minivoxEntity))
						{
							destroyEntities.Add(minivoxEntity);
						}
					}
					/*#if DEBUG_BIGMINIVOXES_HEALTH
					UnityEngine.Debug.LogError("Destroying Big Minivox: " + bigMinivoxLink.positions[0] + " - " + bigMinivoxLink.positions[1]);
					#endif*/
				}
				for (int i = 0; i < destroyEntities.Length; i++)
				{
					// check if chunklink is this?
					var minivoxEntity = destroyEntities[i];
					PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, minivoxEntity);
					var multiVoxelPositions2 = multiVoxelPositions[minivoxEntity];
					var multiChunkLinks2 = multiChunkLinks[minivoxEntity];
					for (int j = 0; j < multiVoxelPositions2.positions.Length; j++)
					{
						grassesRemoved.Add(multiVoxelPositions2.positions[j]);
						grassesChunksRemoved.Add(multiChunkLinks2.chunks[j]);
					}
				}
				// spawned events
				if (grassesAdded.Length > 0)
				{
					PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, bigMinivoxesSpawnedPrefab),
						new OnBigMinivoxesSpawned(in grassesChunksAdded, in grassesAdded));
                	//UnityEngine.Debug.LogError("Spawned BigMinivoxes: " + grassesChunksAdded.Length + " :: " + grassesAdded.Length);
				}
				if (grassesRemoved.Length > 0)
				{
					PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, bigMinivoxesRemovedPrefab),
						new OnBigMinivoxesRemoved(in grassesChunksRemoved, in grassesRemoved));
                	//UnityEngine.Debug.LogError("Removed BigMinivoxes: " + grassesChunksRemoved.Length + " :: "  + grassesRemoved.Length);
				}
				// Dispose Originals``
				bigMinivoxVoxelTypes.Dispose();
				bigMinivoxPositions.Dispose();
				bigMinivoxRotations.Dispose();
				bigMinivoxTypes.Dispose();
				bigMinivoxLocalVoxelPositions.Dispose();
				bigMinivoxRotations2.Dispose();
				voxels.Dispose();
                chunkNeighbors2.Dispose();
				grassesAdded.Dispose();
				grassesChunksAdded.Dispose();
				grassesRemoved.Dispose();
				grassesChunksRemoved.Dispose();
				spawnGlobalPositions.Dispose();
				destroyEntities.Dispose();
            })  .WithReadOnly(worldVoxelLinks)
                .WithReadOnly(worldRealmLinks)
				.WithReadOnly(voxScales)
				.WithReadOnly(chunks)
                .WithReadOnly(chunkPositions)
                .WithReadOnly(chunkLights2)
                .WithReadOnly(minivoxLinks2)
                .WithReadOnly(chunkVoxelRotations)
				.WithReadOnly(chunkHasMinivoxes)
				.WithReadOnly(voxels2)
                .WithReadOnly(voxelsVoxData)
                .WithReadOnly(generateVoxDatas)
                .WithReadOnly(blockDimensions)
				.WithReadOnly(voxelChunkDimensions)
				.WithReadOnly(minivoxPlaceOffsets)
				.WithReadOnly(multiVoxelPositions)
				.WithReadOnly(multiChunkLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}
					// Gets set in next frame
					/*#if DEBUG_BIGMINIVOXES_HEALTH
					UnityEngine.Debug.LogError("Spawning Big Minivox: " + bigMinivoxLink.positions[0] + " - " + bigMinivoxLink.positions[1]);
					#endif*/

					//! If already contained BigMinivoxLink then add previous and continue.
					//! \todo Check all overlapping chunks for the link to this particular minivox
					/*if (overlappingChunks.Length == 0)
					{
						if (minivoxLinks.HasCombinedLink(minivoxGlobalPosition))
						{
							// UnityEngine.Debug.LogError("Big Minivox Already Contains BigMinivox: " + minivoxGlobalPosition);
							bigMinivoxes.Add(minivoxLinks.GetBigMinivoxLink(minivoxGlobalPosition).Clone());
							positionOffsets.Dispose();
							continue;
						}
					}
					else
					{
						var didHaveBigMinivox = false;
						var minivoxLinks3 = minivoxLinks;
						for (int j = 0; j < overlappingChunks.Length; j++)
						{
							var chunkEntity2 = overlappingChunks[j];
							minivoxLinks3 = minivoxLinks2[chunkEntity2];
							if (minivoxLinks3.HasCombinedLink(minivoxGlobalPosition))
							{
								didHaveBigMinivox = true;
								break;
							}
						}
						if (didHaveBigMinivox)
						{
							#if DEBUG_BIG_MINIVOX_MULTICHUNK
							UnityEngine.Debug.LogError("Big Minivox Already Contains BigMinivox: " + minivoxGlobalPosition);
							#endif
							bigMinivoxes.Add(minivoxLinks3.GetBigMinivoxLink(minivoxGlobalPosition).Clone());
							positionOffsets.Dispose();
							continue;
						}
					}*/