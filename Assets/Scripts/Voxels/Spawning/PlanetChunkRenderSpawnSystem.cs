using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Voxels.Lighting;

namespace Zoxel.Voxels
{
    //! Spawns ChunkRender entities from a Chunk!
    /**
    *   - Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class PlanetChunkRenderSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;
        private EntityQuery chunkRenderQuery;
        private EntityQuery voxelQuery;
        private EntityQuery materialsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<Planet>());
            chunkRenderQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<ChunkRender>(),
                ComponentType.ReadOnly<MaterialLink>());
            voxelQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Voxel>());
            materialsQuery = GetEntityQuery(
                ComponentType.ReadOnly<DestroyEntity>(),
                ComponentType.Exclude<MaterialData>());
            RequireForUpdate(processQuery);
            RequireForUpdate(voxesQuery);
            RequireForUpdate(voxelQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var planetChunkRenderPrefab = VoxelSystemGroup.planetChunkRenderPrefab;
            var planetChunkRenderDisabledPrefab = VoxelSystemGroup.planetChunkRenderDisabledPrefab;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<ChunkSpawnRenders>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxelLinks = GetComponentLookup<VoxelLinks>(true);
            voxEntities.Dispose();
            var chunkRenderEntities = chunkRenderQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var chunkRenderMaterialLinks = GetComponentLookup<MaterialLink>(true);
			chunkRenderEntities.Dispose();
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var materialLinks = GetComponentLookup<MaterialLink>(true);
            voxelEntities.Dispose();
            var materialEntities = materialsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var materialDatas = GetComponentLookup<MaterialData>(true);
            materialEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithNone<OnChunkRendersSpawned>()
                .WithAll<ChunkSpawnRenders, PlanetChunk>()
                .ForEach((Entity e, int entityInQueryIndex, in Chunk chunk, in ChunkPosition chunkPosition, in ChunkRenderLinks chunkRenderLinks, in VoxLink voxLink) =>
            {
                if (!voxelLinks.HasComponent(voxLink.vox))
                {
                    return;
                }
                if (chunkRenderLinks.chunkRenders.Length == 0)
                {
                    var isDisabled = HasComponent<DisableRender>(e);
                    PlanetChunkRenderSpawnSystem.SpawnChunkRenders(PostUpdateCommands, entityInQueryIndex, e,
                        planetChunkRenderPrefab, planetChunkRenderDisabledPrefab, isDisabled,
                        in chunkPosition, in chunk, in voxLink, in chunkRenderLinks,
                        in voxelLinks, in materialLinks, in chunkRenderMaterialLinks, in materialDatas);
                }
            })  .WithReadOnly(voxelLinks)
                .WithReadOnly(materialLinks)
                .WithReadOnly(chunkRenderMaterialLinks)
                .WithReadOnly(materialDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        public static void SpawnChunkRenders(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e,
            Entity planetChunkRenderPrefab, Entity planetChunkRenderDisabledPrefab, bool isDisabled,
            in ChunkPosition chunkPosition, in Chunk chunk, in VoxLink voxLink, in ChunkRenderLinks chunkRenderLinks,
            in ComponentLookup<VoxelLinks> voxelLinks, in ComponentLookup<MaterialLink> materialLinks,
            in ComponentLookup<MaterialLink> chunkRenderMaterialLinks, in ComponentLookup<MaterialData> materialDatas)
        {
            var materials = new NativeList<Entity>();
            var materialDatas2 = new NativeList<MaterialData>();
            var chunkRendersCount = (byte) 1;
            if (voxelLinks.HasComponent(voxLink.vox))
            {
                var voxels = voxelLinks[voxLink.vox].voxels;
                var voxelTypes = new NativeList<byte>();
                for (int i = 0; i < chunk.voxels.Length; i++)
                {
                    var voxelIndex = chunk.voxels[i];
                    if (voxelIndex != 0)
                    {
                        var nonAirIndex = (byte)(voxelIndex - 1);
                        if (!voxelTypes.Contains(nonAirIndex))
                        {
                            voxelTypes.Add(nonAirIndex);
                        }
                    }
                }
                for (int i = 0; i < voxelTypes.Length; i++)
                {
                    var voxelIndex = voxelTypes[i];
                    // get voxel meta
                    var voxelEntity = voxels[voxelIndex];
                    if (!materialLinks.HasComponent(voxelEntity))
                    {
                        continue;
                    }
                    var materialEntity = materialLinks[voxelEntity].material;
                    if (!materialDatas.HasComponent(materialEntity))
                    {
                        continue;
                    }
                    if (!materials.Contains(materialEntity))
                    {
                        materials.Add(materialEntity);
                        materialDatas2.Add(materialDatas[materialEntity]);
                    }
                }
                chunkRendersCount = (byte) materials.Length;
                voxelTypes.Dispose();
            }
            if (chunkRendersCount != chunkRenderLinks.chunkRenders.Length)
            {
                // UnityEngine.Debug.LogError("Chunk Render Counts has changed.");
                var keepMaterials = new NativeList<Entity>();
                for (int i = 0; i < chunkRenderLinks.chunkRenders.Length; i++)
                {
                    var chunkRenderEntity = chunkRenderLinks.chunkRenders[i];
                    if (!chunkRenderMaterialLinks.HasComponent(chunkRenderEntity))
                    {
                        continue;
                    }
                    var materialEntity = chunkRenderMaterialLinks[chunkRenderEntity].material;
                    // if material has been removed from chunk!
                    if (!materials.Contains(materialEntity))
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, chunkRenderEntity);
                    }
                    else
                    {
                        PostUpdateCommands.AddComponent<NewChunkRender>(entityInQueryIndex, chunkRenderEntity);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, chunkRenderEntity, new ChunkRenderIndex((byte) keepMaterials.Length));
                        keepMaterials.Add(materialEntity);
                    }
                }
                var voxelDimensions = chunk.voxelDimensions;
                var prefab = planetChunkRenderPrefab;
                if (isDisabled)
                {
                    prefab = planetChunkRenderDisabledPrefab;
                }
                var chunkLink = new ChunkLink(e);
                var j = 0;
                for (int i = 0; i < materials.Length; i++)
                {
                    var materialEntity = materials[i];
                    // if doesnt exists
                    if (keepMaterials.Contains(materialEntity))
                    {
                        continue;
                    }
                    // spawn new ChunkRender
                    var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ChunkSides(voxelDimensions));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ZoxMatrix(chunkPosition.position, voxelDimensions));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new MaterialLink(materialEntity));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ChunkRenderIndex((byte)(j + keepMaterials.Length)));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, chunkLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, chunkPosition);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, voxLink);
                    var materialData = materialDatas2[i];
                    if (materialData.type == MaterialType.Transparent)
                    {
                        PostUpdateCommands.AddComponent<WaterRender>(entityInQueryIndex, e2);
                    }
                    else if (materialData.type == MaterialType.Translucent)
                    {
                        PostUpdateCommands.AddComponent<TranslucentRender>(entityInQueryIndex, e2);
                    }
                    j++;
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChunkRendersSpawned(chunkRendersCount));
                keepMaterials.Dispose();
            }
            materials.Dispose();
            materialDatas2.Dispose();
        }
    }
}