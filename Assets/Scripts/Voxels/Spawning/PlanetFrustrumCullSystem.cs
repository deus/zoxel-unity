using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Cameras;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Rendering.Authoring;
using Zoxel.Voxels.Minivoxes;
using Zoxel.Characters.World;

namespace Zoxel.Voxels
{
    //! Culls Chunk Renders with camera view!
    /**
    *   Adds and Removes DisableRender based on bounds.
    *       - Handles checks for multiple cameras.
    *       - Generates camera frustrum planes in another system
    *   \todo Handle BigMinivox culling as well.
    */
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class PlanetFrustrumCullSystem : SystemBase
	{
        private UnityEngine.Matrix4x4 lastMatrix;
        private int lastPlanetChunkCount = 0;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery camerasQuery;
        private EntityQuery planetsQuery;
        private EntityQuery planetChunkRendersQuery;
        private EntityQuery grassQuery;
        private EntityQuery minivoxQuery;
        private EntityQuery minivoxChunkQuery;
        private EntityQuery charactersQuery;
        private EntityQuery characterChunksQuery;
        private EntityQuery destroyEntitiesQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<EntityBusy>(),
                ComponentType.ReadOnly<PlanetChunk>(),
                ComponentType.ReadOnly<ChunkPosition>());
            camerasQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Camera>(),
                ComponentType.ReadWrite<CameraFrustrumPlanes>(),
                ComponentType.ReadOnly<CameraEnabledState>());
            planetsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<VoxScale>());
            planetChunkRendersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<PlanetRender>());
            grassQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Grass>(),
                ComponentType.ReadOnly<Childrens>());
            minivoxQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Minivox>(),
                ComponentType.ReadOnly<Vox>());
            minivoxChunkQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<MinivoxChunk>(),
                ComponentType.ReadOnly<ChunkRenderLinks>());
            charactersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Character>(),
                ComponentType.ReadOnly<Vox>());
            characterChunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<ModelChunk>(),
                ComponentType.ReadOnly<ChunkRenderLinks>());
            destroyEntitiesQuery = GetEntityQuery(ComponentType.ReadOnly<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate<RenderSettings>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var renderSettings = GetSingleton<RenderSettings>();
            if (renderSettings.disableCulling)
            {
                return;
            }
            var planetChunkCount = processQuery.CalculateEntityCount();
            var didChangePlanetChunkCount = lastPlanetChunkCount != planetChunkCount;
            lastPlanetChunkCount = planetChunkCount;
            var disableCharacterRenderCulling = renderSettings.disableCharacterRenderCulling;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var cameraEntities = camerasQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleZ).AsArray();
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleZ);
            var cameraFrustrumPlanes = GetComponentLookup<CameraFrustrumPlanes>(true);
            var chunkEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var minivoxLinks2 = GetComponentLookup<MinivoxLinks>(true);
            var chunkCharacters = GetComponentLookup<ChunkCharacters>(true);
            chunkEntities.Dispose();
            var planetEntities = planetsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleY);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleY);
            var voxScales = GetComponentLookup<VoxScale>(true);
            var chunkDimensions = GetComponentLookup<ChunkDimensions>(true);
            planetEntities.Dispose();
            var grassEntities = grassQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var grassChildrens = GetComponentLookup<Childrens>(true);
            grassEntities.Dispose();
            var minivoxEntities = minivoxQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var minivoxVoxes = GetComponentLookup<Vox>(true);
            var bigMinivoxes = GetComponentLookup<BigMinivox>(true);
            minivoxEntities.Dispose();
            var minivoxChunkEntities = minivoxChunkQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var minivoxChunkRenderLinks = GetComponentLookup<ChunkRenderLinks>(true);
            minivoxChunkEntities.Dispose();
            var characterEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleE);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleE);
            var characterVoxes = GetComponentLookup<Vox>(true);
            characterEntities.Dispose();
            var characterChunkEntities = characterChunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleF);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleF);
            var characterChunkRenderLinks = GetComponentLookup<ChunkRenderLinks>(true);
            characterChunkEntities.Dispose();
            var destroyEntitiesEntities = destroyEntitiesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleG);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleG);
            var destroyEntitys = GetComponentLookup<DestroyEntity>(true);
            destroyEntitiesEntities.Dispose();
            // Add Disables
			Dependency = Entities
                .WithNone<DestroyEntity, InitializeEntity, EntityBusy>()
                .WithNone<DisableRender>()
				.WithAll<PlanetChunk>()
				.ForEach((Entity e, int entityInQueryIndex, in ChunkPosition chunkPosition, in ChunkRenderLinks chunkRenderLinks, in VoxLink voxLink) =>
			{
                if (!voxScales.HasComponent(voxLink.vox))
                {
                    return;
                }
                var voxScale = voxScales[voxLink.vox].scale;
                var voxelDimensions = chunkDimensions[voxLink.vox].voxelDimensions;
                var scaledDimensions = voxelDimensions.ToFloat3();
                scaledDimensions.x *= voxScale.x;
                scaledDimensions.y *= voxScale.y;
                scaledDimensions.z *= voxScale.z;
                var position = chunkPosition.GetRealPosition(voxelDimensions, voxScale);
                var bounds = new UnityEngine.Bounds(position + scaledDimensions / 2f, scaledDimensions);
                if (TestPlanesAABB(in cameraEntities, in cameraFrustrumPlanes, in bounds))
                {
                    return;
                }
                var changeDisableRenderEntities = new NativeList<Entity>();
                changeDisableRenderEntities.Add(e);
                for (int i = 0; i < chunkRenderLinks.chunkRenders.Length; i++)
                {
                    var renderEntity = chunkRenderLinks.chunkRenders[i];
                    if (destroyEntitys.HasComponent(renderEntity))
                    {
                        continue;
                    }
                    changeDisableRenderEntities.Add(renderEntity);
                }
                if (minivoxLinks2.HasComponent(e))
                {
                    var minivoxLinks = minivoxLinks2[e];
                    AddMinivoxLinks(in minivoxLinks, ref changeDisableRenderEntities, in destroyEntitys, in grassChildrens,
                        in minivoxVoxes, in minivoxChunkRenderLinks, in bigMinivoxes);
                }
                if (!disableCharacterRenderCulling && chunkCharacters.HasComponent(e))
                {
                    var chunkCharacters2 = chunkCharacters[e];
                    AddCharacters(in chunkCharacters2, ref changeDisableRenderEntities, in destroyEntitys, in characterVoxes, in characterChunkRenderLinks);
                }
                var disableEntitiesArray = changeDisableRenderEntities.AsArray();
                PostUpdateCommands.AddComponent<DisableRender>(entityInQueryIndex, disableEntitiesArray);
                disableEntitiesArray.Dispose();
                changeDisableRenderEntities.Dispose();
			})  .WithReadOnly(cameraEntities)
                .WithReadOnly(cameraFrustrumPlanes)
                .WithReadOnly(chunkDimensions)
                .WithReadOnly(voxScales)
                .WithReadOnly(grassChildrens)
                .WithReadOnly(minivoxVoxes)
                .WithReadOnly(bigMinivoxes)
                .WithReadOnly(minivoxChunkRenderLinks)
                .WithReadOnly(minivoxLinks2)
                .WithReadOnly(chunkCharacters)
                .WithReadOnly(characterVoxes)
                .WithReadOnly(characterChunkRenderLinks)
                .WithReadOnly(destroyEntitys)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // Remove Disables
			Dependency = Entities
                .WithNone<DestroyEntity, InitializeEntity, EntityBusy>()
                .WithAll<DisableRender, PlanetChunk>()
				.ForEach((Entity e, int entityInQueryIndex, in ChunkPosition chunkPosition, in ChunkRenderLinks chunkRenderLinks, in VoxLink voxLink) =>
			{
                if (!voxScales.HasComponent(voxLink.vox))
                {
                    return;
                }
                var voxScale = voxScales[voxLink.vox].scale;
                var voxelDimensions = chunkDimensions[voxLink.vox].voxelDimensions;
                var scaledDimensions = voxelDimensions.ToFloat3();
                scaledDimensions.x *= voxScale.x;
                scaledDimensions.y *= voxScale.y;
                scaledDimensions.z *= voxScale.z;
                var position = chunkPosition.GetRealPosition(voxelDimensions, voxScale);
                var bounds = new UnityEngine.Bounds(position + scaledDimensions / 2f, scaledDimensions);
                if (!TestPlanesAABB(in cameraEntities, in cameraFrustrumPlanes, in bounds))
                {
                    return;
                }
                var changeDisableRenderEntities = new NativeList<Entity>();
                changeDisableRenderEntities.Add(e);
                for (int i = 0; i < chunkRenderLinks.chunkRenders.Length; i++)
                {
                    var renderEntity = chunkRenderLinks.chunkRenders[i];
                    if (destroyEntitys.HasComponent(renderEntity))
                    {
                        continue;
                    }
                    changeDisableRenderEntities.Add(renderEntity);
                }
                if (minivoxLinks2.HasComponent(e))
                {
                    var minivoxLinks = minivoxLinks2[e];
                    AddMinivoxLinks(in minivoxLinks, ref changeDisableRenderEntities, in destroyEntitys, in grassChildrens,
                        in minivoxVoxes, in minivoxChunkRenderLinks, in bigMinivoxes);
                }
                if (!disableCharacterRenderCulling && chunkCharacters.HasComponent(e))
                {
                    var chunkCharacters2 = chunkCharacters[e];
                    AddCharacters(in chunkCharacters2, ref changeDisableRenderEntities, in destroyEntitys, in characterVoxes, in characterChunkRenderLinks);
                }
                var enableEntitiesArray = changeDisableRenderEntities.AsArray();
                PostUpdateCommands.RemoveComponent<DisableRender>(entityInQueryIndex, enableEntitiesArray);
                enableEntitiesArray.Dispose();
                changeDisableRenderEntities.Dispose();
			})  .WithReadOnly(cameraEntities)
                .WithReadOnly(cameraFrustrumPlanes)
                .WithDisposeOnCompletion(cameraEntities)
                .WithReadOnly(chunkDimensions)
                .WithReadOnly(voxScales)
                .WithReadOnly(grassChildrens)
                .WithReadOnly(minivoxVoxes)
                .WithReadOnly(bigMinivoxes)
                .WithReadOnly(minivoxChunkRenderLinks)
                .WithReadOnly(minivoxLinks2)
                .WithReadOnly(chunkCharacters)
                .WithReadOnly(characterVoxes)
                .WithReadOnly(characterChunkRenderLinks)
                .WithReadOnly(destroyEntitys)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}

        private static void AddCharacters(in ChunkCharacters chunkCharacters, ref NativeList<Entity> disableEntities,
            in ComponentLookup<DestroyEntity> destroyEntitys,
            in ComponentLookup<Vox> characterVoxes,
            in ComponentLookup<ChunkRenderLinks> characterChunkRenderLinks)
        {
            for (int i = 0; i < chunkCharacters.characters.Length; i++)
            {
                var characterEntity = chunkCharacters.characters[i];
                if (destroyEntitys.HasComponent(characterEntity))
                {
                    continue;
                }
                else if (characterVoxes.HasComponent(characterEntity))
                {
                    disableEntities.Add(characterEntity);
                    var characterVox = characterVoxes[characterEntity];
                    for (int j = 0; j < characterVox.chunks.Length; j++)
                    {
                        var chunkEntity = characterVox.chunks[j];
                        if (characterChunkRenderLinks.HasComponent(chunkEntity))
                        {
                            var characterChunkRenderLinks2 = characterChunkRenderLinks[chunkEntity];
                            for (int k = 0; k < characterChunkRenderLinks2.chunkRenders.Length; k++)
                            {
                                var characterRenderEntity = characterChunkRenderLinks2.chunkRenders[k];
                                if (characterRenderEntity.Index > 0 && !destroyEntitys.HasComponent(characterRenderEntity))
                                {
                                    disableEntities.Add(characterRenderEntity);
                                }
                            }
                        }
                    }
                }
            }
        }

        private static void AddMinivoxLinks(in MinivoxLinks minivoxLinks, ref NativeList<Entity> disableEntities,
            in ComponentLookup<DestroyEntity> destroyEntitys,
            in ComponentLookup<Childrens> grassChildrens,
            in ComponentLookup<Vox> minivoxVoxes,
            in ComponentLookup<ChunkRenderLinks> minivoxChunkRenderLinks,
            in ComponentLookup<BigMinivox> bigMinivoxes)
        {
            if (minivoxLinks.minivoxes.IsCreated)
            {
                foreach (var KVP in minivoxLinks.minivoxes)
                {
                    var minivoxEntity = KVP.Value;
                    if (destroyEntitys.HasComponent(minivoxEntity))
                    {
                        continue;
                    }
                    else if (grassChildrens.HasComponent(minivoxEntity))
                    {
                        var grassChildren = grassChildrens[minivoxEntity];
                        for (int i = 0; i < grassChildren.children.Length; i++)
                        {
                            disableEntities.Add(grassChildren.children[i]);
                        }
                    }
                    else if (minivoxVoxes.HasComponent(minivoxEntity) && !bigMinivoxes.HasComponent(minivoxEntity))
                    {
                        disableEntities.Add(minivoxEntity);
                        var minivoxVox = minivoxVoxes[minivoxEntity];
                        if (minivoxVox.chunks.Length > 0)
                        {
                            var minivoxChunkEntity = minivoxVox.chunks[0];
                            if (minivoxChunkEntity.Index > 0)
                            {
                                var minivoxChunkRenderLinks2 = minivoxChunkRenderLinks[minivoxChunkEntity];
                                if (minivoxChunkRenderLinks2.chunkRenders.Length > 0)
                                {
                                    var minivoxRenderEntity = minivoxChunkRenderLinks2.chunkRenders[0];
                                    if (minivoxRenderEntity.Index > 0 && !destroyEntitys.HasComponent(minivoxRenderEntity))
                                    {
                                        disableEntities.Add(minivoxRenderEntity);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        //! Add Disables if true
        public static bool TestPlanesAABB(in NativeArray<Entity> cameraEntities, in ComponentLookup<CameraFrustrumPlanes> cameraFrustrumPlanes,
            in UnityEngine.Bounds bounds)
        {
            for (byte i = 0; i < cameraEntities.Length; i++)
            {
                var cameraEntity = cameraEntities[i];
                var doesCameraSeeThing = true;
                var cameraFrustrumPlanes2 = cameraFrustrumPlanes[cameraEntity];
                for (byte j = 0; j < cameraFrustrumPlanes2.planes.Length; j++)
                {
                    var plane = cameraFrustrumPlanes2.planes[j];
                    float3 normal_sign = math.sign(plane.normal);
                    float3 test_point = (float3)(bounds.center) + (bounds.extents * normal_sign);
                    var dot = math.dot(test_point, plane.normal);
                    if (dot + plane.distance < 0)
                    {
                        doesCameraSeeThing = false;
                        break;
                    }
                }
                if (doesCameraSeeThing)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool TestPlanesAABB2(in NativeArray<Entity> cameraEntities, in ComponentLookup<CameraFrustrumPlanes> cameraFrustrumPlanes,
            in UnityEngine.Bounds bounds)
        {
            for (byte i = 0; i < cameraEntities.Length; i++)
            {
                var cameraEntity = cameraEntities[i];
                var cameraFrustrumPlanes2 = cameraFrustrumPlanes[cameraEntity];
                for (byte j = 0; j < cameraFrustrumPlanes2.planes.Length; j++)
                {
                    var plane = cameraFrustrumPlanes2.planes[j];
                    float3 normal_sign = math.sign(plane.normal);
                    float3 test_point = (float3)(bounds.center) + (bounds.extents * normal_sign);
                    var dot = math.dot(test_point, plane.normal);
                    if (dot + plane.distance < 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        void CalculateFrustumPlanes(UnityEngine.Matrix4x4 mat, ref NativeArray<UnityEngine.Plane> planes)
        {
            // left
            var planeLeft = new UnityEngine.Plane();
            planeLeft.normal = new UnityEngine.Vector3(mat.m30 + mat.m00, mat.m31 + mat.m01, mat.m32 + mat.m02);
            planeLeft.distance = mat.m33 + mat.m03;
            planes[0] = planeLeft;
        
            // right
            var planeRight = new UnityEngine.Plane();
            planeRight.normal = new UnityEngine.Vector3(mat.m30 - mat.m00, mat.m31 - mat.m01, mat.m32 - mat.m02);
            planeRight.distance = mat.m33 - mat.m03;
            planes[1] = planeRight;
        
            // bottom
            var plane = new UnityEngine.Plane();
            plane.normal = new UnityEngine.Vector3(mat.m30 + mat.m10, mat.m31 + mat.m11, mat.m32 + mat.m12);
            plane.distance = mat.m33 + mat.m13;
            planes[2] = plane;
        
            // top
            plane = new UnityEngine.Plane();
            plane.normal = new UnityEngine.Vector3(mat.m30 - mat.m10, mat.m31 - mat.m11, mat.m32 - mat.m12);
            plane.distance = mat.m33 - mat.m13;
            planes[3] = plane;
        
            // near
            plane = new UnityEngine.Plane();
            plane.normal = new UnityEngine.Vector3(mat.m30 + mat.m20, mat.m31 + mat.m21, mat.m32 + mat.m22);
            plane.distance = mat.m33 + mat.m23;
            planes[4] = planeLeft;
        
            // far
            plane = new UnityEngine.Plane();
            plane.normal = new UnityEngine.Vector3(mat.m30 - mat.m20, mat.m31 - mat.m21, mat.m32 - mat.m22);
            plane.distance = mat.m33 - mat.m23;
            planes[5] = plane;
        
            // normalize
            for (var i = 0; i < 6; i++)
            {
                plane = planes[i];
                var length = plane.normal.magnitude;
                plane.normal /= length;
                plane.distance /= length;
                planes[i] = plane;
            }
        }
	}
}
        
        /*public enum TestPlanesResults
        {
            /// <summary>
            /// The AABB is completely in the frustrum.
            /// </summary>
            Inside = 0,
            /// <summary>
            /// The AABB is partially in the frustrum.
            /// </summary>
            Intersect,
            /// <summary>
            /// The AABB is completely outside the frustrum.
            /// </summary>
            Outside
        }
        public static TestPlanesResults TestPlanesAABB(in NativeArray<UnityEngine.Plane> planes, UnityEngine.Bounds bounds)
        {
            var testIntersection = true;
            UnityEngine.Vector3 vmin, vmax;
            var boundsMin = bounds.min;
            var boundsMax = bounds.max;
            var testResult = TestPlanesResults.Inside;
 
            for(int planeIndex = 0; planeIndex < planes.Length; planeIndex++)
            {
                var normal          = planes [planeIndex].normal;
                var planeDistance   = planes [planeIndex].distance;
 
                // X axis
                if(normal.x < 0)
                {
                    vmin.x = boundsMin.x;
                    vmax.x = boundsMax.x;
                }
                else
                {
                    vmin.x = boundsMax.x;
                    vmax.x = boundsMin.x;
                }
 
                // Y axis
                if (normal.y < 0)
                {
                    vmin.y = boundsMin.y;
                    vmax.y = boundsMax.y;
                }
                else
                {
                    vmin.y = boundsMax.y;
                    vmax.y = boundsMin.y;
                }
 
                // Z axis
                if (normal.z < 0)
                {
                    vmin.z = boundsMin.z;
                    vmax.z = boundsMax.z;
                }
                else
                {              
                    vmin.z = boundsMax.z;
                    vmax.z = boundsMin.z;
                }
 
                var dot1 = normal.x * vmin.x + normal.y * vmin.y + normal.z * vmin.z;
                if (dot1 + planeDistance < 0)
                    return TestPlanesResults.Outside;
 
                if (testIntersection)
                {
                    var dot2 = normal.x * vmax.x + normal.y * vmax.y + normal.z * vmax.z;
                    if (dot2 + planeDistance <= 0)
                        testResult = TestPlanesResults.Intersect;
                }
            }
 
            return testResult;
        }*/

            /*var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
			Entities
                .WithNone<DisableRender>()
				.WithAll<PlanetRender>()
				.ForEach((Entity e, in ChunkPosition chunkPosition) =>
			{
                var bounds = new UnityEngine.Bounds(chunkPosition.GetVoxelPosition(voxelDimensions).ToFloat3(), voxelDimensionsFloat3);
                if (!UnityEngine.GeometryUtility.TestPlanesAABB(planes, bounds))
                {
                    PostUpdateCommands.AddComponent<DisableRender>(e);
                }
			}).WithoutBurst().Run();
			Entities
                .WithAll<DisableRender>()
				.WithAll<PlanetRender>()
				.ForEach((Entity e, in ChunkPosition chunkPosition) =>
			{
                var bounds = new UnityEngine.Bounds(chunkPosition.GetVoxelPosition(voxelDimensions).ToFloat3(), voxelDimensionsFloat3);
                if (UnityEngine.GeometryUtility.TestPlanesAABB(planes, bounds))
                {
                    PostUpdateCommands.RemoveComponent<DisableRender>(e);
                }
			}).WithoutBurst().Run();*/
            // Check if any camera changed
            /*var camera = CameraReferences.GetMainCamera(EntityManager);
            if (camera == null || (!didChangePlanetChunkCount && camera.cameraToWorldMatrix == lastMatrix))
            {
                return;
            }
            lastMatrix = camera.cameraToWorldMatrix;*/

            /*var planes = new NativeArray<UnityEngine.Plane>(6, Allocator.TempJob);
            // CalculateFrustumPlanes(camera.projectionMatrix, ref planes);
            //var unityPlanes = UnityEngine.GeometryUtility.CalculateFrustumPlanes(camera.projectionMatrix);
            var unityPlanes = UnityEngine.GeometryUtility.CalculateFrustumPlanes(camera);
            for (int i = 0; i < unityPlanes.Length; i++)
            {
                planes[i] = unityPlanes[i];
            }*/
            /*var planes = new NativeArray<UnityEngine.Plane>(6 * cameraEntities.Length, Allocator.TempJob);
            for (int i = 0; i < cameraEntities.Length; i++)
            {
                // var unityPlanes = UnityEngine.GeometryUtility.CalculateFrustumPlanes(camera);
                var cameraEntity = cameraEntities[i];
                var cameraFrustrumPlanes2 = cameraFrustrumPlanes[];
                for (int j = 0; j < cameraFrustrumPlanes2.planes.Length; j++)
                {
                    planes[i * 6 + j] = cameraFrustrumPlanes2.planes[j];
                }
            }
            cameraEntities.Dispose();*/
                    /*if (minivoxLinks.minivoxes.IsCreated)
                    {
                        foreach (var KVP in minivoxLinks.minivoxes)
                        {
                            var minivoxEntity = KVP.Value;
                            if (destroyEntitys.HasComponent(minivoxEntity))
                            {
                                continue;
                            }
                            else if (grassChildrens.HasComponent(minivoxEntity))
                            {
                                var grassChildren = grassChildrens[minivoxEntity];
                                for (int i = 0; i < grassChildren.children.Length; i++)
                                {
                                    changeDisableRenderEntities.Add(grassChildren.children[i]);
                                }
                            }
                            else if (minivoxVoxes.HasComponent(minivoxEntity) && !HasComponent<BigMinivox>(minivoxEntity))
                            {
                                changeDisableRenderEntities.Add(minivoxEntity);
                                var minivoxVox = minivoxVoxes[minivoxEntity];
                                if (minivoxVox.chunks.Length > 0)
                                {
                                    var minivoxChunkEntity = minivoxVox.chunks[0];
                                    if (minivoxChunkEntity.Index > 0)
                                    {
                                        var minivoxChunkRenderLinks2 = minivoxChunkRenderLinks[minivoxChunkEntity];
                                        if (minivoxChunkRenderLinks2.chunkRenders.Length > 0)
                                        {
                                            var minivoxRenderEntity = minivoxChunkRenderLinks2.chunkRenders[0];
                                            if (minivoxRenderEntity.Index > 0 && !destroyEntitys.HasComponent(minivoxRenderEntity))
                                            {
                                                changeDisableRenderEntities.Add(minivoxRenderEntity);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }*/
                    /*for (int i = 0; i < chunkCharacters2.characters.Length; i++)
                    {
                        var characterEntity = chunkCharacters2.characters[i];
                        if (destroyEntitys.HasComponent(characterEntity))
                        {
                            continue;
                        }
                        else if (characterVoxes.HasComponent(characterEntity))
                        {
                            changeDisableRenderEntities.Add(characterEntity);
                            var characterVox = characterVoxes[characterEntity];
                            for (int j = 0; j < characterVox.chunks.Length; j++)
                            {
                                var chunkEntity = characterVox.chunks[j];
                                if (characterChunkRenderLinks.HasComponent(chunkEntity))
                                {
                                    var characterChunkRenderLinks2 = characterChunkRenderLinks[chunkEntity];
                                    for (int k = 0; k < characterChunkRenderLinks2.chunkRenders.Length; k++)
                                    {
                                        var characterRenderEntity = characterChunkRenderLinks2.chunkRenders[k];
                                        if (characterRenderEntity.Index > 0 && !destroyEntitys.HasComponent(characterRenderEntity))
                                        {
                                            changeDisableRenderEntities.Add(characterRenderEntity);
                                        }
                                    }
                                }
                            }
                        }
                    }*/