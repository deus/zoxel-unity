using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Voxels
{
	//! Begins chunk updates.
	/**
	*	This system also spawns/destroys chunk renders based on materials during ChunkBuilder step.
	*	\todo Removes Spawning from other system and just use this for map chunks.
	*	\todo Add collision components to non water chunk renders. Or just create ChunkCollision entities.
	*/
	[BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
    public partial class ChunkBuildBeginSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;
        private EntityQuery chunkRenderQuery;
        private EntityQuery voxelQuery;
        private EntityQuery materialsQuery;
        // private EntityQuery chunksGeneratingQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<DeadEntity>(),
                ComponentType.ReadOnly<Vox>());
            chunkRenderQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<ChunkRender>(),
                ComponentType.ReadOnly<MaterialLink>());
            voxelQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Voxel>());
            materialsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<MaterialData>());
            /*chunksGeneratingQuery = GetEntityQuery(
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<EntityGenerating>());*/
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var planetChunkRenderPrefab = VoxelSystemGroup.planetChunkRenderPrefab;
            var planetChunkRenderDisabledPrefab = VoxelSystemGroup.planetChunkRenderDisabledPrefab;
			var elapsedTime = World.Time.ElapsedTime;
			var disableVoxelLighting = VoxelManager.instance.voxelSettings.disableVoxelLighting;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxelLinks = GetComponentLookup<VoxelLinks>(true);
			voxEntities.Dispose();
            var chunkRenderEntities = chunkRenderQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var chunkRenderMaterialLinks = GetComponentLookup<MaterialLink>(true);
			chunkRenderEntities.Dispose();
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var materialLinks = GetComponentLookup<MaterialLink>(true);
			voxelEntities.Dispose();
            var materialEntities = materialsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var materialDatas = GetComponentLookup<MaterialData>(true);
			materialEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<LoadChunk, EdgeChunk>()
				.WithNone<InitializeEntity, ChunkSpawnRenders, OnChunkRendersSpawned>()
				.WithNone<DelayChunkBuilder, UpdateChunkNeighbors, EntityGenerating>() // GenerateTrees>()
				.WithAll<ChunkBuilder, PlanetChunk>()
				.ForEach((Entity e, int entityInQueryIndex, in Chunk chunk, in ChunkRenderLinks chunkRenderLinks, in ChunkPosition chunkPosition, in VoxLink voxLink) =>
			{
				#if DEBUG_VOXEL_PLACEMENT
				UnityEngine.Debug.LogError(e.Index + " - Voxel Placement - ChunkBuildState.Begin: " + elapsedTime);
				#endif
                var isDisabled = HasComponent<DisableRender>(e);
                PlanetChunkRenderSpawnSystem.SpawnChunkRenders(PostUpdateCommands, entityInQueryIndex, e,
                    planetChunkRenderPrefab, planetChunkRenderDisabledPrefab, isDisabled,
                    in chunkPosition, in chunk, in voxLink, in chunkRenderLinks,
                    in voxelLinks, in materialLinks, in chunkRenderMaterialLinks, in materialDatas);
			})	.WithReadOnly(voxelLinks)
                .WithReadOnly(materialLinks)
                .WithReadOnly(chunkRenderMaterialLinks)
                .WithReadOnly(materialDatas)
				.ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}

//! \todo Pass in voxels, check amount of materials used. Spawn or destroy chunk renders based on all voxel materials.
// Supporting: Diffuse, Water, Translucent (for Leaves, with missing parts, or window textures)
// Pass in previous chunk renders, and check which chunk renders exist.
// Use same functions to count as ChunkRenderSpawnSystem.
// Passes in MaterialLink attached to Voxel entity.
//	Each ChunkRender should have a materialLink. I can compare these!
// Spawn 'Material' Entities in Realm. One for Diffuse, Water, Leaves.
//  - stores the original materials in them
//  - links to tilemaps generated for voxels
// how is it previously handled? From VoxelMaterials -> ChunkRenders