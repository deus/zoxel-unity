using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Voxels.Lighting;

namespace Zoxel.Voxels
{
    //! Spawns ChunkRender entities from a Chunk!
    /**
    *   - Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ModelChunkRenderSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;
        private Entity modelChunkRenderPrefab;
        private Entity minivoxChunkRenderPrefab;
        private Entity skeletonModelChunkRenderPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Vox>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<DeadEntity>());
            var minivoxChunkRenderArchtype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(ZoxID),
                typeof(StaticRender),
                typeof(MinivoxRender),
                typeof(MinivoxBounds),
                typeof(ModelMesh),
                typeof(CentredBounds),
                typeof(NewChunkRender),
                typeof(ChunkRender),
                typeof(ChunkRenderIndex),
                typeof(ChunkSides),
                typeof(ChunkPosition),
                typeof(VoxLink),
                typeof(ChunkLink),
                typeof(ZoxMesh),
                typeof(ZoxMatrix),
                typeof(DontDestroyTexture),
                typeof(MaterialBaseColor),
                typeof(OutlineColor),
                typeof(MaterialAddColor)
            );
            var modelChunkRenderArchtype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(ZoxID),
                typeof(BasicRender),
                typeof(ModelRender),
                typeof(NewChunkRender),
                typeof(ChunkRender),
                typeof(ChunkRenderIndex),
                typeof(ModelMesh),
                typeof(CentredBounds),
                typeof(ChunkSides),
                typeof(ChunkPosition),
                typeof(VoxLink),
                typeof(ChunkLink),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(LocalScale),
                typeof(ZoxMesh),
                typeof(DontDestroyTexture),
                typeof(MaterialBaseColor),
                typeof(OutlineColor),
                typeof(MaterialAddColor),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld)
            );
            // Models
            minivoxChunkRenderPrefab = EntityManager.CreateEntity(minivoxChunkRenderArchtype);
            EntityManager.SetComponentData(minivoxChunkRenderPrefab, new MaterialBaseColor { Value = new float4(3, 0, 0, 1) });
            modelChunkRenderPrefab = EntityManager.CreateEntity(modelChunkRenderArchtype);
            EntityManager.SetComponentData(modelChunkRenderPrefab, new MaterialBaseColor { Value = new float4(3, 0, 0, 1) });
            EntityManager.SetComponentData(modelChunkRenderPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(modelChunkRenderPrefab, LocalScale.One);
            skeletonModelChunkRenderPrefab = EntityManager.Instantiate(modelChunkRenderPrefab);
            EntityManager.AddComponent<Prefab>(skeletonModelChunkRenderPrefab);
            EntityManager.AddComponent<SkeletonRender>(skeletonModelChunkRenderPrefab);
            EntityManager.AddComponent<SkeletonBounds>(skeletonModelChunkRenderPrefab);
            EntityManager.AddComponent<SkeletonWeights>(skeletonModelChunkRenderPrefab);
            EntityManager.AddSharedComponentManaged(skeletonModelChunkRenderPrefab, new OriginalBoneBuffer());
            EntityManager.AddSharedComponentManaged(skeletonModelChunkRenderPrefab, new CurrentBoneBuffer());
            EntityManager.AddSharedComponentManaged(skeletonModelChunkRenderPrefab, new BoneIndexBuffer());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var minivoxChunkRenderPrefab = this.minivoxChunkRenderPrefab;
            var modelChunkRenderPrefab = this.modelChunkRenderPrefab;
            var skeletonModelChunkRenderPrefab = this.skeletonModelChunkRenderPrefab;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<ChunkSpawnRenders>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var minivoxes = GetComponentLookup<Minivox>(true);
            var materialFaders = GetComponentLookup<MaterialFader>(true);
            var entityLightings = GetComponentLookup<EntityLighting>(true);
            voxEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithNone<PlanetChunk>()
                .WithAll<ChunkSpawnRenders>()
                .ForEach((Entity e, int entityInQueryIndex, in Chunk chunk, in ChunkPosition chunkPosition, in VoxLink voxLink, in ZoxID zoxID, in ChunkRenderLinks chunkRenderLinks) =>
            {
                if (!HasComponent<Vox>(voxLink.vox) || HasComponent<DestroyEntity>(voxLink.vox))
                {
                    return;
                }
                var hasFader = false;
                var materialFader = new MaterialFader();
                if (materialFaders.HasComponent(voxLink.vox))
                {
                    materialFader = materialFaders[voxLink.vox];
                    hasFader = true;
                }
                var minivox = new Minivox();
                if (minivoxes.HasComponent(voxLink.vox))
                {
                    minivox = minivoxes[voxLink.vox];
                }
                if (chunkRenderLinks.chunkRenders.Length != 0)
                {
                    // UnityEngine.Debug.LogError("Chunk renders already exist.");
                    PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, e);   // maybe still need?
                    PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, e);
                    return;
                }
                // set prefab
                var voxelDimensions = chunk.voxelDimensions;
                var isMinivox = HasComponent<Minivox>(voxLink.vox);
                var isAnimatedModel = HasComponent<Skeleton>(voxLink.vox);
                var isModelChunk = !isMinivox && !isAnimatedModel;
                Entity prefab;
                if (isMinivox)
                {
                    prefab = minivoxChunkRenderPrefab;
                }
                else if (isAnimatedModel)
                {
                    prefab = skeletonModelChunkRenderPrefab;
                }
                else
                {
                    prefab = modelChunkRenderPrefab;
                }
                // UnityEngine.Debug.LogError("Chunk BecomeVisible: " + chunkRendersCount);
                var entityLighting = new float4(1, 0, 0, 1);
                if (entityLightings.HasComponent(voxLink.vox))
                {
                    var lightValue = entityLightings[voxLink.vox].GetBrightness();
                    entityLighting = new float4(lightValue, lightValue, lightValue, 1);
                }
                if (hasFader)
                {
                    entityLighting.w = 0;
                }
                var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, voxLink);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ChunkLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, chunkPosition);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ChunkSides(voxelDimensions));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new MaterialBaseColor { Value = entityLighting });
                var isDisabled = HasComponent<DisableRender>(voxLink.vox);
                if (isDisabled)
                {
                    PostUpdateCommands.AddComponent<DisableRender>(entityInQueryIndex, e2);
                }
                if (hasFader)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new MaterialFadeIn(materialFader.delay, materialFader.fadeInTime));
                }
                if (HasComponent<Bullet>(voxLink.vox))
                {
                    PostUpdateCommands.AddComponent<BulletChunkRender>(entityInQueryIndex, e2);
                }
                if (isMinivox)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ZoxMatrix(minivox.position, minivox.rotation));
                }
                else // if (isModelChunk)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ParentLink(voxLink.vox));
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChunkRendersSpawned(1));
                PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, e);
            })  .WithReadOnly(minivoxes)
                .WithReadOnly(materialFaders)
                .WithReadOnly(entityLightings)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}