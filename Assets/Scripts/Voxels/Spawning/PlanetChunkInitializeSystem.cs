using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Voxels.Lighting;
using Zoxel.Worlds;
using Zoxel.Voxels.Lighting.Authoring;

namespace Zoxel.Voxels
{
	//! Initializes PlanetChunk's by pushing in data from parent.
    /**
    *   \todo  Convert to Parallel. (last bit)
    */
	[BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class PlanetChunkInitializeSystem : SystemBase
	{
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;

		protected override void OnCreate()
		{
            voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxScale>(),
                ComponentType.ReadOnly<SimplexNoise>(),
                ComponentType.ReadOnly<Seed>(),
                ComponentType.ReadOnly<ZoxID>());
            RequireForUpdate(processQuery);
            RequireForUpdate<LightsSettings>();
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
            var lightsSettings = GetSingleton<LightsSettings>();
			var lowestLight = lightsSettings.lowestLight;
            var disableVoxelLighting = lightsSettings.disableVoxelLighting;
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunkDimensions = GetComponentLookup<ChunkDimensions>(true);
            var voxScales = GetComponentLookup<VoxScale>(true);
            var streamableVoxs = GetComponentLookup<StreamableVox>(true);
            var voxIDs = GetComponentLookup<ZoxID>(true);
            var seeds = GetComponentLookup<Seed>(true);
            var simplexNoises = GetComponentLookup<SimplexNoise>(true);
            voxEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<InitializeEntity, PlanetChunk>()
                .ForEach((ref Chunk chunk, ref ChunkLights chunkLights, ref ChunkDivision chunkDivision, ref Translation translation, in ChunkPosition chunkPosition, in VoxLink voxLink) =>
			{
                var voxScale = voxScales[voxLink.vox];
                var voxelDimensions = chunkDimensions[voxLink.vox].voxelDimensions;
                var streamPosition = streamableVoxs[voxLink.vox].position;
                var translationMultiplyer = new float3(voxScale.scale.x * voxelDimensions.x, voxScale.scale.y * voxelDimensions.y, voxScale.scale.z * voxelDimensions.z);
                // Set variables
                var chunkPosition2 = chunkPosition.position;
                chunk.voxelDimensions = voxelDimensions;
                chunk.Initialize();
                // chunk.SetAllToAir();
                chunkLights.Initialize(voxelDimensions, lowestLight, disableVoxelLighting);
                var newDistance = (byte)((int) math.distance(chunkPosition2.ToFloat3(), streamPosition.ToFloat3()));
                chunkDivision.viewDistance = newDistance;
                translation.Value = new float3(chunkPosition2.x * translationMultiplyer.x, chunkPosition2.y * translationMultiplyer.y, chunkPosition2.z * translationMultiplyer.z);;
            })  .WithReadOnly(chunkDimensions)
                .WithReadOnly(voxScales)
                .WithReadOnly(streamableVoxs)
                .ScheduleParallel(Dependency);
			Dependency = Entities
                .WithAll<InitializeEntity, PlanetChunk>()
                .ForEach((ref ZoxID zoxID, in ChunkPosition chunkPosition, in VoxLink voxLink) =>
			{
                var voxID = voxIDs[voxLink.vox].id;
                var random = new Random();
                random.InitState((uint)(voxID + ChunkPosition.ConvertToUniqueID(chunkPosition.position)));
                zoxID.id = random.NextInt(int.MaxValue);
            })  .WithReadOnly(voxIDs)
                .WithNativeDisableContainerSafetyRestriction(voxIDs)
                .ScheduleParallel(Dependency);
			Dependency = Entities
                .WithAll<InitializeEntity, PlanetChunk>()
                .ForEach((ref Seed seed, ref SimplexNoise simplexNoise, in VoxLink voxLink) =>
			{
                seed.seed = seeds[voxLink.vox].seed;
                simplexNoise.values = simplexNoises[voxLink.vox].values;
            })  .WithReadOnly(seeds)
                .WithNativeDisableContainerSafetyRestriction(seeds)
                .WithReadOnly(simplexNoises)
                .WithNativeDisableContainerSafetyRestriction(simplexNoises)
                .ScheduleParallel(Dependency);
            #if UNITY_EDITOR
            Entities
                .WithAll<InitializeEntity, PlanetChunk>()
                .ForEach((ref EditorName editorName, in ChunkPosition chunkPosition) =>
            {
                // Maybe place planet name before this!
                var chunkPosition2 = chunkPosition.position;
                editorName.name = new Text("Planet Chunk [" + chunkPosition2.x + "," + chunkPosition2.y + "," + chunkPosition2.z + "]");
			}).WithoutBurst().Run();
            #endif
		}
	}
}