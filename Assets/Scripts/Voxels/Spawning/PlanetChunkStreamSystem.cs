﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Worlds;
using Zoxel.Voxels.Lighting;
using Zoxel.Voxels.Minivoxes;
using Zoxel.Characters.World;

namespace Zoxel.Voxels
{
    //! Spawns MapChunk entities for our planets using the StreamVox event.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class PlanetChunkStreamSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;
        private Entity mapChunkVisiblePrefab;
        private Entity mapChunkInvisiblePrefab;
        private Entity mapChunkUIOnlyPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var planetChunkArchetype = EntityManager.CreateArchetype(
                typeof(PlanetChunk),
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(GenerateChunk),
                typeof(EntityGenerating),
                typeof(LoadedPath),
                typeof(LoadChunk),
                typeof(EntityBusy),
                typeof(ZoxID),
                typeof(NewChunk),
                typeof(ChunkIndex),
                typeof(Chunk),
                typeof(ChunkGravity),
                typeof(ChunkLights), 
                typeof(ChunkVoxelRotations),
                typeof(ChunkHeights),
                typeof(ChunkPosition),
                typeof(ChunkTimer),
                typeof(ChunkDivision),
                typeof(VoxLink),
                typeof(ChunkNeighbors),
                typeof(MinivoxLinks),
                typeof(ChunkRenderLinks),
                typeof(ChunkPlanetChunk2DLinks),
                typeof(StructureLinks),
                typeof(MegaChunkLink),
                typeof(MegaChunk2DLink),
                typeof(ChunkCharacters),
                // typeof(VoxelTextureResolution),
                typeof(Seed),
                typeof(SimplexNoise),
                typeof(ChunkGravityQuadrant),
                typeof(Translation)
            );
            mapChunkVisiblePrefab = EntityManager.CreateEntity(planetChunkArchetype);
            mapChunkInvisiblePrefab = EntityManager.CreateEntity(planetChunkArchetype);
            EntityManager.AddComponent<EdgeChunk>(mapChunkInvisiblePrefab);
            mapChunkUIOnlyPrefab = EntityManager.CreateEntity(planetChunkArchetype);
            EntityManager.AddComponent<MapUIOnly>(mapChunkUIOnlyPrefab);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(mapChunkVisiblePrefab); // EntityManager.AddComponentData(mapChunkVisiblePrefab, new EditorName("[planetchunk]"));
            EntityManager.AddComponent<EditorName>(mapChunkInvisiblePrefab); // EntityManager.AddComponentData(mapChunkInvisiblePrefab, new EditorName("[planetchunk]"));
            #endif
            chunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<PlanetChunk>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadWrite<ChunkIndex>(),
                ComponentType.ReadWrite<ChunkDivision>());
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var disableLightNextToEdgeChunks = VoxelManager.instance.voxelSettings.disableLightNextToEdgeChunks;
            var outerPlanetChunkEdge = WorldsManager.instance.worldsSettings.planetHeightRadius / 16; // 1;
            var mapChunkVisiblePrefab = this.mapChunkVisiblePrefab;
            var mapChunkInvisiblePrefab = this.mapChunkInvisiblePrefab;
            var mapChunkUIOnlyPrefab = this.mapChunkUIOnlyPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var planets = GetComponentLookup<Planet>(true);
            voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunkIndexes = GetComponentLookup<ChunkIndex>(false);
            var chunkDivisions = GetComponentLookup<ChunkDivision>(false);
            var edgeChunks = GetComponentLookup<EdgeChunk>(true);
            chunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, OnChunksSpawned>()
                .WithAll<StreamVox>()
                .ForEach((Entity e, int entityInQueryIndex, ref ChunkLinks chunkLinks, ref Vox vox, in StreamableVox streamableVox, in ChunkDimensions chunkDimensions) =>
            {
                var mapOnly = HasComponent<MapUIOnly>(e);
                var newChunks = new NativeList<int3>();
                var newWorldEdges = new NativeList<int3>();
                var newEdgeChunkEntities = new NativeList<Entity>();
                var oldChunksCount = chunkLinks.chunks.Count();
                var destroyCount = 0;
                var planet = planets[e];
                var streamPosition = streamableVox.position;
                var renderDistance = streamableVox.renderDistance;
                var chunkRadius = planet.GetChunkRadius();
                var planetRadius = (chunkRadius - 1) * (planet.voxelDimensions) + 2;
                // find new chunks to spawn
                var planetRadius2 = chunkRadius + 2 + outerPlanetChunkEdge;
                var planetLowerBounds = new int3(- planetRadius2, - planetRadius2, - planetRadius2);
                var planetUpperBounds = new int3(planetRadius2, planetRadius2, planetRadius2);
                var voxelDimensions = chunkDimensions.voxelDimensions;
                var boundsRadius = renderDistance;
                var lowerBounds = streamPosition + new int3(-boundsRadius,  -boundsRadius, -boundsRadius);
                var upperBounds = streamPosition + new int3(boundsRadius,  boundsRadius, boundsRadius);
                int3 position;
                bool isEdge;
                var totalChunksCount = 0;
                if (oldChunksCount == 0)
                {
                    // Get a list of chunk spawn positions
                    for (position.x = lowerBounds.x; position.x <= upperBounds.x; position.x++)
                    {
                        for (position.y = lowerBounds.y; position.y <= upperBounds.y; position.y++)
                        {
                            for (position.z = lowerBounds.z; position.z <= upperBounds.z; position.z++)
                            {
                                if (position.x >= planetLowerBounds.x && position.x <= planetUpperBounds.x
                                    && position.y >= planetLowerBounds.y && position.y <= planetUpperBounds.y
                                    && position.z >= planetLowerBounds.z && position.z <= planetUpperBounds.z)
                                {
                                    newChunks.Add(position);
                                }
                            }
                        }
                    }
                }
                else
                {
                    var previousChunkPositions = new NativeList<int3>();
                    var previousWorldEdges = new NativeList<Entity>();
                    var previousWorldEdgePositions = new NativeList<int3>();
                    // Get a list of chunk spawn positions
                    for (position.x = lowerBounds.x; position.x <= upperBounds.x; position.x++)
                    {
                        for (position.y = lowerBounds.y; position.y <= upperBounds.y; position.y++)
                        {
                            for (position.z = lowerBounds.z; position.z <= upperBounds.z; position.z++)
                            {
                                if (position.x >= planetLowerBounds.x && position.x <= planetUpperBounds.x
                                    && position.y >= planetLowerBounds.y && position.y <= planetUpperBounds.y
                                    && position.z >= planetLowerBounds.z && position.z <= planetUpperBounds.z)
                                {
                                    // if doesn't exist, add to list
                                    if (!chunkLinks.chunks.ContainsKey(position))
                                    {
                                        newChunks.Add(position);
                                    }
                                    else
                                    {
                                        previousChunkPositions.Add(position);
                                    }
                                }
                            }
                        }
                    }
                    var newIndex = 0;
                    var destroyChunkEntities = new NativeList<Entity>();
                    foreach (var KVP in chunkLinks.chunks)
                    {
                        var chunkPosition = KVP.Key;
                        var chunkEntity = KVP.Value;
                        // if out of bounds
                        var isOutsideEdge = chunkPosition.x < lowerBounds.x || chunkPosition.x > upperBounds.x
                            || chunkPosition.y < lowerBounds.y || chunkPosition.y > upperBounds.y
                            || chunkPosition.z < lowerBounds.z || chunkPosition.z > upperBounds.z;
                        if (isOutsideEdge)
                        {
                            // Remember to remove this for lights
                            chunkLinks.chunks.Remove(chunkPosition);
                            // PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, chunkEntity);
                            destroyChunkEntities.Add(chunkEntity);
                            destroyCount++;             
                        }
                        else
                        {
                            // destroyed, set remaining indexes
                            // if (destroyCount > 0)    // Vox SetAs only re creates array
                            {
                                vox.chunks[newIndex] = chunkEntity;
                                // PostUpdateCommands.AddComponent<NewChunk>(entityInQueryIndex, chunkEntity);
                                // PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, new ChunkIndex(newIndex));
                                var chunkIndex = chunkIndexes[chunkEntity];
                                if (newIndex != chunkIndex.index)
                                {
                                    chunkIndex.index = newIndex;
                                    chunkIndexes[chunkEntity] = chunkIndex;
                                }
                            }
                            newIndex++;
                            totalChunksCount++;
                            var chunkDivision = chunkDivisions[chunkEntity];
                            var newDistance = (byte) ((int) math.distance(chunkPosition.ToFloat3(), streamPosition.ToFloat3()));
                            if (chunkDivision.viewDistance != newDistance)
                            {
                                var oldViewDistance = chunkDivision.viewDistance;
                                chunkDivision.viewDistance = newDistance;
                                chunkDivisions[chunkEntity] = chunkDivision;
                                // This causes massive lag
                                // Is it cheaper to spawn new event entities for them?
                                // try disabling lots of other sub systems of this event
					            // PostUpdateCommands.AddComponent(entityInQueryIndex, chunkEntity, new OnChunkDivisionUpdated(oldViewDistance));
                            }
                            //! \todo Make ChunkDivisionUpdate BecomeInvisible. Also if outside edge. If it's next to previous world edge, is previous chunk and not new world edge, update lights.

                            // shouldn't this be done in setChunkDivision instead!
                            isEdge = chunkPosition.x == lowerBounds.x || chunkPosition.x == upperBounds.x
                                || chunkPosition.z == lowerBounds.z || chunkPosition.z == upperBounds.z
                                || chunkPosition.y == lowerBounds.y || chunkPosition.y == upperBounds.y;
                            if (!isEdge)
                            {
                                isEdge = (chunkPosition.x == planetLowerBounds.x || chunkPosition.x == planetUpperBounds.x
                                    || chunkPosition.y == planetLowerBounds.y || chunkPosition.y == planetUpperBounds.y
                                    || chunkPosition.z == planetLowerBounds.z || chunkPosition.z == planetUpperBounds.z);
                            }
                            var hasWorldEdge = edgeChunks.HasComponent(chunkEntity);
                            if (!isEdge && hasWorldEdge)
                            {
                                previousWorldEdges.Add(chunkEntity);
                                previousWorldEdgePositions.Add(chunkPosition);
                            }
                            else if (isEdge && !hasWorldEdge)
                            {
                                newWorldEdges.Add(chunkPosition);
                                newEdgeChunkEntities.Add(chunkEntity);
                                /*PostUpdateCommands.AddComponent<EdgeChunk>(entityInQueryIndex, chunkEntity);
                                PostUpdateCommands.AddComponent<UpdateChunkNeighbors>(entityInQueryIndex, chunkEntity);
                                PostUpdateCommands.AddComponent<ChunkBecomeInvisible>(entityInQueryIndex, chunkEntity);*/
                            }
                        }
                    }
                    var destroyChunkEntities2 = destroyChunkEntities.AsArray();
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, destroyChunkEntities2);
                    destroyChunkEntities.Dispose();
                    destroyChunkEntities2.Dispose();
                    // Apply Updates to previous world edges
                    var newEdgeChunkEntities2 = newEdgeChunkEntities.AsArray();
                    PostUpdateCommands.AddComponent<EdgeChunk>(entityInQueryIndex, newEdgeChunkEntities2);
                    PostUpdateCommands.AddComponent<UpdateChunkNeighbors>(entityInQueryIndex, newEdgeChunkEntities2);
                    PostUpdateCommands.AddComponent<ChunkBecomeInvisible>(entityInQueryIndex, newEdgeChunkEntities2);
                    newEdgeChunkEntities.Dispose();
                    newEdgeChunkEntities2.Dispose();
                    var previousEdgeEntities = previousWorldEdges.AsArray();
                    PostUpdateCommands.RemoveComponent<EdgeChunk>(entityInQueryIndex, previousEdgeEntities);
                    PostUpdateCommands.AddComponent<UpdateChunkNeighbors>(entityInQueryIndex, previousEdgeEntities);
                    PostUpdateCommands.AddComponent<GenerateTrees>(entityInQueryIndex, previousEdgeEntities);
                    PostUpdateCommands.AddComponent<ChunkSpawnRenders>(entityInQueryIndex, previousEdgeEntities);
                    // Update any chunks that need lights updating
                    if (!disableLightNextToEdgeChunks)
                    {
                        var lightUpdatedChunks = new NativeList<Entity>();
                        for (int i = 0; i < previousWorldEdges.Length; i++)
                        {
                            // This logic contains 13ms of Contains Functions
                            var chunkPosition = previousWorldEdgePositions[i];
                            AddChunkNeighborsToLightsUpdate(chunkPosition, in chunkLinks, in previousChunkPositions, in previousWorldEdgePositions,
                                in newWorldEdges, ref lightUpdatedChunks);
                            // UnityEngine.Debug.LogError("ChunkSpawnRenders: " + chunkPosition + " at " + elapsedTime);
                        }
                        if (lightUpdatedChunks.Length > 0)
                        {
                            var lightUpdatedChunksArray = lightUpdatedChunks.AsArray();
                            PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, lightUpdatedChunksArray);
                            PostUpdateCommands.AddComponent<VoxelBuilderLightsOnly>(entityInQueryIndex, lightUpdatedChunksArray);
                            lightUpdatedChunksArray.Dispose();
                            // UnityEngine.Debug.LogError("Streaming updated " + lightUpdatedChunks.Length + " chunk lightings along edges.");
                        }
                        lightUpdatedChunks.Dispose();
                    }
                    previousChunkPositions.Dispose();
                    previousWorldEdges.Dispose();
                    previousWorldEdgePositions.Dispose();
                    previousEdgeEntities.Dispose();
                }
                // Spawn New Chunks here
                // maybe base off different prefabs
                var leftOverChunksCount = oldChunksCount - destroyCount;
                var newChunkEntities = new NativeArray<Entity>(newChunks.Length, Allocator.Temp);
                for (int i = 0; i < newChunks.Length; i++)
                {
                    var chunkPosition = newChunks[i];
                    var prefab = mapChunkVisiblePrefab;
                    if (!mapOnly)
                    {
                        isEdge = chunkPosition.x == lowerBounds.x || chunkPosition.x == upperBounds.x
                            || chunkPosition.z == lowerBounds.z || chunkPosition.z == upperBounds.z
                            || chunkPosition.y == lowerBounds.y || chunkPosition.y == upperBounds.y;
                        if (!isEdge && (chunkPosition.x == planetLowerBounds.x || chunkPosition.x == planetUpperBounds.x
                            || chunkPosition.y == planetLowerBounds.y || chunkPosition.y == planetUpperBounds.y
                            || chunkPosition.z == planetLowerBounds.z || chunkPosition.z == planetUpperBounds.z))
                        {
                            isEdge = true;
                        }
                        if (isEdge)
                        {
                            prefab = mapChunkInvisiblePrefab;
                        }
                    }
                    else
                    {
                        prefab = mapChunkUIOnlyPrefab;
                    }
                    //UnityEngine.Debug.LogError("Chunk Spawned: " + chunkPosition);
                    chunkLinks.chunks.Add(chunkPosition, new Entity());
                    //! \todo Instantiate with NativeArray
                    var chunkEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
                    // PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, voxLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, new ChunkPosition(chunkPosition));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, new ChunkIndex(leftOverChunksCount + i));
                    newChunkEntities[i] = chunkEntity;
                    totalChunksCount++;
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, newChunkEntities, new VoxLink(e));
                newChunkEntities.Dispose();
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChunksSpawned(totalChunksCount));   // make sure to set new chunks on this
                //! \todo Reuse destroyed chunks - reposition them
                newChunks.Dispose();
                newWorldEdges.Dispose();
                // UnityEngine.Debug.LogError("Spawn Count: " + newChunks.Length + " - Compared to Destroy: " + destroyCount);
                // UnityEngine.Debug.LogError("New Streaming: " + streamPosition + ":::" + renderDistance + ", boundsRadius: " + boundsRadius);
            })  .WithReadOnly(planets)
                .WithNativeDisableContainerSafetyRestriction(chunkDivisions)
                .WithNativeDisableContainerSafetyRestriction(chunkIndexes)
                .WithReadOnly(edgeChunks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static void AddChunk(int3 position, in ChunkLinks chunkLinks, ref NativeList<Entity> chunks)
        {
            var otherChunk = chunkLinks.chunks[position];
            if (otherChunk.Index > 0 && !chunks.Contains(otherChunk))
            {
                chunks.Add(otherChunk);
            }
        }

        // if previous chunk, but not previous world edge, then add to a light update
        private static void AddChunkNeighborsToLightsUpdate(int3 chunkPosition, in ChunkLinks chunkLinks, in NativeList<int3> previousChunkPositions, in NativeList<int3> previousWorldEdges, in NativeList<int3> newWorldEdges,
            ref NativeList<Entity> lightUpdatedChunks)
        {
            // X
            var chunkPositionLeft = chunkPosition.Left();
            if (previousChunkPositions.Contains(chunkPositionLeft) &&
                !previousWorldEdges.Contains(chunkPositionLeft) &&
                !newWorldEdges.Contains(chunkPositionLeft))
            {
                AddChunk(chunkPositionLeft, in chunkLinks, ref lightUpdatedChunks);
            }
            var chunkPositionRight = chunkPosition.Right();
            if (previousChunkPositions.Contains(chunkPositionRight) &&
                !previousWorldEdges.Contains(chunkPositionRight) &&
                !newWorldEdges.Contains(chunkPositionRight))
            {
                AddChunk(chunkPositionRight, in chunkLinks, ref lightUpdatedChunks);
            }
            // Y
            var chunkPositionDown = chunkPosition.Down();
            if (previousChunkPositions.Contains(chunkPositionDown) &&
                !previousWorldEdges.Contains(chunkPositionDown) &&
                !newWorldEdges.Contains(chunkPositionDown))
            {
                AddChunk(chunkPositionDown, in chunkLinks, ref lightUpdatedChunks);
            }
            var chunkPositionUp = chunkPosition.Up();
            if (previousChunkPositions.Contains(chunkPositionUp) &&
                !previousWorldEdges.Contains(chunkPositionUp) &&
                !newWorldEdges.Contains(chunkPositionUp))
            {
                AddChunk(chunkPositionUp, in chunkLinks, ref lightUpdatedChunks);
            }
            // Z
            var chunkPositionBack = chunkPosition.Backward();
            if (previousChunkPositions.Contains(chunkPositionBack) &&
                !previousWorldEdges.Contains(chunkPositionBack) &&
                !newWorldEdges.Contains(chunkPositionBack))
            {
                AddChunk(chunkPositionBack, in chunkLinks, ref lightUpdatedChunks);
            }
            var chunkPositionForward = chunkPosition.Forward();
            if (previousChunkPositions.Contains(chunkPositionForward) &&
                !previousWorldEdges.Contains(chunkPositionForward) &&
                !newWorldEdges.Contains(chunkPositionForward))
            {
                AddChunk(chunkPositionForward, in chunkLinks, ref lightUpdatedChunks);
            }
        }
    }
}