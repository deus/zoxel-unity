using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Portals;

namespace Zoxel.Voxels.Minivoxes
{
	//! Spawns Portal entities from our chunk voxel data.
	/**
    *   - Spawn System -
	*	\todo Make e2 use a shared (BigMinivoxLink) position.
	*	\todo Support for unique e2 blocks, instead of just the one.
	*/
	[BurstCompile, UpdateInGroup(typeof(MinivoxSystemGroup))]
    public partial class PortalSpawnSystem : SystemBase
	{
		private const int maxPortalPerChunk = 32;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery worldQuery;
        private EntityQuery voxelQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            worldQuery = GetEntityQuery(
				ComponentType.ReadOnly<Planet>(),
				ComponentType.ReadOnly<ZoxID>(),
				ComponentType.ReadOnly<VoxelLinks>(),
				ComponentType.ReadOnly<RealmLink>());
            voxelQuery = GetEntityQuery(ComponentType.ReadOnly<Voxel>());
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
			if (VoxelManager.instance.voxelSettings.disablePortals || processQuery.IsEmpty)
			{
				return;
			}
			var nearClipPlaneDistance = PortalTargetSystem.nearClipPlaneDistance;
			var degreesToRadians = ((math.PI * 2) / 360f);
			var flippedCharacter = quaternion.EulerXYZ(new float3(180, 0, 0) * degreesToRadians);
			var rotation1 = quaternion.EulerXYZ(new float3(0, 0, -90) * degreesToRadians);
			var rotation2 = quaternion.EulerXYZ(new float3(0, 0, 90) * degreesToRadians);
			var rotation3 = quaternion.EulerXYZ(new float3(-90, 0, 0) * degreesToRadians);
			var rotation4 = quaternion.EulerXYZ(new float3(90, 0, 0) * degreesToRadians);
			var minivoxesSpawnedPrefab = MinivoxSystemGroup.minivoxesSpawnedPrefab;
			var minivoxesRemovedPrefab = MinivoxSystemGroup.minivoxesRemovedPrefab;
			var portalPrefab = PortalSystem.portalPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var worldEntities = worldQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var worldVoxelLinks = GetComponentLookup<VoxelLinks>(true);
            var worldRealmLinks = GetComponentLookup<RealmLink>(true);
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxels2 = GetComponentLookup<Voxel>(true);
			worldEntities.Dispose();
			voxelEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<DestroyChunkMinivoxes, EntityBusy>()
				.WithAll<SpawnMinivoxes>()
				.ForEach((Entity e, int entityInQueryIndex, in MinivoxLinks minivoxLinks, in Chunk chunk, in VoxLink voxLink, in ChunkPosition chunkPosition) =>
			{
				if (chunk.voxels.Length == 0 || !HasComponent<VoxelLinks>(voxLink.vox))
				{
					return;
				}
				var portalIndividualPositions = new NativeList<int3>();
				var spawnLocalPositions = new NativeList<int3>();
				var spawnGlobalPositions = new NativeList<int3>();
				var grassesAdded = new NativeList<int3>();
				var planet = voxLink.vox;
				var voxelLinks = worldVoxelLinks[planet];
				var voxels = new NativeArray<Voxel>(voxelLinks.voxels.Length, Allocator.Temp);
				var voxelDimensions = chunk.voxelDimensions;
				for (int i = 0; i < voxels.Length; i++)
				{
					voxels[i] = voxels2[voxelLinks.voxels[i]];
				}
				var realmEntity = worldRealmLinks[planet].realm;
				var chunkVoxelPosition = chunkPosition.GetVoxelPosition(voxelDimensions);
				int3 position;
				var voxelIndex = 0;
				byte voxelMetaIndex = 0;
				byte voxelMetaIndexMinusAir = 0;
				Voxel voxelMeta;
				// byte meshIndex;
				for (position.x = 0; position.x < voxelDimensions.x; position.x++)
				{
					for (position.y = 0; position.y < voxelDimensions.y; position.y++)
					{
						for (position.z = 0; position.z < voxelDimensions.z; position.z++)
						{
							if (portalIndividualPositions.Length >= maxPortalPerChunk * 4)
							{
								voxelIndex++;
								continue;
							}
							voxelMetaIndex = chunk.voxels[voxelIndex];  //((int) - 1);
							if (voxelMetaIndex == 0)
							{
								voxelIndex++;
								continue;
							}
							voxelMetaIndexMinusAir = (byte)(voxelMetaIndex - 1);
							if (voxelMetaIndexMinusAir >= voxelLinks.voxels.Length)
							{
								voxelIndex++;
								continue;
							}
							voxelMeta = voxels[voxelMetaIndexMinusAir];
							var voxelEntity = voxelLinks.voxels[voxelMetaIndexMinusAir];
							if (HasComponent<PortalVoxel>(voxelEntity))
							{
								portalIndividualPositions.Add(position);
							}
							voxelIndex++;
						}
					}
				}
				
				// Spawn Portals
				for (int i = 0; i < portalIndividualPositions.Length; i++)
				{
					var minivoxLocalPosition = portalIndividualPositions[i];
					var minivoxGlobalPosition = chunkVoxelPosition + minivoxLocalPosition;
					var adjacent1 = minivoxLocalPosition + new int3(1,0,0);
					var adjacent2 = minivoxLocalPosition + new int3(0,1,0);
					var adjacent3 = minivoxLocalPosition + new int3(1,1,0);
					if (portalIndividualPositions.Contains(adjacent1) && portalIndividualPositions.Contains(adjacent2) && portalIndividualPositions.Contains(adjacent3))
					{
						spawnLocalPositions.Add(minivoxLocalPosition);
						spawnGlobalPositions.Add(minivoxGlobalPosition);
						if (minivoxLinks.HasMinivox(minivoxGlobalPosition))
						{
							continue;
						}
						grassesAdded.Add(minivoxGlobalPosition);
						// new spawn
						var position2 = minivoxLocalPosition.ToFloat3() + new float3(1f, 1f, 0) + chunkVoxelPosition.ToFloat3();
						var spawnPosition = position2 + new float3(0, 0, 1 + 0.001f); // + new float3(0, 0, 0.6f);
						spawnPosition.z += nearClipPlaneDistance; // 0.08f;
						var portalSpawnRotation = quaternion.EulerXYZ(new float3(0, 180, 0) * degreesToRadians);
						var portalSize = new float2(2f, 2f) * 0.94f;
						var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, portalPrefab); // minivoxEntities[newMinivoxCount];
						PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new InitializePortal
						{
							position = spawnPosition,
							rotation = portalSpawnRotation
						});
						PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Body2D(portalSize));
						PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ChunkLink(e));
						PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new VoxelPosition(minivoxGlobalPosition));
						PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Minivox(spawnPosition, quaternion.identity));
						/*if (minivoxLinks.Exists(localPosition))
						{
							//spawnLocalPositions.Add(localPosition); // minivoxLinks.GetMinivoxLink(localPosition));
							//UnityEngine.Debug.LogError("Portal already exist at: " + localPosition);
							//var debugPosition = (minivoxLink.position + chunkPosition).ToFloat3() + new float3(0.5f, 0.5f, 0);
							//Debug.DrawLine(debugPosition, debugPosition + new float3(0, 3, 0), UnityEngine.Color.green, 4);
						}
						else
						{*/
						//spawnLocalPositions.Add(localPosition);
						//UnityEngine.Debug.LogError("New Portal at: " + position2);
						//}
					}
				}
				// Remove Removed Portal's.
				var grassesRemoved = new NativeList<int3>();
				if (minivoxLinks.minivoxes.IsCreated)
				{
					foreach (var KVP in minivoxLinks.minivoxes)
					{
						var minivoxPosition = KVP.Key;
						var minivoxEntity = KVP.Value;
						if (minivoxEntity.Index <= 0 || !HasComponent<Portal>(minivoxEntity) || spawnGlobalPositions.Contains(minivoxPosition))
						{
							continue;
						}
						PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, minivoxEntity);
						grassesRemoved.Add(minivoxPosition);
					}
				}
				/*if (spawnLocalPositions.Length > 0)
				{
					// var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, minivoxesSpawnedPrefab);
					// PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnMinivoxesSpawned(e, (byte) spawnLocalPositions.Length));
				}*/
				if (grassesAdded.Length > 0)
				{
					PostUpdateCommands.SetComponent(entityInQueryIndex,
						PostUpdateCommands.Instantiate(entityInQueryIndex, minivoxesSpawnedPrefab),
						new OnMinivoxesSpawned(e, in grassesAdded));
				}
				if (grassesRemoved.Length > 0)
				{
					PostUpdateCommands.SetComponent(entityInQueryIndex,
						PostUpdateCommands.Instantiate(entityInQueryIndex, minivoxesRemovedPrefab),
						new OnMinivoxesRemoved(e, in grassesRemoved));
				}
				voxels.Dispose();
				portalIndividualPositions.Dispose();
				spawnLocalPositions.Dispose();
				spawnGlobalPositions.Dispose();
				grassesAdded.Dispose();
				grassesRemoved.Dispose();
            })  .WithReadOnly(worldVoxelLinks)
                .WithReadOnly(worldRealmLinks)
				.WithReadOnly(voxels2)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}