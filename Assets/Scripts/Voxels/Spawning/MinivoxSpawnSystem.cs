using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;			// Positioning
using Zoxel.Voxels.Lighting;	// Starting Light
using Zoxel.Rendering;			// MaterialsManager
using Zoxel.VoxelInteraction;	// Chest Prefab
using Zoxel.Items;				// Chest Prefab
using Zoxel.Particles;			// Torch Prefab
using Zoxel.Voxels.Authoring;

namespace Zoxel.Voxels.Minivoxes
{
	//! Spawns Minivox entities from our chunk voxel data.
	/**
    *   - Spawn System -
	*	\todo Seperate spawning: ChestSpawnSystem, TorchSpawnSystem.
	*	\todo Voxel particles to be unique per voxel - atm its seed is 666
	*/
	[BurstCompile, UpdateInGroup(typeof(MinivoxSystemGroup))]
    public partial class MinivoxSpawnSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery worldQuery;
        private EntityQuery voxelQuery;
		public static Entity minivoxesSpawnedPrefab;
        public static Entity minivoxPrefab;
        public static Entity chestPrefab;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            worldQuery = GetEntityQuery(
				ComponentType.ReadOnly<Planet>(),
				ComponentType.ReadOnly<ZoxID>(),
				ComponentType.ReadOnly<VoxelLinks>(),
				ComponentType.ReadOnly<RealmLink>());
            voxelQuery = GetEntityQuery(ComponentType.ReadOnly<Voxel>());
            var minivoxArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
				typeof(InitializeEntity),
				typeof(NewMinivox),
                typeof(Seed),
                typeof(ZoxID),
				typeof(RealmLink),
				typeof(VoxLink),
				typeof(ChunkLink),
				typeof(EntityLighting),
				typeof(BasicMinivox),
				typeof(Minivox),
                typeof(VoxelPosition),
				typeof(MinivoxColor),
                typeof(Vox),
                typeof(ChunkDimensions),
				typeof(VoxScale),
                typeof(VoxColors),
                typeof(EntityMaterials),
                typeof(Translation),
                typeof(Rotation)
            );
            minivoxPrefab = EntityManager.CreateEntity(minivoxArchetype);
			chestPrefab = EntityManager.CreateEntity(minivoxArchetype);
			EntityManager.AddComponent<Chest>(chestPrefab);
			EntityManager.AddComponent<Saver>(chestPrefab);
			EntityManager.AddComponent<Inventory>(chestPrefab);
			// EntityManager.AddComponent<LoadUniqueItems>(chestPrefab);
			EntityManager.AddComponent<LoadInventory>(chestPrefab);
            RequireForUpdate<MinivoxSettings>();
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var minivoxSettings = GetSingleton<MinivoxSettings>();
			if (minivoxSettings.disableMinivoxes)
			{
				return;
			}
            var minivoxResolution = minivoxSettings.minivoxResolution / 16f;
			var maxMinivoxesPerChunk = minivoxSettings.maxMinivoxesPerChunk;
            var disableVoxelLighting = VoxelManager.instance.voxelSettings.disableVoxelLighting;
			var minivoxOffset = new float3(0.5f, 0.5f, 0.5f);
			var elapsedTime = World.Time.ElapsedTime;
			var degreesToRadians = ((math.PI * 2) / 360f);
			var flippedCharacter = quaternion.EulerXYZ(new float3(180, 0, 0) * degreesToRadians);
			var rotation1 = quaternion.EulerXYZ(new float3(0, 0, -90) * degreesToRadians);
			var rotation2 = quaternion.EulerXYZ(new float3(0, 0, 90) * degreesToRadians);
			var rotation3 = quaternion.EulerXYZ(new float3(-90, 0, 0) * degreesToRadians);
			var rotation4 = quaternion.EulerXYZ(new float3(90, 0, 0) * degreesToRadians);  
			var minivoxVoxelDimensions = new int3(16, 16, 16);
			var minivoxScale = (new float3(1f / 16f, 1f / 16f, 1f / 16f));
			var chestPrefab = MinivoxSpawnSystem.chestPrefab;
			var minivoxesSpawnedPrefab = MinivoxSystemGroup.minivoxesSpawnedPrefab;
			var minivoxesRemovedPrefab = MinivoxSystemGroup.minivoxesRemovedPrefab;
			var minivoxPrefab = MinivoxSpawnSystem.minivoxPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var worldEntities = worldQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var worldVoxelLinks = GetComponentLookup<VoxelLinks>(true);
            var worldRealmLinks = GetComponentLookup<RealmLink>(true);
			var voxScales = GetComponentLookup<VoxScale>(true);
			worldEntities.Dispose();
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxels2 = GetComponentLookup<Voxel>(true);
            var generateVoxDatas = GetComponentLookup<GenerateVoxData>(true);
            var voxelChunkDimensions = GetComponentLookup<ChunkDimensions>(true);
            var voxelsVoxData = GetComponentLookup<VoxDataEditor>(true);
            var minivoxPlaceOffsets = GetComponentLookup<MinivoxPlaceOffset>(true);
			voxelEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<DestroyChunkMinivoxes, EntityBusy>()
				.WithAll<SpawnMinivoxes>()
				.ForEach((Entity e, int entityInQueryIndex, in MinivoxLinks minivoxLinks, in Chunk chunk, in VoxLink voxLink, in ChunkPosition chunkPosition,
					in ChunkVoxelRotations chunkVoxelRotations, in ChunkLights chunkLights) =>
			{
				if (chunk.voxels.Length == 0 || !HasComponent<VoxelLinks>(voxLink.vox))
				{
					return;
				}
				var planetEntity = voxLink.vox;
				var voxelLinks = worldVoxelLinks[planetEntity];
				var voxels = new NativeArray<Voxel>(voxelLinks.voxels.Length, Allocator.Temp);
				var totalMinivoxTypes = new NativeList<byte>();
				var spawnLocalPositions = new NativeList<int3>();
				var spawnGlobalPositions = new NativeList<int3>();
				var voxelDimensions = chunk.voxelDimensions;
				for (int i = 0; i < voxels.Length; i++)
				{
					voxels[i] = voxels2[voxelLinks.voxels[i]];
				}
				var chunkVoxelPosition = chunkPosition.GetVoxelPosition(voxelDimensions);
				int3 position;
				var voxelIndex = 0;
				byte voxelMetaIndex = 0;
				byte voxelMetaIndexMinusAir = 0;
				Voxel voxelMeta;
				for (position.x = 0; position.x < voxelDimensions.x; position.x++)
				{
					for (position.y = 0; position.y < voxelDimensions.y; position.y++)
					{
						for (position.z = 0; position.z < voxelDimensions.z; position.z++)
						{
							if (spawnLocalPositions.Length >= maxMinivoxesPerChunk)
							{
								voxelIndex++;
								continue;
							}
							voxelMetaIndex = chunk.voxels[voxelIndex];
							if (voxelMetaIndex == 0)
							{
								voxelIndex++;
								continue;
							}
							voxelMetaIndexMinusAir = (byte)(voxelMetaIndex - 1);
							if (voxelMetaIndexMinusAir >= voxels.Length)
							{
								voxelIndex++;
								continue;
							}
							var voxelEntity = voxelLinks.voxels[voxelMetaIndexMinusAir];
							if (!HasComponent<BasicMinivoxVoxel>(voxelEntity))
							{
								voxelIndex++;
								continue;
							}
							spawnLocalPositions.Add(position);
							totalMinivoxTypes.Add(voxelMetaIndexMinusAir);
							voxelIndex++;
						}
					}
				}
				// Spawning Entities!
				var voxScale = voxScales[planetEntity].scale;
				var thisMinivoxScale = minivoxScale;
				thisMinivoxScale.x *= voxScale.x;
				thisMinivoxScale.y *= voxScale.y;
				thisMinivoxScale.z *= voxScale.z;
				// var minivoxOffset = voxScale / 2f;
				var realmEntity = worldRealmLinks[planetEntity].realm;
				var realmLink = new RealmLink(realmEntity);
				var chunkLink = new ChunkLink(e);
				var grassesAdded = new NativeList<int3>();
				for (int i = 0; i < totalMinivoxTypes.Length; i++)
				{
					voxelIndex = totalMinivoxTypes[i];
					var minivoxLocalPosition = spawnLocalPositions[i];
					var minivoxGlobalPosition = chunkVoxelPosition + minivoxLocalPosition;
					spawnGlobalPositions.Add(minivoxGlobalPosition);
					if (minivoxLinks.HasMinivox(minivoxGlobalPosition))
					{
						continue;
					}
					grassesAdded.Add(minivoxGlobalPosition);
					voxelMeta = voxels[voxelIndex];
					var voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(minivoxLocalPosition, voxelDimensions);
					var voxelEntity = voxelLinks.voxels[voxelIndex];
					// Prefabs
					var minivoxPrefab3 = minivoxPrefab;
					if (voxelMeta.functionType == (byte) VoxelFunctionType.Chest)
					{
						minivoxPrefab3 = chestPrefab;
					}
					// todo: add torch prefab
					/*else if (voxelMeta.functionType == (byte) VoxelFunctionType.Torch)
					{

					}*/
					// now set link
					var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, minivoxPrefab3);
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, realmLink);
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, voxLink);
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, chunkLink);
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new VoxelPosition(minivoxGlobalPosition));
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new MinivoxColor(voxelMeta.color, voxelMeta.secondaryColor));
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new EntityLighting(chunkLights.lights[voxelArrayIndex]));
					// set links
					// minivoxes.Add(minivoxLocalPosition);
					// set properties per minivox
					var rotation = quaternion.identity;
					var spawnPosition = minivoxGlobalPosition.ToFloat3() + minivoxOffset;
					if (voxelMeta.canRotate == 1)
					{
						var placeOffset = new int3();
						if (minivoxPlaceOffsets.HasComponent(voxelEntity))
						{
							placeOffset = minivoxPlaceOffsets[voxelEntity].placeOffset;
						}
						var offsetPosition = new float3(minivoxScale.x * placeOffset.x, minivoxScale.y * placeOffset.y, minivoxScale.z * placeOffset.z);
						var voxelRotation = chunkVoxelRotations.GetRotation(minivoxLocalPosition);
						rotation = BlockRotation.GetRotation(voxelRotation);
						spawnPosition = spawnPosition + math.rotate(rotation, offsetPosition);
						// UnityEngine.Debug.LogError("voxelRotation " + voxelRotation + ", spawnPosition: " + spawnPosition);
					}
					spawnPosition.x *= voxScale.x;
					spawnPosition.y *= voxScale.y;
					spawnPosition.z *= voxScale.z;
					// for all Minivoxes
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Minivox(spawnPosition, rotation));
					var minivoxVoxelDimensions2 = minivoxVoxelDimensions;
					if (voxelChunkDimensions.HasComponent(voxelEntity))
					{
						minivoxVoxelDimensions2 = voxelChunkDimensions[voxelEntity].voxelDimensions;
					}
					if (HasComponent<GenerateVoxData>(voxelEntity))
					{
						//! \todo Move the components into separate prefabs
						var generateVoxData = generateVoxDatas[voxelEntity];
						PostUpdateCommands.AddComponent<SpawnVoxChunk>(entityInQueryIndex, e2);
						PostUpdateCommands.AddComponent<GenerateVox>(entityInQueryIndex, e2);
						PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new GenerateVoxData(generateVoxData.generationType));
						PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new VoxScale(thisMinivoxScale / minivoxResolution));
						minivoxVoxelDimensions2 *= minivoxResolution;
					}
					else if (HasComponent<VoxDataEditor>(voxelEntity))
					{
						//! \todo Move the components into separate prefabs
						var voxData = voxelsVoxData[voxelEntity];
						PostUpdateCommands.AddComponent(entityInQueryIndex, e2, voxData);
						PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new VoxScale(thisMinivoxScale));
						PostUpdateCommands.AddComponent<SpawnVoxChunk>(entityInQueryIndex, e2);
						minivoxVoxelDimensions2 = voxData.size;
					}
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ChunkDimensions(minivoxVoxelDimensions2));
					// Particles
					if (voxelMeta.functionType == (byte) VoxelFunctionType.Torch)
					{
						//! \todo Move the components into separate prefabs
						var random = new Random();
						random.InitState((uint) 666);
						// set position Spawn Point, needs to be up a little more
						// I can set this data in the prefab actually
						// Can I set Minivox / Voxel Prefabs outside this system?
						PostUpdateCommands.AddComponent(entityInQueryIndex, e2, new ParticleSystem
						{
							timeSpawn = 0.05,
							random = random,
							spawnRate = 16,
							particleLife = 3,
							baseColor = new Color(200, 25, 25),
							colorVariance = new float3(0.15f, 0.1f, 0.1f),
							positionOffset = math.mul(rotation, new float3(0, 0.12f, 0)),
							positionAdd = math.mul(rotation, new float3(0, 0.8f, 0)),
							spawnSize = math.mul(rotation, new float3(0.14f, 0.12f, 0.14f))
						} );
					}
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Rotation { Value = rotation });
					PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Translation { Value = spawnPosition });
				}
				// Remove Removed BasicMinivox's
				var grassesRemoved = new NativeList<int3>();
				if (minivoxLinks.minivoxes.IsCreated)
				{
					foreach (var KVP in minivoxLinks.minivoxes)
					{
						var minivoxPosition = KVP.Key;
						var minivoxEntity = KVP.Value;
						if (minivoxEntity.Index <= 0 || !HasComponent<BasicMinivox>(minivoxEntity) || spawnGlobalPositions.Contains(minivoxPosition))
						{
							continue;
						}
						PostUpdateCommands.AddComponent<DyingEntity>(entityInQueryIndex, minivoxEntity);
						PostUpdateCommands.AddComponent(entityInQueryIndex, minivoxEntity, new DestroyEntityInTime(elapsedTime, 0.01f));
						grassesRemoved.Add(minivoxPosition);
						// UnityEngine.Debug.LogError("Destroying Minivox: " + minivoxEntity.Index);
					}
				}
				// Spawn event
				if (grassesAdded.Length > 0)
				{
					PostUpdateCommands.SetComponent(entityInQueryIndex,
						PostUpdateCommands.Instantiate(entityInQueryIndex, minivoxesSpawnedPrefab),
						new OnMinivoxesSpawned(e, in grassesAdded));
				}
				if (grassesRemoved.Length > 0)
				{
					PostUpdateCommands.SetComponent(entityInQueryIndex,
						PostUpdateCommands.Instantiate(entityInQueryIndex, minivoxesRemovedPrefab),
						new OnMinivoxesRemoved(e, in grassesRemoved));
				}
				voxels.Dispose();
				totalMinivoxTypes.Dispose();
				spawnLocalPositions.Dispose();
				spawnGlobalPositions.Dispose();
				grassesAdded.Dispose();
				grassesRemoved.Dispose();
            })  .WithReadOnly(worldVoxelLinks)
                .WithReadOnly(worldRealmLinks)
				.WithReadOnly(voxScales)
                .WithReadOnly(voxels2)
                .WithReadOnly(generateVoxDatas)
				.WithReadOnly(voxelChunkDimensions)
                .WithReadOnly(voxelsVoxData)
				.WithReadOnly(minivoxPlaceOffsets)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}