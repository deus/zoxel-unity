using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Voxels.Authoring;
using Zoxel.Worlds;
using Zoxel.Items;
using Zoxel.Stats;
using Zoxel.Stats.Authoring;

namespace Zoxel.Voxels
{
    //! Spawns the Realm's Voxel's
    /**
    *   - Spawn System -
    *   Workflow: Should I create entity prefabs in editor for voxel types? What are the current workflows?
    *   I should make it data driven. The list of unique voxels will be too long here.
    *   Or maybe I just add to the list from new systems (Grass, Torch, Door, Ladder, Portal, Etc)
    *       - Make a biome a seperate entity linking to the voxels
    *   \todo Solve the unique ID issue. If I update voxel IDs here, they won't map to world properly. Unique VoxelMap per chunk?
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxelUniqueSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        const int voxelIDDifference = 256;
        public static int uniqueVoxelsCount = 6;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<VoxelTextureSettings>();
            RequireForUpdate<StatSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var statSettings = GetSingleton<StatSettings>();
            var voxelTextureSettings = GetSingleton<VoxelTextureSettings>();
            var uniqueVoxelsCount = VoxelUniqueSpawnSystem.uniqueVoxelsCount;
            var biomesCount = VoxelSpawnSystem.biomesCount;
            var voxelsPerBiome = VoxelSpawnSystem.voxelsPerBiome;
            var defaultVoxelHealth = statSettings.defaultVoxelHealth;
            var voxelPrefab = VoxelSystemGroup.voxelPrefab;
            var minivoxVoxelPrefab = VoxelSystemGroup.minivoxVoxelPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<GenerateRealmVoxels>()
                .ForEach((Entity e, int entityInQueryIndex, ref VoxelLinks voxelLinks, in ZoxID zoxID, in MaterialLinks materialLinks) =>
            {
                var realmID = zoxID.id;
                if (realmID == 0)
                {
                    return;
                }
                var modelVoxelsStart = biomesCount * voxelsPerBiome;
                var random = new Random();
                // Torch
                var voxelIndex = (byte) modelVoxelsStart;
                var voxelSeed = (realmID + voxelIndex * voxelIDDifference);
                random.InitState((uint) voxelSeed);
                // wood
                var woodHSV = new float3(random.NextInt(0, 120), random.NextInt(32, 42), random.NextInt(16, 24));
                var woodColor = Color.GetColorFromHSV(woodHSV);
                // metal
                var metalHSV = new float3(random.NextInt(120, 360), random.NextInt(12, 24), random.NextInt(1, 6));
                var metalColor = Color.GetColorFromHSV(metalHSV);
                // Fire

                // var torchColor = new Color(random.NextInt(255), random.NextInt(255), random.NextInt(255)).MultiplyBrightness(0.5f);
                // var torchColor2 = new Color(random.NextInt(255), random.NextInt(255), random.NextInt(255)).MultiplyBrightness(0.5f);
                VoxelSpawnSystem.CreateMinivoxVoxel(PostUpdateCommands, entityInQueryIndex, e, minivoxVoxelPrefab, new VoxDataEditor(),
                    voxelIndex, realmID, VoxelType.Torch, woodColor, metalColor, defaultVoxelHealth);
                // Chest
                voxelIndex = (byte)(modelVoxelsStart + 1);
                voxelSeed = (realmID + voxelIndex * voxelIDDifference);
                random.InitState((uint) voxelSeed);
                VoxelSpawnSystem.CreateMinivoxVoxel(PostUpdateCommands, entityInQueryIndex, e, minivoxVoxelPrefab, new VoxDataEditor(),
                    voxelIndex, realmID, VoxelType.Chest, woodColor, metalColor, defaultVoxelHealth);
                // Portal
                voxelIndex = (byte)(modelVoxelsStart + 2);
                voxelSeed = (realmID + voxelIndex * voxelIDDifference);
                random.InitState((uint) voxelSeed);
                var portalColor = new Color(random.NextInt(255), random.NextInt(255), random.NextInt(255));
                VoxelSpawnSystem.CreateVoxelEntity(PostUpdateCommands, entityInQueryIndex, e, voxelPrefab, in voxelTextureSettings, VoxelPrefabType.SingleTexture, VoxelType.Portal,
                    materialLinks, voxelIndex, realmID, portalColor, portalColor, defaultVoxelHealth, 0);
                // Water
                voxelIndex = (byte)(modelVoxelsStart + 3);
                var waterColor = new Color(0, 125, 180, 140);
                voxelSeed = (realmID + voxelIndex * voxelIDDifference);
                // random.InitState((uint) voxelSeed);
                VoxelSpawnSystem.CreateVoxelEntity(PostUpdateCommands, entityInQueryIndex, e, voxelPrefab, in voxelTextureSettings, VoxelPrefabType.SingleTexture, VoxelType.Water, materialLinks,
                    voxelIndex, realmID, waterColor, waterColor, defaultVoxelHealth, 0);
                // Door
                voxelIndex = (byte)(modelVoxelsStart + 4);
                voxelSeed = (realmID + voxelIndex * voxelIDDifference);
                random.InitState((uint) voxelSeed);
                VoxelSpawnSystem.CreateMinivoxVoxel(PostUpdateCommands, entityInQueryIndex, e, minivoxVoxelPrefab, new VoxDataEditor(),
                    voxelIndex, realmID, VoxelType.Door, woodColor, metalColor, defaultVoxelHealth);
                // Bed
                voxelIndex = (byte)(modelVoxelsStart + 5);
                voxelSeed = (realmID + voxelIndex * voxelIDDifference);
                random.InitState((uint) voxelSeed);
                VoxelSpawnSystem.CreateMinivoxVoxel(PostUpdateCommands, entityInQueryIndex, e, minivoxVoxelPrefab, new VoxDataEditor(),
                    voxelIndex, realmID, VoxelType.Bed, metalColor, woodColor, defaultVoxelHealth);
                // covers sides of voxel with small cube moss
                /*Voxel moss;
                voxelSeed = (uint)(realmID + voxels.Length * voxelIDDifference);
                random.InitState(voxelSeed);
                var mossEntity = CreateVoxelEntity(PostUpdateCommands, entityInQueryIndex, e, (byte) voxels.Length, ref random, VoxelType.Moss, voxelSeed,
                    out moss, minivoxVoxelPrefab, firstColor2, firstColor2, 0);
                voxelLinks.voxels[modelVoxelsStart + 4] = mossEntity;
                voxels.Add(moss);*/
                PostUpdateCommands.AddComponent(entityInQueryIndex, PostUpdateCommands.CreateEntity(entityInQueryIndex), new OnVoxelsSpawned(e, uniqueVoxelsCount));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
