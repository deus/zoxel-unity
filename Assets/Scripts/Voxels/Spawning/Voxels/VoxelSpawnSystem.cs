using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Voxels.Authoring;
using Zoxel.Worlds;
using Zoxel.Items;
using Zoxel.Stats;
using Zoxel.Stats.Authoring;

namespace Zoxel.Voxels
{
    //! Spawns the Realm's Voxel's for n number of biomes.
    /**
    *   - Spawn System -
    *   \todo Base voxel colours off biome colours.
    *   \todo Find a better way to author unique voxels.
    *   \todo Multiple VoxelUVMaps per voxel - set based on rotation of a voxel.
    *   \todo Better way to author colour rules.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxelSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        const int voxelIDDifference = 256;
        public static int biomesCount = 12;
        public static int voxelsPerBiome = 10;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<VoxelTextureSettings>();
            RequireForUpdate<StatSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var statSettings = GetSingleton<StatSettings>();
            var voxelTextureSettings = GetSingleton<VoxelTextureSettings>();
            var biomesCount = VoxelSpawnSystem.biomesCount;
            var voxelsPerBiome = VoxelSpawnSystem.voxelsPerBiome;
            var uniqueVoxelsCount = VoxelUniqueSpawnSystem.uniqueVoxelsCount;
            var defaultVoxelHealth = statSettings.defaultVoxelHealth;
            var voxelPrefab = VoxelSystemGroup.voxelPrefab;
            var minivoxVoxelPrefab = VoxelSystemGroup.minivoxVoxelPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<GenerateRealmVoxels>()
                .ForEach((Entity e, int entityInQueryIndex, ref VoxelLinks voxelLinks, in ZoxID zoxID, in MaterialLinks materialLinks) =>
            {
                var realmID = zoxID.id;
                for (int i = 0; i < voxelLinks.voxels.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, voxelLinks.voxels[i]);
                }
                for (byte i = 0; i < biomesCount; i++)
                {
                    GenerateBiomeVoxels(PostUpdateCommands, entityInQueryIndex, e, voxelPrefab, in voxelTextureSettings,
                        minivoxVoxelPrefab, materialLinks, i, realmID, voxelsPerBiome, defaultVoxelHealth);
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, PostUpdateCommands.CreateEntity(entityInQueryIndex),
                    new OnVoxelsSpawned(e, voxelsPerBiome * biomesCount));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static void GenerateBiomeVoxels(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e, Entity voxelPrefab,
            in VoxelTextureSettings voxelTextureSettings, Entity minivoxVoxelPrefab, in MaterialLinks materialLinks,
            byte biomeIndex, int realmID, int voxelsPerBiome, int defaultVoxelHealth)
        {
            // Textures
            var variance = new int3(255, 255, 255);
            // Grass, Dirt, Wood, Leaves, Stone, Tiles
            var randomer = new Random();
            randomer.InitState((uint) (realmID + (biomeIndex) * 4096));
            var baseColor = new int3(0, 0, 0);
            // generate biome colours
            // var soilHSV = new float3(randomer.NextInt(360), 46 + randomer.NextInt(14), 30 + randomer.NextInt(14));
            var soilHSV = new float3(
                randomer.NextFloat(voxelTextureSettings.soilHSVMin.x, voxelTextureSettings.soilHSVMax.x),
                randomer.NextFloat(voxelTextureSettings.soilHSVMin.y, voxelTextureSettings.soilHSVMax.y),
                randomer.NextFloat(voxelTextureSettings.soilHSVMin.z, voxelTextureSettings.soilHSVMax.z));
            // var grassHSV = new float3((soilHSV.x + randomer.NextInt(180) - 90) % 360, 22 + randomer.NextInt(22), 30 + randomer.NextInt(20));
            var grassHSV = new float3(
                randomer.NextFloat(voxelTextureSettings.grassHSVMin.x, voxelTextureSettings.grassHSVMax.x),
                randomer.NextFloat(voxelTextureSettings.grassHSVMin.y, voxelTextureSettings.grassHSVMax.y),
                randomer.NextFloat(voxelTextureSettings.grassHSVMin.z, voxelTextureSettings.grassHSVMax.z));
            if (voxelTextureSettings.isSoilSwapRed && Color.GetColorFromHSV(grassHSV).red > Color.GetColorFromHSV(soilHSV).red)
            {
                var soilHue = soilHSV.x;
                soilHSV.x = grassHSV.x;
                grassHSV.x = soilHue;
            }
            var soilColor = Color.GetColorFromHSV(soilHSV);
            var grassColor = Color.GetColorFromHSV(grassHSV);
            //var grassHSV = grassColor.GetHSV();
            // var leafColor = grassColor.MultiplyBrightness(0.83f);
            var leafColorHSV = new float3(((int)grassHSV.x + randomer.NextFloat(-60f, 60f)) % 360, randomer.NextFloat(30f, 60f), randomer.NextFloat(40f, 60f));
            var leafColor = Color.GetColorFromHSV(leafColorHSV);
            var woodHSV = new float3(randomer.NextFloat(soilHSV.x - 20, soilHSV.x + 20), randomer.NextFloat(soilHSV.y - 6, soilHSV.y + 6), randomer.NextFloat(soilHSV.z - 10, soilHSV.z));
            var woodColor = Color.GetColorFromHSV(woodHSV);
            // var woodColor = soilColor.MultiplyBrightness(1.5f);
            var woodTopColor = woodColor.Saturate(0.7f).MultiplyBrightness(0.7f);
            var stoneHSV = new float3((soilHSV.x + randomer.NextInt(-60, 60)) % 360, randomer.NextInt(16, 28), randomer.NextInt(44, 54));   // 40-54
            var stoneColor = Color.GetColorFromHSV(stoneHSV).MultiplyBrightness(0.6f);
            var brickHSV = new float3((soilHSV.x + randomer.NextInt(-40, 40)) % 360, randomer.NextInt(12, 24), randomer.NextInt(30, 42));
            var brickColor = Color.GetColorFromHSV(brickHSV); // new Color();
            var tilesHSV = new float3((soilHSV.x + randomer.NextInt(-40, 40)) % 360, randomer.NextInt(6, 16), randomer.NextInt(22, 42));
            var tilesColor =  Color.GetColorFromHSV(tilesHSV); // brickColor.Saturate(1.5f);
            var sandHSV = new float3(0, randomer.NextInt(10, 40), randomer.NextInt(42, 62));
            var sandHueType = randomer.NextInt(0, 100);
            if (sandHueType <= 33)
            {
                sandHSV.x = randomer.NextInt(40, 80);
            }
            else if (sandHueType <= 66)
            {
                sandHSV.x = randomer.NextInt(160, 200);
            }
            else
            {
                sandHSV.x = randomer.NextInt(280, 320);
            }
            var sandColor =  Color.GetColorFromHSV(sandHSV);

            //UnityEngine.Debug.LogError("Soil Color: " + soilColor + " - HSV: " + soilHSV);
            //UnityEngine.Debug.LogError("Grass Color: " + grassColor + " - HSV: " + grassHSV);
            byte voxelIndex = (byte) (biomeIndex * voxelsPerBiome);
            // Soil
            CreateVoxelEntity(PostUpdateCommands, entityInQueryIndex, e, voxelPrefab, in voxelTextureSettings,
                VoxelPrefabType.SingleTexture, VoxelType.Soil, materialLinks,
                voxelIndex, realmID, soilColor, soilColor, defaultVoxelHealth, biomeIndex);
            voxelIndex++;

            // Grass
            // CreateVoxelEntity(PostUpdateCommands, entityInQueryIndex, e, voxelPrefab, VoxelPrefabType.SingleTexture, VoxelType.Grass,
            //    voxelIndex, realmID, grassColor, grassColor, defaultVoxelHealth, biomeIndex);
            CreateVoxelEntity(PostUpdateCommands, entityInQueryIndex, e, voxelPrefab, in voxelTextureSettings,
                VoxelPrefabType.MultiTexture, VoxelType.Grass, materialLinks,
                voxelIndex, realmID, grassColor, soilColor, defaultVoxelHealth, biomeIndex);
            voxelIndex++;

            // Sand
            CreateVoxelEntity(PostUpdateCommands, entityInQueryIndex, e, voxelPrefab, in voxelTextureSettings,
                VoxelPrefabType.SingleTexture, VoxelType.Sand, materialLinks,
                voxelIndex, realmID, sandColor, sandColor, defaultVoxelHealth, biomeIndex);
            voxelIndex++;

            // Stone
            CreateVoxelEntity(PostUpdateCommands, entityInQueryIndex, e, voxelPrefab, in voxelTextureSettings,
                VoxelPrefabType.SingleTexture, VoxelType.Stone, materialLinks,
                voxelIndex, realmID, stoneColor, stoneColor, defaultVoxelHealth, biomeIndex);
            voxelIndex++;

            // Bricks
            CreateVoxelEntity(PostUpdateCommands, entityInQueryIndex, e, voxelPrefab, in voxelTextureSettings,
                VoxelPrefabType.SingleTexture, VoxelType.Bricks, materialLinks,
                voxelIndex, realmID, brickColor, brickColor, defaultVoxelHealth, biomeIndex);
            voxelIndex++;

            // Tiles
            CreateVoxelEntity(PostUpdateCommands, entityInQueryIndex, e, voxelPrefab, in voxelTextureSettings,
                VoxelPrefabType.SingleTexture, VoxelType.Tiles, materialLinks,
                voxelIndex, realmID, tilesColor, tilesColor, defaultVoxelHealth, biomeIndex);
            voxelIndex++;

            // Wood
            CreateVoxelEntity(PostUpdateCommands, entityInQueryIndex, e, voxelPrefab, in voxelTextureSettings,
                VoxelPrefabType.MultiTexture, VoxelType.Wood, materialLinks,
                voxelIndex, realmID, woodColor, woodTopColor, defaultVoxelHealth, biomeIndex);
            voxelIndex++;

            // Leaf
            CreateVoxelEntity(PostUpdateCommands, entityInQueryIndex, e, voxelPrefab, in voxelTextureSettings,
                VoxelPrefabType.SingleTexture, VoxelType.Leaf, materialLinks,
                voxelIndex, realmID, leafColor, leafColor, defaultVoxelHealth, biomeIndex);
            voxelIndex++;

            // Bedrock
            CreateVoxelEntity(PostUpdateCommands, entityInQueryIndex, e, voxelPrefab, in voxelTextureSettings,
                VoxelPrefabType.SingleTexture, VoxelType.Bedrock, materialLinks,
                voxelIndex, realmID, stoneColor, stoneColor, defaultVoxelHealth, biomeIndex);
            voxelIndex++;

            // Weeds
            // var weedColor = grassColor.MultiplyBrightness(0.5f);
            var weedHSV = grassHSV;
            weedHSV.y += 20;
            weedHSV.z *= 0.4f;
            var weedColor = Color.GetColorFromHSV(weedHSV);
            var weedOutlineColor = weedColor.ShiftHue(90).MultiplyBrightness(0.5f);
            CreateWeedsVoxel(PostUpdateCommands, entityInQueryIndex, e, minivoxVoxelPrefab, voxelIndex, weedColor, weedOutlineColor, realmID, biomeIndex);
            voxelIndex++;
            // UnityEngine.Debug.LogError("WEed color: " + grassColor2);
            // .MultiplyBrightness(0.6f)
        }

        public static void CreateVoxelEntity(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e,
            Entity prefab, in VoxelTextureSettings voxelTextureSettings,
            VoxelPrefabType voxelPrefabType, VoxelType voxelType, in MaterialLinks materialLinks,
            byte voxelIndex, int realmID, Color baseColor, Color secondaryColor, int defaultVoxelHealth, byte biomeIndex = 0)
        {
            var voxelEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
            var voxelSeed = (realmID + voxelIndex * voxelIDDifference);
            var nameRandom = new Random();
            nameRandom.InitState((uint) voxelSeed);
            var name = NameGenerator.GenerateName2(ref nameRandom);
            var voxel = new Voxel
            {
                type = (byte) voxelType,
                biomeIndex = biomeIndex,
                color = baseColor,
                secondaryColor = secondaryColor,
                startingHealth = defaultVoxelHealth - 1,
                // voxelSeed = voxelSeed,
                raycastMode = 1
            };
            var defaultMaterial = materialLinks.materials[0];
            var waterMaterial = materialLinks.materials[1];
            var leafMaterial = materialLinks.materials[2];
            var materialEntity = defaultMaterial;
            // special types
            if (voxelType == VoxelType.Soil)
            {
                PostUpdateCommands.AddComponent<VoxelSoil>(entityInQueryIndex, voxelEntity);
            }
            else if (voxelType == VoxelType.Grass)
            {
                PostUpdateCommands.AddComponent<VoxelGrass>(entityInQueryIndex, voxelEntity);
            }
            else if (voxelType == VoxelType.Wood)
            {
                voxel.startingHealth += 2;
            }
            else if (voxelType == VoxelType.Leaf)
            {
                voxel.startingHealth = 1;
                PostUpdateCommands.AddComponent<VoxelLightPassDimmed>(entityInQueryIndex, voxelEntity);
            }
            else if (voxelType == VoxelType.Sand)
            {
                voxel.startingHealth = 2;
            }
            else if (voxelType == VoxelType.Stone)
            {
                voxel.startingHealth += 4;
            }
            else if (voxelType == VoxelType.Tiles || voxelType == VoxelType.Bricks)
            {
                voxel.startingHealth += 6;
            }
            else if (voxelType == VoxelType.Portal)
            {
                voxel.functionType = (byte) VoxelFunctionType.Portal;
                voxel.startingHealth += 3;
                PostUpdateCommands.RemoveComponent<VoxelSolidWithin>(entityInQueryIndex, voxelEntity);
                PostUpdateCommands.AddComponent<DisableVoxelCollisionFront>(entityInQueryIndex, voxelEntity);
                PostUpdateCommands.AddComponent<PortalVoxel>(entityInQueryIndex, voxelEntity);
            }
            else if (voxelType == VoxelType.Bedrock)
            {
                voxel.startingHealth += 999;
            }
            else if (voxelType == VoxelType.Water)
            {
                voxel.raycastMode = 0;
                voxel.startingHealth = 1;
                materialEntity = waterMaterial;
                PostUpdateCommands.AddComponent<VoxelLightPassDimmed>(entityInQueryIndex, voxelEntity);
                PostUpdateCommands.RemoveComponent<VoxelSolidWithin>(entityInQueryIndex, voxelEntity);
                PostUpdateCommands.AddComponent<WaterVoxel>(entityInQueryIndex, voxelEntity);
            }
            var voxelTextureType = VoxelTextureType.Soil;
            if (voxelPrefabType == VoxelPrefabType.SingleTexture)
            {
                if (voxelType == VoxelType.Soil)
                {
                    voxelTextureType = VoxelTextureType.Soil;
                }
                else if (voxelType == VoxelType.Sand)
                {
                    voxelTextureType = VoxelTextureType.Sand;
                }
                else if (voxelType == VoxelType.Stone)
                {
                    voxelTextureType = VoxelTextureType.Stone;
                }
                else if (voxelType == VoxelType.Bricks)
                {
                    voxelTextureType = VoxelTextureType.Bricks;
                }
                else if (voxelType == VoxelType.Tiles)
                {
                    voxelTextureType = VoxelTextureType.Tiles;
                }
                else if (voxelType == VoxelType.Leaf)
                {
                    voxelTextureType = VoxelTextureType.Leaf;
                    materialEntity = leafMaterial;
                }
                else if (voxelType == VoxelType.Portal)
                {
                    voxelTextureType = VoxelTextureType.Portal;
                }
                else if (voxelType == VoxelType.Bedrock)
                {
                    voxelTextureType = VoxelTextureType.Bedrock;
                }
            }
            voxel.textureType = (byte) voxelTextureType;
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, new ZoxID(voxelIndex + 1));
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, new ZoxName(name));
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, new Seed(voxelSeed));
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, voxel);
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, new VoxLink(e));
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, new MaterialLink(materialEntity));
            if (voxelType != VoxelType.Water)
            {
                PostUpdateCommands.AddComponent<VoxelWillCollide>(entityInQueryIndex, voxelEntity);
            }
        }
        
        public static void CreateMinivoxVoxel(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e, Entity prefab,
            VoxDataEditor voxData, byte voxelIndex, int realmID, VoxelType voxelType, Color baseColor, Color secondaryColor, int defaultVoxelHealth, byte biomeIndex = 0)
        {
            var nameRandom = new Random();
            var voxelSeed = (realmID + voxelIndex * voxelIDDifference);
            nameRandom.InitState((uint) voxelSeed);
            var voxelEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
            var name = NameGenerator.GenerateName2(ref nameRandom);
            var meshType = MeshType.Block;
            var isCollide = true;
            var voxel = new Voxel
            {
                // voxelSeed = voxelSeed,
                type = (byte) voxelType,
                biomeIndex = biomeIndex,
                color = baseColor,
                secondaryColor = secondaryColor,
                startingHealth = defaultVoxelHealth - 1,
                raycastMode = 1
            };
            // special types
            if (voxelType == VoxelType.Torch)
            {
                meshType = MeshType.Minivox;
                // var dimensions = new int3(4, 12, 4);
                voxel.functionType = (byte) VoxelFunctionType.Torch;
                isCollide = false;
                voxel.startingHealth = 1;
                voxel.canRotate = 1;
                AddGenerationComponents(PostUpdateCommands, entityInQueryIndex, voxelEntity, (int) voxelSeed, new int3(4, 12, 4), GenerateVoxType.Torch);
                // todo: Make this torch spawn - and then generate texture on the voxel
                PostUpdateCommands.RemoveComponent<VoxelSolidWithin>(entityInQueryIndex, voxelEntity);
                PostUpdateCommands.AddComponent<VoxelEmitLight>(entityInQueryIndex, voxelEntity);
                PostUpdateCommands.AddComponent<BasicMinivoxVoxel>(entityInQueryIndex, voxelEntity);
                // PostUpdateCommands.AddComponent(entityInQueryIndex, voxelEntity, new ChunkDimensions(dimensions));
                PostUpdateCommands.AddComponent(entityInQueryIndex, voxelEntity, new MinivoxPlaceOffset(new int3(0, 0, 0)));
                PostUpdateCommands.AddComponent<VoxelAttachToSolid>(entityInQueryIndex, voxelEntity);
            }
            else if (voxelType == VoxelType.Chest)
            {
                meshType = MeshType.Minivox;
                voxel.functionType = (byte) VoxelFunctionType.Chest;
                voxel.startingHealth += 3;
                voxel.canRotate = 1;
                PostUpdateCommands.AddComponent<VoxelAttachToSolid>(entityInQueryIndex, voxelEntity);
                PostUpdateCommands.AddComponent<BasicMinivoxVoxel>(entityInQueryIndex, voxelEntity);
                AddGenerationComponents(PostUpdateCommands, entityInQueryIndex, voxelEntity, voxelSeed, new int3(14, 10, 10), GenerateVoxType.ItemChest);
            }
            else if (voxelType == VoxelType.Door)
            {
                //! Change Door userStatLinks based on Owner and Tier
                meshType = MeshType.Minivox;
                voxel.startingHealth = 6;
                voxel.functionType = (byte) VoxelFunctionType.Door;
                PostUpdateCommands.AddComponent<VoxelAttachToSolid>(entityInQueryIndex, voxelEntity);
                voxel.canRotate = 1;
                PostUpdateCommands.AddComponent<DoorVoxel>(entityInQueryIndex, voxelEntity);
                PostUpdateCommands.AddComponent<BigMinivoxVoxel>(entityInQueryIndex, voxelEntity);
                PostUpdateCommands.AddComponent(entityInQueryIndex, voxelEntity, new BlockDimensions(new int3(1, 2, 1)));
                // PostUpdateCommands.AddComponent(entityInQueryIndex, voxelEntity, new MinivoxPlaceOffset(new int3(0, -16, 6)));
                PostUpdateCommands.AddComponent(entityInQueryIndex, voxelEntity, new MinivoxPlaceOffset(new int3(0, -8, 6)));
                AddGenerationComponents(PostUpdateCommands, entityInQueryIndex, voxelEntity, voxelSeed, new int3(16, 32, 4), GenerateVoxType.Door);
            }
            else if (voxelType == VoxelType.Bed)
            {
                //! Change Door userStatLinks based on Owner and Tier
                meshType = MeshType.Minivox;
                voxel.startingHealth = 6;
                voxel.functionType = (byte) VoxelFunctionType.Bed;
                PostUpdateCommands.AddComponent<VoxelAttachToSolid>(entityInQueryIndex, voxelEntity);
                voxel.canRotate = 1;
                PostUpdateCommands.AddComponent<DoorVoxel>(entityInQueryIndex, voxelEntity);
                PostUpdateCommands.AddComponent<BigMinivoxVoxel>(entityInQueryIndex, voxelEntity);
                PostUpdateCommands.AddComponent(entityInQueryIndex, voxelEntity, new BlockDimensions(new int3(1, 1, 2)));
                PostUpdateCommands.AddComponent(entityInQueryIndex, voxelEntity, new MinivoxPlaceOffset(new int3(0, 0, 0)));    // -8
                AddGenerationComponents(PostUpdateCommands, entityInQueryIndex, voxelEntity, voxelSeed, new int3(16, 8, 32), GenerateVoxType.Bed);
            }
            if (voxData.data.Length > 0)
            {
                PostUpdateCommands.AddComponent(entityInQueryIndex, voxelEntity, voxData);
            }
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, new ZoxName(name));
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, new ZoxID(voxelIndex + 1));
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, new Seed(voxelSeed));
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, voxel);
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, new VoxelMeshType(meshType));
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, new VoxLink(e));
            if (isCollide)
            {
                PostUpdateCommands.AddComponent<VoxelWillCollide>(entityInQueryIndex, voxelEntity);
            }
        }

        private static void AddGenerationComponents(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e, int seed, int3 dimensions, byte generationType)
        {
            PostUpdateCommands.AddComponent(entityInQueryIndex, e, new Chunk(dimensions, 1));
            PostUpdateCommands.AddComponent(entityInQueryIndex, e, new ChunkDimensions(dimensions));
            PostUpdateCommands.AddComponent<VoxColors>(entityInQueryIndex, e);
            PostUpdateCommands.AddComponent(entityInQueryIndex, e, new GenerateVoxData(generationType));
            PostUpdateCommands.AddComponent<ChunkDataOnly>(entityInQueryIndex, e);
            PostUpdateCommands.AddComponent<GenerateVox>(entityInQueryIndex, e);
            // PostUpdateCommands.AddComponent(entityInQueryIndex, e, new Seed(seed));
        }
        
        //! This voxel prefab shouldn't be a minivox, it's different ay
        public static void CreateWeedsVoxel(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity e, Entity prefab,
            byte voxelIndex, Color baseColor, Color secondaryColor, int realmID, byte biomeIndex)
        {
            var voxelEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
            var nameRandom = new Random();
            var voxelSeed = (realmID + voxelIndex * voxelIDDifference);
            nameRandom.InitState((uint) voxelSeed);
            var name = NameGenerator.GenerateName2(ref nameRandom);
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, new ZoxName(name));
            var voxel = new Voxel
            {
                type = (byte) VoxelType.Weeds,
                biomeIndex = biomeIndex,
                color = baseColor,
                secondaryColor = secondaryColor,
                startingHealth = 1,
                // voxelSeed = voxelSeed,
                raycastMode = 2
            };
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, voxel);
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, new ZoxID(voxelIndex + 1));
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, new Seed(voxelSeed));
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, new VoxelMeshType(MeshType.Minivox));
            PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, new VoxLink(e));
            PostUpdateCommands.AddComponent<GrassVoxel>(entityInQueryIndex, voxelEntity);
            PostUpdateCommands.RemoveComponent<VoxelSolidWithin>(entityInQueryIndex, voxelEntity);
            PostUpdateCommands.AddComponent<VoxelAttachToSolid>(entityInQueryIndex, voxelEntity);
            // PostUpdateCommands.AddComponent<VoxelLightPass>(entityInQueryIndex, voxelEntity);    // I like to keep ground a little darker - should place dirt under tho in generation process
        }
    }
}
