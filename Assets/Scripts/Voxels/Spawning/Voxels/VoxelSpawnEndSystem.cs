using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
    //! Completes the event for GenerateRealmVoxels.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxelSpawnEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<GenerateRealmVoxels>()
                .ForEach((Entity e, int entityInQueryIndex, ref GenerateRealm generateRealm) =>
            {
                generateRealm.state = (byte) GenerateRealmState.GenerateVoxelsLinking;
                PostUpdateCommands.RemoveComponent<GenerateRealmVoxels>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithNone<DestroyEntity, GenerateRealmVoxels>()
                .ForEach((Entity e, int entityInQueryIndex, ref GenerateRealm generateRealm) =>
            {
                if (generateRealm.state == (byte) GenerateRealmState.GenerateVoxelsLinking)
                {
                    generateRealm.state = (byte) GenerateRealmState.GenerateItems;
                }
            }).ScheduleParallel(Dependency);
        }
    }
}
