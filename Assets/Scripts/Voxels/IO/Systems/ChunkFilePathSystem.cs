
using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Voxels
{
    //! Loads chunk data from file.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ChunkFilePathSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery worldQuery;
        private EntityQuery realmQuery;
        private NativeArray<Text> texts;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.ReadOnly<Realm>());
            worldQuery = GetEntityQuery(
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.ReadOnly<RealmLink>());
            var realmsPath = SaveUtilities.GetRealmsPath();
            texts = new NativeArray<Text>(5, Allocator.Persistent);
            texts[0] = new Text(realmsPath);
            texts[1] = new Text("/Worlds/");
            texts[2] = new Text("/Chunks/Chunk_");
            texts[3] = new Text("_");
            texts[4] = new Text(".zox");   // "_.zox"
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (VoxelManager.instance.voxelSettings.disablePlanetChunkIO)
            {
                SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<LoadChunk>(processQuery);
                return;
            }
            var texts = this.texts;
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var realmIDs = GetComponentLookup<ZoxID>(true);
            realmEntities.Dispose();
            var worldEntities = worldQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var worldIDs = GetComponentLookup<ZoxID>(true);
            var realmLinks = GetComponentLookup<RealmLink>(true);
            worldEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<InitializeEntity, PlanetChunk>()   // added planet chunk
                .ForEach((Entity e, ref LoadedPath loadedPath, in ChunkPosition chunkPosition, in VoxLink voxLink) =>
            {
                if (!worldIDs.HasComponent(voxLink.vox))
                {
                    return;
                }
                var worldID = worldIDs[voxLink.vox].id;
                var realmEntity = realmLinks[voxLink.vox].realm;
                if (!realmIDs.HasComponent(realmEntity))
                {
                    return;
                }
                var realmID = realmIDs[realmEntity].id;
                var underscore = texts[3];
                var textBuilder = new TextBuilder(11);
                textBuilder.texts[0] = texts[0];
                textBuilder.texts[1] = Text.IntegerToText(realmID, Allocator.Temp);
                textBuilder.texts[2] = texts[1];
                textBuilder.texts[3] = Text.IntegerToText(worldID, Allocator.Temp);
                textBuilder.texts[4] = texts[2];
                textBuilder.texts[5] = Text.IntegerToText(chunkPosition.position.x, Allocator.Temp);
                textBuilder.texts[6] = underscore;
                textBuilder.texts[7] = Text.IntegerToText(chunkPosition.position.y, Allocator.Temp);
                textBuilder.texts[8] = underscore;
                textBuilder.texts[9] = Text.IntegerToText(chunkPosition.position.z, Allocator.Temp);
                textBuilder.texts[10] = texts[4];
                loadedPath.path = textBuilder.Build();
                textBuilder.texts[1].Dispose();
                textBuilder.texts[3].Dispose();
                textBuilder.texts[5].Dispose();
                textBuilder.texts[7].Dispose();
                textBuilder.texts[9].Dispose();
                textBuilder.DisposeFinal();
            })  .WithReadOnly(texts)
                .WithReadOnly(worldIDs)
                .WithReadOnly(realmLinks)
                .WithReadOnly(realmIDs)
                .ScheduleParallel(Dependency);
        }
    }
}