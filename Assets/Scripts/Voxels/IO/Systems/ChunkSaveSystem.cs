using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using System;
using System.Text;
using System.IO;

namespace Zoxel.Voxels
{
    //! Saves chunk data to file.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ChunkSaveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery nonEditedQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			nonEditedQuery = GetEntityQuery(
                ComponentType.ReadOnly<SaveChunk>(),
                ComponentType.Exclude<EditedChunk>());
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            if (VoxelManager.instance.voxelSettings.disablePlanetChunkIO)
            {
                PostUpdateCommands.RemoveComponent<SaveChunk>(processQuery);
                return;
            }
            PostUpdateCommands.AddComponent<EditedChunk>(nonEditedQuery);
            PostUpdateCommands.RemoveComponent<SaveChunk>(processQuery);
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity>()
                .WithAll<SaveChunk>()
                .ForEach((in Chunk chunk, in ChunkVoxelRotations chunkVoxelRotations, in LoadedPath loadedPath) => //in ChunkPosition chunkPosition, in VoxLink voxLink) =>
            {
                var filePath = loadedPath.path.ToString();
                try
                {
                    //Debug.LogError("Saving to: " + filePath);
                    var stream = new BinaryWriter(File.OpenWrite(filePath));
                    var buffer = new byte[chunk.voxels.Length + chunkVoxelRotations.Count() * 4];
                    for (int i = 0; i < chunk.voxels.Length; i++)
                    {
                        buffer[i] = chunk.voxels[i];
                    }
                    if (chunkVoxelRotations.rotations.IsCreated)
                    {
                        var j = chunk.voxels.Length;
                        foreach (var KVP in chunkVoxelRotations.rotations)
                        {
                            var voxelPosition = KVP.Key;
                            var voxelRotation = KVP.Value;
                            buffer[j] = voxelPosition.x;
                            buffer[j + 1] = voxelPosition.y;
                            buffer[j + 2] = voxelPosition.z;
                            buffer[j + 3] = voxelRotation;
                            j += 4;
                        }
                    }
                    // var buffer = chunk.voxels.AsArray().ToArray();
                    stream.Write(buffer, 0, buffer.Length);
                    stream.Close();
                    stream.Dispose();
                }
                catch (Exception exception)
                {
                    UnityEngine.Debug.LogError("Error Saving Chunk: " + filePath + " : " + exception);  // chunkPosition.position
                }
            }).WithoutBurst().Run();
        }
    }
}