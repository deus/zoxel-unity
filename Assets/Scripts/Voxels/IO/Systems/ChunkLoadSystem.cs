
using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
    //! Loads chunk data from file.
    /**
    *   - Load System -
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ChunkLoadSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (VoxelManager.instance.voxelSettings.disablePlanetChunkIO)
            {
                return;
            }
            Entities
                .WithNone<InitializeEntity>()
                .ForEach((Entity e, ref LoadChunk loadChunk, in LoadedPath loadedPath) =>
            {
                loadChunk.reader.UpdateOnMainThread(in loadedPath.path);
            }).WithoutBurst().Run();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity>()
                .WithAll<LoadedPath>()
                .ForEach((Entity e, int entityInQueryIndex, ref LoadChunk loadChunk, ref Chunk chunk, ref ChunkVoxelRotations chunkVoxelRotations) =>
            {
                if (loadChunk.reader.IsReadFileInfoComplete())
                {
                    if (loadChunk.reader.DoesFileExist())
                    {
                        loadChunk.reader.state = AsyncReadState.StartOpenFile;
                    }
                    else
                    {
                        // UnityEngine.Debug.LogError("File Did not exist.");
                        loadChunk.Dispose();
                        PostUpdateCommands.RemoveComponent<LoadChunk>(entityInQueryIndex, e);
                    }
                }
                else if (loadChunk.reader.IsReadFileComplete())
                {
                    if (loadChunk.reader.DidFileReadSuccessfully())
                    {
                        PostUpdateCommands.AddComponent<EditedChunk>(entityInQueryIndex, e);
                        // Use bytes for component data.
                        for (int i = 0; i < chunk.voxels.Length; i++) 
                        {
                            chunk.voxels[i] = loadChunk.reader[i];
                        }
                        var position = byte3.zero;
                        var rotation = (byte) 0;
                        for (int i = chunk.voxels.Length; i < loadChunk.reader.Length; i += 4) 
                        {
                            position.x = loadChunk.reader[i];
                            position.y = loadChunk.reader[i + 1];
                            position.z = loadChunk.reader[i + 2];
                            rotation = loadChunk.reader[i + 3];
                            chunkVoxelRotations.AddRotation(position, rotation);
                        }
                    }
                    // Dispose
                    loadChunk.Dispose();
                    PostUpdateCommands.RemoveComponent<LoadChunk>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}