using Unity.Entities;

namespace Zoxel.Voxels
{
    //! An event component to save chunk to file.
    public struct SaveChunk : IComponentData { }
}