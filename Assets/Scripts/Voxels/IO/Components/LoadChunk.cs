using Unity.Entities;
using Unity.IO.LowLevel.Unsafe;

namespace Zoxel.Voxels
{
    //! An event component to load chunk from file.
    public struct LoadChunk : IComponentData
    {
        public AsyncFileReader reader;

        public void Dispose()
        {
            reader.Dispose();
        }
    }
}