﻿using Unity.Entities;
using Unity.Mathematics;

using Zoxel.Rendering;
using Zoxel.Transforms;

namespace Zoxel.Voxels
{
    //! Voxel Systems are everything! Everything is Cube.
    [AlwaysUpdateSystem]
    public partial class VoxelSystemGroup : ComponentSystemGroup
    {
        // Prefabs
        public static Entity worldVoxPrefab;
        public static Entity worldVoxelPrefab;
        public static Entity voxelPrefab;
        public static Entity minivoxVoxelPrefab;
        public static Entity planetChunkRenderPrefab;
        public static Entity planetChunkRenderDisabledPrefab;

        // Debug Queries 1
        public static int voxesCount;
		private EntityQuery voxesQuery;
        public static int chunksCount;
		private EntityQuery chunksQuery;
        public static int chunkRendersCount;
		private EntityQuery chunkRendersQuery;
        // Debug Queries
        public static int busyChunksCount;
		private EntityQuery busyChunksQuery;
        public static int busyChunkRendersCount;
		private EntityQuery busyChunkRendersQuery;
        public static int chunksMeshOnlyCount;
		private EntityQuery chunksMeshOnlyQuery;
        public static int chunksLightsOnlyCount;
		private EntityQuery chunksLightsOnlyQuery;
        public static int chunksSpawnedCount;
		private EntityQuery chunksSpawnedQuery;
        public static int chunkCharactersSpawnedCount;
		private EntityQuery chunkCharactersSpawnedQuery;
        
        // Voxels
        public static int voxelsCount;
		private EntityQuery voxelsQuery;
        public static int minivoxesCount;
		private EntityQuery minivoxesQuery;
        public static int bigMinivoxesCount;
		private EntityQuery bigMinivoxesQuery;
        public static int grassCount;
        public static int grassStrandCount;
        public static int minivoxesSpawnedCount;

        protected override void OnCreate()
        {
            base.OnCreate();
			voxesQuery = GetEntityQuery(ComponentType.ReadOnly<Vox>());
			chunksQuery = GetEntityQuery(ComponentType.ReadOnly<PlanetChunk>(), ComponentType.ReadOnly<Chunk>());
			chunkRendersQuery = GetEntityQuery(ComponentType.ReadOnly<PlanetRender>(), ComponentType.ReadOnly<ChunkRender>());
			busyChunksQuery = GetEntityQuery(ComponentType.ReadOnly<Chunk>(), ComponentType.ReadOnly<EntityBusy>());
			busyChunkRendersQuery = GetEntityQuery(ComponentType.ReadOnly<ChunkRender>(), ComponentType.ReadOnly<ChunkRenderBuilder>());
			chunksMeshOnlyQuery = GetEntityQuery(ComponentType.ReadOnly<Chunk>(), ComponentType.ReadOnly<VoxelBuilderMeshOnly>());
			chunksLightsOnlyQuery = GetEntityQuery(ComponentType.ReadOnly<Chunk>(), ComponentType.ReadOnly<VoxelBuilderLightsOnly>());
			voxelsQuery = GetEntityQuery(ComponentType.ReadOnly<Voxel>());
			minivoxesQuery = GetEntityQuery(ComponentType.ReadOnly<Minivox>());
			bigMinivoxesQuery = GetEntityQuery(ComponentType.ReadOnly<Minivox>(), ComponentType.ReadOnly<BigMinivox>());
			chunksSpawnedQuery = GetEntityQuery(ComponentType.ReadOnly<OnChunksSpawned>());
			chunkCharactersSpawnedQuery = GetEntityQuery(ComponentType.ReadOnly<ChunkCharacterSpawned>());
            var voxelArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnVoxelTextures),
                typeof(ZoxID),
                typeof(ZoxName),
                typeof(Seed),
                typeof(Voxel),
                typeof(VoxelMeshType),
                typeof(VoxelUVMap),
                typeof(VoxelTextureTypes),
                typeof(VoxelSolidWithin),
                typeof(MaterialLink),
                typeof(Childrens),
                typeof(VoxLink),
                typeof(ItemLink)
            );
            voxelPrefab = EntityManager.CreateEntity(voxelArchetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(voxelPrefab); // EntityManager.AddComponentData(voxelPrefab, new EditorName("[voxel]"));
            #endif
            var minivoxVoxelArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(ZoxID),
                typeof(ZoxName),
                typeof(MinivoxVoxel),
                typeof(Seed),
                typeof(Voxel),
                typeof(VoxelSolidWithin),
                typeof(VoxelMeshType),
                typeof(ItemLink),
                typeof(VoxLink)
            );
            minivoxVoxelPrefab =  EntityManager.CreateEntity(minivoxVoxelArchetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(minivoxVoxelPrefab); // EntityManager.AddComponentData(minivoxVoxelPrefab, new EditorName("[minivoxvoxel]"));
            #endif
            var worldVoxArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnVoxChunk),
                typeof(InitializeEntity),
                typeof(ZoxID),
                typeof(Seed),
                typeof(Model),
                typeof(Vox),
                typeof(ChunkDimensions),
                typeof(VoxColors),
                typeof(VoxScale),
                typeof(EntityMaterials),
                typeof(Chunk),
                typeof(EntityLighting),
                typeof(VoxLink),            // Link to Planet
                typeof(Translation),
                typeof(Rotation),
                typeof(Scale),
                typeof(LocalToWorld)
            );
            worldVoxPrefab = EntityManager.CreateEntity(worldVoxArchetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(worldVoxPrefab); // EntityManager.AddComponentData(worldVoxPrefab, new EditorName("[planet]"));
            #endif
            EntityManager.SetComponentData(worldVoxPrefab, new Scale { Value = 1f });
            // prefabs
            var worldVoxelArchtype = EntityManager.CreateArchetype(
                typeof(Prefab), 
                typeof(InitializeEntity),
                typeof(ZoxID),
                typeof(Seed),
                typeof(WorldVoxel),
                typeof(VoxelLink),
                typeof(EntityLighting),
                typeof(MaterialBaseColor),
                typeof(VoxelTileIndex),
                typeof(VoxLink),            // Link to Planet
                typeof(Translation),
                typeof(Rotation),
                typeof(Scale),
                typeof(LocalToWorld)
            );
            worldVoxelPrefab = EntityManager.CreateEntity(worldVoxelArchtype);
            EntityManager.SetComponentData(worldVoxelPrefab, new Scale { Value = 0.42f });
            EntityManager.SetComponentData(worldVoxelPrefab, new MaterialBaseColor { Value = new float4(1, 1, 1, 1) });
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(worldVoxelPrefab); // EntityManager.AddComponentData(worldVoxelPrefab, new EditorName("[worldvoxel]"));
            #endif
            var planetChunkRenderArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(StaticMultiRender),
                typeof(PlanetRender),
                typeof(NewChunkRender),
                typeof(ChunkRender),
                typeof(ChunkRenderIndex),
                typeof(ChunkLink),
                typeof(ChunkDivision),
                typeof(ZoxMatrix),
                typeof(TerrainMesh),
                typeof(RenderLayer),
                typeof(CornerBounds),
                typeof(ZoxMeshOnly),
                typeof(ZoxMaterials),
                typeof(MaterialLink),
                typeof(MaterialIndex),
                typeof(DontDestroyMaterial),
                typeof(DontDestroyTexture),
                typeof(Seed),
                typeof(ZoxID),          // Remove this
                typeof(ChunkSides),     // Chunk Only
                typeof(ChunkPosition),  // Chunk Only
                typeof(VoxLink)         // Chunk Only
            );
            planetChunkRenderPrefab = EntityManager.CreateEntity(planetChunkRenderArchetype);
            planetChunkRenderDisabledPrefab = EntityManager.CreateEntity(planetChunkRenderArchetype);
            EntityManager.AddComponent<DisableRender>(planetChunkRenderDisabledPrefab);
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();
            voxesCount = voxesQuery.CalculateEntityCount();
            chunksCount = chunksQuery.CalculateEntityCount();
            chunkRendersCount = chunkRendersQuery.CalculateEntityCount();
            busyChunksCount = busyChunksQuery.CalculateEntityCount();
            busyChunkRendersCount = busyChunkRendersQuery.CalculateEntityCount();
            chunksMeshOnlyCount = chunksMeshOnlyQuery.CalculateEntityCount();
            chunksLightsOnlyCount = chunksLightsOnlyQuery.CalculateEntityCount();
            voxelsCount = voxelsQuery.CalculateEntityCount();
            minivoxesCount = minivoxesQuery.CalculateEntityCount();
            bigMinivoxesCount = bigMinivoxesQuery.CalculateEntityCount();
            chunksSpawnedCount = chunksSpawnedQuery.CalculateEntityCount();
            chunkCharactersSpawnedCount = chunkCharactersSpawnedQuery.CalculateEntityCount();
        }
    }
}