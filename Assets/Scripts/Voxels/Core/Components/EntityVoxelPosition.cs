using Unity.Entities;

namespace Zoxel.Voxels
{
    //! A entity's voxel position. May store multiple positions (future).
    public struct EntityVoxelPosition : IComponentData
	{
        public int3 position;
    }
}