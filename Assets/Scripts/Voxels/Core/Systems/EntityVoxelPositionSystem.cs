using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Voxels
{
	//! Updates character lighting when moving around.
	[BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class EntityVoxelPositionSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery processQuery;
		private EntityQuery voxesQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			processQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.Exclude<DeadEntity>(),
				ComponentType.Exclude<InitializeEntity>(),
				ComponentType.ReadWrite<EntityVoxelPosition>(),
				ComponentType.ReadOnly<Translation>(),
				ComponentType.ReadOnly<VoxLink>());
            processQuery.SetChangedVersionFilter(typeof(Translation));
			voxesQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.Exclude<InitializeEntity>(),
				ComponentType.ReadOnly<Planet>(),
				ComponentType.ReadOnly<VoxScale>());
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            if (processQuery.IsEmpty || VoxelManager.instance.voxelSettings.disableCharacterLighting)
            {
                return;
            }
            var worldEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxScales = GetComponentLookup<VoxScale>(true);
			worldEntities.Dispose();
			Dependency = Entities
                .WithNone<DestroyEntity, DeadEntity, InitializeEntity>()
				.WithChangeFilter<Translation>()
				.ForEach((ref EntityVoxelPosition entityVoxelPosition, in Translation translation, in VoxLink voxLink) =>
			{
				if (!voxScales.HasComponent(voxLink.vox))
				{
					return;
				}
				var voxScale = voxScales[voxLink.vox].scale;
				var voxelPosition = VoxelUtilities.GetVoxelPosition(translation.Value, voxScale);
				if (entityVoxelPosition.position != voxelPosition)
				{
					entityVoxelPosition.position = voxelPosition;
				}
            })	.WithReadOnly(voxScales)
				.ScheduleParallel(Dependency);
		}
	}
}