using Unity.Entities;

namespace Zoxel.Voxels
{
    public struct DisableVoxelNoise : IComponentData { }
}