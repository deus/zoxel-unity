using Unity.Entities;

namespace Zoxel.Voxels
{
    //! A render entity with weight data.
    public struct ModelRender : IComponentData { }
}