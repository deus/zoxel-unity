using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Voxels 
{
    //! Links a Vox model to another entity's.
    public struct VoxDataLink : IComponentData
    {
        public Entity vox;

        public VoxDataLink(Entity vox)
        {
            this.vox = vox;
        }
    }
}