using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels
{
	public struct ResizeChunkRender : IComponentData
	{
        public int3 size;
		[ReadOnly] public Chunk data;   // todo: pass in chunk data instead

        public ResizeChunkRender(in Chunk data, int3 size)
        {
            this.size = size;
            this.data = data;
        }

    }
}