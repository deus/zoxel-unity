using Unity.Entities;

namespace Zoxel.Voxels
{
    public struct SetModelData : IComponentData
    {
        public int voxID;

        public SetModelData(int voxID)
        {
            this.voxID = voxID;
        }
    }
}