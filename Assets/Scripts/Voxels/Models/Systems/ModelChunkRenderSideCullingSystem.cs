﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Voxels
{
	//! Culls model voxel sides.
    [BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
    public partial class ModelChunkRenderSideCullingSystem : SystemBase
	{
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            chunksQuery = GetEntityQuery(
				ComponentType.ReadOnly<Chunk>(),
				ComponentType.Exclude<PlanetRender>(),
				ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

		[BurstCompile]
        protected override void OnUpdate()
        {
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunks = GetComponentLookup<Chunk>(true);
			chunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<InitializeEntity, ResizeChunkRender, PlanetRender>()
				.ForEach((Entity e, ref ChunkRenderBuilder chunkRenderBuilder, ref ChunkSides chunkSides, in ChunkLink chunkLink) =>
			{
				if (chunkRenderBuilder.chunkRenderBuildState != (byte) ChunkRenderBuildState.CullSides)
				{
					return;
				}
				chunkRenderBuilder.chunkRenderBuildState = (byte) ChunkRenderBuildState.GenerateMesh;
				if (!chunks.HasComponent(chunkLink.chunk))
				{
					return;
				}
				var localChunk = chunks[chunkLink.chunk];
				if (localChunk.voxels.Length == 0 || chunkSides.sides.Length == 0)
				{
					return;
				}
				var voxelDimensions = localChunk.voxelDimensions;
				if (chunkSides.sides.Length != (voxelDimensions.x * voxelDimensions.y * voxelDimensions.z))
				{
					return;
				}
				var localChunkVoxels = localChunk.voxels;
				var voxelMetaUp = (byte) 0;
				var voxelMetaDown = (byte) 0;
				var voxelMetaLeft = (byte) 0;
				var voxelMetaRight = (byte) 0;
				var voxelMetaForward = (byte) 0;
				var voxelMetaBackward = (byte) 0;
				int voxelIndex = 0;
				byte side = 0;
				byte meta = 0;
				// first cull block sides
				int3 localPosition;
				for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
				{
					for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
					{
						for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
						{
							meta = localChunkVoxels[voxelIndex];
							side = 0;
							// if air don't draw anything!
							if (meta != 0)
							{
								// Y AXIS - Up - Down
								if (localPosition.y < voxelDimensions.y - 1)
								{
									voxelMetaUp = localChunkVoxels[VoxelUtilities.GetVoxelArrayIndex(localPosition.Up(), voxelDimensions)];
								}
								else
								{
									voxelMetaUp = 0; // chunkRender.data.voxelsUp[VoxelUtilities.GetVoxelArrayIndexXZ(localPosition, voxelDimensions)];
								}
								if (localPosition.y > 0)
								{
									voxelMetaDown = localChunkVoxels[VoxelUtilities.GetVoxelArrayIndex(localPosition.Down(), voxelDimensions)];
								}
								else
								{
									voxelMetaDown = 0; // chunkRender.data.voxelsDown[VoxelUtilities.GetVoxelArrayIndexXZ(localPosition, voxelDimensions)];
								}
								// Z AXIS - Forward - Back
								if (localPosition.z < voxelDimensions.z - 1)
								{
									voxelMetaForward = localChunkVoxels[VoxelUtilities.GetVoxelArrayIndex(localPosition.Forward(), voxelDimensions)];
								}
								else
								{
									voxelMetaForward = 0;
								}
								if (localPosition.z > 0)
								{
									voxelMetaBackward = localChunkVoxels[VoxelUtilities.GetVoxelArrayIndex(localPosition.Backward(), voxelDimensions)];
								}
								else
								{
									voxelMetaBackward = 0; // chunkRender.data.voxelsBack[VoxelUtilities.GetVoxelArrayIndexXY(localPosition, voxelDimensions)];
								}
								// X AXIS - Left - Right
								if (localPosition.x > 0)
								{
									voxelMetaLeft = localChunkVoxels[VoxelUtilities.GetVoxelArrayIndex(localPosition.Left(), voxelDimensions)];
								}
								else //if (localPosition.x == 0)
								{
									voxelMetaLeft = 0; // chunkRender.data.voxelsLeft[VoxelUtilities.GetVoxelArrayIndexYZ(localPosition, voxelDimensions)];
								}
								if (localPosition.x < voxelDimensions.x - 1)
								{
									voxelMetaRight = localChunkVoxels[VoxelUtilities.GetVoxelArrayIndex(localPosition.Right(), voxelDimensions)];
								}
								else // if (localPosition.x == voxelDimensions.x - 1)
								{
									voxelMetaRight = 0; // chunkRender.data.voxelsRight[VoxelUtilities.GetVoxelArrayIndexYZ(localPosition, voxelDimensions)];
								}
								if (voxelMetaLeft == 0)
								{
									side |= (byte)(1 << VoxelSide.Left);
								}
								if (voxelMetaRight == 0)
								{
									side |= (byte)(1 << VoxelSide.Right);
								}
								if (voxelMetaDown == 0)
								{
									side |= (byte)(1 << VoxelSide.Down);
								}
								if (voxelMetaUp == 0)
								{
									side |= (byte)(1 << VoxelSide.Up);
								}
								if (voxelMetaBackward == 0)
								{
									side |= (byte)(1 << VoxelSide.Backward);
								}
								if (voxelMetaForward == 0)
								{
									side |= (byte)(1 << VoxelSide.Forward);
								}
							}
							chunkSides.sides[voxelIndex] = side;
							voxelIndex++;
						}
					}
				}
			})	.WithReadOnly(chunks)
				.ScheduleParallel(Dependency);
        }
	}
}