using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Voxels
{
    //! Spawns a chunk on a model vox.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ModelChunkSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity minivoxChunkPrefab;
        private Entity modelChunkPrefab;
        private Entity skeletonModelChunkPrefab;
        private Entity minivoxChunkPrefab2;
        private Entity modelChunkPrefab2;
        private Entity skeletonModelChunkPrefab2;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var minivoxChunkArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                //typeof(InitializeEntity),
                typeof(ChunkSpawnRenders),
                typeof(ZoxID),
                typeof(MinivoxChunk),
                typeof(NewChunk),
                typeof(ChunkIndex),
                typeof(Chunk),
                typeof(ChunkDivision),
                typeof(ChunkPosition),
                typeof(ChunkRenderLinks),
                typeof(VoxLink),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(LocalScale),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld)
            );
            minivoxChunkPrefab = EntityManager.CreateEntity(minivoxChunkArchetype);
            EntityManager.SetComponentData(minivoxChunkPrefab, new Rotation { Value = quaternion.identity });
            EntityManager.SetComponentData(minivoxChunkPrefab, new NonUniformScale { Value = new float3(1, 1, 1) });
            EntityManager.SetComponentData(minivoxChunkPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(minivoxChunkPrefab, LocalScale.One);
            var modelChunkArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                //typeof(InitializeEntity),
                typeof(ChunkSpawnRenders),
                typeof(ZoxID),
                typeof(ModelChunk),
                typeof(NewChunk),
                typeof(ChunkIndex),
                typeof(Chunk),
                typeof(ChunkRenderLinks),
                typeof(ChunkDivision),
                typeof(ChunkPosition),
                typeof(VoxLink),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(LocalScale),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale),
                typeof(LocalToWorld)
            );
            modelChunkPrefab = EntityManager.CreateEntity(modelChunkArchetype);
            EntityManager.SetComponentData(modelChunkPrefab, new Rotation { Value = quaternion.identity });
            EntityManager.SetComponentData(modelChunkPrefab, new NonUniformScale { Value = new float3(1, 1, 1) });
            EntityManager.SetComponentData(modelChunkPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(modelChunkPrefab, LocalScale.One);
            skeletonModelChunkPrefab = EntityManager.Instantiate(modelChunkPrefab);
            EntityManager.AddComponent<Prefab>(skeletonModelChunkPrefab);
            EntityManager.AddComponent<InitializeEntity>(skeletonModelChunkPrefab);
            EntityManager.AddComponent<SkeletonChunk>(skeletonModelChunkPrefab);
            EntityManager.AddComponent<ChunkWeights>(skeletonModelChunkPrefab);
            this.minivoxChunkPrefab2 = EntityManager.Instantiate(minivoxChunkPrefab);
            EntityManager.AddComponent<Prefab>(minivoxChunkPrefab2);
            EntityManager.AddComponent<EntityBusy>(minivoxChunkPrefab2);
            EntityManager.AddComponent<ChunkBuilder>(minivoxChunkPrefab2);
            this.modelChunkPrefab2 = EntityManager.Instantiate(modelChunkPrefab);
            EntityManager.AddComponent<Prefab>(modelChunkPrefab2);
            EntityManager.AddComponent<EntityBusy>(modelChunkPrefab2);
            EntityManager.AddComponent<ChunkBuilder>(modelChunkPrefab2);
            this.skeletonModelChunkPrefab2 = EntityManager.Instantiate(skeletonModelChunkPrefab);
            EntityManager.AddComponent<Prefab>(skeletonModelChunkPrefab2);
            EntityManager.AddComponent<EntityBusy>(skeletonModelChunkPrefab2);
            EntityManager.AddComponent<ChunkBuilder>(skeletonModelChunkPrefab2);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var minivoxChunkPrefab = this.minivoxChunkPrefab;
            var modelChunkPrefab = this.modelChunkPrefab;
            var skeletonModelChunkPrefab = this.skeletonModelChunkPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithNone<OnChunksSpawned, InitializeEntity, Chunk>()
                .WithAll<SpawnVoxChunk>()
                .ForEach((Entity e, int entityInQueryIndex, in ChunkDimensions chunkDimensions, in VoxScale voxScale, in ZoxID zoxID) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnVoxChunk>(entityInQueryIndex, e);
                var voxelDimensions = chunkDimensions.voxelDimensions;
                var prefab = modelChunkPrefab;
                var hasSkeleton = false;
                if (HasComponent<Minivox>(e))
                {
                    prefab = minivoxChunkPrefab;
                }
                else if (HasComponent<Skeleton>(e))
                {
                    prefab = skeletonModelChunkPrefab;
                    hasSkeleton = true;
                }
                var chunkEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, new VoxLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, new Chunk(voxelDimensions, 1));
                if (hasSkeleton)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, new ChunkWeights(voxelDimensions));
                }
                var random = new Random();
                random.InitState((uint)(zoxID.id + 1));
                PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, new ZoxID(random.NextInt(int.MaxValue)));
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChunksSpawned(1));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // With VoxData
            var minivoxChunkPrefab2 = this.minivoxChunkPrefab2;
            var modelChunkPrefab2 = this.modelChunkPrefab2;
            var skeletonModelChunkPrefab2 = this.skeletonModelChunkPrefab2;
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithNone<OnChunksSpawned, InitializeEntity>()
                .WithAll<SpawnVoxChunk>()
                .ForEach((Entity e, int entityInQueryIndex, in ChunkDimensions chunkDimensions, in VoxScale voxScale, in Chunk chunk, in ZoxID zoxID) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnVoxChunk>(entityInQueryIndex, e);
                var voxelDimensions = chunkDimensions.voxelDimensions;
                var prefab = modelChunkPrefab2;
                if (HasComponent<Minivox>(e))
                {
                    prefab = minivoxChunkPrefab2;
                }
                else if (HasComponent<Skeleton>(e))
                {
                    prefab = skeletonModelChunkPrefab2;
                }
                var chunkEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, prefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, new VoxLink(e));
                var random = new Random();
                random.InitState((uint)(zoxID.id + 1));
                PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, new ZoxID(random.NextInt(int.MaxValue)));
                PostUpdateCommands.SetComponent(entityInQueryIndex, chunkEntity, new Chunk(voxelDimensions, chunk.voxels.Clone()));
                // UnityEngine.Debug.LogError("SpawnVoxChunk with:" + e.Index + " : " + voxelDimensions);        // this is for Mushroom, Pickaxe, etc
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChunksSpawned(1));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}