using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Voxels.Authoring;

namespace Zoxel.Voxels
{
    //! Generates meshes for models.
    /**
    *   Does complex stuff for AO here
    *   Does complex stuff for Outlines (with normals) here.
    *   \todo Use tesselation for thickness of outline mesh rendring
    *   \todo Color noise variation on the shader rather than in the build process.
    *   \todo Greedy Mesh implementation for optimization.
    *       - Merge all voxels if corresponding sides are the same color/index, greedymesh.
    */
	[BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
	public partial class ModelMeshBuilderSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.ReadOnly<VoxColors>());
            chunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.Exclude<PlanetRender>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate<ModelSettings>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
		protected override void OnUpdate()
		{
            var modelSettings = GetSingleton<ModelSettings>();
            const float outlineWidth = 1f;
            var aoCornerDarkness = modelSettings.aoCornerDarkness; // 0.25f;
            var aoEdgeDarkness = modelSettings.aoEdgeDarkness; // 0.5f;
            var voxelNoiseColorVariation = modelSettings.voxelNoiseColorVariation; // 0.03f;
            var disableVoxelNoise = modelSettings.disableVoxelNoise;
            var isColourWeights = modelSettings.isColourWeights;
            var disableAO = modelSettings.disableAO;
            var degreesToRadians = ((math.PI * 2) / 360f);
            var flipRotation = quaternion.EulerXYZ(new float3(180, 0, 180) * degreesToRadians);
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var seeds = GetComponentLookup<Seed>(true);
            var voxScales = GetComponentLookup<VoxScale>(true);
            var voxColors2 = GetComponentLookup<VoxColors>(true);
            voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunks = GetComponentLookup<Chunk>(true);
            chunkEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<PlanetRender>()  // uvs
                .ForEach((Entity e, ref ModelMesh modelMesh, ref ChunkRenderBuilder chunkRenderBuilder, in ChunkRender chunkRender, in ChunkSides chunkSides,
                    in ChunkLink chunkLink, in VoxLink voxLink) =>
			{
				if (chunkRenderBuilder.chunkRenderBuildState != (byte) ChunkRenderBuildState.GenerateMesh)
                {
                    return;
                }
                chunkRenderBuilder.chunkRenderBuildState = (byte) ChunkRenderBuildState.CalculateBounds;
                var voxEntity = voxLink.vox;
                var chunkEntity = chunkLink.chunk;
                if (!seeds.HasComponent(voxEntity) || !voxColors2.HasComponent(voxEntity) || !chunks.HasComponent(chunkEntity) || !voxScales.HasComponent(voxEntity))
                {
                    return;
                }
                var disableAO2 = disableAO;
                if (!disableAO2)
                {
                    disableAO2 = HasComponent<DisableAO>(voxEntity);
                }
                var disableVoxelNoise2 = disableVoxelNoise;
                if (!disableVoxelNoise)
                {
                    disableVoxelNoise2 = HasComponent<DisableVoxelNoise>(voxEntity);
                }
                var chunk = chunks[chunkEntity];
                if (chunkSides.sides.Length == 0 || chunk.voxels.Length == 0)
                {
                    return;
                }
                var voxelDimensions = chunk.voxelDimensions;
                var voxScale = voxScales[voxEntity].scale;
                var voxColors = voxColors2[voxEntity].colors;
                var seed = seeds[voxEntity].seed;
                if (seed == 0)
                {
                    seed = 666;
                    // UnityEngine.Debug.LogError("666 Seed not set: " + voxEntity.Index + " with render: " + e.Index);
                }
                /*if (HasComponent<BigMinivox>(voxEntity))
                {
                    UnityEngine.Debug.LogError("Model Updating with Seed: " + seed);
                }*/
                var pigmentationRandom = new Random();
                pigmentationRandom.InitState((uint) seed);
                byte sideIndex;
                byte voxelType;
                var modelVertex = new ModelVertex();
                var vertices = new NativeList<ModelVertex>();
                var triangles = new NativeList<uint>();
                int vertexArrayIndex;
                byte faceVertIndex;
                var voxelArrayIndex = 0;
                var colorAddition = new float3();
                float3 localPositionFloat3;
                float3 vertColor;
                float3 voxelColor;
				byte sideLeft;
				byte sideRight;
				byte sideDown;
				byte sideUp;
				byte sideBackward;
				byte sideForward;
                // X YZ
                byte voxelLeftDown;
                byte voxelLeftUp;
                byte voxelLeftBackward;
                byte voxelLeftForward;
                // X YZ
                byte voxelRightDown;
                byte voxelRightUp;
                byte voxelRightBackward;
                byte voxelRightForward;
                // Y Z
                byte voxelDownBackward;
                byte voxelDownForward;
                byte voxelUpBackward;
                byte voxelUpForward;
                // Y Z
                byte voxelDownBackwardLeft;
                byte voxelDownBackwardRight;
                byte voxelDownForwardLeft;
                byte voxelDownForwardRight;
                byte voxelUpBackwardLeft;
                byte voxelUpBackwardRight;
                byte voxelUpForwardLeft;
                byte voxelUpForwardRight;
                // do the thing
                int3 localPosition;
                for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                {
                    for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                    {
                        for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
                        {
                            if (!disableVoxelNoise2)
                            {
                                colorAddition.x = pigmentationRandom.NextFloat(-voxelNoiseColorVariation, voxelNoiseColorVariation);
                                colorAddition.y = pigmentationRandom.NextFloat(-voxelNoiseColorVariation, voxelNoiseColorVariation);
                                colorAddition.z = pigmentationRandom.NextFloat(-voxelNoiseColorVariation, voxelNoiseColorVariation);
                            }
                            voxelType = chunk.voxels[voxelArrayIndex];
                            // if air, no need to build mesh
                            if (voxelType == 0)
                            {
                                voxelArrayIndex++;
                                continue;
                            }
                            localPositionFloat3 = localPosition.ToFloat3();
                            voxelType--;
                            /*if (voxelType >= voxColors.Length)
                            {
                                // UnityEngine.Debug.LogError("Issue with voxColors Chunk: " + chunkEntity.Index + " :: " + voxelType + " out of " + voxColors.Length);
                                voxelArrayIndex++;
                                continue;
                            }*/
                            voxelColor = voxColors[voxelType];
                            sideLeft = chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Left);
                            sideRight = chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Right);
                            sideDown = chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Down);
                            sideUp = chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Up);
                            sideBackward = chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Backward);
                            sideForward = chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Forward);
                            // X YZ
                            /*byte voxelLeft = GetVoxelType(in chunk, localPosition.Left(), voxelDimensions);
                            byte voxelRight = GetVoxelType(in chunk, localPosition.Left(), voxelDimensions);
                            byte voxelBackward = GetVoxelType(in chunk, localPosition.Left(), voxelDimensions);
                            byte voxelForward = GetVoxelType(in chunk, localPosition.Left(), voxelDimensions);*/
                            // Neighbor Voxels
                            // X YZ
                            voxelLeftDown = GetVoxelType(in chunk, localPosition.Left().Down(), voxelDimensions);
                            voxelLeftUp = GetVoxelType(in chunk, localPosition.Left().Up(), voxelDimensions);
                            voxelLeftBackward = GetVoxelType(in chunk, localPosition.Left().Backward(), voxelDimensions);
                            voxelLeftForward = GetVoxelType(in chunk, localPosition.Left().Forward(), voxelDimensions);
                            // X YZ
                            voxelRightDown = GetVoxelType(in chunk, localPosition.Right().Down(), voxelDimensions);
                            voxelRightUp = GetVoxelType(in chunk, localPosition.Right().Up(), voxelDimensions);
                            voxelRightBackward = GetVoxelType(in chunk, localPosition.Right().Backward(), voxelDimensions);
                            voxelRightForward = GetVoxelType(in chunk, localPosition.Right().Forward(), voxelDimensions);
                            // Y Z
                            voxelDownBackward = GetVoxelType(in chunk, localPosition.Down().Backward(), voxelDimensions);
                            voxelDownForward = GetVoxelType(in chunk, localPosition.Down().Forward(), voxelDimensions);
                            voxelUpBackward = GetVoxelType(in chunk, localPosition.Up().Backward(), voxelDimensions);
                            voxelUpForward = GetVoxelType(in chunk, localPosition.Up().Forward(), voxelDimensions);
                            // Y Z
                            voxelDownBackwardLeft = GetVoxelType(in chunk, localPosition.Down().Backward().Left(), voxelDimensions);
                            voxelDownBackwardRight = GetVoxelType(in chunk, localPosition.Down().Backward().Right(), voxelDimensions);
                            voxelDownForwardLeft = GetVoxelType(in chunk, localPosition.Down().Forward().Left(), voxelDimensions);
                            voxelDownForwardRight = GetVoxelType(in chunk, localPosition.Down().Forward().Right(), voxelDimensions);
                            voxelUpBackwardLeft = GetVoxelType(in chunk, localPosition.Up().Backward().Left(), voxelDimensions);
                            voxelUpBackwardRight = GetVoxelType(in chunk, localPosition.Up().Backward().Right(), voxelDimensions);
                            voxelUpForwardLeft = GetVoxelType(in chunk, localPosition.Up().Forward().Left(), voxelDimensions);
                            voxelUpForwardRight = GetVoxelType(in chunk, localPosition.Up().Forward().Right(), voxelDimensions);
                            
                            for (sideIndex = 0; sideIndex < 6; sideIndex++)
                            {
                                // Add Voxel Side
                                if (   (sideIndex == VoxelSide.Left && sideLeft != 0)
                                    || (sideIndex == VoxelSide.Right && sideRight != 0)
                                    || (sideIndex == VoxelSide.Down && sideDown != 0)
                                    || (sideIndex == VoxelSide.Up && sideUp != 0)
                                    || (sideIndex == VoxelSide.Backward && sideBackward != 0)
                                    || (sideIndex == VoxelSide.Forward && sideForward != 0))
                                {
                                    for (faceVertIndex = 0; faceVertIndex < 4; faceVertIndex++)
                                    {
                                        vertexArrayIndex = sideIndex * 4 + faceVertIndex;
                                        modelVertex.position = MeshUtilities.cubeVertices[vertexArrayIndex] + localPositionFloat3;
                                        modelVertex.position.x *= voxScale.x;
                                        modelVertex.position.y *= voxScale.y;
                                        modelVertex.position.z *= voxScale.z;
                                        modelVertex.normal = MeshUtilities.cubeNormals[sideIndex] * outlineWidth; //  * 0.01f;
                                        // Calculate edge normals for our outline shaders
                                        //! \todo Make sure normal offsets doesn't clip into a voxel - only offset if voxel is empty next to it
                                        if (sideIndex == VoxelSide.Left)
                                        {
                                            // modelVertex.normal = new float3();
                                            //  (0, 0, 0)
                                            if (faceVertIndex == 0)
                                            {
                                                if (voxelLeftDown == 0)
                                                {
                                                    modelVertex.normal.y -= outlineWidth;
                                                }
                                                if (voxelLeftBackward == 0)
                                                {
                                                    modelVertex.normal.z -= outlineWidth;
                                                }
                                            }
                                            //  (0, 1, 0)
                                            else if (faceVertIndex == 1)
                                            {
                                                if (voxelLeftUp == 0)
                                                {
                                                    modelVertex.normal.y += outlineWidth;
                                                }
                                                if (voxelLeftBackward == 0)
                                                {
                                                    modelVertex.normal.z -= outlineWidth;
                                                }
                                            }
                                            //  (0, 1, 1)
                                            else if (faceVertIndex == 2)
                                            {
                                                if (voxelLeftUp == 0)
                                                {
                                                    modelVertex.normal.y += outlineWidth;
                                                }
                                                if (voxelLeftForward == 0)
                                                {
                                                    modelVertex.normal.z += outlineWidth;
                                                }
                                            }
                                            //  (0, 0, 1)
                                            else if (faceVertIndex == 3)
                                            {
                                                if (voxelLeftDown == 0)
                                                {
                                                    modelVertex.normal.y -= outlineWidth;
                                                }
                                                if (voxelLeftForward == 0)
                                                {
                                                    modelVertex.normal.z += outlineWidth;
                                                }
                                            }
                                        }
                                        else if (sideIndex == VoxelSide.Right)
                                        {
                                            // modelVertex.normal = new float3();
                                            //  (1, 0, 0)
                                            if (faceVertIndex == 0)
                                            {
                                                if (voxelRightDown == 0)
                                                {
                                                    modelVertex.normal.y -= outlineWidth;
                                                }
                                                if (voxelRightBackward == 0)
                                                {
                                                    modelVertex.normal.z -= outlineWidth;
                                                }
                                            }
                                            //  (1, 1, 0)
                                            else if (faceVertIndex == 1)
                                            {
                                                if (voxelRightUp == 0)
                                                {
                                                    modelVertex.normal.y += outlineWidth;
                                                }
                                                if (voxelRightBackward == 0)
                                                {
                                                    modelVertex.normal.z -= outlineWidth;
                                                }
                                            }
                                            //  (1, 1, 1)
                                            else if (faceVertIndex == 2)
                                            {
                                                if (voxelRightUp == 0)
                                                {
                                                    modelVertex.normal.y += outlineWidth;
                                                }
                                                if (voxelRightForward == 0)
                                                {
                                                    modelVertex.normal.z += outlineWidth;
                                                }
                                            }
                                            //  (1, 0, 1)
                                            else if (faceVertIndex == 3)
                                            {
                                                if (voxelRightDown == 0)
                                                {
                                                    modelVertex.normal.y -= outlineWidth;
                                                }
                                                if (voxelRightForward == 0)
                                                {
                                                    modelVertex.normal.z += outlineWidth;
                                                }
                                            }
                                        }
                                        else if (sideIndex == VoxelSide.Down)
                                        {
                                            // modelVertex.normal = new float3();
                                            //  (0, 1, 0)
                                            if (faceVertIndex == 0)
                                            {
                                                if (voxelLeftDown == 0)
                                                {
                                                    modelVertex.normal.x -= outlineWidth;
                                                }
                                                if (voxelDownBackward == 0)
                                                {
                                                    modelVertex.normal.z -= outlineWidth;
                                                }
                                            }
                                            //  (1, 1, 0)
                                            else if (faceVertIndex == 1)
                                            {
                                                if (voxelRightDown == 0)
                                                {
                                                    modelVertex.normal.x += outlineWidth;
                                                }
                                                if (voxelDownBackward == 0)
                                                {
                                                    modelVertex.normal.z -= outlineWidth;
                                                }
                                            }
                                            //  (1, 1, 1),
                                            else if (faceVertIndex == 2)
                                            {
                                                if (voxelRightDown == 0)
                                                {
                                                    modelVertex.normal.x += outlineWidth;
                                                }
                                                if (voxelDownForward == 0)
                                                {
                                                    modelVertex.normal.z += outlineWidth;
                                                }
                                            }
                                            //  (0, 1, 1),
                                            else if (faceVertIndex == 3)
                                            {
                                                if (voxelLeftDown == 0)
                                                {
                                                    modelVertex.normal.x -= outlineWidth;
                                                }
                                                if (voxelDownForward == 0)
                                                {
                                                    modelVertex.normal.z += outlineWidth;
                                                }
                                            }
                                        }
                                        else if (sideIndex == VoxelSide.Up)
                                        {
                                            // modelVertex.normal = new float3();
                                            //  (0, 1, 0)
                                            if (faceVertIndex == 0)
                                            {
                                                if (voxelLeftUp == 0)
                                                {
                                                    modelVertex.normal.x -= outlineWidth;
                                                }
                                                if (voxelUpBackward == 0)
                                                {
                                                    modelVertex.normal.z -= outlineWidth;
                                                }
                                            }
                                            //  (1, 1, 0)
                                            else if (faceVertIndex == 1)
                                            {
                                                if (voxelRightUp == 0)
                                                {
                                                    modelVertex.normal.x += outlineWidth;
                                                }
                                                if (voxelUpBackward == 0)
                                                {
                                                    modelVertex.normal.z -= outlineWidth;
                                                }
                                            }
                                            //  (1, 1, 1),
                                            else if (faceVertIndex == 2)
                                            {
                                                if (voxelRightUp == 0)
                                                {
                                                    modelVertex.normal.x += outlineWidth;
                                                }
                                                if (voxelUpForward == 0)
                                                {
                                                    modelVertex.normal.z += outlineWidth;
                                                }
                                            }
                                            //  (0, 1, 1),
                                            else if (faceVertIndex == 3)
                                            {
                                                if (voxelLeftUp == 0)
                                                {
                                                    modelVertex.normal.x -= outlineWidth;
                                                }
                                                if (voxelUpForward == 0)
                                                {
                                                    modelVertex.normal.z += outlineWidth;
                                                }
                                            }
                                        }
                                        else if (sideIndex == VoxelSide.Backward)
                                        {
                                            // modelVertex.normal = new float3();
                                            //  (0, 0, 0)
                                            if (faceVertIndex == 0)
                                            {
                                                if (voxelLeftBackward  == 0)
                                                {
                                                    modelVertex.normal.x -= outlineWidth;
                                                }
                                                if (voxelDownBackward  == 0)
                                                {
                                                    modelVertex.normal.y -= outlineWidth;
                                                }
                                            }
                                            //  (1, 0, 0)
                                            else if (faceVertIndex == 1)
                                            {
                                                if (voxelRightBackward  == 0)
                                                {
                                                    modelVertex.normal.x += outlineWidth;
                                                }
                                                if (voxelDownBackward  == 0)
                                                {
                                                    modelVertex.normal.y -= outlineWidth;
                                                }
                                            }
                                            //  (1, 1, 0)
                                            else if (faceVertIndex == 2)
                                            {
                                                if (voxelRightBackward  == 0)
                                                {
                                                    modelVertex.normal.x += outlineWidth;
                                                }
                                                if (voxelUpBackward  == 0)
                                                {
                                                    modelVertex.normal.y += outlineWidth;
                                                }
                                            }
                                            //  (0, 1, 0)
                                            else if (faceVertIndex == 3)
                                            {
                                                if (voxelLeftBackward  == 0)
                                                {
                                                    modelVertex.normal.x -= outlineWidth;
                                                }
                                                if (voxelUpBackward == 0)
                                                {
                                                    modelVertex.normal.y += outlineWidth;
                                                }
                                            }
                                        }
                                        else if (sideIndex == VoxelSide.Forward)
                                        {
                                            // modelVertex.normal = new float3();
                                            //  (0, 0, 1)
                                            if (faceVertIndex == 0)
                                            {
                                                if (voxelLeftForward == 0)
                                                {
                                                    modelVertex.normal.x -= outlineWidth;
                                                }
                                                if (voxelDownForward == 0)
                                                {
                                                    modelVertex.normal.y -= outlineWidth;
                                                }
                                            }
                                            //  (1, 0, 1)
                                            else if (faceVertIndex == 1)
                                            {
                                                if (voxelRightForward == 0)
                                                {
                                                    modelVertex.normal.x += outlineWidth;
                                                }
                                                if (voxelDownForward == 0)
                                                {
                                                    modelVertex.normal.y -= outlineWidth;
                                                }
                                            }
                                            //  (1, 1, 1)
                                            else if (faceVertIndex == 2)
                                            {
                                                if (voxelRightForward == 0)
                                                {
                                                    modelVertex.normal.x += outlineWidth;
                                                }
                                                if (voxelUpForward == 0)
                                                {
                                                    modelVertex.normal.y += outlineWidth;
                                                }
                                            }
                                            //  (0, 1, 1)
                                            else if (faceVertIndex == 3)
                                            {
                                                if (voxelLeftForward == 0)
                                                {
                                                    modelVertex.normal.x -= outlineWidth;
                                                }
                                                if (voxelUpForward == 0)
                                                {
                                                    modelVertex.normal.y += outlineWidth;
                                                }
                                            }
                                        }

                                        if (!isColourWeights)
                                        {
                                            vertColor = voxelColor;
                                            
                                            if (!disableVoxelNoise)
                                            {
                                                vertColor += colorAddition; // since theres no alpha
                                                vertColor.x = math.clamp(vertColor.x, 0, 1f);
                                                vertColor.y = math.clamp(vertColor.y, 0, 1f);
                                                vertColor.z = math.clamp(vertColor.z, 0, 1f);
                                            }

                                            if (!disableAO2)
                                            {
                                                // aoPositionOffsets[vertexArrayIndex * 2 + 0, + 1]
                                                // Keep offsets in a list like vertices, double its length
                                                if (sideIndex == VoxelSide.Left)
                                                {
                                                    //  (0, 0, 0)
                                                    if (faceVertIndex == 0)
                                                    {
                                                        if ((voxelLeftDown == 0 && voxelLeftBackward != 0) || (voxelLeftDown != 0 && voxelLeftBackward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelLeftDown != 0 && voxelLeftBackward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelLeftDown == 0 && voxelLeftBackward == 0 && voxelDownBackwardLeft != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (0, 1, 0)
                                                    else if (faceVertIndex == 1)
                                                    {
                                                        if ((voxelLeftUp == 0 && voxelLeftBackward != 0) || (voxelLeftUp != 0 && voxelLeftBackward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelLeftUp != 0 && voxelLeftBackward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelLeftUp == 0 && voxelLeftBackward == 0 && voxelUpBackwardLeft != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (0, 1, 1)
                                                    else if (faceVertIndex == 2)
                                                    {
                                                        if ((voxelLeftUp == 0 && voxelLeftForward != 0) || (voxelLeftUp != 0 && voxelLeftForward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelLeftUp != 0 && voxelLeftForward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelLeftUp == 0 && voxelLeftForward == 0 && voxelUpForwardLeft != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (0, 0, 1)
                                                    else if (faceVertIndex == 3)
                                                    {
                                                        if ((voxelLeftDown == 0 && voxelLeftForward != 0) || (voxelLeftDown != 0 && voxelLeftForward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelLeftDown != 0 && voxelLeftForward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelLeftDown == 0 && voxelLeftForward == 0 && voxelDownForwardLeft != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                }
                                                else if (sideIndex == VoxelSide.Right)
                                                {
                                                    //  (1, 0, 0)
                                                    if (faceVertIndex == 0)
                                                    {
                                                        if ((voxelRightDown == 0 && voxelRightBackward != 0) || (voxelRightDown != 0 && voxelRightBackward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelRightDown != 0 && voxelRightBackward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelRightDown == 0 && voxelRightBackward == 0 && voxelDownBackwardRight != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (1, 1, 0)
                                                    else if (faceVertIndex == 1)
                                                    {
                                                        if ((voxelRightUp == 0 && voxelRightBackward != 0) || (voxelRightUp != 0 && voxelRightBackward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelRightUp != 0 && voxelRightBackward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelRightUp == 0 && voxelRightBackward == 0 && voxelUpBackwardRight != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (1, 1, 1)
                                                    else if (faceVertIndex == 2)
                                                    {
                                                        if ((voxelRightUp == 0 && voxelRightForward != 0) || (voxelRightUp != 0 && voxelRightForward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelRightUp != 0 && voxelRightForward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelRightUp == 0 && voxelRightForward == 0 && voxelUpForwardRight != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (1, 0, 1)
                                                    else if (faceVertIndex == 3)
                                                    {
                                                        if ((voxelRightDown == 0 && voxelRightForward != 0) || (voxelRightDown != 0 && voxelRightForward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelRightDown != 0 && voxelRightForward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelRightDown == 0 && voxelRightForward == 0 && voxelDownForwardRight != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                }

                                                else if (sideIndex == VoxelSide.Down)
                                                {
                                                    //  (0, 1, 0)
                                                    if (faceVertIndex == 0)
                                                    {
                                                        if ((voxelLeftDown == 0 && voxelDownBackward != 0) || (voxelLeftDown != 0 && voxelDownBackward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelLeftDown != 0 && voxelDownBackward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelLeftDown == 0 && voxelDownBackward == 0 && voxelDownBackwardLeft != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (1, 1, 0)
                                                    else if (faceVertIndex == 1)
                                                    {
                                                        if ((voxelRightDown == 0 && voxelDownBackward != 0) || (voxelRightDown != 0 && voxelDownBackward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelRightDown != 0 && voxelDownBackward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelRightDown == 0 && voxelDownBackward == 0 && voxelDownBackwardRight != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (1, 1, 1),
                                                    else if (faceVertIndex == 2)
                                                    {
                                                        if ((voxelRightDown == 0 && voxelDownForward != 0) || (voxelRightDown != 0 && voxelDownForward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelRightDown != 0 && voxelDownForward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelRightDown == 0 && voxelDownForward == 0 && voxelDownForwardRight != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (0, 1, 1),
                                                    else if (faceVertIndex == 3)
                                                    {
                                                        if ((voxelLeftDown == 0 && voxelDownForward != 0) || (voxelLeftDown != 0 && voxelDownForward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelLeftDown != 0 && voxelDownForward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelLeftDown == 0 && voxelDownForward == 0 && voxelDownForwardLeft != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                }

                                                else if (sideIndex == VoxelSide.Up)
                                                {
                                                    //  (0, 1, 0)
                                                    if (faceVertIndex == 0)
                                                    {
                                                        if ((voxelLeftUp == 0 && voxelUpBackward != 0) || (voxelLeftUp != 0 && voxelUpBackward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelLeftUp != 0 && voxelUpBackward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelLeftUp == 0 && voxelUpBackward == 0 && voxelUpBackwardLeft != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (1, 1, 0)
                                                    else if (faceVertIndex == 1)
                                                    {
                                                        if ((voxelRightUp == 0 && voxelUpBackward != 0) || (voxelRightUp != 0 && voxelUpBackward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelRightUp != 0 && voxelUpBackward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelRightUp == 0 && voxelUpBackward == 0 && voxelUpBackwardRight != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (1, 1, 1)
                                                    else if (faceVertIndex == 2)
                                                    {
                                                        if ((voxelRightUp == 0 && voxelUpForward != 0) || (voxelRightUp != 0 && voxelUpForward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelRightUp != 0 && voxelUpForward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelRightUp == 0 && voxelUpForward == 0 && voxelUpForwardRight != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (0, 1, 1)
                                                    else if (faceVertIndex == 3)
                                                    {
                                                        if ((voxelLeftUp == 0 && voxelUpForward != 0) || (voxelLeftUp != 0 && voxelUpForward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelLeftUp != 0 && voxelUpForward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelLeftUp == 0 && voxelUpForward == 0 && voxelUpForwardLeft != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                }

                                                else if (sideIndex == VoxelSide.Backward)
                                                {
                                                    //  (0, 0, 0)
                                                    if (faceVertIndex == 0)
                                                    {
                                                        if ((voxelLeftBackward == 0 && voxelDownBackward != 0) || (voxelLeftBackward != 0 && voxelDownBackward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelLeftBackward != 0 && voxelDownBackward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelLeftBackward == 0 && voxelDownBackward == 0 && voxelDownBackwardLeft != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (1, 0, 0)
                                                    else if (faceVertIndex == 1)
                                                    {
                                                        if ((voxelRightBackward == 0 && voxelDownBackward != 0) || (voxelRightBackward != 0 && voxelDownBackward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelRightBackward != 0 && voxelDownBackward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelRightBackward == 0 && voxelDownBackward == 0 && voxelDownBackwardRight != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (1, 1, 0)
                                                    else if (faceVertIndex == 2)
                                                    {
                                                        if ((voxelRightBackward == 0 && voxelUpBackward != 0) || (voxelRightBackward != 0 && voxelUpBackward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelRightBackward != 0 && voxelUpBackward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelRightBackward == 0 && voxelUpBackward == 0 && voxelUpBackwardRight != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (0, 1, 0)
                                                    else if (faceVertIndex == 3)
                                                    {
                                                        if ((voxelLeftBackward == 0 && voxelUpBackward != 0) || (voxelLeftBackward != 0 && voxelUpBackward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelLeftBackward != 0 && voxelUpBackward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelLeftBackward == 0 && voxelUpBackward == 0 && voxelUpBackwardLeft != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                }
                                                else if (sideIndex == VoxelSide.Forward)
                                                {
                                                    //  (0, 0, 1)
                                                    if (faceVertIndex == 0)
                                                    {
                                                        if ((voxelLeftForward == 0 && voxelDownForward != 0) || (voxelLeftForward != 0 && voxelDownForward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelLeftForward != 0 && voxelDownForward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelLeftForward == 0 && voxelDownForward == 0 && voxelDownForwardLeft != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (1, 0, 1)
                                                    else if (faceVertIndex == 1)
                                                    {
                                                        if ((voxelRightForward == 0 && voxelDownForward != 0) || (voxelRightForward != 0 && voxelDownForward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelRightForward != 0 && voxelDownForward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelRightForward == 0 && voxelDownForward == 0 && voxelDownForwardRight != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (1, 1, 1)
                                                    else if (faceVertIndex == 2)
                                                    {
                                                        if ((voxelRightForward == 0 && voxelUpForward != 0) || (voxelRightForward != 0 && voxelUpForward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelRightForward != 0 && voxelUpForward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelRightForward == 0 && voxelUpForward == 0 && voxelUpForwardRight != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                    //  (0, 1, 1)
                                                    else if (faceVertIndex == 3)
                                                    {
                                                        if ((voxelLeftForward == 0 && voxelUpForward != 0) || (voxelLeftForward != 0 && voxelUpForward == 0))
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                        else if (voxelLeftForward != 0 && voxelUpForward != 0)
                                                        {
                                                            vertColor *= aoCornerDarkness;
                                                        }
                                                        else if (voxelLeftForward == 0 && voxelUpForward == 0 && voxelUpForwardLeft != 0)
                                                        {
                                                            vertColor *= aoEdgeDarkness;
                                                        }
                                                    }
                                                } 
                                            }
                                            
                                            modelVertex.color = vertColor;
                                        }
                                        vertices.Add(modelVertex);
                                    }
                                    for (vertexArrayIndex = sideIndex * 6; vertexArrayIndex < sideIndex * 6 + 6; vertexArrayIndex++)
                                    {
                                        triangles.Add((uint)(MeshUtilities.cubeTriangles[vertexArrayIndex] + (vertices.Length - 4)));
                                    }
                                }
                            }
                            voxelArrayIndex++;
                        }
                    }
                }
                /*if (vertices.Length == 0)
                {
                    UnityEngine.Debug.LogError("Issue with verts on: " + voxLink.vox.Index
                        + " with colors: " + voxColors.Length);
                    return;
                }*/
                modelMesh.Dispose();
                modelMesh.vertices = new BlitableArray<ModelVertex>(vertices.Length, Allocator.Persistent);
                for (var i = 0; i < vertices.Length; i++)
                {
                    modelMesh.vertices[i] = vertices[i];
                }
                modelMesh.triangles = new BlitableArray<uint>(triangles.Length, Allocator.Persistent);
                for (var i = 0; i < triangles.Length; i++)
                {
                    modelMesh.triangles[i] = triangles[i];
                }
				vertices.Dispose();
				triangles.Dispose();
            })  .WithReadOnly(chunks)
                .WithReadOnly(seeds)
                .WithReadOnly(voxScales)
                .WithReadOnly(voxColors2)
                .ScheduleParallel(Dependency);
        }

		public static byte GetVoxelType(in Chunk chunk, int3 localPosition, int3 voxelDimensions)
		{
			if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
			{
                var voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
                if (voxelArrayIndex >= chunk.voxels.Length || voxelArrayIndex < 0)
                {
                    return 0;
                }
			    return chunk.voxels[voxelArrayIndex];
			}
			else
			{
                return 0;
			}
		}
	}
}