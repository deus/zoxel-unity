using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
    //! Resizes a ChunkRender.
	[BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ChunkRenderResizeSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref ChunkSides chunkSides, in ResizeChunkRender resizeChunkRender) =>
            {
                PostUpdateCommands.RemoveComponent<ResizeChunkRender>(entityInQueryIndex, e);
                // UnityEngine.Debug.LogError("Chunk Sides: " + e.Index + ", side: " + resizeChunkRender.size + " from " + chunkSides.voxelDimensions); 
                chunkSides.Initialize(resizeChunkRender.size);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}