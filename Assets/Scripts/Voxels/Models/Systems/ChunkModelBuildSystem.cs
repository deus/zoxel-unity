using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
	//! Triggers model chunks renders to build.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class ChunkModelBuildSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
				.WithNone<ChunkSpawnRenders, OnChunkRendersSpawned>()
                .WithNone<InitializeEntity, DelayChunkBuilder, PlanetChunk>()
				.WithAll<ChunkBuilder>()
				.ForEach((Entity e, int entityInQueryIndex, in ChunkRenderLinks chunkRenderLinks, in VoxLink voxLink) => //, in Chunk chunk) =>
            {
				if (HasComponent<SkeletonDirty>(voxLink.vox) || HasComponent<EntityBusy>(voxLink.vox) || chunkRenderLinks.chunkRenders.Length == 0)
				{
					return;
				}
				PostUpdateCommands.RemoveComponent<ChunkBuilder>(entityInQueryIndex, e);
				PostUpdateCommands.AddComponent<ChunkBuilderComplete>(entityInQueryIndex, e);
				for (int i = 0; i < chunkRenderLinks.chunkRenders.Length; i++)
				{
					PostUpdateCommands.AddComponent<ChunkRenderBuilder>(entityInQueryIndex, chunkRenderLinks.chunkRenders[i]);
				}
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}