using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
    //! Disposes VoxShapes ddata on DestroyEntity.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class VoxShapesDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, VoxShapes>()
                .ForEach((in VoxShapes voxShapes) =>
			{
                voxShapes.DisposeFinal();
			}).ScheduleParallel();
        }
    }
}