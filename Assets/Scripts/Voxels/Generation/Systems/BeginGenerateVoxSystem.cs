/*using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Voxels
{ 
    //! Initializes slimes.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class BeginGenerateVoxSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref ChunkDimensions chunkDimensions,ref Body body, in VoxScale voxScale, in BeginGenerateVox beginGenerateVox) =>
            {
                PostUpdateCommands.RemoveComponent<BeginGenerateVox>(entityInQueryIndex, e);
                // UnityEngine.Debug.LogError("BeginGenerateVox - Size: " + voxelDimensions + " from spawn type: " + beginGenerateVox.spawnType);
                var uniqueSeed = (uint) zoxID.id;
                if (uniqueSeed == 0)
                {
                    uniqueSeed = 666;
                }
                var worldSeed = (uint) beginGenerateVox.planetID;
                if (worldSeed == 0)
                {
                    worldSeed = uniqueSeed * 8;
                }
                //var random = new Random();
                //random.InitState(uniqueSeed);
                // UnityEngine.Debug.LogError("Size: " + vox.voxelDimensions);
                chunkDimensions.voxelDimensions = voxelDimensions;
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

            Dependency = Entities.ForEach((ref VoxColors voxColors, in BeginGenerateVox beginGenerateVox, in Seed seed) =>
            {
                VoxColorsGenerationSystem.GenerateRandomColors(ref voxColors, beginGenerateVox.spawnType, (uint) beginGenerateVox.planetID, (uint) seed.seed);
            }).ScheduleParallel(Dependency);
        }
    }
}*/

/**
*   \todo Put this into entity spawning
*   \todo Generate colours based off race and dna.
*   \todo Voxel dimension problems when changing dimensions to all unique dimensions
*/

/*Dependency = Entities.ForEach((ref GenerateVoxData generateVoxData, ref Seed seed, in BeginGenerateVox beginGenerateVox, in ZoxID zoxID) =>
{
    var uniqueSeed = zoxID.id;
    if (uniqueSeed == 0)
    {
        uniqueSeed = 666;
    }
    // generateVoxData.generationType = GenerateVoxType.Head;
    seed.seed = uniqueSeed;
}).ScheduleParallel(Dependency);*/