using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Voxels
{ 
    //! Generate Vox Shapes in Chunk.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxShapesGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;
        private EntityQuery seedsQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<Vox>(),
                ComponentType.Exclude<EntityBusy>(),
                ComponentType.ReadWrite<Chunk>(),
                ComponentType.ReadWrite<VoxColors>(),
                ComponentType.ReadOnly<VoxShapes>(),
                ComponentType.ReadOnly<GenerateVox>());
            chunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<VoxLink>(),
                ComponentType.Exclude<DestroyEntity>());
            seedsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Seed>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            var seedEntities = seedsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var seeds = GetComponentLookup<Seed>(true);
            chunkEntities.Dispose();
            seedEntities.Dispose();
            Dependency = Entities
                .WithNone<InitializeEntity, SpawnVoxChunk>()
                .WithAll<GenerateVox>()
                .ForEach((Entity e, int entityInQueryIndex, ref Chunk chunk, ref VoxColors voxColors, in VoxShapes voxShapes) =>
            {
                PostUpdateCommands.RemoveComponent<GenerateVox>(entityInQueryIndex, e);
                var seed = new Seed();
                if (seeds.HasComponent(e))
                {
                    seed = seeds[e];
                }
                else if (voxLinks.HasComponent(e))
                {
                    // Get Parent VoxLink seed & generateVoxData
                    var voxEntity = voxLinks[e].vox;
                    if (seeds.HasComponent(voxEntity))
                    {
                        seed = seeds[voxEntity];
                    }
                }
                if (seed.seed == 0)
                {
                    return;
                }
                var random = new Random();
                random.InitState((uint) seed.seed);
                chunk.SetAllToAir();
                //! \todo Optimize colours if there are repeated ones here.
                voxColors.colors = new BlitableArray<float3>(voxShapes.shapes.Length, Allocator.Persistent);
                for (var i = 0; i < voxShapes.shapes.Length; i++)
                {
                    voxColors.colors[i] = voxShapes.shapes[i].color.ToFloat3();
                    // UnityEngine.Debug.LogError("Color: " + e.Index + " :: " + i + " is " + voxColors.colors[i]);
                }
                var size = chunk.voxelDimensions;
                // Generate Head Colors
                // Hair Color
                int3 position;
                var eyeDistanceApart = 2 + (int) random.NextFloat(2);
                for (var i = 0; i < voxShapes.shapes.Length; i++)
                {
                    var voxPlaceIndex = (byte) (i + 1);
                    var colorCheck = voxColors.colors[i];
                    var shape = voxShapes.shapes[i];
                    var shapeCornerPosition = shape.GetCornerPosition();
                    var shapeUpperPosition = shape.GetUpperPosition();
                    var shapeCenterPosition = shape.GetCenterPosition(size);
                    var voxelIndex = 0;
                    for (position.x = 0; position.x < size.x; position.x++)
                    {
                        for (position.y = 0; position.y < size.y; position.y++)
                        {
                            for (position.z = 0; position.z < size.z; position.z++)
                            {
                                if (shape.brushType == VoxBrushType.Paint && chunk.voxels[voxelIndex] == 0)
                                {
                                    voxelIndex++;
                                    continue;
                                }
                                // var isAir = true;
                                var variance = 0;   // shape.variance;
                                var variancePosition = new int3((int)(-variance * 2 + random.NextFloat(variance * 2)), (int)(-variance * 2 + random.NextFloat(variance * 2)),
                                    (int)(-variance * 2 + random.NextFloat(variance * 2)));
                                var shapePosition = shapeCenterPosition + variancePosition;
                                // size
                                var leftSide = shapePosition - shape.size;
                                var rightSide = shapePosition + shape.size;
                                if (shape.type == VoxShapeType.Cube)
                                {
                                    if (position.x >= leftSide.x && position.x <= rightSide.x
                                        && position.y >= leftSide.y && position.y <= rightSide.y
                                        && position.z >= leftSide.z && position.z <= rightSide.z)
                                    {
                                        chunk.voxels[voxelIndex] = voxPlaceIndex;
                                    }
                                }
                                else if (shape.type == VoxShapeType.CubeNonCentred)
                                {
                                    // UnityEngine.Debug.LogError("From: " + shapePosition + " to " + rightSide);
                                    if (position.x >= shapeCornerPosition.x && position.x <= shapeUpperPosition.x
                                        && position.y >= shapeCornerPosition.y && position.y <= shapeUpperPosition.y
                                        && position.z >= shapeCornerPosition.z && position.z <= shapeUpperPosition.z)
                                    {
                                        chunk.voxels[voxelIndex] = voxPlaceIndex;
                                    }
                                }
                                else if (shape.type == VoxShapeType.CurveCubeZ)
                                {
                                    if (position.x >= leftSide.x && position.x <= rightSide.x
                                        && position.y >= leftSide.y && position.y <= rightSide.y
                                        && position.z >= leftSide.z && position.z <= rightSide.z)
                                    {
                                        // todo: modify what curves get added to what sides of the cube and by how much curve
                                        var isInEmptySpace = false;
                                        var positionDiffY = math.abs(position.y - rightSide.y);
                                        var positionRightDiffZ = math.abs(position.z - rightSide.z);
                                        var positionLeftDiffZ = math.abs(position.z - leftSide.z);
                                        var positionRightDiffX = math.abs(position.x - rightSide.x);
                                        for (int spaceCheck2 = 3; spaceCheck2 >= 0; spaceCheck2--)
                                        {
                                            for (int spaceCheck = 0; spaceCheck <= spaceCheck2; spaceCheck++)
                                            {
                                                if (positionDiffY == spaceCheck && positionRightDiffZ == spaceCheck2 - spaceCheck)
                                                {
                                                    isInEmptySpace = true;
                                                    break;
                                                }
                                                if (positionDiffY == spaceCheck && positionLeftDiffZ == spaceCheck2 - spaceCheck)
                                                {
                                                    isInEmptySpace = true;
                                                    break;
                                                }
                                                if (positionDiffY == spaceCheck && positionRightDiffX == spaceCheck2 - spaceCheck)
                                                {
                                                    isInEmptySpace = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if (!isInEmptySpace)
                                        {
                                            chunk.voxels[voxelIndex] = voxPlaceIndex;
                                        }
                                    }
                                }
                                else if (shape.type == VoxShapeType.Sphere)
                                {
                                    var radius = shape.size.ToFloat3();
                                    radius.x = ((int)(radius.x * 2)) / 2;
                                    radius.y = ((int)(radius.y * 2)) / 2;
                                    radius.z = ((int)(radius.z * 2)) / 2;
                                    var isInSphere = false;
                                    for (int a = 0; a <= 1; a++)
                                    {
                                        for (int b = 0; b <= 1; b++)
                                        {
                                            for (int c = 0; c <= 1; c++)
                                            {
                                                var distance = math.distance(position.ToFloat3(), shapePosition.ToFloat3() - new float3(a, b, c));
                                                if (distance <= radius.x && distance <= radius.y && distance <= radius.z)
                                                {
                                                    isInSphere = true;
                                                    a = 2; b = 2; c = 2;
                                                }
                                            }
                                        }
                                    }
                                    if (isInSphere)
                                    {
                                        chunk.voxels[voxelIndex] = voxPlaceIndex;
                                        // isAir = false;
                                    }
                                }
                                else if (shape.type == VoxShapeType.Cylinder)
                                {
                                    // CylinderXZ
                                    var radius = shape.size;
                                    if (position.y >= shapePosition.y - radius.y && 
                                        position.y <= shapePosition.y + radius.y)
                                    {
                                        var isInCylinder = false;
                                        var cylinderRadius =  1 + math.max(radius.x, radius.z) / 2f;
                                        for (int a = 0; a <= 1; a++)
                                        {
                                            for (int b = 0; b <= 1; b++)
                                            {
                                                var distanceXZ = math.distance(new float3(position.x, 0, position.z),
                                                    new float3(shapePosition.x, 0, shapePosition.z) - new float3(a, 0, b));
                                                if (distanceXZ < cylinderRadius)
                                                {
                                                    isInCylinder = true;
                                                    a = 2; b = 2;
                                                }
                                            }
                                        }
                                        if (isInCylinder)
                                        {
                                            // isAir = false;
                                            chunk.voxels[voxelIndex] = voxPlaceIndex;
                                        }
                                    }
                                }
                                voxelIndex++;
                            }
                        }
                    }
                }
			})  .WithReadOnly(voxLinks)
                .WithReadOnly(seeds)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }

}