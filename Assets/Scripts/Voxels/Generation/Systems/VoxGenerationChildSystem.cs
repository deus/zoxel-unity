using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{ 
    //! Pass on event to sub entities.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxGenerationChildSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithNone<SpawnVoxChunk, OnChunksSpawned>()
                .WithAll<GenerateVox>()
                .ForEach((Entity e, int entityInQueryIndex, in Vox vox) =>
            {
                // UnityEngine.Debug.LogError("VoxGenerationChildSystem:" + vox.chunks.Length);
                PostUpdateCommands.RemoveComponent<GenerateVox>(entityInQueryIndex, e);
                for (var i = 0; i < vox.chunks.Length; i++)
                {
                    PostUpdateCommands.AddComponent<GenerateVox>(entityInQueryIndex, vox.chunks[i]);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}