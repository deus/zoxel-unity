using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
// Generate a new vox model for enemies
// TODO:    Test voxel creation of characters at all sizes
//          Also generate body parts for these creatures
//          Create support for multiple chunks streamed on characters
//          Store body parts as seperate voxes, then apply to chunks by sending data there to update on

namespace Zoxel.Voxels
{ 
    //! Generates various model structures in the Chunk.
    /**
    *   \todo Add a Noise Filter.
    *   \todo Convert a shape into a node entity.
    *   \todo Save node stack as a BluePrint.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;
        private EntityQuery seedsQuery;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<Vox>(),
                ComponentType.Exclude<VoxShapes>(),
                ComponentType.ReadOnly<GenerateVox>(),
                ComponentType.ReadWrite<Chunk>());
            chunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<VoxLink>(),
                ComponentType.Exclude<DestroyEntity>());
            seedsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Seed>(),
                ComponentType.ReadOnly<GenerateVoxData>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithNone<Vox, VoxShapes>()
                .WithAll<GenerateVox>()
                .ForEach((ref VoxColors voxColors, in Seed seed) =>
            {
                if (voxColors.colors.Length == 0)
                {
                    VoxColorsGenerationSystem.GenerateRandomColors(ref voxColors, 3, (uint) seed.seed + 666, (uint) seed.seed);
                }
            }).ScheduleParallel(Dependency);
            // Note: does it actually spawn the chunk and build it when i don't generate vox?
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            chunkEntities.Dispose();
            var seedEntities = seedsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var seeds = GetComponentLookup<Seed>(true);
            var generateVoxDatas = GetComponentLookup<GenerateVoxData>(true);
            seedEntities.Dispose();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .WithNone<Vox, VoxShapes>()
                .WithAll<GenerateVox>()
                .ForEach((Entity e, int entityInQueryIndex, ref Chunk chunk) =>
            {
                PostUpdateCommands.RemoveComponent<GenerateVox>(entityInQueryIndex, e);
                var seed = new Seed();
                var generateVoxData = new GenerateVoxData();
                if (seeds.HasComponent(e))
                {
                    seed = seeds[e];
                    generateVoxData = generateVoxDatas[e];
                }
                else if (voxLinks.HasComponent(e))
                {
                    // Get Parent VoxLink seed & generateVoxData
                    var voxEntity = voxLinks[e].vox;
                    if (seeds.HasComponent(voxEntity))
                    {
                        seed = seeds[voxEntity];
                        generateVoxData = generateVoxDatas[voxEntity];
                    }
                }
                if (seed.seed == 0)
                {
                    // UnityEngine.Debug.LogError("Seed is 0.");
                    for (int i = 0; i < chunk.voxels.Length; i++)
                    {
                        chunk.voxels[i] = 1;
                    }
                    return;
                }
                var generationType = generateVoxData.generationType;
                var random = new Random();
                random.InitState((uint) seed.seed);
                // Generate simple sphere for now
                var voxelDimensions = chunk.voxelDimensions;
                var voxelIndex = 0;
                var midPoint = voxelDimensions / 2;
                int3 localPosition;
                // Clear first
                for (int i = 0; i < chunk.voxels.Length; i++)
                {
                    chunk.voxels[i] = 0;
                }
                /*for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                {
                    for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                    {
                        for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
                        {
                            chunk.voxels[voxelIndex] = 0;
                            voxelIndex++;
                        }
                    }
                }*/
                // Head with Eye
                if (generationType == GenerateVoxType.Head)
                {
                    // generate slightly different sizes
                    var skinAIndex = (byte)(1);
                    var skinBIndex = (byte)(2);
                    var eyeOuterIndex = (byte)(4);
                    var eyeInnerIndex = (byte)(5);
                    var size = new int3(voxelDimensions.x / 2, voxelDimensions.y / 2, voxelDimensions.z / 2);
                    var maxSize = math.max(math.max(size.x, size.z), size.y);
                    /*var varianceSize = math.min(voxelDimensions.x / 2 - 1, 3);
                    size.x -= random.NextInt(varianceSize);
                    size.y -= random.NextInt(varianceSize);
                    size.z -= random.NextInt(varianceSize);*/
                    var eyeDistanceApart = 2 + (int) random.NextFloat(3);
                    var heightAddition = (voxelDimensions.y / 2) - size.y;
                    midPoint.y -= heightAddition;
                    voxelIndex = 0;
                    for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                    {
                        for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                        {
                            for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
                            {
                                //int newIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, model.size);
                                // var distance = math.distance(localPosition.ToFloat3(),
                                //     midPoint + new float3(
                                //         -variance *  2 + random.NextFloat(variance *  2),
                                //         -variance *  2 + random.NextFloat(variance *  2),
                                //         -variance *  2 + random.NextFloat(variance *  2)));
                                var variancePosition = new int3(0,0,0); /*
                                        (int)(-variance *  2 + random.NextFloat(variance *  2)),
                                        (int)(-variance *  2 + random.NextFloat(variance *  2)),
                                        (int)(-variance *  2 + random.NextFloat(variance *  2)));*/
                                var distance = localPosition - (midPoint + variancePosition);
                                distance.x = math.abs(distance.x);
                                distance.y = math.abs(distance.y);
                                distance.z = math.abs(distance.z);

                                if (distance.x <= size.x && distance.y <= size.y && distance.z <= size.z) //random.NextFloat(24) + (voxelDimensions.x - distance) * 3 >= 48)
                                {
                                    var distance2 = math.distance(localPosition.ToFloat3(), midPoint.ToFloat3());
                                    if (distance2 <= maxSize)
                                    {
                                        if (random.NextInt(50) >= 44)
                                        {
                                            chunk.voxels[voxelIndex] = skinBIndex;
                                        }
                                        else
                                        {
                                            chunk.voxels[voxelIndex] = skinAIndex;
                                        }
                                    }
                                }
                                // two eyes
                                for (int a = 0; a <= 1; a++)
                                {
                                    var eyePosition = midPoint.ToFloat3() + new float3(0, 2, -size.z + 1f);
                                    if (a == 0)
                                    {
                                        eyePosition.x -= eyeDistanceApart;
                                    }
                                    else
                                    {
                                        eyePosition.x += eyeDistanceApart;
                                    }
                                    var distanceToEye = math.distance(localPosition.ToFloat3(), eyePosition);
                                    if (distanceToEye <= 2) //random.NextFloat(24) + (voxelDimensions.x - distance) * 3 >= 48)
                                    {
                                        if (chunk.voxels[voxelIndex] != 0)
                                        {
                                            chunk.voxels[voxelIndex] = eyeOuterIndex;
                                        }
                                    }
                                    var distanceToEye2 = math.distance(localPosition.ToFloat3(), eyePosition + new float3(0, 0, -1));
                                    if (distanceToEye2 <= 1) //random.NextFloat(24) + (voxelDimensions.x - distance) * 3 >= 48)
                                    {
                                        if (chunk.voxels[voxelIndex] != 0)
                                        {
                                            chunk.voxels[voxelIndex] = eyeInnerIndex;
                                        }
                                    }
                                }
                                voxelIndex++;
                            }
                        }
                    }
                }
                else if (generationType == GenerateVoxType.Grass)
                {
                    var grassStrands = 48 + random.NextInt(32); // 60
                    for (int i = 0; i < grassStrands; i++)
                    {
                        // find a point where it isn't next to others
                        // check if it or the ones next to it is not 0
                        var voxelPosition = new int3(
                            1 + random.NextInt(voxelDimensions.x - 2), 
                            0,
                            1 + random.NextInt(voxelDimensions.z - 2)
                        );
                        var voxelIndex2 = VoxelUtilities.GetVoxelArrayIndex(voxelPosition, voxelDimensions);
                        var voxelData = chunk.voxels[voxelIndex2];
                        if (voxelData == 0)
                        {
                            var voxelIndexForward = VoxelUtilities.GetVoxelArrayIndex(voxelPosition.Forward(), voxelDimensions);
                            var voxelIndexBackward = VoxelUtilities.GetVoxelArrayIndex(voxelPosition.Backward(), voxelDimensions);
                            var voxelIndexLeft = VoxelUtilities.GetVoxelArrayIndex(voxelPosition.Left(), voxelDimensions);
                            var voxelIndexRight = VoxelUtilities.GetVoxelArrayIndex(voxelPosition.Right(), voxelDimensions);
                            if (chunk.voxels[voxelIndexForward] == 0 && chunk.voxels[voxelIndexBackward] == 0 && 
                                chunk.voxels[voxelIndexLeft] == 0 && chunk.voxels[voxelIndexRight] == 0)
                            {
                                // Check nearby ones to make sure not touching them!
                                var grassHeight = 2 + random.NextInt(voxelDimensions.y - 8);
                                var grassType = (byte)(1 + random.NextInt(2));
                                for (int j = 0; j < grassHeight; j++)
                                {
                                    var voxelPositionGrass = voxelPosition + new int3(0, j, 0);
                                    var voxelIndexGrass = VoxelUtilities.GetVoxelArrayIndex(voxelPositionGrass, voxelDimensions);
                                    chunk.voxels[voxelIndexGrass] = grassType;
                                }
                            }
                        }
                    }
                }
                else if (generationType == GenerateVoxType.Torch)
                {
                    // UnityEngine.Debug.LogError("Spawned Torch Generation: " + e.Index);
                    var metalType = (byte) 1;
                    var metalType2 = (byte) 2;
                    var fireType = (byte) 3;
                    var baseHeight = voxelDimensions.y / 6; // 2
                    var headStartHeight = 2 * voxelDimensions.y / 3; // 8
                    var torchHeadSize = voxelDimensions.y / 3;  // 4
                    var headEndHeight = headStartHeight + torchHeadSize; // voxelDimensions.y / 2; // 8
                    var standSize = voxelDimensions.x / 4; // 2
                    voxelIndex = 0;
                    for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                    {
                        for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                        {
                            for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
                            {
                                if (localPosition.y >= headEndHeight)
                                {
                                    voxelIndex++;
                                    continue;
                                }
                                if (localPosition.y >= headStartHeight)
                                {
                                    if (localPosition.x > 0 && localPosition.x < voxelDimensions.x - 1
                                        && localPosition.y > headStartHeight && localPosition.y < voxelDimensions.y - 1)
                                    {
                                        chunk.voxels[voxelIndex] = fireType;
                                    }
                                    else if (localPosition.z > 0 && localPosition.z < voxelDimensions.z - 1
                                        && localPosition.y > headStartHeight && localPosition.y < voxelDimensions.y - 1)
                                    {
                                        chunk.voxels[voxelIndex] = fireType;
                                    }
                                    else if (localPosition.x > 0 && localPosition.x < voxelDimensions.x - 1
                                        && localPosition.z > 0 && localPosition.z < voxelDimensions.z - 1
                                        && localPosition.y == voxelDimensions.y - 1)
                                    {
                                        chunk.voxels[voxelIndex] = fireType;
                                    }
                                    // if not corner parts of torch head
                                    else if (localPosition != new int3(0, voxelDimensions.y - 1, 0)
                                        && localPosition != new int3(voxelDimensions.x - 1, voxelDimensions.y - 1, 0)
                                        && localPosition != new int3(0, voxelDimensions.y - 1, voxelDimensions.z - 1)
                                        && localPosition != new int3(voxelDimensions.x - 1, voxelDimensions.y - 1, voxelDimensions.z - 1)
                                        && localPosition != new int3(0, headStartHeight, 0)
                                        && localPosition != new int3(voxelDimensions.x - 1, headStartHeight, 0)
                                        && localPosition != new int3(0, headStartHeight, voxelDimensions.z - 1)
                                        && localPosition != new int3(voxelDimensions.x - 1, headStartHeight, voxelDimensions.z - 1))
                                    {
                                        chunk.voxels[voxelIndex] = metalType2;
                                    }
                                }
                                // stand
                                else if (localPosition.y >= baseHeight && localPosition.y < headStartHeight)
                                {
                                    if (localPosition.x >= standSize && localPosition.x <= voxelDimensions.x - 1 - standSize
                                        && localPosition.z >= standSize && localPosition.z <= voxelDimensions.z - 1 - standSize)
                                    {
                                        chunk.voxels[voxelIndex] = metalType;
                                        //! \todo Torch: Add bands on stand with rust color!
                                    }
                                }
                                // base
                                else
                                {
                                    chunk.voxels[voxelIndex] = metalType2;
                                }
                                voxelIndex++;
                            }
                        }
                    }
                }
                else if (generationType == GenerateVoxType.ItemChest)
                {
                    var woodType = (byte) 1;
                    // var wood2Type = (byte) 2;
                    var metalType = (byte) 2;
                    // var metal2Type = (byte) 4;
                    // lid
                    var lidStartHeight = (int) (7.4f * voxelDimensions.y / 10); //  7;
                    // base
                    // lock position
                    voxelIndex = 0;
                    var lidShrink = 0;
                    for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                    {
                        for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                        {
                            for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
                            {
                                if (localPosition.y > lidStartHeight)
                                {
                                    lidShrink = (localPosition.y - lidStartHeight);
                                    if (localPosition.x < lidShrink || localPosition.x > voxelDimensions.x - 1 - lidShrink
                                        || localPosition.z < lidShrink || localPosition.z > voxelDimensions.z - 1 - lidShrink)
                                    {
                                        voxelIndex++;
                                        continue;
                                    }
                                }
                                else
                                {
                                    lidShrink = 0;
                                }
                                if (localPosition.y == lidStartHeight || localPosition.y == lidStartHeight - 1
                                    // Bottom edges
                                    || (localPosition.x == lidShrink && localPosition.y == 0)
                                    || (localPosition.z == lidShrink && localPosition.y == 0)
                                    || (localPosition.x == -lidShrink + voxelDimensions.x - 1 && localPosition.y == 0)
                                    || (localPosition.z == -lidShrink + voxelDimensions.z - 1 && localPosition.y == 0)
                                    // Top edges
                                    || (localPosition.x == lidShrink && localPosition.y == voxelDimensions.y - 1)
                                    || (localPosition.z == lidShrink && localPosition.y == voxelDimensions.y - 1)
                                    || (localPosition.x == -lidShrink + voxelDimensions.x - 1 && localPosition.y == voxelDimensions.y - 1)
                                    || (localPosition.z == -lidShrink + voxelDimensions.z - 1 && localPosition.y == voxelDimensions.y - 1)
                                    // Side edges
                                    || (localPosition.x == lidShrink && localPosition.z == 0)
                                    || (localPosition.x == lidShrink && localPosition.z == voxelDimensions.z - 1)
                                    || (localPosition.x == -lidShrink + voxelDimensions.x - 1 && localPosition.z == 0)
                                    || (localPosition.x == -lidShrink + voxelDimensions.x - 1 && localPosition.z == voxelDimensions.z - 1)
                                    )
                                {
                                    chunk.voxels[voxelIndex] = metalType;
                                }
                                else
                                {
                                    chunk.voxels[voxelIndex] = woodType;
                                }
                                voxelIndex++;
                            }
                        }
                    }
                }
                else if (generationType == GenerateVoxType.Door)
                {
                    // todo: add door knob
                    // todo: make look a bit more like wood - different planks of different wood colors
                    var frontCore = (byte) 1;
                    var backCore = (byte) 1;
                    var frontFrame = (byte) 2;
                    var backFrame = (byte) 2;
                    var frameSize = voxelDimensions.x / 16;
                    voxelIndex = 0;
                    for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                    {
                        for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                        {
                            for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
                            {
                                // var distanceToMid = math.distance(localPosition.ToFloat3(), midPoint.ToFloat3());
                                /*if (localPosition.z < voxelDimensions.z / 2f)
                                {
                                    if (localPosition.x < voxelDimensions.x / 2f)
                                    {
                                        chunk.voxels[voxelIndex] = frontCore;
                                    }
                                    else
                                    {
                                        chunk.voxels[voxelIndex] = frontFrame;
                                    }
                                }
                                else
                                {
                                    if (localPosition.x < voxelDimensions.x / 2f)
                                {
                                        chunk.voxels[voxelIndex] = frontFrame;
                                    }
                                    else
                                    {
                                        chunk.voxels[voxelIndex] = frontCore;
                                    }
                                }*/
                                // frame thickness
                                if ((((localPosition.x <= frameSize) || (localPosition.x >= voxelDimensions.x - 1 - frameSize))
                                    || (localPosition.z <= frameSize)
                                    || (localPosition.y >= voxelDimensions.y - 1 - frameSize))
                                        && ((localPosition.y <= frameSize) || (localPosition.y >= voxelDimensions.y - 1 - frameSize)))
                                {
                                    chunk.voxels[voxelIndex] = backFrame;
                                }
                                else if (((localPosition.x <= frameSize || localPosition.x >= voxelDimensions.x - 1 - frameSize)
                                    || (localPosition.y <= frameSize ||  localPosition.y >= voxelDimensions.y - 1 - frameSize)) &&
                                    (localPosition.z <= frameSize ||  localPosition.z >= voxelDimensions.z - 1 - frameSize))
                                {
                                    chunk.voxels[voxelIndex] = frontFrame; // (byte)(2);
                                }
                                else if (!(localPosition.z == 0 || localPosition.z == voxelDimensions.z - 1))
                                {
                                    if (localPosition.z == voxelDimensions.z - 2)
                                    {
                                        chunk.voxels[voxelIndex] = backCore; // (byte)(1);
                                    }
                                    else
                                    {
                                        chunk.voxels[voxelIndex] = frontCore; // (byte)(1);
                                    }
                                }
                                voxelIndex++;
                            }
                        }
                    }
                }
                else if (generationType == GenerateVoxType.Bed)
                {
                    // UnityEngine.Debug.LogError("Spawning Bed");
                    // voxel.voxelDimensions = new int3(16, 8, 32);
                    // todo: add door knob
                    // todo: make look a bit more like wood - different planks of different wood colors
                    var frontCore = (byte) 2;
                    var backCore = (byte) 2;
                    var frontFrame = (byte) 1;
                    var backFrame = (byte) 1;
                    voxelIndex = 0;
                    var bedLegHeight = voxelDimensions.y / 4; // 2
                    for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                    {
                        for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                        {
                            for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
                            {
                                if (localPosition.y < bedLegHeight)
                                {
                                    if ((localPosition.z <= bedLegHeight || localPosition.z >= voxelDimensions.z - 1 - bedLegHeight)
                                        && (localPosition.x <= bedLegHeight || localPosition.x >= voxelDimensions.x - 1 - bedLegHeight))
                                    {
                                        chunk.voxels[voxelIndex] = backFrame; // (byte)(3);
                                    }
                                    voxelIndex++;
                                    continue;
                                }
                                // var distanceToMid = math.distance(localPosition.ToFloat3(), midPoint.ToFloat3());
                                // frame thickness
                                if (((localPosition.x == 0 || localPosition.x == voxelDimensions.x - 1)
                                    || (localPosition.z == 0 ||  localPosition.z == voxelDimensions.z - 1)) &&
                                    (localPosition.y == bedLegHeight ||  localPosition.y == voxelDimensions.y - 1))
                                {
                                        chunk.voxels[voxelIndex] = backFrame; // (byte)(3);
                                }
                                else if (((localPosition.x == 0 || localPosition.x == voxelDimensions.x - 1)
                                    || (localPosition.y == bedLegHeight ||  localPosition.y == voxelDimensions.y - 1)) &&
                                    (localPosition.z == 0 ||  localPosition.z == voxelDimensions.z - 1))
                                {
                                    chunk.voxels[voxelIndex] = frontFrame; // (byte)(2);
                                }
                                else if (!(localPosition.z == 0 || localPosition.z == voxelDimensions.z - 1))
                                {
                                    if (localPosition.z == voxelDimensions.z - 2)
                                    {
                                        chunk.voxels[voxelIndex] = backCore; // (byte)(1);
                                    }
                                    else
                                    {
                                        chunk.voxels[voxelIndex] = frontCore; // (byte)(1);
                                    }
                                }
                                voxelIndex++;
                            }
                        }
                    }
                }
                else if (generationType == GenerateVoxType.Bullet)
                {
                    voxelIndex = 0;
                    var midDistance = voxelDimensions.min; // math.min(voxelDimensions.x, voxelDimensions.z);
                    for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                    {
                        for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                        {
                            for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
                            {
                                var distanceToMid = math.distance(localPosition.ToFloat3(), midPoint.ToFloat3());
                                if (distanceToMid < 6)
                                {
                                    chunk.voxels[voxelIndex] = (byte)(1);
                                    /*if (random.NextInt(10) >= 6)
                                    {
                                        chunk.voxels[voxelIndex] = (byte)(2);
                                    }
                                    else
                                    {
                                        chunk.voxels[voxelIndex] = (byte)(1);
                                    }*/
                                }
                                voxelIndex++;
                            }
                        }
                    }
                }
                else if (generationType == GenerateVoxType.Cloud)
                {
                    voxelIndex = 0;
                    var midDistance = voxelDimensions.min / 2f; // math.min(voxelDimensions.x, voxelDimensions.z);
                    for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                    {
                        for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                        {
                            for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
                            {
                                var distanceToMid = 1f * math.distance(new float3(
                                    /*localPosition.x * (1f * voxelDimensions.x / (voxelDimensions.y + voxelDimensions.z)),
                                    localPosition.y * (1f * voxelDimensions.y / (voxelDimensions.x + voxelDimensions.z)),
                                    localPosition.z * (1f * voxelDimensions.z / (voxelDimensions.y + voxelDimensions.x))),*/
                                    localPosition.x * ((voxelDimensions.y + voxelDimensions.z) / voxelDimensions.x),
                                    localPosition.y * ((voxelDimensions.x + voxelDimensions.z) / voxelDimensions.y),
                                    localPosition.z * ((voxelDimensions.y + voxelDimensions.x) / voxelDimensions.z)),
                                    midPoint.ToFloat3());
                                // var distanceToMid = math.distance(localPosition.ToFloat3(), midPoint.ToFloat3());
                                /*var distanceX = math.abs(localPosition.x - voxelDimensions.x / 2) + random.NextInt(-2, 1);
                                var distanceY = math.abs(localPosition.y - voxelDimensions.y / 2) + random.NextInt(-2, 1);
                                var distanceZ = math.abs(localPosition.z - voxelDimensions.z / 2) + random.NextInt(-2, 1);*/
                                /*if (distanceX < voxelDimensions.x / 2 - 1
                                    && distanceY < voxelDimensions.y / 2 - 1
                                    && distanceZ < voxelDimensions.z / 2 - 1)*/

                                if (distanceToMid <= midDistance)
                                {
                                    chunk.voxels[voxelIndex] = (byte)(1);
                                }
                                voxelIndex++;
                            }
                        }
                    }
                }
                if (!HasComponent<ChunkDataOnly>(e))
                {
                    PostUpdateCommands.AddComponent<ChunkBuilder>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<EntityBusy>(entityInQueryIndex, e);
                }
			})  .WithReadOnly(voxLinks)
                .WithReadOnly(seeds)
                .WithReadOnly(generateVoxDatas)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }

}