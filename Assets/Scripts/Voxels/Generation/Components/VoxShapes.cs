using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels
{
    //! Vox Shapes used to generate VoxData.
    public struct VoxShapes : IComponentData
    {
        public BlitableArray<VoxShape> shapes;

        public VoxShapes(in NativeList<VoxShape> shapes)
        {
            this.shapes = new BlitableArray<VoxShape>(shapes.Length, Allocator.Persistent);
            for (int i = 0; i < shapes.Length; i++)
            {
                this.shapes[i] = shapes[i];
            }
        }

        public void DisposeFinal()
        {
            shapes.DisposeFinal();
        }
    }
}