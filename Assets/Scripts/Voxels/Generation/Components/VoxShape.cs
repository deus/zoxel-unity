using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
// todo: make vox shapes seperate entities
// todo: Visualize vox shapes with node graph

namespace Zoxel.Voxels
{
    //! A instruction for generation a Vox.
    public struct VoxShape : IComponentData
    {
        public byte type;   // 0 for rect, 1 for sphere, 
        public byte anchor; // center, top, bottom, etc
        public byte brushType;
        public int3 position;
        public int3 size;
        public ColorRGB color;
        public const int byteSize = 30;

        public void AddBytes(ref NativeList<byte> bytes)
        {
            bytes.Add(type);
            bytes.Add(anchor);
            bytes.Add(brushType);
            ByteUtil.AddInt3(ref bytes, position);
            ByteUtil.AddInt3(ref bytes, size);
            ByteUtil.AddColorRGB(ref bytes, color);
        }

        public VoxShape(in byte[] bytes, int index)
        {
            type = bytes[index];
            anchor = bytes[index + 1];
            brushType = bytes[index + 2];
            position = ByteUtil.GetInt3(in bytes, index + 3);
            size = ByteUtil.GetInt3(in bytes, index + 15);
            color = ByteUtil.GetColorRGB(in bytes, index + 27);
        }

        public VoxShape(in BlitableArray<byte> bytes, int index)
        {
            type = bytes[index];
            anchor = bytes[index + 1];
            brushType = bytes[index + 2];
            position = ByteUtil.GetInt3(in bytes, index + 3);
            size = ByteUtil.GetInt3(in bytes, index + 15);
            color = ByteUtil.GetColorRGB(in bytes, index + 27);
        }

        public VoxShape(ColorRGB color, byte type, int3 size)
        {
            this.type = type;
            this.anchor = 0;
            this.position = new int3();
            this.size = size;
            this.brushType = VoxBrushType.Fill;
            this.color = color;
        }

        public VoxShape(ColorRGB color, byte type, int3 position, int3 size)
        {
            this.type = type;
            this.anchor = 0;
            this.position = position;
            this.size = size;
            this.brushType = VoxBrushType.Fill;
            this.color = color;
        }

        public VoxShape(ColorRGB color, byte type, byte voxBrushType, int3 position, int3 size)
        {
            this.type = type;
            this.brushType = voxBrushType;
            this.anchor = 0;
            this.position = position;
            this.size = size;
            this.color = color;
        }

        public int3 GetCenterPosition(int3 modelSize)
        {
            var positionOutput = this.position;
            if (anchor == 0)
            {
                positionOutput += modelSize / 2;
            }
            return positionOutput;
        }

        public int3 GetCornerPosition()
        {
            return this.position;
        }

        public int3 GetUpperPosition()
        {
            return this.position + size;
        }
    }
}