using Unity.Entities;

namespace Zoxel.Voxels
{
    //! An event to generate Vox.
    public struct GenerateVox : IComponentData { }
}