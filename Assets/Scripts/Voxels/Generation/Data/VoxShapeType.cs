using Unity.Entities;

namespace Zoxel.Voxels
{
    public static class VoxShapeType
    {
        public const byte Cube = 0;
        public const byte Sphere = 1;
        public const byte Cylinder = 2;
        public const byte Pyramid = 3;
        public const byte CurveCubeZ = 4;
        public const byte CubeNonCentred = 5;
    }
}