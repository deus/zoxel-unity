using Unity.Entities;

namespace Zoxel.Voxels
{
    public static class VoxBrushType
    {
        public const byte Fill = 0;
        public const byte Paint = 1;
    }
}