namespace Zoxel.Voxels
{
    public static class GenerateVoxType
    {
        public const byte Shapes = 0;
        public const byte Head = 1;
        public const byte Grass = 2;
        public const byte Bullet = 3;
        public const byte Cloud = 4;
        public const byte Door = 5;
        public const byte Bed = 6;
        public const byte Torch = 7;
        public const byte Chest = 8;
        public const byte ItemChest = 9;
    }
}