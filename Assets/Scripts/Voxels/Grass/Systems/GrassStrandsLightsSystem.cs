using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Voxels.Minivoxes;
using Zoxel.Voxels.Lighting;
using Zoxel.Voxels.Lighting.Authoring;

namespace Zoxel.Voxels.Grasses
{
	//! Updates GrassStrand's Lighting.
	/**
	*	\todo Combine with character lighting updates.
	*/
	[BurstCompile, UpdateInGroup(typeof(GrassSystemGroup))]
    public partial class GrassStrandsLightsSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery grassesQuery;
		
		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			grassesQuery = GetEntityQuery(
				ComponentType.ReadOnly<UpdateEntityLighting>(),
				ComponentType.ReadOnly<MaterialBaseColor>(),
				ComponentType.ReadOnly<Grass>(),
				ComponentType.Exclude<InitializeEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(grassesQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var grassEntities = grassesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var updateMinivoxLightings = GetComponentLookup<UpdateEntityLighting>(true);
			grassEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<InitializeEntity>()
				.WithAll<GrassStrand>()
				.ForEach((Entity e, int entityInQueryIndex, ref MaterialBaseColor materialBaseColor, ref OutlineColor outlineColor, ref EntityLighting entityLighting,
					in BaseColor baseColor, in BaseOutlineColor baseOutlineColor, in ParentLink parentLink) =>
			{
				if (!updateMinivoxLightings.HasComponent(parentLink.parent))
				{
					return;
				}
				var updateEntityLighting = updateMinivoxLightings[parentLink.parent];
				entityLighting.lightValue = updateEntityLighting.lightValue;
				float brightness = LightingCurve.lightValues[updateEntityLighting.lightValue];
				//! \todo Adjust brightness for selected entity as well.
                if (!HasComponent<EntitySelected>(parentLink.parent))
                {
					materialBaseColor.Value = new float4(
						baseColor.color.x * brightness,
						baseColor.color.y * brightness,
						baseColor.color.z * brightness,
						materialBaseColor.Value.w);
					outlineColor.color = new float4(
						baseOutlineColor.outlineColor.x * brightness,
						baseOutlineColor.outlineColor.y * brightness,
						baseOutlineColor.outlineColor.z * brightness,
						outlineColor.color.w);
				}
                // UnityEngine.Debug.LogError("brightness: " + brightness);
			})	.WithReadOnly(updateMinivoxLightings)
				.ScheduleParallel(Dependency);
			SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<UpdateEntityLighting>(grassesQuery);
		}
	}
}

//materialBaseColor.Value = new float4(brightness, brightness, brightness, materialBaseColor.Value.w);
// UnityEngine.Debug.LogError("Grass Strand: " + brightness);