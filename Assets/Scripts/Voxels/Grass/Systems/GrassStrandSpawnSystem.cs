using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Voxels.Minivoxes;
using Zoxel.Voxels.Lighting;
using Zoxel.Voxels.Authoring;
using Zoxel.Voxels.Lighting.Authoring;

namespace Zoxel.Voxels.Grasses
{
    //! Spawns grass strands from a grass with InitilizationEntity.
    /**
    *   Optimization difficulties originally spawning. Make sure to test again. Memory useage and speed.
    *   Why does it have to run before MinivoxInitializeSystem?
    */
	[BurstCompile, UpdateInGroup(typeof(GrassSystemGroup))]
    public partial class GrassStrandSpawnSystem : SystemBase
	{
        private const int maxGrassInitializations = 64;
        const int maxInt = 214748; // 3647;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private Entity grassStrandPrefab;
        private Entity grassStrandDisabledPrefab;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var grassStrandArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
				typeof(GrassStrand),
                typeof(BaseColor),
                typeof(BaseOutlineColor),
				typeof(Seed),
                typeof(EntityLighting),
				typeof(MaterialBaseColor),
                typeof(OutlineColor),
                typeof(GrassWindDirection),
                typeof(GrassWindTargetDirection),
                typeof(ParentLink),
                typeof(DestroyEntityDirect),
				typeof(Translation),
				typeof(Rotation),
				typeof(Scale)
            );
            grassStrandPrefab = EntityManager.CreateEntity(grassStrandArchetype);
            grassStrandDisabledPrefab = EntityManager.CreateEntity(grassStrandArchetype);
            EntityManager.AddComponent<DisableRender>(grassStrandDisabledPrefab);
            RequireForUpdate<GrassSettings>();
            RequireForUpdate<LightsSettings>();
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var grassSettings = GetSingleton<GrassSettings>();
            var lightsSettings = GetSingleton<LightsSettings>();
            var adjacentGrassStrandChance = 40;
			var brightestBright = (float) lightsSettings.brightestBright;
            var grassStrandColorVariance = grassSettings.grassStrandColorVariance; // 0.06f;
            var spawnChance = grassSettings.grassStrandSpawnChance;
            var grassStrandsCount = grassSettings.grassStrandsCount;
            if (grassStrandsCount.y > 255)
            {
                grassStrandsCount.y = 255;
            }
            var grassResolution = grassSettings.grassResolution; // 16f * resolution;
            var resolution = grassResolution / 16f;
            var positionBounds = new int2(-8, 8) * resolution;
            var elapsedTime = World.Time.ElapsedTime;
            var grassStrandPrefab = this.grassStrandPrefab;
            var grassStrandDisabledPrefab = this.grassStrandDisabledPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var grassEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var minivoxColors = GetComponentLookup<MinivoxColor>(true);
            var chunkLinks = GetComponentLookup<ChunkLink>(true);
            grassEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
				.WithAll<InitializeGrass, Grass>()
				.ForEach((Entity e, int entityInQueryIndex, ref Seed seed, ref MaterialBaseColor materialBaseColor, in Translation translation, in Rotation rotation,
                    in EntityLighting entityLighting, in VoxScale voxScale) =>
			{
                if (entityInQueryIndex >= maxGrassInitializations)
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<InitializeGrass>(entityInQueryIndex, e);
                var positions = new NativeList<int2>();
                var minivoxColor = minivoxColors[e];
                var chunkLink = chunkLinks[e];
                var idRandom = new Random();
                idRandom.InitState((uint) math.max(1, (1 + elapsedTime) * 333 + entityInQueryIndex * 666));
                seed.seed = idRandom.NextInt(1, maxInt);
                var random = new Random();
                random.InitState((uint) seed.seed);
                var grassColor = minivoxColor.color;
                var grassOutlineColor = minivoxColor.secondaryColor.ToFloat4();
                materialBaseColor.Value = new float4(grassColor.ToFloat3(), 1f);
                var lightValue = entityLighting.lightValue; // (byte) 6;
				var brightness = LightingCurve.lightValues[lightValue];
				materialBaseColor.Value = new float4(materialBaseColor.Value.x * brightness, materialBaseColor.Value.y * brightness, 
                    materialBaseColor.Value.z * brightness, materialBaseColor.Value.w);
                var grassChance = random.NextInt(spawnChance.x, spawnChance.y); // 60
                var maxStrands = random.NextInt(grassStrandsCount.x, grassStrandsCount.y);
                int2 spawnXZ;
                const int maxChecks = 256;
                var count = 0;
                byte strandsCount = 0;
                var positionBounds2 = new int2((int) (positionBounds.x * voxScale.scale.x), (int) (positionBounds.y * voxScale.scale.z));
                while (strandsCount <= maxStrands && count <= maxChecks)
                {
                    spawnXZ.x = random.NextInt(positionBounds2.x, positionBounds2.y);
                    spawnXZ.y = random.NextInt(positionBounds2.x, positionBounds2.y);
                    if (positions.Contains(spawnXZ))
                    {
                        continue;
                    }
                    if (random.NextInt(100) >= adjacentGrassStrandChance &&
                        // is adjacent grass strand
                        (positions.Contains(spawnXZ.Up()) || positions.Contains(spawnXZ.Down()) || positions.Contains(spawnXZ.Left()) || positions.Contains(spawnXZ.Right())))
                    {
                        continue;
                    }
                    positions.Add(spawnXZ);
                    //UnityEngine.Debug.DrawLine(spawnPosition, spawnPosition + new float3(0, 1f, 0), UnityEngine.Color.green, 30f);
                    strandsCount++;
                    count++;
                }
                // spawn grass strands
                float3 spawnPosition;
                var strandEntities = new NativeArray<Entity>(positions.Length, Allocator.Temp);
                var grassStrandPrefab2 = grassStrandPrefab;
                var isDisabled = HasComponent<DisableRender>(chunkLink.chunk);
                if (isDisabled)
                {
                    grassStrandPrefab2 = grassStrandDisabledPrefab;
                }
                PostUpdateCommands.Instantiate(entityInQueryIndex, grassStrandPrefab2, strandEntities);
                PostUpdateCommands.AddComponent(entityInQueryIndex, strandEntities, new ParentLink(e));
                for (int i = 0; i < positions.Length; i++)
                {
                    var position = positions[i];
                    spawnPosition = translation.Value + math.rotate(rotation.Value, new float3(position.x + voxScale.scale.x / 2f, 0, position.y + voxScale.scale.z / 2f) / grassResolution);
                        // + new float3(0, voxScale.scale.y / 2f, 0));
                        // + new float3(0, voxScale.scale.y / 4f, 0));
                    var e2 = strandEntities[i]; // PostUpdateCommands.Instantiate(entityInQueryIndex, grassStrandPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Zoxel.Transforms.Child(i));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Translation { Value = spawnPosition });
                }
                // onChildrenSpawnedUnordered.spawned = strandsCount;
                strandEntities.Dispose();
                positions.Dispose();
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChildrenSpawned(strandsCount));
            })  .WithReadOnly(minivoxColors)
                .WithReadOnly(chunkLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}

//PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChildrenSpawnedUnordered(strandsCount));
//UnityEngine.Debug.DrawLine(translation.Value, translation.Value + new float3(0, 3f, 0), UnityEngine.Color.cyan, 30f);
// UnityEngine.Debug.LogError("Spawned Strands: " + strandsCount + " out of " + maxStrands + " with height " + grassHeight);
// UnityEngine.Debug.LogError("grassOutlineColor: " + grassOutlineColor);
// UnityEngine.Debug.LogError("lightValue: " + lightValue);