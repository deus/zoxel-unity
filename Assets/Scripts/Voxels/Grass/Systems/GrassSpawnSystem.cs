using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Voxels.Lighting;
using Zoxel.Voxels.Minivoxes;
using Zoxel.Voxels.Authoring;
using Zoxel.Voxels.Lighting.Authoring;

namespace Zoxel.Voxels.Grasses
{
	//! Spawns Grass entities from our chunk voxel data.
	[BurstCompile, UpdateInGroup(typeof(GrassSystemGroup))]
    public partial class GrassSpawnSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery worldQuery;
        private EntityQuery voxelQuery;
        private Entity grassPrefab;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            worldQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.ReadOnly<Planet>(),
				ComponentType.ReadOnly<ZoxID>(),
				ComponentType.ReadOnly<VoxelLinks>(),
				ComponentType.ReadOnly<RealmLink>());
            voxelQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.ReadOnly<Voxel>());
            var grassArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
				// typeof(InitializeEntity),
				typeof(InitializeGrass),
				typeof(NewMinivox),
				typeof(Minivox),
                typeof(ZoxID),
				typeof(ChunkLink),
                typeof(Seed),
				typeof(Grass),
				typeof(GrassWind),
				typeof(MinivoxColor),
				typeof(MaterialBaseColor),
				typeof(Childrens),
                typeof(VoxelPosition),
				typeof(EntityLighting),
				typeof(Translation),
				typeof(Rotation),
				typeof(Scale),
				typeof(VoxScale)
            );
            grassPrefab = EntityManager.CreateEntity(grassArchetype);
			EntityManager.SetComponentData(grassPrefab, new Scale { Value = 1 });
			EntityManager.SetComponentData(grassPrefab, new MaterialBaseColor(1, 1, 1, 1));
            RequireForUpdate<GrassSettings>();
            RequireForUpdate<LightsSettings>();
            RequireForUpdate(processQuery);
            RequireForUpdate(worldQuery);
            RequireForUpdate(voxelQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var grassSettings = GetSingleton<GrassSettings>();
            if (grassSettings.disableGrass)
            {
                return;
            }
            var lightsSettings = GetSingleton<LightsSettings>();
			var maxGrassPerChunk = grassSettings.maxGrassPerChunk;
            var disableVoxelLighting = lightsSettings.disableVoxelLighting;
			var brightestBright = lightsSettings.brightestBright;
			var positionOffset = new float3(0.5f, 0.5f, 0.5f);
			var degreesToRadians = ((math.PI * 2) / 360f);
			var upRotation = quaternion.identity;
			var downRotation = quaternion.EulerXYZ(new float3(180, 0, 0) * degreesToRadians);
			var rightRotation = quaternion.EulerXYZ(new float3(0, 0, -90) * degreesToRadians);
			var leftRotation = quaternion.EulerXYZ(new float3(0, 0, 90) * degreesToRadians);
			var backwardRotation = quaternion.EulerXYZ(new float3(-90, 0, 0) * degreesToRadians);
			var forwardRotation = quaternion.EulerXYZ(new float3(90, 0, 0) * degreesToRadians);  
			var minivoxesSpawnedPrefab = MinivoxSystemGroup.minivoxesSpawnedPrefab;
			var minivoxesRemovedPrefab = MinivoxSystemGroup.minivoxesRemovedPrefab;
			var grassPrefab = this.grassPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var worldEntities = worldQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var worldZoxIDs = GetComponentLookup<ZoxID>(true);
            var worldVoxelLinks = GetComponentLookup<VoxelLinks>(true);
            var worldRealmLinks = GetComponentLookup<RealmLink>(true);
			var voxScales = GetComponentLookup<VoxScale>(true);
			worldEntities.Dispose();
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxels2 = GetComponentLookup<Voxel>(true);
			voxelEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<DestroyChunkMinivoxes, EntityBusy>()
				.WithAll<SpawnMinivoxes>()
				.ForEach((Entity e, int entityInQueryIndex, in MinivoxLinks minivoxLinks, in Chunk chunk, in VoxLink voxLink, in ChunkPosition chunkPosition,
					in ChunkVoxelRotations chunkVoxelRotations, in ChunkLights chunkLights) =>
			{
				if (chunk.voxels.Length == 0 || !HasComponent<VoxelLinks>(voxLink.vox))
				{
					return;
				}
				var voxEntity = voxLink.vox;
				var voxelDimensions = chunk.voxelDimensions;
				var worldID = (uint) worldZoxIDs[voxEntity].id;
				var voxelLinks = worldVoxelLinks[voxEntity];
				var voxels = new NativeArray<Voxel>(voxelLinks.voxels.Length, Allocator.Temp);
				var grassTypes = new NativeList<byte>();
				var spawnLocalPositions = new NativeList<int3>();
				var totalGrassPositions = new NativeList<int3>();
				for (int i = 0; i < voxels.Length; i++)
				{
					voxels[i] = voxels2[voxelLinks.voxels[i]];
				}
				var realmEntity = worldRealmLinks[voxEntity].realm;
				var chunkVoxelPosition = chunkPosition.GetVoxelPosition(voxelDimensions);
				var voxelIndex = 0;
				byte voxelMetaIndex = 0;
				byte voxelMetaIndexMinusAir = 0;
				Voxel voxelMeta;
				// byte meshIndex;
				int3 localPosition;
				for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
				{
					for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
					{
						for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
						{
							if (spawnLocalPositions.Length >= maxGrassPerChunk)
							{
								voxelIndex++;
								continue;
							}
							voxelMetaIndex = chunk.voxels[voxelIndex];  //((int) - 1);
							if (voxelMetaIndex == 0)
							{
								voxelIndex++;
								continue;
							}
							voxelMetaIndexMinusAir = (byte)(voxelMetaIndex - 1);
							if (voxelMetaIndexMinusAir >= voxelLinks.voxels.Length)
							{
								voxelIndex++;
								continue;
							}
							var voxelEntity = voxelLinks.voxels[voxelMetaIndexMinusAir];
							if (HasComponent<GrassVoxel>(voxelEntity))
							{
								spawnLocalPositions.Add(localPosition);
								grassTypes.Add(voxelMetaIndexMinusAir);
							}
							voxelIndex++;
						}
					}
				}
				// Spawning Entities!
				var voxScale = voxScales[voxEntity].scale;
				// var realmLink = new RealmLink(realmEntity);
				var newGrassPositions = new NativeList<int3>();
				var newGrassLocalPositions = new NativeList<int3>();
				var grassTypesAdded = new NativeList<byte>();
				for (int i = 0; i < grassTypes.Length; i++)
				{
					var minivoxLocalPosition = spawnLocalPositions[i];
					var minivoxGlobalPosition = chunkVoxelPosition + minivoxLocalPosition;
					totalGrassPositions.Add(minivoxGlobalPosition);
					if (!minivoxLinks.HasMinivox(minivoxGlobalPosition))
					{
						newGrassPositions.Add(minivoxGlobalPosition);
						newGrassLocalPositions.Add(minivoxLocalPosition);
						grassTypesAdded.Add(grassTypes[i]);
					}
				}
				if (grassTypesAdded.Length > 0)
				{
					var grassEntities = new NativeArray<Entity>(grassTypesAdded.Length, Allocator.Temp);
					PostUpdateCommands.Instantiate(entityInQueryIndex, grassPrefab, grassEntities);
					PostUpdateCommands.AddComponent(entityInQueryIndex, grassEntities, new ChunkLink(e));
					PostUpdateCommands.AddComponent(entityInQueryIndex, grassEntities, new VoxScale(voxScale));
					for (int i = 0; i < grassTypesAdded.Length; i++)
					{
						var grassType = grassTypesAdded[i];
						var minivoxGlobalPosition = newGrassPositions[i];
						var minivoxLocalPosition = newGrassLocalPositions[i];
						voxelMeta = voxels[grassType];
						var voxelMetaEntity = voxelLinks.voxels[grassType];
						var voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(minivoxLocalPosition, voxelDimensions);
						var spawnPosition = minivoxGlobalPosition.ToFloat3() + positionOffset;
						spawnPosition.x *= voxScale.x;
						spawnPosition.y *= voxScale.y;
						spawnPosition.z *= voxScale.z;
						var voxelRotation = chunkVoxelRotations.GetRotation(minivoxLocalPosition);
						var rotation = BlockRotation.GetRotation(voxelRotation);
						// Spawn
						var e2 = grassEntities[i];
						// Set minivox lighting based on chunk lights
						// UnityEngine.Debug.LogError("lightValue: " + chunkLights.lights[voxelArrayIndex]);
						PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new EntityLighting(chunkLights.lights[voxelArrayIndex]));
						PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new VoxelPosition(minivoxGlobalPosition));
						PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Minivox(spawnPosition, rotation));
						PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new MinivoxColor(voxelMeta.color, voxelMeta.secondaryColor));
						PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Rotation { Value = rotation });
						PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Translation { Value = spawnPosition });
					}
					grassEntities.Dispose();
				}
				//! Remove Removed Grass's.
				var grassesRemoved = new NativeList<int3>();
				if (minivoxLinks.minivoxes.IsCreated)
				{
					foreach (var KVP in minivoxLinks.minivoxes)
					{
						var minivoxPosition = KVP.Key;
						var minivoxEntity = KVP.Value;
						if (minivoxEntity.Index <= 0 || !HasComponent<Grass>(minivoxEntity) || totalGrassPositions.Contains(minivoxPosition))
						{
							continue;
						}
						PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, minivoxEntity);
						grassesRemoved.Add(minivoxPosition);
					}
				}
				if (newGrassPositions.Length > 0)
				{
					PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, minivoxesSpawnedPrefab),
						new OnMinivoxesSpawned(e, in newGrassPositions));
                	//UnityEngine.Debug.LogError("Spawned Grasses: " + newGrassPositions);
				}
				if (grassesRemoved.Length > 0)
				{
					PostUpdateCommands.SetComponent(entityInQueryIndex,
						PostUpdateCommands.Instantiate(entityInQueryIndex, minivoxesRemovedPrefab),
						new OnMinivoxesRemoved(e, in grassesRemoved));
                	//UnityEngine.Debug.LogError("Removed Grasses: " + grassesRemoved);
				}
				voxels.Dispose();
				grassTypes.Dispose();
				spawnLocalPositions.Dispose();
				totalGrassPositions.Dispose();
				newGrassPositions.Dispose();
				newGrassLocalPositions.Dispose();
				grassTypesAdded.Dispose();
				grassesRemoved.Dispose();
            })  .WithReadOnly(worldZoxIDs)
                .WithReadOnly(worldVoxelLinks)
                .WithReadOnly(worldRealmLinks)
				.WithReadOnly(voxScales)
                .WithReadOnly(voxels2)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}