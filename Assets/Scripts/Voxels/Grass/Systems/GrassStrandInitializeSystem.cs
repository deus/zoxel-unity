using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Voxels.Minivoxes;
using Zoxel.Voxels.Lighting;
using Zoxel.Voxels.Authoring;

namespace Zoxel.Voxels.Grasses
{
    //! Spawns grass strands from a grass with InitilizationEntity.
    /**
    *   Optimization difficulties originally spawning. Make sure to test again. Memory useage and speed.
    *   Why does it have to run before MinivoxInitializeSystem?
    */
	[BurstCompile, UpdateInGroup(typeof(GrassSystemGroup))]
    public partial class GrassStrandInitializeSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery grassQuery;

		protected override void OnCreate()
		{
            RequireForUpdate<GrassSettings>();
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            grassQuery = GetEntityQuery(
                ComponentType.ReadOnly<Grass>(),
                ComponentType.ReadOnly<MinivoxColor>(),
                ComponentType.ReadOnly<Rotation>(),
                ComponentType.ReadOnly<Seed>());
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var grassSettings = GetSingleton<GrassSettings>();
            // UnityEngine.Debug.LogError("Grass Strands Updated: " + processQuery.CalculateEntityCount() + " WIth Grass: " + grassQuery.CalculateEntityCount());
            var grassStrandColorVariance = grassSettings.grassStrandColorVariance;
            var grassResolution = grassSettings.grassResolution;
            var grassHeights = grassSettings.grassHeight * grassResolution;
            grassHeights.x = ((int) grassHeights.x) / (float) grassResolution;
            grassHeights.y = ((int) grassHeights.y) / (float) grassResolution;
            var elapsedTime = World.Time.ElapsedTime;
            var grassEntities = grassQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var minivoxColors = GetComponentLookup<MinivoxColor>(true);
            var rotations = GetComponentLookup<Rotation>(true);
            var seeds = GetComponentLookup<Seed>(true);
            var entityLightings = GetComponentLookup<EntityLighting>(true);
            var voxScales = GetComponentLookup<VoxScale>(true);
            grassEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithAll<InitializeEntity, GrassStrand>()
				.ForEach((int entityInQueryIndex, ref Seed seed, ref Scale scale, ref Rotation rotation, in ParentLink parentLink) =>
			{
                var parentSeed = (seeds[parentLink.parent].seed); //  + entityInQueryIndex * 100);
                seed.seed = parentSeed + entityInQueryIndex * 100;
                rotation.Value = rotations[parentLink.parent].Value;
                var voxScale = voxScales[parentLink.parent].scale;
                var random = new Random();
                random.InitState((uint) parentSeed);
                var grassHeight = random.NextFloat(grassHeights.x, grassHeights.y);
                random = new Random();
                random.InitState((uint) seed.seed);
                scale.Value = random.NextFloat(grassHeights.x, grassHeight) * voxScale.y;
            })  .WithReadOnly(rotations)
                .WithNativeDisableContainerSafetyRestriction(rotations)
                .WithReadOnly(seeds)
                .WithReadOnly(voxScales)
                .WithNativeDisableContainerSafetyRestriction(seeds)
                .ScheduleParallel(Dependency);
            Dependency = Entities
				.WithAll<InitializeEntity, GrassStrand>()
				.ForEach((int entityInQueryIndex, ref EntityLighting entityLighting, in ParentLink parentLink) =>
			{
                entityLighting.lightValue = entityLightings[parentLink.parent].lightValue;
            })  .WithReadOnly(entityLightings)
                .WithNativeDisableContainerSafetyRestriction(entityLightings)
                .ScheduleParallel(Dependency);
            Dependency = Entities
				.WithAll<InitializeEntity, GrassStrand>()
				.ForEach((int entityInQueryIndex, ref MaterialBaseColor materialBaseColor, ref BaseColor baseColor, in EntityLighting entityLighting, in ParentLink parentLink) =>
			{
				var brightness = LightingCurve.lightValues[entityLighting.lightValue];
                var minivoxColor = minivoxColors[parentLink.parent];
                var grassColor = minivoxColor.color.ToFloat3();
                var seed = (uint) (seeds[parentLink.parent].seed + entityInQueryIndex * 100);
                var random = new Random();
                random.InitState(seed);
                // Color
                materialBaseColor.Value.x = math.clamp(grassColor.x + random.NextFloat(-grassStrandColorVariance, grassStrandColorVariance), 0, 1);
                materialBaseColor.Value.y = math.clamp(grassColor.y + random.NextFloat(-grassStrandColorVariance, grassStrandColorVariance), 0, 1);
                materialBaseColor.Value.z = math.clamp(grassColor.z + random.NextFloat(-grassStrandColorVariance, grassStrandColorVariance), 0, 1);
                materialBaseColor.Value.w = 1f;
                baseColor.color = materialBaseColor.Value;
                // After setting as base color, multiply by brightness
                materialBaseColor.Value.x *= brightness;
                materialBaseColor.Value.y *= brightness;
                materialBaseColor.Value.z *= brightness;
            })  .WithReadOnly(minivoxColors)
                .WithReadOnly(seeds)
                .ScheduleParallel(Dependency);
            Dependency = Entities
				.WithAll<InitializeEntity, GrassStrand>()
				.ForEach((int entityInQueryIndex, ref OutlineColor outlineColor, ref BaseOutlineColor baseOutlineColor, in ParentLink parentLink, in EntityLighting entityLighting) =>
			{
				var brightness = LightingCurve.lightValues[entityLighting.lightValue];
                var minivoxColor = minivoxColors[parentLink.parent];
                var grassOutlineColor = minivoxColor.secondaryColor.ToFloat4();
                var seed = (uint) (seeds[parentLink.parent].seed + entityInQueryIndex * 100);
                var random = new Random();
                random.InitState(seed);
                // Color
                outlineColor.color.x = math.clamp(grassOutlineColor.x + random.NextFloat(-grassStrandColorVariance, grassStrandColorVariance), 0, 1);
                outlineColor.color.y = math.clamp(grassOutlineColor.y + random.NextFloat(-grassStrandColorVariance, grassStrandColorVariance), 0, 1);
                outlineColor.color.z = math.clamp(grassOutlineColor.z + random.NextFloat(-grassStrandColorVariance, grassStrandColorVariance), 0, 1);
                outlineColor.color.w = 0.6f;
                baseOutlineColor.outlineColor = outlineColor.color;
                outlineColor.color.x *= brightness;
                outlineColor.color.y *= brightness;
                outlineColor.color.z *= brightness;
            })  .WithReadOnly(minivoxColors)
                .WithReadOnly(seeds)
                .ScheduleParallel(Dependency);
		}
	}
}