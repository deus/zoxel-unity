using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Voxels.Grasses
{
    //! This handles changese in wind directions over time.
    public struct GrassWindTargetDirection : IComponentData
    {
        public double lastTimeChanged;
        public float changeRate;
        public float3 direction;

        public GrassWindTargetDirection(float3 direction)
        {
            this.direction = direction;
            this.lastTimeChanged = 0;
            this.changeRate = 0;
        }
    }
}