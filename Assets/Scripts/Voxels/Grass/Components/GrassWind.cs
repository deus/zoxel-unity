using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Voxels
{
    //! The direction of the wind for the grass.
    public struct GrassWind : IComponentData
    {
        public float3 direction;

        public GrassWind(float3 direction)
        {
            this.direction = direction;
        }
    }
}