using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Tags an entity that is grass.
	public struct Grass : IComponentData { }
}