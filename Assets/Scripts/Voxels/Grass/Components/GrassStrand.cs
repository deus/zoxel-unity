using Unity.Entities;

namespace Zoxel.Voxels
{
	//! A tag for the entity of a grass strand.
	public struct GrassStrand : IComponentData { }
	
}