using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Voxels.Grasses
{
    //! A wind direction value used inside the grass shaders.
    public struct GrassWindDirection : IComponentData
    {
        public float3 direction;

        public GrassWindDirection(float3 direction)
        {
            this.direction = direction;
        }
    }
}