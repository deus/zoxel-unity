using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Voxels.Grasses
{
    //! Passes grass strand wind direction data to the gpu.
    [UpdateAfter(typeof(GrassRenderBeginSystem))]
    [BurstCompile, UpdateInGroup(typeof(GrassSystemGroup))]
    public partial class GrassRenderWindDirectionSystem : SystemBase
    {
        private const string shaderArrayName = "wind_directions";
        public static int renderCount;
        private EntityQuery changeQuery;
        private EntityQuery changeQuery2;
        public static UnityEngine.ComputeBuffer buffer;

        protected override void OnDestroy()
        {
            ReleaseBuffers();
        }

        public static void ReleaseBuffers()
        {
            if (buffer != null)
            {
                buffer.Release();
            }
        }

        public static void SetBuffers(int renderCount)
        {
            ReleaseBuffers();
            buffer = new UnityEngine.ComputeBuffer(renderCount, 3 * 4, GrassRenderSystem.bufferType, GrassRenderSystem.bufferMode);
        }

        protected override void OnCreate()
        {
            changeQuery = GetEntityQuery(
                ComponentType.ReadOnly<GrassStrand>(),
                ComponentType.ReadOnly<GrassWindDirection>(),
                ComponentType.Exclude<DisableRender>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            changeQuery.SetChangedVersionFilter(typeof(GrassWindDirection));
            changeQuery2 = GetEntityQuery(
                ComponentType.ReadOnly<GrassStrand>(),
                ComponentType.ReadOnly<GrassWindDirection>(),
                ComponentType.ReadOnly<ParentLink>(),
                ComponentType.Exclude<DisableRender>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            #if DISABLE_COMPUTER_BUFFERS
            return;
            #endif
            var renderCount = GrassRenderBeginSystem.renderCount;
            if (GrassRenderBeginSystem.didChangeFrameBeforeLast)
            {
                SetBuffers(renderCount);
            }
            var didQueryNotChange = changeQuery.CalculateEntityCount() == 0; // changeQuery.IsEmpty;
            if (buffer != null && !GrassRenderBeginSystem.isUpdating &&
                (GrassRenderBeginSystem.didChangeFrameBeforeLast || !didQueryNotChange))
            {
                if (renderCount != changeQuery2.CalculateEntityCount())
                {
                    return;
                }
                // UnityEngine.Debug.LogError("Updating Outline Colors!");
                var grassMaterial = VoxelManager.instance.voxelMaterials.grassMaterial;
                var windDirections = buffer.BeginWrite<float3>(0, renderCount);
                Dependency = Entities
                    .WithNone<InitializeEntity, DestroyEntity, DisableRender>()
                    .WithAll<GrassStrand, ParentLink>()
                    .ForEach((int entityInQueryIndex, in GrassWindDirection windDirection) =>
                {
                    windDirections[entityInQueryIndex] = windDirection.direction;
                }).ScheduleParallel(Dependency);
                buffer.EndWrite<float3>(renderCount);
                grassMaterial.SetBuffer(shaderArrayName, buffer);
                // UnityEngine.Debug.LogError("GrassStrand Wind Updated: " + renderCount);
            }
        }
    }
}