using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Voxels.Grasses
{
    //! Updates outline color of grass to the GPU.
    [UpdateAfter(typeof(GrassRenderBeginSystem))]
    [BurstCompile, UpdateInGroup(typeof(GrassSystemGroup))]
    public partial class GrassRenderOutlineColorSystem : SystemBase
    {
        private const string shaderArrayName = "outline_colors";
        public static int renderCount;
        private EntityQuery changeQuery;
        private EntityQuery processQuery;
        public static UnityEngine.ComputeBuffer buffer;

        protected override void OnCreate()
        {
            changeQuery = GetEntityQuery(
                ComponentType.ReadOnly<GrassStrand>(),
                ComponentType.ReadOnly<OutlineColor>(),
                ComponentType.Exclude<DisableRender>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            changeQuery.SetChangedVersionFilter(typeof(OutlineColor));
            processQuery = GetEntityQuery(
                ComponentType.ReadOnly<GrassStrand>(),
                ComponentType.ReadOnly<OutlineColor>(),
                ComponentType.ReadOnly<ParentLink>(),
                ComponentType.Exclude<DisableRender>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
        }

        protected override void OnDestroy()
        {
            ReleaseBuffers();
        }

        public static void ReleaseBuffers()
        {
            if (buffer != null)
            {
                buffer.Release();
            }
        }

        public static void SetBuffers(int renderCount)
        {
            ReleaseBuffers();
            buffer = new UnityEngine.ComputeBuffer(renderCount, 4 * 4, GrassRenderSystem.bufferType, GrassRenderSystem.bufferMode);
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            #if DISABLE_COMPUTER_BUFFERS
            return;
            #endif
            var renderCount = GrassRenderBeginSystem.renderCount;
            if (GrassRenderBeginSystem.didChangeFrameBeforeLast)
            {
                SetBuffers(renderCount);
            }
            if (buffer == null)
            {
                //UnityEngine.Debug.LogError("GrassStrands buffer is null.");
                return;
            }
            if (GrassRenderBeginSystem.isUpdating)
            {
                //UnityEngine.Debug.LogError("GrassRenderBeginSystem isUpdating.");
                return;
            }
            var isRenderQueryEmpty = changeQuery.CalculateEntityCount() == 0; // changeQuery.IsEmpty;
            if (isRenderQueryEmpty && !GrassRenderBeginSystem.didChangeFrameBeforeLast)
            {
                //UnityEngine.Debug.LogError("GrassStrands RenderQuery is empty and size did not change last frame: " 
                //    + GrassRenderBeginSystem.didChangeFrameBeforeLast + " : " + changeQuery.CalculateEntityCount() + " : " + isRenderQueryEmpty);
                return;
            }
            if (renderCount != processQuery.CalculateEntityCount())
            {
                //UnityEngine.Debug.LogError("GrassStrands processQuery Count is not equals to GrassRenderBeginSystem.renderCount.");
                return;
            }
            var grassMaterial = VoxelManager.instance.voxelMaterials.grassMaterial;
            var colors = buffer.BeginWrite<float4>(0, renderCount);
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity, DisableRender>()
                .WithAll<GrassStrand, ParentLink>()
                .ForEach((int entityInQueryIndex, in OutlineColor outlineColor) =>
            {
                colors[entityInQueryIndex] = outlineColor.color;
            }).ScheduleParallel(Dependency);
            buffer.EndWrite<float4>(renderCount);
            grassMaterial.SetBuffer(shaderArrayName, buffer);
            // UnityEngine.Debug.LogError("GrassStrand Outline Colors Updated: " + renderCount);
            //UnityEngine.Debug.LogError("Updating GrassStrand Outline Colors!");
        }
    }
}