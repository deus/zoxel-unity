using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Voxels.Grasses
{
    //! Passes grass strand scale data to the gpu.
    [UpdateAfter(typeof(GrassRenderBeginSystem))]
    [BurstCompile, UpdateInGroup(typeof(GrassSystemGroup))]
    public partial class GrassRenderScaleSystem : SystemBase
    {
        private const string shaderArrayName = "scales";
        public static int renderCount;
        private EntityQuery changeQuery;
        private EntityQuery renderQuery;
        public static UnityEngine.ComputeBuffer buffer;

        protected override void OnDestroy()
        {
            ReleaseBuffers();
        }

        protected override void OnCreate()
        {
            changeQuery = GetEntityQuery(
                ComponentType.ReadOnly<GrassStrand>(),
                ComponentType.ReadOnly<Scale>(),
                ComponentType.Exclude<DisableRender>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            changeQuery.SetChangedVersionFilter(typeof(Scale));
            renderQuery = GetEntityQuery(
                ComponentType.ReadOnly<GrassStrand>(),
                ComponentType.ReadOnly<Scale>(),
                ComponentType.ReadOnly<ParentLink>(),
                ComponentType.Exclude<DisableRender>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            #if DISABLE_COMPUTER_BUFFERS
            return;
            #endif
            var renderCount = GrassRenderBeginSystem.renderCount;
            if (GrassRenderBeginSystem.didChangeFrameBeforeLast)
            {
                SetBuffers(renderCount);
            }
            var didQueryNotChange = changeQuery.CalculateEntityCount() == 0; // changeQuery.IsEmpty;
            if (buffer != null && !GrassRenderBeginSystem.isUpdating &&
                (GrassRenderBeginSystem.didChangeFrameBeforeLast || !didQueryNotChange))
            {
                if (renderCount != renderQuery.CalculateEntityCount())
                {
                    return;
                }
                var grassMaterial = VoxelManager.instance.voxelMaterials.grassMaterial;
                var scales = buffer.BeginWrite<float>(0, renderCount);
                Dependency = Entities
                    .WithNone<InitializeEntity, DestroyEntity, DisableRender>()
                    .WithAll<GrassStrand, ParentLink>()
                    .ForEach((int entityInQueryIndex, in Scale scale) =>
                {
                    scales[entityInQueryIndex] = scale.Value;
                }).ScheduleParallel(Dependency);
                buffer.EndWrite<float>(renderCount);
                grassMaterial.SetBuffer(shaderArrayName, buffer);
                // UnityEngine.Debug.LogError("GrassStrand Scales Updated: " + renderCount);
            }
        }

        public static void ReleaseBuffers()
        {
            if (buffer != null)
            {
                buffer.Release();
            }
        }

        public static void SetBuffers(int renderCount)
        {
            ReleaseBuffers();
            buffer = new UnityEngine.ComputeBuffer(renderCount, 1 * 4, GrassRenderSystem.bufferType, GrassRenderSystem.bufferMode);
        }
    }
}