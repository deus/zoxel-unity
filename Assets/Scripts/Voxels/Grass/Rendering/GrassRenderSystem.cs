using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Cameras;
using Zoxel.Rendering;
using Zoxel.Rendering.Authoring;
using Zoxel.Voxels.Authoring;

namespace Zoxel.Voxels.Grasses
{
    //! Renders GrassStrand's as cubes in a single draw call.
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public partial class GrassRenderSystem : SystemBase
    {
        private EntityQuery processQuery;
        public static bool hasBoundsUpdated;
        private static float3 newBoundsPosition;
        public static UnityEngine.Bounds bounds;
        public static UnityEngine.Mesh voxelCube;
        public const UnityEngine.ComputeBufferType bufferType = UnityEngine.ComputeBufferType.IndirectArguments;
        public const UnityEngine.ComputeBufferMode bufferMode = UnityEngine.ComputeBufferMode.SubUpdates;

        protected override void OnCreate()
        {
            bounds = new UnityEngine.Bounds(float3.zero, new float3(512, 512, 512));
            processQuery = GetEntityQuery(
                ComponentType.ReadOnly<GrassStrand>(),
                ComponentType.Exclude<DisableRender>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate<GrassSettings>();
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            UnityEngine.GameObject.Destroy(voxelCube);
        }

        public static void SetBoundsPosition(float3 newPosition)
        {
            if (bounds.center.x != newPosition.x || bounds.center.y != newPosition.y || bounds.center.z != newPosition.z)
            {
                newBoundsPosition = newPosition;
                hasBoundsUpdated = true;
            }
        }

        public static void SetNewBoundsPosition()
        {
            bounds.center = newBoundsPosition;
            hasBoundsUpdated = false;
        }

        protected override void OnUpdate()
        {
            var grassMaterial = VoxelManager.instance.voxelMaterials.grassMaterial;
            if (voxelCube == null)
            {
                var grassSettings = GetSingleton<GrassSettings>();
                //! Support for multiple grass types? With SharedComponents for grouping entites?
                var planetVoxelScale = VoxelManager.instance.voxelSettings.planetVoxelScale;
                var topThickness = grassSettings.grassTopThickness;
                var grassResolution = grassSettings.grassResolution; // 32f
                voxelCube = GenerateCubeGrass(1f, // planetVoxelScale.y,
                    new float3(1 / grassResolution, 1, 1 / grassResolution),
                    new float3((1 / grassResolution) * topThickness, 1, (1 / grassResolution) * topThickness));
                grassMaterial.SetFloat("_voxelSize", grassResolution);
            }
            var renderCount = GrassRenderBeginSystem.renderCount;
            if (renderCount == 0)
            {
                return;
            }
            var isShadows = false;
            var isRenderToAllCameras = true;
            var shadowMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            if (HasSingleton<RenderSettings>())
            {
                var renderSettings = GetSingleton<RenderSettings>();
                isShadows = renderSettings.isShadows;
                isRenderToAllCameras = renderSettings.isRenderToAllCameras;
                shadowMode = renderSettings.shadowMode;
            }
            UnityEngine.Camera renderCamera = null;
            if (!isRenderToAllCameras)
            {
                renderCamera = CameraReferences.GetMainCamera(EntityManager);
            }
            // UnityEngine.Debug.LogError("GrassStrand is Rendering: " + renderCount + " with bounds: " + bounds);
            UnityEngine.Graphics.DrawMeshInstancedProcedural(voxelCube, 0, grassMaterial, bounds, renderCount, null, shadowMode, isShadows, 0, renderCamera);
        }

        public static UnityEngine.Mesh GenerateCubeGrass(float scaleY, float3 lowerSize, float3 upperSize)
        {
            var mesh = new UnityEngine.Mesh();
            var verts = new UnityEngine.Vector3[MeshUtilities.cubeVerticesVector3.Length];
            for (int i = 0; i < verts.Length; i++)
            {
                var vert = MeshUtilities.cubeVerticesVector3[i];
                vert -= new UnityEngine.Vector3(0.5f, 0.5f, 0.5f);
                vert.y *= scaleY;
                if (vert.y < 0) // == -0.5f)
                {
                    vert = new UnityEngine.Vector3(vert.x * lowerSize.x, vert.y * lowerSize.y, vert.z * lowerSize.z);
                }
                else
                {
                    vert = new UnityEngine.Vector3(vert.x * upperSize.x, vert.y * upperSize.y, vert.z * upperSize.z);
                }
                verts[i] = vert;
            }
            mesh.vertices = verts;
            var triangles = new int[MeshUtilities.cubeTriangles2.Length - 6];
            for (int i = 0; i < triangles.Length; i++)
            {
                if (i < 6)
                {
                    triangles[i] = MeshUtilities.cubeTriangles2[i];
                }
                else if (i >= 6)
                {
                    triangles[i] = MeshUtilities.cubeTriangles2[i + 6];
                }
            }
            mesh.triangles = triangles;
            var normals = new NativeArray<float3>(verts.Length, Allocator.Temp);
            for (int i = 0; i < 6; i++)
            {
                normals[i * 4] = MeshUtilities.cubeNormals[i];
                normals[i * 4 + 1] = MeshUtilities.cubeNormals[i];
                normals[i * 4 + 2] = MeshUtilities.cubeNormals[i];
                normals[i * 4 + 3] = MeshUtilities.cubeNormals[i];
            }
            mesh.SetNormals(normals);
            normals.Dispose();
            return mesh;
        }
    }
}