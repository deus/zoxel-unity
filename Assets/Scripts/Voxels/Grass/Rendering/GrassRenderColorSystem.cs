using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Voxels.Grasses
{
    //! Updates base color of grass to the GPU.
    [UpdateAfter(typeof(GrassRenderBeginSystem))]
    [BurstCompile, UpdateInGroup(typeof(GrassSystemGroup))]
    public partial class GrassRenderColorSystem : SystemBase
    {
        public const UnityEngine.ComputeBufferType bufferType = UnityEngine.ComputeBufferType.IndirectArguments;
        public const UnityEngine.ComputeBufferMode bufferMode = UnityEngine.ComputeBufferMode.SubUpdates;
        private const string shaderArrayName = "colors";
        public static int renderCount;
        public static UnityEngine.ComputeBuffer buffer;
        private EntityQuery changeQuery;
        private EntityQuery changeQuery2;

        protected override void OnDestroy()
        {
            ReleaseBuffers();
        }

        public static void ReleaseBuffers()
        {
            if (buffer != null)
            {
                buffer.Release();
            }
        }

        public static void SetBuffers(int renderCount)
        {
            ReleaseBuffers();
            buffer = new UnityEngine.ComputeBuffer(renderCount, 4 * 4, bufferType, bufferMode);
        }

        protected override void OnCreate()
        {
            changeQuery = GetEntityQuery(
                ComponentType.ReadOnly<GrassStrand>(),
                ComponentType.ReadOnly<MaterialBaseColor>(),
                ComponentType.Exclude<DisableRender>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            changeQuery.SetChangedVersionFilter(typeof(MaterialBaseColor));
            changeQuery2 = GetEntityQuery(
                ComponentType.ReadOnly<GrassStrand>(),
                ComponentType.ReadOnly<MaterialBaseColor>(),
                ComponentType.ReadOnly<ParentLink>(),
                ComponentType.Exclude<DisableRender>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            #if DISABLE_COMPUTER_BUFFERS
            return;
            #endif
            var renderCount = GrassRenderBeginSystem.renderCount;
            // Update buffers if changed
            if (GrassRenderBeginSystem.didChangeFrameBeforeLast)
            {
                SetBuffers(renderCount);
            }
            var didQueryNotChange = changeQuery.CalculateEntityCount() == 0; // changeQuery.IsEmpty;
            if (buffer != null && !GrassRenderBeginSystem.isUpdating &&
                (GrassRenderBeginSystem.didChangeFrameBeforeLast || !didQueryNotChange))
            {
                if (renderCount != changeQuery2.CalculateEntityCount())
                {
                    return;
                }
                var grassMaterial = VoxelManager.instance.voxelMaterials.grassMaterial;
                var colors =  buffer.BeginWrite<float3>(0, renderCount);
                Dependency = Entities
                    .WithNone<InitializeEntity, DestroyEntity, DisableRender>()
                    .WithAll<GrassStrand, ParentLink>()
                    .ForEach((int entityInQueryIndex, in MaterialBaseColor materialBaseColor) =>
                {
                    // materialBaseColor.Value
                    colors[entityInQueryIndex] = new float3(materialBaseColor.Value.x, materialBaseColor.Value.y, materialBaseColor.Value.z); // * 0.5f;
                }).ScheduleParallel(Dependency);
                buffer.EndWrite<float3>(renderCount);
                grassMaterial.SetBuffer(shaderArrayName, buffer);
                // UnityEngine.Debug.LogError("GrassStrand Colors Updated: " + renderCount);
            }
        }
    }
}