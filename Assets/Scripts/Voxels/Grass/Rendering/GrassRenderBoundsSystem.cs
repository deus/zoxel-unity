using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.Voxels.Grasses
{
    //! Updates render systems origin positions when ChunkStreamPoint position changes.
    /**
    *   - Render System -
    */
    [UpdateBefore(typeof(RenderBoundsPostUpdateSystem))]
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class GrassRenderBoundsSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((in OnChunkStreamPointUpdated onChunkStreamPointUpdated) =>
            {
                GrassRenderSystem.SetBoundsPosition(onChunkStreamPointUpdated.worldPosition);
            }).WithoutBurst().Run();
        }
    }
}