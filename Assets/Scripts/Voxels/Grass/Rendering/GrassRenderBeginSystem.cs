using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Rendering;
// [UpdateAfter(typeof(RenderBoundsPostUpdateSystem))]

namespace Zoxel.Voxels.Grasses
{
    //! Sets the grass render size at the start of the system group.
    /**
    *   Note: Filter queries have to be seperate to non filtered ones, for the logic of the strand update systems to work
    */
    [BurstCompile, UpdateInGroup(typeof(GrassSystemGroup))]
    public partial class GrassRenderBeginSystem : SystemBase
    {
        public static int renderCount;
        public int renderCount2;
        public bool didChangeLastFrame;
        public static bool didChangeFrameBeforeLast;
        public static bool isUpdating;
        private EntityQuery renderQuery;

        protected override void OnCreate()
        {
            renderQuery = GetEntityQuery(
                ComponentType.ReadOnly<GrassStrand>(),
                ComponentType.ReadOnly<ParentLink>(),
                ComponentType.Exclude<DisableRender>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var oldDidUpdate = didChangeLastFrame;
            var newRenderCount = renderQuery.CalculateEntityCount();
            if (renderCount2 != newRenderCount)
            {
                renderCount2 = newRenderCount;
                didChangeLastFrame = true;
                //UnityEngine.Debug.LogError("GrassStrand Buffers Updated: " + renderCount);
            }
            else
            {
                didChangeLastFrame = false;
            }
            didChangeFrameBeforeLast = oldDidUpdate && !didChangeLastFrame;
            if (didChangeFrameBeforeLast)
            {
                renderCount = renderCount2;
                // GrassRenderSystem.SetBuffers(renderCount);
                // UnityEngine.Debug.LogError("Set GrassStrand Buffers to: " + renderCount);
            }
            isUpdating = oldDidUpdate && didChangeLastFrame;
        }
    }
}