using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Voxels.Grasses
{
    //! Passes grass strand position data to the gpu.
    [UpdateAfter(typeof(GrassRenderBeginSystem))]
    [BurstCompile, UpdateInGroup(typeof(GrassSystemGroup))]
    public partial class GrassPositionSystem : SystemBase
    {
        private const string shaderArrayName = "positions";
        public static int renderCount;
        private EntityQuery changeQuery;
        private EntityQuery changeQuery2;
        public static UnityEngine.ComputeBuffer buffer;

        protected override void OnDestroy()
        {
            ReleaseBuffers();
        }

        public static void ReleaseBuffers()
        {
            if (buffer != null)
            {
                buffer.Release();
            }
        }

        public static void SetBuffers(int renderCount)
        {
            ReleaseBuffers();
            buffer = new UnityEngine.ComputeBuffer(renderCount, 3 * 4, GrassRenderSystem.bufferType, GrassRenderSystem.bufferMode);
        }

        protected override void OnCreate()
        {
            changeQuery = GetEntityQuery(
                ComponentType.ReadOnly<GrassStrand>(),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.Exclude<DisableRender>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            changeQuery.SetChangedVersionFilter(typeof(Translation));
            changeQuery2 = GetEntityQuery(
                ComponentType.ReadOnly<GrassStrand>(),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<ParentLink>(),
                ComponentType.Exclude<DisableRender>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            #if DISABLE_COMPUTER_BUFFERS
            return;
            #endif
            var renderCount = GrassRenderBeginSystem.renderCount;
            if (GrassRenderBeginSystem.didChangeFrameBeforeLast)
            {
                SetBuffers(renderCount);
            }
            if (buffer == null)
            {
                return;
            }
            var didQueryNotChange = changeQuery.CalculateEntityCount() == 0; // changeQuery.IsEmpty;
            if (GrassRenderSystem.hasBoundsUpdated || (!GrassRenderBeginSystem.isUpdating &&
                (GrassRenderBeginSystem.didChangeFrameBeforeLast || !didQueryNotChange)))
            {
                if (renderCount != changeQuery2.CalculateEntityCount())
                {
                    //UnityEngine.Debug.LogError("Count not the same as query: " + changeQuery2.CalculateEntityCount() + ":" + renderCount);
                    return;
                }
                var grassMaterial = VoxelManager.instance.voxelMaterials.grassMaterial;
                if (GrassRenderSystem.hasBoundsUpdated)
                {
                    GrassRenderSystem.SetNewBoundsPosition();
                }
                var boundsPosition = new float3(GrassRenderSystem.bounds.center.x, GrassRenderSystem.bounds.center.y, GrassRenderSystem.bounds.center.z);
                var positions = buffer.BeginWrite<float3>(0, renderCount);
                Dependency = Entities
                    .WithNone<InitializeEntity, DestroyEntity, DisableRender>()
                    .WithAll<GrassStrand, ParentLink>()
                    .ForEach((int entityInQueryIndex, in Translation translation) =>
                {
                    positions[entityInQueryIndex] = translation.Value - boundsPosition;
                }).ScheduleParallel(Dependency);
                buffer.EndWrite<float3>(renderCount);
                grassMaterial.SetBuffer(shaderArrayName, buffer);
                // UnityEngine.Debug.LogError("GrassStrand Positions Updated: " + renderCount + " at position: " + boundsPosition);
            }
        }
    }
}