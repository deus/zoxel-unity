using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Voxels.Grasses
{
    //! Uploads grass rotations to the gpu!
    [UpdateAfter(typeof(GrassRenderBeginSystem))]
    [BurstCompile, UpdateInGroup(typeof(GrassSystemGroup))]
    public partial class GrassRotationSystem : SystemBase
    {
        private const string shaderArrayName = "rotations";
        public static int renderCount;
        private EntityQuery changeQuery;
        private EntityQuery changeQuery2;
        public static UnityEngine.ComputeBuffer buffer;

        protected override void OnDestroy()
        {
            ReleaseBuffers();
        }

        public static void ReleaseBuffers()
        {
            if (buffer != null)
            {
                buffer.Release();
            }
        }

        public static void SetBuffers(int renderCount)
        {
            ReleaseBuffers();
            buffer = new UnityEngine.ComputeBuffer(renderCount, 4 * 4, GrassRenderSystem.bufferType, GrassRenderSystem.bufferMode);
        }

        protected override void OnCreate()
        {
            changeQuery = GetEntityQuery(
                ComponentType.ReadOnly<GrassStrand>(),
                ComponentType.ReadOnly<Rotation>(),
                ComponentType.Exclude<DisableRender>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            changeQuery.SetChangedVersionFilter(typeof(Rotation));
            changeQuery2 = GetEntityQuery(
                ComponentType.ReadOnly<GrassStrand>(),
                ComponentType.ReadOnly<Rotation>(),
                ComponentType.ReadOnly<ParentLink>(),
                ComponentType.Exclude<DisableRender>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            #if DISABLE_COMPUTER_BUFFERS
            return;
            #endif
            var renderCount = GrassRenderBeginSystem.renderCount;
            if (GrassRenderBeginSystem.didChangeFrameBeforeLast)
            {
                SetBuffers(renderCount);
            }
            var didQueryNotChange = changeQuery.CalculateEntityCount() == 0; // changeQuery.IsEmpty;
            if (buffer != null && !GrassRenderBeginSystem.isUpdating &&
                (GrassRenderBeginSystem.didChangeFrameBeforeLast || !didQueryNotChange))
            {
                if (renderCount != changeQuery2.CalculateEntityCount())
                {
                    //UnityEngine.Debug.LogError("Count not the same as query: " + changeQuery2.CalculateEntityCount() + ":" + renderCount);
                    return;
                }
                var grassMaterial = VoxelManager.instance.voxelMaterials.grassMaterial;
                var rotations = buffer.BeginWrite<quaternion>(0, renderCount);
                Dependency = Entities
                    .WithNone<InitializeEntity, DestroyEntity, DisableRender>()
                    .WithAll<GrassStrand, ParentLink>()
                    .ForEach((int entityInQueryIndex, in Rotation rotation) =>
                {
                    rotations[entityInQueryIndex] = rotation.Value;
                }).ScheduleParallel(Dependency);
                buffer.EndWrite<quaternion>(renderCount);
                grassMaterial.SetBuffer(shaderArrayName, buffer);
                // UnityEngine.Debug.LogError("GrassStrand Rotations Updated: " + renderCount);
            }
        }
    }
}