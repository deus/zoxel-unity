using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Voxels.Authoring;

namespace Zoxel.Voxels.Grasses
{
    //! Wind direction per each strand
    /**
    *   \todo Move wind per grass - based on wind flows
    */
    [BurstCompile, UpdateInGroup(typeof(GrassSystemGroup))]
    public partial class GrassWindSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery grassQuery;

        protected override void OnCreate()
        {
            grassQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Grass>(),
                ComponentType.ReadOnly<GrassWind>());
            RequireForUpdate<GrassSettings>();
            RequireForUpdate(processQuery);
            RequireForUpdate(grassQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var grassSettings = GetSingleton<GrassSettings>();
            if (grassSettings.disableGrassWind)
            {
                return;
            }
            var grassWindTick = grassSettings.grassWindTick;
            if (grassWindTick.y < grassWindTick.x)
            {
                grassWindTick.y = grassWindTick.x;
            }
            var grassWindSpeed = grassSettings.grassWindSpeed;
            var windVariance = grassSettings.grassWindVariance;
            var elapsedTime = World.Time.ElapsedTime;
            var deltaTime = World.Time.DeltaTime * grassWindSpeed;
            var grassEntities = grassQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var grassWinds = GetComponentLookup<GrassWind>(true);
            grassEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity, DisableRender>()
				.WithAll<GrassStrand>()
				.ForEach((int entityInQueryIndex, ref GrassWindDirection windDirection, ref GrassWindTargetDirection grassWindTargetDirection, in ParentLink parentLink, in Seed seed) =>
			{
                if (elapsedTime - grassWindTargetDirection.lastTimeChanged >= grassWindTargetDirection.changeRate)
                {
                    if (!grassWinds.HasComponent(parentLink.parent))
                    {
                        return;
                    }
                    var grassWind = grassWinds[parentLink.parent].direction;
                    var random = new Random();
                    random.InitState((uint) (seed.seed + elapsedTime * 10));
                    grassWindTargetDirection.lastTimeChanged = elapsedTime;
                    grassWindTargetDirection.changeRate = random.NextFloat(grassWindTick.x, grassWindTick.y);
                    // set new target direction
                    grassWindTargetDirection.direction = grassWind + new float3(
                        random.NextFloat(-windVariance, windVariance),
                        random.NextFloat(-windVariance, windVariance),
                        random.NextFloat(-windVariance, windVariance));
                    grassWindTargetDirection.direction.x = math.clamp(grassWindTargetDirection.direction.x, -1, 1);
                    grassWindTargetDirection.direction.y = math.clamp(grassWindTargetDirection.direction.y, -1, 1);
                    grassWindTargetDirection.direction.z = math.clamp(grassWindTargetDirection.direction.z, -1, 1);
                }
                //! \todo Use sine movement curve to lerp - slerp
                windDirection.direction = math.lerp(windDirection.direction, grassWindTargetDirection.direction, deltaTime);
            })  .WithReadOnly(grassWinds)
                .ScheduleParallel(Dependency);
        }
    }
}
            /*var seedID = (uint) IDUtil.GenerateUniqueID();
            if (seedID == 0)
            {
                seedID = 1;
            }*/
                    /*var seed = (uint) (seedID + entityInQueryIndex * 99);
                    if (seed == 0)
                    {
                        seed = 1;
                    }*/

                /*windDirection.direction.x = math.clamp(windDirection.direction.x + random.NextFloat(-windVariance, windVariance), -1, 1);
                windDirection.direction.y = math.clamp(windDirection.direction.y + random.NextFloat(-windVariance, windVariance), -1, 1);
                windDirection.direction.z = math.clamp(windDirection.direction.z + random.NextFloat(-windVariance, windVariance), -1, 1);
                windDirection.direction.x = math.clamp(windDirection.direction.x, grassWind.x - windVariance * 2f, grassWind.x + windVariance * 2f);
                windDirection.direction.y = math.clamp(windDirection.direction.y, grassWind.y - windVariance * 2f, grassWind.y + windVariance * 2f);
                windDirection.direction.z = math.clamp(windDirection.direction.z, grassWind.z - windVariance * 2f, grassWind.z + windVariance * 2f);*/

                /*windDirection.direction.x += random.NextFloat(-windVariance, windVariance);
                windDirection.direction.y += random.NextFloat(-windVariance, windVariance);
                windDirection.direction.z += random.NextFloat(-windVariance, windVariance);
                windDirection.direction.y = math.clamp(windDirection.direction.x, -1, 1);
                windDirection.direction.y = math.clamp(windDirection.direction.y, -1, 1);
                windDirection.direction.z = math.clamp(windDirection.direction.z, -1, 1);*/