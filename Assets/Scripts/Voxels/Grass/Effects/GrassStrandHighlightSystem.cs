using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Voxels.Authoring;

namespace Zoxel.Voxels.Grasses
{
    //! Alters grass outline color
    /**
    *   \todo Generic Selection events - used for Minivoxes and characters too.
    *   \todo Events for grass selection and deselection - so I can only change colours upon these things.
    */
    [BurstCompile, UpdateInGroup(typeof(GrassSystemGroup))]
    public partial class GrassStrandHighlightSystem : SystemBase
    {
        protected override void OnCreate()
        {
            RequireForUpdate<GrassSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var grassSettings = GetSingleton<GrassSettings>();
            if (grassSettings.disableGrassHighlighting)
            {
                return;
            }
            var grassSelectedColor = grassSettings.grassSelectedColor.ToFloat4();
            var grassSelectionSpeed = grassSettings.grassSelectionSpeed;
            var selectionSpeed = World.Time.DeltaTime * (1f / grassSelectionSpeed);
			Entities
				.WithAll<GrassStrand>()
				.ForEach((int entityInQueryIndex, ref OutlineColor outlineColor, in BaseOutlineColor baseOutlineColor, in EntityLighting entityLighting, in ParentLink parentLink) =>
			{
                if (!HasComponent<EntitySelected>(parentLink.parent))
                {
                    if (HasComponent<OnEntityDeselected>(parentLink.parent))
                    {
                        var lightingMultiply = LightingCurve.lightValues[entityLighting.lightValue];
                        var outlineColor2 = new float4(
                            baseOutlineColor.outlineColor.x * lightingMultiply,
                            baseOutlineColor.outlineColor.y * lightingMultiply,
                            baseOutlineColor.outlineColor.z * lightingMultiply,
                            outlineColor.color.w);
                        outlineColor.color = math.lerp(outlineColor.color, outlineColor2, selectionSpeed);
                    }
                }
                else
                {
                    if (HasComponent<OnEntitySelected>(parentLink.parent))
                    {
                        var lightingMultiply = LightingCurve.lightValues[entityLighting.lightValue];
                        var selectedColor2 = grassSelectedColor;
                        selectedColor2.x = (selectedColor2.x * 8f + baseOutlineColor.outlineColor.x * 2f) / 10f;
                        selectedColor2.y = (selectedColor2.y * 8f + baseOutlineColor.outlineColor.y * 2f) / 10f;
                        selectedColor2.z = (selectedColor2.z * 8f + baseOutlineColor.outlineColor.z * 2f) / 10f;
                        selectedColor2.x *= lightingMultiply;
                        selectedColor2.y *= lightingMultiply;
                        selectedColor2.z *= lightingMultiply;
                        outlineColor.color = math.lerp(outlineColor.color, selectedColor2, selectionSpeed);
                    }
                }
			}).ScheduleParallel();
        }
    }
}
/*var random = new Random();
var seed = (uint) (seedID + entityInQueryIndex * 66);
if (seed == 0)
{
    seed = 1;
}
random.InitState(seed);*/
            // const float colorVariance = 0.01f;
            // const float colorVarianceMax = 0.12f;