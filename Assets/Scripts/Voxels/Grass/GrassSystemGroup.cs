using Unity.Entities;
using Zoxel.Rendering;
using Zoxel.Voxels;

namespace Zoxel.Voxels.Grasses
{
    //! Minivox Systmems go under here.
    [AlwaysUpdateSystem]
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class GrassSystemGroup : ComponentSystemGroup
    {
		private EntityQuery grassQuery;
		private EntityQuery grassStrandQuery;

        protected override void OnCreate()
        {
            base.OnCreate();
			grassQuery = GetEntityQuery(ComponentType.ReadOnly<Grass>());
			grassStrandQuery = GetEntityQuery(ComponentType.ReadOnly<GrassStrand>());
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();
            VoxelSystemGroup.grassCount = grassQuery.CalculateEntityCount();
            VoxelSystemGroup.grassStrandCount = grassStrandQuery.CalculateEntityCount();
        }
    }
}