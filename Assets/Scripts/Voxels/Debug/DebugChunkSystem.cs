﻿using Unity.Entities;

using Unity.Mathematics;
using Zoxel.Lines;
// Todo: Draw Grid lines
// Todo: Make work in game as well, efficient line drawing system?

namespace Zoxel.Voxels
{
	[UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class DebugChunkSystem : SystemBase
    {
        float timing = 0.01f;
        UnityEngine.Color lineColor = UnityEngine.Color.green;
        UnityEngine.Color lineColorEdge = new UnityEngine.Color(0.66f, 0.1f, 0.1f);
        UnityEngine.Color lineColorCorner = new UnityEngine.Color(0f, 0f, 0f);
        UnityEngine.Color lineColorMiddle = new UnityEngine.Color(0.8f, 0.8f, 0.8f);
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (VoxelManager.instance == null) return;
            if (!VoxelManager.instance.voxelSettings.isDrawChunkColliders)
            {
                return;
            }
            var materialType = (byte) 0;
            var lineWidth = 0.025f;
            var lineLife = 0.3f;
            // var lineLength = 16;
            var elapsedTime = World.Time.ElapsedTime;
            var linePrefab = RenderLineGroup.linePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<PlanetChunk, Chunk, ChunkPosition>()
                .ForEach((Entity e, in Chunk chunk, in ChunkPosition chunkPosition) =>
            {
                var heightAddition = new float3(0, chunk.voxelDimensions.y, 0);
                var position = chunkPosition.GetVoxelPosition(chunk.voxelDimensions).ToFloat3() + new float3(8, 0, 8);
                RenderLineGroup.SpawnLine(PostUpdateCommands, linePrefab, elapsedTime, position, position + new float3(0, 8, 0), materialType, lineWidth, lineLife);
            }).Run();
        }

        private void DrawCubeLines(float3 position, float3 size)
        {
            DrawQuadLines(position + new float3(0, -size.y, 0), size);
            DrawQuadLines(position + new float3(0, size.y, 0), size);
            // draw 4 lines instead of these
            DrawQuadLines3(position + new float3(-size.x, 0, 0), size);
            DrawQuadLines3(position + new float3(size.x, 0, 0), size);
            DrawQuadLines2(position + new float3(0, 0, -size.z), size);
            DrawQuadLines2(position + new float3(0, 0, size.z), size);
        }

        private void DrawQuadLines(float3 position, float3 size)
        {
            DrawQuadLinesCore(position, size,
                new float3(-size.x, 0, size.z),
                new float3(size.x, 0, size.z),
                new float3(size.x, 0, -size.z),
                new float3(-size.x, 0, -size.z));
        }

        private void DrawQuadLines2(float3 position, float3 size)
        {
            DrawQuadLinesCore(position, size,
                new float3(-size.x, size.y, 0),
                new float3(size.x, size.y, 0),
                new float3(size.x, -size.y, 0),
                new float3(-size.x, -size.y, 0));
        }

        private void DrawQuadLines3(float3 position, float3 size)
        {
            DrawQuadLinesCore(position, size,
                new float3(0, -size.y, size.z),
                new float3(0, size.y, size.z),
                new float3(0, size.y, -size.z),
                new float3(0, -size.y, -size.z));
        }

        private void DrawQuadLinesCore(float3 position, float3 size, float3 cornerA, float3 cornerB,
            float3 cornerC, float3 cornerD)
        {
            var pointA = position + cornerA;
            var pointB = position + cornerB;
            var pointC = position + cornerC;
            var pointD = position + cornerD;

            // rotate the points

            UnityEngine.Debug.DrawLine(pointA, pointB, lineColor, timing);
            UnityEngine.Debug.DrawLine(pointB, pointC, lineColor, timing);
            UnityEngine.Debug.DrawLine(pointC, pointD, lineColor, timing);
            UnityEngine.Debug.DrawLine(pointD, pointA, lineColor, timing);
        }
    }
}

/*RenderLineGroup.CreateCubeLines(PostUpdateCommands, elapsedTime, position,
    quaternion.identity,
    );*/

//var positionF = chunkPosition.GetVoxelPosition(chunk.voxelDimensions).ToFloat3() + new float3(chunk.voxelDimensions.x, 0, chunk.voxelDimensions.z) / 2f;
//Debug.DrawLine(positionF, positionF + heightAddition, lineColor2, timing);
// var position = chunk.voxelDimensions.ToFloat3();

/*var positionA = chunkPosition.GetVoxelPosition(chunk.voxelDimensions).ToFloat3();
var positionB = chunkPosition.GetVoxelPosition(chunk.voxelDimensions).ToFloat3() + new float3(chunk.voxelDimensions.x, 0, 0);
var positionC = chunkPosition.GetVoxelPosition(chunk.voxelDimensions).ToFloat3() + new float3(0, 0, chunk.voxelDimensions.z); 
var positionD = chunkPosition.GetVoxelPosition(chunk.voxelDimensions).ToFloat3() + new float3(chunk.voxelDimensions.x, 0, chunk.voxelDimensions.z);

UnityEngine.Debug.DrawLine(positionA, positionA + heightAddition, lineColorEdge, timing);
UnityEngine.Debug.DrawLine(positionB, positionB + heightAddition, lineColorEdge, timing);
UnityEngine.Debug.DrawLine(positionC, positionC + heightAddition, lineColorEdge, timing);
UnityEngine.Debug.DrawLine(positionD, positionD + heightAddition, lineColorEdge, timing);*/

// var boneSize =  1.2f * (new float3(boneDebugSize, boneDebugSize, boneDebugSize));

/*bool existsBack = Planet.EntityManager.Exists(chunkNeighbors.chunkBack); //chunk.indexBack != 0;
bool existsFront = Planet.EntityManager.Exists(chunkNeighbors.chunkForward); //chunk.indexForward != 0;
bool existsLeft = Planet.EntityManager.Exists(chunkNeighbors.chunkLeft); //chunk.indexLeft != 0;
bool existsRight = Planet.EntityManager.Exists(chunkNeighbors.chunkRight); //chunk.indexRight != 0;*/

// Bottom Left
/*if (existsFront && existsRight && !existsBack && !existsLeft) 
{
    Debug.DrawLine(positionA, positionA + heightAddition, lineColorCorner, timing);
    Debug.DrawLine(positionB, positionB + heightAddition, lineColorEdge, timing);
    Debug.DrawLine(positionC, positionC + heightAddition, lineColorEdge, timing);
    Debug.DrawLine(positionD, positionD + heightAddition, lineColorMiddle, timing);
}
// Bottom Right
else if (existsFront && !existsRight && existsLeft && !existsBack) 
{
    Debug.DrawLine(positionB, positionB + heightAddition, lineColorCorner, timing);
    Debug.DrawLine(positionD, positionD + heightAddition, lineColorEdge, timing);
}
// Bottom Side
else if (existsFront && existsRight && existsLeft && !existsBack) 
{
    Debug.DrawLine(positionB, positionB + heightAddition, lineColorEdge, timing);
    Debug.DrawLine(positionD, positionD + heightAddition, lineColorMiddle, timing);
}
// Left Side
else if (existsFront && existsRight && !existsLeft && existsBack) 
{
    Debug.DrawLine(positionC, positionC + heightAddition, lineColorEdge, timing);
    Debug.DrawLine(positionD, positionD + heightAddition, lineColorMiddle, timing);
}
// Right Side
else if (existsFront && !existsRight && existsLeft && existsBack) 
{
    Debug.DrawLine(positionD, positionD + heightAddition, lineColorEdge, timing);
}
// Top Side
else if (!existsFront && existsRight && existsLeft && existsBack) 
{
    Debug.DrawLine(positionD, positionD + heightAddition, lineColorEdge, timing);
}
// Top Left Side
else if (!existsFront && existsRight && !existsLeft && existsBack) 
{
    Debug.DrawLine(positionC, positionC + heightAddition, lineColorCorner, timing);
    Debug.DrawLine(positionD, positionD + heightAddition, lineColorEdge, timing);  // right point
}
// Top Right
else if (!existsFront && !existsRight && existsLeft && existsBack) 
{
    Debug.DrawLine(positionD, positionD + heightAddition, lineColorCorner, timing);
}
// all others just to 1 point
else
{
    Debug.DrawLine(positionD, positionD + heightAddition, lineColorMiddle, timing);
}*/