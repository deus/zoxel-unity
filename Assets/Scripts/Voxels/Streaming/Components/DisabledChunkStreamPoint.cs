using Unity.Entities;

namespace Zoxel.Voxels
{
    public struct DisabledChunkStreamPoint : IComponentData { }
}