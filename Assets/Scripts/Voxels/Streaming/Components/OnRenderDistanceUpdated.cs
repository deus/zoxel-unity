using Unity.Entities;

namespace Zoxel.Voxels
{
    public struct OnRenderDistanceUpdated : IComponentData
    {
        public byte renderDistance;

        public OnRenderDistanceUpdated(byte renderDistance)
        {
            this.renderDistance = renderDistance;
        }
    }
}