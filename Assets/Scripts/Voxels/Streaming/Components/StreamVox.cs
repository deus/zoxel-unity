using Unity.Entities;

namespace Zoxel.Voxels
{
    //! Generic event for streaming Vox to a new center position.
    /**
    *   Used by Chunks, MegaChunks and PlanetChunks to load/unload entities.
    */
    public struct StreamVox : IComponentData { }
}