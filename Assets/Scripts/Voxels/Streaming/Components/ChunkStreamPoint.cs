﻿using Unity.Entities;

namespace Zoxel.Voxels
{
    public struct ChunkStreamPoint : IComponentData
    {
        public byte init;
        public double lastUpdated;
        public int3 chunkPosition;      // when position changes, update the position
        public byte renderDistance;

        public ChunkStreamPoint(byte renderDistance)
        {
            this.renderDistance = renderDistance;
            this.init = 0;
            this.lastUpdated = 0;
            this.chunkPosition = new int3();
        }
    }
}