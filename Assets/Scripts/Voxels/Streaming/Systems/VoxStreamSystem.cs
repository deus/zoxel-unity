using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Voxels
{
    //! Adds the initial event for streaming a vox.
    /**
    *   Uses one StreamVox event to cover Chunk's, MegaChunk's and PlanetChunks's
    *   \todo Add support for many stream points, instead of one to one, so I can stream from portals and other player locations.
    *   \todo Use DynamicChunkLink data instead of finding chunk position again.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxStreamSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<StreamVox>(),
                ComponentType.Exclude<OnChunksSpawned>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<StreamableVox>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (VoxelManager.instance.voxelSettings.disableWorldStreaming)
            {
                return;
            }
            var timePerStream = VoxelManager.instance.voxelSettings.timePerStream;
            var time = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxes = GetComponentLookup<Vox>(true);
            var chunkDimensions = GetComponentLookup<ChunkDimensions>(true);
            var voxScales = GetComponentLookup<VoxScale>(true);
            voxEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithChangeFilter<Translation>()
                .WithNone<DisabledChunkStreamPoint, SetTransformPosition>()
                .ForEach((Entity e, int entityInQueryIndex, ref ChunkStreamPoint chunkStreamPoint, in GravityQuadrant gravityQuadrant, in Translation position, in VoxLink voxLink) =>
            {
                if (chunkStreamPoint.init != 0 && (time - chunkStreamPoint.lastUpdated < timePerStream))
                {
                    return;
                }
                var voxEntity = voxLink.vox;
                if (!chunkDimensions.HasComponent(voxEntity)) 
                {
                    return;
                }
                //! Check if chunks still loading, before restreaming
                var voxelDimensions = chunkDimensions[voxEntity].voxelDimensions;
                // This breaks if not here
                var vox = voxes[voxEntity];
                for (int i = 0; i < vox.chunks.Length; i++)
                {
                    var chunkEntity = vox.chunks[i];
                    if (chunkEntity.Index <= 0 || HasComponent<EntityBusy>(chunkEntity) || HasComponent<InitializeEntity>(chunkEntity) || HasComponent<UpdateChunkNeighbors>(chunkEntity))
                    {
                        return;
                    }
                }
                chunkStreamPoint.lastUpdated = time;
                var voxScale = voxScales[voxEntity].scale;
                // should do some transform stuff for the position - if distorted vox mesh
                var positionScaled = position.Value;
                positionScaled.x /= voxScale.x;
                positionScaled.y /= voxScale.y;
                positionScaled.z /= voxScale.z;
                var chunkPosition = VoxelUtilities.GetChunkPosition(new int3(positionScaled), voxelDimensions);
                if (chunkStreamPoint.init == 0 || chunkPosition != chunkStreamPoint.chunkPosition)
                {
                    if (chunkStreamPoint.init == 0)
                    {
                        chunkStreamPoint.init = 1;
                    }
                    // old position if needed
                    var oldPosition = chunkPosition;
                    var renderDistance = chunkStreamPoint.renderDistance;
                    chunkStreamPoint.chunkPosition = chunkPosition;
                    // a special check if quadrant is 0 - for no quadrant
                    var quadrant = gravityQuadrant.quadrant;
                    if (quadrant == 0)
                    {
                        quadrant = 1;
                    }
                    // add new position
                    PostUpdateCommands.SetComponent(entityInQueryIndex, voxEntity, new StreamableVox(chunkPosition, quadrant, renderDistance));
                    PostUpdateCommands.AddComponent<StreamVox>(entityInQueryIndex, voxEntity);
                    var worldPosition = VoxelUtilities.GetChunkVoxelPosition(chunkStreamPoint.chunkPosition, voxelDimensions).ToFloat3();
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChunkStreamPointUpdated(worldPosition));
                    // UnityEngine.Debug.LogError("New StreamableVox: " + chunkPosition + ":::" + renderDistance);
                }
            })  .WithReadOnly(voxes)
                .WithReadOnly(chunkDimensions)
                .WithReadOnly(voxScales)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}