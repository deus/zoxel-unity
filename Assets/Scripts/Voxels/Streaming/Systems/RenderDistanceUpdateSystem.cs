using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
    //! Updates the RenderDistance for a chunk stream point.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class RenderDistanceUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DisabledChunkStreamPoint>()
                .ForEach((Entity e, int entityInQueryIndex, ref ChunkStreamPoint chunkStreamPoint, in OnRenderDistanceUpdated onRenderDistanceUpdated, in VoxLink voxLink) =>
            {
                if (!HasComponent<Vox>(voxLink.vox))
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<OnRenderDistanceUpdated>(entityInQueryIndex, e);
                chunkStreamPoint.renderDistance = onRenderDistanceUpdated.renderDistance;
                // todo: make Stream Chunks Many (stream points) to one system, instead of one to one
                // \todo: Pass in quadrant for this update
                PostUpdateCommands.SetComponent(entityInQueryIndex, voxLink.vox, new StreamableVox(chunkStreamPoint.chunkPosition, 1, chunkStreamPoint.renderDistance));
                PostUpdateCommands.AddComponent<StreamVox>(entityInQueryIndex, voxLink.vox);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}