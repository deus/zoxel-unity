using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
    //! Removes StreamVox events after use.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxStreamEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(ComponentType.ReadOnly<StreamVox>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<StreamVox>(processQuery);
        }
    }
}