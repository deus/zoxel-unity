using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Voxels.Minivoxes
{
    //! The original colours of a minivox. Used for model generation.
	public struct MinivoxColor : IComponentData
	{
		public Color color;
		public Color secondaryColor;

        public MinivoxColor(Color color, Color secondaryColor)
        {
            this.color = color;
            this.secondaryColor = secondaryColor;
        }
	}
}