using Unity.Entities;

namespace Zoxel.Voxels.Minivoxes
{
    //! An Minivox tagged will have it's collision ignored in VoxelCollisionDetectSystem.
    public struct IgnoreMinivoxCollision : IComponentData { }
}