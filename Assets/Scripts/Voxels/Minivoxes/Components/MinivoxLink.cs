using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels.Minivoxes
{
    //! Links a voxel position to store a Minivox Entity link.
	public struct MinivoxLink
	{
		public int3 position;
		public Entity entity;

		public MinivoxLink(int3 position)
		{
			this.position = position;
			this.entity = new Entity();
		}
	}
}