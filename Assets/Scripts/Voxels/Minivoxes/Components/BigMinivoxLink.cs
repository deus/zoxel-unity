/*using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels.Minivoxes
{
	//! Uses multiple voxel positions to store an Minivox Entity link.
	public struct BigMinivoxLink
	{
		//! The entity of our Minivox!
		public Entity entity;
		//! Positions of a voxel (global).
		public BlitableArray<int3> positions;
		//! For Minivoxes that span over multiple chunks, link them as well so we can destroy them properly.
		public BlitableArray<Entity> chunkLinks;

		public BigMinivoxLink(in NativeList<int3> positions, in NativeList<Entity> chunkLinks)
		{
			this.entity = new Entity();	// Will link after spawned!
			this.positions = new BlitableArray<int3>(positions.Length, Allocator.Persistent);
			for (int i = 0; i < positions.Length; i++)
			{
				this.positions[i] = positions[i];
			}
			this.chunkLinks = new BlitableArray<Entity>(chunkLinks.Length, Allocator.Persistent);
			for (int i = 0; i < chunkLinks.Length; i++)
			{
				this.chunkLinks[i] = chunkLinks[i];
			}
		}

		public void DisposeFinal()
		{
			positions.DisposeFinal();
			chunkLinks.DisposeFinal();
		}

		public void Dispose()
		{
			positions.Dispose();
			chunkLinks.Dispose();
		}

		public bool IsMultiChunk()
		{
			return chunkLinks.Length > 0;
		}

		public BigMinivoxLink Clone()
		{
			var newBigMinivox = new BigMinivoxLink();
			newBigMinivox.entity = new Entity();
			newBigMinivox.positions = new BlitableArray<int3>(positions.Length, Allocator.Persistent);
			for (int i = 0; i < positions.Length; i++)
			{
				newBigMinivox.positions[i] = positions[i];
			}
			newBigMinivox.chunkLinks = new BlitableArray<Entity>(chunkLinks.Length, Allocator.Persistent);
			for (int i = 0; i < chunkLinks.Length; i++)
			{
				newBigMinivox.chunkLinks[i] = chunkLinks[i];
			}
			return newBigMinivox;
		}

		public bool HasPosition(int3 position)
		{
			for (int i = 0; i < positions.Length; i++)
			{
				if (positions[i] == position)
				{
					return true;
				}
			}
			return false;
		}


		public int GetPositionIndex(int3 globalPosition)
		{
			for (int i = 0; i < positions.Length; i++)
			{
				if (positions[i] == globalPosition)
				{
					return i;
				}
			}
			return -1;
		}
	}
}*/
			/*if (chunkLinks.Length > 0)
			{
				UnityEngine.Debug.LogError("MultiChunk Voxel Added: " + chunkLinks.Length);
			}*/