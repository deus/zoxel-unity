using Unity.Entities;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Zoxel.Voxels.Minivoxes
{
	//! Links a Chunk to Minivox entities using voxel positions.
	/**
	*	BigMinivoxes just link up inside a minivox position.
	*/
	public struct MinivoxLinks : IComponentData
	{ 
        public UnsafeParallelHashMap<int3, Entity> minivoxes;
		public int removedCount;

		public void Dispose()
		{
			if (minivoxes.IsCreated)
			{
            	minivoxes.Dispose();
				removedCount = 0;
			}
		}

        public int Length
        {
            get
            { 
				if (!minivoxes.IsCreated)
				{
					return 0;
				}
                return minivoxes.Count();
            }
        }

		public bool Remove(int3 removed)
		{
			//UnityEngine.Debug.LogError("Before removed: " + this.minivoxes.Capacity);
			var success = minivoxes.Remove(removed);
			if (success)
			{
				removedCount++;
			}
			//UnityEngine.Debug.LogError("Decreased Minivoxes: " + this.minivoxes.Capacity);
			return success;
		}

		public void Remove(in BlitableArray<int3> removed)
		{
			if (minivoxes.IsCreated)
			{
				// UnityEngine.Debug.LogError("Before removed: " + this.minivoxes.Capacity + " :: " + removed.Length);
				for (int i = 0; i < removed.Length; i++)
				{
					// UnityEngine.Debug.LogError("Chunk Removed Key [" + removed[i] + "]");
					if (minivoxes.Remove(removed[i]))
					{
						removedCount++;
					}
				}
			}
		}

		public void Add(int3 added)
		{
			if (!minivoxes.IsCreated)
			{
            	this.minivoxes = new UnsafeParallelHashMap<int3, Entity>(1, Allocator.Persistent);
				minivoxes[added] = new Entity();
				return;
			}
			Add2(added);
		}

		public void Add2(int3 added)
		{
			if (HasMinivox(added))
			{
				// UnityEngine.Debug.LogError("Key already existed: " + added);
				minivoxes[added] = new Entity();
				return;
			}
			removedCount -= 1;
			if (removedCount < 0)
			{
				this.minivoxes.Capacity++;
				removedCount++;
			}
			minivoxes[added] = new Entity();
		}

		public int GetPoolCount()
		{
			if (!minivoxes.IsCreated)
			{
				return 0;
			}
			return minivoxes.Capacity - minivoxes.Count();
		}

		public void Add(in BlitableArray<int3> added)
		{
			if (!minivoxes.IsCreated)
			{
            	this.minivoxes = new UnsafeParallelHashMap<int3, Entity>(added.Length, Allocator.Persistent);
				for (int i = 0; i < added.Length; i++)
				{
					minivoxes[added[i]] = new Entity();
				}
				return;
			}
			for (int i = 0; i < added.Length; i++)
			{
				Add2(added[i]);
			}
		}

		public bool HasMinivox(int3 position)
		{
			if (!minivoxes.IsCreated)
			{
				return false;
			}
			return minivoxes.ContainsKey(position);
		}

		public Entity GetMinivox(int3 position)
		{
			Entity minivoxEntity;
			if (minivoxes.IsCreated && minivoxes.TryGetValue(position, out minivoxEntity))
			{
				return minivoxEntity;
			}
			return new Entity();
		}
	}
}