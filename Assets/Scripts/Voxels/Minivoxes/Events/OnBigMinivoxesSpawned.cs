using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels.Minivoxes
{
    //! Links BigMinivoxes to chunk after they spawn.
	public struct OnBigMinivoxesSpawned : IComponentData
    {
        public BlitableArray<Entity> chunks;
        public BlitableArray<int3> added;

        public OnBigMinivoxesSpawned(in NativeList<Entity> chunks, in NativeList<int3> added)
        {
            this.chunks = new BlitableArray<Entity>(chunks.Length, Allocator.Persistent);
            for (int i = 0; i < chunks.Length; i++)
            {
                this.chunks[i] = chunks[i];
            }
            this.added = new BlitableArray<int3>(added.Length, Allocator.Persistent);
            for (int i = 0; i < added.Length; i++)
            {
                this.added[i] = added[i];
            }
        }

        public void DisposeFinal()
        {
            this.chunks.DisposeFinal();
            this.added.DisposeFinal();
        }
    }
}