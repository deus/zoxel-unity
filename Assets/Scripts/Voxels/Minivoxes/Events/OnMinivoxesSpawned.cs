using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels.Minivoxes
{
    //! Links minivoxes to chunk after they spawn.
	public struct OnMinivoxesSpawned : IComponentData
    {
        public Entity chunk;
        public BlitableArray<int3> added;

        public OnMinivoxesSpawned(Entity chunk, in NativeList<int3> added)
        {
            this.chunk = chunk;
            this.added = new BlitableArray<int3>(added.Length, Allocator.Persistent);
            for (int i = 0; i < added.Length; i++)
            {
                this.added[i] = added[i];
            }
        }

        public void DisposeFinal()
        {
            added.DisposeFinal();
        }
    }
}