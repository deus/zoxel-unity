using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels.Minivoxes
{
    //! Removes minivoxes from chunk MinivoxLinks when they are destroyed.
	public struct OnBigMinivoxesRemoved : IComponentData
    {

        /*public int removedCount;

        public OnMinivoxesRemoved(Entity chunk, int removedCount)
        {
            this.chunk = chunk;
            this.removedCount = removedCount;
        }*/
        
        public BlitableArray<Entity> chunks;
        public BlitableArray<int3> removed;

        public OnBigMinivoxesRemoved(in NativeList<Entity> chunks, in NativeList<int3> removed)
        {
            this.chunks = new BlitableArray<Entity>(chunks.Length, Allocator.Persistent);
            for (int i = 0; i < chunks.Length; i++)
            {
                this.chunks[i] = chunks[i];
            }
            this.removed = new BlitableArray<int3>(removed.Length, Allocator.Persistent);
            for (int i = 0; i < removed.Length; i++)
            {
                this.removed[i] = removed[i];
            }
        }

        public void DisposeFinal()
        {
            this.chunks.DisposeFinal();
            this.removed.DisposeFinal();
        }
    }
}