using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels.Minivoxes
{
    //! Removes minivoxes from chunk MinivoxLinks when they are destroyed.
	public struct OnMinivoxesRemoved : IComponentData
    {
        public Entity chunk;
        public BlitableArray<int3> removed;

        /*public int removedCount;

        public OnMinivoxesRemoved(Entity chunk, int removedCount)
        {
            this.chunk = chunk;
            this.removedCount = removedCount;
        }*/

        public OnMinivoxesRemoved(Entity chunk, in NativeList<int3> removed)
        {
            this.chunk = chunk;
            this.removed = new BlitableArray<int3>(removed.Length, Allocator.Persistent);
            for (int i = 0; i < removed.Length; i++)
            {
                this.removed[i] = removed[i];
            }
        }

        public void DisposeFinal()
        {
            this.removed.DisposeFinal();
        }
    }
}