using Unity.Entities;

namespace Zoxel.Voxels.Minivoxes
{
	//! Removes minivoxes from a chunk.
	public struct DestroyChunkMinivoxes : IComponentData { }
}