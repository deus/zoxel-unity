using Unity.Entities;

namespace Zoxel.Voxels.Minivoxes
{
    //! Minivox Systems go under here.
    [AlwaysUpdateSystem]
    [UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class MinivoxSystemGroup : ComponentSystemGroup
    {
        public static Entity minivoxesSpawnedPrefab;
        public static Entity minivoxesRemovedPrefab;
        public static Entity bigMinivoxesSpawnedPrefab;
        public static Entity bigMinivoxesRemovedPrefab;
		private EntityQuery minivoxesSpawnedQuery;

		protected override void OnCreate()
		{
            base.OnCreate();
			minivoxesSpawnedQuery = GetEntityQuery(ComponentType.ReadOnly<OnMinivoxesSpawned>());
            var minivoxesSpawnedArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
				typeof(OnMinivoxesSpawned));
            minivoxesSpawnedPrefab = EntityManager.CreateEntity(minivoxesSpawnedArchetype);
            var minivoxesRemovedArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
				typeof(OnMinivoxesRemoved));
            minivoxesRemovedPrefab = EntityManager.CreateEntity(minivoxesRemovedArchetype);
            var bigMinivoxesSpawnedArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
				typeof(OnBigMinivoxesSpawned));
            bigMinivoxesSpawnedPrefab = EntityManager.CreateEntity(bigMinivoxesSpawnedArchetype);
            var bigMinivoxesRemovedArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
				typeof(OnBigMinivoxesRemoved));
            bigMinivoxesRemovedPrefab = EntityManager.CreateEntity(bigMinivoxesRemovedArchetype);
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();
            VoxelSystemGroup.minivoxesSpawnedCount = minivoxesSpawnedQuery.CalculateEntityCount();
        }
    }
}