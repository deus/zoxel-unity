using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Voxels.Minivoxes
{
	//! Minivoxes link up to chunks after spawning.
    /**
    *   - Linking System -
    */
	[BurstCompile, UpdateInGroup(typeof(MinivoxSystemGroup))]
    public partial class MinivoxesRemovedSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery chunksQuery;
		private EntityQuery removedMinivoxesQuery;
        private EntityQuery removedMinivoxQuery;
		
		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			chunksQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.Exclude<DestroyChunkMinivoxes>(),
				ComponentType.ReadWrite<MinivoxLinks>());
			removedMinivoxesQuery = GetEntityQuery(ComponentType.ReadOnly<OnMinivoxesRemoved>());
			removedMinivoxQuery = GetEntityQuery(
				ComponentType.ReadOnly<DestroyEntity>(),
				ComponentType.ReadOnly<Minivox>(),
				ComponentType.ReadOnly<ChunkLink>());
				
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
			if (removedMinivoxesQuery.IsEmpty)
			{
                return;
            }
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
			//! First Removed removed children.
            PostUpdateCommands2.DestroyEntity(removedMinivoxesQuery);
            var minivoxesRemovedEntities = removedMinivoxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var onMinivoxesRemoved = GetComponentLookup<OnMinivoxesRemoved>(true);
            Dependency = Entities.ForEach((Entity e, ref MinivoxLinks minivoxLinks) =>
            {
                if (!minivoxLinks.minivoxes.IsCreated || minivoxLinks.minivoxes.Count() == 0)
                {
                    return;
                }
                for (int i = 0; i < minivoxesRemovedEntities.Length; i++)
                {
                    var e2 = minivoxesRemovedEntities[i];
                    var onMinivoxesRemoved2 = onMinivoxesRemoved[e2];
                    if (e == onMinivoxesRemoved2.chunk)
                    {
                        minivoxLinks.Remove(in onMinivoxesRemoved2.removed);
                        onMinivoxesRemoved2.DisposeFinal();
                    }
                }
                // UnityEngine.Debug.LogError("Added " + onMinivoxesSpawned.spawned + " to chunk.");
            })  .WithReadOnly(minivoxesRemovedEntities)
                .WithDisposeOnCompletion(minivoxesRemovedEntities)
                .WithReadOnly(onMinivoxesRemoved)
                .ScheduleParallel(Dependency);
		}
	}
}