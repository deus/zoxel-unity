using Unity.Entities;
using Unity.Burst;
using Zoxel.Voxels.Authoring;

namespace Zoxel.Voxels.Minivoxes
{
	//! Spawns and destroys minivoxes based on Chunk event's OnChunkDivisionUpdated.
	[BurstCompile, UpdateInGroup(typeof(MinivoxSystemGroup))]
    public partial class MinivoxDivisionSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<MinivoxSettings>();
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
            var minivoxSettings = GetSingleton<MinivoxSettings>();
			var minivoxDistance = minivoxSettings.minivoxDistance;
			var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
			// PostUpdateCommands2.MinimumChunkSize = 4096 * 16;
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
			Dependency = Entities
				.WithNone<DestroyChunkMinivoxes, SpawnMinivoxes, OnMinivoxesSpawned>()
				.WithNone<DestroyEntity, ChunkHasMinivoxes>()
                .WithChangeFilter<ChunkDivision>()
				.WithAll<PlanetChunk>()
				.ForEach((Entity e, int entityInQueryIndex, in ChunkDivision chunkDivision) =>
			{
				if (chunkDivision.viewDistance <= minivoxDistance)
				{
					PostUpdateCommands.AddComponent<ChunkHasMinivoxes>(entityInQueryIndex, e);
					PostUpdateCommands.AddComponent<SpawnMinivoxes>(entityInQueryIndex, e);
				}
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
			Dependency = Entities
				.WithNone<DestroyChunkMinivoxes, SpawnMinivoxes, OnMinivoxesSpawned>()
				.WithNone<DestroyEntity>()
                .WithChangeFilter<ChunkDivision>()
				.WithAll<PlanetChunk, ChunkHasMinivoxes>()
				.ForEach((Entity e, int entityInQueryIndex, in ChunkDivision chunkDivision) =>
			{
				if (chunkDivision.viewDistance > minivoxDistance)
				{
					PostUpdateCommands.RemoveComponent<ChunkHasMinivoxes>(entityInQueryIndex, e);
					PostUpdateCommands.AddComponent<DestroyChunkMinivoxes>(entityInQueryIndex, e);
				}
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}