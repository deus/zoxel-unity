using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels.Minivoxes
{ 
    //! Removes SpawnMinivoxes event after use 2.
    /**
    *   - End System -
    */
	[BurstCompile, UpdateInGroup(typeof(MinivoxSystemGroup))]
    public partial class MinivoxesSpawnEnd2System : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery removeChunkBuilderMinivoxLightsQuery;
		private EntityQuery addSpawnMinivoxesQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			addSpawnMinivoxesQuery = GetEntityQuery(
				ComponentType.Exclude<VoxelBuilderLightsOnly>(),
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.Exclude<DestroyChunkMinivoxes>(),
				ComponentType.Exclude<SpawnMinivoxes>(),
				ComponentType.ReadOnly<ChunkHasMinivoxes>(),
				ComponentType.ReadOnly<ChunkBuilderMinivoxLights>());
			removeChunkBuilderMinivoxLightsQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.Exclude<DestroyChunkMinivoxes>(),
				ComponentType.Exclude<SpawnMinivoxes>(),
				ComponentType.ReadOnly<ChunkBuilderMinivoxLights>());
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
			var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
			if (addSpawnMinivoxesQuery.CalculateEntityCount() > 0)
			{
				PostUpdateCommands2.AddComponent<SpawnMinivoxes>(addSpawnMinivoxesQuery);
			}
			if (removeChunkBuilderMinivoxLightsQuery.CalculateEntityCount() > 0)
			{
				PostUpdateCommands2.RemoveComponent<ChunkBuilderMinivoxLights>(removeChunkBuilderMinivoxLightsQuery);
			}
		}
	}
}