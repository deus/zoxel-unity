using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Voxels.Minivoxes
{
	//! Minivoxes link up to chunks after spawning.
    /**
    *   - Linking System -
    */
	[BurstCompile, UpdateInGroup(typeof(MinivoxSystemGroup))]
    public partial class BigMinivoxesSpawnedSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery chunksQuery;
		private EntityQuery bigMinivoxesSpawnedQuery;
        private EntityQuery newBigMinivoxQuery;
		
		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			chunksQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.Exclude<DestroyChunkMinivoxes>(),
				ComponentType.ReadWrite<MinivoxLinks>());
			bigMinivoxesSpawnedQuery = GetEntityQuery(ComponentType.ReadOnly<OnBigMinivoxesSpawned>());
			newBigMinivoxQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.ReadOnly<NewBigMinivox>(),
				ComponentType.ReadOnly<BigMinivox>(),
				ComponentType.ReadOnly<ChunkLink>());
				
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
			if (bigMinivoxesSpawnedQuery.IsEmpty)
            {
                return;
            }
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
			PostUpdateCommands2.DestroyEntity(bigMinivoxesSpawnedQuery);
			PostUpdateCommands2.RemoveComponent<NewBigMinivox>(newBigMinivoxQuery);
			//! Next initializes children.
			var bigMinivoxesSpawnedEntities = bigMinivoxesSpawnedQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
			Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var onBigMinivoxesSpawned = GetComponentLookup<OnBigMinivoxesSpawned>(true);
			Dependency = Entities.ForEach((Entity e, ref MinivoxLinks minivoxLinks) =>
			{
				for (int i = 0; i < bigMinivoxesSpawnedEntities.Length; i++)
				{
					var e2 = bigMinivoxesSpawnedEntities[i];
					var onBigMinivoxesSpawned2 = onBigMinivoxesSpawned[e2];
					for (int j = 0; j < onBigMinivoxesSpawned2.chunks.Length; j++)
					{
						var chunkEntity = onBigMinivoxesSpawned2.chunks[j];
						if (e == chunkEntity)
						{
							// Add key for this BigMinivox
							var addedPosition = onBigMinivoxesSpawned2.added[j];
							minivoxLinks.Add(addedPosition);
						}
					}
				}
				// UnityEngine.Debug.LogError("Added " + onBigMinivoxesSpawned.spawned + " to chunk.");
			})  .WithReadOnly(bigMinivoxesSpawnedEntities)
				.WithDisposeOnCompletion(bigMinivoxesSpawnedEntities)
				.WithReadOnly(onBigMinivoxesSpawned)
				.ScheduleParallel(Dependency);
			// Dispose of events
			Dependency = Entities.ForEach((in OnBigMinivoxesSpawned onBigMinivoxesSpawned) =>
			{
				onBigMinivoxesSpawned.DisposeFinal();
			}).ScheduleParallel(Dependency);
			//! Finally, For each child, using the index, sets into parents children that is passed in.
			var parentEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
			Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var minivoxLinks = GetComponentLookup<MinivoxLinks>(false);
			// parentEntities.Dispose();
			//! Sets position per chunk/position link
			Dependency = Entities
				.WithAll<NewBigMinivox, BigMinivox>()
				.ForEach((Entity e, in MultiVoxelPosition multiVoxelPosition, in MultiChunkLink multiChunkLink) =>
			{
				for (int i = 0; i < multiChunkLink.chunks.Length; i++)
				{
					var chunkEntity = multiChunkLink.chunks[i];
					var minivoxLinks2 = minivoxLinks[chunkEntity];
					var position = multiVoxelPosition.positions[i];
					minivoxLinks2.minivoxes[position] = e;
					// minivoxLinks[chunkLink.chunk] = minivoxLinks2;
					// UnityEngine.Debug.LogError("Chunk [" + chunkEntity.Index + "] BigMinivox Key Set of [" + position + "] " + e.Index);
				}
                if (parentEntities.Length > 0) { var e2 = parentEntities[0]; }
			})  .WithNativeDisableContainerSafetyRestriction(minivoxLinks)
                .WithReadOnly(parentEntities)
                .WithDisposeOnCompletion(parentEntities)
				.ScheduleParallel(Dependency);
			// UnityEngine.Debug.LogError("Updated Minivoxes: " + bigMinivoxesSpawnedQuery.CalculateEntityCount() + " : " + bigMinivoxesRemovedQuery.CalculateEntityCount());
		}
	}
}

/*var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
var minivoxSpawnedEntities = minivoxesSpawnedQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
var onBigMinivoxesSpawned = GetComponentLookup<OnBigMinivoxesSpawned>(true);
var minivoxEntities = newBigMinivoxQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
var minivoxes2 = GetComponentLookup<BigMinivox>(true);
var chunkLinks = GetComponentLookup<ChunkLink>(true);
Dependency = Entities
	.WithNone<DestroyChunkMinivoxes>()
	.ForEach((Entity e, int entityInQueryIndex, ref MinivoxLinks minivoxLinks) =>
{
	var eventTriggered = false;
	var minivoxes = new NativeList<int3>();
	var bigMinivoxes = new NativeList<BigMinivoxLink>();
	for (int i = 0; i < minivoxSpawnedEntities.Length; i++)
	{
		var e2 = minivoxSpawnedEntities[i];
		var onBigMinivoxesSpawned2 = onBigMinivoxesSpawned[e2];
		if (onBigMinivoxesSpawned2.chunk == e)
		{
			// PostUpdateCommands.DestroyEntity(entityInQueryIndex, e2);
			eventTriggered = true;
			for (int j = 0; j < onBigMinivoxesSpawned2.minivoxes.Length; j++)
			{
				minivoxes.Add(onBigMinivoxesSpawned2.minivoxes[j]);
			}
			for (int j = 0; j < onBigMinivoxesSpawned2.bigMinivoxes.Length; j++)
			{
				bigMinivoxes.Add(onBigMinivoxesSpawned2.bigMinivoxes[j].Clone());
			}
		}
	}
	if (!eventTriggered)
	{
		minivoxes.Dispose();
		bigMinivoxes.Dispose();
		return;
	}
	minivoxLinks.Dispose();
	var totalMinivoxes = minivoxes.Length + bigMinivoxes.Length;
	if (totalMinivoxes == 0)
	{
		// Event used to clear minivoxes
		for (int i = 0; i < minivoxSpawnedEntities.Length; i++)
		{
			var e2 = minivoxSpawnedEntities[i];
			var onBigMinivoxesSpawned2 = onBigMinivoxesSpawned[e2];
			if (onBigMinivoxesSpawned2.chunk == e)
			{
				onBigMinivoxesSpawned2.Dispose();
				PostUpdateCommands.DestroyEntity(entityInQueryIndex, e2);
			}
		}
		minivoxes.Dispose();
		bigMinivoxes.Dispose();
		return;
	}
	minivoxLinks.InitializeMinivoxes(minivoxes.Length);
	minivoxLinks.bigMinivoxes = new BlitableArray<BigMinivoxLink>(bigMinivoxes.Length, Allocator.Persistent);
	for (int i = 0; i < bigMinivoxes.Length; i++)
	{
		minivoxLinks.bigMinivoxes[i] = bigMinivoxes[i];
	}
	// UnityEngine.Debug.LogError("minivoxes: " + minivoxLinks.minivoxes.Length + ", bigMinivoxes: " + minivoxLinks.bigMinivoxes.Length);
	var count = 0;
	var bigMinivoxesCount = 0;
	for (int i = 0; i < minivoxEntities.Length; i++)
	{
		var e2 = minivoxEntities[i];
		var chunkLink = chunkLinks[e2];
		var isBigMinivox = HasComponent<BigMinivox>(e2);
		if (chunkLink.chunk == e || isBigMinivox)
		{
			var minivox = minivoxes2[e2];
			// find place in links
			if (!isBigMinivox)
			{
				if (minivoxes.Contains(minivox.voxelPosition))
				{
					minivoxLinks.minivoxes[minivox.voxelPosition] = e2;
					count++;
					if (count == totalMinivoxes)
					{
						//UnityEngine.Debug.LogError("Found all minivoxes: " + count);
						i = minivoxEntities.Length;
					}
				}
			}
			else if (isBigMinivox)
			{
				for (int j = 0; j < minivoxLinks.bigMinivoxes.Length; j++)
				{
					var minivoxLink = minivoxLinks.bigMinivoxes[j];
					if (minivoxLink.HasPosition(minivox.globalVoxelPosition))
					{
						minivoxLink.entity = e2;
						minivoxLinks.bigMinivoxes[j] = minivoxLink;
						count++;
						bigMinivoxesCount++;
						if (count == totalMinivoxes)
						{
							//UnityEngine.Debug.LogError("Found all minivoxes: " + count);
							i = minivoxEntities.Length;
						}
						break;
					}
				}
			}
		}
	}
	if (count == totalMinivoxes)
	{
		for (int i = 0; i < minivoxSpawnedEntities.Length; i++)
		{
			var e2 = minivoxSpawnedEntities[i];
			var onBigMinivoxesSpawned2 = onBigMinivoxesSpawned[e2];
			if (onBigMinivoxesSpawned2.chunk == e)
			{
				onBigMinivoxesSpawned2.Dispose();
				PostUpdateCommands.DestroyEntity(entityInQueryIndex, e2);
			}
		}
		//UnityEngine.Debug.LogError("Found all minivoxes: " + count + " with bigMinivoxes: " + bigMinivoxesCount);
	}
	#if DEBUG_BIGMINIVOXES_HEALTH
	if (count != totalMinivoxes)
	{
		UnityEngine.Debug.LogError("Failed to Find all minivoxes: " + count + " but target is: " + totalMinivoxes
			+ " with bigMinivoxes: " + bigMinivoxesCount + " but target is: " + minivoxLinks.bigMinivoxes.Length);
		// debug all BigMinivoxxes
		for (int i = 0; i < minivoxLinks.bigMinivoxes.Length; i++)
		{
			UnityEngine.Debug.LogError("	- minivoxLinks.bigMinivoxes[" + i + "]: " + minivoxLinks.bigMinivoxes[i].positions[0]
				+ " - " + minivoxLinks.bigMinivoxes[i].positions[1]);
		}
	}
	#endif
	minivoxes.Dispose();
	bigMinivoxes.Dispose();
})  .WithReadOnly(minivoxSpawnedEntities)
	.WithDisposeOnCompletion(minivoxSpawnedEntities)
	.WithReadOnly(onBigMinivoxesSpawned)
	.WithReadOnly(minivoxEntities)
	.WithDisposeOnCompletion(minivoxEntities)
	.WithReadOnly(minivoxes2)
	.WithReadOnly(chunkLinks)
	.ScheduleParallel(Dependency);
// commandBufferSystem.AddJobHandleForProducer(Dependency);*/