using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Generic;

namespace Zoxel.Voxels.Minivoxes
{
    //! Destroys minivoxes attached to a chunk on DestroyChunkMinivoxes event.
    /**
    *   -done- Passes in Chunks, to handle BigMinivox's that span over multiple chunks. Only destroy BigMinivox if all chunks don't have ChunkHasMinivoxes.
    *   -done- Handle activating this DestroyChunkMinivoxes event again for chunks. Atm it won't destroy the ones that span chunks.
    */
    [UpdateBefore(typeof(EntityDestroySystem))]
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ChunkMinivoxesDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery bigMinivoxQuery;
        private EntityQuery chunksQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            bigMinivoxQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<BigMinivox>());
            chunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<PlanetChunk>(),
                ComponentType.ReadOnly<Chunk>());
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
			var bigMinivoxesRemovedPrefab = MinivoxSystemGroup.bigMinivoxesRemovedPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var bigMinivoxEntities = bigMinivoxQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var multiVoxelPositions = GetComponentLookup<MultiVoxelPosition>(true);
            var multiChunkLinks = GetComponentLookup<MultiChunkLink>(true);
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var chunkHasMinivoxes = GetComponentLookup<ChunkHasMinivoxes>(true);
            bigMinivoxEntities.Dispose();
            chunkEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<DestroyChunkMinivoxes>()
                .ForEach((Entity e, int entityInQueryIndex, ref MinivoxLinks minivoxLinks) =>
			{
				PostUpdateCommands.RemoveComponent<DestroyChunkMinivoxes>(entityInQueryIndex, e);
				if (minivoxLinks.minivoxes.IsCreated && minivoxLinks.minivoxes.Count() > 0)
				{
                    // records removedBigMinivoxes ones
                    var removedMinivoxes = new NativeList<int3>();
                    // gets unique list of BigMinivoxes
                    var bigMinivoxEntities = new NativeList<Entity>();
                    // Records MultiChunkLinks and MultiVoxelPosition data of the removed BigMinvioxes
                    var removedBigMinivoxes = new NativeList<int3>();
                    var removedBigMinivoxesChunks = new NativeList<Entity>();
                    //! For all minivoxes on chunk.
                    foreach (var KVP in minivoxLinks.minivoxes)
                    {
                        var minivoxEntity = KVP.Value;
                        if (HasComponent<Minivox>(minivoxEntity))
                        {
                            if (multiChunkLinks.HasComponent(minivoxEntity))
                            {
                                if (!bigMinivoxEntities.Contains(minivoxEntity))
                                {
                                    bigMinivoxEntities.Add(minivoxEntity);
                                }
                            }
                            else
                            {
                                PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, minivoxEntity);
                                removedMinivoxes.Add(KVP.Key);
                            }
                        }
                    }
                    //! Removes keys for any minivoxes.
                    for (int i = 0; i < removedMinivoxes.Length; i++)
                    {
                        minivoxLinks.minivoxes.Remove(removedMinivoxes[i]);
                    }
                    //! Checks if all chunks of a BigMinivox have no Minivoxes.
                    for (int i = 0; i < bigMinivoxEntities.Length; i++)
                    {
                        var minivoxEntity = bigMinivoxEntities[i];
                        var multiChunkLinks2 = multiChunkLinks[minivoxEntity];
                        var keepBigMinivox = false;
                        for (int j = 0; j < multiChunkLinks2.chunks.Length; j++)
                        {
                            var chunkEntity = multiChunkLinks2.chunks[j];
                            if (chunkHasMinivoxes.HasComponent(chunkEntity))
                            {
                                // UnityEngine.Debug.LogError("Keeping MultiChunk BigMinivox at: " + i);
                                keepBigMinivox = true;
                                break;
                            }
                        }
                        if (!keepBigMinivox)
                        {
                            //! Destroys BigMinivox, and adds an event to remove keys from corresponding chunks.
                            PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, minivoxEntity);
                            var multiVoxelPositions2 = multiVoxelPositions[minivoxEntity];
                            for (int j = 0; j < multiChunkLinks2.chunks.Length; j++)
                            {
                                var chunkEntity = multiChunkLinks2.chunks[j];
                                /*if (chunkEntity == e)
                                {
                                    removedBigMinivoxes.Add(multiVoxelPositions2.positions[j]);
                                }*/
                                removedBigMinivoxes.Add(multiVoxelPositions2.positions[j]);
                                removedBigMinivoxesChunks.Add(chunkEntity);
                            }
                        }
                    }
                    //! Adds an event to remove keys from corresponding chunks of BigMinivoxes.
                    if (removedBigMinivoxes.Length > 0)
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, bigMinivoxesRemovedPrefab),
                            new OnBigMinivoxesRemoved(in removedBigMinivoxesChunks, in removedBigMinivoxes));
                        //UnityEngine.Debug.LogError("Removed BigMinivoxes: " + grassesChunksRemoved.Length + " :: "  + grassesRemoved.Length);
                    }
                    //! Disposes Temporary arrays.
                    removedMinivoxes.Dispose();
                    removedBigMinivoxes.Dispose();
                    removedBigMinivoxesChunks.Dispose();
                    bigMinivoxEntities.Dispose();
                }
			})  .WithReadOnly(multiChunkLinks)
                .WithReadOnly(multiVoxelPositions)
                .WithReadOnly(chunkHasMinivoxes)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}