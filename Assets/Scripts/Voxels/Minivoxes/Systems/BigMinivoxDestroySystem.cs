using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels.Minivoxes
{
    //! Destroys MultiVoxelPosition, MultiChunkLink on DestroyEntity event.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class BigMinivoxDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Dependency = Entities
                .WithAll<DestroyEntity, MultiVoxelPosition>()
                .ForEach((in MultiVoxelPosition multiVoxelPosition) =>
			{
                multiVoxelPosition.DisposeFinal();
			}).ScheduleParallel(Dependency);
			Dependency = Entities
                .WithAll<DestroyEntity, MultiChunkLink>()
                .ForEach((in MultiChunkLink multiChunkLink) =>
			{
                multiChunkLink.DisposeFinal();
			}).ScheduleParallel(Dependency);
		}
	}
}