using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels.Minivoxes
{
    //! Destroys minivoxes attached to a chunk on DestroyEntity event.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class MinivoxLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithAll<DestroyEntity, MinivoxLinks>()
                .ForEach((int entityInQueryIndex, in MinivoxLinks minivoxLinks) =>
			{
				if (minivoxLinks.minivoxes.IsCreated)
				{
                    foreach (var KVP in minivoxLinks.minivoxes)
                    {
                        var minivoxEntity = KVP.Value;
                        if (HasComponent<Minivox>(minivoxEntity))
                        {
                            PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, minivoxEntity);
                        }
                    }
                    minivoxLinks.Dispose();
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}