using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Rendering;

namespace Zoxel.Voxels.Minivoxes
{
	//!	Initializes minivoxes after they spawn.
	/**
	*	\todo Remove the color gen here and just use GenerateVoxColors component.
	*	\todo 14 second lag here, try to optimize?
	*/
	[BurstCompile, UpdateInGroup(typeof(MinivoxSystemGroup))]
    public partial class MinivoxInitializeSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery worldsQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            worldsQuery = GetEntityQuery(
				ComponentType.ReadOnly<Planet>(),
				ComponentType.ReadOnly<VoxelLinks>());
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithAll<InitializeEntity, Minivox>()
				.ForEach((Entity e, ref Seed seed, in VoxelPosition voxelPosition) =>
			{
				seed.seed = ChunkPosition.ConvertToUniqueID(voxelPosition.position);
				// UnityEngine.Debug.LogError(e.Index + " - Minivox Seed: " + seed.seed + " :: " + voxelPosition.position);
				if (seed.seed == 0)
				{
					seed.seed = 666;
				}
            }).ScheduleParallel(Dependency);
			var minivoxTransparentMaterial = VoxelManager.instance.voxelMaterials.minivoxTransparentMaterial;
			var minivoxMaterial = VoxelManager.instance.voxelMaterials.minivoxMaterial;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var worldEntities = worldsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var worldIDs = GetComponentLookup<ZoxID>(true);
			worldEntities.Dispose();
			Dependency = Entities
				.WithNone<DestroyEntity>()
				.WithAll<InitializeEntity, Minivox, GenerateVox>()
				.ForEach((Entity e, int entityInQueryIndex, ref VoxColors voxColors, in VoxLink voxLink, in MinivoxColor minivoxColor, in Seed seed) =>
			{
				voxColors.GenerateRandomColors((uint) worldIDs[voxLink.vox].id, (uint) seed.seed);
				voxColors.colors[0] = (1f * + voxColors.colors[0] + 9f * minivoxColor.color.ToFloat3()) / 10f;
				voxColors.colors[1] = (1f * + voxColors.colors[1] + 9f * minivoxColor.secondaryColor.ToFloat3()) / 10f;
            })  .WithReadOnly(worldIDs)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
			Entities
				.WithNone<DestroyEntity>()
				.WithAll<InitializeEntity, Minivox, EntityMaterials>()
				.ForEach((Entity e, int entityInQueryIndex) =>
			{
				if (HasComponent<MaterialFadeIn>(e))
				{
					PostUpdateCommands2.SetSharedComponentManaged(e, new EntityMaterials(minivoxTransparentMaterial));
				}
				else
				{
					PostUpdateCommands2.SetSharedComponentManaged(e, new EntityMaterials(minivoxMaterial));
				}
			}).WithoutBurst().Run();
		}
	}
}
			/*Entities
				.WithNone<DestroyEntity>()
				.WithAll<InitializeEntity, Minivox>()
				.ForEach((Entity e, int entityInQueryIndex, ref ZoxID zoxID) =>
			{
				zoxID.id = IDUtil.GenerateUniqueID();
			}).WithoutBurst().Run();*/