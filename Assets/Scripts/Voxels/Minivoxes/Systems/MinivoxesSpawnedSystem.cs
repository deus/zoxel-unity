using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Voxels.Minivoxes
{
	//! Minivoxes link up to chunks after spawning.
    /**
    *   - Linking System -
    */
	[BurstCompile, UpdateInGroup(typeof(MinivoxSystemGroup))]
    public partial class MinivoxesSpawnedSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery chunksQuery;
		private EntityQuery spawnedMinivoxesQuery;
        private EntityQuery newMinivoxQuery;
		
		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			spawnedMinivoxesQuery = GetEntityQuery(ComponentType.ReadOnly<OnMinivoxesSpawned>());
			chunksQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.Exclude<DestroyChunkMinivoxes>(),
				ComponentType.ReadWrite<MinivoxLinks>());
			newMinivoxQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.ReadOnly<NewMinivox>(),
				ComponentType.ReadOnly<Minivox>(),
				ComponentType.ReadOnly<ChunkLink>());
				
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
			if (spawnedMinivoxesQuery.IsEmpty)
			{
                return;
            }
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
			PostUpdateCommands2.DestroyEntity(spawnedMinivoxesQuery);
			PostUpdateCommands2.RemoveComponent<NewMinivox>(newMinivoxQuery);
			//! Next initializes children.
			var minivoxesSpawnedEntities = spawnedMinivoxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
			Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
			var onMinivoxesSpawned = GetComponentLookup<OnMinivoxesSpawned>(true);
			Dependency = Entities.ForEach((Entity e, ref MinivoxLinks minivoxLinks) =>
			{
				for (int i = 0; i < minivoxesSpawnedEntities.Length; i++)
				{
					var e2 = minivoxesSpawnedEntities[i];
					var onMinivoxesSpawned2 = onMinivoxesSpawned[e2];
					if (e == onMinivoxesSpawned2.chunk)
					{
						minivoxLinks.Add(in onMinivoxesSpawned2.added);
						onMinivoxesSpawned2.DisposeFinal();
					}
				}
				// UnityEngine.Debug.LogError("Added " + onMinivoxesSpawned.spawned + " to chunk.");
			})  .WithReadOnly(minivoxesSpawnedEntities)
				.WithDisposeOnCompletion(minivoxesSpawnedEntities)
				.WithReadOnly(onMinivoxesSpawned)
				.ScheduleParallel(Dependency);
			//! Finally, For each child, using the index, sets into parents children that is passed in.
			var parentEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
			Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
			var minivoxLinks = GetComponentLookup<MinivoxLinks>(false);
			// parentEntities.Dispose();
			Dependency = Entities
				.WithAll<NewMinivox>()
				.ForEach((Entity e, in VoxelPosition voxelPosition, in ChunkLink chunkLink) =>
			{
				var minivoxLinks2 = minivoxLinks[chunkLink.chunk];
				minivoxLinks2.minivoxes[voxelPosition.position] = e;
				// minivoxLinks[chunkLink.chunk] = minivoxLinks2;
                if (parentEntities.Length > 0) { var e2 = parentEntities[0]; }
			})  .WithNativeDisableContainerSafetyRestriction(minivoxLinks)
                .WithReadOnly(parentEntities)
                .WithDisposeOnCompletion(parentEntities)
				.ScheduleParallel(Dependency);
			// UnityEngine.Debug.LogError("Updated Minivoxes: " + spawnedMinivoxesQuery.CalculateEntityCount() + " : " + removedMinivoxesQuery.CalculateEntityCount());
		}
	}
}