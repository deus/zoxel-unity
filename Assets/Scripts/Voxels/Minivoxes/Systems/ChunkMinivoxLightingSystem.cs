using Unity.Entities;
using Unity.Burst;
using Zoxel.Voxels.Lighting;
// Maybe - i can pass in all grass/grassStrands and update them here instead

namespace Zoxel.Voxels.Minivoxes
{
	//! Updates lighting for each minivox connected to Chunk.
	/**
	*	This is part of the chunk build process.
	*	Note: if lighting is disabled, it never reaches here, but goes straight to building.
	*
	*	\todo Pass in Lighting (EntityLightingEntityLighting, MaterialBaseColor), directly instead of using EntityCommandBuffers.
	*		The UpdateEntityLighting event might be tricky. Parallel writing should be faster though then EntityCommandBuffers.
	*/
    [UpdateAfter(typeof(ChunkBuiltSystem))]
	[BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
    public partial class ChunkMinivoxLightingSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery processQuery;
		private EntityQuery minivoxesSpawnedQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			minivoxesSpawnedQuery = GetEntityQuery(ComponentType.ReadOnly<OnMinivoxesSpawned>());
			RequireForUpdate(processQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            if (minivoxesSpawnedQuery.CalculateEntityCount() != 0)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<DestroyEntity, DestroyChunkMinivoxes, SpawnMinivoxes>()
				.WithAll<ChunkBuilderMinivoxLights, ChunkHasMinivoxes>()
				.ForEach((int entityInQueryIndex, in MinivoxLinks minivoxLinks, in ChunkLights chunkLights, in Chunk chunk, in ChunkPosition chunkPosition) =>
			{
				var voxelDimensions = chunk.voxelDimensions;
				if (minivoxLinks.minivoxes.IsCreated)
				{
					foreach (var KVP in minivoxLinks.minivoxes)
					{
						var minivoxEntity = KVP.Value;
						if (!HasComponent<Minivox>(minivoxEntity) || HasComponent<BigMinivox>(minivoxEntity))
						{
							// UnityEngine.Debug.LogError("Dead Minivox....");
							continue;
						}
						var minivoxPosition = KVP.Key;
						var localPosition = VoxelUtilities.GetLocalPosition(minivoxPosition, chunkPosition.position, voxelDimensions);
						var lightIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
						var lightValue = chunkLights.lights[lightIndex];
						PostUpdateCommands.AddComponent(entityInQueryIndex, minivoxEntity, new UpdateEntityLighting(lightValue));
						// UnityEngine.Debug.LogError("Updating minivox: " + i + " - " + lightValue);
					}
				}
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}