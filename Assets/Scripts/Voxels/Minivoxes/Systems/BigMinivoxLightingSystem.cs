using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels.Lighting;

namespace Zoxel.Voxels.Minivoxes
{
	//! Updates lighting for each BigMinivox connected to Chunk.
    /**
    *   Uses BigMinivox's MultiVoxelPosition component to find multiple lights.
    */
    [UpdateAfter(typeof(ChunkBuiltSystem))]
	[BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
    public partial class BigMinivoxLightingSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery processQuery;
		private EntityQuery chunksQuery;
		private EntityQuery minivoxesSpawnedQuery;
        private EntityQuery bigMinivoxQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			minivoxesSpawnedQuery = GetEntityQuery(ComponentType.ReadOnly<OnMinivoxesSpawned>());
            bigMinivoxQuery = GetEntityQuery(ComponentType.ReadOnly<BigMinivox>());
            chunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkLights>(),
                ComponentType.ReadOnly<ChunkNeighbors>());
			RequireForUpdate(processQuery);
			RequireForUpdate(chunksQuery);
			RequireForUpdate(bigMinivoxQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            if (minivoxesSpawnedQuery.CalculateEntityCount() != 0)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var bigMinivoxEntities = bigMinivoxQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var multiVoxelPositions = GetComponentLookup<MultiVoxelPosition>(true);
			bigMinivoxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var chunkLights2 = GetComponentLookup<ChunkLights>(true);
            chunkEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<DestroyEntity, DestroyChunkMinivoxes, SpawnMinivoxes>()
				.WithAll<ChunkBuilderMinivoxLights, ChunkHasMinivoxes>()
				.ForEach((int entityInQueryIndex, in MinivoxLinks minivoxLinks, in ChunkLights chunkLights, in Chunk chunk, in ChunkPosition chunkPosition, in ChunkNeighbors chunkNeighbors) =>
			{
				var voxelDimensions = chunk.voxelDimensions;
				if (minivoxLinks.minivoxes.IsCreated)
				{
					foreach (var KVP in minivoxLinks.minivoxes)
					{
						var minivoxEntity = KVP.Value;
						if (!HasComponent<BigMinivox>(minivoxEntity))
						{
							continue;
						}
						// UnityEngine.Debug.LogError("Updating minivox: " + i + " - " + lightValue);
						var lightCombinedValue = 0;
						var lightsAdded = 0;
                    	var multiVoxelPosition = multiVoxelPositions[minivoxEntity];
						for (var j = 0; j < multiVoxelPosition.positions.Length; j++)
						{
							var globalPosition = multiVoxelPosition.positions[j];
							var localPosition = VoxelUtilities.GetLocalPosition(globalPosition, chunkPosition.position, voxelDimensions);
                            var lightValue = GetLightValue(localPosition, voxelDimensions, in chunkLights, in chunkNeighbors, in chunkLights2);
                            lightCombinedValue += lightValue;
                            lightsAdded++;
							/*if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
							{
								var voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
								lightValue += chunkLights.lights[voxelArrayIndex];
								lightsAdded++;
							}*/
							//! \todo Get lights outside chunk for this calculation
						}
						if (lightsAdded != 0)
						{
							lightCombinedValue /= lightsAdded;
						}
						PostUpdateCommands.AddComponent(entityInQueryIndex, minivoxEntity, new UpdateEntityLighting((byte) lightCombinedValue));
					}
				}
			})	.WithReadOnly(multiVoxelPositions)
                .WithReadOnly(chunkLights2)
				.ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
        
        private static byte GetLightValue(int3 localPosition, int3 voxelDimensions, in ChunkLights chunkLights, in ChunkNeighbors chunkNeighbors,
            in ComponentLookup<ChunkLights> chunkLights2)
        {
            if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
            {
                return chunkLights.lights[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)];
            }
            else
            {
                var chunkEntity = chunkNeighbors.GetChunk(ref localPosition, voxelDimensions);
                var chunkLights3 = chunkLights2[chunkEntity];
                if (chunkLights3.lights.Length == 0)
                {
                    return 0;
                }
                return chunkLights3.lights[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)];
            }
        }
	}
}
						/*var minivoxPosition = KVP.Key;
						var localPosition = VoxelUtilities.GetLocalPosition(minivoxPosition, chunkPosition.position, voxelDimensions);
						var lightIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
						var lightValue = chunkLights.lights[lightIndex];
						PostUpdateCommands.AddComponent(entityInQueryIndex, minivoxEntity, new UpdateEntityLighting(lightValue));*/

				/*for (int i = 0; i < minivoxLinks.bigMinivoxes.Length; i++)
				{
					var lightValue = 0;
					var lightsAdded = 0;
					var bigMinivoxLink = minivoxLinks.bigMinivoxes[i];
					if (!HasComponent<Minivox>(bigMinivoxLink.entity))
					{
						// UnityEngine.Debug.LogError("Dead with BigMinivox....");
						continue;
					}
					for (int j = 0; j < bigMinivoxLink.positions.Length; j++)
					{
						var globalPosition = bigMinivoxLink.positions[j];
						var localPosition = VoxelUtilities.GetLocalPosition(globalPosition, chunkPosition.position, voxelDimensions);
						if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
						{
							var voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
							lightValue += chunkLights.lights[voxelArrayIndex];
							lightsAdded++;
						}
						//! \todo Get lights outside chunk for this calculation
					}
					if (lightsAdded != 0)
					{
						lightValue /= lightsAdded;
					}
					PostUpdateCommands.AddComponent(entityInQueryIndex, bigMinivoxLink.entity, new UpdateEntityLighting((byte) lightValue));
				}*/
				//UnityEngine.Debug.LogError("Updating Minivox Lights for: " + minivoxLinks.minivoxes.Length);