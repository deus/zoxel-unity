using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels.Minivoxes
{ 
    //! Removes SpawnMinivoxes event after use.
    /**
    *   - End System -
    */
    [BurstCompile, UpdateInGroup(typeof(MinivoxSystemGroup))]
    public partial class MinivoxesSpawnEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyChunkMinivoxes>(),
                ComponentType.Exclude<EntityBusy>(),
                ComponentType.ReadOnly<SpawnMinivoxes>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<SpawnMinivoxes>(processQuery);
        }
    }
}