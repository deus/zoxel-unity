using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Voxels.Minivoxes
{
	//! Minivoxes link up to chunks after spawning.
    /**
    *   - Linking System -
    */
	[BurstCompile, UpdateInGroup(typeof(MinivoxSystemGroup))]
    public partial class BigMinivoxesRemovedSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery chunksQuery;
		private EntityQuery bigMinivoxesRemovedQuery;
        private EntityQuery removedMinivoxQuery;
		
		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			chunksQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.Exclude<DestroyChunkMinivoxes>(),
				ComponentType.ReadWrite<MinivoxLinks>());
			bigMinivoxesRemovedQuery = GetEntityQuery(ComponentType.ReadOnly<OnBigMinivoxesRemoved>());
			removedMinivoxQuery = GetEntityQuery(
				ComponentType.ReadOnly<DestroyEntity>(),
				ComponentType.ReadOnly<BigMinivox>(),
				ComponentType.ReadOnly<ChunkLink>());
				
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
			if (bigMinivoxesRemovedQuery.IsEmpty)
            {
                return;
            }
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
			//! First Removed removed children.
            PostUpdateCommands2.DestroyEntity(bigMinivoxesRemovedQuery);
            var minivoxesRemovedEntities = bigMinivoxesRemovedQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var onMinivoxesRemoved = GetComponentLookup<OnBigMinivoxesRemoved>(true);
            Dependency = Entities.ForEach((Entity e, ref MinivoxLinks minivoxLinks) =>
            {
                if (!minivoxLinks.minivoxes.IsCreated || minivoxLinks.minivoxes.Count() == 0)
                {
                    return;
                }
                for (int i = 0; i < minivoxesRemovedEntities.Length; i++)
                {
                    var e2 = minivoxesRemovedEntities[i];
                    var onMinivoxesRemoved2 = onMinivoxesRemoved[e2];
                    for (int j = 0; j < onMinivoxesRemoved2.chunks.Length; j++)
                    {
                        var chunkEntity = onMinivoxesRemoved2.chunks[j];
                        if (e == chunkEntity)
                        {
                            for (int k = 0; k < onMinivoxesRemoved2.removed.Length; k++)
                            {
                                var position = onMinivoxesRemoved2.removed[k];
                                if (minivoxLinks.Remove(position))
                                {
                                    // UnityEngine.Debug.LogError("Chunk [" + e.Index + "] BigMinivox Removed Key of [" + position + "]");
                                }
                            }
                        }
                    }
                }
                // UnityEngine.Debug.LogError("Added " + onBigMinivoxesSpawned.spawned + " to chunk.");
            })  .WithReadOnly(minivoxesRemovedEntities)
                .WithDisposeOnCompletion(minivoxesRemovedEntities)
                .WithReadOnly(onMinivoxesRemoved)
                .ScheduleParallel(Dependency);
            // Dispose of events
            Dependency = Entities.ForEach((in OnBigMinivoxesRemoved onBigMinivoxesRemoved) =>
            {
                onBigMinivoxesRemoved.DisposeFinal();
            }).ScheduleParallel(Dependency);
        }
	}
}