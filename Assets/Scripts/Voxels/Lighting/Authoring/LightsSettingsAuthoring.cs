using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Zoxel.Voxels.Lighting.Authoring
{
    //! Game settings for Models.
    // [GenerateAuthoringComponent]
    public struct LightsSettings : IComponentData
    {
        public byte lowestLight;        // 1
        public byte brightestBright;    // 16
        public byte sunlight;           // 14
        public int maxGrassSunDivision; // 3
        public bool heightOnlySunlight; // true for now
        public bool disableSunlight;
        public byte moonlight;          // 2
        public byte rainLight;          // 9
        public byte torchLight;         // 12
        public bool disableTorchLight;  // 
        public byte airDecreaseRate;    // 2
        public byte waterDecreaseRate;  // 3
        public int propogatesPerFrame;  // 1200
        public bool disableGrassSun;
        public bool disableVoxelLighting;
        public bool disableSmoothLighting;
        public bool disableLightingPropogation;
        public bool disableCharacterLighting;
    }

    public class LightsSettingsAuthoring : MonoBehaviour
    {
        public byte lowestLight;        // 1
        public byte brightestBright;    // 16
        public byte sunlight;           // 14
        public int maxGrassSunDivision; // 3
        public bool heightOnlySunlight; // true for now
        public bool disableSunlight;
        public byte moonlight;          // 2
        public byte rainLight;          // 9
        public byte torchLight;         // 12
        public bool disableTorchLight;  // 
        public byte airDecreaseRate;    // 2
        public byte waterDecreaseRate;  // 3
        public int propogatesPerFrame;  // 1200
        public bool disableGrassSun;
        public bool disableVoxelLighting;
        public bool disableSmoothLighting;
        public bool disableLightingPropogation;
        public bool disableCharacterLighting;
    }

    public class LightsSettingsAuthoringBaker : Baker<LightsSettingsAuthoring>
    {
        public override void Bake(LightsSettingsAuthoring authoring)
        {
            AddComponent(new LightsSettings
            {
                lowestLight = authoring.lowestLight,
                brightestBright = authoring.brightestBright
            });       
        }
    }
}