using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Voxels.Lighting
{
	//! Updates character lighting when moving around.
	/**
	*	\todo update ChunkLink in another system, when translation updates, push chunk to recheck links
	*	\todo Make Non Dynamic - Push these updates how from chunk builder as components to update Minivox lights
	*/
	[BurstCompile, UpdateInGroup(typeof(VoxelLightsSystemGroup))]
    public partial class EntityLightingUpdateSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery processQuery;
		private EntityQuery voxesQuery;
		private EntityQuery chunksQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			voxesQuery = GetEntityQuery(
				ComponentType.ReadOnly<Planet>(),
				ComponentType.ReadOnly<Vox>(),
				ComponentType.Exclude<DestroyEntity>());
			chunksQuery = GetEntityQuery(
				ComponentType.ReadOnly<PlanetChunk>(),
				ComponentType.ReadOnly<ChunkLights>(),
				ComponentType.Exclude<InitializeEntity>(),
				ComponentType.Exclude<DestroyEntity>());
			RequireForUpdate(processQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            if (VoxelManager.instance.voxelSettings.disableCharacterLighting)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var worldEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunkLinks2 = GetComponentLookup<ChunkLinks>(true);
            var chunkDimensions = GetComponentLookup<ChunkDimensions>(true);
            var voxScales = GetComponentLookup<VoxScale>(true);
			worldEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            var chunkLights = GetComponentLookup<ChunkLights>(true);
			chunkEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, DeadEntity, EntityBusy>()
				.WithNone<InitializeEntity, UpdateEntityLighting>()
				.WithChangeFilter<Translation>()
				.ForEach((Entity e, int entityInQueryIndex, ref EntityLighting entityLighting, ref ChunkLightingLink chunkLightingLink, in Translation translation, in VoxLink voxLink) =>
			{
				if (!voxScales.HasComponent(voxLink.vox))
				{
					return;
				}
				var voxScale = voxScales[voxLink.vox].scale;
				var voxelPosition = VoxelUtilities.GetVoxelPosition(translation.Value, voxScale);
				var isUpdateLighting = false;
				if (chunkLightingLink.lastLightPosition != voxelPosition)
				{
					chunkLightingLink.lastLightPosition = voxelPosition;
					isUpdateLighting = true;
				}
				if (!chunkDimensions.HasComponent(voxLink.vox))
				{
					return;
				}
				var voxelDimensions = chunkDimensions[voxLink.vox].voxelDimensions;
				var chunkPosition = new int3(-66666, 66666, 66666); 
				if (chunkPositions.HasComponent(chunkLightingLink.chunk))
				{
					chunkPosition = chunkPositions[chunkLightingLink.chunk].position;
				}
				var characterChunkPosition = VoxelUtilities.GetChunkPosition(voxelPosition, voxelDimensions);
				if (characterChunkPosition != chunkPosition)
				{
					chunkPosition = characterChunkPosition;
					isUpdateLighting = true;
					chunkLightingLink.wasChunkBuilding = 0;
					// Set new chunk
					var chunkLinks = chunkLinks2[voxLink.vox];
					Entity chunkEntity;
					if (chunkLinks.chunks.TryGetValue(characterChunkPosition, out chunkEntity) && !HasComponent<EdgeChunk>(chunkEntity))
					{
						chunkLightingLink.chunk = chunkEntity;
						chunkLightingLink.chunkPosition = characterChunkPosition;
					}
					else
					{
						return;	// building now, so just skip
					}
				}
				if (chunkLightingLink.wasChunkBuilding == 0)
				{
					if (HasComponent<EntityBusy>(chunkLightingLink.chunk))
					{
						chunkLightingLink.wasChunkBuilding = 1;
						return;	// building now, so just skip
					}
				}
				else if (chunkLightingLink.wasChunkBuilding == 1)
				{
					if (!HasComponent<EntityBusy>(chunkLightingLink.chunk))
					{
						chunkLightingLink.wasChunkBuilding = 0;
						isUpdateLighting = true;
					}
				}
				if (isUpdateLighting && chunkLights.HasComponent(chunkLightingLink.chunk))
				{
					var voxelPosition2 = VoxelUtilities.GetLocalPosition(voxelPosition, chunkPosition, voxelDimensions);
					var chunkLights2 = chunkLights[chunkLightingLink.chunk];
					var voxelIndex = VoxelUtilities.GetVoxelArrayIndex(voxelPosition2, voxelDimensions);
					if (voxelIndex >= 0 && voxelIndex < chunkLights2.lights.Length)
					{
						var lightValue = chunkLights2.lights[voxelIndex];
						if (lightValue != entityLighting.lightValue)
						{
							entityLighting.lightValue = lightValue;
							PostUpdateCommands.AddComponent(entityInQueryIndex, e, new UpdateEntityLighting(lightValue));
						}
					}
				}
            })	.WithReadOnly(chunkLinks2)
				.WithReadOnly(chunkDimensions)
				.WithReadOnly(voxScales)
				.WithReadOnly(chunkPositions)
				.WithReadOnly(chunkLights)
				.ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}