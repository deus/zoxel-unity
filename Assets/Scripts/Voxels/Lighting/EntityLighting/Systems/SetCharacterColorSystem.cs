/*using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
// todo: update ChunkLink in another system, when translation updates, push chunk to recheck links
// todo: Push these updates how from chunk builder as components to update lights

namespace Zoxel.Voxels.Lighting
{
	[BurstCompile, UpdateInGroup(typeof(VoxelLightsSystemGroup))]
    public partial class SetCharacterColorSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithNone<InitializeEntity, DestroyEntity>()
				.ForEach((Entity e, int entityInQueryIndex, in ChunkRenderLinks chunkRenderLinks, in SetCharacterColor setCharacterColor) =>
			{
				for (int i = 0; i < chunkRenderLinks.chunkRenders.Length; i++)
				{
					var chunkRenderEntity = chunkRenderLinks.chunkRenders[i];
					if (HasComponent<ChunkRender>(chunkRenderEntity))
					{
						PostUpdateCommands.AddComponent(entityInQueryIndex, chunkRenderEntity, setCharacterColor);
					}
				}
				PostUpdateCommands.RemoveComponent<SetCharacterColor>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
			Dependency = Entities
				.WithNone<InitializeEntity, DestroyEntity>()
				.WithAll<ChunkRender>()
				.ForEach((Entity e, int entityInQueryIndex, ref MaterialBaseColor materialBaseColor, in SetCharacterColor setCharacterColor) =>
			{
				var colorFloat3 = setCharacterColor.color.ToFloat3();
				var materialColor = materialBaseColor.Value;
				materialColor.x = colorFloat3.x;
				materialColor.y = colorFloat3.y;
				materialColor.z = colorFloat3.z;
				materialBaseColor.Value = materialColor;
				PostUpdateCommands.RemoveComponent<SetCharacterColor>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}*/