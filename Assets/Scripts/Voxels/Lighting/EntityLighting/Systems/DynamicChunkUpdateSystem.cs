using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Voxels.Lighting
{
	//! Updates charactr lighting when moving around
	/**
    *   Dynamically checks position everyu frame to see if moved into new chunk.
	*	\todo Make Non Dynamic - Push these updates how from chunk builder as components to update Minivox lights.
	*/
	[BurstCompile, UpdateInGroup(typeof(VoxelLightsSystemGroup))]
    public partial class DynamicChunkUpdateSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity chunkCharacterMovedPrefab;
		private EntityQuery processQuery;
		private EntityQuery voxesQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var chunkCharacterSpawnedArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(ChunkCharacterMoved),
                typeof(GenericEvent));
            chunkCharacterMovedPrefab = EntityManager.CreateEntity(chunkCharacterSpawnedArchetype);
			voxesQuery = GetEntityQuery(
				ComponentType.ReadOnly<Planet>(),
				ComponentType.ReadOnly<Vox>(),
				ComponentType.ReadOnly<ChunkLinks>(),
				ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
			var chunkCharacterMovedPrefab = this.chunkCharacterMovedPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var worldEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var chunkLinks2 = GetComponentLookup<ChunkLinks>(true);
            var chunkDimensions = GetComponentLookup<ChunkDimensions>(true);
            var voxScales = GetComponentLookup<VoxScale>(true);
			worldEntities.Dispose();
            var chunkEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunks = GetComponentLookup<Chunk>(true);
			chunkEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				// .WithChangeFilter<Translation>()
                .WithNone<DestroyEntity, DeadEntity, InitializeSaver>()
                .WithNone<EntityBusy, EntityLoading, NewCharacter>()
				.ForEach((Entity e, int entityInQueryIndex, ref ChunkLink chunkLink, ref DynamicChunkLink dynamicChunkLink, in Translation translation, in VoxLink voxLink) =>
			{
				if (!voxScales.HasComponent(voxLink.vox) || !chunkLinks2.HasComponent(voxLink.vox))
				{
					return;
				}
                if (!chunks.HasComponent(chunkLink.chunk))
                {
                    dynamicChunkLink.chunkPosition = new int3(-6666, -6666, -6666);
                }
				var voxScale = voxScales[voxLink.vox].scale;
				var voxelDimensions = chunkDimensions[voxLink.vox].voxelDimensions;
				var voxelPosition = VoxelUtilities.GetVoxelPosition(translation.Value, voxScale);
				var characterChunkPosition = VoxelUtilities.GetChunkPosition(voxelPosition, voxelDimensions);
				if (characterChunkPosition != dynamicChunkLink.chunkPosition)
				{
					var chunkLinks = chunkLinks2[voxLink.vox];
					Entity chunkEntity;
					if (!chunkLinks.chunks.TryGetValue(characterChunkPosition, out chunkEntity))
					{
						return;
					}
					var oldChunk = chunkLink.chunk;
					chunkLink.chunk = chunkEntity;
					dynamicChunkLink.chunkPosition = characterChunkPosition;
					// UnityEngine.Debug.LogError("Set ChunkLink to: " + chunkEntity.Index);
					if (!HasComponent<PlayerCharacter>(e))
					{
						// Create MoveChunkCharacter event entity!
						var movedEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, chunkCharacterMovedPrefab);
						PostUpdateCommands.SetComponent(entityInQueryIndex, movedEntity, new ChunkCharacterMoved(e, oldChunk, chunkEntity));
					}
				}
            })	.WithReadOnly(chunkLinks2)
				.WithReadOnly(chunkDimensions)
				.WithReadOnly(voxScales)
				.WithReadOnly(chunks)
				.ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}