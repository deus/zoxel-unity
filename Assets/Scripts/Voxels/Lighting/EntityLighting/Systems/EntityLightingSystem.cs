using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Rendering;

namespace Zoxel.Voxels.Lighting
{
	//! Updates MaterialBaseColor with a lighting value!
	[BurstCompile, UpdateInGroup(typeof(VoxelLightsSystemGroup))]
    public partial class EntityLightingSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery processQuery;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			RequireForUpdate(processQuery);
        }

        [BurstCompile]
		protected override void OnUpdate()
		{
			SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<UpdateEntityLighting>(processQuery);
			Entities
				.WithStoreEntityQueryInField(ref processQuery)
				.WithNone<DestroyEntity>()
				.ForEach((ref EntityLighting entityLighting, in UpdateEntityLighting updateEntityLighting) =>
			{
				entityLighting.lightValue = updateEntityLighting.lightValue;
			}).ScheduleParallel();
			Entities
				.WithNone<DestroyEntity, InitializeEntity, BaseOutlineColor>()
				.ForEach((ref MaterialBaseColor materialBaseColor, in UpdateEntityLighting updateEntityLighting) =>
			{
				float brightness = LightingCurve.lightValues[updateEntityLighting.lightValue];
				materialBaseColor.Value = new float4(brightness, brightness, brightness, materialBaseColor.Value.w);
				// UnityEngine.Debug.LogError("UpdateEntityLighting MaterialBaseColor " + updateEntityLighting.lightValue);
			}).ScheduleParallel();
		}
	}
}