using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels.Lighting
{
	//! Updates Vox's Chunk Renders!
	[BurstCompile, UpdateInGroup(typeof(VoxelLightsSystemGroup))]
    public partial class VoxLightingSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithNone<InitializeEntity, DestroyEntity>()
				.WithNone<SpawnVoxChunk, OnChunksSpawned, GenerateVox>()
				.ForEach((Entity e, int entityInQueryIndex, in Vox vox, in UpdateEntityLighting updateEntityLighting) =>
			{
				PostUpdateCommands.RemoveComponent<UpdateEntityLighting>(entityInQueryIndex, e);
				for (int i = 0; i < vox.chunks.Length; i++)
				{
					PostUpdateCommands.AddComponent(entityInQueryIndex, vox.chunks[i], updateEntityLighting);
				}
				// UnityEngine.Debug.LogError("Updating: " + vox.chunks.Length + " chunks.");
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}