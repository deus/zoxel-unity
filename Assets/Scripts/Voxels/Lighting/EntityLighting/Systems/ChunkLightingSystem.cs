using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels.Lighting
{
	//! Updates Lighting for hunk's ChunkRenderLinks's.
	[BurstCompile, UpdateInGroup(typeof(VoxelLightsSystemGroup))]
    public partial class ChunkLightingSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithNone<InitializeEntity, DestroyEntity, EntityBusy>()
				.WithNone<ChunkSpawnRenders, OnChunkRendersSpawned, GenerateVox>()
				.ForEach((Entity e, int entityInQueryIndex, in ChunkRenderLinks chunkRenderLinks, in UpdateEntityLighting updateEntityLighting) =>
			{
				PostUpdateCommands.RemoveComponent<UpdateEntityLighting>(entityInQueryIndex, e);
				for (int i = 0; i < chunkRenderLinks.chunkRenders.Length; i++)
				{
					PostUpdateCommands.AddComponent(entityInQueryIndex, chunkRenderLinks.chunkRenders[i], updateEntityLighting);
				}
				// UnityEngine.Debug.LogError("Updating: " + chunkRenderLinks.chunkRenders.Length + " chunk renders.");
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}