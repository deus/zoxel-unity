using Unity.Entities;

namespace Zoxel.Voxels.Lighting
{
	//! Updates lighting on a Entity.
	public struct UpdateEntityLighting : IComponentData
	{
		public byte lightValue;

		public UpdateEntityLighting(byte lightValue)
		{
			this.lightValue = lightValue;
		}
	}
}