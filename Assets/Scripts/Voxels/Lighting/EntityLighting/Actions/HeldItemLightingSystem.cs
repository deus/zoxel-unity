
using Unity.Entities;
using Unity.Burst;
using Zoxel.Actions;

namespace Zoxel.Voxels.Lighting
{
	//! Updates HeldActionItemLink's Lighting!
	[BurstCompile, UpdateInGroup(typeof(VoxelLightsSystemGroup))]
    public partial class HeldItemLightingSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithNone<InitializeEntity, DestroyEntity>()
				.ForEach((Entity e, int entityInQueryIndex, in HeldActionItemLink heldActionItemLink, in UpdateEntityLighting updateEntityLighting) =>
			{
                if (heldActionItemLink.item.Index > 0)
                {
					PostUpdateCommands.AddComponent(entityInQueryIndex,heldActionItemLink.item, updateEntityLighting);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}