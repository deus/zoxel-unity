using Unity.Entities;
using Unity.Burst;
using Zoxel.Skeletons;

namespace Zoxel.Voxels.Lighting
{
	//! Updates HeldActionItemLink's Lighting!
	[BurstCompile, UpdateInGroup(typeof(VoxelLightsSystemGroup))]
    public partial class HeldWeaponsLightingSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithNone<InitializeEntity, DestroyEntity>()
				.ForEach((Entity e, int entityInQueryIndex, in HeldWeaponLinks heldWeaponLinks, in UpdateEntityLighting updateEntityLighting) =>
			{
                if (heldWeaponLinks.heldWeaponRightHand.Index > 0)
                {
					PostUpdateCommands.AddComponent(entityInQueryIndex, heldWeaponLinks.heldWeaponRightHand, updateEntityLighting);
                }
                if (heldWeaponLinks.heldWeaponLeftHand.Index > 0)
                {
					PostUpdateCommands.AddComponent(entityInQueryIndex, heldWeaponLinks.heldWeaponLeftHand, updateEntityLighting);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}