﻿using Unity.Entities;

namespace Zoxel.Voxels.Lighting
{
    //! A systemgroup that manages lighting.
    [UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxelLightsSystemGroup : ComponentSystemGroup { }
}