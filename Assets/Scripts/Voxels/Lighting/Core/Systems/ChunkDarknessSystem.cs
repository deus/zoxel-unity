using Unity.Entities;
using Unity.Burst;
using Zoxel.Voxels.Lighting.Authoring;

namespace Zoxel.Voxels.Lighting
{
    //! Voxels that emit light will set voxelLights value during build stp.
    [BurstCompile, UpdateInGroup(typeof(VoxelLightsSystemGroup))]
    public partial class ChunkDarknessSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<LightsSettings>();
        }

        [BurstCompile]
		protected override void OnUpdate()
		{
            var lightsSettings = GetSingleton<LightsSettings>();
			var lowestLight = lightsSettings.lowestLight;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithNone<UpdateChunkNeighbors>()
                .WithAll<ChunkDarknessBuilder>()
                .ForEach((Entity e, int entityInQueryIndex, ref ChunkLights chunkLights) =>
			{
                PostUpdateCommands.RemoveComponent<ChunkDarknessBuilder>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<ChunkSunlightBuilder>(entityInQueryIndex, e);
                chunkLights.SetAllLights(lowestLight);
                // PostUpdateCommands2.AddComponentForEntityQuery<ChunkSunlightBuilder>(processQuery);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}