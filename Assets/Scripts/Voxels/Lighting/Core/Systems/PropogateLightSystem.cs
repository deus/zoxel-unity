using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels.Lighting.Authoring;

namespace Zoxel.Voxels.Lighting
{
	//! Propogates lights accross chunks
	/**
	*	\todo Streaming can lag this by 30ms lag spikes
	*/
	[UpdateAfter(typeof(TorchLightSystem))]
    [BurstCompile, UpdateInGroup(typeof(VoxelLightsSystemGroup))]
    public partial class PropogateLightSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery processQuery;
		private EntityQuery chunksQuery;
		private EntityQuery voxesQuery;
		private EntityQuery voxelQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			voxesQuery = GetEntityQuery(
				ComponentType.ReadOnly<Vox>(),
				ComponentType.ReadOnly<VoxelLinks>());
			chunksQuery = GetEntityQuery(
				ComponentType.ReadOnly<Chunk>(),
				ComponentType.ReadWrite<ChunkLights>(),
				ComponentType.Exclude<DestroyEntity>());
			voxelQuery = GetEntityQuery(ComponentType.ReadOnly<VoxelMeshType>());
            RequireForUpdate<LightsSettings>();
            RequireForUpdate(processQuery);
            RequireForUpdate(voxesQuery);
            RequireForUpdate(chunksQuery);
            RequireForUpdate(voxelQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var lightsSettings = GetSingleton<LightsSettings>();
			var propogatesPerFrame = lightsSettings.propogatesPerFrame; // 5000;
			var disableLightingPropogation = lightsSettings.disableLightingPropogation;
			var lowestLight = lightsSettings.lowestLight;
			var lesserLight = lightsSettings.airDecreaseRate;
			var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxelLinks = GetComponentLookup<VoxelLinks>(true);
			voxEntities.Dispose();
            var voxelEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxelMeshTypes = GetComponentLookup<VoxelMeshType>(true);
			voxelEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var chunkLights2 = GetComponentLookup<ChunkLights>(false);
            var chunks = GetComponentLookup<Chunk>(true);
			chunkEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, UpdateChunkNeighbors>()
                .WithAll<PropogateLights>()
				.ForEach((Entity e, int entityInQueryIndex, ref PropogateLights propogateLights, in Chunk chunk, in VoxLink voxLink, in ChunkNeighbors chunkNeighbors, in ChunkPosition chunkPosition) =>
			{
				if (disableLightingPropogation)
				{
					PostUpdateCommands.RemoveComponent<PropogateLights>(entityInQueryIndex, e);
					PostUpdateCommands.AddComponent<ChunkBuilderComplete>(entityInQueryIndex, e);
					return;
				}
				var voxelDimensions = chunk.voxelDimensions;
				// if already finished, keep waiting for neighbors
				if (propogateLights.voxelArrayIndex == -1)
				{
					return;
				}
                if (!voxelLinks.HasComponent(voxLink.vox))
                {
                    return;
                }
				var voxelEntities2 = voxelLinks[voxLink.vox].voxels;
				var voxelMeshTypes2 = new NativeArray<byte>(voxelEntities2.Length, Allocator.Temp);
				for (int i = 0; i < voxelMeshTypes2.Length; i++)
				{
					var voxelEntity = voxelEntities2[i];
					if (voxelMeshTypes.HasComponent(voxelEntity))
					{
						if (voxelMeshTypes[voxelEntity].meshType == MeshType.Block)
						{
							voxelMeshTypes2[i] = 1;
						}
						else
						{
							voxelMeshTypes2[i] = 0;
						}
					}
				}
				var chunkLights = chunkLights2[e];
				if (chunkLights.lights.Length == 0)
				{
					PostUpdateCommands.RemoveComponent<PropogateLights>(entityInQueryIndex, e);
					PostUpdateCommands.AddComponent<ChunkBuilderComplete>(entityInQueryIndex, e);
					return;
				}
				var count = 0;
				byte lightValue;
				byte voxelType;
				var voxelArrayIndex = 0;
				/*if (chunkPosition.position == new int3(-1, 9, 1))
				{
					UnityEngine.Debug.LogError(e.Index + " - " + elapsedTime + " - startPosition: " + startPosition + ", voxelArrayIndex: " + propogateLights.voxelArrayIndex);
				}*/
				var hasInit = propogateLights.voxelArrayIndex == 0;
				int3 localPosition;
				for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
				{
					for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
					{
						for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
						{
							//! \todo Find a bttr way to rsum this loop
							if (!hasInit) // voxelArrayIndex < propogateLights.voxelArrayIndex)
							{
								hasInit = true;
								localPosition = propogateLights.localPosition;
								voxelArrayIndex = propogateLights.voxelArrayIndex;
								//voxelArrayIndex++;
								//continue;
							}
							lightValue = chunkLights.lights[voxelArrayIndex];
							if (lightValue != lowestLight)
							{
								if (count >= propogatesPerFrame)
								{
									propogateLights.voxelArrayIndex = voxelArrayIndex;
									propogateLights.localPosition = localPosition;
									/*if (chunkPosition.position == new int3(-1, 9, 1))
									{
										UnityEngine.Debug.LogError(e.Index + " - " + elapsedTime + " - Skipping at: " + count + ", Index: " + voxelArrayIndex
											+ " at " + localPosition);
									}*/
									return;
								}
								voxelType = chunk.voxels[voxelArrayIndex];
								if (voxelType == 0 || voxelMeshTypes2[voxelType - 1] == 0)
								{
									InitialPropogateLight(in voxelMeshTypes2, in chunks, in chunkLights2, e, in chunk, in chunkNeighbors, ref chunkLights,
										localPosition, lightValue, voxelDimensions, lowestLight, lesserLight, ref count);
								}
							}
							voxelArrayIndex++;
							/*if (chunkPosition.position == new int3(-1, 9, 1))
							{
								UnityEngine.Debug.LogError("	- voxelArrayIndex: " + voxelArrayIndex + ", position - " + localPosition);
							}*/
						}
					}
				}
				propogateLights.voxelArrayIndex = -1;
				/*if (chunkPosition.position == new int3(-1, 9, 1))
				{
					UnityEngine.Debug.LogError(e.Index + " - " + elapsedTime + " - RemoveComponent - Count: " + count + ", voxelArrayIndex: " + voxelArrayIndex);
				}*/
				// UnityEngine.Debug.LogError("Final Propogation Light Count: " + count);
				// before moving on, check if surrounding chunks have finished
				//PostUpdateCommands.RemoveComponent<PropogateLights>(entityInQueryIndex, e);
				//PostUpdateCommands.AddComponent<ChunkBuilderComplete>(entityInQueryIndex, e);
				#if DEBUG_VOXEL_PLACEMENT
				UnityEngine.Debug.LogError(e.Index + " - Voxel Placement - Ended ChunkLightsBuilderState.PropogateLightSides: " + elapsedTime);
				#endif
				voxelMeshTypes2.Dispose();
			})  .WithReadOnly(voxelLinks)
                .WithReadOnly(voxelMeshTypes)
                .WithNativeDisableContainerSafetyRestriction(chunkLights2)
                .WithReadOnly(chunks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
		
		private static void InitialPropogateLight(in NativeArray<byte> voxelMeshTypes2, in ComponentLookup<Chunk> chunks, in ComponentLookup<ChunkLights> chunkLights2,
			Entity chunkEntity, in Chunk chunk, in ChunkNeighbors chunkNeighbors, ref ChunkLights chunkLights, 
			int3 localPosition, byte lightValue, int3 voxelDimensions, byte lowestLight, byte lesserLight, ref int count)
		{
			PropogateLight(in voxelMeshTypes2, in chunks, in chunkLights2, chunkEntity, in chunk, in chunkNeighbors, ref chunkLights,
				lightValue, VoxelSide.Left, localPosition, voxelDimensions, lowestLight, lesserLight, ref count);
			PropogateLight(in voxelMeshTypes2, in chunks, in chunkLights2, chunkEntity, in chunk, in chunkNeighbors, ref chunkLights,
				lightValue, VoxelSide.Right, localPosition, voxelDimensions, lowestLight, lesserLight, ref count);
			PropogateLight(in voxelMeshTypes2, in chunks, in chunkLights2, chunkEntity, in chunk, in chunkNeighbors, ref chunkLights,
				lightValue, VoxelSide.Down, localPosition, voxelDimensions, lowestLight, lesserLight, ref count);
			PropogateLight(in voxelMeshTypes2, in chunks, in chunkLights2, chunkEntity, in chunk, in chunkNeighbors, ref chunkLights,
				lightValue, VoxelSide.Up, localPosition, voxelDimensions, lowestLight, lesserLight, ref count);
			PropogateLight(in voxelMeshTypes2, in chunks, in chunkLights2, chunkEntity, in chunk, in chunkNeighbors, ref chunkLights,
				lightValue, VoxelSide.Backward, localPosition, voxelDimensions, lowestLight, lesserLight, ref count);
			PropogateLight(in voxelMeshTypes2, in chunks, in chunkLights2, chunkEntity, in chunk, in chunkNeighbors, ref chunkLights,
				lightValue, VoxelSide.Forward, localPosition, voxelDimensions, lowestLight, lesserLight, ref count);
		}
		
		private static void PropogateLight(in NativeArray<byte> voxelMeshTypes2, in ComponentLookup<Chunk> chunks, in ComponentLookup<ChunkLights> chunkLights2,
			Entity chunkEntity, in Chunk chunk, in ChunkNeighbors chunkNeighbors, ref ChunkLights chunkLights, 
			byte lightValue, byte direction, int3 localPosition, int3 voxelDimensions, byte lowestLight, byte lesserLight, ref int count)
		{
			count++;
			// var localPosition2 = new int3(localPosition);
			if (direction == VoxelSide.Left)
			{
				localPosition.x -= 1;
			}
			else if (direction == VoxelSide.Right)
			{
				localPosition.x += 1;
			}
			else if (direction == VoxelSide.Down)
			{
				localPosition.y -= 1;
			}
			else if (direction == VoxelSide.Up)
			{
				localPosition.y += 1;
			}
			else if (direction == VoxelSide.Backward)
			{
				localPosition.z -= 1;
			}
			else if (direction == VoxelSide.Forward)
			{
				localPosition.z += 1;
			}
			var chunkEntity2 = chunkEntity;
			var chunk2 = chunk;
			var chunkLights3 = chunkLights;
			if (!VoxelUtilities.InBounds(localPosition, voxelDimensions))
			{
				chunkEntity2 = chunkNeighbors.GetChunkAdjacent(ref localPosition, voxelDimensions);
				if (chunkEntity2.Index == 0 || !chunks.HasComponent(chunkEntity2))
				{
					return;
				}
				chunk2 = chunks[chunkEntity2];
				if (chunk2.voxels.Length == 0)
				{
					return;
				}
				chunkLights3 = chunkLights2[chunkEntity2];
			}
			// can actually propogate into this voxel - If Block, return!
			var voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
			var voxelIndex = chunk2.voxels[voxelArrayIndex];
			if (voxelIndex != 0)
			{
				var voxelIndexMinusAir = voxelIndex - 1;
				if (voxelIndexMinusAir >= voxelMeshTypes2.Length || voxelMeshTypes2[voxelIndexMinusAir] == 1)
				{
					return;
				}
			}
			// Next Decrease Light
			if (lightValue > lowestLight + lesserLight)
			{
				lightValue = (byte) (lightValue - lesserLight);
			}
			else
			{
				lightValue = lowestLight;
			}
			// Now set light at value
			// And No need to propogate lowest light value
			if (lightValue > chunkLights3.lights[voxelArrayIndex])
			{
				chunkLights3.lights[voxelArrayIndex] = lightValue;
			}
			else
			{
				return;	// failed to set chunk lights, outside of bounds.
			}
			if (lightValue == lowestLight)
			{
				return;	// failed to set chunk lights, outside of bounds.
			}
			//UnityEngine.Profiling.Profiler.EndSample();
			// now propogate new value to surrounding ones, maybe minus the previous direction, so pass through direction too
			if (direction != VoxelSide.Left)
			{
				PropogateLight(in voxelMeshTypes2, in chunks, in chunkLights2, chunkEntity2, in chunk2, in chunkNeighbors, ref chunkLights3,
					lightValue, VoxelSide.Right, localPosition, voxelDimensions, lowestLight, lesserLight, ref count);
			}
			if (direction != VoxelSide.Right)
			{
				PropogateLight(in voxelMeshTypes2, in chunks, in chunkLights2, chunkEntity2, in chunk2, in chunkNeighbors, ref chunkLights3,
					lightValue, VoxelSide.Left, localPosition, voxelDimensions, lowestLight, lesserLight, ref count);
			}
			if (direction != VoxelSide.Down)
			{
				PropogateLight(in voxelMeshTypes2, in chunks, in chunkLights2, chunkEntity2, in chunk2, in chunkNeighbors, ref chunkLights3,
					lightValue, VoxelSide.Up, localPosition, voxelDimensions, lowestLight, lesserLight, ref count);
			}
			if (direction != VoxelSide.Up)
			{
				PropogateLight(in voxelMeshTypes2, in chunks, in chunkLights2, chunkEntity2, in chunk2, in chunkNeighbors, ref chunkLights3,
					lightValue, VoxelSide.Down, localPosition, voxelDimensions, lowestLight, lesserLight, ref count);
			}
			if (direction != VoxelSide.Backward)
			{
				PropogateLight(in voxelMeshTypes2, in chunks, in chunkLights2, chunkEntity2, in chunk2, in chunkNeighbors, ref chunkLights3,
					lightValue, VoxelSide.Forward, localPosition, voxelDimensions, lowestLight, lesserLight, ref count);
			}
			if (direction != VoxelSide.Forward)
			{
				PropogateLight(in voxelMeshTypes2, in chunks, in chunkLights2, chunkEntity2, in chunk2, in chunkNeighbors, ref chunkLights3,
					lightValue, VoxelSide.Backward, localPosition, voxelDimensions, lowestLight, lesserLight, ref count);
			}
		}
    }
}