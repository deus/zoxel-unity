using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Voxels.Lighting.Authoring;
using Unity.Burst.Intrinsics;

namespace Zoxel.Voxels.Lighting
{
    //! Generates basic lighting using vertex colours for our voxel meshes.
    [UpdateAfter(typeof(CalculateBoundsSystem))]
	[BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
	public partial class ChunkMeshLightsBasicSystem : SystemBase
    {
        // public static int chunksCount;
		private EntityQuery processQuery;
		private EntityQuery chunksQuery;
		private EntityQuery voxesQuery;
		private EntityQuery voxelQuery;

        protected override void OnCreate()
        {
			processQuery = GetEntityQuery(
                ComponentType.ReadWrite<TerrainMesh>(),
                ComponentType.ReadWrite<ChunkRenderBuilder>(), 
                ComponentType.ReadOnly<ChunkRender>(),
                ComponentType.ReadOnly<ChunkSides>(),
                ComponentType.ReadOnly<ChunkLink>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            chunksQuery = GetEntityQuery(
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkLights>(),
                ComponentType.ReadOnly<ChunkNeighbors>(),
                ComponentType.Exclude<DestroyEntity>());
			voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxelLinks>());
			voxelQuery = GetEntityQuery(ComponentType.ReadOnly<VoxelMeshType>());
            RequireForUpdate(processQuery);
            RequireForUpdate<LightsSettings>();
		}
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var lightsSettings = GetSingleton<LightsSettings>();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxelLinks = GetComponentLookup<VoxelLinks>(true);
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var chunks = GetComponentLookup<Chunk>(true);
            var chunkLights2 = GetComponentLookup<ChunkLights>(true);
            var chunkNeighbors2 = GetComponentLookup<ChunkNeighbors>(true);
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxelMeshTypes = GetComponentLookup<VoxelMeshType>(true);
			Dependency = new ChunkRenderBasicLightsJob
            { 
                disableVoxelLighting = lightsSettings.disableVoxelLighting,
                brightestBright = (float) lightsSettings.brightestBright,
                chunkMeshHandle = GetComponentTypeHandle<TerrainMesh>(false),
                chunkRendererBuilderHandle = GetComponentTypeHandle<ChunkRenderBuilder>(false),
                chunkSidesHandle = GetComponentTypeHandle<ChunkSides>(true),
                voxLinksHandle = GetComponentTypeHandle<VoxLink>(true),
                chunkLinksHandle = GetComponentTypeHandle<ChunkLink>(true),
				voxEntities = voxEntities,
				voxelLinks = voxelLinks,
				voxelEntities = voxelEntities,
				voxelMeshTypes = voxelMeshTypes,
                chunkEntities = chunkEntities,
                chunks = chunks,
                chunkNeighbors2 = chunkNeighbors2,
                chunkLights2 = chunkLights2
            }.ScheduleParallel(processQuery, Dependency);
		}

        [BurstCompile]
		struct ChunkRenderBasicLightsJob : IJobChunk 
		{
			[ReadOnly] public bool disableVoxelLighting;
            [ReadOnly] public float brightestBright;
            // chunk data
            public ComponentTypeHandle<TerrainMesh> chunkMeshHandle;
            public ComponentTypeHandle<ChunkRenderBuilder> chunkRendererBuilderHandle;
            [ReadOnly] public ComponentTypeHandle<ChunkSides> chunkSidesHandle;
            [ReadOnly] public ComponentTypeHandle<VoxLink> voxLinksHandle;
            [ReadOnly] public ComponentTypeHandle<ChunkLink> chunkLinksHandle;
            // injected data
            [ReadOnly, DeallocateOnJobCompletion] public NativeArray<Entity> voxEntities;
            [ReadOnly, NativeDisableParallelForRestriction] public ComponentLookup<VoxelLinks> voxelLinks;
            [ReadOnly, DeallocateOnJobCompletion] public NativeArray<Entity> voxelEntities;
            [ReadOnly, NativeDisableParallelForRestriction] public ComponentLookup<VoxelMeshType> voxelMeshTypes;
            [ReadOnly, DeallocateOnJobCompletion] public NativeArray<Entity> chunkEntities;
			[ReadOnly, NativeDisableParallelForRestriction] public ComponentLookup<Chunk> chunks;
            [ReadOnly, NativeDisableParallelForRestriction] public ComponentLookup<ChunkNeighbors> chunkNeighbors2;
            [ReadOnly, NativeDisableParallelForRestriction] public ComponentLookup<ChunkLights> chunkLights2;

            public void Execute(in ArchetypeChunk chunk, int unfilteredChunkIndex, bool useEnabledMask, in v128 chunkEnabledMask)
            // public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
            {
                if (voxelEntities.Length == 0 || voxEntities.Length == 0)
                {
                    return;
                }
                var chunkMeshes = chunk.GetNativeArray(chunkMeshHandle);
                var chunkRenderBuilders = chunk.GetNativeArray(chunkRendererBuilderHandle);
                var chunkSides = chunk.GetNativeArray(chunkSidesHandle);
                var voxLinks = chunk.GetNativeArray(voxLinksHandle);
                var chunkLinks = chunk.GetNativeArray(chunkLinksHandle);
                for (var i = 0; i < chunk.Count; i++)
                {
                    var entityInQueryIndex = i; // + firstEntityIndex;
                    var terrainMesh = chunkMeshes[i];
                    var chunkRenderBuilder = chunkRenderBuilders[i];
                    var chunkSide = chunkSides[i];
                    var voxLink = voxLinks[i];
                    var chunkLink = chunkLinks[i];
                    Process(entityInQueryIndex, ref terrainMesh, ref chunkRenderBuilder, in chunkSide, in voxLink, in chunkLink);
                    chunkMeshes[i] = terrainMesh;
                    chunkRenderBuilders[i] = chunkRenderBuilder;
                }
            }
            
            public void Process(int entityInQueryIndex, ref TerrainMesh terrainMesh, ref ChunkRenderBuilder chunkRenderBuilder,
                in ChunkSides chunkSides, in VoxLink voxLink, in ChunkLink chunkLink)
			{
				if (chunkRenderBuilder.chunkRenderBuildState != (byte) ChunkRenderBuildState.GenerateBasicLights)
                {
                    return;
                }
                chunkRenderBuilder.chunkRenderBuildState = (byte) ChunkRenderBuildState.CompletedChunkRenderBuilder;
                if (disableVoxelLighting)
                {
                    return;
                }
                var chunkLights = chunkLights2[chunkLink.chunk];
                var chunkNeighbors = chunkNeighbors2[chunkLink.chunk];
                var chunk = chunks[chunkLink.chunk];
                var worldVoxelEntities = voxelLinks[voxLink.vox].voxels;
                var voxelMeshTypes2 = new NativeArray<byte>(worldVoxelEntities.Length, Allocator.Temp);
                for (int i = 0; i < voxelMeshTypes2.Length; i++)
                {
                    voxelMeshTypes2[i] = voxelMeshTypes[worldVoxelEntities[i]].meshType;
                }
                ProcessBasicLights(ref terrainMesh, in chunk, in chunkSides, in chunkNeighbors, in chunkLights, in voxelMeshTypes2);
				voxelMeshTypes2.Dispose();
            }

            private void ProcessBasicLights(ref TerrainMesh terrainMesh, in Chunk chunk, in ChunkSides chunkSides, in ChunkNeighbors chunkNeighbors,
                in ChunkLights chunkLights, in NativeArray<byte> voxelMeshTypes2)
            {
                var voxelDimensions = chunk.voxelDimensions;
                byte voxelMetaIndex;
                var voxelIndex = 0;
                var vertIndex = 0;
                int3 localPosition;
                for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                {
                    for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                    {
                        for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
                        {
                            voxelMetaIndex = chunk.voxels[voxelIndex];
                            if (voxelMetaIndex != 0)
                            {
                                voxelMetaIndex--;
                                var modelIndex = (byte) 0;
                                if (voxelMetaIndex >= 0 && voxelMetaIndex < voxelMeshTypes2.Length)
                                {
                                    modelIndex = voxelMeshTypes2[voxelMetaIndex];
                                }
                                if (modelIndex == 0)
                                {
                                    for (var sideIndex = 0; sideIndex < 6; sideIndex++)
                                    {
                                        if (ProcessVoxelLightSideBasic(ref terrainMesh, in chunkLights, in chunkSides, in chunkNeighbors, localPosition, voxelDimensions, sideIndex, vertIndex))
                                        {
                                            vertIndex += 4;
                                        }
                                    }
                                }
                            }
                            voxelIndex++;
                        }
                    }
                }
            }

            private bool ProcessVoxelLightSideBasic(ref TerrainMesh terrainMesh, in ChunkLights chunkLights, in ChunkSides chunkSides, in ChunkNeighbors chunkNeighbors,
                int3 localPosition, int3 voxelDimensions, int sideIndex, int vertIndex)
            {
                var voxelArrayIndexMesh = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
                var canLight = (sideIndex == VoxelSide.Up && chunkSides.GetSideIndex(voxelArrayIndexMesh, VoxelSide.Up) != 0)
                    || (sideIndex == VoxelSide.Down && chunkSides.GetSideIndex(voxelArrayIndexMesh, VoxelSide.Down) != 0)
                    || (sideIndex == VoxelSide.Left && chunkSides.GetSideIndex(voxelArrayIndexMesh, VoxelSide.Left) != 0)
                    || (sideIndex == VoxelSide.Right && chunkSides.GetSideIndex(voxelArrayIndexMesh, VoxelSide.Right) != 0)
                    || (sideIndex == VoxelSide.Backward && chunkSides.GetSideIndex(voxelArrayIndexMesh, VoxelSide.Backward) != 0)
                    || (sideIndex == VoxelSide.Forward && chunkSides.GetSideIndex(voxelArrayIndexMesh, VoxelSide.Forward) != 0);
                if (!canLight)
                {
                    return false;
                }
                byte chunkSideType = 0;
                var voxelArrayIndex = 0;
                byte lightValue = 0;
                // for each corner point, get closest voxel lights
                // E.G. corner point on top, grab surrounding top voxels (x - 1, x, z, z - 1) for top left point
                // combine lights together and set to smoothed
                if (sideIndex == VoxelSide.Down)
                {
                    if (localPosition.y != 0)
                    {
                        voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition.Down(), voxelDimensions);
                    }
                    else
                    {
                        chunkSideType = 1;
                        var otherVoxelIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(localPosition.x, voxelDimensions.y - 1, localPosition.z), voxelDimensions);
                        var otherChunkLights = chunkLights2[chunkNeighbors.chunkDown].lights;
                        if (otherChunkLights.Length != 0)
                        {
                            lightValue = otherChunkLights[otherVoxelIndex];
                        }
                    }
                }
                else if (sideIndex == VoxelSide.Up)
                {
                    if (localPosition.y != voxelDimensions.y - 1)
                    {
                        voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition.Up(), voxelDimensions);
                    }
                    else
                    {
                        chunkSideType = 1;
                        var otherVoxelIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(localPosition.x, 0, localPosition.z), voxelDimensions);
                        var otherChunkLights = chunkLights2[chunkNeighbors.chunkUp].lights;
                        if (otherChunkLights.Length != 0)
                        {
                            lightValue = otherChunkLights[otherVoxelIndex];
                        }
                    }
                }
                // left right
                else if (sideIndex == VoxelSide.Left)
                {
                    if (localPosition.x != 0)
                    {
                        voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition.Left(), voxelDimensions);
                    }
                    else
                    {
                        chunkSideType = 1;
                        var otherVoxelIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(voxelDimensions.x - 1, localPosition.y, localPosition.z), voxelDimensions);
                        var otherChunkLights = chunkLights2[chunkNeighbors.chunkLeft].lights;
                        if (otherChunkLights.Length != 0)
                        {
                            lightValue = otherChunkLights[otherVoxelIndex];
                        }
                    }
                }
                else if (sideIndex == VoxelSide.Right)
                {
                    if (localPosition.x != voxelDimensions.x - 1)
                    {
                        voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition.Right(), voxelDimensions);
                    }
                    else
                    {
                        chunkSideType = 1;
                        var otherVoxelIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(0, localPosition.y, localPosition.z), voxelDimensions);
                        var otherChunkLights = chunkLights2[chunkNeighbors.chunkRight].lights;
                        if (otherChunkLights.Length != 0)
                        {
                            lightValue = otherChunkLights[otherVoxelIndex];
                        }
                    }
                }
                // forward back
                else if (sideIndex == VoxelSide.Backward)
                {
                    if (localPosition.z != 0)
                    {
                        voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition.Backward(), voxelDimensions);
                    }
                    else
                    {
                        chunkSideType = 1;
                        var otherVoxelIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(localPosition.x, localPosition.y, voxelDimensions.z - 1), voxelDimensions);
                        var otherChunkLights = chunkLights2[chunkNeighbors.chunkBack].lights;
                        if (otherChunkLights.Length != 0)
                        {
                            lightValue = otherChunkLights[otherVoxelIndex];
                        }
                    }
                }
                else if (sideIndex == VoxelSide.Forward)
                {
                    if (localPosition.z != voxelDimensions.z - 1)
                    {
                        voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition.Forward(), voxelDimensions);
                    }
                    else
                    {
                        chunkSideType = 1;
                        var otherVoxelIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(localPosition.x, localPosition.y, 0), voxelDimensions);
                        var otherChunkLights = chunkLights2[chunkNeighbors.chunkForward].lights;
                        if (otherChunkLights.Length != 0)
                        {
                            lightValue = otherChunkLights[otherVoxelIndex];
                        }
                    }
                }

                if (chunkSideType == 0)
                {
                    lightValue = chunkLights.lights[voxelArrayIndex];
                }
                // if sides allow it, then add vertex side of model!
                // var brightnessValue = ((int)lightValue) / brightestBright;
                float brightnessValue = LightingCurve.lightValues[lightValue];
                for (var arrayIndex = 0; arrayIndex < 4; arrayIndex++)
                {
                    var vert = terrainMesh.vertices[vertIndex + arrayIndex];
                    // base it on the chunkLights value
                    vert.color = new float3(brightnessValue, brightnessValue, brightnessValue);
                    terrainMesh.vertices[vertIndex + arrayIndex] = vert;
                }
                return true;
            }
        }
    }
}
                /*else if (chunkSideType == 3)
                {
                    lightValue = chunkLights.data.lightsLeft[voxelArrayIndex];
                }
                else if (chunkSideType == 4)
                {
                    lightValue = chunkLights.data.lightsRight[voxelArrayIndex];
                }
                else if (chunkSideType == 5)
                {
                    lightValue = chunkLights.data.lightsBack[voxelArrayIndex];
                }
                else if (chunkSideType == 6)
                {
                    // lightValue = chunkLights2[chunkNeighbors.chunkForward].lights[otherVoxelIndex];
                    // lightValue = chunkLights.data.lightsForward[voxelArrayIndex];
                }
                else if (chunkSideType == 1)
                {
                    //lightValue = chunkLights.data.lightsUp[voxelArrayIndex];
                }
                else if (chunkSideType == 2)
                {
                    //lightValue = chunkLights.data.lightsDown[voxelArrayIndex];
                }
                else
                {
                    lightValue = 0;
                }*/