using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Voxels.Lighting
{
	//! Propogates lights accross chunks
    /**
    *   - End System -
	*	Checks surrounding chunks to see if they have finished as well.
    */
	[UpdateAfter(typeof(PropogateLightSystem))]
    [BurstCompile, UpdateInGroup(typeof(VoxelLightsSystemGroup))]
    public partial class PropogateLightEndSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery processQuery;
		private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			chunksQuery = GetEntityQuery(
				ComponentType.ReadOnly<Chunk>(),
				ComponentType.ReadOnly<PropogateLights>(),
				ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
			var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var propogateLights2 = GetComponentLookup<PropogateLights>(true);
			chunkEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.ForEach((Entity e, int entityInQueryIndex, in ChunkNeighbors chunkNeighbors, in Chunk chunk, in PropogateLights propogateLights) =>
			{
				var voxelDimensions = chunk.voxelDimensions;
				if (propogateLights.voxelArrayIndex != -1)
				{
					return;
				}
				var chunkNeighbors2 = chunkNeighbors.GetNeighbors();
				for (int i = 0; i < chunkNeighbors2.Length; i++)
				{
					var chunkEntity = chunkNeighbors2[i];
					if (chunkEntity.Index != 0)
					{
						if (propogateLights2.HasComponent(chunkEntity))
						{
							if (propogateLights2[chunkEntity].voxelArrayIndex != -1)
							{
                				chunkNeighbors2.Dispose();
								return;
							}
						}
					}
				}
                chunkNeighbors2.Dispose();
				// before moving on, check if surrounding chunks have finished
				PostUpdateCommands.RemoveComponent<PropogateLights>(entityInQueryIndex, e);
				PostUpdateCommands.AddComponent<ChunkBuilderComplete>(entityInQueryIndex, e);
				// UnityEngine.Debug.LogError(e.Index + " - " + elapsedTime + " - Finished Propogation Lights.");
			})	.WithReadOnly(propogateLights2)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}