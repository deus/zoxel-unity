using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Voxels.Lighting.Authoring;

namespace Zoxel.Voxels.Lighting
{
    //! Generates lighting using vertex colours for our voxel meshes.
    /**
    *   Optimized using a lookup table.
    *   Fixed all the diagonals lighting gather routines for voxels.
    */
    // [UpdateAfter(typeof(ChunkMeshLightsBasicSystem))]
	[BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
	public partial class ChunkMeshLightSmoothSystem : SystemBase
    {
		private EntityQuery processQuery;
		private EntityQuery chunksQuery;
		private EntityQuery voxesQuery;
		private EntityQuery voxelQuery;

        protected override void OnCreate()
        {
			voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxelLinks>());
            chunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkLights>(),
                ComponentType.ReadOnly<ChunkNeighbors>());
			voxelQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<VoxelMeshType>());
            RequireForUpdate<LightsSettings>();
			RequireForUpdate(processQuery);
			RequireForUpdate(voxesQuery);
			RequireForUpdate(chunksQuery);
			RequireForUpdate(voxelQuery);
		}
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            var lightsSettings = GetSingleton<LightsSettings>();
            var disableVoxelLighting = lightsSettings.disableVoxelLighting;
            var brightestBright = (float) lightsSettings.brightestBright;
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxelLinks = GetComponentLookup<VoxelLinks>(true);
            voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var chunks = GetComponentLookup<Chunk>(true);
            var chunkLights2 = GetComponentLookup<ChunkLights>(true);
            var chunkNeighbors2 = GetComponentLookup<ChunkNeighbors>(true);
            chunkEntities.Dispose();
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxelMeshTypes = GetComponentLookup<VoxelMeshType>(true);
            voxelEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, UpdateChunkNeighbors>()
				.ForEach((ref TerrainMesh terrainMesh, ref ChunkRenderBuilder chunkRenderBuilder, in ChunkSides chunkSides, in VoxLink voxLink, in ChunkLink chunkLink) =>
			{
				if (chunkRenderBuilder.chunkRenderBuildState != (byte) ChunkRenderBuildState.GenerateSmoothLights)
                {
                    return;
                }
                chunkRenderBuilder.chunkRenderBuildState = (byte) ChunkRenderBuildState.CompletedChunkRenderBuilder;
                if (disableVoxelLighting)
                {
                    return;
                }
                if (!voxelLinks.HasComponent(voxLink.vox))
                {
                    return;
                }
                var worldVoxelEntities = voxelLinks[voxLink.vox].voxels;
                //! 40 ms total over 24 instances.
                if (!chunkLights2.HasComponent(chunkLink.chunk))
                {
                    return;
                }
                var chunkLights = chunkLights2[chunkLink.chunk];
                var chunkNeighbors = chunkNeighbors2[chunkLink.chunk];
                var chunk = chunks[chunkLink.chunk];
                var voxelDimensions = chunk.voxelDimensions;
                var voxelMeshTypes2 = new NativeArray<byte>(worldVoxelEntities.Length, Allocator.Temp);
                for (int i = 0; i < voxelMeshTypes2.Length; i++)
                {
                    var voxelEntity = worldVoxelEntities[i];
                    voxelMeshTypes2[i] = voxelMeshTypes[voxelEntity].meshType;
                }
                byte voxelType;
                byte sideIndex;
                var vertIndex = 0;
                var voxelArrayIndex = 0;
                int3 localPosition;
                for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                {
                    for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                    {
                        for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
                        {
                            voxelType = chunk.voxels[voxelArrayIndex];
                            if (voxelType == 0)
                            {
                                voxelArrayIndex++;
                                continue;
                            }
                            voxelType--;
                            if (voxelMeshTypes2[voxelType] != MeshType.Block)
                            {
                                voxelArrayIndex++;
                                continue;
                            }
                            for (sideIndex = 0; sideIndex < 6; sideIndex++)
                            {
                                // if sides allow it, then add vertex side of model!
                                if ((sideIndex == VoxelSide.Left && chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Left) != 0)
                                    || (sideIndex == VoxelSide.Right && chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Right) != 0)
                                    || (sideIndex == VoxelSide.Down && chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Down) != 0)
                                    || (sideIndex == VoxelSide.Up && chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Up) != 0)
                                    || (sideIndex == VoxelSide.Backward && chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Backward) != 0)
                                    || (sideIndex == VoxelSide.Forward && chunkSides.GetSideIndex(voxelArrayIndex, VoxelSide.Forward) != 0))
                                {
                                    ProcessVoxelLightSideSmooth(ref terrainMesh, in chunkLights, in chunk, in chunkSides, in chunkNeighbors, localPosition, voxelDimensions, sideIndex, vertIndex,
                                        voxelArrayIndex, brightestBright, in chunkLights2, in chunks);
                                    vertIndex += 4;
                                }
                            }
                            voxelArrayIndex++;
                        }
                    }
                }
				voxelMeshTypes2.Dispose();
			})  .WithReadOnly(voxelLinks)	
                .WithReadOnly(voxelMeshTypes)
                .WithReadOnly(chunks)
                .WithReadOnly(chunkLights2)
                .WithReadOnly(chunkNeighbors2)
                .ScheduleParallel(Dependency);
		}

        // Add safety checks here, somehow goes past array indexoutofbounds - during day to night transition.
        //! Only adds lighting of nearby air voxels!
        private static void ProcessVoxelLightSideSmooth(ref TerrainMesh terrainMesh, in ChunkLights chunkLights, in Chunk chunk,
            in ChunkSides chunkSides, in ChunkNeighbors chunkNeighbors, int3 localPosition, int3 voxelDimensions, byte sideIndex, int vertIndex,
            int voxelArrayIndex, float brightestBright, in ComponentLookup<ChunkLights> chunkLights2, in ComponentLookup<Chunk> chunks)
        {
            if (vertIndex + 3 >= terrainMesh.vertices.Length)
            {
                return;
            }
            int lightValue;
            int lightsAdded;
            int solidAdjacents;
            int3 lightVoxelPosition;
            int3 lightVoxelPosition2;
            ZoxVertex zoxVertex;
            int arrayIndex;
            int vertIndex2;
            int positionOffsetArrayIndex;
            int positionOffsetArrayIndex2;
            int lightOffsetIndex;
            int lightOffsetIndex2;
            for (arrayIndex = 0; arrayIndex < 4; arrayIndex++)
            {
                vertIndex2 = vertIndex + arrayIndex;
                zoxVertex = terrainMesh.vertices[vertIndex2];
                lightValue = 0;
                lightsAdded = 0;
                for (positionOffsetArrayIndex = 0; positionOffsetArrayIndex < 4; positionOffsetArrayIndex++)
                {
                    // lightVoxelPosition = chunkRenderLightPositions.closestLightPositions[k];
                    lightOffsetIndex = sideIndex * 16 + arrayIndex * 4 + positionOffsetArrayIndex;
                    if (lightOffsetIndex >= LightingOffsets.lightingOffsets.Length)
                    {
                        return;
                    }
                    lightVoxelPosition = localPosition + LightingOffsets.lightingOffsets[lightOffsetIndex];
                    // if light adjacent with voxel
                    if (lightVoxelPosition.x == localPosition.x || lightVoxelPosition.y == localPosition.y || lightVoxelPosition.z == localPosition.z)
                    {
                        lightValue += GetLightValue(lightVoxelPosition, voxelDimensions, sideIndex, in chunkLights, in chunkNeighbors, in chunkLights2);
                        lightsAdded++;
                    }
                    //! If Corner Position, do a adjacent light check. Light cannot pass through walls.
                    else
                    {
                        // get other positions
                        // if both adjact voxels have solid, continue and skip voxel lights for corner
                        solidAdjacents = 0;
                        for (positionOffsetArrayIndex2 = 0; positionOffsetArrayIndex2 < 4; positionOffsetArrayIndex2++)
                        {
                            if (positionOffsetArrayIndex == positionOffsetArrayIndex2)
                            {
                                continue;
                            }
                            // lightVoxelPosition2 = chunkRenderLightPositions.closestLightPositions[a];
                            lightOffsetIndex2 = sideIndex * 16 + arrayIndex * 4 + positionOffsetArrayIndex2;
                            if (lightOffsetIndex2 >= LightingOffsets.lightingOffsets.Length)
                            {
                                return;
                            }
                            lightVoxelPosition2 = localPosition + LightingOffsets.lightingOffsets[lightOffsetIndex2];
                            if (lightVoxelPosition2 != localPosition)     // if not voxel inside of
                            {
                                if (GetVoxelValue(lightVoxelPosition2, voxelDimensions, in chunk, in chunkNeighbors, in chunks) != 0)
                                {
                                    solidAdjacents++;
                                }
                            }
                        }
                        if (solidAdjacents != 2)
                        {
                            lightValue += GetLightValue(lightVoxelPosition, voxelDimensions, sideIndex, in chunkLights, in chunkNeighbors, in chunkLights2);
                            lightsAdded++;
                        }
                    }
                }
                lightValue /= lightsAdded;
                if (lightValue >= LightingCurve.lightValues.Length)
                {
                    return;
                }
                float lightValueFloat = LightingCurve.lightValues[lightValue];  // maybe just get this to return float3
                zoxVertex.color = new float3(lightValueFloat, lightValueFloat, lightValueFloat);
                terrainMesh.vertices[vertIndex2] = zoxVertex;
            }
        }

        // todo: Cache the chunk and light data before processing array
        // TODO: If perpendicular sides is solid, dont get corner voxel lights
        private static byte GetLightValue(int3 localPosition, int3 voxelDimensions, int sideIndex, in ChunkLights chunkLights, in ChunkNeighbors chunkNeighbors,
            in ComponentLookup<ChunkLights> chunkLights2)
        {
            if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
            {
                return chunkLights.lights[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)];
            }
            else
            {
                var chunkEntity = chunkNeighbors.GetChunk(ref localPosition, voxelDimensions);
                var chunkLights3 = chunkLights2[chunkEntity];
                if (chunkLights3.lights.Length == 0)
                {
                    return 0;
                }
                return chunkLights3.lights[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)];
            }
        }

        private static byte GetVoxelValue(int3 localPosition, int3 voxelDimensions, in Chunk chunk, in ChunkNeighbors chunkNeighbors,
            in ComponentLookup<Chunk> chunks)
        {
            if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
            {
                return chunk.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)];
            }
            else
            {
                var chunkEntity = chunkNeighbors.GetChunk(ref localPosition, voxelDimensions);
                var chunk2 = chunks[chunkEntity];
                if (chunk2.voxels.Length == 0)
                {
                    return 1;
                }
                return chunk2.voxels[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)];
            }
        }
    }
}

/*const float vertOffset2 = 0.1f;

public static void OffsetVertex(ref float3 vertex, byte sideIndex)
{
    if (sideIndex == VoxelSide.Left)
    {
        vertex.x -= vertOffset2;
    }
    else if (sideIndex == VoxelSide.Right)
    {
        vertex.x += vertOffset2;
    }
    else if (sideIndex == VoxelSide.Down)
    {
        vertex.y -= vertOffset2;
    }
    else if (sideIndex == VoxelSide.Up)
    {
        vertex.y += vertOffset2;
    }
    else if (sideIndex == VoxelSide.Backward)
    {
        vertex.z -= vertOffset2;
    }
    else if (sideIndex == VoxelSide.Forward)
    {
        vertex.z += vertOffset2;
    }
}

//! This sorts lights by closeness
private static void SetClosestLightPositions(ref ChunkRenderLightPositions chunkRenderLightPositions, float3 vertexPosition)
{
    bool alreadyAdded;
    int i;
    int j;
    int k;
    int3 lightPosition;
    float distanceToVertex;
    float smallestDistance;
    int smallestDistanceIndex;
    for (i = 0; i < chunkRenderLightPositions.closestLightPositions.Length; i++)
    {
        smallestDistanceIndex = 0;
        smallestDistance = 2;
        for (j = 0; j < chunkRenderLightPositions.lightPositions.Length; j++)
        {
            lightPosition = chunkRenderLightPositions.lightPositions[j];
            // check it isn't already added
            alreadyAdded = false;
            for (k = i - 1; k >= 0; k--)
            {
                if (lightPosition == chunkRenderLightPositions.closestLightPositions[k])
                {
                    alreadyAdded = true;
                    break;
                }
            }
            if (!alreadyAdded)
            {
                distanceToVertex = math.distance(lightPosition.ToFloat3() + new float3(0.5f, 0.5f, 0.5f), vertexPosition);
                if (distanceToVertex < smallestDistance)
                {
                    smallestDistance = distanceToVertex;
                    smallestDistanceIndex = j;
                }
            }
        }
        chunkRenderLightPositions.closestLightPositions[i] = chunkRenderLightPositions.lightPositions[smallestDistanceIndex];
    }
}*/