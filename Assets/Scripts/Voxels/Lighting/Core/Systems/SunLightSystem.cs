using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels.Lighting.Authoring;

namespace Zoxel.Voxels.Lighting
{
    //! Creates rays of voxel light for our world.
    /**
    *   This works for each side of the planet.
    *   \todo Optimize System; uses 14ms on thread.
    *   \todo Make light come from a space object and update based on rotations.
    */
    [UpdateAfter(typeof(ChunkDarknessSystem))]
    [BurstCompile, UpdateInGroup(typeof(VoxelLightsSystemGroup))]
    public partial class SunlightSystem : SystemBase
	{
        private const int checkUpDistance = 64;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
		private EntityQuery voxesQuery;
		private EntityQuery chunksQuery;
		private EntityQuery voxelsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			voxesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxelLinks>());
			chunksQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<ChunkPosition>(),
                ComponentType.ReadOnly<ChunkNeighbors>());
			voxelsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Voxel>());
            RequireForUpdate<LightsSettings>();
            RequireForUpdate(processQuery);
            RequireForUpdate(voxesQuery);
            RequireForUpdate(chunksQuery);
            RequireForUpdate(voxelsQuery);
        }

        public struct ShineLightInput
        {
            public byte dimmedLight;
            public Entity chunkEntity;
            public int3 chunkPosition;
            public int3 chunkGlobalVoxelPosition;
            // public ChunkLights chunkLights;
            [ReadOnly] public Chunk chunk;
            [ReadOnly] public ChunkNeighbors localChunkNeighbors;
        }

        [BurstCompile]
		protected override void OnUpdate()
		{
            var lightsSettings = GetSingleton<LightsSettings>();
            var disableSunlight = lightsSettings.disableSunlight;
            if (disableSunlight)
            {
                var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
                PostUpdateCommands2.RemoveComponent<ChunkDarknessBuilder>(processQuery);
                PostUpdateCommands2.AddComponent<PropogateLights>(processQuery);
                return;
            }
            // UnityEngine.Debug.LogError("Updating Sunlight: " + World.Time.ElapsedTime);
            var heightOnlySunlight = lightsSettings.heightOnlySunlight;
            var elapsedTime = World.Time.ElapsedTime;
            var waterDecreaseRate = lightsSettings.waterDecreaseRate; // (byte)32;
			var torchLight = lightsSettings.torchLight;
			var lowestLight = lightsSettings.lowestLight;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var skyLights = GetComponentLookup<Skylight>(true);
            var voxelLinks = GetComponentLookup<VoxelLinks>(true);
            voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunks2 = GetComponentLookup<Chunk>(true);
            var chunkNeighbors = GetComponentLookup<ChunkNeighbors>(true);
            var chunkLights2 = GetComponentLookup<ChunkLights>(true);
            chunkEntities.Dispose();
            var voxelEntities = voxelsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var voxelLightPasss = GetComponentLookup<VoxelLightPass>(true);
            var voxelLightPassDimmeds = GetComponentLookup<VoxelLightPassDimmed>(true);
            voxelEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<UpdateChunkNeighbors>()
                // .WithNone<UpdateChunkNeighbors, EdgeChunk>()
                .WithAll<ChunkSunlightBuilder>()
                .ForEach((Entity e, int entityInQueryIndex, ref ChunkLights chunkLights, in ChunkPosition chunkPosition, in Chunk chunk, in ChunkNeighbors localChunkNeighbors,
                    in VoxLink voxLink, in ChunkGravityQuadrant chunkGravityQuadrant) =>
			{
                // UnityEngine.Profiling.Profiler.BeginSample("Sunlight System");
                if (!skyLights.HasComponent(voxLink.vox))
                {
                    return;
                }
                // check if any chunks in the neighbor chunk beams are building
				#if DEBUG_VOXEL_PLACEMENT
				UnityEngine.Debug.LogError(e.Index + " - Voxel Placement - ChunkLightsBuilderState.Sunlight: " + elapsedTime);
				#endif
                // var chunk = chunks2[e];
                // var localChunkNeighbors = chunkNeighbors[e];
                var voxelDimensions = chunk.voxelDimensions;
                var hasHeightsLeft = chunkGravityQuadrant.quadrantA.GetBoolB();
                var hasHeightsRight = chunkGravityQuadrant.quadrantB.GetBoolB();
                var hasHeightsDown = chunkGravityQuadrant.quadrantA.GetBoolA();
                var hasHeightsUp = chunkGravityQuadrant.quadrantB.GetBoolA();
                var hasHeightsBackward = chunkGravityQuadrant.quadrantA.GetBoolC();
                var hasHeightsForward = chunkGravityQuadrant.quadrantB.GetBoolC();
                if (heightOnlySunlight)
                {
                    hasHeightsLeft = false;
                    hasHeightsRight = false;
                    hasHeightsDown = false;
                    hasHeightsUp = true;
                    hasHeightsBackward = false;
                    hasHeightsForward = false;
                }
                if (!(hasHeightsDown || hasHeightsUp || hasHeightsLeft || hasHeightsRight || hasHeightsBackward || hasHeightsForward))
                {
                    PostUpdateCommands.RemoveComponent<ChunkSunlightBuilder>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<ChunkSunlit>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<TorchLightsBuilder>(entityInQueryIndex, e);
                    return;
                }
                var isSkyChunk = false;
                if (hasHeightsUp)
                {
                    var chunkUpEntity = localChunkNeighbors.chunkUp;
                    if (chunkUpEntity.Index == 0)
                    {
                        isSkyChunk = true;
                    }
                    else if (chunkUpEntity.Index > 0 &&
                        (!HasComponent<ChunkSunlit>(chunkUpEntity) || 
                        HasComponent<ChunkBuilder>(chunkUpEntity) || HasComponent<ChunkSunlightBuilder>(chunkUpEntity) || HasComponent<ChunkDarknessBuilder>(chunkUpEntity)))
                    {
                        /*if (!HasComponent<ChunkSunlit>(chunkUpEntity) && chunkPosition.position.x == 0 && chunkPosition.position.z == 0)
                        {
                            UnityEngine.Debug.LogError("Chunk [" + chunkPosition.position.y + "] is waiting for above chunk: " + chunkUpEntity.Index);
                        }*/
                        return; // wait for chunk above to be done
                    }
                }
                var skylight = skyLights[voxLink.vox].skylight;
                if (skylight == lowestLight)
                {
                    // UnityEngine.Debug.LogError("Skylight is lowestLight!");
                    skylight++;
                }
                var voxelEntities3 = voxelLinks[voxLink.vox].voxels;
                var isLightPassThroughs = new NativeArray<byte>(voxelEntities3.Length, Allocator.Temp);
                for (byte i = 0; i < voxelEntities3.Length; i++)
                {
                    var voxelEntity = voxelEntities3[i];
                    if (voxelLightPasss.HasComponent(voxelEntity))
                    {
                        isLightPassThroughs[i] = 1;
                        //UnityEngine.Debug.LogError("Light pass through: " + i);
                    }
                    else if (voxelLightPassDimmeds.HasComponent(voxelEntity))
                    {
                        isLightPassThroughs[i] = 2;
                        // UnityEngine.Debug.LogError("Light pass through dimmed: " + i);
                    }
                    else
                    {
                        isLightPassThroughs[i] = 0;
                    }
                }
                
                var chunkVoxelPosition = chunkPosition.GetVoxelPosition(voxelDimensions);
                // var lowestChunkVoxelPosition = chunkVoxelPosition;
                var localChunkPosition = chunkPosition.position;
                if (hasHeightsUp)
                {
                    SunlightVertical(e, ref chunkLights, voxelDimensions, localChunkPosition, chunkVoxelPosition,
                        lowestLight, waterDecreaseRate, isSkyChunk, skylight,
                        in chunk, in localChunkNeighbors,
                        in chunks2, in chunkLights2, in chunkNeighbors, in isLightPassThroughs);
                }
				isLightPassThroughs.Dispose();
                // UnityEngine.Debug.LogError("Sunlight Done at: " + chunkPosition.position.y);
                PostUpdateCommands.RemoveComponent<ChunkSunlightBuilder>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<ChunkSunlit>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<TorchLightsBuilder>(entityInQueryIndex, e);
                // UnityEngine.Profiling.Profiler.EndSample();
			})  .WithReadOnly(skyLights)
                .WithReadOnly(voxelLinks)
                .WithReadOnly(chunks2)
                .WithReadOnly(chunkLights2)
                .WithReadOnly(chunkNeighbors)
                .WithReadOnly(voxelLightPasss)
                .WithReadOnly(voxelLightPassDimmeds)
                .WithNativeDisableContainerSafetyRestriction(chunkLights2)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}

        // only perform this from top chunk
		private static void SunlightVertical(Entity e, ref ChunkLights chunkLights,
            int3 voxelDimensions, int3 localChunkPosition, int3 chunkVoxelPosition,
            byte lowestLight, byte waterDecreaseRate, bool isSkyChunk, byte skylight,
            in Chunk chunk, in ChunkNeighbors localChunkNeighbors,
            in ComponentLookup<Chunk> chunks,
            in ComponentLookup<ChunkLights> chunkLights2,
            in ComponentLookup<ChunkNeighbors> chunkNeighbors,
            in NativeArray<byte> isLightPassThroughs)
		{
            // var chunk = chunks[e];
            // var localChunkNeighbors = chunkNeighbors[e];
            // var chunkLights2 = chunkLights[e];
            // UnityEngine.Debug.LogError("sunlightStartHeight: " + sunlightStartHeight);
            // UnityEngine.Debug.LogError("Sunlight: VoxelPosition: " + highestChunkVoxelPosition.y + " to " + lowestChunkVoxelPosition.y);
            int3 globalPosition;
            ShineLightInput shineLightInput;
            var sunlightStartHeight = chunkVoxelPosition.y + voxelDimensions.y - 1;
            var aboveChunkLights = new ChunkLights();
            if (!isSkyChunk)
            {
                aboveChunkLights = chunkLights2[localChunkNeighbors.chunkUp];
            }
            for (globalPosition.x = chunkVoxelPosition.x; globalPosition.x < chunkVoxelPosition.x + voxelDimensions.x; globalPosition.x++)
            {
                for (globalPosition.z = chunkVoxelPosition.z; globalPosition.z < chunkVoxelPosition.z + voxelDimensions.z; globalPosition.z++)
                {
                    shineLightInput = new ShineLightInput();
                    shineLightInput.chunkEntity = e;
                    shineLightInput.chunk = chunk;
                    shineLightInput.chunkPosition = localChunkPosition;
                    shineLightInput.chunkGlobalVoxelPosition = chunkVoxelPosition;
                    shineLightInput.localChunkNeighbors = localChunkNeighbors;
                    // shineLightInput.chunkLights = chunkLights2;
                    // set light to either skylight or above chunk light
                    if (isSkyChunk)
                    {
                        shineLightInput.dimmedLight = skylight;
                    }
                    else
                    {
                        globalPosition.y = 0;
                        var aboveLocalPosition = VoxelUtilities.GetLocalPosition(globalPosition, shineLightInput.chunkPosition, voxelDimensions);
                        aboveLocalPosition.y = 0;
                        var voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(aboveLocalPosition, voxelDimensions);
                        shineLightInput.dimmedLight = aboveChunkLights.lights[voxelArrayIndex];
                    }
                    for (globalPosition.y = sunlightStartHeight; globalPosition.y >= chunkVoxelPosition.y; globalPosition.y--)
                    {
                        // This uses alot of processing atm
                        if (!ShineLight(ref shineLightInput, ref chunkLights, globalPosition, voxelDimensions,
                            lowestLight, waterDecreaseRate, in chunks, in chunkNeighbors, in isLightPassThroughs))
                        {
                            // chunkLights[shineLightInput.chunkEntity] = shineLightInput.chunkLights;
                            // UnityEngine.Debug.LogError("Sunlight Stopped at: " + globalPosition.y);
                            break;  // break when hit solid
                        }
                    }
                }
            }
        }

		private static bool ShineLight(
            ref ShineLightInput shineLightInput, ref ChunkLights chunkLights, int3 globalPosition, int3 voxelDimensions,
            byte lowestLight, byte waterDecreaseRate,
            in ComponentLookup<Chunk> chunks,
            in ComponentLookup<ChunkNeighbors> chunkNeighbors,
            in NativeArray<byte> isLightPassThroughs)
		{
            if (chunkLights.lights.Length == 0)
            {
                // UnityEngine.Debug.LogError("AWEAWEtrey5rey4");
                return false;
            }
            var localPosition = VoxelUtilities.GetLocalPosition(globalPosition, shineLightInput.chunkPosition, voxelDimensions);
            if (!VoxelUtilities.InBounds(localPosition, voxelDimensions))
            {
                // chunkLights[shineLightInput.chunkEntity] = shineLightInput.chunkLights;
                var beforeChunkPosition = shineLightInput.chunkPosition;
                shineLightInput.chunkEntity = shineLightInput.localChunkNeighbors.GetChunkAdjacent(ref localPosition, ref shineLightInput.chunkPosition, voxelDimensions);
                if (shineLightInput.chunkEntity.Index == 0 || !chunks.HasComponent(shineLightInput.chunkEntity))
                {
                    // UnityEngine.Debug.LogError("Before position: " + beforeChunkPosition.y + " after: " + shineLightInput.chunkPosition.y);
                    return false;
                }
                // shineLightInput.chunkLights = chunkLights[shineLightInput.chunkEntity];
                shineLightInput.chunk = chunks[shineLightInput.chunkEntity];
                shineLightInput.localChunkNeighbors = chunkNeighbors[shineLightInput.chunkEntity];
                shineLightInput.chunkGlobalVoxelPosition = ChunkPosition.GetChunkVoxelPosition(shineLightInput.chunkPosition, voxelDimensions);
            }
            if (shineLightInput.chunk.voxels.Length == 0)
            {
                //UnityEngine.Debug.LogError("AWEAWEtrey5rey2");
                return false;
            }
            // if chunk changes, update here
            var voxelArrayIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
            if (voxelArrayIndex >= shineLightInput.chunk.voxels.Length || voxelArrayIndex >= chunkLights.lights.Length)
            {
                //UnityEngine.Debug.LogError("AWEAWEtrey5rey");
                return false;
            }
            var voxelIndex = shineLightInput.chunk.voxels[voxelArrayIndex];
            if (voxelIndex == 0)
            {
                chunkLights.lights[voxelArrayIndex] = shineLightInput.dimmedLight;
                // UnityEngine.Debug.LogError("Light Set Air: " + shineLightInput.dimmedLight);
                return true;
            }
            else 
            {
                var voxelMinusAirIndex = (voxelIndex - 1);
                if (voxelMinusAirIndex >= isLightPassThroughs.Length)
                {
                    return false;
                }
                var isLightPassThrough = isLightPassThroughs[voxelMinusAirIndex];
                if (isLightPassThrough == 1)
                {
                    // UnityEngine.Debug.LogError("Light Set Pass Through");
                    chunkLights.lights[voxelArrayIndex] = shineLightInput.dimmedLight;
                    return true;
                }
                else if (isLightPassThrough == 2)
                {
                    // UnityEngine.Debug.LogError("Light Dimmed: " + voxelIndex);
                    var lightValue = chunkLights.lights[voxelArrayIndex];
                    // to handle adjacent sun rays going through one another
                    if (lightValue == lowestLight) // lightValue > shineLightInput.dimmedLight) // lightValue != lowestLight)
                    {
                        if (((int) shineLightInput.dimmedLight) - ((int) waterDecreaseRate) < lowestLight)
                        {
                            shineLightInput.dimmedLight = lowestLight;
                        }
                        else
                        {
                            shineLightInput.dimmedLight -= waterDecreaseRate;
                        }
                        chunkLights.lights[voxelArrayIndex] = shineLightInput.dimmedLight;
                        return false;
                    }
                    else
                    {
                        if (lightValue < shineLightInput.dimmedLight)
                        {
                            chunkLights.lights[voxelArrayIndex] = shineLightInput.dimmedLight;
                        }
                        else
                        {
                            shineLightInput.dimmedLight = lightValue; // chunkLights.lights[voxelArrayIndex];
                        }
                        return true;
                    }
                }
                else
                {
                    return false;
                    //UnityEngine.Debug.LogError("Sunblocked at: " + globalPosition.y);
                }
            }
        }
    }
}
            // highest chunk
            // var highestChunkEntity = e;
            // var highestChunkPosition = localChunkPosition;
            // var highestChunkVoxelPosition = chunkVoxelPosition; // VoxelUtilities.GetChunkVoxelPosition(highestChunkPosition, voxelDimensions);
            // lowest chunk
            /*var lowestChunkEntity = e;
            var lowestChunkPosition = localChunkPosition;
            chunkLights[lowestChunkEntity].SetAllLights(lowestLight);
            for (var i = 1; i < checkUpDistance; i++)
            {
                lowestChunkEntity = chunkNeighbors[lowestChunkEntity].chunkDown;
                if (lowestChunkEntity.Index == 0 || !chunkNeighbors.HasComponent(lowestChunkEntity))
                {
                    break;
                }
                else
                {
                    chunkLights[lowestChunkEntity].SetAllLights(lowestLight);
                    lowestChunkPosition = localChunkPosition - new int3(0, i, 0);
                }
            }
            var lowestChunkVoxelPosition = VoxelUtilities.GetChunkVoxelPosition(lowestChunkPosition, voxelDimensions);*/



            
            /*for (var i = 1; i < checkUpDistance; i++)
            {
                checkChunkEntity = chunkNeighbors[checkChunkEntity].chunkUp;
                if (checkChunkEntity.Index == 0 || !chunkNeighbors.HasComponent(checkChunkEntity))
                {
                    break;
                }
                else
                {
                    highestChunkPosition = localChunkPosition + new int3(0, i, 0);
                    highestChunkEntity = checkChunkEntity;
                }
            }
            if (highestChunk != localChunk)
            {
                return;
            }*/

                // shineLightInput.isInLocalChunk = shineLightInput.chunkPosition == localChunkPosition;
                /*if (!VoxelUtilities.InBounds(localPosition, voxelDimensions))
                {
                    // UnityEngine.Debug.LogError("Jam");
                    return;
                }*/

            // var localPosition = globalPosition - shineLightInput.chunkGlobalVoxelPosition;
            // var chunkPosition4 = chunkPositions[targetChunkEntity];
            // localPosition = globalPosition - chunkGlobalVoxelPosition;

            /*if (!VoxelUtilities.InBounds(localPosition, voxelDimensions))
            {
                if (!chunkNeighbors.HasComponent(targetChunkEntity))
                {
                    // UnityEngine.Debug.LogError("Dam");
                    return;
                }
                var chunkNeighbors2 = chunkNeighbors[targetChunkEntity];
                if (direction == PlanetSide.Right)
                {
                    targetChunkEntity = chunkNeighbors2.chunkLeft;
                }
                else if (direction == PlanetSide.Left)
                {
                    targetChunkEntity = chunkNeighbors2.chunkRight;
                }
                else if (direction == PlanetSide.Forward)
                {
                    targetChunkEntity = chunkNeighbors2.chunkBack;
                }
                else if (direction == PlanetSide.Backward)
                {
                    targetChunkEntity = chunkNeighbors2.chunkForward;
                }
                else if (direction == PlanetSide.Up)
                {
                    targetChunkEntity = chunkNeighbors2.chunkDown;
                }
                else if (direction == PlanetSide.Down)
                {
                    targetChunkEntity = chunkNeighbors2.chunkUp;
                }
                if (targetChunkEntity.Index == 0)
                {
                    // UnityEngine.Debug.LogError("Sam");
                    return;
                }
                if (!chunks2.HasComponent(targetChunkEntity))
                {
                    // UnityEngine.Debug.LogError("Dam");
                    return;
                }
                chunk = chunks2[targetChunkEntity];
                var chunkPosition4 = chunkPositions[targetChunkEntity];
                shineLightInput.isInLocalChunk = chunkPosition4.position == localChunkPosition;
                chunkGlobalVoxelPosition = chunkPosition4.GetVoxelPosition(voxelDimensions);
                localPosition = globalPosition - chunkGlobalVoxelPosition;
                if (!VoxelUtilities.InBounds(localPosition, voxelDimensions))
                {
                    // UnityEngine.Debug.LogError("Jam");
                    return;
                }
            }*/
                // get list of chunks in XZ?
                /*if (hasHeightsDown)
                {
                    highestChunkPosition = localChunkPosition;
                    highestChunkEntity = e;
                    var checkChunkEntity = e;
                    for (int i = 1; i < checkUpDistance; i++)
                    {
                        checkChunkEntity = chunkNeighbors[checkChunkEntity].chunkDown;
                        if (checkChunkEntity.Index == 0 || !chunkNeighbors.HasComponent(checkChunkEntity))
                        {
                            break;
                        }
                        else
                        {
                            highestChunkPosition = localChunkPosition - new int3(0, i, 0);
                            highestChunkEntity = checkChunkEntity;
                        }
                    }
                    var highestChunkVoxelPosition = VoxelUtilities.GetChunkVoxelPosition(highestChunkPosition, voxelDimensions);
                    var highestChunk = chunks2[highestChunkEntity];
                    var targetChunkEntity = highestChunkEntity;
                    var sunlightStartHeight = highestChunkVoxelPosition.y;                          // starrt at lowestChunkPosition
                    var sunlightEndHeight = lowestChunkVoxelPosition.y + voxelDimensions.y - 1;     // move to CurrentChunk!
                    // UnityEngine.Debug.LogError("sunlightStartHeight: " + sunlightStartHeight);
                    // UnityEngine.Debug.LogError("Sunlight: VoxelPosition Y: " + sunlightStartHeight + " to " + sunlightEndHeight);
                    for (globalPosition.x = chunkVoxelPosition.x; globalPosition.x < chunkVoxelPosition.x + voxelDimensions.x; globalPosition.x++)
                    {
                        for (globalPosition.z = chunkVoxelPosition.z; globalPosition.z < chunkVoxelPosition.z + voxelDimensions.z; globalPosition.z++)
                        {
                            targetChunkPosition = highestChunkPosition;
                            chunkGlobalVoxelPosition = highestChunkVoxelPosition;
                            targetChunkEntity = highestChunkEntity;
                            chunk = highestChunk;
                            var shineLightInput = new ShineLightInput();
                            shineLightInput.isSunBlocked = 0;
                            shineLightInput.dimmedLight = skylight;
                            shineLightInput.isInLocalChunk = targetChunkPosition == localChunkPosition;
                            // for Sunlight Y
                            for (globalPosition.y = sunlightStartHeight; globalPosition.y <= sunlightEndHeight; globalPosition.y++)
                            {
                                ShineLight(PlanetSide.Down, ref chunkLights, ref shineLightInput, ref targetChunkEntity,
                                    in chunk, ref chunkGlobalVoxelPosition, in chunks2, in chunkNeighbors, // in chunkPositions,
                                    globalPosition, voxelDimensions, localChunkPosition, lowestLight, waterDecreaseRate, in isLightPassThroughs);
                            }
                        }
                    }
                }*/

                            /*targetChunkPosition = highestChunkPosition;
                            chunkGlobalVoxelPosition = highestChunkVoxelPosition;
                            targetChunkEntity = highestChunkEntity;
                            chunk = highestChunk;*/
                            /*var shineLightInput = new ShineLightInput();
                            shineLightInput.isSunBlocked = 0;
                            shineLightInput.dimmedLight = skylight;
                            shineLightInput.isInLocalChunk = targetChunkPosition == localChunkPosition;*/

                /*if (hasHeightsBackward)
                {
                    highestChunkPosition = localChunkPosition;
                    highestChunkEntity = e;
                    var checkChunkEntity = e;
                    for (int i = 1; i < checkUpDistance; i++)
                    {
                        checkChunkEntity = chunkNeighbors[checkChunkEntity].chunkBack;
                        if (checkChunkEntity.Index == 0 || !chunkNeighbors.HasComponent(checkChunkEntity))
                        {
                            break;
                        }
                        else
                        {
                            highestChunkPosition = localChunkPosition - new int3(0, 0, i);
                            highestChunkEntity = checkChunkEntity;
                        }
                    }
                    var highestChunkVoxelPosition = VoxelUtilities.GetChunkVoxelPosition(highestChunkPosition, voxelDimensions);
                    var highestChunk = chunks2[highestChunkEntity];
                    var targetChunkEntity = highestChunkEntity;
                    var sunlightStartHeight = highestChunkVoxelPosition.z;
                    var sunlightEndHeight = lowestChunkVoxelPosition.z + voxelDimensions.z - 1;
                    // UnityEngine.Debug.LogError("Sunlight: VoxelPosition X: " + sunlightStartHeight + " to " + sunlightEndHeight);
                    for (globalPosition.x = chunkVoxelPosition.x; globalPosition.x < chunkVoxelPosition.x + voxelDimensions.x; globalPosition.x++)
                    {
                        for (globalPosition.y = chunkVoxelPosition.y; globalPosition.y < chunkVoxelPosition.y + voxelDimensions.y; globalPosition.y++)
                        {
                            targetChunkPosition = highestChunkPosition;
                            chunkGlobalVoxelPosition = highestChunkVoxelPosition;
                            targetChunkEntity = highestChunkEntity;
                            chunk = highestChunk;
                            var shineLightInput = new ShineLightInput();
                            shineLightInput.isSunBlocked = 0;
                            shineLightInput.dimmedLight = skylight;
                            shineLightInput.isInLocalChunk = targetChunkPosition == localChunkPosition;
                            // for Sunlight Y
                            for (globalPosition.z = sunlightStartHeight; globalPosition.z <= sunlightEndHeight; globalPosition.z++)
                            {
                                ShineLight(PlanetSide.Backward, ref chunkLights, ref shineLightInput, ref targetChunkEntity,
                                    in chunk, ref chunkGlobalVoxelPosition, in chunks2, in chunkNeighbors, // in chunkPositions,
                                    globalPosition, voxelDimensions, localChunkPosition, lowestLight, waterDecreaseRate, in isLightPassThroughs);
                            }
                        }
                    }
                }
                if (hasHeightsForward)
                {
                    highestChunkPosition = localChunkPosition;
                    highestChunkEntity = e;
                    var checkChunkEntity = e;
                    for (int i = 1; i < checkUpDistance; i++)
                    {
                        checkChunkEntity = chunkNeighbors[checkChunkEntity].chunkForward;
                        if (checkChunkEntity.Index == 0 || !chunkNeighbors.HasComponent(checkChunkEntity))
                        {
                            break;
                        }
                        else
                        {
                            highestChunkPosition = localChunkPosition + new int3(0, 0, i);
                            highestChunkEntity = checkChunkEntity;
                        }
                    }
                    var highestChunkVoxelPosition = VoxelUtilities.GetChunkVoxelPosition(highestChunkPosition, voxelDimensions);
                    var highestChunk = chunks2[highestChunkEntity];
                    var targetChunkEntity = highestChunkEntity;

                    var sunlightStartHeight = highestChunkVoxelPosition.z + voxelDimensions.z - 1;
                    // UnityEngine.Debug.LogError("Sunlight: VoxelPosition: " + highestChunkVoxelPosition.y + " to " + lowestChunkVoxelPosition.y);
                    for (globalPosition.x = chunkVoxelPosition.x; globalPosition.x < chunkVoxelPosition.x + voxelDimensions.x; globalPosition.x++)
                    {
                        for (globalPosition.y = chunkVoxelPosition.y; globalPosition.y < chunkVoxelPosition.y + voxelDimensions.y; globalPosition.y++)
                        {
                            targetChunkPosition = highestChunkPosition;
                            chunkGlobalVoxelPosition = highestChunkVoxelPosition;
                            targetChunkEntity = highestChunkEntity;
                            chunk = highestChunk;
                            var shineLightInput = new ShineLightInput();
                            shineLightInput.isSunBlocked = 0;
                            shineLightInput.dimmedLight = skylight;
                            shineLightInput.isInLocalChunk = targetChunkPosition == localChunkPosition;
                            // for Sunlight Y
                            for (globalPosition.z = sunlightStartHeight; globalPosition.z >= lowestChunkVoxelPosition.z; globalPosition.z--)
                            {
                                ShineLight(PlanetSide.Forward, ref chunkLights, ref shineLightInput, ref targetChunkEntity,
                                    in chunk, ref chunkGlobalVoxelPosition, in chunks2, in chunkNeighbors, // in chunkPositions,
                                    globalPosition, voxelDimensions, localChunkPosition, lowestLight, waterDecreaseRate, in isLightPassThroughs);
                            }
                        }
                    }
                }
                if (hasHeightsLeft)
                {
                    highestChunkPosition = localChunkPosition;
                    highestChunkEntity = e;
                    var checkChunkEntity = e;
                    for (int i = 1; i < checkUpDistance; i++)
                    {
                        checkChunkEntity = chunkNeighbors[checkChunkEntity].chunkLeft;
                        if (checkChunkEntity.Index == 0 || !chunkNeighbors.HasComponent(checkChunkEntity))
                        {
                            break;
                        }
                        else
                        {
                            highestChunkPosition = localChunkPosition - new int3(i, 0, 0);
                            highestChunkEntity = checkChunkEntity;
                        }
                    }
                    var highestChunkVoxelPosition = VoxelUtilities.GetChunkVoxelPosition(highestChunkPosition, voxelDimensions);
                    var highestChunk = chunks2[highestChunkEntity];
                    var targetChunkEntity = highestChunkEntity;
                    var sunlightStartHeight = highestChunkVoxelPosition.x;
                    var sunlightEndHeight = lowestChunkVoxelPosition.x + voxelDimensions.x - 1;
                    // UnityEngine.Debug.LogError("Sunlight: VoxelPosition X: " + sunlightStartHeight + " to " + sunlightEndHeight);
                    for (globalPosition.y = chunkVoxelPosition.y; globalPosition.y < chunkVoxelPosition.y + voxelDimensions.y; globalPosition.y++)
                    {
                        for (globalPosition.z = chunkVoxelPosition.z; globalPosition.z < chunkVoxelPosition.z + voxelDimensions.z; globalPosition.z++)
                        {
                            targetChunkPosition = highestChunkPosition;
                            chunkGlobalVoxelPosition = highestChunkVoxelPosition;
                            targetChunkEntity = highestChunkEntity;
                            chunk = highestChunk;
                            var shineLightInput = new ShineLightInput();
                            shineLightInput.isSunBlocked = 0;
                            shineLightInput.dimmedLight = skylight;
                            shineLightInput.isInLocalChunk = targetChunkPosition == localChunkPosition;
                            // for Sunlight Y
                            for (globalPosition.x = sunlightStartHeight; globalPosition.x <= sunlightEndHeight; globalPosition.x++)
                            {
                                ShineLight(PlanetSide.Left, ref chunkLights, ref shineLightInput, ref targetChunkEntity,
                                    in chunk, ref chunkGlobalVoxelPosition, in chunks2, in chunkNeighbors, // in chunkPositions,
                                    globalPosition, voxelDimensions, localChunkPosition, lowestLight, waterDecreaseRate, in isLightPassThroughs);
                            }
                        }
                    }
                }
                if (hasHeightsRight)
                {
                    highestChunkPosition = localChunkPosition;
                    highestChunkEntity = e;
                    var checkChunkEntity = e;
                    for (int i = 1; i < checkUpDistance; i++)
                    {
                        checkChunkEntity = chunkNeighbors[checkChunkEntity].chunkRight;
                        if (checkChunkEntity.Index == 0 || !chunkNeighbors.HasComponent(checkChunkEntity))
                        {
                            break;
                        }
                        else
                        {
                            highestChunkPosition = localChunkPosition + new int3(i, 0, 0);
                            highestChunkEntity = checkChunkEntity;
                        }
                    }
                    var highestChunkVoxelPosition = VoxelUtilities.GetChunkVoxelPosition(highestChunkPosition, voxelDimensions);
                    var highestChunk = chunks2[highestChunkEntity];
                    var targetChunkEntity = highestChunkEntity;
                    var sunlightStartHeight = highestChunkVoxelPosition.x + voxelDimensions.x - 1;
                    var sunlightEndHeight = lowestChunkVoxelPosition.x;
                    // UnityEngine.Debug.LogError("Sunlight: VoxelPosition: " + highestChunkVoxelPosition.y + " to " + lowestChunkVoxelPosition.y);
                    for (globalPosition.y = chunkVoxelPosition.y; globalPosition.y < chunkVoxelPosition.y + voxelDimensions.y; globalPosition.y++)
                    {
                        for (globalPosition.z = chunkVoxelPosition.z; globalPosition.z < chunkVoxelPosition.z + voxelDimensions.z; globalPosition.z++)
                        {
                            var shineLightInput = new ShineLightInput();
                            shineLightInput.isSunBlocked = 0;
                            shineLightInput.dimmedLight = skylight;
                            shineLightInput.chunkEntity = highestChunkEntity;
                            shineLightInput.chunkGlobalVoxelPosition = highestChunkVoxelPosition;
                            shineLightInput.chunkPosition = highestChunkPosition;
                            shineLightInput.chunk = highestChunk;
                            shineLightInput.isInLocalChunk = highestChunkPosition == localChunkPosition;
                            // for Sunlight Y
                            for (globalPosition.x = sunlightStartHeight; globalPosition.x >= sunlightEndHeight; globalPosition.x--)
                            {
                                ShineLight(PlanetSide.Right, ref chunkLights, ref shineLightInput,
                                    in chunks2, in chunkNeighbors, // in chunkPositions,
                                    globalPosition, voxelDimensions, localChunkPosition,
                                    lowestLight, waterDecreaseRate, in isLightPassThroughs);
                            }
                        }
                    }
                }*/