using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels.Lighting.Authoring;

namespace Zoxel.Voxels.Lighting
{
    //! Voxels that emit light will set voxelLights value during build stp.
	[UpdateAfter(typeof(SunlightSystem))]
    [BurstCompile, UpdateInGroup(typeof(VoxelLightsSystemGroup))]
    public partial class TorchLightSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
		private EntityQuery voxesQuery;
		private EntityQuery voxelQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<VoxelLinks>());
            voxelQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Voxel>());
            RequireForUpdate<LightsSettings>();
            RequireForUpdate(processQuery);
            RequireForUpdate(voxesQuery);
            RequireForUpdate(voxelQuery);
        }

        [BurstCompile]
		protected override void OnUpdate()
		{
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<TorchLightsBuilder>(processQuery);
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AddComponent<PropogateLights>(processQuery);
            var lightsSettings = GetSingleton<LightsSettings>();
            if (lightsSettings.disableTorchLight)
            {
                return;
            }
			var torchLight = lightsSettings.torchLight;
			var lowestLight = lightsSettings.lowestLight;
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxelLinks = GetComponentLookup<VoxelLinks>(true);
            voxEntities.Dispose();
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxelEmitLights = GetComponentLookup<VoxelEmitLight>(true);
            voxelEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<UpdateChunkNeighbors>()
                .WithAll<TorchLightsBuilder>()
                .ForEach((ref ChunkLights chunkLights, in Chunk chunk, in VoxLink voxLink) =>
			{
                if (!voxelLinks.HasComponent(voxLink.vox))
                {
                    return;
                }
                var voxelDimensions = chunk.voxelDimensions;
                byte voxelMetaID;
                var voxelArrayIndex = 0;
                int3 localPosition;
                var voxelEntities2 = voxelLinks[voxLink.vox].voxels;
                // this methods faster than the other, maybe due to the small amounts of emissive blocks
                var isEmitLights = new NativeList<byte>();
                for (byte i = 0; i < voxelEntities2.Length; i++)
                {
                    var voxelEntity = voxelEntities2[i];
                    if (voxelEmitLights.HasComponent(voxelEntity))
                    {
                        isEmitLights.Add(1);
                    }
                    else
                    {
                        isEmitLights.Add(0);
                    }
                }
                for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
                {
                    for (localPosition.y = 0; localPosition.y < voxelDimensions.y; localPosition.y++)
                    {
                        for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
                        {
                            voxelMetaID = chunk.voxels[voxelArrayIndex];
                            if (voxelMetaID == 0)
                            {
                                voxelArrayIndex++;
                                continue;
                            }
                            if (isEmitLights[voxelMetaID - 1] == 1 && chunkLights.lights[voxelArrayIndex] < torchLight)
                            {
                                chunkLights.lights[voxelArrayIndex] = torchLight;
                            }
                            voxelArrayIndex++;
                        }
                    }
                }
				isEmitLights.Dispose();
			})  .WithReadOnly(voxelLinks)
                .WithReadOnly(voxelEmitLights)
                .ScheduleParallel(Dependency);
		}
    }
}