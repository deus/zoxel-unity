using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels.Lighting
{
    //! Disposes of ChunkLights.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ChunkLightsDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
                .WithAll<DestroyEntity, ChunkLights>()
                .ForEach((in ChunkLights chunkLights) =>
			{
                chunkLights.DisposeFinal();
			}).ScheduleParallel();
		}
	}
}