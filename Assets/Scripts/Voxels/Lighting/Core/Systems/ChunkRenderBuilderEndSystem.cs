using Unity.Entities;
using Unity.Burst;
using Zoxel.Rendering;

namespace Zoxel.Voxels.Lighting
{
    //! Completes the ChunkRenderBuilder process.
    /**
    *   Removes EntityBusy from Chunk entity.
    *   Adds MeshDirty to update the ZoxMesh.
    */
    [UpdateAfter(typeof(ChunkMeshLightSmoothSystem))]
    [BurstCompile, UpdateInGroup(typeof(VoxelBuilderSystemGroup))]
    public partial class ChunkRenderBuilderEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, in ChunkRenderBuilder chunkRenderBuilder) =>
            {
				if (chunkRenderBuilder.chunkRenderBuildState != (byte) ChunkRenderBuildState.CompletedChunkRenderBuilder)
                {
                    return;
                }
                // UnityEngine.Debug.LogError(chunkLink.chunk.Index + " ChunkRenderBuilder End: " + elapsedTime);  
                PostUpdateCommands.RemoveComponent<ChunkRenderBuilder>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<MeshDirty>(entityInQueryIndex, e);
                #if DEBUG_VOXEL_PLACEMENT
                UnityEngine.Debug.LogError(chunkLink.chunk.Index + " has Finished Voxel Placement at: " + elapsedTime);
                #endif
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}