using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels.Lighting
{
	//! Holds Chunk light data.
	public struct ChunkLights : IComponentData
	{
		//! The Chunk size.
		public int3 voxelDimensions;
		//! An array of lights based on chunk size.
		public BlitableArray<byte> lights;

		public ChunkLights(int3 voxelDimensions, byte lowestLight, bool disableVoxelLighting)
		{
			this.voxelDimensions = voxelDimensions;
			int xyzSize = (int)(voxelDimensions.x * voxelDimensions.y * voxelDimensions.z);
			this.lights = new BlitableArray<byte>(xyzSize, Allocator.Persistent);
			// for now randomize the light values
			if (disableVoxelLighting)
			{
				for (int i = 0; i < lights.Length; i++)
				{
					lights[i] = 16;
				}
			}
			else
			{
				for (int i = 0; i < lights.Length; i++)
				{
					lights[i] = lowestLight;
				}
			}
		}

		//! Disposes of our light array.
		public void Dispose()
		{
			lights.Dispose();
		}
		
		public void DisposeFinal()
		{
			lights.DisposeFinal();
		}
		

		public void Initialize(int3 voxelDimensions, byte lowestLight, bool disableVoxelLighting)
		{
			this.voxelDimensions = voxelDimensions;
			int xyzSize = (int)(voxelDimensions.x * voxelDimensions.y * voxelDimensions.z);
			this.lights = new BlitableArray<byte>(xyzSize, Allocator.Persistent);
			// for now randomize the light values
			if (disableVoxelLighting)
			{
				for (int i = 0; i < lights.Length; i++)
				{
					lights[i] = 16;
				}
			}
			else
			{
				for (int i = 0; i < lights.Length; i++)
				{
					lights[i] = lowestLight;
				}
			}
		}

		//! Sets all light bytes to a single value.
		public void SetAllLights(byte value)
		{
			for (int i = 0; i < lights.Length; i++)
			{
				lights[i] = value;
			}
		}
	}
}