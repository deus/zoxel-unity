using Unity.Entities;

namespace Zoxel.Voxels.Lighting
{
    //! An event to build chunk torches.
    public struct TorchLightsBuilder : IComponentData { }
}