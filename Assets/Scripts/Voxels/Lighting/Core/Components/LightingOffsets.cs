using Unity.Entities;

namespace Zoxel.Voxels.Lighting
{
	public static class LightingOffsets
	{
        //! Each face has 4 verts, and 16 possible offsets, 6 * 16 is 96 points.
        public static readonly int3[] lightingOffsets = new int3[]
        {
            // 16 offsets per side, 4 per vertex!
            // Side 0 - Left
            // vert (0, 0, 0) - bottom-right
            new int3(-1, 0, 0),   // left-only
            new int3(-1, -1, 0),    // left-down
            new int3(-1, 0, -1),    // left-back
            new int3(-1, -1, -1),   // left-down-back
            // vert (0, 1, 0) - top-right
            new int3(-1, 0, 0),     // left-only
            new int3(-1, 1, 0),     // left-up
            new int3(-1, 0, -1),    // left-back
            new int3(-1, 1, -1),    // left-up-back
            // vert (0, 1, 1) - top-left
            new int3(-1, 0, 0),   // left-only
            new int3(-1, 1, 0),   // left-up
            new int3(-1, 0, 1),   // left-forward
            new int3(-1, 1, 1),   // left-up-forward
            // vert (0, 0, 1) - bottom-left
            new int3(-1, 0, 0),   // left-only
            new int3(-1, -1, 0),  // left-down
            new int3(-1, 0, 1),   // left-forward
            new int3(-1, -1, 1),  // left-down-forward

            // Side 0 - Right
            // vert (0, 0, 0) - bottom-right
            new int3(1, 0, 0),   // left-only
            new int3(1, -1, 0),    // left-down
            new int3(1, 0, -1),    // left-back
            new int3(1, -1, -1),   // left-down-back
            // vert (0, 1, 0) - top-right
            new int3(1, 0, 0),     // left-only
            new int3(1, 1, 0),     // left-up
            new int3(1, 0, -1),    // left-back
            new int3(1, 1, -1),    // left-up-back
            // vert (0, 1, 1) - top-left
            new int3(1, 0, 0),   // left-only
            new int3(1, 1, 0),   // left-up
            new int3(1, 0, 1),   // left-forward
            new int3(1, 1, 1),   // left-up-forward
            // vert (0, 0, 1) - bottom-left
            new int3(1, 0, 0),   // left-only
            new int3(1, -1, 0),  // left-down
            new int3(1, 0, 1),   // left-forward
            new int3(1, -1, 1),  // left-down-forward

            // Side 0 - Down
            // vert (0, 0, 0)
            new int3(0, -1, 0),   // left-only
            new int3(-1, -1, 0),    // left-down
            new int3(0, -1, -1),    // left-back
            new int3(-1, -1, -1),   // left-down-back
            // vert (0, 1, 0) - top-right
            new int3(0, -1, 0),     // left-only
            new int3(1, -1, 0),     // left-up
            new int3(0, -1, -1),    // left-back
            new int3(1, -1, -1),    // left-up-back
            // vert (0, 1, 1) - top-left
            new int3(0, -1, 0),   // left-only
            new int3(1, -1, 0),   // left-up
            new int3(0, -1, 1),   // left-forward
            new int3(1, -1, 1),   // left-up-forward
            // vert (0, 0, 1) - bottom-left
            new int3(0, -1, 0),   // left-only
            new int3(-1, -1, 0),  // left-down
            new int3(0, -1, 1),   // left-forward
            new int3(-1, -1, 1),  // left-down-forward

            // Side 0 - Up
            // vert (0, 0, 0)
            new int3(0, 1, 0),   // left-only
            new int3(-1, 1, 0),    // left-down
            new int3(0, 1, -1),    // left-back
            new int3(-1, 1, -1),   // left-down-back
            // vert (0, 1, 0) - top-right
            new int3(0, 1, 0),     // left-only
            new int3(1, 1, 0),     // left-up
            new int3(0, 1, -1),    // left-back
            new int3(1, 1, -1),    // left-up-back
            // vert (0, 1, 1) - top-left
            new int3(0, 1, 0),   // left-only
            new int3(1, 1, 0),   // left-up
            new int3(0, 1, 1),   // left-forward
            new int3(1, 1, 1),   // left-up-forward
            // vert (0, 0, 1) - bottom-left
            new int3(0, 1, 0),   // left-only
            new int3(-1, 1, 0),  // left-down
            new int3(0, 1, 1),   // left-forward
            new int3(-1, 1, 1),  // left-down-forward

            // Side 0 - Backward
            // vert (0, 0, 0) - bottom-right
            new int3(0, 0, -1),   // left-only
            new int3(-1, 0, -1),    // left-down
            new int3(0, -1, -1),    // left-back
            new int3(-1, -1, -1),   // left-down-back
            // vert (0, 1, 0) - top-right
            new int3(0, 0, -1),     // left-only
            new int3(1, 0, -1),     // left-up
            new int3(0, -1, -1),    // left-back
            new int3(1, -1, -1),    // left-up-back
            // vert (0, 1, 1) - top-left
            new int3(0, 0, -1),   // left-only
            new int3(1, 0, -1),   // left-up
            new int3(0, 1, -1),   // left-forward
            new int3(1, 1, -1),   // left-up-forward
            // vert (0, 0, 1) - bottom-left
            new int3(0, 0, -1),   // left-only
            new int3(-1, 0, -1),  // left-down
            new int3(0, 1, -1),   // left-forward
            new int3(-1, 1, -1),  // left-down-forward

            // Side 0 - Forward
            // vert (0, 0, 0) - bottom-right
            new int3(0, 0, 1),   // left-only
            new int3(-1, 0, 1),    // left-down
            new int3(0, -1, 1),    // left-back
            new int3(-1, -1, 1),   // left-down-back
            // vert (0, 1, 0) - top-right
            new int3(0, 0, 1),     // left-only
            new int3(1, 0, 1),     // left-up
            new int3(0, -1, 1),    // left-back
            new int3(1, -1, 1),    // left-up-back
            // vert (0, 1, 1) - top-left
            new int3(0, 0, 1),   // left-only
            new int3(1, 0, 1),   // left-up
            new int3(0, 1, 1),   // left-forward
            new int3(1, 1, 1),   // left-up-forward
            // vert (0, 0, 1) - bottom-left
            new int3(0, 0, 1),   // left-only
            new int3(-1, 0, 1),  // left-down
            new int3(0, 1, 1),   // left-forward
            new int3(-1, 1, 1)  // left-down-forward
        };
    }
}