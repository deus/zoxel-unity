using Unity.Entities;

namespace Zoxel.Voxels.Lighting
{
    //! An event to build chunk sunlight.
    public struct ChunkSunlightBuilder : IComponentData { }
}