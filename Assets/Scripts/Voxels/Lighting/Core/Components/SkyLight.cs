using Unity.Entities;

namespace Zoxel.Voxels.Lighting
{
    //! Planet's light used for Sun Lighting.
    public struct Skylight : IComponentData
    {
        public byte skylight;

        public Skylight(byte skylight)
        {
            this.skylight = skylight;
        }
    }
}