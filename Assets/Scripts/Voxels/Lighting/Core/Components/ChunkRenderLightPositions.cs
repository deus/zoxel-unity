using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Voxels.Lighting
{
	public struct ChunkRenderLightPositions
	{
		public BlitableArray<int3> lightPositions;
		public BlitableArray<int3> closestLightPositions;
		
		public ChunkRenderLightPositions(Allocator allocator)
		{
			lightPositions = new BlitableArray<int3>(27, allocator);
			closestLightPositions = new BlitableArray<int3>(4, allocator);
		}

		public void SetPositions(int3 centerVoxelPosition)
		{
			int a = 0;
			for (int i = -1; i <= 1; i++)
			{
				for (int j = -1; j <= 1; j++)
				{
					for (int k = -1; k <= 1; k++)
					{
						lightPositions[a] = centerVoxelPosition + new int3(i, j, k);
						a++;
					}
				}
			}
		}

		public void SetClosestPositions()
		{
			//! \todo Lookup table, based on vert position offset from centre of voxel - i.e. corner goes to 4 positions
			closestLightPositions[0] = lightPositions[0];
			closestLightPositions[1] = lightPositions[1];
			closestLightPositions[2] = lightPositions[2];
			closestLightPositions[3] = lightPositions[3];
		}
	}
}