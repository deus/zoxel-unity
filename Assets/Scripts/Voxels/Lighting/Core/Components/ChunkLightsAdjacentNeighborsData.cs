using Unity.Entities;

namespace Zoxel.Voxels.Lighting
{
	//! Holds ChunkLights Neighbor Data. Used for quick access to surrounding lights.
	public struct ChunkLightsAdjacentNeighborsData
	{
		public readonly ChunkLights chunkLightsLeft;
		public readonly ChunkLights chunkLightsRight;
		public readonly ChunkLights chunkLightsDown;
		public readonly ChunkLights chunkLightsUp;
		public readonly ChunkLights chunkLightsBack;
		public readonly ChunkLights chunkLightsForward;

		public ChunkLightsAdjacentNeighborsData(in ChunkNeighbors chunkNeighbors, in ComponentLookup<ChunkLights> chunkLights)
		{
			if (chunkNeighbors.chunkLeft.Index > 0 && chunkLights.HasComponent(chunkNeighbors.chunkLeft))
			{
				chunkLightsLeft = chunkLights[chunkNeighbors.chunkLeft];
			}
			else
			{
				chunkLightsLeft = new ChunkLights();
			}
			if (chunkNeighbors.chunkRight.Index > 0 && chunkLights.HasComponent(chunkNeighbors.chunkRight))
			{
				chunkLightsRight = chunkLights[chunkNeighbors.chunkRight];
			}
			else
			{
				chunkLightsRight = new ChunkLights();
			}
			if (chunkNeighbors.chunkDown.Index > 0 && chunkLights.HasComponent(chunkNeighbors.chunkDown))
			{
				chunkLightsDown = chunkLights[chunkNeighbors.chunkDown];
			}
			else
			{
				chunkLightsDown = new ChunkLights();
			}
			if (chunkNeighbors.chunkUp.Index > 0 && chunkLights.HasComponent(chunkNeighbors.chunkUp))
			{
				chunkLightsUp = chunkLights[chunkNeighbors.chunkUp];
			}
			else
			{
				chunkLightsUp = new ChunkLights();
			}
			if (chunkNeighbors.chunkBack.Index > 0 && chunkLights.HasComponent(chunkNeighbors.chunkBack))
			{
				chunkLightsBack = chunkLights[chunkNeighbors.chunkBack];
			}
			else
			{
				chunkLightsBack = new ChunkLights();
			}
			if (chunkNeighbors.chunkForward.Index > 0 && chunkLights.HasComponent(chunkNeighbors.chunkForward))
			{
				chunkLightsForward = chunkLights[chunkNeighbors.chunkForward];
			}
			else
			{
				chunkLightsForward = new ChunkLights();
			}
		}

		//! Returns the light value at a location. Returns 0 if no ChunkLights there.
		public byte GetLightValue(in ChunkLights localChunkLights, int3 localPosition, int3 voxelDimensions)
		{
			var chunk2 = GetChunkAdjacent(in localChunkLights, ref localPosition, voxelDimensions);
			if (chunk2.lights.Length == 0)
			{
				return 0;
			}
			return chunk2.lights[VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions)];
		}

		public ChunkLights GetChunkAdjacent(in ChunkLights localChunkLights, ref int3 localPosition, int3 voxelDimensions)
		{
			if (VoxelUtilities.InBounds(localPosition, voxelDimensions))
			{
				return localChunkLights;
			}
			else if (localPosition.x == -1)
			{
				localPosition.x += voxelDimensions.x;
				return chunkLightsLeft;
			}
			else if (localPosition.x == voxelDimensions.x)
			{
				localPosition.x -= voxelDimensions.x;
				return chunkLightsRight;
			}
			else if (localPosition.y == -1)
			{
				localPosition.y += voxelDimensions.y;
				return chunkLightsDown;
			}
			else if (localPosition.y == voxelDimensions.y)
			{
				localPosition.y -= voxelDimensions.y;
				return chunkLightsUp;
			}
			else if (localPosition.z == -1)
			{
				localPosition.z += voxelDimensions.z;
				return chunkLightsBack;
			}
			else if (localPosition.z == voxelDimensions.z)
			{
				localPosition.z -= voxelDimensions.z;
				return chunkLightsForward;
			}
			else
			{
				return new ChunkLights();
			}
		}
	}
}