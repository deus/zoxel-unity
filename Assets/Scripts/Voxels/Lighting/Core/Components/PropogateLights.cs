using Unity.Entities;

namespace Zoxel.Voxels.Lighting
{
    //! An event for propogating light.
    public struct PropogateLights : IComponentData
    {
        public int voxelArrayIndex;
        public int3 localPosition;
    }
}