using Unity.Entities;

namespace Zoxel.Voxels
{
    //! Links voxels to realm after spawned.
    public struct OnVoxelsSpawned : IComponentData
    {
        public Entity realm;
        public int spawned;

        public OnVoxelsSpawned(Entity realm, int spawned)
        {
            this.realm = realm;
            this.spawned = spawned;
        }
    }
}