using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Generates a Vox's shape.
	public struct GenerateVoxData : IComponentData
	{
		public byte generationType;

		public GenerateVoxData(byte generationType)
		{
			this.generationType = generationType;
		}
	}
}