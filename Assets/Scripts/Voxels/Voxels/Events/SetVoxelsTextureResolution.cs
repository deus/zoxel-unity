using Unity.Entities;

namespace Zoxel.Voxels
{
    public struct SetVoxelsTextureResolution : IComponentData
    {
        public byte textureResolution;
        
        public SetVoxelsTextureResolution(byte textureResolution)
        {
            this.textureResolution = textureResolution;
        }
    }
}