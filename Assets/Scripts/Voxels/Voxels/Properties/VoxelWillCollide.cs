using Unity.Entities;
//! \todo Rename to VoxelCollider, rename other VoxelCollider to just Collider!

namespace Zoxel.Voxels
{
	//! A tag so that a Voxel collides with things.
	public struct VoxelWillCollide : IComponentData { }
}