using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Voxel gets removed when solid does.
	public struct VoxelAttachToSolid : IComponentData { }	// base ground on rotation as well
}