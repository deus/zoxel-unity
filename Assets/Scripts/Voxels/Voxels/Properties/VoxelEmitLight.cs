using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Voxel Emission Light Properties.
	/**
	*	\todo Different emission properties (colours, brightness)
	*	\todo MinivoxParticles Component. Make the particles spawn out of the voxels themselves.
	*/
	public struct VoxelEmitLight : IComponentData { }
}