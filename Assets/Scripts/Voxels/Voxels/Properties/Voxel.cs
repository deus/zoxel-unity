using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Voxel meta data.
	/**
	*	\todo Fix minivox item uis.
	*	\todo Break this struct up into seperate components.
	*/
	public struct Voxel : IComponentData
	{
		public byte type;			// Voxel Type! - Relates to a voxel enum.
		public byte biomeIndex;		// chunkBiome created for - Replace this with BiomeLink
		public byte textureType;
		// Replace with EntityColors - used for any procedural generation
		public Color color;
		public Color secondaryColor;
		// Stats
		public int startingHealth;	// used when destroying them
		// More Properties
		public byte functionType;
		public byte raycastMode;
		public byte canRotate;
	}
}