using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Voxel Soil occasionally turns into Grass with sunlight, love and care.
	/**
	*	\todo Link a soil to types of grass that it can grow.
	*/
	public struct VoxelSoil : IComponentData { }
}