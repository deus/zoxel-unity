using Unity.Entities;

namespace Zoxel.Voxels
{
	//! A tag for water voxels.
	public struct WaterVoxel : IComponentData { }
}