using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Voxel Grass occasionally turns into Soil without sunlight, love and care.
	public struct VoxelGrass : IComponentData { }
}