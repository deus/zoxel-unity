using Unity.Entities;

namespace Zoxel.Voxels
{
	//! A tag for grass voxels.
	public struct GrassVoxel : IComponentData { }
}