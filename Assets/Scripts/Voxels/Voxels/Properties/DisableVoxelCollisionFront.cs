using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Disables front of voxel collisions.
	public struct DisableVoxelCollisionFront : IComponentData { }
}