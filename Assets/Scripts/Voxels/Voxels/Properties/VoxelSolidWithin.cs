using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Can a character be inside this voxel?
	public struct VoxelSolidWithin : IComponentData { }
}