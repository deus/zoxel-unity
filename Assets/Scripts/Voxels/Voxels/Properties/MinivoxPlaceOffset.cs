using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Placement offset in grid.
	public struct MinivoxPlaceOffset : IComponentData
	{
		public int3 placeOffset;

		public MinivoxPlaceOffset(int3 placeOffset)
		{
			this.placeOffset = placeOffset;
		}
	}
}