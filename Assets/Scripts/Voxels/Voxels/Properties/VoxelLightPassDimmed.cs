using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Voxel Light Dimming Pass Properties.
	public struct VoxelLightPassDimmed : IComponentData { }
}