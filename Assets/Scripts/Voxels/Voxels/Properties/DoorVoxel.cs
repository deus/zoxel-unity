using Unity.Entities;

namespace Zoxel.Voxels
{
	//! A tag for door voxels.
	public struct DoorVoxel : IComponentData { }
}