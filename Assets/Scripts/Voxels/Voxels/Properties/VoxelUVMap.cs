using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Voxels
{
    //! A texture map used for voxels.
    /**
    *   \todo Support multiple UVmaps per voxel based on direction byte.
    */
    public struct VoxelUVMap : IComponentData
    {
        //! An array of uvs for texturing. 24 in Length. 1 for each vertex.
        public BlitableArray<float2> uvs;

        public void Initialize()
        {
            Dispose();
            uvs = new BlitableArray<float2>(24, Allocator.Persistent);    // 6 sides, 4 corner points of uvs
        }

        public void Dispose()
        {
            if (uvs.Length > 0)
            {
                uvs.Dispose();
            }
        }

        public void SetSide(float2 size, float2 position, TextureVoxelSide side)
        {
            int uvPointer = 0;
            if (side == TextureVoxelSide.Left)
            {
                uvPointer = 0;
            }
            else if (side == TextureVoxelSide.Right)
            {
                uvPointer += 4;
            }
            else if (side == TextureVoxelSide.Down)
            {
                uvPointer += 8;
            }
            else if (side == TextureVoxelSide.Up)
            {
                uvPointer += 12;
            }
            else if (side == TextureVoxelSide.Forward)
            {
                uvPointer += 16;
            }
            else if (side == TextureVoxelSide.Back)
            {
                uvPointer += 20;
            }
            if (side == TextureVoxelSide.Left || side == TextureVoxelSide.Right)
            {
                // reverse
                uvs[uvPointer + 3] = new float2(position.x, position.y);
                uvs[uvPointer] = new float2(position.x + size.x, position.y);
                uvs[uvPointer + 1] = new float2(position.x + size.x, position.y + size.y);
                uvs[uvPointer + 2] = new float2(position.x, position.y + size.y);
            }
            else
            {
                uvs[uvPointer] = new float2(position.x, position.y);
                uvs[uvPointer + 1] = new float2(position.x + size.x, position.y);
                uvs[uvPointer + 2] = new float2(position.x + size.x, position.y + size.y);
                uvs[uvPointer + 3] = new float2(position.x, position.y + size.y);
            }
        }
    }
}