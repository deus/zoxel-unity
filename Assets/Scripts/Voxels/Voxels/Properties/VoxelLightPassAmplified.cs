using Unity.Entities;

namespace Zoxel.Voxels
{
	//! Causes light to increase as it passes through!
	public struct VoxelLightPassAmplified : IComponentData { }
}