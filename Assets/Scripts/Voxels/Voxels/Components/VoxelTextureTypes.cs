using Unity.Entities;

namespace Zoxel.Voxels
{
    public struct VoxelTextureTypes : IComponentData
    {
        public BlitableArray<byte> textureTypes;

        public void Dispose()
        {
            if (textureTypes.Length > 0)
            {
                textureTypes.Dispose();
            }
        }
    }
}