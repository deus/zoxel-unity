using Unity.Entities;

namespace Zoxel.Voxels
{
    //! Holds resolution for textures
    public struct VoxelTextureResolution : IComponentData
    {
        public int resolution;

        public VoxelTextureResolution(int resolution)
        {
            this.resolution = resolution;
        }
    }
}