using Unity.Entities;

namespace Zoxel.Voxels
{
    public struct VoxelLink : IComponentData
    {
        public Entity voxel;

        public VoxelLink(Entity voxel)
        {
            this.voxel = voxel;
        }
    }
}