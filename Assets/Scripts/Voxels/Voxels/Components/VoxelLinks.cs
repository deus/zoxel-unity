using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Voxels
{
    //! Links a Realm to voxels.
    public struct VoxelLinks : IComponentData
    {
        public BlitableArray<Entity> voxels;

        public void Dispose()
        {
            if (voxels.Length > 0)
            {
                voxels.Dispose();
            }
        }

        public void InitializeVoxelsGroup(int count)
        {
            Dispose();
            this.voxels = new BlitableArray<Entity>(count, Allocator.Persistent);
            for (int i = 0; i < count; i++)
            {
                this.voxels[i] = new Entity();
            }
        }
    }

}