using Unity.Entities;

namespace Zoxel.Voxels
{
    //! A portal type voxel tag.
    public struct PortalVoxel : IComponentData { }
}