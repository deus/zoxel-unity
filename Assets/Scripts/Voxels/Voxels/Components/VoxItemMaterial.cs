using Unity.Entities;
using System;

namespace Zoxel.Voxels
{
    //! Holds item material.
    public struct VoxItemMaterial : ISharedComponentData, IEquatable<VoxItemMaterial>
    {
        public UnityEngine.Material itemMaterial;

        public VoxItemMaterial(UnityEngine.Material itemMaterial)
        {
            this.itemMaterial = itemMaterial;
        }

        public bool Equals(VoxItemMaterial obj)
        {
            return itemMaterial == obj.itemMaterial;
        }

        public override int GetHashCode()
        {
            return itemMaterial.GetHashCode(); //1371622046 + EqualityComparer<Mesh>.Default.GetHashCode(mesh);
        }
    }
}