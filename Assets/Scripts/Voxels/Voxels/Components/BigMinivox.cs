using Unity.Entities;

namespace Zoxel.Voxels // .Minivoxes
{
	//! A Minivox that takes up more then one block size. (This is added to spawned Minivox Voxels)
	public struct BigMinivox : IComponentData { }
}