using Unity.Entities;

namespace Zoxel.Voxels
{
    //! What type of mesh the voxel has.
	/**
	*	\todo Link to a mesh entity, unlinked for cube.
	*/
    public struct VoxelMeshType : IComponentData
	{
		public byte meshType;		// later can link it to a actual mesh

		public VoxelMeshType(byte meshType)
		{
			this.meshType = meshType;
		}
	}
}