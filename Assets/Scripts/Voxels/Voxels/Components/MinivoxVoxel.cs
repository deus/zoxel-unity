using Unity.Entities;

namespace Zoxel.Voxels
{
    //! Attached to a voxel that is of type Minivox.
    public struct MinivoxVoxel : IComponentData { }
}