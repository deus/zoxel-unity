namespace Zoxel.Voxels
{
	//! Temporary enum for voxels!
	/**
	*	Later on I will remove this and just define them based on component datas.
	*	\todo Generate Ore voxels.
	*	\todo Generate MossStone voxels.
	*	\todo Generate cobble stone voxels.
	*	\todo Generate Sandstone voxels.
	*	\todo Generate Trophy voxel - a decoration.
	*/
	public enum VoxelType // : byte
	{
		Soil,
		Grass,
		Sand,		// bottom of sea
		Stone,		// below soil
		Bricks,		// harder to destroy
		Tiles,		// bottom of town floors
		Wood,		// core of trees
		Leaf,		// top of trees
		Bedrock,	// bottom of earth
		Water,		// bottom of earth
		Ore,		// todo: Generate ore texture todo: ore to drop special ore items
		MossStone,	// mosstone for ruins left behind
		SandStone,
		CobbleStone,
		Portal,
        Torch,
        Chest,
        Weeds,
		Door,
		Bed,
		Decoration	// this is for - teddy bears, paintings, etc, things that a have no functionality but just a vox model
	}
}