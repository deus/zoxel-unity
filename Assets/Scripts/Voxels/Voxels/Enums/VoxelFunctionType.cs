namespace Zoxel.Voxels
{
	public enum VoxelFunctionType : byte
	{
		None,
		Chest,
		Door,
		Bed,
		Torch,
		Portal
	}
}