namespace Zoxel.Voxels
{
    public enum VoxelPrefabType : byte
    {
        SingleTexture,
        MultiTexture,
        Minivox
    }
}