namespace Zoxel.Voxels
{
	//! A basic type of mesh differentiation for voxels.
	public static class MeshType
	{
		public const byte Block = 0;
		public const byte Minivox = 1;
	}
}