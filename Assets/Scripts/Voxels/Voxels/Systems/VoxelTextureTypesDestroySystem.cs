using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
	//! Disposes of VoxelTextureTypes on DestroyEntity event.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class VoxelTextureTypesDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
				.WithAll<DestroyEntity, VoxelTextureTypes>()
				.ForEach((in VoxelTextureTypes voxelTextureTypes) =>
			{
				voxelTextureTypes.Dispose();
			}).ScheduleParallel();
		}
	}
}