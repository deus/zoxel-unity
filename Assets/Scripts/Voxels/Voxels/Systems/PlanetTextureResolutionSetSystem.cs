using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
    //! Seets the highest resolution for texture resolutions.
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class PlanetTextureResolutionSetSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref VoxelLinks voxelLinks, in SetVoxelsTextureResolution setVoxelsTextureResolution) =>
            {
                PostUpdateCommands.RemoveComponent<SetVoxelsTextureResolution>(entityInQueryIndex, e);
                //! \todo Fix Texture Resizing algorithm.
                /*if (voxelLinks.textureResolution != setVoxelsTextureResolution.textureResolution)
                {
                    voxelLinks.textureResolution = setVoxelsTextureResolution.textureResolution;
                    // UnityEngine.Debug.LogError("Setting Voxel Texture Resolution to: " + voxelLinks.textureResolution);
                    // for all voxels, set texture resolution
                    for (int i = 0; i < voxelLinks.voxels.Length; i++)
                    {
                        var voxelEntity = voxelLinks.voxels[i];
                        if (HasComponent<Voxel>(voxelEntity))
                        {
                            PostUpdateCommands.AddComponent(entityInQueryIndex, voxelEntity, new ResizeChildrenTextures(voxelLinks.textureResolution));
                        }
                    }
                    // Since only rescaling resolution, no need to update UVs of voxels!
                    // PostUpdateCommands.AddComponent(entityInQueryIndex, e, new VoxelLinksDirty(0));
                }*/
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}