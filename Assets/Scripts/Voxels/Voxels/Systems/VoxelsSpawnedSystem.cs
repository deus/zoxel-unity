using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Voxels
{
    //! Spawned voxels, must find them.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxelsSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxelsSpawnedQuery;
        private EntityQuery voxelQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			voxelsSpawnedQuery = GetEntityQuery(ComponentType.ReadOnly<OnVoxelsSpawned>());
            voxelQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Voxel>(),
                ComponentType.ReadOnly<VoxLink>());
            RequireForUpdate(processQuery);
            RequireForUpdate(voxelsSpawnedQuery);
            RequireForUpdate(voxelQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxelsSpawnedEntities = voxelsSpawnedQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var onVoxelsSpawned2 = GetComponentLookup<OnVoxelsSpawned>(true);
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxels = GetComponentLookup<Voxel>(true);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            var voxelIDs = GetComponentLookup<ZoxID>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<Realm>()
                .ForEach((Entity e, int entityInQueryIndex, ref VoxelLinks voxelLinks) =>
            {
                var didSpawn = false;
                var targetCount = 0;
                for (int i = 0; i < voxelsSpawnedEntities.Length; i++)
                {
                    var e2 = voxelsSpawnedEntities[i];
                    var onVoxelsSpawned = onVoxelsSpawned2[e2];
                    if (onVoxelsSpawned.realm == e)
                    {
                        didSpawn = true;
                        targetCount += onVoxelsSpawned.spawned;
                    }
                }
                if (!didSpawn)
                {
                    return;
                }
                voxelLinks.InitializeVoxelsGroup(targetCount);
                var count = 0;
                for (int i = 0; i < voxelEntities.Length; i++)
                {
                    var e2 = voxelEntities[i];
                    var voxLink = voxLinks[e2];
                    if (voxLink.vox == e)
                    {
                        var voxel = voxels[e2];
                        var voxelID = voxelIDs[e2].id - 1;
                        if (voxelID >= 0 && voxelID < targetCount)
                        {
                            voxelLinks.voxels[voxelID] = e2;
                            count++;
                            if (count >= targetCount)
                            {
                                break;
                            }
                        }
                    }
                }
                // Destroy event entities
                if (count == targetCount)
                {
                    for (int i = 0; i < voxelsSpawnedEntities.Length; i++)
                    {
                        var e2 = voxelsSpawnedEntities[i];
                        var onVoxelsSpawned = onVoxelsSpawned2[e2];
                        if (onVoxelsSpawned.realm == e)
                        {
                            PostUpdateCommands.DestroyEntity(entityInQueryIndex, e2);
                        }
                    }
                    // UnityEngine.Debug.LogError("OnVoxelsSpawned: " + targetCount);
                    // PostUpdateCommands.AddComponent<SpawnTilemaps>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(voxelsSpawnedEntities)
                .WithReadOnly(onVoxelsSpawned2)
                .WithReadOnly(voxelEntities)
                .WithReadOnly(voxels)
                .WithReadOnly(voxLinks)
                .WithReadOnly(voxelIDs)
                .WithDisposeOnCompletion(voxelsSpawnedEntities)
                .WithDisposeOnCompletion(voxelEntities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}