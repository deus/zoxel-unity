using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Voxels
{
	//! Disposes of VoxelUVMap on DestroyEntity event.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class VoxelUVMapDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
				.WithAll<DestroyEntity, VoxelUVMap>()
				.ForEach((in VoxelUVMap voxelUVMap) =>
			{
				voxelUVMap.Dispose();
			}).ScheduleParallel();
		}
	}
}