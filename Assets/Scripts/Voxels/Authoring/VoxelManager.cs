using Unity.Mathematics;
using UnityEngine;
using System.Collections.Generic;

namespace Zoxel.Voxels
{
    //! Manages VoxelSettings.
    public partial class VoxelManager : MonoBehaviour
    {
        public static VoxelManager instance;
        [UnityEngine.SerializeField] public VoxelSettingsDatam VoxelSettings;
        [UnityEngine.HideInInspector] public VoxelSettings realVoxelSettings;
        public List<VoxDatam> minionModels; // can we do this
        [UnityEngine.SerializeField] public VoxelMaterialsDatam voxelMaterials;
        // GUI
        private int fontSize = 26;
        private UnityEngine.Color fontColor = UnityEngine.Color.green;

        public VoxelSettings voxelSettings
        {
            get
            { 
                return realVoxelSettings;
            }
        }

        public void Awake()
        {
            instance = this;
            realVoxelSettings = VoxelSettings.voxelSettings;
        }

        public void OnGUI()
        {
            GUI.skin.label.fontSize = fontSize;
            GUI.color = fontColor;
            DrawUI();
        }

        public void DrawUI()
        {
            realVoxelSettings.DrawUI();
            realVoxelSettings.DrawDisableUI();
        }

        /*public float3 GetVoxelScale()
        {
            return (new float3(1f / voxelSettings.voxelScale, 1f / voxelSettings.voxelScale, 1f / voxelSettings.voxelScale));
        }

        public float3 GetVoxelSize()
        {
            return (new float3(voxelSettings.voxelScale, voxelSettings.voxelScale, voxelSettings.voxelScale));
        }*/
    }
}