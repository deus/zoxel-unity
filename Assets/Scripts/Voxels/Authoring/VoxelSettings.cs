using UnityEngine;
using Unity.Mathematics;
using System;

namespace Zoxel.Voxels
{
    //! Contains voxel settings data.
    /**
    *   \todo Seperate materials from this struct.
    */
    [Serializable]
    public struct VoxelSettings
    {
        [Header("Planet & Streaming")]
        public int3 voxelDimensions;            // 16, 16, 16
        public float3 planetVoxelScale;         // 0.75, 0.75, 0.75
        public float timePerStream;             // 4
        public bool isDrawWireframes;           // for chunk meshes
        public bool isDrawChunkColliders;       
        public bool disableWorldStreaming;
        public bool disableWorlds;
        public bool disablePlanetIO;
        public bool disablePlanetChunkIO;

        public int[] textureLowerDistances;     /// 1, 3, 5, 7, 8, 9 (make this a const)

        [Header("Raycasting")]
        public float raycastLength;             // 6
        public bool disableEntityHighlight;     // false

        [Header("Gravity")]
        public byte maxGravityDepth;            // 3 (4 when i can fix this)
        public bool isDebugGravity;             // false
        public byte debugGravityViewDistance;   // 0
        public byte debugGravityDepth;          // 4
        public bool debugGravityShowNone;       // false

        [Header("Debug")]
        public bool isDebugStructures;
        public bool disableLightNextToEdgeChunks;
        public bool disableVoxelPlacement;
        public bool disablePortals;
        public bool disableRaycasting;

        //! \todo Remove lighting disables out of Voxels Namespace.
        [Header("Voxel Lighting Disables")]
        public bool disableCharacterLighting;
        public bool disableVoxelLighting;       
        public bool disableSmoothLighting;

        // Cheats
        [Header("Cheats")]
        public int3 eraseBrushSize;
        public bool isStatistics;

        [Header("BoneLinks Debug")]
        public bool isColourWeights;
        public bool isDrawBoneBoxes;
        public bool disableAnimations;
        public bool disableModels;
        public bool disableGear;

        public void DrawUI()
        {
            isStatistics = GUILayout.Toggle(isStatistics, " [statistics] ");
            if (isStatistics)
            {
                DrawStatistics();
                // GUILayout.Label("   > Chunk Renders Building [" + ChunkMeshLightBuilderSystem.chunksCount + "]");
                // GUILayout.Label("   > Chunk Lights Building [" + PropogateLightSystem.chunksCount + "]");
            }
            else
            {
                //GUILayout.Label("   Minivox Render Distance");
                // minivoxDistance = int.Parse(GUILayout.TextField(minivoxDistance.ToString()));
                //GUILayout.Label("   Character Load Distance");
                //characterRenderDistance = int.Parse(GUILayout.TextField(characterRenderDistance.ToString()));
                GUILayout.Label(" [debug visuals] ");
                isColourWeights = GUILayout.Toggle(isColourWeights, "Color Body Weights");

                GUILayout.Label("   Planet Voxel Scale");
                var newPlanetScale = int.Parse(GUILayout.TextField(planetVoxelScale.x.ToString()));
                if (newPlanetScale != planetVoxelScale.x)
                {
                    planetVoxelScale.x = newPlanetScale;
                    planetVoxelScale.y = newPlanetScale;
                    planetVoxelScale.z = newPlanetScale;
                }
            }
        }

        private void DrawStatistics()
        {
            GUILayout.Label("   > Voxes [" + VoxelSystemGroup.voxesCount + "]");
            GUILayout.Label("   > Chunks [" + VoxelSystemGroup.chunksCount + "]");
            GUILayout.Label("   > ChunkRenders [" + VoxelSystemGroup.chunkRendersCount + "]");
            GUILayout.Label("   > Busy Chunks [" + VoxelSystemGroup.busyChunksCount + "]");
            GUILayout.Label("   > Busy Chunk Renders [" + VoxelSystemGroup.busyChunkRendersCount + "]");
            GUILayout.Label("   > Chunk MeshOnly [" + VoxelSystemGroup.chunksMeshOnlyCount + "]");
            GUILayout.Label("   > Chunk LightsOnly [" + VoxelSystemGroup.chunksLightsOnlyCount + "]");
            GUILayout.Label("   > Voxels (Meta) [" + VoxelSystemGroup.voxelsCount + "]");
            GUILayout.Label("   > Minivoxes [" + VoxelSystemGroup.minivoxesCount + "]");
            GUILayout.Label("   > BigMinivoxes [" + VoxelSystemGroup.bigMinivoxesCount + "]");
            GUILayout.Label("   > Grasses [" + VoxelSystemGroup.grassCount + "]");
            GUILayout.Label("   > Grass Strands [" + VoxelSystemGroup.grassStrandCount + "]");
            GUILayout.Label("   > Chunks Spawned [" + VoxelSystemGroup.chunksSpawnedCount + "]");
            GUILayout.Label("   > Chunk Characters Spawned [" + VoxelSystemGroup.chunkCharactersSpawnedCount + "]");
            GUILayout.Label("   > Minivoxes Spawned [" + VoxelSystemGroup.minivoxesSpawnedCount + "]");
        }

        public void DrawDisableUI()
        {
            if (isStatistics)
            {
                return;
            }
            GUILayout.Label(" [debug voxels] ");
            // disableWorlds = GUILayout.Toggle(disableWorlds, "Disable Worlds");
            //disableMinivoxes = GUILayout.Toggle(disableMinivoxes, "Disable Minivoxes");
            disableVoxelPlacement = GUILayout.Toggle(disableVoxelPlacement, "Disable Voxel Placement");
            //disableBigMinivoxes = GUILayout.Toggle(disableBigMinivoxes, "Disable BigMinivoxes");
            disablePortals = GUILayout.Toggle(disablePortals, "Disable Portals");
            
            GUILayout.Label(" [debug skeletons] ");
            disableAnimations = GUILayout.Toggle(disableAnimations, "Disable Animations");
            disableGear = GUILayout.Toggle(disableGear, "Disable Gear");
            disableModels = GUILayout.Toggle(disableModels, "Disable Models");
            
            GUILayout.Label(" [debug lighting] ");
            disableCharacterLighting = GUILayout.Toggle(disableCharacterLighting, "Disable Character Lighting");
        }
    }
}


        /*GUILayout.Label(" [Grass] ");
        GUILayout.Label("   Grass Wind Variance");
        grassWindVariance  = float.Parse(GUILayout.TextField(grassWindVariance.ToString()));
        GUILayout.Label("   Grass Strand Color Variance");
        grassStrandColorVariance  = float.Parse(GUILayout.TextField(grassStrandColorVariance.ToString()));
        GUILayout.Label("   grassWindSpeed");
        grassWindSpeed  = float.Parse(GUILayout.TextField(grassWindSpeed.ToString()));
        GUILayout.Label("   grassWindTick");
        grassWindTick.x  = float.Parse(GUILayout.TextField(grassWindTick.x.ToString()));
        grassWindTick.y  = float.Parse(GUILayout.TextField(grassWindTick.y.ToString()));*/

        // disableGrass = GUILayout.Toggle(disableGrass, "Disable Grass");
        /*
        [Header("Grass")]
        public float grassResolution;           // 32
        public float grassTopThickness;         // 0.4f
        public float grassSelectionSpeed;       // 0.4
        public float2 grassHeight;              // 0.2, 1.3
        public int2 grassStrandSpawnChance;     // 4, 22
        public int2 grassStrandsCount;          // 4, 90
        public float grassStrandColorVariance;  // 0.016
        public UnityEngine.Color grassSelectedColor; // 137, 18, 18

        [Header("Grass Wind")]
        //! The amount wind will change
        public float grassWindVariance;         // 0.14
        //! Every (x,y) time direction of wind will change
        public float2 grassWindTick;            // 0.4, 0.7
        //! The delta rate of change to new direction
        public float grassWindSpeed;            // 2
        public bool disableGrassWind;           // false
        */
        // public bool isTextureDarkOutlines;      // true
        // public int voxelTextureResolution;      // 32
        // public bool disableVoxelTextures;       // false

        /*[Header("Minivoxes")]
        public int minivoxDistance;             // 2
        public float minivoxResolution;         // 32
        public bool debugDoorAnimations;        // false*/

        // Move Next
        /*[Header("Models")]
        public int characterRenderDistance;     // 2
        //! Character model resolution
        public float voxelScale;                // 48
        public UnityEngine.Color entitySelectedColor;   // 152, 19, 19
        public float entitySelecionSpeed;       // 0.4
        public float voxelNoiseColorVariation;  // 0.04
        public bool disableVoxelNoise;          // false

        [Header("AO")]
        public float aoCornerDarkness;          // 0.3
        public float aoEdgeDarkness;            // 0.4
        public bool disableAO;                  // false
        */