using UnityEngine;
using Zoxel.Rendering;

namespace Zoxel.Voxels
{
    //! Stores the Voxel Materials.
    [CreateAssetMenu(fileName = "VoxelMaterials", menuName = "Zoxel/Settings/VoxelMaterials")]
    public partial class VoxelMaterialsDatam : ScriptableObject
    {
        [Header("Voxels")]
        //! Diffuse, Water and Leaf materials.
        public UnityEngine.Material[] voxelMaterials;
        public MaterialTypeEditor[] voxelMaterialTypes;
        public UnityEngine.Material wiredMaterial;

        [Header("Other")]
        public UnityEngine.Material grassMaterial;
        public UnityEngine.Material voxelDamageMaterial;
        public UnityEngine.Material minivoxMaterial;
        public UnityEngine.Material minivoxTransparentMaterial;
    }
}