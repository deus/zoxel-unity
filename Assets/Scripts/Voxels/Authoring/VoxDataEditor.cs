using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

// Remove this and use ChunkDimensions, Chunk, VoxColors, BoneType

namespace Zoxel.Voxels
{
    //! An storage container for voxels.
    /**
    *   This is used purely for importing data.
    *   \todo Store VoxDatam data in Chunk and VoxColors for faster cloning.
    *   \todo Seperate any boneLinks functions from this and move to their systems
    */
    public struct VoxDataEditor : IComponentData
    {
        public int id;
        public int3 size;
        public BlitableArray<byte> data;
        public BlitableArray<ColorRGB> colors;
        // public byte isClone;
        public byte boneType;

        // Remember it clones the incoming native array
        public VoxDataEditor(int id, int3 size, in NativeArray<byte> data, in BlitableArray<ColorRGB> colors, byte boneType)
        {
            this.id = id;
            this.size = size;
            this.data = new BlitableArray<byte>(data.Length, Allocator.Persistent);
            for (int i = 0; i < data.Length; i++)
            {
                this.data[i] = data[i];
            }
            this.colors = colors.Clone();
            // this.isClone = 0;
            this.boneType = boneType;
        }
        public VoxDataEditor(int id, int3 size, in VoxDataEditor oldVoxData, in BlitableArray<ColorRGB> colors, byte boneType)
        {
            this.id = id;
            this.size = size;
            this.data = oldVoxData.data.Clone();
            /*this.data = new BlitableArray<byte>(oldVoxData.data.Length, Allocator.Persistent);
            for (int i = 0; i < oldVoxData.data.Length; i++)
            {
                this.data[i] = oldVoxData.data[i];
            }*/
            this.colors = colors.Clone();
            // this.isClone = 0;
            this.boneType = boneType;
        }

        public void InitializeVoxData()
        {
            int count = (int) math.floor(size.x * size.y * size.z);
            if (data.Length > 0)
            {
                data.Dispose();
            }
            data = new BlitableArray<byte>(count, Allocator.Persistent);
        }

        public void InitializeColors(int count)
        {
            DisposeColors();
            colors = new BlitableArray<ColorRGB>(count, Allocator.Persistent);
        }

        public void DisposeColors()
        {
            if (colors.Length > 0)
            {
                colors.Dispose();
            }
        }

        public void Dispose()
        {
            DisposeColors();
            if (data.Length > 0)
            {
                data.Dispose();
            }
        }

        public float3 GetSize(float3 voxelScale)
        {
            var newSize = 0.5f * voxelScale * new float3(size.x, size.y, size.z);    // scaled size
            return newSize;
        }

        public void SetAllAir()
        {
            int3 localPosition;
            int voxelIndex = 0;
            for (localPosition.x = 0; localPosition.x < size.x; localPosition.x++)
            {
                for (localPosition.y = 0; localPosition.y < size.y; localPosition.y++)
                {
                    for (localPosition.z = 0; localPosition.z < size.z; localPosition.z++)
                    {
                        data[voxelIndex] = 0;
                        voxelIndex++;
                    }
                }
            }
        }

        public void RotateVox(byte flipType)
        {
            int3 localPosition;
            var newData = new BlitableArray<byte>(data.Length, Allocator.Persistent);
            int3 newSize = int3.zero;
            if (flipType == 0)
            {
                newSize = new int3(size.y, size.x, size.z); // X Y
            }
            else
            {
                newSize = new int3(size.z, size.y, size.x); // X Z
            }
            for (localPosition.x = 0; localPosition.x < size.x; localPosition.x++)
            {
                for (localPosition.y = 0; localPosition.y < size.y; localPosition.y++)
                {
                    for (localPosition.z = 0; localPosition.z < size.z; localPosition.z++)
                    {
                        int oldIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, size);
                        int flippedIndex = 0;
                        if (flipType == 0)
                        {
                            flippedIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(
                                localPosition.y,
                                localPosition.x,
                                localPosition.z), newSize);
                        }
                        else
                        {
                            flippedIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(
                                localPosition.z,
                                localPosition.y,
                                localPosition.x), newSize);
                        }
                        newData[flippedIndex] = data[oldIndex];
                    }
                }
            }
            size = newSize;
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = newData[i];
            }
            newData.Dispose();
        }

        public void FlipVox(byte flipType)
        {
            int3 localPosition;
            var newData = new BlitableArray<byte>(data.Length, Allocator.Persistent);
            for (localPosition.x = 0; localPosition.x < size.x; localPosition.x++)
            {
                for (localPosition.y = 0; localPosition.y < size.y; localPosition.y++)
                {
                    for (localPosition.z = 0; localPosition.z < size.z; localPosition.z++)
                    {
                        int oldIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, size);
                        int flippedIndex = 0;
                        if (flipType == 0)
                        {
                            flippedIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(
                                size.x - 1 - localPosition.x,
                                localPosition.y,
                                localPosition.z), size);
                        }
                        else if (flipType == 1)
                        {
                            flippedIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(
                                localPosition.x,
                                size.y - 1 - localPosition.y,
                                localPosition.z), size);
                        }
                        else if (flipType == 2)
                        {
                            flippedIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(
                                localPosition.x,
                                localPosition.y,
                                size.z - 1 - localPosition.z), size);
                        }
                        newData[flippedIndex] = data[oldIndex];
                    }
                }
            }
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = newData[i];
            }
            newData.Dispose();
        }

        public static NativeArray<byte> RotateVox(byte flipType, ref NativeArray<byte> data, int3 size, out int3 newSize)
        {
            int3 localPosition;
            var newData = new NativeArray<byte>(data.Length, Allocator.Persistent);
            if (flipType == 0)
            {
                newSize = new int3(size.y, size.x, size.z); // X Y
            }
            else
            {
                newSize = new int3(size.z, size.y, size.x); // X Z
            }
            for (localPosition.x = 0; localPosition.x < size.x; localPosition.x++)
            {
                for (localPosition.y = 0; localPosition.y < size.y; localPosition.y++)
                {
                    for (localPosition.z = 0; localPosition.z < size.z; localPosition.z++)
                    {
                        int oldIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, size);
                        int flippedIndex = 0;
                        if (flipType == 0)
                        {
                            flippedIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(
                                localPosition.y,
                                localPosition.x,
                                localPosition.z), newSize);
                        }
                        else
                        {
                            flippedIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(
                                localPosition.z,
                                localPosition.y,
                                localPosition.x), newSize);
                        }
                        newData[flippedIndex] = data[oldIndex];
                    }
                }
            }
            size = newSize;
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = newData[i];
            }
            newData.Dispose();
            return data;
        }

        public static void RotateVox(byte flipType, in NativeArray<byte> oldData, ref NativeArray<byte> newData,
            int3 size, out int3 newSize)
        {
            int3 localPosition;
            if (flipType == 0)
            {
                newSize = new int3(size.y, size.x, size.z); // X Y
            }
            else
            {
                newSize = new int3(size.z, size.y, size.x); // X Z
            }
            for (localPosition.x = 0; localPosition.x < size.x; localPosition.x++)
            {
                for (localPosition.y = 0; localPosition.y < size.y; localPosition.y++)
                {
                    for (localPosition.z = 0; localPosition.z < size.z; localPosition.z++)
                    {
                        int oldIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, size);
                        int flippedIndex = 0;
                        if (flipType == 0)
                        {
                            flippedIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(
                                localPosition.y,
                                localPosition.x,
                                localPosition.z), newSize);
                        }
                        else
                        {
                            flippedIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(
                                localPosition.z,
                                localPosition.y,
                                localPosition.x), newSize);
                        }
                        newData[flippedIndex] = oldData[oldIndex];
                    }
                }
            }
            size = newSize;
        }

        public static void FlipVox(byte flipType, ref NativeArray<byte> data, int3 size)
        {
            int3 localPosition;
            var newData = new NativeArray<byte>(data.Length, Allocator.Temp);
            for (localPosition.x = 0; localPosition.x < size.x; localPosition.x++)
            {
                for (localPosition.y = 0; localPosition.y < size.y; localPosition.y++)
                {
                    for (localPosition.z = 0; localPosition.z < size.z; localPosition.z++)
                    {
                        int oldIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, size);
                        int flippedIndex = 0;
                        if (flipType == 0)
                        {
                            flippedIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(
                                size.x - 1 - localPosition.x,
                                localPosition.y,
                                localPosition.z), size);
                        }
                        else if (flipType == 1)
                        {
                            flippedIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(
                                localPosition.x,
                                size.y - 1 - localPosition.y,
                                localPosition.z), size);
                        }
                        else if (flipType == 2)
                        {
                            flippedIndex = VoxelUtilities.GetVoxelArrayIndex(new int3(
                                localPosition.x,
                                localPosition.y,
                                size.z - 1 - localPosition.z), size);
                        }
                        newData[flippedIndex] = data[oldIndex];
                    }
                }
            }
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = newData[i];
            }
            // newData.Dispose();
        }

        public void OptimizeColors()
        {
            int3 localPosition;
            var usedVoxelIndexes = new NativeList<byte>();      // get old indexes
            for (localPosition.x = 0; localPosition.x < size.x; localPosition.x++)
            {
                for (localPosition.y = 0; localPosition.y < size.y; localPosition.y++)
                {
                    for (localPosition.z = 0; localPosition.z < size.z; localPosition.z++)
                    {
                        int newIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, size);
                        var voxelIndex = data[newIndex];
                        if (voxelIndex == 0)
                        {
                            continue;
                        }
                        // if new index
                        var isNewIndex = true;
                        for (var i = 0; i < usedVoxelIndexes.Length; i++)
                        {
                            if (usedVoxelIndexes[i] == voxelIndex)
                            {
                                isNewIndex = false;
                                break;
                            }
                        }
                        if (isNewIndex)
                        {
                            usedVoxelIndexes.Add(voxelIndex);
                        }
                    }
                }
            }
            // Get Total Colors
            var newColors = new NativeList<ColorRGB>();
            for (var i = 0; i < usedVoxelIndexes.Length; i++)
            {
                // add color to new colors
                var voxelIndex = usedVoxelIndexes[i] - 1;    // remove air
                var color = colors[voxelIndex];
                // replace index with new one
                var newIndex = newColors.Length;
                newColors.Add(color);
                ReplaceIndex(((byte) (voxelIndex + 1)), ((byte) (newIndex + 1)));
            }
            if (colors.Length > 0)
            {
                colors.Dispose();
            }
            InitializeColors(newColors.Length);
            for (var i = 0; i < colors.Length; i++)
            {
                colors[i] = newColors[i];
            }
        }

        public void ReplaceIndex(byte voxelIndexA, byte voxelIndexB)
        {
            int3 localPosition;
            for (localPosition.x = 0; localPosition.x < size.x; localPosition.x++)
            {
                for (localPosition.y = 0; localPosition.y < size.y; localPosition.y++)
                {
                    for (localPosition.z = 0; localPosition.z < size.z; localPosition.z++)
                    {
                        int newIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, size);
                        var voxelIndex = data[newIndex];
                        if (voxelIndex == voxelIndexA)
                        {
                            data[newIndex] = voxelIndexB;
                        }
                    }
                }
            }
        }

        public static void ReplaceIndex(ref byte[] data, int3 size, byte voxelIndexA, byte voxelIndexB)
        {
            int3 localPosition;
            for (localPosition.x = 0; localPosition.x < size.x; localPosition.x++)
            {
                for (localPosition.y = 0; localPosition.y < size.y; localPosition.y++)
                {
                    for (localPosition.z = 0; localPosition.z < size.z; localPosition.z++)
                    {
                        int newIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, size);
                        var voxelIndex = data[newIndex];
                        if (voxelIndex == voxelIndexA)
                        {
                            data[newIndex] = voxelIndexB;
                            if (size == new int3(12, 12, 12) && (newIndex == 0 || newIndex == 2))
                            {
                                UnityEngine.Debug.LogError("(" + newIndex + ") Replaced: " + voxelIndexA + " with " + voxelIndexB);
                            }
                        }
                    }
                }
            }
        }

        public SerializeableVoxData GetSerializeableClone()
        {
            var clone = new SerializeableVoxData();
            clone.id = id;
            clone.size = size;
            clone.data = new byte[data.Length];
            var dataArray = data.ToArray();
            for (int i = 0; i < dataArray.Length; i++)
            {
                clone.data[i] = dataArray[i];
            }
            clone.colors = new ColorRGB[colors.Length];
            var colorsArray = colors.ToArray();
            for (int i = 0; i < colorsArray.Length; i++)
            {
                clone.colors[i] = colorsArray[i];
            }
            return clone;
        }
    }
}