using UnityEngine;

namespace Zoxel.Voxels
{
    [CreateAssetMenu(fileName = "VoxelSettings", menuName = "ZoxelSettings/VoxelSettings")]
    public partial class VoxelSettingsDatam : ScriptableObject
    {
        public VoxelSettings voxelSettings;
    }
}