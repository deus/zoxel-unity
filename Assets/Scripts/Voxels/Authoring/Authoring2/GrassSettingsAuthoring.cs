using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Zoxel.Voxels.Authoring
{
    //! Game settings for grass.
    // [GenerateAuthoringComponent]
    public struct GrassSettings : IComponentData
    {
        public float grassResolution;           // 32
        public float grassTopThickness;         // 0.4f
        public float2 grassHeight;              // 0.2, 1.3
        public int2 grassStrandSpawnChance;     // 4, 22
        public int2 grassStrandsCount;          // 4, 90
        public float grassStrandColorVariance;  // 0.016
        public int maxGrassPerChunk;             // 512
        public bool disableGrass;
        public float grassWindVariance;         // 0.14
        public float2 grassWindTick;            // 0.4, 0.7
        public float grassWindSpeed;            // 2
        public bool disableGrassWind;           // false
        public float grassSelectionSpeed;       // 0.4
        public Color grassSelectedColor;        // 137, 18, 18
        public bool disableGrassHighlighting;
    }

    public class GrassSettingsAuthoring : MonoBehaviour
    {
        public float grassResolution;           // 32
        public float grassTopThickness;         // 0.4f
        public float2 grassHeight;              // 0.2, 1.3
        public int2 grassStrandSpawnChance;     // 4, 22
        public int2 grassStrandsCount;          // 4, 90
        public float grassStrandColorVariance;  // 0.016
        public int maxGrassPerChunk;             // 512
        public bool disableGrass;
        public float grassWindVariance;         // 0.14
        public float2 grassWindTick;            // 0.4, 0.7
        public float grassWindSpeed;            // 2
        public bool disableGrassWind;           // false
        public float grassSelectionSpeed;       // 0.4
        public Color grassSelectedColor;        // 137, 18, 18
        public bool disableGrassHighlighting;
    }

    public class GrassSettingsAuthoringBaker : Baker<GrassSettingsAuthoring>
    {
        public override void Bake(GrassSettingsAuthoring authoring)
        {
            AddComponent(new GrassSettings
            {
                grassResolution = authoring.grassResolution,
                grassTopThickness = authoring.grassTopThickness
            });       
        }
    }
}