using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Zoxel.Voxels.Authoring
{
    //! Game settings for grass.
    // [GenerateAuthoringComponent]
    public struct VoxelTextureSettings : IComponentData
    {
        public bool isTextureDarkOutlines;      // true
        public byte voxelTextureResolution;      // 32
        public bool disableVoxelTextures;       // false
        public float3 soilHSVMin;
        public float3 soilHSVMax;
        public float3 grassHSVMin;
        public float3 grassHSVMax;
        public bool isSoilSwapRed;
    }

    public class VoxelTextureSettingsAuthoring : MonoBehaviour
    {
        public bool isTextureDarkOutlines;      // true
        public byte voxelTextureResolution;      // 32
        public bool disableVoxelTextures;       // false
        public float3 soilHSVMin;
        public float3 soilHSVMax;
        public float3 grassHSVMin;
        public float3 grassHSVMax;
        public bool isSoilSwapRed;
    }

    public class VoxelTextureSettingsAuthoringBaker : Baker<VoxelTextureSettingsAuthoring>
    {
        public override void Bake(VoxelTextureSettingsAuthoring authoring)
        {
            AddComponent(new VoxelTextureSettings
            {
                isTextureDarkOutlines = authoring.isTextureDarkOutlines,
                voxelTextureResolution = authoring.voxelTextureResolution
            });       
        }
    }
}
        // public int[] textureLowerDistances;     /// 1, 3, 5, 7, 8, 9 (make this a const)