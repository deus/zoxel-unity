using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Zoxel.Voxels.Authoring
{
    //! Game settings for Models.
    // [GenerateAuthoringComponent]
    public struct VoxelCoreSettings : IComponentData
    {
        public int3 voxelDimensions;            // 16, 16, 16
        public float3 planetVoxelScale;         // 0.75, 0.75, 0.75
        public float timePerStream;             // 4
        public bool isDrawWireframes;           // for chunk meshes
        public bool isDrawChunkColliders;       
        public bool disableWorldStreaming;
        public bool disableWorlds;
        public bool disablePlanetIO;
        public bool disablePlanetChunkIO;
    }

    public class VoxelCoreSettingsAuthoring : MonoBehaviour
    {
        public int3 voxelDimensions;            // 16, 16, 16
        public float3 planetVoxelScale;         // 0.75, 0.75, 0.75
        public float timePerStream;             // 4
        public bool isDrawWireframes;           // for chunk meshes
        public bool isDrawChunkColliders;       
        public bool disableWorldStreaming;
        public bool disableWorlds;
        public bool disablePlanetIO;
        public bool disablePlanetChunkIO;
    }

    public class VoxelCoreSettingsAuthoringBaker : Baker<VoxelCoreSettingsAuthoring>
    {
        public override void Bake(VoxelCoreSettingsAuthoring authoring)
        {
            AddComponent(new VoxelCoreSettings
            {
                voxelDimensions = authoring.voxelDimensions,
                planetVoxelScale = authoring.planetVoxelScale,
                timePerStream = authoring.timePerStream,
                isDrawWireframes = authoring.isDrawWireframes,
                isDrawChunkColliders = authoring.isDrawChunkColliders,
                disableWorldStreaming = authoring.disableWorldStreaming,
                disableWorlds = authoring.disableWorlds,
                disablePlanetIO = authoring.disablePlanetIO,
                disablePlanetChunkIO = authoring.disablePlanetChunkIO,
            });       
        }
    }
}