using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace Zoxel.Voxels.Authoring
{
    //! Game settings for Models.
    // [GenerateAuthoringComponent]
    public struct ModelSettings : IComponentData
    {
        public int characterRenderDistance;     // 2
        public float voxelScale;                // 48
        public Color entitySelectedColor;       // 152, 19, 19
        public float entitySelecionSpeed;       // 0.4
        public float voxelNoiseColorVariation;  // 0.04
        public float aoCornerDarkness;          // 0.3
        public float aoEdgeDarkness;            // 0.4
        public bool disableAO;                  // false
        public bool isColourWeights;
        public bool isDrawBoneBoxes;
        public bool disableAnimations;
        public bool disableModels;
        public bool disableGear;
        public bool disableCharacterLighting;
        public bool disableVoxelNoise;          // false
        
        public float3 GetVoxelScale()
        {
            return (new float3(1f / voxelScale, 1f / voxelScale, 1f / voxelScale));
        }

        public float3 GetVoxelSize()
        {
            return (new float3(voxelScale, voxelScale, voxelScale));
        }
    }

    public class ModelSettingsAuthoring : MonoBehaviour
    {
        public int characterRenderDistance;     // 2
        public float voxelScale;                // 48
        public Color entitySelectedColor;       // 152, 19, 19
        public float entitySelecionSpeed;       // 0.4
        public float voxelNoiseColorVariation;  // 0.04
        public float aoCornerDarkness;          // 0.3
        public float aoEdgeDarkness;            // 0.4
        public bool disableAO;                  // false
        public bool isColourWeights;
        public bool isDrawBoneBoxes;
        public bool disableAnimations;
        public bool disableModels;
        public bool disableGear;
        public bool disableCharacterLighting;
        public bool disableVoxelNoise;          // false
    }

    public class ModelSettingsAuthoringBaker : Baker<ModelSettingsAuthoring>
    {
        public override void Bake(ModelSettingsAuthoring authoring)
        {
            AddComponent(new ModelSettings
            {
                characterRenderDistance = authoring.characterRenderDistance,
                voxelScale = authoring.voxelScale
            });       
        }
    }
}