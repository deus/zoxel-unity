using Unity.Entities;
using UnityEngine;

namespace Zoxel.Voxels.Authoring
{
    //! Game settings for Minivoxes.
    // [GenerateAuthoringComponent]
    public struct MinivoxSettings : IComponentData
    {
        public int minivoxDistance;             // 2
        public float minivoxResolution;         // 32
        public int maxMinivoxesPerChunk;        // 256
        public bool debugDoorAnimations;        // false
        public bool disableMinivoxes;
        public bool disableBigMinivoxes;
    }

    public class MinivoxSettingsAuthoring : MonoBehaviour
    {
        public int minivoxDistance;             // 2
        public float minivoxResolution;         // 32
        public int maxMinivoxesPerChunk;        // 256
        public bool debugDoorAnimations;        // false
        public bool disableMinivoxes;
        public bool disableBigMinivoxes;
    }

    public class MinivoxSettingsAuthoringBaker : Baker<MinivoxSettingsAuthoring>
    {
        public override void Bake(MinivoxSettingsAuthoring authoring)
        {
            AddComponent(new MinivoxSettings
            {
                minivoxDistance = authoring.minivoxDistance,
                minivoxResolution = authoring.minivoxResolution
            });       
        }
    }
}