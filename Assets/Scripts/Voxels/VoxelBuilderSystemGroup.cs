using Unity.Entities;

namespace Zoxel.Voxels
{
    //! Voxel Builder Systems are everything when building cubes.
    /**
    *   Using DEBUG_VOXEL_PLACEMENT define tag used for timing of voxel.
    *   Using DEBUG_VOXEL_LIGHTS_ONLY define tag used for debugging lights only process.
    */
    [UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxelBuilderSystemGroup : ComponentSystemGroup { }
}