﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;

namespace Zoxel.Maps
{
	//! Extracts height and voxel type data from chunks.
	/**
	*	Traverses chunks and builds from several chunks into a vertical slice.
	*/
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
	public partial class MapPartHeightSystem : SystemBase
	{
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            chunksQuery = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.Exclude<EntityBusy>(),
				ComponentType.Exclude<EdgeChunk>(),
				ComponentType.ReadOnly<ChunkPosition>(),
				ComponentType.ReadOnly<Chunk>(),
				ComponentType.ReadOnly<ChunkNeighbors>(),
				ComponentType.ReadOnly<PlanetChunk>());
            RequireForUpdate(processQuery);
        }
		
		[BurstCompile]
        protected override void OnUpdate()
        {
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunks = GetComponentLookup<Chunk>(true);
            var chunkNeighbors2 = GetComponentLookup<ChunkNeighbors>(true);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
			chunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<InitializeEntity>()
				.WithAll<MapPartBuilder>()
				.ForEach((ref MapPart mapPart, in ChunkDataLink chunkDataLink) =>
            {
				if (!chunks.HasComponent(chunkDataLink.chunk))
				{
					return;
				}
				// get top most chunk
				var highestChunkEntity = chunkDataLink.chunk;
				var highestChunkPosition = chunkPositions[highestChunkEntity].position;
				while (true)
				{
					var chunkNeighbors = chunkNeighbors2[highestChunkEntity];
					if (chunkNeighbors2.HasComponent(chunkNeighbors.chunkUp))
					{
						highestChunkEntity = chunkNeighbors.chunkUp;
					}
					else
					{
						break;
					}
				}
				var lowestChunkEntity = chunkDataLink.chunk;
				var lowestChunkPosition = chunkPositions[lowestChunkEntity].position;
				while (true)
				{
					var chunkNeighbors = chunkNeighbors2[lowestChunkEntity];
					if (chunkNeighbors2.HasComponent(chunkNeighbors.chunkDown))
					{
						lowestChunkEntity = chunkNeighbors.chunkDown;
					}
					else
					{
						break;
					}
				}
				lowestChunkPosition = chunkPositions[lowestChunkEntity].position;
				highestChunkPosition = chunkPositions[highestChunkEntity].position;
				var chunk = chunks[highestChunkEntity];
				if (chunk.voxels.Length == 0)
				{
					return;
				}
				var voxelDimensions = chunk.voxelDimensions;
				mapPart.Initialize(voxelDimensions);
				// get all the top voxels
				var localPosition = new int3();
				var highestPositionY = (highestChunkPosition.y * voxelDimensions.y) + voxelDimensions.y - 1;
				var lowestPositionY = lowestChunkPosition.y * voxelDimensions.y;
				// UnityEngine.Debug.LogError("lowestPositionY: " + lowestPositionY + " to highestPositionY:" + highestPositionY);
				var bigDifference = (byte) (highestPositionY - lowestPositionY);
				for (int i = 0; i < mapPart.topVoxels.Length; i++)
				{
					mapPart.topVoxels[i] = 0; // 1;
					mapPart.heights[i] = 0;
				}
				var highestChunk = chunks[highestChunkEntity];
				for (localPosition.x = 0; localPosition.x < voxelDimensions.x; localPosition.x++)
				{
					for (localPosition.z = 0; localPosition.z < voxelDimensions.z; localPosition.z++)
					{
						byte voxel = 0;
						byte height = 0;
						var chunkEntity = highestChunkEntity;
						chunk = highestChunk;
						var previousChunkPositionY = highestChunkPosition.y;
						for (var positionY = highestPositionY; positionY >= lowestPositionY; positionY--)
						{
							localPosition.y = positionY % voxelDimensions.y;
							if (localPosition.y < 0)
							{
								localPosition.y += voxelDimensions.y;
							}
							var chunkPositionY = VoxelUtilities.GetChunkPositionInt(positionY, voxelDimensions.y);
							if (previousChunkPositionY != chunkPositionY)
							{
								previousChunkPositionY = chunkPositionY;
								if (!chunkNeighbors2.HasComponent(chunkEntity))
								{
									break;
								}
								chunkEntity = chunkNeighbors2[chunkEntity].chunkDown;
								if (!chunks.HasComponent(chunkEntity))
								{
									break;
								}
								chunk = chunks[chunkEntity];
								if (chunk.voxels.Length == 0)
								{
									break;
								}
							}
							var xyzIndex = VoxelUtilities.GetVoxelArrayIndex(localPosition, voxelDimensions);
							var thatVoxel = chunk.voxels[xyzIndex];
							if (thatVoxel != 0)
							{
								voxel = thatVoxel;
								height = (byte) (positionY - lowestPositionY);
								break;
							}
						}
						var xzIndex = localPosition.x + localPosition.z * voxelDimensions.x;
						mapPart.topVoxels[xzIndex] = voxel;
						mapPart.heights[xzIndex] = height;
						/*if (voxel > 1)
						{
							UnityEngine.Debug.LogError("Invalid Voxel at [" + localPosition + "] voxel: " + voxel + " :: " + height);
						}*/
						// UnityEngine.Debug.LogError("height: " + height);
					}
				}
            })  .WithReadOnly(chunks)
				.WithReadOnly(chunkNeighbors2)
				.WithReadOnly(chunkPositions)
                .ScheduleParallel(Dependency);
		}
	}
}