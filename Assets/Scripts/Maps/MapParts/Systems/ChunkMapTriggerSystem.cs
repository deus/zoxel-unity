using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;
using Zoxel.UI;
// todo: rework this system with proper links and events

namespace Zoxel.Maps
{
	//! Triggers chunks map parts to update.
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
	public partial class ChunkMapTriggerSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            chunksQuery = GetEntityQuery(
				ComponentType.ReadOnly<ChunkPosition>(),
				ComponentType.ReadOnly<Chunk>(),
				ComponentType.ReadOnly<ChunkNeighbors>(),
				ComponentType.ReadOnly<PlanetChunk>(),
                ComponentType.Exclude<InitializeEntity>(),
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.Exclude<EntityBusy>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
		protected override void OnUpdate()
		{
			var isDebugMapMegaChunks = UIManager.instance.uiSettings.isDebugMapMegaChunks;
			var isDebugMapMegaChunk2Ds = UIManager.instance.uiSettings.isDebugMapMegaChunk2Ds;
			var isDebugMapBiomes = UIManager.instance.uiSettings.isDebugMapBiomes;
			var isDebugMapTowns = UIManager.instance.uiSettings.isDebugMapTowns;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunkNeighbors2 = GetComponentLookup<ChunkNeighbors>(true);
			chunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<InitializeEntity, MapPartBuilder>()
				.ForEach((Entity e, int entityInQueryIndex, ref ChunkDataLink chunkDataLink) =>
            {
				if (!chunkNeighbors2.HasComponent(chunkDataLink.chunk))
				{
					return;
				}
				var isChunkStackBuilding = (byte) 0;
				if (HasComponent<EntityBusy>(chunkDataLink.chunk) && !HasComponent<VoxelBuilderMeshOnly>(chunkDataLink.chunk) && !HasComponent<VoxelBuilderLightsOnly>(chunkDataLink.chunk))
				{
					isChunkStackBuilding = 1;
				}
				else
				{
					// do checks here for chunk's being busy
					var chunkEntity = chunkDataLink.chunk;
					while (true)
					{
						var chunkNeighbors = chunkNeighbors2[chunkEntity];
						if (!HasComponent<Chunk>(chunkNeighbors.chunkUp))
						{
							break;
						}
						else
						{
							chunkEntity = chunkNeighbors.chunkUp;
							if (HasComponent<EntityBusy>(chunkEntity) && !HasComponent<VoxelBuilderMeshOnly>(chunkEntity) && !HasComponent<VoxelBuilderLightsOnly>(chunkEntity))
							{
								isChunkStackBuilding = 1;
								break;
							}
						}
					}
					chunkEntity = chunkDataLink.chunk;
					while (isChunkStackBuilding == 0 && true)
					{
						var chunkNeighbors = chunkNeighbors2[chunkEntity];
						if (!HasComponent<Chunk>(chunkNeighbors.chunkDown))
						{
							break;
						}
						else
						{
							chunkEntity = chunkNeighbors.chunkDown;
							if (HasComponent<EntityBusy>(chunkEntity) && !HasComponent<VoxelBuilderMeshOnly>(chunkEntity) && !HasComponent<VoxelBuilderLightsOnly>(chunkEntity))
							{
								isChunkStackBuilding = 1;
								break;
							}
						}
					}
				}
				// Update and trigger
				if (chunkDataLink.isBuilding != isChunkStackBuilding)
				{
					chunkDataLink.isBuilding = isChunkStackBuilding;
					if (chunkDataLink.isBuilding == 0)
					{
						if (isDebugMapMegaChunks)
						{
							PostUpdateCommands.AddComponent<MapPartBuilderMegaChunks>(entityInQueryIndex, e);
						}
						else if (isDebugMapMegaChunk2Ds)
						{
							PostUpdateCommands.AddComponent<MapPartBuilderMegaChunk2Ds>(entityInQueryIndex, e);
						}
						else if (isDebugMapBiomes)
						{
							PostUpdateCommands.AddComponent<MapPartBuilderBiomes>(entityInQueryIndex, e);
						}
						else if (isDebugMapTowns)
						{
							PostUpdateCommands.AddComponent<MapBuilderTowns>(entityInQueryIndex, e);
						}
						else
						{
							PostUpdateCommands.AddComponent<MapPartBuilder>(entityInQueryIndex, e);
						}
					}
				}
            })  .WithReadOnly(chunkNeighbors2)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}