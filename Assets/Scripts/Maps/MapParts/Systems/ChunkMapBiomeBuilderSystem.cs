﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using Zoxel.Voxels;
// using Zoxel.Worlds;

namespace Zoxel.Maps
{
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
	public partial class ChunkMapBiomeBuilderSystem : SystemBase
	{
        [BurstCompile]
        protected override void OnUpdate()
		{
            /*var time = (float)World.Time.ElapsedTime;
            Entities.ForEach(() =>    // ref Shrink component, 
            {
            }).ScheduleParallel();*/
        }
		/*protected override JobHandle OnUpdate(JobHandle inputDeps)
		{
			return new ChunkMapBiomeJob { }.Schedule(this, inputDeps);
		}
		
		[BurstCompile]
		struct ChunkMapBiomeJob : IJobForEach<MapPart, ChunkBiomeMap>
		{
			public void Execute(ref MapPart mapPart, ref ChunkBiomeMap chunkBiomeMap)   //Entity entity, int index,  
			{
				if (chunkBiomeMap.state == 1)
				{
					chunkBiomeMap.state = 2;
					// get all the top voxels
					CreateFloatBiomeMap(ref mapPart, chunkBiomeMap);
				}
			}

			public void CreateIntBiomeMap(ref MapPart mapPart, ChunkBiomeMap chunkBiomeMap)
			{
				var voxelDimensions = mapPart.voxelDimensions;
				int xzIndex2 = 0;
				for (int i = 0; i < voxelDimensions.x; i++)
				{
					for (int j = 0; j < voxelDimensions.z; j++)
					{
						int xzIndex = i + j * mapPart.voxelDimensions.z;
						float multiplyerA = chunkBiomeMap.data.biomes[xzIndex2];
						int biomeIndex = (int)math.floor(multiplyerA);
						//multiplyerA -= (int)math.floor(multiplyerA);
						mapPart.topVoxels[xzIndex] = (byte)(biomeIndex + 1);
						mapPart.heights[xzIndex] = (byte)((int)(1 * voxelDimensions.y));
						xzIndex2++;
					}
				}
			}

			public void CreateFloatBiomeMap(ref MapPart mapPart, ChunkBiomeMap chunkBiomeMap)
			{
				var voxelDimensions = mapPart.voxelDimensions;
				int xzIndex2 = 0;
				for (int i = 0; i < voxelDimensions.x; i++)
				{
					for (int j = 0; j < voxelDimensions.z; j++)
					{
						int xzIndex = i + j * mapPart.voxelDimensions.z;
						float multiplyerA = chunkBiomeMap.data.biomes[xzIndex2];
						int biomeIndex = (int)math.floor(multiplyerA);
						multiplyerA -= biomeIndex;
						mapPart.topVoxels[xzIndex] = (byte)(biomeIndex + 1);
						if (biomeIndex != 1)
						{
							multiplyerA = 1 - multiplyerA;
						}
						mapPart.heights[xzIndex] = (byte)((int)(multiplyerA * voxelDimensions.y));
						xzIndex2++;
					}
				}
			}
		}*/
	}
}