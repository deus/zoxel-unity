using Unity.Entities;
using Zoxel.UI;
using Zoxel.Rendering;
using Zoxel.Textures;

namespace Zoxel.Maps
{
    //! Initializes material and mesh on a MapPart entity.
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class MapPartInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var materials = UIManager.instance.materials;
            var mapPartMaterial = materials.mapPartMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<MinimapPart>()
                .WithAll<InitializeEntity, MapPart>()
                .ForEach((Entity e, in MeshSize2D meshSize2D) =>
            {
                var zoxMesh = new ZoxMesh();
                zoxMesh.mesh = MeshUtilities.CreateQuadMesh(meshSize2D.size);
                zoxMesh.material = new UnityEngine.Material(mapPartMaterial);
                zoxMesh.layer = worldUILayer;
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
        }
    }
}