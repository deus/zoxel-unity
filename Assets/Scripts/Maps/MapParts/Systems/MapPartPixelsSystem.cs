﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Textures;

namespace Zoxel.Maps
{
	//! Builds the pixels of our map!
	/**
	*	Add option back for size.y to modify colours.
	*/
    [UpdateAfter(typeof(MapPartHeightSystem))]
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
	public partial class MapPartPixelsSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery mapsQuery;
        private EntityQuery worldsQuery;
        private EntityQuery voxelsQuery;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            mapsQuery = GetEntityQuery(
				ComponentType.ReadOnly<MapUI>(),
				ComponentType.ReadOnly<VoxLink>());
            worldsQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.ReadOnly<VoxelLinks>());
            voxelsQuery = GetEntityQuery(ComponentType.ReadOnly<Voxel>());
            chunksQuery = GetEntityQuery(
				ComponentType.Exclude<InitializeEntity>(),
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.ReadOnly<PlanetChunk>());
            RequireForUpdate(processQuery);
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
			var voidColor = new Color(16, 16, 16, 125);	// new Color(0, 0, 0, 255);
			var voidColor2 = new Color(255, 16, 16, 125);	// new Color(0, 0, 0, 255);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var worldEntities = worldsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxelLinks = GetComponentLookup<VoxelLinks>(true);
            var chunkDimensions = GetComponentLookup<ChunkDimensions>(true);
            var voxelEntities = voxelsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxels2 = GetComponentLookup<Voxel>(true);
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var chunkVoxLinks = GetComponentLookup<VoxLink>(true);
			worldEntities.Dispose();
			chunkEntities.Dispose();
			voxelEntities.Dispose();
            var mapEntities = mapsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var voxLinks = GetComponentLookup<VoxLink>(true);
			mapEntities.Dispose();
			//! Initializes map textures.
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<InitializeEntity>()
				.WithAll<MapPartBuilder>()
				.ForEach((Entity e, int entityInQueryIndex, ref Texture texture, in ParentLink parentLink) =>
			{
				PostUpdateCommands.RemoveComponent<MapPartBuilder>(entityInQueryIndex, e);
				PostUpdateCommands.AddComponent<TextureDirty>(entityInQueryIndex, e);
				if (!voxLinks.HasComponent(parentLink.parent))
				{
					return;
				}
				var voxEntity = voxLinks[parentLink.parent].vox;
				if (!chunkDimensions.HasComponent(voxEntity))
				{
					return;
				}
				var size = chunkDimensions[voxEntity].voxelDimensions.ToXZ();
				if (texture.size != size)
				{
					texture.Dispose();
					texture.Initialize(size);
				}
            })  .WithReadOnly(chunkDimensions)
                .WithReadOnly(voxLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
			//! Sets to all void if cannot find chunk.
			Dependency = Entities
				.WithNone<InitializeEntity>()
				.WithAll<MapPartBuilder>()
				.ForEach((Entity e, int entityInQueryIndex, ref Texture texture, in ChunkDataLink chunkDataLink) =>
			{
				if (!chunkVoxLinks.HasComponent(chunkDataLink.chunk)) 
				{
					for (int i = 0; i < texture.colors.Length; i++)
					{
						texture.colors[i] = voidColor;
					}
				}
            })  .WithReadOnly(chunkVoxLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
			//! For those with chunk linked, it will set map texture pixels as voxel colors.
			Dependency = Entities
				.WithNone<InitializeEntity>()
				.WithAll<MapPartBuilder>()
				.ForEach((Entity e, int entityInQueryIndex, ref Texture texture, in MapPart mapPart, in ChunkDataLink chunkDataLink) =>
			{
				if (!chunkVoxLinks.HasComponent(chunkDataLink.chunk)) 
				{
					return;
				}
				var voxLink = chunkVoxLinks[chunkDataLink.chunk];
				// get voxel colors
				var voxelLinks2 = voxelLinks[voxLink.vox];
				var voxels = new NativeArray<Voxel>(voxelLinks2.voxels.Length, Allocator.Temp);
				for (int i = 0; i < voxels.Length; i++)
				{
					var voxelEntity = voxelLinks2.voxels[i];
					if (voxelEntity.Index != 0 && HasComponent<Voxel>(voxelEntity))
					{
						voxels[i] = voxels2[voxelEntity];
					}
					else
					{
						voxels[i] = new Voxel();
					}
				}
				// set pixels
				var size = texture.size;
				for (var i = 0; i < size.x; i++)
				{
					for (var j = 0; j < size.y; j++)
					{
						var xzIndex = i + j * size.x;
						var voxel = mapPart.topVoxels[xzIndex];
						//UnityEngine.Debug.LogError("Top Voxel is: " + voxel);
						if (voxel == 0 || voxel - 1 >= voxels.Length)
						{
							texture.colors[xzIndex] = voidColor;
						}
						else
						{
							texture.colors[xzIndex] = voxels[voxel - 1].color;
						}
					}
				}
				voxels.Dispose();
            })  .WithReadOnly(voxelLinks)
                .WithReadOnly(voxels2)
                .WithReadOnly(chunkVoxLinks)
                .ScheduleParallel(Dependency);
		}
	}
}

/*if (voxel > 0)
{
	color = new Color(255, 0, 0, 255);
}*/
/*if (voxel > 1)
{
	texture.colors[xzIndex] = voidColor2;
	UnityEngine.Debug.LogError("pos [" + i + "x" + j + "] voxel: " + voxel);
	continue;
}*/

// var newWidth = (int) mapPart.heights[xzIndex];
// var multi = 2f * (((float) newWidth) / (float) mapPart.voxelDimensions.y);
// color.MultiplyRGB(multi); //math.min(1, colorBrightness);

// float colorBrightness = 3 * ((float)size.y / (float)mapPart.highestHeight);
// float multi = ((48 + 4 * ((float) (size.y % 16))) / (float)mapPart.voxelDimensions.y);
// float multi = ((16 + (7f *  (float) (size.y - 32))) / (float)mapPart.voxelDimensions.y);