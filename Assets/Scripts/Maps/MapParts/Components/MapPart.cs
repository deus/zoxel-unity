﻿using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Maps
{
	//! A section of the map.
	public struct MapPart : IComponentData
	{
		public int3 voxelDimensions;
		public BlitableArray<byte> topVoxels;   // x * z chunk slice
		public BlitableArray<byte> heights;   // x * z chunk slice

		public void Initialize(int3 newSize)
		{
			if (voxelDimensions != newSize)
			{
				voxelDimensions = newSize;
				Dispose();
				topVoxels = new BlitableArray<byte>(voxelDimensions.x * voxelDimensions.z, Allocator.Persistent);
				heights = new BlitableArray<byte>(voxelDimensions.x * voxelDimensions.z, Allocator.Persistent);
			}
		}

		public void DisposeFinal()
		{
			topVoxels.DisposeFinal();
			heights.DisposeFinal();
		}

		public void Dispose()
		{
			topVoxels.Dispose();
			heights.Dispose();
		}
	}
}