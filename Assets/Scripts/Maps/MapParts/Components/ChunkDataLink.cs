using Unity.Entities;

namespace Zoxel.Maps
{
	//! Links a map to a chunk and gives it a 2D position!
	public struct ChunkDataLink : IComponentData
	{
		public int2 position;
		public Entity chunk;
		public byte isBuilding;	// need to establish links between planet and a connected planet

		public ChunkDataLink(int2 position, Entity chunk)
		{
			this.position = position;
			this.isBuilding = 0;
			this.chunk = chunk;
		}
	}
}