using Unity.Entities;

namespace Zoxel.Maps
{
	//! Triggers a MapPart to rebuild texture.
	public struct MapPartBuilder : IComponentData { }
}