using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Rendering;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Rendering.Authoring;

namespace Zoxel.Maps
{
    //! Spawns a Minimap ui!
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
    public partial class MinimapSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity spawnPrefab;
        private Entity panelPrefab;
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;
        private EntityQuery planetsQuery;
        private EntityQuery chunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnMinimap),
                typeof(CharacterLink),
                typeof(GenericEvent),
                typeof(DelayEvent));
            spawnPrefab = EntityManager.CreateEntity(spawnArchetype);
            uiHoldersQuery = GetEntityQuery(
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<UILink>(),
                ComponentType.ReadOnly<ChunkLink>(),
                ComponentType.ReadOnly<ChunkStreamPoint>());
            planetsQuery = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<EntityBusy>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<Planet>());
            chunksQuery = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<EntityBusy>(),
                ComponentType.ReadOnly<ChunkPosition>(),
                ComponentType.ReadOnly<Chunk>(),
				ComponentType.ReadOnly<ChunkNeighbors>(),
                ComponentType.ReadOnly<PlanetChunk>());
            RequireForUpdate(processQuery);
            RequireForUpdate<RenderSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (UIManager.instance.uiSettings.disableMinimap)
            {
                return;
            }
            InitializePrefabs();
            var renderSettings = GetSingleton<RenderSettings>();
            var renderDistance = renderSettings.renderDistance - 2;
            var minimapRenderDistance = UIManager.instance.uiSettings.minimapRenderDistance;
            var mapRenderDistance = (byte) math.clamp(minimapRenderDistance, 1, renderDistance);
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var iconSize = uiDatam.GetIconSize(uiScale);
            var elapsedTime = World.Time.ElapsedTime;
            var panelPrefab = this.panelPrefab;
            var playerIconMultiplier = UIManager.instance.uiDatam.playerIconMultiplier * 0.25f;
            // prefabs
            var mapPrefabs = MapUpdateSystem.mapPrefabs;
            var spawnPrefab = MinimapSpawnSystem.spawnPrefab;
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var characterEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
			var voxLinks = GetComponentLookup<VoxLink>(true);
            var chunkStreamPoints = GetComponentLookup<ChunkStreamPoint>(true);
            var characterChunkLinks = GetComponentLookup<ChunkLink>(true);
            characterEntities.Dispose();
            var planetEntities = planetsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var planetChunkLinks = GetComponentLookup<ChunkLinks>(true);
            planetEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            chunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<SpawnMinimap>()
                .ForEach((int entityInQueryIndex, in CharacterLink characterLink) =>
            {
                var voxEntity = new Entity();
                if (voxLinks.HasComponent(characterLink.character))
                {
                    voxEntity = voxLinks[characterLink.character].vox;
                }
                // spawn a new one
                //! \todo Do this in a more elegant way.. Feels bad, to respawn the spawner entity event.
                if (!MapUpdateSystem.CanSpawnMapParts(characterLink.character, voxEntity, in characterChunkLinks, in planetChunkLinks, in chunkPositions))
                {
                    var spawnMinimapEntity2 = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMinimapEntity2, characterLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMinimapEntity2, new DelayEvent(elapsedTime, 0));
                    return;
                }
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, panelPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new VoxLink(voxEntity));
                // spawn parts
                var mapUI = new MapUI(mapRenderDistance, iconSize.x / 2f);
                var zoomLevel = 2.5f;
                var isMinimap = true;
                var childrenCount = MapUpdateSystem.SpawnMapParts(PostUpdateCommands, entityInQueryIndex, mapPrefabs,
                    e3, ref mapUI, isMinimap, mapRenderDistance, zoomLevel, playerIconMultiplier, characterLink.character, voxEntity,
                    in chunkStreamPoints, in characterChunkLinks, in planetChunkLinks, in chunkPositions, true);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, mapUI);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawnedInt(childrenCount));
                if (characterLink.character.Index > 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, characterLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab),
                        new OnUISpawned(characterLink.character, 1));
                    if (cameraLinks.HasComponent(characterLink.character))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLinks[characterLink.character]);
                    }
                }
            })  .WithReadOnly(cameraLinks)
                .WithReadOnly(voxLinks)
                .WithReadOnly(chunkStreamPoints)
				.WithReadOnly(characterChunkLinks)
				.WithReadOnly(planetChunkLinks)
				.WithReadOnly(chunkPositions)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private void InitializePrefabs()
        {
            MapUpdateSystem.InitializePrefabs(EntityManager);
            if (panelPrefab.Index != 0)
            {
                return;
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var iconSize = uiDatam.GetIconSize(uiScale);
            panelPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(panelPrefab);
            EntityManager.AddComponent<MapUI>(panelPrefab);
            EntityManager.AddComponent<Minimap>(panelPrefab);
            EntityManager.AddComponentData(panelPrefab, new MapUIZoom(1f));
            EntityManager.AddComponent<VoxLink>(panelPrefab);
            EntityManager.AddComponent<GameOnlyUI>(panelPrefab);
            EntityManager.SetComponentData(panelPrefab, new PanelUI(PanelType.Minimap));
            EntityManager.AddComponentData(panelPrefab, new PanelPosition(new float3(iconSize.x, iconSize.y, 0) * 0.8f));
            EntityManager.SetComponentData(panelPrefab, new UIAnchor(AnchorUIType.TopRight));
            EntityManager.SetComponentData(panelPrefab, new Size2D(new float2(iconSize.x * 5, iconSize.y * 5)));
            EntityManager.SetComponentData(panelPrefab, new MaterialBaseColor { Value = new float4() });
            EntityManager.SetComponentData(panelPrefab, new MaterialFrameColor { Value = new float4() });
            EntityManager.AddComponent<OnChildrenSpawnedInt>(panelPrefab);
            EntityManager.RemoveComponent<NavigationDirty>(panelPrefab);
            EntityManager.RemoveComponent<GridUI>(panelPrefab);
            EntityManager.RemoveComponent<GridUIDirty>(panelPrefab);
        }
    }
}