using Unity.Entities;
using Zoxel.UI;
using Zoxel.Rendering;
using Zoxel.Textures;

namespace Zoxel.Maps
{
    //! Initializes material and mesh on a MapPart entity.
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class MinimapPartInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var materials = UIManager.instance.materials;
            var miniMapPartMaterial = materials.miniMapPartMaterial;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, MapPart, MinimapPart>()
                .ForEach((Entity e, in MeshSize2D meshSize2D) =>
            {
                var zoxMesh = new ZoxMesh();
                zoxMesh.mesh = MeshUtilities.CreateQuadMesh(meshSize2D.size);
                zoxMesh.material = new UnityEngine.Material(miniMapPartMaterial);
                zoxMesh.layer = worldUILayer;
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
        }
    }
}