using Unity.Entities;
using Zoxel.UI;
// needs Worlds, Voxels, UIs, Transforms, Textures

namespace Zoxel.Maps
{
    //! Holds systems about the map ui.
    /**
    *   \todo Add character icons
    *   \todo Move map parts based on player position
    *   \todo Mask the map parts inside the minimap total size
    *   \todo Quest Start markers on map
    *   \todo Quest Hand in markers on map
    *   \todo Move minimap and mapui out of characteruispawnsystem
    */
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class MapSystemGroup : ComponentSystemGroup { }
}
