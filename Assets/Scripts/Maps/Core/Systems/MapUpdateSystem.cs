using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Textures;
using Zoxel.Voxels;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Maps
{
    //! Prefabs for the map ui parts.
    public struct MapPrefabs
    {
        public Entity mapPartPrefab;
        public Entity arrowPrefab;

        public MapPrefabs(Entity mapPartPrefab, Entity arrowPrefab)
        {
            this.mapPartPrefab = mapPartPrefab;
            this.arrowPrefab = arrowPrefab;
        }
    }
    //! Updates the mapUI by spawning parts.
    /**
    *   \todo Use UICoreSystem's prefabs and just edit them for use with MapParts and ArrowPrefab.
    *   \todo Spawn new pieces for new chunks and position them in the grid, and delete old ui's, purely for visuals, dont want it confuse the player.
    *   \todo A map piece should also link to planet chunk. Chunk reference can be used as a starting point for the building.
    *       I can use chunk neighbors to navigate if ground is not found.
    *   \todo A chunk to contain any mapuilinks and then send signals directly when it's updated, instead of checking every frame. Can put the trigger system in map namespace.
    */
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
    public partial class MapUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery charactersQuery;
        private EntityQuery planetsQuery;
        private EntityQuery chunksQuery;
        public static MapPrefabs mapPrefabs;
        public static Entity mapPiecePrefabDebugMegaChunks;
        public static Entity mapPiecePrefabDebugMegaChunk2Ds;
        public static Entity mapPiecePrefabDebugBiomes;
        public static Entity mapPiecePrefabDebugTowns;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            InitializePrefabs(EntityManager);
            planetsQuery = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<EntityBusy>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<Planet>());
            chunksQuery = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<EntityBusy>(),
                ComponentType.ReadOnly<ChunkPosition>(),
                ComponentType.ReadOnly<Chunk>(),
				ComponentType.ReadOnly<ChunkNeighbors>(),
                ComponentType.ReadOnly<PlanetChunk>());
            charactersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<ChunkStreamPoint>());
            RequireForUpdate(processQuery);
        }

        public static void InitializePrefabs(EntityManager EntityManager)
        {
            var mapPartArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(SetUIElementPosition),
                typeof(MapPartBuilder),
                typeof(MapPart),
                typeof(MeshSize2D),
                typeof(UINavigational),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(Texture),
                typeof(ChunkDataLink),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(ZoxMesh),
                typeof(MaterialBaseColor),
                typeof(RenderQueue),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            MapUpdateSystem.mapPrefabs.mapPartPrefab = EntityManager.CreateEntity(mapPartArchetype);
            EntityManager.SetComponentData(MapUpdateSystem.mapPrefabs.mapPartPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(MapUpdateSystem.mapPrefabs.mapPartPrefab, new NonUniformScale { Value = new float3(1f, 1f, 1f) });
            var whiteColor = new Color(UnityEngine.Color.white);
            EntityManager.SetComponentData(MapUpdateSystem.mapPrefabs.mapPartPrefab, new MaterialBaseColor(whiteColor.ToFloat4()));
            EntityManager.SetComponentData(MapUpdateSystem.mapPrefabs.mapPartPrefab, new RenderQueue(UICoreSystem.buttonQueue + 10));
            var arrowArchtype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(MapArrow),
                typeof(IgnoreGrid),
                typeof(CharacterMapIcon),
                typeof(UIElement), typeof(UIMeshData), typeof(RaycastUIOrigin),
                typeof(BasicRender),
                typeof(Size2D),
                typeof(CharacterLink),
                typeof(ParentLink),
                typeof(LocalPosition),
                typeof(LocalRotation),
                typeof(NewChild),
                typeof(Zoxel.Transforms.Child),
                typeof(ZoxMesh),
                typeof(MaterialBaseColor),
                typeof(RenderQueue),
                typeof(LocalToWorld),
                typeof(Translation),
                typeof(Rotation),
                typeof(NonUniformScale));
            MapUpdateSystem.mapPrefabs.arrowPrefab = EntityManager.CreateEntity(arrowArchtype);
            EntityManager.SetComponentData(MapUpdateSystem.mapPrefabs.arrowPrefab, LocalRotation.Identity);
            EntityManager.SetComponentData(MapUpdateSystem.mapPrefabs.arrowPrefab, new NonUniformScale { Value = new float3(2f, 2f, 2f) });
            EntityManager.SetComponentData(MapUpdateSystem.mapPrefabs.arrowPrefab, new MaterialBaseColor(whiteColor.ToFloat4()));
            EntityManager.SetComponentData(MapUpdateSystem.mapPrefabs.arrowPrefab, new RenderQueue(UICoreSystem.iconQueue + 15));
        }

        private void InitializeTestPrefabs()
        {
            if (mapPiecePrefabDebugMegaChunks.Index != 0)
            {
                return;
            }
            // DebugMegaChunks
            mapPiecePrefabDebugMegaChunks = EntityManager.Instantiate(MapUpdateSystem.mapPrefabs.mapPartPrefab);
            EntityManager.AddComponent<Prefab>(mapPiecePrefabDebugMegaChunks);
            EntityManager.RemoveComponent<MapPartBuilder>(mapPiecePrefabDebugMegaChunks);
            EntityManager.AddComponent<MapPartBuilderMegaChunks>(mapPiecePrefabDebugMegaChunks);
            // DebugMegaChunk2Ds
            mapPiecePrefabDebugMegaChunk2Ds = EntityManager.Instantiate(MapUpdateSystem.mapPrefabs.mapPartPrefab);
            EntityManager.AddComponent<Prefab>(mapPiecePrefabDebugMegaChunk2Ds);
            EntityManager.RemoveComponent<MapPartBuilder>(mapPiecePrefabDebugMegaChunk2Ds);
            EntityManager.AddComponent<MapPartBuilderMegaChunk2Ds>(mapPiecePrefabDebugMegaChunk2Ds);
            // DebugBiomes
            mapPiecePrefabDebugBiomes = EntityManager.Instantiate(MapUpdateSystem.mapPrefabs.mapPartPrefab);
            EntityManager.AddComponent<Prefab>(mapPiecePrefabDebugBiomes);
            EntityManager.RemoveComponent<MapPartBuilder>(mapPiecePrefabDebugBiomes);
            EntityManager.AddComponent<MapPartBuilderBiomes>(mapPiecePrefabDebugBiomes);
            // DebugBiomes
            mapPiecePrefabDebugTowns = EntityManager.Instantiate(MapUpdateSystem.mapPrefabs.mapPartPrefab);
            EntityManager.AddComponent<Prefab>(mapPiecePrefabDebugTowns);
            EntityManager.RemoveComponent<MapPartBuilder>(mapPiecePrefabDebugTowns);
            EntityManager.AddComponent<MapBuilderTowns>(mapPiecePrefabDebugTowns);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializeTestPrefabs();
            var mapPartPrefab = MapUpdateSystem.mapPrefabs.mapPartPrefab;
            var arrowPrefab = MapUpdateSystem.mapPrefabs.arrowPrefab;
			var isDebugMapMegaChunks = UIManager.instance.uiSettings.isDebugMapMegaChunks;
			var isDebugMapMegaChunk2Ds = UIManager.instance.uiSettings.isDebugMapMegaChunk2Ds;
			var isDebugMapBiomes = UIManager.instance.uiSettings.isDebugMapBiomes;
			var isDebugMapTowns = UIManager.instance.uiSettings.isDebugMapTowns;
            if (isDebugMapMegaChunks)
            {
                mapPartPrefab = mapPiecePrefabDebugMegaChunks;
            }
            else if (isDebugMapMegaChunk2Ds)
            {
                mapPartPrefab = mapPiecePrefabDebugMegaChunk2Ds;
            }
            else if (isDebugMapBiomes)
            {
                mapPartPrefab = mapPiecePrefabDebugBiomes;
            }
            else if (isDebugMapTowns)
            {
                mapPartPrefab = mapPiecePrefabDebugTowns;
            }
            var mapPrefabs = new MapPrefabs(mapPartPrefab, arrowPrefab);
            var playerIconMultiplier = UIManager.instance.uiDatam.playerIconMultiplier;
            var whiteColor = new Color(UnityEngine.Color.white);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var characterEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunkStreamPoints = GetComponentLookup<ChunkStreamPoint>(true);
            var characterChunkLinks = GetComponentLookup<ChunkLink>(true);
            characterEntities.Dispose();
            var voxEntities = planetsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var planetChunkLinks = GetComponentLookup<ChunkLinks>(true);
            voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            chunkEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, OnChildrenSpawned, NavigationDirty>()
                .ForEach((Entity e, int entityInQueryIndex, ref MapUI mapUI, in MapUIZoom mapUIZoom, in VoxLink voxLink, in SetMapSize setMapSize, in CharacterLink characterLink) =>
            {
                if (mapUI.size == setMapSize.newSize)
                {
                    PostUpdateCommands.RemoveComponent<SetMapSize>(entityInQueryIndex, e);
                    return;
                }
                var voxEntity = voxLink.vox;
                var characterEntity = characterLink.character;
                var isMinimap = HasComponent<Minimap>(e);
                var zoomLevel = mapUIZoom.zoomLevel;
                var newSize = setMapSize.newSize;
                var childrenCount = MapUpdateSystem.SpawnMapParts(PostUpdateCommands, entityInQueryIndex, mapPrefabs,
                    e, ref mapUI, isMinimap, newSize, zoomLevel, playerIconMultiplier, characterEntity, voxLink.vox,
                    in chunkStreamPoints, in characterChunkLinks, in planetChunkLinks, in chunkPositions);
                if (childrenCount == 0)
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<SetMapSize>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<DestroyChildren>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChildrenSpawnedInt(childrenCount));
                PostUpdateCommands.AddComponent<OnChildrenSpawned>(entityInQueryIndex, e);
                if (!isMinimap)
                {
                    PostUpdateCommands.AddComponent<NavigationDirty>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(chunkStreamPoints)
				.WithReadOnly(characterChunkLinks)
				.WithReadOnly(planetChunkLinks)
				.WithReadOnly(chunkPositions)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }


        public static bool CanSpawnMapParts(Entity characterEntity, Entity voxEntity,
            in ComponentLookup<ChunkLink> characterChunkLinks, in ComponentLookup<ChunkLinks> planetChunkLinks,
            in ComponentLookup<ChunkPosition> chunkPositions)
        {
            if (voxEntity.Index == 0 || !planetChunkLinks.HasComponent(voxEntity))
            {
                // UnityEngine.Debug.LogError("voxEntity Index is 0.");
                return false;
            }
            var chunkLinks3 = planetChunkLinks[voxEntity];
            if (!characterChunkLinks.HasComponent(characterEntity))
            {
                // UnityEngine.Debug.LogError("characterEntity Index is 0.");
                return false;
            }
            var firstChunkEntity = characterChunkLinks[characterEntity].chunk;
            if (firstChunkEntity.Index == 0 || !chunkPositions.HasComponent(firstChunkEntity))
            {
                // UnityEngine.Debug.LogError("firstChunkEntity Index is 0.");
                return false;
            }
            return true;
        }

        public static int SpawnMapParts(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, MapPrefabs mapPrefabs,
            Entity mapEntity, ref MapUI mapUI, bool isMinimap, byte newSize, float zoomLevel, float playerIconMultiplier,
            Entity characterEntity, Entity voxEntity, in ComponentLookup<ChunkStreamPoint> chunkStreamPoints,
            in ComponentLookup<ChunkLink> characterChunkLinks, in ComponentLookup<ChunkLinks> planetChunkLinks,
            in ComponentLookup<ChunkPosition> chunkPositions, bool isMapEntityPending = false)
        {
            if (voxEntity.Index == 0 || !planetChunkLinks.HasComponent(voxEntity))
            {
                // UnityEngine.Debug.LogError("voxEntity Index is 0.");
                return 0;
            }
            var chunkLinks3 = planetChunkLinks[voxEntity];
            if (!characterChunkLinks.HasComponent(characterEntity))
            {
                // UnityEngine.Debug.LogError("characterEntity Index is 0.");
                return 0;
            }
            var firstChunkEntity = characterChunkLinks[characterEntity].chunk;
            if (firstChunkEntity.Index == 0 || !chunkPositions.HasComponent(firstChunkEntity))
            {
                // UnityEngine.Debug.LogError("firstChunkEntity Index is 0.");
                return 0;
            }
            var playerChunkPosition = int3.zero;
            if (chunkStreamPoints.HasComponent(characterEntity))
            {
                playerChunkPosition = chunkStreamPoints[characterEntity].chunkPosition;
            }
            var renderDistance = newSize;
            var mapPosition = new int2(- renderDistance, + renderDistance);
            mapPosition += new int2(playerChunkPosition.x, playerChunkPosition.z);
            mapUI.mapPosition = mapPosition;
            mapUI.size = newSize;
            var iconSize = mapUI.GetIconSize();
            var iconSize2 = mapUI.GetIconSize() * zoomLevel;
            var gridSize = mapUI.GetGridSize();
            int rowCount = renderDistance + renderDistance + 1;
            var totalMapParts = rowCount * rowCount;
            var firstChunkPosition = chunkPositions[firstChunkEntity].position;
            var mapParts = new NativeArray<Entity>(totalMapParts, Allocator.Temp);
            PostUpdateCommands.Instantiate(entityInQueryIndex, mapPrefabs.mapPartPrefab, mapParts);
            if (!isMapEntityPending)
            {
                PostUpdateCommands.AddComponent(entityInQueryIndex, mapParts, new ParentLink(mapEntity));
            }
            PostUpdateCommands.AddComponent(entityInQueryIndex, mapParts, new MeshSize2D(iconSize));   //  * 2f * 2f
            PostUpdateCommands.AddComponent(entityInQueryIndex, mapParts, new Size2D(iconSize2));
            if (isMinimap)
            {
                PostUpdateCommands.AddComponent<MapPartMoving>(entityInQueryIndex, mapParts);
                PostUpdateCommands.AddComponent<MinimapPart>(entityInQueryIndex, mapParts);
                PostUpdateCommands.RemoveComponent<UINavigational>(entityInQueryIndex, mapParts);
            }
            else
            {
                PostUpdateCommands.AddComponent(entityInQueryIndex, mapParts, new NonUniformScale { Value = new float3(1f, 1f, 1f) * zoomLevel });
            }
            for (int chunkIndex = 0; chunkIndex < totalMapParts; chunkIndex++)
            {
                var mapPartEntity = mapParts[chunkIndex];
                if (isMapEntityPending)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, mapPartEntity, new ParentLink(mapEntity));
                }
                UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, mapPartEntity, chunkIndex);
                var x = (chunkIndex % rowCount);
                var z = (chunkIndex / rowCount);
                var chunkPosition = new int3(mapPosition.x + x, firstChunkPosition.y, mapPosition.y - z);
                Entity chunkEntity;
                if (chunkLinks3.chunks.TryGetValue(chunkPosition, out chunkEntity))
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, mapPartEntity, new ChunkDataLink(new int2(mapPosition.x + x, mapPosition.y - z), chunkEntity));
                }
                else
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, mapPartEntity, new ChunkDataLink(new int2(mapPosition.x + x, mapPosition.y - z), new Entity()));
                }
                if (!isMinimap)
                {
                    var cellPosition = new int2(x, z);
                    var mapPartPosition = GridUIPositionSystem.GetGridPosition(cellPosition, gridSize, iconSize2, float2.zero, float2.zero);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, mapPartEntity, new LocalPosition(mapPartPosition));
                }
            }
            mapParts.Dispose();
            // Spawn Arrow
            var arrowEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, mapPrefabs.arrowPrefab, mapEntity);
            PostUpdateCommands.AddComponent<PlayerMapIcon>(entityInQueryIndex, arrowEntity);
            PostUpdateCommands.SetComponent(entityInQueryIndex, arrowEntity, new CharacterLink(characterEntity));
            UICoreSystem.SetChild(PostUpdateCommands, entityInQueryIndex, arrowEntity, totalMapParts);
            PostUpdateCommands.SetComponent(entityInQueryIndex, arrowEntity, new Size2D(iconSize2 * playerIconMultiplier));
            PostUpdateCommands.SetComponent(entityInQueryIndex, arrowEntity, new NonUniformScale { Value = new float3(1f, 1f, 1f) * zoomLevel * 2f });
            // link map to player map icon link
            PostUpdateCommands.AddComponent(entityInQueryIndex, mapEntity, new PlayerMapIconLink(arrowEntity));
            totalMapParts++;
            return totalMapParts;
        }
    }
}