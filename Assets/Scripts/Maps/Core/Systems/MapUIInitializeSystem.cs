using Unity.Entities;
using Zoxel.UI;
using Zoxel.Rendering;

namespace Zoxel.Maps
{
    //! Initializes material and mesh on a MapUI entity.
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class MapUIInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var materials = UIManager.instance.materials;
            var subPanelMaterial = materials.subPanelMaterial;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            // SubPanel MapUI
            Entities
                .WithNone<PanelUI>()
                .WithAll<InitializeEntity, MapUI>()
                .ForEach((Entity e, ZoxMesh zoxMesh, in Size2D size2D, in UIMeshData uiMeshData) =>
            {
                zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size2D.size, uiMeshData.horizontalAlignment, uiMeshData.verticalAlignment, uiMeshData.offset);
                zoxMesh.material = new UnityEngine.Material(subPanelMaterial);
                zoxMesh.layer = worldUILayer;
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
        }
    }
}