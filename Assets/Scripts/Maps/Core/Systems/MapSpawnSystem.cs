using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Cameras;
using Zoxel.UI;
using Zoxel.Rendering;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.Rendering.Authoring;

namespace Zoxel.Maps
{
    //! Spawns a Map UI! \todo Use SpawnMap as process query, it's more efficient.
    /**
    *   - UI Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
    public partial class MapSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery charactersQuery;
        private EntityQuery planetsQuery;
        private EntityQuery chunksQuery;
        public static Entity spawnMapPrefab;
        private Entity mapPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnMap),
                typeof(CharacterLink),
                typeof(GenericEvent),
                typeof(DelayEvent));
            MapSpawnSystem.spawnMapPrefab = EntityManager.CreateEntity(spawnArchetype);
            processQuery = GetEntityQuery(
                ComponentType.ReadOnly<SpawnMap>(),
                ComponentType.ReadOnly<CharacterLink>(),
                ComponentType.Exclude<DelayEvent>());
            planetsQuery = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<EntityBusy>(),
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<Planet>());
            chunksQuery = GetEntityQuery(
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<EntityBusy>(),
                ComponentType.ReadOnly<ChunkPosition>(),
                ComponentType.ReadOnly<Chunk>(),
				ComponentType.ReadOnly<ChunkNeighbors>(),
                ComponentType.ReadOnly<PlanetChunk>());
            charactersQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<ChunkStreamPoint>());
            RequireForUpdate(processQuery);
            RequireForUpdate<RenderSettings>();
        }

        private void InitializePrefabs()
        {
            MapUpdateSystem.InitializePrefabs(EntityManager);
            if (mapPrefab.Index != 0)
            {
                return;
            }
            mapPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(mapPrefab);
            EntityManager.RemoveComponent<GridUI>(mapPrefab);
            EntityManager.RemoveComponent<GridUIDirty>(mapPrefab);
            EntityManager.AddComponent<MapUI>(mapPrefab);
            EntityManager.SetComponentData(mapPrefab, new PanelUI(PanelType.Map));
            EntityManager.AddComponent<GameOnlyUI>(mapPrefab);
            EntityManager.AddComponent<VoxLink>(mapPrefab);
            EntityManager.AddComponentData(mapPrefab, new MapUIZoom(2.5f));
            EntityManager.SetComponentData(mapPrefab, new RaycastUIOrigin(new float2(0.5f, 0.5f)));
            EntityManager.AddComponent<OnChildrenSpawnedInt>(mapPrefab);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var renderSettings = GetSingleton<RenderSettings>();
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var iconSize = uiDatam.GetIconSize(uiScale);
            var panelStyle = uiDatam.panelStyle.GetStyle();
            var renderDistance = renderSettings.renderDistance - 1;
            var mapRenderDistance = (byte) math.clamp(UIManager.instance.uiSettings.mapRenderDistance, 1, renderDistance);
            var margins = uiDatam.GetIconMargins(uiScale);
            var mapPrefab = this.mapPrefab;
            var playerIconMultiplier = UIManager.instance.uiDatam.playerIconMultiplier;
            var mapPrefabs = MapUpdateSystem.mapPrefabs;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var spawnMapEntities = processQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleZ);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleZ);
			var characterLinks = GetComponentLookup<CharacterLink>(true);
            // map parts
            var characterEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunkStreamPoints = GetComponentLookup<ChunkStreamPoint>(true);
            var characterChunkLinks = GetComponentLookup<ChunkLink>(true);
            characterEntities.Dispose();
            var voxEntities = planetsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var planetChunkLinks = GetComponentLookup<ChunkLinks>(true);
            voxEntities.Dispose();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            chunkEntities.Dispose();
            Dependency = Entities
                // .WithStoreProcess(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, in UILink uiLink, in CameraLink cameraLink, in VoxLink voxLink) =>
            {
                if (cameraLink.camera.Index == 0)
                {
                    return;
                }
                var spawnMapEntity = new Entity();
                for (int i = 0; i < spawnMapEntities.Length; i++)
                {
                    var e2 = spawnMapEntities[i];
                    var characterLink = characterLinks[e2];
                    if (characterLink.character == e)
                    {
                        spawnMapEntity = e2;
                        break;
                    }
                }
                if (spawnMapEntity.Index == 0)
                {
                    return;
                }
                PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab), new OnUISpawned(e, 1));
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, mapPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CharacterLink(e));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new CameraLink(cameraLink.camera));
                UICoreSystem.SetTextureFrame(PostUpdateCommands, entityInQueryIndex, e3, in panelStyle.frameGenerationData);
                var mapUI = new MapUI(mapRenderDistance, iconSize.x);
                var totalLength = iconSize.x * 7;
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new Size2D(new float2(totalLength, totalLength) + margins));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, voxLink);
                // spawn parts
                var zoomLevel = 2.5f;
                var isMinimap = false;
                var characterEntity = e;
                var childrenCount = MapUpdateSystem.SpawnMapParts(PostUpdateCommands, entityInQueryIndex, mapPrefabs,
                    e3, ref mapUI, isMinimap, mapRenderDistance, zoomLevel, playerIconMultiplier, characterEntity, voxLink.vox,
                    in chunkStreamPoints, in characterChunkLinks, in planetChunkLinks, in chunkPositions, true);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, mapUI);
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawnedInt(childrenCount));
            })  .WithReadOnly(spawnMapEntities)
                .WithDisposeOnCompletion(spawnMapEntities)
                .WithReadOnly(characterLinks)
                .WithReadOnly(chunkStreamPoints)
				.WithReadOnly(characterChunkLinks)
				.WithReadOnly(planetChunkLinks)
				.WithReadOnly(chunkPositions)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}