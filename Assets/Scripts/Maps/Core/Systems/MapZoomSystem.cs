using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Maps
{
    //! Changes zoom level of map by spawning or despawning parts.
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
    public partial class MapZoomSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var smallestZoom = 0.6f;
            var biggestZoom = 3.5f;
            var incrementA = 0.1f;
            var incrementB = 0.25f;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<MapZoomIn>()
                .ForEach((Entity e, int entityInQueryIndex, ref MapUIZoom mapUIZoom) =>
            {
                PostUpdateCommands.RemoveComponent<MapZoomIn>(entityInQueryIndex, e);
                if (mapUIZoom.zoomLevel > biggestZoom)
                {
                    return;
                }
                // enlarge scale and reposition uis
                if (mapUIZoom.zoomLevel >= 1f)
                {
                    mapUIZoom.zoomLevel += incrementB;
                }
                else
                {
                    mapUIZoom.zoomLevel += incrementA;
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<MapZoomOut>()
                .ForEach((Entity e, int entityInQueryIndex, ref MapUIZoom mapUIZoom) =>
            {
                PostUpdateCommands.RemoveComponent<MapZoomOut>(entityInQueryIndex, e);
                if (mapUIZoom.zoomLevel < smallestZoom)
                {
                    return;
                }
                // enlarge scale and reposition uis
                if (mapUIZoom.zoomLevel > 1f)
                {
                    mapUIZoom.zoomLevel -= incrementB;
                }
                else
                {
                    mapUIZoom.zoomLevel -= incrementA;
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}