using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Voxels;
using Zoxel.UI;

namespace Zoxel.Maps
{
    //! Streams Map UI when MapUI.position updates.
    /**
    *   When position updates, this will re link MapPart's to the Chunk entities.
    */
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
    public partial class MapUIStreamStartSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunkStreamPointQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            chunkStreamPointQuery = GetEntityQuery(
                ComponentType.ReadOnly<ChunkStreamPoint>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>());
            chunkStreamPointQuery.SetChangedVersionFilter(typeof(ChunkStreamPoint));
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var characterEntities = chunkStreamPointQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var chunkStreamPoints = GetComponentLookup<ChunkStreamPoint>(true);
            characterEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<OnChildrenSpawned, GridUIDirty, InitializeEntity>()
                .WithNone<SetMapSize, MapUIPositionUpdated>()
                .ForEach((Entity e, int entityInQueryIndex, ref MapUI mapUI, in CharacterLink characterLink) =>
            {
                if (!chunkStreamPoints.HasComponent(characterLink.character))
                {
                    return;
                }
                var mapPosition = new int2(- mapUI.renderDistance, mapUI.renderDistance);
                var position = chunkStreamPoints[characterLink.character].chunkPosition;
                mapPosition += new int2(position.x, position.z);
                if (mapPosition != mapUI.mapPosition)
                {
                    mapUI.firstChunkPosition = new int2(position.x, position.z);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new MapUIPositionUpdated(mapPosition));
                }
            })  .WithReadOnly(chunkStreamPoints)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}