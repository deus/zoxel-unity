using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.UI;

namespace Zoxel.Maps
{
    //! Sends map position data onto each MapPart's shader.
    /**
    *   Note: Has to update in Zender group due to setting material data.
    */
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class MaterialMapPositionSystem : SystemBase
    {
        private const string vectorName = "mapPosition";
        private const string vectorName2 = "mapRotation";
        private const string floatName = "mapSize";
        private EntityQuery processQuery;
        private EntityQuery parentsQuery;

        protected override void OnCreate()
        {
            parentsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<Size2D>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            //var rotation2 = CameraReferences.GetMainCamera(EntityManager).transform.rotation;
            //var rotation = new quaternion(rotation2.x, rotation2.y, rotation2.z, rotation2.w);
            var entities = parentsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var translations = GetComponentLookup<Translation>(true);
            var rotations = GetComponentLookup<Rotation>(true);
            var size2Ds = GetComponentLookup<Size2D>(true);
            entities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity, InitializeEntity>()
                .WithAll<MapPart>()
                .ForEach((Entity e, in ZoxMesh zoxMesh, in ParentLink parentLink) =>
            {
                if (zoxMesh.material == null || parentLink.parent.Index == 0)
                {
                    return;
                }
                var parentPosition = translations[parentLink.parent].Value;
                zoxMesh.material.SetVector(vectorName, new UnityEngine.Vector3(parentPosition.x, parentPosition.y, parentPosition.z));
                if (!HasComponent<MapPartMoving>(e))
                {
                    var parentRotation = rotations[parentLink.parent].Value;
                    parentRotation = math.inverse(parentRotation);
                    zoxMesh.material.SetVector(vectorName2, new UnityEngine.Vector4(parentRotation.value.x, parentRotation.value.y, parentRotation.value.z, parentRotation.value.w));
                }
                /*else
                {
                    var uiElement = uiElements[parentLink.parent].position;
                    zoxMesh.material.SetVector(vectorName, new UnityEngine.Vector3(uiElement.x, uiElement.y, 0));
                }*/
                var parentSize = size2Ds[parentLink.parent].size;
                zoxMesh.material.SetFloat(floatName, parentSize.x * 0.8f);
            })  .WithReadOnly(translations)
                .WithReadOnly(rotations)
                .WithReadOnly(size2Ds)
                .WithoutBurst().Run();
        }
    }
}