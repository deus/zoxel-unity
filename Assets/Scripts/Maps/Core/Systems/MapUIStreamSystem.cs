using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Zoxel.Voxels;
using Zoxel.Transforms;

namespace Zoxel.Maps
{
    //! Streams Map UI when MapUI.position updates.
    /**
    *   When position updates, this will re link MapPart's to the Chunk entities.
    *   \todo Fix for planet sides by changing position of map ui based on quadrant.
    *   \todo Atm I need to move old UIs to other side of map - reposition any basically, but keep old ones in right position
    */
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
    public partial class MapUIStreamSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery voxesQuery;
        private EntityQuery mapPartQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Vox>(),
                ComponentType.ReadOnly<Planet>(),
                ComponentType.ReadOnly<ChunkLinks>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.Exclude<StreamVox>(),
                ComponentType.Exclude<OnChunksSpawned>());
            mapPartQuery = GetEntityQuery(
                ComponentType.ReadOnly<MapPart>(),
                ComponentType.ReadOnly<ChunkDataLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
			var isDebugMapMegaChunks = UIManager.instance.uiSettings.isDebugMapMegaChunks;
			var isDebugMapMegaChunk2Ds = UIManager.instance.uiSettings.isDebugMapMegaChunk2Ds;
			var isDebugMapBiomes = UIManager.instance.uiSettings.isDebugMapBiomes;
			var isDebugMapTowns = UIManager.instance.uiSettings.isDebugMapTowns;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var chunkLinks = GetComponentLookup<ChunkLinks>(true);
            voxEntities.Dispose();
            var mapPartEntities = mapPartQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunkDataLinks = GetComponentLookup<ChunkDataLink>(true);
            mapPartEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<SetMapSize, OnChildrenSpawned, GridUIDirty>()
                .WithAll<MapUIPositionUpdated>()
                .ForEach((Entity e, int entityInQueryIndex, ref MapUI mapUI, in MapUIPositionUpdated mapUIPositionUpdated, in VoxLink voxLink, in Childrens childrens) =>
            {
                var voxEntity = voxLink.vox;
                if (!chunkLinks.HasComponent(voxEntity))
                {
                    return;
                }
                var chunkLinks2 = chunkLinks[voxEntity];
                mapUI.mapPosition = mapUIPositionUpdated.position;
                PostUpdateCommands.RemoveComponent<MapUIPositionUpdated>(entityInQueryIndex, e);
                var positionY = 0;
                var rowCount = mapUI.size + mapUI.size + 1; // 7;
                // var chunksHashmap = new NativeHashMap<int3, Entity>(vox.chunks.Length, Allocator.Temp);
                foreach (var KVP in chunkLinks2.chunks)
                {
                    var chunkPosition = KVP.Key;
                    if (chunkPosition.y > positionY)
                    {
                        positionY = chunkPosition.y;
                    }
                }
                //UnityEngine.Debug.LogError("Streaming Map at position: " + mapUI.mapPosition);
                var mapPosition = mapUI.mapPosition;
                // var firstChunkPosition = mapUI.firstChunkPosition;
                var firstChunkPosition = new int3(mapUI.firstChunkPosition.x, positionY, mapUI.firstChunkPosition.y);
                // first get totalPositions needed
                var totalPositions = new NativeList<int2>();
                var keepPositions = new NativeList<int2>();
                var newPositions = new NativeList<int2>();
                var freeMapParts = new NativeList<Entity>();
                // Calculate total positions of our map
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    var child = childrens.children[i];
                    if (!chunkDataLinks.HasComponent(child))
                    {
                        continue;
                    }
                    int x = (i % rowCount);
                    int z = (i / rowCount);
                    var cellPosition = new int2(mapPosition.x + x, mapPosition.y - z);
                    totalPositions.Add(cellPosition);
                    //UnityEngine.Debug.LogError("Total Position [" + i + "] : " + cellPosition);
                }
                // Get positions to keep or old uis to use
                var gridSize = mapUI.GetGridSize();
                var iconSize = mapUI.GetIconSize();
                for (int i = 0; i < childrens.children.Length; i++) 
                {
                    var mapPartEntity = childrens.children[i];
                    if (!chunkDataLinks.HasComponent(mapPartEntity))
                    {
                        continue;
                    }
                    var chunkDataLink = chunkDataLinks[mapPartEntity];
                    var cellPosition = chunkDataLink.position; //  - mapUI.mapPosition;
                    if (!totalPositions.Contains(cellPosition))
                    {
                        //UnityEngine.Debug.LogError("Remove Position [" + i + "] : " + cellPosition);
                        freeMapParts.Add(mapPartEntity);
                    }
                    else
                    {
                        keepPositions.Add(cellPosition);
                        //UnityEngine.Debug.LogError("Keep Position [" + i + "] : " + cellPosition);
                        if (!HasComponent<MapPartMoving>(mapPartEntity)) // mapUI.isPlayerArrows == 0)
                        {
                            var mapPartPosition = GridUIPositionSystem.GetGridPosition((cellPosition - mapUI.mapPosition).FlipY(), gridSize, iconSize, float2.zero, float2.zero);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, mapPartEntity, new LocalPosition(mapPartPosition));
                        }
                    }
                }
                // get new positions needed
                for (int i = 0; i < totalPositions.Length; i++)
                {
                    var cellPosition = totalPositions[i];
                    if (!keepPositions.Contains(cellPosition))
                    {           
                        // need this new position
                        newPositions.Add(cellPosition);
                        //UnityEngine.Debug.LogError("New Position [" + i + "] : " + cellPosition);
                    }
                }
                if (freeMapParts.Length != newPositions.Length)
                {
                    totalPositions.Dispose();
                    keepPositions.Dispose();
                    newPositions.Dispose();
                    freeMapParts.Dispose();
                    /*UnityEngine.Debug.LogError("Maths No Workie! freeMapParts: " + freeMapParts.Length
                        + ", newPositions: " + newPositions.Length
                        + ", keepPositions: " + keepPositions.Length
                        + ", totalPositions: " + totalPositions.Length
                        );*/
                    return;
                }
                /*UnityEngine.Debug.LogError("Maths Workie! freeMapParts: " + freeMapParts.Length
                        + ", newPositions: " + newPositions.Length
                        + ", keepPositions: " + keepPositions.Length
                        + ", totalPositions: " + totalPositions.Length
                        );*/
                //! Reuse old parts for new chunk positions
                for (int i = 0; i < newPositions.Length; i++)
                {
                    var cellPosition = newPositions[i];
                    var chunkPositionMap = new int3(cellPosition.x, firstChunkPosition.y, cellPosition.y);
                    Entity chunkEntity;
                    //! Uses Hashmap for inventory for fast access.
                    if (chunkLinks2.chunks.TryGetValue(chunkPositionMap, out chunkEntity))
                    {
                        var mapPartEntity = freeMapParts[i];
                        PostUpdateCommands.SetComponent(entityInQueryIndex, mapPartEntity, new ChunkDataLink(cellPosition, chunkEntity));
                        if (!HasComponent<MapPartMoving>(mapPartEntity)) // mapUI.isPlayerArrows == 0)
                        {
                            var mapPartPosition = GridUIPositionSystem.GetGridPosition((cellPosition - mapUI.mapPosition).FlipY(), gridSize, iconSize, float2.zero, float2.zero);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, mapPartEntity, new LocalPosition(mapPartPosition));
                        }
						if (isDebugMapMegaChunks)
						{
							PostUpdateCommands.AddComponent<MapPartBuilderMegaChunks>(entityInQueryIndex, mapPartEntity);
						}
						else if (isDebugMapMegaChunk2Ds)
						{
							PostUpdateCommands.AddComponent<MapPartBuilderMegaChunk2Ds>(entityInQueryIndex, mapPartEntity);
						}
                        else if (isDebugMapBiomes)
                        {
							PostUpdateCommands.AddComponent<MapPartBuilderBiomes>(entityInQueryIndex, mapPartEntity);
                        }
						else if (isDebugMapTowns)
						{
							PostUpdateCommands.AddComponent<MapBuilderTowns>(entityInQueryIndex, mapPartEntity);
						}
						else
						{
							PostUpdateCommands.AddComponent<MapPartBuilder>(entityInQueryIndex, mapPartEntity);
						}
                    }
                }
                totalPositions.Dispose();
                keepPositions.Dispose();
                newPositions.Dispose();
                freeMapParts.Dispose();
            })  .WithReadOnly(chunkLinks)
                .WithReadOnly(chunkDataLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}