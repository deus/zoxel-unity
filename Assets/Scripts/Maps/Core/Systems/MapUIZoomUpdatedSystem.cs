using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.UI;

namespace Zoxel.Maps
{
    //! Changes zoom level of map by spawning or despawning parts.
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
    public partial class MapUIZoomUpdatedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithChangeFilter<MapUIZoom>()
                .ForEach((int entityInQueryIndex, in MapUIZoom mapUIZoom, in MapUI mapUI, in Childrens childrens) =>
            {
                var iconSize = mapUI.GetIconSize() * mapUIZoom.zoomLevel;
                var gridSize = mapUI.GetGridSize();
                var renderDistance = mapUI.size;
                int rowCount = renderDistance + renderDistance + 1;
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    var mapPartEntity = childrens.children[i];
                    if (!HasComponent<MapPart>(mapPartEntity))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, mapPartEntity, new NonUniformScale { Value = new float3(1f, 1f, 1f) * mapUIZoom.zoomLevel * 2f });
                        continue;
                    }
                    var cellPosition = new int2((i % rowCount), (i / rowCount));
                    var mapPartPosition = GridUIPositionSystem.GetGridPosition(cellPosition, gridSize, iconSize, float2.zero, float2.zero);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, mapPartEntity, new LocalPosition(mapPartPosition));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, mapPartEntity, new NonUniformScale { Value = new float3(1f, 1f, 1f) * mapUIZoom.zoomLevel });
                    PostUpdateCommands.SetComponent(entityInQueryIndex, mapPartEntity, new Size2D(iconSize));
                    PostUpdateCommands.AddComponent<SetUIElementPosition>(entityInQueryIndex, mapPartEntity);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}