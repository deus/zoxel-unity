﻿/*using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels;
using Zoxel.UI;

namespace Zoxel.Maps
{
    //! Initialially spawns the map ui - well sets it to dirty.. For another system to.
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
    public partial class MapUISystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery characterQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.ReadOnly<SpawnMapParts>(),
                ComponentType.ReadOnly<MapUI>(),
                ComponentType.ReadOnly<CharacterLink>());
            characterQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Character>(),
                ComponentType.ReadOnly<VoxLink>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (processQuery.IsEmpty)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var characterEntities = characterQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var voxLinks = GetComponentLookup<VoxLink>(true);
            characterEntities.Dispose();
            Dependency = Entities
                .WithAll<SpawnMapParts>()
                .ForEach((Entity e, int entityInQueryIndex, in MapUI mapUI, in CharacterLink characterLink) =>
            {
                var characterEntity = characterLink.character;
                if (voxLinks.HasComponent(characterEntity))
                {
                    PostUpdateCommands.RemoveComponent<SpawnMapParts>(entityInQueryIndex, e);
                    var voxLink = voxLinks[characterEntity];
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, voxLink);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new SetMapSize(mapUI.renderDistance));
                }
            })  .WithReadOnly(voxLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}*/