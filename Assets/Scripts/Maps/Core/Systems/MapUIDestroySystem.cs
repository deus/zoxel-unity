using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Maps
{
    //! Uses DestroyEntity to dispose of MapPart and destroy MapUI icons.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
	public partial class MapUIDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // MapUI
            Dependency = Entities
                .WithAll<DestroyEntity, MapUI>()
                .ForEach((int entityInQueryIndex, in MapUI mapUI) =>
            {
                if (HasComponent<CharacterMapIcon>(mapUI.characterIcon))
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, mapUI.characterIcon);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // MapPart UI
            Dependency = Entities
                .WithAll<DestroyEntity, MapPart>()
                .ForEach((in MapPart mapPart) =>
            {
                mapPart.DisposeFinal();
            }).ScheduleParallel(Dependency);
        }
    }
}