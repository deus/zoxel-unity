using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Maps
{
    //! A ui that shows a map of the world.
    public struct MapUI : IComponentData
    {
        public float iconSize;
        public int renderDistance;
        public int size;
        public int2 mapPosition;
        public int2 firstChunkPosition;
        public int firstChunkPositionY;
        public Entity characterIcon;

        public MapUI(int renderDistance, float iconSize)
        {
            this.characterIcon = new Entity();
            this.size = 0;
            this.mapPosition = int2.zero;
            this.firstChunkPosition = int2.zero;
            this.firstChunkPositionY = 0;
            float iconRatio = 3f / renderDistance;
            this.iconSize = iconRatio * iconSize;
            this.renderDistance = renderDistance;
        }

        public int GetRowCount()
        {
            return renderDistance + renderDistance + 1;
        }

        public int2 GetGridSize()
        {
            var rowCount = GetRowCount();
            return new int2(rowCount, rowCount);
        }

        public float2 GetIconSize()
        {
            return new float2(iconSize, iconSize);
        }
    }
}