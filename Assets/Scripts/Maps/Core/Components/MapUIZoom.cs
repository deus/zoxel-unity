using Unity.Entities;

namespace Zoxel.Maps
{
    //! A ui that shows a map of the world.
    public struct MapUIZoom : IComponentData
    {
        public float zoomLevel;

        public MapUIZoom(float zoomLevel)
        {
            this.zoomLevel = zoomLevel;
        }
    }
}