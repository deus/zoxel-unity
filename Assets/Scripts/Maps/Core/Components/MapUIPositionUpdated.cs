using Unity.Entities;

namespace Zoxel.Maps
{
    public struct MapUIPositionUpdated : IComponentData
    {
        public int2 position;
        public MapUIPositionUpdated(int2 position)
        {
            this.position = position;
        }
    }
}