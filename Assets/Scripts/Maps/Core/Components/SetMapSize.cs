using Unity.Entities;

namespace Zoxel.Maps
{
    //! Updating the render size of the map!
    public struct SetMapSize : IComponentData
    {
        public byte newSize;

        public SetMapSize(int newSize)
        {
            this.newSize = (byte) newSize;
        }
    }
}