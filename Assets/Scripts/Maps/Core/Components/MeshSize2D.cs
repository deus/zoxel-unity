using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Maps
{
    public struct MeshSize2D : IComponentData
    {
        public float2 size;

        public MeshSize2D(float2 size)
        {
            this.size = size;
        }
    }
}