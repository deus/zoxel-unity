using Unity.Entities;

namespace Zoxel.Maps
{
    //! A map part that moves with the player, used on MinimapUI.
    public struct MapPartMoving : IComponentData { }
}