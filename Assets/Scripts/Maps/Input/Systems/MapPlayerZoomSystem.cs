using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Input;
using Zoxel.UI;
using Zoxel.Transforms;

namespace Zoxel.Maps.Input
{
    //! When mouse is over map part, it will zoom.
    [BurstCompile, UpdateInGroup(typeof(UISystemGroup))]
    public partial class MapPlayerZoomSystem : SystemBase
	{
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<SetControllerMapping>()
                .ForEach((int entityInQueryIndex, in NavigatorSelected navigatorSelected, in Controller controller) =>
            {
                if (navigatorSelected.mouseOverEntity.Index == 0 || !HasComponent<MapPart>(navigatorSelected.mouseOverEntity))
                {
                    return;
                }
                if (controller.mouse.scroll.y < 0)
                {
                    // UnityEngine.Debug.LogError("Zooming In Map.");
                    PostUpdateCommands.AddComponent<MapZoomIn>(entityInQueryIndex, navigatorSelected.mouseOverEntity);
                }
                else if (controller.mouse.scroll.y > 0)
                {
                    // UnityEngine.Debug.LogError("Zooming Out Map.");
                    PostUpdateCommands.AddComponent<MapZoomOut>(entityInQueryIndex, navigatorSelected.mouseOverEntity);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<MapZoomIn, MapPart>()
                .ForEach((Entity e, int entityInQueryIndex, in ParentLink parentLink) =>
            {
                PostUpdateCommands.RemoveComponent<MapZoomIn>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<MapZoomIn>(entityInQueryIndex, parentLink.parent);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            Dependency = Entities
                .WithAll<MapZoomOut, MapPart>()
                .ForEach((Entity e, int entityInQueryIndex, in ParentLink parentLink) =>
            {
                PostUpdateCommands.RemoveComponent<MapZoomOut>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<MapZoomOut>(entityInQueryIndex, parentLink.parent);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}