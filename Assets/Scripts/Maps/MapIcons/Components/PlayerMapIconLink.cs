using Unity.Entities;

namespace Zoxel.Maps
{
    //! A taglink for a characters map icon.
	public struct PlayerMapIconLink : IComponentData
	{
		public Entity playerMapIcon;
		public int3 characterPosition;

		public PlayerMapIconLink(Entity playerMapIcon)
		{
			this.playerMapIcon = playerMapIcon;
			this.characterPosition = int3.zero;
		}
	}
}