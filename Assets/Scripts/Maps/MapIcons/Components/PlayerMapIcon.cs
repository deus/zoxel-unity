using Unity.Entities;

namespace Zoxel.Maps
{
    //! A tag for the players map icon.
	public struct PlayerMapIcon : IComponentData { }
}