using Unity.Entities;

namespace Zoxel.Maps
{
    //! A taglink for a characters map icon.
	public struct CharacterMapIcon : IComponentData { }
}