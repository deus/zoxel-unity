using Unity.Entities;
using Zoxel.UI;
using Zoxel.Rendering;
using Zoxel.Textures;

namespace Zoxel.Maps
{
    //! Initializes material and mesh on a MapPart entity.
    [UpdateInGroup(typeof(UISystemGroup))]
    public partial class MapArrowInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            if (UIManager.instance == null) return;
            var arrowColorA = UIManager.instance.uiDatam.arrowColor;
            var arrowColorB = UIManager.instance.uiDatam.arrowColor;
            var materials = UIManager.instance.materials;
            var mapIconMaterial = materials.mapIconMaterial;
            var worldUILayer = (byte) UnityEngine.LayerMask.NameToLayer("WorldUI");
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<InitializeEntity, MapArrow>()
                .ForEach((Entity e, in Size2D size2D) =>
            {
                var zoxMesh = new ZoxMesh();
                zoxMesh.mesh = MeshUtilities.CreateQuadMesh(size2D.size);
                zoxMesh.material = new UnityEngine.Material(mapIconMaterial);
                var arrowTexture = TextureUtilities.CreateArrowTexture(arrowColorA, arrowColorB);
                zoxMesh.material.SetTexture("_BaseMap", arrowTexture);
                zoxMesh.layer = worldUILayer;
                PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
            }).WithoutBurst().Run();
        }
    }
}