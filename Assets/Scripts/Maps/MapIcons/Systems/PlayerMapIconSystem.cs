﻿using Unity.Entities;
using Unity.Mathematics;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;

using Zoxel.Transforms;
using Zoxel.UI;

namespace Zoxel.Maps
{
	//! Rotates the character icons based on character rotations.
	/**
	*	\todo Convert to parallel.
	*/
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
	public partial class PlayerMapIconSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}
		
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
			Entities
				.WithNone<DestroyEntity, MapUIPositionUpdated>()
				.WithAll<Minimap>()
				.ForEach((ref PlayerMapIconLink playerMapIconLink, in Childrens childrens, in MapUI mapUI) =>
			{
				if (childrens.children.Length == 0 || mapUI.iconSize == 0)
				{
					return;
				}
				var playerMapIcon = playerMapIconLink.playerMapIcon;
				if (!HasComponent<CharacterLink>(playerMapIcon))
				{
					return;
				}
				// set icon position
				var playerCharacterEntity = EntityManager.GetComponentData<CharacterLink>(playerMapIcon).character;
				if (!HasComponent<Translation>(playerCharacterEntity))
				{
					return;
				}
				var positionRaw = EntityManager.GetComponentData<Translation>(playerCharacterEntity).Value;
				var position = new int3((int) math.floor(positionRaw.x), 0, (int) math.floor(positionRaw.z));
				var positionOffset = new int3(position.x % 16, 0, position.z % 16);
				position.x /= 16;
				position.z /= 16;
                var mapPosition = mapUI.mapPosition;
				mapPosition -= new int2(- mapUI.renderDistance, mapUI.renderDistance);
				position -= new int3(mapPosition.x, 0, mapPosition.y);
				position.x *= 16;
				position.z *= 16;
				position += positionOffset;
				if (playerMapIconLink.characterPosition == position)
				{
					return;
				}
				var iconSize = mapUI.GetIconSize();
				var offset = new float3();
					offset.x = (- iconSize.x / 2f + position.x * iconSize.x / 16f);
					offset.y = (- iconSize.y / 2f + position.z * iconSize.y / 16f);
				var gridSize = mapUI.GetGridSize();
				//UnityEngine.Debug.LogError("Map Position: " + mapUI.mapPosition +
				//	", with offset Position: " + position + " :: " + mapUI.mapPosition);
				playerMapIconLink.characterPosition = position;
				// update based on local position of map piece instead of child index
				for (int i = 0; i < childrens.children.Length; i++)
				{
					var child = childrens.children[i];
					if (HasComponent<UIElement>(child) && !HasComponent<CharacterMapIcon>(child))
					{
						var chunkDataLink = EntityManager.GetComponentData<ChunkDataLink>(child);
						var cellPosition = chunkDataLink.position - mapUI.mapPosition;
						cellPosition.y *= -1;
						// UnityEngine.Debug.LogError("CellPosition: " + i + ":" + cellPosition);
						var gridPosition = GridUIPositionSystem.GetGridPosition(
							cellPosition, gridSize, iconSize,
                    		float2.zero, float2.zero);
						var parentSynch = EntityManager.GetComponentData<LocalPosition>(child);
						parentSynch.position = gridPosition - offset;
						PostUpdateCommands.SetComponent(child, parentSynch);
					}
				}
			}).WithoutBurst().Run();
		}
	}
}