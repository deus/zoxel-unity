﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

using Zoxel.Transforms;

namespace Zoxel.Maps
{
	//! Rotates the character icons based on character rotations.
	/**
	*	\todo Pass all character rotations into the system at once.
	*	\todo Add more icons for all the npcs as well but mark as red.
    */
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
	public partial class CharacterMapIconSystem : SystemBase
	{
        private EntityQuery processQuery;
        private EntityQuery charactersQuery;

        protected override void OnCreate()
        {
			charactersQuery = GetEntityQuery(
                ComponentType.ReadOnly<Character>(),
                ComponentType.ReadOnly<Rotation>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
            var degreesToRadians = ((math.PI * 2) / 360f);
			// pass in positions/transforms of characters into this system in parallel
            var characterEntities = charactersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
			var rotations = GetComponentLookup<Rotation>(true);
			characterEntities.Dispose();
			Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithAll<CharacterMapIcon>()
				.ForEach((ref LocalRotation localRotation, in CharacterLink characterLink) =>
			{
				if (rotations.HasComponent(characterLink.character))
				{
					var rotation2 = rotations[characterLink.character].Value;
					var euler = new UnityEngine.Quaternion(
						rotation2.value.x, rotation2.value.y,
						rotation2.value.z, rotation2.value.w).eulerAngles;
					localRotation.rotation = quaternion.EulerXYZ(new float3(0, 0, -euler.y + 90) * degreesToRadians);
				}
			}).WithReadOnly(rotations).ScheduleParallel();
		}
	}
}