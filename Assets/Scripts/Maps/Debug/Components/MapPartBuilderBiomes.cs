using Unity.Entities;

namespace Zoxel.Maps
{
	//! Triggers a MapPart to build a biomes texture.
	public struct MapPartBuilderBiomes : IComponentData { }
}