using Unity.Entities;

namespace Zoxel.Maps
{
	//! Triggers a MapPart to build a Town's texture.
	public struct MapBuilderTowns : IComponentData { }
}