using Unity.Entities;

namespace Zoxel.Maps
{
	//! Triggers a MapPart to build a MegaChunk2D texture.
	public struct MapPartBuilderMegaChunk2Ds : IComponentData { }
}