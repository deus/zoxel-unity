﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;
using Zoxel.Textures;
using Zoxel.Worlds;

namespace Zoxel.Maps
{
	//! Builds the pixels of our map for megachunk debugging!
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
	public partial class MapPartMegaChunkPixelsSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;
        private EntityQuery megaChunksQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            chunksQuery = GetEntityQuery(
				ComponentType.ReadOnly<PlanetChunk>(),
				ComponentType.Exclude<InitializeEntity>(),
				ComponentType.Exclude<DestroyEntity>());
            megaChunksQuery = GetEntityQuery(
				ComponentType.ReadOnly<MegaChunk>(),
				ComponentType.Exclude<InitializeEntity>(),
				ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var megaChunkLinks = GetComponentLookup<MegaChunkLink>(true);
            var megaChunkEntities = megaChunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var megaChunks = GetComponentLookup<MegaChunk>(true);
			chunkEntities.Dispose();
			megaChunkEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<InitializeEntity>()
				.WithAll<MapPartBuilderMegaChunks>()
				.ForEach((Entity e, int entityInQueryIndex, ref Texture texture, in MapPart mapPart, in ChunkDataLink chunkDataLink) =>
			{
				if (!megaChunkLinks.HasComponent(chunkDataLink.chunk)) 
				{
					return;
				}
				var megaChunkLink = megaChunkLinks[chunkDataLink.chunk];
				if (!megaChunks.HasComponent(megaChunkLink.megaChunk))
				{
					return;
				}
				var megaChunk = megaChunks[megaChunkLink.megaChunk];
				PostUpdateCommands.RemoveComponent<MapPartBuilderMegaChunks>(entityInQueryIndex, e);
				if (texture.size.x != 16)
				{
					texture.Dispose();
					texture.Initialize(new int2(16, 16));
				}
				var color = new Color(
						(byte) math.clamp(64 + math.abs(megaChunk.position.x) * 16, 0, 255),
						(byte) math.clamp(64 + math.abs(megaChunk.position.y) * 16, 0, 255),
						(byte) math.clamp(64 + math.abs(megaChunk.position.z) * 16, 0, 255),
						255);
				// set color based on megachunk
				for (int i = 0; i < texture.colors.Length; i++)
				{
					texture.colors[i] = color;
				}
				var blackColor = new Color(12, 12, 12, 255);
				for (int i = 0; i < texture.size.x; i++)
				{
					for (int j = 0; j < texture.size.y; j++)
					{
						if (i == 0 || i == texture.size.x - 1 ||
							j == 0 || j == texture.size.y - 1)
						{
							var xzIndex = i + j * texture.size.y;
							texture.colors[xzIndex] = blackColor;
							// UnityEngine.Debug.LogError("Mega Chunk Pixels Blackness!");
						}

					}
				}
				PostUpdateCommands.AddComponent<TextureDirty>(entityInQueryIndex, e);
            })  .WithReadOnly(megaChunkLinks)
                .WithReadOnly(megaChunks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}