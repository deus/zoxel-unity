﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;
using Zoxel.Textures;
using Zoxel.Worlds;

namespace Zoxel.Maps
{
	//! Builds the pixels of our map for megachunk debugging!
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
	public partial class MapPartBiomesPixelsSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;
        private EntityQuery planetChunk2DQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            chunksQuery = GetEntityQuery(
				ComponentType.ReadOnly<PlanetChunk>(),
				ComponentType.Exclude<InitializeEntity>(),
				ComponentType.Exclude<DestroyEntity>());
            planetChunk2DQuery = GetEntityQuery(
				ComponentType.ReadOnly<PlanetChunk2D>(),
				ComponentType.Exclude<InitializeEntity>(),
				ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunkPlanetChunk2DLinks = GetComponentLookup<ChunkPlanetChunk2DLinks>(true);
            var planetChunk2DEntities = planetChunk2DQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var planetChunkPositions = GetComponentLookup<QuadrantPosition2D>(true);
            var biomes = GetComponentLookup<ChunkBiome>(true);
			chunkEntities.Dispose();
			planetChunk2DEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<InitializeEntity>()
				.WithAll<MapPartBuilderBiomes>()
				.ForEach((Entity e, int entityInQueryIndex, ref Texture texture, in MapPart mapPart, in ChunkDataLink chunkDataLink) =>
			{
				if (!chunkPlanetChunk2DLinks.HasComponent(chunkDataLink.chunk)) 
				{
					return;
				}
				var planetChunk2DLinks2 = chunkPlanetChunk2DLinks[chunkDataLink.chunk];
				if (planetChunk2DLinks2.chunks.Length == 0)
				{
					return;
				}
				var planetChunk2DEntity = new Entity(); // planetChunk2DLinks2.chunks[0];
				for (int i = 0; i < planetChunk2DLinks2.chunks.Length; i++)
				{
					var planetChunkEntity = planetChunk2DLinks2.chunks[i];
					var quadrantPosition2D = planetChunkPositions[planetChunkEntity];
					if (quadrantPosition2D.quadrant == PlanetSide.Up)
					{
						planetChunk2DEntity = planetChunk2DLinks2.chunks[i];
						break;
					}
				}
				if (planetChunk2DEntity.Index == 0)
				{
					return;
					// UnityEngine.Debug.LogError("Top planet chunk 2D not found!");
				}
				if (!biomes.HasComponent(planetChunk2DEntity))
				{
					return;
				}
				var biome = biomes[planetChunk2DEntity];
				PostUpdateCommands.RemoveComponent<MapPartBuilderBiomes>(entityInQueryIndex, e);
				if (texture.size.x != 16)
				{
					texture.Dispose();
					texture.Initialize(new int2(16, 16));
				}
				// set color based on megachunk
				for (int i = 0; i < texture.colors.Length; i++)
				{
					var biomeIndex = biome.biomes[i];
					var color = new Color(
							(byte) math.clamp(32 + biomeIndex * 16, 0, 255),
							(byte) math.clamp(32 + biomeIndex * 16, 0, 255),
							(byte) math.clamp(32 + biomeIndex * 16, 0, 255),
							255);
					texture.colors[i] = color;
				}
				/*var blackColor = new Color(25, 25, 25, 255);
				for (int i = 0; i < mapPart.voxelDimensions.x; i++)
				{
					for (int j = 0; j < mapPart.voxelDimensions.z; j++)
					{
						if (i == 0 || i == mapPart.voxelDimensions.x - 1 
							|| j == 0 || j = mapPart.voxelDimensions.z - 1)
						{
							var xzIndex = i + j * mapPart.voxelDimensions.z;
							texture.colors[i] = blackColor;
						}

					}
				}*/
				PostUpdateCommands.AddComponent<TextureDirty>(entityInQueryIndex, e);
            }) .WithReadOnly(chunkPlanetChunk2DLinks)
                .WithReadOnly(planetChunkPositions)
                .WithReadOnly(biomes)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}