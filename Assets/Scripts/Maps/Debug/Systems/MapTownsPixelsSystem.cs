﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;
using Zoxel.Textures;
using Zoxel.Worlds;

namespace Zoxel.Maps
{
	//! Builds the pixels of our map for megachunk debugging!
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
	public partial class MapTownsPixelsSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;
        private EntityQuery megaChunk2DsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            chunksQuery = GetEntityQuery(
				ComponentType.ReadOnly<PlanetChunk>(),
				ComponentType.Exclude<InitializeEntity>(),
				ComponentType.Exclude<DestroyEntity>());
            megaChunk2DsQuery = GetEntityQuery(
				ComponentType.ReadOnly<MegaChunk2D>(),
				ComponentType.ReadOnly<QuadrantPosition2D>(),
				ComponentType.ReadOnly<TownMap2D>(),
				ComponentType.Exclude<GenerateTownMap2D>(),
				ComponentType.Exclude<InitializeEntity>(),
				ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
            var megaChunkDivision = WorldsManager.instance.worldsSettings.megaChunkDivision;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var chunkPositions = GetComponentLookup<ChunkPosition>(true);
            var megaChunk2DLinks = GetComponentLookup<MegaChunk2DLink>(true);
            var megaChunkEntities = megaChunk2DsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var quadrantPosition2Ds = GetComponentLookup<QuadrantPosition2D>(true);
            var townMap2Ds = GetComponentLookup<TownMap2D>(true);
			chunkEntities.Dispose();
			megaChunkEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<InitializeEntity>()
				.WithAll<MapBuilderTowns>()
				.ForEach((Entity e, int entityInQueryIndex, ref Texture texture, in MapPart mapPart, in ChunkDataLink chunkDataLink) =>
			{
				var chunkEntity = chunkDataLink.chunk;
				if (!megaChunk2DLinks.HasComponent(chunkEntity)) 
				{
					return;
				}
				var megaChunk2DLink = megaChunk2DLinks[chunkEntity];
				if (!quadrantPosition2Ds.HasComponent(megaChunk2DLink.megaChunk2D))
				{
					return;
				}
				var quadrantPosition2D = quadrantPosition2Ds[megaChunk2DLink.megaChunk2D];
				if (!townMap2Ds.HasComponent(megaChunk2DLink.megaChunk2D))
				{
					return;
				}
				var townMap2D = townMap2Ds[megaChunk2DLink.megaChunk2D];
				PostUpdateCommands.RemoveComponent<MapBuilderTowns>(entityInQueryIndex, e);
				PostUpdateCommands.AddComponent<TextureDirty>(entityInQueryIndex, e);
				if (texture.size.x != 16)
				{
					texture.Dispose();
					texture.Initialize(new int2(16, 16));
				}
				var color = new Color(
						(byte) math.clamp(32 + math.abs(quadrantPosition2D.position.x) * 16, 0, 255),
						(byte) math.clamp(32 + math.abs(quadrantPosition2D.position.y) * 16, 0, 255),
						(byte) math.clamp(32 + math.abs(quadrantPosition2D.quadrant) * 16, 0, 255),
						255);
				// set color based on megachunk
				for (int i = 0; i < texture.colors.Length; i++)
				{
					texture.colors[i] = color;
				}
				// outline
				var blackColor = new Color(12, 12, 12, 255);
				for (int i = 0; i < texture.size.x; i++)
				{
					for (int j = 0; j < texture.size.y; j++)
					{
						if (i == 0 || i == texture.size.x - 1 || j == 0 || j == texture.size.y - 1)
						{
							var xzIndex = i + j * texture.size.y;
							texture.colors[xzIndex] = blackColor;
						}

					}
				}
				// Town colors
				var chunkPosition = chunkPositions[chunkEntity];
				var voxelDimensions = new int3(16, 16, 16);
				var chunkVoxelPosition2 = chunkPosition.GetVoxelPosition(voxelDimensions);
				var chunkVoxelPosition = new int2(chunkVoxelPosition2.x, chunkVoxelPosition2.z);
				var townPositionOffset = quadrantPosition2D.GetVoxelPosition2D(voxelDimensions, megaChunkDivision) + townMap2D.position;
				// UnityEngine.Debug.LogError("townPositionOffset: " + townPositionOffset);
				var darkGrayColor = new Color(22, 22, 22, 255);
				var roadColor = new Color(88, 66, 66, 255);
				var houseColor = new Color(77, 133, 133, 255);
				var doorColor = new Color(111, 199, 199, 255);
				var townWallColor = new Color(55, 22, 22, 255);
				var globalPosition = new int2();
				var townPosition = new int2();
				var texturePosition = new int2();
                var roadTile = (byte) 1;
                var buildingTile = (byte) 2;
                var doorTile = (byte) 3;
                var townWallTile = (byte) 4;
				for (globalPosition.x = townPositionOffset.x; globalPosition.x < townPositionOffset.x + townMap2D.size.x; globalPosition.x++)
				{
					for (globalPosition.y = townPositionOffset.y; globalPosition.y < townPositionOffset.y + townMap2D.size.y; globalPosition.y++)
					{
						texturePosition = globalPosition - chunkVoxelPosition;
						var isInTexture = texturePosition.x >= 0 && texturePosition.x < texture.size.x
							&& texturePosition.y >= 0 && texturePosition.y < texture.size.y;
						if (!isInTexture)
						{
							continue;
						}
						var textureArrayIndex = texturePosition.x + texturePosition.y * texture.size.x;
						if (textureArrayIndex < 0 || textureArrayIndex >= texture.colors.Length)
						{
							// UnityEngine.Debug.LogError("textureArrayIndex out of Index: " + textureArrayIndex);
							return;
						}
						townPosition = globalPosition - townPositionOffset;
						var townMapArrayIndex = townPosition.x + townPosition.y * townMap2D.size.x;
						if (townMapArrayIndex < 0 || townMapArrayIndex >= townMap2D.data.Length)
						{
							// UnityEngine.Debug.LogError("townMapArrayIndex out of Index: " + townMapArrayIndex);
							return;
						}
						// Set map ui based on townMap2D
						var townByte = townMap2D.data[townMapArrayIndex];
						if (townByte == 0)
						{
							texture.colors[textureArrayIndex] = darkGrayColor;
						}
						else if (townByte == roadTile)
						{
							texture.colors[textureArrayIndex] = roadColor;
						}
						else if (townByte == buildingTile)
						{
							texture.colors[textureArrayIndex] = houseColor;
						}
						else if (townByte == doorTile)
						{
							texture.colors[textureArrayIndex] = doorColor;
						}
						else if (townByte == townWallTile)
						{
							texture.colors[textureArrayIndex] = townWallColor;
						}
					}
				}
            })  .WithReadOnly(chunkPositions)
                .WithReadOnly(megaChunk2DLinks)
                .WithReadOnly(quadrantPosition2Ds)
                .WithReadOnly(townMap2Ds)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}