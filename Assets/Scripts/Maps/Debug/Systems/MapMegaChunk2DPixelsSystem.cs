﻿using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;
using Zoxel.Textures;
using Zoxel.Worlds;

namespace Zoxel.Maps
{
	//! Builds the pixels of our map for megachunk debugging!
    [BurstCompile, UpdateInGroup(typeof(MapSystemGroup))]
	public partial class MapMegaChunk2DPixelsSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery chunksQuery;
        private EntityQuery megaChunk2DsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            chunksQuery = GetEntityQuery(
				ComponentType.ReadOnly<PlanetChunk>(),
				ComponentType.Exclude<InitializeEntity>(),
				ComponentType.Exclude<DestroyEntity>());
            megaChunk2DsQuery = GetEntityQuery(
				ComponentType.ReadOnly<MegaChunk2D>(),
				ComponentType.Exclude<InitializeEntity>(),
				ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
		}

		[BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var chunkEntities = chunksQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var megaChunk2DLinks = GetComponentLookup<MegaChunk2DLink>(true);
            var megaChunkEntities = megaChunk2DsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var quadrantPosition2Ds = GetComponentLookup<QuadrantPosition2D>(true);
			chunkEntities.Dispose();
			megaChunkEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<InitializeEntity>()
				.WithAll<MapPartBuilderMegaChunk2Ds>()
				.ForEach((Entity e, int entityInQueryIndex, ref Texture texture, in MapPart mapPart, in ChunkDataLink chunkDataLink) =>
			{
				if (!megaChunk2DLinks.HasComponent(chunkDataLink.chunk)) 
				{
					return;
				}
				var megaChunk2DLink = megaChunk2DLinks[chunkDataLink.chunk];
				if (!quadrantPosition2Ds.HasComponent(megaChunk2DLink.megaChunk2D))
				{
					return;
				}
				var quadrantPosition2D = quadrantPosition2Ds[megaChunk2DLink.megaChunk2D];
				PostUpdateCommands.RemoveComponent<MapPartBuilderMegaChunk2Ds>(entityInQueryIndex, e);
				if (texture.size.x != 16)
				{
					texture.Dispose();
					texture.Initialize(new int2(16, 16));
				}
				var color = new Color(
						(byte) math.clamp(32 + math.abs(quadrantPosition2D.position.x) * 16, 0, 255),
						(byte) math.clamp(32 + math.abs(quadrantPosition2D.position.y) * 16, 0, 255),
						(byte) math.clamp(32 + math.abs(quadrantPosition2D.quadrant) * 16, 0, 255),
						255);
				// set color based on megachunk
				for (int i = 0; i < texture.colors.Length; i++)
				{
					texture.colors[i] = color;
				}
				var blackColor = new Color(12, 12, 12, 255);
				for (int i = 0; i < texture.size.x; i++)
				{
					for (int j = 0; j < texture.size.y; j++)
					{
						if (i == 0 || i == texture.size.x - 1 ||
							j == 0 || j == texture.size.y - 1)
						{
							var xzIndex = i + j * texture.size.y;
							texture.colors[xzIndex] = blackColor;
						}

					}
				}
				PostUpdateCommands.AddComponent<TextureDirty>(entityInQueryIndex, e);
            })  .WithReadOnly(megaChunk2DLinks)
                .WithReadOnly(quadrantPosition2Ds)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}