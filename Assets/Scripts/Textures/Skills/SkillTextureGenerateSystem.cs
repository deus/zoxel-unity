using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Skills.Authoring;
using Zoxel.Skills;

namespace Zoxel.Textures.Skills
{
    //! Generates textures for userSkillLinks
    /**
    *   - Texture Generation System -
    */
    [BurstCompile, UpdateInGroup(typeof(TextureSystemGroup))]
    public partial class SkillTextureGenerateSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<SkillsSettings>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var skillsSettings = GetSingleton<SkillsSettings>();
            var textureSize = skillsSettings.skillTextureSize;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<TextureDirty>()
                .WithAll<GenerateSkillTexture>()
                .ForEach((Entity e, int entityInQueryIndex, ref Texture texture, in GenerateSkillTexture generateSkillTexture) =>
            {
                PostUpdateCommands.RemoveComponent<GenerateSkillTexture>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<TextureDirty>(entityInQueryIndex, e);
                var random = new Random();
                if (generateSkillTexture.seed == 0)
                {
                    random.InitState((uint) 666);
                }
                else
                {
                    random.InitState((uint) generateSkillTexture.seed);
                }
                // generate texture
                texture.Initialize(textureSize);
                var red = (byte)(0 + (int)(255 * random.NextFloat(1)));
                var green = (byte)(0 + (int)(255 * random.NextFloat(1)));
                var blue = (byte)(0 + (int)(255 * random.NextFloat(1)));
                var fillColor = new Color(red, green, blue);
                var midFillColor = new Color(blue, red, green);
                var clearColor = new Color(0, 0, 0, 0);
                #if DEBUG_SKILL_TEXTURES
                    for (int x = 0; x < texture.size.x; x++)
                    {
                        for (int y = 0; y < texture.size.y; y++)
                        {
                            int pixelIndex = (int)y * texture.size.x + (int)x;
                            if (y >= texture.size.y / 2f)
                            {
                                texture.colors[pixelIndex] = midFillColor;
                            }
                            else
                            {
                                texture.colors[pixelIndex] = fillColor;
                            }
                        }
                    }
                    return;
                #endif
                var shape = generateSkillTexture.shape; //ShapeType.Circle;
                var quarterSize = textureSize.x / 6;   // 3
                var halfSize = textureSize.x / 3;      // 6
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        if (shape == ShapeType.Circle)
                        {
                            var distanceToMid = math.distance(new float2(x,y), new float2(texture.size.x, texture.size.y) / 2f);
                            if (distanceToMid <= quarterSize)
                            {
                                texture.colors[pixelIndex] = midFillColor;
                            }
                            else if (distanceToMid <= halfSize)
                            {
                                texture.colors[pixelIndex] = fillColor;
                            }
                            else
                            {
                                texture.colors[pixelIndex] = clearColor;
                            }
                        }
                        else if (shape == ShapeType.Square)
                        {
                            var distanceToMidX = math.abs((texture.size.x / 2) - x);
                            var distanceToMidY = math.abs((texture.size.y / 2) - y);
                            if (distanceToMidX <= quarterSize && distanceToMidY <= quarterSize)
                            {
                                texture.colors[pixelIndex] = midFillColor;
                            }
                            else if (distanceToMidX <= halfSize && distanceToMidY <= halfSize)
                            {
                                texture.colors[pixelIndex] = fillColor;
                            }
                            else
                            {
                                texture.colors[pixelIndex] = clearColor;
                            }
                        }
                        else if (shape == ShapeType.Triangle)
                        {
                            var distanceToMidX = math.abs((texture.size.x / 2) - x);
                            var distanceToMidY = math.abs((texture.size.y / 2) - y);
                            var isInTriangleShape = distanceToMidX < ((texture.size.y - y) / 2);   //
                            if (distanceToMidY <= halfSize && isInTriangleShape)
                            {
                                var isInTriangleShape2 = distanceToMidX < ((texture.size.y - y) / 4);   //
                                if (distanceToMidY <= quarterSize && isInTriangleShape2)
                                {
                                    texture.colors[pixelIndex] = midFillColor;
                                }
                                else
                                {
                                    texture.colors[pixelIndex] = fillColor;
                                }
                            }
                            else
                            {
                                texture.colors[pixelIndex] = clearColor;
                            }
                        }
                        else if (shape == ShapeType.Diamond)
                        {
                            var distanceToMidX = math.abs((texture.size.x / 2) - x);
                            var distanceToMidY = math.abs((texture.size.y / 2) - y);
                            var distanceToMid = (distanceToMidX + distanceToMidY) / 2f;
                            if (distanceToMid <= texture.size.x / 4.2f)
                            {
                                if (distanceToMid <= texture.size.x / 6.2f)
                                {
                                    texture.colors[pixelIndex] = midFillColor;
                                }
                                else
                                {
                                    texture.colors[pixelIndex] = fillColor;
                                }
                            }
                            else
                            {
                                texture.colors[pixelIndex] = clearColor;
                            }
                        }
                        else
                        {
                            texture.colors[pixelIndex] = clearColor;
                        }
                    }
                }

                // add outline - need an outline system for this
                var size = new int3(texture.size.x, texture.size.y, 0);
                var outlineColor = new Color(0, 0, 0); // generateFontTexture.outlineColor; //new Color(0, 0, 0, 255);
                for (int i = 0; i < texture.size.x; i++)
                {
                    for (int j = 0; j < texture.size.y; j++)
                    {
                        var position = new int3(i, j, 0);
                        var index = VoxelUtilities.GetTextureArrayIndexXY(position, size);
                        var thisColour = texture.colors[index];
                        if (i == 0 || j == 0 || i == texture.size.x - 1 || j == texture.size.y - 1)
                        {
                            if (thisColour.alpha != 0 && thisColour.red != outlineColor.red)
                            {
                                texture.colors[index] = outlineColor;
                            }
                            else
                            {
                                // check top if bottom row
                                if (i == 0)
                                {
                                    var positionRight = new int3(i + 1, j, 0);
                                    var indexRight = VoxelUtilities.GetTextureArrayIndexXY(positionRight, size);
                                    var colourRight = texture.colors[indexRight];
                                    if (colourRight.alpha != 0 && colourRight.red != outlineColor.red)
                                    {
                                        texture.colors[index] = outlineColor;
                                    }
                                }
                                else // i isi size.x - 1
                                {
                                    var positionLeft = new int3(i - 1, j, 0);
                                    var indexLeft  = VoxelUtilities.GetTextureArrayIndexXY(positionLeft , size);
                                    var colourLeft  = texture.colors[indexLeft ];
                                    if (colourLeft.alpha != 0 && colourLeft.red != outlineColor.red)
                                    {
                                        texture.colors[index] = outlineColor;
                                    }
                                }
                                if (j == 0)
                                {
                                    var positionDown = new int3(i, j + 1, 0);
                                    var indexDown = VoxelUtilities.GetTextureArrayIndexXY(positionDown, size);
                                    var colourDown = texture.colors[indexDown];
                                    if (colourDown.alpha != 0 && colourDown.red != outlineColor.red)
                                    {
                                        texture.colors[index] = outlineColor;
                                    }
                                }
                                else // j is size.y - 1
                                {
                                    var positionUp = new int3(i, j - 1, 0);
                                    var indexUp = VoxelUtilities.GetTextureArrayIndexXY(positionUp, size);
                                    var colourUp = texture.colors[indexUp];
                                    if (colourUp.alpha != 0 && colourUp.red != outlineColor.red)
                                    {
                                        texture.colors[index] = outlineColor;
                                    }
                                }
                            }
                        }
                        else if (thisColour.alpha == 0)
                        {
                            var positionUp = new int3(i, j - 1, 0);
                            var indexUp = VoxelUtilities.GetTextureArrayIndexXY(positionUp, size);
                            var colourUp = texture.colors[indexUp];
                            if (colourUp.alpha != 0 && colourUp.red != outlineColor.red)
                            {
                                texture.colors[index] = outlineColor;
                            }
                            var positionDown = new int3(i, j + 1, 0);
                            var indexDown = VoxelUtilities.GetTextureArrayIndexXY(positionDown, size);
                            var colourDown = texture.colors[indexDown];
                            if (colourDown.alpha != 0 && colourDown.red != outlineColor.red)
                            {
                                texture.colors[index] = outlineColor;
                            }
                            var positionLeft = new int3(i - 1, j, 0);
                            var indexLeft  = VoxelUtilities.GetTextureArrayIndexXY(positionLeft , size);
                            var colourLeft = texture.colors[indexLeft];
                            if (colourLeft.alpha != 0 && colourLeft .red != outlineColor.red)
                            {
                                texture.colors[index] = outlineColor;
                            }
                            var positionRight = new int3(i + 1, j, 0);
                            var indexRight = VoxelUtilities.GetTextureArrayIndexXY(positionRight, size);
                            var colourRight = texture.colors[indexRight];
                            if (colourRight.alpha != 0 && colourRight.red != outlineColor.red)
                            {
                                texture.colors[index] = outlineColor;
                            }
                        }
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}