using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;
using Zoxel.Voxels.Authoring;
using Unity.Burst.Intrinsics;

namespace Zoxel.Textures.Voxels
{
    //! Generates voxel textures.
    /**
    *   - Texture Generation System -
    *   \todo Add a process stack to the texture to generate it.
    *   \todo Add Gravel textures for roads.
    *   \todo Make grass more grassy, maybe add rooty like things. Remove crosses from it.
    *   \todo Add wooden planks by having x tiled rects
    */
    [BurstCompile, UpdateInGroup(typeof(TextureSystemGroup))]
    public partial class VoxelTextureGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        	processQuery = GetEntityQuery(
                ComponentType.ReadWrite<GenerateTextureVoxel>(),
                ComponentType.ReadWrite<Texture>(),
                ComponentType.ReadOnly<VoxelTexture>());
            RequireForUpdate<VoxelTextureSettings>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var voxelTextureSettings = GetSingleton<VoxelTextureSettings>();
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<GenerateTextureVoxel>(processQuery);
            var job = new VoxelTextureJob
            {
                outlineTexturesBlack = voxelTextureSettings.isTextureDarkOutlines,
                textureHandle = GetComponentTypeHandle<Texture>(false),
                seedHandle = GetComponentTypeHandle<Seed>(true),
                voxelTextureHandle = GetComponentTypeHandle<VoxelTexture>(true)
            };
            Dependency = job.ScheduleParallel(processQuery, Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        [BurstCompile]
        struct VoxelTextureJob : IJobChunk
        {
            [ReadOnly] public bool outlineTexturesBlack;
            public ComponentTypeHandle<Texture> textureHandle;
            [ReadOnly] public ComponentTypeHandle<Seed> seedHandle;
            [ReadOnly] public ComponentTypeHandle<VoxelTexture> voxelTextureHandle;

            public void Execute(in ArchetypeChunk chunk, int unfilteredChunkIndex, bool useEnabledMask, in v128 chunkEnabledMask)
            {
                // var entities = chunk.GetNativeArray(entityHandle);
                var textures = chunk.GetNativeArray(textureHandle);
                var seeds = chunk.GetNativeArray(seedHandle);
                var voxelTextures = chunk.GetNativeArray(voxelTextureHandle);
                for (var i = 0; i < chunk.Count; i++)
                {
                    var texture = textures[i];
                    var seed = seeds[i];
                    var voxelTexture = voxelTextures[i];
                    // entities[i], i + firstEntityIndex, 
                    Process(ref texture, in seed, in voxelTexture);
                    textures[i] = texture;
                    // generateVoxelTextures[i] = generateTextureVoxel;
                }
            }

            // Entity e, int jobIndex, 
            public void Process(ref Texture texture, in Seed seed, in VoxelTexture voxelTexture)
            {
                if (seed.seed == 0 || texture.size.x == 0 || texture.size.y == 0)
                {
                    // UnityEngine.Debug.LogError("Texture does not havve size: " + e.Index);
                    return;
                }
                var randomData = new RandomData((uint) seed.seed);
                // randomData.InitializeRandom(voxelTexture.textureSeed);
                var generationType = (VoxelTextureType) voxelTexture.generationType;
                if (generationType == VoxelTextureType.Soil || generationType == VoxelTextureType.SoilGrass)
                {
                    Voronoi(ref texture, in voxelTexture, ref randomData, 0.6f, 0.92f);
                    Noise(ref texture, in voxelTexture, ref randomData);
                }
                else if (generationType == VoxelTextureType.Grass)
                {
                    GenerateNoise(ref texture,  ref randomData, in voxelTexture);
                    SpreadGrass(ref texture,  ref randomData, in voxelTexture);
                }
                else if (generationType == VoxelTextureType.Leaf)
                {
                    GenerateLeafNoise(ref texture,  ref randomData, in voxelTexture, voxelTexture.baseColor, voxelTexture.baseVariance);
                    //GenerateNoise(ref texture,  ref randomData, in voxelTexture);
                    //SpreadGrass(ref texture,  ref randomData, in voxelTexture, 60);
                }
                else if (generationType == VoxelTextureType.Sand)
                {
                    GenerateNoise(ref texture,  ref randomData, in voxelTexture);
                }
                else if (generationType == VoxelTextureType.Portal)
                {
                    GenerateFlat(ref texture,  ref randomData, in voxelTexture);
                    GenerateNoise(ref texture,  ref randomData, in voxelTexture);
                }
                else if (generationType == VoxelTextureType.Tiles)
                {
                    GenerateBricks(ref texture,  ref randomData, in voxelTexture, new int3(35, 35, 35), false);
                }
                else if (generationType == VoxelTextureType.Bricks)
                {
                    GenerateBricks(ref texture,  ref randomData, in voxelTexture, new int3(35, 35, 35), true);
                }
                else if (generationType == VoxelTextureType.Stone)
                {
                    // GenerateStone(ref texture, ref randomData, in voxelTexture, new int3(35, 35, 35));
                    Voronoi(ref texture, in voxelTexture, ref randomData, 1f, 0.76f);
                    Light(ref texture, in voxelTexture);
                    Noise(ref texture, in voxelTexture, ref randomData);
                }
                else if (generationType == VoxelTextureType.Wood)
                {
                    GenerateWood(ref texture, ref randomData, in voxelTexture, new int3(40, 40, 40));
                }
                else if (generationType == VoxelTextureType.WoodTop)
                {
                    GenerateWoodTop(ref texture, ref randomData, in voxelTexture, new int3(40, 40, 40));
                }

                else if (generationType == VoxelTextureType.Bedrock)
                {
                    Voronoi(ref texture, in voxelTexture, ref randomData, 5);
                    Light(ref texture, in voxelTexture, 0.5f);
                    Noise(ref texture, in voxelTexture, ref randomData);
                }
                else
                {
                    GenerateFlat(ref texture,  ref randomData, in voxelTexture);
                }
                if (generationType == VoxelTextureType.SoilGrass)
                {
                    var secondTexture = texture.Clone(); // new Texture();
                    GenerateNoise(ref secondTexture, ref randomData, in voxelTexture, voxelTexture.secondaryColor, voxelTexture.baseVariance);
                    SpreadGrass(ref secondTexture, ref randomData, in voxelTexture);
                    GenerateMixedTexture(ref texture, ref randomData, in voxelTexture, in secondTexture, 0.76f);
                    secondTexture.Dispose();
                }
                if (outlineTexturesBlack && generationType != VoxelTextureType.Leaf)
                {
                    ApplyOutline(ref texture);
                }
            }

            public void ApplyOutline(ref Texture texture)
            {
                int2 position;
                int pixelIndex = 0;
                for (position.x = 0; position.x < texture.size.x; position.x++)
                {
                    for (position.y = 0; position.y < texture.size.y; position.y++)
                    {
                        // pixelIndex = position.y * texture.size.x + position.x;
                        if (position.x == 0 || position.x == texture.size.x - 1 ||
                            position.y == 0 || position.y == texture.size.y - 1)
                        {
                            texture.colors[pixelIndex] = new Color(0, 0, 0, 255);
                        }
                        pixelIndex++;
                    }
                }
            }
            
            public void GenerateFlat(ref Texture texture, ref RandomData randomData, in VoxelTexture voxelTexture)
            {
                //var baseValue = 70;
                //var baseValueVariance = 50;
                var baseColor = voxelTexture.baseColor;
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        var red = (byte)(baseColor.red);
                        var green = (byte)(baseColor.green);
                        var blue = (byte)(baseColor.blue);
                        texture.colors[pixelIndex] = new Color(red, green, blue);
                    }
                }
            }

            public void GenerateLeafNoise(ref Texture texture, ref RandomData randomData, in VoxelTexture voxelTexture, Color baseColor, int3 noiseVariance)
            {
                var emptyChance = 60;
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        if (randomData.random.NextInt(0, 100) >= emptyChance)
                        {
                            texture.colors[pixelIndex] = new Color(0, 0, 0, 0);
                            continue;
                        }
                        var red = ((int)baseColor.red + (int)((float)noiseVariance.x * randomData.random.NextFloat(1)));
                        var green = ((int)baseColor.green + (int)((float)noiseVariance.y * randomData.random2.NextFloat(1)));
                        var blue = ((int)baseColor.blue + (int)((float)noiseVariance.z * randomData.random3.NextFloat(1)));
                        red = math.clamp(red, 0, 255);
                        green = math.clamp(green, 0, 255);
                        blue = math.clamp(blue, 0, 255);
                        texture.colors[pixelIndex] = new Color((byte)red, (byte)green, (byte)blue);
                    }
                }
            }


            public void GenerateNoise(ref Texture texture, ref RandomData randomData, in VoxelTexture voxelTexture)
            {
                GenerateNoise(ref texture, ref randomData, in voxelTexture, voxelTexture.baseColor, voxelTexture.baseVariance);
            }

            public void GenerateNoise(ref Texture texture, ref RandomData randomData, in VoxelTexture voxelTexture, Color baseColor, int3 noiseVariance)
            {
                //var noiseVariance2 = new float3(); //  noiseVariance.ToFloat3();
                var noiseVariance2 = noiseVariance.ToFloat3();
                int2 position;
                int pixelIndex;
                byte red;
                byte green;
                byte blue;
                for (position.x = 0; position.x < texture.size.x; position.x++)
                {
                    for (position.y = 0; position.y < texture.size.y; position.y++)
                    {
                        pixelIndex = position.y * texture.size.x + position.x;
                        red = (byte) (math.clamp(
                            baseColor.red + noiseVariance2.x * randomData.random.NextFloat(1), 0, 255));
                        green = (byte) (math.clamp(
                            baseColor.green + noiseVariance2.y * randomData.random2.NextFloat(1), 0, 255));
                        blue = (byte) (math.clamp(
                            baseColor.blue + noiseVariance2.z * randomData.random3.NextFloat(1), 0, 255));
                        //red = (byte) math.clamp(red, 0, 255);
                        //green = (byte) math.clamp(green, 0, 255);
                        //blue = (byte) math.clamp(blue, 0, 255);
                        texture.colors[pixelIndex] = new Color(red, green, blue);
                    }
                }
            }

            public void GenerateMixedTexture(ref Texture texture, ref RandomData randomData, in VoxelTexture voxelTexture,
                in Texture texture2, float mixAmount)
            {
                var noiseVariance = voxelTexture.baseVariance;
                var baseColor = voxelTexture.baseColor;
                var secondaryColor = voxelTexture.secondaryColor;
                for (int x = 0; x < texture.size.x; x++)
                {
                    var heightDifference = -2 + randomData.random.NextInt(5);
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        if (y <= (int) (mixAmount * texture.size.y) + heightDifference)
                        {
                            // dirt
                            //red = 0; // (baseColor.red + (int)(noiseVariance.x * randomData.random.NextFloat(1)));
                            //green = 0; // (baseColor.green + (int)(noiseVariance.y * randomData.random2.NextFloat(1)));
                            //blue = 0; // (baseColor.blue + (int)(noiseVariance.z * randomData.random3.NextFloat(1)));
                        }
                        else
                        {
                            int pixelIndex = (int)y * texture.size.x + (int)x;
                            texture.colors[pixelIndex] = texture2.colors[pixelIndex];
                        }
                    }
                }
            } 
            
            public void GenerateBricks(ref Texture texture, ref RandomData randomData, in VoxelTexture voxelTexture, int3 noiseVariance, bool isBricks)
            {
                // Wooden Planks are just rectangles accross the X Axis
                var darkenEdgesMultiplier = 0.6f;
                var linesThickness = 2;
                var baseColor = voxelTexture.baseColor.Saturate(0.9f);
                var redBase = voxelTexture.baseColor.red;
                var greenBase = voxelTexture.baseColor.green;
                var blueBase = voxelTexture.baseColor.blue;
                //redBase = (byte) (((int)redBase + (int)greenBase) / 2); 
                //blueBase = (byte) (((int)blueBase + (int)greenBase) / 2);
                var tileSize = (int)math.ceil(texture.size.x / 4);
                linesThickness = (int)math.ceil((float)tileSize / 10f);
                if (tileSize == 0)
                {
                    tileSize = 1;
                }
                if (linesThickness == 0)
                {
                    linesThickness = 1;
                }
                //UnityEngine.Debug.LogError("linesThickness: " + linesThickness);
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int brickLayer = y / tileSize;
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        var red = (redBase + (int)(noiseVariance.x * randomData.random.NextFloat(1)));
                        var green = (greenBase + (int)(noiseVariance.y * randomData.random2.NextFloat(1)));
                        var blue = (blueBase + (int)(noiseVariance.z * randomData.random3.NextFloat(1)));
                        red = math.clamp(red, 0, 255);
                        green = math.clamp(green, 0, 255);
                        blue = math.clamp(blue, 0, 255);
                        texture.colors[pixelIndex] = new Color((byte)red, (byte)green, (byte)blue);
                    }
                }
                // now add in grooves
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int brickLayer = y / tileSize;
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        var secondaryColor = texture.colors[pixelIndex];
                        var color = new int3(secondaryColor.red, secondaryColor.blue, secondaryColor.green);
                        // outer edge
                        var isDarken = false;
                        if (x < linesThickness || x > texture.size.x - 1 - linesThickness
                            || y < linesThickness || y > texture.size.y - 1 - linesThickness)
                        {
                            isDarken = true;
                        }
                        else
                        {
                            // inside
                            //  || (y + k) == texture.size.y - 1
                            for (int k = -linesThickness + 1; k < linesThickness; k++)
                            {
                                if ((y + k) % tileSize == 0)    // horizontal lines
                                {
                                    isDarken = true;
                                    break;
                                }
                                else if (!isBricks && ((x + k) % tileSize == 0))
                                {
                                    isDarken = true;
                                    break;
                                }
                                else if (isBricks && ((brickLayer % 2 == 0 && ((x + k) % tileSize == 0)) || 
                                            (brickLayer % 2 != 0 && (((x + k) + (tileSize / 2)) % tileSize == 0))))
                                {
                                    isDarken = true;
                                    break;
                                }
                            }
                        }
                        if (isDarken)
                        {
                            color.x = ((int)(color.x * darkenEdgesMultiplier));
                            color.y = ((int)(color.y * darkenEdgesMultiplier));
                            color.z = ((int)(color.z * darkenEdgesMultiplier));
                            secondaryColor.red = (byte) color.x;
                            secondaryColor.green = (byte) color.y;
                            secondaryColor.blue = (byte) color.z;
                            texture.colors[pixelIndex] = secondaryColor;
                        }
                    }
                }
            }
            
            public void GenerateWood(ref Texture texture, ref RandomData randomData, in VoxelTexture voxelTexture, int3 noiseVariance)
            {
                const int woodlineChecks = 128; // 2048
                const int woodlineColorChecks = 32;
                var darkenMultiplier = 0.74f;
                var darkenChance = 66;
                var brightenMultiplier = 1.15f;
                var brightenChance = 46;
                var redBase = voxelTexture.baseColor.red;
                var greenBase = voxelTexture.baseColor.green;
                var blueBase = voxelTexture.baseColor.blue;
                var woodPadding = 1;
                var woodLineColorDifference = 30;
                var minimumWoodLineLength = (int) (0.4f * texture.size.y);  // 1
                var maximumWoodLineLength = (int) (0.7f * texture.size.y);  // 9
                var lineThickness = (int)math.ceil((float)texture.size.x / 30f);
                for (int x = 0; x < texture.size.x; x++)
                {
                    // first push colours as base
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        var red = (redBase + (int)(noiseVariance.x * randomData.random.NextFloat(1)));
                        var green = (greenBase + (int)(noiseVariance.y * randomData.random2.NextFloat(1)));
                        var blue = (blueBase + (int)(noiseVariance.z * randomData.random3.NextFloat(1)));
                        red = math.clamp(red, 0, 255);
                        green = math.clamp(green, 0, 255);
                        blue = math.clamp(blue, 0, 255);
                        texture.colors[pixelIndex] = new Color((byte)red, (byte)green, (byte)blue);
                    }
                }

                for (int x = 0; x < texture.size.x; x += lineThickness) //  + 1)
                {
                    // next dark colours with random thingo
                    // random start
                    var lines = new NativeList<int2>();
                    for (int a = 0; a < woodlineChecks; a++)
                    {
                        // new line?
                        var woodLineStart = (int)(0 + (texture.size.y - 1) * randomData.random.NextFloat(1));
                        var woodLineLength = (int)(minimumWoodLineLength + randomData.random.NextFloat(maximumWoodLineLength - minimumWoodLineLength));
                        // if not clashing with others, add!
                        var isTooClose = false;
                        for (var b = 0; b < lines.Length; b++)
                        {
                            var woodLineStartB = lines[b].x;
                            var woodLineLengthB = lines[b].x;
                            // RHSB < LHSA   || RHSA > LHSB
                            if (!(woodLineStart > woodLineStartB + woodLineLengthB + woodPadding || woodLineStart + woodLineLength + woodPadding < woodLineStartB))
                            {
                                isTooClose = true;
                                break;
                            }
                        }
                        if (!isTooClose)
                        {
                            lines.Add(new int2(woodLineStart, woodLineLength));
                            var chance = randomData.random.NextInt(100);
                            var isDarken = chance >= darkenChance;
                            var isBrighten = !isDarken && chance >= brightenChance;
                            var addition = new int3();
                            for (int c = 0; c < woodlineColorChecks; c++)
                            {
                                // find value difference of >= 32
                                var redAddition = -woodLineColorDifference + randomData.random.NextInt(woodLineColorDifference * 2);
                                var greenAddition = -woodLineColorDifference + randomData.random.NextInt(woodLineColorDifference * 2);
                                var blueAddition = -woodLineColorDifference + randomData.random.NextInt(woodLineColorDifference * 2);
                                var valueAddition = math.abs(redAddition) + math.abs(greenAddition) + math.abs(blueAddition);
                                if (valueAddition >= 32)
                                {
                                    addition.x = redAddition;
                                    addition.y = greenAddition;
                                    addition.z = blueAddition;
                                    break;
                                }
                            }
                            for (int y = woodLineStart; y < math.min(texture.size.y, woodLineStart + woodLineLength); y++)
                            {
                                for (int x2 = 0; x2 < lineThickness; x2++)
                                {
                                    if (x + x2 >= texture.size.x)
                                    {
                                        continue;
                                    }
                                    int pixelIndex = (int)y * texture.size.x + (int)(x + x2);
                                    var red = (int) texture.colors[pixelIndex].red;
                                    var green = (int) texture.colors[pixelIndex].green;
                                    var blue = (int) texture.colors[pixelIndex].blue;
                                    red += addition.x;
                                    green += addition.y;
                                    blue += addition.z;
                                    if (isDarken)
                                    {
                                        red = ((int)(red * darkenMultiplier));
                                        green = ((int)(green * darkenMultiplier));
                                        blue = ((int)(blue * darkenMultiplier));
                                    }
                                    else if (isBrighten)
                                    {
                                        red = ((int)(red * brightenMultiplier));
                                        green = ((int)(green * brightenMultiplier));
                                        blue = ((int)(blue * brightenMultiplier));
                                    }
                                    red = math.clamp(red, 0, 255);
                                    green = math.clamp(green, 0, 255);
                                    blue = math.clamp(blue, 0, 255);
                                    texture.colors[pixelIndex] = new Color((byte)red, (byte)green, (byte)blue);
                                }
                            }
                        }
                    }
                }
            }

            public void GenerateWoodTop(ref Texture texture, ref RandomData randomData, in VoxelTexture voxelTexture, int3 noiseVariance)
            {
                // const int woodlineChecks = 128; // 2048
                const int woodlineColorChecks = 32;
                var darkenMultiplier = 0.74f;
                var darkenChance = 66;
                var brightenMultiplier = 1.15f;
                var brightenChance = 46;
                var redBase = voxelTexture.baseColor.red;
                var greenBase = voxelTexture.baseColor.green;
                var blueBase = voxelTexture.baseColor.blue;
                // var woodPadding = 1;
                var woodLineColorDifference = 30;
                var minimumWoodLineLength = (int) (0.4f * texture.size.y);  // 1
                var maximumWoodLineLength = (int) (0.7f * texture.size.y);  // 9
                var lineThickness = (int)math.ceil((float)texture.size.x / 30f);
                for (int x = 0; x < texture.size.x; x++)
                {
                    // first push colours as base
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        var red = (redBase + (int)(noiseVariance.x * randomData.random.NextFloat(1)));
                        var green = (greenBase + (int)(noiseVariance.y * randomData.random2.NextFloat(1)));
                        var blue = (blueBase + (int)(noiseVariance.z * randomData.random3.NextFloat(1)));
                        red = math.clamp(red, 0, 255);
                        green = math.clamp(green, 0, 255);
                        blue = math.clamp(blue, 0, 255);
                        texture.colors[pixelIndex] = new Color((byte)red, (byte)green, (byte)blue);
                    }
                }
                // generate rims from center
                var isBrighten = false;
                for (int radius = texture.size.x / 2; radius >= 1; radius--) //  + 1)
                {
                    var chance = randomData.random.NextInt(100);
                    var isDarken = !isBrighten && chance >= darkenChance;
                    isBrighten = !isDarken && chance >= brightenChance;
                    var addition = new int3();
                    for (int c = 0; c < woodlineColorChecks; c++)
                    {
                        var redAddition = -woodLineColorDifference + randomData.random.NextInt(woodLineColorDifference * 2);
                        var greenAddition = -woodLineColorDifference + randomData.random.NextInt(woodLineColorDifference * 2);
                        var blueAddition = -woodLineColorDifference + randomData.random.NextInt(woodLineColorDifference * 2);
                        var valueAddition = math.abs(redAddition) + math.abs(greenAddition) + math.abs(blueAddition);
                        if (valueAddition >= 40)
                        {
                            addition.x = redAddition;
                            addition.y = greenAddition;
                            addition.z = blueAddition;
                            break;
                        }
                    }
                    var halfway = texture.size.y / 2;
                    var nextRadius = radius - 1; 
                    for (int x = 1; x < texture.size.x - 1; x++)
                    {
                        for (int y = 1; y < texture.size.y - 1; y++)
                        {
                            if (x >= - radius + halfway && x <= halfway + radius && y >= -radius + halfway && y <= radius + halfway
                                && !(x >= - nextRadius + halfway && x <= halfway + nextRadius && y >= -nextRadius + halfway && y <= nextRadius + halfway))
                            {
                                int pixelIndex = (int)y * texture.size.x + (int)(x);
                                var red = (int) texture.colors[pixelIndex].red;
                                var green = (int) texture.colors[pixelIndex].green;
                                var blue = (int) texture.colors[pixelIndex].blue;
                                red += addition.x;
                                green += addition.y;
                                blue += addition.z;
                                if (isDarken)
                                {
                                    red = ((int)(red * darkenMultiplier));
                                    green = ((int)(green * darkenMultiplier));
                                    blue = ((int)(blue * darkenMultiplier));
                                }
                                else if (isBrighten)
                                {
                                    red = ((int)(red * brightenMultiplier));
                                    green = ((int)(green * brightenMultiplier));
                                    blue = ((int)(blue * brightenMultiplier));
                                }
                                red = math.clamp(red, 0, 255);
                                green = math.clamp(green, 0, 255);
                                blue = math.clamp(blue, 0, 255);
                                texture.colors[pixelIndex] = new Color((byte)red, (byte)green, (byte)blue);
                            }
                        }
                    }
                }
            }

            void SpreadGrass(ref Texture texture, ref RandomData randomData, in VoxelTexture voxelTexture, int leafAmounts = 16)
            {
                const float darkenAmount = 0.08f;
                // first generate 16 random points
                var points = new NativeList<int2>();
                var textureSize = new int2(texture.size.x, texture.size.y);
                for (int x = 0; x < leafAmounts; x++)
                {
                    var newPoint = new int2(
                        randomData.random.NextInt(0, texture.size.x - 1), 
                        randomData.random.NextInt(0, texture.size.y - 1)
                    );
                    var count = 0;
                    while (count <= 255)
                    {
                        newPoint = new int2(
                            randomData.random.NextInt(0, texture.size.x - 1), 
                            randomData.random.NextInt(0, texture.size.y - 1)
                        );
                        if (!points.Contains(newPoint)
                            && !points.Contains(newPoint.MoveUp(textureSize))
                            && !points.Contains(newPoint.MoveDown(textureSize))
                            && !points.Contains(newPoint.MoveLeft(textureSize))
                            && !points.Contains(newPoint.MoveRight(textureSize)))
                        {
                            points.Add(newPoint);
                            break;
                        }
                        count++;
                    }
                }
                for (int i = 0; i < points.Length; i++)
                {
                    var point = points[i];
                    var color = texture.colors[point.GetArrayIndex(textureSize)];
                    if (randomData.random.NextInt(100) >= 60)
                    {
                        color = color.MultiplyBrightness(1f - darkenAmount - randomData.random.NextFloat(darkenAmount));
                    }
                    else
                    {
                        color = color.MultiplyBrightness(1f + darkenAmount + randomData.random.NextFloat(darkenAmount));
                    }
                    texture.colors[point.MoveUp(textureSize).GetArrayIndex(textureSize)] = color;
                    texture.colors[point.MoveDown(textureSize).GetArrayIndex(textureSize)] = color;
                    texture.colors[point.MoveLeft(textureSize).GetArrayIndex(textureSize)] = color;
                    texture.colors[point.MoveRight(textureSize).GetArrayIndex(textureSize)] = color;
                    texture.colors[point.GetArrayIndex(textureSize)] = color;
                }
            }

            public void Light(ref Texture texture, in VoxelTexture voxelTexture, float distanceMultiplier = 0.28f)
            {
                // var distanceMultiplier = 0.28f;
                var baseDistanceMultiplier = 1f;
                var center = new float2(texture.size.x / 2, texture.size.y / 2);
                var radius = texture.size.x / 2f;
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        var color = texture.colors[pixelIndex];
                        var red = ((int)color.red);
                        var green = ((int)color.green);
                        var blue =((int)color.blue);
                        // multiply based on distance to center
                        var distance = distanceMultiplier * (math.distance(new float2(x, y), center) / radius);
                        var multiplier = baseDistanceMultiplier - distance;
                        multiplier = math.clamp(multiplier, 0, 1);
                        red = (int)(red * multiplier);
                        green = (int)(green * multiplier);
                        blue = (int)(blue * multiplier);
                        red = (int)math.clamp(red, 0, 255);
                        green = (int)math.clamp(green, 0, 255);
                        blue = (int)math.clamp(blue, 0, 255);
                        texture.colors[pixelIndex] = new Color((byte)red, (byte)green, (byte)blue);
                    }
                }
            }

            public void Fill(ref Texture texture, in VoxelTexture voxelTexture)
            {
                var baseColor = voxelTexture.baseColor;
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        texture.colors[pixelIndex] = baseColor;
                    }
                }
            }

            public void Noise(ref Texture texture, in VoxelTexture voxelTexture, ref RandomData randomData)
            {
                var baseVariance = voxelTexture.baseVariance;
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        var color = texture.colors[pixelIndex];
                        var red = ((int)color.red + (int)(baseVariance.x * randomData.random.NextFloat(1)));
                        var green = ((int)color.green + (int)(baseVariance.y * randomData.random2.NextFloat(1)));
                        var blue =((int)color.blue + (int)(baseVariance.z * randomData.random3.NextFloat(1)));
                        red = (int)math.clamp(red, 0, 255);
                        green = (int)math.clamp(green, 0, 255);
                        blue = (int)math.clamp(blue, 0, 255);
                        texture.colors[pixelIndex] = new Color((byte)red, (byte)green, (byte)blue);
                    }
                }
            }

            public void GrowColours(ref Texture texture, in VoxelTexture voxelTexture, ref RandomData randomData)
            {
                var darkenedColor = new Color(0,0,0);
                var baseColor = voxelTexture.baseColor;
                var darkenedColor2 = voxelTexture.baseColor.MultiplyBrightness(0.8f);
                for (int x = 1; x < texture.size.x - 1; x++)
                {
                    for (int y = 1; y < texture.size.y - 1; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        int pixelIndexUp = (int)(y + 1) * texture.size.x + (int)x;
                        int pixelIndexDown = (int)(y - 1) * texture.size.x + (int)x;
                        int pixelIndexLeft = (int)(y) * texture.size.x + (int)(x - 1);
                        int pixelIndexRight = (int)(y) * texture.size.x + (int)(x + 1);
                        var darkPixelNeighbors = 0;
                        if (texture.colors[pixelIndexUp] == darkenedColor)
                        {
                            darkPixelNeighbors++;
                        }
                        if (texture.colors[pixelIndexDown] == darkenedColor)
                        {
                            darkPixelNeighbors++;
                        }
                        if (texture.colors[pixelIndexLeft] == darkenedColor)
                        {
                            darkPixelNeighbors++;
                        }
                        if (texture.colors[pixelIndexRight] == darkenedColor)
                        {
                            darkPixelNeighbors++;
                        }
                        if (darkPixelNeighbors >= 3)
                        {
                            texture.colors[pixelIndex] = baseColor;
                        }
                    }
                }
                for (int x = 1; x < texture.size.x - 1; x++)
                {
                    for (int y = 1; y < texture.size.y - 1; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        int pixelIndexUp = (int)(y + 1) * texture.size.x + (int)x;
                        int pixelIndexDown = (int)(y - 1) * texture.size.x + (int)x;
                        int pixelIndexLeft = (int)(y) * texture.size.x + (int)(x - 1);
                        int pixelIndexRight = (int)(y) * texture.size.x + (int)(x + 1);
                        var darkPixelNeighbors = 0;
                        if (texture.colors[pixelIndexUp] == darkenedColor)
                        {
                            darkPixelNeighbors++;
                        }
                        if (texture.colors[pixelIndexDown] == darkenedColor)
                        {
                            darkPixelNeighbors++;
                        }
                        if (texture.colors[pixelIndexLeft] == darkenedColor)
                        {
                            darkPixelNeighbors++;
                        }
                        if (texture.colors[pixelIndexRight] == darkenedColor)
                        {
                            darkPixelNeighbors++;
                        }
                        if (darkPixelNeighbors == 4)
                        {
                            texture.colors[pixelIndex] = baseColor;
                        }
                    }
                }
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        if (texture.colors[pixelIndex] == darkenedColor)
                        {
                            texture.colors[pixelIndex] = darkenedColor2;
                        }
                    }
                }
            }

            // todo: find nearest points at bottom too
            public void Voronoi(ref Texture texture, in VoxelTexture voxelTexture, ref RandomData randomData,
                float multiplier = 1, float darkenMultiplier = 0.6f, bool isDarkenEdge = false)
            {
                var pointsCount = texture.size.x; // 16;
                var pointCloseness = texture.size.x / 5; //3;
                var baseColor = voxelTexture.baseColor;
                var baseVariance = voxelTexture.baseVariance * multiplier;
                // generate random points
                var points = new NativeArray<int2>(pointsCount, Allocator.Temp);
                var regionColors = new NativeArray<Color>(pointsCount, Allocator.Temp);
                for (int i = 0; i < pointsCount; i++)
                {
                    // get new point
                    var newPoint = new int2(randomData.random.NextInt(texture.size.x), randomData.random.NextInt(texture.size.y));
                    var count = 0;
                    while (count <= 128)
                    {
                        newPoint = new int2(randomData.random.NextInt(texture.size.x), randomData.random.NextInt(texture.size.y));
                        bool isTooClose = false;
                        for (var j = i - 1; j >= 0; j--)
                        {
                            var otherPoint = points[j];
                            if (    newPoint.x + pointCloseness > otherPoint.x - pointCloseness &&
                                    newPoint.x - pointCloseness < otherPoint.x + pointCloseness &&
                                    newPoint.y + pointCloseness > otherPoint.y - pointCloseness &&
                                    newPoint.y - pointCloseness < otherPoint.y + pointCloseness)
                            {
                                isTooClose = true;
                                break;
                            }
                        }
                        if (!isTooClose)
                        {
                            break;
                        }
                        count++;
                    }
                    points[i] = newPoint;
                    var red = ((int)baseColor.red + randomData.random.NextInt(baseVariance.x));
                    var green = ((int)baseColor.green + randomData.random2.NextInt(baseVariance.y));
                    var blue = ((int)baseColor.blue + randomData.random3.NextInt(baseVariance.z));
                    regionColors[i] = new Color((byte)red, (byte)green, (byte)blue);
                }

                var size = new int2(texture.size.x, texture.size.y);
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        var regionIndex = GetClosestIndex(new int2(x, y), in points, size);
                        texture.colors[pixelIndex] = regionColors[regionIndex];
                    }
                }
                // darken edges
               // var darkenMultiplier = 0.6f;
                // var newPixels = new BlitableArray<Color>(texture.colors.Length, Allocator.Temp);
                var isDarken = new NativeArray<byte>(texture.colors.Length, Allocator.Temp);
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        {
                            var pixel = texture.colors[pixelIndex];
                            var positionUp = new int2(x, y + 1);
                            if (positionUp.y == texture.size.y)
                            {
                                positionUp.y = 0;
                            }
                            var positionRight = new int2(x + 1, y);
                            if (positionRight.x == texture.size.x)
                            {
                                positionRight.x = 0;
                            }
                            var pixelIndexUp = (int) (positionUp.y) * texture.size.x + (int)(positionUp.x);
                            //var pixelIndexDown = (int) (y - 1) * texture.size.x + (int)(x);
                            var pixelIndexRight = (int) (positionRight.y) * texture.size.x + (int)(positionRight.x);
                            //var pixelIndexLeft = (int) (y + 1) * texture.size.x + (int)(x - 1);
                            var colorDiffUp = pixel != texture.colors[pixelIndexUp];
                            //var colorDiffDown = pixel != texture.colors[pixelIndexDown];
                            //var colorDiffLeft = pixel != texture.colors[pixelIndexLeft];
                            var colorDiffRight = pixel != texture.colors[pixelIndexRight];
                            if (colorDiffUp || colorDiffRight) // colorDiffDown ||  colorDiffLeft || 
                                //|| colorDiffLeft ||colorDiffRight)
                            {
                                isDarken[pixelIndex] = 1;
                            }
                            else
                            {
                                // any positions outside of borders, flip around
                                isDarken[pixelIndex] = 0;
                            }
                        }
                    }
                }
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        if (isDarken[pixelIndex] == 1)
                        {
                            var red = (int) texture.colors[pixelIndex].red;
                            var green = (int) texture.colors[pixelIndex].green;
                            var blue = (int) texture.colors[pixelIndex].blue;
                            red = ((int)(red * darkenMultiplier));
                            green = ((int)(green * darkenMultiplier));
                            blue = ((int)(blue * darkenMultiplier));
                            texture.colors[pixelIndex] = new Color((byte)red, (byte)green, (byte)blue);
                        }
                    }
                }
            }

            public int GetClosestIndex(int2 point, in NativeArray<int2> points, int2 size)
            {
                var point2 = new float2(point.x, point.y);
                var smallestDistance = float.MaxValue;
                var smallestIndex = 0;
                for (int i = 0; i < points.Length; i++)
                {
                    var otherPoint = points[i];
                    var distance = math.distance(point2, new float2(otherPoint.x, otherPoint.y));
                    if (distance < smallestDistance)
                    {
                        smallestDistance = distance;
                        smallestIndex = i;
                    }
                    var distance2 = math.distance(point2, new float2(otherPoint.x, otherPoint.y + size.y));
                    if (distance2 < smallestDistance)
                    {
                        smallestDistance = distance2;
                        smallestIndex = i;
                    }
                    var distance3 = math.distance(point2, new float2(otherPoint.x, otherPoint.y - size.y));
                    if (distance3 < smallestDistance)
                    {
                        smallestDistance = distance3;
                        smallestIndex = i;
                    }
                    var distance4 = math.distance(point2, new float2(otherPoint.x + size.x, otherPoint.y));
                    if (distance4 < smallestDistance)
                    {
                        smallestDistance = distance4;
                        smallestIndex = i;
                    }
                    var distance5 = math.distance(point2, new float2(otherPoint.x - size.x, otherPoint.y));
                    if (distance5 < smallestDistance)
                    {
                        smallestDistance = distance5;
                        smallestIndex = i;
                    }
                }
                return smallestIndex;
            }
        }
    }
}
                            /*if ( 
                                // vertical lines
                                //(brickLayer % 2 == 0 && (x % tileSize == 0)) 
                                //|| (brickLayer % 2 != 0 && ((x + (tileSize / 2)) % tileSize == 0)) || 

                                y % tileSize == 0 || y == texture.size.y - 1)    // horizontal lines
                            {
                                isDarken = true;
                            }*/
                        /*float distance = 0.5f * (math.distance(new float2(x, y), new float2(texture.size.x / 2, texture.size.y / 2)) / (texture.size.x / 2f));
                        distance = math.clamp(distance, 0, 1);
                        float multiplier = 1f * distance;
                        //float multiplier = 1f - 0.8f * distance;
                        red = (byte) ((int)(red * multiplier));
                        green = (byte) ((int)(green * multiplier));
                        blue = (byte) ((int)(blue * multiplier));*/
                        // outer edge
                        /*if (x < linesThickness || x > texture.size.x - 1 - linesThickness
                            || y < linesThickness || y > texture.size.y - 1 - linesThickness)
                        {
                            red = ((int)(red * darkenEdgesMultiplier));
                            green = ((int)(green * darkenEdgesMultiplier));
                            blue = ((int)(blue * darkenEdgesMultiplier));
                        }
                        else
                        {
                            // inside
                            if ( (brickLayer % 2 == 0 && (x % tileSize == 0)) 
                                || (brickLayer % 2 != 0 && ((x + (tileSize / 2)) % tileSize == 0)) 
                                || y % tileSize == 0 || y == texture.size.y - 1)    // horizontal lines
                            {
                                red = ((int)(red * darkenEdgesMultiplier));
                                green = ((int)(green * darkenEdgesMultiplier));
                                blue = ((int)(blue * darkenEdgesMultiplier));
                            }
                        }*/

            /*public void GenerateTiles(ref Texture texture, ref RandomData randomData, in VoxelTexture voxelTexture, int3 baseVariance, int3 noiseVariance)
            {
                var baseColor = voxelTexture.baseColor; // .Saturate(0.8f);
                var tileSize = texture.size.x / 4;
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;

                        var red = (baseColor.red + (int)(noiseVariance.x * randomData.random.NextFloat(1)));
                        var green = (baseColor.green + (int)(noiseVariance.y * randomData.random2.NextFloat(1)));
                        var blue = (baseColor.blue + (int)(noiseVariance.z * randomData.random3.NextFloat(1)));
                        if (x % tileSize == 0 || x == texture.size.x - 1 || y % tileSize == 0 || y == texture.size.y - 1)
                        {
                            var darkness = 0.8f;
                            red = ((int)(red * darkness));
                            green = ((int)(green * darkness));
                            blue = ((int)(blue * darkness));
                        }
                        if (x== 0 || x == texture.size.x - 1 || y == 0 || y == texture.size.y - 1)
                        {
                            var darkness = 0.95f;
                            red = ((int)(red * darkness));
                            green = ((int)(green * darkness));
                            blue = ((int)(blue * darkness));
                        }
                        red = math.clamp(red, 0, 255);
                        green = math.clamp(green, 0, 255);
                        blue = math.clamp(blue, 0, 255);
                        texture.colors[pixelIndex] = new Color((byte)red, (byte)green, (byte)blue);
                    }
                }
            }*/  

            /*public void GenerateStone(ref Texture texture, ref RandomData randomData, in VoxelTexture voxelTexture, int3 noiseVariance)
            {
                // Fill(ref texture, in voxelTexture);

                var baseColor = voxelTexture.baseColor; // .Saturate(0.7f);
                var distanceMultiplier = 0.28f;
                var baseDistanceMultiplier = 1f;
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        var red = ((int)baseColor.red + (int)(noiseVariance.x * randomData.random.NextFloat(1)));
                        var green = ((int)baseColor.green + (int)(noiseVariance.y * randomData.random2.NextFloat(1)));
                        var blue =((int)baseColor.blue + (int)(noiseVariance.z * randomData.random3.NextFloat(1)));
                        // multiply based on distance to center
                        var distance = distanceMultiplier * (math.distance(new float2(x, y), new float2(texture.size.x / 2, texture.size.y / 2)) / (texture.size.x / 2f));
                        var multiplier = baseDistanceMultiplier - distance;
                        multiplier = math.clamp(multiplier, 0, 1);
                        red += 20;
                        green += 20;
                        blue += 20;
                        red = (int)math.clamp(red * multiplier, 0, 255);
                        green = (int)math.clamp(green * multiplier, 0, 255);
                        blue = (int)math.clamp(blue * multiplier, 0, 255);
                        texture.colors[pixelIndex] = new Color((byte)red, (byte)green, (byte)blue);
                    }
                }
            }*/
            
            // var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
                // entityHandle = GetEntityTypeHandle(),
                // PostUpdateCommands = PostUpdateCommands,
            // public EntityCommandBuffer.ParallelWriter PostUpdateCommands;
            // [ReadOnly] public EntityTypeHandle entityHandle;

                /*var isDarken2 = new NativeArray<byte>(texture.colors.Length, Allocator.Temp);
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        if ((x == 0 || y == 0 || x == texture.size.x - 1 || y == texture.size.y - 1))
                        {
                            isDarken2[pixelIndex] = isDarken[pixelIndex];
                        }
                        else
                        {
                            var pixel = texture.colors[pixelIndex];
                            var pixelIndexUp = (int) (y + 1) * texture.size.x + (int)(x);
                            var pixelIndexDown = (int) (y - 1) * texture.size.x + (int)(x);
                            var pixelIndexRight = (int) (y) * texture.size.x + (int)(x + 1);
                            var pixelIndexLeft = (int) (y + 1) * texture.size.x + (int)(x - 1);
                            var isDarkenUp = isDarken[pixelIndexUp] == 1;
                            var isDarkenDown = isDarken[pixelIndexDown] == 1;
                            var isDarkenLeft = isDarken[pixelIndexLeft] == 1;
                            var isDarkenRight = isDarken[pixelIndexRight] == 1;
                            if ((isDarkenUp && isDarkenDown && !isDarkenLeft && !isDarkenRight)
                                || (isDarkenLeft && isDarkenRight && !isDarkenUp && !isDarkenDown)
                                || (isDarkenLeft && isDarkenUp && !isDarkenRight && !isDarkenDown)
                                || (isDarkenRight && isDarkenDown && !isDarkenLeft && !isDarkenUp))
                            {
                                isDarken2[pixelIndex] = 1;
                            }
                            else
                            {
                                isDarken2[pixelIndex] = 0;
                            }
                        }
                    }
                }*/
                        /*int red = 0;
                        int green = 0;
                        int blue = 0;*/
                            /*red = (secondaryColor.red + (int)(noiseVariance.x * randomData.random.NextFloat(1)));
                            green = (secondaryColor.green + (int)(noiseVariance.y * randomData.random2.NextFloat(1)));
                            blue = (secondaryColor.blue + (int)(noiseVariance.z * randomData.random3.NextFloat(1)));
                            red = math.clamp(red, 0, 255);
                            green = math.clamp(green, 0, 255);
                            blue = math.clamp(blue, 0, 255);
                            texture.colors[pixelIndex] = new Color((byte)red, (byte)green, (byte)blue);*/
                            /*i/f (x == 4)
                            {
                                UnityEngine.Debug.LogError(lines.Length + " ~ Adding new line: " + woodLineStart + " with length: " + woodLineLength);
                            }*/
                        /*red += 20;
                        green += 20;
                        blue += 20;*/
                                /*(colorDiffUp && !colorDiffDown && !colorDiffLeft && !colorDiffRight)
                                || (!colorDiffUp && colorDiffDown && !colorDiffLeft && !colorDiffRight)
                                || (!colorDiffLeft && colorDiffRight && !colorDiffUp && !colorDiffDown)
                                || (colorDiffLeft && !colorDiffRight && !colorDiffUp && !colorDiffDown))*/
                        /*if ((x == 0 || y == 0 || x == texture.size.x - 1 || y == texture.size.y - 1))
                        {
                            if (isDarkenEdge)
                            {
                                isDarken[pixelIndex] = 1;
                            }
                            else
                            {
                                // any positions outside of borders, flip around
                                isDarken[pixelIndex] = 0;
                            }
                        }
                        else*/