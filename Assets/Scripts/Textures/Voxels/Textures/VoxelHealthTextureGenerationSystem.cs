using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Rendering.Authoring;

namespace Zoxel.Textures.Voxels
{
    //! Generates a destruction texture animation.
    /**
    *   - Texture Generation System -
    *   \todo Give unique seeds.
    */
    [BurstCompile, UpdateInGroup(typeof(TextureSystemGroup))]
    public partial class VoxelHealthTextureGenerationSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<RenderSettings>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            // const bool isDebugTexture = false;
            var seed = IDUtil.GenerateUniqueID();
            var renderSettings = GetSingleton<RenderSettings>();
            var textureSize = new int2(renderSettings.voxelDestructionTextureResolution, renderSettings.voxelDestructionTextureResolution);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<TextureDirty>()
                .WithAll<GenerateVoxelHealthTexture>()
                .ForEach((Entity e, int entityInQueryIndex, ref Texture texture) =>
            {
                // generate texture
                var texturesRow = 8;
                texture.Initialize(new int2(textureSize.x * texturesRow, textureSize.y * texturesRow));
                // Clear Pixels
                for (int i = 0; i < texture.colors.Length; i++)
                {
                    texture.colors[i] = new Color(255, 255, 255, 0);
                }
                var maxPixels = textureSize.x * textureSize.y;
                // var maxRadius = 9f;
                var k = 0;
                for (int j = 0; j < texturesRow; j++)
                {
                    for (int i = 0; i < texturesRow; i++)
                    {
                        var texturePosition = new int2(i * textureSize.x, j * textureSize.y);
                        k++;
                        var percentage = (float) (k / (float)(texturesRow * texturesRow));
                        if (percentage >= 1f)
                        {
                            CreateBlackTexture(ref texture, texturePosition, textureSize);
                        }
                        else
                        {
                            var random = new Random();
                            random.InitState((uint) seed);
                            // var radius = (int) (maxRadius * percentage);
                            var targetPixelsCount = (int) (percentage * maxPixels);
                            //UnityEngine.Debug.LogError(k + " - targetPixelsCount: " + targetPixelsCount
                            //    + ", texturePosition: " + texturePosition);
                            CreateDestructionTexture(ref texture, ref random, texturePosition, textureSize, targetPixelsCount);
                        }
                    }
                }
                PostUpdateCommands.AddComponent<TextureDirty>(entityInQueryIndex, e);
                PostUpdateCommands.RemoveComponent<GenerateVoxelHealthTexture>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static void CreateBlackTexture(ref Texture texture, int2 position, int2 size)
        {
            var totalSize = new int2(texture.size.x, texture.size.y);
            for (int i = position.x; i < position.x + size.x; i++)
            {
                for (int j = position.y; j < position.y + size.y; j++)
                {
                    var index = VoxelUtilities.GetTextureArrayIndexXY(new int2(i, j), totalSize);
                    texture.colors[index] = new Color(0, 0, 0, 255);
                }
            }
        }

        private static void CreateDestructionTexture(ref Texture texture, ref Random random, int2 position, int2 size, int targetPixelsCount)
        {
            var totalSize = new int2(texture.size.x, texture.size.y);
            for (int i = 0; i < targetPixelsCount; i++)
            {
                var hasFilledInPixel = false;
                var count = 0;
                while (!hasFilledInPixel && count < 64)
                {
                    var pixelPosition = position + new int2(random.NextInt(0, size.x), random.NextInt(0, size.y));
                    var index = VoxelUtilities.GetTextureArrayIndexXY(pixelPosition, totalSize);
                    if (texture.colors[index].alpha == 0)
                    {
                        texture.colors[index] = new Color(0, 0, 0, 255); 
                        hasFilledInPixel = true;
                        //UnityEngine.Debug.LogError("Darkness at: " + pixelPosition);
                    }
                    else
                    {
                        // UnityEngine.Debug.LogError("Position off: " + pixelPosition);
                        count++;
                    }
                }
            }
        }
    }
}

        /*private static void CreateDestructionTexture(ref Texture texture, int2 position, int2 size, int radius)
        {
            var totalSize = new int2(texture.size.x, texture.size.y);
            var midPoint = position + size / 2;
            for (int i = position.x; i < position.x + size.x; i++)
            {
                for (int j = position.y; j < position.y + size.y; j++)
                {
                    var index = VoxelUtilities.GetTextureArrayIndexXY(new int2(i, j), totalSize);
                    if (i >= midPoint.x - radius && i <= midPoint.x + radius && 
                        j >= midPoint.y - radius && j <= midPoint.y + radius)
                    {
                        texture.colors[index] = new Color(0, 0, 0, 255);
                    }
                    else
                    {
                        texture.colors[index] = new Color(0, 0, 0, 0);
                    }
                }
            }
        }*/