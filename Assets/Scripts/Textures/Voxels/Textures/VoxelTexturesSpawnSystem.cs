using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Voxels.Authoring;
using Zoxel.Transforms;
using Zoxel.Textures;
using Zoxel.Rendering;
using Zoxel.Voxels;

namespace Zoxel.Textures.Voxels
{
    //! Spawns texture entities linked to a voxel.
    /**
    *   Spawns multiple textures per voxel. Spawns them per resolution as well.
    *   \todo Pass in materials, base resolutions off voxel materials.
    */
    [BurstCompile, UpdateInGroup(typeof(VoxelSystemGroup))]
    public partial class VoxelTexturesSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity voxelTexturePrefab;
        const int differenceName = 256;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var voxelTextureArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(GenerateTextureVoxel),
                typeof(Seed),
                typeof(Texture),
                typeof(VoxelTexture),
                typeof(VoxelTextureResolution),
                typeof(ParentLink),
                typeof(NewChild),
                typeof(Child)
            );
            voxelTexturePrefab = EntityManager.CreateEntity(voxelTextureArchetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(voxelTexturePrefab);
            // EntityManager.AddComponentData(voxelTexturePrefab, new EditorName("[voxeltexture]"));
            #endif
            RequireForUpdate<VoxelTextureSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
			var resolutionsCount = VoxelManager.instance.voxelSettings.textureLowerDistances.Length;
            var voxelTextureSettings = GetSingleton<VoxelTextureSettings>();
            var disableVoxelTextures = voxelTextureSettings.disableVoxelTextures;
            var voxelTextureResolution = voxelTextureSettings.voxelTextureResolution;
            var voxelTexturePrefab = this.voxelTexturePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<SpawnVoxelTextures>()
                .ForEach((Entity e, int entityInQueryIndex, ref VoxelTextureTypes voxelTextureTypes, in Voxel voxel, in Seed seed) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnVoxelTextures>(entityInQueryIndex, e);
                if (disableVoxelTextures)
                {
                    return;
                }
                // multi texture - Grass
                var voxelType = (VoxelType) voxel.type;
                var texturesCount = 1;
                if (voxelType == VoxelType.Grass)
                {
                    texturesCount = 3;
                }
                else if (voxelType == VoxelType.Wood)
                {
                    texturesCount = 2;
                }
                var baseColor = voxel.color;    // this?
                voxelTextureTypes.textureTypes = new BlitableArray<byte>(texturesCount, Allocator.Persistent);
                var textureCount = 0;
                var textureResolution = voxelTextureResolution;
                for (int j = 0; j < resolutionsCount; j++)
                {
                    for (int i = 0; i < texturesCount; i++)
                    {
                        var textureSeed = seed.seed + i;  //  (uint) 
                        var textureEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, voxelTexturePrefab);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, textureEntity, new ParentLink(e));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, textureEntity, new Zoxel.Transforms.Child(textureCount));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, textureEntity, new VoxelTextureResolution(textureResolution));
                        textureCount++;
                        var color = voxel.color;
                        var secondaryColor = voxel.secondaryColor;
                        //var voxelType2 = (byte)(voxel.type);
                        var voxelTextureType = VoxelTextureType.Bedrock;
                        // todo: redo grass texture
                        if (voxelType == VoxelType.Grass)
                        {
                            // chose more red one for soil color
                            if (i == 0)
                            {
                                voxelTextureTypes.textureTypes[i] = (byte) TextureVoxelSide.Up;
                                voxelTextureType = VoxelTextureType.Grass;
                                //color = secondaryColor;
                            }
                            else if (i == 1)
                            {
                                voxelTextureTypes.textureTypes[i] = (byte) TextureVoxelSide.Down;
                                voxelTextureType = VoxelTextureType.Soil;
                                color = voxel.secondaryColor;
                                secondaryColor = voxel.color;
                            }
                            else
                            {
                                voxelTextureTypes.textureTypes[i] = (byte) TextureVoxelSide.Default;
                                voxelTextureType = VoxelTextureType.SoilGrass;
                                color = voxel.secondaryColor;
                                secondaryColor = voxel.color;
                                //UnityEngine.Debug.LogError("Setting SoilGrass!");
                            }
                        }
                        /*if (voxelType == VoxelType.Grass)
                        {
                            voxelTextureTypes.textureTypes[i] = (byte) TextureVoxelSide.Default;
                            voxelTextureType = VoxelTextureType.Grass;
                        }
                        */
                        else if (voxelType == VoxelType.Wood)
                        {
                            if (i == 0)
                            {
                                voxelTextureTypes.textureTypes[i] = (byte) TextureVoxelSide.UpDown;
                                voxelTextureType = VoxelTextureType.WoodTop;
                            }
                            else
                            {
                                voxelTextureTypes.textureTypes[i] = (byte) TextureVoxelSide.Default;
                                voxelTextureType = VoxelTextureType.Wood;
                            }
                        }
                        else 
                        {
                            voxelTextureType = (VoxelTextureType) voxel.textureType;
                            voxelTextureTypes.textureTypes[i] = (byte) TextureVoxelSide.Default;
                        }
                        InitializeTextureEntity(PostUpdateCommands, entityInQueryIndex, textureEntity, textureSeed, voxelTextureType, textureResolution, color, secondaryColor);
                    }
                    textureResolution /= 2;
                }
                PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnChildrenSpawned((byte)(texturesCount * resolutionsCount)));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private static void InitializeTextureEntity(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, Entity textureEntity, int textureSeed,
            VoxelTextureType generationType, int textureResolution, Color baseColor, Color secondaryColor)
        {
            // set permanent texture generation data here
            var voxelTexture = new VoxelTexture
            {
                // textureSeed = textureSeed,
                generationType = (byte) generationType,
                baseColor = baseColor,
                secondaryColor = secondaryColor,
                baseVariance = new int3(24, 18, 18),
                secondaryVariance = new int3(22, 22, 22)
            };
            PostUpdateCommands.SetComponent(entityInQueryIndex, textureEntity, voxelTexture);
            PostUpdateCommands.SetComponent(entityInQueryIndex, textureEntity, new Seed(textureSeed));
            var texture = new Texture();
            texture.Initialize(new int2(textureResolution, textureResolution));
            PostUpdateCommands.SetComponent(entityInQueryIndex, textureEntity, texture);
        }
    }
}