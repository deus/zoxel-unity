using Unity.Entities;
using Zoxel.Voxels;

namespace Zoxel.Textures.Voxels
{
    //! Uses a Vox to generate a texture.
    public struct GenerateTextureVox : IComponentData
    {
        public Entity vox;
        public int resolution;
        public byte count;
        public byte delay;

        public GenerateTextureVox(Entity vox, int resolution = 32, byte delay = 0)
        {
            this.vox = vox;
            this.resolution = resolution;
            this.delay = delay;
            this.count = 0;
        }
    }
}