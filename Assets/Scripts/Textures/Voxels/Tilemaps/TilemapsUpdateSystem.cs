using Unity.Entities;
using Unity.Burst;
using Zoxel.Rendering;

namespace Zoxel.Textures.Tilemaps
{
    //! Passes on SpawnTilemaps event onto MaterialLinks materials.
    [BurstCompile, UpdateInGroup(typeof(TilemapSystemGroup))]
    public partial class TilemapsUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<SpawnTilemaps>()
                .ForEach((Entity e, int entityInQueryIndex, in MaterialLinks materialLinks) =>
            {
                PostUpdateCommands.RemoveComponent<SpawnTilemaps>(entityInQueryIndex, e);
                // UnityEngine.Debug.LogError("SpawnTilemaps materials: " + materialLinks.materials.Length + "::" + voxelLinks.textureResolution);
                for (int i = 0; i < materialLinks.materials.Length; i++)
                {
                    // PostUpdateCommands.AddComponent(entityInQueryIndex, materialLinks.materials[i], new SpawnTilemaps((byte) voxelTextureResolution));
                    PostUpdateCommands.AddComponent<SpawnTilemaps>(entityInQueryIndex, materialLinks.materials[i]);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}