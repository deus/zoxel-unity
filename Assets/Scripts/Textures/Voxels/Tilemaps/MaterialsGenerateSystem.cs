using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Zoxel.Rendering;
using Zoxel.Voxels;
using Zoxel.Voxels.Authoring;

namespace Zoxel.Textures.Tilemaps
{
    //! Generates Materials for the Realm. Materials are authored.
    /**
    *   Convert by setting UnityMaterial in Initialize system instead.
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(TilemapSystemGroup))]
    public partial class MaterialsGenerateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity materialPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var materialArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(ZoxID),
                typeof(ZoxName),
                // typeof(Material),
                typeof(MaterialData),
                typeof(UnityMaterial),
                typeof(TilemapLinks),
                typeof(RealmLink),
                typeof(UserDataIndex),
                typeof(CreatorLink)
            );
            materialPrefab = EntityManager.CreateEntity(materialArchetype);
            RequireForUpdate<VoxelTextureSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var voxelTextureSettings = GetSingleton<VoxelTextureSettings>();
            var voxelTextureResolution = voxelTextureSettings.voxelTextureResolution;
			var resolutionsCount = (byte) VoxelManager.instance.voxelSettings.textureLowerDistances.Length;
            var voxelMaterials = VoxelManager.instance.voxelMaterials.voxelMaterials;
            var voxelMaterialTypes = VoxelManager.instance.voxelMaterials.voxelMaterialTypes;
            var materialPrefab = MaterialsGenerateSystem.materialPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); //.AsParallelWriter();
            Entities
                .ForEach((Entity e, ref MaterialLinks materialLinks, ref GenerateRealm generateRealm) =>
            {
                if (generateRealm.state != (byte) GenerateRealmState.GenerateMaterials)
                {
                    return; // wait for thingo
                }
                generateRealm.state = (byte) GenerateRealmState.GenerateStats;
                // Destroy Previous materials
                for (int i = 0; i < materialLinks.materials.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(materialLinks.materials[i]);
                }
                materialLinks.Dispose();
                var count = (byte) 0;
                var realmLink = new RealmLink(e);
                var creatorLink = new CreatorLink(e);
                for (byte i = 0; i < voxelMaterials.Length; i++)
                {
                    var material = voxelMaterials[i];
                    var voxelMaterialType = (byte) voxelMaterialTypes[i];
                    if (material == null)
                    {
                        UnityEngine.Debug.LogError("Problem with Material at: " + i);
                        continue;
                    }
                    material = new UnityEngine.Material(material);
                    var materialEntity = PostUpdateCommands.Instantiate(materialPrefab);
                    // PostUpdateCommands.SetComponent(materialEntity, new ZoxID(slotDatam.data.id));
                    PostUpdateCommands.SetComponent(materialEntity, new ZoxName(new Text(material.name)));
                    var resolutions = (byte) 1;
                    if (i == 0 || i == 2)
                    {
                        resolutions = resolutionsCount;
                    }
                    PostUpdateCommands.SetComponent(materialEntity, new MaterialData(voxelMaterialType, resolutions, voxelTextureResolution));
                    PostUpdateCommands.SetSharedComponentManaged(materialEntity, new UnityMaterial(material));
                    PostUpdateCommands.SetComponent(materialEntity, realmLink);
                    PostUpdateCommands.SetComponent(materialEntity, creatorLink);
                    PostUpdateCommands.SetComponent(materialEntity, new UserDataIndex(i));
                    count++;
                }
                PostUpdateCommands.AddComponent(e, new OnMaterialsSpawned(count));
            }).WithoutBurst().Run();
        }
    }
}