using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.Textures.Tilemaps
{
    //! Updates UnityTexture after tilemap is generated.
    [UpdateAfter(typeof(VoxelTilemapGenerationSystem))]
    [UpdateInGroup(typeof(TilemapSystemGroup))]
    public partial class VoxelTilemapUpdateSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null) return;
            var voxelItemMaterial = MaterialsManager.instance.materials.voxelItemMaterial;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<GenerateTilemap, VoxelTilemap>()
                .ForEach((Entity e, UnityTexture unityTexture, UnityMaterial unityMaterial, in Texture texture, in Tilemap tilemap, in MaterialLink materialLink) =>
            {
                if (unityTexture.texture)
                {
                    ObjectUtil.Destroy(unityTexture.texture);
                }
                unityTexture.texture = texture.GetTexture();
                PostUpdateCommands.SetSharedComponentManaged(e, unityTexture);
                if (TextureDebugger.instance)
                {
                    TextureDebugger.instance.AddTilemap(unityTexture.texture);
                }
                if (unityMaterial.material == null)
                {
                    var baseMaterial = EntityManager.GetSharedComponentManaged<UnityMaterial>(materialLink.material).material;
                    unityMaterial.material = new UnityEngine.Material(baseMaterial);
                    PostUpdateCommands.SetSharedComponentManaged(e, unityMaterial);
                }
                unityMaterial.material.SetTexture("_BaseMap", unityTexture.texture);
                if (HasComponent<HighestResolutionTilemap>(e))
                {
                    var materialData = EntityManager.GetComponentData<MaterialData>(materialLink.material);
                    if (materialData.type == MaterialType.Diffuse)
                    {
                        voxelItemMaterial.SetTexture("_BaseMap", unityTexture.texture);
                        voxelItemMaterial.SetInt("_TilesRowCount", tilemap.gridSize);
                    }
                }
            }).WithoutBurst().Run();
        }
    }
}