using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Zoxel.Rendering;
using Zoxel.Voxels;

namespace Zoxel.Textures.Tilemaps
{
    //! Generates Materials for the Realm. Materials are authored.
    /**
    *   - Spawn System -
    */
    [BurstCompile, UpdateInGroup(typeof(TilemapSystemGroup))]
    public partial class TilemapSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        public Entity tilemapPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(GenerateTilemap),
                typeof(ZoxID),
                typeof(ZoxName),
                typeof(VoxelTilemap),
                typeof(Tilemap),
                typeof(UserDataIndex),
                typeof(UnityTexture),
                typeof(UnityMaterial),
                typeof(Texture),
                typeof(MaterialLink),
                typeof(RealmLink)
            );
            tilemapPrefab = EntityManager.CreateEntity(archetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(tilemapPrefab);
            // EntityManager.AddComponentData(tilemapPrefab, new EditorName("[tilemap]"));
            #endif
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var tilemapPrefab = this.tilemapPrefab;
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<SpawnTilemaps>(processQuery);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<SpawnTilemaps>()
                .ForEach((Entity e, int entityInQueryIndex, in TilemapLinks tilemapLinks, in MaterialData materialData, in RealmLink realmLink) =>
            {
                if (tilemapLinks.tilemaps.Length == 0)
                {
                    var textureResolution = materialData.textureResolution;
                    for (byte i = 0; i < materialData.resolutions; i++)
                    {
                        var tilemapEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, tilemapPrefab);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, tilemapEntity, new MaterialLink(e));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, tilemapEntity, realmLink);
                        if (i == 0)
                        {
                            PostUpdateCommands.AddComponent<HighestResolutionTilemap>(entityInQueryIndex, tilemapEntity);
                        }
                        PostUpdateCommands.SetComponent(entityInQueryIndex, tilemapEntity, new UserDataIndex(i));
                        // set resolution here for tilemap generation
                        PostUpdateCommands.SetComponent(entityInQueryIndex, tilemapEntity, new Tilemap(i, textureResolution));
                        if (textureResolution != 1)
                        {
                            textureResolution /= 2;
                        }
                        // link first one to WorldVoxelRenderer Singleton
                        //      When finishes updatingg texture, set to this material
                    }
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnSpawnedTilemaps(materialData.resolutions));
                }
                else
                {
                    for (int i = 0; i < tilemapLinks.tilemaps.Length; i++)
                    {
                        PostUpdateCommands.AddComponent<GenerateTilemap>(entityInQueryIndex, tilemapLinks.tilemaps[i]);
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}