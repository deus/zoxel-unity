using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Voxels;
using Zoxel.Voxels.Authoring;

namespace Zoxel.Textures.Tilemaps
{
    //! Converts a bunch of Entity's Texture's into a Tilemap.
    /**
    *   - Texture Generation System -
    *   This will generate a tilemap per a tilemap entity.
    *   After voxels are spawned in realm, it will trigger materials to be dirty.
    *   Then for each material, it will spawn / update each tilemap linked to the material, per resolution.
    *   \todo Update voxel uvs in a different system.
    */
    [BurstCompile, UpdateInGroup(typeof(TilemapSystemGroup))]
    public partial class VoxelTilemapGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery realmQuery;
        private EntityQuery voxelQuery;
        private EntityQuery textureQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            realmQuery = GetEntityQuery(
				ComponentType.ReadOnly<Realm>(),
				ComponentType.ReadOnly<VoxelLinks>(),
				ComponentType.Exclude<DestroyEntity>());
            voxelQuery = GetEntityQuery(
				ComponentType.ReadOnly<Voxel>(),
				ComponentType.ReadOnly<MaterialLink>(),
				ComponentType.Exclude<DestroyEntity>());
            textureQuery = GetEntityQuery(
				ComponentType.Exclude<DestroyEntity>(),
				ComponentType.Exclude<GenerateTextureVoxel>(),
				ComponentType.ReadOnly<Texture>(),
				ComponentType.ReadOnly<VoxelTextureResolution>());
            RequireForUpdate<VoxelTextureSettings>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var voxelTextureSettings = GetSingleton<VoxelTextureSettings>();
            var disableVoxelTextures = voxelTextureSettings.disableVoxelTextures;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var realmEntities = realmQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var voxelLinks2 = GetComponentLookup<VoxelLinks>(true);
            realmEntities.Dispose();
            var voxelEntities = voxelQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var voxelUVMaps = GetComponentLookup<VoxelUVMap>(true);
            var materialLinks = GetComponentLookup<MaterialLink>(true);
            var childrens = GetComponentLookup<Childrens>(true);
            var voxelTextureTypes2 = GetComponentLookup<VoxelTextureTypes>(true);
            voxelEntities.Dispose();
            var textureEntities = textureQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var textures2 = GetComponentLookup<Texture>(true);
            var voxelTextureResolutions = GetComponentLookup<VoxelTextureResolution>(true);
            textureEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<GenerateTilemap>()
                .ForEach((Entity e, int entityInQueryIndex, ref Texture texture, ref Tilemap tilemap, in MaterialLink materialLink, in RealmLink realmLink) =>
            {
                if (disableVoxelTextures)
                {
                    PostUpdateCommands.RemoveComponent<GenerateTilemap>(entityInQueryIndex, e);
                    texture.Initialize(new int2(1, 1));
                    for (int i = 0; i < texture.colors.Length; i++)
                    {
                        texture.colors[i] = new Color(111, 111, 111, 255);
                    }
                    return;
                }
                var voxelLinks = voxelLinks2[realmLink.realm];
                // check if still loading textures
                if (voxelLinks.voxels.Length == 0)
                {
                    return;
                }
                for (int i = 0; i < voxelLinks.voxels.Length; i++)
                {
                    var voxelEntity = voxelLinks.voxels[i];
                    if (!HasComponent<Voxel>(voxelEntity))
                    {
                        return;
                    }
                    /*if (HasComponent<GenerateTextureVoxel>(voxelEntity))
                    {
                        //UnityEngine.Debug.LogError("Still building texture.");
                        return;
                    }
                    else */
                    if (voxelTextureTypes2.HasComponent(voxelEntity))
                    {
                        if (HasComponent<SpawnVoxelTextures>(voxelEntity) || HasComponent<OnChildrenSpawned>(voxelEntity))
                        {
                            return;
                        }
                        var textureLinks = childrens[voxelEntity];
                        if (textureLinks.children.Length == 0)
                        {
                            //UnityEngine.Debug.LogError("Voxel has 0 textures at " + i);
                            return;
                        }
                        for (int j = 0; j < textureLinks.children.Length; j++)
                        {
                            var textureEntity = textureLinks.children[j];
                            if (!textures2.HasComponent(textureEntity))
                            {
                                //UnityEngine.Debug.LogError("Still building texture.");
                                return;
                            }
                        }
                    }
                }
                PostUpdateCommands.RemoveComponent<GenerateTilemap>(entityInQueryIndex, e);
                var voxels = new NativeList<Entity>();
                for (int i = 0; i < voxelLinks.voxels.Length; i++)
                {
                    var voxelEntity = voxelLinks.voxels[i];
                    if (!HasComponent<MinivoxVoxel>(voxelEntity))
                    {
                        voxels.Add(voxelEntity);
                    }
                }
                int texturesCount = 0;
                var textureResolution = tilemap.resolution;
                var materialEntity = materialLink.material;
                // Get Textures
                var textures = new NativeList<Texture>();
                // for all voxels in group, add their textures to list
                for (int i = 0; i < voxels.Length; i++)
                {
                    var voxelEntity = voxels[i];
                    var materialEntity2 = materialLinks[voxelEntity].material;
                    if (materialEntity2 != materialEntity)
                    {
                        continue;
                    }
                    var textureLinks = childrens[voxelEntity];
                    for (int j = 0; j < textureLinks.children.Length; j++)
                    {
                        var textureEntity = textureLinks.children[j];
                        var voxelTextureResolution = voxelTextureResolutions[textureEntity];
                        if (voxelTextureResolution.resolution == textureResolution)
                        {
                            textures.Add(textures2[textureEntity]);
                        }
                    }
                }
                var largestSize = new float2();
                for (int i = 0; i < textures.Length; i++)
                {
                    var newTexture = textures[i];
                    var newSize = new float2(newTexture.size.x, newTexture.size.y);
                    if (newSize.x > largestSize.x)
                    {
                        largestSize.x = newSize.x;
                    }
                    if (newSize.y > largestSize.y)
                    {
                        largestSize.y = newSize.y;
                    }
                }
                texturesCount = textures.Length;
                // Generate Texture Tilemap
                // find next power of 2
                tilemap.gridSize = 0;
                int squared = 1;
                while (squared < texturesCount)
                {
                    tilemap.gridSize++;
                    squared = tilemap.gridSize * tilemap.gridSize;
                }
                if (texturesCount == 1)
                {
                    tilemap.gridSize = 1;
                }
                // for all textures set horizontalCount, verticalCount
                CreateVoxelTilemap(ref texture, in textures, tilemap.gridSize, (int) largestSize.x);
                var textureWidth = 1 / (float)(tilemap.gridSize);
                var textureHeight = 1 / (float)(tilemap.gridSize);
                // only continue if to set UVs
                if (tilemap.resolutionIndex != 0 || HasComponent<DisableRegnerateVoxelUVs>(e))
                {
                    PostUpdateCommands.RemoveComponent<DisableRegnerateVoxelUVs>(entityInQueryIndex, e);
                    voxels.Dispose();
                    textures.Dispose();
                    return;
                }
                // Set Voxel UVs
                var texturePositions = new NativeArray<float2>(texturesCount, Allocator.Temp);
                for (int i = 0; i < texturesCount; i++)
                {
                    int positionX = i % tilemap.gridSize;
                    int positionY = i / tilemap.gridSize;
                    var position = new float2(((float)positionX) * textureWidth, ((float)positionY) * textureHeight);
                    texturePositions[i] = (position);
                }
                var textureSize = new float2(textureWidth, textureHeight);
                int textureCount = 0;
                for (int i = 0; i < voxels.Length; i++)
                {
                    var voxelEntity = voxels[i];
                    var materialEntity2 = materialLinks[voxelEntity].material;
                    if (materialEntity2 != materialEntity)
                    {
                        continue;
                    }
                    var uvMap = voxelUVMaps[voxelEntity];
                    uvMap.Initialize();
                    // multi sides
                    var voxelTextureTypes = voxelTextureTypes2[voxelEntity];
                    var textureLinks = childrens[voxelEntity];
                    var doneSides = new NativeList<TextureVoxelSide>();
                    var textureIndex = 0;
                    for (int j = 0; j < textureLinks.children.Length; j++)
                    {
                        var textureEntity = textureLinks.children[j];
                        var voxelTextureResolution = voxelTextureResolutions[textureEntity];
                        if (voxelTextureResolution.resolution != textureResolution)
                        {
                            continue;
                        }
                        var position = texturePositions[textureCount];
                        var voxelTextureType = (TextureVoxelSide) voxelTextureTypes.textureTypes[textureIndex];
                        textureIndex++;
                        var isNormalSide = voxelTextureType == TextureVoxelSide.Up || voxelTextureType == TextureVoxelSide.Down
                            || voxelTextureType == TextureVoxelSide.Left || voxelTextureType == TextureVoxelSide.Right 
                            || voxelTextureType == TextureVoxelSide.Forward || voxelTextureType == TextureVoxelSide.Back;
                        if (isNormalSide)
                        {
                            uvMap.SetSide(textureSize, position, voxelTextureType);
                            doneSides.Add(voxelTextureType);
                            textureCount++;
                        }
                        else if (voxelTextureType == TextureVoxelSide.UpDown)
                        {
                            uvMap.SetSide(textureSize, position, TextureVoxelSide.Up);
                            doneSides.Add(TextureVoxelSide.Up);
                            uvMap.SetSide(textureSize, position, TextureVoxelSide.Down);
                            doneSides.Add(TextureVoxelSide.Down);
                            textureCount++;
                        }
                    }
                    // do default after all sides
                    textureIndex = 0;
                    for (int j = 0; j < textureLinks.children.Length; j++)
                    {
                        var textureEntity = textureLinks.children[j];
                        var voxelTextureResolution = voxelTextureResolutions[textureEntity];
                        if (voxelTextureResolution.resolution != textureResolution)
                        {
                            continue;
                        }
                        var position = texturePositions[textureCount];
                        if (voxelTextureTypes.textureTypes[textureIndex] == (byte) TextureVoxelSide.Default)
                        {
                            for (int k = 1; k <= 6; k++)
                            {
                                var sideAdding = ((TextureVoxelSide) k);
                                // does contain
                                var doesContain = false;
                                for (int a = 0; a < doneSides.Length; a++)
                                {
                                    if (doneSides[a] == sideAdding)
                                    {
                                        doesContain = true;
                                        break;
                                    }
                                }
                                if (!doesContain)
                                {
                                    uvMap.SetSide(textureSize, position, sideAdding);
                                }
                            }
                            textureCount++;
                        }
                        textureIndex++;
                    }
                    PostUpdateCommands.SetComponent(entityInQueryIndex, voxelEntity, uvMap);
                    doneSides.Dispose();
                }
                voxels.Dispose();
                textures.Dispose();
            })  .WithReadOnly(voxelLinks2)
                .WithReadOnly(voxelUVMaps)
				.WithReadOnly(materialLinks)
				.WithReadOnly(childrens)
				.WithReadOnly(voxelTextureTypes2)
                .WithReadOnly(textures2)
                .WithNativeDisableContainerSafetyRestriction(textures2)
                .WithReadOnly(voxelTextureResolutions)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
        
        public static void CreateVoxelTilemap(ref Texture tilmapTexture, in NativeList<Texture> textures, int gridSize, int textureSize = 16, bool hasAlpha = false)
        {
            if (textures.Length == 0)
            {
                //Debug.LogError("tiledTextures count is 0. Cannot create a tile planet.");
                return;
            }
            var tilemapWidth = textureSize * gridSize;
            var tilemapHeight = textureSize * gridSize;
            if (tilemapWidth == 0 || tilemapHeight == 0)
            {
                // UnityEngine.Debug.LogError("Tilemap size.x or size.y is 0.");
                return;
            }
            tilmapTexture.Initialize(new int2(tilemapWidth, tilemapHeight));
            var tileIndex = -1;                     // Our real index !
            var maxTiles = gridSize * gridSize;
            for (int i = 0; i < textures.Length; i++)
            {
                tileIndex++;
                var texture = textures[i];
                if (i < maxTiles && texture.size.x != 0 && texture.size.y != 0)
                {
                    var positionX = (tileIndex / gridSize);
                    var positionY = (tileIndex % gridSize);
                    for (int j = 0; j < textureSize; j++)
                    {
                        for (int k = 0; k < textureSize; k++)
                        {
                            var j2 = j + positionX * (textureSize);
                            var k2 = k + positionY * (textureSize);
                            var tileColorIndex = (int) math.floor(j * textureSize + k);
                            if (tileColorIndex < texture.colors.Length)
                            {
                                var color = texture.colors[tileColorIndex];
                                var tilemapColorIndex = (int) math.floor(j2 * gridSize * textureSize + k2);
                                tilmapTexture.colors[tilemapColorIndex] = color;
                            }
                            /*else
                            {
                                UnityEngine.Debug.LogError("Tilemap Index out of bounds for pixels.");
                            }*/
                        }
                    }
                }
            }
        }
    }
}

//voxelTilemap.SetPixels32(colors, 0);
/*if (hasAlpha)
{
    voxelTilemap.alphaIsTransparency = true;
}*/