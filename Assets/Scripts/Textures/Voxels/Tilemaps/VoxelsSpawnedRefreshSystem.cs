using Unity.Entities;
using Unity.Burst;
using Zoxel.Voxels;

namespace Zoxel.Textures.Tilemaps
{
    //! When voxels are linked, spawn tilemaps aggain from realm.
    /**
    *   - Event System -
    */
    [BurstCompile, UpdateInGroup(typeof(TilemapSystemGroup))]
    public partial class VoxelsSpawnedRefreshSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities.ForEach((int entityInQueryIndex, in OnVoxelsSpawned onVoxelsSpawned) =>
            {
                PostUpdateCommands.AddComponent<SpawnTilemaps>(entityInQueryIndex, onVoxelsSpawned.realm);
            }).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}