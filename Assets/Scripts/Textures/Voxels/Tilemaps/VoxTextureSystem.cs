using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Voxels;
using Unity.Burst.Intrinsics;

namespace Zoxel.Textures.Voxels
{
    //! Generates textures with a Vox entity input.
    /**
    *   - Texture Generation System -
    *   Added black outline to it. Added noise as well.
    */
    [BurstCompile, UpdateInGroup(typeof(TextureSystemGroup))]
    public partial class VoxTextureSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery processQuery;
		private EntityQuery voxesQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        	processQuery = GetEntityQuery(
                ComponentType.ReadWrite<GenerateTextureVox>(),
                ComponentType.ReadWrite<Texture>(),
                ComponentType.Exclude<TextureDirty>());
        	voxesQuery = GetEntityQuery(
                ComponentType.ReadOnly<Chunk>(),
                ComponentType.ReadOnly<VoxColors>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var noiseAddition = new int2(26, 26);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var voxEntities = voxesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var voxDatas = GetComponentLookup<Chunk>(true);
            var voxColors = GetComponentLookup<VoxColors>(true);
            var zoxIDs = GetComponentLookup<ZoxID>(true);
            voxEntities.Dispose();
            NativeArray<int> chunkBaseEntityIndices = processQuery.CalculateBaseEntityIndexArrayAsync(World.UpdateAllocator.ToAllocator, Dependency, out JobHandle baseIndexJobHandle);
            var job = new VoxTextureJob
            {
                ChunkBaseEntityIndices = chunkBaseEntityIndices,
                PostUpdateCommands = PostUpdateCommands,
                entityHandle = GetEntityTypeHandle(),
                generateTextureVoxHandle = GetComponentTypeHandle<GenerateTextureVox>(false),
                textureHandle = GetComponentTypeHandle<Texture>(false),
                voxDatas = voxDatas,
                voxColors = voxColors,
                zoxIDs = zoxIDs,
                noiseAddition = noiseAddition
            };
            Dependency = job.ScheduleParallel(processQuery, Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        [BurstCompile]
        struct VoxTextureJob : IJobChunk
        {
            [ReadOnly] public EntityTypeHandle entityHandle;
            public ComponentTypeHandle<GenerateTextureVox> generateTextureVoxHandle;
            public ComponentTypeHandle<Texture> textureHandle;
            [ReadOnly] public ComponentLookup<Chunk> voxDatas;
            [ReadOnly] public ComponentLookup<VoxColors> voxColors;
            [ReadOnly] public ComponentLookup<ZoxID> zoxIDs;
            public EntityCommandBuffer.ParallelWriter PostUpdateCommands;
            [ReadOnly] public int2 noiseAddition;

            [ReadOnly] [DeallocateOnJobCompletion] public NativeArray<int> ChunkBaseEntityIndices;

            // public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
            public void Execute(in ArchetypeChunk chunk, int unfilteredChunkIndex, bool useEnabledMask, in v128 chunkEnabledMask)
            {
                int firstEntityIndex = ChunkBaseEntityIndices[unfilteredChunkIndex];
                var entities = chunk.GetNativeArray(entityHandle);
                var generateTextureVoxes = chunk.GetNativeArray(generateTextureVoxHandle);
                var textures = chunk.GetNativeArray(textureHandle);
                for (var i = 0; i < chunk.Count; i++)
                {
                    var generateTextureVox = generateTextureVoxes[i];
                    var texture = textures[i];
                    Process(entities[i], i + firstEntityIndex, ref texture, ref generateTextureVox);
                    textures[i] = texture;
                    generateTextureVoxes[i] = generateTextureVox;
                }
            }

            private void Process(Entity e, int entityInQueryIndex, ref Texture texture, ref GenerateTextureVox generateTextureVox)
            {
                if (generateTextureVox.delay != 0 && generateTextureVox.count < generateTextureVox.delay)
                {
                    generateTextureVox.count++;
                }
                else if (generateTextureVox.count == generateTextureVox.delay)
                {
                    if (generateTextureVox.resolution == 0)
                    {
                        return;
                    }
                    PostUpdateCommands.RemoveComponent<GenerateTextureVox>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<TextureDirty>(entityInQueryIndex, e);
                    texture.Initialize(new int2(generateTextureVox.resolution, generateTextureVox.resolution));
                    var voxData = voxDatas[generateTextureVox.vox];
                    var voxColors2 = voxColors[generateTextureVox.vox];
                    var zoxID = zoxIDs[generateTextureVox.vox].id;
                    GenerateVoxTexture(ref texture, in voxData, in voxColors2, zoxID);
                    // UnityEngine.Debug.LogError("Generated Vox Texture: " + generateTextureVox.vox.Index);
                }
            }

            public void GenerateVoxTexture(ref Texture texture, in Chunk voxData, in VoxColors voxColors, int zoxID)
            {
                var size = voxData.voxelDimensions;
                //UnityEngine.Debug.LogError("voxData.colorsR: " + voxData.colorsR.Length);
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        texture.colors[pixelIndex] = new Color(0, 0, 0, 0);
                    }
                }
                var maxSize = (float) math.max(size.x, size.y);
                int differenceX = ((int)(maxSize - size.x));
                int differenceY = ((int)(maxSize - size.y));
                differenceX = differenceX / 2 + differenceX % 2;
                differenceY = differenceY / 2 + differenceY % 2;
                if (size.x != maxSize && size.x % 2 != 0)
                {
                    differenceX++;
                }
                if (size.y != maxSize && size.y % 2 != 0)
                {
                    differenceY++;
                }
                var ratioDifference = new float2(maxSize / (float)texture.size.x, maxSize / (float)texture.size.y);
                differenceX = ((int) (differenceX / ratioDifference.x));
                differenceY = ((int) (differenceY / ratioDifference.y));
                //UnityEngine.Debug.LogError("ratioDifference: " + ratioDifference + ", size.x: " + size.x + ", size.y: " + size.y + 
                //    ", differenceX: " + differenceX + ", differenceY: " + differenceY);
                //UnityEngine.Debug.LogError("Difference: " + differenceX + ":" + differenceY + "   : size: " + size);
                int2 position2;
                for (int i = 0; i < texture.size.x; i++)
                {
                    for (int j = 0; j < texture.size.y; j++)
                    {
                        //var pixelIndex = i + differenceX + j + differenceY * texture.size.x;
                        var pixelIndex = (int)((float)i + differenceX) + (int)((float)j + differenceY) * texture.size.x;
                        if (pixelIndex < 0 || pixelIndex >= texture.colors.Length)
                        {
                            continue;
                        }
                        position2.x = (int)((i) * ratioDifference.x);
                        position2.y = (int)((j) * ratioDifference.y);
                        if (position2.x < size.x && position2.y < size.y)
                        {
                            for (int k = 0; k < size.z; k++)
                            {
                                var index = VoxelUtilities.GetVoxelArrayIndex(new int3(position2.x, position2.y, k), size);
                                if (index >= 0 && index < voxData.voxels.Length)
                                {
                                    int voxIndex = voxData.voxels[index] - 1;    // minus 1 for air
                                    if (voxIndex >= 0 && voxIndex < voxColors.colors.Length)
                                    {
                                        texture.colors[pixelIndex] = new Color(voxColors.colors[voxIndex]);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                var outlineColor = new Color(3, 3, 3, 255);
                AddOutline(ref texture, outlineColor, 1);
                AddNoise(ref texture, outlineColor, zoxID, noiseAddition);
            }

            private void AddNoise(ref Texture texture, Color outlineColor, int seed, int2 noiseAddition)
            {
                var random = new Random();
                if (seed == 0)
                {
                    seed = 666;
                }
                random.InitState((uint) seed);
                int2 position;
                for (position.x = 0; position.x < texture.size.x; position.x++)
                {
                    for (position.y = 0; position.y < texture.size.y; position.y++)
                    {
                        var index = position.GetArrayIndex(texture.size);
                        var thisColour = texture.colors[index];
                        if (thisColour.alpha != 0 && thisColour != outlineColor)
                        {
                            thisColour.red = (byte)(math.min(255, (int) thisColour.red + random.NextInt(-noiseAddition.x, noiseAddition.y)));
                            thisColour.green = (byte)(math.min(255, (int) thisColour.green + random.NextInt(-noiseAddition.x, noiseAddition.y)));
                            thisColour.blue = (byte)(math.min(255, (int) thisColour.blue + random.NextInt(-noiseAddition.x, noiseAddition.y)));
                            texture.colors[index] = thisColour;
                        }
                    }
                }
            }

            private bool IsFillColor(in Color color)
            {
                return !(color.alpha == 0); // color != outlineColor && 
            }

            //! Adds an outline onto text
            public void AddOutline(ref Texture texture, Color outlineColor, float outlineThickness)
            {
                // first grab outline positions
                // var fillColor = generateFontTexture.fillColor;
                var outlineThicknessByte = (byte)((int) outlineThickness);
                var lengths = new NativeArray<byte>(texture.colors.Length, Allocator.Temp);
                for (int i = 0; i < lengths.Length; i++)
                {
                    lengths[i] = 255;
                }
                for (int i = 0; i < texture.size.x; i++)
                {
                    for (int j = 0; j < texture.size.y; j++)
                    {
                        var hasClearColorNextTo = false;
                        var position = new int2(i, j);
                        var thisColour = texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position, texture.size)];
                        if (i == 0 || j == 0 || i == texture.size.x - 1 || j == texture.size.y - 1)
                        {
                            if (IsFillColor(thisColour)) // , outlineColor)  == fillColor)
                            {
                                hasClearColorNextTo = true;
                            }
                        }
                        else if (IsFillColor(thisColour))
                        {
                            hasClearColorNextTo = !IsFillColor(texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Up(), texture.size)])
                                || !IsFillColor(texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Down(), texture.size)])
                                || !IsFillColor(texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Left(), texture.size)])
                                || !IsFillColor(texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Right(), texture.size)]);
                        }
                        if (hasClearColorNextTo)
                        {
                            FloodFillOutline(ref lengths, in texture, position, outlineThicknessByte, 0);
                        }
                    }
                }
                // Color our outline positions
                for (int i = 0; i < lengths.Length; i++)
                {
                    if (lengths[i] != 255)
                    {
                        var position = new int2(i % texture.size.x, i / texture.size.x);
                        if (position.y == texture.size.y - 1)
                        {
                            texture.colors[i] = outlineColor;
                        }
                        else if (!IsFillColor(texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Up(), texture.size)]))
                        {
                            texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Up(), texture.size)] = outlineColor;
                        }
                        if (position.y == 0)
                        {
                            texture.colors[i] = outlineColor;
                        }
                        else if (!IsFillColor(texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Down(), texture.size)]))
                        {
                            texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Down(), texture.size)] = outlineColor;
                        }

                        if (position.x == texture.size.x - 1)
                        {
                            texture.colors[i] = outlineColor;
                        }
                        else if (!IsFillColor(texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Right(), texture.size)]))
                        {
                            texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Right(), texture.size)] = outlineColor;
                        }
                        if (position.x == 0)
                        {
                            texture.colors[i] = outlineColor;
                        }
                        else if (!IsFillColor(texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Left(), texture.size)]))
                        {
                            texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Left(), texture.size)] = outlineColor;
                        }
                        // texture.colors[i] = outlineColor;
                    }
                }
                lengths.Dispose();
            }

            //! Remember distance per point, and keep the lowest one, so it doesn't repeat steps.
            private void FloodFillOutline(ref NativeArray<byte> lengths, in Texture texture, int2 position, byte maxLength, byte length = 0)
            {
                if (position.x >= 0 && position.x < texture.size.x && position.y >= 0 && position.y < texture.size.y)
                {
                    var arrayIndex = VoxelUtilities.GetTextureArrayIndexXY(position, texture.size);
                    var thisColor = texture.colors[arrayIndex];
                    if (length == 0 || IsFillColor(thisColor))
                    {
                        var oldLength = lengths[arrayIndex];
                        if (oldLength < length + 1)
                        {
                            return;
                        }
                        lengths[arrayIndex] = (byte) (length + 1);
                        if (length < maxLength)
                        {
                            length++;
                            FloodFillOutline(ref lengths, in texture, position.Up(), maxLength, length);
                            FloodFillOutline(ref lengths, in texture, position.Down(), maxLength, length);
                            FloodFillOutline(ref lengths, in texture, position.Left(), maxLength, length);
                            FloodFillOutline(ref lengths, in texture, position.Right(), maxLength, length);
                        }
                    }
                }
            }
        }
    }
}