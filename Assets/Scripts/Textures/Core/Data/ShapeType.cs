namespace Zoxel.Textures
{
    public static class ShapeType
    {
        public const byte None = 0;
        public const byte Square = 1;
        public const byte Triangle = 2;
        public const byte Circle = 3;
        public const byte Diamond = 4;
    }
}