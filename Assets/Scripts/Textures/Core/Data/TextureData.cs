using Unity.Entities;
using System;

namespace Zoxel.Textures
{
    [Serializable]
    public struct TextureData
    {
        public int id;
        public int width;
        public int height;
        public Color[] colors;

        public void Initialize()
        {
            int pixelsCount = width * height;
        }
    }
}