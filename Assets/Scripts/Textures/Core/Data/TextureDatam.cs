﻿using UnityEngine;

namespace Zoxel.Textures
{
    //! Used for authored textures.
    [CreateAssetMenu(fileName = "Texture", menuName = "ZoxelArt/Texture")]
    public partial class TextureDatam : ScriptableObject
    {
        // public TextureData data;
        public Texture2D texture;
    }
}