using Unity.Mathematics;

namespace Zoxel.Textures
{
    public struct RandomData
    { 
        public Random random;
        public Random random2;
        public Random random3;
        
        public RandomData(uint seed)
        {
            var ran = new Random();
            ran.InitState(seed);
            random = new Random();
            random2 = new Random();
            random3 = new Random();
            random.InitState((uint)ran.NextInt(1, 255 * 255));
            random2.InitState((uint)ran.NextInt(1, 255 * 255));
            random3.InitState((uint)ran.NextInt(1, 255 * 255));
        }
    }
}