using Unity.Mathematics;

namespace Zoxel.Textures
{
    public static class TextureUtilities
    {
        public static UnityEngine.Texture2D CreateEmptyIconTexture()
        {
            var texture = new UnityEngine.Texture2D(16, 16, UnityEngine.Experimental.Rendering.DefaultFormat.LDR,
                UnityEngine.Experimental.Rendering.TextureCreationFlags.None);
            texture.filterMode = UnityEngine.FilterMode.Point;
            var pixels = new UnityEngine.Color[texture.width * texture.height];
            var middlePosition = new float2(texture.width / 2, texture.height / 2);
            for (int i = 0; i < texture.width; i++)
            {
                for (int j = 0; j < texture.height; j++)
                {
                    var xzIndex = VoxelUtilities.GetTextureArrayIndexXY(new int3(i, j, 0), new int3(texture.width, texture.height, 0));
                    var distance = math.distance(middlePosition, new float2(i, j));
                    if (distance <= texture.width / 4)
                    {
                        pixels[xzIndex] = new UnityEngine.Color(0.1f, 0.6f, 0.2f, 1f);
                    }
                    else
                    {
                        pixels[xzIndex] = new UnityEngine.Color(0, 0, 0, 0);
                    }
                }
            }
            texture.SetPixels(pixels);
            texture.Apply();
            return texture;
        }

        public static UnityEngine.Texture2D CreateArrowTexture(UnityEngine.Color colorA, UnityEngine.Color colorB)
        {
            var texture = new UnityEngine.Texture2D(32, 32, UnityEngine.Experimental.Rendering.DefaultFormat.LDR,
                UnityEngine.Experimental.Rendering.TextureCreationFlags.None);
            texture.filterMode = UnityEngine.FilterMode.Point;
            var pixels = new UnityEngine.Color[texture.width * texture.height];
            var xzIndex = 0;
            var halfHeight = texture.height / 2;
            var halfWidth = texture.width / 2;
            for (int i = 0; i < texture.width; i++)
            {
                var arrowHeadWidth = math.abs((texture.width) / 2 - i);
                for (int j = 0; j < texture.height; j++)
                {
                    if (j <= halfHeight)
                    {
                        if (i >= halfWidth - 1 && i <= halfWidth + 1)
                        {
                            pixels[xzIndex] = colorA; // new UnityEngine.Color(0.8f, 0.1f, 0, 1);
                        }
                        else
                        {
                            pixels[xzIndex] = new UnityEngine.Color(0, 0, 0, 0);
                        }
                    }
                    else
                    {
                        if (i >= (halfHeight - (texture.height - j)) && i <= (halfHeight + (texture.height - j)))
                        {
                            pixels[xzIndex] = colorB; // new UnityEngine.Color(0.6f, 0, 0, 1);
                        }
                        else
                        {
                            pixels[xzIndex] = new UnityEngine.Color(0, 0, 0, 0);
                        }
                    }
                    xzIndex++;
                }
            }
            texture.SetPixels(pixels);
            texture.Apply();
            return texture;
        }
    }
}