using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Textures
{
    //! Updates the Texture size and resizes the buffer.
    [BurstCompile, UpdateInGroup(typeof(TextureSystemGroup))]
    public partial class TextureResolutionSetSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref Texture texture, in ResizeTexture resizeTexture) =>
            {
                PostUpdateCommands.RemoveComponent<ResizeTexture>(entityInQueryIndex, e);
                if (texture.size.x != resizeTexture.textureResolution || texture.size.y != resizeTexture.textureResolution)
                {
                    texture.Initialize(new int2(resizeTexture.textureResolution, resizeTexture.textureResolution));
                    PostUpdateCommands.AddComponent<GenerateTextureVoxel>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}