using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Textures
{
	//! Disposes of texture on DestroyEntity.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class TextureDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
				.WithAll<DestroyEntity, Texture>()
				.ForEach((in Texture texture) =>
			{
				texture.DisposeFinal();
			}).ScheduleParallel();
		}
	}
}