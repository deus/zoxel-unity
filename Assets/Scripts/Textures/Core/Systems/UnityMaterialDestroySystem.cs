using Unity.Entities;

namespace Zoxel.Textures
{
	//! Cleans up EntityMaterials for Planet's.
    /**
    *   - Destroy System -
    */
    [UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class UnityMaterialDestroySystem : SystemBase
	{
		protected override void OnUpdate()
		{
			Entities
				.WithAll<DestroyEntity, UnityMaterial>()
				.ForEach((in UnityMaterial unityMaterial) =>
			{
                if (unityMaterial.material)
                {
                    ObjectUtil.Destroy(unityMaterial.material);
                }
			}).WithoutBurst().Run();
		}
	}
}