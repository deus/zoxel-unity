using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Textures
{
	//! Disposes of texture on DestroyEntity.
    /**
    *   - Destroy System -
    */
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class UnityTextureDestroySystem : SystemBase
	{
		protected override void OnUpdate()
		{
			Entities
				.WithAll<DestroyEntity, UnityTexture>()
				.ForEach((in UnityTexture unityTexture) =>
			{
				ObjectUtil.Destroy(unityTexture.texture);
			}).WithoutBurst().Run();
		}
	}
}