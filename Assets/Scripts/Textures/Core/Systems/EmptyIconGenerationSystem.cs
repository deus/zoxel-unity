using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;

namespace Zoxel.Textures
{
    //! Generates an empty icon for our ui.
    /**
    *   - Texture Generation System -
    */
    [BurstCompile, UpdateInGroup(typeof(TextureSystemGroup))]
    public partial class EmptyIconGenerationSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var edgeSize = 1;
            var fillColor = new Color(48, 39, 29, 185); // new Color((int)(255 * 0.1f), (int)(255 * 0.6f), (int)(255 * 0.2f), 255);
            var fillColor2 =  new Color(40, 38, 35, 88); //new Color(255, 255, 255, 0);
            var clearColor =  new Color(0, 0, 0, 180);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<TextureDirty>()
                .ForEach((Entity e, int entityInQueryIndex, ref Texture texture, ref GenerateEmptyIcon generateEmptyIcon) =>
            {
                if (generateEmptyIcon.delay != 0 && generateEmptyIcon.count < generateEmptyIcon.delay)
                {
                    generateEmptyIcon.count++;
                    return;
                }
                PostUpdateCommands.RemoveComponent<GenerateEmptyIcon>(entityInQueryIndex, e);
                // generate texture
                // add outline - need an outline system for this
                // var size = new int3(texture.size.x, texture.size.y, 0);
                texture.Initialize(new int2(generateEmptyIcon.resolution, generateEmptyIcon.resolution));
                var middlePosition = new int2(texture.size.x / 2, texture.size.y / 2);
                var circleRadius = texture.size.x / 4;
                int2 position;
                for (position.x = 0; position.x < texture.size.x; position.x++)
                {
                    for (position.y = 0; position.y < texture.size.y; position.y++)
                    {
                        var pixelIndex = position.y * texture.size.x + position.x;
                        var distance = math.distance(middlePosition.ToFloat2(), position.ToFloat2());
                        if (distance <= circleRadius)
                        {
                            texture.colors[pixelIndex] = fillColor;
                        }
                        else if (position.x >= edgeSize && position.x < texture.size.x - edgeSize
                            && position.y >= edgeSize && position.y < texture.size.y - edgeSize)
                        {
                            texture.colors[pixelIndex] = fillColor2;
                        }
                        else
                        {
                            texture.colors[pixelIndex] = clearColor;
                        }
                    }
                }
                PostUpdateCommands.AddComponent<TextureDirty>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}