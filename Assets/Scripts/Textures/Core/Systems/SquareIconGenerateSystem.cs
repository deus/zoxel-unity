using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;

namespace Zoxel.Textures
{
    //! Generates an empty icon for our ui.
    [BurstCompile, UpdateInGroup(typeof(TextureSystemGroup))]
    public partial class SquareIconGenerateSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var seed = (uint) IDUtil.GenerateUniqueID();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<TextureDirty>()
                .ForEach((Entity e, int entityInQueryIndex, ref Texture texture, in GenerateSquareIcon generateSquareIcon) =>
            {
                var random = new Random();
                random.InitState((uint) (entityInQueryIndex * 666 + seed));
                // generate texture
                texture.Initialize(new int2(generateSquareIcon.resolution, generateSquareIcon.resolution));
                var fillColor = new Color((int)(255 * 0.1f), (int)(255 * 0.6f), (int)(255 * 0.2f), 255);
                fillColor.red = (byte) random.NextInt(0, 255);
                fillColor.green = (byte) random.NextInt(0, 255);
                fillColor.blue = (byte) random.NextInt(0, 255);
                var clearColor = new Color(255, 255, 255, 0);
                // add outline - need an outline system for this
                var middlePosition = new int2(texture.size.x / 2, texture.size.y / 2);
                var position = new int2();
                var squareSize = (int)(texture.size.x * (1 / 3f)) - random.NextInt(0, 4);
                var squareSizeAdd = texture.size.x / 6;
                for (position.x = 0; position.x < texture.size.x; position.x++)
                {
                    for (position.y = 0; position.y < texture.size.y; position.y++)
                    {
                        var pixelIndex = position.y * texture.size.x + position.x;
                        var topPosition = middlePosition.y + squareSize - random.NextInt(0, squareSizeAdd);
                        var bottomPosition = middlePosition.y - squareSize + random.NextInt(0, squareSizeAdd);
                        var rightPosition = middlePosition.x + squareSize - random.NextInt(0, squareSizeAdd);
                        var leftPosition = middlePosition.x - squareSize + random.NextInt(0, squareSizeAdd);
                        if (position.x >= leftPosition && position.x <= rightPosition
                            && position.y >= bottomPosition && position.y <= topPosition)
                        {
                            texture.colors[pixelIndex] = fillColor;
                        }
                        else
                        {
                            texture.colors[pixelIndex] = clearColor;
                        }
                    }
                }
                PostUpdateCommands.RemoveComponent<GenerateSquareIcon>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<TextureDirty>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}