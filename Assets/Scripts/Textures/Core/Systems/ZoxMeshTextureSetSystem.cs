using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.Textures
{
    //! Sets a ZoxMesh material texture from an entity texture.
    [UpdateInGroup(typeof(TextureSystemGroup))]
    public partial class ZoxMeshTextureSetSystem : SystemBase
    {
        const string textureName = "_BaseMap";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities.ForEach((Entity e, ZoxMesh zoxMesh, in SetZoxMeshTexture setZoxMeshTexture) =>
            {
                PostUpdateCommands.RemoveComponent<SetZoxMeshTexture>(e);
                var iconTexture = EntityManager.GetComponentData<Texture>(setZoxMeshTexture.textureEntity).GetTexture();
                if (iconTexture != null)
                {
                    var previousTexture = zoxMesh.material.GetTexture(textureName);
                    if (previousTexture != null)
                    {
                        ObjectUtil.Destroy(previousTexture);
                    }
                    zoxMesh.material.SetTexture(textureName, iconTexture);
                    PostUpdateCommands.SetSharedComponentManaged(e, zoxMesh);
                }
            }).WithoutBurst().Run();
        }
    }
}