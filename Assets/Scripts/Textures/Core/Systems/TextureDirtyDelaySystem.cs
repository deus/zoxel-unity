using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Textures
{
    //! Delays texture updates by x frames.
    [BurstCompile, UpdateInGroup(typeof(TextureSystemGroup))]
    public partial class TextureDirtyDelaySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<InitializeEntity, DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref TextureDirtyDelayed textureDirtyDelayed) =>
            {
                if (textureDirtyDelayed.delay != 0 && textureDirtyDelayed.count < textureDirtyDelayed.delay)
                {
                    textureDirtyDelayed.count++;
                    return;
                }
                PostUpdateCommands.RemoveComponent<TextureDirtyDelayed>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}