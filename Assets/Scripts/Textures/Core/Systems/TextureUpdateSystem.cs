using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.Textures
{
    //! Updates textures to material when they are done processing in our job systems.
    [UpdateInGroup(typeof(TextureSystemGroup))]
    public partial class TextureUpdateSystem : SystemBase
    {
        private const int textureUpdateLimit = 32; // 10;
        private const string textureName = "_BaseMap";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        protected override void OnUpdate()
        {
            var textureManager = TextureDebugger.instance;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            // PostUpdateCommands.RemoveComponentForEntityQuery<TextureDirty>(processQuery);
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<InitializeEntity, DestroyEntity, TextureDirtyDelayed>()
                .WithAll<TextureDirty>()
                .ForEach((Entity e, int entityInQueryIndex, in ZoxMesh zoxMesh, in Texture texture) =>
            {
                if (textureUpdateLimit != 0 && entityInQueryIndex > textureUpdateLimit)
                {
                    return; // max 10 at a time
                }
                var material = zoxMesh.material;
                if (material == null)
                {
                    // Debug which materials are null!
                    return;
                }
                PostUpdateCommands.RemoveComponent<TextureDirty>(e);
                UnityEngine.Texture2D unityTexture = null;
                if (!HasComponent<DontDestroyTexture>(e))
                {
                    unityTexture = material.GetTexture(textureName) as UnityEngine.Texture2D;
                }
                if (unityTexture == null)
                {
                    unityTexture = texture.CreateEmptyTexture();
                }
                unityTexture = texture.SetTexture(ref unityTexture);
                material.SetTexture(textureName, unityTexture);
                if (textureManager)
                {
                    textureManager.AddMaterial(material);
                    textureManager.AddTexture(unityTexture);
                }
            }).WithoutBurst().Run();
        }
    }
}