using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Textures
{
    //! Generates an empty icon for our ui.
    /**
    *   - Texture Generation System -
    *   Just a simple system that will create a frame texture onto a UI element.
    *   \todo Implement Rounding of Frame Textures
    *   \todo For shader, take in a black and white planet that is used only for knowing what is frame and what isn't
    *
    *   Generate bars on edge of frame, dark, light, dark, that gives thickness to frame ui
    *       - for each dark place, grow based on chance into the light pixels
    *       - Each ui will look unique therefor
    */
    [BurstCompile, UpdateInGroup(typeof(TextureSystemGroup))]
    public partial class TextureFrameGenerationSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var seed = (uint) IDUtil.GenerateUniqueID();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<TextureDirty>()
                .WithAll<GenerateTextureFrame>()
                .ForEach((Entity e, int entityInQueryIndex, ref Texture texture, in Size2D size2D, in TextureFrame textureFrame) =>
            {
                PostUpdateCommands.RemoveComponent<GenerateTextureFrame>(entityInQueryIndex, e);
                if (size2D.size.x == 0 || size2D.size.y == 0)
                {
                    return;
                }
                var ratio = size2D.size.x / size2D.size.y;
                var resolution = textureFrame.resolution;   // 32
                texture.Initialize(new int2((int) (resolution * ratio), resolution));
                var frameColor = textureFrame.frameColor;
                var fillColor = textureFrame.fillColor;
                var frameThickness = textureFrame.frameThickness;
                var textureSize = new int2(texture.size.x, texture.size.y);
                var position = new int2();
                for (position.x = 0; position.x < texture.size.x; position.x++)
                {
                    for (position.y = 0; position.y < texture.size.y; position.y++)
                    {
                        var xzIndex = VoxelUtilities.GetTextureArrayIndexXY(position, textureSize);
                        if (position.x <= frameThickness || position.x >= texture.size.x - frameThickness - 1 
                            || position.y <= frameThickness || position.y >= texture.size.y - frameThickness - 1)
                        {
                            texture.colors[xzIndex] = frameColor;
                        }
                        else
                        {
                            texture.colors[xzIndex] = fillColor;
                            //! \todo Add noise on frame textures.
                        }
                    }
                }
                PostUpdateCommands.AddComponent<TextureDirty>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}