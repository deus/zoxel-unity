using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;

namespace Zoxel.Textures
{
    //! Generates the crosshair texture.
    /**
    *   - Texture Generation System -
    */
    [BurstCompile, UpdateInGroup(typeof(TextureSystemGroup))]
    public partial class CrosshairTextureGenerationSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        
        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var crosshairResolution = 32;
            var crosshairRadius = 1;
            var fillColor = new Color(255, 255, 255);
            var clearColor = new Color(255, 255, 255, 0);
            var outlineColor = new Color(0, 0, 0); // generateFontTexture.outlineColor; //new Color(0, 0, 0, 255);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<TextureDirty, InitializeEntity>()
                .WithAll<GenerateCrosshairUI>()
                .ForEach((Entity e, int entityInQueryIndex, ref Texture texture) =>
            {
                PostUpdateCommands.RemoveComponent<GenerateCrosshairUI>(entityInQueryIndex, e);
                texture.Initialize(new int2(crosshairResolution, crosshairResolution));
                var size = new int3(texture.size.x, texture.size.y, 0);
                for (int x = 0; x < texture.size.x; x++)
                {
                    for (int y = 0; y < texture.size.y; y++)
                    {
                        int pixelIndex = (int)y * texture.size.x + (int)x;
                        var distanceToMidX = math.distance(x, texture.size.x / 2f);
                        var distanceToMidY = math.distance(y, texture.size.y / 2f);
                        if (distanceToMidX <= crosshairRadius || distanceToMidY <= crosshairRadius)
                        {
                            texture.colors[pixelIndex] = fillColor;
                        }
                        else
                        {
                            texture.colors[pixelIndex] = clearColor;
                        }
                    }
                }
                for (int i = 0; i < texture.size.x; i++)
                {
                    for (int j = 0; j < texture.size.y; j++)
                    {
                        var position = new int3(i, j, 0);
                        var index = VoxelUtilities.GetTextureArrayIndexXY(position, size);
                        var thisColour = texture.colors[index];
                        if (i == 0 || j == 0 || i == texture.size.x - 1 || j == texture.size.y - 1)
                        {
                            if (thisColour.alpha != 0 && thisColour.red != outlineColor.red)
                            {
                                texture.colors[index] = outlineColor;
                            }
                            else
                            {
                                // check top if bottom row
                                if (i == 0)
                                {
                                    var positionRight = new int3(i + 1, j, 0);
                                    var indexRight = VoxelUtilities.GetTextureArrayIndexXY(positionRight, size);
                                    var colourRight = texture.colors[indexRight];
                                    if (colourRight.alpha != 0 && colourRight.red != outlineColor.red)
                                    {
                                        texture.colors[index] = outlineColor;
                                    }
                                }
                                else // i isi size.x - 1
                                {
                                    var positionLeft = new int3(i - 1, j, 0);
                                    var indexLeft  = VoxelUtilities.GetTextureArrayIndexXY(positionLeft , size);
                                    var colourLeft  = texture.colors[indexLeft ];
                                    if (colourLeft.alpha != 0 && colourLeft.red != outlineColor.red)
                                    {
                                        texture.colors[index] = outlineColor;
                                    }
                                }
                                if (j == 0)
                                {
                                    var positionDown = new int3(i, j + 1, 0);
                                    var indexDown = VoxelUtilities.GetTextureArrayIndexXY(positionDown, size);
                                    var colourDown = texture.colors[indexDown];
                                    if (colourDown.alpha != 0 && colourDown.red != outlineColor.red)
                                    {
                                        texture.colors[index] = outlineColor;
                                    }
                                }
                                else // j is size.y - 1
                                {
                                    var positionUp = new int3(i, j - 1, 0);
                                    var indexUp = VoxelUtilities.GetTextureArrayIndexXY(positionUp, size);
                                    var colourUp = texture.colors[indexUp];
                                    if (colourUp.alpha != 0 && colourUp.red != outlineColor.red)
                                    {
                                        texture.colors[index] = outlineColor;
                                    }
                                }
                            }
                        }
                        else if (thisColour.alpha == 0)
                        {
                            var positionUp = new int3(i, j - 1, 0);
                            var indexUp = VoxelUtilities.GetTextureArrayIndexXY(positionUp, size);
                            var colourUp = texture.colors[indexUp];
                            if (colourUp.alpha != 0 && colourUp.red != outlineColor.red)
                            {
                                texture.colors[index] = outlineColor;
                            }
                            var positionDown = new int3(i, j + 1, 0);
                            var indexDown = VoxelUtilities.GetTextureArrayIndexXY(positionDown, size);
                            var colourDown = texture.colors[indexDown];
                            if (colourDown.alpha != 0 && colourDown.red != outlineColor.red)
                            {
                                texture.colors[index] = outlineColor;
                            }
                            var positionLeft = new int3(i - 1, j, 0);
                            var indexLeft  = VoxelUtilities.GetTextureArrayIndexXY(positionLeft , size);
                            var colourLeft = texture.colors[indexLeft];
                            if (colourLeft .alpha != 0 && colourLeft .red != outlineColor.red)
                            {
                                texture.colors[index] = outlineColor;
                            }
                            var positionRight = new int3(i + 1, j, 0);
                            var indexRight = VoxelUtilities.GetTextureArrayIndexXY(positionRight, size);
                            var colourRight = texture.colors[indexRight];
                            if (colourRight.alpha != 0 && colourRight.red != outlineColor.red)
                            {
                                texture.colors[index] = outlineColor;
                            }
                        }
                    }
                }
                PostUpdateCommands.AddComponent<TextureDirty>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}