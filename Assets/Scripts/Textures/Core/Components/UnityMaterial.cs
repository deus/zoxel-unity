using Unity.Entities;
using System;

namespace Zoxel.Textures
{
    //! A Material holding component.
    public struct UnityMaterial : ISharedComponentData, IEquatable<UnityMaterial>
    {
        public UnityEngine.Material material;

        public UnityMaterial(UnityEngine.Material material)
        {
            this.material = material;
        }
        
        public bool Equals(UnityMaterial other)
        {
            return material == other.material;
        }
        
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                if (ReferenceEquals(material, null))
                {
                    return 0;
                }
                else
                {
                    return material.GetHashCode();
                }
            }
        }
    }
}