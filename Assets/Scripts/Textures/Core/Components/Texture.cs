using Unity.Entities;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Textures
{
    //! Stores a 2D array of color information.
    public struct Texture : IComponentData
    {
        public int2 size;
        public BlitableArray<Color> colors;

        public Texture(in UnityEngine.Texture2D texture)
        {
            size.x = texture.width;
            size.y = texture.height;
            colors = new BlitableArray<Color>(size.x * size.y, Allocator.Persistent);
            var data = texture.GetRawTextureData<byte>();
            if (data.Length != colors.Length * 4)
            {
                UnityEngine.Debug.LogError("SetFromUnityTexture incorrect format of texture: " + texture.name + " - " + data.Length + " :: " + colors.Length);
                return;
            }
            for (int i = 0; i < colors.Length; i++)
            {
                var color = new Color();
                color.red = data[i * 4];
                color.green = data[i * 4 + 1];
                color.blue = data[i * 4 + 2];
                color.alpha = data[i * 4 + 3];
                colors[i] = color;
            }
        }

        public void DisposeFinal()
        {
            colors.DisposeFinal();
        }

        public void Dispose()
        {
            colors.Dispose();
        }

        public void Initialize(int2 size)
        {
            if (this.size.x != size.x || this.size.y != size.y) 
            {
                Dispose();
                this.size.x = size.x;
                this.size.y = size.y;
                colors = new BlitableArray<Color>(size.x * size.y, Allocator.Persistent);
            }
        }

        public UnityEngine.Texture2D GetTexture()
        {
            if (size.x == 0 || size.y == 0 || colors.Length == 0 || size.x * size.y != colors.Length)
            {
                return null;
            }
            var texture = new UnityEngine.Texture2D(size.x, size.y, UnityEngine.TextureFormat.RGBA32, false);
            texture.filterMode = UnityEngine.FilterMode.Point;
            texture.LoadRawTextureData(colors.ToNativeArray());
            /*var data = texture.GetRawTextureData<byte>();
            for (int i = 0; i < colors.Length; i++)
            {
                var color = colors[i];
                data[i * 4] = color.red;
                data[i * 4 + 1] = color.green;
                data[i * 4 + 2] = color.blue;
                data[i * 4 + 3] = color.alpha;
            }*/
            texture.Apply(false);    // Upload to GPU
            return texture;
        }

        public UnityEngine.Texture2D CreateEmptyTexture()
        {
            if (size.x == 0 || size.y == 0 || colors.Length == 0 || size.x * size.y != colors.Length)
            {
                return null;
            }
            var texture = new UnityEngine.Texture2D(size.x, size.y, UnityEngine.TextureFormat.RGBA32, false);
            texture.filterMode = UnityEngine.FilterMode.Point;
            return texture;
        }

        public UnityEngine.Texture2D SetTexture(ref UnityEngine.Texture2D texture)
        {
            if (size.x == 0 || size.y == 0 || colors.Length == 0 || size.x * size.y != colors.Length)
            {
                return null;
            }
            if (texture.width != size.x || texture.height != size.y)
            {
                ObjectUtil.Destroy(texture);
                texture = CreateEmptyTexture();
            }
            texture.LoadRawTextureData(colors.ToNativeArray());
            texture.Apply(false);    // Upload to GPU
            return texture;
        }

        public void GenerateNoise2()
        {
            var random = new Random();
            random.InitState(666);
            var random2 = new Random();
            random2.InitState(777);
            var random3 = new Random();
            random3.InitState(888);
            for (int x = 0; x < size.x; x++)
            {
                for (int y = 0; y < size.y; y++)
                {
                    int pixelIndex = (int)y * size.x + (int)x;
                    var red = (byte)((int)(255 * random.NextFloat(1)));
                    var green = (byte)((int)(255 * random2.NextFloat(1)));
                    var blue = (byte)((int)(255 * random3.NextFloat(1)));
                    colors[pixelIndex] = new Color(red, green, blue); //, (byte)(100 + random.NextInt(155)));
                }
            }
        }

        public Texture Clone()
        {
            var newTexture = new Texture();
            newTexture.colors = new BlitableArray<Color>(colors.Length, Allocator.Persistent);
            newTexture.size = size;
            for (int i = 0; i < colors.Length; i++)
            {
                newTexture.colors[i] = colors[i];
            }
            return newTexture;
        }
    }
}
/*var data = texture.GetRawTextureData<byte>();
for (int i = 0; i < colors.Length * 4; i += 4)
{
    var color = colors[i];
    data[i] = color.red;
    data[i + 1] = color.green;
    data[i + 2] = color.blue;
    data[i + 3] = color.alpha;
}*/
/*
var data = texture.GetRawTextureData<Color>();
for (int i = 0; i < colors.Length; i++)
{
    data[i] = colors[i];
}
*/

/*public void Initialize(int size.x, int size.y)
{
    if (this.size.x != size.x || this.size.y != size.y) 
    {
        Dispose();
        this.size.x = size.x;
        this.size.y = size.y;
        colors = new BlitableArray<Color>(size.x * size.y, Allocator.Persistent);
    }
}*/