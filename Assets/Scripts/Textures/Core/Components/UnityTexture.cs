using Unity.Entities;
using System;
// using UnityEngine;

namespace Zoxel.Textures
{
    //! A Texture holding component.
    public struct UnityTexture : ISharedComponentData, IEquatable<UnityTexture>
    {
        public UnityEngine.Texture texture;
        
        public bool Equals(UnityTexture other)
        {
            return texture == other.texture;
        }
        
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                if (ReferenceEquals(texture, null))
                {
                    return 0;
                }
                else
                {
                    return texture.GetHashCode();
                }
            }
        }
    }
}