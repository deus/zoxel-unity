using Unity.Entities;

namespace Zoxel.Textures
{
    //! Contains data for texture frames.
    public struct TextureFrame : IComponentData
    {
        public int resolution;
        public int frameThickness;
        public Color fillColor;
        public Color frameColor;

        public TextureFrame(in FrameGenerationData frameGenerationData)
        {
            this.resolution = frameGenerationData.resolution;
            this.frameThickness = frameGenerationData.thickness;
            this.fillColor = new Color(255, 255, 255, 255);
            this.frameColor = new Color(0, 0, 0, 255);
        }
    }
}