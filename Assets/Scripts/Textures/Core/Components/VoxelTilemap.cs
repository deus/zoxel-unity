using Unity.Entities;

namespace Zoxel.Textures
{
    //! A tilemap based on voxels.
    public struct VoxelTilemap : IComponentData { }
}