using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Textures
{
    public struct VoxelTexture : IComponentData
    {
        // public uint textureSeed;
        public byte generationType;
        public Color baseColor;
        public Color secondaryColor;
        public int3 baseVariance;
        public int3 secondaryVariance;
        public int3 noiseVariance;
    }
}