using Unity.Entities;

namespace Zoxel.Textures
{
    //! Generates a crosshair texture.
    public struct GenerateCrosshairUI : IComponentData { }
}