using Unity.Entities;

namespace Zoxel.Textures
{
    //! Event to generate a new texture frame when UI size changes, will need to update.
    public struct GenerateTextureFrame : IComponentData { }
}