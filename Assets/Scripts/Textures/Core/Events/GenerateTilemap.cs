using Unity.Entities;

namespace Zoxel.Textures
{
    //! An event for generating a tilemap.
    public struct GenerateTilemap : IComponentData { }
}