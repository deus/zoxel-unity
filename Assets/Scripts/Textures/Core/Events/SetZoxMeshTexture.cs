using Unity.Entities;

namespace Zoxel.Textures
{
    public struct SetZoxMeshTexture : IComponentData
    {
        public Entity textureEntity;

        public SetZoxMeshTexture(Entity textureEntity)
        {
            this.textureEntity = textureEntity;
        }
    }
}