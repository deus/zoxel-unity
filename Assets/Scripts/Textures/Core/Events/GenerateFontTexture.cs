using Unity.Entities;
using Unity.Mathematics;
// what font is it?

namespace Zoxel.Textures
{
    //! Generates a Font Texture.
    public struct GenerateFontTexture : IComponentData
    {
        public byte fontIndex;
        public Random random;
        public Color outlineColor;
        public Color fillColor;
        public TextGenerationData textGenerationData;
        // public Entity font; // BlitableArray<int2> points;
        public BlitableArray<int2> points;

        public void Dispose() 
        {
            if (points.Length > 0)
            {
                points.Dispose();
            }
        }

        public void SetRandom(int uniqueID)
        {
            random = new Random();
            random.InitState((uint)uniqueID);
        }
    }
}