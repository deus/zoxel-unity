using Unity.Entities;

namespace Zoxel.Textures
{
    //! Generates a basic square icon.
    public struct GenerateSquareIcon : IComponentData
    {
        public int resolution;

        public GenerateSquareIcon(int resolution = 32)
        {
            this.resolution = resolution;
        }
    }
}