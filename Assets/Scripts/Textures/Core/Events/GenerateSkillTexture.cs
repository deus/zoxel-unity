using Unity.Entities;
using Unity.Mathematics;
// TODO: Rename to GenerateIconTexture
//      Or use node system - add this particular part will be a circle texture, with an outline, maybe some noise, or some parts removed
//      Fill Circle, Fill Little Circle, Add Outline, Add Noise to fill colours

namespace Zoxel.Textures
{
    public struct GenerateSkillTexture : IComponentData
    {
        public int seed;
        public byte shape;

        public GenerateSkillTexture(int seed, byte skillType)
        {
            this.seed = seed;
            if (skillType == SkillType.PhysicalAttack)
            {
                shape = ShapeType.Square;
            }
            else if (skillType == SkillType.Projectile)
            {
                shape = ShapeType.Triangle;
            }
            else if (skillType == SkillType.Shield)
            {
                shape = ShapeType.Diamond;
            }
            else
            {
                shape = ShapeType.Circle;
            }
        }
    }
}