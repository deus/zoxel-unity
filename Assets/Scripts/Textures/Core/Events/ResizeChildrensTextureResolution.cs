using Unity.Entities;

namespace Zoxel.Textures
{
    //! Resizes childrens textures.
    public struct ResizeChildrenTextures : IComponentData
    {
        public int textureResolution;
        
        public ResizeChildrenTextures(int textureResolution)
        {
            this.textureResolution = textureResolution;
        }
    }
}