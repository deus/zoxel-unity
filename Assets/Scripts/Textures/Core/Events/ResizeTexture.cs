using Unity.Entities;

namespace Zoxel.Textures
{
    public struct ResizeTexture : IComponentData
    {
        public int textureResolution;
        
        public ResizeTexture(int textureResolution)
        {
            this.textureResolution = textureResolution;
        }
    }
}