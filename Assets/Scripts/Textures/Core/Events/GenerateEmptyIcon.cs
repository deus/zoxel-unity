using Unity.Entities;

namespace Zoxel.Textures
{
    //! Generates a basic circle texture.
    public struct GenerateEmptyIcon : IComponentData
    {
        public int resolution;
        public byte count;
        public byte delay;

        public GenerateEmptyIcon(int resolution = 32, byte delay = 0)
        {
            this.resolution = resolution;
            this.delay = delay;
            this.count = 0;
        }
    }
}