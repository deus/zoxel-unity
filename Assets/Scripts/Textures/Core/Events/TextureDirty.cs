using Unity.Entities;

namespace Zoxel.Textures
{
    public struct TextureDirtyDelayed : IComponentData
    {
        public byte count;
        public byte delay;

        public TextureDirtyDelayed(byte delay = 0)
        {
            this.delay = delay;
            this.count = 0;
        }
    }
    public struct TextureDirty : IComponentData { }
}