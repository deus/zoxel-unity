using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;

namespace Zoxel.Textures
{
    //! Resizes voxel textures for all children.
    [BurstCompile, UpdateInGroup(typeof(TextureSystemGroup))]
    public partial class ChildrenTexturesResizeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Entities.ForEach((Entity e, int entityInQueryIndex, in Childrens childrens, in ResizeChildrenTextures resizeChildrenTextures) =>
            {
                PostUpdateCommands.RemoveComponent<ResizeChildrenTextures>(entityInQueryIndex, e);
                var textureResolution = resizeChildrenTextures.textureResolution;
                for (int i = 0; i < childrens.children.Length; i++)
                {
                    var child = childrens.children[i];
                    PostUpdateCommands.AddComponent(entityInQueryIndex, child, new ResizeTexture(textureResolution));
                }
            }).ScheduleParallel();
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}