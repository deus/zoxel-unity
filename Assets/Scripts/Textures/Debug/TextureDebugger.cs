using UnityEngine;
using System.Collections.Generic;
// add tilemaps

namespace Zoxel.Textures // .Debug
{
    //! When textures are generated, they get added here.
    public partial class TextureDebugger : MonoBehaviour
    {
        public static TextureDebugger instance;
        public int maxTextures = 100;
        public List<Texture2D> textures;
        public List<Material> materials;
        public List<UnityEngine.Texture> tilemaps;

        void Awake()
        {
            TextureDebugger.instance = this;
        }
        
        public void AddTilemap(UnityEngine.Texture newTexture)
        {
            tilemaps.Insert(0, newTexture);
            if (tilemaps.Count >= maxTextures)
            {
                tilemaps.RemoveAt(tilemaps.Count - 1);
            }
        }
        
        public void AddTexture(Texture2D newTexture)
        {
            textures.Insert(0, newTexture);
            if (textures.Count >= maxTextures)
            {
                textures.RemoveAt(textures.Count - 1);
            }
        }
        
        public void AddMaterial(Material newMaterial)
        {
            materials.Insert(0, newMaterial);
            if (materials.Count >= maxTextures)
            {
                materials.RemoveAt(materials.Count - 1);
            }
        }
    }
}