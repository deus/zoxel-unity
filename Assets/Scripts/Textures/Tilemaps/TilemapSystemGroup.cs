using Unity.Entities;

namespace Zoxel.Textures.Tilemaps
{
    //! Updates material systems.
    [UpdateInGroup(typeof(TextureSystemGroup))]
    public partial class TilemapSystemGroup : ComponentSystemGroup { }
}