using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Textures.Tilemaps
{
    //! Links up Tilemaps to a material after they spawn.
    /**
    *   - Linking System -
    */
    [BurstCompile, UpdateInGroup(typeof(TilemapSystemGroup))]
    public partial class TilemapsSpawnedSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery tilemapsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            tilemapsQuery = GetEntityQuery(
                ComponentType.ReadOnly<Tilemap>(),
                ComponentType.ReadOnly<MaterialLink>(),
                ComponentType.Exclude<DestroyEntity>());
            RequireForUpdate(processQuery);
            RequireForUpdate(tilemapsQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var tilemapEntities = tilemapsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var materialLinks = GetComponentLookup<MaterialLink>(true);
            var dataIndexes = GetComponentLookup<UserDataIndex>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref TilemapLinks tilemapLinks, ref OnSpawnedTilemaps onSpawnedTilemaps) =>
            {
                if (onSpawnedTilemaps.spawned != 0)
                {
                    tilemapLinks.Initialize(onSpawnedTilemaps.spawned);
                    onSpawnedTilemaps.spawned = 0;
                }
                var count = 0;
                for (int i = 0; i < tilemapEntities.Length; i++)
                {
                    var e2 = tilemapEntities[i];
                    var materialEntity = materialLinks[e2].material;
                    if (materialEntity == e)
                    {
                        var dataIndex = dataIndexes[e2];
                        tilemapLinks.tilemaps[dataIndex.index] = e2;
                        count++;
                        if (count >= tilemapLinks.tilemaps.Length)
                        {
                            break;
                        }
                    }
                }
                if (count == tilemapLinks.tilemaps.Length)
                {
                    // UnityEngine.Debug.LogError("tilemapLinks Set: " + count);
                    PostUpdateCommands.RemoveComponent<OnSpawnedTilemaps>(entityInQueryIndex, e);
                }
            })  .WithReadOnly(tilemapEntities)
                .WithDisposeOnCompletion(tilemapEntities)
                .WithReadOnly(materialLinks)
                .WithReadOnly(dataIndexes)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
} 
