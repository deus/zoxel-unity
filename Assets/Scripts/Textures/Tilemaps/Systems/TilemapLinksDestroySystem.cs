using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Textures.Tilemaps
{
    //! Cleans up TilemapLinks on DestroyEntity.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class TilemapLinksDestroySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
				.WithAll<DestroyEntity, TilemapLinks>()
				.ForEach((int entityInQueryIndex, in TilemapLinks tilemapLinks) =>
			{
                for (int i = 0; i < tilemapLinks.tilemaps.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, tilemapLinks.tilemaps[i]);
                }
                tilemapLinks.Dispose();
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}