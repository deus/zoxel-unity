using Unity.Entities;

namespace Zoxel.Textures.Tilemaps
{
    //! Event for linking new tilemaps to material.
    public struct OnSpawnedTilemaps : IComponentData
    {
        public int spawned;

        public OnSpawnedTilemaps(int spawned)
        {
            this.spawned = spawned;
        }
    }
}