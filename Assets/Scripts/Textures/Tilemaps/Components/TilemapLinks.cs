using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Textures.Tilemaps
{
    //! Links each Material to Tilemap's based on resolutions.
    public struct TilemapLinks : IComponentData
    {
        public BlitableArray<Entity> tilemaps;

        public void Dispose()
        {
            tilemaps.Dispose();
        }

        public void Initialize(int count)
        {
            Dispose();
            this.tilemaps = new BlitableArray<Entity>(count, Allocator.Persistent);
            for (int i = 0; i < count; i++)
            {
                this.tilemaps[i] = new Entity();
            }
        }
    }

}