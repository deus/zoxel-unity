using Unity.Entities;

namespace Zoxel.Textures.Tilemaps
{
	//! Tilemap meta data.
	public struct Tilemap : IComponentData
	{
        //! The index of the tilemap based on a group of tilemap resolutions.
        public byte resolutionIndex;
        //! The resolution of the textures.
        public byte resolution;
        //! The grid size of a tilemap. Gets updated when generated.
        public byte gridSize;

        public Tilemap(byte resolutionIndex, byte resolution)
        {
            this.resolutionIndex = resolutionIndex;
            this.resolution = resolution;
            this.gridSize = 0;
        }
    }
}