using Unity.Entities;

namespace Zoxel.Textures.Tilemaps
{
    //! A spawn event for spawning new textures for each material.
    public struct SpawnTilemaps : IComponentData { }
}