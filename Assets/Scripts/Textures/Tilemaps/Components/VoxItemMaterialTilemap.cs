using Unity.Entities;

namespace Zoxel.Textures.Tilemaps
{
    //! Holds item material.
    public struct VoxItemMaterialTilemap : IComponentData
    {
        public byte gridSize;

        public VoxItemMaterialTilemap(byte gridSize)
        {
            this.gridSize = gridSize;
        }
    }
}