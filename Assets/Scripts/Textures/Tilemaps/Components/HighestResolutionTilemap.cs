using Unity.Entities;

namespace Zoxel.Textures.Tilemaps
{
    //! The highest resolution tilemap connected to a material.
    public struct HighestResolutionTilemap : IComponentData { }
}