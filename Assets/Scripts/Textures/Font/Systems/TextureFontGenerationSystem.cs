using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.UI;
using Unity.Burst.Intrinsics;

namespace Zoxel.Textures.Font
{
    //! Generates font textures using point data.
    /**
    *   Add in FontData for different font point styles.
    */
    [BurstCompile, UpdateInGroup(typeof(TextureSystemGroup))]
    public partial class TextureFontGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery uis;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        	uis = GetEntityQuery(
                ComponentType.Exclude<TextureDirty>(),
                ComponentType.Exclude<InitializeEntity>(),
                ComponentType.ReadOnly<Size2D>(),
                ComponentType.ReadWrite<Texture>(),
                ComponentType.ReadWrite<GenerateFontTexture>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            // var fontEntities = chunkTerrainsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            // Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            // var fonts = GetComponentLookup<Font>(true);
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<GenerateFontTexture>(uis);
            PostUpdateCommands2.AddComponent<TextureDirty>(uis);
            // var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var job = new GenerateTextureFontJob
            {
                // PostUpdateCommands = PostUpdateCommands,
                // entityHandle = GetEntityTypeHandle(),
                uiElementHandle = GetComponentTypeHandle<Size2D>(true),
                textureHandle = GetComponentTypeHandle<Texture>(false),
                generateTextureFontHandle = GetComponentTypeHandle<GenerateFontTexture>(false),
                //fontEntities = fontEntities,
                //fonts = fonts
            };
            Dependency = job.ScheduleParallel(uis, Dependency);
            // // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        [BurstCompile]
        struct GenerateTextureFontJob : IJobChunk
        {
            // public EntityCommandBuffer.ParallelWriter PostUpdateCommands;
            // [ReadOnly] public EntityTypeHandle entityHandle;
            [ReadOnly] public ComponentTypeHandle<Size2D> uiElementHandle;  // [ReadOnly] 
            public ComponentTypeHandle<Texture> textureHandle;
            public ComponentTypeHandle<GenerateFontTexture> generateTextureFontHandle;
            /// [DeallocateOnJobCompletion] 
            //[ReadOnly, DeallocateOnJobCompletion] public NativeArray<Entity> fontEntities;
            //[ReadOnly] public ComponentLookup<Font> fonts;

            public void Execute(in ArchetypeChunk chunk, int unfilteredChunkIndex, bool useEnabledMask, in v128 chunkEnabledMask)
            // public void Execute(ArchetypeChunk chunk, int chunkIndex, int firstEntityIndex)
            {
                // var entities = chunk.GetNativeArray(entityHandle);
                var textures = chunk.GetNativeArray(textureHandle);
                var generateTextureFonts = chunk.GetNativeArray(generateTextureFontHandle);
                var uiElements = chunk.GetNativeArray(uiElementHandle);
                for (var i = 0; i < chunk.Count; i++)
                {
                    var texture = textures[i];
                    var generateFontTexture = generateTextureFonts[i];
                    var uiElement = uiElements[i];
                    Process(ref texture, ref generateFontTexture, in uiElement);
                    textures[i] = texture;
                    generateTextureFonts[i] = generateFontTexture;
                }
                textures.Dispose();
                generateTextureFonts.Dispose();
                uiElements.Dispose();
            }

            private void Process(ref Texture texture, ref GenerateFontTexture generateFontTexture, in Size2D uiElement)
            {
                var resolution = generateFontTexture.textGenerationData.resolution;
                if (resolution == 0)
                {
                    resolution = 64;
                }
                var pointSize = generateFontTexture.textGenerationData.pointSize;
                var pointNoise = generateFontTexture.textGenerationData.pointNoise;
                var outlineThickness = generateFontTexture.textGenerationData.outlineThickness;
                var pointVariation = generateFontTexture.textGenerationData.pointVariation;
                if (pointSize.x == 0 && pointSize.y == 0)
                {
                    pointSize = new float2(0.97f, 1.3f);
                }
                if (pointNoise.x == 0 && pointNoise.y == 0)
                {
                    pointNoise = new float2(0.8f, 1.1f);
                }
                if (outlineThickness == 0)
                {
                    outlineThickness = 2f;
                }
                if (pointVariation == 0)
                {
                    pointVariation = 0.5f;
                }
                texture.Initialize(new int2(resolution, resolution));
                var resolutionIncrease = texture.size.x / 16f;
                pointSize.y *= resolutionIncrease;
                ShakeLetters(ref texture, ref generateFontTexture, pointVariation);
                ApplyClear(ref texture);
                ApplyFill(ref texture, ref generateFontTexture, pointSize, pointNoise);
                AddOutline(ref texture, ref generateFontTexture, outlineThickness);
                AddNoise(ref texture, ref generateFontTexture);
                generateFontTexture.Dispose();
            }

            public void ShakeLetters(ref Texture texture, ref GenerateFontTexture generateFontTexture, float pointVariation)
            {
                var resolutionIncrease = texture.size.x / 16f;
                var shakens = new NativeArray<byte>(generateFontTexture.points.Length, Allocator.Temp);
                for (int i = 0; i < shakens.Length; i++)
                {
                    shakens[i] = 0;
                }
                for (int i = 0; i < generateFontTexture.points.Length; i++)
                {
                    if (shakens[i] == 1)
                    {
                        continue;
                    }
                    var originalPoint = generateFontTexture.points[i];
                    var newPoint = new int2(
                        (int)((originalPoint.x + generateFontTexture.random.NextFloat(pointVariation, pointVariation)) * resolutionIncrease), 
                        (int)((originalPoint.y + generateFontTexture.random.NextFloat(pointVariation, pointVariation)) * resolutionIncrease));
                    newPoint = new int2(
                        math.clamp(newPoint.x, 1, texture.size.x - 1),
                        math.clamp(newPoint.y, 1, texture.size.y - 1));
                    for (int j = i + 1; j < generateFontTexture.points.Length; j++)
                    {
                        if (generateFontTexture.points[j] == originalPoint)
                        {
                            generateFontTexture.points[j] = newPoint;
                            shakens[j] = 1;
                        }
                    }
                    generateFontTexture.points[i] = newPoint;
                    shakens[i] = 1;
                }
                shakens.Dispose();
            }

            public void ApplyClear(ref Texture texture)
            {
                var clearColor = new Color(255, 0, 0, 0);
                for (int i = 0; i < texture.colors.Length; i++)
                {
                    texture.colors[i] = clearColor;
                }
            }

            public void ApplyFill(ref Texture texture, ref GenerateFontTexture generateFontTexture, float2 pointSize, float2 pointNoise)
            {
                var random = generateFontTexture.random;
                var size = new int3(texture.size.x, texture.size.y, 0);
                for (int i = 0; i < generateFontTexture.points.Length; i += 2)
                {
                    var pointA = generateFontTexture.points[i];
                    var pointB = generateFontTexture.points[i + 1];
                    var pointRealA = new float3(pointA.x, pointA.y, 0);
                    var pointRealB = new float3(pointB.x, pointB.y, 0);
                    var distance = math.distance(pointRealA, pointRealB);
                    var direction = math.normalize(pointRealB - pointRealA);
                    for (int j = 0; j < distance; j++)
                    {
                        var newPoint = direction * j + pointRealA + new float3(
                            random.NextFloat(pointNoise.x * 2) - pointNoise.x, 
                            random.NextFloat(pointNoise.y * 2) - pointNoise.y,
                            0);
                        var pointSize2 = math.floor(random.NextFloat(pointSize.x, pointSize.y));
                        for (var k = -pointSize2; k <= pointSize2; k++)
                        {
                            for (var l = -pointSize2; l <= pointSize2; l++)
                            {
                                var drawPoint = new int3((int)(newPoint.x + k), (int)(newPoint.y + l), 0);
                                if (drawPoint.x >= 0 && drawPoint.x < size.x && drawPoint.y >= 0 && drawPoint.y < size.y)
                                {
                                    var arrayIndex = VoxelUtilities.GetTextureArrayIndexXY(drawPoint, size);
                                    texture.colors[arrayIndex] = generateFontTexture.fillColor;
                                }
                            }
                        }
                        j += (int)math.clamp(pointSize2, 0, pointSize2 - 1);
                    }
                }
            }

            //! Adds Random Variation to the colours
            public void AddNoise(ref Texture texture, ref GenerateFontTexture generateFontTexture)
            {
                var noiseMinus = -40;
                var noiseAddition = 30;
                var outlineColor = generateFontTexture.outlineColor;
                var size = new int3(texture.size.x, texture.size.y, 0);
                for (int i = 1; i < texture.size.x - 1; i++)
                {
                    for (int j = 1; j < texture.size.y - 1; j++)
                    {
                        var position = new int3(i, j, 0);
                        var index = VoxelUtilities.GetTextureArrayIndexXY(position, size);
                        var thisColour = texture.colors[index];
                        if (thisColour.alpha != 0 && thisColour != outlineColor)
                        {
                            thisColour.red = (byte)(math.min(255, (int) thisColour.red + generateFontTexture.random.NextInt(noiseMinus, noiseAddition)));
                            thisColour.green = (byte)(math.min(255, (int) thisColour.green + generateFontTexture.random.NextInt(noiseMinus, noiseAddition)));
                            thisColour.blue = (byte)(math.min(255, (int) thisColour.blue + generateFontTexture.random.NextInt(noiseMinus, noiseAddition)));
                            texture.colors[index] = thisColour;
                        }
                    }
                }
            }

            //! Adds an outline onto text
            public void AddOutline(ref Texture texture, ref GenerateFontTexture generateFontTexture, float outlineThickness)
            {
                // first grab outline positions
                var fillColor = generateFontTexture.fillColor;
                var outlineColor = generateFontTexture.outlineColor;
                if (fillColor == outlineColor)
                {
                    return;
                }
                var size = new int2(texture.size.x, texture.size.y);
                var lengths = new NativeArray<byte>(texture.colors.Length, Allocator.Temp);
                for (int i = 0; i < lengths.Length; i++)
                {
                    lengths[i] = 255;
                }
                for (int i = 0; i < texture.size.x; i++)
                {
                    for (int j = 0; j < texture.size.y; j++)
                    {
                        var hasClearColorNextTo = false;
                        var position = new int2(i, j);
                        var thisColour = texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position, size)];
                        if (i == 0 || j == 0 || i == texture.size.x - 1 || j == texture.size.y - 1)
                        {
                            if (thisColour == fillColor)
                            {
                                hasClearColorNextTo = true;
                            }
                        }
                        else if (thisColour == fillColor)
                        {
                            hasClearColorNextTo = texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Up(), size)] != fillColor
                                || texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Down(), size)] != fillColor
                                || texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Left(), size)] != fillColor
                                || texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position.Right(), size)] != fillColor;
                        }
                        if (hasClearColorNextTo)
                        {
                            FloodFillOutline(ref lengths, in texture, position, size, fillColor, (byte)((int) outlineThickness), 0);
                        }
                    }
                }
                // Color our outline positions
                for (int i = 0; i < lengths.Length; i++)
                {
                    if (lengths[i] != 255)
                    {
                        texture.colors[i] = outlineColor;
                    }
                }
                lengths.Dispose();
            }

            //! Remember distance per point, and keep the lowest one, so it doesn't repeat steps.
            private void FloodFillOutline(ref NativeArray<byte> lengths, in Texture texture, int2 position, int2 size, Color fillColor, byte maxLength, byte length = 0)
            {
                if (position.x >= 0 && position.x < size.x && position.y >= 0 && position.y < size.y)
                {
                    var arrayIndex = VoxelUtilities.GetTextureArrayIndexXY(position, size);
                    if (length == 0 || texture.colors[arrayIndex] == fillColor)
                    {
                        var oldLength = lengths[arrayIndex];
                        if (oldLength < length + 1)
                        {
                            return;
                        }
                        lengths[arrayIndex] = (byte) (length + 1);
                        if (length < maxLength)
                        {
                            length++;
                            FloodFillOutline(ref lengths, in texture, position.Up(), size, fillColor, maxLength, length);
                            FloodFillOutline(ref lengths, in texture, position.Down(), size, fillColor, maxLength, length);
                            FloodFillOutline(ref lengths, in texture, position.Left(), size, fillColor, maxLength, length);
                            FloodFillOutline(ref lengths, in texture, position.Right(), size, fillColor, maxLength, length);
                        }
                    }
                }
            }
        }
    }
}

//! Remember distance per point, and keep the lowest one, so it doesn't repeat steps.
/*private void FloodFillOutline(ref NativeList<int2> positions, ref NativeList<int> lengths, in Texture texture, int2 position, int2 size, Color fillColor, float maxLength, int length = 0)
{
    if (position.x >= 0 && position.x < size.x && position.y >= 0 && position.y < size.y)
    {
        if (length == 0 || texture.colors[VoxelUtilities.GetTextureArrayIndexXY(position, size)] == fillColor)
        {
            var index = positions.IndexOf(position);
            if (index == -1) // !positions.Contains(position))
            {
                positions.Add(position);
                lengths.Add(length + 1);
            }
            else
            {
                var oldLength = lengths[index];
                if (oldLength < length + 1)
                {
                    return;
                }
                lengths[index] = length + 1;
            }
            if (length < maxLength)
            {
                length++;
                FloodFillOutline(ref positions, ref lengths, in texture, position.Up(), size, fillColor, maxLength, length);
                FloodFillOutline(ref positions, ref lengths, in texture, position.Down(), size, fillColor, maxLength, length);
                FloodFillOutline(ref positions, ref lengths, in texture, position.Left(), size, fillColor, maxLength, length);
                FloodFillOutline(ref positions, ref lengths, in texture, position.Right(), size, fillColor, maxLength, length);
            }
        }
    }
}*/