using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Cameras;
using Zoxel.Rendering;
using Zoxel.Rendering.Authoring;

namespace Zoxel.Particles
{
    //! Renders Particle's as cubes in a single draw call.
    /**
    *   - Render System -
    *   \todo Replace with DrawMeshInstancedIndirect using a compute buffer for arguments
    *   DrawProceduralIndirect?
    */
    [UpdateInGroup(typeof(PresentationSystemGroup))]
    public partial class ParticleRenderSystem : SystemBase
    {
        public const UnityEngine.ComputeBufferType bufferType =UnityEngine.ComputeBufferType.IndirectArguments;
        public const UnityEngine.ComputeBufferMode bufferMode = UnityEngine.ComputeBufferMode.SubUpdates;
        private EntityQuery particlesQuery;
        public static bool hasBoundsUpdated;
        public static UnityEngine.Bounds bounds;
        public static UnityEngine.Mesh particleCubeMesh;
        public static UnityEngine.ComputeBuffer particlesPositionBuffer;
        public static UnityEngine.ComputeBuffer particlesScaleBuffer;
        public static UnityEngine.ComputeBuffer particlesColorBuffer;

        protected override void OnCreate()
        {
            particlesQuery = GetEntityQuery(ComponentType.ReadOnly<Particle>());
            particleCubeMesh = MeshUtilities.GenerateCube(new float3(1,1,1));
            bounds = new UnityEngine.Bounds(float3.zero, new float3(512, 512, 512));
            RequireForUpdate(particlesQuery);
            RequireForUpdate<RenderSettings>();
        }
    
        protected override void OnUpdate()
        {
            var renderCount = ParticleRenderUpdateSystem.renderCount;
            if (renderCount == 0)
            {
                return;
            }
            var renderSettings = GetSingleton<RenderSettings>();
            var material = MaterialsManager.instance.materials.particlesMaterial;
            var shadowMode = renderSettings.shadowMode;
            var isShadows = renderSettings.isShadows;
            var isRenderToAllCameras = renderSettings.isRenderToAllCameras;
            UnityEngine.Camera renderCamera = null;
            if (!isRenderToAllCameras)
            {
                renderCamera = CameraReferences.GetMainCamera(EntityManager);
            }
            UnityEngine.Graphics.DrawMeshInstancedProcedural(particleCubeMesh, 0, material, bounds, renderCount,
                null, shadowMode, isShadows, 0, renderCamera);
        }

        public static void SetBoundsPosition(float3 newPosition)
        {
            if (bounds.center.x != newPosition.x || bounds.center.y != newPosition.y || bounds.center.z != newPosition.z)
            {
                bounds.center = newPosition;
                hasBoundsUpdated = true;
            }
        }

        protected override void OnDestroy()
        {
            if (particlesPositionBuffer != null)
            {
                particlesPositionBuffer.Release();
                particlesScaleBuffer.Release();
                particlesColorBuffer.Release();
            }
        }
    }
}