using Unity.Entities;
using Zoxel.Rendering;

namespace Zoxel.Particles
{
    //! Updates render systems origin positions when ChunkStreamPoint position changes.
    [UpdateBefore(typeof(RenderBoundsPostUpdateSystem))]
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class ParticleRenderBoundsSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((in OnChunkStreamPointUpdated onChunkStreamPointUpdated) =>
            {
                ParticleRenderSystem.SetBoundsPosition(onChunkStreamPointUpdated.worldPosition);
            }).WithoutBurst().Run();
        }
    }
}