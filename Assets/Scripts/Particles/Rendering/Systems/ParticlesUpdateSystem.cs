using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using UnityEngine;
using Zoxel.Transforms;
using Zoxel.Animations;
using Zoxel.Cameras;
using Zoxel.Rendering;

namespace Zoxel.Particles
{
    //! Updates particle data in our shader.
    /**
    *   \todo Optimize MaterialBaseColor and NonUniformScale using filters.
    */
    [UpdateAfter(typeof(RenderBoundsPostUpdateSystem))]
    [BurstCompile, UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class ParticleRenderUpdateSystem : SystemBase
    {
        public static int renderCount;
        private EntityQuery particlesQuery;

        protected override void OnCreate()
        {
            particlesQuery = GetEntityQuery(ComponentType.ReadOnly<Particle>());
        }
        
        [BurstCompile]
        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null) return;
            #if DISABLE_COMPUTER_BUFFERS
            return;
            #endif
            var material = MaterialsManager.instance.materials.particlesMaterial;
            var newParticlesCount = particlesQuery.CalculateEntityCount();
            if (renderCount != newParticlesCount)
            {
                renderCount = newParticlesCount;
                if (ParticleRenderSystem.particlesColorBuffer != null)
                {
                    ParticleRenderSystem.particlesColorBuffer.Release();
                    ParticleRenderSystem.particlesPositionBuffer.Release();
                    ParticleRenderSystem.particlesScaleBuffer.Release();
                }
                ParticleRenderSystem.particlesColorBuffer = new ComputeBuffer(renderCount, 4 * 4, ParticleRenderSystem.bufferType, ParticleRenderSystem.bufferMode);
                ParticleRenderSystem.particlesPositionBuffer = new ComputeBuffer(renderCount, 3 * 4, ParticleRenderSystem.bufferType, ParticleRenderSystem.bufferMode);
                ParticleRenderSystem.particlesScaleBuffer = new ComputeBuffer(renderCount, 3 * 4, ParticleRenderSystem.bufferType, ParticleRenderSystem.bufferMode);
            }
            ParticleRenderSystem.hasBoundsUpdated = false;
            var boundsPosition = new float3(ParticleRenderSystem.bounds.center.x, ParticleRenderSystem.bounds.center.y, ParticleRenderSystem.bounds.center.z);
            var particlesPositionBuffer = ParticleRenderSystem.particlesPositionBuffer;
            if (particlesPositionBuffer == null) return;
            var positions = particlesPositionBuffer.BeginWrite<float3>(0, renderCount);
            var particlesScaleBuffer = ParticleRenderSystem.particlesScaleBuffer;
            var scales = particlesScaleBuffer.BeginWrite<float3>(0, renderCount);
            var particlesColorBuffer = ParticleRenderSystem.particlesColorBuffer;
            var colors = particlesColorBuffer.BeginWrite<float4>(0, renderCount);
            Dependency = Entities
                .WithAll<Particle>()
                .ForEach((Entity e, int entityInQueryIndex, in Translation translation, in MaterialBaseColor materialBaseColor, in NonUniformScale scale) =>
            {
                positions[entityInQueryIndex] = translation.Value - boundsPosition;
                scales[entityInQueryIndex] = scale.Value;
                colors[entityInQueryIndex] = materialBaseColor.Value;
            })  .WithNativeDisableContainerSafetyRestriction(positions)
                .WithNativeDisableContainerSafetyRestriction(scales)
                .WithNativeDisableContainerSafetyRestriction(colors)
                .ScheduleParallel(Dependency);
            particlesPositionBuffer.EndWrite<float3>(renderCount);
            material.SetBuffer("positions", particlesPositionBuffer);
            particlesScaleBuffer.EndWrite<float3>(renderCount);
            material.SetBuffer("scales", particlesScaleBuffer);
            particlesColorBuffer.EndWrite<float4>(renderCount);
            material.SetBuffer("colors", particlesColorBuffer);
        }
    }
}