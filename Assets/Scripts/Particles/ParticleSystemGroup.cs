using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;

namespace Zoxel.Particles
{
    [UpdateInGroup(typeof(RenderSystemGroup))]
    public partial class ParticleSystemGroup : ComponentSystemGroup
    {
        public static Entity particleSystemPrefab;

        protected override void OnCreate()
        {
            base.OnCreate();
            var particleSystemArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(DestroyAfterEvent),
                typeof(ParticleSystem),
                typeof(VoxLink),
                typeof(Translation),
                typeof(Rotation)
            );
            particleSystemPrefab = EntityManager.CreateEntity(particleSystemArchetype);
            EntityManager.SetComponentData(particleSystemPrefab, new Rotation { Value = quaternion.identity });
        }
     }
}