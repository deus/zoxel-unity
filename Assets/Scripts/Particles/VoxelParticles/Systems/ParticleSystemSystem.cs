using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Animations;
using Zoxel.Movement;
using Zoxel.Voxels;
using Zoxel.Rendering;
using Zoxel.Rendering.Authoring;

namespace Zoxel.Particles.VoxelParticles
{
    //! Spawns individual particles from a ParticleEmitter.
    /**
    *   \todo Make sure ffor slower fframes, it spawns the same rate per second.
    *   \todo SpawnShapes. Edge Only option. Cube / Sphere / Pyramid etc. A DebugParticleSystemShapesSystem.
    */
    [BurstCompile, UpdateInGroup(typeof(ParticleSystemGroup))]
    public partial class ParticleSystemSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity particlePrefab;
        private EntityQuery processQuery;
        private Entity collisionParticleSystemPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var particleArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(Particle),
                typeof(ParticleLifeTime),
                typeof(ParticleLerpPosition),
                typeof(ParticleLerpScale),
                typeof(MaterialBaseColor),
                typeof(Translation),
                typeof(NonUniformScale)
            );
            particlePrefab = EntityManager.CreateEntity(particleArchetype);
            var rainArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(Particle),
                typeof(ParticleLifeTime),
                typeof(ParticleLerpPosition),
                //typeof(ParticleLerpScale),
                typeof(MaterialBaseColor),
                typeof(Translation),
                typeof(NonUniformScale),
                typeof(RainParticle),
                typeof(BasicVoxelCollider),
                typeof(VoxLink)
            );
            collisionParticleSystemPrefab = EntityManager.CreateEntity(rainArchetype);
            EntityManager.SetComponentData(particlePrefab, new MaterialBaseColor { Value = new float4(0, 0, 0, 1) });
            RequireForUpdate(processQuery);
            RequireForUpdate<RenderSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var renderSettings = GetSingleton<RenderSettings>();
            if (renderSettings.disableParticles)
            {
                return;
            }
            var elapsedTime = World.Time.ElapsedTime;
            var particlePrefab2 = particlePrefab;
            var collisionParticleSystemPrefab2 = collisionParticleSystemPrefab;
            //var heightAddition = 1f;
            var scaleBegin = 1 / 32f;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref ParticleSystem particleSystem, in Translation position, in Rotation rotation, in VoxLink voxLink) =>
            {
                if (particleSystem.lifeTime != 0 && elapsedTime - particleSystem.timeBegun >= particleSystem.lifeTime)
                {
                    if (HasComponent<DestroyAfterEvent>(e))
                    {
                        // PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, e);
                    }
                    else
                    {
                        PostUpdateCommands.RemoveComponent<ParticleSystem>(entityInQueryIndex, e);
                    }
                }
                else if (particleSystem.timeSpawn == 0 || elapsedTime - particleSystem.lastTime >= particleSystem.timeSpawn)
                {
                    var percentageOfSpawn = 1f;
                    if (particleSystem.timeSpawn != 0)
                    {
                        var timePassed = elapsedTime - particleSystem.lastTime;
                        var timeOverLimit = timePassed - particleSystem.timeSpawn;
                        percentageOfSpawn = 1f + ((float) timeOverLimit / (float) particleSystem.timeSpawn);
                        if (percentageOfSpawn > 10f)
                        {
                            percentageOfSpawn = 10f;
                        }
                        /*var timeOverLimit = (elapsedTime - particleSystem.lastTime) - particleSystem.timeSpawn;
                        percentageOfSpawn = 1f + ((float) timeOverLimit / (float) particleSystem.timeSpawn);
                        // UnityEngine.Debug.LogError("timeOverLimit: " + timeOverLimit + " percentageOfSpawn:" + percentageOfSpawn + ":" + particleSystem.timeSpawn);
                        */
                    }
                    particleSystem.lastTime = elapsedTime;
                    var spawnRate = (int) math.ceil(particleSystem.spawnRate * percentageOfSpawn);
                    spawnRate = math.max(spawnRate, 1);
                    var isCollision = particleSystem.hasCollision == 1; //  && planet.Index != 0;
                    var particlePrefab5 = particlePrefab2;
                    if (isCollision)
                    {
                        particlePrefab5 = collisionParticleSystemPrefab2;
                    }
                    var baseColor = particleSystem.baseColor;
                    var particleEntities = new NativeArray<Entity>(spawnRate, Allocator.Temp);
                    PostUpdateCommands.Instantiate(entityInQueryIndex, particlePrefab5, particleEntities);
                    for (int i = 0; i < spawnRate; i++)
                    {
                        var e2 = particleEntities[i]; //  PostUpdateCommands.Instantiate(entityInQueryIndex, particlePrefab5);
                        var positionBegin = particleSystem.positionOffset; //  + new float3(randomX, randomY, randomZ);
                        positionBegin.x += particleSystem.random.NextFloat(particleSystem.spawnSize.x * 2) - particleSystem.spawnSize.x;
                        positionBegin.y += particleSystem.random.NextFloat(particleSystem.spawnSize.y * 2) - particleSystem.spawnSize.y;
                        positionBegin.z += particleSystem.random.NextFloat(particleSystem.spawnSize.z * 2) - particleSystem.spawnSize.z;
                        positionBegin = position.Value + math.rotate(rotation.Value, positionBegin);
                        var positionEnd = positionBegin + particleSystem.positionAdd; // new float3(0, heightAddition, 0);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ParticleLifeTime(elapsedTime, particleSystem.particleLife));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ParticleLerpPosition
                        {
                            positionBegin = positionBegin,
                            positionEnd = positionEnd
                        });
                        var particleColor = baseColor;
                        particleColor.red = (byte) (255 * ((particleColor.red / 255f) + particleSystem.random.NextFloat(particleSystem.colorVariance.x)));
                        particleColor.green = (byte) (255 * ((particleColor.green / 255f) + particleSystem.random.NextFloat(particleSystem.colorVariance.y)));
                        particleColor.blue = (byte) (255 * ((particleColor.blue / 255f) + particleSystem.random.NextFloat(particleSystem.colorVariance.z)));
                        if (particleColor.alpha == 255)
                        {
                            particleColor.alpha = (byte) (255 * (particleSystem.random.NextFloat(0.4f, 0.8f)));
                        }
                        else
                        {
                            particleColor.alpha = (byte) (255 * ((particleColor.alpha / 255f) + particleSystem.random.NextFloat(-0.1f, 0.1f)));
                        }
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new MaterialBaseColor(particleColor.ToFloat4()));
                        // add basic voxel collider onto it
                        // same one bullets use
                        if (isCollision)
                        {
                            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, voxLink); // new VoxLink(planet));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new NonUniformScale { Value = 
                                new float3(scaleBegin, particleSystem.random.NextFloat(scaleBegin, scaleBegin * 3), scaleBegin) });
                        }
                        else
                        {
                            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ParticleLerpScale(new float3(scaleBegin, scaleBegin, scaleBegin), float3.zero));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new NonUniformScale { Value = new float3(scaleBegin, scaleBegin, scaleBegin) });
                        }
                        // unity transform
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Translation { Value = positionBegin });
                        //PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Rotation { Value = quaternion.identity }); // rotation.Value });
                    }
                    particleEntities.Dispose();
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                        /*{
                            Value = new float4(
                                ,
                                ((int) baseColor.green) / 255f + particleSystem.random.NextFloat(particleSystem.colorVariance.y),
                                ((int) baseColor.blue) / 255f + particleSystem.random.NextFloat(particleSystem.colorVariance.z),
                                particleSystem.random.NextFloat(0.5f) + 0.5f)
                        });*/