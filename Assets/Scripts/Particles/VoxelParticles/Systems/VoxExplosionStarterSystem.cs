using Unity.Entities;
using Unity.Burst;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Skeletons;
using Zoxel.Voxels;
using Zoxel.Voxels.Lighting;

namespace Zoxel.Particles.VoxelParticles
{
    [BurstCompile, UpdateInGroup(typeof(ParticleSystemGroup))]
    public partial class VoxExplosionStarterSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // first stage just passes data into the component
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in ParticleExplosion particleExplosion, in HeldWeaponLinks heldWeaponLinks) =>
            {
                if (heldWeaponLinks.heldWeaponRightHand.Index > 0)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, heldWeaponLinks.heldWeaponRightHand, particleExplosion);
                }
                if (heldWeaponLinks.heldWeaponLeftHand.Index > 0)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, heldWeaponLinks.heldWeaponLeftHand, particleExplosion);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            
            Dependency = Entities.ForEach((ref ParticleExplosion particleExplosion, in Translation translation, in Rotation rotation, in VoxColors voxColors,
                in VoxScale voxScale, in NonUniformScale scale, in EntityLighting entityLighting) =>
            {
                particleExplosion.position = translation.Value;
                particleExplosion.rotation = rotation.Value;
                particleExplosion.voxelScale = voxScale.scale;
                particleExplosion.globalScale = scale.Value;
                //! \todo Multiply these colours by character's lighting. Dispose of the colours after.
                var lightingMultiply = LightingCurve.lightValues[entityLighting.lightValue];
                particleExplosion.colors = voxColors.colors.Clone();
                for (int i = 0; i < particleExplosion.colors.Length; i++)
                {
                    particleExplosion.colors[i] *= lightingMultiply;
                }
			}).ScheduleParallel(Dependency);
            // second stage sends the data to the chunks
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, ref ParticleExplosion particleExplosion, in Vox vox) =>
            {
                for (int i = 0; i < vox.chunks.Length; i++)
                {
                    PostUpdateCommands.AddComponent(entityInQueryIndex, vox.chunks[i], particleExplosion);
                }
                PostUpdateCommands.RemoveComponent<ParticleExplosion>(entityInQueryIndex, e);
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}