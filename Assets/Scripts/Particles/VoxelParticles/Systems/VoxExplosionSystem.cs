using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.Voxels;
using Zoxel.Particles;
using Zoxel.Rendering.Authoring;

namespace Zoxel.Particles.VoxelParticles
{
    //! Spawns particles from a Vox while removing them.
    [BurstCompile, UpdateInGroup(typeof(ParticleSystemGroup))]
    public partial class VoxExplosionSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
		private EntityQuery chunkRenderQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			chunkRenderQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<ChunkRender>(),
                ComponentType.ReadOnly<ModelMesh>(),
                ComponentType.ReadOnly<ModelRender>());
            RequireForUpdate(processQuery);
            RequireForUpdate(chunkRenderQuery);
            RequireForUpdate<RenderSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var renderSettings = GetSingleton<RenderSettings>();
            if (renderSettings.disableParticles)
            {
                return;
            }
            var forceScale = 0.6f;
            var forceScale2 = 0f; // 0.3f;
            var particlePrefab = ParticleSystemSystem.particlePrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var chunkRenderEntities = chunkRenderQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var chunkMeshes = GetComponentLookup<ModelMesh>(true);
            chunkRenderEntities.Dispose();            
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, ref ChunkRenderLinks chunkRenderLinks, in ParticleExplosion particleExplosion,
                in Chunk chunk, in VoxLink voxLink) =>
            {
                /*UnityEngine.Debug.DrawLine(particleExplosion.position + math.rotate(particleExplosion.rotation, new float3(0, 0, 1)),
                    particleExplosion.position,
                    UnityEngine.Color.red, 12);*/
                // destroy all chunk renders
                PostUpdateCommands.RemoveComponent<ParticleExplosion>(entityInQueryIndex, e);
                if (chunkRenderLinks.chunkRenders.Length == 0)
                {
                    return;
                }
                var renderEntity = chunkRenderLinks.chunkRenders[0];
                if (!chunkMeshes.HasComponent(renderEntity))
                {
                    return;
                }
                var terrainMesh = chunkMeshes[renderEntity];
                var explosionDelay = particleExplosion.delay;
                var explosionTimeAlive = particleExplosion.aliveTime;

                // particleExplosion.voxelScale.x * 
                //var scaleBegin = particleExplosion.voxelScale.x; // particleExplosion.globalScale.x / particleExplosion.voxelScale.x; // new float3(1f / particleExplosion.scale.x, 1f / particleExplosion.scale.y, 1f / particleExplosion.scale.z);
                var voxelScale = (particleExplosion.voxelScale.x);
                var globalScale = particleExplosion.globalScale.x;
                // for all voxels
                // if not air
                // draw line here (create particle later)
                var voxelDimensions = chunk.voxelDimensions;
                var position = new int3();
                var particleEntities = new NativeList<Entity>();
                // using solid ChunkRender, the first one
                var meshOffset = terrainMesh.position; // terrainMesh.extents + terrainMesh.min; // .extents;
                var midPoint =  voxelDimensions.ToFloat3() / 2f;
                var characterScale = new float3(voxelScale * globalScale, voxelScale * globalScale, voxelScale * globalScale);
                for (position.x = 0; position.x < voxelDimensions.x; position.x++)
                {
                    for (position.y = 0; position.y < voxelDimensions.y; position.y++)
                    {
                        for (position.z = 0; position.z < voxelDimensions.z; position.z++)
                        {
                            var voxelIndex = VoxelUtilities.GetVoxelArrayIndex(position, voxelDimensions);
                            if (!(voxelIndex >= 0 && voxelIndex < chunk.voxels.Length))
                            {
                                return;
                            }
                            var voxelType = chunk.voxels[voxelIndex];
                            if (voxelType != 0)
                            {
                                //UnityEngine.Debug.LogError("");
                                var force = math.normalize(position.ToFloat3() - midPoint);
                                force.z *= -1;
                                force.x *= -1;
                                force = math.rotate(particleExplosion.rotation, force);
                                force *= forceScale;
                                force += forceScale2 * math.normalize(particleExplosion.hitForce);
                                //force += new float3(0, 1.5f, 0);    // go up a little
                                //var voxelPosition = (position.ToFloat3() + new float3(0.5f, 0.5f, 0.5f) ) / (voxelScale / 2f) - meshOffset; // + scaleBegin / 2f;
                                // particleExplosion.globalScale.x * 
                                var voxelPosition = (voxelScale * (position.ToFloat3() + new float3(0.5f, 0.5f, 0.5f) )) - meshOffset; // + scaleBegin / 2f;
                                voxelPosition.z *= -1;
                                voxelPosition.x *= -1;
                                voxelPosition *= globalScale;
                                // grab individual offset of chunk render - create a chunk offset and use that on any chunkRenders to offset the mesh (centre it)
                                voxelPosition = particleExplosion.position + math.rotate(particleExplosion.rotation, voxelPosition);
                                //voxelPosition = particleExplosion.position + voxelPosition;
                                /*UnityEngine.Debug.DrawLine(voxelPosition + force * 0.3f, voxelPosition,
                                    UnityEngine.Color.red, 12);*/
                                var e2 = PostUpdateCommands.Instantiate(entityInQueryIndex, particlePrefab);
                                particleEntities.Add(e2);
                                // PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new NonUniformScale { Value = characterScale });
                                // PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ParticleLifeTime(elapsedTime, explosionTimeAlive));
                                // PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Rotation { Value = particleExplosion.rotation });
                                //PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Rotation { Value = quaternion.identity });
                                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new Translation { Value = voxelPosition });
                                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ParticleLerpPosition
                                {
                                    positionBegin = voxelPosition,
                                    positionEnd = voxelPosition + force * 1f,
                                    delay = explosionDelay
                                });
                                // Shrink Particles
                                PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new ParticleLerpScale(
                                    new float3(voxelScale * globalScale, voxelScale * globalScale, voxelScale * globalScale),
                                    float3.zero, explosionDelay));
                                //PostUpdateCommands.RemoveComponent<ParticleLerpScale>(entityInQueryIndex, e2);
                                if (voxelType - 1 < particleExplosion.colors.Length)
                                {
                                    var color = particleExplosion.colors[voxelType - 1];
                                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new MaterialBaseColor
                                    {
                                        // Value = new float4(color.x, color.y, color.z, 1)
                                        // Value = new float4(color.x / 2f, color.y / 2f, color.z / 2f, 1)
                                        Value = new float4(color.x, color.y, color.z, 1)
                                    });
                                }
                                /*else
                                {
                                    PostUpdateCommands.SetComponent(entityInQueryIndex, e2, new MaterialBaseColor
                                    {
                                        Value = new float4(0, 0, 0, 1)
                                    });
                                }*/
                            }
                        }
                    }
                }
                if (particleEntities.Length > 0)
                {
                    var particleEntitiesArray = particleEntities.AsArray();
                    PostUpdateCommands.AddComponent(entityInQueryIndex, particleEntitiesArray, new NonUniformScale { Value = characterScale });
                    PostUpdateCommands.AddComponent(entityInQueryIndex, particleEntitiesArray, new ParticleLifeTime(elapsedTime, explosionTimeAlive));
                    // PostUpdateCommands.RemoveComponent<DisableRender>(entityInQueryIndex, enableEntitiesArray);
                    particleEntitiesArray.Dispose();
                }
                particleEntities.Dispose();
                // Permanent or temporary destruction of chunk renders
                if (!HasComponent<RevivingEntity>(voxLink.vox))
                {
                    for (int i = 0; i < chunkRenderLinks.chunkRenders.Length; i++)
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, chunkRenderLinks.chunkRenders[i]);
                    }
                    chunkRenderLinks.Dispose();
                }
                else
                {
                    for (int i = 0; i < chunkRenderLinks.chunkRenders.Length; i++)
                    {
                        PostUpdateCommands.AddComponent<RemoveZoxMesh>(entityInQueryIndex, chunkRenderLinks.chunkRenders[i]);
                    }
                }
                particleExplosion.colors.Dispose();
            })	.WithReadOnly(chunkMeshes)
				.ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}