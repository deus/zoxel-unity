using Unity.Entities;

namespace Zoxel.Particles
{
    //! A tag for a particle.
    public struct Particle : IComponentData { }
}