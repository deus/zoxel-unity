using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Particles
{
    // add this to character when dying
    // every turns more of the voxels into particles
    // First scans the chunk model to see how many solids are left
    //! Spawns particles from the entity.
    public struct ParticleSystem : IComponentData
    {
        public int spawnRate;
        public double lastTime;
        public double timeSpawn;
        public double lifeTime;
        public double timeBegun;
        public Color baseColor;
        public float3 colorVariance;
        public float particleLife;
        public float3 positionOffset;
        public float3 spawnSize;
        public float3 positionAdd;
        public Random random;
        public byte hasCollision;
    }
}