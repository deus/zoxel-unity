using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Particles
{
    public struct ParticleLerpPosition : IComponentData
    { 
        public float3 positionBegin;
        public float3 positionEnd;
        public double delay;

        public ParticleLerpPosition(float3 positionBegin, float3 positionEnd, double delay = 0)
        {
            this.positionBegin = positionBegin;
            this.positionEnd = positionEnd;
            this.delay = delay;
        }
    }
}