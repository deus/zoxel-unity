using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Particles
{
    public struct ParticleLerpColor : IComponentData
    { 
        public float4 colorBegin;
        public float4 colorEnd;
        public double animationTime;
        public double delay;

        public ParticleLerpColor(float4 colorBegin, float4 colorEnd, double animationTime, double delay = 0)
        {
            this.colorBegin = colorBegin;
            this.colorEnd = colorEnd;
            this.animationTime = animationTime;
            this.delay = delay;
        }
    }
}