using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Particles
{
    public struct ParticleLerpAlpha : IComponentData
    { 
        public float alphaBegin;
        public float alphaEnd;
        public double animationTime;
        public double delay;

        public ParticleLerpAlpha(float alphaBegin, float alphaEnd, double animationTime, double delay = 0)
        {
            this.alphaBegin = alphaBegin;
            this.alphaEnd = alphaEnd;
            this.animationTime = animationTime;
            this.delay = delay;
        }
    }
}