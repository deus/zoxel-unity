using Unity.Entities;

namespace Zoxel.Particles
{
    //! A particles life time consists of it's time born and time alive.
    public struct ParticleLifeTime : IComponentData
    { 
        public double timeBorn;
        public double timeAlive;

        public ParticleLifeTime(double timeBorn, double timeAlive)
        {
            this.timeBorn = timeBorn;
            this.timeAlive = timeAlive;
        }
    }
}