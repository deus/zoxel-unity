using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Particles
{
    //! Causes vox model to have a particle explosion!
    public struct ParticleExplosion : IComponentData
    {
        public double createdTime;
        public double aliveTime;
        public double delay;
        public float lifeTime;
        public float3 hitForce;
        public float3 voxelScale;
        public float3 globalScale;
        public float3 position;
        public quaternion rotation;
        [ReadOnly] public BlitableArray<float3> colors;
    }
}