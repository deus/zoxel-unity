using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Particles
{
    public struct ParticleLerpScale : IComponentData
    { 
        public float3 scaleBegin;
        public float3 scaleEnd;
        public double delay;

        public ParticleLerpScale(float3 scaleBegin, float3 scaleEnd, double delay = 0)
        {
            this.scaleBegin = scaleBegin;
            this.scaleEnd = scaleEnd;
            this.delay = delay;
        }
    }
}