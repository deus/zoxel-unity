using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Particles
{
    public struct RainParticle : IComponentData { public float3 lastPosition; }
}