using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Particles
{
    //! Handles when a particle dies by destroying it.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ParticleDeathSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities.ForEach((Entity e, int entityInQueryIndex, in ParticleLifeTime particleLifeTime) =>
            {
                var timePassed = elapsedTime - particleLifeTime.timeBorn;
                if (timePassed >= particleLifeTime.timeAlive)
                {
                    PostUpdateCommands.DestroyEntity(entityInQueryIndex, e);
                }
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}