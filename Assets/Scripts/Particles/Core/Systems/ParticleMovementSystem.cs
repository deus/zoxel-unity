using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Animations;

namespace Zoxel.Particles
{
    //! Moves particle over it's lifetime.
    [BurstCompile, UpdateInGroup(typeof(ParticleSystemGroup))]
    public partial class ParticleMovementSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            Entities.ForEach((ref Translation position, in ParticleLerpPosition particleLerp, in ParticleLifeTime particleLifeTime) =>
            {
                var timePassed = elapsedTime - particleLifeTime.timeBorn;
                if (timePassed >= particleLerp.delay)
                {
                    timePassed -= particleLerp.delay;
                    var timeAlive = particleLifeTime.timeAlive - particleLerp.delay;
                    var timeLerp = (float)(timePassed / timeAlive);
                    position.Value = math.lerp(particleLerp.positionBegin, particleLerp.positionEnd, timeLerp);
                }
            }).ScheduleParallel();
        }
    }
}