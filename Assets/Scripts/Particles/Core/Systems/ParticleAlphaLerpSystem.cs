using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Animations;

namespace Zoxel.Particles
{
    //! Color's particle over it's lifetime.
    [BurstCompile, UpdateInGroup(typeof(ParticleSystemGroup))]
    public partial class ParticleAlphaLerpSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            Entities.ForEach((ref MaterialBaseColor materialBaseColor, in ParticleLerpAlpha particleLerpAlpha, in ParticleLifeTime particleLifeTime) =>
            {
                var timePassed = elapsedTime - particleLifeTime.timeBorn;
                if (timePassed >= particleLerpAlpha.delay)
                {
                    timePassed -= particleLerpAlpha.delay;
                    var timeAlive = particleLerpAlpha.animationTime - particleLerpAlpha.delay;
                    var timeLerp = (float)(timePassed / timeAlive);
                    timeLerp = math.clamp(timeLerp, 0, 1);
                    var colorBegin = new float4(materialBaseColor.Value.x, materialBaseColor.Value.y, materialBaseColor.Value.z, particleLerpAlpha.alphaBegin);
                    var colorEnd = new float4(colorBegin.x, colorBegin.y, colorBegin.z, particleLerpAlpha.alphaEnd);
                    materialBaseColor.Value = math.lerp(colorBegin, colorEnd, timeLerp);
                }
            }).ScheduleParallel();
        }
    }
}