using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Animations;

namespace Zoxel.Particles
{
    //! When rain collides with the voxels, it splooshes.
    [BurstCompile, UpdateInGroup(typeof(ParticleSystemGroup))]
    public partial class RainVoxelCollisionSystem : SystemBase
    {
		public EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<BodyForce>()
                .WithNone<DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref Translation translation, ref RainParticle rainParticle, in BasicVoxelCollider basicVoxelCollider, in NonUniformScale scale) =>
            {
                if (basicVoxelCollider.hitVoxelType != 0)
                {
                    translation.Value = rainParticle.lastPosition; //  - new float3(0, scale.Value.y * 4, 0);
                    PostUpdateCommands.RemoveComponent<ParticleLerpPosition>(entityInQueryIndex, e);
                    PostUpdateCommands.RemoveComponent<BasicVoxelCollider>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new DestroyEntityInTime(elapsedTime, 1.4f));
                    // ParticleLerpPosition Still moves position after this
                    // Movement should possibly go after everything else?
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e, translation);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new LerpScale(elapsedTime, 1.4f, scale.Value,
                        new float3(scale.Value.x, 0, scale.Value.z)));
                    PostUpdateCommands.AddComponent(entityInQueryIndex, e, new LerpPosition(elapsedTime, 1.4f, translation.Value,
                        translation.Value + new float3(0, - scale.Value.y / 2f, 0)));
                    // UnityEngine.Debug.LogError("Scale Diff: " + (scale.Value.y * 4) + " ---Pos Diff: " + (rainParticle.lastPosition - translation.Value));
                    // UnityEngine.Debug.DrawLine(translation.Value, rainParticle.lastPosition, UnityEngine.Color.red, 4f);
                }
                else
                {
                    rainParticle.lastPosition = translation.Value;
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
    }
}