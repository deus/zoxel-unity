using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Animations;

namespace Zoxel.Particles
{
    //! Color's particle over it's lifetime.
    [BurstCompile, UpdateInGroup(typeof(ParticleSystemGroup))]
    public partial class ParticleColorLerpSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            Entities.ForEach((ref MaterialBaseColor materialBaseColor, in ParticleLerpColor particleLerpColor, in ParticleLifeTime particleLifeTime) =>
            {
                var timePassed = elapsedTime - particleLifeTime.timeBorn;
                if (timePassed >= particleLerpColor.delay)
                {
                    timePassed -= particleLerpColor.delay;
                    var timeAlive = particleLerpColor.animationTime - particleLerpColor.delay;
                    var timeLerp = (float)(timePassed / timeAlive);
                    timeLerp = math.clamp(timeLerp, 0, 1);
                    materialBaseColor.Value = math.lerp(particleLerpColor.colorBegin, particleLerpColor.colorEnd, timeLerp);
                }
            }).ScheduleParallel();
        }
    }
}