using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Animations;

namespace Zoxel.Particles
{
    //! Scales particle over it's lifetime.
    [BurstCompile, UpdateInGroup(typeof(ParticleSystemGroup))]
    public partial class ParticleScaleSystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            Entities.ForEach((ref NonUniformScale scale, in ParticleLerpScale particleLerp, in ParticleLifeTime particleLifeTime) =>
            {
                var timePassed = elapsedTime - particleLifeTime.timeBorn;
                if (timePassed >= particleLerp.delay)
                {
                    timePassed -= particleLerp.delay;
                    var timeAlive = particleLifeTime.timeAlive - particleLerp.delay;
                    var timeLerp = (float)(timePassed / timeAlive);
                    scale.Value = math.lerp(particleLerp.scaleBegin, particleLerp.scaleEnd, timeLerp);
                }
            }).ScheduleParallel();
        }
    }
}