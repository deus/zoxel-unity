using Unity.Entities;
using System;
using System.IO;

namespace Zoxel.Classes
{
    //! Loads a characters class.
    /**
    *   \todo Refactor like I do with Race.
    *   - Load System -
    */
    [UpdateInGroup(typeof(ClassSystemGroup))]
    public partial class ClassLoadSystem : SystemBase
    {
        private const string filename = "Class.zox";
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<InitializeSaver, DisableSaving>()
                .WithAll<LoadClass>()
                .ForEach((Entity e, ref ClassLink classLink, in EntitySaver entitySaver, in ZoxID zoxID, in RealmLink realmLink) =>
            {
                /*if (classLink.classs.Index == 0)
                {
                    return;
                }*/
                PostUpdateCommands.RemoveComponent<LoadClass>(e);
                var directoryPath = entitySaver.GetPath();
                if (!Directory.Exists(directoryPath))
                {
                    UnityEngine.Debug.LogError("Save Directory did not exist: " + directoryPath);
                    return;
                }
                var filePath = directoryPath + filename;
                if (!File.Exists(filePath))
                {
                    // UnityEngine.Debug.LogError(" Directory did not exist: " + directoryPath);
                    // Character does not have class!
                    return;
                }
                try
                {
                    var dataID = int.Parse(File.ReadAllText(filePath));
                    var classLinks = EntityManager.GetComponentData<ClassLinks>(realmLink.realm);
                    classLink.classs = classLinks.GetData(EntityManager, dataID);
                }
                catch (Exception exception)
                {
                    UnityEngine.Debug.LogError("Exception Loading Class: " + exception);
                }
            }).WithoutBurst().Run();
        }
    }
}