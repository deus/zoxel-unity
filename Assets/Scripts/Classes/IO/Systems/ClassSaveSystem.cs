using Unity.Entities;
using System;
using System.IO;

namespace Zoxel.Classes
{
    //! Saves UserClass data!
    [UpdateInGroup(typeof(ClassSystemGroup))]
    public partial class ClassSaveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }
        
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            Entities
                .WithNone<DeadEntity, DestroyEntity>()
                .WithNone<InitializeSaver, DisableSaving>()
                .WithAll<SaveClass>()
                .ForEach((Entity e, in EntitySaver entitySaver, in ClassLink classLink) =>
            {
                /*if (classLink.classs.Index == 0)
                {
                    return;
                }*/
                PostUpdateCommands.RemoveComponent<SaveClass>(e);
                var directoryPath = entitySaver.GetPath();
                if (!Directory.Exists(directoryPath))
                {
                    UnityEngine.Debug.LogError("Save Directory did not exist: " + directoryPath);
                    return;
                }
                var filePath = directoryPath + "Class.zox";
                try
                {
                    var classID = 0;
                    if (classLink.classs.Index > 0)
                    {
                        classID = EntityManager.GetComponentData<ZoxID>(classLink.classs).id;
                    }
                    File.WriteAllText(filePath, classID.ToString());
                }
                catch (Exception exception)
                {
                    UnityEngine.Debug.LogError("Exception Saving Class: " + exception);
                }
            }).WithoutBurst().Run();
        }
    }
}