using Unity.Entities;

namespace Zoxel.Classes
{
    //! Set a new class.
    public struct SetClass : IComponentData
    {
        public Entity classs;

        public SetClass(Entity classs)
        {
            this.classs = classs;
        }
    }
}