using Unity.Entities;

namespace Zoxel.Classes
{
    //! Links to a class entity.
    public struct ClassLink : IComponentData
    {
        public Entity classs;

        public ClassLink(Entity classs)
        {
            this.classs = classs;
        }
    }
}