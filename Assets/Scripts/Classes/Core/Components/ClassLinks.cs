using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Classes
{
    //! Links to race meta datas.
    public struct ClassLinks : IComponentData
    {
        public BlitableArray<Entity> data;

        public void Dispose()
        {
            if (data.Length > 0)
            {
                data.Dispose();
            }
        }

        public void Initialize(int count)
        {
            data = new BlitableArray<Entity>(count, Allocator.Persistent);
        }

        public Entity GetData(EntityManager EntityManager, int targetID)
        {
            for (int i = 0; i < data.Length; i++)
            {
                if (EntityManager.GetComponentData<ZoxID>(data[i]).id == targetID)
                {
                    return data[i];
                }
            }
            return new Entity();
        }
    }
}