using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.Skills;

namespace Zoxel.Classes
{
    //! Handles Class changes on a character.
    [BurstCompile, UpdateInGroup(typeof(ClassSystemGroup))]
    public partial class ClassSetSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery classesQuery;
        private EntityQuery skillsQuery;
        private EntityQuery userSkillsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            classesQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Class>());
            skillsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<Skill>());
            userSkillsQuery = GetEntityQuery(
                ComponentType.Exclude<DestroyEntity>(),
                ComponentType.ReadOnly<UserSkill>());
            RequireForUpdate(processQuery);
            RequireForUpdate(classesQuery);
            RequireForUpdate(skillsQuery);
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var userSkillPrefab = SkillSystemGroup.userSkillPrefab;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<SetClass>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var classEntities = classesQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var skilltreeLinks = GetComponentLookup<SkilltreeLink>(true);
            classEntities.Dispose();
            var skilltreeEntities = userSkillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var skilltrees = GetComponentLookup<Skilltree>(true);
            skilltreeEntities.Dispose();
            var skilltreeNodeEntities = skillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleC);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleC);
            var skilltreeNodes = GetComponentLookup<SkilltreeNode>(true);
            var startingSkilltreeNodes = GetComponentLookup<StartingSkilltreeNode>(true);
            skilltreeNodeEntities.Dispose();
            var userSkillEntities = userSkillsQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleD);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleD);
            var userSkillMetaDatas = GetComponentLookup<MetaData>(true);
            userSkillEntities.Dispose();
			Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
				.WithNone<DestroyEntity, InitializeEntity, OnUserSkillSpawned>()
				.ForEach((Entity e, int entityInQueryIndex, ref UserSkillLinks userSkillLinks, ref ClassLink classLink, in SetClass setClass) =>
			{
                if (classLink.classs == setClass.classs)
                {
                    return;
                }
                // Remove old starting userSkillLinks
                if (skilltreeLinks.HasComponent(classLink.classs))
                {
                    var skilltreeEntity = skilltreeLinks[classLink.classs].skilltree;
                    var skilltree = skilltrees[skilltreeEntity];
                    for (int i = 0; i < skilltree.nodes.Length; i++)
                    {
                        var skilltreeNodeEntity = skilltree.nodes[i];
                        var skilltreeNode = skilltreeNodes[skilltreeNodeEntity];
                        var skillEntity = skilltreeNode.skill;
                        // remove any user skill with this data
                        for (int j = 0; j < userSkillLinks.skills.Length; j++)
                        {
                            var userSkillEntity = userSkillLinks.skills[j];
                            if (!userSkillMetaDatas.HasComponent(userSkillEntity))
                            {
                                // UnityEngine.Debug.LogError("userSkillEntity broken: " + j + " : " + userSkillEntity.Index + " - t " + userSkillLinks.skills.Length);
                                continue;
                            }
                            var userSkillMetaData = userSkillMetaDatas[userSkillEntity];
                            if (userSkillMetaData.data == skillEntity)
                            {
                                // UnityEngine.Debug.LogError("Destroying userSkillEntity: " + j + " - t " + userSkillLinks.skills.Length);
                                userSkillLinks.RemoveAt(j);
                                PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, userSkillEntity);
                                break;
                            }
                        }
                    }
                    // userSkillLinks.Dispose();
                }
                classLink.classs = setClass.classs;
                // Add new starting userSkillLinks
                if (skilltreeLinks.HasComponent(classLink.classs))
                {
                    //! Uses skills leftover as starting index.
                    byte addedSkills = (byte) userSkillLinks.skills.Length; // 0;
                    var skilltreeEntity = skilltreeLinks[classLink.classs].skilltree;
                    var skilltree = skilltrees[skilltreeEntity];
                    for (int i = 0; i < skilltree.nodes.Length; i++)
                    {
                        var skilltreeNodeEntity = skilltree.nodes[i];
                        if (startingSkilltreeNodes.HasComponent(skilltreeNodeEntity))
                        {
                            var skilltreeNode = skilltreeNodes[skilltreeNodeEntity];
                            var skillEntity = skilltreeNode.skill;
                            var userSkillEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, userSkillPrefab);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new MetaData(skillEntity));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new UserDataIndex(addedSkills));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, userSkillEntity, new CharacterLink(e));
                            addedSkills++;
                        }
                    }
                    if (addedSkills > 0)
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, e, new OnUserSkillSpawned(addedSkills));
                    }
                }
                if (!HasComponent<DisableSaving>(e))
                {
                    PostUpdateCommands.AddComponent<SaveSkills>(entityInQueryIndex, e);
                }
                // UnityEngine.Debug.LogError("Class Changed: " + classLink.classs.Index);
			})  .WithReadOnly(skilltreeLinks)
                .WithReadOnly(userSkillMetaDatas)
                .WithReadOnly(skilltrees)
                .WithReadOnly(skilltreeNodes)
                .WithReadOnly(startingSkilltreeNodes)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
		}
	}
}