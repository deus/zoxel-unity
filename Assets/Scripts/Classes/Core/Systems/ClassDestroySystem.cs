/*using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Classes
{
    //! Cleans up Classes.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class ClassDestroySystem : SystemBase
	{
        [BurstCompile]
		protected override void OnUpdate()
		{
			Entities
				.WithAll<DestroyEntity, Class>()
				.ForEach((in Class class2) =>
			{
                class2.Dispose();
			}).ScheduleParallel();
		}
	}
}*/