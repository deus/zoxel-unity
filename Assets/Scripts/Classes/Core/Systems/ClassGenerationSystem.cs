using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Skills;

namespace Zoxel.Classes
{
    //! Generates classes.
    /**
    *   - Data Generation System -
    *   \todo Convert to Parallel.
    */
    [BurstCompile, UpdateInGroup(typeof(ClassSystemGroup))]
    public partial class ClassGenerationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity classPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var classArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(Class),
                typeof(SkilltreeLink),
                typeof(ZoxID),
                typeof(ZoxName),
                typeof(ZoxDescription));
            classPrefab = EntityManager.CreateEntity(classArchetype);
            #if UNITY_EDITOR
            EntityManager.AddComponent<EditorName>(classPrefab); // EntityManager.AddComponentData(classPrefab, new EditorName("[class]"));
            #endif
        }

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); //.AsParallelWriter();
            Entities.ForEach((Entity e, ref GenerateRealm generateRealm, ref ClassLinks classLinks, in SkillLinks skillLinks, in SkilltreeLinks skilltreeLinks, in ZoxID zoxID) =>
            {
                if (generateRealm.state != (byte)GenerateRealmState.GenerateClasses)
                {
                    return; // wait for thingo
                }
                var realmSeed = zoxID.id;
                if (realmSeed == 0)
                {
                    return;
                }
                PostUpdateCommands.AddComponent<GenerateRealmVoxels>(e);
                generateRealm.state = (byte) GenerateRealmState.GenerateVoxels;
                // Destroy UserSkillLinks and SkillTrees
                for (int i = 0; i < classLinks.data.Length; i++)
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(classLinks.data[i]);
                }
                classLinks.Dispose();
                var punchLvl1Entity = skillLinks.skills[0];
                var fireballLvl1Entity = skillLinks.skills[1];
                var stabLvl1Entity = skillLinks.skills[7];
                // Spawn Skill Entities
                var dataCount = 7;
                classLinks.Initialize(dataCount);
                for (int i = 0; i < dataCount; i++)
                {
                    var skilltreeIndex = i;
                    var skilltreeEntity = skilltreeLinks.skilltrees[skilltreeIndex];
                    var e2 = EntityManager.Instantiate(classPrefab);
                    PostUpdateCommands.SetComponent(e2, new SkilltreeLink(skilltreeEntity));
                    classLinks.data[i] = e2;
                    var dataID = realmSeed + 2000 + i * 1;
                    // var classData = new Class();
                    // classData.startingSkills = new BlitableArray<Entity>(1, Allocator.Persistent);
                    var random = new Random();
                    random.InitState((uint) dataID);
                    PostUpdateCommands.SetComponent(e2, new ZoxID(dataID));
                    var name = new Text();
                    var description = new Text();
                    if (i <= 5)
                    {
                        if (i == 0)
                        {
                            name = new Text("Warrior");
                            description.SetText("A strong melee focused fighter.");
                            // classData.startingSkills[0] = punchLvl1Entity;
                        }
                        else if (i == 1)
                        {
                            name = new Text("Mage");
                            description.SetText("A powerful ranged mana focused fighter.");
                            // classData.startingSkills[0] = fireballLvl1Entity;
                        }
                        else if (i == 2)
                        { 
                            name = new Text("Rogue");
                            description.SetText("A silent ambush focused assassin.");
                            // classData.startingSkills[0] = stabLvl1Entity;
                        }
                        else if (i == 3)
                        { 
                            name = new Text("Summoner");
                            description.SetText("A magic class based on summoning creatures from the void.");
                            // classData.startingSkills[0] = fireballLvl1Entity;
                        }
                        else if (i == 4)
                        { 
                            name = new Text("Engineer");
                            description.SetText("Someone who uses the power of magicraft.");
                            // classData.startingSkills[0] = punchLvl1Entity;
                        }
                        else if (i == 5)
                        {
                            name = new Text("Healer");
                            description.SetText("An apostle of the northern god.");
                            // classData.startingSkills[0] = fireballLvl1Entity;
                        }
                    }
                    else
                    {
                        name = NameGenerator.GenerateName2(ref random);
                        description.SetText("A class without rules.");
                        // classData.startingSkills[0] = punchLvl1Entity;
                    }
                    PostUpdateCommands.SetComponent(e2, new ZoxName(name));
                    PostUpdateCommands.SetComponent(e2, new ZoxDescription(description));
                    // set class Data
                    // PostUpdateCommands.SetComponent(e2, classData);
                    #if UNITY_EDITOR
                    var editorName = "[class] " + name.ToString();
                    PostUpdateCommands.SetComponent(e2, new EditorName(editorName));
                    #endif
                }
            }).WithStructuralChanges().Run();   // spawns skill tree and skill data
        }
    }
}
                    /*if (i == 1 || i == 2)
                    {
                        skilltreeIndex = i;
                    }*/