using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Realms.UI;

namespace Zoxel.Networking.UI.RealmsUI
{
    //! Handles MainMenuButton's UIClickEvent's.
    /**
    *   - UIClickEvent System -
    */
    /*[BurstCompile, UpdateInGroup(typeof(NetSystemUIGroup))]
    public partial class ServerUIClickSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;
        private Entity closeButtonPrefab;
        private NativeArray<Text> texts;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            uiHoldersQuery = GetEntityQuery(
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<UILink>());
            var texts2 = new NativeList<Text>();
            texts2.Add(new Text("Connecting"));
            texts2.Add(new Text("zoxel.duckdns.org")); //  my friend
            texts = texts2.ToArray(Allocator.Persistent);
            texts2.Dispose();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            PopupUISpawnSystem.InitializePrefabs(EntityManager);
            InitializePrefabs();
            var spawnPopupUIPrefab = PopupUISpawnSystem.spawnPanelPrefab;
            var spawnMainMenuPrefab = MainMenuSpawnSystem.spawnPanelPrefab;
            var spawnChatUIPrefab = ChatUISpawnSystem.spawnPanelPrefab;
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var closeButtonPrefab = this.closeButtonPrefab;
            var texts = this.texts;
            var connectToLobbyPrefab = ConnectingToLobbySystem.connectToLobbyPrefab;
            var localAddress = NetSystemGroup.localAddress;
            var lobbyAddress = NetSystemGroup.lobbyAddress;
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;   // move to ui system group
            var popupUIPrefabs = PopupUISpawnSystem.popupUIPrefabs;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uiHolderEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
            uiHolderEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<Toggle>()
                .WithAll<ServerLoadButton>()
                .ForEach((Entity e, int entityInQueryIndex, in Child child, in PanelLink panelLink, in UIClickEvent uiClickEvent) =>
            {
                var controllerEntity = uiClickEvent.controller;
                var panelEntity = panelLink.panel;
                if (panelEntity.Index == 0 || !HasComponent<UILink>(controllerEntity) || uiClickEvent.buttonType != ButtonActionType.Confirm)
                {
                    return;
                }
                if (child.index == 0)
                {
                    var removeUIPrefab =  PostUpdateCommands.Instantiate(entityInQueryIndex, removeCharacterUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, removeUIPrefab, new RemoveUI(controllerEntity, panelEntity));
                    var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnMainMenuPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new CharacterLink(controllerEntity));
                }
                else if (child.index == 1)
                {
                    //! \todo Delete Server
                }
                //! Connects to a selected server
                else if (child.index == 3)
                {
                    var removeUIPrefab =  PostUpdateCommands.Instantiate(entityInQueryIndex, removeCharacterUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, removeUIPrefab, new RemoveUI(controllerEntity, panelEntity));
                    // spawn connecting ui popup!
                    // closing this popup will cancel connecting event
                    var connectEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, connectToLobbyPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, connectEntity, new EntityNetAddress(localAddress));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, connectEntity, new LocalNetAddress(localAddress));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, connectEntity, new TargetNetAddress(lobbyAddress));
                    PostUpdateCommands.AddComponent(entityInQueryIndex, connectEntity, new UIHolderLink(controllerEntity));
                    PostUpdateCommands.AddComponent(entityInQueryIndex, connectEntity, new PlayerLink(controllerEntity));

                    // link ui to connecting event
                    var connectingHeaderText = texts[0].Clone();
                    var connectingCoreText = texts[1].Clone();
                    var popupUI = PopupUISpawnSystem.SpawnPopupUI(PostUpdateCommands, entityInQueryIndex, in popupUIPrefabs, closeButtonPrefab,
                        controllerEntity, in cameraLinks, uiSpawnedEventPrefab, connectingHeaderText, connectingCoreText);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, connectEntity, new PopupUILink(popupUI));
                }
			})  .WithReadOnly(cameraLinks)
                .WithReadOnly(texts)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private void InitializePrefabs()
        {
            if (closeButtonPrefab.Index != 0)
            {
                return;
            }
            closeButtonPrefab = EntityManager.Instantiate(UICoreSystem.closeButtonPrefab);
            EntityManager.AddComponent<Prefab>(closeButtonPrefab);
            EntityManager.AddComponent<ConnectingToServerButton>(closeButtonPrefab);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }
    }*/
}

                   /* var spawnPopupUIEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnPopupUIPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnPopupUIEntity, new CharacterLink(controllerEntity));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnPopupUIEntity,
                        new SpawnPopupUI(texts[0].Clone(), connectingToServerText, closeButtonPrefab));*/
                    // spawn chat ui after popup closes
                    // var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnChatUIPrefab);
                    // PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new CharacterLink(controllerEntity));