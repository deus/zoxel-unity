using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Transforms;
using Zoxel.Realms.UI;

namespace Zoxel.Networking.UI.RealmsUI
{
    [BurstCompile, UpdateInGroup(typeof(NetSystemUIGroup))]
    public partial class ConnectingToLobbyUIEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var spawnChatUIPrefab = ChatUISpawnSystem.spawnPanelPrefab;
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // Cancel Connecting
            var spawnServerUIPrefab = ServerUISpawnSystem.spawnPanelPrefab;
            var connectEntity = NetSystemGroup.connectEntity;
            Dependency = Entities
                .WithAll<CloseButton, ConnectingToServerButton>()
                .ForEach((Entity e, int entityInQueryIndex, in PanelLink panelLink, in UIClickEvent uiClickEvent) =>
            {
                var controllerEntity = uiClickEvent.controller;
                var panelEntity = panelLink.panel;
                if (panelEntity.Index == 0 || !HasComponent<UILink>(controllerEntity) || uiClickEvent.buttonType != ButtonActionType.Confirm)
                {
                    return;
                }
                if (HasComponent<ConnectingToLobby>(connectEntity))
                {
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, connectEntity);
                }
                var removeUIPrefab =  PostUpdateCommands.Instantiate(entityInQueryIndex, removeCharacterUIPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, removeUIPrefab, new RemoveUI(controllerEntity, panelEntity));
                var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnServerUIPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new CharacterLink(controllerEntity));
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // Finished Connecting!
            Dependency = Entities
                .WithAll<ConnectedToLobbySuccess, ConnectingToLobby>()
                .ForEach((int entityInQueryIndex, in UIHolderLink uiHolderLink, in PopupUILink popupUILink) =>
            {
                // close connecting ui (link to connecting entity)
                var removeUIPrefab =  PostUpdateCommands.Instantiate(entityInQueryIndex, removeCharacterUIPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, removeUIPrefab, new RemoveUI(uiHolderLink.holder, popupUILink.ui));
                var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnChatUIPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new CharacterLink(uiHolderLink.holder));
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}