using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Transforms;

namespace Zoxel.Networking.UI
{
    //! Handles ChatButton's UIClickEvent's.
    /**
    *   - UIClickEvent System -
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemUIGroup))]
    public partial class ChatUICloseDisconnectSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var spawnServerUIPrefab = ServerUISpawnSystem.spawnPanelPrefab;
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<CloseButton, DisconnectedFromServerButton>()
                .ForEach((Entity e, int entityInQueryIndex, in PanelLink panelLink, in UIClickEvent uiClickEvent) =>
            {
                var controllerEntity = uiClickEvent.controller;
                var panelEntity = panelLink.panel;
                if (panelEntity.Index == 0 || !HasComponent<UILink>(controllerEntity) || uiClickEvent.buttonType != ButtonActionType.Confirm)
                {
                    return;
                }
                var removeUIPrefab =  PostUpdateCommands.Instantiate(entityInQueryIndex, removeCharacterUIPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, removeUIPrefab, new RemoveUI(controllerEntity, panelEntity));
                var spawnEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnServerUIPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnEntity, new CharacterLink(controllerEntity));
			}).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}