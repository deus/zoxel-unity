// ServerUI > ChatUI (with players tab)
using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.UI;

namespace Zoxel.Networking.UI
{
    //! Spawns a ServerUI.
    /**
    *   - UI Spawn System -
    *   \todo Event for AddUILine, will add in a button.
    *   \todo Align UI Elements from the bottom up, instead of a grid.
    *   \todo Use a mask shader to hide those that go outside of panel bounds.
    *   \todo Add a footer InputField for inputting chat.
    *   \todo Add a [X] button on top right of window to exit chat and go back to main menu.
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemUIGroup))]
    public partial class ServerUISpawnSystem : SystemBase
    {
        private static bool isTesting;
        private NativeArray<Text> texts;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity spawnPanelPrefab;
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;
        private Entity panelPrefab;
        private Entity togglePrefab;
        private Entity controlButtonPrefab;
        private Entity controlPanelPrefab;
        private float2 panelSize;

        /*protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            uiHoldersQuery = GetEntityQuery(
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<UILink>());
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnServerUI),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnPanelPrefab = EntityManager.CreateEntity(spawnArchetype);
            var texts2 = new NativeList<Text>();
            texts2.Add(new Text("Servers"));
            texts2.Add(new Text("zoxel.duckdns.org"));
            texts2.Add(new Text("woxel.duckdns.org"));
            texts = texts2.ToArray(Allocator.Persistent);
            texts2.Dispose();
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }*/

        [BurstCompile]
        protected override void OnUpdate()
        {
            /*InitializePrefabs();
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var headerStyle = uiDatam.headerStyle.GetStyle(uiScale);
            var panelSize = this.panelSize;
            var controlsLabels = UICoreSystem.controlsLabels;
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;   // move to ui system group
            HeaderSpawnSystem.InitializePrefabs(EntityManager);
            var headerPrefab = HeaderSpawnSystem.headerPrefab;
            var panelPrefab = this.panelPrefab;
            var togglePrefab = this.togglePrefab;
            var controlPanelPrefab = this.controlPanelPrefab;
            var controlButtonPrefab = this.controlButtonPrefab;
            var texts = this.texts;
            var servers = SaveUtilities.GetServers();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uiHolderEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
            uiHolderEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<SpawnServerUI>()
                .ForEach((int entityInQueryIndex, in CharacterLink characterLink) =>
            {
                //! \todo Spawn as 'pixel' size.
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, panelPrefab);
                // Header UI
                var headerText = texts[0].Clone();
                HeaderSpawnSystem.SpawnHeaderUI(PostUpdateCommands, entityInQueryIndex, headerPrefab, e3, panelSize,
                    headerStyle.fontSize, headerStyle.textPadding, in headerText);
                // Spawn ControlPanel
                var controlPanelEntity = FooterUISpawnSystem.SpawnFooterUI(PostUpdateCommands, entityInQueryIndex, controlPanelPrefab, e3);
                UICoreSystem.SetPanelLink(PostUpdateCommands, entityInQueryIndex, controlPanelEntity, e3);
                PostUpdateCommands.SetComponent(entityInQueryIndex, controlPanelEntity, new SpawnButtonsList(in controlsLabels));
                // Spawn Control panel on it & spawn ui list
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawned((byte) (texts.Length - 1)));
                for (int i = 1; i < texts.Length; i++)
                {
                    var buttonEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, togglePrefab, e3);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, buttonEntity, new Child(i - 1));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, buttonEntity, new RenderText(texts[i].Clone()));
                }
                if (characterLink.character.Index > 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, characterLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab),
                        new OnUISpawned(characterLink.character, 1));
                    if (cameraLinks.HasComponent(characterLink.character))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLinks[characterLink.character]);
                    }
                }
			})  .WithReadOnly(texts)
                .WithReadOnly(controlsLabels)
                .WithReadOnly(cameraLinks)
                .ScheduleParallel(Dependency);*/
        }

        private void InitializePrefabs()
        {
            if (panelPrefab.Index != 0)
            {
                return;
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var buttonStyle = uiDatam.buttonStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var buttonFontSize = buttonStyle.fontSize * 0.3f;
            var buttonTextPadding = buttonStyle.textPadding * 0.6f;
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            var iconSize = uiDatam.GetIconSize(uiScale) * 0.64f;
            var margins = uiDatam.GetIconMargins(uiScale);
            var padding = uiDatam.GetIconPadding(uiScale);
            var headerStyle = uiDatam.headerStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var gridUI = new GridUI();
            gridUI.gridSize = new int2(1, 8); // texts.Length - 1);
            gridUI.margins = 0.2f * new float2(buttonStyle.fontSize, buttonStyle.fontSize);
            gridUI.padding = padding;
            var gridUISize = new GridUISize(new float2(buttonFontSize * 24 + buttonTextPadding.x * 2f, buttonFontSize + buttonTextPadding.y * 2f));
            panelSize = gridUI.GetSize(gridUISize.size);
            // panel
            panelPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(panelPrefab);
            EntityManager.RemoveComponent<SpawnHeaderUI>(panelPrefab);
            EntityManager.AddComponent<ServerUI>(panelPrefab);
            EntityManager.SetComponentData(panelPrefab, new PanelUI(PanelType.ServerUI));
            EntityManager.SetComponentData(panelPrefab, gridUI);
            EntityManager.SetComponentData(panelPrefab, gridUISize);
            EntityManager.SetComponentData(panelPrefab, new Size2D(panelSize));
            // add toggle group parts
            EntityManager.AddComponentData(panelPrefab, new ToggleGroup(255));
            EntityManager.AddComponent<SetToggleGroup>(panelPrefab);
            // control panel
            // controlPanelPrefab = (UICoreSystem.controlPanelFooterPrefab);
            controlPanelPrefab = EntityManager.Instantiate(UICoreSystem.controlPanelFooterPrefab);
            EntityManager.AddComponent<Prefab>(controlPanelPrefab);
            // button
            togglePrefab = EntityManager.Instantiate(UICoreSystem.togglePrefab);
            EntityManager.AddComponent<Prefab>(togglePrefab);
            EntityManager.AddComponent<ServerLoadButton>(togglePrefab);
            UICoreSystem.SetRenderTextData(EntityManager, togglePrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, buttonFontSize, buttonTextPadding);
            // control button
            controlButtonPrefab = EntityManager.Instantiate(UICoreSystem.controlButtonPrefab);
            EntityManager.AddComponent<Prefab>(controlButtonPrefab);
            EntityManager.AddComponent<ServerLoadButton>(controlButtonPrefab);
            // link list to element prefab
            EntityManager.AddComponentData(panelPrefab, new ListPrefabLink(togglePrefab));
            EntityManager.SetComponentData(controlPanelPrefab, new ListPrefabLink(controlButtonPrefab));
            // testing, keep here for now until I finish testing it
            //      - this spawns it at origin
            if (ServerUISpawnSystem.isTesting)
            {
                EntityManager.SetComponentData(panelPrefab, new Translation { Value = new float3(0, 0, 0.06f) });
                EntityManager.SetComponentData(panelPrefab, new Rotation { Value = new quaternion(new float4(0, 0, 0, -1f)) });
                EntityManager.RemoveComponent<OrbitTransform>(panelPrefab);
                EntityManager.RemoveComponent<OrbitPosition>(panelPrefab);
            }
            //! \todo This should be done dynamically! Refactor UIElement data.
            // EntityManager.AddComponent<SetUIElementPosition>(togglePrefab);
        }

        public static void TestUI(EntityManager EntityManager)
        {
            EntityManager.SetComponentData(spawnPanelPrefab, new DelayEvent(0, 3));
            EntityManager.Instantiate(spawnPanelPrefab);
            ServerUISpawnSystem.isTesting = true;
        }
    }
}
            /*texts2.Add(new Text("zixel.duckdns.org"));
            texts2.Add(new Text("toxel.duckdns.org"));*/