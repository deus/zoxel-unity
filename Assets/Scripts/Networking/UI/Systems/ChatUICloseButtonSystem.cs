using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Transforms;

namespace Zoxel.Networking.UI
{
    //! Handles ChatButton's UIClickEvent's.
    /**
    *   - UIClickEvent System -
    */
    /*[BurstCompile, UpdateInGroup(typeof(NetSystemUIGroup))]
    public partial class ChatUICloseButtonSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private Entity closeButtonPrefab;
        private NativeArray<Text> texts;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var texts2 = new NativeList<Text>();
            texts2.Add(new Text("Goodbye"));
            texts2.Add(new Text("You have left the server."));
            texts = texts2.ToArray(Allocator.Persistent);
            texts2.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var localNetPlayer = NetSystemGroup.localNetPlayer;
            var spawnPopupUIPrefab = PopupUISpawnSystem.spawnPanelPrefab;
            var spawnServerUIPrefab = ServerUISpawnSystem.spawnPanelPrefab;
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var closeButtonPrefab = this.closeButtonPrefab;
            var texts = this.texts;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<CloseButton, ChatButton>()
                .ForEach((Entity e, int entityInQueryIndex, in PanelLink panelLink, in UIClickEvent uiClickEvent) =>
            {
                var controllerEntity = uiClickEvent.controller;
                var panelEntity = panelLink.panel;
                if (panelEntity.Index == 0 || !HasComponent<UILink>(controllerEntity) || uiClickEvent.buttonType != ButtonActionType.Confirm)
                {
                    return;
                }
                if (HasComponent<NetPlayer>(localNetPlayer))
                {
                    PostUpdateCommands.AddComponent<LeaveLobby>(entityInQueryIndex, localNetPlayer);
                }
                var removeUIPrefab =  PostUpdateCommands.Instantiate(entityInQueryIndex, removeCharacterUIPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, removeUIPrefab, new RemoveUI(controllerEntity, panelEntity));
                var spawnPopupUIEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnPopupUIPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnPopupUIEntity, new CharacterLink(controllerEntity));
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnPopupUIEntity, new SpawnPopupUI(texts[0].Clone(), texts[1].Clone(), closeButtonPrefab));
			})  .WithReadOnly(texts)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private void InitializePrefabs()
        {
            if (closeButtonPrefab.Index != 0)
            {
                return;
            }
            closeButtonPrefab = EntityManager.Instantiate(UICoreSystem.closeButtonPrefab);
            EntityManager.AddComponent<Prefab>(closeButtonPrefab);
            EntityManager.AddComponent<DisconnectedFromServerButton>(closeButtonPrefab);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }
    }*/
}