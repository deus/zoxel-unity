using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Rendering;
using Zoxel.UI;

namespace Zoxel.Networking.UI
{
    //! Spawns a MusicUI.
    /**
    *   - UI Spawn System -
    *   \todo Event for AddUILine, will add in a button.
    *   \todo Align UI Elements from the bottom up, instead of a grid.
    *   \todo Use a mask shader to hide those that go outside of panel bounds.
    *   \todo Add a footer InputField for inputting chat.
    *   \todo Add a [X] button on top right of window to exit chat and go back to main menu.
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemUIGroup))]
    public partial class ChatUISpawnSystem : SystemBase
    {
        private static bool isTesting;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery uiHoldersQuery;
        public static Entity spawnPanelPrefab;
        private Entity panelPrefab;
        private Entity headerPrefab;
        private Entity buttonPrefab;
        private Entity closeButtonPrefab;
        private NativeArray<Text> texts;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            uiHoldersQuery = GetEntityQuery(
                ComponentType.ReadOnly<CameraLink>(),
                ComponentType.ReadOnly<UILink>());
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnChatUI),
                typeof(CharacterLink),
                typeof(DelayEvent),
                typeof(GenericEvent));
            spawnPanelPrefab = EntityManager.CreateEntity(spawnArchetype);
            var texts2 = new NativeList<Text>(Allocator.Temp);
            texts2.Add(new Text("[chat]"));
            texts2.Add(new Text("X"));
            // texts2.Add(new Text("[peter] Hello my friend."));
            texts2.Add(new Text("[zoxel] Welcome to Zoxel Server!")); //  my friend
            texts = texts2.ToArray(Allocator.Persistent);
            texts2.Dispose();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            HeaderSpawnSystem.InitializePrefabs(EntityManager);
            var uiDatam = UIManager.instance.uiDatam;
            var headerStyle = uiDatam.headerStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var texts = this.texts;
            var uiSpawnedEventPrefab = UISystemGroup.uiSpawnedEventPrefab;   // move to ui system group
            var panelPrefab = this.panelPrefab;
            var headerPrefab = this.headerPrefab;
            var closeButtonPrefab = this.closeButtonPrefab;
            var buttonPrefab = this.buttonPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var uiHolderEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
			var cameraLinks = GetComponentLookup<CameraLink>(true);
            uiHolderEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DelayEvent>()
                .WithAll<SpawnChatUI>()
                .ForEach((int entityInQueryIndex, in CharacterLink characterLink) =>
            {
                var e3 = PostUpdateCommands.Instantiate(entityInQueryIndex, panelPrefab);
                // Spawn Header
                var headerText = texts[0].Clone();
                var headerEntity = HeaderSpawnSystem.SpawnHeaderUI(PostUpdateCommands, entityInQueryIndex, headerPrefab, e3, in headerText);
                PostUpdateCommands.AddComponent(entityInQueryIndex, headerEntity, new PanelLink(e3));
                // Spawn Footer with Text Input
                // Create close button
                var closeText = texts[1].Clone();
                var closeButtonEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, closeButtonPrefab, headerEntity);
                PostUpdateCommands.SetComponent(entityInQueryIndex, closeButtonEntity, new RenderText(closeText));
                PostUpdateCommands.AddComponent(entityInQueryIndex, closeButtonEntity, new PanelLink(e3));
                PostUpdateCommands.AddComponent<Childrens>(entityInQueryIndex, headerEntity);
                PostUpdateCommands.AddComponent(entityInQueryIndex, headerEntity, new OnChildrenSpawned((byte) 1));

                // Spawn texts
                var buttonEntity = UICoreSystem.SpawnElement(PostUpdateCommands, entityInQueryIndex, buttonPrefab, e3);
                var noteText = texts[2].Clone();
                PostUpdateCommands.SetComponent(entityInQueryIndex, buttonEntity, new RenderText(noteText));
                PostUpdateCommands.SetComponent(entityInQueryIndex, e3, new OnChildrenSpawned((byte) 1));
                if (characterLink.character.Index > 0)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex, e3, characterLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, PostUpdateCommands.Instantiate(entityInQueryIndex, uiSpawnedEventPrefab),
                        new OnUISpawned(characterLink.character, 1));
                    if (cameraLinks.HasComponent(characterLink.character))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, e3, cameraLinks[characterLink.character]);
                    }
                }
			})  .WithReadOnly(texts)
                .WithReadOnly(cameraLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }

        private void InitializePrefabs()
        {
            if (panelPrefab.Index != 0)
            {
                return;
            }
            var uiScale = UIManager.instance.uiSettings.uiScale;
            var uiDatam = UIManager.instance.uiDatam;
            var buttonStyle = uiDatam.buttonStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var headerStyle = uiDatam.headerStyle.GetStyle(UIManager.instance.uiSettings.uiScale);
            var buttonFontSize = buttonStyle.fontSize * 0.3f;
            var buttonTextPadding = buttonStyle.textPadding * 0.6f;
            var textPadding = uiDatam.GetMenuPaddings(uiScale);
            var padding = uiDatam.GetIconPadding(uiScale);
            var gridUI = new GridUI();
            gridUI.gridSize = new int2(1, 8); // texts.Length - 1);
            gridUI.margins = 0.2f * new float2(buttonStyle.fontSize, buttonStyle.fontSize);
            gridUI.padding = padding;
            var gridUISize = new GridUISize(new float2(buttonFontSize * 24 + buttonTextPadding.x * 2f, buttonFontSize + buttonTextPadding.y * 2f));
            var panelSize = gridUI.GetSize(gridUISize.size) * 1.4f;
            gridUI.gridSize.y = 1;
            // panel
            panelPrefab = EntityManager.Instantiate(UICoreSystem.realmPanelPrefab);
            EntityManager.AddComponent<Prefab>(panelPrefab);
            EntityManager.RemoveComponent<SpawnHeaderUI>(panelPrefab);
            EntityManager.AddComponent<ChatUI>(panelPrefab);
            EntityManager.SetComponentData(panelPrefab, new PanelUI(PanelType.ChatUI));
            EntityManager.SetComponentData(panelPrefab, gridUI);
            EntityManager.SetComponentData(panelPrefab, gridUISize);
            EntityManager.SetComponentData(panelPrefab, new Size2D(panelSize));
            EntityManager.AddComponent<DisableGridResize>(panelPrefab);
            // header
            var headerHeight = headerStyle.fontSize + headerStyle.textPadding.y * 2f;
            var headerSize = new float2(panelSize.x, headerHeight);
            var headerPosition = new float3(0, 0.5f * panelSize.y + headerSize.y / 2f, 0);
            headerPrefab = EntityManager.Instantiate(HeaderSpawnSystem.headerPrefab);
            EntityManager.AddComponent<Prefab>(headerPrefab);
            EntityManager.SetComponentData(headerPrefab, new LocalPosition(headerPosition));
            EntityManager.SetComponentData(headerPrefab, new Size2D(headerSize));
            // close button
            closeButtonPrefab = EntityManager.Instantiate(UICoreSystem.closeButtonPrefab);
            EntityManager.AddComponent<Prefab>(closeButtonPrefab);
            EntityManager.AddComponent<ChatButton>(closeButtonPrefab);
            EntityManager.SetComponentData(closeButtonPrefab, new LocalPosition(new float3(panelSize.x * 0.44f, 0, 0)));
            // button
            buttonPrefab = EntityManager.Instantiate(UICoreSystem.buttonPrefab);
            EntityManager.AddComponent<Prefab>(buttonPrefab);
            UICoreSystem.SetRenderTextData(EntityManager, buttonPrefab, buttonStyle.textGenerationData,
                buttonStyle.textColor, buttonStyle.textOutlineColor, buttonStyle.fontSize * 0.3f, buttonStyle.textPadding * 0.6f);
            // EntityManager.AddComponent<IgnoreGrid>(buttonPrefab);
            // EntityManager.AddComponent<SetUIElementPosition>(buttonPrefab);
            // testing, keep here for now until I finish testing it
            //      - this spawns it at origin
            if (ChatUISpawnSystem.isTesting)
            {
                EntityManager.SetComponentData(panelPrefab, new Translation { Value = new float3(0, 0, 0.06f) });
                EntityManager.SetComponentData(panelPrefab, new Rotation { Value = new quaternion(new float4(0, 0, 0, -1f)) });
                // EntityManager.SetComponentData(panelPrefab, new UIAnchor(AnchorUIType.None));
                EntityManager.RemoveComponent<OrbitTransform>(panelPrefab);
                EntityManager.RemoveComponent<OrbitPosition>(panelPrefab);
            }
        }

        public static void TestUI(EntityManager EntityManager)
        {
            EntityManager.SetComponentData(spawnPanelPrefab, new DelayEvent(0, 3));
            EntityManager.Instantiate(spawnPanelPrefab);
            ChatUISpawnSystem.isTesting = true;
        }
    }
}
            /*EntityManager.AddComponent<CloseButton>(closeButtonPrefab);
            EntityManager.SetComponentData(closeButtonPrefab, new RenderQueue(UICoreSystem.iconQueue + 20));
            EntityManager.AddComponent<SetUIElementPosition>(closeButtonPrefab);*/