using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Zoxel.UI;
using Zoxel.Transforms;

namespace Zoxel.Networking.UI
{
    //! Handles ChatButton's UIClickEvent's.
    /**
    *   - UIClickEvent System -
    */
    /*[BurstCompile, UpdateInGroup(typeof(NetSystemUIGroup))]
    public partial class ChatUIDisconnectSystem : SystemBase
    {
        private Entity closeButtonPrefab;
        private NativeArray<Text> texts;
        private EntityQuery processQuery;
        private EntityQuery netPlayersQuery;
        private EntityQuery uiHoldersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var texts2 = new NativeList<Text>();
            texts2.Add(new Text("Disconnected"));
            texts2.Add(new Text("You have disconnected from server."));
            texts = texts2.ToArray(Allocator.Persistent);
            texts2.Dispose();
            netPlayersQuery = GetEntityQuery(ComponentType.ReadOnly<NetPlayer>());
            uiHoldersQuery = GetEntityQuery(ComponentType.ReadOnly<UILink>());
            RequireForUpdate(processQuery);
            RequireForUpdate(netPlayersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            InitializePrefabs();
            var localNetPlayer = NetSystemGroup.localNetPlayer;
            var spawnPopupUIPrefab = PopupUISpawnSystem.spawnPanelPrefab;
            var spawnServerUIPrefab = ServerUISpawnSystem.spawnPanelPrefab;
            var removeCharacterUIPrefab = UICoreSystem.removeCharacterUIPrefab;
            var closeButtonPrefab = this.closeButtonPrefab;
            var texts = this.texts;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var playerLinks = GetComponentLookup<PlayerLink>(true);
            netPlayerEntities.Dispose();
            var uiHolderEntities = uiHoldersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var uiLinks = GetComponentLookup<UILink>(true);
            uiHolderEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<LeaveLobby>()
                .WithAll<DestroyEntity, Lobby>()
                .ForEach((Entity e, int entityInQueryIndex, in NetPlayerLinks netPlayerLinks) =>
            {
                // for each net player
                for (var i = 0; i < netPlayerLinks.players.Length; i++)
                {
                    var netPlayerEntity = netPlayerLinks.players[i];
                    if (HasComponent<LocalNetPlayer>(netPlayerEntity))
                    {
                        var playerEntity = playerLinks[netPlayerEntity].player;
                        var uiLink = uiLinks[playerEntity];
                        for (byte j = 0; j < uiLink.uis.Length; j++)
                        {
                            var uiEntity = uiLink.uis[i];
                            if (HasComponent<ChatUI>(uiEntity))
                            {
                                var removeUIPrefab =  PostUpdateCommands.Instantiate(entityInQueryIndex, removeCharacterUIPrefab);
                                PostUpdateCommands.SetComponent(entityInQueryIndex, removeUIPrefab, new RemoveUI(playerEntity, uiEntity));
                                break;
                            }
                        }
                        // get chatUIs of playerEntity and Remove it

                        var spawnPopupUIEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, spawnPopupUIPrefab);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, spawnPopupUIEntity, new CharacterLink(playerEntity));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, spawnPopupUIEntity, new SpawnPopupUI(texts[0].Clone(), texts[1].Clone(), closeButtonPrefab));
                    }
                }
			})  .WithReadOnly(texts)
                .WithReadOnly(playerLinks)
                .WithReadOnly(uiLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        private void InitializePrefabs()
        {
            if (closeButtonPrefab.Index != 0)
            {
                return;
            }
            closeButtonPrefab = EntityManager.Instantiate(UICoreSystem.closeButtonPrefab);
            EntityManager.AddComponent<Prefab>(closeButtonPrefab);
            EntityManager.AddComponent<DisconnectedFromServerButton>(closeButtonPrefab);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < texts.Length; i++)
            {
                texts[i].Dispose();
            }
            texts.Dispose();
        }
    }*/
}