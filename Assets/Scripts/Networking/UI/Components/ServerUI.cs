using Unity.Entities;
    
namespace Zoxel.Networking.UI
{
    //! A UI where you can join a server.
    public struct ServerUI : IComponentData { }
}