using Unity.Entities;
    
namespace Zoxel.Networking.UI
{
    //! A spawn event, to spawn a ChatUI.
    public struct SpawnChatUI : IComponentData { }
}