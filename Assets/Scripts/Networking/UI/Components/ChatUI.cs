using Unity.Entities;
    
namespace Zoxel.Networking.UI
{
    //! A UI where you can chat to your friends.
    public struct ChatUI : IComponentData { }
}