using Unity.Entities;
    
namespace Zoxel.Networking.UI
{
    //! A spawn event, to spawn a ServerUI.
    public struct SpawnServerUI : IComponentData { }
}