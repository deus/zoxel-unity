


            /*
            Synch Rooms when new player joins
            for (int i = 0; i < lobby.rooms.Count; i++)
            {
                var room = lobby.rooms[i];
                SendPacketToPlayer("[addroom]" + room.GetData(EntityManager), newPlayerEntity, lobby.port); // localEndPoint.Port);
            }*/
            // remove any rooms they have
            /*for (int j = 0; j < lobby.rooms.Count; j++)
            {
                var room = lobby.rooms[j];
                // if host, remove room from lobby rooms
                if (room.players[0] == playerEntity)
                {
                    // remove their hosted room
                    HostRemoveLobbyRoom(room);
                    Debug.Log("  - Player was Hosting a room. Removing room: " + room.name);
                    break;
                }
            }*/
            // ===== RoOmS =====
            // --- Rooms ---
            /*else if (message.Contains("[roommessage]"))
            {
                message = message.Replace("[roommessage]", "");
                var split = message.Split(':');
                var roomName = split[0];
                message = split[1];
                var room = GetRoom(roomName);
                if (message[0] != '[')
                {
                    AddRoomMessage(message);
                }
                RoomBroadcastMessage(room, message, localEndPoint.Port);
            }
            else if (message.Contains("[closeroom]"))
            {
                message = message.Replace("[closeroom]", "");
                var roomName = message;
                var removeRoomMessage = "[removeroom]" + roomName;
                for (int i = 0; i < lobby.players.Count; i++)
                {
                    SendPacketToPlayer(removeRoomMessage, lobby.players[i], localEndPoint.Port);
                }
            }
            else if (message.Contains("[createroom]"))
            {
                // room data
                message = message.Replace("[createroom]", "");
                ServerOnClientCreatedRoom(message);
            }
            else if (message.Contains(joinRoomMessage))
            {
                message = message.Replace(joinRoomMessage, "");
                ServerOnClientJoinedRoom(message);
            }
            
            else if (message.Contains("[addroom]"))
            {
                message = message.Replace("[addroom]", "");
                var newRoom = new ZoxRoom();
                newRoom.SetData(EntityManager, message, lobby.players);
                lobby.AddRoom(newRoom);
            }

            // Add Player to room inside of
            else if (message.Contains(addRoomPlayerMessage))
            {
                message = message.Replace(addRoomPlayerMessage, "");
                var split = message.Split(':');
                var roomName = split[0];    // which room to join
                var playerName = split[1];    // which room to join
                var newPlayer = GetLobbyPlayer(playerName); //new ZoxPlayer();
                AddRoomPlayer(roomName, newPlayer);
            }
            else if (message.Contains(confirmJoinRoomMessage))
            {
                ConfirmJoinedRoom();
            }
            else if (message.Contains("[leaveroom]"))
            {
                message = message.Replace("[leaveroom]", "");
                var split = message.Split(':');
                var roomName = split[0];    // which room to join
                var playerName = split[1];    // which room to join
                RemoveRoomPlayer(roomName, playerName); // remoteEndPoint.Address, remoteEndPoint.Port);
                LobbyBroadcastPacket("[removeroomplayer]" + roomName + ":" + playerName);
            }
            // client message
            else if (message.Contains("[removeroomplayer]"))
            {
                message = message.Replace("[removeroomplayer]", "");
                var split = message.Split(':');
                var roomName = split[0];    // which room to join
                var playerName = split[1];    // which room to join
                RemoveRoomPlayer(roomName, playerName); // remoteEndPoint.Address, remoteEndPoint.Port);
            }
            // sent from host leaving room
            else if (message.Contains("[removeroom]"))
            {
                message = message.Replace("[removeroom]", "");
                var roomName = message;
                if (room.connectedState == 2 && room.name == roomName)
                {
                    room.connectedState = (byte) ConnectedState.None;   // leave room
                    room.Clear();
                }
                RemoveLobbyRoom(roomName);
            }*/
            /*else
            {
                AddRoomMessage(message);
            }*/
        // ROOM
        /*
        const string addRoomPlayerMessage = "[addroomplayer]";
        const string joinRoomMessage = "[joinroom]";
        const string confirmJoinRoomMessage = "[confirmjoinroom]";

        public void SetRoom(ZoxRoom room)
        {
            for (int i = 0; i < lobby.rooms.Count; i++)
            {
                if (lobby.rooms[i].name == room.name)
                {
                    lobby.rooms[i] = room;
                    return;
                }
            }
            Debug.Log("COULD NOT FIND " + room.name + " SetRoom in lobby.");
            //return new ZoxRoom();
        }

        public void AddRoomMessage(string message)
        {
            room.messages.Insert(0, message);
            if (room.messages.Count >= maxMessages)
            {
                room.messages.RemoveAt(room.messages.Count - 1);
            }
        }

        public void RoomBroadcastMessage(ZoxRoom room, string message, int port)
        {
            Debug.Log("Lobby: Room Broadcast Message to room players: " + (room.players.Count - 1));
            for (int i = 0; i < room.players.Count; i++)
            {
                var player = room.players[i];
                if (player != netPlayerEntity)
                {
                    SendPacketToPlayer(message, player, port);
                }
            }
        }

        void ServerOnClientCreatedRoom(string message)
        {
            var split = message.Split(':');
            var roomName = split[0];
            var playerName = split[1];
            //Debug.Log(remoteEndPoint.Port.ToString() + " at " + localEndPoint.Address + "::" + localEndPoint.Port
            //    + " is Creating a New Room [" + roomName + "]");  // one room for now
            // get player
            var clientCreatedRoom = GetLobbyPlayer(playerName);
            // shouldn't this be added per client in lobby
            var newRoom = new ZoxRoom();
            newRoom.Initialize();
            newRoom.name = roomName;
            newRoom.Add(EntityManager, clientCreatedRoom);
            lobby.AddRoom(newRoom); // add room to lobby
            var addroommessage = "[addroom]" + newRoom.GetData(EntityManager);
            //var addplayermessage = "[addroomplayer]" + roomName + ":" + playerName;
            for (int i = 0; i < lobby.players.Count; i++)
            {
                var otherPlayerEntity = lobby.players[i];
                if (!EntityManager.HasComponent<LocalNetPlayer>(otherPlayerEntity))
                {
                    SendPacketToPlayer(addroommessage, otherPlayerEntity); // , localEndPoint.Port);
                }
            }
        }

        // On Lobby Host
        public void ServerOnClientJoinedRoom(string message)
        {
            var split = message.Split(':');
            var roomName = split[0];    // which room to join
            var playerName = split[1];    // which room to join
            var netPlayerEntity = GetLobbyPlayer(playerName);
            var netPlayer = EntityManager.GetComponentData<NetPlayer>(netPlayerEntity);
            AddRoomPlayer(roomName, netPlayerEntity);
            var room = ZoxNet.instance.GetRoom(roomName);
            // to all previous players - send new player - as an add player command
            var addRoomPlayerPacket = addRoomPlayerMessage + roomName + ":" + playerName;
            for (int i = 0; i < lobby.players.Count; i++)
            {
                var otherPlayerEntity = lobby.players[i];
                if (!EntityManager.HasComponent<LocalNetPlayer>(otherPlayerEntity))
                {
                    SendPacketToPlayer(addRoomPlayerPacket, otherPlayerEntity); // , localEndPoint.Port);
                }
            }
            SendPacketToPlayer(confirmJoinRoomMessage, netPlayerEntity); // localEndPoint.Port, remoteEndPoint.Address.ToString(), remoteEndPoint.Port);
            Debug.Log("Player [" + playerName + "] has joined Room [" + roomName + "]");
            // Debug.Log(remoteEndPoint.Port.ToString() + " at " + localEndPoint.Address + "::" + localEndPoint.Port + " is joining room [" + roomName + "]");
        }

        // when player leaves the room
        public void LeaveRoom()
        {
            if (LocalPlayerHost)    //roomListener != null)
            {
                EntityManager.RemoveComponent<LobbyHost>(netPlayerEntity);
                // netPlayerEntity.isHost = false;
                HostRemoveLobbyRoom(room);
            }
            else
            {
                Debug.Log("Leaving Room " + room.name);
                // for all clients, room that player
                RemoveRoomPlayer(room.name, NetPlayerName);
                if (IsLobbyHost())
                {
                    LobbyBroadcastPacket("[removeroomplayer]" + room.name + ":" + NetPlayerName);
                }
                else
                {
                    SendPacketToLobby("[leaveroom]" + room.name + ":" + NetPlayerName);
                }
            }
            room.connectedState = 0;
            room.Clear();
        }

        public void HostRemoveLobbyRoom(ZoxRoom room)
        {
            Debug.Log("Closing Room " + room.name);
            RemoveLobbyRoom(room);
            SendPacketToLobby("[closeroom]" + room.name);
        }

        public void ConfirmJoinedRoom()
        {
            if (room.connectedState == 1)
            {
                Debug.Log("Confirm Joined Room " + room.name);
                room.messages.Clear();
                room.connectedState = 2;
            }
        }

        public void RemoveLobbyRoom(ZoxRoom room)
        {
            Debug.Log("Removing Room: " + room.name);
            lobby.RemoveRoom(room);
        }

        public void RemoveLobbyRoom(string roomName)
        {
            Debug.Log("Removing Room: " + roomName);
            lobby.RemoveRoom(roomName);
        }

        public ZoxRoom GetRoom(string name)
        {
            for (int i = 0; i < lobby.rooms.Count; i++)
            {
                if (lobby.rooms[i].name == name)
                {
                    return lobby.rooms[i];
                }
            }
            Debug.Log("COULD NOT FIND " + name + " Room in lobby.");
            return new ZoxRoom();
        }
        public void AddRoomPlayer(string roomName, Entity newPlayer)
        {
            Debug.Log("Adding New Room Player: " + newPlayer.Index);
            for (int i = 0; i < lobby.rooms.Count; i++)
            {
                var thatRoom = lobby.rooms[i];
                if (thatRoom.name == roomName)
                {
                    thatRoom.Add(EntityManager, newPlayer);
                    break;
                }
            }
            if (room.connectedState == 2 && roomName == room.name)
            {
                room.Add(EntityManager, newPlayer);
            }
        }

        public void RemoveRoomPlayer(string roomName, string playerName)
        {
            var roomIndex = - 1;
            for (int i = 0; i < lobby.rooms.Count; i++)
            {
                if (lobby.rooms[i].name == roomName)
                {
                    roomIndex = i;
                    break;
                }
            }
            if (roomIndex == -1)
            {
                Debug.Log("Could not find room to leave: " + roomName);
                return;
            }
            // make sure to update current room with updated room
            //      quick fix for when messages get cleared when someone leaves room
            for (int i = 0; i < lobby.rooms.Count; i++)
            {
                if (lobby.rooms[i].name == room.name)
                {
                    lobby.rooms[i] = room;
                    break;
                }
            }

            var thatRoom = lobby.rooms[roomIndex];
            var removeIndex = -1;
            for (int i = 0; i < thatRoom.players.Count; i++)
            {
                if (EntityManager.GetComponentData<ZoxName>(thatRoom.players[i]).name.ToString() == playerName)
                {
                    removeIndex = i;
                    break;
                }
            }
            if (removeIndex != -1)
            {
                Debug.Log("RemoveRoomPlayer Player [" + playerName + "] from room. At Index: " + removeIndex);
                thatRoom.Remove(removeIndex);
            }
            else
            {
                Debug.Log("Player " + playerName + " was not in room.");
            }
            lobby.rooms[roomIndex] = thatRoom;
            if (room.connectedState == 2 && room.name == thatRoom.name)
            {
                room = thatRoom;
            }
        }
        */