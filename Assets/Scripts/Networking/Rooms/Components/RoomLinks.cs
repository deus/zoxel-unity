using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Networking
{
    // Lobby contains a bunch of rooms
    public struct RoomLinks : IComponentData
    {
        public BlitableArray<Entity> rooms;
        
        public void DisposeRoomLinks()
        {
            if (rooms.Length > 0)
            {
                rooms.Dispose();
            }
        }
    }
}