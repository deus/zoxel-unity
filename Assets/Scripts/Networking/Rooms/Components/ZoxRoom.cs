/*using Unity.Entities;
using Unity.Mathematics;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Zoxel.Voxels;
using Zoxel.AI;
using Zoxel.UI;
using Zoxel.Cameras;
using Zoxel.Worlds;

    
namespace Zoxel.Networking
{
    public struct ZoxRoom // : IComponentData
    {
        public byte connectedState;
        public string name;
        public List<Entity> players;
        // public List<ZoxPing> pings;
        public List<string> messages;

        public void Initialize()
        {
            players = new List<Entity>();
            // pings = new List<ZoxPing>();
            messages = new List<string>();
        }

        public void Clear()
        {
            name = "";
            Initialize();
        }

        public string GetData(EntityManager EntityManager)
        {
            var data = name;
            for (int i = 0; i < players.Count; i++)
            {
                data += ":" + EntityManager.GetComponentData<ZoxID>(players[i]).id;
            }
            return data;
        }

        public void SetData(EntityManager EntityManager, string data, List<Entity> players2)
        {
            Initialize();
            var split = data.Split(':');
            name = split[0];
            for (int i = 1; i < split.Length; i++)
            {
                var playerID = int.Parse(split[i]);
                for (int j = 0; j < players2.Count; j++)
                {
                    var playerEntity = players2[j];
                    if (EntityManager.GetComponentData<ZoxID>(playerEntity).id == playerID)
                    {
                        players.Add(playerEntity);
                        // pings.Add(new ZoxPing());
                        break;
                    }
                }
            }
        }

        public void Add(EntityManager EntityManager, Entity newPlayer)
        {
        }

        public void Remove(int index)
        {
            players.RemoveAt(index);
            // pings.RemoveAt(index);
        }

        public static bool operator ==(ZoxRoom a, ZoxRoom b)
        {
            return a.name == b.name;
        }

        public static bool operator !=(ZoxRoom a, ZoxRoom b)
        {
            return a.name != b.name;
        }
    }
}*/
            /*var split = data.Split(':');
            if (split.Length != 5)
            {
                UnityEngine.Debug.LogError("Cannot set player data as wrong amount of split strings: "+  data);
                return;
            }
            name = split[0];
            ip = IPAddress.Parse(split[1]);
            port = int.Parse(split[2]);
            localIP = IPAddress.Parse(split[3]);
            localPort = int.Parse(split[4]);*/
            /*for (int i = 0; i < players.Count; i++)
            {
                if (players[i].name == newPlayer.name)
                {
                    UnityEngine.Debug.LogError("Duplicate Add Room");
                    return;
                }
            }
            players.Add(newPlayer);
            pings.Add(new ZoxPing());*/