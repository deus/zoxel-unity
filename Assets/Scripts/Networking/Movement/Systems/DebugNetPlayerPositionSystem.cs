using Unity.Entities;
using Unity.Burst;
using Unity.Mathematics;
using Zoxel.Networking.Authoring;
using Zoxel.Transforms;
using Zoxel.Lines;
// todo: Generic Timer Events for reconnecting

namespace Zoxel.Networking
{
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class DebugNetPlayerPositionSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate<NetworkSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var networkSettings = GetSingleton<NetworkSettings>();
            if (!networkSettings.isDebugPlayers)
            {
                return;
            }
            var elapsedTime = World.Time.ElapsedTime;
            var linePrefab = RenderLineGroup.linePrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var debugSize = 0.32f * (new float3(1, 2, 1));
            Entities
                .WithAll<NetPlayer>()
                .ForEach((Entity e, in Translation translation) =>
            {
                // UnityEngine.Debug.LogError(e.Index + " - Netplayer is at position: " + translation.Value);
                RenderLineGroup.CreateCubeLines(PostUpdateCommands, linePrefab, elapsedTime, translation.Value, quaternion.identity, debugSize, 1); 
            }).WithoutBurst().Run();
        }
    }
}