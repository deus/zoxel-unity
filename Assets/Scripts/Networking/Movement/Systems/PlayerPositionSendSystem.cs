using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Lines;

namespace Zoxel.Networking
{
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class NetPlayerAddTranslationSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            processQuery = GetEntityQuery(
                ComponentType.Exclude<Translation>(),
                ComponentType.ReadOnly<NetPlayer>());
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AddComponent<Translation>(processQuery);
        }
    }

    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class PlayerPositionSendSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery lobbyQuery;
        private EntityQuery netPlayersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            lobbyQuery = GetEntityQuery(ComponentType.ReadOnly<Lobby>());
            netPlayersQuery = GetEntityQuery(ComponentType.ReadOnly<NetPlayer>());
            RequireForUpdate(processQuery);
            RequireForUpdate(lobbyQuery);
            RequireForUpdate(netPlayersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).RemoveComponent<UpdateNetPlayerPosition>(processQuery);
            var lobbyEntities = lobbyQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var lobbys = GetComponentLookup<EntityNetAddress>(true);
            var netPlayerLinks = GetComponentLookup<NetPlayerLinks>(true);
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var netAddresses = GetComponentLookup<EntityNetAddress>(true);
            lobbyEntities.Dispose();
            netPlayerEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<UpdateNetPlayerPosition>()
                .ForEach((Entity e, in Translation translation, in EntityNetAddress entityNetAddress, in ZoxID zoxID, in LobbyLink lobbyLink) =>
            {
                if (!lobbys.HasComponent(lobbyLink.lobby))
                {
                    return;
                }
                var lobbyEntity = lobbyLink.lobby;
                var lobby = lobbys[lobbyEntity].netAddress;
                var bytes = new BlitableArray<byte>(17, Allocator.Temp);
                bytes[0] = PacketType.PositionSynch;
                ByteUtil.SetInt(ref bytes, 1, zoxID.id);
                ByteUtil.SetFloat3(ref bytes, 5, translation.Value);
                var bytesArray = bytes.ToArray();
                var netPlayerLinks2 = netPlayerLinks[lobbyEntity];
                // for lan only - others will need to route through the lobby
                NetSendUtil.LobbyBroadcastPacket(in bytesArray, in lobby, in netPlayerLinks2, e, in netAddresses);
                //UnityEngine.Debug.LogError("Sending Position!");
                bytes.Dispose();
            })  .WithReadOnly(lobbys)
                .WithReadOnly(netPlayerLinks)
                .WithReadOnly(netAddresses)
                .WithoutBurst().Run();
        }
    }
}