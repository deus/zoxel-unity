using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;

namespace Zoxel.Networking
{
    //! Reads ping packets and adds events to NetPlayer entities.
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class PlayerPositionRecieveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery netPlayersQuery;
        private EntityQuery lobbyQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            netPlayersQuery = GetEntityQuery(
                ComponentType.ReadOnly<NetPlayer>(),
                ComponentType.ReadWrite<Translation>());
            lobbyQuery = GetEntityQuery(ComponentType.ReadOnly<Lobby>());
            RequireForUpdate(processQuery);
            RequireForUpdate(netPlayersQuery);
            RequireForUpdate(lobbyQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var lobbyEntities = lobbyQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var netPlayerLinks = GetComponentLookup<NetPlayerLinks>(true);
            lobbyEntities.Dispose();
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var netPlayerPositions = GetComponentLookup<Translation>(false);
            var lobbyLinks = GetComponentLookup<LobbyLink>(true);
            netPlayerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved>()
                .ForEach((Entity e, int entityInQueryIndex, in EntityNetAddress entityNetAddress, in PacketData packetData, in NetPlayerLink netPlayerLink) =>
            {
                if (packetData.data.Length > 0 && packetData.data[0] == PacketType.PositionSynch)
                {
                    var updatedID = ByteUtil.GetInt(in packetData.data, 1);
                    float3 output;
                    ByteUtil.GetFloat3(in packetData.data, 5, out output);
                    var localPlayerEntity = netPlayerLink.player;
                    if (localPlayerEntity.Index > 0 && lobbyLinks.HasComponent(localPlayerEntity))
                    {
                        var lobbyEntity = lobbyLinks[localPlayerEntity].lobby;
                        var netPlayerLinks2 = netPlayerLinks[lobbyEntity];
                        var playerEntity = netPlayerLinks2.Get(updatedID);
                        if (playerEntity.Index > 0 && netPlayerPositions.HasComponent(playerEntity))
                        {
                            var position = netPlayerPositions[playerEntity];
                            position.Value = output;
                            netPlayerPositions[playerEntity] = position;
                        }
                    }
                }
            })  .WithReadOnly(netPlayerLinks)
                .WithReadOnly(lobbyLinks)
                .WithNativeDisableContainerSafetyRestriction(netPlayerPositions)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                    // var didFindPlayer = playerEntity.Index > 0;
                    /*if (!didFindPlayer)
                    {
                        UnityEngine.Debug.LogError("Could not find player of ID!"); // + updatedID);
                    }*/
                    // var didFindPlayer = false;
                    /*for (int i = 0; i < netPlayerLinks2.players.Length; i++)
                    {
                        var playerEntity = netPlayerLinks2.players[i];
                        var playerID = netPlayerIDs[playerEntity];
                        if (playerID.id == updatedID)
                        {
                            // set position
                            var position = netPlayerPositions[playerEntity];
                            position.Value = output;
                            netPlayerPositions[playerEntity] = position;
                            didFindPlayer = true;
                            break;
                        }
                    }*/
                    // send position back to other players that this wasn't send from