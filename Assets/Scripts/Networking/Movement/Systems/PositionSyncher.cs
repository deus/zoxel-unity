using Unity.Entities;
using Unity.Mathematics;
using System.Collections;
using UnityEngine;
using Zoxel.Transforms;
using Zoxel.Players;
//! \todo Give Character a NetPlayerLink. Use that to send packets to server to stream it's transform.

namespace Zoxel.Networking
{
    //! For now sends translation data from local Character to NetPlayer.
    public class PositionSyncher : MonoBehaviour
    {
        public static PositionSyncher instance;
        private float lastSentPosition;
        private float sendPositionTiming = 0.2f;

        public EntityManager EntityManager
        {
            get
            {
                if (World.DefaultGameObjectInjectionWorld == null)
                {
                    return new EntityManager();
                }
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        void Awake()
        {
            instance = this;
        }

        void Update()
        {
            if (World.DefaultGameObjectInjectionWorld == null)
            {
                return;
            }
            ClientUpdatePosition();
        }

        void ClientUpdatePosition()
        {
            if (Time.time - lastSentPosition >= sendPositionTiming)
            {
                if (!EntityManager.Exists(PlayerSystemGroup.playerHome))
                {
                    // UnityEngine.Debug.LogError("Player home does not exist.");
                    return;
                }
                var playerLinks = EntityManager.GetComponentData<PlayerLinks>(PlayerSystemGroup.playerHome);
                if (playerLinks.players.Length > 0)
                {
                    lastSentPosition = Time.time;
                    var localNetPlayerEntity = NetSystemGroup.localNetPlayer;
                    var inputPlayerEntity = playerLinks.players[0];
                    var characterEntity = EntityManager.GetComponentData<CharacterLink>(inputPlayerEntity).character;
                    if (characterEntity.Index > 0 && EntityManager.Exists(characterEntity) && EntityManager.Exists(localNetPlayerEntity))
                    {
                        var position = EntityManager.GetComponentData<Translation>(characterEntity);
                        EntityManager.SetComponentData(localNetPlayerEntity, position);
                        EntityManager.AddComponent<UpdateNetPlayerPosition>(localNetPlayerEntity);
                    }
                }
            }
        }
    }
}

        /*private void DrawOtherClients()
        {
            var netPlayerLinks = zoxNet.NetPlayerLinks;
            for (int i = 0; i < netPlayerLinks.players.Length; i++)
            {
                var playerEntity = netPlayerLinks.players[i];
                if (zoxNet.netPlayerEntity != playerEntity)
                {
                    // todo: convert ZoxNet to ECS
                    // spawn a network player instead of a static list here
                    // var position = EntityManager.GetComponentData<Translation>(playerEntity);
                    var boneSize = 0.32f * (new float3(1, 2, 1));
                    RenderLineGroup.CreateCubeLines(EntityManager, elapsedTime, lobby.players[i].position, quaternion.identity, boneSize); 
                }
            }
        }*/

        /*public override void OnRecievedMessage(string message)
        {
            if (message.Contains("[clientupdateposition]"))
            {
                message = message.Replace("[clientupdateposition]", "");
                var netPlayerLinks = zoxNet.NetPlayerLinks;
                for (int i = 0; i < netPlayerLinks.players.Length; i++)
                {
                    zoxNet.SendPacketToPlayer("[hostupdateposition]" + message, recievedFromPort, netPlayerLinks.players[i]);
                }
                UpdateClientPosition(message);
            }
            else if (message.Contains("[hostupdateposition]"))
            {
                message = message.Replace("[hostupdateposition]", "");
                UpdateClientPosition(message);
            }
        }*/

        /*private void UpdateClientPosition(string message)
        {
            var split = message.Split(':');
            var playerID = int.Parse(split[0]);
            var position = new float3(
                float.Parse(split[1]),
                float.Parse(split[2]),
                float.Parse(split[3])
            );
            Debug.Log("Updating Player " + playerID + "'s position: " + position);
            // find player of ID and update its position
            var netPlayerLinks = zoxNet.NetPlayerLinks;
            for (int i = 0; i < netPlayerLinks.players.Length; i++)
            {
                var player = netPlayerLinks.players[i];
                if (EntityManager.GetComponentData<ZoxID>(player).id == playerID)
                {
                    // player.position = position;
                    // zoxNet.lobby.players[i] = player;
                    break;
                }
            }
        }*/