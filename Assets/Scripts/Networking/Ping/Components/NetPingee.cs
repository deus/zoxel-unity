using Unity.Entities;

namespace Zoxel.Networking
{
    public struct NetPingee : IComponentData
    {
        public double lastTimePinged;

        public NetPingee(double elapsedTime)
        {
            lastTimePinged = elapsedTime;
        }
    }
}