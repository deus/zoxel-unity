using Unity.Entities;

namespace Zoxel.Networking
{
    public struct NetPinger : IComponentData
    {
        public double lastTimePinged;

        public NetPinger(double elapsedTime)
        {
            lastTimePinged = elapsedTime;
        }
    }
}