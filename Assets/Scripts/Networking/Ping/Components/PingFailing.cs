using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Networking
{
    //! Allows for a gracefull disconnection.
    public struct PingFailing : IComponentData
    {
        //! The amount of times ping failed.
        public byte count;
    }
}