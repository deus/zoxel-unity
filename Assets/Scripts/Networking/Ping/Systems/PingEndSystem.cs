using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! When lobby recieves a ping packet.
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class PingEndSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery lobbyQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            lobbyQuery = GetEntityQuery(ComponentType.ReadOnly<Lobby>());
            RequireForUpdate(processQuery);
            RequireForUpdate(lobbyQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var lobbyEntities = lobbyQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var netPlayerLinks = GetComponentLookup<NetPlayerLinks>(true);
            lobbyEntities.Dispose();
            // Detect Ping Returns
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved>()
                .ForEach((Entity e, int entityInQueryIndex, in PacketData packetData, in LobbyLink lobbyLink) =>
            {
                if (packetData.data.Length == 5 && packetData.data[0] == PacketType.Ping)
                {
                    var playerID = ByteUtil.GetInt(in packetData.data, 1);
                    //! \todo Usehashmap for players
                    var netPlayerLinks2 = netPlayerLinks[lobbyLink.lobby];
                    var playerEntity = netPlayerLinks2.Get(playerID);
                    if (playerEntity.Index > 0)
                    {
                        PostUpdateCommands.RemoveComponent<Pinging>(entityInQueryIndex, playerEntity);
                        if (HasComponent<PingFailing>(playerEntity))
                        {
                            PostUpdateCommands.RemoveComponent<PingFailing>(entityInQueryIndex, playerEntity);
                        }
                    }
                }
            })  .WithReadOnly(netPlayerLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}