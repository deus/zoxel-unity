using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! Sends a ping to a NetPlayer from the lobby.
    /**
    *   - Lobby Send System -
    *   Also removes StartPing and adds a Pinging component onto a NetPlayer.
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class PingSendSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery lobbyQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            lobbyQuery = GetEntityQuery(ComponentType.ReadOnly<Lobby>());
            RequireForUpdate(processQuery);
            RequireForUpdate(lobbyQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<StartPing>(processQuery);
            PostUpdateCommands2.AddComponent<Pinging>(processQuery);
            var packetSendPrefab = PacketSendSystem.packetSendPrefab;
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var lobbyEntities = lobbyQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var lobbyNetAddresss = GetComponentLookup<EntityNetAddress>(true);
            lobbyEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<StartPing>()
                .ForEach((int entityInQueryIndex, in EntityNetAddress entityNetAddress, in LobbyLink lobbyLink) =>
            {
                if (!lobbyNetAddresss.HasComponent(lobbyLink.lobby))
                {
                    return;
                }
                var lobbyNetAddress = lobbyNetAddresss[lobbyLink.lobby].netAddress;
                var packetData = new BlitableArray<byte>(1, Allocator.Persistent);
                packetData[0] = PacketType.Ping;
                // Send response
                var sendPacketEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, packetSendPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new EntityNetAddress(lobbyNetAddress));
                PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new TargetNetAddress(entityNetAddress.netAddress));
                PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new PacketData(packetData));
            })  .WithReadOnly(lobbyNetAddresss)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}