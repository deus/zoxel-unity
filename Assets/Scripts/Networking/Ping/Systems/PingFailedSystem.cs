using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Networking
{
    //! Lobby disconnects any NetPlayers that arn't pinging back over 10 times.
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class PingFailedSystem : SystemBase
    {
        public const byte maxFailCount = 12;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery failedPingQuery;
        private EntityQuery failedPingQuery2;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            failedPingQuery = GetEntityQuery(ComponentType.ReadOnly<FailedPing>());
            failedPingQuery2 = GetEntityQuery(
                ComponentType.Exclude<PingFailing>(),
                ComponentType.ReadOnly<FailedPing>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<FailedPing>(failedPingQuery);
            PostUpdateCommands2.AddComponent<PingFailing>(failedPingQuery2);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<FailedPing>()
                .ForEach((ref PingFailing pingFailing) =>
            {
                pingFailing.count++;
            }).ScheduleParallel(Dependency);
            var lobbyRemovePlayerPrefab = LobbyRemovePlayerSystem.lobbyRemovePlayerPrefab;
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<FailedPing>()
                .ForEach((Entity e, int entityInQueryIndex, in PingFailing pingFailing, in LobbyLink lobbyLink) =>
            {
                if (pingFailing.count >= PingFailedSystem.maxFailCount)
                {
                    PostUpdateCommands.SetComponent(entityInQueryIndex,
                        PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyRemovePlayerPrefab),
                        new LobbyRemovePlayer(lobbyLink.lobby, e));
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}