using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Networking
{
    //! Triggers the StartPing using a timer.
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class NetPingSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static float pingRate = 2f;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var pingRate = NetPingSystem.pingRate;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // [Server] - Start Pinging every PingRate
            Dependency = Entities
                .WithNone<DestroyEntity, StartPing, FailedPing>()
                .WithNone<Pinging>()
                .ForEach((Entity e, int entityInQueryIndex, ref NetPinger netPinger) =>
            {
                if (elapsedTime - netPinger.lastTimePinged >= pingRate)
                {
                    netPinger.lastTimePinged = elapsedTime;
                    PostUpdateCommands.AddComponent<StartPing>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // [Server] - Already Pinging - No Response - Add FailedPing Event
            Dependency = Entities
                .WithNone<DestroyEntity, StartPing, FailedPing>()
                .WithAll<Pinging>()
                .ForEach((Entity e, int entityInQueryIndex, ref NetPinger netPinger) =>
            {
                if (elapsedTime - netPinger.lastTimePinged >= pingRate)
                {
                    netPinger.lastTimePinged = elapsedTime;
                    PostUpdateCommands.RemoveComponent<Pinging>(entityInQueryIndex, e);
                    PostUpdateCommands.AddComponent<FailedPing>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}