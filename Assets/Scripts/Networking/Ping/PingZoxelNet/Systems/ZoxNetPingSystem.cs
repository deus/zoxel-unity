/*using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! When player first ping server, return them their public IP.
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class ZoxNetPingSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery lobbyQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            lobbyQuery = GetEntityQuery(ComponentType.ReadOnly<Lobby>());
            RequireForUpdate(processQuery);
            RequireForUpdate(lobbyQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var packetSendPrefab = PacketSendSystem.packetSendPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var lobbyEntities = lobbyQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var netAddresses = GetComponentLookup<EntityNetAddress>(true);
            lobbyEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved>()
                .ForEach((Entity e, int entityInQueryIndex, in PacketData packetData, in EntityNetAddress entityNetAddress, in LobbyLink lobbyLink) =>
            {
                if (packetData.data.Length == 1 && packetData.data[0] == PacketType.PingZoxelNet)
                {
                    if (!netAddresses.HasComponent(lobbyLink.lobby))
                    {
                        return;
                    }
                    var returnPacketData = new BlitableArray<byte>(5, Allocator.Persistent);
                    returnPacketData[0] = PacketType.PingZoxelNet;
                    returnPacketData[1] = netAddress.ip.digit0;
                    returnPacketData[2] = netAddress.ip.digit1;
                    returnPacketData[3] = netAddress.ip.digit2;
                    returnPacketData[4] = netAddress.ip.digit3;
                    var lobbyNetAddress = netAddresses[lobbyLink.lobby];
                    // Send response packet
                    var sendPacketEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, packetSendPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, lobbyNetAddress);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new TargetNetAddress(netAddress));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new PacketData(returnPacketData));
                }
            })  .WithReadOnly(netAddresses)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}*/

/**
*   - Lobby Recieve System -
*   \todo Run this functionality on another server/port?
*/
// debug
/*Entities
    .WithNone<DestroyEntity>()
    .WithAll<PacketRecieved>()
    .ForEach((Entity e, int entityInQueryIndex, in PacketData packetData, in EntityNetAddress entityNetAddress, in LobbyLink lobbyLink) =>
{
    if (packetData.data.Length == 1 && packetData.data[0] == PacketType.PingZoxelNet)
    {
        if (!netAddresses.HasComponent(lobbyLink.lobby))
        {
            return;
        }
        var lobbyNetAddress = netAddresses[lobbyLink.lobby];
        // Send response packet
        UnityEngine.Debug.LogError("Uknown Player [" + netAddress + "] Requested IP - Lobby: " + lobbyNetAddress);
    }
}).WithReadOnly(netAddresses).WithoutBurst().Run();*/