/*using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! NetPlayer to ping server on initialization.
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class ZoxNetNewPlayerSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var packetSendPrefab = PacketSendSystem.packetSendPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<ConnectToZoxNet, LocalNetPlayer>()
                .ForEach((Entity e, int entityInQueryIndex, in EntityNetAddress entityNetAddress, in ZoxNetAddress zoxNetAddress) =>
            {
                PostUpdateCommands.RemoveComponent<ConnectToZoxNet>(entityInQueryIndex, e);
                // Send response packet
                var targetNetAddress = zoxNetAddress.netAddress; // netAddresses[lobbyLink.lobby];
                var packetData = new BlitableArray<byte>(1, Allocator.Persistent);
                packetData[0] = PacketType.PingZoxelNet;
                var sendPacketEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, packetSendPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, netAddress);
                PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new TargetNetAddress(targetNetAddress));
                PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new PacketData(packetData));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}*/
    /**
    *   - Player Send System -
    */
                /*if (NetworkManager.instance.isDebugPacketsSent)
                {
                    UnityEngine.Debug.LogError("Requesting Player (" + e.Index + ") Public IP to Lobby: " + targetNetAddress);
                }*/