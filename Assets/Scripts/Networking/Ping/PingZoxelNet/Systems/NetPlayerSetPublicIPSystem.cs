/*using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! When player first ping server, return them their public IP.
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class NetPlayerSetPublicIPSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery netPlayersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            netPlayersQuery = GetEntityQuery(ComponentType.ReadOnly<NetPlayer>());
            RequireForUpdate(processQuery);
            RequireForUpdate(netPlayersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var netAddresses = GetComponentLookup<EntityNetAddress>(false);
            netPlayerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved>()
                .ForEach((Entity e, int entityInQueryIndex, in PacketData packetData, in NetPlayerLink netPlayerLink) =>
            {
                if (packetData.data.Length == 5 && packetData.data[0] == PacketType.PingZoxelNet)
                {
                    // if already connected, no need to update!
                    var playerNetAddress = netAddresses[netPlayerLink.player];
                    playerNetAddress.ip.digit0 = packetData.data[1];
                    playerNetAddress.ip.digit1 = packetData.data[2];
                    playerNetAddress.ip.digit2 = packetData.data[3];
                    playerNetAddress.ip.digit3 = packetData.data[4];
                    netAddresses[netPlayerLink.player] = playerNetAddress;
                    PostUpdateCommands.AddComponent<ConnectedToZoxNet>(entityInQueryIndex, netPlayerLink.player);
                    // UnityEngine.Debug.LogError("Set playerNetAddress: " + playerNetAddress);
                }
            })  .WithNativeDisableContainerSafetyRestriction(netAddresses)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}*/
    /**
    *   - Player Recieve System -
    */
            // Debug PingZoxelNet return message.
            /*Entities
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved>()
                .ForEach((Entity e, int entityInQueryIndex, in PacketData packetData, in NetPlayerLink netPlayerLink) =>
            {
                if (packetData.data.Length == 5 && packetData.data[0] == PacketType.PingZoxelNet)
                {
                    // if already connected, no need to update!
                    var playerNetAddress = new NetAddress();
                    playerNetAddress.ip.digit0 = packetData.data[1];
                    playerNetAddress.ip.digit1 = packetData.data[2];
                    playerNetAddress.ip.digit2 = packetData.data[3];
                    playerNetAddress.ip.digit3 = packetData.data[4];
                    UnityEngine.Debug.LogError("Set playerNetAddress: " + playerNetAddress);
                }
            }).WithoutBurst().Run();*/


            // - Debug Begin -
            /*Entities
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved>()
                .ForEach((Entity e, int entityInQueryIndex, in PacketData packetData, in NetPlayerLink netPlayerLink) =>
            {
                if (packetData.data.Length > 0 && packetData.data[0] == PacketType.PingZoxelNet)
                {
                    var ip = new IPAddress();
                    ip.digit0 = packetData.data[1];
                    ip.digit1 = packetData.data[2];
                    ip.digit2 = packetData.data[3];
                    ip.digit3 = packetData.data[4];
                    UnityEngine.Debug.LogError("Setting Player (" + netPlayerLink.player.Index + ") IP to: " + ip);
                }
            }).WithoutBurst().Run();*/
            // - Debug End -
