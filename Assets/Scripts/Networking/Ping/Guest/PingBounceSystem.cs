using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! Reads ping packets and adds events to NetPlayer entities.
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class PingRecieveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery netPlayersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            netPlayersQuery = GetEntityQuery(ComponentType.ReadOnly<NetPlayer>());
            RequireForUpdate(processQuery);
            RequireForUpdate(netPlayersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var packetSendPrefab = PacketSendSystem.packetSendPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var netPlayerIDs = GetComponentLookup<ZoxID>(true);
            var lobbyLinks = GetComponentLookup<LobbyLink>(true);
            var netAddresses = GetComponentLookup<EntityNetAddress>(true);
            var netPingees = GetComponentLookup<NetPingee>(false);
            netPlayerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved, NetPlayerLink>()
                .ForEach((Entity e, int entityInQueryIndex, in PacketData packetData, in EntityNetAddress entityNetAddress, in NetListenerLink netListenerLink) =>
            {
                if (packetData.data.Length == 1 && packetData.data[0] == PacketType.Ping)
                {
                    var netPlayerEntity = netListenerLink.listener;
                    if (!lobbyLinks.HasComponent(netPlayerEntity))
                    {
                        return;
                    }
                    var lobbyLink = lobbyLinks[netPlayerEntity];
                    var netPlayerID = netPlayerIDs[netPlayerEntity];
                    var netPlayerNetAddress = netAddresses[netPlayerEntity].netAddress;
                    var lobbyNetAddress = entityNetAddress.netAddress;
                    var packetData2 = new BlitableArray<byte>(5, Allocator.Persistent);
                    packetData2[0] = PacketType.Ping;
                    ByteUtil.SetInt(ref packetData2, 1, netPlayerID.id);
                    // Send response
                    var sendPacketEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, packetSendPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new EntityNetAddress(netPlayerNetAddress));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new TargetNetAddress(lobbyNetAddress));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new PacketData(packetData2));
                    if (netPingees.HasComponent(netPlayerEntity))
                    {
                        var netPingee = netPingees[netPlayerEntity];
                        netPingee.lastTimePinged = elapsedTime;
                        netPingees[netPlayerEntity] = netPingee;
                        if (HasComponent<PingFailing>(netPlayerEntity))
                        {
                            PostUpdateCommands.RemoveComponent<PingFailing>(entityInQueryIndex, netPlayerEntity);
                        }
                    }
                }
            })  .WithReadOnly(netPlayerIDs)
                .WithReadOnly(lobbyLinks)
                .WithReadOnly(netAddresses)
                .WithNativeDisableContainerSafetyRestriction(netPingees)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            /* Debug
            Entities
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved, NetPlayerLink>()
                .ForEach((in PacketData packetData) =>
            {
                if (packetData.data.Length == 1 && packetData.data[0] == PacketType.Ping)
                {
                    // PostUpdateCommands.AddComponent<Pinged>(entityInQueryIndex, netListenerLink.listener);
                    UnityEngine.Debug.LogError("LOCAL PLAYER IS PINGED!!!");
                }
            }).WithoutBurst().Run();*/
        }
    }
}