using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Networking
{
    //! Disconnects client from server if failed to return ping for a while.
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class NetPingeeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var pingReturnRate = NetPingSystem.pingRate * 2f;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            //! For first ping failing to lobby.
            Dependency = Entities
                .WithNone<DestroyEntity, PingFailing>()
                .ForEach((Entity e, int entityInQueryIndex, ref NetPingee netPingee) =>
            {
                if (elapsedTime - netPingee.lastTimePinged >= pingReturnRate)
                {
                    netPingee.lastTimePinged = elapsedTime;
                    PostUpdateCommands.AddComponent<PingFailing>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            //! For consecutive ping failings to lobby.
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref NetPingee netPingee, ref PingFailing pingFailing, in LobbyLink lobbyLink) =>
            {
                if (elapsedTime - netPingee.lastTimePinged >= pingReturnRate)
                {
                    netPingee.lastTimePinged = elapsedTime;
                    pingFailing.count++;
                    if (pingFailing.count >= PingFailedSystem.maxFailCount)
                    {
                        PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, lobbyLink.lobby);
                    }
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}