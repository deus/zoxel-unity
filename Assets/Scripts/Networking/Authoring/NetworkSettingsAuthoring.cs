using Unity.Entities;
using UnityEngine;

namespace Zoxel.Networking.Authoring
{
    //! Settings data for our ai namespace.
    // [GenerateAuthoringComponent]
    public struct NetworkSettings : IComponentData
    {
        public bool isDebugPlayers;
        public bool isDebugPacketsRecieved;
        public bool isDebugPacketsSent;
    }

    public class NetworkSettingsAuthoring : MonoBehaviour
    {
        public bool isDebugPlayers;
        public bool isDebugPacketsRecieved;
        public bool isDebugPacketsSent;
    }

    public class NetworkSettingsAuthoringBaker : Baker<NetworkSettingsAuthoring>
    {
        public override void Bake(NetworkSettingsAuthoring authoring)
        {
            AddComponent(new NetworkSettings
            {
                isDebugPlayers = authoring.isDebugPlayers,
                isDebugPacketsRecieved = authoring.isDebugPacketsRecieved,
                isDebugPacketsSent = authoring.isDebugPacketsSent,
            });       
        }
    }
}