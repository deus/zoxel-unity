using Unity.Entities;
using System.Net;
// Player can only listen to lobby from Public IP
// I keep both depending on if lan game or over web

namespace Zoxel.Networking
{
    public struct NetPlayer : IComponentData
    {

        public static string GetUILabel(EntityManager EntityManager, Entity playerEntity)
        {
            var id = EntityManager.GetComponentData<ZoxID>(playerEntity).id;
            var name = EntityManager.GetComponentData<ZoxName>(playerEntity).name.ToString();
            var netAddress = EntityManager.GetComponentData<EntityNetAddress>(playerEntity);
            // if locally to client
            var playerUILabel = "[" + id + "]: " + name + ": ";
            //if (netPlayer.ip.ToString() == netPlayer.ip.ToString())
            playerUILabel += netAddress.netAddress;
            return playerUILabel;
        }

        public static string GetNameLabel(EntityManager EntityManager, Entity playerEntity)
        {
            var name = EntityManager.GetComponentData<ZoxName>(playerEntity).name.ToString();
            // if locally to client
            var playerUILabel = "[" + name.ToString() + "]";
            return playerUILabel;
        }

        // for now use strings as messages
        /*public static string GetData(EntityManager EntityManager, Entity localPlayer)
        {
            var id = EntityManager.GetComponentData<ZoxID>(localPlayer).id;
            var name = EntityManager.GetComponentData<ZoxName>(localPlayer).name.ToString();
            if (name == "")
            {
                name = "Null";
            }
            var netPlayer = EntityManager.GetComponentData<NetAddress>(localPlayer);
            var ip = netPlayer.ip.ToString();
            if (ip == "")
            {
                ip = "0.0.0.0";
            }
            return id + ":"
                + name + ":" 
                + ip + ":"
                + netPlayer.port; // + ":" 
                //+ localIP + ":"
                //+ netPlayer.localPort;
        }*/

        public static string GetDataName(string data)
        {
            var split = data.Split(':');
            if (split.Length != 4)
            {
                UnityEngine.Debug.LogError("Cannot set player data as wrong amount of split strings: "+  data);
                return "";
            }
            return (split[1]);
        }

        public static int GetDataID(string data)
        {
            var split = data.Split(':');
            if (split.Length != 4)
            {
                UnityEngine.Debug.LogError("Cannot set player data as wrong amount of split strings: "+  data);
                return 0;
            }
            return int.Parse(split[0]);
        }
    }
}