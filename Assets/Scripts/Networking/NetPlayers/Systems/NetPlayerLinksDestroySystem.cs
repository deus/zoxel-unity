using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Networking
{
    //! Destroys all net player links and disposes the lobby data.
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class NetPlayerLinksDestroySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<DestroyEntity, NetPlayerLinks>()
                .ForEach((int entityInQueryIndex, in NetPlayerLinks netPlayerLinks) =>
            {
                for (int i = 0; i < netPlayerLinks.players.Length; i++)
                {
                    var player = netPlayerLinks.players[i];
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, player);
                }
                netPlayerLinks.Dispose();
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}