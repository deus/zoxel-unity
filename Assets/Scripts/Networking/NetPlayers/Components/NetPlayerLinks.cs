using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Zoxel.Networking
{
    // Lobby contains a bunch of rooms
    public struct NetPlayerLinks : IComponentData
    {
        public BlitableArray<Entity> players;
        public UnsafeParallelHashMap<int, Entity> playersHash;
        
        public void Dispose()
        {
            players.Dispose();
			if (playersHash.IsCreated)
			{
				playersHash.Dispose();
			}
        }

        public Entity Get(int id)
        {
            if (playersHash.IsCreated)
            {
                Entity outputEntity;
                if (playersHash.TryGetValue(id, out outputEntity))
                {
                    return outputEntity;
                }
            }
            return new Entity();
        }

        public void Add(Entity newPlayer, int newPlayerID)
        {
            var players2 = new BlitableArray<Entity>(players.Length + 1, Allocator.Persistent, players);
            players2[players.Length] = newPlayer;
            players.Dispose();
            players = players2;
            if (!playersHash.IsCreated)
            {
                playersHash = new UnsafeParallelHashMap<int, Entity>(1, Allocator.Persistent);
            }
            playersHash[newPlayerID] = newPlayer;
        }

        public void Remove(Entity playerEntity, int playerID)
        {
            Remove(playerID);
            Remove(playerEntity);
        }
        
        private void Remove(Entity playerEntity)
        {
            if (players.Length == 0)
            {
                return;
            }
            var index = -1;
            for (int i = 0; i < players.Length; i++)
            {
                if (playerEntity == players[i])
                {
                    index = i;
                    break;
                }
            }
            // player not found
            if (index == -1)
            {
                return;
            }
            var players2 = new BlitableArray<Entity>(players.Length - 1, Allocator.Persistent, players);
            for (int i = index; i < players2.Length; i++)
            {
                players2[i] = players[i + 1];
            }
            players.Dispose();
            players = players2;
        }

		public void Remove(int id)
		{
			if (playersHash.IsCreated && playersHash.ContainsKey(id))
			{
				playersHash.Remove(id);
			}
		}
    }
}
            // var players2 = new BlitableArray<Entity>(players.Length + 1, Allocator.Persistent);
            /*for (int i = 0; i < players.Length; i++)
            {
                players2[i] = players[i];
            }*/
            /*var newOnes = new BlitableArray<Entity>(players.Length - 1, Allocator.Persistent);
            for (int i = 0; i < newOnes.Length; i++)
            {
                if (i < index)
                {
                    newOnes[i] = players[i];
                }
                else
                {
                    newOnes[i] = players[i + 1];
                }
            }*/