using Unity.Entities;

namespace Zoxel.Networking
{
    public struct LocalNetPlayerLink : IComponentData
    {
        public Entity player;

        public LocalNetPlayerLink(Entity player)
        {
            this.player = player;
        }
    }
}