using Unity.Entities;

namespace Zoxel.Networking
{
    public struct NetPlayerLink : IComponentData
    {
        public Entity player;

        public NetPlayerLink(Entity player)
        {
            this.player = player;
        }
    }
}