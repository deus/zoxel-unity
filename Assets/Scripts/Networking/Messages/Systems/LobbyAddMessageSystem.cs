using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! Add a message in the lobby. Supports multiple per frame.
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LobbyAddMessageSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity lobbyAddMessageEventPrefab;
        private EntityQuery processQuery;
        private EntityQuery addMessageQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var spawnArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(SpawnLobbyMessageClient),
                typeof(LobbyLink),
                typeof(GenericEvent));  // EntityDestroySingleFrame
            lobbyAddMessageEventPrefab = EntityManager.CreateEntity(spawnArchetype);
            addMessageQuery = GetEntityQuery(ComponentType.ReadOnly<SpawnLobbyMessageClient>());
            RequireForUpdate(processQuery);
            RequireForUpdate(addMessageQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var addMessageEntities = addMessageQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var lobbyLinks = GetComponentLookup<LobbyLink>(true);
            var addLobbyMessages = GetComponentLookup<SpawnLobbyMessageClient>(true);
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .ForEach((Entity e, int entityInQueryIndex, ref NetMessages netMessages) =>
            {
                for (int i = 0; i < addMessageEntities.Length; i++)
                {
                    var addMessageEntity = addMessageEntities[i];
                    var lobbyLink = lobbyLinks[addMessageEntity];
                    if (lobbyLink.lobby != e)
                    {
                        continue;
                    }
                    var addLobbyMessage = addLobbyMessages[addMessageEntity];
                    netMessages.AddMessage(addLobbyMessage.message);
                }
            })  .WithReadOnly(addMessageEntities)
                .WithReadOnly(lobbyLinks)
                .WithReadOnly(addLobbyMessages)
                .WithDisposeOnCompletion(addMessageEntities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}