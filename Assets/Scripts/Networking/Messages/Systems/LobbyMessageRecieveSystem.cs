using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Networking
{
    //! When a host recieves a lobby message. Creates an event to echo it to all lobby players.
    /**
    *   - Recieve Packet System -
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LobbyMessageRecieveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery netPlayersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            netPlayersQuery = GetEntityQuery(ComponentType.ReadOnly<NetPlayer>());
            RequireForUpdate(processQuery);
            RequireForUpdate(netPlayersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var lobbyAddMessageEventPrefab = LobbyAddMessageSystem.lobbyAddMessageEventPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var lobbyLinks = GetComponentLookup<LobbyLink>(true);
            netPlayerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved>()
                .ForEach((Entity e, int entityInQueryIndex, in PacketData packetData, in NetPlayerLink netPlayerLink) =>
            {
                if (packetData.data.Length > 1 && packetData.data[0] == PacketType.LobbyMessage)
                {
                    Text message;
                    ByteUtil.GetText(in packetData.data, 5, out message);
                    var lobbyLink = lobbyLinks[netPlayerLink.player];
                    var spawnMessageEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyAddMessageEventPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMessageEntity, new SpawnLobbyMessageClient(message));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMessageEntity, lobbyLink);
                }
            })  .WithReadOnly(lobbyLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}