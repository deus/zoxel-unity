using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! Reads ping packets and adds events to NetPlayer entities.
    /**
    *   - Bounce Packet System -
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LobbyMessagesBounceSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery lobbyQuery;
        private EntityQuery netPlayersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            lobbyQuery = GetEntityQuery(ComponentType.ReadOnly<Lobby>());
            netPlayersQuery = GetEntityQuery(ComponentType.ReadOnly<NetPlayer>());
            RequireForUpdate(processQuery);
            RequireForUpdate(lobbyQuery);
            RequireForUpdate(netPlayersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var packetSendPrefab = PacketSendSystem.packetSendPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var lobbyEntities = lobbyQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var lobbyNetAddresses = GetComponentLookup<EntityNetAddress>(true);
            var netPlayerLinks = GetComponentLookup<NetPlayerLinks>(true);
            lobbyEntities.Dispose();
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var netAddresses = GetComponentLookup<EntityNetAddress>(true);
            var playerIDs = GetComponentLookup<ZoxID>(true);
            netPlayerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved>()
                .ForEach((Entity e, int entityInQueryIndex, in PacketData packetData, in LobbyLink lobbyLink) =>
            {
                if (packetData.data.Length > 1 && packetData.data[0] == PacketType.LobbyMessage)
                {
                    var senderID = ByteUtil.GetInt(in packetData.data, 1);
                    var lobbyNetAddress = lobbyNetAddresses[lobbyLink.lobby].netAddress;
                    var netPlayerLinks2 = netPlayerLinks[lobbyLink.lobby];
                    for (int i = 0; i < netPlayerLinks2.players.Length; i++)
                    {
                        var playerEntity = netPlayerLinks2.players[i];
                        var playerID = playerIDs[playerEntity].id;
                        if (playerID == senderID)
                        {
                            continue;
                        }
                        var netAddress = netAddresses[playerEntity].netAddress;
                        var packetData2 = packetData.data.Clone();
                        var sendPacketEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, packetSendPrefab);
                        PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new EntityNetAddress(lobbyNetAddress));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new TargetNetAddress(netAddress));
                        PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new PacketData(packetData2));
                    }
                }
            })  .WithReadOnly(lobbyNetAddresses)
                .WithReadOnly(netPlayerLinks)
                .WithReadOnly(netAddresses)
                .WithReadOnly(playerIDs)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}