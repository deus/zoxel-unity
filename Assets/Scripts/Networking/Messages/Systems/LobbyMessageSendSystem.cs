using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! When a player wants to send a message through the lobby.
    /**
    *   - Send Packet System -
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LobbyMessageSendSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery lobbyQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            lobbyQuery = GetEntityQuery(ComponentType.ReadOnly<Lobby>());
            RequireForUpdate(processQuery);
            RequireForUpdate(lobbyQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var packetSendPrefab = PacketSendSystem.packetSendPrefab;
            var lobbyAddMessageEventPrefab = LobbyAddMessageSystem.lobbyAddMessageEventPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<SpawnLobbyMessage>(processQuery);
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var lobbyEntities = lobbyQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var lobbyNetAddresses = GetComponentLookup<EntityNetAddress>(true);
            lobbyEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .ForEach((Entity e, int entityInQueryIndex, in LobbyLink lobbyLink, in EntityNetAddress entityNetAddress, in ZoxID zoxID, in SpawnLobbyMessage spawnLobbyMessage) =>
            {
                if (!lobbyNetAddresses.HasComponent(lobbyLink.lobby))
                {
                    return;
                }
                var netAddress = entityNetAddress.netAddress;
                var lobbyEntity = lobbyLink.lobby;
                var lobbyNetAddress = lobbyNetAddresses[lobbyEntity].netAddress;
                var packetData = new BlitableArray<byte>(1 + 4 + spawnLobbyMessage.message.Length, Allocator.Persistent);
                packetData[0] = PacketType.LobbyMessage;
                ByteUtil.SetInt(ref packetData, 1, zoxID.id);
                ByteUtil.SetText(ref packetData, 5, in spawnLobbyMessage.message);
                var sendPacketEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, packetSendPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new EntityNetAddress(netAddress));
                PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new TargetNetAddress(lobbyNetAddress));
                PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new PacketData(packetData));
                // add client message
                var spawnMessageEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyAddMessageEventPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMessageEntity, new SpawnLobbyMessageClient(spawnLobbyMessage.message.Clone()));
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMessageEntity, lobbyLink);
            })  .WithReadOnly(lobbyNetAddresses)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}