using Unity.Entities;

namespace Zoxel.Networking
{
    public struct SpawnLobbyMessageClient : IComponentData
    {
        public Text message;

        public SpawnLobbyMessageClient(Text message)
        {
            this.message = message;
        }

        public SpawnLobbyMessageClient(string message)
        {
            this.message = new Text(message);
        }
    }
}