using Unity.Entities;
using Unity.Mathematics;
using Unity.Collections;

namespace Zoxel.Networking
{
    // Lobby contains a bunch of rooms
    public struct NetMessages : IComponentData
    {
        public BlitableArray<Text> messages;
        
        public void Dispose()
        {
            for (int i = 0; i < messages.Length; i++)
            {
                messages[i].Dispose();
            }
            messages.Dispose();
        }

        public void AddMessage(Text message)
        {
            var newOnes = new BlitableArray<Text>(messages.Length + 1, Allocator.Persistent);
            for (int i = 0; i < messages.Length; i++)
            {
                newOnes[i] = messages[i];
            }
            newOnes[messages.Length] = message;
            if (messages.Length > 0)
            {
                messages.Dispose();
            }
            messages = newOnes;
        }
    }
}