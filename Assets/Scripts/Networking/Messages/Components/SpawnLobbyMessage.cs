using Unity.Entities;

namespace Zoxel.Networking
{
    public struct SpawnLobbyMessage : IComponentData
    {
        public Text message;

        public SpawnLobbyMessage(Text message)
        {
            this.message = message;
        }

        public SpawnLobbyMessage(string message)
        {
            this.message = new Text(message);
        }
    }
}