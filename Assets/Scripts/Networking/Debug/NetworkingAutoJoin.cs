/*using Unity.Entities;
using Unity.Mathematics;
using System.Collections;
using UnityEngine;

namespace Zoxel.Networking
{
    public class NetworkingAutoJoin : MonoBehaviour
    {
        public bool IsECSEnabled
        {
            get
            {
                return World.DefaultGameObjectInjectionWorld != null;
            }
        }

        public EntityManager EntityManager
        {
            get
            {
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        public NetSystemGroup netSystemGroup
        {
            get
            {
                return World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<NetSystemGroup>();
            }
        }

        private float lastTried;

        void Update()
        {
            if (NetworkUtil.IsHeadless())
            {
                enabled = false;
                return;
            }
            if (!IsECSEnabled || NetSystemGroup.lobbyIP == "")
            {
                return;
            }
            // wait until connected to ZoxNet
            if (!EntityManager.HasComponent<ConnectedToZoxNet>(NetSystemGroup.localNetPlayer))
            {
                if (Time.time - lastTried >= 1)
                {
                    lastTried = Time.time;
                    EntityManager.AddComponent<ConnectToZoxNet>(NetSystemGroup.localNetPlayer);
                }
                return;
            }
            // ZoxNet.instance.JoinLobby(netSystemGroup.lobbyIP, netSystemGroup.lobbyPort);
            enabled = false;
        }
    }
}*/