using Unity.Entities;
using Unity.Mathematics;
using System.Collections;
using UnityEngine;
using System.Net;

namespace Zoxel.Networking
{
    public class NetworkingChatUI : MonoBehaviour
    {
        public bool isCenter = false;
        public int fontSize = 22;
        public UnityEngine.Color fontColor = UnityEngine.Color.green;
        public UnityEngine.Font font;
        public UnityEngine.TextAnchor textAnchor;
        [Header("UI Settings")]
        public bool isShowLobbyPlayers;
        public bool isShowChat;
        private string message = "";

        void OnGUI()
        {
            if (NetworkUtil.IsHeadless())
            {
                enabled = false;
                return;
            }
            if (!IsECSEnabled)
            {
                return;
            }
            if (font)
            {
                GUI.skin.font = font;
            }
            GUI.color = fontColor;
            GUI.skin.label.fontSize = fontSize;
            GUI.skin.label.alignment = textAnchor;
            GUI.skin.button.alignment = textAnchor;
            if (isCenter)
            {
                var bufferX = Screen.width / 4;
                var bufferY = Screen.width / 8;
                GUILayout.BeginArea(new UnityEngine.Rect(bufferX, bufferY, Screen.width - bufferX * 2, Screen.height - bufferY * 2));
                GUILayout.BeginVertical();
            }
            DrawUI();
            if (isCenter)
            {
                GUILayout.EndVertical();
                GUILayout.EndArea();
            }
        }

        void DrawUI() 
        {
            if (EntityManager.Exists(NetSystemGroup.connectEntity))
            {
                var entityNetAddress = EntityManager.GetComponentData<EntityNetAddress>(NetSystemGroup.connectEntity);
                var targetNetAddress = EntityManager.GetComponentData<TargetNetAddress>(NetSystemGroup.connectEntity);
                DrawLabel(entityNetAddress.netAddress + " is connecting to Lobby [" + targetNetAddress.netAddress + "]");
            }
            // else if (connectedState == ConnectedState.Connected)
            else if (netPlayerEntity.Index > 0)
            {
                isShowLobbyPlayers = DrawToggle("Players", isShowLobbyPlayers, fontSize + 8); // , 20);
                if (isShowLobbyPlayers)
                {
                    var netPlayerLinks = NetPlayerLinks;
                    for (int i = 0; i < netPlayerLinks.players.Length; i++)
                    {
                        var player = netPlayerLinks.players[i];
                        if (!EntityManager.Exists(player))
                        {
                            DrawLabel("[" + i + "] is destroyed.", fontSize + 8);
                            continue;
                        }
                        var playerLabel = "    ";
                        if (EntityManager.HasComponent<LocalNetPlayer>(player))
                        {
                            playerLabel += " (you) ";
                        }
                        if (EntityManager.HasComponent<LobbyHost>(player))
                        {
                            playerLabel += " (host) ";
                        }
                        if (EntityManager.HasComponent<PingFailing>(player))
                        {
                            playerLabel += "(unstable) ";
                        }
                        playerLabel += NetPlayer.GetNameLabel(EntityManager, player);
                        DrawLabel(playerLabel, fontSize + 8);
                    }
                }
                isShowChat = DrawToggle("Chat", isShowChat, fontSize + 8);
                if (isShowChat)
                {
                    message = DrawInput(message, "");
                    if (DrawButton("Send", fontSize + 8))
                    {
                        if (message != "")
                        {
                            SendLobbyMessage(NetPlayerName + ": " + message);
                            message = "";
                        }
                    }
                    var netMessages = NetMessages;
                    for (var i = netMessages.messages.Length - 1; i >= math.max(0, netMessages.messages.Length - 10) ; i--)
                    {
                        DrawLabel("   [" + i + "] " + netMessages.messages[i], fontSize + 8);
                    }
                }
            }
        }

        public void SendLobbyMessage(string message)
        {
            EntityManager.AddComponentData(netPlayerEntity, new SpawnLobbyMessage(message));
        }

        bool DrawButton(string label, int height = 25, int width = 300)
        {
            return UnityEngine.GUILayout.Button(label, UnityEngine.GUILayout.Height(height)); // , UnityEngine.GUILayout.Width(width));
        }

        void DrawLabel(string label, int height = 24, int width = 300)
        {
            // UnityEngine.GUILayout.Label(label); // , UnityEngine.GUILayout.Height(height), UnityEngine.GUILayout.Width(width));
            UnityEngine.GUILayout.Label(label, UnityEngine.GUILayout.Height(height)); // , UnityEngine.GUILayout.Width(width));
        }

        string DrawInput(string input, string label, int height = 25, int width = 300)
        {
            if (label != "")
            {
                DrawLabel(label, height, width);
            }
            // return UnityEngine.GUILayout.TextField(input, UnityEngine.GUILayout.Height(height), UnityEngine.GUILayout.Width(width));
            return UnityEngine.GUILayout.TextField(input, UnityEngine.GUILayout.Height(height));
        }

        bool DrawToggle(string label, bool state, int height = 45, int width = 300)
        {
            label = (state ? "[ON]" : "[OFF]") + " " + label;
            // return !UnityEngine.GUILayout.Button(label, UnityEngine.GUILayout.Height(height), UnityEngine.GUILayout.Width(width));
            var buttonValue = UnityEngine.GUILayout.Button(label, UnityEngine.GUILayout.Height(height)); // UnityEngine.GUILayout.Width(width));
            if (state)
            {
                return !buttonValue;
            }
            else
            {
                return buttonValue;
            }
        }

        public EntityManager EntityManager
        {
            get
            {
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        public bool IsECSEnabled
        {
            get
            {
                return World.DefaultGameObjectInjectionWorld != null;
            }
        }

        public Entity netPlayerEntity
        {
            get
            {
                if (!EntityManager.Exists(NetSystemGroup.localNetPlayer))
                {
                    return new Entity();
                }
                return NetSystemGroup.localNetPlayer;
            }
        }

        public Entity lobbyEntity
        {
            get
            {
                if (!IsECSEnabled || netPlayerEntity.Index == 0 || !EntityManager.Exists(netPlayerEntity))
                { 
                    return new Entity();
                }
                else
                {
                    var lobbyEntity = EntityManager.GetComponentData<LobbyLink>(netPlayerEntity).lobby;
                    if (!EntityManager.Exists(lobbyEntity))
                    {
                        return new Entity();
                    }
                    return lobbyEntity;
                }
            }
        }

        public NetPlayerLinks NetPlayerLinks
        {
            get
            {
                if (!IsECSEnabled || lobbyEntity.Index == 0)
                {
                    return new NetPlayerLinks();
                }
                return EntityManager.GetComponentData<NetPlayerLinks>(lobbyEntity);
            }
        }

        public NetMessages NetMessages
        {
            get
            {
                if (!IsECSEnabled || lobbyEntity.Index == 0)
                {
                    return new NetMessages();
                }
                return EntityManager.GetComponentData<NetMessages>(lobbyEntity);
            }
        }

        public bool LocalPlayerHost
        {
            get
            {
                if (!IsECSEnabled || netPlayerEntity.Index == 0)
                {
                    return false;
                }
                return EntityManager.HasComponent<LobbyHost>(netPlayerEntity);
            }
        }

        public string NetPlayerName
        {
            get
            {
                if (!IsECSEnabled || netPlayerEntity.Index == 0 || !EntityManager.Exists(netPlayerEntity))
                {
                    return "";
                }
                return EntityManager.GetComponentData<ZoxName>(netPlayerEntity).name.ToString();
            }
        }
    }
}