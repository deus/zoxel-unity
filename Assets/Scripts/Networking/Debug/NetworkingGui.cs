using Unity.Entities;
using Unity.Mathematics;
using System.Collections;
using UnityEngine;
using System.Net;

namespace Zoxel.Networking
{
    /*public class NetworkingGui : MonoBehaviour
    {
        public static NetworkingGui instance;
        // Test UI
        public bool isCenter = true;
        public int fontSize = 34;
        public UnityEngine.Color fontColor = UnityEngine.Color.green;
        public UnityEngine.Font font;
        public UnityEngine.TextAnchor textAnchor;
        [Header("UI Settings")]
        public bool isShowOverview;
        public bool isShowLobbyPlayers;
        public bool isShowChat;
        // private bool isShowRoomPlayers;
        [Header("Alt Settings")]
        public bool isAutoJoin = true;
        public bool canHost;
        public bool canLeaveLobby = true;
        private bool isPreHosting;
        private bool isPreJoining;
        // gui stuff
        private string message = "";

        public void Awake()
        {
            instance = this;
        }

        public void SendLobbyMessage(string message)
        {
            EntityManager.AddComponentData(netPlayerEntity, new SpawnLobbyMessage(message));
        }

        public void CloseLobby()
        {
            // Debug.Log(NetPlayerName + " has disconnected from lobbyEntity at: " + elapsedTime);
            EntityManager.AddComponent<CloseLobby>(netPlayerEntity);
        }

        private void LeaveLobby()
        {
            EntityManager.AddComponent<LeaveLobby>(netPlayerEntity);
        }

        public void HostLobby()
        {
            EntityManager.SetComponentData(EntityManager.Instantiate(LobbyHostSystem.hostLobbyPrefab),
                new HostLobby(new NetAddress(NetSystemGroup.localAddress.ip, NetSystemGroup.lobbyAddress.port)));
        }

        public void CancelConnecting()
        {
            if (!EntityManager.Exists(NetSystemGroup.connectEntity))
            {
                return;
            }
            EntityManager.AddComponent<DestroyEntity>(NetSystemGroup.connectEntity);
        }

        public void JoinLobby()
        {
            if (EntityManager.Exists(NetSystemGroup.connectEntity))
            {
                return;
            }
            var commandBufferSystem = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var connectEntity = PostUpdateCommands.Instantiate(ConnectingToLobbySystem.connectToLobbyPrefab);
            PostUpdateCommands.SetComponent(connectEntity, new EntityNetAddress(NetSystemGroup.localAddress));
            PostUpdateCommands.SetComponent(connectEntity, new LocalNetAddress(NetSystemGroup.localAddress));
            PostUpdateCommands.SetComponent(connectEntity, new TargetNetAddress(NetSystemGroup.lobbyAddress));
        }

        public bool IsLobbyHost()
        {
            return EntityManager.HasComponent<LobbyHost>(lobbyEntity);
        }

        public void StartGame()
        {
            // > spawn realm
            // > spawn world
            // > spawn character
            // for now just spawn character
            // and have it wander on plane
            EntityManager.AddComponent<LobbyStartGame>(netPlayerEntity);
        }

        void OnGUI()
        {
            if (NetworkUtil.IsHeadless())
            {
                enabled = false;
                return;
            }
            if (!IsECSEnabled)
            {
                return;
            }
            if (font)
            {
                GUI.skin.font = font;
            }
            GUI.color = fontColor;
            GUI.skin.label.fontSize = fontSize;
            GUI.skin.label.alignment = textAnchor;
            GUI.skin.button.alignment = textAnchor;
            if (isCenter)
            {
                var bufferX = Screen.width / 4;
                var bufferY = Screen.width / 8;
                GUILayout.BeginArea(new UnityEngine.Rect(bufferX, bufferY, Screen.width - bufferX * 2, Screen.height - bufferY * 2));
                GUILayout.BeginVertical();
            }
            DrawUI();
            if (isCenter)
            {
                GUILayout.EndVertical();
                GUILayout.EndArea();
            }
        }

        private void DrawOverview()
        {
            isShowOverview = DrawToggle("Overview", isShowOverview, 20);
            if (isShowOverview)
            {
                DrawLabel("Hello [" + NetPlayerName + "]");
                if (EntityManager.HasComponent<PingFailing>(NetSystemGroup.localNetPlayer))
                {
                    DrawLabel("> disconnecting");
                }
                var entityNetAddress = EntityManager.GetComponentData<EntityNetAddress>(NetSystemGroup.localNetPlayer);
                var localNetAddress = EntityManager.GetComponentData<LocalNetAddress>(NetSystemGroup.localNetPlayer);
                DrawLabel("> local ip [" + localNetAddress.netAddress + "]");
                DrawLabel("> public ip [" + entityNetAddress.netAddress + "]");
            }
        }

        void DrawUI() 
        {
            if (EntityManager.Exists(NetSystemGroup.connectEntity))
            {
                var entityNetAddress = EntityManager.GetComponentData<EntityNetAddress>(NetSystemGroup.connectEntity);
                var targetNetAddress = EntityManager.GetComponentData<TargetNetAddress>(NetSystemGroup.connectEntity);
                DrawLabel(entityNetAddress.netAddress + " is connecting to Lobby [" + targetNetAddress.netAddress + "]");
                // cancel connect to lobby?
                if (DrawButton("Cancel Connecting to Lobby.", 20))
                {
                    CancelConnecting();
                    // somehow need to keep track of wwhat request was made
                }
                return;
            }
            else if (isPreHosting)
            {
                NetSystemGroup.lobbyAddress.port = int.Parse(DrawInput(NetSystemGroup.lobbyAddress.port.ToString(), "Lobby Port"));
                if (DrawButton("Cancel"))
                {
                    isPreHosting = false;
                }
                if (DrawButton("Confirm"))
                {
                    isPreHosting = false;
                    HostLobby();
                }
                return;
            }
            else if (isPreJoining)
            {
                NetSystemGroup.lobbyAddress.ip = new IPAddress(DrawInput(NetSystemGroup.lobbyAddress.ip.ToString(), "Lobby IP"));
                NetSystemGroup.lobbyAddress.port = int.Parse(DrawInput(NetSystemGroup.lobbyAddress.port.ToString(), "Lobby Port"));
                if (DrawButton("Cancel"))
                {
                    isPreJoining = false;
                }
                if (DrawButton("Confirm"))
                {
                    isPreJoining = false;
                    JoinLobby();
                }
                return;
            }
        }

        bool DrawButton(string label, int height = 25, int width = 300)
        {
            return UnityEngine.GUILayout.Button(label, UnityEngine.GUILayout.Height(height)); // , UnityEngine.GUILayout.Width(width));
        }

        void DrawLabel(string label, int height = 24, int width = 300)
        {
            // UnityEngine.GUILayout.Label(label); // , UnityEngine.GUILayout.Height(height), UnityEngine.GUILayout.Width(width));
            UnityEngine.GUILayout.Label(label, UnityEngine.GUILayout.Height(height)); // , UnityEngine.GUILayout.Width(width));
        }

        string DrawInput(string input, string label, int height = 25, int width = 300)
        {
            if (label != "")
            {
                DrawLabel(label, height, width);
            }
            // return UnityEngine.GUILayout.TextField(input, UnityEngine.GUILayout.Height(height), UnityEngine.GUILayout.Width(width));
            return UnityEngine.GUILayout.TextField(input, UnityEngine.GUILayout.Height(height));
        }

        bool DrawToggle(string label, bool state, int height = 45, int width = 300)
        {
            label = (state ? "[ON]" : "[OFF]") + " " + label;
            // return !UnityEngine.GUILayout.Button(label, UnityEngine.GUILayout.Height(height), UnityEngine.GUILayout.Width(width));
            var buttonValue = UnityEngine.GUILayout.Button(label, UnityEngine.GUILayout.Height(height)); // UnityEngine.GUILayout.Width(width));
            if (state)
            {
                return !buttonValue;
            }
            else
            {
                return buttonValue;
            }
        }

        public EntityManager EntityManager
        {
            get
            {
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        public NetSystemGroup netSystemGroup
        {
            get
            {
                if (!IsECSEnabled)
                {
                    return null; // new NetSystemGroup();
                }
                return World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<NetSystemGroup>();
            }
        }

        public bool IsECSEnabled
        {
            get
            {
                return World.DefaultGameObjectInjectionWorld != null;
            }
        }

        public NetAddress netPlayerAddress
        {
            get
            {
                if (!IsECSEnabled || netPlayerEntity.Index == 0 || !EntityManager.Exists(netPlayerEntity))
                {
                    return new NetAddress();
                }
                else
                {
                    return EntityManager.GetComponentData<EntityNetAddress>(netPlayerEntity).netAddress;
                }
            }
        }

        public Entity netPlayerEntity
        {
            get
            {
                return NetSystemGroup.localNetPlayer;
            }
        }

        public Entity lobbyEntity
        {
            get
            {
                if (!IsECSEnabled || netPlayerEntity.Index == 0 || !EntityManager.Exists(netPlayerEntity))
                { 
                    return new Entity();
                }
                else
                {
                    return EntityManager.GetComponentData<LobbyLink>(netPlayerEntity).lobby;
                }
            }
        }

        public NetListener NetPlayerConnection
        {
            get
            {
                if (!IsECSEnabled || netPlayerEntity.Index == 0 || !EntityManager.Exists(netPlayerEntity)
                    || !EntityManager.HasComponent<NetListener>(netPlayerEntity))
                {
                    return new NetListener();
                }
                else
                {
                    return EntityManager.GetSharedComponentManaged<NetListener>(netPlayerEntity);
                }
            }
        }

        public NetAddress Lobby
        {
            get
            {
                if (!IsECSEnabled || lobbyEntity.Index == 0)
                {
                    return new NetAddress();
                }
                return EntityManager.GetComponentData<EntityNetAddress>(lobbyEntity).netAddress;
            }
        }

        public NetPlayerLinks NetPlayerLinks
        {
            get
            {
                if (!IsECSEnabled || lobbyEntity.Index == 0)
                {
                    return new NetPlayerLinks();
                }
                return EntityManager.GetComponentData<NetPlayerLinks>(lobbyEntity);
            }
        }

        public NetMessages NetMessages
        {
            get
            {
                if (!IsECSEnabled || lobbyEntity.Index == 0)
                {
                    return new NetMessages();
                }
                return EntityManager.GetComponentData<NetMessages>(lobbyEntity);
            }
        }

        public bool LocalPlayerHost
        {
            get
            {
                if (!IsECSEnabled || netPlayerEntity.Index == 0)
                {
                    return false;
                }
                return EntityManager.HasComponent<LobbyHost>(netPlayerEntity);
            }
        }

        public string NetPlayerName
        {
            get
            {
                if (!IsECSEnabled || netPlayerEntity.Index == 0 || !EntityManager.Exists(netPlayerEntity))
                {
                    return "";
                }
                return EntityManager.GetComponentData<ZoxName>(netPlayerEntity).name.ToString();
            }
        }

        public int NetPlayerID
        {
            get
            {
                if (!IsECSEnabled || netPlayerEntity.Index == 0 || !EntityManager.Exists(netPlayerEntity))
                {
                    return 0;
                }
                else
                {
                    return EntityManager.GetComponentData<ZoxID>(netPlayerEntity).id;
                }
            }
        }
    }*/
}
                    // lobbyLabel += " [" + zoxNet.lobby.rooms.Count + "] Rooms.";
                    /*if (zoxNet.IsLobbyHost() && DrawButton("Start Game"))
                    {
                        zoxNet.StartGame();
                    }*/
                    /*var count = 0;
                    if (netPlayerLinks.playersHash.IsCreated)
                    {
                        count = netPlayerLinks.playersHash.Count();
                    }*/

/*
if (zoxNet.room.connectedState == 0)
{
    DrawLobbyRoomsUI();
}
else if (zoxNet.room.connectedState == 2)
{
    DrawInRoomUI();
}
else
{
    DrawJoiningRoomUI();
}*/

/*void DrawLobbyRoomsUI()
{
    if (DrawButton("New Room"))
    {
        EntityManager.AddComponent<LobbyHost>(zoxNet.netPlayerEntity);
        zoxNet.room.connectedState = 2;
        zoxNet.room.name = NameGenerator.GenerateName();   // "Room A";
        var playerName = EntityManager.GetComponentData<ZoxName>(zoxNet.netPlayerEntity).name.ToString();
        var createRoomData = zoxNet.room.name + ":" + playerName;       // + ":" + localPlayer.localIP.ToString() + ":" + localPlayer.localPort;
        zoxNet.room.Add(EntityManager, zoxNet.netPlayerEntity);
        zoxNet.SendPacketToLobby("[createroom]" + createRoomData);
    }
    for (int i = 0; i < zoxNet.lobby.rooms.Count; i++)
    {
        var lobbyRoom = zoxNet.lobby.rooms[i];
        if (DrawButton("Join Room [" + lobbyRoom.name + "] p(" + lobbyRoom.players.Count + ")"))
        {
            // zoxNet.netPlayerEntity.isHost = false;
            EntityManager.RemoveComponent<LobbyHost>(zoxNet.netPlayerEntity);
            zoxNet.room = lobbyRoom;
            zoxNet.room.connectedState = 1;
            var joinRoomData = zoxNet.room.name + ":" + zoxNet.NetPlayerName;
            zoxNet.SendPacketToLobby("[joinroom]" + joinRoomData);
            break;
        }
    }
}

void DrawJoiningRoomUI()
{
    DrawLabel("Joining Room [" + zoxNet.room.name + "]"); //  + room.ip + "::" + room.port);
    if (DrawButton("Cancel Joining"))
    {
        zoxNet.room.connectedState = 0;
        zoxNet.room.Clear();   // clear room
    }
}

void DrawInRoomUI()
{
    if (EntityManager.HasComponent<LobbyHost>(zoxNet.netPlayerEntity))
    {
        DrawLabel(zoxNet.NetPlayerName + " is Hosting Room [" + zoxNet.room.name + "]");
    }
    else
    {
        DrawLabel(zoxNet.NetPlayerName + " is in Room [" + zoxNet.room.name + "]");
    }
    // who is host?
    isShowRoomPlayers = DrawToggle("Show Room Players (" + zoxNet.room.players.Count + ")", isShowRoomPlayers);
    if (isShowRoomPlayers)
    {
        for (int i = 0; i < zoxNet.room.players.Count; i++)
        {
            var player = zoxNet.room.players[i];
            if (zoxNet.netPlayerEntity != player)
            {
                DrawLabel("   " + NetPlayer.GetUILabel(EntityManager, zoxNet.netPlayerEntity));
            }
            // name + " - Public: " + ip.ToString() + ":" + port + " - Local: " +  localIP.ToString() + ":" + localPort;
            // DrawLabel("   " + player.GetUILabel()); //  + " at: " + player.ip + " :: " + player.port);
        }
    }
    DrawLabel("-       -----       -");
    for (var i = 0; i < zoxNet.room.messages.Count; i++)
    {
        DrawLabel("   [" + i + "] " + zoxNet.room.messages[i]);
    }
    // perhaps a message input box here
    if (DrawButton("Broadcast Room Message"))
    {
        var message = zoxNet.NetPlayerName + " - " + "Hi, are you " + NameGenerator.GenerateName();
        //zoxNet.SendRoomMessage(message);

        //zoxNet.AddMessage(message);
        //zoxNet.RoomBroadcastMessage(zoxNet.room, message, zoxNet.NetPlayer.port);
    }
    if (DrawButton("Leave Room"))
    {
        zoxNet.LeaveRoom();
    }
}*/

        /*public byte connectedState
        {
            get
            {
                if (!IsECSEnabled || !EntityManager.Exists(netPlayerEntity))
                {
                    return 0;
                }
                else
                {
                    return EntityManager.GetComponentData<LobbyConnectedState>(netPlayerEntity).connectedState;
                }
            }
        }*/
            /*if (connectedState == ConnectedState.None)
            {
                if (DrawButton("Join"))
                {
                    isPreJoining = true;
                }
                if (canHost)
                {
                    if (DrawButton("Host"))
                    {
                        isPreHosting = true;
                    }
                }
            }
            else if (connectedState == ConnectedState.Connecting)
            {
                var lobby = Lobby;
                DrawLabel("Connecting to Lobby: " + lobby.ip.ToString() + "::" + lobby.port);
                // cancel connect to lobby?
                if (DrawButton("Cancel Connecting to Lobby.", 20))
                {
                    CancelConnecting();
                    // somehow need to keep track of wwhat request was made
                }
            }
            else if (connectedState == ConnectedState.Connected)
            {
                if (IsLobbyHost() && DrawButton("Close Lobby"))
                {
                    CloseLobby();
                }
                if (canLeaveLobby && !IsLobbyHost() && DrawButton("Leave Lobby"))
                {
                    LeaveLobby();
                }
                DrawOverview();
                var lobby = Lobby;
                var netPlayerLinks = NetPlayerLinks;
                var netMessages = NetMessages;
                var playerName = EntityManager.GetComponentData<ZoxName>(netPlayerEntity).name.ToString();
                var playerID = EntityManager.GetComponentData<ZoxID>(netPlayerEntity).id;
                if (isShowOverview)
                {
                    DrawLabel("In Lobby [" + lobby + "]");
                    DrawLabel("> players [" + netPlayerLinks.players.Length + "]");
                    DrawLabel("> you are " + (IsLobbyHost() ? "[host]" : "[guest]"));
                }
                // need some IsInLobby information
                isShowLobbyPlayers = DrawToggle("Players", isShowLobbyPlayers, 20);
                if (isShowLobbyPlayers)
                {
                    for (int i = 0; i < netPlayerLinks.players.Length; i++)
                    {
                        var player = netPlayerLinks.players[i];
                        if (!EntityManager.Exists(player))
                        {
                            DrawLabel("[" + i + "] is destroyed.");
                            continue;
                        }
                        var playerLabel = "    ";
                        if (EntityManager.HasComponent<LocalNetPlayer>(player))
                        {
                            playerLabel += " (you) ";
                        }
                        if (EntityManager.HasComponent<LobbyHost>(player))
                        {
                            playerLabel += " (host) ";
                        }
                        if (EntityManager.HasComponent<PingFailing>(player))
                        {
                            playerLabel += "(unstable) ";
                        }
                        playerLabel += NetPlayer.GetUILabel(EntityManager, player);
                        DrawLabel(playerLabel);
                    }
                }
                isShowChat = DrawToggle("Chat", isShowChat, 20);
                if (isShowChat)
                {
                    message = DrawInput(message, "");
                    if (message != "" && DrawButton(">", 24))
                    {
                        SendLobbyMessage(NetPlayerName + ": " + message);
                        message = "";
                    }
                    for (var i = netMessages.messages.Length - 1; i >= math.max(0, netMessages.messages.Length - 10) ; i--)
                    {
                        DrawLabel("   [" + i + "] " + netMessages.messages[i]);
                    }
                }
                //DrawLabel("-       -----       -");
            }*/