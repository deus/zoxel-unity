using System;
    
namespace Zoxel.Networking
{
    //! Stores an IP Address using 4 bytes.
    public struct IPAddress : IEquatable<IPAddress>
    {
        public byte digit0;
        public byte digit1;
        public byte digit2;
        public byte digit3;

        public IPAddress(string ip)
        {
            if (ip == "")
            {
                this.digit0 = 0;
                this.digit1 = 0;
                this.digit2 = 0;
                this.digit3 = 0;
                return;
            }
            try
            {
                var ipSplits = ip.Split('.');
                this.digit0 = ((byte) int.Parse(ipSplits[0]));
                this.digit1 = ((byte) int.Parse(ipSplits[1]));
                this.digit2 = ((byte) int.Parse(ipSplits[2]));
                this.digit3 = ((byte) int.Parse(ipSplits[3]));
            }
            catch (Exception e2)
            {
                UnityEngine.Debug.LogError("Exception Passing [" + ip + "] IPAddress:" + e2);
                this.digit0 = 0;
                this.digit1 = 0;
                this.digit2 = 0;
                this.digit3 = 0;
            }
        }

        public override string ToString()
        {
            return "" + ((int) digit0) + '.' + ((int) digit1) + '.' + ((int) digit2) + '.' + ((int) digit3);
        }

        /*public override bool Equals(object obj)
        {
            return ToString() == obj.ToString();
        }*/

        public override bool Equals(object other)
        {
            return other is IPAddress && Equals((IPAddress) other);
        }
        
        public bool Equals(IPAddress other)
        {
            return digit0 == other.digit0 && digit1 == other.digit1 && digit2 == other.digit2 && digit3 == other.digit3;
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                // Shifts bytes into an integer
                return  (digit0  & 0xff << 24) |
                        (digit1  & 0xff << 16) |
                        (digit2  & 0xff <<  8) |
                        (digit3  & 0xff);
            }
        }

        public static bool operator ==(IPAddress a, IPAddress b)
        {
            return a.digit0 == b.digit0 && a.digit1 == b.digit1 && a.digit2 == b.digit2 && a.digit3 == b.digit3;
        }

        public static bool operator !=(IPAddress a, IPAddress b)
        {
            return a.digit0 != b.digit0 || a.digit1 != b.digit1 || a.digit2 != b.digit2 || a.digit3 != b.digit3;
        }
    }
}