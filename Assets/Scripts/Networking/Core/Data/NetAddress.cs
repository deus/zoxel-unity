using Unity.Entities;
using System;
    
namespace Zoxel.Networking
{
    //! Stores an IP Address and a port.
    public struct NetAddress
    {
        public IPAddress ip;
        public int port;

        public NetAddress(string ip, int port)
        {
            this.ip = new IPAddress(ip);
            // this.ip = new Text(ip);
            this.port = port;
        }

        public NetAddress(IPAddress ip, int port)
        {
            this.ip = ip;
            this.port = port;
        }

        public override string ToString()
        {
            return ip + ":" + port;
        }
    }
}