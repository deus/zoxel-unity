using Unity.Entities;

namespace Zoxel.Networking
{
    //! Starts listening for incoming packets.
    [UpdateInGroup(typeof(NetSystemGroup))]
    public partial class NetConnectionInitializeSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        protected override void OnUpdate()
        {
            //! Move this elsewhere. Links Connect entity to UI.
            Entities
                .WithAll<InitializeEntity, ConnectToLobby>()
                .ForEach((Entity e) =>
            {
                NetSystemGroup.connectEntity = e;
            }).WithoutBurst().Run();
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithNone<TransferNetListener>()
                .WithAll<InitializeEntity, NetListener>()
                .ForEach((Entity e, in LocalNetAddress localNetAddress) =>
            {
                var netAddress = localNetAddress.netAddress;
                var netListener = new NetListener();
                netListener.listener = e;
                netListener.Start(netAddress.ip, netAddress.port);
                PostUpdateCommands.SetSharedComponentManaged(e, netListener);
            }).WithoutBurst().Run();
            Entities
                .WithAll<TransferNetListener, NetListener>()
                .ForEach((Entity e, in TransferNetListener transferNetListener) =>
            {
                PostUpdateCommands.RemoveComponent<TransferNetListener>(e);
                var netListener = EntityManager.GetSharedComponentManaged<NetListener>(transferNetListener.listener);
                netListener.listener = e;
                PostUpdateCommands.SetSharedComponentManaged(e, netListener);
                PostUpdateCommands.SetSharedComponentManaged(transferNetListener.listener, new NetListener());
                NetListener.transferedListeners.Add(transferNetListener.listener, e);
                // UnityEngine.Debug.LogError("Old Listener [" + transferNetListener.listener.Index + "] - New Listener [" + e.Index + "].");
            }).WithoutBurst().Run();
        }
    }
}