using Unity.Entities;

namespace Zoxel.Networking
{
    [UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class NetConnectionDestroySystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, NetListener>()
                .ForEach((in NetListener netListener) =>
            {
                netListener.Dispose();
            }).WithoutBurst().Run();
        }
    }
}