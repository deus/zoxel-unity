using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Networking
{
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class NetMessagesDestroySystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, NetMessages>()
                .ForEach((in NetMessages netMessages) =>
            {
                netMessages.Dispose();
            }).ScheduleParallel();
        }
    }
}