using Unity.Entities;
using System;
    
namespace Zoxel.Networking
{
    public struct LocalNetAddress : IComponentData
    {
        public NetAddress netAddress;

        public LocalNetAddress(NetAddress netAddress)
        {
            this.netAddress = netAddress;
        }
    }
}