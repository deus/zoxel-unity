using Unity.Entities;

namespace Zoxel.Networking
{
    //! The lobby will send all data to the NetPlayer.
    public struct SynchNetPlayer : IComponentData { public byte count; }
}