using Unity.Entities;

namespace Zoxel.Networking
{
    public struct TargetNetAddress : IComponentData
    {
        public NetAddress netAddress;

        public TargetNetAddress(NetAddress netAddress)
        {
            this.netAddress = netAddress;
        }
    }
}