using Unity.Entities;
using System;
    
namespace Zoxel.Networking
{
    public struct EntityNetAddress : IComponentData
    {
        public NetAddress netAddress;

        public EntityNetAddress(NetAddress netAddress)
        {
            this.netAddress = netAddress;
        }
    }
}