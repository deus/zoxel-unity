using Unity.Entities;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;          // Encoding
using System.Collections.Generic;

namespace Zoxel.Networking
{
    //! Holds the UDPClient for net connections.
    public struct NetListener : ISharedComponentData, IEquatable<NetListener>
    {
        public static Dictionary<Entity, Entity> transferedListeners = new Dictionary<Entity, Entity>();
        public UdpClient client;
        public IPEndPoint endpoint;
        public Entity listener;

        public bool Equals(NetListener other) 
            => ReferenceEquals(client, other.client);

        public override int GetHashCode()
        {
            if (client == null)
            {
                return 0;
            }
            else
            {
                return client.GetHashCode();
            }
        }

        public void Dispose()
        {
            if (client != null)
            {
                client.Close();
                client.Dispose();
                client = null;
            }
        }
        
        public bool Start(IPAddress address2, int port)
        {
            if (client == null)
            {
                var address = System.Net.IPAddress.Parse(address2.ToString());
                try
                {
                    endpoint = new IPEndPoint(address, port);
                    client = new UdpClient(endpoint);
                    client.BeginReceive(new AsyncCallback(RecieveMessage), this);   // client);  // SocketFlags.Multicast, 
                    UnityEngine.Debug.Log("Listening to messages from Location: " + address + ":" + port);
                    return true;
                }
                catch (Exception e)
                {
                    UnityEngine.Debug.LogError("Start Error for [" + address2 + ":" + port + "]: " + e.ToString());
                }
            }
            return false;
        }
        
        private static void RecieveMessage(IAsyncResult result)
        {
            var netListener = (NetListener) result.AsyncState;
            var udpClient = netListener.client; // (UdpClient) result.AsyncState;
            if (netListener.client == null)
            {
                return;
            }
            if (NetListener.transferedListeners.ContainsKey(netListener.listener))
            {
                netListener.listener = transferedListeners[netListener.listener];
                NetListener.transferedListeners.Remove(netListener.listener);
            }
            try
            {
                var remoteEndPoint = new IPEndPoint(System.Net.IPAddress.Any, 0);
                var bytes = netListener.client.EndReceive(result, ref remoteEndPoint);
                // UnityEngine.Debug.LogError("Net Connection, Packet Recieved [" + bytes.Length + "]");
                if (bytes.Length > 0)
                {
                    #if DEBUG_PACKETS_RECIEVED
                    if (bytes[0] != PacketType.Ping)
                    {
                        UnityEngine.Debug.Log("Recieved packet [" + bytes.Length + "] from IP [" + remoteEndPoint.Address + "] - [" + bytes[0] + "]");
                    }
                    #endif
                    PacketSpawnSystem.messages.Enqueue(new PacketStaticData(netListener.listener, remoteEndPoint.Address.ToString(), remoteEndPoint.Port, ref bytes));
                }
            }
            catch (Exception e)
            {
                if (e is ObjectDisposedException || e is SocketException)
                {
                    return;
                }
                UnityEngine.Debug.LogError(e.ToString());
            }
            netListener.client.BeginReceive(new AsyncCallback(RecieveMessage), netListener);
        }
    }
}