using Unity.Entities;

namespace Zoxel.Networking
{
    //! An entity is created when messages are recieved and an entity is created for it for processing.
    public struct NetListenerLink : IComponentData
    {
        public Entity listener;

        public NetListenerLink(Entity listener)
        {
            this.listener = listener;
        }
    }
}