namespace Zoxel.Networking
{
    public static class ConnectedState
    {
        public const byte None = 0;
        public const byte Connecting = 1;
        public const byte Connected = 2;
    }
}