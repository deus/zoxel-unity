using Unity.Entities;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;          // Encoding
    
namespace Zoxel.Networking
{
    //! Some functions used to send using a temporary UdpClient.
    public static class NetSendUtil
    {
        public static void LobbyBroadcastPacket(in byte[] bytes, in NetAddress lobby, in NetPlayerLinks netPlayerLinks, Entity hostPlayer,
            in ComponentLookup<EntityNetAddress> netPlayers)
        {
            #if DEBUG_PACKETS
                UnityEngine.Debug.Log("Lobby Broadcast Packet to not Host Lobby Players: " + (netPlayerLinks.players.Length - 1));
            #endif
            for (int i = 0; i < netPlayerLinks.players.Length; i++)
            {
                var netPlayerEntity = netPlayerLinks.players[i];
                if (hostPlayer != netPlayerEntity)
                {
                    var netPlayer = netPlayers[netPlayerEntity].netAddress;
                    SendPacketToPlayer(in bytes, in lobby, in netPlayer);
                }
            }
        }

        public static void LobbyBroadcastPacket(string packet, in NetAddress lobby, in NetPlayerLinks netPlayerLinks, Entity hostPlayer,
            in ComponentLookup<EntityNetAddress> netPlayers)
        {
            // var netPlayerLinks = EntityManager.GetComponentData<NetPlayerLinks>(lobbyEntity);
            #if DEBUG_PACKETS
                UnityEngine.Debug.Log("Lobby Broadcast Packet to not Host Lobby Players: " + (netPlayerLinks.players.Length - 1));
            #endif
            for (int i = 0; i < netPlayerLinks.players.Length; i++)
            {
                var netPlayerEntity = netPlayerLinks.players[i];
                if (hostPlayer != netPlayerEntity) // !EntityManager.HasComponent<LocalNetPlayer>(playerEntity))
                {
                    var netPlayer = netPlayers[netPlayerEntity].netAddress;
                    SendPacketToPlayer(packet, in lobby, in netPlayer);
                }
            }
        }

        public static void SendPacketToPlayer(string packet, in NetAddress lobby, in NetAddress netPlayer)
        {
            SendPacket(packet, lobby.port, netPlayer.ip.ToString(), netPlayer.port);
        }

        public static void SendPacketToLobby(string packet, in NetAddress lobby, in NetAddress netPlayer)
        {
            SendPacket(packet, netPlayer.port, lobby.ip.ToString(), lobby.port);
        }

        public static void SendPacket(string packet, int fromPort, string ip = "localhost", int port = 11000)
        {
            try
            {
                var bytes = Encoding.ASCII.GetBytes(packet);
                using (var sendClient = new UdpClient(fromPort))
                {
                    var endPoint = ((IPEndPoint) sendClient.Client.LocalEndPoint);
                    #if DEBUG_PACKETS
                        UnityEngine.Debug.Log(endPoint.Address + "::" + endPoint.Port + " is Sending Packet [" + packet + "] to: " + ip + "::" + port);
                    #endif
                    sendClient.Send(bytes, bytes.Length, ip, port);
                }
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogError("Exception Sending Packet: " + e.ToString());
            }
        }

        public static void SendPacketToLobby(in byte[] bytes, in NetAddress lobby, in NetAddress netPlayer)
        {
            SendPacket(in bytes, netPlayer.port, lobby.ip.ToString(), lobby.port);
        }

        public static void SendPacketToPlayer(in byte[] bytes, in NetAddress lobby, in NetAddress netPlayer)
        {
            SendPacket(bytes, lobby.port, netPlayer.ip.ToString(), netPlayer.port);
        }

        public static void SendPacketTo(in byte[] bytes, in NetAddress fromAddress, in NetAddress toAddress)
        {
            SendPacket(bytes, fromAddress.port, toAddress.ip.ToString(), toAddress.port);
        }

        public static void SendPacket(in byte[] bytes, int fromPort, string toIP, int toPort)
        {
            try
            {
                using (var sendClient = new UdpClient(fromPort))
                {
                    var endPoint = ((IPEndPoint) sendClient.Client.LocalEndPoint);
                    #if DEBUG_PACKETS
                        UnityEngine.Debug.Log(endPoint.Address + "::" + endPoint.Port + " is Sending Packet [" + bytes.Length + "] bytes to: " + toIP + "::" + toPort);
                    #endif
                    sendClient.Send(bytes, bytes.Length, toIP, toPort);
                }
            }
            catch (Exception e)
            {
                UnityEngine.Debug.LogError("Exception Sending Packet: " + e.ToString());
            }
        }
    }
}