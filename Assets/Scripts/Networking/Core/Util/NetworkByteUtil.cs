namespace Zoxel.Networking
{
    public static class NetworkByteUtil
    {
        public static void SetNetAddress(ref BlitableArray<byte> bytes, int index, in NetAddress netAddress)
        {
            bytes[index + 0] = netAddress.ip.digit0;
            bytes[index + 1] = netAddress.ip.digit1;
            bytes[index + 2] = netAddress.ip.digit2;
            bytes[index + 3] = netAddress.ip.digit3;
            ByteUtil.SetInt(ref bytes, index + 4, netAddress.port);
        }

        public static NetAddress GetNetAddress(in BlitableArray<byte> bytes, int index)
        {
            var netAddress = new NetAddress();
            netAddress.ip.digit0 = bytes[index + 0];
            netAddress.ip.digit1 = bytes[index + 1];
            netAddress.ip.digit2 = bytes[index + 2];
            netAddress.ip.digit3 = bytes[index + 3];
            netAddress.port = ByteUtil.GetInt(in bytes, index + 4);
            return netAddress;
        }
    }
}