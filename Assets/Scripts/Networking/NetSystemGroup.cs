using Unity.Entities;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace Zoxel.Networking
{
    //! The grouping of network systems.
    /**
    *   \todo Handle disconnection in new UI flow.
    *   \todo Load Servers Realm State. Load Realm automatically in headless server.
    */
    [AlwaysUpdateSystem]
    public partial class NetSystemGroup : ComponentSystemGroup
    {
        public static NetSystemGroup instance;
        public static Entity clientLobbyPrefab;
        public static Entity hostLobbyPrefab;
        public static Entity netPlayerPrefab;
        public static Entity localNetPlayerPrefab;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        // UI entities
        private bool hasInitialized;            // sets the local ip in update function
        public static Entity localNetPlayer;
        public static Entity connectEntity;
        // UI / Init
        private const string zoxelURL = "zoxel.duckdns.org";
        public static int2 portRange = new int2(11100, 12000);
        public static NetAddress localAddress;
        public static NetAddress lobbyAddress;
        private const int lobbyPort = 8088;
        private const int connectPort = 9099;
        
        protected override void OnCreate()
        {
            base.OnCreate();
            instance = this;
            var lobbyAchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(Lobby),
                typeof(EntityNetAddress),
                typeof(LocalNetPlayerLink),
                typeof(NetPlayerLinks),
                typeof(NetMessages),
                typeof(ZoxID),
                typeof(ZoxName)
            );
            clientLobbyPrefab = EntityManager.CreateEntity(lobbyAchetype);
            var hostLobbyAchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(Lobby),
                typeof(EntityNetAddress),
                typeof(LocalNetPlayerLink),
                typeof(NetPlayerLinks),
                typeof(NetMessages),
                typeof(ZoxID),
                typeof(ZoxName),
                typeof(LobbyHost),
                typeof(LocalNetAddress),
                typeof(NetListener)
            );
            hostLobbyPrefab = EntityManager.CreateEntity(hostLobbyAchetype);
            var localNetPlayerArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                // typeof(ZoxNetAddress),      // location of place to get public IP from
                typeof(ZoxID),
                typeof(ZoxName),
                typeof(NetPlayer),
                typeof(EntityNetAddress),
                typeof(LobbyLink),
                typeof(NetListener),
                typeof(LocalNetPlayer),
                typeof(LocalNetAddress)
            );
            localNetPlayerPrefab = EntityManager.CreateEntity(localNetPlayerArchetype);
            var netPlayerArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(ZoxID),
                typeof(ZoxName),
                typeof(NetPlayer),
                typeof(EntityNetAddress),
                typeof(LobbyLink)
            );
            netPlayerPrefab = EntityManager.CreateEntity(netPlayerArchetype);
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            //! \todo Set port after player connects, atm this doesn't listen properly afterwards. UDPClient issues?
            NetSystemGroup.localAddress.port = UnityEngine.Random.Range(portRange.x, portRange.y);  //connectPort;
            NetSystemGroup.lobbyAddress.port = lobbyPort;
        }

        protected override void OnUpdate()
        {
            base.OnUpdate();
            if (hasInitialized)
            {
                return;
            }
            hasInitialized = true;
            // Set IPs
            var lobbyIPString = "";
            try
            {
                // var localIPString = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0].ToString();
                var localIPHostInfo = Dns.GetHostEntry(Dns.GetHostName());
                if (localIPHostInfo != null)
                {
                    System.Net.IPAddress localIPAddress = null;
                    foreach (var possibleIP in localIPHostInfo.AddressList)
                    {
                        if (possibleIP.AddressFamily == AddressFamily.InterNetwork)
                        {
                            localIPAddress = possibleIP;
                            break;
                        }
                    }
                    //var localIPAddress = localIPHostInfo.AddressList
                    //    .FindLast(a => a.AddressFamily == AddressFamily.InterNetwork);
                    if (localIPAddress != null)
                    {
                        var localIPString = localIPAddress.ToString();
                        UnityEngine.Debug.Log("Local Dns.GetHostEntry [" + localIPString + "]");
                        localAddress.ip = new IPAddress(localIPString);
                    }
                    else
                    {
                        UnityEngine.Debug.LogError("Local InterNetwork was not found.");
                    }
                }
                //! \todo Keep url in ui.
                lobbyIPString = Dns.Resolve(zoxelURL).AddressList[0].ToString();
            }
            catch (Exception e2)
            {
                UnityEngine.Debug.LogError("Exception trying to get IP!:\n" + e2);
            }
            if (lobbyIPString == "")
            {
                UnityEngine.Debug.LogError("DNS Failed to resolve [" + zoxelURL + "]");
                hasInitialized = false;
                return;
            }
            UnityEngine.Debug.Log("Dns.Resolve [" + zoxelURL + "]: " + lobbyIPString);
            lobbyAddress.ip = new IPAddress(lobbyIPString);
            if (NetworkUtil.IsHeadless())
            {
                lobbyAddress.ip = localAddress.ip;
                var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
                PostUpdateCommands.SetComponent(PostUpdateCommands.Instantiate(LobbyHostSystem.hostLobbyPrefab),
                    new HostLobby(lobbyAddress));
                UnityEngine.Debug.LogError("===========");
                UnityEngine.Debug.LogError("Started Zoxel Server.");
                UnityEngine.Debug.LogError("===========");
            }
        }
    }
}