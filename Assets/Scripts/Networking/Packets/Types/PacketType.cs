namespace Zoxel.Networking
{
    //! Added to packet header to indicate what type of packet it is.
    public static class PacketType
    {
        public const byte Ping = 0;
        // lobby
        public const byte ConnectToLobby = 1;
        public const byte ConnectedToLobby = 2;
        public const byte CloseLobby = 3;
        public const byte LeaveLobby = 4;
        public const byte LobbyAddPlayer = 5;
        public const byte LobbyRemovePlayer = 6;
        public const byte LobbyMessage = 7;
        // transforms
        public const byte PositionSynch = 8;
    }
}