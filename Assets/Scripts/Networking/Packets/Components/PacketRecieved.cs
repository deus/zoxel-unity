using Unity.Entities;

namespace Zoxel.Networking
{
    //! An packet entity that has been recieved.
    public struct PacketRecieved : IComponentData { }
}