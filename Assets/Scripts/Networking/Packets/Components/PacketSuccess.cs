using Unity.Entities;

namespace Zoxel.Networking
{
    //! If a SecurePacket recieved a confirmation.
    public struct PacketSuccess : IComponentData { }
}