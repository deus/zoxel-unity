using Unity.Entities;

namespace Zoxel.Networking
{
    //! An entity is created when messages are recieved and an entity is created for it for processing.
    public struct PacketMessage : IComponentData
    {
        public Text data;

        public PacketMessage(Text data)
        {
            this.data = data;
        }

        public void Dispose()
        {
            data.Dispose();
        }
    }
}