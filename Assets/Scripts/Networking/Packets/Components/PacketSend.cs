using Unity.Entities;

namespace Zoxel.Networking
{
    //! An packet entity is being sent.
    public struct PacketSend : IComponentData { }
}