using Unity.Entities;

namespace Zoxel.Networking
{
    //! Raw data recieved by UDPListener
    public struct PacketStaticData
    {
        public Entity listenedEntity;
        public string ip;
        public int port;
        public byte[] data;

        public PacketStaticData(Entity listenedEntity, string ip, int port, ref byte[] data)
        {
            this.listenedEntity = listenedEntity;
            this.ip = ip;
            this.port = port;
            this.data = data;
        }
    }
}