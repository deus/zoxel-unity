using Unity.Entities;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! An entity is created when messages are recieved and an entity is created for it for processing.
    public struct PacketData : IComponentData
    {
        public BlitableArray<byte> data;

        public PacketData(BlitableArray<byte> data)
        {
            this.data = data;
        }

        public PacketData(in byte[] data)
        {
            this.data = new BlitableArray<byte>(data.Length, Allocator.Persistent);
            for (int i = 0; i < data.Length; i++)
            {
                this.data[i] = data[i];
            }
        }

        public void Dispose()
        {
            data.Dispose();
        }
    }
}