using Unity.Entities;

namespace Zoxel.Networking
{
    //! If a SecurePacket sends too many times, it will fail.
    public struct PacketFailed : IComponentData { }
}