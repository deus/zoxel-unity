using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Networking
{
    [BurstCompile, UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class PacketDataDestroySystem : SystemBase
    {
        [BurstCompile]
        protected override void OnUpdate()
        {
            Entities
                .WithAll<DestroyEntity, PacketData>()
                .ForEach((in PacketData packetData) =>
            {
                packetData.Dispose();
            }).ScheduleParallel();
            Entities
                .WithAll<DestroyEntity, PacketMessage>()
                .ForEach((in PacketMessage packetMessage) =>
            {
                packetMessage.Dispose();
            }).ScheduleParallel();
        }
    }
}