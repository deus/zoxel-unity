using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Zoxel.Networking.Authoring;
using System.Collections.Generic;
using System.Text;      // Encoding

namespace Zoxel.Networking
{
    //! Spawns Packet Entities to be used by ECS. Data queued by NetListener component.
    /**
    *   - Spawn System -
    *   Should these packets die after one frame? GenericEvent?
    */
    [AlwaysUpdateSystem]
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class PacketSpawnSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Queue<PacketStaticData> messages = new Queue<PacketStaticData>();
        private Entity packetRecievedPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(EntityDestroySingleFrame),
                typeof(NetListenerLink),
                typeof(Packet),
                typeof(PacketRecieved),
                typeof(PacketData),
                typeof(EntityNetAddress));
            packetRecievedPrefab = EntityManager.CreateEntity(archetype);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var isDebugPacketsRecieved = false;
            if (HasSingleton<NetworkSettings>())
            {
                var networkSettings = GetSingleton<NetworkSettings>();
                isDebugPacketsRecieved = networkSettings.isDebugPacketsRecieved;
            }
            var messages = PacketSpawnSystem.messages;
            if (messages.Count == 0)
            {
                return;
            }
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            var messageEntities = new NativeArray<Entity>(messages.Count, Allocator.Temp);
            PostUpdateCommands.Instantiate(packetRecievedPrefab, messageEntities);
            var i = 0;
            foreach (var messageData in messages)
            // for (int i = 0; i < messages.Count; i++)
            {
                // var messageData = messages.First();
                // messages.Remove(messageData);
                // var messageData = messages[i];
                var e = messageEntities[i]; // PostUpdateCommands.Instantiate(packetRecievedPrefab);
                i++;
                PostUpdateCommands.SetComponent(e, new PacketData(in messageData.data));
                PostUpdateCommands.SetComponent(e, new EntityNetAddress(new NetAddress(messageData.ip, messageData.port)));
                // change this to PacketTarget - entity
                //      And PacketToLobby, PacketToPlayer
                PostUpdateCommands.SetComponent(e, new NetListenerLink(messageData.listenedEntity));
                if (HasComponent<Lobby>(messageData.listenedEntity))
                {
                    // UnityEngine.Debug.LogError("Lobby got a packet. Size was: " + messageData.data.Length);
                    PostUpdateCommands.AddComponent(e, new LobbyLink(messageData.listenedEntity));
                }
                else if (HasComponent<NetPlayer>(messageData.listenedEntity))
                {
                    // UnityEngine.Debug.LogError("Player got a packet. Size was: " + messageData.data.Length);
                    PostUpdateCommands.AddComponent(e, new NetPlayerLink(messageData.listenedEntity));
                }
                if (isDebugPacketsRecieved)
                {
                    UnityEngine.Debug.LogError("Packet Recieved [" + messageData.data[0] + "] Recieved on Listener [" + messageData.listenedEntity.Index + "].");
                }
            }
            messages.Clear();
        }
    }
}