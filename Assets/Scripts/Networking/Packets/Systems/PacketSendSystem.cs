using Unity.Entities;
using Unity.Burst;
using Zoxel.Networking.Authoring;

namespace Zoxel.Networking
{
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class PacketSendSystem : SystemBase
    {
        public static Entity packetSendPrefab;

        protected override void OnCreate()
        {
            var packetSendArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(EntityDestroySingleFrame),
                typeof(Packet),
                typeof(PacketSend),
                typeof(PacketData),
                typeof(TargetNetAddress),
                typeof(EntityNetAddress));
            packetSendPrefab = EntityManager.CreateEntity(packetSendArchetype);
            RequireForUpdate<NetworkSettings>();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var networkSettings = GetSingleton<NetworkSettings>();
            var isDebugPacketsSent = networkSettings.isDebugPacketsSent;
            Entities
                .WithNone<DestroyEntity>()
                .WithAll<PacketSend>()
                .ForEach((in PacketData packetData, in EntityNetAddress entityNetAddress, in TargetNetAddress targetNetAddress) =>
            {
                var netAddress = entityNetAddress.netAddress;
                var targetNetAddress2 = targetNetAddress.netAddress;
                var bytesArray = packetData.data.ToArray();
                NetSendUtil.SendPacketTo(in bytesArray, in netAddress, in targetNetAddress2);
                if (isDebugPacketsSent)
                {
                    UnityEngine.Debug.LogError("Packet Send [" + bytesArray[0] + "] to Target: " + targetNetAddress2 + " from: " + netAddress);
                }
            }).WithoutBurst().Run();
        }
    }
}