using Unity.Entities;

namespace Zoxel.Networking
{
    //! A secure packet waits for a recieved confirmation, if not recieved then it tries to send again.
    /**
    *   If recieved no reply, resend again, every second until recieved.
    *   For synching player to game state, keep a hashmap of secure packets and wait until they've all been confirmed before progressing connection state.
    *   \todo This. Implement security systems for a packet.
    *   \todo Use this for joining. Can cancel the action as well.
    */
    public struct SecurePacket : IComponentData { }
}