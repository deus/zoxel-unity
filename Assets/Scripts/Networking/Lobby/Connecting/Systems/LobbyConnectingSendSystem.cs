using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! When a connection wants to connect to the lobby.
    /**
    *   - Send Packet System -
    *   JoinLobby > ConnectingToLobby > ConnectToLobby sends packet to lobby
    *   Lobby gets > ConnectedToLobbyRecieveSystem > ConfirmConnectingToLobby
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LobbyConnectingSendSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var packetSendPrefab = PacketSendSystem.packetSendPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<ConnectToLobby>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<ConnectToLobby>()
                .ForEach((Entity e, int entityInQueryIndex, in EntityNetAddress entityNetAddress, in TargetNetAddress targetNetAddress) =>
            {
                // Send response packet
                var packetData = new BlitableArray<byte>(1, Allocator.Persistent);
                packetData[0] = PacketType.ConnectToLobby;
                var sendPacketEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, packetSendPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, entityNetAddress);
                PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, targetNetAddress);
                PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new PacketData(packetData));
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}