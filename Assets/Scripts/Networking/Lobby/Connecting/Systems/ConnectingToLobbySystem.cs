using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Networking
{
    //! Connect to lobby every x seconds
    /**
    *   \todo Generic Timer Events for reconnecting
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class ConnectingToLobbySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity connectToLobbyPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(ConnectToLobby),
                typeof(ConnectingToLobby),
                typeof(EntityNetAddress),
                typeof(TargetNetAddress),
                // use this for recieving confirmation
                typeof(LocalNetAddress),
                typeof(NetListener));
            connectToLobbyPrefab = EntityManager.CreateEntity(archetype);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            const float reconnectRate = 3f;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithNone<ConnectToLobby>()
                .ForEach((Entity e, int entityInQueryIndex, ref ConnectingToLobby connectingToLobby) =>
            {
                if (connectingToLobby.timeRate == 0)
                {
                    connectingToLobby.timeRate = reconnectRate;
                    connectingToLobby.timeStartedConnecting = elapsedTime;
                }
                else if (elapsedTime - connectingToLobby.timeStartedConnecting >= connectingToLobby.timeRate)
                {
                    connectingToLobby.timeStartedConnecting = elapsedTime;
                    PostUpdateCommands.AddComponent<ConnectToLobby>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}