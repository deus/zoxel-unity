using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Networking
{
    //! Reads ping packets and adds events to NetPlayer entities.
    /**
    *   - Bounce Packet System -
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LobbyConnectBounceSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery lobbyQuery;
        private EntityQuery netPlayersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            lobbyQuery = GetEntityQuery(ComponentType.ReadOnly<Lobby>());
            netPlayersQuery = GetEntityQuery(ComponentType.ReadOnly<NetPlayer>());
            RequireForUpdate(processQuery);
            RequireForUpdate(lobbyQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var portRange = NetSystemGroup.portRange;
            var uniqueID = IDUtil.GenerateUniqueID();
            var lobbyAddPlayerPrefab = LobbyAddPlayerSystem.lobbyAddPlayerPrefab;
            var netPlayerPrefab = NetSystemGroup.netPlayerPrefab;
            var packetSendPrefab = PacketSendSystem.packetSendPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var lobbyEntities = lobbyQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var lobbyNetAddresses = GetComponentLookup<EntityNetAddress>(true);
            var netPlayerLinks = GetComponentLookup<NetPlayerLinks>(true);
            lobbyEntities.Dispose();
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var netAddresses = GetComponentLookup<EntityNetAddress>(true);
            netPlayerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved>()
                .ForEach((Entity e, int entityInQueryIndex, in PacketData packetData, in EntityNetAddress entityNetAddress, in LobbyLink lobbyLink) =>
            {
                if (packetData.data.Length == 1 && packetData.data[0] == PacketType.ConnectToLobby)
                {
                    var netAddress = entityNetAddress.netAddress;
                    var lobbyNetAddress = lobbyNetAddresses[lobbyLink.lobby].netAddress;
                    // generate player data
                    var newPlayerID = uniqueID + entityInQueryIndex;
                    var random = new Random();
                    random.InitState((uint) newPlayerID);
                    var newPlayerName = NameGenerator.GenerateName2(ref random);
                    // spawn new net player
                    var playerName = new ZoxName(newPlayerName);
                    var playerID = new ZoxID(newPlayerID);
                    var playerNetAddress = new EntityNetAddress(netAddress);
                    var playerEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, netPlayerPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, lobbyLink);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, playerID);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, playerName);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, playerNetAddress);
                    PostUpdateCommands.SetComponent(entityInQueryIndex,
                        PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyAddPlayerPrefab),
                        new LobbyAddPlayer(lobbyLink.lobby, playerEntity));
                    PostUpdateCommands.AddComponent<SynchNetPlayer>(entityInQueryIndex, playerEntity);
                    // Send response
                    var playerJoinedLobbyPacket = new PlayerJoinedLobbyPacket(newPlayerID, in netAddress);
                    var sendPacketEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, packetSendPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new EntityNetAddress(lobbyNetAddress));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new TargetNetAddress(entityNetAddress.netAddress));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, playerJoinedLobbyPacket.GetPacketData());
                    // send add new player command to all previous players

                    var netPlayerLinks2 = netPlayerLinks[lobbyLink.lobby];
                    for (int i = 0; i < netPlayerLinks2.players.Length; i++)
                    {
                        var existingPlayerEntity = netPlayerLinks2.players[i];
                        // note: should they both be to the local end point port?
                        if (!HasComponent<LobbyHost>(existingPlayerEntity)) //  && existingPlayerEntity != newPlayerEntity)
                        {
                            BlitableArray<byte> addPlayerPacketData;
                            SetPacketAddPlayer(in playerName, in playerID, in playerNetAddress, out addPlayerPacketData);
                            var existingPlayerNetAddress = netAddresses[existingPlayerEntity].netAddress;
                            // Send response packet
                            var sendPacketEntity2 = PostUpdateCommands.Instantiate(entityInQueryIndex, packetSendPrefab);
                            PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity2, new EntityNetAddress(lobbyNetAddress));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity2, new TargetNetAddress(existingPlayerNetAddress));
                            PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity2, new PacketData(addPlayerPacketData));
                            // UnityEngine.Debug.LogError("    Sending NewPlayerDataMessage to: " + i);
                        }
                    }
                }
            })  .WithReadOnly(lobbyNetAddresses)
                .WithReadOnly(netPlayerLinks)
                .WithReadOnly(netAddresses)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            // debug new player on server
            /*Entities
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved>()
                .ForEach((Entity e, int entityInQueryIndex, in PacketData packetData, in EntityNetAddress entityNetAddress, in LobbyLink lobbyLink) =>
            {
                // UnityEngine.Debug.LogError("Packet Recieved by player.");
                if (packetData.data.Length == 5 && packetData.data[0] == PacketType.ConnectedToLobby)
                {
                    var newPlayerID = uniqueID + entityInQueryIndex;
                    UnityEngine.Debug.LogError("New Player Connected [" + newPlayerID + "]");
                }
            }).WithoutBurst().Run();*/
        }

        public static void SetPacketAddPlayer(in ZoxName playerName, in ZoxID playerID, in EntityNetAddress entityNetAddress, out BlitableArray<byte> bytes)
        {
            var playerNetAddress = entityNetAddress.netAddress;
            bytes = new BlitableArray<byte>(13 + playerName.name.Length, Allocator.Persistent);
            bytes[0] = PacketType.LobbyAddPlayer;
            ByteUtil.SetInt(ref bytes, 1, playerID.id);
            NetworkByteUtil.SetNetAddress(ref bytes, 5, in playerNetAddress);
            // set the last as name
            var playerName2 = playerName.name;
            ByteUtil.SetText(ref bytes, 13, in playerName2);
        }
    }
}