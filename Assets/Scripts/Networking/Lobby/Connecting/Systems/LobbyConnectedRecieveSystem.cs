using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Networking
{
    //! Reads ping packets and adds events to NetPlayer entities.
    /**
    *   - Recieve Packet System -
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LobbyConnectedRecieveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery connectingToLobbysQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            connectingToLobbysQuery = GetEntityQuery(ComponentType.ReadOnly<ConnectingToLobby>());
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var lobbyAddPlayerPrefab = LobbyAddPlayerSystem.lobbyAddPlayerPrefab;
            var localAddress = NetSystemGroup.localAddress;
            var lobbyPrefab = NetSystemGroup.clientLobbyPrefab;
            var netPlayerPrefab = NetSystemGroup.localNetPlayerPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .WithAll<ConnectedToLobbySuccess>()
                .ForEach((Entity e, int entityInQueryIndex) =>
            {
                PostUpdateCommands.RemoveComponent<ConnectedToLobbySuccess>(entityInQueryIndex, e);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
            var entities = connectingToLobbysQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var playerLinks = GetComponentLookup<PlayerLink>(true);
            // entities.Dispose();
            Dependency = Entities
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved>()
                .ForEach((Entity e, int entityInQueryIndex, in PacketData packetData, in EntityNetAddress entityNetAddress, in NetListenerLink netListenerLink) =>
            {
                // UnityEngine.Debug.LogError("Packet Recieved by player.");
                if (packetData.data.Length == (1 + 4 + 8) && packetData.data[0] == PacketType.ConnectedToLobby)
                {
                    if (HasComponent<DestroyEntityInFrames>(netListenerLink.listener))
                    {
                        return;
                    }
                    var netAddress = entityNetAddress.netAddress;
                    // check listening entity has ConnectToLobby
                    PostUpdateCommands.AddComponent<ConnectedToLobbySuccess>(entityInQueryIndex, netListenerLink.listener);
                    PostUpdateCommands.AddComponent(entityInQueryIndex, netListenerLink.listener, new DestroyEntityInFrames(1));
                    var playerLink = playerLinks[netListenerLink.listener];
                    // spawn new player
                    var playerJoinedLobbyPacket = new PlayerJoinedLobbyPacket(in packetData);
                    var random = new Random();
                    random.InitState((uint) playerJoinedLobbyPacket.playerID);
                    var newPlayerName = NameGenerator.GenerateName2(ref random);
                    // spawn the lobby
                    var lobbyEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, lobbyEntity, new EntityNetAddress(netAddress));
                    // spawn player
                    var playerEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, netPlayerPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, new ZoxID(playerJoinedLobbyPacket.playerID));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, new ZoxName(newPlayerName));
                    // PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, new LobbyConnectedState(ConnectedState.Connected));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, new EntityNetAddress(playerJoinedLobbyPacket.netAddress));
                    var localAddress2 = localAddress;
                    localAddress2.port = playerJoinedLobbyPacket.netAddress.port;
                    PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, new LocalNetAddress(localAddress2));
                    PostUpdateCommands.AddComponent(entityInQueryIndex, playerEntity, playerLink);
                    // connect them
                    PostUpdateCommands.SetComponent(entityInQueryIndex, lobbyEntity, new LocalNetPlayerLink(playerEntity));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, new LobbyLink(lobbyEntity));
                    PostUpdateCommands.SetComponent(entityInQueryIndex,
                        PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyAddPlayerPrefab),
                        new LobbyAddPlayer(lobbyEntity, playerEntity));
                    // Initialize with connection listener
                    PostUpdateCommands.AddComponent(entityInQueryIndex, playerEntity, new TransferNetListener(netListenerLink.listener));
                    // PostUpdateCommands.AddComponent(entityInQueryIndex, netListenerLink.listener, new NewNetListener(playerEntity));
                }
                if (entities.Length > 0) { var e2 = entities[0]; }
            })  .WithReadOnly(playerLinks)
                .WithReadOnly(entities)
                .WithDisposeOnCompletion(entities)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
            // Debug
            /*Entities
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved, NetAddress, NetListenerLink>()
                .ForEach((int entityInQueryIndex, in PacketData packetData) =>
            {
                // UnityEngine.Debug.LogError("Packet Recieved by player.");
                if (packetData.data.Length == 5 && packetData.data[0] == PacketType.ConnectedToLobby)
                {
                    // spawn new player
                    var newPlayerID = ByteUtil.GetInt(in packetData.data, 1);
                    UnityEngine.Debug.LogError("Client Connected. New PlayerID: " + newPlayerID);
                }
            }).WithoutBurst().Run();*/