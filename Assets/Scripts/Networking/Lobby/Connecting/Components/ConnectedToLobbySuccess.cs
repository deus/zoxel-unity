using Unity.Entities;

namespace Zoxel.Networking
{
    //! A success event for a ConnectingToLobby event.
    public struct ConnectedToLobbySuccess : IComponentData { }
}