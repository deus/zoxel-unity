using Unity.Entities;

namespace Zoxel.Networking
{
    //! Keeps trying to connect to lobby until it works.
    public struct ConnectingToLobby : IComponentData
    {
        public double timeStartedConnecting;
        public float timeRate;

        public ConnectingToLobby(double timeStartedConnecting, float timeRate)
        {
            this.timeStartedConnecting = timeStartedConnecting;
            this.timeRate = timeRate;
        }
    }
}