using Unity.Entities;

namespace Zoxel.Networking
{
    //! Send a message to lobby.
    public struct ConnectToLobby : IComponentData { }
}