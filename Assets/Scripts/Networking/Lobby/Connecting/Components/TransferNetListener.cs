using Unity.Entities;

namespace Zoxel.Networking
{
    //! Transfers NetListener from ConnectingToLobby to a LocalNetPlayer.
    public struct TransferNetListener : IComponentData
    {
        public Entity listener;

        public TransferNetListener(Entity listener)
        {
            this.listener = listener;
        }
    }
}