using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Networking
{
    //! A copy of the packet (ConnectedToLobby) data.
    public struct PlayerJoinedLobbyPacket
    {
        public int playerID;
        public NetAddress netAddress;

        public PlayerJoinedLobbyPacket(in PacketData packetData)
        {
            this.playerID = ByteUtil.GetInt(in packetData.data, 1);
            this.netAddress = NetworkByteUtil.GetNetAddress(in packetData.data, 5);
        }

        public PlayerJoinedLobbyPacket(int playerID, in NetAddress netAddress)
        {
            this.playerID = playerID;
            this.netAddress = netAddress;
        }

        public PacketData GetPacketData()
        {
            var packetData = new BlitableArray<byte>(1 + 4 + 8, Allocator.Persistent);
            packetData[0] = PacketType.ConnectedToLobby;
            ByteUtil.SetInt(ref packetData, 1, playerID);
            NetworkByteUtil.SetNetAddress(ref packetData, 5, in netAddress);
            return new PacketData(packetData);
        }
    }
}