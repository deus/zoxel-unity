using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;

namespace Zoxel.Networking
{
    //! Remove a NetPlayer from the lobby.
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LobbyRemovePlayerSystem : SystemBase
    {
        private NativeArray<Text> labels;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity lobbyRemovePlayerPrefab;
        private EntityQuery processQuery;
        private EntityQuery lobbyAddPlayerEvents;
        private EntityQuery netPlayersQuery;

        protected override void OnCreate()
        {
            labels = new NativeArray<Text>(2, Allocator.Persistent);
            labels[0] = new Text("Player [");
            labels[1] = new Text("] has left the Lobby.");
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(LobbyRemovePlayer),
                typeof(GenericEvent));
            lobbyRemovePlayerPrefab = EntityManager.CreateEntity(archetype);
            lobbyAddPlayerEvents = GetEntityQuery(ComponentType.ReadOnly<LobbyRemovePlayer>());
            netPlayersQuery = GetEntityQuery(ComponentType.ReadOnly<NetPlayer>());
            RequireForUpdate(processQuery);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < labels.Length; i++)
            {
                labels[i].Dispose();
            }
            labels.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var labels = this.labels;
            var lobbyAddMessageEventPrefab = LobbyAddMessageSystem.lobbyAddMessageEventPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var lobbyRemovePlayerEventEntities = lobbyAddPlayerEvents.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var lobbyRemovePlayers = GetComponentLookup<LobbyRemovePlayer>(true);
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var netPlayerIDs = GetComponentLookup<ZoxID>(true);
            var netPlayerNames = GetComponentLookup<ZoxName>(true);
            netPlayerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<Lobby>()
                .ForEach((Entity e, int entityInQueryIndex, ref NetPlayerLinks netPlayerLinks) =>
            {
                var isListening = HasComponent<LobbyHost>(e);
                for (int i = 0; i < lobbyRemovePlayerEventEntities.Length; i++)
                {
                    var e2 = lobbyRemovePlayerEventEntities[i];
                    var lobbyRemovePlayer = lobbyRemovePlayers[e2];
                    if (lobbyRemovePlayer.lobby != e)
                    {
                        continue;
                    }
                    var playerEntity = lobbyRemovePlayer.player;
                    if (!netPlayerIDs.HasComponent(playerEntity))
                    {
                        continue;
                    }
                    var netPlayerID = netPlayerIDs[playerEntity].id;
                    netPlayerLinks.Remove(playerEntity, netPlayerID);
                    PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, playerEntity);
                    var message = labels[0].Clone();
                    var leftPlayerName = netPlayerNames[playerEntity].name;
                    message.AddText(in leftPlayerName);
                    var label2 = labels[1];
                    message.AddText(in label2);
                    var spawnMessageEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyAddMessageEventPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMessageEntity, new SpawnLobbyMessageClient(message));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMessageEntity, new LobbyLink(e));
                    // UnityEngine.Debug.LogError("Event - LobbyRemovePlayer - Removing Player: " + netPlayerID);
                }
            })  .WithReadOnly(lobbyRemovePlayerEventEntities)
                .WithDisposeOnCompletion(lobbyRemovePlayerEventEntities)
                .WithReadOnly(lobbyRemovePlayers)
                .WithReadOnly(netPlayerIDs)
                .WithReadOnly(netPlayerNames)
                .WithReadOnly(labels)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}