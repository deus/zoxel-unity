using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! Reads ping packets and adds events to NetPlayer entities.
    /**
    *   - Recieve Packet System -
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LobbyAddPlayerRecieveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery netPlayersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            netPlayersQuery = GetEntityQuery(ComponentType.ReadOnly<NetPlayer>());
            RequireForUpdate(processQuery);
            RequireForUpdate(netPlayersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var lobbyAddPlayerPrefab = LobbyAddPlayerSystem.lobbyAddPlayerPrefab;
            var netPlayerPrefab = NetSystemGroup.netPlayerPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var lobbyLinks = GetComponentLookup<LobbyLink>(true);
            netPlayerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved>()
                .ForEach((Entity e, int entityInQueryIndex, in PacketData packetData, in NetPlayerLink netPlayerLink) =>
            {
                if (packetData.data.Length > 0 && packetData.data[0] == PacketType.LobbyAddPlayer)
                {
                    var lobbyEntity = lobbyLinks[netPlayerLink.player].lobby;
                    var playerID = ByteUtil.GetInt(in packetData.data, 1);
                    var netAddress = NetworkByteUtil.GetNetAddress(in packetData.data, 5);
                    Text playerName;
                    ByteUtil.GetText(in packetData.data, 13, out playerName);
                    var newPlayerEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, netPlayerPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, newPlayerEntity, new ZoxID(playerID));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, newPlayerEntity, new EntityNetAddress(netAddress));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, newPlayerEntity, new ZoxName(playerName));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, newPlayerEntity, new LobbyLink(lobbyEntity));
                    PostUpdateCommands.SetComponent(entityInQueryIndex,
                        PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyAddPlayerPrefab),
                        new LobbyAddPlayer(lobbyEntity, newPlayerEntity));
                    // UnityEngine.Debug.LogError("Recieved Packet for [LobbyAddPlayer] New Player [" + playerID + "]");
                    //! \todo For packets recieved, link to sending player using Lobby's list of IPs/Ports
                }
            })  .WithReadOnly(lobbyLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}