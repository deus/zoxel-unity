using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! Reads ping packets and adds events to NetPlayer entities.
    /**
    *   - Recieve Packet System -
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LobbyRemovePlayerRecieveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery lobbyQuery;
        private EntityQuery netPlayersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            lobbyQuery = GetEntityQuery(ComponentType.ReadOnly<Lobby>());
            netPlayersQuery = GetEntityQuery(ComponentType.ReadOnly<NetPlayer>());
            RequireForUpdate(processQuery);
            RequireForUpdate(lobbyQuery);
            RequireForUpdate(netPlayersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var lobbyRemovePlayerPrefab = LobbyRemovePlayerSystem.lobbyRemovePlayerPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            // Player left
            var lobbyEntities = lobbyQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var netPlayerLinks = GetComponentLookup<NetPlayerLinks>(true);
            lobbyEntities.Dispose();
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            // var playerIDs = GetComponentLookup<ZoxID>(true);
            var lobbyLinks = GetComponentLookup<LobbyLink>(true);
            netPlayerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved>()
                .ForEach((Entity e, int entityInQueryIndex, in PacketData packetData, in EntityNetAddress entityNetAddress, in NetPlayerLink netPlayerLink) =>
            {
                if (packetData.data.Length > 0 && packetData.data[0] == PacketType.LobbyRemovePlayer)
                {
                    var lobbyEntity = lobbyLinks[netPlayerLink.player].lobby;
                    var removePlayerID = ByteUtil.GetInt(in packetData.data, 1);
                    // Kick Player as they just left
                    // check for player, remove
                    var netPlayerLinks2 = netPlayerLinks[lobbyEntity];
                    var playerEntity = netPlayerLinks2.Get(removePlayerID);
                    if (playerEntity.Index > 0)
                    {
                        // UnityEngine.Debug.LogError("PacketRecieved - LobbyRemovePlayer - Removing Player: " + removePlayerID);
                        PostUpdateCommands.SetComponent(entityInQueryIndex,
                            PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyRemovePlayerPrefab),
                            new LobbyRemovePlayer(lobbyEntity, playerEntity));
                    }
                }
            })  .WithReadOnly(netPlayerLinks)
                .WithReadOnly(lobbyLinks)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
                    /*for (int i = 0; i < netPlayerLinks2.players.Length; i++)
                    {
                        var playerEntity = netPlayerLinks2.players[i];
                        if (!HasComponent<LobbyHost>(playerEntity))
                        {
                            if (removePlayerID == playerIDs[playerEntity].id)
                            {
                                PostUpdateCommands.SetComponent(entityInQueryIndex,
                                    PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyRemovePlayerPrefab),
                                    new LobbyRemovePlayer(lobbyEntity, playerEntity));
                                break;
                            }
                        }
                    }*/