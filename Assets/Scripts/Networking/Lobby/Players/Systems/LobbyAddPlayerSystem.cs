using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;

namespace Zoxel.Networking
{
    //! Adds a NetPlayer to the lobby.
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LobbyAddPlayerSystem : SystemBase
    {
        private NativeArray<Text> labels;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity lobbyAddPlayerPrefab;
        private EntityQuery processQuery;
        private EntityQuery lobbyAddPlayerEvents;
        private EntityQuery netPlayersQuery;

        protected override void OnCreate()
        {
            labels = new NativeArray<Text>(2, Allocator.Persistent);
            labels[0] = new Text("Player [");
            labels[1] = new Text("] has entered the Lobby.");
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(LobbyAddPlayer),
                typeof(GenericEvent));
            lobbyAddPlayerPrefab = EntityManager.CreateEntity(archetype);
            lobbyAddPlayerEvents = GetEntityQuery(ComponentType.ReadOnly<LobbyAddPlayer>());
            netPlayersQuery = GetEntityQuery(ComponentType.ReadOnly<NetPlayer>());
            RequireForUpdate(processQuery);
            RequireForUpdate(lobbyAddPlayerEvents);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < labels.Length; i++)
            {
                labels[i].Dispose();
            }
            labels.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var pingRate = NetPingSystem.pingRate;
            var labels = this.labels;
            var elapsedTime = World.Time.ElapsedTime;
            var lobbyAddMessageEventPrefab = LobbyAddMessageSystem.lobbyAddMessageEventPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var lobbyAddPlayerEventEntities = lobbyAddPlayerEvents.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var lobbyAddPlayers = GetComponentLookup<LobbyAddPlayer>(true);
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var netPlayerIDs = GetComponentLookup<ZoxID>(true);
            var netPlayerNames = GetComponentLookup<ZoxName>(true);
            netPlayerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<Lobby>()
                .ForEach((Entity e, int entityInQueryIndex, ref NetPlayerLinks netPlayerLinks) =>
            {
                var isListening = HasComponent<LobbyHost>(e);
                for (int i = 0; i < lobbyAddPlayerEventEntities.Length; i++)
                {
                    var e2 = lobbyAddPlayerEventEntities[i];
                    var lobbyAddPlayer = lobbyAddPlayers[e2];
                    if (lobbyAddPlayer.lobby != e)
                    {
                        continue;
                    }
                    var playerEntity = lobbyAddPlayer.player;
                    if (!netPlayerIDs.HasComponent(playerEntity))
                    {
                        continue;
                    }
                    var netPlayerID = netPlayerIDs[playerEntity].id;
                    netPlayerLinks.Add(playerEntity, netPlayerID);
                    var isLocalPlayer = HasComponent<LocalNetPlayer>(playerEntity);
                    if (isListening && !isLocalPlayer)
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, playerEntity, new NetPinger(elapsedTime + pingRate));
                    }
                    else if (!isListening && isLocalPlayer)
                    {
                        PostUpdateCommands.AddComponent(entityInQueryIndex, playerEntity, new NetPingee(elapsedTime + pingRate));
                    }
                    var message = labels[0].Clone();
                    var leftPlayerName = netPlayerNames[playerEntity].name;
                    message.AddText(in leftPlayerName);
                    var label2 = labels[1];
                    message.AddText(in label2);
                    var spawnMessageEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyAddMessageEventPrefab);
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMessageEntity, new SpawnLobbyMessageClient(message));
                    PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMessageEntity, new LobbyLink(e));
                }
            })  .WithReadOnly(lobbyAddPlayerEventEntities)
                .WithDisposeOnCompletion(lobbyAddPlayerEventEntities)
                .WithReadOnly(lobbyAddPlayers)
                .WithReadOnly(netPlayerIDs)
                .WithReadOnly(netPlayerNames)
                .WithReadOnly(labels)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}