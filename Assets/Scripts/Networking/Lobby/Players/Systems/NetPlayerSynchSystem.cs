using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! The lobby sends a confirm packet to the new player that joined. Uses OnNetPlayerSpawned event.
    /**
    *   - Send Packet System -
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class NetPlayerSynchSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery lobbysQuery;
        private EntityQuery netPlayersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            lobbysQuery = GetEntityQuery(ComponentType.ReadOnly<Lobby>());
            netPlayersQuery = GetEntityQuery(ComponentType.ReadOnly<NetPlayer>());
            RequireForUpdate(processQuery);
            RequireForUpdate(lobbysQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            var lobbyEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var lobbyNetAddresses = GetComponentLookup<EntityNetAddress>(true);
            var netPlayerLinkss = GetComponentLookup<NetPlayerLinks>(true);
            lobbyEntities.Dispose();
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var playerIDs = GetComponentLookup<ZoxID>(true);
            var playerNames = GetComponentLookup<ZoxName>(true);
            var netAddresses = GetComponentLookup<EntityNetAddress>(true);
            netPlayerEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<SynchNetPlayer>()
                .ForEach((Entity e, ref SynchNetPlayer synchNetPlayer, in LobbyLink lobbyLink, in EntityNetAddress entityNetAddress) =>
            {
                synchNetPlayer.count++;
                if (synchNetPlayer.count < 63)
                {
                    return;
                }
                PostUpdateCommands.RemoveComponent<SynchNetPlayer>(e);
                var netAddress = entityNetAddress.netAddress;
                var lobbyNetAddress = lobbyNetAddresses[lobbyLink.lobby].netAddress;
                var netPlayerLinks = netPlayerLinkss[lobbyLink.lobby];
                //! Sending all old players to new player.
                for (int i = 0; i < netPlayerLinks.players.Length; i++)
                {
                    var playerEntity = netPlayerLinks.players[i];
                    if (playerEntity != e)
                    {
                        var playerNetAddress = netAddresses[playerEntity].netAddress;
                        var playerName = playerNames[playerEntity];
                        var playerID = playerIDs[playerEntity];
                        BlitableArray<byte> addPlayerBytes;
                        SetPacketAddPlayer(in playerName, in playerID, in playerNetAddress, out addPlayerBytes);
                        var addPlayerBytesArray = addPlayerBytes.ToArray();
                        //UnityEngine.Debug.LogError("> Sending New Player [" + e.Index + "], Old Player of [" + playerID.id + "] at: " + i
                        //    + " of bytes: " + addPlayerBytesArray.Length);
                        NetSendUtil.SendPacketToPlayer(in addPlayerBytesArray, in lobbyNetAddress, in netAddress);
                        addPlayerBytes.Dispose();
                    }
                }
            })  .WithReadOnly(playerIDs)
                .WithReadOnly(playerNames)
                .WithReadOnly(netAddresses)
                .WithReadOnly(lobbyNetAddresses)
                .WithReadOnly(netPlayerLinkss)
                .WithoutBurst().Run();
        }
        
        public static void SetPacketAddPlayer(in ZoxName playerName, in ZoxID playerID, in NetAddress playerNetAddress, out BlitableArray<byte> bytes)
        {
            bytes = new BlitableArray<byte>(13 + playerName.name.Length, Allocator.Persistent);
            bytes[0] = PacketType.LobbyAddPlayer;
            ByteUtil.SetInt(ref bytes, 1, playerID.id);
            NetworkByteUtil.SetNetAddress(ref bytes, 5, playerNetAddress);
            ByteUtil.SetText(ref bytes, 13, in playerName.name);
        }
    }
}