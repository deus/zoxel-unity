using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! [Server] - Remove Unresponsive Player.
    /**
    *   - Send Packet System -
    */
    [UpdateAfter(typeof(LobbyRemovePlayerSystem))]
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LobbyRemovePlayerSendSystem : SystemBase
    {
        private EntityQuery processQuery;
        private EntityQuery lobbyQuery;
        private EntityQuery netPlayersQuery;

        protected override void OnCreate()
        {
            lobbyQuery = GetEntityQuery(
                ComponentType.ReadOnly<Lobby>(),
                ComponentType.ReadOnly<NetPlayerLinks>(),
                ComponentType.ReadOnly<LocalNetPlayerLink>());
            netPlayersQuery = GetEntityQuery(ComponentType.ReadOnly<NetPlayer>());
            RequireForUpdate(processQuery);
            RequireForUpdate(lobbyQuery);
            RequireForUpdate(netPlayersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var lobbyEntities = lobbyQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var lobbys = GetComponentLookup<EntityNetAddress>(true);
            var netPlayerLinks = GetComponentLookup<NetPlayerLinks>(true);
            var localNetPlayerLinks = GetComponentLookup<LocalNetPlayerLink>(true);
            lobbyEntities.Dispose();
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var netAddresses = GetComponentLookup<EntityNetAddress>(true);
            var playerIDs = GetComponentLookup<ZoxID>(true);
            netPlayerEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .ForEach((in LobbyRemovePlayer lobbyRemovePlayer) =>
            {
                var lobbyEntity = lobbyRemovePlayer.lobby;
                if (!lobbys.HasComponent(lobbyEntity) || !HasComponent<LobbyHost>(lobbyEntity))
                {
                    return;
                }
                // send command to remove them
                var lobby = lobbys[lobbyEntity].netAddress;
                var netPlayerLinks2 = netPlayerLinks[lobbyEntity];
                var hostPlayer = localNetPlayerLinks[lobbyEntity].player;
                if (!playerIDs.HasComponent(lobbyRemovePlayer.player))
                {
                    return;
                }
                var removedPlayerID = playerIDs[lobbyRemovePlayer.player].id;
                var bytes = new BlitableArray<byte>(5, Allocator.Temp);
                bytes[0] = PacketType.LobbyRemovePlayer;
                ByteUtil.SetInt(ref bytes, 1, removedPlayerID);
                var bytesArray = bytes.ToArray();
                NetSendUtil.LobbyBroadcastPacket(in bytesArray, in lobby, in netPlayerLinks2, hostPlayer, in netAddresses);
                bytes.Dispose();
            })  .WithReadOnly(lobbys)
                .WithReadOnly(netPlayerLinks)
                .WithReadOnly(localNetPlayerLinks)
                .WithReadOnly(netAddresses)
                .WithReadOnly(playerIDs)
                .WithoutBurst().Run();
        }
    }
}

// Spawn a local one when system starts
// Spawn a shadow one - when you join a room or lobby - that will spawn all the other ones
/* var kickPlayerPacket2 = kickPlayerPacket.ToString() +;*/