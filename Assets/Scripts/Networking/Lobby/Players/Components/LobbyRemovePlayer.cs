using Unity.Entities;

namespace Zoxel.Networking
{
    public struct LobbyRemovePlayer : IComponentData
    {
        public Entity lobby;
        public Entity player;

        public LobbyRemovePlayer(Entity lobby, Entity player)
        {
            this.lobby = lobby;
            this.player = player;
        }
    }
}