using Unity.Entities;

namespace Zoxel.Networking
{
    public struct LobbyAddPlayer : IComponentData
    {
        public Entity lobby;
        public Entity player;

        public LobbyAddPlayer(Entity lobby, Entity player)
        {
            this.lobby = lobby;
            this.player = player;
        }
    }
}