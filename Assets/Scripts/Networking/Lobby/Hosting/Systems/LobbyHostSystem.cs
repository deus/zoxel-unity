using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Networking
{
    //! Spawns a Lobby by the host.
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LobbyHostSystem : SystemBase
    {
        private NativeArray<Text> menus;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        public static Entity hostLobbyPrefab;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            menus = new NativeArray<Text>(1, Allocator.Persistent);
            menus[0] = new Text("You are now Hosting Zoxel Lobby.");
            var archetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(HostLobby),
                typeof(GenericEvent));
            hostLobbyPrefab = EntityManager.CreateEntity(archetype);
        }

        protected override void OnDestroy()
        {
            for (int i = 0; i < menus.Length; i++)
            {
                menus[i].Dispose();
            }
            menus.Dispose();
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var isHeadless = NetworkUtil.IsHeadless();
            var localAddress = NetSystemGroup.localAddress;
            var newPlayerID = IDUtil.GenerateUniqueID();
            var welcomeMessage = this.menus;
            var lobbyAddMessageEventPrefab = LobbyAddMessageSystem.lobbyAddMessageEventPrefab;
            var lobbyAddPlayerPrefab = LobbyAddPlayerSystem.lobbyAddPlayerPrefab;
            var netPlayerPrefab = NetSystemGroup.localNetPlayerPrefab;
            var lobbyPrefab = NetSystemGroup.hostLobbyPrefab;
            var elapsedTime = World.Time.ElapsedTime;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            Dependency = Entities
                .ForEach((int entityInQueryIndex, in HostLobby hostLobby) =>
            {
                // spawn the lobby
                var lobbyEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyPrefab);
                var lobbyNetAddress = hostLobby.address;
                PostUpdateCommands.SetComponent(entityInQueryIndex, lobbyEntity, new EntityNetAddress(lobbyNetAddress));
                PostUpdateCommands.SetComponent(entityInQueryIndex, lobbyEntity, new LocalNetAddress(lobbyNetAddress));
                // Spawn welcome message
                var message = welcomeMessage[0].Clone();
                var spawnMessageEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyAddMessageEventPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMessageEntity, new SpawnLobbyMessageClient(message));
                PostUpdateCommands.SetComponent(entityInQueryIndex, spawnMessageEntity, new LobbyLink(lobbyEntity));
                //! No NetPlayer in headless mode
                if (isHeadless)
                {
                    return;
                }
                newPlayerID += entityInQueryIndex;
                var random = new Random();
                random.InitState((uint) newPlayerID);
                var newPlayerName = NameGenerator.GenerateName2(ref random);
                var netAddress = new NetAddress(localAddress.ip, random.NextInt(11100, 14000));
                // spawn player
                var playerEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, netPlayerPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, new ZoxID(newPlayerID));
                PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, new ZoxName(newPlayerName));
                PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, new EntityNetAddress(netAddress));
                PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, new LocalNetAddress(netAddress));
                // PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, new LobbyConnectedState(ConnectedState.Connected));
                // connect them
                PostUpdateCommands.AddComponent<LobbyHost>(entityInQueryIndex, playerEntity);
                PostUpdateCommands.SetComponent(entityInQueryIndex, lobbyEntity, new LocalNetPlayerLink(playerEntity));
                PostUpdateCommands.SetComponent(entityInQueryIndex, playerEntity, new LobbyLink(lobbyEntity));
                PostUpdateCommands.SetComponent(entityInQueryIndex,
                    PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyAddPlayerPrefab),
                    new LobbyAddPlayer(lobbyEntity, playerEntity));
            })  .WithReadOnly(welcomeMessage)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}