using Unity.Entities;
// todo: Create a struct for IP, that just uses some bytes instead - converts the numbers to bytes

namespace Zoxel.Networking
{
    public struct HostLobby : IComponentData
    {
        public NetAddress address;

        public HostLobby(NetAddress address)
        {
            this.address = address;
        }
    }
}