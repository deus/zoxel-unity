using Unity.Entities;
using Unity.Burst;

namespace Zoxel.Networking
{
    //! When disconnecting from Lobby, destroys it.
    /**
    *   \todo Refactor DisconnectedFromLobby and just destroy lobby?
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LobbyDisconnectSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            RequireForUpdate(processQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<CloseLobby>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<CloseLobby>()
                .ForEach((Entity e, int entityInQueryIndex, in LobbyLink lobbyLink) =>
            {
                PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, lobbyLink.lobby);
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}
            // ref LobbyConnectedState lobbyConnectedState, 
            // PostUpdateCommands2.RemoveComponentForEntityQuery<DisconnectedFromLobby>(disconnectedFromLobbyQuery);
                // lobbyConnectedState.connectedState = ConnectedState.None;
                // PostUpdateCommands.AddComponent<DisconnectedFromLobby>(entityInQueryIndex, e);
                // PostUpdateCommands.SetComponent(entityInQueryIndex, e, new LobbyConnectedState(ConnectedState.None));
                /*if (HasComponent<LobbyHost>(e))
                {
                    if (HasComponent<NetPinger>(e))
                    {
                        PostUpdateCommands.RemoveComponent<NetPinger>(entityInQueryIndex, e);
                    }
                }
                else
                {
                    if (HasComponent<NetPingee>(e))
                    {
                        PostUpdateCommands.RemoveComponent<NetPingee>(entityInQueryIndex, e);
                    }
                }*/