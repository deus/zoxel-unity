using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! When client disconnects from lobby, send host a message.
    /**
    *   - Send Packet System -
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class ClientDisconnectFromLobbySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery lobbyQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            lobbyQuery = GetEntityQuery(ComponentType.ReadOnly<Lobby>());
            RequireForUpdate(processQuery);
            RequireForUpdate(lobbyQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var packetSendPrefab = PacketSendSystem.packetSendPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var lobbyEntities = lobbyQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var lobbyNetAddresses = GetComponentLookup<EntityNetAddress>(true);
            lobbyEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<LobbyHost>()
                .WithAll<LeaveLobby, NetPlayer>()
                .ForEach((Entity e, int entityInQueryIndex, in EntityNetAddress entityNetAddress, in LobbyLink lobbyLink) =>
            {
                PostUpdateCommands.RemoveComponent<LeaveLobby>(entityInQueryIndex, e);
                PostUpdateCommands.AddComponent<DestroyEntity>(entityInQueryIndex, lobbyLink.lobby);
                PostUpdateCommands.AddComponent<LeaveLobby>(entityInQueryIndex, lobbyLink.lobby);
                var lobbyNetAddress = lobbyNetAddresses[lobbyLink.lobby].netAddress;
                // Send leave packet to lobby
                var packetData = new BlitableArray<byte>(1, Allocator.Persistent);
                packetData[0] = PacketType.LeaveLobby;
                var sendPacketEntity = PostUpdateCommands.Instantiate(entityInQueryIndex, packetSendPrefab);
                PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, entityNetAddress);
                PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new TargetNetAddress(lobbyNetAddress));
                PostUpdateCommands.SetComponent(entityInQueryIndex, sendPacketEntity, new PacketData(packetData));
                // NetSendUtil.SendPacketToLobby(in closeLobbyBytes, in lobby, in netAddress);
            })  .WithReadOnly(lobbyNetAddresses)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}