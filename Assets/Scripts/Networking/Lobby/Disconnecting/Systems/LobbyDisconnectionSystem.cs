using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! When host closes the lobbyy.
    /**
    *   - Send Packet System -
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LobbyDisconnectionSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery lobbyQuery;
        private EntityQuery netPlayersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            lobbyQuery = GetEntityQuery(ComponentType.ReadOnly<Lobby>());
            netPlayersQuery = GetEntityQuery(ComponentType.ReadOnly<NetPlayer>());
            RequireForUpdate(processQuery);
            RequireForUpdate(lobbyQuery);
            RequireForUpdate(netPlayersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var closeLobbyBytes = new byte[] { PacketType.CloseLobby };
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            PostUpdateCommands2.RemoveComponent<LobbyHost>(processQuery);
            var PostUpdateCommands = PostUpdateCommands2.AsParallelWriter();
            var lobbyEntities = lobbyQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var lobbys = GetComponentLookup<EntityNetAddress>(true);
            var netPlayerLinks = GetComponentLookup<NetPlayerLinks>(true);
            lobbyEntities.Dispose();
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var netAddresss = GetComponentLookup<EntityNetAddress>(true);
            netPlayerEntities.Dispose();
            Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithAll<CloseLobby, LobbyHost>()
                .ForEach((Entity e, in EntityNetAddress entityNetAddress, in LobbyLink lobbyLink) =>
            {
                var lobbyEntity = lobbyLink.lobby;
                var lobbyNetAddress = lobbys[lobbyEntity].netAddress;
                var netPlayerLinks2 = netPlayerLinks[lobbyEntity];
                for (int i = 0; i < netPlayerLinks2.players.Length; i++)
                {
                    var playerEntity = netPlayerLinks2.players[i];
                    if (playerEntity != e)
                    {
                        // UnityEngine.Debug.LogError("Lobby Host Kicking player at " + i);
                        var playerNetAddress = netAddresss[playerEntity].netAddress;
                        NetSendUtil.SendPacketToPlayer(in closeLobbyBytes, in lobbyNetAddress, in playerNetAddress);
                    }
                }
            })  .WithReadOnly(lobbys)
                .WithReadOnly(netPlayerLinks)
                .WithReadOnly(netAddresss)
                .WithoutBurst().Run();
        }
    }
}