using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;

namespace Zoxel.Networking
{
    //! Reads ping packets and adds events to NetPlayer entities.
    /**
    *   - Recieve Packet System -
    */
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class HostCloseLobbyRecieveSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery processQuery;
        private EntityQuery lobbyQuery;
        private EntityQuery netPlayersQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            lobbyQuery = GetEntityQuery(ComponentType.ReadOnly<Lobby>());
            netPlayersQuery = GetEntityQuery(ComponentType.ReadOnly<NetPlayer>());
            RequireForUpdate(processQuery);
            RequireForUpdate(lobbyQuery);
            RequireForUpdate(netPlayersQuery);
        }

        [BurstCompile]
        protected override void OnUpdate()
        {
            var lobbyRemovePlayerPrefab = LobbyRemovePlayerSystem.lobbyRemovePlayerPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var lobbyEntities = lobbyQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandle);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandle);
            var netPlayerLinks = GetComponentLookup<NetPlayerLinks>(true);
            lobbyEntities.Dispose();
            var netPlayerEntities = netPlayersQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var netAddresss = GetComponentLookup<EntityNetAddress>(true);
            netPlayerEntities.Dispose();
            Dependency = Entities
                .WithStoreEntityQueryInField(ref processQuery)
                .WithNone<DestroyEntity>()
                .WithAll<PacketRecieved>()
                .ForEach((Entity e, int entityInQueryIndex, in PacketData packetData, in EntityNetAddress entityNetAddress, in LobbyLink lobbyLink) =>
            {
                var netAddress = entityNetAddress.netAddress;
                if (packetData.data.Length > 0 && packetData.data[0] == PacketType.CloseLobby)
                {
                    // Kick Player as they just left
                    // check for player, remove
                    var netPlayerLinks2 = netPlayerLinks[lobbyLink.lobby];
                    for (int i = 0; i < netPlayerLinks2.players.Length; i++)
                    {
                        var playerEntity = netPlayerLinks2.players[i];
                        if (!HasComponent<LobbyHost>(playerEntity))
                        {
                            var netAddress2 = netAddresss[playerEntity].netAddress;
                            if (netAddress.ip == netAddress2.ip && netAddress.port == netAddress2.port)
                            {
                                // UnityEngine.Debug.LogError("CloseLobby - Removing Player: " + playerEntity.Index);
                                PostUpdateCommands.SetComponent(entityInQueryIndex,
                                    PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyRemovePlayerPrefab),
                                    new LobbyRemovePlayer(lobbyLink.lobby, playerEntity));
                                break;
                            }
                        }
                    }
                }
                else if (packetData.data.Length > 0 && packetData.data[0] == PacketType.LeaveLobby)
                {
                    // Kick Player as they just left
                    // check for player, remove
                    var netPlayerLinks2 = netPlayerLinks[lobbyLink.lobby];
                    for (int i = 0; i < netPlayerLinks2.players.Length; i++)
                    {
                        var playerEntity = netPlayerLinks2.players[i];
                        if (!HasComponent<LobbyHost>(playerEntity))
                        {
                            var netAddress2 = netAddresss[playerEntity].netAddress;
                            if (netAddress.ip == netAddress2.ip && netAddress.port == netAddress2.port)
                            {
                                // UnityEngine.Debug.LogError("LeaveLobby - Removing Player: " + playerEntity.Index);
                                PostUpdateCommands.SetComponent(entityInQueryIndex,
                                    PostUpdateCommands.Instantiate(entityInQueryIndex, lobbyRemovePlayerPrefab),
                                    new LobbyRemovePlayer(lobbyLink.lobby, playerEntity));
                                break;
                            }
                        }
                    }
                }
            })  .WithReadOnly(netPlayerLinks)
                .WithReadOnly(netAddresss)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}