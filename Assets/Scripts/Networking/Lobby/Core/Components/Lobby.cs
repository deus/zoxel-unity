using Unity.Entities;
    
namespace Zoxel.Networking
{
    //! A big room where people can chat and play.
    public struct Lobby : IComponentData { }
}