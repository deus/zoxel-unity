using Unity.Entities;

namespace Zoxel.Networking
{
    public struct LobbyLink : IComponentData
    {
        public Entity lobby;

        public LobbyLink(Entity lobby)
        {
            this.lobby = lobby;
        }
    }
}