using Unity.Entities;
using Unity.Burst;
using Unity.Collections;
using Unity.Mathematics;

namespace Zoxel.Networking
{
    //! Sets local net player.
    [BurstCompile, UpdateInGroup(typeof(NetSystemGroup))]
    public partial class LocalNetPlayerSetSystem : SystemBase
    {
        protected override void OnUpdate()
        {
            Entities
                .WithAll<InitializeEntity, LocalNetPlayer>()
                .ForEach((Entity e) =>
            {
                NetSystemGroup.localNetPlayer = e;
            }).WithoutBurst().Run();
        }
    }
}