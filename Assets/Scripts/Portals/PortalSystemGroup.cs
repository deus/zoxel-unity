using Unity.Entities;
using Zoxel.Rendering;
// using UnityEngine.PlayerLoop;

namespace Zoxel.Portals
{
    //! Handles all portally things.
    [UpdateAfter(typeof(RenderSystemGroup))]
    public partial class PortalSystemGroup : ComponentSystemGroup { }
}

    // [UpdateBefore(typeof(TransformSystemGroup))]
    // [UpdateAfter(typeof(PresentationSystemGroup))]
    /*[UpdateAfter(typeof(PreLateUpdate))]
    public partial class PortalRenderSystemGroup : ComponentSystemGroup { }*/