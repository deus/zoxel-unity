using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Transforms;
using Zoxel.Cameras;

namespace Zoxel.Portals
{
    //! Auto links portal to closest one.
    [BurstCompile, UpdateInGroup(typeof(PortalSystemGroup))]
    public partial class PortalLinkSystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery portalQuery;
        public static float linkPortalRange = 256;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            portalQuery = GetEntityQuery(
                ComponentType.ReadOnly<Portal>(),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<ZoxID>(),
                ComponentType.Exclude<InitializePortal>());
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            if (portalQuery.CalculateEntityCount() <= 1)
            {
                return;
            }
            float smallestDistance2 = linkPortalRange;
			// When lighting chunks are completed
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
            var portalEntities = portalQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var portalPositions = GetComponentLookup<Translation>(true);
			Dependency = Entities
                .WithNone<InitializePortal>()
                .WithAll<AutoLinkPortal>()
                .ForEach((Entity e, int entityInQueryIndex, ref Portal portal, in Translation translation) =>
			{
                if (HasComponent<Translation>(portal.linkedPortal))
                {
                    return;
                }
                portal.linkedPortal = new Entity();
                // find new portal
                float smallestDistance = smallestDistance2;
                int targetIndex = -1;
                for (int i = 0; i < portalEntities.Length; i++)
                {
                    if (portalEntities[i] != e)
                    {
                        var e2 = portalEntities[i];
                        var portalPosition = portalPositions[e2].Value;
                        var distanceTo = math.distance(translation.Value, portalPosition);
                        if (distanceTo < smallestDistance)
                        {
                            targetIndex = i;
                        }
                    }
                }
                if (targetIndex != -1)
                {
                    var e2 = portalEntities[targetIndex];
                    portal.linkedPortal = e2;
                    // linkPortal.linkPortalID = newID;
                    // linkPortal.textureDirty = 1;
                    PostUpdateCommands.AddComponent<PortalDirty>(entityInQueryIndex, e);
                }
			})  .WithReadOnly(portalEntities)
                .WithDisposeOnCompletion(portalEntities)
                .WithReadOnly(portalPositions)
                .WithNativeDisableContainerSafetyRestriction(portalPositions)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }
    }
}