﻿using Unity.Entities;
using Unity.Burst;

using Unity.Mathematics;
using Unity.Collections;
using System.Collections.Generic;   // todo: replace dictionary with shared components
using Zoxel.Cameras;
using Zoxel.Voxels;                 // todo: replace voxel components with generic ones
using Zoxel.Rendering;
using Zoxel.Transforms;

namespace Zoxel.Portals
{
    //! Initializes portals after they spawn, spawns portal cameras.
    [BurstCompile, UpdateInGroup(typeof(PortalSystemGroup))]
    public partial class PortalSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		public static Entity portalPrefab;
		public static Entity portalCameraPrefab;
        private const int portalTextureResolution = 1024;
        public static float portalOpenDistance = 16;
        public static Dictionary<int, UnityEngine.Camera> portalCameras = new Dictionary<int, UnityEngine.Camera>();
        public static Dictionary<int, UnityEngine.Material> portalMaterials = new Dictionary<int, UnityEngine.Material>();

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            var portalArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(InitializePortal),
				typeof(NewMinivox),
                typeof(ZoxID),
                typeof(BasicRender),
                typeof(Portal),
                typeof(AutoLinkPortal),
                typeof(Body2D),
                typeof(CameraLink), // links to portal camera
                typeof(Minivox),
                typeof(VoxelPosition),
                typeof(ChunkLink),
                typeof(DontDestroyTexture),
				typeof(Translation),
				typeof(Rotation),
				typeof(NonUniformScale),
				typeof(LocalToWorld),
                typeof(ZoxMesh));
            portalPrefab = EntityManager.CreateEntity(portalArchetype);
            EntityManager.SetComponentData(portalPrefab, new NonUniformScale { Value = new float3(1, 1, 1) });
            var portalCameraArchetype = EntityManager.CreateArchetype(
                typeof(Prefab),
                typeof(InitializeEntity),
                typeof(PortalCamera),
                typeof(PortalLink),
                typeof(GameObjectLink),
                typeof(RenderTextureLink)
				//typeof(Translation),
				//typeof(Rotation)
                );
            portalCameraPrefab = EntityManager.CreateEntity(portalCameraArchetype);
            
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (MaterialsManager.instance == null) return;
            var portalMaterial = MaterialsManager.instance.materials.portalMaterial;
            var portalCameraPrefab2 = CameraManager.instance.CameraSettings.portalCameraPrefab;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); //.AsParallelWriter();
            Entities
                .WithAll<InitializeEntity, Portal>()
                .ForEach((Entity e, ref Translation position, ref Rotation rotation, ref NonUniformScale scale, in InitializePortal initializePortal) =>
            {
                position.Value = initializePortal.position;
                rotation.Value = initializePortal.rotation;
            }).ScheduleParallel();
            // anything with camera, rendertexture, material and mesh needs to be done on main thread
            Entities
                .WithAll<InitializeEntity, Portal>()
                .ForEach((Entity e, in Body2D body2D) =>
            {
                PostUpdateCommands.RemoveComponent<InitializePortal>(e);
                var portalSize = body2D.size;
                var portalMesh = MeshUtilities.CreateQuadMesh(portalSize);
                var textureSize = new float2((portalTextureResolution * portalSize.x), (portalTextureResolution * portalSize.y));
                int id = IDUtil.GenerateUniqueID();
                var material = new UnityEngine.Material(portalMaterial); // new UnityEngine.Material(UnityEngine.Shader.Find("Custom/Portal"));
                material.name = "PortalMaterial[" + id + "]";
                material.enableInstancing = true;
                material.SetInt("displayMask", 1);
                var renderMesh = new ZoxMesh
                { 
                    mesh = portalMesh,
                    material = material
                };
                var portalCamera = UnityEngine.GameObject.Instantiate(portalCameraPrefab2);
                var portalCamera2 = portalCamera.GetComponent<UnityEngine.Camera>();
                portalCamera.name = "PortalCamera[" + id + "]";
                portalCamera.SetActive(true);
                var renderTexture = new UnityEngine.RenderTexture((int)textureSize.x, (int)textureSize.y, 24, UnityEngine.RenderTextureFormat.ARGB32);
                renderTexture.filterMode = UnityEngine.FilterMode.Point;
                renderTexture.Create();
                portalCamera2.targetTexture = renderTexture;
                PostUpdateCommands.SetComponent(e, new ZoxID(id));
                PostUpdateCommands.SetSharedComponentManaged(e, renderMesh);
                var portalCameraEntity = PostUpdateCommands.Instantiate(portalCameraPrefab);
                PostUpdateCommands.SetComponent(portalCameraEntity, new PortalLink(e));
                PostUpdateCommands.SetSharedComponentManaged(portalCameraEntity, new GameObjectLink(portalCamera));
                PostUpdateCommands.SetSharedComponentManaged(portalCameraEntity, new RenderTextureLink(renderTexture));
                PostUpdateCommands.AddComponent(e, new CameraLink(portalCameraEntity));
                portalCameras.Add(id, portalCamera2);
                portalMaterials.Add(id, material);
            }).WithoutBurst().Run();
        }
    }
}