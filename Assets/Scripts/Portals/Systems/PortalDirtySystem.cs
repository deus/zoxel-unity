using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Rendering;
using Zoxel.Transforms;
using Zoxel.Cameras;

namespace Zoxel.Portals
{
    //! Links up portal material to linking render texture when linked.
    [BurstCompile, UpdateInGroup(typeof(PortalSystemGroup))]
    public partial class PortalDirtySystem : SystemBase
	{
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        [BurstCompile]
		protected override void OnUpdate()
		{
            var PostUpdateCommands2 = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged);
            Entities
                .WithAll<PortalDirty>()
                .ForEach((Entity e, in Portal portal, in ZoxMesh renderMesh) =>
            {
                PostUpdateCommands2.RemoveComponent<PortalDirty>(e);
                if (portal.linkedPortal.Index != 0 && HasComponent<Portal>(portal.linkedPortal))   // linkPortal.textureDirty == 1 && 
                {
                    var linkedPortalCameraEntity = EntityManager.GetComponentData<CameraLink>(portal.linkedPortal).camera;
                    //var linkedPortalCameraGO = EntityManager.GetSharedComponentData<GameObjectLink>(linkedPortalCameraEntity).gameObject;
                    //var linkedCamera = linkedPortalCameraGO.GetComponent<UnityEngine.Camera>(); // PortalSystem.portalCameras[zoxID.id];
                    var renderTexture = EntityManager.GetSharedComponentManaged<RenderTextureLink>(linkedPortalCameraEntity).renderTexture;
                    if (renderTexture != null)
                    {
                        renderMesh.material.SetTexture("_BaseMap", renderTexture);
                    }
                    //UnityEngine.Debug.Log("Linking portal " + e.Index + " to linkPortal: " + linkPortal.linkPortalID);
                }
            }).WithoutBurst().Run();
        }
    }
}