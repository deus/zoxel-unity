using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Cameras;

namespace Zoxel.Portals
{
    //! Cleans up portal when destroyed.
    [UpdateBefore(typeof(GameObjectDestroySystem))]
    [UpdateInGroup(typeof(LateSimulationSystemGroup))]
    public partial class PortalDestroySystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
		}

        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); //.AsParallelWriter();
            Entities
                .WithAll<DestroyEntity, Portal>()
                .ForEach((Entity e, in Portal portal, in ZoxID zoxID, in CameraLink cameraLink) =>
            {
                PostUpdateCommands.AddComponent<DestroyEntity>(cameraLink.camera);  // destroy portal camera too
                // Get linked portal:
                var linkedPortalEntity = portal.linkedPortal;
                if (HasComponent<Portal>(linkedPortalEntity) && !HasComponent<DestroyEntity>(linkedPortalEntity))
                {
                    // unlink
                    PostUpdateCommands.SetComponent(linkedPortalEntity, new Portal());
                    PostUpdateCommands.AddComponent<PortalDirty>(linkedPortalEntity);
                    var linkedPortalCameraEntity = EntityManager.GetComponentData<CameraLink>(linkedPortalEntity).camera;
                    var linkedPortalCameraGO = EntityManager.GetSharedComponentManaged<GameObjectLink>(linkedPortalCameraEntity).gameObject;
                    var linkedPortalCamera = linkedPortalCameraGO.GetComponent<UnityEngine.Camera>();
                    linkedPortalCamera.targetTexture = EntityManager.GetSharedComponentManaged<RenderTextureLink>(linkedPortalCameraEntity).renderTexture;
                }
            }).WithoutBurst().Run();
            Entities
                .WithAll<DestroyEntity, Portal>()
                .ForEach((in ZoxID zoxID) =>
            {
                PortalSystem.portalCameras.Remove(zoxID.id);
                PortalSystem.portalMaterials.Remove(zoxID.id);
            }).WithoutBurst().Run();
        }
    }
}

// var portalCameraGO = EntityManager.GetSharedComponentData<GameObjectLink>(portalCameraEntity).gameObject;
// var portalCamera = portalCameraGO.GetComponent<UnityEngine.Camera>();
// UnityEngine.GameObject.Destroy(portalCamera.targetTexture);