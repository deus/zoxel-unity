﻿using Unity.Entities;

using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Cameras;

namespace Zoxel.Portals
{
    //! Positions portal cameras behind opposing cameras
    /**
    *   \todo Convert to Parallel.
    */
    [UpdateInGroup(typeof(PortalSystemGroup))]
    public partial class PortalCameraSystem : SystemBase
    {
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private const int recursionLimit = 1;
        private const float nearClipOffset = 0.05f;
        private const float nearClipLimit = 0.2f;
        //private UnityEngine.Camera mainCamera;

		protected override void OnCreate()
		{
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
        }

        // todo: Reverse this, this should be using it's own material and rendering the linked camera
        //      rather then rendering its own camera for its linked material
        protected override void OnUpdate()
        {
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); //.AsParallelWriter();
            //SetMainCamera();
            var mainCamera = CameraReferences.GetMainCamera(EntityManager);
            // , in RenderMesh renderMesh in RenderBounds renderBounds
            var degreesToRadians = ((math.PI * 2) / 360f);
            var rot = quaternion.EulerXYZ(new float3(0, 180, 0) * degreesToRadians);
            // For each portal camera
            // pass in transforms/matrix of both portals
            // Set portal camera transform and projection matrix
            // in second system, set the matricies of the portal cameras to their linked unity object one 
            // todo: pass in portal LocalToWorld and ROtation
            Entities.ForEach((Entity e, int entityInQueryIndex, in Portal portal, in LocalToWorld localToWorld, in Rotation rotation, in ZoxID zoxID) =>
            {
                if (!HasComponent<Portal>(portal.linkedPortal) || !PortalSystem.portalCameras.ContainsKey(zoxID.id))    //  && HasComponent<Translation>(portal.linkedPortal)
                {
                    return;
                }
                var portalCamera = PortalSystem.portalCameras[zoxID.id];
                portalCamera.backgroundColor = mainCamera.backgroundColor;
                // portal is the output portal
                var portalForward = localToWorld.Forward;
                var portalMatrix = localToWorld.Value;
                var portalRotation = rotation.Value;
                var portalPosition = localToWorld.Position;
                // linked portal is the input portal
                var linkedPortalRotation = EntityManager.GetComponentData<Rotation>(portal.linkedPortal).Value;
                var linkedPortalMatrix = EntityManager.GetComponentData<LocalToWorld>(portal.linkedPortal).Value;
                // calculate position and rotation for our camera
                var relativePos = math.transform(math.inverse(linkedPortalMatrix), mainCamera.transform.position);
                relativePos = math.mul(rot, relativePos);
                var newPosition = math.transform(portalMatrix, relativePos);
                // Rotate the camera to look through the other portal
                var relativeRot = math.mul(math.inverse(linkedPortalRotation), mainCamera.transform.rotation);
                relativeRot = math.mul(rot, relativeRot);
                var newRotation = math.mul(portalRotation, relativeRot);
                var p = new UnityEngine.Plane(-portalForward, portalPosition);
                var clipPlane = new float4(p.normal.x, p.normal.y, p.normal.z, p.distance);
                var clipPlaneCameraSpace = UnityEngine.Matrix4x4.Transpose(math.inverse(portalCamera.worldToCameraMatrix)) * clipPlane;
                if (float.IsNaN(newPosition.x) || float.IsNaN(newPosition.y) || float.IsNaN(newPosition.z))
                {
                    newPosition = float3.zero;
                }
                portalCamera.transform.position = newPosition;
                portalCamera.transform.rotation = newRotation;
                portalCamera.projectionMatrix = mainCamera.CalculateObliqueMatrix(clipPlaneCameraSpace);
            }).WithoutBurst().Run();
        }
    }
}

//portalCamera.Render();
            
//var linkedPortalRenderMesh = EntityManager.GetSharedComponentData<RenderMesh>(portal.linkedPortal);
//var linkedPortalMaterial = linkedPortalRenderMesh.material;
//linkedPortalMatrix = math.mul(linkedPortalMatrix, rotMatrix);
/*linkedPortalMatrix = math.rotate(linkedPortalMatrix, rot.value);*/
//linkedPortalMatrix = math.inverse(linkedPortalMatrix);
/*var linkedPortalBounds = CreateBounds(EntityManager.GetComponentData<RenderBounds>(portal.linkedPortal), linkedPortalMatrix);
if (linkedPortalMaterial == null || float.IsNaN(linkedPortalBounds.center.x) || mainCamera == null)
{
    //linkedPortalMaterial.SetInt("displayMask", 0);
    return;
}*/
//float4x4 localToWorldMatrix = mainCamera.transform.localToWorldMatrix;
//for (int i = 0; i < recursionLimit; i++) 
//{
    //localToWorldMatrix = portalMatrix * (linkedPortalMatrix) * localToWorldMatrix;
    // portalMatrix * 
    //localToWorldMatrix = math.mul(portalMatrix, math.mul(math.inverse(linkedPortalMatrix), localToWorldMatrix));

    /*localToWorldMatrix = math.mul(portalMatrix, math.mul(linkedPortalMatrix, localToWorldMatrix ));
    UnityEngine.Matrix4x4 matrix = localToWorldMatrix;
    portalCamera.transform.position = new float3(matrix.GetColumn(3).x, matrix.GetColumn(3).y, matrix.GetColumn(3).z);
    portalCamera.transform.rotation = matrix.rotation;*/

    //Transform inTransform = inPortal.transform;
    //Transform outTransform = outPortal.transform;
    // Position the camera behind the other portal.
    // out portal is the e
    // in portal is linked one
    //portalCamera.nearClipPlane = math.distance(portalCamera.transform.position, linkedPortalPosition);
    //portalCamera.nearClipPlane = math.distance(portalCamera.transform.position, linkedPortalPosition);

    //portalCamera.Render();
//}
//linkedPortalMaterial.SetInt("displayMask", 1);
//var linkedPortalPosition = EntityManager.GetComponentData<Translation>(portal.linkedPortal).Value;
/*var distanceToCamera = math.distance(mainCamera.transform.position, linkedPortalPosition);
if (distanceToCamera > PortalSystem.portalOpenDistance)
{
    linkedPortalMaterial.SetInt("displayMask", 0);
    return;
}*/

/*if (distanceToCamera > 16 && !VisibleFromCamera(linkedPortalBounds, playerCamera))
{
    return false;
}*/

/*public static quaternion ExtractRotation(UnityEngine.Matrix4x4 matrix)
{
    float3 forward;
    forward.x = matrix.m02;
    forward.y = matrix.m12;
    forward.z = matrix.m22;
    float3 upwards;
    upwards.x = matrix.m01;
    upwards.y = matrix.m11;
    upwards.z = matrix.m21;
    return quaternion.LookRotation(forward, upwards);
}

public static float3 ExtractPosition(UnityEngine.Matrix4x4 matrix)
{
    float3 position;
    position.x = matrix.m03;
    position.y = matrix.m13;
    position.z = matrix.m23;
    return position;
}*/

/* public static bool VisibleFromCamera(UnityEngine.Bounds bounds, UnityEngine.Camera camera)
{
    var frustumPlanes = UnityEngine.GeometryUtility.CalculateFrustumPlanes(camera);
    return UnityEngine.GeometryUtility.TestPlanesAABB(frustumPlanes, bounds);
}*/

/*
// this isn't perfect as doesn't get the centre point properly of the object
private UnityEngine.Bounds CreateBounds(RenderBounds otherBounds, UnityEngine.Matrix4x4 localToWorld)
{
    var newBounds = new UnityEngine.Bounds();
    newBounds.center = localToWorld.MultiplyPoint(otherBounds.Value.Center);
    newBounds.extents = localToWorld.MultiplyVector(otherBounds.Value.Extents);
    newBounds.extents = new float3(newBounds.extents.x, newBounds.extents.y, 1);
    return newBounds;
}

public static bool BoundsOverlap(float4x4 nearObjectTransform,
    UnityEngine.Bounds nearObjectBounds,
    float4x4 farObjectTransform,
    UnityEngine.Bounds farObjectBounds,
    UnityEngine.Camera camera)
{
    var near = GetScreenRectFromBounds(nearObjectTransform, nearObjectBounds, camera);
    var far = GetScreenRectFromBounds(farObjectTransform, farObjectBounds,camera);
    // ensure far object is indeed further away than near object
    if (far.zMax > near.zMin)
    {
        // Doesn't overlap on x axis
        if (far.xMax < near.xMin || far.xMin > near.xMax)
        {
            return false;
        }
        // Doesn't overlap on y axis
        if (far.yMax < near.yMin || far.yMin > near.yMax)
        {
            return false;
        }
        // Overlaps
        return true;
    }
    return false;
}

static readonly float3[] cubeCornerOffsets =
{
    new float3 (1, 1, 1),
    new float3 (-1, 1, 1),
    new float3 (-1, -1, 1),
    new float3 (-1, -1, -1),
    new float3 (-1, 1, -1),
    new float3 (1, -1, -1),
    new float3 (1, 1, -1),
    new float3 (1, -1, 1),
};

// With thanks to http://www.turiyaware.com/a-solution-to-unitys-camera-worldtoscreenpoint-causing-ui-elements-to-display-when-object-is-behind-the-camera/
public static MinMax3D GetScreenRectFromBounds(float4x4 transform, UnityEngine.Bounds bounds, UnityEngine.Camera mainCamera)
{
    var minMax = new MinMax3D (float.MaxValue, float.MinValue);
    //var screenBoundsExtents = new float3[8];
    var localBounds = bounds; //renderer.sharedMesh.bounds;
    var anyPointIsInFrontOfCamera = true; // false;

    for (int i = 0; i < 8; i++)
    {
        var localSpaceCorner = localBounds.center + UnityEngine.Vector3.Scale(localBounds.extents, cubeCornerOffsets[i]);
        var worldSpaceCorner = math.transform(transform, localSpaceCorner);
        float3 viewportSpaceCorner = mainCamera.WorldToViewportPoint(worldSpaceCorner);
        // if (viewportSpaceCorner.z > 0)
        // {
        //     anyPointIsInFrontOfCamera = true;
        // }
        // else
        // {
        //     // If point is behind camera, it gets flipped to the opposite side
        //     // So clamp to opposite edge to correct for this
        //     viewportSpaceCorner.x = (viewportSpaceCorner.x <= 0.5f) ? 1 : 0;
        //     viewportSpaceCorner.y = (viewportSpaceCorner.y <= 0.5f) ? 1 : 0;
        // }
        // Update bounds with new corner point
        minMax.AddPoint (viewportSpaceCorner);
    }

    // All points are behind camera so just return empty bounds
    if (!anyPointIsInFrontOfCamera)
    {
        return new MinMax3D();
    }

    return minMax;
}

public struct MinMax3D
{
    public float xMin;
    public float xMax;
    public float yMin;
    public float yMax;
    public float zMin;
    public float zMax;

    public MinMax3D (float min, float max)
    {
        this.xMin = min;
        this.xMax = max;
        this.yMin = min;
        this.yMax = max;
        this.zMin = min;
        this.zMax = max;
    }

    public void AddPoint (float3 point)
    {
        xMin = math.min(xMin, point.x);
        xMax = math.max(xMax, point.x);
        yMin = math.min(yMin, point.y);
        yMax = math.max(yMax, point.y);
        zMin = math.min(zMin, point.z);
        zMax = math.max(zMax, point.z);
    }
}*/

//UnityEngine.Matrix4x4 
// used to set material in portal
//SynchPortalCameras(e, in portal, in localToWorld, in renderBounds, in zoxID);
//var portalForward = math.normalize(localToWorld.Forward);
//var portalBounds = CreateBounds(renderBounds, portalMatrix);
/*Entities.WithAll<PortalCamera>().ForEach((Entity e, ref PortalCamera camera) =>
{
    if (EntityManager.Exists(camera.portal))
    {
        // position camera near portal
        // Force camera to render it!
        // thats all!
    }
});*/
//var preFarClipPlane = playerCamera.farClipPlane;
//playerCamera.farClipPlane = portalCamera.farClipPlane;
//SetNearClipPlane(playerCamera, portalCamera, portalPosition, portalForward);
//playerCamera.farClipPlane = preFarClipPlane;
//if (i > 0)
//{
    // No need for recursive rendering if linked portal is not visible through this portal
    //if (!BoundsOverlap(portalMatrix, portalBounds, linkedPortalMatrix, linkedPortalBounds, portalCamera))
    //{
        //break;
    //}
//}
// this gets the difference between player, and the portals matrices
//UnityEngine.Matrix4x4 localToWorldMatrix2 = localToWorldMatrix;

// var portalCameraPosition = (UnityEngine.Vector3) localToWorldMatrix2.GetColumn(3);
// var portalCameraRotation = localToWorldMatrix2.rotation;
// renderPositions[renderOrderIndex] = column3; // localToWorldMatrix[]; // (localToWorldMatrix.GetColumn(3));
/*if (float.IsNaN(renderPositions[renderOrderIndex].x))
{
    linkedPortalMaterial.SetInt("displayMask", 0);
    return;
}*/
//renderRotations[renderOrderIndex] = mainCameraRotation; // localToWorldMatrix.rotation;
//startIndex = renderOrderIndex;
//var cameraPosition = mainCamera.transform.position;
//portalCamera.transform.position = portalCameraPosition;
//portalCamera.transform.rotation = portalCameraRotation;

//portalCamera.transform.localToWorldMatrix = localToWorldMatrix;   
// Hide portal mesh so that camera can see through portal
/*var preMesh = renderMesh.mesh;
renderMesh.mesh = null;
EntityManager.SetSharedComponentData(portal, renderMesh);*/
// renders them backwards
/*for (int i = startIndex; i < recursionLimit; i++)
{
portalCamera.transform.SetPositionAndRotation(renderPositions[i], renderRotations[i]);
SetProjectionMatrix(mainCamera, portalCamera, portalPosition, portalForward);
portalCamera.Render();
//UnityEngine.Debug.LogError("Rendering Camera [" + portalCamera.name + "]");
}*/
//renderMesh.mesh = preMesh;
//EntityManager.SetSharedComponentData(portal, renderMesh);

/*public bool Render(
// used to calculate projection matrix
UnityEngine.Camera playerCamera, 
UnityEngine.Camera portalCamera, 
float3 portalPosition,
float3 portalForward,
// used for recursion and checking overlapping within camera projection matrix
UnityEngine.Matrix4x4 portalMatrix,
UnityEngine.Bounds portalBounds,
UnityEngine.Matrix4x4 linkedPortalMatrix,
UnityEngine.Bounds linkedPortalBounds, 
// used to hide mesh
Entity portal,
RenderMesh renderMesh,
// used to disable portal
float distanceToCamera)
{

}*/

// Use custom projection matrix to align portal camera's near clip plane with the surface of the portal
// Note that this affects precision of the depth buffer, which can cause issues with effects like screenspace AO
/*void SetProjectionMatrix(UnityEngine.Camera playerCamera, UnityEngine.Camera portalCamera, float3 portalPosition, float3 portalForward)
{
//UnityEngine.Debug.LogError("camSpaceDst: " + camSpaceDst);
// Don't use oblique clip plane if very close to portal as it seems this can cause some visual artifacts
//if (math.abs (camSpaceDst) > nearClipLimit)
{
float3 portalCameraPosition = portalCamera.transform.position;
float4x4 portalCameraMatrixInverse = portalCamera.worldToCameraMatrix;  // UnityEngine.Matrix4x4
var portalCameraDifference = math.normalize(portalPosition - portalCameraPosition);
var dot = (int) math.ceil(math.sign(math.dot(portalForward, portalCameraDifference))); 
var camSpacePos = math.transform(portalCameraMatrixInverse, portalPosition);        // portalCameraMatrixInverse.MultiplyPoint(portalPosition);
var camSpaceNormal = math.rotate(portalCameraMatrixInverse, portalForward) * dot;   // portalCameraMatrixInverse.MultiplyVector(portalForward) * dot;
var camSpaceDst = -(math.dot(camSpacePos, camSpaceNormal)) + nearClipOffset;
var clipPlaneCameraSpace = new float4(camSpaceNormal.x, camSpaceNormal.y, camSpaceNormal.z, camSpaceDst);
// Update projection based on new clip plane
// Calculate matrix with player cam so that player camera settings (fov, etc) are used
//portalCamera.projectionMatrix = playerCamera.CalculateObliqueMatrix(clipPlaneCameraSpace);
//portalCamera.projectionMatrix = portalCamera.CalculateObliqueMatrix(clipPlaneCameraSpace);
//UnityEngine.Debug.DrawLine(portalPosition, portalCameraPosition, UnityEngine.Color.red, 1);
portalCamera.nearClipPlane = math.distance(portalPosition, portalCameraPosition);

}
else
{
portalCamera.projectionMatrix = playerCamera.projectionMatrix;
}
}*/
/*if (mainCamera != null)
{
    foreach(var portalCamera in PortalSystem.portalCameras.Values)
    {
        //portalCamera.shadows = mainCamera.shadows;
        portalCamera.depth = mainCamera.depth - 1;
        portalCamera.backgroundColor = mainCamera.backgroundColor;
        portalCamera.farClipPlane = mainCamera.farClipPlane; // portalFarClipPlane;
        portalCamera.clearFlags = mainCamera.clearFlags;
        portalCamera.fieldOfView = mainCamera.fieldOfView;
    }
}*/
/*var didRender = Render(
        // calculate projection matrix
        mainCamera,
        portalCamera, 
        portalPosition,
        portalForward,
        // bounds overlapping
        portalMatrix,
        portalBounds,
        linkedPortalMatrix,
        linkedPortalBounds,
        // used to hide mesh
        e,
        renderMesh,
        // used to disable portal
        distanceToCamera);*/
            // Skip rendering the view from this portal if player is not looking at the linked portal
//mainCamera.enabled = false;
//mainCamera.Render();
//quaternion cameraRotation = mainCamera.transform.rotation;
//var renderPositions = new float3[recursionLimit];
//var renderRotations = new quaternion[recursionLimit];
//int startIndex = 0;
//portalCamera.projectionMatrix = playerCamera.projectionMatrix;
//UnityEngine.Matrix4x4