using Unity.Entities;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Cameras;
using Zoxel.Voxels;

namespace Zoxel.Portals
{
    [BurstCompile, UpdateInGroup(typeof(PortalSystemGroup))]
    public partial class PortalTargetSystem : SystemBase
    {
        public static float nearClipPlaneDistance = 0.08f;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
		private EntityQuery portalQuery;
		private EntityQuery cameraQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
			portalQuery = GetEntityQuery(
                ComponentType.ReadOnly<Portal>(),
                ComponentType.ReadOnly<Translation>());
			cameraQuery = GetEntityQuery(
                ComponentType.ReadOnly<Camera>(),
                ComponentType.ReadOnly<Translation>(),
                ComponentType.ReadOnly<Rotation>());
		}

        [BurstCompile]
        protected override void OnUpdate()
        {
            if (portalQuery.IsEmpty)
            {
                return;
            }
            var nearClipPlaneDistance2 = PortalTargetSystem.nearClipPlaneDistance;
            var portalOpenDistance = PortalSystem.portalOpenDistance;
            // var isDebugLine = PortalTargetSystem.isDebugLine;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged).AsParallelWriter();
			Dependency = Entities
                .WithAll<Traveler, OnTeleported>()
                .ForEach((ref Traveler traveler) =>
            {
                traveler.previousPortalEntryPosition = traveler.portalEntryPosition;
            }).ScheduleParallel(Dependency);
            var cameraEntities = portalQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleA);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleA);
            var cameraPositions = GetComponentLookup<Translation>(true);
            var cameraRotations = GetComponentLookup<Rotation>(true);
            cameraEntities.Dispose();
			Dependency = Entities
                .WithAll<Traveler, OnTeleported>()
                .ForEach((ref Traveler traveler, in CameraLink cameraLink) =>
            {
                var cameraEntity = cameraLink.camera;
                if (HasComponent<Camera>(cameraEntity))
                {
                    var cameraPosition = cameraPositions[cameraEntity].Value;
                    var cameraRotation = cameraRotations[cameraEntity].Value;
                    traveler.portalEntryPosition = cameraPosition + math.rotate(cameraRotation, new float3(0, 0, nearClipPlaneDistance2));
                    traveler.portalEntryPosition2 = cameraPosition;
                }
            })  .WithReadOnly(cameraPositions)
                .WithReadOnly(cameraRotations)
                .ScheduleParallel(Dependency);
			Dependency = Entities
                .WithNone<CameraLink>()
                .WithAll<Traveler, OnTeleported>()
                .ForEach((ref Traveler traveler, in Translation translation) =>
            {
                traveler.portalEntryPosition = translation.Value;
                traveler.portalEntryPosition2 = translation.Value;
            }).ScheduleParallel(Dependency);
            var portalEntities = portalQuery.ToEntityListAsync(World.UpdateAllocator.ToAllocator, out var jobHandleB);
            Dependency = JobHandle.CombineDependencies(Dependency, jobHandleB);
            var portals = GetComponentLookup<Translation>(true);
			Dependency = Entities
                .WithNone<OnTeleported>()
                .ForEach((Entity e, int entityInQueryIndex, ref Traveler traveler) =>
            {
                var characterPosition = traveler.portalEntryPosition;
                var closestDistance = portalOpenDistance;
                var closestPortal = new Entity();
                var closestPosition = new float3();
                for (int i = 0; i < portalEntities.Length; i++)
                {
                    var portalEntity = portalEntities[i];
                    var portalTranslation = portals[portalEntity].Value;
                    // distance to
                    float newDistance = math.distance(portalTranslation, characterPosition);
                    if (newDistance < closestDistance)
                    {
                        closestDistance = newDistance;
                        closestPosition = portalTranslation;
                        closestPortal = portalEntity;
                    }
                }
                if (closestPortal.Index != 0 && traveler.portal != closestPortal)
                {
                    if (traveler.portal.Index != 0 && HasComponent<Translation>(traveler.portal))
                    {
                        PostUpdateCommands.SetComponent(entityInQueryIndex, traveler.portal, new Translation { Value = traveler.originalPortalPosition });
                    }
                    traveler.portal = closestPortal;
                    traveler.originalPortalPosition = closestPosition;
                    // var portalForward = EntityManager.GetComponentData<LocalToWorld>(traveler.portal).Forward;
                    // traveler.portalSide = SideOfPortal(characterPosition, closestPosition, portalForward);
                    // UnityEngine.Debug.LogError("Set originalPortalPosition: " + closestPosition);
                }
                // portalEntities.Dispose();
                /*#if UNITY_EDITOR
                if (isDebugLine)
                {
                    UnityEngine.Debug.DrawLine(translation.Value, traveler.portalEntryPosition, UnityEngine.Color.red, 1f);
                    UnityEngine.Debug.DrawLine(translation.Value, traveler.originalPortalPosition, UnityEngine.Color.cyan, 1f);
                }
                #endif*/
            })  .WithReadOnly(portalEntities)
                .WithDisposeOnCompletion(portalEntities)
                .WithReadOnly(portals)
                .ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);

			Dependency = Entities
                .WithAll<Traveler, OnTeleported>()
                .ForEach((Entity e, int entityInQueryIndex, ref OnTeleported onTeleported) =>
            {
                onTeleported.count++;
                if (onTeleported.count == 16)
                {
                    PostUpdateCommands.RemoveComponent<OnTeleported>(entityInQueryIndex, e);
                }
            }).ScheduleParallel(Dependency);
            // commandBufferSystem.AddJobHandleForProducer(Dependency);
        }

        /*private void SetClosestPortal(EntityCommandBuffer.ParallelWriter PostUpdateCommands, int entityInQueryIndex, ref Traveler traveler, float3 characterPosition)
        {
        }*/
    }
}

        /*private int SideOfPortal(float3 characterPosition, float3 portalPosition, float3 portalForward)
        {
            return (int) math.ceil(math.sign(math.dot(characterPosition - portalPosition, portalForward))); 
        }*/