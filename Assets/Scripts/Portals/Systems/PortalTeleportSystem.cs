﻿using Unity.Entities;
using Unity.Burst;

using Unity.Mathematics;
using Unity.Collections;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Cameras;
using Zoxel.Voxels;

namespace Zoxel.Portals
{
    //! Teleports the user between portals.
    /**
    *   \todo Spawn clone and put on other side.
    *   \todo Fix lighting inside portal.
    */
    [UpdateAfter(typeof(PortalTargetSystem))]
    [BurstCompile, UpdateInGroup(typeof(PortalSystemGroup))]
    public partial class PortalTeleportSystem : SystemBase
    {
        private const bool isDebug = false;
        // private EndSimulationEntityCommandBufferSystem.Singleton commandBufferSystem;
        private EntityQuery portalsQuery;

        protected override void OnCreate()
        {
            // commandBufferSystem = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>();
            portalsQuery = GetEntityQuery(ComponentType.ReadOnly<Portal>());
		}

        protected override void OnUpdate()
        {
            if (portalsQuery.IsEmpty)
            {
                return;
            }
            var nearClipPlaneDistance = PortalTargetSystem.nearClipPlaneDistance;
            // var isDebugLine = PortalTargetSystem.isDebugLine;
            var PostUpdateCommands = SystemAPI.GetSingleton<EndSimulationEntityCommandBufferSystem.Singleton>().CreateCommandBuffer(World.Unmanaged); // .AsParallelWriter();
            Entities
                .WithNone<OnTeleported>()
                .ForEach((Entity e, ref Traveler traveler, ref Translation translation, ref Rotation rotation, ref BodyForce bodyForce,
                    ref ChunkLink chunkLink, ref VoxelCollider voxelCollider) =>
            {
                var inPortalEntity = traveler.portal;
                if (inPortalEntity.Index == 0 || !HasComponent<Portal>(inPortalEntity))
                {
                    // UnityEngine.Debug.DrawLine(translation.Value, translation.Value + new float3(0, 1, 0), UnityEngine.Color.red, 3f);
                    return;
                }
                if (HasComponent<DestroyEntity>(inPortalEntity))
                {
                    traveler.portal = new Entity();
                    return;
                }
                var teleportPosition = traveler.portalEntryPosition;
                // var portalPosition = traveler.originalPortalPosition;
                var portalPosition = EntityManager.GetComponentData<Translation>(inPortalEntity).Value;
                //Debug.DrawLine(characterPosition, portalPosition, Color.cyan);
                var portalForward = EntityManager.GetComponentData<LocalToWorld>(inPortalEntity).Forward;
                // detect intersection with plane of portal
                var portalPlane = new UnityEngine.Plane(portalForward, portalPosition);
                var distanceToPortal = portalPlane.GetDistanceToPoint(teleportPosition);
                // bend our portal for our camera
                bool camFacingSameDirAsPortal = math.dot(portalForward, traveler.originalPortalPosition - traveler.portalEntryPosition2) > 0;
                float3 newPosition = portalForward * ((camFacingSameDirAsPortal) ? 0 : nearClipPlaneDistance);  // -nearClipPlaneDistance
                PostUpdateCommands.SetComponent(inPortalEntity, new Translation { Value = traveler.originalPortalPosition + newPosition });
                // UnityEngine.Debug.LogError("Facing Same Direction as portal? " + camFacingSameDirAsPortal);
                //UnityEngine.Debug.LogError("Portal Offset: " + newPosition + ", camFacingSameDirAsPortal: " + camFacingSameDirAsPortal
                //    + ", traveler.originalPortalPosition: " + traveler.originalPortalPosition);
                if (distanceToPortal >= 2f) // todo: use body radius
                {
                    return;
                }
                var portal = EntityManager.GetComponentData<Portal>(inPortalEntity);
                var outPortalEntity = portal.linkedPortal;
                if (!HasComponent<Portal>(outPortalEntity))
                {
                    return;
                }
                var portalRotation = EntityManager.GetComponentData<Rotation>(inPortalEntity).Value;
                var inPortalLocalToWorld = EntityManager.GetComponentData<LocalToWorld>(inPortalEntity);
                // should also check if within border of portal
                var deltaToInPortal = teleportPosition - portalPosition;
                var previousDeltaToInPortal = traveler.previousPortalEntryPosition - portalPosition;
                var sideDistance = math.dot(inPortalLocalToWorld.Right, deltaToInPortal);
                var frontDistance = math.dot(inPortalLocalToWorld.Forward, deltaToInPortal);
                var heightDistance = math.dot(inPortalLocalToWorld.Up, deltaToInPortal);
                var previousFrontDistance = math.dot(inPortalLocalToWorld.Forward, previousDeltaToInPortal);
                if (    frontDistance > 0.0f
                    &&  previousFrontDistance <= 0.0f
                    &&  math.abs(sideDistance) < 1f   // approx portal_width
                    &&  math.abs(heightDistance) < 1f)  // approx portal_height
                {
                    var linkedPortalPosition = EntityManager.GetComponentData<Translation>(outPortalEntity).Value;
                    var linkedPortalRotation = EntityManager.GetComponentData<Rotation>(outPortalEntity).Value;
                    TeleportTraveler(EntityManager, PostUpdateCommands, e, ref traveler, ref translation, ref rotation,
                        ref bodyForce, ref voxelCollider, portalPosition, portalRotation,
                        linkedPortalPosition, linkedPortalRotation, isDebug);
                    if (traveler.portal.Index != 0)
                    {
                        PostUpdateCommands.SetComponent(inPortalEntity, new Translation { Value = traveler.originalPortalPosition });
                    }
                    traveler.portal = outPortalEntity;
                    traveler.originalPortalPosition = linkedPortalPosition;
                    // var linkedPortalForward = EntityManager.GetComponentData<LocalToWorld>(outPortalEntity).Forward;
                    var portalChunkLink = EntityManager.GetComponentData<ChunkLink>(outPortalEntity);
                    chunkLink.chunk = portalChunkLink.chunk;
                    // chunkLink.chunkPosition = portalChunkLink.chunkPosition;
                    traveler.previousPortalEntryPosition = traveler.portalEntryPosition;
                    PostUpdateCommands.AddComponent<OnTeleported>(e);
                }
            }).WithoutBurst().Run();
        }
        
        private static void TeleportTraveler(EntityManager EntityManager, EntityCommandBuffer PostUpdateCommands, Entity e, ref Traveler traveler,
            ref Translation position, ref Rotation rotation, ref BodyForce bodyForce, ref VoxelCollider voxelCollider,
            float3 portalPosition, quaternion portalRotation,
            float3 linkedPortalPosition, quaternion linkedPortalRotation,
            bool isDebugLine) 
        {
            var inPortalEntity = traveler.portal;
            // Portal
            var myPosition = position.Value;
            var degreesToRadians = ((math.PI * 2) / 360f);
            var flipRotation = quaternion.EulerXYZ(new float3(0, 180, 0) * degreesToRadians);
            var reverseRotation = math.mul(linkedPortalRotation, math.mul(flipRotation, portalRotation));
            var offsetFromPortal = position.Value - portalPosition;
            offsetFromPortal = math.mul(reverseRotation, offsetFromPortal);
            position.Value = linkedPortalPosition + offsetFromPortal; // + new float3(0, 0.05f, 0);
            // UnityEngine.Debug.LogError("offsetFromPortalY: " + offsetFromPortal.y);
            // rotate around
            var portalFlippedRotation = math.mul(math.mul(flipRotation, linkedPortalRotation), portalRotation);
            rotation.Value = math.mul(portalFlippedRotation, rotation.Value);
            var beforeVelocity = bodyForce.velocity;
            var beforeAcceleration = bodyForce.acceleration;
            bodyForce.velocity = math.rotate(reverseRotation, bodyForce.velocity);
            bodyForce.acceleration = math.rotate(reverseRotation, bodyForce.acceleration);
            voxelCollider.DisplacePoints(position.Value - myPosition);
            #if UNITY_EDITOR
            if (isDebugLine)
            {
                // Portal Positions - Red
                // initial position and portal rotations
                // UnityEngine.Debug.DrawLine(myPosition, myPosition + math.mul(portalRotation, new float3(0, 0, 1)), UnityEngine.Color.red, 10f);
                // teleported position, and linked portal rotation
                // UnityEngine.Debug.DrawLine(position.Value, position.Value + math.mul(linkedPortalRotation, new float3(0, 0, 1)), UnityEngine.Color.red, 10f);
                // Position Offsets - gray
                UnityEngine.Debug.DrawLine(portalPosition, portalPosition + offsetFromPortal, UnityEngine.Color.gray, 10f);
                UnityEngine.Debug.DrawLine(linkedPortalPosition, linkedPortalPosition + offsetFromPortal, UnityEngine.Color.gray, 10f);
                // Velocity green
                UnityEngine.Debug.DrawLine(myPosition, myPosition + math.normalize(beforeVelocity), UnityEngine.Color.green, 10f);
                UnityEngine.Debug.DrawLine(position.Value, position.Value + math.normalize(bodyForce.velocity), UnityEngine.Color.green, 10f);
                // Acceleration cyan
                UnityEngine.Debug.DrawLine(myPosition, myPosition + math.normalize(beforeAcceleration), UnityEngine.Color.cyan, 10f);
                UnityEngine.Debug.DrawLine(position.Value, position.Value + math.normalize(bodyForce.acceleration), UnityEngine.Color.cyan, 10f);

                UnityEngine.Debug.DrawLine(position.Value, position.Value + math.mul(rotation.Value, new float3(0,0,1)), UnityEngine.Color.red, 10f);
            }
            #endif
            //UnityEngine.Debug.LogError("Teleported Character to: " + position.Value);
        }
    }
}
            // todo: Fix for rotated camera up, breaks and flips down
            // if (EntityManager.HasComponent<CameraLink>(e))
                /*var cameraEntity = EntityManager.GetComponentData<CameraLink>(e).camera;
                var cameraRotation = EntityManager.GetComponentData<Zoxel.Transforms.LocalRotation>(cameraEntity);
                cameraRotation.rotation = math.mul(portalFlippedRotation, cameraRotation.rotation);
                PostUpdateCommands.SetComponent(cameraEntity, cameraRotation);*/

                //var firstPersonCamera = EntityManager.GetComponentData<Zoxel.Transforms.LocalRotation>(cameraEntity);
                //var euler = (new UnityEngine.Quaternion(cameraRotation.Value.value.x, cameraRotation.Value.value.y,
                //    cameraRotation.Value.value.z, cameraRotation.Value.value.w)).eulerAngles;
                //cameraRotation.rotation = cameraRotation;
                /*cameraRotation.Value = math.mul(reverseRotation2, cameraRotation.Value);
                PostUpdateCommands.SetComponent(cameraEntity, cameraRotation);*/
                // euler.y = 0;
                //cameraRotation.Value = UnityEngine.Quaternion.Euler(euler.x, euler.y, euler.z);

                // cameraRotation.Value = math.mul(reverseRotation2, cameraRotation.Value);
                
                // var parentSynch = EntityManager.GetComponentData<ParentSynch>(cameraEntity);
                // PostUpdateCommands.SetComponent(cameraEntity, cameraRotation);

                //var euler = new UnityEngine.Quaternion(cameraRotation.Value.value.x, cameraRotation.Value.value.y,
                //    cameraRotation.Value.value.z, cameraRotation.Value.value.w).eulerAngles;


        // var cameraRotation2 = math.mul((linkedPortalRotation), (math.mul(math.inverse(portalRotation), cameraRotation.Value)));
        //var degreesToRadians = ((math.PI * 2) / 360f);
        //var rotationFlip = quaternion.EulerXYZ(new float3(0, 180, 0) * degreesToRadians);
        //cameraRotation2 = math.mul(reverseRotation, cameraRotation2);
        //cameraRotation.Value = cameraRotation2;

        /*private int SideOfPortal(float3 characterPosition, float3 portalPosition, float3 portalForward)
        {
            return (int) math.ceil(math.sign(math.dot(characterPosition - portalPosition, portalForward))); 
        }*/

        //var portalMatrix = EntityManager.GetComponentData<LocalToWorld>(inPortalEntity).Value;
        //var portalMatrixInverse =  math.inverse(EntityManager.GetComponentData<LocalToWorld>(inPortalEntity).Value);
        // linked portal
        //var linkedPortalPosition = EntityManager.GetComponentData<Translation>(outPortalEntity).Value;
        //var linkedPortalMatrix = EntityManager.GetComponentData<LocalToWorld>(outPortalEntity).Value;
        /* var flipRotationMatrix = float4x4.TRS(new float3(), flipRotation, new float3(1,1,1));
        var inInverseMatrix = math.mul(flipRotationMatrix, portalMatrix);
        var offsetFromPortal = math.transform(inInverseMatrix, myPosition); //  
        position.Value = math.transform(linkedPortalMatrix, offsetFromPortal);*/

//RepositionPortal(playerCam, traveler, characterPosition, portalForward);
/*if (EntityManager.Exists(traveler.portal))
{
    PostUpdateCommands.SetComponent(traveler.portal, new Translation { Value = traveler.originalPortalPosition });
}*/
/*RepositionPortal(playerCam, 
    traveler, 
    translation.Value + offsetCameraPosition,
    EntityManager.GetComponentData<LocalToWorld>(traveler.portal).Forward);*/
/*private void RepositionPortal(UnityEngine.Camera playerCam, Traveler traveler, float3 cameraPosition, float3 portalForward)
{
const float portalRepositionDistance = 0.5f;    // 0.5
float nearClipPlane = playerCam.nearClipPlane + nearClipPlaneAddition;
float halfHeight = nearClipPlane * Mathf.Tan(playerCam.fieldOfView * portalRepositionDistance * Mathf.Deg2Rad);
float halfWidth = halfHeight * playerCam.aspect;
float dstToNearClipPlaneCorner = new Vector3 (halfWidth, halfHeight, playerCam.nearClipPlane).magnitude;
float screenThickness = dstToNearClipPlaneCorner;
//Transform screenT = screen.transform;
bool camFacingSameDirAsPortal = math.dot(portalForward, traveler.originalPortalPosition  - cameraPosition) > 0;
float3 newPosition = portalForward * screenThickness * ((camFacingSameDirAsPortal) ? portalRepositionDistance : -portalRepositionDistance); //  Vector3.forward
PostUpdateCommands.SetComponent(traveler.portal, new Translation { Value = traveler.originalPortalPosition + newPosition });
}*/



//UnityEngine.Debug.LogError("Teleported character.");
//var travelerTranslation = EntityManager.GetComponentData<Translation>(e).Value;
//var rotation = EntityManager.GetComponentData<Rotation>(e).Value; //EntityManager.GetComponentData<LocalToWorld>(e).Value;
//var travelerMatrix = new UnityEngine.Matrix4x4();
//travelerMatrix.SetTRS(position.Value, rotation.Value, new float3(1,1,1));


    
/*firstPersonCamera.rotationVelocity = new float3();
firstPersonCamera.rotationAcceleration = new float3();
firstPersonCamera.rightStick = float2.zero;*/
//firstPersonCamera.stateTargetRotation = cameraRotation2;
//UnityEngine.Debug.LogError("Set camera rotation to: " + euler);




// if (characterPosition.x >= portalPosition.x - 1f && characterPosition.x <= portalPosition.x + 1)
// if (sideOfPortal != traveler.portalSide) //  && distanceToPortal <= 1.5f)
//{
/*if (traveler.portalSide != -1)
{
    // UnityEngine.Debug.LogError("What is this doing here?? " + traveler.portalSide);
    traveler.portalSide = sideOfPortal;
}
else
{*/

        // camera won't have been updated yet
        //var camera = EntityManager.GetComponentData<CameraLink>(e).camera;
        //var cameraPosition = EntityManager.GetComponentData<Translation>(camera).Value;
        //var cameraRotation = EntityManager.GetComponentData<Rotation>(camera).Value;
        //traveler.portalEntryPosition = cameraPosition + math.rotate(cameraRotation, new float3(0, 0, nearClipPlaneDistance));
        //traveler.portalEntryPosition2 = cameraPosition;
        // traveler.portalSide = SideOfPortal(traveler.portalEntryPosition, linkedPortalPosition, linkedPortalForward);
        //UnityEngine.Debug.LogError("Teleported Character with new Portal Side: " + traveler.portalSide);
    //}
//}
                /*var sideOfPortal = SideOfPortal(teleportPosition, portalPosition, portalForward);  //  - portalForward * bodyRadius.z
                #if UNITY_EDITOR
                if (isDebugLine)
                {
                    if (traveler.portalSide == -1)
                    {
                        UnityEngine.Debug.DrawLine(translation.Value, translation.Value + new float3(0, 1, 0), UnityEngine.Color.white, 1f);
                    }
                    else if (traveler.portalSide == 1)
                    {
                        UnityEngine.Debug.DrawLine(translation.Value, translation.Value + new float3(0, 1, 0), UnityEngine.Color.black, 1f);
                    }
                    else
                    {
                        UnityEngine.Debug.DrawLine(translation.Value, translation.Value + new float3(0, 1, 0), UnityEngine.Color.red, 3f);
                    }
                }
                #endif*/