﻿#if UNITY_RP
using Unity.Entities;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using Zoxel.Cameras;

namespace Zoxel.Portals
{
    //! Uses scriptableRender pipeline to render portal things.
    public partial class PortalRenderer : ScriptableRendererFeature
    {
        private PortalRenderPass portalRenderPass;
        
        public override void Create()
        {
            portalRenderPass = new PortalRenderPass();
            // portalRenderPass.renderPassEvent = RenderPassEvent.AfterRendering;
            portalRenderPass.renderPassEvent = RenderPassEvent.AfterRenderingPostProcessing;
        }
        
        public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
        {
            renderer.EnqueuePass(portalRenderPass);
        }
        
        class PortalRenderPass : ScriptableRenderPass
        {
            public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
            {
                var camera = renderingData.cameraData.camera;
                var commandBuffer = CommandBufferPool.Get();
                var src = BuiltinRenderTextureType.CameraTarget;
                var dst = BuiltinRenderTextureType.CurrentActive;
                // Currently breaks if split screen cameras
                if (CameraReferences.cameras.Count == 1)
                {
                    if (PortalSystem.portalCameras != null && PortalSystem.portalMaterials != null)
                    {
                        foreach (var KVP in PortalSystem.portalCameras)
                        {
                            var portalCamera = KVP.Value;
                            if (portalCamera != camera)
                            {
                                continue;
                            }
                            if (portalCamera) //  && PortalSystem.portalMaterials.ContainsKey(KVP.Key))
                            {
                                var portalMaterial = PortalSystem.portalMaterials[KVP.Key];
                                if (portalMaterial)
                                {
                                    // UnityEngine.Debug.LogError("Blitted Portal!");
                                    Blit(commandBuffer, portalCamera.targetTexture, src, portalMaterial);
                                }
                            }
                        }
                    }
                }
                commandBuffer.Blit(src, dst);
                context.ExecuteCommandBuffer(commandBuffer);
                CommandBufferPool.Release(commandBuffer);
            }
        }
    }
}
#endif