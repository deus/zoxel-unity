using Unity.Entities;

namespace Zoxel.Portals
{
    //! Links to a portal.
    public struct PortalLink : IComponentData
    {
        public Entity portal;

        public PortalLink(Entity portal)
        {
            this.portal = portal;
        }
    }
}