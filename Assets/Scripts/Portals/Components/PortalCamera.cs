﻿using Unity.Entities;

namespace Zoxel.Portals
{
    //! A portal camera tag.
    public struct PortalCamera : IComponentData { }
}