using Unity.Entities;

namespace Zoxel.Portals
{
    //! A tag that sets portals to auto link up.
    public struct AutoLinkPortal : IComponentData { }
}