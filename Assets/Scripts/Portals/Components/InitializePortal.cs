using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Portals
{
    public struct InitializePortal : IComponentData
    {
        public byte state;
        public float3 position;
        public quaternion rotation;
        // public float2 portalSize;
    }
}