using Unity.Entities;
using Unity.Mathematics;

namespace Zoxel.Portals
{
    public struct Traveler : IComponentData
    {
        public Entity portal;   // target portal
        public float3 originalPortalPosition;
        public float3 portalEntryPosition;
        public float3 portalEntryPosition2;
        public float3 previousPortalEntryPosition;
    }
}