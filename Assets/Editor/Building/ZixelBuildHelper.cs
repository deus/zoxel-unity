/*using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEditor.SceneManagement;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
// todo: also add OpenGL version for builds
//          This should also generate push scripts for different versions?

namespace Zoxel.Editor
{
    //! Build buttons for Zixel
    public partial class ZixelBuildHelper
    {
        public static string productName = "Zelltos";
        public static string gameName = "zixel";
        public static string scenePath = "Assets/Zixel/Zixel.unity";
        
        private static string sceneName = "Assets/Zixel/Zixel.unity";
        private const string automationFileLocation = "/home/deus/projects/zoxel/automation/Automation/";
        private const string buildFolderLocation = "/home/deus/builds/";
        private const string genericPushGameFilename = "butler-zoxel-generic-push.sh";
        //private const string buildPath = "/home/deus/builds/";
        private static UnityEditor.BuildOptions buildOptions = UnityEditor.BuildOptions.None; // BuildOptions.AutoRunPlayer


        [MenuItem("Zoxel/Zixel/Build_Upload_Windows")]
        static void BuildUploadZeltosWindows()
        {
            if (ZeltosBuildWindows())
            {
                ZeltosUploadWindows();
            }
        }

        [MenuItem("Zoxel/Zixel/Build_Windows")]
        static bool ZeltosBuildWindows()
        {
            UnityEngine.Debug.Log("Building Zixel for Windows.");
            return BuildHelper.BuildGeneric(gameName, scenePath, "windows", "mono");
            // return BuildHelper.BuildWindowsGame("Zixel", "Zixel", "zixel-windows", buildOptions);
            // if success
            // copy files from /home/deus/projects/zoxel/_Extra/Maps/Zixel/ into /Zeltos_Data/maps/
        }

        [MenuItem("Zoxel/Zixel/Upload_Windows")]
        static void ZeltosUploadWindows()
        {
            UnityEngine.Debug.Log("Uploading Zixel for Windows.");
            //ExecuteProcessTerminal("gnome-terminal -x bash -c " + automationFileLocation + "butler-zoxel-windows-push.sh");
            BuildHelper.ExecuteProcessTerminal("x-terminal-emulator -e " + automationFileLocation + genericPushGameFilename + " " + "zixel-windows");
        }


        [MenuItem("Zoxel/Zixel/Build_Upload_Linux")]
        static void BuildUploadZeltosLinux()
        {
            //BuildGame(BuildTargetGroup.Standalone, BuildTarget.StandaloneWindows64, ScriptingImplementation.Mono2x, 
            //    sceneName, "/home/deus/projects/Builds/zoxel-windows/Zoxel.exe", "", buildOptions);
            if (ZeltosBuildLinux())
            {
                ZeltosUploadLinux();
            }
        }

        [MenuItem("Zoxel/Zixel/Build_Linux")]
        static bool ZeltosBuildLinux()
        {
            UnityEngine.Debug.Log("Building Zixel for Linux.");
            // return BuildHelper.BuildLinuxGame("Zixel", "Zixel", "zixel-linux", buildOptions);
            return BuildHelper.BuildGeneric(gameName, scenePath, "linux", "mono", false);
            // if success
            // copy files from /home/deus/projects/zoxel/_Extra/Maps/Zixel/ into /Zeltos_Data/maps/
        }

        [MenuItem("Zoxel/Zixel/Upload_Linux")]
        static void ZeltosUploadLinux()
        {
            UnityEngine.Debug.Log("Uploading Zixel for Linux.");
            //ExecuteProcessTerminal("gnome-terminal -x bash -c " + automationFileLocation + "butler-zoxel-windows-push.sh");
            BuildHelper.ExecuteProcessTerminal("x-terminal-emulator -e " + automationFileLocation + genericPushGameFilename + " " + "zixel-linux");
        }

        [MenuItem("Zoxel/Zixel/Debug/RunNoProfilerMono")]
        static void TestRunMonoNoProfiler()
        {
            var timeBegun = UnityEngine.Time.time;
            UnityEngine.Debug.Log("Testing Zonk's Adventure Mono at: " + timeBegun);
            var success = BuildHelper.BuildGame(BuildTargetGroup.Standalone, BuildTarget.StandaloneLinux64, ScriptingImplementation.Mono2x, 
                sceneName, buildFolderLocation + "zixel-linux-mono-dev/zixel.x86_64", "",
                UnityEditor.BuildOptions.AutoRunPlayer | UnityEditor.BuildOptions.Development);
            if (success)
            {
                var timeComplete = UnityEngine.Time.time;
                UnityEngine.Debug.Log("Finished Build of Zoxel Mono at: " + timeComplete);
            }
        }

        [MenuItem("Zoxel/Zixel/Debug/RunMono")]
        static void TestRunMono()
        {
            var timeBegun = UnityEngine.Time.time;
            UnityEngine.Debug.Log("Testing Zonk's Adventure Mono at: " + timeBegun);
            var success = BuildHelper.BuildGame(BuildTargetGroup.Standalone, BuildTarget.StandaloneLinux64, ScriptingImplementation.Mono2x, 
                sceneName, buildFolderLocation + "zixel-linux-mono-dev/zixel.x86_64", "",
                UnityEditor.BuildOptions.AutoRunPlayer | UnityEditor.BuildOptions.Development | UnityEditor.BuildOptions.ConnectWithProfiler);
            if (success)
            {
                var timeComplete = UnityEngine.Time.time;
                UnityEngine.Debug.Log("Finished Build of Zoxel Mono at: " + timeComplete);
            }
        }
    }
}*/