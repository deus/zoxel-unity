/*using UnityEditor;
using UnityEngine;

namespace Zoxel.Editor
{
    //! Helper functions to build the game to itch.
    public partial class WoxelHelper
    {
        public static string productName = "Woxel";
        public static string gameName = "woxel";
        public static string scenePath = "Assets/Woxel/Woxel.unity";

        public static Texture2D GetIcon()
        {     
            return (Texture2D) Resources.Load(gameName); //  + ".png");
        }

        [MenuItem("Zoxel/Woxel/SetPlayerSettings")]
        private static void SetPlayerSettings()
        {
            PlayerSettings.productName = productName;
            PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Unknown, new Texture2D[] { GetIcon() });
        }

        #region linux

        [MenuItem("Zoxel/Woxel/Linux/TestMono")]
        public static void TestMono()
        {
            SetPlayerSettings();
            BuildHelper.BuildGenericAndRun(gameName, scenePath, "linux", "mono", false);
        }

        [MenuItem("Zoxel/Woxel/Linux/TestMonoProfiler")]
        public static void TestMonoProfiler()
        {
            SetPlayerSettings();
            BuildHelper.BuildGenericAndRun(gameName, scenePath, "linux", "mono", true);
        }

        [MenuItem("Zoxel/Woxel/Linux/BuildMono")]
        public static bool BuildMono()
        {
            SetPlayerSettings();
            return BuildHelper.BuildGeneric(gameName, scenePath, "linux", "mono");
        }

        [MenuItem("Zoxel/Woxel/Linux/BuildPlayMono")]
        public static void BuildPlayMono()
        {
            SetPlayerSettings();
            // BuildHelper.BuildAndRunLinuxGameMonoGeneric(gameName, scenePath);
            BuildHelper.BuildGeneric(gameName, scenePath, "linux", "mono", true);
        }

        [MenuItem("Zoxel/Woxel/Linux/BuildUploadMono")]
        public static void BuildUploadMono()
        {
            if (BuildMono())
            {
                UploadMono();
            }
        }

        [MenuItem("Zoxel/Woxel/Linux/UploadMono")]
        public static void UploadMono()
        {
            BuildHelper.UploadGeneric(gameName, gameName + "-linux-mono");
        }

        #endregion

        #region linux

        [MenuItem("Zoxel/Woxel/Windows/BuildMono")]
        public static bool BuildWindowsMono()
        {
            SetPlayerSettings();
            return BuildHelper.BuildGeneric(gameName, scenePath, "windows", "mono");
        }

        [MenuItem("Zoxel/Woxel/Windows/BuildUploadMonoWindows")]
        public static void BuildUploadMonoWindows()
        {
            SetPlayerSettings();
            if (BuildWindowsMono())
            {
                BuildHelper.UploadGeneric(gameName,"windows", "mono");
            }
        }

        #endregion
    }
}*/