﻿using UnityEditor;
using UnityEditor.Build.Reporting;
using UnityEditor.SceneManagement;
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unity.Entities;
using Unity.Mathematics;
// todo: also add OpenGL version for builds
//          This should also generate push scripts for different versions?

namespace Zoxel.Editor
{
    //! Helper functions to build the game to itch.
    public class BuildHelper
    {
        private const string automationFileLocation = "/home/deus/projects/zoxel/automation/Automation/";
        private const string pushGameFilename = "butler-push.sh";
        private const string buildFolderLocation = "/home/deus/builds/";

        private static BuildTarget GetBuildPlatform(string platform)
        {
            var buildPlatform = BuildTarget.StandaloneLinux64;
            if (platform == "linux")
            {
                buildPlatform = BuildTarget.StandaloneLinux64;
            }
            else if (platform == "linux-server")
            {
                // supported only in unity 2022
                // buildPlatform = BuildTarget.LinuxHeadlessSimulation;
            }
            else if (platform == "windows")
            {
                buildPlatform = BuildTarget.StandaloneWindows64;
            }
            else if (platform == "android")
            {
                buildPlatform = BuildTarget.Android;
            }
            else if (platform == "ios")
            {
                buildPlatform = BuildTarget.iOS;
            }
            else if (platform == "webgl")
            {
                buildPlatform = BuildTarget.WebGL;
            }
            else if (platform == "mac")
            {
                buildPlatform = BuildTarget.StandaloneOSX;
            }
            return buildPlatform;
        }

        private static ScriptingImplementation GetScripting(string scripting)
        {
            var scripting2 = ScriptingImplementation.Mono2x;
            if (scripting == "mono")
            {
                scripting2 = ScriptingImplementation.Mono2x;
            }
            else if (scripting == "il2cpp")
            {
                scripting2 = ScriptingImplementation.IL2CPP;
            }
            return scripting2;
        }

        private static string GetBuildFolderName(string gameName, string platform, string scripting, bool isDev)
        {
            var folderName = gameName + "-" + platform + "-" + scripting;
            if (BuildOptions.isDemo == true)
            {
                folderName += "-demo";
            }
            if (isDev)
            {
                folderName += "-dev";
            }
            return folderName;
        }

        public static void BuildAndRunWebGeneric(string gameName, string sceneName)
        {
            UnityEngine.Debug.Log("Building " + gameName + " for Web and then Running.");
            var success = BuildGame(BuildTargetGroup.Standalone, BuildTarget.WebGL, ScriptingImplementation.IL2CPP, 
                sceneName, buildFolderLocation + gameName + "/" + gameName + "-web", "", UnityEditor.BuildOptions.AutoRunPlayer);  // /zoxel
        }

        public static void BuildAndRunWebDevGeneric(string gameName, string sceneName)
        {
            UnityEngine.Debug.Log("Building " + gameName + " for Web Dev and then Running.");
            var success = BuildGame(BuildTargetGroup.Standalone, BuildTarget.WebGL, ScriptingImplementation.IL2CPP, 
                sceneName, buildFolderLocation + gameName + "/" + gameName + "-web-dev", "", 
                UnityEditor.BuildOptions.AutoRunPlayer | UnityEditor.BuildOptions.Development | UnityEditor.BuildOptions.ConnectWithProfiler);
        }

        private static string GetFilePath(string buildPath, string gameName, string platform)
        {
            var gamePath = buildPath + "/" + gameName;
            if (platform == "linux")
            {
                gamePath += ".x86_64";
            }
            else if (platform == "android")
            {
                gamePath += ".apk";
            }
            else
            {
                gamePath += ".exe";
            }
            return gamePath;
        }

        //! Use only this, remove others. Add a dev/profiler option.
        public static bool BuildGeneric(string gameName, string sceneName, string platform, string scripting,
            bool isRunAfter = false, bool isDev = false, bool isProfiler = false)
        {
            UnityEngine.Debug.Log("Building [" + gameName + "] for [" + platform + "] with [" + scripting + "]");
            var buildPlatform = GetBuildPlatform(platform);
            var scripting2 = GetScripting(scripting);
            var buildOptions = UnityEditor.BuildOptions.None; // BuildOptions.AutoRunPlayer
            if (isDev)
            {
                buildOptions = buildOptions | UnityEditor.BuildOptions.Development;
            }
            if (isRunAfter)
            {
                buildOptions = buildOptions | UnityEditor.BuildOptions.AutoRunPlayer;
            }
            if (isProfiler)
            {
                buildOptions = buildOptions | UnityEditor.BuildOptions.ConnectWithProfiler;
            }
            var buildPath = buildFolderLocation + gameName + "/" + GetBuildFolderName(gameName, platform, scripting, isDev);
            UnityEngine.Debug.Log("Building to path [" + buildPath + "]");
            var gamePath = GetFilePath(buildPath, gameName, platform);
            var success = BuildGame(BuildTargetGroup.Standalone, buildPlatform, scripting2, sceneName, gamePath, "", buildOptions);
            if (success && !isDev && platform == "linux")
            {
                var debugFilePath = buildPath + "/UnityPlayer_s.debug";
                UnityEngine.Debug.Log("Deleting File: " + debugFilePath);
                System.IO.File.Delete(debugFilePath);
                var debugFilePath2 = buildPath + "/LinuxPlayer_s.debug";
                UnityEngine.Debug.Log("Deleting File: " + debugFilePath2);
                System.IO.File.Delete(debugFilePath2);
            }
            return success;
        }

        public static bool BuildGame(BuildTargetGroup targetGroup, BuildTarget buildTarget, ScriptingImplementation scriptingBackend,
            string sceneName, string buildLocation, string butlerScript, UnityEditor.BuildOptions buildOptions)
        {
            PlayerSettings.SetScriptingBackend(targetGroup, scriptingBackend);
            var buildPlayerOptions = new BuildPlayerOptions();
            buildPlayerOptions.scenes = new[] { sceneName };
            buildPlayerOptions.locationPathName = buildLocation;
            buildPlayerOptions.target = buildTarget;
            buildPlayerOptions.options = buildOptions; //BuildOptions.AutoRunPlayer;
            try
            {
                var report = BuildPipeline.BuildPlayer(buildPlayerOptions);
                var summary = report.summary;
                if (summary.result == BuildResult.Succeeded)
                {
                    UnityEngine.Debug.Log("Build succeeded: " + summary.totalSize + " bytes");
                }
                if (summary.result == BuildResult.Failed)
                {
                    UnityEngine.Debug.Log("Build failed!");
                }
                return true;
            }
            catch (System.Exception e)
            {
                UnityEngine.Debug.Log("Build failed: " + e.ToString());
                return false;
            }
        }

        public static void UploadGeneric(string gameName, string platform, string scripting)
        {
            if (gameName == "")
            {
                UnityEngine.Debug.LogError("Game Name is null.");
                return;
            }
            var buildFolderName = GetBuildFolderName(gameName, platform, scripting, false);
            UnityEngine.Debug.Log("Uploading Build for [" + gameName + "] - [" + buildFolderName + "]");
            //ExecuteProcessTerminal("gnome-terminal -x bash -c " + automationFileLocation + "butler-zoxel-generic-push.sh -command zoxel-windows-dev");
            ExecuteProcessTerminal("x-terminal-emulator -e " + automationFileLocation + pushGameFilename + " " + gameName + " " + buildFolderName);
        }
        
        public static void ExecuteProcessTerminal(string argument)
        {
            try
            {
                UnityEngine.Debug.Log("============== Start Executing [" + argument + "] ===============");
                var startInfo = new ProcessStartInfo()
                {
                    FileName = "/bin/bash",
                    UseShellExecute = false,
                    RedirectStandardError = true,
                    RedirectStandardInput = true,
                    RedirectStandardOutput = true,
                    CreateNoWindow = false,
                    Arguments = " -c \"" + argument + " \""
                };
                var myProcess = new Process
                {
                    StartInfo = startInfo
                };
                myProcess.Start();
                while (!myProcess.StandardOutput.EndOfStream)
                {
                    UnityEngine.Debug.Log(myProcess.StandardOutput.ReadLine());
                }
                //string output = myProcess.StandardOutput.ReadToEnd();
                //UnityEngine.Debug.Log("Output: " + output);
                //myProcess.WaitForExit();
                UnityEngine.Debug.Log("============== End ===============");
        
                //return output;
            }
            catch (Exception e)
            {
                UnityEngine.Debug.Log(e);
                //return null;
            }
        }
    }
}