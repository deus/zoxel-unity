using UnityEditor;
using UnityEngine;

namespace Zoxel.Editor
{
    //! Helper functions to build the game to itch.
    public class ZoxelBuildHelper
    {
        public static string gameName = ""; // zoxel
        public static string scenePath = ""; // "Assets/Zoxel/Zoxel.unity";
        public static string scenePathWeb = ""; // "Assets/Zoxel/Zoxel_WebGL.unity";

        public static Texture2D GetIcon()
        {     
            return (Texture2D) Resources.Load(gameName); //  + ".png");
        }

        [MenuItem("Zoxel/GameBuilder/SetPlayerSettings")]
        private static void SetPlayerSettings()
        {
            gameName = BuildOptions.gameName;
            var productName = gameName.ToUpperFirstLetter();
            scenePath = "Assets/" + productName + "/" + productName + ".unity";
            scenePathWeb = "Assets/" + productName + "/" + productName + "_WebGL.unity";
            PlayerSettings.productName = productName;
            PlayerSettings.SetIconsForTargetGroup(BuildTargetGroup.Unknown, new Texture2D[] { GetIcon() });
            UnityEngine.Debug.Log("Set Project Build to [" + productName + "]");
        }

        [MenuItem("Zoxel/GameBuilder/BuildUploadAllWithDemo")]
        public static void BuildUploadAllWithDemo()
        {
            var wasDemo = BuildOptions.isDemo;
            BuildOptions.isDemo = true;
            BuildUploadAll();
            BuildOptions.isDemo = false;
            BuildUploadAll();
            BuildOptions.isDemo = wasDemo;
        }

        [MenuItem("Zoxel/GameBuilder/BuildUploadAll")]
        public static void BuildUploadAll()
        {
            BuildIL2CPP();
            BuildMono();
            BuildMonoWindows();
            UploadIL2CPP();
            UploadMono();
            UploadMonoWindows();
        }

        #region android

        [MenuItem("Zoxel/GameBuilder/Android/IL2CPP/Test")]
        public static void TestAndroidIL2CPP()
        {
            SetPlayerSettings();
            BuildHelper.BuildGeneric(gameName, scenePath, "android", "il2cpp", true, true);
        }

        [MenuItem("Zoxel/GameBuilder/Android/Mono/Test")]
        public static void TestAndroidMono()
        {
            SetPlayerSettings();
            BuildHelper.BuildGeneric(gameName, scenePath, "android", "mono", true, true);
        }

        [MenuItem("Zoxel/GameBuilder/Android/Mono/TestProfiler")]
        public static void TestAndroidMonoProfiler()
        {
            SetPlayerSettings();
            BuildHelper.BuildGeneric(gameName, scenePath, "android", "mono", true, true, true);
        }

        [MenuItem("Zoxel/GameBuilder/Android/Mono/BuildUpload")]
        public static void BuildUploadAndroidMono()
        {
            if (BuildAndroidMono())
            {
                UploadAndroidMono();
            }
        }

        [MenuItem("Zoxel/GameBuilder/Android/Mono/Build")]
        public static bool BuildAndroidMono()
        {
            SetPlayerSettings();
            return BuildHelper.BuildGeneric(gameName, scenePath, "android", "mono", false);
        }

        [MenuItem("Zoxel/GameBuilder/Android/Mono/Upload")]
        public static void UploadAndroidMono()
        {
            SetPlayerSettings();
            BuildHelper.UploadGeneric(gameName, "android", "mono");
        }

        #endregion

        #region linux-il2cpp

        [MenuItem("Zoxel/GameBuilder/Linux/IL2CPP/BuildPlay")]
        public static void BuildPlayIL2CPP()
        {
            SetPlayerSettings();
            BuildHelper.BuildGeneric(gameName, scenePath, "linux", "il2cpp", true);
        }

        [MenuItem("Zoxel/GameBuilder/Linux/IL2CPP/Build")]
        public static void BuildIL2CPP()
        {
            SetPlayerSettings();
            BuildHelper.BuildGeneric(gameName, scenePath, "linux", "il2cpp", false);
        }

        [MenuItem("Zoxel/GameBuilder/Linux/IL2CPP/BuildUpload")]
        public static void BuildUploadIL2CPP()
        {
            BuildIL2CPP();
            UploadIL2CPP();
        }

        [MenuItem("Zoxel/GameBuilder/Linux/IL2CPP/Upload")]
        public static void UploadIL2CPP()
        {
            SetPlayerSettings();
            BuildHelper.UploadGeneric(gameName, "linux", "il2cpp"); //  + "-linux-il2cpp");
        }
        
        #endregion

        #region linux

        [MenuItem("Zoxel/GameBuilder/Linux/Mono/Test")]
        public static void TestMono()
        {
            SetPlayerSettings();
            BuildHelper.BuildGeneric(gameName, scenePath, "linux", "mono", true, true);
        }

        /*[MenuItem("Zoxel/GameBuilder/Linux/Mono/TestMonoProfiler")]
        public static void TestMonoProfiler()
        {
            SetPlayerSettings();
            BuildHelper.BuildGenericAndRun(gameName, scenePath, "linux", "mono", true, true);
        }*/

        [MenuItem("Zoxel/GameBuilder/Linux/Mono/BuildPlay")]
        public static void BuildPlayMono()
        {
            SetPlayerSettings();
            BuildHelper.BuildGeneric(gameName, scenePath, "linux", "mono", true);
        }

        [MenuItem("Zoxel/GameBuilder/Linux/Mono/Build")]
        public static bool BuildMono()
        {
            SetPlayerSettings();
            return BuildHelper.BuildGeneric(gameName, scenePath, "linux", "mono", false);
        }

        [MenuItem("Zoxel/GameBuilder/Linux/Mono/BuildUpload")]
        public static void BuildUploadMono()
        {
            if (BuildMono())
            {
                UploadMono();
            }
        }

        [MenuItem("Zoxel/GameBuilder/Linux/Mono/Upload")]
        public static void UploadMono()
        {
            SetPlayerSettings();
            BuildHelper.UploadGeneric(gameName, "linux", "mono"); //   + "-linux-mono");
        }

        #endregion

        #region windows

        [MenuItem("Zoxel/GameBuilder/Windows/Mono/Build")]
        public static bool BuildMonoWindows()
        {
            SetPlayerSettings();
            return BuildHelper.BuildGeneric(gameName, scenePath, "windows", "mono", false);
        }

        [MenuItem("Zoxel/GameBuilder/Windows/Mono/BuildUpload")]
        public static void BuildUploadMonoWindows()
        {
            if (BuildMonoWindows())
            {
                UploadMonoWindows();
            }
        }

        [MenuItem("Zoxel/GameBuilder/Windows/Mono/Upload")]
        public static void UploadMonoWindows()
        {
            SetPlayerSettings();
            BuildHelper.UploadGeneric(gameName, "windows", "mono"); //   + "-windows-mono");
        }

        [MenuItem("Zoxel/GameBuilder/Windows/Mono/BuildPlay")]
        public static void BuildPlayMonoWindows()
        {
            SetPlayerSettings();
            BuildHelper.BuildGeneric(gameName, scenePath, "windows", "mono", true);
        }

        #endregion

        #region web

        [MenuItem("Zoxel/GameBuilder/Web/UploadWeb")]
        public static void UploadWeb()
        {
            SetPlayerSettings();
            BuildHelper.UploadGeneric(gameName, gameName + "-web", "il2cpp");
        }

        [MenuItem("Zoxel/GameBuilder/Web/EnableMultiThreadWebGL")]
        public static void EnableMultiThreadWebGL()
        {
            // PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
            PlayerSettings.WebGL.threadsSupport = true;
            // PlayerSettings.WebGL.emscriptenArgs = "-s ALLOW_MEMORY_GROWTH=1";
            // PlayerSettings.WebGL.memorySize = 2032;
        }

        [MenuItem("Zoxel/GameBuilder/Web/DisableMultiThreadWebGL")]
        public static void DisableMultiThreadWebGL()
        {
            // PlayerSettings.WebGL.linkerTarget = WebGLLinkerTarget.Wasm;
            // PlayerSettings.WebGL.emscriptenArgs = "-s ALLOW_MEMORY_GROWTH=1";
            // PlayerSettings.WebGL.memorySize = 2032;
            PlayerSettings.WebGL.threadsSupport = false;
        }

        [MenuItem("Zoxel/GameBuilder/Web/BuildPlayWeb")]
        public static void BuildPlayWeb()
        {
            SetPlayerSettings();
            BuildHelper.BuildAndRunWebGeneric(gameName, scenePathWeb);
        }

        [MenuItem("Zoxel/GameBuilder/Web/BuildPlayWebDev")]
        public static void BuildPlayWebDev()
        {
            SetPlayerSettings();
            BuildHelper.BuildAndRunWebDevGeneric(gameName, scenePathWeb);
        }
        #endregion

        // Enable WebGL Directivez
        // DISABLE_ASYNC_FILE_LOADING
        // DISABLE_COMPUTER_BUFFERS
        // DATA_COUNT_CHECKS
    }
}