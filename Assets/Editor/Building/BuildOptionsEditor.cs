using UnityEditor;
using UnityEngine;

namespace Zoxel.Editor
{
    [InitializeOnLoad]
    public static class BuildOptionsEditor
    {
        private const string isDemoMenuName = "Zoxel/Options/isDemo";
        private const string gameMenuNameZoxel = "Zoxel/Game/Zoxel";
        private const string gameMenuNameWoxel = "Zoxel/Game/Woxel";
        private const string gameMenuNameZixel = "Zoxel/Game/Zixel";
        private const string gameMenuNameZusix = "Zoxel/Game/Zusix";

        static BuildOptionsEditor()
        {
            EditorApplication.delayCall += () =>
            {
                Menu.SetChecked(isDemoMenuName, isDemo);
                SetGameMenus();
            };
        }

        private static bool isDemo
        {
            get
            {
                return BuildOptions.isDemo;
            }
            set
            {
                BuildOptions.isDemo = value;
                BuildOptions.SaveOptions();
            }
        }
            
        [MenuItem(isDemoMenuName)]
        private static void ToggleDemo()
        {
            isDemo = !isDemo;
            Menu.SetChecked(isDemoMenuName, isDemo);
        }

        private static byte gameType
        {
            get
            {
                return BuildOptions.gameType;
            }
            set
            {
                BuildOptions.gameType = value;
                BuildOptions.SaveOptions();
            }
        }
            
        [MenuItem(gameMenuNameZoxel)]
        private static void SetGameZoxel()
        {
            gameType = GameType.Zoxel;
            SetGameMenus();
        }
            
        [MenuItem(gameMenuNameWoxel)]
        private static void SetGameWoxel()
        {
            gameType = GameType.Woxel;
            SetGameMenus();
        }
            
        [MenuItem(gameMenuNameZixel)]
        private static void SetGameZixel()
        {
            gameType = GameType.Zixel;
            SetGameMenus();
        }
            
        [MenuItem(gameMenuNameZusix)]
        private static void SetGameZusix()
        {
            gameType = GameType.Zusix;
            SetGameMenus();
        }

        private static void SetGameMenus()
        {
            Menu.SetChecked(gameMenuNameZoxel, gameType == GameType.Zoxel);
            Menu.SetChecked(gameMenuNameWoxel, gameType == GameType.Woxel);
            Menu.SetChecked(gameMenuNameZixel, gameType == GameType.Zixel);
            Menu.SetChecked(gameMenuNameZusix, gameType == GameType.Zusix);
        }
    }
}
  
//  private const string SettingName = "isDemo";
// BuildOptions.isDemo = EditorPrefs.GetBool(SettingName, false);
// Debug.Log("Loaded isDemo: " + BuildOptions.isDemo);  
// EditorPrefs.SetBool(SettingName, value);
/*[MenuItem(isDemoMenuName, true)]
private static bool ToggleTrueDemo()
{
    isDemo = !isDemo;
    Menu.SetChecked(isDemoMenuName, isDemo);
    return true;
}

[MenuItem(isDemoMenuName, false)]
private static bool ToggleFalseDemo()
{
    isDemo = !isDemo;
    Menu.SetChecked(isDemoMenuName, isDemo);
    return false;
}*/