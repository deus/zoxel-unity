using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Unity.Entities;
using Unity.Mathematics;
using Zoxel.Zoxel2D;
using Zoxel.Zoxel2D.Physics;
using Zoxel.Generic;
using Zoxel.Rendering;
using Zoxel.Rendering.Renderer;
using Zoxel.Transforms;
using Zoxel.Movement;
using Zoxel.Input;
using Zoxel.Cameras;
using Zoxel.UI;
using Zoxel.Lines;
using Zoxel.Textures;

namespace Zoxel.Editor
{
    //! Editor tools for Zoxel2D.
    /**
    *   \todo Debug Component that spawns a gameobject hybrid. It will use that to render the mesh. Used only for editor - EditorMesh.
    *       EditorMesh added. This will stop render system rendering. It will also spawn a game object companion with MeshRenderer and MeshFilter.
    *       Once entity gets destroyed, it will destroy the game object.
    */
    /*public static class Zoxel2DEditor
    {
        // [Header("World2D")]
        public static bool isLoad;
        public static string lastLoadedPath = "/home/deus/projects/zoxel/automation/Maps/Zixel/map3.znk";
        // ecs
        private static ComponentSystemGroup[] systemGroups;
        private static SystemBase[] systems;
        private static SystemBase[] destroySystems;
        private static ComponentSystem[] componentSystems;
        // private static JobComponentSystem[] jobComponentSystems;
        private static SystemBase[] lateSystems;
        
        private static SystemBase[] renderSystems;
        // private static EndSimulationEntityCommandBufferSystem endSimulationSystem;
        private static Entity playerEntity;
        private static Entity cameraEntity;
        private static Entity world2DEntity;

        public static void InitializeManagers()
        {
            Bootstrap2D.instance = bootstrap2D;
            MaterialsManager.instance = materialsManager;
            // PhysicsManager.instance = physicsManager;
            TileManager.instance = tileManager;
            UIManager.instance = uiManager;
            // materialsManager.Initialize();
            // physicsManager.Initialize();
            uiManager.Initialize();
        }
 
        public static void AddUpdates()
        {
            EditorApplication.update += UpdateSystems;
            SceneView.duringSceneGui += RenderSceneViewGui;
        }
 
        public static void RemoveUpdates()
        {
            EditorApplication.update -= UpdateSystems;
            SceneView.duringSceneGui -= RenderSceneViewGui;
        }

        [MenuItem("ZoxelEditor/File/Open Last")]
        public static void OpenLastMap()
        {
            CloseMap();
            isLoad = true;
            SpawnWorld2D();
        }

        [MenuItem("ZoxelEditor/File/Open")]
        public static void OpenMap()
        {
            CloseMap();
            isLoad = true;
            lastLoadedPath = UnityEditor.EditorUtility.OpenFilePanel("Load World2D", "", "znk");
            EditorPrefs.SetString("ZeltosSavePath", lastLoadedPath);
            SpawnWorld2D();
        }

        [MenuItem("ZoxelEditor/File/New")]
        public static void NewMap()
        {
            CloseMap();
            isLoad = false;
            SpawnWorld2D();
        }

        [MenuItem("ZoxelEditor/File/Close")]
        public static void CloseMap()
        {
            if (EditorApplication.isPlaying) 
            {
                Debug.Log("Editor is in Play Mode.");
                return;
            }
            if (World.DefaultGameObjectInjectionWorld == null)
            {
                Debug.Log("Entities World is not existing.");
                return;
            }
            if (systems == null) 
            {
                Debug.Log("Systems doesn't exist");
                return;
            }
            if (world2DEntity.Index == 0 || !EntityManager.Exists(world2DEntity))
            {
                Debug.Log("World doesn't exist. Cannot close.");
                return;
            }
            if (!EntityManager.Exists(world2DEntity))
            {
                Debug.Log("World doesn't exist.");
                world2DEntity = new Entity();
                return;
            }
            EntityManager.AddComponent<DestroyEntity>(world2DEntity);
            EntityManager.AddComponent<DestroyAllCharacter2Ds>(EntityManager.CreateEntity());
            world2DEntity = new Entity();
        }
        
        [MenuItem("ZoxelEditor/File/Save")]
        public static void Save()
        {
            EntityManager.AddComponent<SaveEntity>(world2DEntity);
        }
        
        [MenuItem("ZoxelEditor/File/SaveAs")]
        public static void SaveAs()
        {
            EntityManager.AddComponent<SaveEntity>(world2DEntity);
            EntityManager.AddComponent<SaveAsNew>(world2DEntity);
        }
        
        [MenuItem("ZoxelEditor/ECS/DestroyGameObjects")]
        public static void DestroyGameObjects()
        {
            var gameObjects = UnityEngine.GameObject.FindGameObjectsWithTag("Entity");
            UnityEngine.Debug.Log("Destroying GameObjects with Entity tag: " + gameObjects.Length);
            for (int i = 0; i < gameObjects.Length; i++)
            {
                UnityEngine.GameObject.DestroyImmediate(gameObjects[i]);
            }
        }

        [MenuItem("ZoxelEditor/ECS/RunAndOpenLast")]
        public static void RunAndOpenLast()
        {
            RunZoxel2D();
            OpenLastMap();
        }

        [MenuItem("ZoxelEditor/ECS/RunZoxel2D")]
        public static void RunZoxel2D()
        {
            AddUpdates();
            if (World.DefaultGameObjectInjectionWorld == null)
            {
                // World.DefaultGameObjectInjectionWorld = new World("Zoxel");
                DefaultWorldInitialization.DefaultLazyEditModeInitialize();
            }
            CreateSystems();
            SpawnController();
            // SpawnWorld2D();
        }

        [MenuItem("ZoxelEditor/ECS/StopZoxel2D")]
        public static void StopZoxel2D()
        {
            RemoveUpdates();
            DestroySystems();
            World.DisposeAllWorlds();
            DestroyGameObjects();
        }

        // [ContextMenu("Spawn World2D")]
        public static void SpawnWorld2D()
        {
            if (EditorApplication.isPlaying) 
            {
                Debug.Log("Editor is in Play Mode.");
                return;
            }
            if (World.DefaultGameObjectInjectionWorld == null)
            {
                Debug.Log("Entities World is not existing.");
                return;
            }
            if (systems == null) 
            {
                Debug.Log("Systems doesn't exist");
                return;
            }
            if (world2DEntity.Index != 0)
            {
                Debug.Log("World already exists.");
                return;
            }
            var EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            UnityEngine.Debug.Log("Loading Planet: " + lastLoadedPath);
            // var lastLoadedPathText = new Text(lastLoadedPath);
            // world.GetOrCreateSystem<World2DSpawnSystem>().
            // World2DSpawnSystem.SpawnWorld2D(EntityManager, lastLoadedPathText);
            if (isLoad)
            {
                world2DEntity = EntityManager.Instantiate(World2DSpawnSystem.world2DPrefab);
                EntityManager.SetComponentData(world2DEntity, new LoadEntity(lastLoadedPath));
            }
            else
            {
                world2DEntity = EntityManager.Instantiate(World2DSpawnSystem.freshWorld2DPrefab);
            }
            EntityManager.AddComponentData(playerEntity, new World2DLink(world2DEntity));
        }

        // [ContextMenu("Spawn World2D")]
        public static void SpawnController()
        {
            if (EditorApplication.isPlaying) 
            {
                Debug.Log("Editor is in Play Mode.");
                return;
            }
            if (World.DefaultGameObjectInjectionWorld == null)
            {
                Debug.Log("Entities World is not existing.");
                return;
            }
            if (playerEntity.Index != 0) 
            {
                Debug.Log("Player already exists");
                return;
            }
            var EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
            UnityEngine.Debug.Log("Spawning Player and Camera.");
            playerEntity = EntityManager.CreateEntity();
            // var deviceID = UnityEngine.InputSystem.Keyboard.current.deviceId;
            EntityManager.AddComponentData(playerEntity, new Controller(ControllerMapping.InGame));
            EntityManager.AddComponentData(playerEntity, new DeviceTypeData(DeviceType.Keyboard));
            EntityManager.AddComponentData(playerEntity, new TileEditor { });
            EntityManager.AddComponentData(playerEntity, new UILink { });
            // Make entity grab position off gameobject instead
            cameraEntity = EntityManager.CreateEntity();
            // EntityManager.AddComponent<Zoxel.Cameras.Camera>(cameraEntity);
            var panelDepth = UIManager.instance.uiSettings.panelDepth;  // do i need panel depth here?
            var sceneView = UnityEditor.SceneView.lastActiveSceneView;
            var sceneViewCamera = sceneView.camera;
            var screenDimensions = new int2((int) sceneViewCamera.pixelWidth, (int) sceneViewCamera.pixelHeight);
            EntityManager.AddComponentData(cameraEntity, new Zoxel.Cameras.Camera(screenDimensions, panelDepth, 90));
            EntityManager.AddComponentData(cameraEntity, new Zoxel.Cameras.FieldOfView(90));
            EntityManager.AddComponent<LocalToWorld>(cameraEntity);
            EntityManager.AddSharedComponentData(cameraEntity, new SceneViewLink(sceneView));
            EntityManager.AddComponent<InitializeEntity>(cameraEntity);
            EntityManager.AddComponent<EditorCamera>(cameraEntity);
            EntityManager.AddComponent<DisableGameObjectSync>(cameraEntity);
            EntityManager.AddComponentData(cameraEntity, new Translation { Value = new float3(10, 10, -10f) });
            EntityManager.AddComponentData(cameraEntity, new Rotation { Value = quaternion.identity });
            EntityManager.AddComponent<Zoxel.Animations.ShakeEntity>(cameraEntity);
            // EntityManager.AddComponent<GameObjectReverseSync>(cameraEntity);
            // EntityManager.AddComponent<InitializeEditorCamera>(cameraEntity);
            EntityManager.AddComponent<GameObjectLink>(cameraEntity);
            // Link camera to player
            EntityManager.AddComponentData(playerEntity, new CameraLink(cameraEntity));
            EntityManager.AddComponentData(cameraEntity, new ControllerLink(playerEntity));
            // Spawn crosshair
            var spawnCrosshairEntity = EntityManager.Instantiate(CrosshairSpawnSystem.spawnCrosshairInstantPrefab);
            EntityManager.SetComponentData(spawnCrosshairEntity, new CharacterLink(playerEntity));
            // EditorToolbarSpawnSystem.spawnInstantPrefab
        }

        private static void RenderSceneViewGui(SceneView sceneView)
        {
            DisableEditorControls();
        }

        static void DisableEditorControls()
        {
            if (Event.current == null)
            {
                return;
            }
            if (renderSystems == null)
            {
                return;
            }
            // Event.current.Use();
            if (Event.current.type == EventType.MouseDown)
            {
                if (Event.current.button == 0 && Event.current.isMouse)
                {
                    // Tell the UI your event is the main one to use, it override the selection in  the scene view
                    GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                    // Don't forget to use the event
                    Event.current.Use();
                }
            }
            else if (Event.current.type == EventType.KeyDown)
            {
                if (Event.current.keyCode == KeyCode.Space || Event.current.keyCode == KeyCode.LeftControl
                    || Event.current.keyCode == KeyCode.W || Event.current.keyCode == KeyCode.A
                    || Event.current.keyCode == KeyCode.S || Event.current.keyCode == KeyCode.D
                    || Event.current.keyCode == KeyCode.L
                    || Event.current.keyCode == KeyCode.Alpha1 || Event.current.keyCode == KeyCode.Alpha2 || Event.current.keyCode == KeyCode.Alpha3)
                {
                    GUIUtility.hotControl = GUIUtility.GetControlID(FocusType.Passive);
                    Event.current.Use();
                }
            }
        }

        // [ContextMenu("Create Systems")]
        public static void CreateSystems()
        {
            if (EditorApplication.isPlaying) 
            {
                Debug.Log("Editor is in Play Mode.");
                return;
            }
            if (World.DefaultGameObjectInjectionWorld == null)
            {
                Debug.Log("Entities World is not existing.");
                return;
            }
            if (systems != null) 
            {
                Debug.Log("Systems already exist");
                return;
            }
            InitializeManagers();
            var world = World.DefaultGameObjectInjectionWorld;
            Debug.Log("Creating TilesEditor Systems");
            // world.DestroySystem(world.GetOrCreateSystem<TransformSystemGroup>());
            var systemGroupList = new List<ComponentSystemGroup>();
            systemGroupList.Add(world.GetOrCreateSystem<TileSystemGroup>());
            systemGroupList.Add(world.GetOrCreateSystem<RenderLineGroup>());
            // systemGroupList.Add(world.GetOrCreateSystem<Unity.Transforms.TransformSystemGroup>());
            systemGroupList.Add(world.GetOrCreateSystem<Zoxel.Transforms.TransformSystemGroup>());
            // systemGroupList.Add(world.GetOrCreateSystem<InitializationSystemGroup>());
            systemGroups = systemGroupList.AsArray().ToArray();

            var componentSystemsList = new List<ComponentSystem>();
            // componentSystemsList.Add(world.GetOrCreateSystem<UpdateWorldTimeSystem>());
            componentSystems = componentSystemsList.AsArray().ToArray();

            var systemsList = new List<SystemBase>();
            var lateSystemsList = new List<SystemBase>();
            var destroySystemsList = new List<SystemBase>();

            // Rendering
            systemsList.Add(world.GetOrCreateSystem<EditorMeshInitializationSystem>());
            destroySystemsList.Add(world.GetOrCreateSystem<ZoxMeshDestroySystem>());

            // Input (Controller)
            // systemsList.Add(world.GetOrCreateSystem<ControllerDevicesSystem>());

            // Camera
            systemsList.Add(world.GetOrCreateSystem<SceneViewCameraInitializeSystem>());
            systemsList.Add(world.GetOrCreateSystem<CameraScreenDimensionsSystem>());

            // Worlds
            systemsList.Add(world.GetOrCreateSystem<World2DSpawnSystem>());
            systemsList.Add(world.GetOrCreateSystem<World2DLoadSystem>());
            systemsList.Add(world.GetOrCreateSystem<World2DSaveSystem>());
            
            // Chunks
            systemsList.Add(world.GetOrCreateSystem<TilesChunkSpawnSystem>());
            systemsList.Add(world.GetOrCreateSystem<TilesChunkInitializeSystem>());
            systemsList.Add(world.GetOrCreateSystem<TilesChunkBuildSystem>());
            systemsList.Add(world.GetOrCreateSystem<TilesChunkMeshUpdateSystem>());

            // Tiles
            systemsList.Add(world.GetOrCreateSystem<TilesSpawnSystem>());
            systemsList.Add(world.GetOrCreateSystem<TilesSpawnedSystem>());
            systemsList.Add(world.GetOrCreateSystem<TilesTilemapGenerationSystem>());
            systemsList.Add(world.GetOrCreateSystem<TileRulesSpawnedSystem>());
            destroySystemsList.Add(world.GetOrCreateSystem<TileLinksDestroySystem>());
            destroySystemsList.Add(world.GetOrCreateSystem<TileRuleLinksDestroySystem>());
            
            // Characters
            systemsList.Add(world.GetOrCreateSystem<Character2DSpawnSystem>());
            systemsList.Add(world.GetOrCreateSystem<Character2DInitializeSystem>());
            systemsList.Add(world.GetOrCreateSystem<DestroyAllCharacter2DsSystem>());

            // Physics
            systemsList.Add(world.GetOrCreateSystem<Force2DSystem>());
            systemsList.Add(world.GetOrCreateSystem<Gravity2DSystem>());
            systemsList.Add(world.GetOrCreateSystem<Collision2DSystem>());
            systemsList.Add(world.GetOrCreateSystem<Friction2DSystem>());
            systemsList.Add(world.GetOrCreateSystem<Jump2DSystem>());
            
            // Animations
            systemsList.Add(world.GetOrCreateSystem<Character2DFlipSystem>());
            systemsList.Add(world.GetOrCreateSystem<Character2DAnimationSystem>());

            // RenderLines
            systemsList.Add(world.GetOrCreateSystem<RenderLineInitializationSystem>());
            systemsList.Add(world.GetOrCreateSystem<RenderLineTransformSystem>());

            // Editor
            systemsList.Add(world.GetOrCreateSystem<EditorShortcutsSystem>());
            systemsList.Add(world.GetOrCreateSystem<Editor2DInputSystem>());
            systemsList.Add(world.GetOrCreateSystem<PlaceTileSystem>());

            // generation            
            systemsList.Add(world.GetOrCreateSystem<World2DGenerationSystem>());
            systemsList.Add(world.GetOrCreateSystem<Chunk2DGenerateSystem>());

            // UI
            systemsList.Add(world.GetOrCreateSystem<UICoreSystem>());
            systemsList.Add(world.GetOrCreateSystem<UISpawnedSystem>());
            systemsList.Add(world.GetOrCreateSystem<MaterialBaseColorSystem>());
            systemsList.Add(world.GetOrCreateSystem<MaterialFrameColorSystem>());
            systemsList.Add(world.GetOrCreateSystem<CrosshairSpawnSystem>());
            systemsList.Add(world.GetOrCreateSystem<CrosshairInitializeSystem>());
            systemsList.Add(world.GetOrCreateSystem<CrosshairTextureGenerationSystem>());
            //lateSystemsList.Add(world.GetOrCreateSystem<OrbitCameraSystem>());
            destroySystemsList.Add(world.GetOrCreateSystem<UILinkDestroySystem>());
            
            // Textures
            systemsList.Add(world.GetOrCreateSystem<TextureUpdateSystem>());
            destroySystemsList.Add(world.GetOrCreateSystem<TextureDestroySystem>());
            
            // IO
            destroySystemsList.Add(world.GetOrCreateSystem<LoadedPathDestroySystem>());

            // Generic
            systemsList.Add(world.GetOrCreateSystem<EntityInitializeSystem>());
            destroySystemsList.Add(world.GetOrCreateSystem<EntityDestroySystem>());

            // Transform
            systemsList.Add(world.GetOrCreateSystem<ChildrensLinkSystem>());
            destroySystemsList.Add(world.GetOrCreateSystem<ChildrensDestroySystem>());
            systemsList.Add(world.GetOrCreateSystem<Zoxel.Animations.ShakeEntitySystem>());
            
            // Hyrbid
            systemsList.Add(world.GetOrCreateSystem<GameObjectReverseSyncSystem>());
            lateSystemsList.Add(world.GetOrCreateSystem<HybridTransformSynchSystem>());
            //lateSystemsList.Add(world.GetOrCreateSystem<TransformRotationSynchSystem>());
            //lateSystemsList.Add(world.GetOrCreateSystem<TransformScaleSynchSystem>());
            destroySystemsList.Add(world.GetOrCreateSystem<EditorCameraSynchSystem>());
            destroySystemsList.Add(world.GetOrCreateSystem<GameObjectDestroySystem>());
            // lateSystemsList.Add(world.GetOrCreateSystem<OrbitCameraSystem>());

            //var jobComponentSystemsList = new List<JobComponentSystem>();
            // componentSystemsList.Add(world.GetOrCreateSystem<UpdateWorldTimeSystem>());
            //jobComponentSystemsList.Add(world.GetOrCreateSystem<TRSToLocalToWorldSystem>());
            //jobComponentSystems = jobComponentSystemsList.AsArray().ToArray();

            systems = systemsList.AsArray().ToArray();
            //jobComponentSystems = jobComponentSystemsList.AsArray().ToArray();
            lateSystems = lateSystemsList.AsArray().ToArray();
            destroySystems = destroySystemsList.AsArray().ToArray();
            // endSimulationSystem = world.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();

            var renderSystemsList = new List<SystemBase>();
            if (!ZoxelEditorSettings.isEditorMeshes)
            {
                renderSystemsList.Add(world.GetOrCreateSystem<TilesRenderSystem>());
                renderSystemsList.Add(world.GetOrCreateSystem<Character2DRenderSystem>());
                // renderSystemsList.Add(world.GetOrCreateSystem<RenderLineRenderSystem>());
                renderSystemsList.Add(world.GetOrCreateSystem<BasicRenderSystem>());
            }
            renderSystems = renderSystemsList.AsArray().ToArray();

            var simulationSystemGroup = world.GetOrCreateSystem<SimulationSystemGroup>();
            for (int i = 0; i < systemGroups.Length; i++)
            {
                simulationSystemGroup.AddSystemToUpdateList(systemGroups[i]);
            }
            var tileSystemGroup = world.GetOrCreateSystem<TileSystemGroup>();
            for (int i = 0; i < systems.Length; i++)
            {
                tileSystemGroup.AddSystemToUpdateList(systems[i]);
            }
            var transformGroup = world.GetOrCreateSystem<Zoxel.Transforms.TransformSystemGroup>();
            for (int i = 0; i < lateSystems.Length; i++)
            {
                transformGroup.AddSystemToUpdateList(lateSystems[i]);
            }
            var lateSimulationSystemGroup = world.GetOrCreateSystem<LateSimulationSystemGroup>();
            for (int i = 0; i < destroySystems.Length; i++)
            {
                lateSimulationSystemGroup.AddSystemToUpdateList(destroySystems[i]);
            }
            var presentationSystemGroup = world.GetOrCreateSystem<PresentationSystemGroup>();
            for (int i = 0; i < renderSystems.Length; i++)
            {
                presentationSystemGroup.AddSystemToUpdateList(renderSystems[i]);
            }

            var gridSize = new int2(256, 64);
            var depth = -0.1f;
            var thickness = 0.04f;
            // x lines
            for (int i = 0; i < gridSize.x; i++)
            {
                if (i <= gridSize.y)
                {
                    RenderLineGroup.SpawnLine(EntityManager, 0, new float3(0, i, depth), new float3(gridSize.x, i, depth), 0, thickness, 0);
                }
                RenderLineGroup.SpawnLine(EntityManager, 0, new float3(i, 0, depth), new float3(i, gridSize.y, depth), 0, thickness, 0);
            }
        }

        public static void UpdateSystems()
        {
            if (EditorApplication.isPlaying)
            {
                //Debug.Log("Editor is in Play Mode, cannot run Zoxel2DEditor.");
                return;
            }
            if (systems == null)
            {
                // Debug.LogError("Systems null");
                return;
            }
            if (World.DefaultGameObjectInjectionWorld == null)
            {
                //Debug.Log("Entities World is not existing.");
                DestroySystems();
                return;
            }
            // SceneView.RepaintAll();       
            var sceneView = SceneView.lastActiveSceneView;
            // sceneView.camera.transform.position += new Vector3(0.1f, 0, 0);
            // sceneView.AlignViewToObject(sceneView.camera.transform);
            EditorUtility.SetDirty(sceneView);
            //return;

            // Debug.Log("Updating TilesEditor Systems [" + systems.Length + "]");
        }

        //[ContextMenu("Destroy Systems")]
        public static void DestroySystems()
        {
            //extra safety against post-compilation problems (typeLoadException) and spamming the console with failed updates
            if (EditorApplication.isPlaying)
            {
                Debug.Log("Editor is in Play Mode.");
                return;
            }
            if (World.DefaultGameObjectInjectionWorld == null)
            {
                Debug.Log("Entities World is not existing.");
                return;
            }
            if (systems == null) 
            {
                Debug.Log("Systems doesn't exist");
                return;
            }
            Debug.Log("Destroying TilesEditor Systems");
            var world = World.DefaultGameObjectInjectionWorld;
            for (int i = 0; i < systemGroups.Length; i++)
            {
                var systemGroup = systemGroups[i];
                if (systemGroup != null)
                {
                    world.DestroySystem(systemGroup);
                }
            }
            for (int i = 0; i < systems.Length; i++)
            {
                var system = systems[i];
                if (system != null)
                {
                    world.DestroySystem(system);
                }
            }
            for (int i = 0; i < lateSystems.Length; i++)
            {
                var lateSystem = lateSystems[i];
                if (lateSystem != null)
                {
                    world.DestroySystem(lateSystem);
                }
            }
            for (int i = 0; i < destroySystems.Length; i++)
            {
                var system = destroySystems[i];
                if (system != null)
                {
                    world.DestroySystem(system);
                }
            }
            //world.DestroySystem(endSimulationSystem);
            for (int i = 0; i < renderSystems.Length; i++)
            {
                var renderSystem = renderSystems[i];
                if (renderSystem != null)
                {
                    world.DestroySystem(renderSystem);
                }
            }
            //endSimulationSystem = null;
            systems = null;
            // jobComponentSystems = null;
            lateSystems = null;
            systemGroups = null;
            renderSystems = null;
            world2DEntity = new Entity();
            playerEntity = new Entity();
            cameraEntity = new Entity();
        }

        public static EntityManager EntityManager
        {
            get
            {
                return World.DefaultGameObjectInjectionWorld.EntityManager; 
            }
        }

        public static Bootstrap2D bootstrap2D
        {
            get
            {
                return GameObject.Find("BootZeltos").GetComponent<Bootstrap2D>(); 
            }
        }

        public static MaterialsManager materialsManager
        {
            get
            {
                return GameObject.Find("BootZeltos").GetComponent<MaterialsManager>(); 
            }
        }

        public static TileManager tileManager
        {
            get
            {
                return GameObject.Find("BootZeltos").GetComponent<TileManager>(); 
            }
        }

        public static UIManager uiManager
        {
            get
            {
                return GameObject.Find("BootZeltos").GetComponent<UIManager>(); 
            }
        }
    }*/
}

        /*public static PhysicsManager physicsManager
        {
            get
            {
                return GameObject.Find("BootZeltos").GetComponent<PhysicsManager>(); 
            }
        }*/
            /*for (int i = 0; i < jobComponentSystems.Length; i++)
            {
                var jobComponentSystem = jobComponentSystems[i];
                if (jobComponentSystem != null)
                {
                    world.DestroySystem(jobComponentSystem);
                }
            }*/
            /*for (int i = 0; i < systemGroups.Length; i++)
            {
                var systemGroup = systemGroups[i];
                if (systemGroup != null)
                {
                    systemGroup.Update();
                }
            }
            for (int i = 0; i < systems.Length; i++)
            {
                var system = systems[i];
                if (system != null)
                {
                    system.Update();
                }
            }
            for (int i = 0; i < lateSystems.Length; i++)
            {
                var lateSystem = lateSystems[i];
                if (lateSystem != null)
                {
                    lateSystem.Update();
                }
            }
            for (int i = 0; i < destroySystems.Length; i++)
            {
                var destroySystem = destroySystems[i];
                if (destroySystem != null)
                {
                    destroySystem.Update();
                }
            }
            for (int i = 0; i < renderSystems.Length; i++)
            {
                var renderSystem = renderSystems[i];
                if (renderSystem != null)
                {
                    renderSystem.Update();
                }
            }*/