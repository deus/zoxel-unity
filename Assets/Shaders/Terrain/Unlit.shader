Shader "zoxel/unlit"
{
	Properties
    {
		_Color ("Main Color", Color) = (.5, .5, .5)
	}
    SubShader
    {
        Pass
        {
            Tags
            {
                "RenderType" = "Opaque"
                "RenderPipeline" = "UniversalPipeline"
            }
            LOD 100
            CGPROGRAM
            // #pragma multi_compile_instancing 
            #pragma multi_compile_fog
            #define shaderdatas
            #pragma vertex vert
            #pragma fragment frag
            // #pragma target 4.5
            #include "UnityCG.cginc"
			#include "shaderdatas.cginc"
            uniform float4 _Color;

            VertOutput vert(VertInput vertInput)
            {
                VertOutput vertOutput;
                vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex);
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                return vertOutput;
            }

            fixed4 frag(VertOutput vertOutput) : SV_Target
            {
                fixed4 color = _Color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color);
                return color;
            }
            ENDCG
        }
    }
}