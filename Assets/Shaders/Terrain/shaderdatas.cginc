// todo: add uv texture to this

#if defined(shaderdatas)

struct VertInput
{
    float4 vertex : POSITION;
};

struct VertOutput
{
    float4 vertex : SV_POSITION;
    UNITY_FOG_COORDS(1)
    #if defined(wireframes)
    float4 worldPos : TEXCOORD4;
    float3 normal : NORMAL;
    float2 barycentricCoordinates : TEXCOORD9;
    // float3 barycentricCoordinates : TEXCOORD9;
    #endif
};
#endif