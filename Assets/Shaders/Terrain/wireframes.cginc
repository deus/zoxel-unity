#if defined(wireframes)

#pragma require geometry

float4 _WireframeColor;
float _WireframeSmoothing;
float _WireframeThickness;

struct GeometryInput
{
    VertOutput data;
};

float4 GetAlbedo(VertOutput vertOutput, float4 albedo)
{
    if (vertOutput.barycentricCoordinates.x == 0 && vertOutput.barycentricCoordinates.y == 0)
    {
        return float4(0, 1, 0, 1);
    }
    float3 barys;
    barys.xy = vertOutput.barycentricCoordinates;
    barys.z = 1 - barys.x - barys.y;

    // debug barys
    // return float4(barys.xyz, 0.8);

    float3 deltas = fwidth(barys);
    float3 thickness = deltas * _WireframeThickness;
    #if defined(wireframeQuads)
        if (barys.x >= thickness.x && barys.x <= 1 - thickness.x
            && barys.y >= thickness.y && barys.y <= 1 - thickness.y)
        {
            return albedo;
        }
    #endif
    float3 smoothing = deltas * _WireframeSmoothing;
    barys = smoothstep(thickness, thickness + smoothing, barys);
    float minBary = min(barys.x, min(barys.y, barys.z));
    return lerp(_WireframeColor, albedo, minBary);
}

// Geometry Shader -----------------------------------------------------
[maxvertexcount(3)]
void GeometryProgram(triangle VertOutput vertOutput[3], inout TriangleStream<GeometryInput> stream)
{
    float3 p0 = vertOutput[0].worldPos.xyz;
    float3 p1 = vertOutput[1].worldPos.xyz;
    float3 p2 = vertOutput[2].worldPos.xyz;
    float3 triangleNormal = normalize(cross(p1 - p0, p2 - p0));
    vertOutput[0].normal = triangleNormal;
    vertOutput[1].normal = triangleNormal;
    vertOutput[2].normal = triangleNormal;
    float line1 = distance(p0, p1);
    float line2 = distance(p0, p2);
    float line3 = distance(p1, p2);
    if (line1 > line2 && line1 > line3)
    {
        vertOutput[0].barycentricCoordinates = float2(1, 0);
        vertOutput[1].barycentricCoordinates = float2(0, 1);
        vertOutput[2].barycentricCoordinates = float2(0, 0);
    }
    else//  if (line2 > line1 && line2 > line3)
    {
        vertOutput[0].barycentricCoordinates = float2(1, 0);
        vertOutput[2].barycentricCoordinates = float2(0, 1);
        vertOutput[1].barycentricCoordinates = float2(0, 0);
    }
    GeometryInput g0, g1, g2;
    g0.data = vertOutput[0];
    g1.data = vertOutput[1];
    g2.data = vertOutput[2];
    stream.Append(g0);
    stream.Append(g1);
    stream.Append(g2);
}
#endif

/*g0.barycentricCoordinates = float2(1, 0);
g1.barycentricCoordinates = float2(0, 1);
g2.barycentricCoordinates = float2(0, 0);*/
/*else
{
    vertOutput[0].barycentricCoordinates = float2(1, 0);//, 0);
    vertOutput[1].barycentricCoordinates = float2(0, 1);//, 0);
    vertOutput[2].barycentricCoordinates = float2(0, 0);//, 1);
}*/
/*vertOutput[0].barycentricCoordinates = float2(1, 0);
vertOutput[1].barycentricCoordinates = float2(0, 1);
vertOutput[2].barycentricCoordinates = float2(0, 0);*/
    
/*if (barys.x >= 0.01 && barys.x <= 0.98
    && barys.y >= 0.01 && barys.y <= 0.98
    && barys.z >= 0.01 && barys.z <= 0.98)*/
// return float4(1, 0, 0, 0.3);