Shader "zoxel/outline1"
{
	Properties
    {
		_Color ("Main Color", Color) = (.5, .5, .5, 1)
		_OutlineWidth ("Outline Width", Range (0, 1)) = .1
		_OutlineColor ("Outline Color", Color) = (0,0,0,1)
	}
    SubShader
    {
        Pass
        {
            Tags
            {
                "RenderType" = "Opaque"
                "RenderPipeline" = "UniversalPipeline"
                "LightMode" = "UniversalForward"
            }
            LOD 100
            ZWrite Off
            Cull Front
            CGPROGRAM
            // #pragma multi_compile_instancing 
            #pragma multi_compile_fog
            #define shaderdatas
            #pragma vertex vert
            #pragma fragment frag
            // #pragma target 4.5
            #include "UnityCG.cginc"
			#include "shaderdatas.cginc"
            uniform float _OutlineWidth;
            uniform float4 _OutlineColor;

            VertOutput vert(VertInput vertInput)
            {
                VertOutput vertOutput;
                vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex  * 1.02); // + float3(-0.6, 0, 0)); //  * 1.04
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                return vertOutput;
            }

            fixed4 frag(VertOutput vertOutput) : SV_Target
            {
                fixed4 color = _OutlineColor;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color);
                return color;
            }
            ENDCG
        }
        
        Pass
        {
            Tags
            {
                "RenderType" = "Opaque"
                "RenderPipeline" = "UniversalPipeline"
                //"LightMode" = "UniversalForward"
                // "Queue" = "Geometry+600"
            }
	        //Tags {"Queue" = "Geometry+100" }
            //Cull Front
            //ZTest LEqual // Do not render "behind" existing pixels
            //ZWrite Off // Do not write to the depth buffer
            //Cull Back // Do not render triangles pointing away from the camera
            //Blend SrcAlpha OneMinusSrcAlpha // Enable alpha blending

            //Tags { "LightMode" = "Always" }
            // ZWrite On
            //ColorMask RGB
            //Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            // #pragma multi_compile_instancing 
            #pragma multi_compile_fog
            #define shaderdatas
            #pragma vertex vert
            #pragma fragment frag
            // #pragma target 4.5
            #include "UnityCG.cginc"
			#include "shaderdatas.cginc"
            uniform float4 _Color;
            
            /*float FragWriteOverlayID(VertInput vertInput) : SV_Target
            {
                return 1.0;
            }*/

            VertOutput vert(VertInput vertInput)
            {
                VertOutput vertOutput;
                vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex); //  + float3(1.2, 0, 0));
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                return vertOutput;
            }

            fixed4 frag(VertOutput vertOutput) : SV_Target
            {
                fixed4 color = _Color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color);
                return color;
            }
            ENDCG
        }
    }
}

        // Pass
        // {
        //     Cull Front
        //     CGPROGRAM
        //     #pragma vertex vert2
        //     #pragma fragment frag2
        //     //#pragma multi_compile_instancing 
        //     #pragma target 4.5
        //     #include "UnityCG.cginc"
            
        //     fixed4 _Color;

        //     VertOutput vert2(VertInput vertInput)   //  SV_Vertex
        //     {
        //         VertOutput vertOutput;
        //         vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex);
        //         return vertOutput;
        //     }

        //     float3 frag2(VertOutput vertOutput) : SV_Target
        //     {
        //         return _Color;
        //     }
        //     ENDCG
        // }
	// Fallback "Diffuse"

// Pass
// {
// 	Name "OUTLINE"
// 	Tags { "LightMode" = "Always" }
// 	Cull Front
// 	ZWrite On
// 	ColorMask RGB
// 	Blend SrcAlpha OneMinusSrcAlpha

// 	CGPROGRAM
// 	#pragma vertex vert
// 	ENDCG
// }
    
    // struct appdata {
    //     float4 vertex : POSITION;
    //     float3 normal : NORMAL;
    // };
    
    // struct v2f {
    //     float4 pos : POSITION;
    //     float4 color : COLOR;
    // };
    
    // v2f vert(appdata v) {
    //     // just make a copy of incoming vertex data but scaled according to normal direction
    //     v2f o;

    //     v.vertex *= ( 1 + _Outline);

    //     o.pos = UnityObjectToClipPos(v.vertex);
    
    //     //float3 norm   = normalize(mul ((float3x3)UNITY_MATRIX_IT_MV, v.normal));
    //     //float2 offset = TransformViewToProjection(norm.xy);

    //     o.color = _OutlineColor;
    //     return o;
    // }
 
	// SubShader {
	// 	//Tags {"Queue" = "Geometry+100" }
    //     CGPROGRAM
    //     #pragma surface surf Lambert
        
    //     sampler2D _MainTex;
    //     fixed4 _Color;
        
    //     struct Input {
    //         float2 uv_MainTex;
    //     };
        
    //     void surf (Input IN, inout SurfaceOutput o) {
    //         fixed4 c =  _Color; //  tex2D(_MainTex, IN.uv_MainTex) * _Color;
    //         o.Albedo = c.rgb;
    //         o.Alpha = c.a;
    //     }
    //     ENDCG
 
	// 	// note that a vertex shader is specified here but its using the one above
	// 	Pass {
	// 		Name "OUTLINE"
	// 		Tags { "LightMode" = "Always" }
	// 		Cull Front
	// 		ZWrite On
	// 		ColorMask RGB
	// 		Blend SrcAlpha OneMinusSrcAlpha
	// 		//Offset 50,50
	// 		CGPROGRAM
	// 		#pragma vertex vert
	// 		#pragma fragment frag
	// 		half4 frag(v2f i) : COLOR { return i.color; }
	// 		ENDCG
	// 	}
	// }