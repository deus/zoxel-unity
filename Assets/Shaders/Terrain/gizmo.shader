﻿Shader "zoxel/gizmo"
{
    Properties
    {
        _Color ("Color", Color) = (0, 0, 0)
        _WireframeColor ("Wireframe Color", Color) = (0, 0, 0)
        _WireframeSmoothing ("Wireframe Smoothing", Range(0, 100)) = 1
        _WireframeThickness ("Wireframe Thickness", Range(0, 100)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
        LOD 100

        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #define wireframes
            #define wireframeQuads
            #define shaderdatas
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog
            #if defined(wireframes)
			    #pragma geometry GeometryProgram
            #endif
		    // #pragma target 4.5
            #include "UnityCG.cginc"
			#include "shaderdatas.cginc"
			#include "wireframes.cginc"
            float4 _Color;

            VertOutput vert(VertInput vertInput)
            {
                VertOutput vertOutput;
                UNITY_INITIALIZE_OUTPUT(VertOutput, vertOutput);
                vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex);
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                #if defined(wireframes)
	            vertOutput.worldPos.xyz = mul(unity_ObjectToWorld, vertInput.vertex);
                #endif
                return vertOutput;
            }

            fixed4 frag(VertOutput vertOutput) : SV_Target
            {
                fixed4 color = _Color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color);
                #if defined(wireframes)
                float4 albedo = GetAlbedo(vertOutput, color); // float3(color.x, color.y, color.z, color.z));
                color = fixed4(albedo.x, albedo.y, albedo.z, albedo.w);
                #endif
                return color;
            }
            ENDCG
        }
    }
}