﻿Shader "Zoxel/PortalRender"
{
    Properties
    {
        _BaseMap ("Texture", 2D) = "white" {}
		//_MaskID("Mask ID", Int) = 1
    }
    SubShader
    {
		Tags
		{ 
			"RenderType" = "Opaque" 
			"Queue" = "Geometry+2" 
		}

        Pass
        {
			// Stencil
			// {
			//     Ref [_MaskID]
			//     Comp equal
			// }

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            uniform sampler2D _BaseMap;

            fixed4 frag (v2f i) : SV_Target
            {
                fixed4 col = tex2D(_BaseMap, i.uv);
                return col;
                //return fixed4(1,0,0,1);
            }
            ENDCG
        }
    }
}
