//UNITY_SHADER_NO_UPGRADE
#ifndef MYHLSLINCLUDE_INCLUDED
#define MYHLSLINCLUDE_INCLUDED

float3 TestFunction_float(float3 In)
{
    return In * In;
}

float3 AnimateUVFloat_float(float3 A, float B, out float3 Out)
{
    A = TestFunction_float(A);
    Out = A + B;
}

#endif //MYHLSLINCLUDE_INCLUDED