﻿Shader "zoxel/voxelhealth"
{
    Properties
    {
        _BaseMap ("Texture", 2D) = "white" {}
        _TilesRowCount ("Tiles Row Count", Int) = 2
    }
    SubShader
    {              
        // Tags { "RenderType"="Opaque" }
        Tags { "RenderType"="Transparent" "Queue"="Transparent"}
        LOD 100

        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing 
            #pragma multi_compile_fog
		    #pragma target 4.5
            #include "UnityCG.cginc"
            sampler2D _BaseMap;
            float4 _BaseMap_ST;
            int _TilesRowCount;
            uniform StructuredBuffer<float> healthPercentages; // use this to get uvvs

            struct VertInput
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };

            VertOutput vert(VertInput vertInput, uint instanceID : SV_InstanceID)   //  SV_Vertex
            {
                VertOutput vertOutput;
                vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex); // draws it using the bounds or matrix
                //vertOutput.uv = TRANSFORM_TEX(vertInput.uv, _BaseMap);
                // calculate uvs based on tile index
                int tileWidth2 = _TilesRowCount; // 2;
                float maxTiles = tileWidth2 * tileWidth2;
                float healthPercentage = healthPercentages[instanceID];
                if (healthPercentage >= 1)
                {
                    healthPercentage = 0.99;
                }
                int tileIndex = (tileWidth2 * tileWidth2 - 1) - maxTiles * healthPercentage; // float4(v.color, 1);
                float tileWidth = tileWidth2; // 4.0;
                float uvWidth = 1 / tileWidth;
                float2 inputUV = vertInput.uv * uvWidth;
                float positionX = tileIndex - (tileWidth2 * floor(tileIndex / (float) tileWidth2)); // modf(tileIndex, tileWidth2); // (tileIndex % tileWidth2); // iter_mod(tileIndex, tileWidth2);
                //
                float positionY = floor(tileIndex /  (float) tileWidth2); // divf(tileIndex, tileWidth2); // (tileIndex % tileWidth2); // iter_mod(tileIndex, tileWidth2);
                inputUV.x += uvWidth * positionX; // (tileIndex % tileWidth2);
                inputUV.y += uvWidth * positionY;
                vertOutput.uv = TRANSFORM_TEX(inputUV, _BaseMap);
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                return vertOutput;
            }

            fixed4 frag (VertOutput vertOutput) : SV_Target
            {
                fixed4 color = tex2D(_BaseMap, vertOutput.uv);
                UNITY_APPLY_FOG(vertOutput.fogCoord, color);
                return color;
            }
            ENDCG
        }
    }
}