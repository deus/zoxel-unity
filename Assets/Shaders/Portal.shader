﻿Shader "Custom/Portal"
{
    Properties
    {
        _BaseMap ("Texture", 2D) = "white" {}
		_MaskID("Mask ID", Int) = 1
        _InactiveColour ("Inactive Colour", Color) = (1, 1, 1, 1)
    }
    SubShader
    {
        //Tags { "RenderType"="Opaque" }
		Tags
		{ 
			"RenderType" = "Opaque" 
			"Queue" = "Geometry+2" 
			// "Queue" = "Geometry" 
		}
        //LOD 100
        // Cull Off

        Pass
        {
			Stencil
			{
			    Ref [_MaskID]
			    Comp equal
			}
            // Blend SrcAlpha OneMinusSrcAlpha
            // ZWrite Off
            // Cull Front
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing 
            #pragma multi_compile_fog
            #include "UnityCG.cginc"

            struct VertInput
            {
                float4 vertex : POSITION;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 screenPos : TEXCOORD0;
            };

            sampler2D _BaseMap;
            float4 _InactiveColour;
            int displayMask; // set to 1 to display texture, otherwise will draw test colour
            
            VertOutput vert (VertInput vertexInput)
            {
                VertOutput vertOutput;
                vertOutput.vertex = UnityObjectToClipPos(vertexInput.vertex);
                vertOutput.screenPos = ComputeScreenPos(vertOutput.vertex);

                // vertOutput.vertex.z += 0.08f;

                // vertOutput.vertex.y -= 0.5f;
                //vertOutput.vertex = UnityObjectToClipPos(vertexInput.vertex.xyz);
                //vertOutput.uv = vertexInput.uv; // ComputeScreenPos(vertOutput.vertex); // using the UnityCG.cginc version unmodified
                //vertOutput.uv = ComputeScreenPos(vertOutput.vertex); // using the UnityCG.cginc version unmodified
                //UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                return vertOutput;
            }

            fixed4 frag (VertOutput vertOutput) : SV_Target
            {
                float2 uv = vertOutput.screenPos.xy / vertOutput.screenPos.w;
                fixed4 portalCol = tex2D(_BaseMap, uv);
                //fixed4 portalCol = tex2Dproj(_BaseMap, vertOutput.uv);
                //fixed4 portalCol = tex2D(_BaseMap, vertOutput.uv);
                //portalCol = portalCol * displayMask + _InactiveColour * (1 - displayMask);
                //UNITY_APPLY_FOG(vertOutput.fogCoord, portalCol);
                return portalCol;
            }
            ENDCG
        }
    }
    Fallback "Standard" // for shadows
}

                //float2 uv : TEXCOORD0;
                //float4 uv : TEXCOORD0;
                //UNITY_FOG_COORDS(1)