﻿Shader "zoxel/terrain/minivox_unstructured"
{
    SubShader
    {              
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog
            #pragma multi_compile_instancing 
		    // #pragma target 4.5
            #include "UnityCG.cginc"
            float4 color;

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 color : COLOR;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                UNITY_FOG_COORDS(1)
            };

            VertOutput vert(VertInput v)   //  SV_Vertex
            {
                VertOutput vertOutput;
                vertOutput.vertex = UnityObjectToClipPos(v.vertex); // draws it using the bounds or matrix
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                float4 color2 = float4(v.color, 1);
                color2 *= color;
                vertOutput.color = color2;
                return vertOutput;
            }
            // fixed4
            half4 frag (VertOutput vertOutput) : SV_Target
            {
                float4 color2 = vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color2);
                return color2;
            }
            ENDCG
        }
    }
}