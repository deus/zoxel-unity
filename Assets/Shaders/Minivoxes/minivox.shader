﻿Shader "zoxel/terrain/minivox"
{
    SubShader
    {              
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog
            // #pragma multi_compile_instancing 
		    // #pragma target 4.5
            #include "UnityCG.cginc"

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 color : COLOR;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                UNITY_FOG_COORDS(1)
            };
            
            StructuredBuffer<float4> colors;

            VertOutput vert(VertInput v, uint instanceID : SV_InstanceID)   //  SV_Vertex
            {
                VertOutput vertOutput;
                float4 color = float4(v.color, 1);
                vertOutput.vertex = UnityObjectToClipPos(v.vertex); // draws it using the bounds or matrix
                float4 colorMultiplier = colors[instanceID];
                color *= colorMultiplier;
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                vertOutput.color = color;
                return vertOutput;
            }
            // fixed4
            half4 frag (VertOutput vertOutput) : SV_Target
            {
                float4 color = vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color);
                return color;
            }
            ENDCG
        }
    }
}