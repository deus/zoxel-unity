﻿Shader "zoxel/terrain/model_outline"
{
    Properties
    {
		outlineWidth ("Outline Width", Range (0, 1)) = 0.004
    }
    SubShader
    {
        // 1st pass is base color
        Pass
        {
            Tags
            {
                "RenderType" = "Opaque"
                "RenderPipeline" = "UniversalPipeline"
            }
            LOD 100
            // ZWrite on
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // #pragma multi_compile_instancing
            #pragma multi_compile_fog
		    // #pragma target 4.5
            #include "UnityCG.cginc"
            uniform float3 color;
            uniform float3 additionColor;

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float3 color : COLOR;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float3 color : COLOR;
                UNITY_FOG_COORDS(1)
            };

            VertOutput vert(VertInput vertInput)
            {
                VertOutput vertOutput;
                vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex);
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                float3 color2 = vertInput.color;
                color2.x *= color.x;
                color2.y *= color.y;
                color2.z *= color.z;
                color2 += additionColor;
                vertOutput.color = color2;
                return vertOutput;
            }

            float3 frag (VertOutput vertOutput) : SV_Target
            {
                float3 color = vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color);
                return color;
            }
            ENDCG
        }

        // 2nd pass is outline color
        Pass
        {
            Tags
            {
                "RenderType" = "Transparent"
                "RenderPipeline" = "UniversalPipeline"
                "LightMode" = "UniversalForward"
            }
            LOD 100
            Cull Front
            // ZTest [_ZTest]
            // ZWrite off
            CGPROGRAM
            #pragma vertex Vert
            #pragma fragment Frag
            // #pragma multi_compile_fog
            #pragma multi_compile_instancing
		    // #pragma target 4.5
            #include "UnityCG.cginc"
            uniform float3 outlineColor;
            uniform float outlineWidth;

            struct VertInput
            {
                float3 vertex : POSITION;
                float3 normal : NORMAL;
                float3 color : COLOR;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float3 color : COLOR;
                UNITY_FOG_COORDS(1)
            };
            
            struct GeometryInput
            {
                VertInput data;
            };

            VertOutput Vert(VertInput vertInput)
            {
                VertOutput vertOutput;
                vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex + vertInput.normal * outlineWidth);
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                vertOutput.color = outlineColor;
                return vertOutput;
            }

            float3 Frag(VertOutput vertOutput) : SV_Target
            {
                float3 color = vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color);
                return color;
            }
            ENDCG
        }
    }
}
                // debug normals
                // /*if (vertInput.normal.x == -1)
                // {
                //     vertOutput.color = float3(1, 0, 0);
                // }
                // else if (vertInput.normal.x == 1)
                // {
                //     vertOutput.color = float3(0, 0, 1);
                // }
                // else if (vertInput.normal.y == -1)
                // {
                //     vertOutput.color = float3(1, 0, 0);
                // }
                // else if (vertInput.normal.y == 1)
                // {
                //     vertOutput.color = float3(0, 0, 1);
                // }
                // else if (vertInput.normal.z == -1)
                // {
                //     vertOutput.color = float3(1, 0, 0);
                // }
                // else if (vertInput.normal.z == 1)
                // {
                //     vertOutput.color = float3(0, 0, 1);
                // }*/

// float3 meshVertex = vertInput.vertex.xyz;
// meshVertex.x *= (1 + outlineWidth * vertInput.normal.x);
// meshVertex.y *= (1 + outlineWidth * vertInput.normal.y);
// meshVertex.z *= (1 + outlineWidth * vertInput.normal.z);
// vertOutput.vertex = UnityObjectToClipPos(meshVertex);
// if (triangleNormal.x == 1)
// {
//     vertOutput[0].color = half3(1, 0, 0);
//     vertOutput[1].color = half3(1, 0, 0);
//     vertOutput[2].color = half3(1, 0, 0);
// }
// if (vertInput.normal.x != 0)
// {
//     vertOutput.color = half3(1, 0, 0);
// }
// if (vertInput.normal.y != 0)
// {
//     vertOutput.color = half3(0, 0, 1);
// }

// triangleStream.RestartStrip();

// GeometryInput g0, g1, g2;
// g0.data = vertInput[0];
// g1.data = vertInput[1];
// g2.data = vertInput[2];
// stream.Append(g0);
// stream.Append(g1);
// stream.Append(g2);
            

// #pragma geometry Geometry
// Calculates normals and adds outlines for them
// [maxvertexcount(3)]
// void Geometry(triangle VertOutput vertOutput[3], inout TriangleStream<VertOutput> triangleStream)
// {
//     float3 vertexA = vertOutput[0].vertex.xyz;
//     float3 vertexB = vertOutput[1].vertex.xyz;
//     float3 vertexC = vertOutput[2].vertex.xyz;
//     float3 triangleNormal = normalize(cross(vertexB - vertexA, vertexC - vertexA));
//     vertexA.x += (outlineWidth * triangleNormal.x);
//     vertexA.y += (outlineWidth * triangleNormal.y);
//     vertexA.z += (outlineWidth * triangleNormal.z);
//     vertexB.x += (outlineWidth * triangleNormal.x);
//     vertexB.y += (outlineWidth * triangleNormal.y);
//     vertexB.z += (outlineWidth * triangleNormal.z);
//     vertexC.x += (outlineWidth * triangleNormal.x);
//     vertexC.y += (outlineWidth * triangleNormal.y);
//     vertexC.z += (outlineWidth * triangleNormal.z);
//     const float boost = 1;
//     const float midPointX = 0;
//     if (triangleNormal.y != 0)
//     {
//         float xSign = 1;
//         if (vertexA.x < midPointX)
//         {
//             vertexA.x -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexA.x += outlineWidth * boost;
//         }
//         if (vertexB.x < midPointX)
//         {
//             vertexB.x -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexB.x += outlineWidth * boost;
//         }
//         if (vertexC.x < midPointX)
//         {
//             vertexC.x -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexC.x += outlineWidth * boost;
//         }
//         if (vertexA.z < midPointX)
//         {
//             vertexA.z -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexA.z += outlineWidth * boost;
//         }
//         if (vertexB.z < midPointX)
//         {
//             vertexB.z -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexB.z += outlineWidth * boost;
//         }
//         if (vertexC.z < midPointX)
//         {
//             vertexC.z -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexC.z += outlineWidth * boost;
//         }
//     }
//     if (triangleNormal.z != 0)
//     {
//         if (vertexA.x < midPointX)
//         {
//             vertexA.x -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexA.x += outlineWidth * boost;
//         }
//         if (vertexB.x < midPointX)
//         {
//             vertexB.x -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexB.x += outlineWidth * boost;
//         }
//         if (vertexC.x < midPointX)
//         {
//             vertexC.x -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexC.x += outlineWidth * boost;
//         }
//         if (vertexA.y < midPointX)
//         {
//             vertexA.y -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexA.y += outlineWidth * boost;
//         }
//         if (vertexB.y < midPointX)
//         {
//             vertexB.y -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexB.y += outlineWidth * boost;
//         }
//         if (vertexC.y < midPointX)
//         {
//             vertexC.y -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexC.y += outlineWidth * boost;
//         }
//     }
//     if (triangleNormal.x != 0)
//     {
//         if (vertexA.y < midPointX)
//         {
//             vertexA.y -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexA.y += outlineWidth * boost;
//         }
//         if (vertexB.y < midPointX)
//         {
//             vertexB.y -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexB.y += outlineWidth * boost;
//         }
//         if (vertexC.y < midPointX)
//         {
//             vertexC.y -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexC.y += outlineWidth * boost;
//         }
//         if (vertexA.z < midPointX)
//         {
//             vertexA.z -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexA.z += outlineWidth * boost;
//         }
//         if (vertexB.z < midPointX)
//         {
//             vertexB.z -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexB.z += outlineWidth * boost;
//         }
//         if (vertexC.z < midPointX)
//         {
//             vertexC.z -= outlineWidth * boost;
//         }
//         else
//         {
//             vertexC.z += outlineWidth * boost;
//         }
//     }
//     vertOutput[0].vertex = UnityObjectToClipPos(vertexA);
//     vertOutput[1].vertex = UnityObjectToClipPos(vertexB);
//     vertOutput[2].vertex = UnityObjectToClipPos(vertexC);
//     triangleStream.Append(vertOutput[0]);
//     triangleStream.Append(vertOutput[1]);
//     triangleStream.Append(vertOutput[2]);
// }