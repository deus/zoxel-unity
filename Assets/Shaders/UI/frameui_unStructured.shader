﻿Shader "zoxel/ui/frameui_unStructured"
{
    Properties
    {
        _BaseMap ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "Queue"="Transparent" }
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex Vertex
            #pragma fragment Fragment
            #pragma multi_compile_fog
		    // #pragma target 4.5
            #include "UnityCG.cginc"
            sampler2D _BaseMap;
            float4 _BaseMap_ST;
            uniform float4 color;
            uniform float4 frameColor;

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 color : COLOR;
                float2 uv : TEXCOORD0;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 frameColor : COLOR1;
            };

            VertOutput Vertex(VertInput vertInput)   //  SV_Vertex
            {
                VertOutput vertOutput;
                vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex); // draws it using the bounds or matrix
                vertOutput.uv = TRANSFORM_TEX(vertInput.uv, _BaseMap);
                vertOutput.color = color;
                vertOutput.frameColor = frameColor;
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                return vertOutput;
            }

            float4 Fragment(VertOutput vertOutput) : SV_Target
            {
                float4 textureColor = tex2D(_BaseMap, vertOutput.uv);
                float4 color2 = float4(0, 0, 0, 1);
                if (textureColor.x == 0 && textureColor.y == 0 && textureColor.z == 0)
                {
                    color2 = vertOutput.frameColor;
                }
                else
                {
                    // color2 = vertOutput.color;
                    color2.x = textureColor.x;
                    color2.x *= vertOutput.color.x;
                    color2.y = textureColor.y;
                    color2.y *= vertOutput.color.y;
                    color2.z = textureColor.z;
                    color2.z *= vertOutput.color.z;
                    color2.w = vertOutput.color.w;
                }
                color2.w *= textureColor.w;
                // UNITY_APPLY_FOG(vertOutput.fogCoord, color2);
                return color2;
            }
            ENDCG
        }
    }
}