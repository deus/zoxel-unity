﻿Shader "zoxel/ui/baseui"
{
    Properties
    {
        _BaseMap ("Texture", 2D) = "white" {}
    }
    SubShader
    {              
        // Tags { "RenderType"="Transparent" "Queue"="Transparent" }
        //Tags { "Queue"="Transparent" "RenderType" = "Opaque" }
        // Tags { "RenderType"="Transparent" "Queue"="Transparent" }
        // LOD 100
        // Stencil
        // {
        //     Ref 1
        //     Comp NotEqual
        // }
        //ZWrite Off
        Tags
        {
            "Queue"="Transparent"
            // "IgnoreProjector"="True"
            // "RenderType"="Transparent"
        }
        // LOD 100
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex Vertex
            #pragma fragment Fragment
            #pragma multi_compile_fog
            // #pragma multi_compile_instancing
		    #pragma target 4.5
            #include "UnityCG.cginc"

            sampler2D _BaseMap;
            float4 _BaseMap_ST;

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 color : COLOR;
                float2 uv : TEXCOORD0;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            
            StructuredBuffer<float4> colors;

            VertOutput Vertex(VertInput vertInput, uint instanceID : SV_InstanceID)   //  SV_Vertex
            {
                VertOutput vertOutput;
                float4 color = float4(vertInput.color, 1);
                vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex); // draws it using the bounds or matrix
                vertOutput.uv = TRANSFORM_TEX(vertInput.uv, _BaseMap);
                float4 colorMultiplier = colors[instanceID];
                color *= colorMultiplier;
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                vertOutput.color = color;
                vertOutput.color.x *= 0.5;
                vertOutput.color.y *= 0.5;
                vertOutput.color.z *= 0.5;
                return vertOutput;
            }

            fixed4 Fragment(VertOutput vertOutput) : SV_Target
            {
                fixed4 color = tex2D(_BaseMap, vertOutput.uv);
                color *= vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color);
                return color;
            }
            ENDCG
        }
    }
}