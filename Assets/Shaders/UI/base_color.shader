﻿Shader "zoxel/ui/base_color"
{
    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
        }
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex Vertex
            #pragma fragment Fragment
            #pragma multi_compile_fog
            // #pragma multi_compile_instancing
		    // #pragma target 4.5
            #include "UnityCG.cginc"
            uniform float4 color;

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 color : COLOR;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                UNITY_FOG_COORDS(1)
            };

            VertOutput Vertex(VertInput vertInput)
            {
                VertOutput vertOutput;
                float4 color2 = float4(vertInput.color, 1);
                vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex); // draws it using the bounds or matrix
                color2 *= color;
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                vertOutput.color = color2;
                return vertOutput;
            }

            fixed4 Fragment(VertOutput vertOutput) : SV_Target
            {
                fixed4 color2 = vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color2);
                return color2;
            }
            ENDCG
        }
    }
}