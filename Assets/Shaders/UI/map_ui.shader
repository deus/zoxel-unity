﻿Shader "zoxel/ui/map"
{
    Properties
    {
        _BaseMap ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
        }
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex Vertex
            #pragma fragment Fragment
            #pragma multi_compile_fog
            #include "UnityCG.cginc"
            sampler2D _BaseMap;
            float4 _BaseMap_ST;
            uniform float4 color;
            uniform float3 mapPosition;
            uniform float4 mapRotation;
            uniform float mapSize;

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 color : COLOR;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                float2 uv : TEXCOORD0;
                float3 worldPosition : TEXCOORD2;
                UNITY_FOG_COORDS(1)
            };

            float4 multiplyRotation(float4 q1, float4 q2)
            { 
                float4 qr;
                qr.x = (q1.w * q2.x) + (q1.x * q2.w) + (q1.y * q2.z) - (q1.z * q2.y);
                qr.y = (q1.w * q2.y) - (q1.x * q2.z) + (q1.y * q2.w) + (q1.z * q2.x);
                qr.z = (q1.w * q2.z) + (q1.x * q2.y) - (q1.y * q2.x) + (q1.z * q2.w);
                qr.w = (q1.w * q2.w) - (q1.x * q2.x) - (q1.y * q2.y) - (q1.z * q2.z);
                return qr;
            }
            
            float4 conjugateRotation(float4 rotation)
            { 
                return float4(-rotation.x, -rotation.y, -rotation.z, rotation.w); 
            }

            float4 inverseRotation(float4 q)
            {
                float sqr = sqrt(q.x*q.x + q.y*q.y + q.z*q.z + q.w*q.w);
                return conjugateRotation(q) / sqr;
            }
            
            float3 rotateVector(float4 rotation, float3 vertex3)
            {
                float4 vertex = float4(vertex3, 0.0);
                rotation = inverseRotation(rotation);
                return multiplyRotation(conjugateRotation(rotation), multiplyRotation(vertex, rotation)).xyz;
            }

            VertOutput Vertex(VertInput vertInput)
            {
                VertOutput vertOutput;
                float4 color2 = float4(vertInput.color, 1);
                vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex); // draws it using the bounds or matrix
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                vertOutput.uv = TRANSFORM_TEX(vertInput.uv, _BaseMap);
                color2 *= color;
                vertOutput.worldPosition = unity_ObjectToWorld._m03_m13_m23 + mul(unity_ObjectToWorld, vertInput.vertex.xyz);
                // vertOutput.worldPosition = mul(unity_ObjectToWorld, vertInput.vertex.xyz);
                // vertOutput.worldPosition = vertInput.vertex.xyz;

                //float3 viewNorm = normalize(mul((float3x3)UNITY_MATRIX_IT_MV, vertInput.normal));
                //viewNorm = float3(-viewCross.y, viewCross.x, dot(viewNorm, -viewDir));

                //float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - unity_ObjectToWorld._m03_m13_m23); // vertOutput.worldPosition);
                //viewDirection = inverse(viewDirection);
                //vertOutput.worldPosition = mul(viewDirection, vertOutput.worldPosition);

                //float3 viewNorm = normalize(mul((float3x3)UNITY_MATRIX_IT_MV, vertInput.vertex.xyz));
                //vertOutput.worldPosition = mul(viewNorm, vertOutput.worldPosition);
                // vvertOutput.worldPosition = mul(unity_ObjectToWorld, vertInput.vertex.xyz);
                // vertOutput.worldPosition = mul(unity_CameraInvProjection, vertOutput.worldPosition);
                // vertOutput.worldPosition = mul(unity_CameraProjection, vertOutput.worldPosition);
                vertOutput.color = color2;
                return vertOutput;
            }

            fixed4 Fragment(VertOutput vertOutput) : SV_Target
            {
                float cutoffDistance = 0.6 * mapSize;
                fixed4 color2 = tex2D(_BaseMap, vertOutput.uv);
                color2 *= vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color2);
                // float3 mapPosition2 = unity_ObjectToWorld._m03_m13_m23 + mul(unity_ObjectToWorld, mapPosition);
                float3 distance = vertOutput.worldPosition - mapPosition;
                distance = rotateVector(mapRotation, distance);
                distance.x = abs(distance.x);
                distance.y = abs(distance.y);
                distance.z = abs(distance.z);
                if (!(distance.x <= cutoffDistance && distance.y <= cutoffDistance && distance.z <= cutoffDistance))
                {
                    color2.w = 0;
                }
                /*float dista3 = distance(vertOutput.worldPosition, mapPosition);
                if (dista3 >= cutoffDistance)
                {
                    color2.w = 0;
                }*/
                /*float dista1 = distance(float3(0, vertOutput.worldPosition.y, vertOutput.worldPosition.z), float3(0, mapPosition.y, mapPosition.z));
                float dista2 = distance(float3(vertOutput.worldPosition.x, vertOutput.worldPosition.y, 0), float3(mapPosition.x, mapPosition.y, 0));
                if (dista1 >= cutoffDistance && dista2 >= cutoffDistance)
                {
                    color2.w = 0;
                }*/

                /*if (dista >= 0.96 * (mapSize / 2))
                {
                    color2.x *= 0.1;
                    color2.y *= 0.1;
                    color2.z *= 0.1;
                }*/
                /*
                if (dista >= 1.0 * (mapSize / 2))
                {
                    color2.w = 0;
                }
                if (dista >= 0.96 * (mapSize / 2))
                {
                    color2.x *= 0.1;
                    color2.y *= 0.1;
                    color2.z *= 0.1;
                }*/
                return color2;
            }
            ENDCG
        }
    }
}