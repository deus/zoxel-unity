﻿Shader "zoxel/ui/map_part"
{
    Properties
    {
        _BaseMap ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
        }
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex Vertex
            #pragma fragment Fragment
            #pragma multi_compile_fog
            #include "UnityCG.cginc"
            sampler2D _BaseMap;
            float4 _BaseMap_ST;
            uniform float4 color;
            uniform float3 mapPosition;
            uniform float mapSize;

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 color : COLOR;
                float2 uv : TEXCOORD0;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                float2 uv : TEXCOORD0;
                float3 worldPosition : TEXCOORD2;
                UNITY_FOG_COORDS(1)
            };

            VertOutput Vertex(VertInput vertInput)
            {
                VertOutput vertOutput;
                float4 color2 = float4(vertInput.color, 1);
                vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex); // draws it using the bounds or matrix
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                vertOutput.uv = TRANSFORM_TEX(vertInput.uv, _BaseMap);
                color2 *= color;
                float3 baseWorldPos = unity_ObjectToWorld._m03_m13_m23;
                vertOutput.worldPosition = baseWorldPos + mul(unity_ObjectToWorld, vertInput.vertex.xyz);
                vertOutput.color = color2;
                return vertOutput;
            }

            fixed4 Fragment(VertOutput vertOutput) : SV_Target
            {
                fixed4 color2 = tex2D(_BaseMap, vertOutput.uv);
                color2 *= vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color2);
                float dista = distance(vertOutput.worldPosition, mapPosition);
                if (dista >= 0.78 * (mapSize / 2))
                {
                    color2.w = 0;
                }
                if (dista >= 0.74 * (mapSize / 2))
                {
                    color2.x *= 0.1;
                    color2.y *= 0.1;
                    color2.z *= 0.1;
                }
                return color2;
            }
            ENDCG
        }
    }
}