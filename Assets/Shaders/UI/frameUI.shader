﻿Shader "zoxel/ui/frameui"
{
    Properties
    {
        _BaseMap ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags { "Queue"="Transparent" }
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex Vertex
            #pragma fragment Fragment
            #pragma multi_compile_fog
		    #pragma target 4.5
            #include "UnityCG.cginc"

            sampler2D _BaseMap;
            float4 _BaseMap_ST;

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 color : COLOR;
                float2 uv : TEXCOORD0;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 frameColor : COLOR1;
            };
            
            StructuredBuffer<float4> colors;
            StructuredBuffer<float4> frameColors;

            VertOutput Vertex(VertInput vertInput, uint instanceID : SV_InstanceID)   //  SV_Vertex
            {
                VertOutput vertOutput;
                vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex); // draws it using the bounds or matrix
                vertOutput.uv = TRANSFORM_TEX(vertInput.uv, _BaseMap);
                vertOutput.color = colors[instanceID];
                vertOutput.frameColor = frameColors[instanceID];
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                return vertOutput;
            }

            fixed4 Fragment(VertOutput vertOutput) : SV_Target
            {
                fixed4 baseColor = tex2D(_BaseMap, vertOutput.uv);
                fixed4 color = fixed4(0, 0, 0, 1);
                if (baseColor.x == 0 && baseColor.y == 0 && baseColor.z == 0)
                {
                    color = vertOutput.frameColor;
                }
                else
                {
                    color.x = baseColor.x;
                    color.x *= vertOutput.color.x;
                    color.y = baseColor.y;
                    color.y *= vertOutput.color.y;
                    color.z = baseColor.z;
                    color.z *= vertOutput.color.z;
                    color.w = vertOutput.color.w;
                }
                color.x *= 0.5;
                color.y *= 0.5;
                color.z *= 0.5;
                color.w *= baseColor.w;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color);
                return color;
            }
            ENDCG
        }
    }
}