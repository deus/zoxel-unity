﻿Shader "zoxel/ui/baseui_unStructured"
{
    Properties
    {
        _BaseMap ("Texture", 2D) = "white" {}
    }
    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
        }
        // LOD 100
        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex Vertex
            #pragma fragment Fragment
            #pragma multi_compile_fog
            // #pragma multi_compile_instancing
		    // #pragma target 4.5
            #include "UnityCG.cginc"
            sampler2D _BaseMap;
            float4 _BaseMap_ST;
            uniform float4 color;

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 color : COLOR;
                float2 uv : TEXCOORD0;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };

            VertOutput Vertex(VertInput vertInput)
            {
                VertOutput vertOutput;
                float4 color2 = float4(vertInput.color, 1);
                vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex); // draws it using the bounds or matrix
                vertOutput.uv = TRANSFORM_TEX(vertInput.uv, _BaseMap);
                color2 *= color;
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                vertOutput.color = color2;
                return vertOutput;
            }

            fixed4 Fragment(VertOutput vertOutput) : SV_Target
            {
                fixed4 color2 = tex2D(_BaseMap, vertOutput.uv);
                color2 *= vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color2);
                return color2;
            }
            ENDCG
        }
    }
}