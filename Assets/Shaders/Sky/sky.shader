
Shader "zoxel/sky"
{
    Properties
    {
        skyColor ("Sky Color", Color) = (.1, .4, .8, 1)
        sunColor ("Sun Color", Color) = (.1, .4, .8, 1)
        skyDarkness ("Sky Darkness", Range(0.033, 1.0)) = 0.5
        sunSize ("Sun Size", Range(0.0, 1.0)) = 0.03
        lowestBrightness ("Lowest Brightness", Range(0.0, 1.0)) = 0.01
        // sunBrightness ("Sun Brightness", Range(0.0, 64.0)) = 4.0
        beltSize ("Belt Size", Range(0.0, 1.0)) = 0.01
        sunAmplitude ("Sun Amplitude", Range(0.0, 128.0)) = 2.0
        skyAmplitude ("Sky Amplitude", Range(0.0, 128.0)) = 2.0
    }

    SubShader
    {
        Tags { "Queue"="Background" "RenderType"="Background" "PreviewType"="Skybox" }
        Cull Off ZWrite Off
        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // #pragma target 4.5
            #include "UnityCG.cginc"
            float3 skyColor;
            float3 sunColor;
            float skyDarkness;
            float sunSize;
            float lowestBrightness;
            float beltSize;
            float sunAmplitude;
            float skyAmplitude;

            struct VertexInput
            {
                float4 vertex : POSITION;
                float3 uv : TEXCOORD0;
            };

            struct VertexOutput
            {
                float4 vertex : SV_POSITION;
                float3 uv : TEXCOORD0;
                float4 vertex2 : COLOR;
            };


            VertexOutput vert (VertexInput vertexInput)
            {
                VertexOutput vertexOutput;
                vertexOutput.vertex2 = vertexInput.vertex;
                vertexOutput.vertex = UnityObjectToClipPos(vertexInput.vertex);
                vertexOutput.uv = vertexInput.vertex.xyz;
                return vertexOutput;
            }
            
            float3x3 m = float3x3(      0.00,  0.80,  0.60,
                                        -0.80,  0.36, -0.48,
                                        -0.60, -0.48,  0.64 );

            float hash( float n )
            {
                return frac(sin(n) * 43758.5453);
            }

            float noise(in float3 x)
            {
                float3 p = floor(x);
                float3 f = frac(x);
                // f = f * f * (3.0 - 4.0 * f);
                f = f * f * (3.0 - 4.0 * f);
                float n = p.x + p.y * 57.0 + 113.0 * p.z;
                float res = lerp(lerp(lerp( hash(n +  0.0), hash(n +  1.0),f.x),
                                    lerp( hash(n + 57.0), hash(n + 58.0),f.x),f.y),
                                lerp(lerp( hash(n + 113.0), hash(n + 114.0),f.x),
                                    lerp( hash(n + 170.0), hash(n + 171.0),f.x),f.y),f.z);
                return res;
            }

            float fbm(float3 p)
            {
                float f = 0.0;
                f += 0.5000 * noise(p);
                p = mul(m, p) * 2.02;
                f += 0.2500 * noise(p);
                p = mul(m, p) * 2.03;
                f += 0.1250 * noise(p);
                p = mul(m, p) * 2.01;
                f += 0.0625 * noise(p);
                return f / 0.9375;
            }

            half3 frag (VertexOutput vertexOutput) : SV_Target
            {
                float2 uv = vertexOutput.uv;
                bool isSun = vertexOutput.vertex2.y >= 0.49 && abs(vertexOutput.vertex2.x) <= sunSize && abs(vertexOutput.vertex2.z) <= sunSize;
                if (uv.y < 0)
                {
                    uv.y = -uv.y;
                }
                if (uv.y < beltSize && uv.y >= 0)
                {
                    uv.y = beltSize; 
                }
                /*if (uv.y > -beltSize && uv.y < 0)
                {
                    uv.y = -beltSize;
                }*/
                /*float noiseValueR = noise(float3(uv.y, uv.y, uv.y));
                float noiseValueG = noise(float3(-uv.y, -uv.y, -uv.y));
                float noiseValueB = 1 - noise(float3(uv.y, uv.y, uv.y));
                float4 color = half4(noiseValueR, noiseValueG, noiseValueB, 1);*/
                // float noiseValue = 0.1 + (0.9 - 0.9 * noise(float3(uv.y, uv.y, uv.y)));
                float3 uvPosition = float3(uv.y, uv.y, uv.y);
                float3 uvPosition2 = float3(vertexOutput.vertex2.x, uv.y, vertexOutput.vertex2.z);
                float noiseValue = 0.1 + 0.9 * noise(uvPosition); //  + 0.5 * fbm(uvPosition);
                half3 color = half3(noiseValue, noiseValue, noiseValue);
                if (isSun)
                {
                    color *= sunAmplitude;
                    color.x *= sunColor.x * skyDarkness;
                    color.y *= sunColor.y * skyDarkness;
                    color.z *= sunColor.z * skyDarkness;
                }
                else
                {
                    color *= skyAmplitude;
                    color.x *= skyColor.x;
                    color.y *= skyColor.y;
                    color.z *= skyColor.z;
                }
                color.x *= skyDarkness;
                color.y *= skyDarkness;
                color.z *= skyDarkness;
                if (color.x < lowestBrightness)
                {
                    color.x = lowestBrightness;
                }
                if (color.y < lowestBrightness)
                {
                    color.y = lowestBrightness;
                }
                if (color.z < lowestBrightness)
                {
                    color.z = lowestBrightness;
                }
                /*const float brightestBrightness = 1.0;
                if (color.x > brightestBrightness)
                {
                    color.x = brightestBrightness;
                }
                if (color.y > brightestBrightness)
                {
                    color.y = brightestBrightness;
                }
                if (color.z > brightestBrightness)
                {
                    color.z = brightestBrightness;
                }*/

                // debug sun
                /*if (isSun)
                {
                    color.x = 0;
                    color.y = 0.5;
                    color.z = 1;
                }*/
                return color;
                /*half3 c = DecodeHDR (tex, _Tex_HDR);
                c = c * _Tint.rgb * unity_ColorSpaceDouble.rgb;
                c *= _Exposure;
                return half4(c, 1);*/
            }

            /*float3 RotateAroundYInDegrees(float3 vertex, float degrees)
            {
                float alpha = degrees * UNITY_PI / 180.0;
                float sina, cosa;
                sincos(alpha, sina, cosa);
                float2x2 m = float2x2(cosa, -sina, sina, cosa);
                return float3(mul(m, vertex.xz), vertex.y).xzy;
            }*/
            ENDCG
        }
    }
    Fallback Off
}