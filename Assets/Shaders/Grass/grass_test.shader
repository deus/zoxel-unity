﻿Shader "zoxel/grass_test"
{
    SubShader
    {              
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing 
            #pragma multi_compile_fog
		    #pragma target 4.5
            #include "UnityCG.cginc" 
            uniform StructuredBuffer<float3> positions;
            uniform StructuredBuffer<float4> rotations;
            uniform StructuredBuffer<float> scales;
            uniform StructuredBuffer<float3> colors;
            uniform StructuredBuffer<float3> wind_directions;

            struct VertInput
            {
                float4 vertex : POSITION;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float3 color : COLOR;
                UNITY_FOG_COORDS(1)
            };

            VertOutput vert(VertInput vertInput, uint instanceID : SV_InstanceID)   //  SV_Vertex
            {
                VertOutput vertOutput;
                float3 meshPosition = positions[instanceID];
                float4 rotation = rotations[instanceID];
                float scale = scales[instanceID];
                vertOutput.vertex = UnityObjectToClipPos(meshPosition + vertInput.vertex.xyz);
                vertOutput.color = colors[instanceID];
                return vertOutput;
            }

            float3 frag (VertOutput vertOutput) : SV_Target
            {
                return vertOutput.color;
            }
            ENDCG
        }
    }
}