﻿Shader "zoxel/grass_outline"
{
    Properties
    {
		_OutlineWidth ("Outline Width", Range (0, 1)) = 0.1
        _speed ("Speed", Float) = 1.0   
        _windPower ("Wind Power", Float) = 0.08
        _voxelSize ("VoxelSize", Float) = 16.0   
    }
    SubShader
    {
        // First pass is outline     
        Pass
        {
            Tags
            {
                "RenderType" = "Opaque"
                "RenderPipeline" = "UniversalPipeline"
                "LightMode" = "UniversalForward"
            }
            LOD 100
            // ZWrite Off
            Cull Front

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing 
            #pragma multi_compile_fog
		    // #pragma target 4.5
            #include "UnityCG.cginc"
            uniform StructuredBuffer<float3> positions;
            uniform StructuredBuffer<float4> rotations;
            uniform StructuredBuffer<float> scales;
            uniform StructuredBuffer<float4> outline_colors;
            uniform StructuredBuffer<float3> wind_directions;
            uniform float _OutlineWidth;
            uniform float _speed;
            uniform float _windPower;
            uniform float _voxelSize;

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                UNITY_FOG_COORDS(1)
            };

            float4 multiplyRotation(float4 q1, float4 q2)
            { 
                float4 qr;
                qr.x = (q1.w * q2.x) + (q1.x * q2.w) + (q1.y * q2.z) - (q1.z * q2.y);
                qr.y = (q1.w * q2.y) - (q1.x * q2.z) + (q1.y * q2.w) + (q1.z * q2.x);
                qr.z = (q1.w * q2.z) + (q1.x * q2.y) - (q1.y * q2.x) + (q1.z * q2.w);
                qr.w = (q1.w * q2.w) - (q1.x * q2.x) - (q1.y * q2.y) - (q1.z * q2.z);
                return qr;
            }
            
            float4 conjugateRotation(float4 rotation)
            { 
                return float4(-rotation.x, -rotation.y, -rotation.z, rotation.w); 
            }

            float4 inverseRotation(float4 q)
            {
                float sqr = sqrt(q.x*q.x + q.y*q.y + q.z*q.z + q.w*q.w);
                return conjugateRotation(q) / sqr;
            }
            
            float3 rotateVector(float4 rotation, float3 vertex3)
            {
                float4 vertex = float4(vertex3, 0.0);
                rotation = inverseRotation(rotation);
                return multiplyRotation(conjugateRotation(rotation), multiplyRotation(vertex, rotation)).xyz;
            }

            VertOutput vert(VertInput vertInput, uint instanceID : SV_InstanceID)   //  SV_Vertex
            {
                // = _Time.y;
                //float time2 = _Time.z;
                float voxelScale = 1 / _voxelSize;
                VertOutput vertOutput;
                float3 meshPosition = positions[instanceID];
                float4 rotation = rotations[instanceID];
                float scale = scales[instanceID];
                float3 meshVertex = vertInput.vertex.xyz;
                // meshVertex.y -= 1.0; // 0.5; // _voxelSize / 2.0; // 16
                meshVertex.y += 4 / _voxelSize; // 16
                meshVertex.x *= (1 + _OutlineWidth);
                meshVertex.z *= (1 + _OutlineWidth);
                if (meshVertex.y >= 0.0) // = 0.1)
                {
                    float3 direction = wind_directions[instanceID];
                    meshVertex.y *= scale;
                    meshVertex.x += _windPower * ((1 + sin(_Time.x * _speed) / 2.0) * direction.x);
                    meshVertex.y += _windPower * ((1 + sin(_Time.x * _speed) / 2.0) * direction.y);
                    meshVertex.z += _windPower * ((1 + sin(_Time.x * _speed) / 2.0) * direction.z);
                    meshVertex.y += voxelScale * _OutlineWidth * 0.4;
                    // meshVertex.y *= (1 + _OutlineWidth);
                }
                else
                {
                    meshVertex.y -= voxelScale * _OutlineWidth;
                }
                vertOutput.vertex = UnityObjectToClipPos(meshPosition + rotateVector(rotation, meshVertex));
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                vertOutput.color = outline_colors[instanceID]; // * 0.5;
                return vertOutput;
            }

            float4 frag (VertOutput vertOutput) : SV_Target
            {
                float4 color = vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color);
                return color;
            }
            ENDCG
        }
        
        // 2nd pass is base color
        Pass
        {
            Tags
            {
                "RenderType" = "Opaque"
                "RenderPipeline" = "UniversalPipeline"
            }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing 
            #pragma multi_compile_fog
		    // #pragma target 4.5
            #include "UnityCG.cginc" 
            uniform StructuredBuffer<float3> positions;
            uniform StructuredBuffer<float4> rotations;
            uniform StructuredBuffer<float> scales;
            uniform StructuredBuffer<float3> colors;
            uniform StructuredBuffer<float3> wind_directions;
            uniform float _speed;
            uniform float _windPower;
            uniform float _voxelSize;

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float3 color : COLOR;
                UNITY_FOG_COORDS(1)
            };

            float4 multiplyRotation(float4 q1, float4 q2)
            { 
                float4 qr;
                qr.x = (q1.w * q2.x) + (q1.x * q2.w) + (q1.y * q2.z) - (q1.z * q2.y);
                qr.y = (q1.w * q2.y) - (q1.x * q2.z) + (q1.y * q2.w) + (q1.z * q2.x);
                qr.z = (q1.w * q2.z) + (q1.x * q2.y) - (q1.y * q2.x) + (q1.z * q2.w);
                qr.w = (q1.w * q2.w) - (q1.x * q2.x) - (q1.y * q2.y) - (q1.z * q2.z);
                return qr;
            }
            
            float4 conjugateRotation(float4 rotation)
            { 
                return float4(-rotation.x, -rotation.y, -rotation.z, rotation.w); 
            }

            float4 inverseRotation(float4 q)
            {
                float sqr = sqrt(q.x*q.x + q.y*q.y + q.z*q.z + q.w*q.w);
                return conjugateRotation(q) / sqr;
            }
            
            float3 rotateVector(float4 rotation, float3 vertex3)
            {
                float4 vertex = float4(vertex3, 0.0);
                rotation = inverseRotation(rotation);
                return multiplyRotation(conjugateRotation(rotation), multiplyRotation(vertex, rotation)).xyz;
            }

            VertOutput vert(VertInput vertInput, uint instanceID : SV_InstanceID)   //  SV_Vertex
            {
                VertOutput vertOutput;
                // float voxelScale = 1 / _voxelSize;
                float3 meshPosition = positions[instanceID];
                float4 rotation = rotations[instanceID];
                float scale = scales[instanceID];
                float3 meshVertex = vertInput.vertex.xyz;
                meshVertex.y += 4 / _voxelSize; // 16
                if (meshVertex.y >= 0)
                {
                    float3 direction = wind_directions[instanceID];
                    meshVertex.y *= scale;
                    meshVertex.x += _windPower * ((1 + sin(_Time.x * _speed) / 2.0) * direction.x);
                    meshVertex.y += _windPower * ((1 + sin(_Time.x * _speed) / 2.0) * direction.y);
                    meshVertex.z += _windPower * ((1 + sin(_Time.x * _speed) / 2.0) * direction.z);
                }
                vertOutput.vertex = UnityObjectToClipPos(meshPosition + rotateVector(rotation, meshVertex));
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                float3 color = colors[instanceID]; // * 0.5;                  // float4(v.color, 1);
                /*if (vertInput.normal.z == 1)
                {
                    color.x *= 0.58;
                    color.y *= 0.58;
                    color.z *= 0.58;
                }
                else if (vertInput.normal.z == -1)
                {
                    color.x *= 0.72;
                    color.y *= 0.72;
                    color.z *= 0.72;
                }
                else if (vertInput.normal.x == 1)
                {
                    color.x *= 0.86;
                    color.y *= 0.86;
                    color.z *= 0.86;
                }*/

                /*color.x *= 0.8;
                color.y *= 0.8;
                color.z *= 0.8;*/
                vertOutput.color = color;
                //vertOutput.color = colors[instanceID];
                return vertOutput;
            }

            float3 frag(VertOutput vertOutput) : SV_Target
            {
                // float3(0.044, 0.038, 0.06); // 
                float3 color = vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color);
                return color;
            }
            ENDCG
        }
    }
}
//float time = _Time.y;
//float time2 = _Time.z;