﻿Shader "zoxel/grass"
{
    Properties
    {
        _speed ("Speed", Float) = 1.0   
        _windPower ("Wind Power", Float) = 0.08
        // _offsetX ("OffsetX",Float) = 0.0
        // _offsetY ("OffsetY",Float) = 0.0      
        // _octaves ("Octaves",Int) = 7
        // _lacunarity("Lacunarity", Range( 1.0 , 5.0)) = 2
        // _gain("Gain", Range( 0.0 , 1.0)) = 0.5
        // _value("Value", Range( -2.0 , 2.0)) = 0.0
        // _amplitude("Amplitude", Range( 0.0 , 5.0)) = 1.5
        // _frequency("Frequency", Range( 0.0 , 6.0)) = 2.0
        // _power("Power", Range( 0.1 , 5.0)) = 1.0
        // _scale("Scale", Float) = 1.0
        // _color ("Color", Color) = (1.0,1.0,1.0,1.0)      
        // [Toggle] _monochromatic("Monochromatic", Float) = 0
        // _range("Monochromatic Range", Range( 0.0 , 1.0)) = 0.5      
    }
    SubShader
    {              
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing 
            #pragma multi_compile_fog
		    #pragma target 4.5
            #include "UnityCG.cginc" 
            uniform StructuredBuffer<float3> positions;
            uniform StructuredBuffer<float4> rotations;
            uniform StructuredBuffer<float> scales;
            uniform StructuredBuffer<float3> colors;
            uniform StructuredBuffer<float3> wind_directions;

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float3 color : COLOR;
                //float3 meshPosition : NORMAL;
                // float3 normal : NORMAL;
                UNITY_FOG_COORDS(1)
            };

            uniform float _speed;
            uniform float _windPower;
 
            // float _octaves,_lacunarity,_gain,_value,_amplitude,_frequency, _offsetX, _offsetY, _power, _scale, _monochromatic, _range;
            // float4 _color;
           
            /*float fbm(float2 p)
            {
                p = p * _scale + float2(_offsetX,_offsetY);
                for( int i = 0; i < _octaves; i++ )
                {
                    float2 i = floor( p * _frequency );
                    float2 f = frac( p * _frequency );      
                    float2 t = f * f * f * ( f * ( f * 6.0 - 15.0 ) + 10.0 );
                    float2 a = i + float2( 0.0, 0.0 );
                    float2 b = i + float2( 1.0, 0.0 );
                    float2 c = i + float2( 0.0, 1.0 );
                    float2 d = i + float2( 1.0, 1.0 );
                    a = -1.0 + 2.0 * frac( sin( float2( dot( a, float2( 127.1, 311.7 ) ),dot( a, float2( 269.5,183.3 ) ) ) ) * 43758.5453123 );
                    b = -1.0 + 2.0 * frac( sin( float2( dot( b, float2( 127.1, 311.7 ) ),dot( b, float2( 269.5,183.3 ) ) ) ) * 43758.5453123 );
                    c = -1.0 + 2.0 * frac( sin( float2( dot( c, float2( 127.1, 311.7 ) ),dot( c, float2( 269.5,183.3 ) ) ) ) * 43758.5453123 );
                    d = -1.0 + 2.0 * frac( sin( float2( dot( d, float2( 127.1, 311.7 ) ),dot( d, float2( 269.5,183.3 ) ) ) ) * 43758.5453123 );
                    float A = dot( a, f - float2( 0.0, 0.0 ) );
                    float B = dot( b, f - float2( 1.0, 0.0 ) );
                    float C = dot( c, f - float2( 0.0, 1.0 ) );
                    float D = dot( d, f - float2( 1.0, 1.0 ) );
                    float noise = ( lerp( lerp( A, B, t.x ), lerp( C, D, t.x ), t.y ) );              
                    _value += _amplitude * noise;
                    _frequency *= _lacunarity;
                    _amplitude *= _gain;
                }
                _value = clamp( _value, -1.0, 1.0 );
                return pow(_value * 0.5 + 0.5,_power);
            }*/

            float4 multiplyRotation(float4 q1, float4 q2)
            { 
                float4 qr;
                qr.x = (q1.w * q2.x) + (q1.x * q2.w) + (q1.y * q2.z) - (q1.z * q2.y);
                qr.y = (q1.w * q2.y) - (q1.x * q2.z) + (q1.y * q2.w) + (q1.z * q2.x);
                qr.z = (q1.w * q2.z) + (q1.x * q2.y) - (q1.y * q2.x) + (q1.z * q2.w);
                qr.w = (q1.w * q2.w) - (q1.x * q2.x) - (q1.y * q2.y) - (q1.z * q2.z);
                return qr;
            }
            
            float4 conjugateRotation(float4 rotation)
            { 
                return float4(-rotation.x, -rotation.y, -rotation.z, rotation.w); 
            }

            float4 inverseRotation(float4 q)
            {
                float sqr = sqrt(q.x*q.x + q.y*q.y + q.z*q.z + q.w*q.w);
                return conjugateRotation(q) / sqr;
            }
            
            float3 rotateVector(float4 rotation, float3 vertex3)
            {
                float4 vertex = float4(vertex3, 0.0);
                rotation = inverseRotation(rotation);
                return multiplyRotation(conjugateRotation(rotation), multiplyRotation(vertex, rotation)).xyz;
            }

            VertOutput vert(VertInput vertInput, uint instanceID : SV_InstanceID)   //  SV_Vertex
            {
                //float time = _Time.y;
                //float time2 = _Time.z;
                VertOutput vertOutput;
                float3 meshPosition = positions[instanceID];
                float4 rotation = rotations[instanceID];
                float scale = scales[instanceID];
                float3 meshVertex = vertInput.vertex.xyz;
                if (meshVertex.y >= 0.1)
                {
                    float3 direction = wind_directions[instanceID];
                    meshVertex.y *= scale;
                    meshVertex.x += _windPower * ((1 + sin(_Time.x * _speed) / 2.0) * direction.x);
                    meshVertex.y += _windPower * ((1 + sin(_Time.x * _speed) / 2.0) * direction.y);
                    meshVertex.z += _windPower * ((1 + sin(_Time.x * _speed) / 2.0) * direction.z);

                    // meshVertex.y *= scale;
                    // float windPower = (2 / 16.0);
                    // //float2 vertexNoiseInput = float2((int)meshVertex.x, (int)meshVertex.z);
                    // //float height = 0.6 * fbm(16 * vertexNoiseInput);
                    // //meshVertex.y *= height;
                    // // meshVertex.y += (1 / 16.0) * sin(time);
                    // float noiseInputX = time; // 1 * sin(time * 1);
                    // float noiseInputY = time2; // 1 * sin(time * 1);
                    // float noisePower = sin(time); //  + fbm(float2(noiseInputX, noiseInputY));
                    // // float noiseX = (4 / 16.0) * noisePower;  // sin(time * 0.05) * 
                    // // float noiseZ = 8 * fbm(float2(noiseInputY, noiseInputX));
                    // float3 direction = wind_directions[instanceID];
                    // meshVertex.x += windPower * sin(_Time.x) ; // sin(time);
                    // meshVertex.y += windPower * sin(_Time.y) ; // sin(time);
                    // meshVertex.z += windPower * sin(_Time.z) ; // sin(time);
                    // //meshVertex.x += noiseX; // sin(time);
                    // //meshVertex.z += noiseX; // sin(time);
                }
                else
                {
                    meshVertex.y -= (1 / 16.0);
                }
                vertOutput.vertex = UnityObjectToClipPos(meshPosition + rotateVector(rotation, meshVertex));
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                float3 color = colors[instanceID];                  // float4(v.color, 1);
                if (vertInput.normal.z == 1)
                {
                    color.x *= 0.58;
                    color.y *= 0.58;
                    color.z *= 0.58;
                }
                else if (vertInput.normal.z == -1)
                {
                    color.x *= 0.72;
                    color.y *= 0.72;
                    color.z *= 0.72;
                }
                else if (vertInput.normal.x == 1)
                {
                    color.x *= 0.86;
                    color.y *= 0.86;
                    color.z *= 0.86;
                }
                vertOutput.color = color;                           // float4(color.x, color.y, color.z, 1); //  * 0.5;
                return vertOutput;
            }

            float3 frag (VertOutput vertOutput) : SV_Target
            {
                float3 color = vertOutput.color;
                /*color.x *= 0.1;
                color.y *= 0.3;
                color.z *= 0.1;*/

                /*float originalMeshVertexY = vertOutput.vertex.y - vertOutput.meshPosition.y;
                int vertPositionY = ((int)(originalMeshVertexY * 16));
                color.x *= vertPositionY / 512.0;
                color.y *= vertPositionY / 512.0;
                color.z *= vertPositionY / 512.0;*/
                UNITY_APPLY_FOG(vertOutput.fogCoord, color);
                return color;
            }
            ENDCG
        }
    }
}