Shader "zoxel2D/character2D"
{
    Properties
    {
        _BaseMap ("Texture", 2D) = "white" { }
        _Index ("_Index", Int) = 0
        _Direction ("_Direction", Int) = 1
        _IsTilemap ("_IsTilemap", Int) = 0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }

        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing

		    #pragma target 4.5

            #include "UnityCG.cginc"

            sampler2D _BaseMap;
            float4 _BaseMap_ST;
            int _Index;
            int _IsTilemap;
            int _Direction;

            struct VertInput
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
            };

            VertOutput vert(VertInput vertInput)
            {
                VertOutput vertOutput;
                vertOutput.vertex = UnityObjectToClipPos(vertInput.vertex);
                vertOutput.uv = TRANSFORM_TEX(vertInput.uv, _BaseMap);
                if (_IsTilemap == 1)
                {
                    vertOutput.uv *= 0.25;
                    vertOutput.uv.y += 0.75; // top row
                    if (_Index == 1)
                    {
                        vertOutput.uv.x += 0.25;
                    }
                    else if (_Index == 2)
                    {
                        vertOutput.uv.x += 0.5;
                        vertOutput.vertex += float4(_Direction * 0.1, -0.1, 0, 0);
                    }
                    else if (_Index == 3)
                    {
                        vertOutput.uv.x += 0.75;
                    }
                }
                return vertOutput;
            }

            fixed4 frag(VertOutput vertOutput) : SV_Target
            {
                // vertOutput.uv = -vertOutput.uv;
                fixed4 color = tex2D(_BaseMap, vertOutput.uv);
                return color;
            }
            ENDCG
        }
    }
}