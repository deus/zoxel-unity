﻿Shader "zoxel/items/worldvoxel"
{
    Properties
    {
        _BaseMap ("Texture", 2D) = "white" {}
        _TilesRowCount ("Tiles Row Count", Int) = 13
    }
    SubShader
    {              
        Tags { "RenderType" = "Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing 
            #pragma multi_compile_fog
		    #pragma target 4.5
            #include "UnityCG.cginc"
            sampler2D _BaseMap;
            float4 _BaseMap_ST;
            int _TilesRowCount;
            uniform StructuredBuffer<float3> positions;
            uniform StructuredBuffer<float4> rotations;
            uniform StructuredBuffer<float> scales;
            uniform StructuredBuffer<float3> colors;
            uniform StructuredBuffer<float> tileIndexes; // use this to get uvvs

            struct VertInput
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float3 color : COLOR;
                UNITY_FOG_COORDS(1)
            };

            float4 multiplyRotation(float4 q1, float4 q2)
            { 
                float4 qr;
                qr.x = (q1.w * q2.x) + (q1.x * q2.w) + (q1.y * q2.z) - (q1.z * q2.y);
                qr.y = (q1.w * q2.y) - (q1.x * q2.z) + (q1.y * q2.w) + (q1.z * q2.x);
                qr.z = (q1.w * q2.z) + (q1.x * q2.y) - (q1.y * q2.x) + (q1.z * q2.w);
                qr.w = (q1.w * q2.w) - (q1.x * q2.x) - (q1.y * q2.y) - (q1.z * q2.z);
                return qr;
            }
            
            float4 conjugateRotation(float4 rotation)
            { 
                return float4(-rotation.x, -rotation.y, -rotation.z, rotation.w); 
            }

            float4 inverseRotation(float4 q)
            {
                float sqr = sqrt(q.x*q.x + q.y*q.y + q.z*q.z + q.w*q.w);
                return conjugateRotation(q) / sqr;
            }
            
            float3 rotateVector(float4 rotation, float3 vertex3)
            {
                float4 vertex = float4(vertex3, 0.0);
                rotation = inverseRotation(rotation);
                return multiplyRotation(conjugateRotation(rotation), multiplyRotation(vertex, rotation)).xyz;
            }

            VertOutput vert(VertInput vertInput, uint instanceID : SV_InstanceID)   //  SV_Vertex
            {
                VertOutput vertOutput;
                float3 meshPosition = positions[instanceID];
                float4 rotation = rotations[instanceID];  // 
                float scale = scales[instanceID];
                float3 meshVertex = scale * vertInput.vertex.xyz;
                // float3 meshVertex = rotateVector(rotation, scale * vertInput.vertex.xyz);
                float3 rotatedMeshVertex = rotateVector(rotation, meshVertex);
                float4 vertex = UnityObjectToClipPos(meshPosition + rotatedMeshVertex);
                vertOutput.vertex = vertex;
                // calculate uvs based on tile index
                int tileIndex = tileIndexes[instanceID]; // float4(v.color, 1);
                int tileWidth2 = _TilesRowCount; // 13;
                float tileWidth = (float) tileWidth2; // 4.0;
                float uvWidth = 1 / tileWidth;
                float2 inputUV = vertInput.uv * uvWidth;
                float positionX = tileIndex - (tileWidth2 * floor(tileIndex / tileWidth)); // modf(tileIndex, tileWidth2); // (tileIndex % tileWidth2); // iter_mod(tileIndex, tileWidth2);
                float positionY =  floor(tileIndex / tileWidth); // divf(tileIndex, tileWidth2); // (tileIndex % tileWidth2); // iter_mod(tileIndex, tileWidth2);
                inputUV.x += uvWidth * positionX; // (tileIndex % tileWidth2);
                inputUV.y += uvWidth * positionY;
                //inputUV.x *= positionX; // (tileIndex % tileWidth2);
                //inputUV.y *= uvWidth * positionY;

                vertOutput.uv = TRANSFORM_TEX(inputUV, _BaseMap);
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                float3 color = colors[instanceID]; // float4(v.color, 1);
                vertOutput.color = color; //  * 0.5;
                return vertOutput;
            }

            half4 frag (VertOutput vertOutput) : SV_Target
            {
                half4 color = tex2D(_BaseMap, vertOutput.uv);
                color.x *= vertOutput.color.x;
                color.y *= vertOutput.color.y;
                color.z *= vertOutput.color.z;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color);
                return color;
            }
            ENDCG
        }
    }
}