﻿Shader "zoxel/character_unstructured"
{
    SubShader
    {              
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            //Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing 
            #pragma multi_compile_fog
            // #pragma instancing_options // procedural:setup
		    // #pragma target 4.5
            #include "UnityCG.cginc"
            //#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
            uniform float4 color; // : register(t1);
            uniform float4 additionColor; // : register(t2);
            //#endif

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 color : COLOR;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                UNITY_FOG_COORDS(1)
            };

            VertOutput vert(VertInput v)
            {
                VertOutput vertOutput;
                float4 color2 = float4(v.color, 1);
                // float4 color = float4(1, 1, 1, 1);
                vertOutput.vertex = UnityObjectToClipPos(v.vertex); // draws it using the bounds or matrix
                //#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
                color2 *= color;
                color2 += additionColor;
                // color2.x *= 0.5;
                // color2.y *= 0.5;
                // color2.z *= 0.5;
                //#endif
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                vertOutput.color = color2;
                return vertOutput;
            }

            fixed4 frag (VertOutput vertOutput) : SV_Target
            {
                float4 color2 = vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color2);
                return color2;
            }
            ENDCG
        }
    }
}

//float3 position;
//vertOutput.instanceID = v.instanceID;
//float4 color = float4(vertOutput.color.x, vertOutput.color.y, vertOutput.color.z, vertOutput.color.w);
//return color;
//#if defined(UNITY_PROCEDURAL_INSTANCING_ENABLED)
//float3 vertex = v.vertex; //pointer.vertex; //  v.vertex
//float3 worldPosition = instanceData.position;
//vertOutput.vertex = mul(UNITY_MATRIX_VP, float4(vertex + worldPosition, 1.0f));
//vertOutput.color = v.color; // pointer.color.xyz; // float3(pointer.color.x, pointer.color.y, pointer.color.z);
//vertOutput.color = float3(1, 0, 0);
//#else
//vertOutput.vertex = float4(0, 0, 0, 0);
//vertOutput.color = float3(1, 0, 0);
//#endif
//vertOutput.vertex = UnityObjectToClipPos(v.vertex); // draws it using the bounds
//o.color = v.color;
//VertInput v, uint instanceID : SV_InstanceID)
//  UnityObjectToClipPos(v.vertex);
//UNITY_FOG_COORDS(1)

//position = float3(2, 0, 0); // _vertexes[unity_InstanceID];
// // sample the texture
// fixed4 col = tex2D(_MainTex, i.uv);
// // apply fog
// UNITY_APPLY_FOG(i.fogCoord, col);
// return col;
// #if defined(UNITY_PROCEDURAL_INSTANCING_ENABLED)
// return float4(1, 0, 0, 1);
// #else
// return float4(0, 0.5, 0.5, 1);
// #endif
// void setup()
// {
// #ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
//     float3 data = vertexes[unity_InstanceID];
//     float size = 1;
//     unity_ObjectToWorld._11_21_31_41 = float4(size, 0, 0, 0);
//     unity_ObjectToWorld._12_22_32_42 = float4(0, size, 0, 0);
//     unity_ObjectToWorld._13_23_33_43 = float4(0, 0, size, 0);
//     unity_ObjectToWorld._14_24_34_44 = float4(data, 1);
//     unity_WorldToObject = unity_ObjectToWorld;
//     unity_WorldToObject._14_24_34 *= -1;
//     unity_WorldToObject._11_22_33 = 1.0f / unity_WorldToObject._11_22_33;
// #endif
// }
//position = float3(2, 0, 0); // _vertexes[unity_InstanceID];
//o.vertex = UnityObjectToClipPos();
//o.vertex = v.vertex + float4(3, 0, 0, 0);
//float3 position = _vertexes[unity_InstanceID];
//unity_ObjectToWorld = 0.0;
//unity_ObjectToWorld._m03_m13_m23_m33 = float4(position, 1.0);
//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
//UNITY_TRANSFER_FOG(o,o.vertex);