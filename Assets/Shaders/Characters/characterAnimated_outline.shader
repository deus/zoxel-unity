﻿Shader "zoxel/characterAnimated_outline"
{
    Properties
    {
		outlineWidth ("Outline Width", Range (0, 1)) = 0.1
    }
    SubShader
    {
        Pass
        {
            Tags
            {
                "RenderType" = "Opaque"
                "RenderPipeline" = "UniversalPipeline"
            }
            LOD 100
            //Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog
            // #pragma multi_compile_instancing
		    #pragma target 4.5
            #include "UnityCG.cginc"
            uniform float4 color;
            uniform float4 additionColor;
            // Write out a float4x4 for 24 bonez, current and original
            // float4x4 matrices[10]; // matrices0, matrices1, etc
            uniform StructuredBuffer<float4x4> currentBones;
            uniform StructuredBuffer<float4x4> originalBones;
            uniform StructuredBuffer<uint> boneIndexes; // move thiz to VertInput

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 color : COLOR;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                UNITY_FOG_COORDS(1)
            };

            VertOutput vert(VertInput v, uint id : SV_VertexID)   //  SV_Vertex
            {
                VertOutput vertOutput;
                float4 color2 = float4(v.color, 1);
                color2 *= color;
                color2 += additionColor;
                vertOutput.color = color2;
                float3 vertex = v.vertex.xyz;
                // For each bone index, set from a bone
                uint index = boneIndexes[id];
                float4x4 originalInverseMatrix = originalBones[index];
                float4x4 currentMatrix = currentBones[index];
                float4 vertex4 = float4(vertex, 1);
                // draws it using the bounds or matrix suppled by Graphics class
                float4 boneVertex = mul(currentMatrix, mul(originalInverseMatrix, vertex4));
                vertOutput.vertex = UnityObjectToClipPos(boneVertex);
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                return vertOutput;
            }

            fixed4 frag (VertOutput vertOutput) : SV_Target
            {
                float4 color2 = vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color2);
                return color2;
            }
            ENDCG
        }

        // Outline Pass
        Pass
        {
            Tags
            {
                "RenderType" = "Opaque"
                "RenderPipeline" = "UniversalPipeline"
                "LightMode" = "UniversalForward"
            }
            LOD 100
            Cull Front
            // ZTest [_ZTest]

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fog
            // #pragma multi_compile_instancing
		    #pragma target 4.5
            #include "UnityCG.cginc"
            uniform float outlineWidth;
            uniform float4 outlineColor;
            uniform StructuredBuffer<float4x4> currentBones;
            uniform StructuredBuffer<float4x4> originalBones;
            uniform StructuredBuffer<uint> boneIndexes;

            struct VertInput
            {
                float3 vertex : POSITION;
                float3 normal : NORMAL;
                float3 color : COLOR;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                UNITY_FOG_COORDS(1)
            };

            VertOutput vert(VertInput vertInput, uint id : SV_VertexID)   //  SV_Vertex
            {
                VertOutput vertOutput;
                //float4x4 originalInverseMatrix = float4x4(); // originalBones[index];
                //float4x4 currentMatrix = float4x4(); // currentBones[index];
                uint index = boneIndexes[id];
                float4x4 originalInverseMatrix = originalBones[index];
                float4x4 currentMatrix = currentBones[index];
                float4 normal = float4(vertInput.normal, 1);
                normal = mul(currentMatrix, mul(originalInverseMatrix, normal));
                float4 vertex4 = float4(vertInput.vertex, 1);
                vertex4 = mul(currentMatrix, mul(originalInverseMatrix, vertex4));
                // vertex4 = vertex4 ;
                // draws it using the bounds or matrix suppled by Graphics class
                float4 boneVertex = vertex4 + normal * outlineWidth;
                vertOutput.vertex = UnityObjectToClipPos(boneVertex);
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                vertOutput.color = outlineColor;
                return vertOutput;
            }

            fixed4 frag (VertOutput vertOutput) : SV_Target
            {
                float4 color2 = vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color2);
                return color2;
            }
            ENDCG
        }
    }
}
                //float4 color2 = float4(vertInput.color, 1);
                //color2 *= color;
                //color2 += additionColor;
                //vertOutput.color = color2;
                //uint index = boneIndexes[id];
                //float4x4 originalInverseMatrix = originalBones[index];
                //float4x4 currentMatrix = currentBones[index];

                //boneVertex.x *= (1 + outlineWIdth);
                //boneVertex.y *= (1 + outlineWIdth);
                //boneVertex.z *= (1 + outlineWIdth);