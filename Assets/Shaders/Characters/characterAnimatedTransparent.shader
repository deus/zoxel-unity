﻿Shader "zoxel/characterAnimatedTransparent"
{
    SubShader
    {              
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
        //Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha

            CGPROGRAM
            #pragma vertex VertexProgram
            #pragma fragment FragmentProgram
            #pragma multi_compile_fog
            //#pragma multi_compile_instancing

		    #pragma target 4.5

            #include "UnityCG.cginc"
            // just one of these
            struct CharacterInstanceData
            {
                float4 baseColor;
                float4 addColor;
            };

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 color : COLOR;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                UNITY_FOG_COORDS(1)
            };

            //#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
            uniform StructuredBuffer<float4> colors; // : register(t1);
            uniform StructuredBuffer<float4> additionColors; // : register(t2);
            uniform StructuredBuffer<float4x4> currentBones; // : register(t2);
            uniform StructuredBuffer<float4x4> originalBones; // : register(t3);
            uniform StructuredBuffer<uint> boneIndexes; // : register(t4);
            //#endif

            VertOutput VertexProgram(VertInput v, uint id : SV_VertexID, uint instanceID : SV_InstanceID)   //  SV_Vertex
            {
                //CharacterInstanceData instanceData = datas[instanceID];
                VertOutput vertOutput;
                uint index = boneIndexes[id];

                float4 color = float4(v.color, 1);
                //color *= instanceData.baseColor;
                //color += instanceData.addColor;
                color *= colors[instanceID];
                color += additionColors[instanceID];
                vertOutput.color = color;
                // get bone index
                float3 vertex = v.vertex.xyz;
                float4x4 originalInverseMatrix = originalBones[index];
                float4x4 currentMatrix = currentBones[index];
                float4 vertex4 = float4(vertex, 1);
                float4 vertex42 = mul(currentMatrix, mul(originalInverseMatrix, vertex4));
                vertOutput.vertex = UnityObjectToClipPos(vertex42); // draws it using the bounds or matrix

                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                return vertOutput;
            }

            fixed4 FragmentProgram(VertOutput vertOutput) : SV_Target
            {
                float4 color = vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color);
                return color;
            }
            ENDCG
        }
    }
}

//float4 vertex4 = float4(vertex, 1);
//vertex = mul(mul(currentMatrix, originalInverseMatrix), vertex);    // matrix transform
//vertex = mul(vertex, mul(originalInverseMatrix, currentMatrix));    // matrix transform
//index = 11;
//index = 0;

// if (index == 0)
// {
//     color = float4(0.33, 0.33, 0.33, 1);
// }
// else if (index == 1)
// {
//     color = float4(0, 1, 0, 1);
// }
// else if (index == 2)
// {
//     color = float4(0, 0, 1, 1);
// }
// else if (index == 3)
// {
//     color = float4(1, 1, 0, 1);
// }
// else if (index == 4)
// {
//     color = float4(1, 0, 0, 1);
// }
// else if (index == 5)
// {
//     color = float4(1, 0, 1, 1);
// }
// else if (index == 6)
// {
//     color = float4(0, 1, 1, 1);
// }
// else
// {
//     color = float4(0, 0, 1, 1);
// }
// if (id < 8000)
// {
//     color = float4(1, 1, 1, 1);
// }

//float3 position;
//vertOutput.instanceID = v.instanceID;
//float4 color = float4(vertOutput.color.x, vertOutput.color.y, vertOutput.color.z, vertOutput.color.w);
//return color;
//#if defined(UNITY_PROCEDURAL_INSTANCING_ENABLED)
//float3 vertex = v.vertex; //pointer.vertex; //  v.vertex
//float3 worldPosition = instanceData.position;
//vertOutput.vertex = mul(UNITY_MATRIX_VP, float4(vertex + worldPosition, 1.0f));
//vertOutput.color = v.color; // pointer.color.xyz; // float3(pointer.color.x, pointer.color.y, pointer.color.z);
//vertOutput.color = float3(1, 0, 0);
//#else
//vertOutput.vertex = float4(0, 0, 0, 0);
//vertOutput.color = float3(1, 0, 0);
//#endif
//vertOutput.vertex = UnityObjectToClipPos(v.vertex); // draws it using the bounds
//o.color = v.color;
//VertInput v, uint instanceID : SV_InstanceID)
//  UnityObjectToClipPos(v.vertex);
//UNITY_FOG_COORDS(1)

//position = float3(2, 0, 0); // _vertexes[unity_InstanceID];
// // sample the texture
// fixed4 col = tex2D(_MainTex, i.uv);
// // apply fog
// UNITY_APPLY_FOG(i.fogCoord, col);
// return col;
// #if defined(UNITY_PROCEDURAL_INSTANCING_ENABLED)
// return float4(1, 0, 0, 1);
// #else
// return float4(0, 0.5, 0.5, 1);
// #endif
// void setup()
// {
// #ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
//     float3 data = vertexes[unity_InstanceID];
//     float size = 1;
//     unity_ObjectToWorld._11_21_31_41 = float4(size, 0, 0, 0);
//     unity_ObjectToWorld._12_22_32_42 = float4(0, size, 0, 0);
//     unity_ObjectToWorld._13_23_33_43 = float4(0, 0, size, 0);
//     unity_ObjectToWorld._14_24_34_44 = float4(data, 1);
//     unity_WorldToObject = unity_ObjectToWorld;
//     unity_WorldToObject._14_24_34 *= -1;
//     unity_WorldToObject._11_22_33 = 1.0f / unity_WorldToObject._11_22_33;
// #endif
// }
//position = float3(2, 0, 0); // _vertexes[unity_InstanceID];
//o.vertex = UnityObjectToClipPos();
//o.vertex = v.vertex + float4(3, 0, 0, 0);
//float3 position = _vertexes[unity_InstanceID];
//unity_ObjectToWorld = 0.0;
//unity_ObjectToWorld._m03_m13_m23_m33 = float4(position, 1.0);
//o.uv = TRANSFORM_TEX(v.uv, _MainTex);
//UNITY_TRANSFER_FOG(o,o.vertex);