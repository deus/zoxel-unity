﻿Shader "zoxel/characterAnimated_unstructured"
{
    SubShader
    {              
        //Tags { "RenderType"="Transparent" "Queue"="Transparent" }
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            //Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_instancing
            #pragma multi_compile_fog
		    // #pragma target 4.5
            #include "UnityCG.cginc"
            //#ifdef UNITY_PROCEDURAL_INSTANCING_ENABLED
            uniform float4 color; // : register(t1);
            uniform float4 additionColor; // : register(t2);
            uniform StructuredBuffer<float4x4> currentBones; // : register(t2);
            uniform StructuredBuffer<float4x4> originalBones; // : register(t3);
            uniform StructuredBuffer<uint> boneIndexes; // : register(t4);
            //#endif

            struct VertInput
            {
                float4 vertex : POSITION;
                float3 color : COLOR;
            };

            struct VertOutput
            {
                float4 vertex : SV_POSITION;
                float4 color : COLOR;
                UNITY_FOG_COORDS(1)
            };

            VertOutput vert(VertInput v, uint id : SV_VertexID)   //  SV_Vertex
            {
                VertOutput vertOutput;
                uint index = boneIndexes[id];
                float4 color2 = float4(v.color, 1);
                color2 *= color;
                color2 += additionColor;
                vertOutput.color = color2;
                float3 vertex = v.vertex.xyz;
                float4x4 originalInverseMatrix = originalBones[index];
                float4x4 currentMatrix = currentBones[index];
                float4 vertex4 = float4(vertex, 1);
                // draws it using the bounds or matrix suppled by Graphics class
                float4 boneVertex = mul(currentMatrix, mul(originalInverseMatrix, vertex4));
                vertOutput.vertex = UnityObjectToClipPos(boneVertex);
                UNITY_TRANSFER_FOG(vertOutput, vertOutput.vertex);
                return vertOutput;
            }

            fixed4 frag (VertOutput vertOutput) : SV_Target
            {
                float4 color2 = vertOutput.color;
                UNITY_APPLY_FOG(vertOutput.fogCoord, color2);
                return color2;
            }
            ENDCG
        }
    }
}